﻿// Decompiled with JetBrains decompiler
// Type: UI2DSprite
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/UI/NGUI Unity2D Sprite")]
public class UI2DSprite : UIBasicSprite
{
  [HideInInspector]
  [SerializeField]
  private Vector4 mBorder = Vector4.zero;
  [NonSerialized]
  private int mPMA = -1;
  [HideInInspector]
  [SerializeField]
  private UnityEngine.Sprite mSprite;
  [HideInInspector]
  [SerializeField]
  private Material mMat;
  [SerializeField]
  [HideInInspector]
  private Shader mShader;
  [HideInInspector]
  [SerializeField]
  private bool mFixedAspect;
  public UnityEngine.Sprite nextSprite;

  public UnityEngine.Sprite sprite2D
  {
    get
    {
      return this.mSprite;
    }
    set
    {
      if (!((UnityEngine.Object) this.mSprite != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mSprite = value;
      this.nextSprite = (UnityEngine.Sprite) null;
      this.CreatePanel();
    }
  }

  public override Material material
  {
    get
    {
      return this.mMat;
    }
    set
    {
      if (!((UnityEngine.Object) this.mMat != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mMat = value;
      this.mPMA = -1;
      this.MarkAsChanged();
    }
  }

  public override Shader shader
  {
    get
    {
      if ((UnityEngine.Object) this.mMat != (UnityEngine.Object) null)
        return this.mMat.shader;
      if ((UnityEngine.Object) this.mShader == (UnityEngine.Object) null)
        this.mShader = Shader.Find("Unlit/Transparent Colored");
      return this.mShader;
    }
    set
    {
      if (!((UnityEngine.Object) this.mShader != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mShader = value;
      if (!((UnityEngine.Object) this.mMat == (UnityEngine.Object) null))
        return;
      this.mPMA = -1;
      this.MarkAsChanged();
    }
  }

  public override Texture mainTexture
  {
    get
    {
      if ((UnityEngine.Object) this.mSprite != (UnityEngine.Object) null)
        return (Texture) this.mSprite.texture;
      if ((UnityEngine.Object) this.mMat != (UnityEngine.Object) null)
        return this.mMat.mainTexture;
      return (Texture) null;
    }
  }

  public override bool premultipliedAlpha
  {
    get
    {
      if (this.mPMA == -1)
      {
        Shader shader = this.shader;
        this.mPMA = !((UnityEngine.Object) shader != (UnityEngine.Object) null) || !shader.name.Contains("Premultiplied") ? 0 : 1;
      }
      return this.mPMA == 1;
    }
  }

  public override Vector4 drawingDimensions
  {
    get
    {
      Vector2 pivotOffset = this.pivotOffset;
      float from1 = -pivotOffset.x * (float) this.mWidth;
      float from2 = -pivotOffset.y * (float) this.mHeight;
      float to1 = from1 + (float) this.mWidth;
      float to2 = from2 + (float) this.mHeight;
      if ((UnityEngine.Object) this.mSprite != (UnityEngine.Object) null && this.mType != UIBasicSprite.Type.Tiled)
      {
        int int1 = Mathf.RoundToInt(this.mSprite.rect.width);
        int int2 = Mathf.RoundToInt(this.mSprite.rect.height);
        int int3 = Mathf.RoundToInt(this.mSprite.textureRectOffset.x);
        int int4 = Mathf.RoundToInt(this.mSprite.textureRectOffset.y);
        int int5 = Mathf.RoundToInt(this.mSprite.rect.width - this.mSprite.textureRect.width - this.mSprite.textureRectOffset.x);
        int int6 = Mathf.RoundToInt(this.mSprite.rect.height - this.mSprite.textureRect.height - this.mSprite.textureRectOffset.y);
        float num1 = 1f;
        float num2 = 1f;
        if (int1 > 0 && int2 > 0 && (this.mType == UIBasicSprite.Type.Simple || this.mType == UIBasicSprite.Type.Filled))
        {
          if ((int1 & 1) != 0)
            ++int5;
          if ((int2 & 1) != 0)
            ++int6;
          num1 = 1f / (float) int1 * (float) this.mWidth;
          num2 = 1f / (float) int2 * (float) this.mHeight;
        }
        if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
        {
          from1 += (float) int5 * num1;
          to1 -= (float) int3 * num1;
        }
        else
        {
          from1 += (float) int3 * num1;
          to1 -= (float) int5 * num1;
        }
        if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
        {
          from2 += (float) int6 * num2;
          to2 -= (float) int4 * num2;
        }
        else
        {
          from2 += (float) int4 * num2;
          to2 -= (float) int6 * num2;
        }
      }
      float num3;
      float num4;
      if (this.mFixedAspect)
      {
        num3 = 0.0f;
        num4 = 0.0f;
      }
      else
      {
        Vector4 border = this.border;
        num3 = border.x + border.z;
        num4 = border.y + border.w;
      }
      return new Vector4(Mathf.Lerp(from1, to1 - num3, this.mDrawRegion.x), Mathf.Lerp(from2, to2 - num4, this.mDrawRegion.y), Mathf.Lerp(from1 + num3, to1, this.mDrawRegion.z), Mathf.Lerp(from2 + num4, to2, this.mDrawRegion.w));
    }
  }

  public override Vector4 border
  {
    get
    {
      return this.mBorder;
    }
    set
    {
      if (!(this.mBorder != value))
        return;
      this.mBorder = value;
      this.MarkAsChanged();
    }
  }

  protected override void OnUpdate()
  {
    if ((UnityEngine.Object) this.nextSprite != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) this.nextSprite != (UnityEngine.Object) this.mSprite)
        this.sprite2D = this.nextSprite;
      this.nextSprite = (UnityEngine.Sprite) null;
    }
    base.OnUpdate();
    if (!this.mFixedAspect || !((UnityEngine.Object) this.mainTexture != (UnityEngine.Object) null))
      return;
    int int1 = Mathf.RoundToInt(this.mSprite.rect.width);
    int int2 = Mathf.RoundToInt(this.mSprite.rect.height);
    int int3 = Mathf.RoundToInt(this.mSprite.textureRectOffset.x);
    int int4 = Mathf.RoundToInt(this.mSprite.textureRectOffset.y);
    int int5 = Mathf.RoundToInt(this.mSprite.rect.width - this.mSprite.textureRect.width - this.mSprite.textureRectOffset.x);
    int int6 = Mathf.RoundToInt(this.mSprite.rect.height - this.mSprite.textureRect.height - this.mSprite.textureRectOffset.y);
    int num1 = int1 + (int3 + int5);
    int num2 = int2 + (int6 + int4);
    float num3 = (float) this.mWidth;
    float num4 = (float) this.mHeight;
    float num5 = num3 / num4;
    float num6 = (float) num1 / (float) num2;
    if ((double) num6 < (double) num5)
    {
      float x = (float) (((double) num3 - (double) num4 * (double) num6) / (double) num3 * 0.5);
      this.drawRegion = new Vector4(x, 0.0f, 1f - x, 1f);
    }
    else
    {
      float y = (float) (((double) num4 - (double) num3 / (double) num6) / (double) num4 * 0.5);
      this.drawRegion = new Vector4(0.0f, y, 1f, 1f - y);
    }
  }

  public override void MakePixelPerfect()
  {
    base.MakePixelPerfect();
    if (this.mType == UIBasicSprite.Type.Tiled)
      return;
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null || this.mType != UIBasicSprite.Type.Simple && this.mType != UIBasicSprite.Type.Filled && this.hasBorder || !((UnityEngine.Object) mainTexture != (UnityEngine.Object) null))
      return;
    Rect rect = this.mSprite.rect;
    int int1 = Mathf.RoundToInt(rect.width);
    int int2 = Mathf.RoundToInt(rect.height);
    if ((int1 & 1) == 1)
      ++int1;
    if ((int2 & 1) == 1)
      ++int2;
    this.width = int1;
    this.height = int2;
  }

  public override void OnFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
      return;
    Rect textureRect = this.mSprite.textureRect;
    Rect inner = textureRect;
    Vector4 border = this.border;
    inner.xMin += border.x;
    inner.yMin += border.y;
    inner.xMax -= border.z;
    inner.yMax -= border.w;
    float num1 = 1f / (float) mainTexture.width;
    float num2 = 1f / (float) mainTexture.height;
    textureRect.xMin *= num1;
    textureRect.xMax *= num1;
    textureRect.yMin *= num2;
    textureRect.yMax *= num2;
    inner.xMin *= num1;
    inner.xMax *= num1;
    inner.yMin *= num2;
    inner.yMax *= num2;
    int bufferOffset = verts.size;
    this.Fill(verts, uvs, cols, textureRect, inner);
    if (this.onPostFill == null)
      return;
    this.onPostFill((UIWidget) this, bufferOffset, verts, uvs, cols);
  }
}
