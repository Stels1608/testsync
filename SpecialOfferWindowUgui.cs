﻿// Decompiled with JetBrains decompiler
// Type: SpecialOfferWindowUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpecialOfferWindowUgui : SpecialOfferWindowUi
{
  [SerializeField]
  private Image offerIcon;
  [SerializeField]
  private TextMeshProUGUI headerText;
  [SerializeField]
  private TextMeshProUGUI timerText;
  [SerializeField]
  private Image offerImage;
  [SerializeField]
  private TextMeshProUGUI mainText;
  [SerializeField]
  private TextMeshProUGUI okButtonText;

  protected override void SetOkButtonText(string text)
  {
    this.okButtonText.text = text;
  }

  protected override void SetMainText(string text)
  {
    this.mainText.text = text;
  }

  protected override void SetOfferIcon(UnityEngine.Sprite iconSprite)
  {
    this.offerIcon.sprite = iconSprite;
  }

  protected override void SetHeaderText(string text)
  {
    this.headerText.text = text;
  }

  protected override void SetTimerText(string text)
  {
    this.timerText.text = text;
  }

  protected override void SetOfferImage(UnityEngine.Sprite offerSprite)
  {
    this.offerImage.sprite = offerSprite;
  }
}
