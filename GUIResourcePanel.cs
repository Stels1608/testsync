﻿// Decompiled with JetBrains decompiler
// Type: GUIResourcePanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System;
using UnityEngine;

public class GUIResourcePanel : GUIPanel
{
  private const float SPACE = 3f;
  private GUIImageNew tokenImage;
  private GUIImageNew cubitsImage;
  private GUIImageNew tyliumImage;
  private GUIImageNew waterImage;
  private GUIImageNew titaniumImage;
  private GUILabelNew tokenLabel;
  private GUILabelNew cubitsLabel;
  private GUILabelNew tyliumLabel;
  private GUILabelNew waterLabel;
  private GUILabelNew titaniumLabel;

  public GUIResourcePanel(SmartRect parent)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GUIResourcePanel.\u003CGUIResourcePanel\u003Ec__AnonStoreyB3 panelCAnonStoreyB3 = new GUIResourcePanel.\u003CGUIResourcePanel\u003Ec__AnonStoreyB3();
    // ISSUE: explicit constructor call
    base.\u002Ector(parent);
    // ISSUE: reference to a compiler-generated field
    panelCAnonStoreyB3.\u003C\u003Ef__this = this;
    this.IsRendered = true;
    this.IsUpdated = true;
    JWindowDescription jwindowDescription = Loaders.LoadResource("GUI/gui_resources_panel_layout");
    // ISSUE: reference to a compiler-generated field
    panelCAnonStoreyB3.tokenGui = Game.Catalogue.FetchCard(130920111U, CardView.GUI) as GUICard;
    this.tokenImage = (jwindowDescription["cubits_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.tokenImage);
    this.tokenLabel = (jwindowDescription["cubits_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.tokenLabel);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    panelCAnonStoreyB3.tokenGui.IsLoaded.AddHandler(new SignalHandler(panelCAnonStoreyB3.\u003C\u003Em__183));
    // ISSUE: reference to a compiler-generated field
    panelCAnonStoreyB3.cubitGui = Game.Catalogue.FetchCard(264733124U, CardView.GUI) as GUICard;
    this.cubitsImage = (jwindowDescription["cubits_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.cubitsImage);
    this.cubitsLabel = (jwindowDescription["cubits_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.cubitsLabel);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    panelCAnonStoreyB3.cubitGui.IsLoaded.AddHandler(new SignalHandler(panelCAnonStoreyB3.\u003C\u003Em__184));
    // ISSUE: reference to a compiler-generated field
    panelCAnonStoreyB3.tyliumGui = Game.Catalogue.FetchCard(215278030U, CardView.GUI) as GUICard;
    this.tyliumImage = (jwindowDescription["tylium_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.tyliumImage);
    this.tyliumLabel = (jwindowDescription["tylium_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.tyliumLabel);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    panelCAnonStoreyB3.tyliumGui.IsLoaded.AddHandler(new SignalHandler(panelCAnonStoreyB3.\u003C\u003Em__185));
    // ISSUE: reference to a compiler-generated field
    panelCAnonStoreyB3.waterGui = Game.Catalogue.FetchCard(130762195U, CardView.GUI) as GUICard;
    this.waterImage = (jwindowDescription["water_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.waterImage);
    this.waterLabel = (jwindowDescription["water_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.waterLabel);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    panelCAnonStoreyB3.waterGui.IsLoaded.AddHandler(new SignalHandler(panelCAnonStoreyB3.\u003C\u003Em__186));
    // ISSUE: reference to a compiler-generated field
    panelCAnonStoreyB3.titaniumGui = Game.Catalogue.FetchCard(207047790U, CardView.GUI) as GUICard;
    this.titaniumImage = (jwindowDescription["titanium_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.titaniumImage);
    this.titaniumLabel = (jwindowDescription["titanium_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.titaniumLabel);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    panelCAnonStoreyB3.titaniumGui.IsLoaded.AddHandler(new SignalHandler(panelCAnonStoreyB3.\u003C\u003Em__187));
    this.UpdateData();
  }

  public override void Update()
  {
    this.UpdateData();
    base.Update();
  }

  protected void UpdateData()
  {
    this.tokenLabel.Text = Game.Me.Hold.Token.ToString();
    this.cubitsLabel.Text = Game.Me.Hold.Cubits.ToString();
    this.tyliumLabel.Text = Game.Me.Hold.Tylium.ToString();
    this.waterLabel.Text = Game.Me.Hold.Water.ToString();
    this.titaniumLabel.Text = Game.Me.Hold.Titanium.ToString();
    this.tokenLabel.MakeCenteredRect();
    this.cubitsLabel.MakeCenteredRect();
    this.tyliumLabel.MakeCenteredRect();
    this.waterLabel.MakeCenteredRect();
    this.titaniumLabel.MakeCenteredRect();
    try
    {
      Texture2D texture2D = ResourceLoader.Load<Texture2D>((Game.Catalogue.FetchCard(130920111U, CardView.GUI) as GUICard).GUIIcon);
      this.tokenImage.Texture = texture2D;
      this.tokenImage.Size = new float2((float) texture2D.width, (float) texture2D.height);
      this.cubitsImage.Texture = ResourceLoader.Load<Texture2D>((Game.Catalogue.FetchCard(264733124U, CardView.GUI) as GUICard).GUIIcon);
      this.tyliumImage.Texture = ResourceLoader.Load<Texture2D>((Game.Catalogue.FetchCard(215278030U, CardView.GUI) as GUICard).GUIIcon);
      this.waterImage.Texture = ResourceLoader.Load<Texture2D>((Game.Catalogue.FetchCard(130762195U, CardView.GUI) as GUICard).GUIIcon);
      this.titaniumImage.Texture = ResourceLoader.Load<Texture2D>((Game.Catalogue.FetchCard(207047790U, CardView.GUI) as GUICard).GUIIcon);
    }
    catch (Exception ex)
    {
    }
    this.RecalcSize();
    this.tokenImage.Position = new float2((float) (-(double) this.root.Width / 2.0 + (double) this.cubitsImage.SmartRect.Width / 2.0), 0.0f);
    this.tokenLabel.PlaceRightOf((GUIPanel) this.tokenImage, 3f);
    this.cubitsImage.PlaceRightOf((GUIPanel) this.tokenLabel, 3f);
    this.cubitsLabel.PlaceRightOf((GUIPanel) this.cubitsImage, 3f);
    this.tyliumImage.PlaceRightOf((GUIPanel) this.cubitsLabel, 3f);
    this.tyliumLabel.PlaceRightOf((GUIPanel) this.tyliumImage, 3f);
    this.waterImage.PlaceRightOf((GUIPanel) this.tyliumLabel, 3f);
    this.waterLabel.PlaceRightOf((GUIPanel) this.waterImage, 3f);
    this.titaniumImage.PlaceRightOf((GUIPanel) this.waterLabel, 3f);
    this.titaniumLabel.PlaceRightOf((GUIPanel) this.titaniumImage, 3f);
    this.RecalculateAbsCoords();
  }

  protected void RecalcSize()
  {
    this.root.Width = (float) ((double) this.tokenImage.SmartRect.Width + 3.0 + (double) this.tokenLabel.SmartRect.Width + 3.0) + (float) ((double) this.cubitsImage.SmartRect.Width + 3.0 + (double) this.cubitsLabel.SmartRect.Width + 3.0) + (float) ((double) this.tyliumImage.SmartRect.Width + 3.0 + (double) this.tyliumLabel.SmartRect.Width + 3.0) + (float) ((double) this.waterImage.SmartRect.Width + 3.0 + (double) this.waterLabel.SmartRect.Width + 3.0) + (float) ((double) this.titaniumImage.SmartRect.Width + 3.0 + (double) this.titaniumLabel.SmartRect.Width + 3.0);
    this.root.Height = this.tokenImage.SmartRect.Height;
  }
}
