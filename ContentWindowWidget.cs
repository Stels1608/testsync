﻿// Decompiled with JetBrains decompiler
// Type: ContentWindowWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class ContentWindowWidget : WindowWidget
{
  [SerializeField]
  private WindowTitleBarWidget titleBar;
  [SerializeField]
  private WindowCloseButtonWidget closeButton;
  [SerializeField]
  private GameObject inactiveOverlay;
  private UIPanel[] panelList;
  public WindowTypes type;

  public WindowTitleBarWidget TitleBar
  {
    get
    {
      return this.titleBar;
    }
    set
    {
      this.titleBar = value;
    }
  }

  public string WindowTitle
  {
    get
    {
      return this.TitleBar.WindowTitle;
    }
    set
    {
      this.TitleBar.WindowTitle = value;
    }
  }

  public WindowCloseButtonWidget CloseButton
  {
    get
    {
      return this.closeButton;
    }
    set
    {
      this.closeButton = value;
    }
  }

  public GameObject InactiveOverlay
  {
    get
    {
      return this.inactiveOverlay;
    }
    set
    {
      this.inactiveOverlay = value;
    }
  }

  public int WindowDepth
  {
    get
    {
      return this.GetComponent<UIPanel>().depth;
    }
    set
    {
      UIPanel component = this.GetComponent<UIPanel>();
      int depth = component.depth;
      component.depth = value;
      if (this.panelList == null)
        this.panelList = this.GetComponentsInChildren<UIPanel>(true);
      int num = component.depth - depth;
      foreach (UIPanel panel in this.panelList)
      {
        if (!((Object) panel.gameObject == (Object) this.gameObject))
          panel.depth += num;
      }
    }
  }

  public bool HasFocus
  {
    get
    {
      return this.WindowManager.WindowHasFocus(this);
    }
  }

  public ContentWindowWidget()
  {
    this.OpenOnStart = false;
    this.IsOpen = false;
  }

  public virtual void OnScreenResize()
  {
  }

  public void BringToFront()
  {
    this.WindowManager.BringContentWindowToFront(this);
    if (!((Object) this.InactiveOverlay != (Object) null) || !this.InactiveOverlay.activeSelf)
      return;
    this.InactiveOverlay.SetActive(false);
  }

  public void SetFocus()
  {
    this.WindowManager.SetFocus(this);
  }

  public void RedrawWindow()
  {
    if (this.HasWindowSizeChanged())
      ;
  }

  public override void OnPress(bool isDown)
  {
    base.OnPress(isDown);
    if (!isDown)
      return;
    this.BringToFront();
  }

  public override void SetSize(float width, float height)
  {
    if (!this.IsResizable)
      return;
    base.SetSize(width, height);
  }

  public override void OnWindowOpen()
  {
    base.OnWindowOpen();
    if (this.WindowManager != null)
      this.WindowManager.RegisterContentWindow(this);
    this.BringToFront();
    PositionUtils.SetUIPosition(this.gameObject, true, UIWidget.Pivot.Center);
  }

  public override void OnWindowClose()
  {
    base.OnWindowClose();
    if (this.WindowManager == null)
      return;
    this.WindowManager.UnregisterContentWindow(this);
  }

  public virtual void OnFocus()
  {
    if (!((Object) this.InactiveOverlay != (Object) null))
      return;
    this.InactiveOverlay.SetActive(false);
  }

  public virtual void OnBlur()
  {
    if ((Object) this.InactiveOverlay == (Object) null)
      return;
    this.InactiveOverlay.SetActive(true);
  }

  public override void ReloadLanguageData()
  {
    this.TitleBar.ReloadLanguageData();
  }
}
