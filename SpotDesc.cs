﻿// Decompiled with JetBrains decompiler
// Type: SpotDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpotDesc : IProtocolRead
{
  public ushort ObjectPointServerHash;
  public string ObjectPointName;
  public SpotType Type;
  public Vector3 LocalPosition;
  public Quaternion LocalRotation;

  public void Read(BgoProtocolReader r)
  {
    this.ObjectPointServerHash = r.ReadUInt16();
    this.ObjectPointName = r.ReadString();
    this.Type = (SpotType) r.ReadByte();
    this.LocalPosition = r.ReadVector3();
    this.LocalRotation = r.ReadQuaternion();
  }
}
