﻿// Decompiled with JetBrains decompiler
// Type: RadioButtonGroupWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class RadioButtonGroupWidget : NguiWidget
{
  [SerializeField]
  protected List<RadioButtonWidget> buttons;
  [SerializeField]
  public RadioButtonWidget activeRadioButton;
  public AnonymousDelegate OnValueChanged;

  protected List<RadioButtonWidget> Buttons
  {
    get
    {
      return this.buttons;
    }
    set
    {
      this.buttons = value;
    }
  }

  public bool SelectNoneAtStart { get; set; }

  public RadioButtonGroupWidget()
  {
    this.Buttons = new List<RadioButtonWidget>();
  }

  public override void Start()
  {
    base.Start();
    if (this.Buttons.Count == 0 || this.SelectNoneAtStart || !((Object) this.activeRadioButton == (Object) null) && !((Object) this.activeRadioButton == (Object) this.Buttons[0]))
      return;
    this.SetActiveButtonSilent(this.Buttons[0]);
    this.SetActiveButton(this.Buttons[0]);
  }

  protected void InitializeButtons()
  {
    for (int index = 0; index < this.transform.childCount; ++index)
    {
      RadioButtonWidget component = this.transform.GetChild(index).gameObject.GetComponent<RadioButtonWidget>();
      if ((Object) null != (Object) component)
        this.AddRadioButton(component);
    }
  }

  public void AddRadioButton(RadioButtonWidget radioButton)
  {
    if (!((Object) null != (Object) radioButton))
      return;
    this.Buttons.Add(radioButton);
    radioButton.radioButtonGroup = this;
  }

  private void RemoveRadioButton(RadioButtonWidget radioButton)
  {
    if (!this.Buttons.Contains(radioButton))
      return;
    this.Buttons.Remove(radioButton);
  }

  public void SetActiveButton(RadioButtonWidget radioButton)
  {
    if (!((Object) null != (Object) radioButton) || !((Object) radioButton != (Object) this.activeRadioButton))
      return;
    if ((Object) this.activeRadioButton != (Object) null)
      this.activeRadioButton.IsActive = false;
    radioButton.IsActive = true;
    this.activeRadioButton = radioButton;
    if (this.OnValueChanged == null)
      return;
    this.OnValueChanged();
  }

  public void SetActiveButtonSilent(int index)
  {
    if (index > this.buttons.Count - 1)
      return;
    this.SetActiveButtonSilent(this.Buttons[index]);
  }

  public void SetActiveButtonSilent(RadioButtonWidget radioButton)
  {
    if (!((Object) null != (Object) radioButton) || !((Object) radioButton != (Object) this.activeRadioButton))
      return;
    if ((Object) this.activeRadioButton != (Object) null)
      this.activeRadioButton.IsActive = false;
    radioButton.IsActive = true;
    this.activeRadioButton = radioButton;
  }

  public void SetActiveButton(int index)
  {
    if (index > this.buttons.Count - 1)
      return;
    this.SetActiveButton(this.Buttons[index]);
  }

  public int GetIndexOfActiveButton()
  {
    return this.Buttons.IndexOf(this.activeRadioButton);
  }
}
