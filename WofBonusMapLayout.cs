﻿// Decompiled with JetBrains decompiler
// Type: WofBonusMapLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public abstract class WofBonusMapLayout
{
  protected int maxParts;
  protected int bonusMapId;
  protected Dictionary<ushort, Vector2> mapPositions;
  protected string mapName;
  protected WofBonusMapDifficulty difficulty;
  protected int requiredLevel;

  public int BonusMapId
  {
    get
    {
      return this.bonusMapId;
    }
  }

  public Dictionary<ushort, Vector2> MapPositions
  {
    get
    {
      return this.mapPositions;
    }
  }

  public int MaxParts
  {
    get
    {
      return this.maxParts;
    }
  }

  public string MapName
  {
    get
    {
      return this.mapName;
    }
  }

  public WofBonusMapDifficulty Difficulty
  {
    get
    {
      return this.difficulty;
    }
  }

  public int RequiredLevel
  {
    get
    {
      return this.requiredLevel;
    }
  }
}
