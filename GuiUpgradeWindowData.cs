﻿// Decompiled with JetBrains decompiler
// Type: GuiUpgradeWindowData
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class GuiUpgradeWindowData
{
  private readonly ushort slotId;
  private readonly ShipSystem system;

  public ShipSystem Item
  {
    get
    {
      if (this.system == null)
        return Game.Me.Hangar.ActiveShip.GetSlot(this.slotId).System;
      return this.system.Container.GetByID(this.system.ServerID) as ShipSystem;
    }
  }

  public GuiUpgradeWindowData(ShipSystem system)
  {
    this.system = system;
  }

  public GuiUpgradeWindowData(ushort slotId)
  {
    this.slotId = slotId;
  }
}
