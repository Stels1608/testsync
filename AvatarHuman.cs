﻿// Decompiled with JetBrains decompiler
// Type: AvatarHuman
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class AvatarHuman : Avatar
{
  public override void ParseProperty(AvatarProperty prop)
  {
    if (!(prop.name == "helmet") || prop.value.Contains("empty"))
      return;
    this.propManager.Deactivate("hair");
    this.propManager.Deactivate("glasses");
  }

  public override string GetItemNameByTexProp(string texProp)
  {
    if (texProp == AvatarInfo.GetTextureFromItem("faces"))
      return "head";
    return base.GetItemNameByTexProp(texProp);
  }
}
