﻿// Decompiled with JetBrains decompiler
// Type: AvatarPropertyManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AvatarPropertyManager
{
  private List<AvatarLoadProperty> load = new List<AvatarLoadProperty>();
  private List<AvatarTypingProperty> used = new List<AvatarTypingProperty>();
  private List<string> deactive = new List<string>();
  private Avatar avatar;

  public bool IsReady
  {
    get
    {
      using (List<AvatarLoadProperty>.Enumerator enumerator = this.load.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (!enumerator.Current.IsLoaded)
            return false;
        }
      }
      return true;
    }
  }

  public bool HaveLoadProperties
  {
    get
    {
      return this.load.Count > 0;
    }
  }

  public AvatarPropertyManager(Avatar avatar)
  {
    this.avatar = avatar;
  }

  public void Use()
  {
    if (!this.IsReady)
      return;
    using (List<AvatarLoadProperty>.Enumerator enumerator = this.load.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Use(this.avatar.obj.transform);
    }
    this.load.Clear();
  }

  public void ActiveAll()
  {
    using (List<string>.Enumerator enumerator = this.deactive.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.load.Add(new AvatarLoadProperty(this.avatar, this.FindProperty(enumerator.Current)));
    }
    this.deactive.Clear();
  }

  public void Deactivate(string property)
  {
    AvatarTypingProperty typing = this.GetTyping(new AvatarProperty(property, string.Empty));
    if (typing.type != AvatarPropertyType.Object)
      return;
    this.load.Add(new AvatarLoadProperty(this.avatar, typing));
    this.deactive.Add(property);
  }

  public void AddProperties(List<AvatarProperty> properties)
  {
    List<AvatarTypingProperty> typings = this.GetTypings(properties);
    this.ReplaceProperties(typings);
    this.ParseProperties(this.avatar, this.used);
    this.load.AddRange((IEnumerable<AvatarLoadProperty>) this.CreateLoads(this.avatar, typings));
  }

  private void ParseProperties(Avatar avatar, List<AvatarTypingProperty> properties)
  {
    this.ActiveAll();
    using (List<AvatarTypingProperty>.Enumerator enumerator = properties.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AvatarTypingProperty current = enumerator.Current;
        avatar.ParseProperty(current.prop);
      }
    }
  }

  public AvatarProperty GetProperty(string name)
  {
    AvatarTypingProperty property = this.FindProperty(name);
    if (property != null)
      return property.prop;
    return (AvatarProperty) null;
  }

  private List<AvatarLoadProperty> CreateLoads(Avatar avatar, List<AvatarTypingProperty> properties)
  {
    List<AvatarLoadProperty> loadProperties = this.GetLoadProperties(properties);
    List<AvatarLoadProperty> avatarLoadPropertyList = new List<AvatarLoadProperty>();
    using (List<AvatarLoadProperty>.Enumerator enumerator = loadProperties.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AvatarLoadProperty current = enumerator.Current;
        if (!this.deactive.Contains(current.typing.prop.name))
          avatarLoadPropertyList.Add(current);
      }
    }
    using (List<AvatarLoadProperty>.Enumerator enumerator = avatarLoadPropertyList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AvatarLoadProperty current = enumerator.Current;
        string str = current.typing.prop.name;
        AvatarPropertyType type = current.typing.type;
        string name;
        switch (type)
        {
          case AvatarPropertyType.Material:
            name = avatar.GetItemNameByMatProp(str);
            break;
          case AvatarPropertyType.Texture:
            name = avatar.GetItemNameByTexProp(str);
            break;
          default:
            continue;
        }
        AvatarTypingProperty property = this.FindProperty(name);
        string objValue = name;
        if (property != null)
          objValue = property.prop.value;
        current.useOn = this.FindObjForMatOrTex(avatar.obj.transform, objValue, type);
        current.Load();
      }
    }
    return avatarLoadPropertyList;
  }

  private Object FindObjForMatOrTex(Transform tr, string objValue, AvatarPropertyType type)
  {
    for (int index = 0; index < tr.transform.childCount; ++index)
    {
      Object objForMatOrTex = this.FindObjForMatOrTex(tr.transform.GetChild(index), objValue, type);
      if (objForMatOrTex != (Object) null)
        return objForMatOrTex;
    }
    if (tr.name.Contains(objValue) && (Object) tr.GetComponent<Renderer>() != (Object) null)
    {
      if (this.avatar.CheckBadObjectName(tr.name))
        return (Object) null;
      if (type == AvatarPropertyType.Material)
        return (Object) tr.GetComponent<Renderer>();
      if (type == AvatarPropertyType.Texture)
        return (Object) tr.GetComponent<Renderer>().material;
    }
    return (Object) null;
  }

  private List<AvatarLoadProperty> GetLoadProperties(List<AvatarTypingProperty> typing)
  {
    List<AvatarLoadProperty> avatarLoadPropertyList = new List<AvatarLoadProperty>();
    using (List<AvatarTypingProperty>.Enumerator enumerator = typing.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AvatarLoadProperty avatarLoadProperty = new AvatarLoadProperty(this.avatar, enumerator.Current);
        avatarLoadPropertyList.Add(avatarLoadProperty);
      }
    }
    return avatarLoadPropertyList;
  }

  private List<AvatarTypingProperty> GetTypings(List<AvatarProperty> properties)
  {
    List<AvatarTypingProperty> avatarTypingPropertyList = new List<AvatarTypingProperty>();
    using (List<AvatarProperty>.Enumerator enumerator = properties.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AvatarProperty current = enumerator.Current;
        avatarTypingPropertyList.Add(this.GetTyping(current));
      }
    }
    return avatarTypingPropertyList;
  }

  private AvatarTypingProperty GetTyping(AvatarProperty property)
  {
    return new AvatarTypingProperty() { prop = property, type = this.GetType(property.name) };
  }

  public AvatarPropertyType GetType(string name)
  {
    if (AvatarInfo.GetItemsOfRace(this.avatar.Race).Contains(name))
      return AvatarPropertyType.Object;
    return AvatarInfo.GetMaterialsOfRace(this.avatar.Race).Contains(name) ? AvatarPropertyType.Material : AvatarPropertyType.Texture;
  }

  private List<AvatarTypingProperty> GetPropFromType(List<AvatarTypingProperty> typings, AvatarPropertyType type)
  {
    List<AvatarTypingProperty> avatarTypingPropertyList = new List<AvatarTypingProperty>();
    using (List<AvatarTypingProperty>.Enumerator enumerator = typings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AvatarTypingProperty current = enumerator.Current;
        if (current.type == type)
        {
          AvatarTypingProperty avatarTypingProperty1 = new AvatarTypingProperty();
          AvatarTypingProperty avatarTypingProperty2 = current;
          avatarTypingPropertyList.Add(avatarTypingProperty2);
        }
      }
    }
    return avatarTypingPropertyList;
  }

  private List<AvatarTypingProperty> GetItems(List<AvatarTypingProperty> typings)
  {
    return this.GetPropFromType(typings, AvatarPropertyType.Object);
  }

  private List<AvatarTypingProperty> GetMaterials(List<AvatarTypingProperty> typings)
  {
    return this.GetPropFromType(typings, AvatarPropertyType.Material);
  }

  private List<AvatarTypingProperty> GetTextures(List<AvatarTypingProperty> typings)
  {
    return this.GetPropFromType(typings, AvatarPropertyType.Texture);
  }

  private void ReplaceProperties(List<AvatarTypingProperty> items)
  {
    using (List<AvatarTypingProperty>.Enumerator enumerator = items.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.Replace(enumerator.Current);
    }
  }

  private AvatarTypingProperty FindProperty(string name)
  {
    using (List<AvatarTypingProperty>.Enumerator enumerator = this.used.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AvatarTypingProperty current = enumerator.Current;
        if (name == current.prop.name)
          return current;
      }
    }
    return (AvatarTypingProperty) null;
  }

  private void Replace(AvatarTypingProperty typing)
  {
    AvatarTypingProperty property = this.FindProperty(typing.prop.name);
    if (property != null)
      this.used[this.used.IndexOf(property)] = typing;
    else
      this.used.Add(typing);
  }
}
