﻿// Decompiled with JetBrains decompiler
// Type: BackgroundConstructor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BackgroundConstructor : NonLODObjectConstructor
{
  protected BackgroundDesc backgroundDesc;

  public BackgroundConstructor(BackgroundDesc desc, string gameObjectName)
    : base(new GameObject(gameObjectName))
  {
    this.backgroundDesc = desc;
  }

  public override void Construct()
  {
    base.Construct();
    this.root.AddComponent<CustomSupply>().Action = (System.Action<GameObject>) (obj =>
    {
      obj.transform.position = this.backgroundDesc.position;
      obj.transform.rotation = this.backgroundDesc.rotation;
      obj.transform.localScale = this.backgroundDesc.scale;
      foreach (MeshRenderer componentsInChild in obj.GetComponentsInChildren<MeshRenderer>())
      {
        componentsInChild.gameObject.layer = 8;
        if (componentsInChild.material.HasProperty("_Color"))
          componentsInChild.material.color = this.backgroundDesc.color;
      }
    });
  }

  protected override string GetPrefabName()
  {
    return this.backgroundDesc.prefabName + ".prefab";
  }
}
