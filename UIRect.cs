﻿// Decompiled with JetBrains decompiler
// Type: UIRect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public abstract class UIRect : MonoBehaviour
{
  protected static Vector3[] mSides = new Vector3[4];
  public UIRect.AnchorPoint leftAnchor = new UIRect.AnchorPoint();
  public UIRect.AnchorPoint rightAnchor = new UIRect.AnchorPoint(1f);
  public UIRect.AnchorPoint bottomAnchor = new UIRect.AnchorPoint();
  public UIRect.AnchorPoint topAnchor = new UIRect.AnchorPoint(1f);
  public UIRect.AnchorUpdate updateAnchors = UIRect.AnchorUpdate.OnUpdate;
  protected BetterList<UIRect> mChildren = new BetterList<UIRect>();
  protected bool mChanged = true;
  [NonSerialized]
  private bool mUpdateAnchors = true;
  [NonSerialized]
  private int mUpdateFrame = -1;
  [NonSerialized]
  public float finalAlpha = 1f;
  protected GameObject mGo;
  protected Transform mTrans;
  protected bool mStarted;
  protected bool mParentFound;
  [NonSerialized]
  private bool mAnchorsCached;
  private UIRoot mRoot;
  private UIRect mParent;
  private bool mRootSet;
  protected Camera mCam;

  public GameObject cachedGameObject
  {
    get
    {
      if ((UnityEngine.Object) this.mGo == (UnityEngine.Object) null)
        this.mGo = this.gameObject;
      return this.mGo;
    }
  }

  public Transform cachedTransform
  {
    get
    {
      if ((UnityEngine.Object) this.mTrans == (UnityEngine.Object) null)
        this.mTrans = this.transform;
      return this.mTrans;
    }
  }

  public Camera anchorCamera
  {
    get
    {
      if (!this.mAnchorsCached)
        this.ResetAnchors();
      return this.mCam;
    }
  }

  public bool isFullyAnchored
  {
    get
    {
      if ((bool) ((UnityEngine.Object) this.leftAnchor.target) && (bool) ((UnityEngine.Object) this.rightAnchor.target) && (bool) ((UnityEngine.Object) this.topAnchor.target))
        return (bool) ((UnityEngine.Object) this.bottomAnchor.target);
      return false;
    }
  }

  public virtual bool isAnchoredHorizontally
  {
    get
    {
      if (!(bool) ((UnityEngine.Object) this.leftAnchor.target))
        return (bool) ((UnityEngine.Object) this.rightAnchor.target);
      return true;
    }
  }

  public virtual bool isAnchoredVertically
  {
    get
    {
      if (!(bool) ((UnityEngine.Object) this.bottomAnchor.target))
        return (bool) ((UnityEngine.Object) this.topAnchor.target);
      return true;
    }
  }

  public virtual bool canBeAnchored
  {
    get
    {
      return true;
    }
  }

  public UIRect parent
  {
    get
    {
      if (!this.mParentFound)
      {
        this.mParentFound = true;
        this.mParent = NGUITools.FindInParents<UIRect>(this.cachedTransform.parent);
      }
      return this.mParent;
    }
  }

  public UIRoot root
  {
    get
    {
      if ((UnityEngine.Object) this.parent != (UnityEngine.Object) null)
        return this.mParent.root;
      if (!this.mRootSet)
      {
        this.mRootSet = true;
        this.mRoot = NGUITools.FindInParents<UIRoot>(this.cachedTransform);
      }
      return this.mRoot;
    }
  }

  public bool isAnchored
  {
    get
    {
      if ((bool) ((UnityEngine.Object) this.leftAnchor.target) || (bool) ((UnityEngine.Object) this.rightAnchor.target) || ((bool) ((UnityEngine.Object) this.topAnchor.target) || (bool) ((UnityEngine.Object) this.bottomAnchor.target)))
        return this.canBeAnchored;
      return false;
    }
  }

  public abstract float alpha { get; set; }

  public abstract Vector3[] localCorners { get; }

  public abstract Vector3[] worldCorners { get; }

  protected float cameraRayDistance
  {
    get
    {
      if ((UnityEngine.Object) this.anchorCamera == (UnityEngine.Object) null)
        return 0.0f;
      if (!this.mCam.orthographic)
      {
        Transform cachedTransform = this.cachedTransform;
        Transform transform = this.mCam.transform;
        float enter;
        if (new Plane(cachedTransform.rotation * Vector3.back, cachedTransform.position).Raycast(new Ray(transform.position, transform.rotation * Vector3.forward), out enter))
          return enter;
      }
      return Mathf.Lerp(this.mCam.nearClipPlane, this.mCam.farClipPlane, 0.5f);
    }
  }

  public abstract float CalculateFinalAlpha(int frameID);

  public virtual void Invalidate(bool includeChildren)
  {
    this.mChanged = true;
    if (!includeChildren)
      return;
    for (int index = 0; index < this.mChildren.size; ++index)
      this.mChildren.buffer[index].Invalidate(true);
  }

  public virtual Vector3[] GetSides(Transform relativeTo)
  {
    if ((UnityEngine.Object) this.anchorCamera != (UnityEngine.Object) null)
      return this.mCam.GetSides(this.cameraRayDistance, relativeTo);
    Vector3 position = this.cachedTransform.position;
    for (int index = 0; index < 4; ++index)
      UIRect.mSides[index] = position;
    if ((UnityEngine.Object) relativeTo != (UnityEngine.Object) null)
    {
      for (int index = 0; index < 4; ++index)
        UIRect.mSides[index] = relativeTo.InverseTransformPoint(UIRect.mSides[index]);
    }
    return UIRect.mSides;
  }

  protected Vector3 GetLocalPos(UIRect.AnchorPoint ac, Transform trans)
  {
    if ((UnityEngine.Object) this.anchorCamera == (UnityEngine.Object) null || (UnityEngine.Object) ac.targetCam == (UnityEngine.Object) null)
      return this.cachedTransform.localPosition;
    Vector3 position = this.mCam.ViewportToWorldPoint(ac.targetCam.WorldToViewportPoint(ac.target.position));
    if ((UnityEngine.Object) trans != (UnityEngine.Object) null)
      position = trans.InverseTransformPoint(position);
    position.x = Mathf.Floor(position.x + 0.5f);
    position.y = Mathf.Floor(position.y + 0.5f);
    return position;
  }

  protected virtual void OnEnable()
  {
    this.mUpdateFrame = -1;
    if (this.updateAnchors == UIRect.AnchorUpdate.OnEnable)
    {
      this.mAnchorsCached = false;
      this.mUpdateAnchors = true;
    }
    if (this.mStarted)
      this.OnInit();
    this.mUpdateFrame = -1;
  }

  protected virtual void OnInit()
  {
    this.mChanged = true;
    this.mRootSet = false;
    this.mParentFound = false;
    if (!((UnityEngine.Object) this.parent != (UnityEngine.Object) null))
      return;
    this.mParent.mChildren.Add(this);
  }

  protected virtual void OnDisable()
  {
    if ((bool) ((UnityEngine.Object) this.mParent))
      this.mParent.mChildren.Remove(this);
    this.mParent = (UIRect) null;
    this.mRoot = (UIRoot) null;
    this.mRootSet = false;
    this.mParentFound = false;
  }

  protected void Start()
  {
    this.mStarted = true;
    this.OnInit();
    this.OnStart();
  }

  public void Update()
  {
    if (!this.mAnchorsCached)
      this.ResetAnchors();
    int frameCount = Time.frameCount;
    if (this.mUpdateFrame == frameCount)
      return;
    if (this.updateAnchors == UIRect.AnchorUpdate.OnUpdate || this.mUpdateAnchors)
    {
      this.mUpdateFrame = frameCount;
      this.mUpdateAnchors = false;
      bool flag = false;
      if ((bool) ((UnityEngine.Object) this.leftAnchor.target))
      {
        flag = true;
        if ((UnityEngine.Object) this.leftAnchor.rect != (UnityEngine.Object) null && this.leftAnchor.rect.mUpdateFrame != frameCount)
          this.leftAnchor.rect.Update();
      }
      if ((bool) ((UnityEngine.Object) this.bottomAnchor.target))
      {
        flag = true;
        if ((UnityEngine.Object) this.bottomAnchor.rect != (UnityEngine.Object) null && this.bottomAnchor.rect.mUpdateFrame != frameCount)
          this.bottomAnchor.rect.Update();
      }
      if ((bool) ((UnityEngine.Object) this.rightAnchor.target))
      {
        flag = true;
        if ((UnityEngine.Object) this.rightAnchor.rect != (UnityEngine.Object) null && this.rightAnchor.rect.mUpdateFrame != frameCount)
          this.rightAnchor.rect.Update();
      }
      if ((bool) ((UnityEngine.Object) this.topAnchor.target))
      {
        flag = true;
        if ((UnityEngine.Object) this.topAnchor.rect != (UnityEngine.Object) null && this.topAnchor.rect.mUpdateFrame != frameCount)
          this.topAnchor.rect.Update();
      }
      if (flag)
        this.OnAnchor();
    }
    this.OnUpdate();
  }

  public void UpdateAnchors()
  {
    if (!this.isAnchored || this.updateAnchors == UIRect.AnchorUpdate.OnStart)
      return;
    this.OnAnchor();
  }

  protected abstract void OnAnchor();

  public void SetAnchor(Transform t)
  {
    this.leftAnchor.target = t;
    this.rightAnchor.target = t;
    this.topAnchor.target = t;
    this.bottomAnchor.target = t;
    this.ResetAnchors();
    this.UpdateAnchors();
  }

  public void SetAnchor(GameObject go)
  {
    Transform transform = !((UnityEngine.Object) go != (UnityEngine.Object) null) ? (Transform) null : go.transform;
    this.leftAnchor.target = transform;
    this.rightAnchor.target = transform;
    this.topAnchor.target = transform;
    this.bottomAnchor.target = transform;
    this.ResetAnchors();
    this.UpdateAnchors();
  }

  public void SetAnchor(GameObject go, int left, int bottom, int right, int top)
  {
    Transform transform = !((UnityEngine.Object) go != (UnityEngine.Object) null) ? (Transform) null : go.transform;
    this.leftAnchor.target = transform;
    this.rightAnchor.target = transform;
    this.topAnchor.target = transform;
    this.bottomAnchor.target = transform;
    this.leftAnchor.relative = 0.0f;
    this.rightAnchor.relative = 1f;
    this.bottomAnchor.relative = 0.0f;
    this.topAnchor.relative = 1f;
    this.leftAnchor.absolute = left;
    this.rightAnchor.absolute = right;
    this.bottomAnchor.absolute = bottom;
    this.topAnchor.absolute = top;
    this.ResetAnchors();
    this.UpdateAnchors();
  }

  public void ResetAnchors()
  {
    this.mAnchorsCached = true;
    this.leftAnchor.rect = !(bool) ((UnityEngine.Object) this.leftAnchor.target) ? (UIRect) null : this.leftAnchor.target.GetComponent<UIRect>();
    this.bottomAnchor.rect = !(bool) ((UnityEngine.Object) this.bottomAnchor.target) ? (UIRect) null : this.bottomAnchor.target.GetComponent<UIRect>();
    this.rightAnchor.rect = !(bool) ((UnityEngine.Object) this.rightAnchor.target) ? (UIRect) null : this.rightAnchor.target.GetComponent<UIRect>();
    this.topAnchor.rect = !(bool) ((UnityEngine.Object) this.topAnchor.target) ? (UIRect) null : this.topAnchor.target.GetComponent<UIRect>();
    this.mCam = NGUITools.FindCameraForLayer(this.cachedGameObject.layer);
    this.FindCameraFor(this.leftAnchor);
    this.FindCameraFor(this.bottomAnchor);
    this.FindCameraFor(this.rightAnchor);
    this.FindCameraFor(this.topAnchor);
    this.mUpdateAnchors = true;
  }

  public void ResetAndUpdateAnchors()
  {
    this.ResetAnchors();
    this.UpdateAnchors();
  }

  public abstract void SetRect(float x, float y, float width, float height);

  private void FindCameraFor(UIRect.AnchorPoint ap)
  {
    if ((UnityEngine.Object) ap.target == (UnityEngine.Object) null || (UnityEngine.Object) ap.rect != (UnityEngine.Object) null)
      ap.targetCam = (Camera) null;
    else
      ap.targetCam = NGUITools.FindCameraForLayer(ap.target.gameObject.layer);
  }

  public virtual void ParentHasChanged()
  {
    this.mParentFound = false;
    UIRect inParents = NGUITools.FindInParents<UIRect>(this.cachedTransform.parent);
    if (!((UnityEngine.Object) this.mParent != (UnityEngine.Object) inParents))
      return;
    if ((bool) ((UnityEngine.Object) this.mParent))
      this.mParent.mChildren.Remove(this);
    this.mParent = inParents;
    if ((bool) ((UnityEngine.Object) this.mParent))
      this.mParent.mChildren.Add(this);
    this.mRootSet = false;
  }

  protected abstract void OnStart();

  protected virtual void OnUpdate()
  {
  }

  [Serializable]
  public class AnchorPoint
  {
    public Transform target;
    public float relative;
    public int absolute;
    [NonSerialized]
    public UIRect rect;
    [NonSerialized]
    public Camera targetCam;

    public AnchorPoint()
    {
    }

    public AnchorPoint(float relative)
    {
      this.relative = relative;
    }

    public void Set(float relative, float absolute)
    {
      this.relative = relative;
      this.absolute = Mathf.FloorToInt(absolute + 0.5f);
    }

    public void Set(Transform target, float relative, float absolute)
    {
      this.target = target;
      this.relative = relative;
      this.absolute = Mathf.FloorToInt(absolute + 0.5f);
    }

    public void SetToNearest(float abs0, float abs1, float abs2)
    {
      this.SetToNearest(0.0f, 0.5f, 1f, abs0, abs1, abs2);
    }

    public void SetToNearest(float rel0, float rel1, float rel2, float abs0, float abs1, float abs2)
    {
      float num1 = Mathf.Abs(abs0);
      float num2 = Mathf.Abs(abs1);
      float num3 = Mathf.Abs(abs2);
      if ((double) num1 < (double) num2 && (double) num1 < (double) num3)
        this.Set(rel0, abs0);
      else if ((double) num2 < (double) num1 && (double) num2 < (double) num3)
        this.Set(rel1, abs1);
      else
        this.Set(rel2, abs2);
    }

    public void SetHorizontal(Transform parent, float localPos)
    {
      if ((bool) ((UnityEngine.Object) this.rect))
      {
        Vector3[] sides = this.rect.GetSides(parent);
        float num = Mathf.Lerp(sides[0].x, sides[2].x, this.relative);
        this.absolute = Mathf.FloorToInt((float) ((double) localPos - (double) num + 0.5));
      }
      else
      {
        Vector3 position = this.target.position;
        if ((UnityEngine.Object) parent != (UnityEngine.Object) null)
          position = parent.InverseTransformPoint(position);
        this.absolute = Mathf.FloorToInt((float) ((double) localPos - (double) position.x + 0.5));
      }
    }

    public void SetVertical(Transform parent, float localPos)
    {
      if ((bool) ((UnityEngine.Object) this.rect))
      {
        Vector3[] sides = this.rect.GetSides(parent);
        float num = Mathf.Lerp(sides[3].y, sides[1].y, this.relative);
        this.absolute = Mathf.FloorToInt((float) ((double) localPos - (double) num + 0.5));
      }
      else
      {
        Vector3 position = this.target.position;
        if ((UnityEngine.Object) parent != (UnityEngine.Object) null)
          position = parent.InverseTransformPoint(position);
        this.absolute = Mathf.FloorToInt((float) ((double) localPos - (double) position.y + 0.5));
      }
    }

    public Vector3[] GetSides(Transform relativeTo)
    {
      if ((UnityEngine.Object) this.target != (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) this.rect != (UnityEngine.Object) null)
          return this.rect.GetSides(relativeTo);
        if ((UnityEngine.Object) this.target.GetComponent<Camera>() != (UnityEngine.Object) null)
          return this.target.GetComponent<Camera>().GetSides(relativeTo);
      }
      return (Vector3[]) null;
    }
  }

  public enum AnchorUpdate
  {
    OnEnable,
    OnUpdate,
    OnStart,
  }
}
