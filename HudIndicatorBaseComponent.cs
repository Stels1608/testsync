﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorBaseComponent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class HudIndicatorBaseComponent : MonoBehaviour, IHudIndicatorComponent
{
  private bool inScannerRange;
  private bool inScreen;
  private bool inMiniModeRange;
  private bool isSelected;
  private bool isDesignated;
  private bool isActiveWaypoint;
  protected float opacity;
  private int multiTargetCounter;
  protected HudIndicatorBase hudIndicator;

  public bool InScannerRange
  {
    get
    {
      return this.inScannerRange;
    }
  }

  public bool InScreen
  {
    get
    {
      return this.inScreen;
    }
  }

  public bool InMiniMode
  {
    get
    {
      if (this.inMiniModeRange)
        return (this.IsSelected ? 1 : (this.IsMultiselected ? 1 : 0)) == 0;
      return false;
    }
  }

  public bool IsSelected
  {
    get
    {
      return this.isSelected;
    }
  }

  protected bool IsMultiselected
  {
    get
    {
      return this.multiTargetCounter > 0;
    }
  }

  public bool IsDesignated
  {
    get
    {
      return this.isDesignated;
    }
  }

  public bool IsActiveWaypoint
  {
    get
    {
      return this.isActiveWaypoint;
    }
  }

  protected virtual void Awake()
  {
    this.ResetStates();
  }

  public void SetHudIndicator(HudIndicatorBase hudIndicator)
  {
    this.hudIndicator = hudIndicator;
  }

  protected abstract void ResetComponentStates();

  public void ResetStates()
  {
    this.inScannerRange = false;
    this.inScreen = false;
    this.isSelected = false;
    this.isDesignated = false;
    this.isActiveWaypoint = false;
    this.opacity = 1f;
    this.multiTargetCounter = 0;
    this.ResetComponentStates();
  }

  public void OnSelectionChange(bool targetIsSelected)
  {
    this.isSelected = targetIsSelected;
    this.UpdateView();
  }

  public void OnScreenLeave()
  {
    this.inScreen = false;
    this.UpdateView();
  }

  public void OnScreenEnter()
  {
    this.inScreen = true;
    this.UpdateView();
  }

  public virtual void OnScannerVisibilityChange(bool targetIsInScannerRange)
  {
    this.inScannerRange = targetIsInScannerRange;
    this.UpdateView();
  }

  public void OnMiniModeChange(bool inMiniMode)
  {
    this.inMiniModeRange = inMiniMode;
    this.UpdateView();
  }

  public virtual void OnScanned(ItemCountable resource)
  {
  }

  public void OnWaypointStatusChange(bool targetIsActiveWaypoint)
  {
    this.isActiveWaypoint = targetIsActiveWaypoint;
    this.UpdateView();
  }

  public void OnDesignationChange(bool targetIsDesignated)
  {
    this.isDesignated = targetIsDesignated;
    this.UpdateView();
  }

  public virtual void OnMultiTargetSelectionChange(bool isAbilityTarget, ShipAbility ability)
  {
    if (isAbilityTarget)
      ++this.multiTargetCounter;
    else
      --this.multiTargetCounter;
    this.UpdateView();
  }

  protected abstract void SetColoring();

  public void UpdateColorAndAlpha()
  {
    this.SetColoring();
    this.ApplyOpacity();
  }

  public void ChangeOpacity(float alpha)
  {
    this.opacity = alpha;
    this.ApplyOpacity();
  }

  public abstract bool RequiresScreenBorderClamping();

  protected abstract void UpdateView();

  protected abstract void ApplyOpacity();
}
