﻿// Decompiled with JetBrains decompiler
// Type: SectorEventTrackerUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SectorEventTrackerUgui : MonoBehaviour, IEventSystemHandler, IPointerEnterHandler, IPointerExitHandler
{
  private const float FADEOUT_TIME = 20f;
  [SerializeField]
  private Image eventIcon;
  [SerializeField]
  private TextMeshProUGUI nameLabel;
  [SerializeField]
  private GameObject contentRoot;
  [SerializeField]
  private UguiCollapseButtonImageSwitcher collapseImageSwitcher;
  [SerializeField]
  private CanvasGroup canvasGroup;
  [SerializeField]
  private Animation flashAnim;
  private SectorEventPanelCompletedUgui completionPanel;
  private SectorEventPanelTaskUgui taskPanel;
  private float flashDuration;
  private float flashTime;
  private bool isMouseOver;
  private bool _collapsed;

  public SectorEvent SectorEvent { get; private set; }

  private bool Collapsed
  {
    get
    {
      return this._collapsed;
    }
    set
    {
      this._collapsed = value;
      this.collapseImageSwitcher.OnCollapseEvent(this._collapsed);
      this.contentRoot.SetActive(!this._collapsed);
    }
  }

  public void ApplyEventCard(SectorEventCard sectorEventCard)
  {
    Texture2D icon = sectorEventCard.Icon;
    Rect rect = new Rect(0.0f, 0.0f, (float) icon.width, (float) icon.height);
    this.eventIcon.sprite = UnityEngine.Sprite.Create(icon, rect, new Vector2(0.5f, 0.5f), 100f);
    this.nameLabel.text = BsgoLocalization.Get(sectorEventCard.Name);
    this.completionPanel = this.CreateCompletionPanel();
    this.SetCurrentPanel((SectorEventPanel) this.completionPanel);
  }

  public void ApplySectorEvent(SectorEvent sectorEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SectorEventTrackerUgui.\u003CApplySectorEvent\u003Ec__AnonStorey125 eventCAnonStorey125 = new SectorEventTrackerUgui.\u003CApplySectorEvent\u003Ec__AnonStorey125();
    // ISSUE: reference to a compiler-generated field
    eventCAnonStorey125.sectorEvent = sectorEvent;
    // ISSUE: reference to a compiler-generated field
    eventCAnonStorey125.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    this.SectorEvent = eventCAnonStorey125.sectorEvent;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    eventCAnonStorey125.sectorEvent.SectorEventCard.IsLoaded.AddHandler(new SignalHandler(eventCAnonStorey125.\u003C\u003Em__2BE));
  }

  private SectorEventPanelCompletedUgui CreateCompletionPanel()
  {
    return UguiTools.CreateChild<SectorEventPanelCompletedUgui>("AccordeonSidebarRight/SectorEventPanelCompletedUgui", this.contentRoot.transform);
  }

  private SectorEventPanelProtectTaskUgui CreateProtectionTaskPanel()
  {
    return UguiTools.CreateChild<SectorEventPanelProtectTaskUgui>("AccordeonSidebarRight/SectorEventPanelProtectTaskUgui", this.contentRoot.transform);
  }

  [DebuggerHidden]
  private IEnumerator TestRewards(SectorEvent sectorEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SectorEventTrackerUgui.\u003CTestRewards\u003Ec__Iterator42()
    {
      sectorEvent = sectorEvent,
      \u003C\u0024\u003EsectorEvent = sectorEvent,
      \u003C\u003Ef__this = this
    };
  }

  public void ToggleCollapse()
  {
    this.Collapsed = !this.Collapsed;
  }

  public void ShowRewards(SectorEventReward reward)
  {
    if ((UnityEngine.Object) this.completionPanel == (UnityEngine.Object) null)
    {
      this.Collapsed = false;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.taskPanel.gameObject);
      if ((UnityEngine.Object) this.completionPanel == (UnityEngine.Object) null)
      {
        this.completionPanel = this.CreateCompletionPanel();
        this.SetCurrentPanel((SectorEventPanel) this.completionPanel);
      }
      this.completionPanel.Initialize(this.SectorEvent.State, this.SectorEvent.OwnerFaction);
    }
    this.completionPanel.ShowReward(reward);
    this.ActivateCloseButton();
    this.FadeOutAndClose();
  }

  public void Refresh()
  {
    this.Flash();
    this.SectorEvent.SectorEventCard.IsLoaded.AddHandler((SignalHandler) (() => this.nameLabel.text = BsgoLocalization.Get(this.SectorEvent.SectorEventCard.Name).ToUpperInvariant()));
    if (this.SectorEvent.State != SectorEventState.Failed && this.SectorEvent.State != SectorEventState.Success)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.taskPanel.gameObject);
    if ((UnityEngine.Object) this.completionPanel == (UnityEngine.Object) null)
    {
      this.completionPanel = this.CreateCompletionPanel();
      this.SetCurrentPanel((SectorEventPanel) this.completionPanel);
    }
    this.completionPanel.Initialize(this.SectorEvent.State, this.SectorEvent.OwnerFaction);
    this.SetCurrentPanel((SectorEventPanel) this.completionPanel);
    this.FadeOutAndClose();
  }

  private void Flash()
  {
    this.flashAnim.Play();
  }

  private void ActivateCloseButton()
  {
  }

  public void UpdateTasks(List<SectorEventTask> tasks)
  {
    if (tasks.Count <= 0)
      return;
    SectorEventTask sectorEventTask = tasks[0];
    if (sectorEventTask.TaskState == SectorEventState.Failed || sectorEventTask.TaskState == SectorEventState.Success)
      return;
    if ((UnityEngine.Object) this.taskPanel != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.taskPanel.gameObject);
    if (sectorEventTask.TaskType != SectorEventTaskType.Protect)
      return;
    this.taskPanel = (SectorEventPanelTaskUgui) this.CreateProtectionTaskPanel();
    (this.taskPanel as SectorEventPanelProtectTaskUgui).Initialize((SectorEventProtectTask) sectorEventTask, this.SectorEvent.OwnerFaction);
    this.SetCurrentPanel((SectorEventPanel) this.taskPanel);
  }

  private void Close()
  {
    if (this.SectorEvent == null)
      return;
    FacadeFactory.GetInstance().SendMessage(Message.SectorEventTerminated, (object) this.SectorEvent.ObjectID);
  }

  private Color GetFlashColor()
  {
    if (Game.Me.Faction == Faction.Cylon)
      return new Color(1f, 0.8f, 0.83f);
    return new Color(0.8f, 0.94f, 1f);
  }

  private void FadeComponent(GuiImage image, float a)
  {
    if (image == null)
      return;
    Color color = !image.OverlayColor.HasValue ? Color.white : image.OverlayColor.Value;
    color.a = a;
    image.OverlayColor = new Color?(color);
  }

  private void FadeComponent(GuiButton button, float a)
  {
    if (button == null)
      return;
    Color color = !button.OverlayColor.HasValue ? Color.white : button.OverlayColor.Value;
    color.a = a;
    button.OverlayColor = new Color?(color);
  }

  private void FadeOutAndClose()
  {
    LeanTween.value(this.gameObject, 1f, 0.0f, 20f).setOnUpdate((System.Action<float>) (alpha => this.canvasGroup.alpha = !this.isMouseOver ? alpha : 1f)).setOnComplete((System.Action) (() => this.gameObject.SetActive(false)));
  }

  private void SetCurrentPanel(SectorEventPanel panel)
  {
    foreach (Component componentsInChild in this.GetComponentsInChildren<SectorEventPanel>())
      componentsInChild.gameObject.SetActive(false);
    panel.gameObject.SetActive(true);
    this.Collapsed = false;
  }

  public void OnPointerEnter(PointerEventData eventData)
  {
    this.isMouseOver = true;
  }

  public void OnPointerExit(PointerEventData eventData)
  {
    this.isMouseOver = false;
  }
}
