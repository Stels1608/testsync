﻿// Decompiled with JetBrains decompiler
// Type: SkillList
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;

public class SkillList : SmartList<PlayerSkill>
{
  private PlayerSkill currentTrainingSkill;
  private float trainingEndTime;
  private bool isUpdated;

  public bool IsTrainingSkill
  {
    get
    {
      return this.currentTrainingSkill != null;
    }
  }

  public float TrainingEndTime
  {
    get
    {
      return this.trainingEndTime;
    }
  }

  public PlayerSkill CurrentTrainingSkill
  {
    get
    {
      return this.currentTrainingSkill;
    }
  }

  public bool IsUpdated
  {
    get
    {
      bool flag = this.isUpdated;
      this.isUpdated = false;
      return flag;
    }
    set
    {
      this.isUpdated = value;
    }
  }

  public SkillList()
  {
  }

  public SkillList(List<PlayerSkill> items)
    : base(items)
  {
  }

  public void EndTraining()
  {
    this.currentTrainingSkill = (PlayerSkill) null;
    this.trainingEndTime = 0.0f;
  }

  protected override void Changed()
  {
    Flag.Schedule<PlayerSkill>(new SignalHandler(this.items.Sort), (IEnumerable<PlayerSkill>) this.items);
  }

  public override void _Clear()
  {
    base._Clear();
    this.currentTrainingSkill = (PlayerSkill) null;
    this.trainingEndTime = 0.0f;
  }

  public void _SetSkills(PlayerSkill learning, float trainingEndTime, IEnumerable<PlayerSkill> newSkills)
  {
    if (this.currentTrainingSkill != null && (bool) this.currentTrainingSkill.IsLoaded && learning == null)
      Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox(BsgoLocalization.Get("%$bgo.notif.training_complete%", (object) (this.currentTrainingSkill.Name + "-" + (object) this.currentTrainingSkill.Level)));
    this.currentTrainingSkill = learning;
    this.trainingEndTime = trainingEndTime;
    this.items.Clear();
    this._AddItems(newSkills);
    this.isUpdated = true;
  }
}
