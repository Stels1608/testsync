﻿// Decompiled with JetBrains decompiler
// Type: EffectCatalogueFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Assets.Scripts.EffectSystem;

public class EffectCatalogueFactory
{
  private static EffectCatalogue effectCatalogue;

  public static EffectCatalogue GetInstance()
  {
    if (EffectCatalogueFactory.effectCatalogue == null)
      EffectCatalogueFactory.effectCatalogue = new EffectCatalogue();
    return EffectCatalogueFactory.effectCatalogue;
  }
}
