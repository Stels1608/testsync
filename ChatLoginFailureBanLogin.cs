﻿// Decompiled with JetBrains decompiler
// Type: ChatLoginFailureBanLogin
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

internal class ChatLoginFailureBanLogin : ChatProtocolParser
{
  public string beginDate;
  public string endDate;
  public string beginTime;
  public string endTime;
  public int periodInMinutes;
  public bool isPermanentBan;

  public ChatLoginFailureBanLogin()
  {
    this.type = ChatProtocolParserType.LoginFailureBanLogin;
  }

  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split(new char[3]{ '%', '@', '#' });
    if (strArray.Length != 8 || strArray[0] != "db")
      return false;
    this.beginDate = strArray[1];
    this.endDate = strArray[2];
    this.beginTime = strArray[3];
    this.endTime = strArray[4];
    if (!int.TryParse(strArray[5], out this.periodInMinutes))
      return false;
    int result = -1;
    if (!int.TryParse(strArray[6], out result))
      return false;
    this.isPermanentBan = result == 1;
    return true;
  }
}
