﻿// Decompiled with JetBrains decompiler
// Type: GuildMemberPromotionMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System;

public class GuildMemberPromotionMessage : IMessage<Message>
{
  public uint PlayerId { get; private set; }

  public GuildRole NewRole { get; private set; }

  public object Data
  {
    get
    {
      throw new NotImplementedException();
    }
    set
    {
      throw new NotImplementedException();
    }
  }

  public Message Id
  {
    get
    {
      return Message.GuildMemberPromotion;
    }
  }

  public GuildMemberPromotionMessage(uint playerId, GuildRole newRole)
  {
    this.PlayerId = playerId;
    this.NewRole = newRole;
  }
}
