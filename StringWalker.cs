﻿// Decompiled with JetBrains decompiler
// Type: StringWalker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class StringWalker
{
  private string _s;

  public int Index { get; set; }

  public StringWalker(string s)
  {
    this._s = s;
    this.Index = -1;
  }

  public bool MoveNext()
  {
    if (this.Index == this._s.Length - 1)
      return false;
    ++this.Index;
    return true;
  }

  public char CharAtIndex()
  {
    return this._s[this.Index];
  }
}
