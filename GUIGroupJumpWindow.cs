﻿// Decompiled with JetBrains decompiler
// Type: GUIGroupJumpWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GUIGroupJumpWindow : GuiPanel
{
  private readonly List<Player> finalJumpList = new List<Player>();
  private readonly HashSet<uint> excludedPlayers = new HashSet<uint>();
  public uint sectorId;
  private readonly GameProtocol.Request request;

  public GUIGroupJumpWindow(GameProtocol.Request request)
  {
    this.request = request;
    if (Game.Me.Party.GetGroupJumpParty().Count == 0)
    {
      FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new GroupJumpNotification(GroupJumpNotification.GroupJumpState.NoMembers, (object) null));
    }
    else
    {
      bool flag = true;
      this.IsRendered = flag;
      this.IsUpdated = flag;
      this.PeriodicUpdate();
    }
  }

  public override void PeriodicUpdate()
  {
    CommunityProtocol.GetProtocol().RequestPartyMemberFtlState(Game.Me.Party.GetGroupJumpParty().Select<Player, uint>((Func<Player, uint>) (player => player.ServerID)).ToArray<uint>());
  }

  public void SetPlayerFtlState(List<Tuple<Player, PartyMemberFtlState>> players)
  {
    this.EmptyChildren();
    this.AddChild((GuiElementBase) new Gui.Timer(3f));
    this.finalJumpList.Clear();
    Color white = Color.white;
    GuiImage guiImage = new GuiImage("GUI/Gui2/tooltip");
    guiImage.NineSliceEdge = (GuiElementPadding) new Rect(10f, 10f, 10f, 10f);
    this.AddChild((GuiElementBase) guiImage);
    QueueVertical queueVertical = new QueueVertical();
    queueVertical.ContainerAutoSize = true;
    queueVertical.AddChild((GuiElementBase) new GuiLabel("%$bgo.group_jump.select_group%", white, white, Gui.Options.FontBGM_BT)
    {
      FontSize = 15
    });
    using (List<Tuple<Player, PartyMemberFtlState>>.Enumerator enumerator = players.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Tuple<Player, PartyMemberFtlState> current = enumerator.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GUIGroupJumpWindow.\u003CSetPlayerFtlState\u003Ec__AnonStoreyA7 stateCAnonStoreyA7 = new GUIGroupJumpWindow.\u003CSetPlayerFtlState\u003Ec__AnonStoreyA7();
        // ISSUE: reference to a compiler-generated field
        stateCAnonStoreyA7.\u003C\u003Ef__this = this;
        // ISSUE: reference to a compiler-generated field
        stateCAnonStoreyA7.player = current.First;
        if (current.Second == PartyMemberFtlState.Ready)
        {
          // ISSUE: reference to a compiler-generated field
          GuiCheckbox guiCheckbox = new GuiCheckbox(stateCAnonStoreyA7.player.Name);
          // ISSUE: reference to a compiler-generated field
          bool flag = !this.excludedPlayers.Contains(stateCAnonStoreyA7.player.ServerID);
          guiCheckbox.IsChecked = flag;
          if (flag)
          {
            // ISSUE: reference to a compiler-generated field
            this.finalJumpList.Add(stateCAnonStoreyA7.player);
          }
          // ISSUE: reference to a compiler-generated method
          guiCheckbox.OnCheck = new System.Action<bool>(stateCAnonStoreyA7.\u003C\u003Em__165);
          guiCheckbox.Size = new Vector2(30f, 30f);
          queueVertical.AddChild((GuiElementBase) guiCheckbox);
        }
      }
    }
    GuiButton guiButton1 = new GuiButton("%$bgo.group_jump.ready%");
    GuiButton guiButton2 = new GuiButton("%$bgo.group_jump.cancel%");
    guiButton1.Pressed = (AnonymousDelegate) (() =>
    {
      GameProtocol.GetProtocol().RequestGroupFTLJump(this.sectorId, this.finalJumpList, this.request);
      this.finalJumpList.Clear();
      Game.Me.Party.inGroupJump = true;
      this.Hide();
    });
    guiButton2.Pressed = new AnonymousDelegate(this.Hide);
    guiButton1.Font = Gui.Options.FontBGM_BT;
    guiButton1.FontSize = 15;
    guiButton1.Size = new Vector2(140f, 30f);
    guiButton2.Font = Gui.Options.FontBGM_BT;
    guiButton2.FontSize = 15;
    guiButton2.Size = new Vector2(140f, 30f);
    QueueHorizontal queueHorizontal = new QueueHorizontal();
    queueHorizontal.ContainerAutoSize = true;
    queueHorizontal.AddChild((GuiElementBase) guiButton1, new Vector2(-2f, 0.0f));
    queueHorizontal.AddChild((GuiElementBase) guiButton2, new Vector2(2f, 0.0f));
    queueVertical.AddChild((GuiElementBase) queueHorizontal);
    this.AddChild((GuiElementBase) queueVertical);
    this.Size = queueVertical.Size;
    this.Align = Align.MiddleCenter;
    guiImage.Size = this.Size + new Vector2(20f, 20f);
    guiImage.PositionX -= 10f;
    guiImage.PositionY -= 10f;
  }

  public void Hide()
  {
    this.IsRendered = false;
    this.EmptyChildren();
    Game.UnregisterDialog((IGUIPanel) this);
  }
}
