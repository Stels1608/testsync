﻿// Decompiled with JetBrains decompiler
// Type: CutsceneCamera
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (Camera))]
public class CutsceneCamera : MonoBehaviour
{
  public string CameraId;
  private Camera _camera;

  public Camera Camera
  {
    get
    {
      return this._camera;
    }
  }

  private void Awake()
  {
    this._camera = this.GetComponent<Camera>();
  }

  public override string ToString()
  {
    return "CutsceneCamera " + this.CameraId;
  }
}
