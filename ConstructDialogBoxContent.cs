﻿// Decompiled with JetBrains decompiler
// Type: ConstructDialogBoxContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class ConstructDialogBoxContent : DialogBoxContent
{
  private const float UPDATE_RATE = 0.07f;
  private float duration;
  private float passedTime;
  [SerializeField]
  private UIProgressBar progressBar;
  [SerializeField]
  private UILabel progressBarLabel;
  [SerializeField]
  private UISprite background;
  [SerializeField]
  private UILabel messageLabel;
  [SerializeField]
  private OneButtonBar buttonBar;
  public AnonymousDelegate onComplete;

  public override void OnAccept()
  {
  }

  public override void OnCancel()
  {
  }

  public void StartTimerBasedProgressBar(float duration)
  {
    this.duration = duration;
    this.passedTime = 0.0f;
    this.InvokeRepeating("UpdateProgress", 0.07f, 0.07f);
    FacadeFactory.GetInstance().SendMessage(Message.Play2dSound, (object) "Sound/construction");
  }

  private void UpdateProgress()
  {
    this.passedTime += 0.07f;
    float num1 = this.passedTime / this.duration;
    if ((double) num1 > 1.0)
      this.CompleteConstruction();
    float num2 = Mathf.Clamp01(num1);
    this.progressBar.value = num2;
    this.progressBarLabel.text = ((int) ((double) num2 * 100.0)).ToString() + "%";
  }

  private void CompleteConstruction()
  {
    this.CancelInvoke("UpdateProgress");
    TweenHeight tweenHeight = this.background.gameObject.AddComponent<TweenHeight>();
    tweenHeight.duration = 0.4f;
    tweenHeight.to = 160;
    tweenHeight.onFinished = new List<EventDelegate>(1);
    tweenHeight.onFinished.Add(new EventDelegate(new EventDelegate.Callback(this.OnHeightTweenCompleted)));
    this.GetComponentInChildren<DialogBoxTitleElement>().Init("%$bgo.event_shop.dialog.success%");
    if (this.onComplete == null)
      return;
    this.onComplete();
  }

  private void OnHeightTweenCompleted()
  {
    this.messageLabel.gameObject.SetActive(true);
    this.messageLabel.text = BsgoLocalization.Get("bgo.event_shop.dialog.construction_completed");
    this.buttonBar.gameObject.SetActive(true);
    this.buttonBar.SetButtonDelegate((AnonymousDelegate) null, Message.HideDialogBox);
  }
}
