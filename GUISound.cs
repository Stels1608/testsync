﻿// Decompiled with JetBrains decompiler
// Type: GUISound
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUISound : MonoBehaviour
{
  public float Volume = 1f;
  public AudioClip MouseOver;
  public AudioClip Select;
  public AudioClip WindowClose;
  public AudioClip Buy;
  public AudioClip Equip;
  public AudioClip IllegalAction;
  public AudioClip LevelUp;
  public AudioClip CountdownClockCompleted;
  public AudioClip BlinkerSound;
  private AudioSource _guiAudio;
  private static GUISound instance;

  private AudioSource guiAudio
  {
    get
    {
      if ((Object) this._guiAudio == (Object) null)
      {
        this._guiAudio = this.gameObject.AddComponent<AudioSource>();
        this._guiAudio.playOnAwake = false;
        this._guiAudio.loop = false;
        this._guiAudio.volume = this.Volume;
      }
      return this._guiAudio;
    }
  }

  public static GUISound Instance
  {
    get
    {
      if ((Object) GUISound.instance == (Object) null)
        GUISound.instance = Object.Instantiate<GameObject>((GameObject) Resources.Load("Fx/GUISound")).GetComponent<GUISound>();
      return GUISound.instance;
    }
  }

  public void PlayOnce(AudioClip clip)
  {
    if (this.guiAudio.isPlaying)
      return;
    this.guiAudio.clip = clip;
    this.guiAudio.Play();
  }

  public void OnMouseOver()
  {
    this.PlayOnce(this.MouseOver);
  }

  public void OnSelect()
  {
    this.PlayOnce(this.Select);
  }

  public void OnWindowClose()
  {
    this.PlayOnce(this.WindowClose);
  }

  public void OnBuy()
  {
    this.PlayOnce(this.Buy);
  }

  public void OnEquip()
  {
    this.PlayOnce(this.Equip);
  }

  public void OnIllegalAction()
  {
    this.PlayOnce(this.IllegalAction);
  }

  public void OnLevelUp()
  {
    this.PlayOnce(this.LevelUp);
  }

  public void OnCountdownClockCompleted()
  {
    this.PlayOnce(this.CountdownClockCompleted);
  }

  public void OnBlinkerBlinks()
  {
    this.PlayOnce(this.BlinkerSound);
  }
}
