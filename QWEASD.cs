﻿// Decompiled with JetBrains decompiler
// Type: QWEASD
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class QWEASD
{
  private int bitmask;
  private bool inputChanged;

  public int Bitmask
  {
    get
    {
      return this.bitmask;
    }
  }

  public bool InputChanged
  {
    get
    {
      return this.inputChanged;
    }
  }

  public int Pitch
  {
    get
    {
      return this.Bit(0) - this.Bit(2);
    }
  }

  public int Yaw
  {
    get
    {
      return this.Bit(3) - this.Bit(1);
    }
  }

  public int Roll
  {
    get
    {
      return this.Bit(4) - this.Bit(5);
    }
  }

  public QWEASD()
  {
    this.bitmask = 0;
  }

  public QWEASD(int bitmask)
  {
    this.bitmask = bitmask;
  }

  public void ResetKeyStates()
  {
    this.bitmask = 0;
  }

  public void Flush()
  {
    this.inputChanged = false;
  }

  public Vector2 DeriveDirectionVectorFromPressedKeys()
  {
    Vector3 vector3 = Vector3.zero + (float) this.Bit(0) * Vector3.up + (float) this.Bit(1) * Vector3.left + (float) this.Bit(2) * Vector3.down + (float) this.Bit(3) * Vector3.right;
    return new Vector2(vector3.x, vector3.y);
  }

  public bool TryToggleAction(Action action, bool bActive)
  {
    int bit;
    switch (action)
    {
      case Action.TurnOrSlideLeft:
        bit = 2;
        break;
      case Action.TurnOrSlideRight:
        bit = 8;
        break;
      case Action.SlopeForwardOrSlideUp:
        bit = 1;
        break;
      case Action.SlopeBackwardOrSlideDown:
        bit = 4;
        break;
      case Action.RollLeft:
        bit = 16;
        break;
      case Action.RollRight:
        bit = 32;
        break;
      default:
        return false;
    }
    this.inputChanged = true;
    this.SetBit(bit, bActive);
    return true;
  }

  public void SetBit(int bit, bool bActive)
  {
    if (bActive)
      this.bitmask = this.Bitmask | bit;
    else
      this.bitmask = this.Bitmask & 63 - bit;
  }

  public bool IsAnyKeyPressed()
  {
    return this.Bitmask > 0;
  }

  private int Bit(int n)
  {
    return (this.Bitmask & 1 << n) >> n;
  }
}
