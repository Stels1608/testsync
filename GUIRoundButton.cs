﻿// Decompiled with JetBrains decompiler
// Type: GUIRoundButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

internal class GUIRoundButton : GUIButtonNew
{
  public float Radius = 12f;

  public GUIRoundButton(JButton desc)
    : base(desc)
  {
  }

  public GUIRoundButton(Texture2D normal, Texture2D over, Texture2D pressed, SmartRect parent)
    : base(normal, over, pressed, parent)
  {
  }

  public override bool Contains(float2 point)
  {
    if (!this.HandleMouseInput)
      return false;
    return (double) (point - this.SmartRect.AbsPosition).magnitude < (double) this.Radius;
  }
}
