﻿// Decompiled with JetBrains decompiler
// Type: ISystemMap3DView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using SystemMap3D;

public interface ISystemMap3DView
{
  bool IsRendered { get; set; }

  void TransitionToMap(TransitionMode transitionMode);

  void TransitionToGame(TransitionMode transitionMode);

  void OnTransitionToGameFinished();

  void ApplySetting(UserSetting setting, object data);

  void Destroy();
}
