﻿// Decompiled with JetBrains decompiler
// Type: SkillCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class SkillCard : Card
{
  public GUICard GUICard;
  public SkillCard NextCard;
  public uint Hash;
  public byte MaxLevel;
  public byte Level;
  public float TrainingTime;
  public int Price;
  public int SortWeight;
  public SkillGroup Group;
  public uint RequireSkillHash;
  public ObjectStats StaticBuff;
  public ObjectStats MultiplyBuff;

  public SkillCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.Level = r.ReadByte();
    this.MaxLevel = r.ReadByte();
    uint cardGUID = r.ReadGUID();
    this.Hash = r.ReadUInt32();
    this.TrainingTime = r.ReadSingle();
    this.Price = r.ReadInt32();
    this.Group = (SkillGroup) r.ReadByte();
    this.StaticBuff = r.ReadDesc<ObjectStats>();
    this.MultiplyBuff = r.ReadDesc<ObjectStats>();
    this.RequireSkillHash = r.ReadUInt32();
    this.SortWeight = (int) r.ReadUInt16();
    this.GUICard = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.GUICard
    });
    if ((int) cardGUID == 0)
      return;
    this.NextCard = (SkillCard) Game.Catalogue.FetchCard(cardGUID, CardView.Skill);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.NextCard
    });
  }
}
