﻿// Decompiled with JetBrains decompiler
// Type: DialogCharacterInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DialogCharacterInfo : MonoBehaviour
{
  public string characterName = string.Empty;

  public DialogCharacterAnimation GetAnimantion()
  {
    return this.GetComponent<DialogCharacterAnimation>();
  }

  public void ChangeCameraBox()
  {
    DialogCameraBox cameraBox = this.FindCameraBox(this.characterName);
    Camera.main.fieldOfView = cameraBox.CameraFOV;
    Camera.main.transform.position = cameraBox.transform.position;
    Camera.main.transform.rotation = cameraBox.transform.rotation;
  }

  public void ChangeCameraBoxToInit()
  {
    DialogCameraBox cameraBox = this.FindCameraBox("init");
    Camera.main.fieldOfView = cameraBox.CameraFOV;
    Camera.main.transform.position = cameraBox.transform.position;
    Camera.main.transform.rotation = cameraBox.transform.rotation;
  }

  private DialogCameraBox FindCameraBox(string cameraName)
  {
    string str = "camerabox_" + cameraName;
    DialogCameraBox[] dialogCameraBoxArray = (DialogCameraBox[]) Object.FindObjectsOfType(typeof (DialogCameraBox));
    if (dialogCameraBoxArray == null)
      return new DialogCameraBox();
    foreach (DialogCameraBox dialogCameraBox in dialogCameraBoxArray)
    {
      if (dialogCameraBox.name.Equals(str))
        return dialogCameraBox;
    }
    return new DialogCameraBox();
  }
}
