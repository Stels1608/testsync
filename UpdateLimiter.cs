﻿// Decompiled with JetBrains decompiler
// Type: UpdateLimiter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UpdateLimiter
{
  public bool UseDelay = true;
  private float UpdateInterval = 0.1f;
  private float lastUpdate;
  private int numLimitedUpdates;
  private int numUnlimitedUpdates;

  public UpdateLimiter(int numUpdatesPerSecond)
  {
    this.SetNumberOfUpdatesPerSecond(numUpdatesPerSecond);
  }

  public int debugNumLimitedUpdates()
  {
    return this.numLimitedUpdates;
  }

  public int debugNumUnlimitedUpdates()
  {
    return this.numUnlimitedUpdates;
  }

  public void SetNumberOfUpdatesPerSecond(int numUPS)
  {
    this.UseDelay = true;
    if (numUPS == -1)
      this.UseDelay = false;
    else
      this.UpdateInterval = 1f / (float) numUPS;
  }

  public bool CanUpdate()
  {
    ++this.numUnlimitedUpdates;
    if (!this.UseDelay)
      return true;
    if ((double) this.lastUpdate <= 0.01)
    {
      this.lastUpdate = Time.realtimeSinceStartup;
      return true;
    }
    if ((double) this.lastUpdate > (double) Time.realtimeSinceStartup - (double) this.UpdateInterval)
      return false;
    ++this.numLimitedUpdates;
    this.lastUpdate = Time.realtimeSinceStartup;
    return true;
  }
}
