﻿// Decompiled with JetBrains decompiler
// Type: GalaxyMapCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class GalaxyMapCard : Card
{
  public Dictionary<uint, MapStarDesc> Stars = new Dictionary<uint, MapStarDesc>();
  public int[] Tiers;

  public GalaxyMapCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    int num1 = r.ReadLength();
    for (int index = 0; index < num1; ++index)
    {
      MapStarDesc mapStarDesc = r.ReadDesc<MapStarDesc>();
      this.Stars[mapStarDesc.Id] = mapStarDesc;
    }
    int length = r.ReadLength();
    this.Tiers = new int[length];
    for (int index = 0; index < length; ++index)
      this.Tiers[index] = (int) r.ReadUInt16();
    Game.Galaxy.BaseScalingMultiplier = r.ReadInt32();
    ushort num2 = r.ReadUInt16();
    for (ushort index = 0; (int) index < (int) num2; ++index)
      Game.Galaxy.AddSectorScalingMultiplier(r.ReadInt32(), r.ReadInt32());
    SectorDesc[] starDescs = new SectorDesc[this.Stars.Count];
    int index1 = 0;
    using (Dictionary<uint, MapStarDesc>.KeyCollection.Enumerator enumerator = this.Stars.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        uint current = enumerator.Current;
        starDescs[index1] = new SectorDesc(current);
        starDescs[index1].ColonialThreatLevel = this.GetStar(current).ColonialThreatLevel;
        starDescs[index1].CylonThreatLevel = this.GetStar(current).CylonThreatLevel;
        ++index1;
      }
    }
    Game.Galaxy.SetStars(starDescs);
  }

  public MapStarDesc GetStar(uint sectorId)
  {
    return this.Stars[sectorId];
  }
}
