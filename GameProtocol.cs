﻿// Decompiled with JetBrains decompiler
// Type: GameProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GameProtocol : BgoProtocol
{
  private readonly HashSet<uint> requestWhoIs = new HashSet<uint>();
  private readonly HashSet<uint> subscribeInfo = new HashSet<uint>();
  private readonly HashSet<uint> unsubscribeInfo = new HashSet<uint>();
  private readonly HashSet<uint> moveInfo = new HashSet<uint>();
  private readonly HashSet<GameProtocol.CastDesc> castSlotAbilities = new HashSet<GameProtocol.CastDesc>();
  private readonly HashSet<GameProtocol.CastDesc> castImmutableSlotAbilities = new HashSet<GameProtocol.CastDesc>();
  private uint requestLoot;
  private uint target;
  private bool targetValid;
  private bool requestAsteroidMining;
  private uint asteroidId;

  public GameProtocol()
    : base(BgoProtocol.ProtocolID.Game)
  {
  }

  public static GameProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Game) as GameProtocol;
  }

  public void Quit()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 54);
    this.SendMessage(bw);
  }

  public void RequestAnchor(uint targetId)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 78);
    bw.Write(targetId);
    this.SendMessage(bw);
  }

  public void RequestUnanchor()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 77);
    this.SendMessage(bw);
  }

  public void RequestLaunchStrikes()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 79);
    this.SendMessage(bw);
  }

  public void JumpIn()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 61);
    this.SendMessage(bw);
  }

  public void RequestDock(uint objectID, float delay = 0.0f)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 45);
    bw.Write(objectID);
    bw.Write(delay);
    this.SendMessage(bw);
  }

  public void CancelDocking()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 102);
    this.SendMessage(bw);
  }

  public void RequestAnsStartQueue(byte Boolean)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 48);
    bw.Write(Boolean);
    this.SendMessage(bw);
  }

  public void RequestAnsJump(byte Boolean)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 50);
    bw.Write(Boolean);
    this.SendMessage(bw);
  }

  public void RequestFTLJump(uint sectorID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 46);
    bw.Write(sectorID);
    this.SendMessage(bw);
  }

  public void RequestGroupFTLJump(uint sectorID, List<Player> playersToJump, GameProtocol.Request request)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) request);
    bw.Write(sectorID);
    bw.Write((ushort) playersToJump.Count);
    for (int index = 0; index < playersToJump.Count; ++index)
      bw.Write(playersToJump[index].ServerID);
    this.SendMessage(bw);
  }

  public void RequestStopJump()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 65);
    this.SendMessage(bw);
  }

  public void RequestStopGroupJump()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 73);
    this.SendMessage(bw);
  }

  public void CastSlotAbility(ushort abilityId, uint[] targetIDs)
  {
    this.castSlotAbilities.Add(new GameProtocol.CastDesc()
    {
      AbilityID = abilityId,
      TargetIDs = targetIDs
    });
  }

  public void ToggleAbilityOn(ushort abilityId, uint[] targetIds)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 86);
    bw.Write(abilityId);
    bw.Write((ushort) targetIds.Length);
    foreach (uint targetId in targetIds)
      bw.Write(targetId);
    this.SendMessage(bw);
  }

  public void UpdateAbilityTargets(ushort abilityId, uint[] targetIds)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 88);
    bw.Write(abilityId);
    bw.Write((ushort) targetIds.Length);
    foreach (uint targetId in targetIds)
      bw.Write(targetId);
    this.SendMessage(bw);
  }

  public void ToggleAbilityOff(ushort abilityId)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 87);
    bw.Write(abilityId);
    this.SendMessage(bw);
  }

  public void CastImmutableSlotAbility(ushort abilityID, uint[] targetIDs)
  {
    this.castImmutableSlotAbilities.Add(new GameProtocol.CastDesc()
    {
      AbilityID = abilityID,
      TargetIDs = targetIDs
    });
  }

  public virtual void MoveToDirection(Euler3 direction)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 12);
    bw.Write(direction);
    this.SendMessage(bw);
  }

  public virtual void MoveToDirectionWithoutRoll(Euler3 direction)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 13);
    bw.Write(direction);
    this.SendMessage(bw);
  }

  public void SubscribeInfo(SpaceObject spaceObject)
  {
    spaceObject.IsSubscribed = true;
    this.unsubscribeInfo.Remove(spaceObject.ObjectID);
    this.subscribeInfo.Add(spaceObject.ObjectID);
  }

  public void UnSubscribeInfo(SpaceObject spaceObject)
  {
    if (!spaceObject.IsSubscribed)
      return;
    spaceObject.IsSubscribed = false;
    this.subscribeInfo.Remove(spaceObject.ObjectID);
    this.unsubscribeInfo.Add(spaceObject.ObjectID);
  }

  public void SetTarget(uint id)
  {
    this.target = id;
    this.targetValid = true;
  }

  public void RequestLoot(uint id)
  {
    this.requestLoot = id;
  }

  public virtual void WASD(QWEASD qweasd)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 29);
    bw.Write((byte) qweasd.Bitmask);
    this.SendMessage(bw);
  }

  public virtual void QWEASD(QWEASD qweasd)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 30);
    bw.Write((byte) qweasd.Bitmask);
    this.SendMessage(bw);
  }

  public virtual void TurnToDirectionStrikes(Euler3 directionInput, float rollInput, Vector2 strafeDirection)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 100);
    bw.Write(directionInput);
    bw.Write(rollInput);
    bw.Write(strafeDirection.x);
    bw.Write(strafeDirection.y);
    this.SendMessage(bw);
  }

  public virtual void TurnByPitchYawStrikes(Vector3 pitchYawRollFactor, Vector2 strafeDirection, float strafeMagnitude)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 101);
    bw.Write(pitchYawRollFactor);
    bw.Write(strafeDirection);
    bw.Write(strafeMagnitude);
    this.SendMessage(bw);
  }

  public void TakeLootItems(ushort lootID, List<ushort> itemIDs)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 43);
    bw.Write(lootID);
    bw.Write((ushort) (itemIDs.Count * 4));
    using (List<ushort>.Enumerator enumerator = itemIDs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        uint num = (uint) enumerator.Current;
        bw.Write(num);
      }
    }
    this.SendMessage(bw);
  }

  public void SetSpeed(ShipControlsBase.SpeedMode mode, float speed)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 56);
    bw.Write((byte) mode);
    bw.Write(speed);
    this.SendMessage(bw);
  }

  public void SetGear(Gear gear)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 57);
    bw.Write((byte) gear);
    this.SendMessage(bw);
  }

  public void RequestMining(uint asteroidID)
  {
    this.asteroidId = asteroidID;
    this.requestAsteroidMining = true;
  }

  public void CancelRequestMining()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 82);
    this.SendMessage(bw);
  }

  public void RequestMoveInfo(uint objectID)
  {
    this.moveInfo.Add(objectID);
  }

  public void SelectRespawnLocation(RespawnLocationInfo locationInfo)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 70);
    bw.Write(locationInfo.SectorId);
    bw.Write(locationInfo.CarrierPlayerId);
    this.SendMessage(bw);
  }

  public void RequestJumpToTarget(uint sectorId, uint playerId)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 75);
    bw.Write(sectorId);
    bw.Write(playerId);
    this.SendMessage(bw);
  }

  public void RequestCompleteJump()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 76);
    this.SendMessage(bw);
  }

  public void RequestBattlespace()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 80);
    this.SendMessage(bw);
  }

  [Obsolete("Use RequestBattlespace for both entering and leaving the Battlespace (server decides)")]
  public void RequestQuitBattlespace()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 81);
    this.SendMessage(bw);
  }

  public void RequestTournament()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 83);
    this.SendMessage(bw);
  }

  public void RequestQuitTournament()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 84);
    this.SendMessage(bw);
  }

  public void RequestJumpToBeacon(uint sectorId)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 85);
    bw.Write(sectorId);
    this.SendMessage(bw);
  }

  public override void UpdateMessage()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bool flag = false;
    if ((int) this.requestLoot != 0)
    {
      flag = true;
      bw.Write((ushort) 41);
      bw.Write(this.requestLoot);
      this.requestLoot = 0U;
    }
    if (this.requestWhoIs.Count > 0)
    {
      flag = true;
      using (HashSet<uint>.Enumerator enumerator = this.requestWhoIs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          uint current = enumerator.Current;
          bw.Write((ushort) 3);
          bw.Write(current);
        }
      }
      this.requestWhoIs.Clear();
    }
    if (this.unsubscribeInfo.Count > 0)
    {
      flag = true;
      using (HashSet<uint>.Enumerator enumerator = this.unsubscribeInfo.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          uint current = enumerator.Current;
          bw.Write((ushort) 11);
          bw.Write(current);
        }
      }
      this.unsubscribeInfo.Clear();
    }
    if (this.subscribeInfo.Count > 0)
    {
      flag = true;
      using (HashSet<uint>.Enumerator enumerator = this.subscribeInfo.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          uint current = enumerator.Current;
          bw.Write((ushort) 10);
          bw.Write(current);
        }
      }
      this.subscribeInfo.Clear();
    }
    if (this.moveInfo.Count > 0)
    {
      flag = true;
      bw.Write((ushort) 63);
      bw.Write((ushort) this.moveInfo.Count);
      using (HashSet<uint>.Enumerator enumerator = this.moveInfo.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          uint current = enumerator.Current;
          bw.Write(current);
        }
      }
      this.moveInfo.Clear();
    }
    if (this.targetValid)
    {
      flag = true;
      bw.Write((ushort) 25);
      bw.Write(this.target);
      this.targetValid = false;
    }
    if (this.requestAsteroidMining)
    {
      flag = true;
      bw.Write((ushort) 35);
      bw.Write(this.asteroidId);
      this.requestAsteroidMining = false;
    }
    if (this.castSlotAbilities.Count > 0)
    {
      flag = true;
      using (HashSet<GameProtocol.CastDesc>.Enumerator enumerator = this.castSlotAbilities.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GameProtocol.CastDesc current = enumerator.Current;
          bw.Write((ushort) 21);
          bw.Write(current.AbilityID);
          bw.Write((ushort) current.TargetIDs.Length);
          foreach (uint targetId in current.TargetIDs)
            bw.Write(targetId);
        }
      }
      this.castSlotAbilities.Clear();
    }
    if (this.castImmutableSlotAbilities.Count > 0)
    {
      flag = true;
      using (HashSet<GameProtocol.CastDesc>.Enumerator enumerator = this.castImmutableSlotAbilities.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GameProtocol.CastDesc current = enumerator.Current;
          bw.Write((ushort) 22);
          bw.Write(current.AbilityID);
          bw.Write((ushort) current.TargetIDs.Length);
          foreach (uint targetId in current.TargetIDs)
            bw.Write(targetId);
        }
      }
      this.castImmutableSlotAbilities.Clear();
    }
    if (!flag)
      return;
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    GameProtocol.Reply reply1 = (GameProtocol.Reply) msgType;
    if ((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null && reply1 != GameProtocol.Reply.AskStartQueue && reply1 != GameProtocol.Reply.AskJump)
      return;
    this.DebugAddMsg((Enum) reply1);
    GameProtocol.Reply reply2 = reply1;
    switch (reply2)
    {
      case GameProtocol.Reply.RemoveMe:
        SpaceLevel.GetLevel().ShipControls.Enabled = false;
        switch (br.ReadByte())
        {
          case 1:
            Timer.CreateTimer("DisconnectTimer", 0.5f, 0.5f, new Timer.TickHandler(SpaceLevel.GetLevel().StopSector));
            break;
          case 2:
            SpaceObject spaceObject1 = SpaceLevel.GetLevel().GetObjectRegistry().Get(br.ReadUInt32());
            if (spaceObject1 != null)
            {
              string str = BsgoLocalization.Get("%$bgo.death_notificator.message1_full%", (object) spaceObject1.Name);
              FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification(str, NotificationCategory.Negative));
              Game.ChatStorage.DangerCombatLog(str);
            }
            GuiDockUndock.HideUndock();
            break;
          case 3:
            if (SpaceLevel.GetLevel().HavePlayerShip)
            {
              SpaceLevel.GetLevel().PlayerShip.JumpOut();
              break;
            }
            break;
          case 5:
            if (SpaceLevel.GetLevel().HavePlayerShip)
            {
              SpaceLevel.GetLevel().PlayerShip.Dock();
              break;
            }
            break;
        }
        using (IEnumerator<SpaceObject> enumerator = SpaceLevel.GetLevel().GetObjectRegistry().GetAll().GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.UnSubscribe();
          break;
        }
      case GameProtocol.Reply.TimeOrigin:
        SpaceLevel.GetLevel().SetTimeOrigin(br.ReadInt64());
        break;
      case GameProtocol.Reply.StopGroupJump:
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new GroupJumpNotification(GroupJumpNotification.GroupJumpState.NotifyMemberCancel, (object) Game.Players[br.ReadUInt32()].Name));
        break;
      case GameProtocol.Reply.LeaderStopGroupJump:
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new GroupJumpNotification(GroupJumpNotification.GroupJumpState.NotifyLeaderCancel, (object) null));
        JumpActionsHandler.LeaderCancelJumpSequence();
        break;
      case GameProtocol.Reply.NotEnoughTylium:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GameProtocol.\u003CParseMessage\u003Ec__AnonStoreyE7 messageCAnonStoreyE7 = new GameProtocol.\u003CParseMessage\u003Ec__AnonStoreyE7();
        uint cardGUID1 = br.ReadUInt32();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE7.card = (SectorCard) Game.Catalogue.FetchCard(cardGUID1, CardView.Sector);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyE7.card.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyE7.\u003C\u003Em__234));
        JumpActionsHandler.JumpFailed();
        break;
      case GameProtocol.Reply.UpdateRoles:
        uint index1 = br.ReadUInt32();
        uint num1 = br.ReadUInt32();
        Player player = Game.Players[index1];
        if (player == null)
          break;
        player.Roles = (BgoAdminRoles) num1;
        if (!player.IsMe)
          break;
        GUISlotManager guiSlotManager = Game.GUIManager.Find<GUISlotManager>();
        if (guiSlotManager == null)
          break;
        guiSlotManager.player.SetName(player.GetTaggedName(Game.Me.Name));
        break;
      case GameProtocol.Reply.StopJump:
        JumpActionsHandler.CancelJumpSequence();
        break;
      case GameProtocol.Reply.ChangeVisibility:
        uint objectID1 = br.ReadUInt32();
        bool isVisible = br.ReadBoolean();
        ChangeVisibilityReason reason = (ChangeVisibilityReason) br.ReadByte();
        PlayerShip playerShip = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID1) as PlayerShip;
        if (playerShip == null)
        {
          Debug.LogError((object) ("ChangeVisibility START: visible: " + (object) isVisible + " reason: " + (object) reason + "NO SHIP!!!!!11111elf"));
          break;
        }
        playerShip.ChangeVisibility(isVisible, reason);
        break;
      case GameProtocol.Reply.UpdateFactionGroup:
        uint objectID2 = br.ReadUInt32();
        FactionGroup factionGroup = !br.ReadBoolean() ? FactionGroup.Group0 : FactionGroup.Group1;
        SpaceObject spaceObject2 = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID2);
        spaceObject2.FactionGroup = factionGroup;
        FacadeFactory.GetInstance().SendMessage(Message.FactionGroupChanged, (object) spaceObject2);
        break;
      case GameProtocol.Reply.MineField:
        int num2 = (int) br.ReadUInt32();
        SpaceObject spaceObject3 = SpaceLevel.GetLevel().GetObjectRegistry().Get(br.ReadUInt32());
        if (spaceObject3 == null)
          break;
        Vector3 position = spaceObject3.Position;
        position.x = position.x + (float) ((double) spaceObject3.WorldCard.Radius * 2.0 * (2.0 * (double) UnityEngine.Random.value - 1.0));
        position.y = position.y + (float) ((double) spaceObject3.WorldCard.Radius * 2.0 * (2.0 * (double) UnityEngine.Random.value - 1.0));
        FXManager.Instance.PutEffect((EffectState) new ExplosionEffectState(position, spaceObject3.Rotation, (ExplosionArgs) new MissileExplosionArgs()
        {
          ExplosionView = MissileExplosionArgs.MissileExplosionView.Standard,
          ExplosionTier = 1
        }, 0.0f));
        break;
      case GameProtocol.Reply.ObjectState:
        SpaceLevel.GetLevel().GetObjectRegistry().UpdateState(br.ReadDesc<SpaceObjectState>());
        break;
      case GameProtocol.Reply.FlareReleased:
        FacadeFactory.GetInstance().SendMessage(Message.FlareReleased, (object) SpaceLevel.GetLevel().GetObjectRegistry().Get(br.ReadUInt32()));
        break;
      case GameProtocol.Reply.LostAbilityTarget:
        int num3 = (int) br.ReadUInt32();
        int num4 = br.ReadLength();
        for (int index2 = 0; index2 < num4; ++index2)
        {
          int num5 = (int) br.ReadUInt16();
        }
        break;
      case GameProtocol.Reply.LostJumpTransponder:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GameProtocol.\u003CParseMessage\u003Ec__AnonStoreyE8 messageCAnonStoreyE8 = new GameProtocol.\u003CParseMessage\u003Ec__AnonStoreyE8();
        uint cardGUID2 = br.ReadUInt32();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE8.card = (SectorCard) Game.Catalogue.FetchCard(cardGUID2, CardView.Sector);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyE8.card.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyE8.\u003C\u003Em__235));
        JumpActionsHandler.JumpFailed();
        break;
      case GameProtocol.Reply.DockingDelay:
        GuiDockUndock.Instance.DockingRequested(br.ReadSingle());
        SpaceLevel.GetLevel().Docking = true;
        break;
      case GameProtocol.Reply.ChangedPlayerSpeed:
        FacadeFactory.GetInstance().SendMessage(Message.ShipSpeedChangeCall, (object) new DataChangeSpeed(ShipControlsBase.SpeedMode.Abs, br.ReadSingle()));
        break;
      case GameProtocol.Reply.ShortCircuitResult:
        bool flag1 = br.ReadBoolean();
        uint objectID3 = br.ReadUInt32();
        uint objectID4 = br.ReadUInt32();
        SpaceObject spaceObject4 = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID3);
        SpaceObject spaceObject5 = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID4);
        if (spaceObject4 == null || spaceObject5 == null || spaceObject4.IsMe && flag1)
          break;
        if (spaceObject4.IsMe && !flag1)
        {
          FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new ShortCircuitNotification(spaceObject5.Name));
          break;
        }
        if (!spaceObject5.IsMe || !flag1)
          break;
        break;
      case GameProtocol.Reply.OutpostStateBroadcast:
        int OP1 = (int) br.ReadUInt16();
        float delta1 = br.ReadSingle();
        int OP2 = (int) br.ReadUInt16();
        float delta2 = br.ReadSingle();
        uint serverId = SpaceLevel.GetLevel().ServerID;
        if (Game.Me.Faction == Faction.Colonial)
        {
          Game.Galaxy.SetSectorOP(serverId, Faction.Colonial, OP1);
          Game.Galaxy.GetStar(serverId).SetColonialDelta(delta1);
          break;
        }
        Game.Galaxy.SetSectorOP(serverId, Faction.Cylon, OP2);
        Game.Galaxy.GetStar(serverId).SetCylonDelta(delta2);
        break;
      case GameProtocol.Reply.RespawnOptions:
        List<uint> uintList1 = br.ReadUInt32List();
        List<uint> uintList2 = br.ReadUInt32List();
        DeathDesc deathDesc = new DeathDesc();
        deathDesc.RespawnLocations = new List<RespawnLocationInfo>();
        if (uintList1.Count == uintList2.Count)
        {
          for (int index2 = 0; index2 < uintList1.Count; ++index2)
          {
            RespawnLocationInfo respawnLocationInfo = new RespawnLocationInfo(uintList1[index2], uintList2[index2]);
            deathDesc.RespawnLocations.Add(respawnLocationInfo);
          }
          Timer.CreateTimer("DisconnectTimer", 2f, 2f, new Timer.TickParametrizedHandler(SpaceLevel.GetLevel().PlayerDied), (object) deathDesc, true);
        }
        else
          Log.Add("Invalid respawn location list received from server.");
        FacadeFactory.GetInstance().SendMessage(Message.Death);
        break;
      case GameProtocol.Reply.AnchorDeclined:
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification(BsgoLocalization.Get("bgo.etc.anchor_declined"), NotificationCategory.Negative));
        break;
      case GameProtocol.Reply.DetachedToSpace:
        break;
      case GameProtocol.Reply.RetachedToSpace:
        GalaxyMapMain galaxyMapMain = Game.GUIManager.Find<GalaxyMapMain>();
        if (galaxyMapMain == null)
          break;
        galaxyMapMain.isJumpActive = false;
        break;
      default:
        switch (reply2)
        {
          case GameProtocol.Reply.MissileDecoyed:
            SpaceLevel.GetLevel().IncomingMissiles.Remove(br.ReadUInt32());
            return;
          case GameProtocol.Reply.SyncMove:
            uint objectID5 = br.ReadUInt32();
            Tick syncTick = br.ReadTick();
            MovementFrame syncFrame = br.ReadDesc<MovementFrame>();
            Maneuver maneuver1 = br.ReadManeuver();
            if (maneuver1 == null)
              return;
            SpaceLevel.GetLevel().GetObjectRegistry().AddManeuver(objectID5, maneuver1, syncTick, syncFrame);
            return;
          case GameProtocol.Reply.Cast:
            ushort slotID = br.ReadUInt16();
            GameProtocol.SlotCategory slotCategory = (GameProtocol.SlotCategory) br.ReadByte();
            ShipAbility.CastReply reply3 = (ShipAbility.CastReply) br.ReadByte();
            switch (slotCategory)
            {
              case GameProtocol.SlotCategory.Slot:
                ShipSlot slot1 = Game.Me.ActiveShip.GetSlot(slotID);
                if (slot1 == null || slot1.Ability == null)
                  return;
                slot1.Ability.OnCasted(reply3);
                return;
              case GameProtocol.SlotCategory.ImmutableSlot:
                return;
              default:
                return;
            }
          case GameProtocol.Reply.StopSlotAbility:
            short num6 = br.ReadInt16();
            if ((int) num6 == -1)
            {
              Game.Me.ActiveShip.DisableAllSlotAbilities();
              return;
            }
            ShipSlot slot2 = Game.Me.ActiveShip.GetSlot((ushort) num6);
            if (slot2 == null || slot2.Ability == null)
              return;
            slot2.Ability.On = false;
            return;
          default:
            switch (reply2)
            {
              case GameProtocol.Reply.Info:
                uint num7 = br.ReadUInt32();
                SpaceObject spaceObject6 = SpaceLevel.GetLevel().GetObjectRegistry().Get(num7);
                if (spaceObject6 != null)
                {
                  spaceObject6.Props.Read(br);
                  return;
                }
                SpaceSubscribeInfo spaceSubscribeInfo = new SpaceSubscribeInfo(num7);
                spaceSubscribeInfo.Read(br);
                Debug.LogWarning((object) ("Unknown Subscribe Info for Id: " + (object) num7 + " Content: " + (object) spaceSubscribeInfo));
                return;
              case GameProtocol.Reply.WhoIs:
                SpaceLevel.GetLevel().GetObjectRegistry().CreateSpaceObject(br.ReadUInt32(), br);
                return;
              case GameProtocol.Reply.Move:
                uint objectID6 = br.ReadUInt32();
                Maneuver maneuver2 = br.ReadManeuver();
                if (maneuver2 == null)
                  return;
                SpaceLevel.GetLevel().GetObjectRegistry().AddManeuver(objectID6, maneuver2, Tick.Current, MovementFrame.Invalid);
                return;
              case GameProtocol.Reply.ObjectLeft:
                int num8 = (int) br.ReadUInt16();
                for (int index2 = 0; index2 < num8; ++index2)
                {
                  uint num5 = br.ReadUInt32();
                  br.ReadTick();
                  RemovingCause removingCause = (RemovingCause) br.ReadByte();
                  SpaceObject spaceObject7 = SpaceLevel.GetLevel().GetObjectRegistry().Get(num5);
                  if (spaceObject7 == null)
                  {
                    Debug.LogError((object) ("Trying to destroy spaceObject with id " + (object) num5 + ",but this is NULL"));
                  }
                  else
                  {
                    if (spaceObject7 is Missile)
                      SpaceLevel.GetLevel().IncomingMissiles.Remove(num5);
                    bool flag2 = true;
                    switch (removingCause)
                    {
                      case RemovingCause.Death:
                        int num9 = (int) br.ReadUInt32();
                        spaceObject7.OnDestroyed();
                        break;
                      case RemovingCause.JumpOut:
                        flag2 = false;
                        Ship ship1 = spaceObject7 as Ship;
                        if (ship1 != null && !ship1.IsMe)
                        {
                          ship1.JumpOut();
                          break;
                        }
                        break;
                      case RemovingCause.Dock:
                        flag2 = false;
                        Ship ship2 = spaceObject7 as Ship;
                        if (ship2 != null && !ship2.IsMe)
                        {
                          ship2.Dock();
                          break;
                        }
                        break;
                      case RemovingCause.Hit:
                        SpaceObject spaceObject8 = SpaceLevel.GetLevel().GetObjectRegistry().Get(br.ReadUInt32());
                        Missile missile = spaceObject7 as Missile;
                        if (missile != null)
                        {
                          MissileScript modelScript = missile.GetModelScript<MissileScript>();
                          if ((UnityEngine.Object) modelScript != (UnityEngine.Object) null)
                          {
                            modelScript.HitTarget = spaceObject8;
                            modelScript.Terminate();
                            flag2 = false;
                          }
                        }
                        Mine mine = spaceObject7 as Mine;
                        if (mine != null)
                        {
                          Mine.MineExplosion(mine);
                          break;
                        }
                        break;
                    }
                    if (flag2)
                      SpaceLevel.GetLevel().GetObjectRegistry().Remove(num5, removingCause);
                  }
                }
                return;
              default:
                switch (reply2)
                {
                  case GameProtocol.Reply.Collide:
                    if (!SpaceLevel.GetLevel().HavePlayerShip)
                      return;
                    ImpactSoundPlayer modelScript1 = SpaceLevel.GetLevel().PlayerShip.GetModelScript<ImpactSoundPlayer>();
                    if (!((UnityEngine.Object) modelScript1 != (UnityEngine.Object) null))
                      return;
                    modelScript1.Play();
                    return;
                  case GameProtocol.Reply.FTLCharge:
                    // ISSUE: object of a compiler-generated type is created
                    // ISSUE: variable of a compiler-generated type
                    GameProtocol.\u003CParseMessage\u003Ec__AnonStoreyE4 messageCAnonStoreyE4 = new GameProtocol.\u003CParseMessage\u003Ec__AnonStoreyE4();
                    float num10 = br.ReadSingle();
                    // ISSUE: reference to a compiler-generated field
                    messageCAnonStoreyE4.sectorGUID = br.ReadUInt32();
                    if (!br.ReadBoolean())
                    {
                      // ISSUE: object of a compiler-generated type is created
                      // ISSUE: variable of a compiler-generated type
                      GameProtocol.\u003CParseMessage\u003Ec__AnonStoreyE6 messageCAnonStoreyE6 = new GameProtocol.\u003CParseMessage\u003Ec__AnonStoreyE6();
                      // ISSUE: reference to a compiler-generated field
                      messageCAnonStoreyE6.\u003C\u003Ef__ref\u0024228 = messageCAnonStoreyE4;
                      FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new GroupJumpNotification(GroupJumpNotification.GroupJumpState.NotifyIncluded, (object) null));
                      // ISSUE: reference to a compiler-generated field
                      messageCAnonStoreyE6.notificationTimer = (Timer) null;
                      // ISSUE: reference to a compiler-generated field
                      // ISSUE: reference to a compiler-generated method
                      messageCAnonStoreyE6.notificationTimer = Timer.CreateTimer("GroupJumpLocationNotify", 4f, 4f, new Timer.TickHandler(messageCAnonStoreyE6.\u003C\u003Em__233));
                      Game.Me.Party.inGroupJump = true;
                      Game.RegisterDialog((IGUIPanel) new GUICancelGroupJumpWindow(), true);
                    }
                    Game.JustJumped = true;
                    GalaxyMapMain.JumpActive = true;
                    FacadeFactory.GetInstance().SendMessage(Message.FtlJumpStarted);
                    JumpCountdown.Show(num10);
                    SpaceLevel.GetLevel().sectorSfx.PlayFTLChargingSound(num10);
                    return;
                  case GameProtocol.Reply.VirusBlocked:
                    int num11 = (int) br.ReadUInt32();
                    FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new VirusBlockedNotification());
                    return;
                  default:
                    switch (reply2)
                    {
                      case GameProtocol.Reply.AskStartQueue:
                        int num12 = (int) br.ReadUInt32();
                        byte num13 = br.ReadByte();
                        ushort num14 = br.ReadUInt16();
                        if ((int) num13 == 1)
                        {
                          if ((int) num14 > 0)
                          {
                            FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification(BsgoLocalization.Get("%$bgo.group_jump.room_in_sector%", (object) num14), NotificationCategory.Neutral));
                            return;
                          }
                          MessageBoxFactory.CreateDialogBox(BsgoCanvas.Windows).SetContent((OnMessageBoxClose) (actionType =>
                          {
                            if (actionType == MessageBoxActionType.Ok)
                              this.RequestAnsStartQueue((byte) 1);
                            else
                              this.RequestAnsStartQueue((byte) 0);
                          }), "%$bgo.GalaxyMap.JumpWindows.agree_layout.title%", BsgoLocalization.Get("%$bgo.GalaxyMap.JumpWindows.jump_layout.tooMany%") + "\n\n" + BsgoLocalization.Get("%$bgo.GalaxyMap.JumpWindows.jump_layout.question%"), 0U);
                          return;
                        }
                        if ((int) num13 != 2 || Game.GUIManager.Find<MessageBoxManager>() == null)
                          return;
                        Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox(BsgoLocalization.Get("%$bgo.game_protocol.ask_start_queue%"));
                        return;
                      case GameProtocol.Reply.AskJump:
                        int num15 = (int) br.ReadUInt32();
                        DialogBoxUi dialogBox = MessageBoxFactory.CreateDialogBox(BsgoCanvas.Windows);
                        OnMessageBoxClose onClose = (OnMessageBoxClose) (actionType =>
                        {
                          if (actionType == MessageBoxActionType.Ok)
                          {
                            JumpCountdown.Show();
                            this.RequestAnsJump((byte) 1);
                          }
                          else
                            this.RequestAnsJump((byte) 0);
                        });
                        dialogBox.SetContent(onClose, "%$bgo.GalaxyMap.JumpWindows.agree_layout.title%", "%$bgo.GalaxyMap.JumpWindows.agree_layout.question%", "%$bgo.GalaxyMap.JumpWindows.agree_layout.yes%", "%$bgo.GalaxyMap.JumpWindows.agree_layout.no%", 15U);
                        dialogBox.ShowTimer(true);
                        return;
                      default:
                        if (reply2 != GameProtocol.Reply.WeaponShot)
                        {
                          if (reply2 != GameProtocol.Reply.Scan)
                          {
                            if (reply2 == GameProtocol.Reply.CombatInfo)
                            {
                              bool flag2 = br.ReadBoolean();
                              uint objectID7 = br.ReadUInt32();
                              float f = br.ReadSingle();
                              byte num5 = br.ReadByte();
                              bool flag3 = 1 == ((int) num5 & 1);
                              bool criticalHit = 2 == ((int) num5 & 2);
                              SpaceObject target = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID7);
                              SpaceObject spaceObject7 = (SpaceObject) SpaceLevel.GetLevel().PlayerShip;
                              string str1 = "?";
                              if (target != null)
                                str1 = target.Name;
                              float damage = Mathf.Round(Mathf.Abs(f));
                              string str2 = damage.ToString();
                              if (criticalHit)
                                str2 = str2 + " " + BsgoLocalization.Get("bgo.common.critical_hit");
                              if (spaceObject7 == null)
                                return;
                              if ((double) f > 0.0)
                              {
                                if (flag2)
                                {
                                  Game.ChatStorage.DangerCombatLog(BsgoLocalization.Get("%$bgo.game_protocol.combat_info%", (object) str2, (object) str1));
                                  return;
                                }
                                if (spaceObject7 == target)
                                {
                                  Game.ChatStorage.NiceCombatLog(BsgoLocalization.Get("%$bgo.game_protocol.combat_info_1%", (object) str2));
                                  return;
                                }
                                Game.ChatStorage.NiceCombatLog(BsgoLocalization.Get("%$bgo.game_protocol.combat_info_2%", (object) str2, (object) str1));
                                return;
                              }
                              if ((double) f >= 0.0)
                                return;
                              if (flag2)
                              {
                                if (target != null)
                                  FacadeFactory.GetInstance().SendMessage(Message.DamageDoneInfo, (object) new DamageCombatInfo(target, damage, criticalHit));
                                Game.ChatStorage.OuchCombatLog(BsgoLocalization.Get("%$bgo.game_protocol.combat_info_3%", (object) str2, (object) str1));
                                if (!flag3)
                                  return;
                                Game.ChatStorage.OuchCombatLog(BsgoLocalization.Get("%$bgo.game_protocol.combat_info_5%", (object) str1));
                                return;
                              }
                              FacadeFactory.GetInstance().SendMessage(Message.DamageReceivedInfo, (object) new DamageCombatInfo(target, damage, criticalHit));
                              Game.ChatStorage.DangerCombatLog(BsgoLocalization.Get("%$bgo.game_protocol.combat_info_4%", (object) str1, (object) str2));
                              if (flag3)
                                Game.ChatStorage.DangerCombatLog(BsgoLocalization.Get("%$bgo.game_protocol.combat_info_6%", (object) str1));
                              TargetSelector.AutoTargetAfterAttack(target);
                              if ((Game.GUIManager.Find<GalaxyMapMain>() == null || !Game.GUIManager.Find<GalaxyMapMain>().IsRendered) && (Game.GUIManager.Find<GUISystemMap>() == null || !Game.GUIManager.Find<GUISystemMap>().IsRendered) || target != null && target.SpaceEntityType == SpaceEntityType.Asteroid)
                                return;
                              FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification(BsgoLocalization.Get("%$bgo.game_protocol.you_are_under_attack%"), NotificationCategory.Negative));
                              return;
                            }
                            DebugUtility.LogError("Unknown MessageType in Game protocol: " + (object) reply1);
                            return;
                          }
                          (SpaceLevel.GetLevel().GetObjectRegistry().Get(br.ReadUInt32()) as Asteroid).InformBeingScanned((ItemCountable) ItemFactory.ReadItem(br), br.ReadBoolean(), br.ReadDesc<Price>());
                          return;
                        }
                        uint objectID8 = br.ReadUInt32();
                        ushort objectPointHash = br.ReadUInt16();
                        uint objectID9 = br.ReadUInt32();
                        WeaponFxType weaponFxType = (WeaponFxType) br.ReadByte();
                        SpaceObject target1 = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID8);
                        SpaceObject target2 = objectID9 <= 0U ? (SpaceObject) null : SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID9);
                        if (target2 != null && target2.IsMe && SpaceLevel.GetLevel().GetPlayerTarget() == null)
                          TargetSelector.SelectTargetRequest(target1, false);
                        if (target1 == null)
                          return;
                        Weaponry modelScript2 = target1.GetModelScript<Weaponry>();
                        if (!((UnityEngine.Object) modelScript2 != (UnityEngine.Object) null))
                          return;
                        modelScript2.Fire(target2, objectPointHash, weaponFxType);
                        return;
                    }
                }
            }
        }
    }
  }

  private struct CastDesc
  {
    public ushort AbilityID;
    public uint[] TargetIDs;
  }

  public enum JumpRejectReason
  {
    JumpAccepted,
    SectorBusy,
    SectorNotAvailable,
  }

  public enum SlotCategory
  {
    Slot,
    ImmutableSlot,
  }

  public enum Reply : ushort
  {
    Info = 2,
    WhoIs = 4,
    Move = 6,
    ObjectLeft = 7,
    WeaponShot = 13,
    MissileDecoyed = 18,
    SyncMove = 20,
    Cast = 22,
    StopSlotAbility = 24,
    Scan = 34,
    CombatInfo = 40,
    AskStartQueue = 47,
    AskJump = 49,
    Collide = 55,
    FTLCharge = 58,
    VirusBlocked = 59,
    RemoveMe = 69,
    TimeOrigin = 70,
    StopGroupJump = 76,
    LeaderStopGroupJump = 77,
    NotEnoughTylium = 81,
    UpdateRoles = 83,
    [Obsolete("Covered by ObjectState now")] PaintTheTarget = 84,
    [Obsolete("Covered by ObjectState now")] UnpaintTheTarget = 85,
    StopJump = 86,
    ChangeVisibility = 87,
    UpdateFactionGroup = 88,
    MineField = 90,
    ObjectState = 91,
    FlareReleased = 92,
    LostAbilityTarget = 93,
    LostJumpTransponder = 94,
    DockingDelay = 95,
    ChangedPlayerSpeed = 96,
    ShortCircuitResult = 97,
    OutpostStateBroadcast = 98,
    RespawnOptions = 99,
    AnchorDeclined = 100,
    DetachedToSpace = 104,
    RetachedToSpace = 105,
  }

  public enum Request : ushort
  {
    WhoIs = 3,
    SubscribeInfo = 10,
    UnSubscribeInfo = 11,
    MoveToDirection = 12,
    MoveToDirectionWithoutRoll = 13,
    CastSlotAbility = 21,
    CastImmutableSlotAbility = 22,
    LockTarget = 25,
    WASD = 29,
    QWEASD = 30,
    Mining = 35,
    Loot = 41,
    TakeLootItems = 43,
    Dock = 45,
    Jump = 46,
    AnsStartQueue = 48,
    AnsJump = 50,
    [Obsolete] Follow = 52,
    Quit = 54,
    SetSpeed = 56,
    SetGear = 57,
    JumpIn = 61,
    MoveInfo = 63,
    StopJump = 65,
    SelectRespawnLocation = 70,
    GroupJump = 72,
    StopGroupJump = 73,
    RequestJumpToTarget = 75,
    CompleteJump = 76,
    RequestUnanchor = 77,
    RequestAnchor = 78,
    RequestLaunchStrikes = 79,
    RequestBattlespace = 80,
    RequestQuitBattlespace = 81,
    CancelMiningRequest = 82,
    RequestTournament = 83,
    RequestQuitTournament = 84,
    RequestJumpToBeacon = 85,
    ToggleAbilityOn = 86,
    ToggleAbilityOff = 87,
    UpdateAbilityTargets = 88,
    GroupJumpToBeacon = 89,
    TurnToDirectionStrikes = 100,
    TurnByPitchYawStrikes = 101,
    CancelDocking = 102,
    GroupJumpToTarget = 103,
  }
}
