﻿// Decompiled with JetBrains decompiler
// Type: BannerWindowUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public abstract class BannerWindowUi : MessageBoxBase
{
  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.SetOkButtonText(Tools.ParseMessage("%$bgo.common.ok%"));
    this.ShowOldUi(false);
  }

  public void SetContent(OnMessageBoxClose onClose, BannerCard bannerCard)
  {
    this.SetContent(onClose, bannerCard.Title, bannerCard.ArtworkPath, bannerCard.Description, bannerCard.FooterText);
  }

  public void SetContent(OnMessageBoxClose onClose, string title, string artworkPath, string description, string footerText)
  {
    this.AddOnClosedAction(onClose);
    this.SetTitle(Tools.ParseMessage(title).ToUpper());
    this.SetMainText(Tools.ParseMessage(description));
    this.SetFooterText(Tools.ParseMessage(footerText));
    Texture2D artwork = (Texture2D) Resources.Load(artworkPath);
    if ((Object) artwork != (Object) null)
      this.SetArtwork(artwork);
    else
      Debug.LogError((object) ("BannerWindowUi(): Couldn't load artwork from path: " + artworkPath));
  }

  protected abstract void SetTitle(string title);

  protected abstract void SetArtwork(Texture2D artwork);

  protected abstract void SetFooterText(string footerText);

  private void ShowOldUi(bool show)
  {
    FacadeFactory.GetInstance().SendMessage(Message.ToggleOldUi, (object) show);
  }

  protected override void OnDestroy()
  {
    base.OnDestroy();
    this.ShowOldUi(true);
  }
}
