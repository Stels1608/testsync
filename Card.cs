﻿// Decompiled with JetBrains decompiler
// Type: Card
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public abstract class Card : ILoadable, IProtocolRead
{
  private uint cardGUID;
  private Flag isLoaded;

  public Flag IsLoaded
  {
    get
    {
      return this.isLoaded;
    }
  }

  public uint CardGUID
  {
    get
    {
      return this.cardGUID;
    }
    set
    {
      this.cardGUID = value;
    }
  }

  public Card(uint cardGUID)
  {
    this.cardGUID = cardGUID;
    this.isLoaded = new Flag();
  }

  public void Load(BgoProtocolReader r)
  {
    this.Read(r);
    this.IsLoaded.Set();
  }

  public abstract void Read(BgoProtocolReader r);
}
