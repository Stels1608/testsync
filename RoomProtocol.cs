﻿// Decompiled with JetBrains decompiler
// Type: RoomProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class RoomProtocol : BgoProtocol
{
  public RoomProtocol()
    : base(BgoProtocol.ProtocolID.Room)
  {
  }

  public static RoomProtocol GetProtocol()
  {
    return (RoomProtocol) Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Room);
  }

  public void Talk(string NPC)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 0);
    bw.Write(NPC);
    this.SendMessage(bw);
  }

  public void Quit()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 5);
    this.SendMessage(bw);
  }

  public void Enter()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 6);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    RoomProtocol.Reply reply = (RoomProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case RoomProtocol.Reply.Talk:
        string npc = br.ReadString();
        if (!((UnityEngine.Object) RoomLevel.GetLevel() != (UnityEngine.Object) null))
          break;
        RoomLevel.GetLevel().ReceiveStartDialog(npc);
        using (IEnumerator<GuiRewardToast> enumerator = Game.GUIManager.FindAll<GuiRewardToast>().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            GuiRewardToast current = enumerator.Current;
            current.IsRendered = false;
            Game.GUIManager.RemovePanel((IGUIPanel) current);
          }
          break;
        }
      case RoomProtocol.Reply.NpcMarks:
        Debug.LogError((object) "ToDo: remove here");
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Hub protocol: " + (object) reply);
        break;
    }
  }

  public enum Request : ushort
  {
    Talk = 0,
    NpcMarks = 2,
    EnterDoor = 4,
    Quit = 5,
    Enter = 6,
  }

  public enum Reply : ushort
  {
    Talk = 1,
    NpcMarks = 3,
  }
}
