﻿// Decompiled with JetBrains decompiler
// Type: DebugMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugMessage : DebugBehaviour<DebugMessage>
{
  private string info = string.Empty;
  private string button = "Ok";
  private Vector2 scroll = new Vector2();
  private List<string> msgs = new List<string>();
  private bool isFirst = true;
  private bool isResize;

  private void Start()
  {
    this.windowID = 3;
    this.SetSize(300f, 100f);
  }

  public static void Add(string msg)
  {
    DebugBehaviour<DebugMessage>.Show();
    DebugBehaviour<DebugMessage>.instance.AddMsg(msg);
  }

  private void AddMsg(string msg)
  {
    this.msgs.Add(msg);
    if (!this.isFirst)
      return;
    this.Next();
    this.isFirst = false;
  }

  private void Update()
  {
    this.button = "Ok (" + (object) this.msgs.Count + ")";
  }

  private void Next()
  {
    if (this.msgs.Count > 0)
    {
      this.info = this.msgs[0];
      this.msgs.RemoveAt(0);
      this.isResize = false;
    }
    else
      DebugBehaviour<DebugMessage>.Close();
  }

  protected override void WindowFunc()
  {
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.Label(this.info);
    GUILayout.EndScrollView();
    if (GUILayout.Button(this.button))
      this.Next();
    if (this.isResize)
      return;
    Size size = BgoDebug.CalcTextSize(this.info, GUI.skin.label);
    this.SetSize((float) Mathf.Min(size.width + 20, Screen.width), (float) Mathf.Min(size.height + 50, Screen.height));
    this.isResize = true;
  }
}
