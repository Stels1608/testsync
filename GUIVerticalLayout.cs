﻿// Decompiled with JetBrains decompiler
// Type: GUIVerticalLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GUIVerticalLayout : GUIPanel
{
  private float gap;

  public float Gap
  {
    get
    {
      return this.gap;
    }
    set
    {
      this.gap = value;
      this.MakeValidRect();
    }
  }

  public GUIVerticalLayout()
    : this((SmartRect) null)
  {
  }

  public GUIVerticalLayout(SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
  }

  public void PushFront(GUIPanel panel)
  {
    this.children.Insert(0, panel);
    panel.Parent = this.root;
    this.MakeValidRect();
  }

  public override void AddPanel(GUIPanel panel)
  {
    base.AddPanel(panel);
    this.MakeValidRect();
  }

  public override void RemovePanel(GUIPanel panel)
  {
    base.RemovePanel(panel);
    this.MakeValidRect();
  }

  public void MakeValidRect()
  {
    float a = 0.0f;
    using (List<GUIPanel>.Enumerator enumerator = this.children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIPanel current = enumerator.Current;
        a = Mathf.Max(a, current.SmartRect.Width);
      }
    }
    float num = 0.0f;
    using (List<GUIPanel>.Enumerator enumerator = this.children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIPanel current = enumerator.Current;
        num += current.SmartRect.Height;
      }
    }
    if (this.children.Count <= 0)
      return;
    this.root.Width = a;
    this.root.Height = num + (float) (this.Children.Count - 1) * this.Gap;
    this.children[0].Parent = this.root;
    this.children[0].Position = new float2(this.children[0].Position.x, (float) -((double) this.root.Height / 2.0 - (double) this.children[0].SmartRect.Height / 2.0));
    float2 float2 = new float2(0.0f, this.children[0].SmartRect.Height);
    SmartRect smartRect = this.children[0].SmartRect;
    for (int index = 1; index < this.children.Count; ++index)
    {
      this.children[index].Parent = smartRect;
      this.children[index].Position = new float2(this.children[index].Position.x, (float) ((double) float2.y / 2.0 + (double) this.gap + (double) this.children[index].SmartRect.Height / 2.0));
      smartRect = this.children[index].SmartRect;
      float2 = new float2(0.0f, this.children[index].SmartRect.Height);
    }
  }
}
