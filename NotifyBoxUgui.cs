﻿// Decompiled with JetBrains decompiler
// Type: NotifyBoxUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using TMPro;
using UnityEngine;

public class NotifyBoxUgui : NotifyBoxUi
{
  [SerializeField]
  private TextMeshProUGUI headerText;
  [SerializeField]
  private TextMeshProUGUI okButtonText;
  [SerializeField]
  private TextMeshProUGUI messageText;
  [SerializeField]
  private TextMeshProUGUI timerText;

  protected override void SetTitleText(string text)
  {
    this.headerText.text = text;
  }

  protected override void SetOkButtonText(string text)
  {
    this.okButtonText.text = text;
  }

  protected override void SetMainText(string mainText)
  {
    this.messageText.text = mainText;
  }

  protected override void DestroyWindow()
  {
    Object.Destroy((Object) this.gameObject);
  }

  protected override void SetTimerText(string text)
  {
    this.timerText.text = text;
  }

  public override void ShowTimer(bool show)
  {
    this.timerText.enabled = show;
  }
}
