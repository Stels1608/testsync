﻿// Decompiled with JetBrains decompiler
// Type: FactionColoredNguiWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FactionColoredNguiWidget : FactionColoredWidget
{
  private void Awake()
  {
    this.Colorize();
  }

  public override void Colorize()
  {
    Component component1;
    if ((Object) (component1 = (Component) this.GetComponent<UIButton>()) != (Object) null)
    {
      this.ColorizeButton(component1 as UIButton);
    }
    else
    {
      Component component2;
      if (!((Object) (component2 = (Component) this.GetComponent<UI2DSprite>()) != (Object) null))
        return;
      this.ColorizeUI2DSprite(component2 as UI2DSprite);
    }
  }

  private void ColorizeButton(UIButton button)
  {
    button.defaultColor = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
    button.hover = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_CLICK_COLOR);
    button.pressed = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_CLICK_COLOR);
    button.disabledColor = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_DISABLED_COLOR);
  }

  private void ColorizeUI2DSprite(UI2DSprite sprite)
  {
    sprite.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
  }
}
