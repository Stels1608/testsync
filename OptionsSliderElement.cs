﻿// Decompiled with JetBrains decompiler
// Type: OptionsSliderElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class OptionsSliderElement : OptionsElement
{
  protected float MaxValue = 1f;
  public int Multiplier = 100;
  public int Steps = 100;
  [SerializeField]
  private SliderWidget slider;
  private OnSettingChanged settingsChanged;
  protected UserSetting setting;
  protected float MinValue;
  private float oldValue;
  public bool ShowPercentage;

  public override OnSettingChanged OnSettingChanged
  {
    get
    {
      return this.settingsChanged;
    }
    set
    {
      this.settingsChanged = value;
    }
  }

  public override void Start()
  {
    base.Start();
    this.ReloadLanguageData();
    this.slider.Slider.onDragFinished = new UIProgressBar.OnDragFinished(this.OnSliderValueChanged);
    EventDelegate.Add(this.slider.Slider.onChange, new EventDelegate.Callback(this.UpdateLabel));
  }

  public void SetLimits01(float minValue, float maxValue)
  {
    this.MinValue = minValue;
    this.MaxValue = maxValue;
  }

  public void OnSliderValueChanged()
  {
    float num = this.MinValue + (this.MaxValue - this.MinValue) * this.slider.Slider.value;
    if ((double) Math.Abs(this.slider.Slider.value - this.oldValue) <= 0.00999999977648258)
      return;
    this.settingsChanged(this.setting, (object) num);
    this.oldValue = this.slider.Slider.value;
  }

  private void UpdateLabel()
  {
    this.slider.maxLabel.text = Math.Round((double) (this.MinValue + (this.MaxValue - this.MinValue) * this.slider.Slider.value) * (double) this.Multiplier).ToString() + (!this.ShowPercentage ? string.Empty : "%");
  }

  public override void Init(UserSetting sett, object settingValue)
  {
    if (settingValue == null)
      Debug.LogError((object) "settingValue is null. Did you forget to define a default value in UserSettings.cs? ");
    float actualValue = (float) (((double) (float) settingValue - (double) this.MinValue) / ((double) this.MaxValue - (double) this.MinValue));
    this.slider.Init(actualValue, this.Steps);
    this.setting = sett;
    this.oldValue = actualValue;
  }

  public override void ReloadLanguageData()
  {
    this.optionDescLabel.text = Tools.GetLocalized(this.setting);
  }
}
