﻿// Decompiled with JetBrains decompiler
// Type: FlightSimGameProtocolEmulator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FlightSimGameProtocolEmulator : GameProtocol
{
  private MovementFrame movementFrame;
  private MovementOptions movementOptions;
  private Maneuver currentManeuver;

  public MovementFrame MovementFrame
  {
    get
    {
      return this.movementFrame;
    }
  }

  public MovementOptions MovementOptions
  {
    get
    {
      return this.movementOptions;
    }
  }

  public FlightSimGameProtocolEmulator()
  {
    this.movementFrame = new MovementFrame()
    {
      mode = 2
    };
    this.currentManeuver = (Maneuver) new RestManeuver();
    this.SetMovementOptions();
  }

  public void SetMovementOptions()
  {
    this.movementOptions = new MovementOptions();
    this.movementOptions.acceleration = 13f;
    this.movementOptions.strafeAcceleration = 145f;
    this.movementOptions.strafeMaxSpeed = 40f;
    this.movementOptions.inertiaCompensation = 85f;
    this.movementOptions.pitchMaxSpeed = 65f;
    this.movementOptions.yawMaxSpeed = 65f;
    this.movementOptions.rollMaxSpeed = 135f;
    this.movementOptions.pitchAcceleration = 120f;
    this.movementOptions.yawAcceleration = 120f;
    this.movementOptions.rollAcceleration = 120f;
    this.movementOptions.speed = 50f;
    this.MovementOptions.ApplyCard(new MovementCard(0U)
    {
      minYawSpeed = 0.1f,
      maxPitch = 360f,
      maxRoll = 80f,
      pitchFading = 2f,
      yawFading = 2f,
      rollFading = 400f
    });
  }

  public override void MoveToDirection(Euler3 direction)
  {
  }

  public override void MoveToDirectionWithoutRoll(Euler3 direction)
  {
  }

  public override void TurnToDirectionStrikes(Euler3 directionInput, float rollInput, Vector2 strafeDirection)
  {
    this.currentManeuver = (Maneuver) new TurnToDirectionStrikes(directionInput, rollInput, strafeDirection.x, strafeDirection.y, this.MovementOptions);
  }

  public override void TurnByPitchYawStrikes(Vector3 pitchYawFactor, Vector2 strafeDirection, float strafeMagnitude)
  {
    this.currentManeuver = (Maneuver) new TurnByPitchYawStrikes(pitchYawFactor, strafeDirection, strafeMagnitude, this.MovementOptions);
  }

  public void UpdateMovementFrame(Tick tick)
  {
    int num = Tick.Current - Tick.Last;
    for (int index = 0; index < num; ++index)
      this.movementFrame = this.currentManeuver.NextFrame(tick, this.movementFrame);
  }
}
