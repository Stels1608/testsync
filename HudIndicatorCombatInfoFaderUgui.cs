﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorCombatInfoFaderUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using TMPro;
using UnityEngine;

public class HudIndicatorCombatInfoFaderUgui
{
  private const float FADE_TIME = 1.5f;
  private const float Y_SPEED = 20f;
  private readonly TextMeshProUGUI parentLabel;
  private float time;

  public bool Expired
  {
    get
    {
      return (double) this.time <= 0.0;
    }
  }

  public TextMeshProUGUI ParentLabel
  {
    get
    {
      return this.parentLabel;
    }
  }

  public HudIndicatorCombatInfoFaderUgui(string displayString, TextMeshProUGUI parentLabel)
  {
    this.parentLabel = parentLabel;
    this.time = 1.5f;
    this.parentLabel.text = displayString;
  }

  public void Update()
  {
    Color color = this.ParentLabel.color;
    color.a = (double) this.time >= 0.5 ? 1f : (float) (((double) this.time + 0.5) / 1.5);
    this.ParentLabel.color = color;
    this.ParentLabel.rectTransform.anchoredPosition = this.ParentLabel.rectTransform.anchoredPosition + Vector2.up * Time.deltaTime * 20f;
    this.time -= Time.deltaTime;
  }
}
