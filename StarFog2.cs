﻿// Decompiled with JetBrains decompiler
// Type: StarFog2
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof (Camera))]
public class StarFog2 : MonoBehaviour
{
  private bool cloudsVisible = true;
  public Material[] Materials;
  public int CloudCount;
  public float MinCloudSize;
  public float MaxCloudSize;
  public float Radius;
  public Color Color;
  private Transform _transform;
  private Camera _camera;
  private GameObject root;
  private StarFog2.Cloud[] clouds;
  private float sqrRadius;
  private float halfRadius;
  private float frontDist;
  private Vector3 lastPosition;

  public void SetCloudsVisible(bool flag)
  {
    this.cloudsVisible = flag;
    if (this.clouds == null)
      return;
    for (int index = 0; index < this.clouds.Length; ++index)
      this.clouds[index].renderer.enabled = flag;
  }

  public void Recreate()
  {
    if ((UnityEngine.Object) this.root != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.root);
      this.clouds = (StarFog2.Cloud[]) null;
    }
    this._transform = this.transform;
    this.lastPosition = this._transform.position;
    this.sqrRadius = this.Radius * this.Radius;
    this.halfRadius = 0.5f * this.Radius;
    this.frontDist = 0.86f * this.Radius;
    this.GenerateClouds();
  }

  private void Awake()
  {
    this._camera = this.GetComponent<Camera>();
  }

  private void Start()
  {
    this.Recreate();
  }

  private void GenerateClouds()
  {
    Mesh[] meshArray = new Mesh[3]
    {
      this.MakeMesh(this.MinCloudSize),
      this.MakeMesh((float) (((double) this.MaxCloudSize - (double) this.MinCloudSize) * 0.5)),
      this.MakeMesh(this.MaxCloudSize)
    };
    GameObject[] gameObjectArray = new GameObject[meshArray.Length * this.Materials.Length];
    for (int index1 = 0; index1 < meshArray.Length; ++index1)
    {
      for (int index2 = 0; index2 < this.Materials.Length; ++index2)
      {
        GameObject gameObject = new GameObject(this.Materials[index2].name);
        gameObject.AddComponent<MeshFilter>().mesh = meshArray[index1];
        gameObject.AddComponent<MeshRenderer>().material = this.Materials[index2];
        gameObject.GetComponent<Renderer>().shadowCastingMode = ShadowCastingMode.Off;
        gameObject.GetComponent<Renderer>().receiveShadows = false;
        gameObjectArray[index1 * this.Materials.Length + index2] = gameObject;
      }
    }
    this.root = new GameObject("Clouds");
    this.clouds = new StarFog2.Cloud[this.CloudCount];
    for (int index = 0; index < this.clouds.Length; ++index)
    {
      GameObject original = gameObjectArray[index % gameObjectArray.Length];
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(original);
      gameObject.name = original.name;
      gameObject.transform.parent = this.root.transform;
      this.clouds[index] = new StarFog2.Cloud(gameObject)
      {
        StarFog = this,
        transform = {
          position = this._transform.position + UnityEngine.Random.insideUnitSphere * this.Radius
        },
        MaxDistance = this.halfRadius * this.halfRadius
      };
      this.clouds[index].renderer.enabled = this.cloudsVisible;
    }
    foreach (UnityEngine.Object @object in gameObjectArray)
      UnityEngine.Object.Destroy(@object);
  }

  private Mesh MakeMesh(float size)
  {
    float num = 0.5f * size;
    Mesh mesh = new Mesh();
    mesh.vertices = new Vector3[4]
    {
      new Vector3(num, num),
      new Vector3(-num, num),
      new Vector3(-num, -num),
      new Vector3(num, -num)
    };
    mesh.triangles = new int[6]{ 0, 1, 2, 2, 3, 0 };
    mesh.RecalculateNormals();
    mesh.uv = new Vector2[4]
    {
      new Vector2(0.0f, 1f),
      new Vector2(1f, 1f),
      new Vector2(1f, 0.0f),
      new Vector2(0.0f, 0.0f)
    };
    return mesh;
  }

  private void LateUpdate()
  {
    bool enabled = this._camera.enabled;
    this.root.SetActive(enabled);
    if (!enabled)
      return;
    Vector3 position = this._transform.position;
    Vector3 forward = position - this.lastPosition;
    if ((double) forward.sqrMagnitude > 1.0 / 1000.0)
    {
      Quaternion quaternion = Quaternion.LookRotation(forward);
      foreach (StarFog2.Cloud cloud in this.clouds)
        cloud.UpdateDistance(position);
      foreach (StarFog2.Cloud cloud in this.clouds)
      {
        if ((double) cloud.Distance > (double) this.sqrRadius)
        {
          Vector3 vector3 = (Vector3) (UnityEngine.Random.insideUnitCircle * this.halfRadius);
          vector3.z = UnityEngine.Random.Range(this.halfRadius, this.frontDist);
          cloud.transform.position = position + quaternion * vector3;
        }
      }
    }
    Quaternion rotation = this._transform.rotation;
    foreach (StarFog2.Cloud cloud in this.clouds)
      cloud.transform.rotation = rotation;
    this.lastPosition = position;
  }

  private void OnDestroy()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.root);
  }

  [Serializable]
  private class Cloud
  {
    public Transform transform;
    public StarFog2 StarFog;
    public float MaxDistance;
    public Renderer renderer;
    private Material material;
    private float distance;

    public float Distance
    {
      get
      {
        return this.distance;
      }
    }

    public Cloud(GameObject gameObject)
    {
      this.renderer = gameObject.GetComponent<Renderer>();
      this.material = gameObject.GetComponent<Renderer>().material;
      this.transform = gameObject.transform;
    }

    public void UpdateDistance(Vector3 center)
    {
      this.distance = (this.transform.position - center).sqrMagnitude;
      this.UpdateColor();
    }

    private void UpdateColor()
    {
      float num = (float) (1.0 - (double) this.distance / (double) this.MaxDistance);
      Color color = this.StarFog.Color;
      color.a *= num;
      this.material.SetColor("_TintColor", color);
    }
  }
}
