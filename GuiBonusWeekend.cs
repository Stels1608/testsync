﻿// Decompiled with JetBrains decompiler
// Type: GuiBonusWeekend
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

internal class GuiBonusWeekend : GuiDialogBonusWeekend
{
  private const string FILE_DIRECTORY = "GUI/Events/DoubleXPWeekend/";
  private const string DEFAULT_BANNER = "DoubleXPweekend_BG";
  private static bool isShowing;
  public static bool showSpecialOfferOnClose;

  private GuiBonusWeekend(GUICard card)
  {
    GuiBonusWeekend.isShowing = true;
    bool flag = false;
    Texture2D bgImage = ResourceLoader.Load<Texture2D>(card.GUITexturePath);
    if ((Object) bgImage == (Object) null)
    {
      bgImage = ResourceLoader.Load<Texture2D>("GUI/Events/DoubleXPWeekend/DoubleXPweekend_BG");
      flag = true;
    }
    this.ChangeBackgroundImage(bgImage);
    this.UpdateSize();
    this.closeButton.PositionX -= 17f;
    this.closeButton.PositionY += 11f;
    this.closeButton.Pressed = new AnonymousDelegate(this.Hide);
    GuiLabel guiLabel1 = new GuiLabel(card.Name);
    GuiLabel guiLabel2 = new GuiLabel(card.Description);
    GuiButton guiButton1 = new GuiButton("%$bgo.common.get_cubits_now%", "GUI/Events/DoubleXPWeekend/Btn_Buy_Cubits_Idle", "GUI/Events/DoubleXPWeekend/Btn_Buy_Cubits_Over", "GUI/Events/DoubleXPWeekend/Btn_Buy_Cubits_Click");
    Texture2D normal = ResourceLoader.Load<Texture2D>(card.GUITexturePath + "_Btn_Close_Idle");
    Texture2D over = ResourceLoader.Load<Texture2D>(card.GUITexturePath + "_Btn_Close_Over");
    Texture2D pressed = ResourceLoader.Load<Texture2D>(card.GUITexturePath + "_Btn_Close_Click");
    if (flag || (Object) normal == (Object) null || ((Object) over == (Object) null || (Object) pressed == (Object) null))
    {
      normal = ResourceLoader.Load<Texture2D>("GUI/Events/DoubleXPWeekend/DoubleXPweekend_BG_Btn_Close_Idle");
      over = ResourceLoader.Load<Texture2D>("GUI/Events/DoubleXPWeekend/DoubleXPweekend_BG_Btn_Close_Over");
      pressed = ResourceLoader.Load<Texture2D>("GUI/Events/DoubleXPWeekend/DoubleXPweekend_BG_Btn_Close_Click");
    }
    GuiButton guiButton2 = new GuiButton("%$bgo.common.close%", normal, over, pressed);
    guiLabel1.Font = Gui.Options.FontBGM_BT;
    guiLabel1.FontSize = 24;
    guiLabel2.Font = Gui.Options.FontBGM_BT;
    guiLabel2.FontSize = 14;
    guiLabel2.NormalColor = new Color(0.89f, 0.87f, 0.87f);
    guiLabel2.WordWrap = true;
    guiLabel2.SizeX = 608f;
    guiButton1.Font = Gui.Options.FontBGM_BT;
    guiButton1.FontSize = 30;
    guiButton1.Size = new Vector2(384f, 116f);
    guiButton1.Pressed = new AnonymousDelegate(ShopWindow.OnGetCubits);
    guiButton1.LabelOffset = new Vector2(0.0f, 2f);
    guiButton2.Font = Gui.Options.FontBGM_BT;
    guiButton2.FontSize = 20;
    guiButton2.Size = new Vector2(192f, 64f);
    guiButton2.Pressed = new AnonymousDelegate(this.Hide);
    guiButton2.LabelOffset = new Vector2(0.0f, 2f);
    this.AddChild((GuiElementBase) guiLabel1, Align.MiddleCenter, new Vector2(0.0f, -220f));
    this.AddChild((GuiElementBase) guiLabel2, Align.MiddleLeft, new Vector2(80f, 40f));
    this.AddChild((GuiElementBase) guiButton1, Align.MiddleCenter, new Vector2(-110f, 180f));
    this.AddChild((GuiElementBase) guiButton2, Align.MiddleCenter, new Vector2(200f, 180f));
    this.IsRendered = true;
  }

  public static bool IsShowing()
  {
    return GuiBonusWeekend.isShowing;
  }

  public override void Update()
  {
    base.Update();
    this.PositionCenter = new Vector2((float) Screen.width / 2f, (float) Screen.height / 2f);
  }

  public static void Show(GUICard card)
  {
    Game.RegisterDialog((IGUIPanel) new GuiBonusWeekend(card), true);
  }

  public void Hide()
  {
    Game.UnregisterDialog((IGUIPanel) this);
    GuiBonusWeekend.isShowing = false;
    if (!GuiBonusWeekend.showSpecialOfferOnClose)
      return;
    Game.DelayedActions.Add(new DelayedActions.Predicate(Game.SpecialOfferManager.Check));
  }
}
