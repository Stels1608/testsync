﻿// Decompiled with JetBrains decompiler
// Type: UITableWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class UITableWidget : UIWidget
{
  private UITableWidgetLayout? layout = new UITableWidgetLayout?(UITableWidgetLayout.Horizontal);
  private int cellsX;
  private int cellsY;
  private float cellSizeX;
  private float cellSizeY;
  private UIOrigin tableOrigin;

  public new void Start()
  {
    base.Start();
    this.Reposition();
  }

  public void SetLayout(int columns, int rows, Vector2 cellSize, UIOrigin tableOrigin)
  {
    this.cellsX = columns;
    this.cellsY = rows;
    this.cellSizeX = cellSize.x;
    this.cellSizeY = cellSize.y;
    this.tableOrigin = tableOrigin;
    this.layout = new UITableWidgetLayout?();
    this.Reposition();
  }

  public void SetLayout(UITableWidgetLayout layout, Vector2 cellSize, UIOrigin tableOrigin)
  {
    this.cellSizeX = cellSize.x;
    this.cellSizeY = cellSize.y;
    this.tableOrigin = tableOrigin;
    this.layout = new UITableWidgetLayout?(layout);
    this.Reposition();
  }

  public void Reposition()
  {
    List<UIWidget> uiWidgetList = new List<UIWidget>();
    foreach (Component component1 in this.transform)
    {
      UIWidget component2;
      if ((Object) (component2 = component1.gameObject.GetComponent<UIWidget>()) != (Object) null)
        uiWidgetList.Add(component2);
    }
    if (uiWidgetList.Count == 0)
      return;
    UITableWidgetLayout? nullable = this.layout;
    if (nullable.HasValue)
    {
      switch (nullable.Value)
      {
        case UITableWidgetLayout.None:
          this.cellsY = 1;
          this.cellsX = uiWidgetList.Count;
          this.cellSizeX = (float) uiWidgetList[0].width;
          this.cellSizeY = (float) uiWidgetList[0].height;
          break;
        case UITableWidgetLayout.Horizontal:
          this.cellsY = 1;
          this.cellsX = uiWidgetList.Count;
          break;
        case UITableWidgetLayout.Vertical:
          this.cellsY = uiWidgetList.Count;
          this.cellsX = 1;
          break;
      }
    }
    this.width = Mathf.RoundToInt((float) this.cellsX * this.cellSizeX);
    this.height = Mathf.RoundToInt((float) this.cellsY * this.cellSizeY);
    Vector3 vector3 = this.tableOrigin != UIOrigin.Centered ? Vector3.zero : new Vector3((float) -this.width / 2f, (float) this.height / 2f);
    int index1 = 0;
    for (int index2 = 0; index2 < this.cellsY; ++index2)
    {
      for (int index3 = 0; index3 < this.cellsX && uiWidgetList.Count > index1; ++index3)
      {
        UIWidget uiWidget = uiWidgetList[index1];
        Vector2i vector2i1 = new Vector2i((float) (((double) this.cellSizeX - (double) uiWidget.width) / 2.0), (float) (((double) this.cellSizeY - (double) uiWidget.height) / 2.0));
        Vector2i vector2i2 = new Vector2i((float) index3 * this.cellSizeX + (float) vector2i1.x, (float) -index2 * this.cellSizeY - (float) vector2i1.y);
        UIWidget.Pivot pivot = this.pivot;
        uiWidgetList[index1].pivot = UIWidget.Pivot.TopLeft;
        uiWidgetList[index1].transform.localPosition = new Vector3((float) vector2i2.x, (float) vector2i2.y, 0.0f) + vector3;
        uiWidgetList[index1].pivot = pivot;
        ++index1;
      }
    }
  }
}
