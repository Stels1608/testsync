﻿// Decompiled with JetBrains decompiler
// Type: CharacterMotor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class CharacterMotor : MonoBehaviour
{
  public float maxForwardSpeed = 1.5f;
  public float maxBackwardsSpeed = 1.5f;
  public float maxSidewaysSpeed = 1.5f;
  public float maxVelocityChange = 0.2f;
  public float gravity = 10f;
  public bool canJump = true;
  public float jumpHeight = 1f;
  public Vector3 forwardVector = Vector3.forward;
  protected Quaternion alignCorrection;
  private bool m_Grounded;
  private bool m_Jumping;
  private Vector3 m_desiredMovementDirection;
  private Vector3 m_desiredFacingDirection;

  public bool grounded
  {
    get
    {
      return this.m_Grounded;
    }
    protected set
    {
      this.m_Grounded = value;
    }
  }

  public bool jumping
  {
    get
    {
      return this.m_Jumping;
    }
    protected set
    {
      this.m_Jumping = value;
    }
  }

  public Vector3 desiredMovementDirection
  {
    get
    {
      return this.m_desiredMovementDirection;
    }
    set
    {
      this.m_desiredMovementDirection = value;
      if ((double) this.m_desiredMovementDirection.magnitude <= 1.0)
        return;
      this.m_desiredMovementDirection = this.m_desiredMovementDirection.normalized;
    }
  }

  public Vector3 desiredFacingDirection
  {
    get
    {
      return this.m_desiredFacingDirection;
    }
    set
    {
      this.m_desiredFacingDirection = value;
      if ((double) this.m_desiredFacingDirection.magnitude <= 1.0)
        return;
      this.m_desiredFacingDirection = this.m_desiredFacingDirection.normalized;
    }
  }

  public Vector3 desiredVelocity
  {
    get
    {
      if (this.m_desiredMovementDirection == Vector3.zero)
        return Vector3.zero;
      float num = ((double) this.m_desiredMovementDirection.z <= 0.0 ? this.maxBackwardsSpeed : this.maxForwardSpeed) / this.maxSidewaysSpeed;
      Vector3 normalized = new Vector3(this.m_desiredMovementDirection.x, 0.0f, this.m_desiredMovementDirection.z / num).normalized;
      return this.transform.rotation * this.m_desiredMovementDirection * (new Vector3(normalized.x, 0.0f, normalized.z * num).magnitude * this.maxSidewaysSpeed);
    }
  }

  private void Start()
  {
    this.alignCorrection = new Quaternion();
    this.alignCorrection.SetLookRotation(this.forwardVector, Vector3.up);
    this.alignCorrection = Quaternion.Inverse(this.alignCorrection);
  }
}
