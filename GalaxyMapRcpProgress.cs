﻿// Decompiled with JetBrains decompiler
// Type: GalaxyMapRcpProgress
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GalaxyMapRcpProgress : GuiPanel
{
  private readonly GuiHealthbar progress = new GuiHealthbar();
  private readonly GuiLabel rcpProgress = new GuiLabel(Gui.Options.FontBGM_BT, 12);
  private readonly GuiLabel capitalShipScale = new GuiLabel(Gui.Options.FontBGM_BT, 12);
  private readonly GuiLabel colonialRCPoints = new GuiLabel();
  private readonly GuiLabel cylonRCPoints = new GuiLabel();
  private readonly GuiLabel scalingBonus = new GuiLabel();

  public GalaxyMapRcpProgress()
  {
    this.Size = new Vector2(538f, 40f);
    this.progress.Color = Tools.Color(110, 152, 190);
    this.progress.ColorInner = Tools.Color(176, 24, 36);
    this.progress.ColorBorder = Color.black;
    this.progress.FreeSpaceWidth = 0.0f;
    Color color = Tools.Color(173, 173, 173);
    GuiLabel guiLabel1 = new GuiLabel("%$bgo.common.colonial_u%", color, color, Gui.Options.FontBGM_BT);
    guiLabel1.FontSize = 12;
    GuiLabel guiLabel2 = new GuiLabel("%$bgo.common.cylon_u%", color, color, Gui.Options.FontBGM_BT);
    guiLabel2.FontSize = 12;
    this.rcpProgress.Alignment = TextAnchor.MiddleCenter;
    this.scalingBonus.Alignment = TextAnchor.MiddleCenter;
    this.capitalShipScale.Alignment = TextAnchor.MiddleCenter;
    this.AddChild((GuiElementBase) this.progress, Align.DownLeft, new Vector2(78f, 0.0f));
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.galaxy_map.sector_control%", Gui.Options.FontBGM_BT, 15), Align.UpCenter, new Vector2(0.0f, -4f));
    this.AddChild((GuiElementBase) guiLabel1, Align.DownLeft, new Vector2(guiLabel1.SizeX + 30f, -5f));
    this.AddChild((GuiElementBase) guiLabel2, Align.DownRight, new Vector2((float) (-(double) guiLabel2.SizeX - 30.0), -5f));
    this.AddChild((GuiElementBase) this.rcpProgress, Align.MiddleCenter, new Vector2(330f, -10f));
    this.AddChild((GuiElementBase) this.scalingBonus, Align.MiddleCenter, new Vector2(330f, 8f));
    this.AddChild((GuiElementBase) this.capitalShipScale, Align.MiddleLeft, new Vector2(486f, 20f));
    this.AddChild((GuiElementBase) this.colonialRCPoints, new Vector2(85f, 20f));
    this.AddChild((GuiElementBase) this.cylonRCPoints, Align.UpRight, new Vector2(-60f, 20f));
    this.progress.Size = new Vector2(405f, 25f);
    this.rcpProgress.AutoSize = false;
    this.rcpProgress.SizeX = 200f;
    this.SetTooltip("%$bgo.galaxy_map.sector_control_tooltip%");
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    float num = Game.Galaxy.ColonialRCPoints + Game.Galaxy.CylonRCPoints;
    this.colonialRCPoints.Text = Game.Galaxy.ColonialRCPoints.ToString();
    this.cylonRCPoints.Text = Game.Galaxy.CylonRCPoints.ToString();
    this.progress.Progress = (double) num != 0.0 ? Mathf.Clamp01(Game.Galaxy.ColonialRCPoints / num) : 0.5f;
    this.rcpProgress.Text = BsgoLocalization.Get(Game.Me.Faction != Faction.Colonial ? "%$bgo.galaxy_map.rcp_bonus_cylon%" : "%$bgo.galaxy_map.rcp_bonus_colonial%", (object) ("+" + (object) this.GetBonus(Game.Me.Faction != Faction.Colonial ? Game.Galaxy.CylonRCPoints : Game.Galaxy.ColonialRCPoints)));
    float remainingCapitalTime = Game.Galaxy.GetRemainingCapitalTime(Game.Me.Faction);
    if ((double) remainingCapitalTime > 0.0)
    {
      string str = Tools.FormatTime(remainingCapitalTime);
      if (Game.Me.Faction == Faction.Cylon)
        this.capitalShipScale.Text = BsgoLocalization.Get("bgo.galaxy_map.basestar_remaining_time", (object) str);
      else if (Game.Me.Faction == Faction.Colonial)
        this.capitalShipScale.Text = BsgoLocalization.Get("bgo.galaxy_map.pegasus_remaining_time", (object) str);
      else
        Debug.LogError((object) ("[GalaxyMapRcpProgress] Player is of unknown faction: " + (object) Game.Me.Faction));
    }
    else
      this.capitalShipScale.Text = Game.Me.Faction != Faction.Cylon ? (Game.Me.Faction != Faction.Colonial ? string.Empty : BsgoLocalization.Get("bgo.galaxy_map.pegasus_merit_cost") + " " + (object) Game.Galaxy.CapitalShipCost) : BsgoLocalization.Get("bgo.galaxy_map.basestar_merit_cost") + " " + (object) Game.Galaxy.CapitalShipCost;
    int scalingMultiplier = Game.Galaxy.GetScalingMultiplier();
    this.scalingBonus.Text = BsgoLocalization.Get(Game.Me.Faction != Faction.Colonial ? "%$bgo.galaxy_map.scaling_bonus_cylon%" : "%$bgo.galaxy_map.scaling_bonus_colonial%", (object) ((scalingMultiplier < 0 ? (object) string.Empty : (object) "+").ToString() + (object) scalingMultiplier));
  }

  [Gui2Editor]
  private int GetBonus(float bonus)
  {
    int[] numArray = StaticCards.GalaxyMap.Tiers;
    if ((double) bonus < (double) numArray[0])
      return 0;
    for (int index = 1; index < numArray.Length; ++index)
    {
      if ((double) numArray[index - 1] <= (double) bonus && (double) bonus < (double) numArray[index])
        return 5 * index;
    }
    return 5 * numArray.Length;
  }
}
