﻿// Decompiled with JetBrains decompiler
// Type: OutpostAttackedNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class OutpostAttackedNotification : OnScreenNotification
{
  private readonly NotificationCategory category;
  private readonly string message;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.OutpostAttacked;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return this.category;
    }
  }

  public override string TextMessage
  {
    get
    {
      return this.message;
    }
  }

  public override bool Show
  {
    get
    {
      if (OnScreenNotification.ShowOutpostMessages)
        return !string.IsNullOrEmpty(this.message);
      return false;
    }
  }

  public OutpostAttackedNotification(Faction faction, GUICard sectorCard, byte damageType)
  {
    this.category = faction == Game.Me.Faction ? NotificationCategory.Negative : NotificationCategory.Positive;
    switch (damageType)
    {
      case 2:
        this.message = BsgoLocalization.Get(faction != Faction.Cylon ? "%$bgo.notif.outpost_attacked_colonial%" : "%$bgo.notif.outpost_attacked_cylon%", (object) sectorCard.Name);
        break;
      case 3:
        this.message = BsgoLocalization.Get(faction != Faction.Cylon ? "%$bgo.notif.outpost_attacked_1_colonial%" : "%$bgo.notif.outpost_attacked_1_cylon%", (object) sectorCard.Name);
        break;
      case 4:
        this.message = BsgoLocalization.Get(faction != Faction.Cylon ? "%$bgo.notif.outpost_attacked_2_colonial%" : "%$bgo.notif.outpost_attacked_2_cylon%", (object) sectorCard.Name);
        break;
    }
    FacadeFactory.GetInstance().SendMessage(this.category != NotificationCategory.Positive ? Message.CombatLogOuch : Message.CombatLogNice, (object) this.message);
  }
}
