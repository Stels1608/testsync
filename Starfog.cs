﻿// Decompiled with JetBrains decompiler
// Type: Starfog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Starfog : MonoBehaviour
{
  public bool emit = true;
  public Color color = new Color(1f, 1f, 1f, 1f);
  public float minWidth = 50f;
  public float maxWidth = 100f;
  public float fogRadius = 100f;
  private Vector3 lastCameraPosition = new Vector3(0.0f, 0.0f, 0.0f);
  private Vector3 cameraPosition = new Vector3(0.0f, 0.0f, 0.0f);
  private int maxPoints = -1;
  public Material material;
  private Material instanceMaterial;
  private bool emittingDone;
  private GameObject fogObj;
  private Mesh mesh;
  private Starfog.Point[] points;
  private Vector3[] vertices;
  private Color[] colors;
  private Vector2[] uvs;
  private int[] triangles;
  private int pointCnt;

  private bool IsFull
  {
    get
    {
      return this.pointCnt >= this.maxPoints;
    }
  }

  private void Start()
  {
    if (this.maxPoints != -1)
      return;
    this.enabled = false;
  }

  public void Initialize(int maxDensity)
  {
    this.maxPoints = maxDensity;
    this.points = new Starfog.Point[this.maxPoints];
    this.vertices = new Vector3[this.maxPoints * 4];
    this.colors = new Color[this.maxPoints * 4];
    this.uvs = new Vector2[this.maxPoints * 4];
    this.triangles = new int[this.maxPoints * 6];
    this.pointCnt = 0;
    if ((Object) this.fogObj == (Object) null)
    {
      this.fogObj = new GameObject("Starfog");
      this.fogObj.transform.parent = (Transform) null;
      this.fogObj.transform.position = Vector3.zero;
      this.fogObj.transform.rotation = Quaternion.identity;
      this.fogObj.transform.localScale = Vector3.one;
      this.mesh = ((MeshFilter) this.fogObj.AddComponent(typeof (MeshFilter))).mesh;
      this.fogObj.AddComponent(typeof (MeshRenderer));
      this.instanceMaterial = new Material(this.material);
      this.fogObj.GetComponent<Renderer>().material = this.instanceMaterial;
    }
    this.cameraPosition = this.transform.position;
    this.lastCameraPosition = this.cameraPosition;
    this.GeneratePoints(true);
    this.enabled = true;
  }

  public int GetMaxPoints()
  {
    return this.maxPoints;
  }

  private void GeneratePoints(bool wholeSphere)
  {
    if (wholeSphere)
    {
      while (!this.IsFull)
      {
        Vector3 vector3 = new Vector3(0.0f, 0.0f, 0.0f);
        Quaternion identity = Quaternion.identity;
        vector3.x = Random.Range(0.0f, 2f) - 1f;
        vector3.y = Random.Range(0.0f, 2f) - 1f;
        vector3.z = Random.Range(0.0f, 2f) - 1f;
        vector3.Normalize();
        vector3 *= Random.Range(this.fogRadius / 5f, this.fogRadius);
        float w = Random.Range(this.minWidth, this.maxWidth);
        this.InsertPoint(this.cameraPosition + vector3, identity, w);
      }
    }
    else
    {
      while (!this.IsFull)
      {
        Vector3 vector3 = new Vector3(0.0f, 0.0f, 0.0f);
        Quaternion identity = Quaternion.identity;
        vector3.x = Random.Range(0.0f, 2f) - 1f;
        vector3.y = Random.Range(0.0f, 2f) - 1f;
        vector3.z = Random.Range(0.0f, 2f) - 1f;
        vector3.Normalize();
        vector3 = (this.cameraPosition - this.lastCameraPosition).normalized + vector3 * 0.5f;
        vector3 = vector3.normalized * Random.Range(this.fogRadius / 2f, this.fogRadius);
        float w = Random.Range(this.minWidth, this.maxWidth);
        this.InsertPoint(this.cameraPosition + vector3, identity, w);
      }
    }
  }

  private void Update()
  {
    if (!this.emit)
      this.emittingDone = true;
    if (this.emittingDone)
      this.emit = false;
    this.cameraPosition = this.transform.position;
    for (int index = this.pointCnt - 1; index >= 0; --index)
    {
      Starfog.Point point = this.points[index];
      point.Update();
      if (point.state == Starfog.Point.State.Alive && (double) Vector3.Distance(point.position, this.cameraPosition) > (double) this.fogRadius)
        point.state = Starfog.Point.State.Disapearing;
      else if (point.state == Starfog.Point.State.Dead)
      {
        this.points[index] = (Starfog.Point) null;
        --this.pointCnt;
      }
    }
    if (!this.IsFull)
      this.GeneratePoints(false);
    this.fogObj.GetComponent<Renderer>().enabled = true;
    this.BuildMesh();
    this.lastCameraPosition = this.cameraPosition;
  }

  private void BuildMesh()
  {
    for (int index1 = 0; index1 < this.pointCnt; ++index1)
    {
      Starfog.Point point = this.points[index1];
      int index2 = index1 * 4;
      Matrix4x4 matrix4x4 = Matrix4x4.TRS(point.position, this.transform.rotation, Vector3.one);
      float num1 = point.width;
      this.vertices[index2] = matrix4x4.MultiplyPoint3x4(new Vector3((float) (-(double) num1 * 0.5), num1 * 0.5f, 0.0f));
      this.vertices[index2 + 1] = matrix4x4.MultiplyPoint3x4(new Vector3(num1 * 0.5f, num1 * 0.5f, 0.0f));
      this.vertices[index2 + 2] = matrix4x4.MultiplyPoint3x4(new Vector3(num1 * 0.5f, (float) (-(double) num1 * 0.5), 0.0f));
      this.vertices[index2 + 3] = matrix4x4.MultiplyPoint3x4(new Vector3((float) (-(double) num1 * 0.5), (float) (-(double) num1 * 0.5), 0.0f));
      int num2 = point.frame / 4;
      float x = (float) (point.frame - num2 * 4) * 0.25f;
      float num3 = (float) (num2 + 1) * 0.25f;
      this.uvs[index2] = new Vector2(x, 1f - num3);
      this.uvs[index2 + 1] = new Vector2(x + 0.25f, 1f - num3);
      this.uvs[index2 + 2] = new Vector2(x + 0.25f, (float) (1.0 - ((double) num3 + 0.25)));
      this.uvs[index2 + 3] = new Vector2(x, (float) (1.0 - ((double) num3 + 0.25)));
      Color color = new Color(this.color.r, this.color.g, this.color.b, this.color.a * point.alpha);
      this.colors[index2] = color;
      this.colors[index2 + 1] = color;
      this.colors[index2 + 2] = color;
      this.colors[index2 + 3] = color;
      int index3 = index1 * 6;
      this.triangles[index3] = index2;
      this.triangles[index3 + 1] = index2 + 1;
      this.triangles[index3 + 2] = index2 + 2;
      this.triangles[index3 + 3] = index2 + 2;
      this.triangles[index3 + 4] = index2 + 3;
      this.triangles[index3 + 5] = index2;
    }
    this.mesh.Clear();
    this.mesh.vertices = this.vertices;
    this.mesh.colors = this.colors;
    this.mesh.uv = this.uvs;
    this.mesh.triangles = this.triangles;
  }

  private int InsertPoint(Vector3 p, Quaternion r, float w)
  {
    if (this.IsFull)
      return -1;
    for (int index = 0; index < this.maxPoints; ++index)
    {
      if (this.points[index] == null)
      {
        this.points[index] = new Starfog.Point(p, r, w);
        ++this.pointCnt;
        return index;
      }
    }
    return -1;
  }

  private class Point
  {
    public Vector3 position = Vector3.zero;
    public Quaternion rotation = Quaternion.identity;
    public int frame;
    public Starfog.Point.State state;
    public float alpha;
    public float width;

    public Point(Vector3 p, Quaternion r, float w)
    {
      this.position = p;
      this.rotation = r;
      this.width = w;
      this.state = Starfog.Point.State.Appearing;
      this.alpha = 0.0f;
      this.frame = Random.Range(0, 7);
    }

    public void Update()
    {
      switch (this.state)
      {
        case Starfog.Point.State.Appearing:
          this.alpha += Time.deltaTime * 10f;
          if ((double) this.alpha < 1.0)
            break;
          this.alpha = 1f;
          this.state = Starfog.Point.State.Alive;
          break;
        case Starfog.Point.State.Disapearing:
          this.alpha -= Time.deltaTime * 10f;
          if ((double) this.alpha > 0.0)
            break;
          this.alpha = 0.0f;
          this.state = Starfog.Point.State.Dead;
          break;
      }
    }

    public enum State
    {
      Appearing,
      Alive,
      Disapearing,
      Dead,
    }
  }
}
