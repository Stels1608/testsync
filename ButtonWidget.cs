﻿// Decompiled with JetBrains decompiler
// Type: ButtonWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class ButtonWidget : NguiWidget
{
  [SerializeField]
  protected UILabel textLabel;
  [SerializeField]
  protected UISprite bgSprite;
  [SerializeField]
  protected UISprite iconSprite;
  public AnonymousDelegate handleClick;

  public UILabel TextLabel
  {
    get
    {
      return this.textLabel;
    }
    set
    {
      this.textLabel = value;
    }
  }

  public UISprite BgSprite
  {
    get
    {
      return this.bgSprite;
    }
    set
    {
      this.bgSprite = value;
    }
  }

  public override bool IsEnabled
  {
    set
    {
      base.IsEnabled = value;
      this.OnHover(false);
      if ((Object) this.bgSprite != (Object) null)
        this.bgSprite.color = ColorManager.currentColorScheme.Get(!value ? WidgetColorType.FACTION_DISABLED_COLOR : WidgetColorType.FACTION_COLOR);
      if (value)
        return;
      if ((Object) this.Icon != (Object) null)
      {
        this.Icon.color = Color.grey;
      }
      else
      {
        if (!((Object) this.textLabel != (Object) null))
          return;
        this.textLabel.color = Color.grey;
      }
    }
  }

  public UISprite Icon
  {
    get
    {
      if ((Object) this.iconSprite != (Object) null)
        return this.iconSprite;
      UISprite component = this.GetComponent<UISprite>();
      if ((Object) component != (Object) this.bgSprite)
        this.iconSprite = component;
      return this.iconSprite;
    }
  }

  public override void Start()
  {
    base.Start();
    this.effectGroupName = string.Empty;
    this.OnHover(false);
  }

  public override void OnClick()
  {
    this.OnHover(false);
    if (this.IsEnabled && this.handleClick != null)
      this.handleClick();
    base.OnClick();
  }

  public override void OnHover(bool isOver)
  {
    if (!this.IsEnabled)
      return;
    if ((Object) this.Icon != (Object) null)
      this.Icon.color = !isOver ? Color.white : Gui.Options.MouseOverInvertColor;
    else if ((Object) this.textLabel != (Object) null)
      this.textLabel.color = !isOver ? Color.white : Gui.Options.MouseOverInvertColor;
    if (!((Object) this.bgSprite != (Object) null))
      return;
    this.bgSprite.color = !isOver ? ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR) : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_CLICK_COLOR);
    string name = !isOver ? this.bgSprite.spriteName.Replace("_hover", "_normal") : this.bgSprite.spriteName.Replace("_normal", "_hover");
    if (this.bgSprite.atlas.GetSprite(name) == null)
      return;
    this.bgSprite.spriteName = name;
  }
}
