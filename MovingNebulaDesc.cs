﻿// Decompiled with JetBrains decompiler
// Type: MovingNebulaDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MovingNebulaDesc : Desc, IProtocolRead
{
  [JsonField(JsonName = "modelName")]
  public string modelName = "movingnebula";
  [JsonField(JsonName = "matSuffix")]
  public string matSuffix = "1";
  [JsonField(JsonName = "color")]
  public Color color = Color.white;
  [JsonField(JsonName = "textureOffset")]
  public Vector2 textureOffset = new Vector2(0.0f, 0.0f);
  [JsonField(JsonName = "textureScale")]
  public Vector2 textureScale = new Vector2(1f, 1f);
  public const int VERSIONS_COUNT = 4;
  public const int BG_LAYER = 8;

  public MovingNebulaDesc()
    : base("MovingNebula")
  {
  }

  public MovingNebulaDesc(string modelName, string matSuffix)
    : this()
  {
    this.modelName = modelName;
    this.matSuffix = matSuffix;
  }

  public void Read(BgoProtocolReader pr)
  {
    this.matSuffix = pr.ReadString();
    this.modelName = pr.ReadString();
    this.rotation = pr.ReadQuaternion();
    this.position = pr.ReadVector3();
    this.textureOffset = pr.ReadVector2();
    this.textureScale = pr.ReadVector2();
    this.color = pr.ReadColor();
  }
}
