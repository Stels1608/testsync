﻿// Decompiled with JetBrains decompiler
// Type: TargetColorsLegacyBrackets
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class TargetColorsLegacyBrackets
{
  public static readonly Color cylonColor = ColorUtility.MakeColorFrom0To255(201U, 46U, 71U);
  public static readonly Color colonialColor = ColorUtility.MakeColorFrom0To255(122U, 163U, 238U);
  public static readonly Color ancientColor = ColorUtility.MakeColorFrom0To255(176U, 176U, 0U);
  public static readonly Color neutralColor = ColorUtility.MakeColorFrom0To255(156U, 158U, 161U);
  public static readonly Color cylonPartyColor = ColorUtility.MakeColorFrom0To255(223U, 131U, 41U);
  public static readonly Color colonialPartyColor = ColorUtility.MakeColorFrom0To255(42U, 208U, 175U);
  public static readonly Color cylonGuildColor = ColorUtility.MakeColorFrom0To255(201U, 46U, 71U);
  public static readonly Color colonialGuildColor = ColorUtility.MakeColorFrom0To255(122U, 163U, 238U);
  public static readonly Color designatedTargetColor = ColorUtility.MakeColorFrom0To255(218U, 212U, 54U);
  public static readonly Color waypointTargetColor = ColorUtility.MakeColorFrom0To255((uint) byte.MaxValue, 94U, 22U);
  public static readonly Color sectorEventColor = ColorUtility.MakeColorFrom0To255(64U, 230U, 128U);
  public static readonly Color friendlyColor = ColorUtility.MakeColorFrom0To255(200U, 200U, 200U);
  public static readonly Color enemyColor = ColorUtility.MakeColorFrom0To255(238U, 54U, 54U);
  public const string cylonColorString = "#c92e47";
  public const string colonialColorString = "#7aa3ee";

  public static Color GetFactionColor(Faction faction)
  {
    switch (faction)
    {
      case Faction.Neutral:
        return TargetColorsLegacyBrackets.neutralColor;
      case Faction.Colonial:
        return TargetColorsLegacyBrackets.colonialColor;
      case Faction.Cylon:
        return TargetColorsLegacyBrackets.cylonColor;
      case Faction.Ancient:
        return TargetColorsLegacyBrackets.ancientColor;
      default:
        return TargetColorsLegacyBrackets.neutralColor;
    }
  }
}
