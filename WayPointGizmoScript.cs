﻿// Decompiled with JetBrains decompiler
// Type: WayPointGizmoScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor;
using System.Collections.Generic;
using UnityEngine;

public class WayPointGizmoScript : MonoBehaviour
{
  public List<WayPoint> Points = new List<WayPoint>();
  public Color color = Color.green;

  private void OnDrawGizmos()
  {
    for (int index = 0; index < this.Points.Count; ++index)
    {
      if (index > 0 && (Object) this.Points[index].gameObject != (Object) null && (Object) this.Points[index - 1].gameObject != (Object) null)
      {
        Gizmos.color = this.color;
        Gizmos.DrawLine(this.Points[index].gameObject.transform.position, this.Points[index - 1].gameObject.transform.position);
      }
    }
  }
}
