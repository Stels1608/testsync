﻿// Decompiled with JetBrains decompiler
// Type: Euler3
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public struct Euler3
{
  public float pitch;
  public float yaw;
  public float roll;

  public static Euler3 zero
  {
    get
    {
      return new Euler3(0.0f, 0.0f, 0.0f);
    }
  }

  public static Euler3 identity
  {
    get
    {
      return Euler3.Rotation(Quaternion.identity);
    }
  }

  public Quaternion rotation
  {
    get
    {
      return Quaternion.Euler(this.pitch, this.yaw, this.roll);
    }
  }

  public Vector3 direction
  {
    get
    {
      return this.rotation * Vector3.forward;
    }
  }

  public Euler3(float pitch, float yaw)
  {
    this.pitch = pitch;
    this.yaw = yaw;
    this.roll = 0.0f;
  }

  public Euler3(float pitch, float yaw, float roll)
  {
    this.pitch = pitch;
    this.yaw = yaw;
    this.roll = roll;
  }

  public static bool operator ==(Euler3 a, Euler3 b)
  {
    if ((double) a.pitch == (double) b.pitch && (double) a.yaw == (double) b.yaw)
      return (double) a.roll == (double) b.roll;
    return false;
  }

  public static bool operator !=(Euler3 a, Euler3 b)
  {
    if ((double) a.pitch == (double) b.pitch && (double) a.yaw == (double) b.yaw)
      return (double) a.roll != (double) b.roll;
    return true;
  }

  public static Euler3 operator +(Euler3 a, Euler3 b)
  {
    return new Euler3(a.pitch + b.pitch, a.yaw + b.yaw, a.roll + b.roll);
  }

  public static Euler3 operator -(Euler3 a, Euler3 b)
  {
    return new Euler3(a.pitch - b.pitch, a.yaw - b.yaw, a.roll - b.roll);
  }

  public static Euler3 operator -(Euler3 a)
  {
    return new Euler3(-a.pitch, -a.yaw, -a.roll);
  }

  public static Euler3 operator *(Euler3 a, float b)
  {
    return new Euler3(a.pitch * b, a.yaw * b, a.roll * b);
  }

  public static Euler3 operator /(Euler3 a, float b)
  {
    return new Euler3(a.pitch / b, a.yaw / b, a.roll / b);
  }

  public Euler3 Normalized(bool forceStraight)
  {
    float num = Euler3.NormAngle(this.pitch);
    float angle1 = this.yaw;
    float angle2 = this.roll;
    if (forceStraight && (double) Mathf.Abs(num) > 90.0)
    {
      num = Euler3.NormAngle(179.99f - num);
      angle1 += 179.99f;
      angle2 += 179.99f;
    }
    return new Euler3(num, Euler3.NormAngle(angle1), Euler3.NormAngle(angle2));
  }

  private static float NormAngle(float angle)
  {
    while ((double) angle <= -180.0)
      angle += 360f;
    while ((double) angle > 180.0)
      angle -= 360f;
    return angle;
  }

  public static Euler3 Direction(Vector3 direction)
  {
    float yaw = Mathf.Atan2(direction.x, direction.z) * 57.29578f;
    return new Euler3((float) (-(double) Mathf.Atan2(direction.y, Mathf.Sqrt((float) ((double) direction.x * (double) direction.x + (double) direction.z * (double) direction.z))) * 57.2957801818848), yaw, 0.0f);
  }

  public static Euler3 Rotation(Quaternion quat)
  {
    float num1 = quat.x * quat.x;
    float num2 = quat.y * quat.y;
    float num3 = quat.z * quat.z;
    float num4 = quat.w * quat.w;
    float num5 = num1 + num2 + num3 + num4;
    float num6 = (float) (-(double) quat.z * (double) quat.y + (double) quat.x * (double) quat.w);
    float pitch;
    float yaw;
    float roll;
    if ((double) num6 > 0.499999 * (double) num5)
    {
      pitch = 1.570796f;
      yaw = 2f * Mathf.Atan2(-quat.z, quat.w);
      roll = 0.0f;
    }
    else if ((double) num6 < -0.499999 * (double) num5)
    {
      pitch = -1.570796f;
      yaw = -2f * Mathf.Atan2(-quat.z, quat.w);
      roll = 0.0f;
    }
    else
    {
      pitch = Mathf.Asin(2f * num6 / num5);
      yaw = Mathf.Atan2((float) (2.0 * (double) quat.y * (double) quat.w + 2.0 * (double) quat.z * (double) quat.x), -num1 - num2 + num3 + num4);
      roll = Mathf.Atan2((float) (2.0 * (double) quat.z * (double) quat.w + 2.0 * (double) quat.y * (double) quat.x), -num1 + num2 - num3 + num4);
    }
    return new Euler3(pitch, yaw, roll) * 57.29578f;
  }

  public void Clamp(Euler3 from, Euler3 to)
  {
    this.pitch = Mathf.Clamp(this.pitch, from.pitch, to.pitch);
    this.yaw = Mathf.Clamp(this.yaw, from.yaw, to.yaw);
    this.roll = Mathf.Clamp(this.roll, from.roll, to.roll);
  }

  public void ClampMax(Euler3 max)
  {
    this.pitch = Mathf.Min(this.pitch, max.pitch);
    this.yaw = Mathf.Min(this.yaw, max.yaw);
    this.roll = Mathf.Min(this.roll, max.roll);
  }

  public void ClampMin(Euler3 min)
  {
    this.pitch = Mathf.Max(this.pitch, min.pitch);
    this.yaw = Mathf.Max(this.yaw, min.yaw);
    this.roll = Mathf.Max(this.roll, min.roll);
  }

  public static Euler3 Scale(Euler3 a, Euler3 b)
  {
    return new Euler3(a.pitch * b.pitch, a.yaw * b.yaw, a.roll * b.roll);
  }

  public override string ToString()
  {
    return string.Format("({0},{1},{2})", (object) this.pitch, (object) this.yaw, (object) this.roll);
  }

  public static Euler3 RotateOverTime(Euler3 start, Euler3 changePerSecond, float dt)
  {
    Vector3 vector3 = changePerSecond.ComponentsToVector3();
    return Euler3.Rotation(Quaternion.AngleAxis(vector3.magnitude * dt, vector3.normalized) * start.rotation);
  }

  public static Euler3 RotateOverTimeLocal(Euler3 start, Euler3 changePerSecond, float dt)
  {
    Quaternion quaternion = Quaternion.Slerp(Quaternion.identity, changePerSecond.rotation, dt);
    return Euler3.Rotation(start.rotation * quaternion);
  }

  public static Quaternion RotateOverTime(Quaternion start, Quaternion changePerSecond, float dt)
  {
    return Quaternion.Slerp(Quaternion.identity, changePerSecond, dt) * start;
  }

  public static Quaternion RotateOverTimeLocal(Quaternion start, Quaternion changePerSecond, float dt)
  {
    Quaternion quaternion = Quaternion.Slerp(Quaternion.identity, changePerSecond, dt);
    return start * quaternion;
  }

  public Vector3 ComponentsToVector3()
  {
    return new Vector3(this.pitch, this.yaw, this.roll);
  }

  public void ComponentsFromVector3(Vector3 input)
  {
    this.pitch = input.x;
    this.yaw = input.y;
    this.roll = input.z;
  }

  public override int GetHashCode()
  {
    return base.GetHashCode();
  }

  public override bool Equals(object o)
  {
    return this == (Euler3) o;
  }
}
