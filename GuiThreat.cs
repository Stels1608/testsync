﻿// Decompiled with JetBrains decompiler
// Type: GuiThreat
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GuiThreat : GuiPanel
{
  private float alphaValue = 1f;
  private float delta = 1f;
  private readonly GuiLabel threat = new GuiLabel("%$bgo.misc.threat%", Color.white, Gui.Options.FontBGM_BT);

  public bool IsCombatGuiShown { get; set; }

  public GuiThreat()
  {
    this.threat.FontSize = 16;
    this.threat.Align = Align.MiddleCenter;
    this.AddChild((GuiElementBase) this.threat);
    this.Align = Align.DownCenter;
    this.PositionY = -220f;
  }

  public override void Update()
  {
    this.threat.IsRendered = Game.Me.Stats.InCombat && this.IsCombatGuiShown;
    if (!this.threat.IsRendered)
      return;
    this.alphaValue += (float) ((double) this.delta * (double) Time.deltaTime * 2.0);
    if ((double) this.alphaValue < 0.0 || (double) this.alphaValue > 1.0)
      this.delta = -this.delta;
    this.alphaValue = Mathf.Clamp01(this.alphaValue);
    Color normalColor = this.threat.NormalColor;
    normalColor.a = this.alphaValue;
    GuiLabel guiLabel = this.threat;
    Color color1 = normalColor;
    this.threat.OverColor = color1;
    Color color2 = color1;
    guiLabel.NormalColor = color2;
  }

  public override bool Contains(float2 point)
  {
    return false;
  }
}
