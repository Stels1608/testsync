﻿// Decompiled with JetBrains decompiler
// Type: SpaceExposer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpaceExposer : MonoBehaviour
{
  public GameObject[] MiningShipExplosions;
  public Material scanMaterial;
  public GameObject[] SmallMissileExplosions;
  public GameObject[] MediumMissileExplosions;
  public GameObject[] LargeMissileExplosions;
  public GameObject[] MiniNukeExplosions;
  public GameObject[] NukeExplosions;
  public GameObject[] Torpedo200Explosions;
  public GameObject[] Torpedo400Explosions;
  public GameObject[] Torpedo600Explosions;
  public GameObject[] ColonialSmallShipExplosions;
  public GameObject[] ColonialMediumShipExplosions;
  public GameObject[] ColonialLargeShipExplosions;
  public GameObject[] ColonialXLargeShipExplosions;
  public GameObject[] CylonSmallShipExplosions;
  public GameObject[] CylonMediumShipExplosions;
  public GameObject[] CylonLargeShipExplosions;
  public GameObject[] CylonXLargeShipExplosions;
  public MusicBox MusicBoxPrefab;
  public SectorSFX SectorSFXPrefab;
  public SoundSet ShipImpactSoundSet;
  public SoundSet MetalBulletHitSoundSet;
  public SoundSet RockBulletHitSoundSet;
  public GameObject ColonialFiringArc20;
  public GameObject ColonialFiringArc30;
  public GameObject ColonialFiringArc40;
  public GameObject ColonialFiringArc50;
  public GameObject ColonialFiringArc60;
  public GameObject ColonialFiringArc75;
  public GameObject ColonialFiringArc90;
  public GameObject ColonialFiringArc120;
  public GameObject ColonialFiringArc180;
  public GameObject ColonialFiringArc360;
  public GameObject CylonFiringArc20;
  public GameObject CylonFiringArc30;
  public GameObject CylonFiringArc40;
  public GameObject CylonFiringArc50;
  public GameObject CylonFiringArc60;
  public GameObject CylonFiringArc75;
  public GameObject CylonFiringArc90;
  public GameObject CylonFiringArc120;
  public GameObject CylonFiringArc180;
  public GameObject CylonFiringArc360;
  public GameObject[] MineFieldExplosions;
  public AudioClip JumpInClip;
  public AudioClip JumpOutClip;
  public AudioClip StealthAmbient;
  public AudioClip FortressModeAmbient;
  private static SpaceExposer instance;

  private void Awake()
  {
    SpaceExposer.instance = this;
  }

  private void OnDisable()
  {
    SpaceExposer.instance = (SpaceExposer) null;
  }

  public static SpaceExposer GetInstance()
  {
    return SpaceExposer.instance;
  }
}
