﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorColorInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class HudIndicatorColorInfo
{
  private static readonly Color partyColorLegacy = Game.Me.Faction != Faction.Colonial ? TargetColorsLegacyBrackets.cylonPartyColor : TargetColorsLegacyBrackets.colonialPartyColor;
  private static readonly Color guildColorLegacy = Game.Me.Faction != Faction.Colonial ? TargetColorsLegacyBrackets.cylonGuildColor : TargetColorsLegacyBrackets.colonialGuildColor;

  public static Color GetTargetColor4LegacyBracket(ISpaceEntity target)
  {
    Color color = TargetColorsLegacyBrackets.neutralColor;
    if (target != null)
    {
      if (target.IsPlayer && target.PlayerRelation == Relation.Friend)
      {
        color = target.Faction != Faction.Cylon ? TargetColorsLegacyBrackets.colonialColor : TargetColorsLegacyBrackets.cylonColor;
        PlayerShip playerShip = target as PlayerShip;
        if (playerShip != null && playerShip.Player != null)
        {
          if (Game.Me.Party.Members.Contains(playerShip.Player))
            color = HudIndicatorColorInfo.partyColorLegacy;
          else if (playerShip.Player.HasGuild && (int) Game.Me.Guild.ServerID == (int) playerShip.Player.guildId)
            color = HudIndicatorColorInfo.guildColorLegacy;
        }
      }
      else
      {
        switch (target.Faction)
        {
          case Faction.Colonial:
            color = !target.IsHostileCongener ? TargetColorsLegacyBrackets.colonialColor : TargetColorsLegacyBrackets.cylonColor;
            break;
          case Faction.Cylon:
            color = !target.IsHostileCongener ? TargetColorsLegacyBrackets.cylonColor : TargetColorsLegacyBrackets.colonialColor;
            break;
          case Faction.Ancient:
            color = TargetColorsLegacyBrackets.ancientColor;
            break;
          default:
            color = TargetColorsLegacyBrackets.neutralColor;
            break;
        }
      }
    }
    return color;
  }

  public static Color GetTargetColor4FactionMode(ISpaceEntity target)
  {
    Color color = TargetColors.neutralColor;
    if (target != null)
    {
      if (target.SpaceEntityType == SpaceEntityType.SectorEvent)
        return TargetColors.sectorEventColor;
      if (target.IsPlayer && target.PlayerRelation == Relation.Friend)
      {
        color = target.Faction != Faction.Cylon ? TargetColors.colonialColor : TargetColors.cylonColor;
        PlayerShip playerShip = target as PlayerShip;
        if (playerShip != null && playerShip.Player != null)
        {
          if (Game.Me.Party.Members.Contains(playerShip.Player))
            color = HudIndicatorColorInfo.partyColorLegacy;
          else if (playerShip.Player.HasGuild && (int) Game.Me.Guild.ServerID == (int) playerShip.Player.guildId)
            color = HudIndicatorColorInfo.guildColorLegacy;
        }
      }
      else
      {
        switch (target.Faction)
        {
          case Faction.Colonial:
            color = !target.IsHostileCongener ? TargetColors.colonialColor : TargetColors.cylonColor;
            break;
          case Faction.Cylon:
            color = !target.IsHostileCongener ? TargetColors.cylonColor : TargetColors.colonialColor;
            break;
          case Faction.Ancient:
            color = TargetColors.ancientColor;
            break;
          default:
            color = TargetColors.neutralColor;
            break;
        }
      }
    }
    return color;
  }
}
