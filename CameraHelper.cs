﻿// Decompiled with JetBrains decompiler
// Type: CameraHelper
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CameraHelper
{
  private Vector3 cachedMainCameraPosition;
  private Camera cachedMainCam;
  private Vector3 cachedMainCamForwardVector;

  public Vector3 CachedMainCameraPosition
  {
    get
    {
      return this.cachedMainCameraPosition;
    }
  }

  public Camera CachedMainCam
  {
    get
    {
      return this.cachedMainCam;
    }
  }

  public Vector3 CachedMainCamForwardVector
  {
    get
    {
      return this.cachedMainCamForwardVector;
    }
  }

  public void Update()
  {
    if ((Object) Camera.main == (Object) null)
      return;
    this.cachedMainCam = Camera.main;
    this.cachedMainCameraPosition = this.cachedMainCam.transform.position;
    this.cachedMainCamForwardVector = this.cachedMainCam.transform.forward;
  }
}
