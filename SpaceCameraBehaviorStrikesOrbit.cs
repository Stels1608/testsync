﻿// Decompiled with JetBrains decompiler
// Type: SpaceCameraBehaviorStrikesOrbit
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpaceCameraBehaviorStrikesOrbit : SpaceCameraBehaviorBase
{
  private float x;
  private float y;
  private readonly float cameraOffset;
  private bool leftMouseDown;

  public override bool BlocksControls
  {
    get
    {
      return true;
    }
  }

  public override bool ShowStrikesHud
  {
    get
    {
      return false;
    }
  }

  public override KeyCode SelectionMouseButton
  {
    get
    {
      return KeyCode.Mouse0;
    }
  }

  public SpaceCameraBehaviorStrikesOrbit()
  {
    this.cameraOffset = Mathf.Max((this.PlayerShip.Position - this.CamPosition).magnitude - SpaceCameraBase.WantedZoomDistance, 0.0f);
    this.leftMouseDown = true;
  }

  public override SpaceCameraParameters CalculateCurrentCameraParameters()
  {
    if (Input.GetMouseButtonDown(0))
      this.leftMouseDown = (GUIManager.MouseOverAnyGuiElement ? 1 : (GUIManager.ModalNguiWindowOpened ? 1 : 0)) == 0;
    else if (Input.GetMouseButtonUp(0))
      this.leftMouseDown = false;
    if (this.leftMouseDown)
    {
      this.x += Input.GetAxis("Mouse X") * 5f;
      this.y -= Input.GetAxis("Mouse Y") * 2.5f;
    }
    Quaternion quaternion = this.PlayerShip.Rotation * Quaternion.Euler(this.y, this.x, 0.0f);
    Vector3 vector3 = quaternion * new Vector3(0.0f, 4.4f, -this.cameraOffset - SpaceCameraBase.WantedZoomDistance) + this.PlayerShip.Position;
    return new SpaceCameraParameters() { Fov = this.Fov, CamPosition = vector3, CamRotation = quaternion };
  }
}
