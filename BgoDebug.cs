﻿// Decompiled with JetBrains decompiler
// Type: BgoDebug
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class BgoDebug
{
  public static string DebugSkin = "Debug/Skin/colonial";
  public static bool enabled;

  public static void DrawPoint(Vector3 point, float size)
  {
    if (!BgoDebug.enabled)
      return;
    Debug.DrawLine(point - Vector3.up * size, point + Vector3.up * size);
    Debug.DrawLine(point - Vector3.forward * size, point + Vector3.forward * size);
    Debug.DrawLine(point - Vector3.right * size, point + Vector3.right * size);
  }

  public static void DrawPathWithNormal(Vector3[] points, Vector3 normal, float size, Color color)
  {
    if (!BgoDebug.enabled)
      return;
    normal.Normalize();
    for (int index = 1; index < points.Length; ++index)
    {
      Vector3 normalized = Vector3.Cross(points[index] - points[index - 1], normal).normalized;
      Debug.DrawLine(points[index - 1], points[index], color);
      Debug.DrawLine(points[index - 1], points[index - 1] + normalized * size, color);
    }
  }

  public static void DrawPath(Vector3[] points, Color color)
  {
    if (!BgoDebug.enabled)
      return;
    for (int index = 1; index < points.Length; ++index)
      Debug.DrawLine(points[index - 1], points[index], color);
  }

  public static void DrawPath(Vector3[] points)
  {
    BgoDebug.DrawPath(points, Color.white);
  }

  public static GUISkin LoadBlackSkin()
  {
    return (GUISkin) Resources.Load("Debug/Skin/black");
  }

  public static GUISkin LoadDebugSkin()
  {
    return (GUISkin) Resources.Load(BgoDebug.DebugSkin);
  }

  public static Size CalcTextSize(string text, GUIStyle style)
  {
    GUIContent content = new GUIContent();
    content.text = text;
    float minWidth;
    float maxWidth;
    style.CalcMinMaxWidth(content, out minWidth, out maxWidth);
    return new Size((int) maxWidth, (int) style.CalcHeight(content, maxWidth));
  }

  public static Size CalcTextSize(string text, GUIStyle style, float fixedWidth)
  {
    GUIContent content = new GUIContent();
    content.text = text;
    style.wordWrap = true;
    return new Size((int) fixedWidth, (int) style.CalcHeight(content, fixedWidth));
  }
}
