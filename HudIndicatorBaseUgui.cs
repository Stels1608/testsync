﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorBaseUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class HudIndicatorBaseUgui : HudIndicatorBase
{
  public static bool ResizeBracketSetting = true;
  private CanvasGroup canvasGroup;
  private RectTransform cachedRectTransform;
  private Component masterComponent;
  public Vector2 BracketSize;
  public Vector2 _minUnselectedBracketSize;
  private bool _neverBlocksRaycast;

  public bool NeverBlocksRaycast
  {
    get
    {
      return this._neverBlocksRaycast;
    }
    set
    {
      this._neverBlocksRaycast = value;
      this.canvasGroup.blocksRaycasts = !value;
    }
  }

  public CanvasGroup CanvasGroup
  {
    get
    {
      return this.canvasGroup;
    }
  }

  private Vector2 MinBracketSize
  {
    get
    {
      if (this.isSelected)
        return new Vector2(60f, 40f);
      return this._minUnselectedBracketSize;
    }
  }

  protected override void Awake()
  {
    this.SetMinimumUnselectedBracketWidth(60f);
    this.canvasGroup = this.gameObject.AddComponent<CanvasGroup>();
    this.cachedRectTransform = this.GetComponent<RectTransform>();
    base.Awake();
  }

  public void SetMinimumUnselectedBracketWidth(float minWidth)
  {
    this._minUnselectedBracketSize = new Vector2(minWidth, minWidth * 0.6666667f);
  }

  public override void ResetStates()
  {
    base.ResetStates();
    this.CanvasGroup.blocksRaycasts = !this.NeverBlocksRaycast;
  }

  public override void RemoteLateUpdate()
  {
    this.UpdateBracketSize();
    base.RemoteLateUpdate();
    this.UpdateMiniMode();
  }

  private void UpdateMiniMode()
  {
    bool flag = !HudIndicatorInfo.IsNeverInMiniMode(this.Target) && (HudIndicatorInfo.IsAlwaysInMiniMode(this.Target) || (double) this.Target.GetDistance() > (double) HudIndicatorInfo.MiniModeRangeThreshold);
    int num1 = flag ? 1 : 0;
    bool? nullable = this.isBeyondMiniModeRange;
    int num2 = nullable.GetValueOrDefault() ? 1 : 0;
    if ((num1 != num2 ? 1 : (!nullable.HasValue ? 1 : 0)) == 0)
      return;
    this.isBeyondMiniModeRange = new bool?(flag);
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnMiniModeChange(this.isBeyondMiniModeRange.Value);
    }
  }

  private void UpdateBracketSize()
  {
    if (!this.isSelected && !this.isMultiSelected && (!this.isTargetInFrontOfCamera || !this.isBeyondMiniModeRange.HasValue || this.isBeyondMiniModeRange.Value))
      this.BracketSize = this.MinBracketSize;
    else if (!HudIndicatorBaseUgui.ResizeBracketSetting)
    {
      this.BracketSize = this.MinBracketSize;
    }
    else
    {
      float magnitude = (SpaceCameraBase.CameraHelper.CachedMainCameraPosition - this.Target.Position).magnitude;
      Camera cachedMainCam = SpaceCameraBase.CameraHelper.CachedMainCam;
      float x = Mathf.Clamp((float) Screen.width * (this.Target.MeshBoundsRaw.size.z / (2f * magnitude * Mathf.Tan((float) ((double) cachedMainCam.fieldOfView * 0.5 * (Math.PI / 180.0))) * cachedMainCam.aspect)), this.MinBracketSize.x, 270f);
      this.BracketSize = new Vector2(x, x * 0.6666667f);
    }
  }

  protected override bool IsInScreenArea(Vector3 targetScreenPos)
  {
    float num1 = this.BracketSize.x / 2f;
    float num2 = this.BracketSize.y / 2f;
    if ((double) targetScreenPos.x + (double) num1 > 0.0 && (double) targetScreenPos.x - (double) num1 < (double) Screen.width && (double) targetScreenPos.y + (double) num2 > 0.0)
      return (double) targetScreenPos.y - (double) num2 < (double) Screen.height;
    return false;
  }

  public override T AddHudIndicatorComponent<T>()
  {
    if ((UnityEngine.Object) this.masterComponent == (UnityEngine.Object) null)
    {
      Debug.LogError((object) ("HudIndicatorBaseUgui.cs: Trying to add component to " + this.name + "without setting up the master component first. "));
      return (T) null;
    }
    T child = UguiTools.CreateChild<T>(this.masterComponent.transform);
    child.SetHudIndicator((HudIndicatorBase) this);
    this.RegisterHudIndicatorComponent<T>(child);
    return child;
  }

  public T AddHudIndicatorComponentFromObject<T>(GameObject prototype, Transform parent = null) where T : Component, IHudIndicatorComponent
  {
    if ((UnityEngine.Object) parent == (UnityEngine.Object) null)
      parent = this.masterComponent.transform;
    if ((UnityEngine.Object) parent == (UnityEngine.Object) null)
    {
      Debug.LogError((object) ("HudIndicatorBaseUgui.cs: Trying to add component to " + this.name + "without setting up the master component first. "));
      return (T) null;
    }
    T child = UguiTools.CreateChild<T>(prototype, parent);
    child.SetHudIndicator((HudIndicatorBase) this);
    this.RegisterHudIndicatorComponent<T>(child);
    return child;
  }

  public T CreateMasterComponentFromObject<T>(GameObject prototype) where T : Component, IHudIndicatorComponent
  {
    if ((UnityEngine.Object) this.masterComponent != (UnityEngine.Object) null)
    {
      Debug.LogError((object) ("HudIndicatorBaseUgui.cs: Master component already created for " + this.name));
      return (T) null;
    }
    T child = UguiTools.CreateChild<T>(prototype, this.transform);
    if ((UnityEngine.Object) child == (UnityEngine.Object) null)
    {
      Debug.LogError((object) ("CreateMasterComponentFromObject(): Component or Prefab not found. Component " + (object) typeof (T) + " Prefab: " + (!((UnityEngine.Object) prototype == (UnityEngine.Object) null) ? prototype.name : "null")));
      return (T) null;
    }
    this.RegisterHudIndicatorComponent<T>(child);
    child.gameObject.name += " (Master)";
    child.SetHudIndicator((HudIndicatorBase) this);
    this.masterComponent = (Component) child;
    return child;
  }

  public T CreateMasterComponent<T>() where T : Component, IHudIndicatorComponent
  {
    if ((UnityEngine.Object) this.masterComponent != (UnityEngine.Object) null)
    {
      Debug.LogError((object) ("HudIndicatorBaseUgui.cs: Master component already created for " + this.name));
      return (T) null;
    }
    T child = UguiTools.CreateChild<T>(this.transform);
    this.RegisterHudIndicatorComponent<T>(child);
    // ISSUE: explicit reference operation
    // ISSUE: explicit reference operation
    (^@child).name += " (Master)";
    child.SetHudIndicator((HudIndicatorBase) this);
    this.masterComponent = (Component) child;
    return child;
  }

  protected override void UpdateIndicatorPositionAndOrientation(Vector3 unityScreenCoordinates)
  {
    if ((UnityEngine.Object) this.cameraHelper.CachedMainCam == (UnityEngine.Object) null)
      return;
    Vector3 vector3;
    if (this.isVisibleOnScreen.Value)
    {
      vector3 = unityScreenCoordinates;
    }
    else
    {
      float indicatorOrientation = this.GetIndicatorOrientation(unityScreenCoordinates);
      this.UpdateArrowOrientation(indicatorOrientation);
      vector3 = this.GetBorderClampedPosition(indicatorOrientation);
    }
    this.cachedRectTransform.position = (Vector3) (Vector2) vector3;
  }

  public override void SetOpacity(float alpha)
  {
    this.CanvasGroup.alpha = alpha;
  }

  protected override void EnableCombatGui(bool enable)
  {
  }

  public override void SetSelected(bool targetIsSelected)
  {
    base.SetSelected(targetIsSelected);
    this.isSelected = targetIsSelected;
    this.CanvasGroup.blocksRaycasts = !targetIsSelected && !this.NeverBlocksRaycast;
    if (!this.isSelected)
      return;
    this.transform.SetAsLastSibling();
  }

  public override void SetMultiSelected(bool isMultiSelected, ShipAbility ability)
  {
    this.isMultiSelected = isMultiSelected;
    base.SetMultiSelected(isMultiSelected, ability);
  }
}
