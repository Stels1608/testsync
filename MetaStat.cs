﻿// Decompiled with JetBrains decompiler
// Type: MetaStat
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class MetaStat
{
  public ObjectStat Meta { get; private set; }

  public List<ObjectStat> EncompassingStats { get; private set; }

  public MetaStat(ObjectStat meta, params ObjectStat[] encompassingStats)
  {
    this.Meta = meta;
    this.EncompassingStats = new List<ObjectStat>((IEnumerable<ObjectStat>) encompassingStats);
  }

  public bool Encompasses(List<ObjectStat> stats)
  {
    using (List<ObjectStat>.Enumerator enumerator = this.EncompassingStats.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ObjectStat current = enumerator.Current;
        if (!stats.Contains(current))
          return false;
      }
    }
    return true;
  }

  public bool IsPartOfMetaStat(ObjectStat stat)
  {
    return this.EncompassingStats.Contains(stat);
  }
}
