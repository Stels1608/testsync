﻿// Decompiled with JetBrains decompiler
// Type: GUIChatNew
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using BsgoEditor.GUIEditor;
using Chat;
using Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GUIChatNew : GUIPanel
{
  private static GUIChatNew.ChatChannel lastPressedTab = GUIChatNew.ChatChannel.System;
  private static bool viewLocal;
  private static bool viewGlobal;
  private static bool minimized;
  private readonly GUITextBox textBox;
  private readonly GUIImageNew messagesArea;
  private readonly GUIVerticalLayout layout;
  private readonly GUIChatButton fleet;
  private readonly GUIChatButton open;
  private readonly GUIChatButton system;
  private readonly GUIChatButton squadron;
  private readonly GUIChatButton wing;
  private readonly GUIChatButton combatLog;
  private readonly GUIChatCulturePanel culturePanel;
  private readonly GUIChatCheckBox globalToggler;
  private readonly GUIChatCheckBox minimize;
  private readonly GUIRadioGroup<GUIChatButton> radio;
  private readonly List<GUIPanel> alwaysRendered;
  private readonly List<GUIPanel> retainsHandlingMouse;
  private ChatDataStorage.Log current;
  public readonly GUISlotPopup Popup;
  private readonly GUIChatNew.Parser parser;
  private GUIScaleState guiScaleState;

  public ChatDataStorage.Log Current
  {
    get
    {
      return this.current;
    }
    private set
    {
      this.current = value;
      this.MakeViewActual();
    }
  }

  public bool ViewLocal
  {
    set
    {
      bool flag = value;
      this.culturePanel.Local = flag;
      GUIChatNew.viewLocal = flag;
    }
  }

  public bool ViewGlobal
  {
    set
    {
      bool flag = value;
      this.culturePanel.Global = flag;
      GUIChatNew.viewGlobal = flag;
    }
  }

  public bool ShowPrefix { get; set; }

  public bool IsFocused
  {
    get
    {
      return this.textBox.IsFocused;
    }
    set
    {
      this.textBox.IsFocused = value;
    }
  }

  private bool Global
  {
    get
    {
      return !this.globalToggler.IsPressed;
    }
    set
    {
      this.globalToggler.IsPressed = !value;
      Game.Me.ChatGlobal = this.globalToggler.IsPressed;
      GUIButtonNew recoursively1 = this.FindRecoursively<GUIButtonNew>("openShortcut");
      GUIButtonNew recoursively2 = this.FindRecoursively<GUIButtonNew>("fleetShortcut");
      GUIButtonNew recoursively3 = this.FindRecoursively<GUIButtonNew>("systemShortcut");
      recoursively1.SetTooltip("%$bgo.chat.tooltip_open_shortcut%");
      recoursively1.TextLabel.Text = Commands.Get(!this.Global ? ChannelName.OpenLocal : ChannelName.OpenGlobal);
      recoursively1.TextLabel.AllColor = ColorScheme.OpenName(Game.Me.Faction);
      recoursively2.TextLabel.Text = Commands.Get(!this.Global ? ChannelName.FleetLocal : ChannelName.FleetGlobal);
      recoursively2.TextLabel.AllColor = ColorScheme.FleetName(this.Global);
      recoursively3.TextLabel.Text = Commands.Get(!this.Global ? ChannelName.SystemLocal : ChannelName.SystemGlobal);
      recoursively3.TextLabel.AllColor = ColorScheme.SystemName(this.Global);
    }
  }

  public ChannelName Selected
  {
    get
    {
      string name = this.radio.Pressed.Name;
      bool global = this.Global;
      ChannelName channelName = (ChannelName) null;
      if (name == "wing")
        channelName = ChannelName.Wing;
      else if (name == "officers")
        channelName = ChannelName.Officer;
      else if (name == "squadron")
        channelName = ChannelName.Squadron;
      else if (name == "fleet" && global)
        channelName = ChannelName.FleetGlobal;
      else if (name == "fleet" && !global)
        channelName = ChannelName.FleetLocal;
      else if (name == "open" && global)
        channelName = ChannelName.OpenGlobal;
      else if (name == "open" && !global)
        channelName = ChannelName.OpenLocal;
      else if (name == "system" && global)
        channelName = ChannelName.SystemGlobal;
      else if (name == "system" && !global)
        channelName = ChannelName.SystemLocal;
      return channelName;
    }
  }

  public GUIChatNew()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GUIChatNew.\u003CGUIChatNew\u003Ec__AnonStorey7F newCAnonStorey7F = new GUIChatNew.\u003CGUIChatNew\u003Ec__AnonStorey7F();
    this.radio = new GUIRadioGroup<GUIChatButton>();
    this.alwaysRendered = new List<GUIPanel>();
    this.retainsHandlingMouse = new List<GUIPanel>();
    this.Popup = new GUISlotPopup();
    this.guiScaleState = GUIScaleState.Original;
    // ISSUE: explicit constructor call
    base.\u002Ector();
    // ISSUE: reference to a compiler-generated field
    newCAnonStorey7F.\u003C\u003Ef__this = this;
    this.Name = "ChatNew";
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/Chat/layout");
    GUIImageNew guiImageNew1 = (jwindowDescription["background"] as JImage).CreateGUIImageNew();
    GUIImageNew guiImageNew2 = (jwindowDescription["backgroundMinimized"] as JImage).CreateGUIImageNew();
    guiImageNew2.Position = guiImageNew1.Position;
    guiImageNew2.IsRendered = false;
    guiImageNew2.HandleMouseInput = false;
    this.root.Size = guiImageNew1.Size;
    this.AddPanel((GUIPanel) guiImageNew1);
    this.AddPanel((GUIPanel) guiImageNew2);
    this.messagesArea = new GUIImageNew(jwindowDescription["messagesArea"] as JImage);
    this.messagesArea.IsRendered = false;
    this.AddPanel((GUIPanel) this.messagesArea);
    this.layout = new GUIVerticalLayout();
    this.AddPanel((GUIPanel) this.layout);
    this.layout.Parent = this.messagesArea.SmartRect;
    // ISSUE: reference to a compiler-generated field
    newCAnonStorey7F.colon = new GUILabelNew(jwindowDescription["colon"] as JLabel);
    // ISSUE: reference to a compiler-generated field
    this.AddPanel((GUIPanel) newCAnonStorey7F.colon);
    this.textBox = new GUITextBox(this.root, string.Empty, Gui.Options.FontVerdana, ColorUtility.MakeColorFrom0To255(195U, 228U, 239U), new GUIStyle()
    {
      clipping = TextClipping.Clip
    });
    this.textBox.FontSize = 12;
    this.textBox.Width = (jwindowDescription["textArea"] as JImage).size.x;
    this.textBox.Height = (jwindowDescription["textArea"] as JImage).size.y;
    this.textBox.Position = (jwindowDescription["textArea"] as JImage).position;
    // ISSUE: reference to a compiler-generated method
    this.textBox.Handler = new GUITextBox.Hndler(newCAnonStorey7F.\u003C\u003Em__9B);
    this.AddPanel((GUIPanel) this.textBox);
    GUIButtonNew guiButtonNew1 = new GUIButtonNew(jwindowDescription["helpShortcut"] as JButton);
    // ISSUE: reference to a compiler-generated field
    newCAnonStorey7F.fleetShortcut = new GUIButtonNew(jwindowDescription["fleetShortcut"] as JButton);
    // ISSUE: reference to a compiler-generated field
    newCAnonStorey7F.systemShortcut = new GUIButtonNew(jwindowDescription["systemShortcut"] as JButton);
    // ISSUE: reference to a compiler-generated field
    newCAnonStorey7F.openShortcut = new GUIButtonNew(jwindowDescription["openShortcut"] as JButton);
    GUIButtonNew guiButtonNew2 = new GUIButtonNew(jwindowDescription["whisperShortcut"] as JButton);
    guiButtonNew1.SetTooltip("%$bgo.chat.tooltip_help_shortcut%");
    // ISSUE: reference to a compiler-generated field
    newCAnonStorey7F.fleetShortcut.SetTooltip("%$bgo.chat.tooltip_fleet_shortcut%");
    // ISSUE: reference to a compiler-generated field
    newCAnonStorey7F.systemShortcut.SetTooltip("%$bgo.chat.tooltip_system_shortcut%");
    guiButtonNew2.SetTooltip("%$bgo.chat.tooltip_whisper_shortcut%");
    this.AddPanel((GUIPanel) guiButtonNew1);
    // ISSUE: reference to a compiler-generated field
    this.AddPanel((GUIPanel) newCAnonStorey7F.fleetShortcut);
    // ISSUE: reference to a compiler-generated field
    this.AddPanel((GUIPanel) newCAnonStorey7F.systemShortcut);
    // ISSUE: reference to a compiler-generated field
    this.AddPanel((GUIPanel) newCAnonStorey7F.openShortcut);
    this.AddPanel((GUIPanel) guiButtonNew2);
    // ISSUE: reference to a compiler-generated method
    guiButtonNew1.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__9C);
    // ISSUE: reference to a compiler-generated field
    newCAnonStorey7F.twoSpaces = "  ";
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    newCAnonStorey7F.fleetShortcut.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__9D);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    newCAnonStorey7F.systemShortcut.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__9E);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    newCAnonStorey7F.openShortcut.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__9F);
    // ISSUE: reference to a compiler-generated method
    guiButtonNew2.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__A0);
    GUIImageNew guiImageNew3 = new GUIImageNew(jwindowDescription["cultureHelper"] as JImage);
    this.culturePanel = new GUIChatCulturePanel(Game.ChatLanguage, GUIChatNew.viewLocal, GUIChatNew.viewGlobal);
    // ISSUE: reference to a compiler-generated method
    this.culturePanel.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__A1);
    this.AddPanel((GUIPanel) this.culturePanel, guiImageNew3.Position);
    GUIChatButton guiChatButton1 = new GUIChatButton(jwindowDescription["scrollUp"] as JButton);
    guiChatButton1.SetTooltip("%$bgo.chat.tooltip_scrollup%");
    // ISSUE: reference to a compiler-generated method
    guiChatButton1.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__A2);
    this.AddPanel((GUIPanel) guiChatButton1);
    GUIChatButton guiChatButton2 = new GUIChatButton(jwindowDescription["scrollDown"] as JButton);
    guiChatButton2.SetTooltip("%$bgo.chat.tooltip_scrolldown%");
    // ISSUE: reference to a compiler-generated method
    guiChatButton2.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__A3);
    this.AddPanel((GUIPanel) guiChatButton2);
    GUIChatButton guiChatButton3 = new GUIChatButton(jwindowDescription["scrollBegin"] as JButton);
    guiChatButton3.SetTooltip("%$bgo.chat.tooltip_scrollbegin%");
    // ISSUE: reference to a compiler-generated method
    guiChatButton3.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__A4);
    this.AddPanel((GUIPanel) guiChatButton3);
    this.minimize = new GUIChatCheckBox(jwindowDescription["minimize"] as JButton);
    this.minimize.SetTooltip("%$bgo.chat.tooltip_minimize%");
    this.minimize.IsPressed = GUIChatNew.minimized;
    // ISSUE: reference to a compiler-generated method
    this.minimize.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__A5);
    this.AddPanel((GUIPanel) this.minimize);
    this.globalToggler = new GUIChatCheckBox(jwindowDescription["globalToggler"] as JButton);
    if (GUIChatCulturePanel.extend.ContainsKey(Game.ChatLanguage))
      this.globalToggler.PressedTexture = ResourceLoader.Load<Texture2D>("GUI/Chat/Culture/button_" + GUIChatCulturePanel.extend[Game.ChatLanguage] + "_selected");
    this.globalToggler.SetTooltip("%$bgo.chat.tooltip_global_local_toggler%");
    // ISSUE: reference to a compiler-generated method
    this.globalToggler.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__A6);
    this.AddPanel((GUIPanel) this.globalToggler);
    this.alwaysRendered.Add((GUIPanel) this.minimize);
    // ISSUE: reference to a compiler-generated field
    this.alwaysRendered.Add((GUIPanel) newCAnonStorey7F.colon);
    this.alwaysRendered.Add((GUIPanel) this.textBox);
    this.alwaysRendered.Add((GUIPanel) this.messagesArea);
    this.alwaysRendered.Add((GUIPanel) this.layout);
    this.alwaysRendered.Add((GUIPanel) this.globalToggler);
    this.retainsHandlingMouse.Add((GUIPanel) this.minimize);
    this.retainsHandlingMouse.Add((GUIPanel) this.textBox);
    this.retainsHandlingMouse.Add((GUIPanel) this.globalToggler);
    this.retainsHandlingMouse.Add((GUIPanel) guiImageNew2);
    this.open = new GUIChatButton(jwindowDescription["open"] as JButton);
    this.open.NormalTexture = (Texture2D) null;
    this.open.SetTooltip("%$bgo.chat.tooltip_open_channel%");
    // ISSUE: reference to a compiler-generated method
    this.open.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__A7);
    this.fleet = new GUIChatButton(this.open);
    this.fleet.TextLabel.Text = "%$bgo.chat.button_text_fleet%";
    this.fleet.SetTooltip("%$bgo.chat.tooltip_fleet_channel%");
    this.fleet.Name = "fleet";
    // ISSUE: reference to a compiler-generated method
    this.fleet.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__A8);
    this.system = new GUIChatButton(this.open);
    this.system.TextLabel.Text = "%$bgo.chat.button_text_system%";
    this.system.SetTooltip("%$bgo.chat.tooltip_system_channel%");
    this.system.Name = "system";
    // ISSUE: reference to a compiler-generated method
    this.system.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__A9);
    this.squadron = new GUIChatButton(jwindowDescription["squadron"] as JButton);
    this.squadron.NormalTexture = (Texture2D) null;
    this.squadron.SetTooltip("%$bgo.chat.tooltip_squadron_channel%");
    this.squadron.Name = "squadron";
    // ISSUE: reference to a compiler-generated method
    this.squadron.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__AA);
    this.wing = new GUIChatButton(jwindowDescription["wing"] as JButton);
    this.wing.NormalTexture = (Texture2D) null;
    this.wing.SetTooltip("%$bgo.chat.tooltip_wing_channel%");
    this.wing.Name = "wing";
    // ISSUE: reference to a compiler-generated method
    this.wing.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__AB);
    this.combatLog = new GUIChatButton(jwindowDescription["combatLog"] as JButton);
    this.combatLog.NormalTexture = (Texture2D) null;
    this.combatLog.SetTooltip("%$bgo.chat.tooltip_combatLog_channel%");
    this.combatLog.Name = "combatLog";
    // ISSUE: reference to a compiler-generated method
    this.combatLog.Handler = new AnonymousDelegate(newCAnonStorey7F.\u003C\u003Em__AC);
    this.radio.AddPanel((GUIPanel) this.fleet);
    this.radio.AddPanel((GUIPanel) this.system);
    this.radio.AddPanel((GUIPanel) this.open);
    this.radio.AddPanel((GUIPanel) this.squadron);
    this.radio.AddPanel((GUIPanel) this.wing);
    this.radio.AddPanel((GUIPanel) this.combatLog);
    this.AddPanel((GUIPanel) this.radio);
    this.Global = Game.Me.ChatGlobal;
    this.PressSystem();
    this.SetRememberedTab();
    this.AddPanel((GUIPanel) new GUIPanel.Timer(0.3f, new AnonymousDelegate(this.MakeViewActual)));
    this.parser = new GUIChatNew.Parser(this);
    this.AddPanel((GUIPanel) this.Popup);
    using (List<GUIPanel>.Enumerator enumerator = this.children.Except<GUIPanel>((IEnumerable<GUIPanel>) this.alwaysRendered).ToList<GUIPanel>().GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.IsRendered = !GUIChatNew.minimized;
    }
    if (GUIScreenResolution.IsDefault())
    {
      this.squadron.TextLabel.Text = "%$bgo.chat.button_short_text_squadron%";
      this.combatLog.TextLabel.Text = "%$bgo.chat.button_short_text_combatLog%";
      this.Scale(0.8f);
      this.guiScaleState = GUIScaleState.Downsized;
    }
    this.MakeViewActual();
  }

  public void Resize(GUIScaleState newScaleState)
  {
    if (newScaleState == this.guiScaleState)
      return;
    if (newScaleState == GUIScaleState.Downsized)
    {
      this.Scale(0.8f);
      this.squadron.TextLabel.Text = "%$bgo.chat.button_short_text_squadron%";
      this.combatLog.TextLabel.Text = "%$bgo.chat.button_short_text_combatLog%";
    }
    else
    {
      this.Scale(1.25f);
      this.squadron.TextLabel.Text = "%$bgo.chat.button_text_squadron%";
      this.combatLog.TextLabel.Text = "%$bgo.chat.button_text_combatLog%";
    }
    this.guiScaleState = newScaleState;
  }

  public void Scale(float scaleFactor)
  {
    List<GUIPanel> allRecoursively = this.FindAllRecoursively<GUIPanel>();
    for (int index = 0; index < allRecoursively.Count; ++index)
    {
      allRecoursively[index].Position = scaleFactor * allRecoursively[index].Position;
      allRecoursively[index].Size = scaleFactor * allRecoursively[index].Size;
    }
    this.RecalculateAbsCoords();
  }

  private void SetRememberedTab()
  {
    if (this.radio.Pressed != null)
      this.radio.Pressed.IsPressed = false;
    if (GUIChatNew.lastPressedTab == GUIChatNew.ChatChannel.Fleet)
      this.fleet.Handler();
    else if (GUIChatNew.lastPressedTab == GUIChatNew.ChatChannel.System)
      this.system.Handler();
    else if (GUIChatNew.lastPressedTab == GUIChatNew.ChatChannel.Open)
      this.open.Handler();
    else if (GUIChatNew.lastPressedTab == GUIChatNew.ChatChannel.Squadron)
      this.squadron.Handler();
    else if (GUIChatNew.lastPressedTab == GUIChatNew.ChatChannel.Wing)
    {
      this.wing.Handler();
    }
    else
    {
      if (GUIChatNew.lastPressedTab != GUIChatNew.ChatChannel.CombatLog)
        return;
      this.combatLog.Handler();
    }
  }

  private void PressRoomButton(GUILabelNew colon)
  {
    this.open.IsRendered = false;
    if (this.Current.Unread)
      this.Current.ViewNewestMessage();
    this.culturePanel.IsRendered = false;
    this.globalToggler.IsRendered = false;
    this.textBox.IsRendered = true;
    colon.IsRendered = true;
  }

  private bool SpellingIsCorrect(string text)
  {
    int num = 0;
    char ch = 'a';
    for (int index = 0; index < text.Length; ++index)
    {
      if ((int) text[index] == (int) ch)
      {
        ++num;
        if (num == 6)
          return false;
      }
      else
        num = 0;
      ch = text[index];
    }
    return true;
  }

  public void Send()
  {
    if (this.textBox.Text != string.Empty)
    {
      if (Game.Me.TournamentParticipant)
      {
        string name = this.radio.Pressed.Name;
        if (name == "wing" || name == "officers")
        {
          Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.no_chat_in_tournament%"));
          this.textBox.Text = string.Empty;
          return;
        }
      }
      this.textBox.Text = this.textBox.Text.Trim(' ');
      this.textBox.Text = this.TrimCommands(this.textBox.Text);
      if (!this.parser.DoIt(this.textBox.Text))
      {
        Game.ChatProtocol.SendText(this.textBox.Text, this.Selected);
        this.textBox.Text = string.Empty;
      }
      else
        this.textBox.Text = this.parser.command + " ";
      if (this.Current != null)
        this.Current.ViewNewestMessage();
      this.MakeViewActual();
    }
    this.IsFocused = false;
  }

  private string TrimCommands(string text)
  {
    string str = text;
    if (GUIChatNew.lastPressedTab != GUIChatNew.ChatChannel.Wing && GUIChatNew.lastPressedTab != GUIChatNew.ChatChannel.Squadron || !str.StartsWith("/fl ") && !str.StartsWith("/sl ") && (!str.StartsWith("/ol ") && !str.StartsWith("/fg ")) && (!str.StartsWith("/sg ") && !str.StartsWith("/og ")))
      return str;
    return str.Substring(4, str.Length - 4).Trim(' ');
  }

  public void StartWhisper(string name)
  {
    this.textBox.Text = "/w " + name;
    this.IsFocused = true;
  }

  public override void RecalculateAbsCoords()
  {
    GUIImageNew guiImageNew = this.Find<GUIImageNew>("background");
    this.Position = new float2((float) ((double) guiImageNew.Width / 2.0 + 10.0), (float) ((double) Screen.height - (double) guiImageNew.Height / 2.0 - 20.0));
    if (Screen.width < 1600 && GameLevel.Instance is RoomLevel)
    {
      UIRoot objectOfType = UnityEngine.Object.FindObjectOfType<UIRoot>();
      if ((UnityEngine.Object) objectOfType == (UnityEngine.Object) null)
        return;
      this.Position = new float2(this.Position.x, this.Position.y - 82f / objectOfType.pixelSizeAdjustment);
    }
    GuiTicker ticker = Game.Ticker;
    ExperienceBar experienceBar = Game.GUIManager.Find<ExperienceBar>();
    if (ticker != null && experienceBar != null)
      this.Position = new float2(this.Position.x, (float) ((double) Screen.height - (double) guiImageNew.Height / 2.0 - (double) ticker.SizeY - (double) experienceBar.Height - 2.0));
    base.RecalculateAbsCoords();
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    bool isRendered = this.Popup.IsRendered;
    bool flag = base.OnMouseUp(mousePosition, mouseKey);
    if (this.Popup.Parent != null && this.Popup.IsRendered && isRendered)
      this.Popup.IsRendered = false;
    return flag;
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (this.textBox.HandleKeyboardInput)
      return this.textBox.OnKeyDown(keyboardKey, action);
    if (action == Action.FocusChat)
    {
      this.IsFocused = true;
      return true;
    }
    if (action != Action.Reply)
      return false;
    this.Reply();
    return true;
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    return this.textBox.OnKeyUp(keyboardKey, action);
  }

  private void MakeViewActual()
  {
    this.layout.Clear();
    if (this.Current == null || this.Current.Count == 0)
      return;
    if (this.Current.Unread && this.Current.ViewingLast)
      this.Current.Unread = false;
    this.squadron.IsBlinking = Game.ChatStorage.Squadron.Unread;
    this.wing.IsBlinking = Game.ChatStorage.Wing.Unread;
    float num = 0.6f;
    int offset = this.Current.Offset;
    do
    {
      if (this.Current[offset] is ChatDataStorage.ExtendedTextMessage)
      {
        GUIChatExtendedTextMessage extendedTextMessage = new GUIChatExtendedTextMessage(this.Current[offset] as ChatDataStorage.ExtendedTextMessage, this.messagesArea.Width, this.ShowPrefix, true);
        if (this.minimize.IsPressed)
          extendedTextMessage.AlphaLevel = num;
        this.layout.PushFront((GUIPanel) extendedTextMessage);
      }
      else if (this.Current[offset] is ChatDataStorage.TextMessage)
      {
        GUIChatExtendedTextMessage extendedTextMessage = new GUIChatExtendedTextMessage(this.Current[offset] as ChatDataStorage.TextMessage, this.messagesArea.Width);
        if (this.minimize.IsPressed)
          extendedTextMessage.AlphaLevel = num;
        this.layout.PushFront((GUIPanel) extendedTextMessage);
      }
      else
      {
        GUIChatExtendedTextMessage extendedTextMessage = new GUIChatExtendedTextMessage(this.Current[offset], this.messagesArea.Width);
        if (this.minimize.IsPressed)
          extendedTextMessage.AlphaLevel = num;
        this.layout.PushFront((GUIPanel) extendedTextMessage);
      }
      num -= 0.035f;
      --offset;
    }
    while ((double) this.layout.Height < (double) this.messagesArea.Height && offset >= 0);
    this.layout.Position = new float2(0.0f, (float) ((double) this.messagesArea.Height / 2.0 - (double) this.layout.Height / 2.0));
    this.layout.RecalculateAbsCoords();
  }

  private void PressFleet()
  {
    this.fleet.IsPressed = true;
    this.fleet.IsRendered = true;
    this.open.IsRendered = false;
    this.system.IsRendered = false;
    this.culturePanel.IsRendered = true;
    this.globalToggler.IsRendered = true;
    this.SelectCurrent();
    if (this.culturePanel.Global)
      Game.ChatStorage.Global.ViewNewestMessage();
    if (!this.culturePanel.Local)
      return;
    Game.ChatStorage.Local.ViewNewestMessage();
  }

  private void PressOpen()
  {
    this.open.IsPressed = true;
    this.fleet.IsRendered = false;
    this.open.IsRendered = true;
    this.system.IsRendered = false;
    this.culturePanel.IsRendered = true;
    this.globalToggler.IsRendered = true;
    this.SelectCurrent();
    if (this.culturePanel.Global)
      Game.ChatStorage.Global.ViewNewestMessage();
    if (!this.culturePanel.Local)
      return;
    Game.ChatStorage.Local.ViewNewestMessage();
  }

  private void PressSystem()
  {
    this.system.IsPressed = true;
    this.fleet.IsRendered = false;
    this.open.IsRendered = false;
    this.system.IsRendered = true;
    this.culturePanel.IsRendered = true;
    this.globalToggler.IsRendered = true;
    this.SelectCurrent();
    if (this.culturePanel.Global)
      Game.ChatStorage.Global.ViewNewestMessage();
    if (!this.culturePanel.Local)
      return;
    Game.ChatStorage.Local.ViewNewestMessage();
  }

  private void SelectCurrent()
  {
    if (this.fleet.IsPressed || this.open.IsPressed || this.system.IsPressed)
    {
      if (this.culturePanel.Both)
        this.Current = Game.ChatStorage.GlobalAndLocal;
      else if (this.culturePanel.Local)
        this.Current = Game.ChatStorage.Local;
      else if (this.culturePanel.Global)
        this.Current = Game.ChatStorage.Global;
    }
    else if (this.squadron.IsPressed)
      this.Current = Game.ChatStorage.Squadron;
    else if (this.wing.IsPressed)
      this.Current = Game.ChatStorage.Wing;
    else if (this.combatLog.IsPressed)
      this.Current = Game.ChatStorage.CombatLog;
    this.MakeViewActual();
  }

  private void Reply()
  {
    if (!((UnityEngine.Object) Game.Instance != (UnityEngine.Object) null) || Game.ChatProtocol == null)
      return;
    this.StartWhisper(Game.ChatProtocol.lastUserWhispered);
  }

  private enum ChatChannel
  {
    Fleet,
    System,
    Open,
    Squadron,
    Wing,
    CombatLog,
  }

  private class Parser
  {
    private readonly List<KeyValuePair<string, Predicate<string[]>>> pool = new List<KeyValuePair<string, Predicate<string[]>>>();
    private GUIChatNew chat;
    public string command;

    public Parser(GUIChatNew chat)
    {
      this.chat = chat;
      this.Add(Commands.Help, new Predicate<string[]>(this.Help));
      this.Add(Commands.HelpExtended, new Predicate<string[]>(this.Help));
      this.Add(Commands.Syfy, new Predicate<string[]>(this.Syfy));
      this.Add(Commands.Scifi, new Predicate<string[]>(this.Syfy));
      this.Add(Commands.Whisper, new Predicate<string[]>(this.Whisper));
      this.Add(Commands.WhisperExtended, new Predicate<string[]>(this.Whisper));
      this.Add(Commands.Mute, new Predicate<string[]>(this.Mute));
      this.Add(Commands.MuteExtended, new Predicate<string[]>(this.Mute));
      this.Add(Commands.IgnoreExtended, new Predicate<string[]>(this.Mute));
      this.Add(Commands.MuteList, new Predicate<string[]>(this.ShowMuteList));
      this.Add(Commands.MuteListExtended, new Predicate<string[]>(this.ShowMuteList));
      this.Add(Commands.IgnoreListExtended, new Predicate<string[]>(this.ShowMuteList));
      this.Add(Commands.Invite, new Predicate<string[]>(this.Invite));
      this.Add(Commands.InviteExtended, new Predicate<string[]>(this.Invite));
      this.Add(Commands.Dismiss, new Predicate<string[]>(this.Dismiss));
      this.Add(Commands.DismissExtended, new Predicate<string[]>(this.Dismiss));
      this.Add(Commands.Global, new Predicate<string[]>(this.SwitchToGlobal));
      this.Add(Commands.GlobalExtended, new Predicate<string[]>(this.SwitchToGlobal));
      this.Add(Commands.Local, new Predicate<string[]>(this.SwitchToLocal));
      this.Add(Commands.LocalExtended, new Predicate<string[]>(this.SwitchToLocal));
      this.Add(Commands.ShowPrefix, new Predicate<string[]>(this.ShowPrefix));
      this.Add(Commands.ShowPrefixExtended, new Predicate<string[]>(this.ShowPrefix));
      this.Add(Commands.HidePrefix, new Predicate<string[]>(this.HidePrefix));
      this.Add(Commands.HidePrefixExtended, new Predicate<string[]>(this.HidePrefix));
      this.Add(Commands.DoNotDisturb, new Predicate<string[]>(this.DoNotDisturb));
      this.Add(Commands.Get(ChannelName.FleetLocal), new Predicate<string[]>(this.SendToFleetLocal));
      this.Add(Commands.GetEx(ChannelName.FleetLocal), new Predicate<string[]>(this.SendToFleetLocal));
      this.Add(Commands.Get(ChannelName.FleetGlobal), new Predicate<string[]>(this.SendToFleetGlobal));
      this.Add(Commands.GetEx(ChannelName.FleetGlobal), new Predicate<string[]>(this.SendToFleetGlobal));
      this.Add(Commands.Get(ChannelName.FleetLocal), new Predicate<string[]>(this.SwitchToFleetLocal));
      this.Add(Commands.GetEx(ChannelName.FleetLocal), new Predicate<string[]>(this.SwitchToFleetLocal));
      this.Add(Commands.Get(ChannelName.FleetGlobal), new Predicate<string[]>(this.SwitchToFleetGlobal));
      this.Add(Commands.GetEx(ChannelName.FleetGlobal), new Predicate<string[]>(this.SwitchToFleetGlobal));
      this.Add(Commands.Get(ChannelName.OpenLocal), new Predicate<string[]>(this.SendToOpenLocal));
      this.Add(Commands.GetEx(ChannelName.OpenLocal), new Predicate<string[]>(this.SendToOpenLocal));
      this.Add(Commands.Get(ChannelName.OpenGlobal), new Predicate<string[]>(this.SendToOpenGlobal));
      this.Add(Commands.GetEx(ChannelName.OpenGlobal), new Predicate<string[]>(this.SendToOpenGlobal));
      this.Add(Commands.Get(ChannelName.OpenLocal), new Predicate<string[]>(this.SwitchToOpenLocal));
      this.Add(Commands.GetEx(ChannelName.OpenLocal), new Predicate<string[]>(this.SwitchToOpenLocal));
      this.Add(Commands.Get(ChannelName.OpenGlobal), new Predicate<string[]>(this.SwitchToOpenGlobal));
      this.Add(Commands.GetEx(ChannelName.OpenGlobal), new Predicate<string[]>(this.SwitchToOpenGlobal));
      this.Add(Commands.Get(ChannelName.SystemLocal), new Predicate<string[]>(this.SendToSystemLocal));
      this.Add(Commands.GetEx(ChannelName.SystemLocal), new Predicate<string[]>(this.SendToSystemLocal));
      this.Add(Commands.Get(ChannelName.SystemGlobal), new Predicate<string[]>(this.SendToSystemGlobal));
      this.Add(Commands.GetEx(ChannelName.SystemGlobal), new Predicate<string[]>(this.SendToSystemGlobal));
      this.Add(Commands.Get(ChannelName.SystemLocal), new Predicate<string[]>(this.SwitchToSystemLocal));
      this.Add(Commands.GetEx(ChannelName.SystemLocal), new Predicate<string[]>(this.SwitchToSystemLocal));
      this.Add(Commands.Get(ChannelName.SystemGlobal), new Predicate<string[]>(this.SwitchToSystemGlobal));
      this.Add(Commands.GetEx(ChannelName.SystemGlobal), new Predicate<string[]>(this.SwitchToSystemGlobal));
      this.Add(Commands.Get(ChannelName.Officer), new Predicate<string[]>(this.SendToOfficer));
      this.Add(Commands.GetEx(ChannelName.Officer), new Predicate<string[]>(this.SendToOfficer));
      this.Add(Commands.Get(ChannelName.Officer), new Predicate<string[]>(this.SwitchToOfficer));
      this.Add(Commands.GetEx(ChannelName.Officer), new Predicate<string[]>(this.SwitchToOfficer));
    }

    private void Add(string commandName, Predicate<string[]> handler)
    {
      this.pool.Add(new KeyValuePair<string, Predicate<string[]>>(commandName.ToLower(), handler));
    }

    public bool DoIt(string inputText)
    {
      List<string> stringList = new List<string>((IEnumerable<string>) inputText.Split(' '));
      if (stringList.Count == 0)
        return false;
      this.command = stringList[0].ToLower();
      stringList.RemoveAt(0);
      string[] array = stringList.ToArray();
      using (List<KeyValuePair<string, Predicate<string[]>>>.Enumerator enumerator = this.pool.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, Predicate<string[]>> current = enumerator.Current;
          if (current.Key == this.command && current.Value(array))
            return true;
        }
      }
      return false;
    }

    private bool Help(string[] text)
    {
      if (text.Length != 0)
        return false;
      for (int index = 1; index <= 24; ++index)
        Game.ChatStorage.Help(BsgoLocalization.Get(string.Format("%$bgo.chat.help_line_{0}%", (object) index)));
      return true;
    }

    private bool DoNotDisturb(string[] text)
    {
      if (Game.Me.SocialState == PlayerSocialState.Normal)
      {
        Game.Me.SocialState = PlayerSocialState.DoNotDisturb;
        Game.ChatStorage.SystemSays(BsgoLocalization.Get("%$bgo.chat.message_dnd_on%"));
      }
      else
      {
        Game.Me.SocialState = PlayerSocialState.Normal;
        Game.ChatStorage.SystemSays(BsgoLocalization.Get("%$bgo.chat.message_dnd_off%"));
      }
      return true;
    }

    private bool Syfy(string[] text)
    {
      bool res;
      if (text.Length < 2 || text[0] != "ship" || (SettingProtocol.GetProtocol() == null || !Console.ParseBool(text[1], out res)))
        return false;
      SettingProtocol.GetProtocol().RequestSyfyShip(res);
      return true;
    }

    private bool Whisper(string[] text)
    {
      if (text.Length < 2)
        return false;
      string text1 = string.Empty;
      for (int index = 1; index < text.Length; ++index)
        text1 = text1 + text[index] + " ";
      text1.TrimEnd(' ');
      Game.ChatProtocol.SendTextWhisper(text[0], text1, ChannelName.OpenLocal);
      return true;
    }

    private bool SwitchToGlobal(string[] text)
    {
      if (text.Length > 0)
        return false;
      this.chat.Global = true;
      return true;
    }

    private bool SwitchToLocal(string[] text)
    {
      if (text.Length > 0)
        return false;
      this.chat.Global = false;
      return true;
    }

    private bool Mute(string[] text)
    {
      if (text.Length != 1)
        return false;
      Game.Me.Friends.ToggleIsIgnored(text[0]);
      return true;
    }

    private bool ShowMuteList(string[] text)
    {
      if (text.Length == 1 && text[0] == "clear")
      {
        Game.Me.Friends.ClearIgnore();
        return true;
      }
      if (text.Length != 0)
        return false;
      ICollection<Player> players = (ICollection<Player>) Game.Me.Friends.Ignore;
      if (players.Count == 0)
        Game.ChatStorage.Help(BsgoLocalization.Get("bgo.chat.ignore_list_empty"));
      else
        Game.ChatStorage.Help(BsgoLocalization.Get("bgo.chat.ignore_list"));
      foreach (Player player in (IEnumerable<Player>) players)
        Game.ChatStorage.Help(" - " + player.Name);
      return true;
    }

    private bool Invite(string[] text)
    {
      if (text.Length == 0)
      {
        if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null && SpaceLevel.GetLevel().GetPlayerTarget() != null)
        {
          SpaceObject playerTarget = SpaceLevel.GetLevel().GetPlayerTarget();
          if (playerTarget != null && playerTarget.IsPlayer && (playerTarget as PlayerShip).Player != null)
            Game.Me.Party.Invite((playerTarget as PlayerShip).Player);
          else
            Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.chat.message_invite_must_be_a_player%"));
        }
        return true;
      }
      if (text.Length != 1)
        return false;
      Player player = Game.Players.GetPlayer(text[0]);
      if (player == null)
        CommunityProtocol.GetProtocol().RequestPartyChatInvite(text[0]);
      else if (Game.Me.Party.Members.Count > 0 && !Game.Me.Party.IsLeader)
        Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.chat.message_invite_must_be_a_leader%"));
      else if (player != Game.Players.GetPlayer(Game.Me.ServerID))
        Game.Me.Party.Invite(Game.Players.GetPlayer(text[0]));
      return true;
    }

    private bool Dismiss(string[] text)
    {
      if (text.Length != 1)
        return false;
      if (Game.Players.GetPlayer(text[0]) == null)
        Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.chat.message_dismiss_user_not_exists%", (object) text[0]));
      else if (Game.Me.Party.Members.Count > 0 && !Game.Me.Party.IsLeader)
        Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.chat.message_dismiss_must_be_a_leader%"));
      else
        Game.Me.Party.Dismiss(Game.Players.GetPlayer(text[0]));
      return true;
    }

    private bool ShowPrefix(string[] text)
    {
      Debug.Log((object) "|| Show Prefix");
      if (text.Length != 0)
        return false;
      FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.ChatShowPrefix, (object) true, true));
      this.chat.MakeViewActual();
      return true;
    }

    private bool HidePrefix(string[] text)
    {
      Debug.Log((object) "|| Hide Prefix");
      if (text.Length != 0)
        return false;
      FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.ChatShowPrefix, (object) false, true));
      this.chat.MakeViewActual();
      return true;
    }

    private bool SendToFleetLocal(string[] text)
    {
      if (text.Length == 0)
        return false;
      string text1 = string.Empty;
      foreach (string str in text)
        text1 = text1 + str + " ";
      text1.TrimEnd(' ');
      Game.ChatProtocol.SendText(text1, ChannelName.FleetLocal);
      return true;
    }

    private bool SendToFleetGlobal(string[] text)
    {
      if (text.Length == 0)
        return false;
      string text1 = string.Empty;
      foreach (string str in text)
        text1 = text1 + str + " ";
      text1.TrimEnd(' ');
      Game.ChatProtocol.SendText(text1, ChannelName.FleetGlobal);
      return true;
    }

    private bool SwitchToFleetLocal(string[] text)
    {
      if (text.Length > 0)
        return false;
      this.chat.PressFleet();
      this.chat.Global = false;
      return true;
    }

    private bool SwitchToFleetGlobal(string[] text)
    {
      if (text.Length > 0)
        return false;
      this.chat.PressFleet();
      this.chat.Global = true;
      return true;
    }

    private bool SendToOpenLocal(string[] text)
    {
      if (text.Length == 0)
        return false;
      string text1 = "(" + (object) (Game.Me.Faction != Faction.Colonial ? 2 : 1) + ")";
      foreach (string str in text)
        text1 = text1 + str + " ";
      text1.TrimEnd(' ');
      Game.ChatProtocol.SendText(text1, ChannelName.OpenLocal);
      return true;
    }

    private bool SendToOpenGlobal(string[] text)
    {
      if (text.Length == 0)
        return false;
      string text1 = "(" + (object) (Game.Me.Faction != Faction.Colonial ? 2 : 1) + ")";
      foreach (string str in text)
        text1 = text1 + str + " ";
      text1.TrimEnd(' ');
      Game.ChatProtocol.SendText(text1, ChannelName.OpenGlobal);
      return true;
    }

    private bool SwitchToOpenLocal(string[] text)
    {
      if (text.Length > 0)
        return false;
      this.chat.PressOpen();
      this.chat.Global = false;
      return true;
    }

    private bool SwitchToOpenGlobal(string[] text)
    {
      if (text.Length > 0)
        return false;
      this.chat.PressOpen();
      this.chat.Global = true;
      return true;
    }

    private bool SendToSystemLocal(string[] text)
    {
      if (text.Length == 0)
        return false;
      string text1 = string.Empty;
      foreach (string str in text)
        text1 = text1 + str + " ";
      text1.TrimEnd(' ');
      Game.ChatProtocol.SendText(text1, ChannelName.SystemLocal);
      return true;
    }

    private bool SendToSystemGlobal(string[] text)
    {
      if (text.Length == 0)
        return false;
      string text1 = string.Empty;
      foreach (string str in text)
        text1 = text1 + str + " ";
      text1.TrimEnd(' ');
      Game.ChatProtocol.SendText(text1, ChannelName.SystemGlobal);
      return true;
    }

    private bool SwitchToSystemLocal(string[] text)
    {
      if (text.Length > 0)
        return false;
      this.chat.PressSystem();
      this.chat.Global = false;
      return true;
    }

    private bool SwitchToSystemGlobal(string[] text)
    {
      if (text.Length > 0)
        return false;
      this.chat.PressSystem();
      this.chat.Global = true;
      return true;
    }

    private bool SendToOfficer(string[] text)
    {
      if (text.Length == 0)
        return false;
      string text1 = string.Empty;
      foreach (string str in text)
        text1 = text1 + str + " ";
      text1.TrimEnd(' ');
      Game.ChatProtocol.SendText(text1, ChannelName.Officer);
      return true;
    }

    private bool SwitchToOfficer(string[] text)
    {
      if (text.Length > 0)
        return false;
      this.chat.PressSystem();
      this.chat.Global = false;
      return true;
    }
  }
}
