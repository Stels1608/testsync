﻿// Decompiled with JetBrains decompiler
// Type: BSGOConfig
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;

public class BSGOConfig
{
  public static string BSGO_CONFIG_PATH = "bsgo.config";
  public static BSGOConfig instance = new BSGOConfig();
  private BSGOConfig.BSGOConfigReader reader;

  public static string ContentDBPath
  {
    get
    {
      if (BSGOConfig.instance != null && BSGOConfig.instance.reader != null)
        return BSGOConfig.instance.reader.contentDBPath;
      return string.Empty;
    }
    set
    {
      if (BSGOConfig.instance == null || BSGOConfig.instance.reader == null)
        return;
      BSGOConfig.instance.reader.contentDBPath = value;
      Stream stream = (Stream) File.Open(BSGOConfig.BSGO_CONFIG_PATH, FileMode.Open);
      new XmlSerializer(typeof (BSGOConfig.BSGOConfigReader)).Serialize(stream, (object) BSGOConfig.instance.reader);
      stream.Close();
    }
  }

  private BSGOConfig()
  {
    try
    {
      Stream stream = (Stream) File.Open(BSGOConfig.BSGO_CONFIG_PATH, FileMode.Open);
      this.reader = (BSGOConfig.BSGOConfigReader) new XmlSerializer(typeof (BSGOConfig.BSGOConfigReader)).Deserialize(stream);
      stream.Close();
    }
    catch (IOException ex)
    {
      Log.Warning((object) ex.ToString());
    }
  }

  [Serializable]
  public class BSGOConfigReader : ISerializable
  {
    public string contentDBPath;

    public BSGOConfigReader()
    {
    }

    public BSGOConfigReader(SerializationInfo info, StreamingContext ctxt)
    {
      this.contentDBPath = (string) info.GetValue("contentdbpath", typeof (string));
    }

    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
      info.AddValue("contentdbpath", (object) this.contentDBPath);
    }
  }
}
