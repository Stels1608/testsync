﻿// Decompiled with JetBrains decompiler
// Type: MotionAnalyzer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class MotionAnalyzer : IMotionAnalyzer
{
  public bool alsoUseBackwards;
  public bool fixFootSkating;
  [HideInInspector]
  public LegCycleData[] m_cycles;
  [HideInInspector]
  public int m_samples;
  [HideInInspector]
  public Vector3 m_cycleDirection;
  [HideInInspector]
  public float m_cycleDistance;
  [HideInInspector]
  public float m_cycleDuration;
  [HideInInspector]
  public float m_cycleSpeed;
  public float nativeSpeed;
  [HideInInspector]
  public float m_cycleOffset;
  [HideInInspector]
  public Vector3 graphMin;
  [HideInInspector]
  public Vector3 graphMax;
  [HideInInspector]
  public GameObject gameObject;
  [HideInInspector]
  public int legs;
  [HideInInspector]
  public LegController legC;
  [HideInInspector]
  public Material lineMaterial;

  public override LegCycleData[] cycles
  {
    get
    {
      return this.m_cycles;
    }
  }

  public override int samples
  {
    get
    {
      return this.m_samples;
    }
  }

  public override Vector3 cycleDirection
  {
    get
    {
      return this.m_cycleDirection;
    }
  }

  public override float cycleDistance
  {
    get
    {
      return this.m_cycleDistance;
    }
  }

  public override Vector3 cycleVector
  {
    get
    {
      return this.m_cycleDirection * this.m_cycleDistance;
    }
  }

  public override float cycleDuration
  {
    get
    {
      return this.m_cycleDuration;
    }
  }

  public override float cycleSpeed
  {
    get
    {
      return this.m_cycleSpeed;
    }
  }

  public override Vector3 cycleVelocity
  {
    get
    {
      return this.m_cycleDirection * this.m_cycleSpeed;
    }
  }

  public override float cycleOffset
  {
    get
    {
      return this.m_cycleOffset;
    }
    set
    {
      this.m_cycleOffset = value;
    }
  }

  private float FindContactTime(LegCycleData data, bool useToe, int searchDirection, float yRange, float threshold)
  {
    int num1 = 5;
    float num2 = 0.0f;
    int num3 = data.stanceIndex;
    int num4 = 0;
    while (num4 < this.samples && num4 > -this.samples)
    {
      int[] numArray1 = new int[3];
      float[] numArray2 = new float[3];
      for (int index = 0; index < 3; ++index)
      {
        numArray1[index] = Util.Mod(num4 + data.stanceIndex - num1 + num1 * index, this.samples);
        numArray2[index] = !useToe ? data.samples[numArray1[index]].heel.y : data.samples[numArray1[index]].toetip.y;
      }
      float num5 = Mathf.Atan((float) (((double) numArray2[2] - (double) numArray2[1]) * 10.0) / yRange) - Mathf.Atan((float) (((double) numArray2[1] - (double) numArray2[0]) * 10.0) / yRange);
      if ((double) numArray2[1] > (double) this.legC.groundPlaneHeight && (double) num5 > (double) num2 && (double) Mathf.Sign(numArray2[2] - numArray2[0]) == (double) Mathf.Sign((float) searchDirection))
      {
        num2 = num5;
        num3 = numArray1[1];
      }
      if ((double) numArray2[1] <= (double) this.legC.groundPlaneHeight + (double) yRange * (double) threshold)
        num4 += searchDirection;
      else
        break;
    }
    return this.GetTimeFromIndex(Util.Mod(num3 - data.stanceIndex, this.samples));
  }

  private float FindSwingChangeTime(LegCycleData data, int searchDirection, float threshold)
  {
    int num1 = this.samples / 5;
    float num2 = 0.0f;
    int num3 = 0;
    while (num3 < this.samples && num3 > -this.samples)
    {
      int[] numArray1 = new int[3];
      float[] numArray2 = new float[3];
      for (int index = 0; index < 3; ++index)
      {
        numArray1[index] = Util.Mod(num3 + data.stanceIndex - num1 + num1 * index, this.samples);
        numArray2[index] = Vector3.Dot(data.samples[numArray1[index]].footBase, data.cycleDirection);
      }
      float num4 = numArray2[2] - numArray2[0];
      if (num3 == 0)
        num2 = num4;
      if ((double) Mathf.Abs((num4 - num2) / num2) > (double) threshold)
        return this.GetTimeFromIndex(Util.Mod(numArray1[1] - data.stanceIndex, this.samples));
      num3 += searchDirection;
    }
    return Util.Mod((float) searchDirection * -0.01f);
  }

  private float GetFootGrounding(int leg, float time)
  {
    if ((double) time <= (double) this.cycles[leg].liftTime || (double) time >= (double) this.cycles[leg].landTime)
      return 0.0f;
    if ((double) time >= (double) this.cycles[leg].postliftTime && (double) time <= (double) this.cycles[leg].prelandTime)
      return 1f;
    return (double) time >= (double) this.cycles[leg].postliftTime ? (float) (1.0 - ((double) time - (double) this.cycles[leg].prelandTime) / ((double) this.cycles[leg].landTime - (double) this.cycles[leg].prelandTime)) : (float) (((double) time - (double) this.cycles[leg].liftTime) / ((double) this.cycles[leg].postliftTime - (double) this.cycles[leg].liftTime));
  }

  private float GetFootGroundingOrig(int leg, float time)
  {
    if ((double) time <= (double) this.cycles[leg].liftTime || (double) time >= (double) this.cycles[leg].landTime)
      return 0.0f;
    if ((double) time >= (double) this.cycles[leg].liftoffTime && (double) time <= (double) this.cycles[leg].strikeTime)
      return 1f;
    return (double) time >= (double) this.cycles[leg].liftoffTime ? (float) (1.0 - ((double) time - (double) this.cycles[leg].strikeTime) / ((double) this.cycles[leg].landTime - (double) this.cycles[leg].strikeTime)) : (float) (((double) time - (double) this.cycles[leg].liftTime) / ((double) this.cycles[leg].liftoffTime - (double) this.cycles[leg].liftTime));
  }

  private int GetIndexFromTime(float time)
  {
    return Util.Mod((int) ((double) time * (double) this.samples + 0.5), this.samples);
  }

  private float GetTimeFromIndex(int index)
  {
    return (float) index * 1f / (float) this.samples;
  }

  private Vector3 FootPositionNormalized(LegCycleSample s)
  {
    return s.footBaseNormalized;
  }

  private Vector3 FootPosition(LegCycleSample s)
  {
    return s.footBase;
  }

  private float Balance(LegCycleSample s)
  {
    return s.balance;
  }

  private Vector3 GetVector3AtTime(int leg, float flightTime, MotionAnalyzer.GetVector3Member get)
  {
    flightTime = Mathf.Clamp01(flightTime);
    int num1 = (int) ((double) flightTime * (double) this.samples);
    float num2 = flightTime * (float) this.samples - (float) num1;
    if (num1 >= this.samples - 1)
    {
      num1 = this.samples - 1;
      num2 = 0.0f;
    }
    int index = Util.Mod(num1 + this.cycles[leg].stanceIndex, this.samples);
    return get(this.cycles[leg].samples[index]) * (1f - num2) + get(this.cycles[leg].samples[Util.Mod(index + 1, this.samples)]) * num2;
  }

  private float GetFloatAtTime(int leg, float flightTime, MotionAnalyzer.GetFloatMember get)
  {
    flightTime = Mathf.Clamp01(flightTime);
    int num1 = (int) ((double) flightTime * (double) this.samples);
    float num2 = flightTime * (float) this.samples - (float) num1;
    if (num1 >= this.samples - 1)
    {
      num1 = this.samples - 1;
      num2 = 0.0f;
    }
    int index = Util.Mod(num1 + this.cycles[leg].stanceIndex, this.samples);
    return (float) ((double) get(this.cycles[leg].samples[index]) * (1.0 - (double) num2) + (double) get(this.cycles[leg].samples[Util.Mod(index + 1, this.samples)]) * (double) num2);
  }

  public override Vector3 GetFlightFootPosition(int leg, float flightTime, int phase)
  {
    if (this.motionType != MotionType.WalkCycle)
    {
      if (phase == 0)
        return Vector3.zero;
      if (phase == 1)
        return (float) (-(double) Mathf.Cos(flightTime * 3.141593f) / 2.0 + 0.5) * Vector3.forward;
      if (phase == 2)
        return Vector3.forward;
    }
    float num1 = phase != 0 ? (phase != 1 ? Mathf.Lerp(this.cycles[leg].strikeTime, 1f, flightTime) : Mathf.Lerp(this.cycles[leg].liftoffTime, this.cycles[leg].strikeTime, flightTime)) : Mathf.Lerp(0.0f, this.cycles[leg].liftoffTime, flightTime);
    int num2 = (int) ((double) num1 * (double) this.samples);
    float num3 = num1 * (float) this.samples - (float) num2;
    if (num2 >= this.samples - 1)
    {
      num2 = this.samples - 1;
      num3 = 0.0f;
    }
    int index = Util.Mod(num2 + this.cycles[leg].stanceIndex, this.samples);
    return this.cycles[leg].samples[index].footBaseNormalized * (1f - num3) + this.cycles[leg].samples[Util.Mod(index + 1, this.samples)].footBaseNormalized * num3;
  }

  public static Vector3 GetHeelOffset(Transform ankleT, Vector3 ankleHeelVector, Transform toeT, Vector3 toeToetipVector, Vector3 stanceFootVector, Quaternion footBaseRotation)
  {
    Vector3 vector3_1 = ankleT.localToWorldMatrix.MultiplyPoint(ankleHeelVector);
    Vector3 vector3_2 = toeT.localToWorldMatrix.MultiplyPoint(toeToetipVector);
    return MotionAnalyzer.GetFootBalance((Quaternion.Inverse(footBaseRotation) * vector3_1).y, (Quaternion.Inverse(footBaseRotation) * vector3_2).y, stanceFootVector.magnitude) * (footBaseRotation * stanceFootVector + vector3_1 - vector3_2);
  }

  public static Vector3 GetAnklePosition(Transform ankleT, Vector3 ankleHeelVector, Transform toeT, Vector3 toeToetipVector, Vector3 stanceFootVector, Vector3 footBasePosition, Quaternion footBaseRotation)
  {
    Vector3 heelOffset = MotionAnalyzer.GetHeelOffset(ankleT, ankleHeelVector, toeT, toeToetipVector, stanceFootVector, footBaseRotation);
    return footBasePosition + heelOffset + ankleT.localToWorldMatrix.MultiplyVector(ankleHeelVector * -1f);
  }

  public static float GetFootBalance(float heelElevation, float toeElevation, float footLength)
  {
    return (float) ((double) Mathf.Atan((float) (((double) heelElevation - (double) toeElevation) / (double) footLength * 20.0)) / 3.14159274101257 + 0.5);
  }

  private void FindCycleAxis(int leg)
  {
    this.cycles[leg].cycleCenter = Vector3.zero;
    for (int index = 0; index < this.samples; ++index)
    {
      LegCycleSample legCycleSample = this.cycles[leg].samples[index];
      this.cycles[leg].cycleCenter += Util.ProjectOntoPlane(legCycleSample.middle, Vector3.up);
    }
    this.cycles[leg].cycleCenter /= (float) this.samples;
    Vector3 vector3_1 = this.cycles[leg].cycleCenter;
    float num1 = 0.0f;
    for (int index = 0; index < this.samples; ++index)
    {
      Vector3 vector3_2 = Util.ProjectOntoPlane(this.cycles[leg].samples[index].middle, Vector3.up);
      float magnitude = (vector3_2 - this.cycles[leg].cycleCenter).magnitude;
      if ((double) magnitude > (double) num1)
      {
        vector3_1 = vector3_2;
        num1 = magnitude;
      }
    }
    Vector3 vector3_3 = vector3_1;
    float num2 = 0.0f;
    for (int index = 0; index < this.samples; ++index)
    {
      Vector3 vector3_2 = Util.ProjectOntoPlane(this.cycles[leg].samples[index].middle, Vector3.up);
      float magnitude = (vector3_2 - vector3_1).magnitude;
      if ((double) magnitude > (double) num2)
      {
        vector3_3 = vector3_2;
        num2 = magnitude;
      }
    }
    this.cycles[leg].cycleDirection = (vector3_3 - vector3_1).normalized;
    this.cycles[leg].cycleScaling = (vector3_3 - vector3_1).magnitude;
  }

  public override void Analyze(GameObject o)
  {
    Debug.Log((object) "Starting analysis");
    this.gameObject = o;
    this.name = this.animation.name;
    this.m_samples = 50;
    this.legC = this.gameObject.GetComponent(typeof (LegController)) as LegController;
    this.legs = this.legC.legs.Length;
    this.m_cycles = new LegCycleData[this.legs];
    for (int index1 = 0; index1 < this.legs; ++index1)
    {
      this.cycles[index1] = new LegCycleData();
      this.cycles[index1].samples = new LegCycleSample[this.samples + 1];
      for (int index2 = 0; index2 < this.samples + 1; ++index2)
        this.cycles[index1].samples[index2] = new LegCycleSample();
      this.cycles[index1].debugInfo = new CycleDebugInfo();
    }
    this.graphMin = new Vector3(0.0f, 1000f, 1000f);
    this.graphMax = new Vector3(0.0f, -1000f, -1000f);
    for (int leg = 0; leg < this.legs; ++leg)
    {
      Transform t1 = this.legC.legs[leg].ankle;
      Transform t2 = this.legC.legs[leg].toe;
      float a1 = 1000f;
      float a2 = -1000f;
      float a3 = 1000f;
      float num1 = -1000f;
      for (int index = 0; index < this.samples + 1; ++index)
      {
        LegCycleSample legCycleSample = this.cycles[leg].samples[index];
        this.animation.SampleAnimation(this.gameObject, (float) index * 1f / (float) this.samples * this.animation.length);
        legCycleSample.ankleMatrix = Util.RelativeMatrix(t1, this.gameObject.transform);
        legCycleSample.toeMatrix = Util.RelativeMatrix(t2, this.gameObject.transform);
        legCycleSample.heel = legCycleSample.ankleMatrix.MultiplyPoint(this.legC.legs[leg].ankleHeelVector);
        legCycleSample.toetip = legCycleSample.toeMatrix.MultiplyPoint(this.legC.legs[leg].toeToetipVector);
        legCycleSample.middle = (legCycleSample.heel + legCycleSample.toetip) / 2f;
        legCycleSample.balance = MotionAnalyzer.GetFootBalance(legCycleSample.heel.y, legCycleSample.toetip.y, this.legC.legs[leg].footLength);
        a1 = Mathf.Min(a1, legCycleSample.heel.y);
        a3 = Mathf.Min(a3, legCycleSample.toetip.y);
        a2 = Mathf.Max(a2, legCycleSample.heel.y);
        num1 = Mathf.Max(num1, legCycleSample.toetip.y);
      }
      float yRange = Mathf.Max(a2 - a1, num1 - a3);
      if (this.motionType == MotionType.WalkCycle)
      {
        this.FindCycleAxis(leg);
        float num2 = float.PositiveInfinity;
        for (int index = 0; index < this.samples + 1; ++index)
        {
          LegCycleSample legCycleSample = this.cycles[leg].samples[index];
          float num3 = (float) ((double) Mathf.Max(legCycleSample.heel.y, legCycleSample.toetip.y) / (double) yRange + (double) Mathf.Abs(Util.ProjectOntoPlane(legCycleSample.middle - this.cycles[leg].cycleCenter, Vector3.up).magnitude) / (double) this.cycles[leg].cycleScaling);
          if ((double) num3 < (double) num2)
          {
            this.cycles[leg].stanceIndex = index;
            num2 = num3;
          }
        }
      }
      else
      {
        this.cycles[leg].cycleDirection = Vector3.forward;
        this.cycles[leg].cycleScaling = 0.0f;
        this.cycles[leg].stanceIndex = 0;
      }
      this.cycles[leg].stanceTime = this.GetTimeFromIndex(this.cycles[leg].stanceIndex);
      LegCycleSample legCycleSample1 = this.cycles[leg].samples[this.cycles[leg].stanceIndex];
      this.animation.SampleAnimation(this.gameObject, this.cycles[leg].stanceTime * this.animation.length);
      this.cycles[leg].heelToetipVector = legCycleSample1.toeMatrix.MultiplyPoint(this.legC.legs[leg].toeToetipVector) - legCycleSample1.ankleMatrix.MultiplyPoint(this.legC.legs[leg].ankleHeelVector);
      this.cycles[leg].heelToetipVector = Util.ProjectOntoPlane(this.cycles[leg].heelToetipVector, Vector3.up);
      this.cycles[leg].heelToetipVector = this.cycles[leg].heelToetipVector.normalized * this.legC.legs[leg].footLength;
      for (int index = 0; index < this.samples + 1; ++index)
      {
        LegCycleSample legCycleSample2 = this.cycles[leg].samples[index];
        legCycleSample2.footBase = legCycleSample2.heel * (1f - legCycleSample2.balance) + (legCycleSample2.toetip - this.cycles[leg].heelToetipVector) * legCycleSample2.balance;
      }
      this.cycles[leg].stancePosition = legCycleSample1.footBase;
      this.cycles[leg].stancePosition.y = this.legC.groundPlaneHeight;
      if (this.motionType == MotionType.WalkCycle)
      {
        float contactTime1 = this.FindContactTime(this.cycles[leg], false, 1, yRange, 0.1f);
        this.cycles[leg].debugInfo.ankleLiftTime = contactTime1;
        float contactTime2 = this.FindContactTime(this.cycles[leg], true, 1, yRange, 0.1f);
        this.cycles[leg].debugInfo.toeLiftTime = contactTime2;
        if ((double) contactTime1 < (double) contactTime2)
        {
          this.cycles[leg].liftTime = contactTime1;
          this.cycles[leg].liftoffTime = contactTime2;
        }
        else
        {
          this.cycles[leg].liftTime = contactTime2;
          this.cycles[leg].liftoffTime = contactTime1;
        }
        float swingChangeTime1 = this.FindSwingChangeTime(this.cycles[leg], 1, 0.5f);
        this.cycles[leg].debugInfo.footLiftTime = swingChangeTime1;
        if ((double) this.cycles[leg].liftoffTime > (double) swingChangeTime1)
        {
          this.cycles[leg].liftoffTime = swingChangeTime1;
          if ((double) this.cycles[leg].liftTime > (double) this.cycles[leg].liftoffTime)
            this.cycles[leg].liftTime = this.cycles[leg].liftoffTime;
        }
        float contactTime3 = this.FindContactTime(this.cycles[leg], false, -1, yRange, 0.1f);
        float contactTime4 = this.FindContactTime(this.cycles[leg], true, -1, yRange, 0.1f);
        if ((double) contactTime3 < (double) contactTime4)
        {
          this.cycles[leg].strikeTime = contactTime3;
          this.cycles[leg].landTime = contactTime4;
        }
        else
        {
          this.cycles[leg].strikeTime = contactTime4;
          this.cycles[leg].landTime = contactTime3;
        }
        float swingChangeTime2 = this.FindSwingChangeTime(this.cycles[leg], -1, 0.5f);
        this.cycles[leg].debugInfo.footLandTime = swingChangeTime2;
        if ((double) this.cycles[leg].strikeTime < (double) swingChangeTime2)
        {
          this.cycles[leg].strikeTime = swingChangeTime2;
          if ((double) this.cycles[leg].landTime < (double) this.cycles[leg].strikeTime)
            this.cycles[leg].landTime = this.cycles[leg].strikeTime;
        }
        float num2 = 0.2f;
        this.cycles[leg].postliftTime = this.cycles[leg].liftoffTime;
        if ((double) this.cycles[leg].postliftTime < (double) this.cycles[leg].liftTime + (double) num2)
          this.cycles[leg].postliftTime = this.cycles[leg].liftTime + num2;
        this.cycles[leg].prelandTime = this.cycles[leg].strikeTime;
        if ((double) this.cycles[leg].prelandTime > (double) this.cycles[leg].landTime - (double) num2)
          this.cycles[leg].prelandTime = this.cycles[leg].landTime - num2;
        Vector3 vector3 = this.cycles[leg].samples[this.GetIndexFromTime(Util.Mod(this.cycles[leg].liftoffTime + this.cycles[leg].stanceTime))].footBase - this.cycles[leg].samples[this.GetIndexFromTime(Util.Mod(this.cycles[leg].strikeTime + this.cycles[leg].stanceTime))].footBase;
        vector3.y = 0.0f;
        this.cycles[leg].cycleDistance = vector3.magnitude / (float) ((double) this.cycles[leg].liftoffTime - (double) this.cycles[leg].strikeTime + 1.0);
        this.cycles[leg].cycleDirection = -vector3.normalized;
      }
      else
      {
        this.cycles[leg].cycleDirection = Vector3.zero;
        this.cycles[leg].cycleDistance = 0.0f;
      }
      this.graphMax.y = Mathf.Max(this.graphMax.y, Mathf.Max(a2, num1));
    }
    this.m_cycleDistance = 0.0f;
    this.m_cycleDirection = Vector3.zero;
    for (int index = 0; index < this.legs; ++index)
    {
      this.m_cycleDistance += this.cycles[index].cycleDistance;
      this.m_cycleDirection += this.cycles[index].cycleDirection;
      Debug.Log((object) ("Cycle direction of leg " + (object) index + " is " + (object) this.cycles[index].cycleDirection + " with step distance " + (object) this.cycles[index].cycleDistance));
    }
    this.m_cycleDistance /= (float) this.legs;
    this.m_cycleDirection /= (float) this.legs;
    this.m_cycleDuration = this.animation.length;
    this.m_cycleSpeed = this.cycleDistance / this.cycleDuration;
    Debug.Log((object) ("Overall cycle direction is " + (object) this.m_cycleDirection + " with step distance " + (object) this.m_cycleDistance + " and speed " + (object) this.m_cycleSpeed));
    this.nativeSpeed = this.m_cycleSpeed * this.gameObject.transform.localScale.x;
    for (int index1 = 0; index1 < this.legs; ++index1)
    {
      if (this.motionType == MotionType.WalkCycle)
      {
        for (int index2 = 0; index2 < this.samples; ++index2)
        {
          int index3 = Util.Mod(index2 + this.cycles[index1].stanceIndex, this.samples);
          LegCycleSample legCycleSample = this.cycles[index1].samples[index3];
          float timeFromIndex = this.GetTimeFromIndex(index2);
          legCycleSample.footBaseNormalized = legCycleSample.footBase;
          if (this.fixFootSkating)
          {
            Vector3 vector3 = -this.cycles[index1].cycleDistance * this.cycles[index1].cycleDirection * (timeFromIndex - this.cycles[index1].liftoffTime) + this.cycles[index1].samples[this.GetIndexFromTime(this.cycles[index1].liftoffTime + this.cycles[index1].stanceTime)].footBase;
            legCycleSample.footBaseNormalized = legCycleSample.footBaseNormalized - vector3;
            if (this.cycles[index1].cycleDirection != Vector3.zero)
              legCycleSample.footBaseNormalized = Quaternion.Inverse(Quaternion.LookRotation(this.cycles[index1].cycleDirection)) * legCycleSample.footBaseNormalized;
            legCycleSample.footBaseNormalized.z /= this.cycles[index1].cycleDistance;
            if ((double) timeFromIndex <= (double) this.cycles[index1].liftoffTime)
              legCycleSample.footBaseNormalized.z = 0.0f;
            if ((double) timeFromIndex >= (double) this.cycles[index1].strikeTime)
              legCycleSample.footBaseNormalized.z = 1f;
            legCycleSample.footBaseNormalized.y = legCycleSample.footBase.y - this.legC.groundPlaneHeight;
          }
          else
          {
            Vector3 vector3 = -this.m_cycleDistance * this.m_cycleDirection * (timeFromIndex - this.cycles[index1].liftoffTime * 0.0f) + this.cycles[index1].samples[this.GetIndexFromTime(this.cycles[index1].liftoffTime * 0.0f + this.cycles[index1].stanceTime)].footBase;
            legCycleSample.footBaseNormalized = legCycleSample.footBaseNormalized - vector3;
            if (this.cycles[index1].cycleDirection != Vector3.zero)
              legCycleSample.footBaseNormalized = Quaternion.Inverse(Quaternion.LookRotation(this.m_cycleDirection)) * legCycleSample.footBaseNormalized;
            legCycleSample.footBaseNormalized.z /= this.m_cycleDistance;
            legCycleSample.footBaseNormalized.y = legCycleSample.footBase.y - this.legC.groundPlaneHeight;
          }
        }
        this.cycles[index1].samples[this.samples] = this.cycles[index1].samples[0];
      }
      else
      {
        for (int index2 = 0; index2 < this.samples; ++index2)
        {
          int index3 = Util.Mod(index2 + this.cycles[index1].stanceIndex, this.samples);
          LegCycleSample legCycleSample = this.cycles[index1].samples[index3];
          legCycleSample.footBaseNormalized = legCycleSample.footBase - this.cycles[index1].stancePosition;
        }
      }
    }
    for (int index1 = 0; index1 < this.legs; ++index1)
    {
      float num1 = Vector3.Dot(this.cycles[index1].heelToetipVector, this.cycleDirection);
      for (int index2 = 0; index2 < this.samples; ++index2)
      {
        float num2 = Vector3.Dot(this.cycles[index1].samples[index2].footBase, this.cycleDirection);
        if ((double) num2 < (double) this.graphMin.z)
          this.graphMin.z = num2;
        if ((double) num2 > (double) this.graphMax.z)
          this.graphMax.z = num2;
        if ((double) num2 + (double) num1 < (double) this.graphMin.z)
          this.graphMin.z = num2 + num1;
        if ((double) num2 + (double) num1 > (double) this.graphMax.z)
          this.graphMax.z = num2 + num1;
      }
    }
    this.graphMin.y = this.legC.groundPlaneHeight;
  }

  private void CreateLineMaterial()
  {
    if ((bool) ((UnityEngine.Object) this.lineMaterial))
      return;
    this.lineMaterial = new Material("Shader \"Lines/Colored Blended\" {SubShader { Pass {     BindChannels { Bind \"Color\",color }     Blend SrcAlpha OneMinusSrcAlpha     ZWrite Off Cull Off Fog { Mode Off } } } }");
    this.lineMaterial.hideFlags = HideFlags.HideAndDontSave;
    this.lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
  }

  private void DrawLine(Vector3 a, Vector3 b, Color color)
  {
    GL.Color(color);
    GL.Vertex(a);
    GL.Vertex(b);
  }

  private void DrawRay(Vector3 a, Vector3 b, Color color)
  {
    this.DrawLine(a, a + b, color);
  }

  private void DrawDiamond(Vector3 a, float size, Color color)
  {
    Vector3 up = Camera.main.transform.up;
    Vector3 right = Camera.main.transform.right;
    GL.Color(color);
    GL.Vertex(a + up * size);
    GL.Vertex(a + right * size);
    GL.Vertex(a - up * size);
    GL.Vertex(a - right * size);
  }

  public void RenderGraph(MotionAnalyzerDrawOptions opt)
  {
    this.CreateLineMaterial();
    this.lineMaterial.SetPass(0);
    if (opt.drawAllFeet)
    {
      for (int currentLeg = 0; currentLeg < this.legs; ++currentLeg)
        this.RenderGraphAll(opt, currentLeg);
    }
    else
      this.RenderGraphAll(opt, opt.currentLeg);
  }

  public void RenderGraphAll(MotionAnalyzerDrawOptions opt, int currentLeg)
  {
    bool flag1 = opt.drawAllFeet;
    bool flag2 = !flag1 || currentLeg == 0;
    int leg = currentLeg;
    Transform t1 = this.legC.legs[leg].ankle;
    Transform t2 = this.legC.legs[leg].toe;
    Vector3 vector3_1 = this.graphMax - this.graphMin;
    Matrix4x4 localToWorldMatrix = this.gameObject.transform.localToWorldMatrix;
    float time1 = Util.Mod(this.gameObject.GetComponent<Animation>()[this.animation.name].normalizedTime);
    int indexFromTime = this.GetIndexFromTime(time1);
    float timeFromIndex1 = this.GetTimeFromIndex(indexFromTime);
    float num1 = this.gameObject.transform.localScale.z;
    float num2 = this.cycleDistance * num1;
    float size = (float) ((double) vector3_1.z * (double) num1 * 0.0299999993294477);
    Color c1 = new Color(0.7f, 0.7f, 0.7f, 1f);
    Color color1 = new Color(0.8f, 0.0f, 0.0f, 1f);
    Color color2 = new Color(0.0f, 0.7f, 0.0f, 1f);
    Color color3 = new Color(0.0f, 0.0f, 0.0f, 1f);
    Color color4 = new Color(0.7f, 0.7f, 0.7f, 1f);
    if (flag1)
    {
      color1 = this.legC.legs[leg].debugColor;
      color2 = this.legC.legs[leg].debugColor;
      color3 = this.legC.legs[leg].debugColor * 0.5f + Color.black * 0.5f;
      color4 = this.legC.legs[leg].debugColor * 0.5f + Color.white * 0.5f;
    }
    Color color5 = color3;
    color5.a = 0.5f;
    GL.Begin(7);
    Vector3 a1 = t1.position + Util.TransformVector(t1, this.legC.legs[leg].ankleHeelVector);
    Vector3 a2 = t2.position + Util.TransformVector(t2, this.legC.legs[leg].toeToetipVector);
    Vector3 a3 = localToWorldMatrix.MultiplyPoint3x4(this.cycles[leg].samples[indexFromTime].footBase);
    Vector3 vector3_2 = localToWorldMatrix.MultiplyPoint3x4(this.cycles[leg].samples[indexFromTime].footBase + this.cycles[leg].heelToetipVector);
    if (opt.drawHeelToe)
    {
      this.DrawDiamond(a1, size, color1);
      this.DrawDiamond(a2, size, color2);
    }
    if (opt.drawFootBase)
    {
      this.DrawDiamond(a3, size, color5);
      this.DrawDiamond(vector3_2, size, color5);
      GL.End();
      GL.Begin(1);
      this.DrawLine(a3, vector3_2, color3);
    }
    GL.End();
    if (opt.drawFootPrints)
    {
      float time2 = Util.Mod(time1 - this.cycles[leg].stanceTime + this.cycleOffset);
      Color c2 = color4 * this.GetFootGrounding(leg, time2) + color3 * (1f - this.GetFootGrounding(leg, time2));
      if (leg == currentLeg)
        GL.Begin(7);
      else
        GL.Begin(1);
      GL.Color(c2);
      Vector3 v = this.cycles[leg].stancePosition + this.cycleDirection * -this.cycleDistance * (Util.Mod(time2 + 0.5f, 1f) - 0.5f);
      Vector3 vector3_3 = Vector3.up * -0.5f * this.cycles[leg].heelToetipVector.magnitude;
      GL.Vertex(localToWorldMatrix.MultiplyPoint3x4(v + vector3_3));
      GL.Vertex(localToWorldMatrix.MultiplyPoint3x4(v));
      GL.Vertex(localToWorldMatrix.MultiplyPoint3x4(v + this.cycles[leg].heelToetipVector));
      GL.Vertex(localToWorldMatrix.MultiplyPoint3x4(v + this.cycles[leg].heelToetipVector + vector3_3));
      if (leg != currentLeg)
      {
        GL.Vertex(localToWorldMatrix.MultiplyPoint3x4(v));
        GL.Vertex(localToWorldMatrix.MultiplyPoint3x4(v + this.cycles[leg].heelToetipVector));
        GL.Vertex(localToWorldMatrix.MultiplyPoint3x4(v + this.cycles[leg].heelToetipVector + vector3_3));
        GL.Vertex(localToWorldMatrix.MultiplyPoint3x4(v + vector3_3));
      }
      GL.End();
    }
    if (this.motionType != MotionType.WalkCycle)
      return;
    if (opt.drawTrajectories)
    {
      GL.Begin(1);
      for (int index1 = 0; index1 < this.samples * 2; ++index1)
      {
        int index2 = Util.Mod(indexFromTime - index1, this.samples);
        LegCycleSample legCycleSample1 = this.cycles[leg].samples[index2];
        LegCycleSample legCycleSample2 = this.cycles[leg].samples[Util.Mod(index2 - 1, this.samples)];
        float timeFromIndex2 = this.GetTimeFromIndex(index1);
        float timeFromIndex3 = this.GetTimeFromIndex(index1 + 1);
        float num3 = 0.0f;
        float num4 = 0.0f;
        if (opt.normalizeGraph)
        {
          num3 = -timeFromIndex2 * this.cycleDistance;
          num4 = -timeFromIndex3 * this.cycleDistance;
        }
        Vector3 a4 = localToWorldMatrix.MultiplyPoint3x4(legCycleSample1.heel + num3 * this.cycleDirection);
        Vector3 b1 = localToWorldMatrix.MultiplyPoint3x4(legCycleSample2.heel + num4 * this.cycleDirection);
        Vector3 a5 = localToWorldMatrix.MultiplyPoint3x4(legCycleSample1.toetip + num3 * this.cycleDirection);
        Vector3 b2 = localToWorldMatrix.MultiplyPoint3x4(legCycleSample2.toetip + num4 * this.cycleDirection);
        Vector3 a6 = localToWorldMatrix.MultiplyPoint3x4(legCycleSample1.footBase + num3 * this.cycleDirection);
        Vector3 b3 = localToWorldMatrix.MultiplyPoint3x4(legCycleSample2.footBase + num4 * this.cycleDirection);
        if (opt.drawHeelToe)
        {
          this.DrawLine(a4, b1, color1);
          this.DrawLine(a5, b2, color2);
        }
        if (opt.drawFootBase)
          this.DrawLine(a6, b3, index2 % 2 != 0 ? color4 : color3);
        if (opt.drawTrajectoriesProjected)
          this.DrawLine(localToWorldMatrix.MultiplyPoint3x4(Util.ProjectOntoPlane(legCycleSample1.heel + legCycleSample1.toetip, Vector3.up) / 2f + (this.legC.groundPlaneHeight - vector3_1.y) * Vector3.up), localToWorldMatrix.MultiplyPoint3x4(Util.ProjectOntoPlane(legCycleSample2.heel + legCycleSample2.toetip, Vector3.up) / 2f + (this.legC.groundPlaneHeight - vector3_1.y) * Vector3.up), color3);
      }
      GL.End();
      if (opt.drawTrajectoriesProjected)
      {
        GL.Begin(7);
        this.DrawDiamond(localToWorldMatrix.MultiplyPoint3x4(Util.ProjectOntoPlane(this.cycles[leg].samples[indexFromTime].heel + this.cycles[leg].samples[indexFromTime].toetip, Vector3.up) / 2f + (this.legC.groundPlaneHeight - vector3_1.y) * Vector3.up), size, color5);
        GL.End();
        if (opt.drawThreePoints)
        {
          for (int index = -1; index <= 1; ++index)
          {
            GL.Begin(7);
            this.DrawDiamond(localToWorldMatrix.MultiplyPoint3x4(this.cycles[leg].cycleCenter + (this.legC.groundPlaneHeight - vector3_1.y) * Vector3.up + (float) index * this.cycles[leg].cycleDirection * this.cycles[leg].cycleScaling * 0.5f), size, color5);
            GL.End();
          }
          GL.Begin(1);
          this.DrawLine(localToWorldMatrix.MultiplyPoint3x4(this.cycles[leg].cycleCenter + (this.legC.groundPlaneHeight - vector3_1.y) * Vector3.up - this.cycles[leg].cycleDirection * this.cycles[leg].cycleScaling), localToWorldMatrix.MultiplyPoint3x4(this.cycles[leg].cycleCenter + (this.legC.groundPlaneHeight - vector3_1.y) * Vector3.up + this.cycles[leg].cycleDirection * this.cycles[leg].cycleScaling), color5);
          GL.End();
        }
      }
    }
    if (!opt.drawGraph)
      return;
    GL.Begin(1);
    Vector3 vector3_4 = this.legC.groundPlaneHeight * this.gameObject.transform.up * num1;
    float num5 = 2f * this.cycleDistance;
    if (!opt.normalizeGraph)
      num5 = 0.0f;
    Quaternion rotation = this.gameObject.transform.rotation;
    Vector3 right = rotation * Quaternion.Euler(0.0f, 90f, 0.0f) * this.cycleDirection;
    Vector3 vector3_5 = rotation * this.gameObject.transform.up;
    Vector3 vector3_6 = rotation * this.cycleDirection;
    Vector3 position = this.gameObject.transform.position + vector3_4;
    DrawArea drawArea1 = (DrawArea) new DrawArea3D(new Vector3((float) (-(double) num2 * 0.5), 0.0f, 0.0f), new Vector3((float) (-(double) num2 * 1.5), num1, num1), Util.CreateMatrix(right, vector3_5 * opt.graphScaleV, vector3_6 * opt.graphScaleH, position));
    DrawArea drawArea2 = (DrawArea) new DrawArea3D(new Vector3((float) (-(double) num2 * 0.5), (float) ((double) vector3_1.y * (double) num1 * 0.699999988079071), 0.0f), new Vector3((float) (-(double) num2 * 1.5), (float) ((double) vector3_1.y * (double) num1 * 1.0), 1f), Util.CreateMatrix(right, vector3_5 + vector3_6, vector3_6 - vector3_5, position + vector3_6 * this.graphMax.z * num1 + vector3_5 * vector3_1.y * num1));
    DrawArea drawArea3 = (DrawArea) new DrawArea3D(new Vector3((float) (-(double) num2 * 0.5), (float) ((double) vector3_1.y * (double) num1 * 0.200000002980232), 0.0f), new Vector3((float) (-(double) num2 * 1.5), (float) ((double) vector3_1.y * (double) num1 * 0.5), 1f), Util.CreateMatrix(right, vector3_5 + vector3_6, vector3_6 - vector3_5, position + vector3_6 * this.graphMax.z * num1 + vector3_5 * vector3_1.y * num1));
    if (flag2)
      drawArea1.DrawCube(new Vector3(0.0f, 0.0f, this.graphMax.z), Vector3.right * 2f, Vector3.up * vector3_1.y, -Vector3.forward * (vector3_1.z + num5), c1);
    for (int index = 0; index < 2; ++index)
    {
      if (opt.drawStanceMarkers)
        drawArea1.DrawRect(new Vector3(Util.Mod(timeFromIndex1 - this.cycles[leg].stanceTime + this.cycleOffset, 1f) + (float) index, 0.0f, this.graphMax.z), Vector3.up * vector3_1.y, -Vector3.forward * (vector3_1.z + num5), color3);
    }
    if (opt.drawHeelToe)
    {
      this.DrawLine(t1.position + Util.TransformVector(t1, this.legC.legs[leg].ankleHeelVector), drawArea1.Point(new Vector3(0.0f, this.cycles[leg].samples[indexFromTime].heel.y - this.legC.groundPlaneHeight, Vector3.Dot(this.cycles[leg].samples[indexFromTime].heel, this.cycleDirection))), color1);
      this.DrawLine(t2.position + Util.TransformVector(t2, this.legC.legs[leg].toeToetipVector), drawArea1.Point(new Vector3(0.0f, this.cycles[leg].samples[indexFromTime].toetip.y - this.legC.groundPlaneHeight, Vector3.Dot(this.cycles[leg].samples[indexFromTime].toetip, this.cycleDirection))), color2);
    }
    if (opt.drawFootBase)
      this.DrawLine(a3, drawArea1.Point(new Vector3(0.0f, this.cycles[leg].samples[indexFromTime].footBase.y - this.legC.groundPlaneHeight, Vector3.Dot(this.cycles[leg].samples[indexFromTime].footBase, this.cycleDirection))), Color.black);
    for (int index1 = 0; index1 < this.samples * 2; ++index1)
    {
      int index2 = Util.Mod(indexFromTime - index1, this.samples);
      LegCycleSample legCycleSample1 = this.cycles[leg].samples[index2];
      LegCycleSample legCycleSample2 = this.cycles[leg].samples[Util.Mod(index2 - 1, this.samples)];
      float timeFromIndex2 = this.GetTimeFromIndex(index1);
      float timeFromIndex3 = this.GetTimeFromIndex(index1 + 1);
      float timeFromIndex4 = this.GetTimeFromIndex(Util.Mod(index2 - this.cycles[leg].stanceIndex, this.samples));
      float timeFromIndex5 = this.GetTimeFromIndex(Util.Mod(index2 - 1 - this.cycles[leg].stanceIndex, this.samples));
      float num3 = 0.0f;
      float num4 = 0.0f;
      if (opt.normalizeGraph)
      {
        num3 = -timeFromIndex2 * this.cycleDistance;
        num4 = -timeFromIndex3 * this.cycleDistance;
      }
      if (opt.drawHeelToe)
      {
        drawArea1.DrawLine(new Vector3(timeFromIndex2, legCycleSample1.heel.y - this.legC.groundPlaneHeight, Vector3.Dot(legCycleSample1.heel, this.cycleDirection) + num3), new Vector3(timeFromIndex3, legCycleSample2.heel.y - this.legC.groundPlaneHeight, Vector3.Dot(legCycleSample2.heel, this.cycleDirection) + num4), color1);
        drawArea1.DrawLine(new Vector3(timeFromIndex2, legCycleSample1.toetip.y - this.legC.groundPlaneHeight, Vector3.Dot(legCycleSample1.toetip, this.cycleDirection) + num3), new Vector3(timeFromIndex3, legCycleSample2.toetip.y - this.legC.groundPlaneHeight, Vector3.Dot(legCycleSample2.toetip, this.cycleDirection) + num4), color2);
      }
      if (opt.drawFootBase && index2 % 2 == 0)
        drawArea1.DrawLine(new Vector3(timeFromIndex2, legCycleSample1.footBase.y - this.legC.groundPlaneHeight, Vector3.Dot(legCycleSample1.footBase, this.cycleDirection) + num3), new Vector3(timeFromIndex3, legCycleSample2.footBase.y - this.legC.groundPlaneHeight, Vector3.Dot(legCycleSample2.footBase, this.cycleDirection) + num4), color3);
      if (opt.drawBalanceCurve)
      {
        float num6 = (float) (((double) legCycleSample1.balance + (double) legCycleSample2.balance) / 2.0);
        drawArea2.DrawLine(new Vector3(timeFromIndex2, legCycleSample1.balance, 0.0f), new Vector3(timeFromIndex3, legCycleSample2.balance, 0.0f), color2 * num6 + color1 * (1f - num6));
      }
      if (opt.drawLiftedCurve)
      {
        float num6 = (float) (((double) this.GetFootGroundingOrig(leg, timeFromIndex4) + (double) this.GetFootGroundingOrig(leg, timeFromIndex5)) / 2.0);
        drawArea3.DrawLine(new Vector3(timeFromIndex2, this.GetFootGroundingOrig(leg, timeFromIndex4), 0.0f), new Vector3(timeFromIndex3, this.GetFootGroundingOrig(leg, timeFromIndex5), 0.0f), color3 * (1f - num6) + color4 * num6);
      }
    }
    GL.End();
  }

  private delegate Vector3 GetVector3Member(LegCycleSample s);

  private delegate float GetFloatMember(LegCycleSample s);
}
