﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DHelpWindowUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;
using UnityEngine.UI;

public class SystemMap3DHelpWindowUgui : Window
{
  [SerializeField]
  private Text headerLabel;
  [SerializeField]
  private Text okButtonLabel;
  [SerializeField]
  private GameObject helpItemsRoot;
  [SerializeField]
  private Text hotkeyFocusShip;
  [SerializeField]
  private Text hotkeyOverview;
  private InputBinder inputBindings;

  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  protected override bool IsInCombatGui
  {
    get
    {
      return false;
    }
  }

  private bool IsVisible
  {
    get
    {
      return this.gameObject.activeSelf;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.SetLocalizedTexts();
    this.inputBindings = (FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider") as SettingsDataProvider).InputBinder;
  }

  protected override void Start()
  {
    base.Start();
    this.CloseWindow();
  }

  private void SetOkButtonText(string text)
  {
    this.okButtonLabel.text = text;
  }

  private void SetHeaderText(string text)
  {
    this.headerLabel.text = text;
  }

  private void CloseWindow()
  {
    this.IsVisible = false;
  }

  public void OkButtonPressed()
  {
    this.CloseWindow();
  }

  public void ToggleVisibility()
  {
    this.IsVisible = !this.IsVisible;
  }

  private void SetLocalizedTexts()
  {
    this.SetOkButtonText(Tools.ParseMessage("%$bgo.common.ok%"));
    this.SetHeaderText(Tools.ParseMessage("%$bgo.sector_map.help_window.title%"));
    foreach (Text componentsInChild in this.helpItemsRoot.GetComponentsInChildren<Text>())
      componentsInChild.text = Tools.ParseMessage(componentsInChild.text);
  }

  protected override void OnEnable()
  {
    this.UpdateHotkeyFocusShip();
    this.UpdateHotkeyOverview();
  }

  private void UpdateHotkeyFocusShip()
  {
    this.hotkeyFocusShip.text = this.CreateHotkeyString(this.inputBindings.TryGetBinding(InputDevice.KeyboardMouse, Action.Map3DFocusYourShip));
  }

  private void UpdateHotkeyOverview()
  {
    this.hotkeyOverview.text = this.CreateHotkeyString(this.inputBindings.TryGetBinding(InputDevice.KeyboardMouse, Action.Map3DBackToOverview));
  }

  private string CreateHotkeyString(IInputBinding inputBinding)
  {
    return (!string.IsNullOrEmpty(inputBinding.ModifierAlias) ? inputBinding.ModifierAlias + "\n" : string.Empty) + inputBinding.TriggerAlias;
  }
}
