﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorBaseComponentNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public abstract class HudIndicatorBaseComponentNgui : HudIndicatorBaseComponent
{
  public abstract bool RequiresAnimationNgui { get; }

  protected override void ApplyOpacity()
  {
    foreach (UIWidget componentsInChild in this.GetComponentsInChildren<UIWidget>(true))
      componentsInChild.alpha = this.opacity;
  }
}
