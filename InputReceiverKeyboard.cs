﻿// Decompiled with JetBrains decompiler
// Type: InputReceiverKeyboard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Language;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class InputReceiverKeyboard
{
  private float2 oldMousePosition = float2.zero;
  private readonly Dictionary<Layout, LanguageLayout> translator = new Dictionary<Layout, LanguageLayout>();
  private const float MOUSE_DELTA = 0.5f;
  private readonly List<InputReceiverKeyboard.Key> keys;
  private readonly JoystickSetup joystickSetup;
  private readonly SettingsDataProvider settingsDataProvider;
  private readonly InputDispatcher inputDispatcher;

  public static Layout Layout { get; set; }

  public InputReceiverKeyboard(InputDispatcher inputDispatcher)
  {
    this.inputDispatcher = inputDispatcher;
    InputReceiverKeyboard.Layout = Layout.UnityDefault;
    this.keys = new List<InputReceiverKeyboard.Key>();
    this.translator.Add(Layout.English, (LanguageLayout) new EnglishLayout());
    this.translator.Add(Layout.German, (LanguageLayout) new GermanLayout());
    this.translator.Add(Layout.French, (LanguageLayout) new FrenchLayout());
    this.translator.Add(Layout.UnityDefault, new LanguageLayout());
    foreach (int num in Enum.GetValues(typeof (KeyCode)))
      this.AddKeyToListen((KeyCode) num);
    this.settingsDataProvider = FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider") as SettingsDataProvider;
    Input.eatKeyPressOnTextFieldFocus = false;
  }

  private void AddKeyToListen(KeyCode key)
  {
    using (List<InputReceiverKeyboard.Key>.Enumerator enumerator = this.keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.code == key)
          return;
      }
    }
    this.keys.Add(new InputReceiverKeyboard.Key(KeyState.Up, key));
  }

  public void ClearKeyStates()
  {
    for (int index = 0; index < this.keys.Count; ++index)
    {
      InputReceiverKeyboard.Key key = this.keys[index];
      if (Input.GetKey(key.code))
      {
        this.NotifyAboutKeyCodeUp(key.code);
        key.previousState = KeyState.Up;
      }
    }
  }

  private void UpdateKeyboardInputs()
  {
    using (List<InputReceiverKeyboard.Key>.Enumerator enumerator = this.keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InputReceiverKeyboard.Key current = enumerator.Current;
        bool key = Input.GetKey(current.code);
        if (key && current.previousState == KeyState.Up)
        {
          this.NotifyAboutKeyDown(current.code);
          current.previousState = KeyState.Down;
        }
        else if (!key && current.previousState == KeyState.Down)
        {
          this.NotifyAboutKeyCodeUp(current.code);
          current.previousState = KeyState.Up;
        }
      }
    }
  }

  private void UpdateMouseInputs()
  {
    float2 mousePositionGui = MouseSetup.MousePositionGui;
    if ((double) (this.oldMousePosition - mousePositionGui).sqrMagnitude <= 0.5)
      return;
    this.inputDispatcher.SendMousePosition(mousePositionGui);
    this.oldMousePosition = mousePositionGui;
  }

  public void Update()
  {
    this.UpdateKeyboardInputs();
    this.UpdateMouseInputs();
    this.CheckScrollWheel();
  }

  public void CheckScrollWheel()
  {
    float axis = Input.GetAxis("Mouse ScrollWheel");
    if ((double) axis == 0.0)
      return;
    if ((double) axis < 0.0)
    {
      this.NotifyAboutScrollDown();
    }
    else
    {
      if ((double) axis <= 0.0)
        return;
      this.NotifyAboutScrollUp();
    }
  }

  public bool GetLanguageLayoutAlias(KeyCode keyCode, out KeyCode alias)
  {
    alias = this.translator[InputReceiverKeyboard.Layout][keyCode];
    return alias != keyCode;
  }

  private void NotifyAboutScrollDown()
  {
    this.inputDispatcher.SendMouseScrollDown();
  }

  private void NotifyAboutScrollUp()
  {
    this.inputDispatcher.SendMouseScrollUp();
  }

  private void NotifyAboutKeyDown(KeyCode keyCode)
  {
    keyCode = this.translator[InputReceiverKeyboard.Layout][keyCode];
    this.inputDispatcher.ResetInactiveTimer(keyCode);
    if (keyCode >= KeyCode.Mouse0 && keyCode <= KeyCode.Mouse1)
    {
      this.inputDispatcher.SendMouseDown(MouseSetup.MousePositionGui, keyCode);
    }
    else
    {
      KeyModifier modifierPressedStatus = this.GetModifierPressedStatus();
      Action keyboardAction1 = this.settingsDataProvider.InputBinder.TryGetKeyboardAction(keyCode, modifierPressedStatus);
      Action keyboardAction2 = this.settingsDataProvider.InputBinder.TryGetKeyboardAction(keyCode, KeyModifier.None);
      if (keyboardAction2 == Action.FocusChat || keyboardAction1 == Action.FocusChat)
        this.ClearKeyStates();
      if (modifierPressedStatus != KeyModifier.None)
        this.inputDispatcher.SendActionDown(keyCode, keyboardAction1);
      if (modifierPressedStatus != KeyModifier.None && !this.ShouldTriggerSkAndDkAction(keyboardAction2, keyboardAction1))
        return;
      this.inputDispatcher.SendActionDown(keyCode, keyboardAction2);
    }
  }

  private bool ShouldTriggerSkAndDkAction(Action skAction, Action dkAction)
  {
    if (InputDispatcher.IsMovementAction(skAction))
      return (skAction != Action.ToggleBoost || dkAction != Action.Boost ? (skAction != Action.Boost ? 0 : (dkAction == Action.ToggleBoost ? 1 : 0)) : 1) == 0;
    return false;
  }

  private void NotifyAboutKeyCodeUp(KeyCode keyCode)
  {
    keyCode = this.translator[InputReceiverKeyboard.Layout][keyCode];
    if (keyCode >= KeyCode.Mouse0 && keyCode <= KeyCode.Mouse1)
      this.inputDispatcher.SendMouseUp(MouseSetup.MousePositionGui, keyCode);
    else
      this.NotifyAboutKeyboardKeyUp(keyCode);
  }

  private void NotifyAboutKeyboardKeyUp(KeyCode keyCode)
  {
    KeyModifier modifierPressedStatus = this.GetModifierPressedStatus();
    Action keyboardAction1 = this.settingsDataProvider.InputBinder.TryGetKeyboardAction(keyCode, modifierPressedStatus);
    Action keyboardAction2 = this.settingsDataProvider.InputBinder.TryGetKeyboardAction(keyCode, KeyModifier.None);
    if (modifierPressedStatus != KeyModifier.None)
      this.inputDispatcher.SendActionUp(keyCode, keyboardAction1);
    if (modifierPressedStatus != KeyModifier.None && !this.ShouldTriggerSkAndDkAction(keyboardAction2, keyboardAction1))
      return;
    this.inputDispatcher.SendActionUp(keyCode, keyboardAction2);
  }

  private KeyModifier GetModifierPressedStatus()
  {
    KeyModifier keyModifier = KeyModifier.None;
    if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
      keyModifier = KeyModifier.Shift;
    if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
      keyModifier = KeyModifier.Control;
    return keyModifier;
  }

  private class Key
  {
    public KeyState previousState;
    public KeyCode code;

    public Key(KeyState previousState, KeyCode code)
    {
      this.previousState = previousState;
      this.code = code;
    }
  }
}
