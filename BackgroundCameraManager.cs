﻿// Decompiled with JetBrains decompiler
// Type: BackgroundCameraManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BackgroundCameraManager : MonoBehaviour
{
  public Camera[] BackgroundCameras;
  private Camera activeCamera;

  private void Awake()
  {
    this.activeCamera = Camera.main;
  }

  public static BackgroundCameraManager Instance()
  {
    GameObject gameObject = GameObject.Find("BackgroundCameras");
    if ((Object) gameObject == (Object) null)
      return (BackgroundCameraManager) null;
    return gameObject.GetComponent<BackgroundCameraManager>();
  }

  private void LateUpdate()
  {
    if ((Object) this.activeCamera == (Object) null)
    {
      Debug.LogError((object) "ActiveCamera is null!");
    }
    else
    {
      for (int index = 0; index < this.BackgroundCameras.Length; ++index)
        this.BackgroundCameras[index].fieldOfView = this.activeCamera.fieldOfView;
      for (int index = 0; index < this.BackgroundCameras.Length; ++index)
        this.BackgroundCameras[index].transform.rotation = this.activeCamera.transform.rotation;
    }
  }

  public void SetActiveCam(Camera camera)
  {
    this.activeCamera = camera;
  }
}
