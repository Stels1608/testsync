﻿// Decompiled with JetBrains decompiler
// Type: RoomLevel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.DamageWindow;
using Gui.Hangar;
using Gui.Tooltips;
using HighlightingSystem;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class RoomLevel : GameLevel
{
  private uint serverID = uint.MaxValue;
  private RoomLevel.RoomLevelLoadedTrigger isLoadedTrigger = new RoomLevel.RoomLevelLoadedTrigger();
  private const float DIALOG_TIME_OUT = 5f;
  private uint cardGUID;
  private List<NPCArea> npcs;
  private RoomState roomState;
  private Dialog dialogModel;
  public GuiAdvancedLabelTooltip roomDetectionToolTip;
  public RoomBlackFading roomBlackFading;
  public ShipCustomizationWindow shipCustomizationWindow;
  public HangarWindow hangarWindow;
  public DWWindow repairGui;
  public ShopWindow shopWindow;
  public Gui.ShipShop.ShipShop shipShop;
  public GUICharacterStatusWindow characterStatusWindow;

  public override GameLevelType LevelType
  {
    get
    {
      return GameLevelType.RoomLevel;
    }
  }

  public override bool IsIngameLevel
  {
    get
    {
      return true;
    }
  }

  public RoomCard Card
  {
    get
    {
      return (RoomCard) Game.Catalogue.FetchCard(this.cardGUID, CardView.Room);
    }
  }

  public WorldCard WorldCard
  {
    get
    {
      return (WorldCard) Game.Catalogue.FetchCard(this.cardGUID, CardView.World);
    }
  }

  public RoomState RoomState
  {
    get
    {
      return this.roomState;
    }
  }

  public uint ServerId
  {
    get
    {
      return this.serverID;
    }
  }

  public static RoomLevel GetLevel()
  {
    return GameLevel.Instance as RoomLevel;
  }

  protected override void AddLoadingScreenDependencies()
  {
    this.IsStarted.Depend(new ILoadable[1]
    {
      (ILoadable) this.isLoadedTrigger
    });
  }

  public override void Initialize(LevelProfile levelProfile)
  {
    RoomLevelProfile roomLevelProfile = levelProfile as RoomLevelProfile;
    this.cardGUID = roomLevelProfile.CardGuid;
    this.serverID = roomLevelProfile.SectorId;
    base.Initialize(levelProfile);
    this.shop = ((ShopDataProvider) FacadeFactory.GetInstance().FetchDataProvider("ShopDataProvider")).Shop;
    this.shop.RequestItems();
    RoomStateLoad roomStateLoad = new RoomStateLoad(this);
    this.roomState = (RoomState) roomStateLoad;
    roomStateLoad.HandlerLoaded = new RoomStateLoad.dHandler(this.OnFinishLoaded);
    roomStateLoad.HandlerLoaded += new RoomStateLoad.dHandler(this.isLoadedTrigger.IsLoaded.Set);
    SceneProtocol.GetProtocol().NotifySceneLoaded();
    if (Game.Me.Party.inGroupJump)
      JumpActionsHandler.CancelJumpSequence();
    Game.JustJumped = false;
  }

  private void CreateGui()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    RoomLevel.\u003CCreateGui\u003Ec__AnonStoreyDA guiCAnonStoreyDa = new RoomLevel.\u003CCreateGui\u003Ec__AnonStoreyDA();
    FacadeFactory.GetInstance().SendMessage(Message.CreateLevelUi, (object) this);
    this.roomDetectionToolTip = new GuiAdvancedLabelTooltip();
    this.roomBlackFading = new RoomBlackFading();
    this.shipCustomizationWindow = new ShipCustomizationWindow();
    this.hangarWindow = new HangarWindow();
    this.repairGui = new DWWindow();
    this.shopWindow = new ShopWindow();
    this.shipShop = new Gui.ShipShop.ShipShop();
    GuiWingsMainPanel guiWingsMainPanel = new GuiWingsMainPanel();
    this.characterStatusWindow = new GUICharacterStatusWindow(false);
    MessageBoxManager messageBoxManager = new MessageBoxManager();
    GUIShopCantbuyWindow shopCantbuyWindow = new GUIShopCantbuyWindow((SmartRect) null);
    shopCantbuyWindow.Hide();
    HudMediator hudMediator = (HudMediator) FacadeFactory.GetInstance().FetchView("HudMediator");
    GuiMediator guiMediator = (GuiMediator) FacadeFactory.GetInstance().FetchView("GuiMediator");
    if (!(FacadeFactory.GetInstance().FetchDataProvider("GuiDataProvider") as GuiDataProvider).MvcRoot.WindowManager.IsDrawn())
      Game.GUIManager.IsRendered = true;
    Game.Ticker.IsRendered = true;
    Game.GUIManager.AddPanel((IGUIPanel) this.shipShop);
    Game.GUIManager.AddPanel((IGUIPanel) this.repairGui);
    Game.GUIManager.AddPanel((IGUIPanel) this.hangarWindow);
    Game.GUIManager.AddPanel((IGUIPanel) this.characterStatusWindow);
    Game.GUIManager.AddPanel((IGUIPanel) this.shipCustomizationWindow);
    Game.GUIManager.AddPanel((IGUIPanel) guiWingsMainPanel);
    Game.GUIManager.AddPanel((IGUIPanel) this.shopWindow);
    Game.GUIManager.AddPanel((IGUIPanel) shopCantbuyWindow);
    Game.GUIManager.AddPanel((IGUIPanel) this.roomDetectionToolTip);
    Game.GUIManager.AddPanel((IGUIPanel) messageBoxManager);
    Game.GUIManager.AddPanel((IGUIPanel) this.roomBlackFading);
    Game.GUIManager.NotifyAboutResolutionChanged();
    Game.InputDispatcher.RemoveListener((InputListener) Game.GUIManager.Find<GUIChatNew>());
    Game.InputDispatcher.AddListener((InputListener) guiMediator);
    Game.InputDispatcher.AddListener((InputListener) hudMediator);
    Game.InputDispatcher.AddListener((InputListener) messageBoxManager);
    Game.InputDispatcher.AddListener((InputListener) shopCantbuyWindow);
    Game.InputDispatcher.AddListener((InputListener) this.shipCustomizationWindow);
    Game.InputDispatcher.AddListener((InputListener) this.shopWindow);
    Game.InputDispatcher.AddListener((InputListener) this.characterStatusWindow);
    Game.InputDispatcher.AddListener((InputListener) this.hangarWindow);
    Game.InputDispatcher.AddListener((InputListener) this.repairGui);
    Game.InputDispatcher.AddListener((InputListener) this.shipShop);
    Game.InputDispatcher.AddListener((InputListener) Game.GUIManager.Find<GUIChatNew>());
    Game.InputDispatcher.AddListener((InputListener) guiWingsMainPanel);
    this.GUIManager.Find<GUIChatNew>().IsRendered = true;
    Game.DelayedActions.Add(new DelayedActions.Predicate(Game.SpecialOfferManager.Check));
    // ISSUE: reference to a compiler-generated field
    guiCAnonStoreyDa.waiter = new GuiPanel();
    // ISSUE: reference to a compiler-generated method
    TimerSimple timerSimple = new TimerSimple(3f, new AnonymousDelegate(guiCAnonStoreyDa.\u003C\u003Em__222));
    // ISSUE: reference to a compiler-generated field
    guiCAnonStoreyDa.waiter.AddChild((GuiElementBase) timerSimple);
    // ISSUE: reference to a compiler-generated field
    Game.RegisterDialog((IGUIPanel) guiCAnonStoreyDa.waiter, true);
    FacadeFactory.GetInstance().SendMessage(Message.RecheckSystemButtons);
    LevelGuiInitializer.Init();
    Vector2.MoveTowards(new Vector2(), Vector2.one * 15f, 1f);
    FacadeFactory.GetInstance().SendMessage(Message.LevelUiInstalled);
  }

  public override void CheckSystemButtons(SystemButtonWindow systemButtons)
  {
    base.CheckSystemButtons(systemButtons);
    systemButtons.ShowArena(true);
    systemButtons.SetTournamentTooltip("%$bgo.option_buttons.tournament%");
  }

  protected override void Update()
  {
    base.Update();
    if (this.roomState == null)
      return;
    this.roomState.Update();
  }

  private List<NPCArea> SetupAreaNpcs()
  {
    GameObject gameObject = GameObject.Find("NPCs");
    if ((Object) gameObject == (Object) null)
      return new List<NPCArea>();
    List<NPCArea> npcAreaList = new List<NPCArea>();
    using (List<RoomNPC>.Enumerator enumerator = this.Card.NPCs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        RoomNPC current = enumerator.Current;
        Transform child = gameObject.transform.FindChild(current.NPC);
        if ((Object) child == (Object) null)
        {
          UnityEngine.Debug.LogWarning((object) ("Could not create NPC: " + current.NPC + " because npcObject is null"));
        }
        else
        {
          Transform transform = (Transform) null;
          foreach (Transform componentsInChild in child.GetComponentsInChildren<Transform>())
          {
            if (componentsInChild.name.ToLower().Contains("pelvis"))
            {
              transform = componentsInChild;
              break;
            }
          }
          if ((Object) transform == (Object) null)
          {
            UnityEngine.Debug.LogWarning((object) "HipsObject is null");
          }
          else
          {
            NPCArea npcArea = transform.gameObject.AddComponent<NPCArea>();
            npcArea.npc = current;
            npcArea.level = this;
            npcArea.npcRoot = child;
            Highlighter highlighter = child.gameObject.AddComponent<Highlighter>();
            npcArea.highlightableObject = highlighter;
            npcAreaList.Add(npcArea);
            transform.gameObject.AddComponent<DialogCharacterInfo>().characterName = current.NPC;
            transform.gameObject.AddComponent<DialogCharacterAnimation>();
          }
        }
      }
    }
    return npcAreaList;
  }

  private NPCArea FindNPCArea(string npcName)
  {
    using (List<NPCArea>.Enumerator enumerator = this.npcs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        NPCArea current = enumerator.Current;
        if (current.npc.NPC == npcName)
          return current;
      }
    }
    return (NPCArea) null;
  }

  protected void OnFinishLoaded()
  {
    Object.Instantiate((this.roomState as RoomStateLoad).ScenePrefab.Asset, new Vector3(), new Quaternion());
    this.npcs = this.SetupAreaNpcs();
    GameObject gameObject = GameObject.Find("ROOM_CAMERA");
    if ((Object) gameObject == (Object) null || (Object) gameObject.GetComponent<Camera>() == (Object) null)
    {
      Log.Warning((object) "WARNING!!!: ROOM_CAMERA not found!");
      gameObject = new GameObject("DefaultCamera");
      gameObject.AddComponent<Camera>();
      gameObject.AddComponent<GUILayer>();
      gameObject.AddComponent<AudioListener>();
    }
    gameObject.AddComponent<HighlightingRenderer>();
    gameObject.tag = "MainCamera";
    this.cameraSwitcher = new CameraSwitcher(Camera.main);
    CameraDetector cameraDetector = gameObject.AddComponent<CameraDetector>();
    this.CreateGui();
    Game.InputDispatcher.AddListener((InputListener) cameraDetector);
    this.roomState = (RoomState) null;
    RoomProtocol.GetProtocol().Enter();
    FacadeFactory.GetInstance().SendMessage(Message.LevelLoaded, (object) this);
    WofProtocol.GetProtocol().RequestWofInit();
    WofProtocol.GetProtocol().RequestWofVisibleMaps();
    WofProtocol.GetProtocol().RequestMapInfo();
  }

  private void OnEndDialog()
  {
    this.roomState = (RoomState) null;
  }

  public void ReceiveStartDialog(string npc)
  {
    this.StartCoroutine(this.WaitForReceiveStartDialog(npc));
  }

  [DebuggerHidden]
  private IEnumerator WaitForReceiveStartDialog(string npc)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RoomLevel.\u003CWaitForReceiveStartDialog\u003Ec__Iterator2C() { npc = npc, \u003C\u0024\u003Enpc = npc, \u003C\u003Ef__this = this };
  }

  public void RequestTalk(string npc)
  {
    RoomProtocol.GetProtocol().Talk(npc);
  }

  public override bool ConsoleCommand(string message)
  {
    string[] strArray = message.Split(' ');
    if (strArray.Length != 2 || !(strArray[0] == "talk"))
      return base.ConsoleCommand(message);
    this.RequestTalk(strArray[1]);
    return true;
  }

  public override void OnQuit()
  {
    base.OnQuit();
    this.dialogModel = (Dialog) null;
  }

  private class RoomLevelLoadedTrigger : ILoadable
  {
    private readonly Flag isLoaded = new Flag();

    public Flag IsLoaded
    {
      get
      {
        return this.isLoaded;
      }
    }
  }
}
