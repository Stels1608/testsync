﻿// Decompiled with JetBrains decompiler
// Type: GUIVScroller
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIVScroller : GUIPanel
{
  private const float MAX_SCROLLER_PERCENT_LIMIT = 0.7f;
  public GUIImageNew scroller;
  private float scrollPosition;
  private bool isScrolling;
  private float offsetToUpperY;

  public float MinScrollerHeight
  {
    get
    {
      return (float) this.scroller.Texture.height;
    }
  }

  public float MaxScrollerHeight
  {
    get
    {
      return this.root.Height * 0.7f;
    }
  }

  public bool IsScrolling
  {
    get
    {
      return this.isScrolling;
    }
    set
    {
      this.isScrolling = value;
      this.offsetToUpperY = 0.0f;
      if (!this.isScrolling)
        return;
      this.offsetToUpperY = MouseSetup.MousePositionGui.y - (this.scroller.SmartRect.AbsPosition.y - this.scroller.SmartRect.Height / 2f);
    }
  }

  public float Value
  {
    get
    {
      return this.scrollPosition;
    }
    set
    {
      this.scrollPosition = Mathf.Clamp(value, 0.0f, 1f);
      this.scroller.Position = new float2(0.0f, Mathf.Clamp((float) ((double) this.scrollPosition * (double) (this.root.Height - this.scroller.SmartRect.Height) + (double) this.scroller.SmartRect.Height / 2.0) - this.root.Height / 2f, (float) (-(double) this.root.Height / 2.0), this.root.Height / 2f));
      this.scroller.RecalculateAbsCoords();
    }
  }

  public float ScrollerHeight
  {
    get
    {
      return this.scroller.Rect.height;
    }
    set
    {
      this.scroller.SmartRect.Height = this.MinScrollerHeight + value * (this.MaxScrollerHeight - this.MinScrollerHeight);
      this.Value = this.Value;
    }
  }

  public GUIVScroller(float areaHeight, Texture2D sliderTexture, SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
    this.scroller = new GUIImageNew(sliderTexture, this.root);
    this.root.Height = areaHeight;
    this.root.Width = 5f;
    this.scroller.Position = new float2(0.0f, 0.0f);
    this.RecalculateAbsCoords();
  }

  public override void Draw()
  {
    base.Draw();
    this.scroller.Draw();
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.scroller.RecalculateAbsCoords();
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey != KeyCode.Mouse0 || !this.scroller.Contains(mousePosition))
      return false;
    this.IsScrolling = true;
    return true;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    if (!this.IsScrolling)
      return;
    this.Value = Mathf.Clamp01(Mathf.Clamp(mousePosition.y - (this.root.AbsPosition.y - this.root.Height / 2f) - this.offsetToUpperY, 0.0f, this.root.Height - this.scroller.SmartRect.Height) / (this.root.Height - this.scroller.SmartRect.Height));
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey != KeyCode.Mouse0 || !this.IsScrolling)
      return false;
    this.IsScrolling = false;
    return true;
  }

  public override bool Contains(float2 point)
  {
    return this.scroller.Contains(point);
  }
}
