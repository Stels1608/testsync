﻿// Decompiled with JetBrains decompiler
// Type: OneTwoButtonLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class OneTwoButtonLayout : ContentLayout
{
  private GameObject contentArea_1;
  private GameObject contentArea_2;
  private GameObject contentArea_3;

  public override void Awake()
  {
    this.contentArea_1 = new GameObject("contentArea1");
    this.contentArea_2 = new GameObject("contentArea2");
    this.contentArea_3 = new GameObject("contentArea3");
    GameObject gameObject = new GameObject("buttonArea");
    this.ContentArea.Add(this.contentArea_1);
    this.ContentArea.Add(this.contentArea_2);
    this.ContentArea.Add(this.contentArea_3);
    this.ContentArea.Add(gameObject);
    base.Awake();
    this.contentArea_1.transform.localPosition = new Vector3(20f, -16f, -1f);
    this.contentArea_2.transform.localPosition = new Vector3(560f, -16f, -1f);
    this.contentArea_3.transform.localPosition = new Vector3(560f, -362f, -1f);
    gameObject.transform.localPosition = this.buttonAreaPosition;
  }
}
