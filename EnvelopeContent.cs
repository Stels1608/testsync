﻿// Decompiled with JetBrains decompiler
// Type: EnvelopeContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIWidget))]
public class EnvelopeContent : MonoBehaviour
{
  public Transform targetRoot;
  public int padLeft;
  public int padRight;
  public int padBottom;
  public int padTop;
  public int minimumHeight;
  private UIWidget targetWidget;
  private UIWidget myWidget;

  private void Start()
  {
    this.Execute();
  }

  [ContextMenu("Execute")]
  public void Execute()
  {
    if ((Object) this.targetRoot == (Object) this.transform)
      Debug.LogError((object) "Target Root object cannot be the same object that has Envelop Content. Make it a sibling instead.", (Object) this);
    else if (NGUITools.IsChild(this.targetRoot, this.transform))
      Debug.LogError((object) "Target Root object should not be a parent of Envelop Content. Make it a sibling instead.", (Object) this);
    else if ((Object) (this.targetWidget = this.targetRoot.GetComponent<UIWidget>()) == (Object) null)
      Debug.LogError((object) "Target Root object has no widget.", (Object) this);
    else if ((Object) (this.myWidget = this.GetComponent<UIWidget>()) == (Object) null)
    {
      Debug.LogError((object) ("EnvelopeContent(): " + this.name + " has no widget"));
    }
    else
    {
      this.targetWidget.onChange += new UIWidget.OnDimensionsChanged(this.UpdateEnveloping);
      this.Init();
    }
  }

  private void Init()
  {
    if ((Object) (this.targetWidget = this.targetRoot.GetComponent<UIWidget>()) != (Object) null)
    {
      this.targetWidget.MarkAsChanged();
      this.targetWidget.Update();
    }
    this.UpdateEnveloping();
  }

  private void UpdateEnveloping()
  {
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.transform.parent, this.targetRoot, false);
    float x = relativeWidgetBounds.min.x + (float) this.padLeft;
    float y = relativeWidgetBounds.min.y + (float) this.padBottom;
    float num = relativeWidgetBounds.max.x + (float) this.padRight;
    float height = relativeWidgetBounds.max.y + (float) this.padTop - y;
    this.myWidget.SetRect(x, y, num - x, height);
    if (this.myWidget.height >= this.minimumHeight)
      return;
    this.myWidget.height = this.minimumHeight;
  }
}
