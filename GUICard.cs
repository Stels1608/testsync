﻿// Decompiled with JetBrains decompiler
// Type: GUICard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUICard : Card
{
  public string Key = string.Empty;
  private string name = string.Empty;
  private string description = string.Empty;
  private string shortDescription = string.Empty;
  public string GUITexturePath = string.Empty;
  public string GUIAtlasTexturePath = string.Empty;
  public string GUIAvatarSlotTexturePath = string.Empty;
  public byte Level;
  private string[] Args;
  public string GUIIcon;
  public Texture2D GUIAvatarSlotTexture;
  public Texture2D GUIIconTexture;
  public Texture2D GUITexture;
  private AtlasCache atlasCache;
  private AtlasEntry atlasEntry;
  public ushort FrameIndex;

  public string Name
  {
    get
    {
      if (string.IsNullOrEmpty(this.name) && !string.IsNullOrEmpty(this.Key))
      {
        string str1 = "%$bgo." + this.Key + ".Name";
        string str2 = (string) null;
        if (Game.Me.Faction == Faction.Cylon)
          BsgoLocalization.TryGet(str1 + "Cylon%", out str2);
        if (str2 == null)
        {
          if (!BsgoLocalization.TryGet(str1 + "_" + (object) this.Level + "%", out str2))
            str2 = BsgoLocalization.Get(str1 + "%");
        }
        this.name = str2;
      }
      return this.name;
    }
  }

  public string Description
  {
    get
    {
      if (string.IsNullOrEmpty(this.description) && !string.IsNullOrEmpty(this.Key))
      {
        string key = "bgo." + this.Key + ".Description";
        if (this.Args == null || this.Args.Length == 0)
        {
          string str = string.Empty;
          if (!BsgoLocalization.TryGet(key + "_" + (object) this.Level, out str))
            BsgoLocalization.TryGet(key, out str);
          this.description = str;
        }
        else
          this.description = BsgoLocalization.Get(key, (object[]) this.Args);
      }
      return this.description;
    }
  }

  public string ShortDescription
  {
    get
    {
      if (string.IsNullOrEmpty(this.shortDescription) && !string.IsNullOrEmpty(this.Key))
      {
        string key = "bgo." + this.Key + ".ShortDescription";
        string str;
        if (!BsgoLocalization.TryGet(key + "_" + (object) this.Level, out str))
          BsgoLocalization.TryGet(key, out str);
        this.shortDescription = str;
      }
      return this.shortDescription;
    }
  }

  public GUICard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.Key = r.ReadString();
    this.Level = r.ReadByte();
    this.GUIAtlasTexturePath = r.ReadString();
    this.FrameIndex = r.ReadUInt16();
    this.GUIIcon = r.ReadString();
    this.GUIAvatarSlotTexturePath = r.ReadString();
    this.GUITexturePath = r.ReadString();
    this.Args = r.ReadStringArray();
    this.GUIAvatarSlotTexture = (Texture2D) Resources.Load(this.GUIAvatarSlotTexturePath);
    this.GUIIconTexture = (Texture2D) Resources.Load(this.GUIIcon);
    if (string.IsNullOrEmpty(this.GUITexturePath))
      return;
    this.GUITexture = (Texture2D) ResourceLoader.Load(this.GUITexturePath);
  }

  public int CompareTo(GUICard other)
  {
    return (int) this.CardGUID - (int) other.CardGUID;
  }

  public bool SetGuiImage(ref GuiImage image, Vector2 elementSize)
  {
    if (!string.IsNullOrEmpty(this.GUIAtlasTexturePath) && (double) elementSize.x > 0.0 && (double) elementSize.y > 0.0)
    {
      this.atlasCache = this.atlasCache ?? new AtlasCache(float2.FromV2(elementSize));
      this.atlasEntry = this.atlasEntry ?? this.atlasCache.GetCachedEntryBy(this.GUIAtlasTexturePath, (int) this.FrameIndex);
      image.Texture = this.atlasEntry.Texture;
      image.SourceRect = this.atlasEntry.FrameRect;
      image.Size = float2.ToV2(this.atlasCache.ElementSize);
    }
    if ((Object) image.Texture == (Object) null)
    {
      image.Texture = this.GUIIconTexture ?? this.GUIAvatarSlotTexture;
      image.SourceRect = new Rect(0.0f, 0.0f, 1f, 1f);
    }
    if ((Object) image.Texture == (Object) null)
    {
      image.Texture = ResourceLoader.Load<Texture2D>("GUI/Inventory/items_atlas");
      image.SourceRect = new Rect()
      {
        x = (float) image.Texture.width - 40f,
        y = (float) image.Texture.height - 35f,
        width = 40f,
        height = 35f
      };
      image.Size = new Vector2(image.SourceRect.width, image.SourceRect.height);
    }
    return (Object) image.Texture != (Object) null;
  }

  public string FrameIndexNew()
  {
    if (this.GUIAtlasTexturePath == "GUI/AbilityToolbar/ammo_atlas")
      return ((int) this.FrameIndex + 300).ToString();
    return this.FrameIndex.ToString();
  }

  public override string ToString()
  {
    return string.Format("[GUICard] Key: {0}, Level: {1}, Args: {2}, GuiIcon: {3}, AtlasCache: {4}, AtlasEntry: {5}, Name: {6}, Description: {7}, ShortDescription: {8}, GuiTexturePath: {9}, GuiAtlasTexturePath: {10}, GuiAvatarSlotTexturePath: {11}, FrameIndex: {12}, Name: {13}, Description: {14}, ShortDescription: {15}", (object) this.Key, (object) this.Level, (object) this.Args, (object) this.GUIIcon, (object) this.atlasCache, (object) this.atlasEntry, (object) this.name, (object) this.description, (object) this.shortDescription, (object) this.GUITexturePath, (object) this.GUIAtlasTexturePath, (object) this.GUIAvatarSlotTexturePath, (object) this.FrameIndex, (object) this.Name, (object) this.Description, (object) this.ShortDescription);
  }
}
