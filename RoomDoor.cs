﻿// Decompiled with JetBrains decompiler
// Type: RoomDoor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class RoomDoor : IProtocolRead
{
  public string Door;
  public uint roomGUID;

  public bool IsUndock
  {
    get
    {
      return (int) this.roomGUID == 115200940;
    }
  }

  public RoomCard RoomCard
  {
    get
    {
      return Game.Catalogue.FetchCard(this.roomGUID, CardView.Room) as RoomCard;
    }
  }

  public GUICard RoomGUICard
  {
    get
    {
      return Game.Catalogue.FetchCard(this.roomGUID, CardView.GUI) as GUICard;
    }
  }

  public void Read(BgoProtocolReader r)
  {
    this.Door = r.ReadString();
    this.roomGUID = r.ReadGUID();
  }
}
