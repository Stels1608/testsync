﻿// Decompiled with JetBrains decompiler
// Type: BMFont
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BMFont
{
  [HideInInspector]
  [SerializeField]
  private int mSize = 16;
  [HideInInspector]
  [SerializeField]
  private List<BMGlyph> mSaved = new List<BMGlyph>();
  private Dictionary<int, BMGlyph> mDict = new Dictionary<int, BMGlyph>();
  [HideInInspector]
  [SerializeField]
  private int mBase;
  [HideInInspector]
  [SerializeField]
  private int mWidth;
  [HideInInspector]
  [SerializeField]
  private int mHeight;
  [HideInInspector]
  [SerializeField]
  private string mSpriteName;

  public bool isValid
  {
    get
    {
      return this.mSaved.Count > 0;
    }
  }

  public int charSize
  {
    get
    {
      return this.mSize;
    }
    set
    {
      this.mSize = value;
    }
  }

  public int baseOffset
  {
    get
    {
      return this.mBase;
    }
    set
    {
      this.mBase = value;
    }
  }

  public int texWidth
  {
    get
    {
      return this.mWidth;
    }
    set
    {
      this.mWidth = value;
    }
  }

  public int texHeight
  {
    get
    {
      return this.mHeight;
    }
    set
    {
      this.mHeight = value;
    }
  }

  public int glyphCount
  {
    get
    {
      if (this.isValid)
        return this.mSaved.Count;
      return 0;
    }
  }

  public string spriteName
  {
    get
    {
      return this.mSpriteName;
    }
    set
    {
      this.mSpriteName = value;
    }
  }

  public List<BMGlyph> glyphs
  {
    get
    {
      return this.mSaved;
    }
  }

  public BMGlyph GetGlyph(int index, bool createIfMissing)
  {
    BMGlyph bmGlyph1 = (BMGlyph) null;
    if (this.mDict.Count == 0)
    {
      int index1 = 0;
      for (int count = this.mSaved.Count; index1 < count; ++index1)
      {
        BMGlyph bmGlyph2 = this.mSaved[index1];
        this.mDict.Add(bmGlyph2.index, bmGlyph2);
      }
    }
    if (!this.mDict.TryGetValue(index, out bmGlyph1) && createIfMissing)
    {
      bmGlyph1 = new BMGlyph();
      bmGlyph1.index = index;
      this.mSaved.Add(bmGlyph1);
      this.mDict.Add(index, bmGlyph1);
    }
    return bmGlyph1;
  }

  public BMGlyph GetGlyph(int index)
  {
    return this.GetGlyph(index, false);
  }

  public void Clear()
  {
    this.mDict.Clear();
    this.mSaved.Clear();
  }

  public void Trim(int xMin, int yMin, int xMax, int yMax)
  {
    if (!this.isValid)
      return;
    int index = 0;
    for (int count = this.mSaved.Count; index < count; ++index)
    {
      BMGlyph bmGlyph = this.mSaved[index];
      if (bmGlyph != null)
        bmGlyph.Trim(xMin, yMin, xMax, yMax);
    }
  }
}
