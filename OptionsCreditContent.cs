﻿// Decompiled with JetBrains decompiler
// Type: OptionsCreditContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class OptionsCreditContent : MonoBehaviour
{
  private OneButtonLayout layout;

  public void Awake()
  {
    this.InitContent();
  }

  private void InitContent()
  {
    GameObject child = new GameObject("layout");
    child.layer = this.gameObject.layer;
    this.layout = child.AddComponent<OneButtonLayout>();
    PositionUtils.CorrectTransform(this.gameObject, child);
    if ((UnityEngine.Object) NGUITools.AddChild(this.layout.ContentArea[0], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/credits/Credits")) == (UnityEngine.Object) null)
      throw new ArgumentNullException("Settings general object failed to load");
  }
}
