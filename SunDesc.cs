﻿// Decompiled with JetBrains decompiler
// Type: SunDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SunDesc : Desc, IProtocolRead
{
  [JsonField(JsonName = "raysColor")]
  public Color raysColor = Color.white;
  [JsonField(JsonName = "streakColor")]
  public Color streakColor = Color.white;
  [JsonField(JsonName = "glowColor")]
  public Color glowColor = Color.red;
  [JsonField(JsonName = "discColor")]
  public Color discColor = Color.white;
  [JsonField(JsonName = "occlusionFade")]
  public bool occlusionFade = true;

  public void Read(BgoProtocolReader pr)
  {
    this.raysColor = pr.ReadColor();
    this.streakColor = pr.ReadColor();
    this.glowColor = pr.ReadColor();
    this.discColor = pr.ReadColor();
    this.occlusionFade = pr.ReadBoolean();
    this.rotation = pr.ReadQuaternion();
    this.position = pr.ReadVector3();
    this.scale = pr.ReadVector3();
  }
}
