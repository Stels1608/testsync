﻿// Decompiled with JetBrains decompiler
// Type: InputBindingChangeMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class InputBindingChangeMessage : IMessage<Message>
{
  private readonly IInputBinding bindingOld;
  private readonly IInputBinding bindingNew;
  private readonly InputDevice inputDevice;

  public Message Id
  {
    get
    {
      return Message.ChangeInputBinding;
    }
  }

  public object Data
  {
    get
    {
      return (object) this.BindingNew;
    }
    set
    {
    }
  }

  public IInputBinding BindingOld
  {
    get
    {
      return this.bindingOld;
    }
  }

  public IInputBinding BindingNew
  {
    get
    {
      return this.bindingNew;
    }
  }

  public InputDevice InputDevice
  {
    get
    {
      return this.inputDevice;
    }
  }

  public InputBindingChangeMessage(InputDevice device, IInputBinding bindingOld, IInputBinding bindingNew)
  {
    this.bindingOld = bindingOld;
    this.bindingNew = bindingNew;
    this.inputDevice = device;
  }

  public override string ToString()
  {
    return string.Format("HotKeyOld: {0}, HotKeyNew: {1}, HotKeyDevice: {2}", (object) this.bindingOld, (object) this.bindingNew, (object) this.inputDevice);
  }
}
