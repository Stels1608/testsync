﻿// Decompiled with JetBrains decompiler
// Type: HSSelectionButtonList
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.ObjectModel;
using UnityEngine;

public class HSSelectionButtonList : GuiPanelVerticalScroll
{
  private Texture2D normalUp;
  private Texture2D selectedUp;
  private Texture2D normalMiddle;
  private Texture2D selectedMiddle;
  private Texture2D normalDown;
  private Texture2D selectedDown;

  public HSSelectionButtonList()
  {
    Texture2D texture2D = TextureCache.Get(new Color(1f, 1f, 1f, 0.0f));
    this.normalUp = texture2D;
    this.normalMiddle = texture2D;
    this.normalDown = texture2D;
    if (Game.Me.Faction == Faction.Colonial)
    {
      this.selectedUp = (Texture2D) ResourceLoader.Load("GUI/Common/skill_selected_up");
      this.selectedMiddle = (Texture2D) ResourceLoader.Load("GUI/Common/skill_selected_middle");
      this.selectedDown = (Texture2D) ResourceLoader.Load("GUI/Common/skill_selected_down");
    }
    else
    {
      this.selectedUp = (Texture2D) ResourceLoader.Load("GUI/Common/skill_unavailable_up");
      this.selectedMiddle = (Texture2D) ResourceLoader.Load("GUI/Common/skill_unavailable_middle");
      this.selectedDown = (Texture2D) ResourceLoader.Load("GUI/Common/skill_unavailable_down");
    }
  }

  public void Fill(ReadOnlyCollection<HSSelectionButton> items)
  {
    this.EmptyChildren();
    foreach (GuiElementBase ch in items)
      this.AddChild(ch);
  }

  public override bool MouseDown(float2 position, KeyCode key)
  {
    HSSelectionButton hsSelectionButton1 = (HSSelectionButton) null;
    foreach (GuiElementBase child in this.Children)
    {
      HSSelectionButton hsSelectionButton2 = child as HSSelectionButton;
      if (hsSelectionButton2 != null)
      {
        if (hsSelectionButton2.selected)
          hsSelectionButton1 = hsSelectionButton2;
        hsSelectionButton2.selected = false;
      }
    }
    bool flag = base.MouseDown(position, key);
    foreach (GuiElementBase child in this.Children)
    {
      HSSelectionButton hsSelectionButton2 = child as HSSelectionButton;
      if (hsSelectionButton2 != null && hsSelectionButton2.selected)
        hsSelectionButton1 = (HSSelectionButton) null;
    }
    if (hsSelectionButton1 != null)
      hsSelectionButton1.selected = true;
    this.UpdateTextures();
    return flag;
  }

  public void UpdateTextures()
  {
    int num = 0;
    foreach (GuiElementBase guiElementBase in this.Queue)
    {
      if (guiElementBase is HSSelectionButton)
      {
        HSSelectionButton hsSelectionButton = guiElementBase as HSSelectionButton;
        hsSelectionButton.SizeY = 36.36f;
        Texture2D newTexture1;
        Texture2D newTexture2;
        if (num == 0)
        {
          newTexture1 = this.normalUp;
          newTexture2 = this.selectedUp;
          hsSelectionButton.SizeY = 40f;
        }
        else if (num == this.Queue.Count - 1)
        {
          newTexture1 = this.normalDown;
          newTexture2 = this.selectedDown;
        }
        else
        {
          newTexture1 = this.normalMiddle;
          newTexture2 = this.selectedMiddle;
        }
        if (hsSelectionButton.selected)
        {
          hsSelectionButton.SetNormalTextureWithoutResize(newTexture2);
          hsSelectionButton.SetOverTextureWithoutResize(newTexture2);
          hsSelectionButton.SetPressedTextureWithoutResize(newTexture2);
        }
        else
        {
          hsSelectionButton.SetNormalTextureWithoutResize(newTexture1);
          hsSelectionButton.SetOverTextureWithoutResize(newTexture1);
          hsSelectionButton.SetPressedTextureWithoutResize(newTexture1);
        }
        ++num;
      }
    }
  }

  public override void OnScroll()
  {
    this.UpdateTextures();
    base.OnScroll();
  }
}
