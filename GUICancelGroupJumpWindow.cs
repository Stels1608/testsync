﻿// Decompiled with JetBrains decompiler
// Type: GUICancelGroupJumpWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUICancelGroupJumpWindow : GuiPanel
{
  public GUICancelGroupJumpWindow()
  {
    GuiButton guiButton = new GuiButton("%$bgo.group_jump.drop_out%");
    guiButton.Font = Gui.Options.FontBGM_BT;
    guiButton.FontSize = 15;
    guiButton.Size = new Vector2(220f, 40f);
    guiButton.Pressed = new AnonymousDelegate(JumpActionsHandler.CancelJumpSequence);
    this.Size = guiButton.Size;
    this.Align = Align.MiddleCenter;
    this.Position = new Vector2(-110f, -50f);
  }

  public void Hide()
  {
    Game.UnregisterDialog((IGUIPanel) this);
  }
}
