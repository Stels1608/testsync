﻿// Decompiled with JetBrains decompiler
// Type: ShipSystemPaintCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class ShipSystemPaintCard : Card
{
  public string paintTexture = string.Empty;
  private string model = string.Empty;
  public ShipCard shipCard;

  public bool UsesDefaultModel
  {
    get
    {
      return this.model == "default";
    }
  }

  public string PrefabName
  {
    get
    {
      if (this.UsesDefaultModel)
        return this.shipCard.WorldCard.PrefabName.ToLower();
      return this.model.ToLower();
    }
  }

  public ShipSystemPaintCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.paintTexture = r.ReadString();
    this.model = r.ReadString();
    r.ReadLength();
    this.shipCard = (ShipCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.Ship);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.shipCard
    });
    this.IsLoaded.Set();
  }
}
