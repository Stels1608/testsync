﻿// Decompiled with JetBrains decompiler
// Type: ImageUtils
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class ImageUtils
{
  public static Color BytesToColor(byte r, byte g, byte b, byte a)
  {
    return new Color(Mathf.Clamp01((float) r / (float) byte.MaxValue), Mathf.Clamp01((float) g / (float) byte.MaxValue), Mathf.Clamp01((float) b / (float) byte.MaxValue), Mathf.Clamp01((float) a / (float) byte.MaxValue));
  }

  public static Color BytesToColor(byte r, byte g, byte b)
  {
    return new Color(Mathf.Clamp01((float) r / (float) byte.MaxValue), Mathf.Clamp01((float) g / (float) byte.MaxValue), Mathf.Clamp01((float) b / (float) byte.MaxValue));
  }

  public static Color HexStringToColor(string hexColor)
  {
    string hexDigits = ImageUtils.ExtractHexDigits(hexColor);
    if (hexDigits.Length != 6)
      throw new ArgumentException("hexColor is not exactly 6 digits.");
    string s1 = hexDigits.Substring(0, 2);
    string s2 = hexDigits.Substring(2, 2);
    string s3 = hexDigits.Substring(4, 2);
    Color color = new Color();
    try
    {
      return ImageUtils.BytesToColor(byte.Parse(s1, NumberStyles.HexNumber), byte.Parse(s2, NumberStyles.HexNumber), byte.Parse(s3, NumberStyles.HexNumber));
    }
    catch
    {
      return Color.clear;
    }
  }

  private static string ExtractHexDigits(string input)
  {
    Regex regex = new Regex("[abcdefABCDEF\\d]+");
    string str = string.Empty;
    foreach (char ch in input)
    {
      if (regex.IsMatch(ch.ToString()))
        str += ch.ToString();
    }
    return str;
  }

  public static Texture2D MergeTexture(float width, Texture2D left, Texture2D center, Texture2D right)
  {
    float num1 = width - (float) left.width - (float) right.width;
    float num2 = Mathf.Ceil(num1 / (float) center.width);
    Texture2D texture2D = new Texture2D((int) width, left.height);
    texture2D.SetPixels(0, 0, left.width, left.height, left.GetPixels());
    Color[] pixels = center.GetPixels();
    for (int index = 0; (double) index < (double) num2; ++index)
      texture2D.SetPixels(left.width + index * center.width, 0, center.width, center.height, pixels);
    texture2D.SetPixels((int) ((double) left.width + (double) num1), 0, right.width, right.height, right.GetPixels());
    texture2D.Apply();
    return texture2D;
  }

  public static Texture2D MergeTexturesVertically(int targetHeight, bool forceShowFirstAndLastAsMinimum, params Texture2D[] textureElement)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ImageUtils.\u003CMergeTexturesVertically\u003Ec__AnonStorey115 verticallyCAnonStorey115 = new ImageUtils.\u003CMergeTexturesVertically\u003Ec__AnonStorey115();
    if (textureElement == null || textureElement.Length == 0)
    {
      Debug.LogError((object) (Utils.GetTime() + "MergeTexturesVertically() called but no textureElement passed"));
      return (Texture2D) null;
    }
    if (textureElement.Length == 1)
      return textureElement[0];
    // ISSUE: reference to a compiler-generated field
    verticallyCAnonStorey115.tileElementIndex = textureElement.Length - 2;
    int index1 = textureElement.Length - 1;
    // ISSUE: reference to a compiler-generated field
    Texture2D texture2D1 = textureElement[verticallyCAnonStorey115.tileElementIndex];
    Texture2D texture2D2 = textureElement[index1];
    if (textureElement[0].height + textureElement[index1].height > targetHeight && !forceShowFirstAndLastAsMinimum)
    {
      int blockHeight1 = targetHeight / 2;
      int blockHeight2 = targetHeight - blockHeight1;
      Texture2D texture2D3 = new Texture2D(textureElement[0].width, targetHeight);
      texture2D3.SetPixels(0, targetHeight - blockHeight1, textureElement[0].width, blockHeight1, textureElement[0].GetPixels(0, textureElement[0].height - blockHeight1, textureElement[0].width, blockHeight1));
      texture2D3.SetPixels(0, 0, texture2D2.width, blockHeight2, texture2D2.GetPixels(0, 0, texture2D2.width, blockHeight2));
      texture2D3.Apply();
      return texture2D3;
    }
    // ISSUE: reference to a compiler-generated method
    int num = Math.Max(((IEnumerable<Texture2D>) textureElement).Where<Texture2D>(new Func<Texture2D, int, bool>(verticallyCAnonStorey115.\u003C\u003Em__29D)).Sum<Texture2D>((Func<Texture2D, int>) (t => t.height)), targetHeight);
    Texture2D texture2D4 = new Texture2D(textureElement[0].width, num);
    // ISSUE: reference to a compiler-generated field
    for (int index2 = 0; index2 < verticallyCAnonStorey115.tileElementIndex; ++index2)
    {
      num -= textureElement[index2].height;
      texture2D4.SetPixels(0, num, textureElement[index2].width, textureElement[index2].height, textureElement[index2].GetPixels());
    }
    while (num - texture2D2.height - texture2D1.height >= 0)
    {
      num -= texture2D1.height;
      texture2D4.SetPixels(0, num, texture2D1.width, texture2D1.height, texture2D1.GetPixels());
    }
    int blockHeight = num - texture2D2.height;
    if (blockHeight > 0)
    {
      int y = num - blockHeight;
      texture2D4.SetPixels(0, y, texture2D1.width, blockHeight, texture2D1.GetPixels());
    }
    texture2D4.SetPixels(0, 0, texture2D2.width, texture2D2.height, texture2D2.GetPixels());
    texture2D4.Apply();
    return texture2D4;
  }

  public static Texture2D ApplyIcon(Texture2D texture, Texture2D icon, Rect iconBounds)
  {
    texture.SetPixels((int) iconBounds.x, (int) iconBounds.y, icon.width, icon.height, icon.GetPixels());
    texture.Apply();
    return texture;
  }

  public static Texture2D CloneTexture(Texture2D source)
  {
    Texture2D texture2D = new Texture2D(source.width, source.height);
    texture2D.SetPixels(0, 0, source.width, source.height, source.GetPixels());
    return texture2D;
  }

  public static Texture2D CreateTexture(Vector2i size, Color color)
  {
    Texture2D texture2D = new Texture2D(size.x, size.y);
    for (int x = 0; x < size.x; ++x)
    {
      for (int y = 0; y < size.y; ++y)
        texture2D.SetPixel(x, y, color);
    }
    texture2D.Apply();
    return texture2D;
  }

  public static void RotateAroundPivot(float angle, Vector2 pivotPoint)
  {
    Matrix4x4 matrix = GUI.matrix;
    GUI.matrix = Matrix4x4.identity;
    Vector2 screenPoint = GUIUtility.GUIToScreenPoint(pivotPoint);
    Matrix4x4 matrix4x4 = Matrix4x4.TRS((Vector3) screenPoint, Quaternion.Euler(0.0f, 0.0f, angle), Vector3.one) * Matrix4x4.TRS(-(Vector3) screenPoint, Quaternion.identity, Vector3.one);
    GUI.matrix = matrix * matrix4x4;
  }

  public static Texture2D Rotate(ref Texture2D source, float angle, int targetWidth, int targetHeight, int startTolerance, int endTolerance)
  {
    angle *= (float) Math.PI / 180f;
    Texture2D texture = ImageUtils.CreateTexture(new Vector2i(targetWidth, targetHeight), Color.clear);
    for (int x1 = 0 + startTolerance; x1 < source.width - endTolerance; ++x1)
    {
      for (int y1 = 0 + startTolerance; y1 < source.height - endTolerance; ++y1)
      {
        int x2 = (int) ((double) x1 * (double) Mathf.Cos(angle) + (double) y1 * (double) Mathf.Sin(angle));
        int y2 = (int) ((double) -x1 * (double) Mathf.Sin(angle) + (double) y1 * (double) Mathf.Cos(angle));
        texture.SetPixel(x2, y2, source.GetPixel(x1, y1));
      }
    }
    texture.Apply();
    return texture;
  }

  public static Texture2D Rotate(ref Texture2D source, float angle)
  {
    return ImageUtils.Rotate(ref source, angle, source.width, source.height, 0, 0);
  }

  public static Texture2D Rotate(ref Texture2D source, float angle, int targetWidth, int targetHeight)
  {
    return ImageUtils.Rotate(ref source, angle, source.width, source.height, 0, 0);
  }

  public static Texture2D Mirror(ref Texture2D source, ImageUtils.MirrorAxis axis)
  {
    Texture2D texture2D = new Texture2D(source.width, source.height);
    if (axis == ImageUtils.MirrorAxis.HORIZONTAL)
    {
      for (int x = 0; x < source.width; ++x)
      {
        for (int y1 = 0; y1 < source.height; ++y1)
        {
          int y2 = source.height - y1;
          texture2D.SetPixel(x, y2, source.GetPixel(x, y1));
        }
      }
    }
    else if (axis == ImageUtils.MirrorAxis.VERTICAL)
    {
      for (int x1 = 0; x1 < source.width; ++x1)
      {
        for (int y = 0; y < source.height; ++y)
        {
          int x2 = source.width - x1;
          texture2D.SetPixel(x2, y, source.GetPixel(x1, y));
        }
      }
    }
    texture2D.Apply();
    return texture2D;
  }

  public enum MirrorAxis
  {
    HORIZONTAL,
    VERTICAL,
  }
}
