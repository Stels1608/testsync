﻿// Decompiled with JetBrains decompiler
// Type: CylonColorSchema
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CylonColorSchema : WidgetColorScheme
{
  public CylonColorSchema()
  {
    this.Set(WidgetColorType.WINDOW_STRIPE, ImageUtils.HexStringToColor("b30738"));
    this.Set(WidgetColorType.FACTION_COLOR, ImageUtils.HexStringToColor("d04545"));
    this.Set(WidgetColorType.FACTION_CLICK_COLOR, ImageUtils.HexStringToColor("eacfcf"));
    this.Set(WidgetColorType.FACTION_DISABLED_COLOR, Color.grey);
    this.Set(WidgetColorType.UGUI_FACTION_COLOR_NORMAL, ImageUtils.HexStringToColor("892C2C"));
    this.Set(WidgetColorType.UGUI_FACTION_COLOR_CLICK, ImageUtils.HexStringToColor("FFD8DC"));
    this.Set(WidgetColorType.UGUI_FACTION_COLOR_DISABLED, Color.grey);
    this.Set(WidgetColorType.DIALOG_NPC_NAME, this.Get(WidgetColorType.FACTION_COLOR));
    this.Set(WidgetColorType.DIALOG_ANSWER_COLOR_NORMAL, this.Get(WidgetColorType.FACTION_CLICK_COLOR));
  }
}
