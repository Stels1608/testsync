﻿// Decompiled with JetBrains decompiler
// Type: ShipPaintSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

internal class ShipPaintSlot : ShipCustomizationSlot
{
  private readonly ShipSystem shipPaint;
  private readonly System.Action<ShipSystem> updateShipPaint;
  private readonly GuiLabel installMessage;

  public ShipPaintSlot(ShipSystem shipPaint, System.Action<ShipSystem> updateShipPaint)
  {
    this.shipPaint = shipPaint;
    this.updateShipPaint = updateShipPaint;
    GuiImage guiImage = new GuiImage();
    if (shipPaint != null)
      guiImage.Texture = shipPaint.ItemGUICard.GUIIconTexture;
    guiImage.PositionX += 20f;
    guiImage.Size = new Vector2(40f, 40f);
    this.AddChild((GuiElementBase) guiImage);
    GuiLabel guiLabel = new GuiLabel("%$bgo.ship_customization.default_paint_name%", Gui.Options.FontBGM_BT, 14);
    if (shipPaint != null)
      guiLabel.Text = shipPaint.ItemGUICard.Name;
    guiLabel.Size = new Vector2(300f, 40f);
    guiLabel.PositionX += 66f;
    guiLabel.PositionY += 5f;
    this.AddChild((GuiElementBase) guiLabel, Align.UpLeft);
    this.installMessage = new GuiLabel();
    this.installMessage.Size = new Vector2(300f, 40f);
    this.installMessage.PositionX += 66f;
    this.installMessage.PositionY -= 5f;
    this.AddChild((GuiElementBase) this.installMessage, Align.DownLeft);
  }

  public void SetInstallMessage(bool isShipUpgraded)
  {
    this.installMessage.Text = "%$bgo.ship_customization.install_message%";
    this.installMessage.AllColor = Gui.Options.NormalColor;
  }

  public override void OnSelected()
  {
    if (this.updateShipPaint == null)
      return;
    this.updateShipPaint(this.shipPaint);
  }
}
