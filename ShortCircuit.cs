﻿// Decompiled with JetBrains decompiler
// Type: ShortCircuit
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ShortCircuit : ModelScript
{
  private float emissionRate = 100f;
  private float emissionShapeSwitchSpeed = 0.5f;
  private const string PREFAB = "Fx/ShortCircuit";
  private Mesh[] emissionShapes;
  private float minSize;
  private float maxSize;
  private float _timeToEmission;
  private ParticleSystem _particleSystem;

  private float EmissionPeriod
  {
    get
    {
      return 1f / this.emissionRate;
    }
  }

  private void Start()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(Resources.Load<GameObject>("Fx/ShortCircuit"));
    gameObject.transform.parent = this.SpaceObject.Model.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.Euler(0.0f, 180f, 0.0f);
    this.emissionShapes = this.GetEmittingMeshes(this.SpaceObject);
    if (this.emissionShapes == null || this.emissionShapes.Length == 0)
    {
      Debug.LogWarning((object) "No Emission shapes found!");
      this.enabled = false;
      Object.Destroy((Object) this);
    }
    else
    {
      this.minSize = (float) (((double) this.SpaceObject.MeshBoundsRaw.size.x + (double) this.SpaceObject.MeshBoundsRaw.size.y + (double) this.SpaceObject.MeshBoundsRaw.size.z) / 3.0) * 0.2f;
      this.maxSize = this.minSize * 2f;
      this._particleSystem = gameObject.GetComponent<ParticleSystem>();
      this._particleSystem.emissionRate = 0.0f;
      this._timeToEmission = this.EmissionPeriod;
    }
  }

  private Mesh[] GetEmittingMeshes(SpaceObject spaceObject)
  {
    Body modelScript1 = spaceObject.GetModelScript<Body>();
    if ((Object) modelScript1 != (Object) null)
    {
      MeshFilter component = modelScript1.ActiveRenderer.GetComponent<MeshFilter>();
      if ((Object) component != (Object) null)
        return new Mesh[1]{ component.mesh };
    }
    MultiBody modelScript2 = spaceObject.GetModelScript<MultiBody>();
    if (!((Object) modelScript2 != (Object) null))
      return (Mesh[]) null;
    Renderer[] activeRenderers = modelScript2.ActiveRenderers;
    List<Mesh> meshList = new List<Mesh>();
    foreach (Component component1 in activeRenderers)
    {
      MeshFilter component2 = component1.GetComponent<MeshFilter>();
      if ((Object) component2 != (Object) null)
        meshList.Add(component2.mesh);
    }
    return meshList.ToArray();
  }

  protected override void OnDestroy()
  {
    if (!((Object) this._particleSystem != (Object) null))
      return;
    Object.Destroy((Object) this._particleSystem.gameObject);
  }

  private void Update()
  {
    int index1 = Random.Range(0, this.emissionShapes.Length);
    this._timeToEmission -= Time.deltaTime;
    while ((double) this._timeToEmission <= 0.0)
    {
      this._timeToEmission = this.EmissionPeriod - this._timeToEmission;
      Mesh mesh = this.emissionShapes[index1];
      int num1 = Random.Range(0, mesh.triangles.Length / 3);
      int index2 = mesh.triangles[num1 * 3];
      int index3 = mesh.triangles[num1 * 3 + 1];
      int index4 = mesh.triangles[num1 * 3 + 2];
      Vector3 vector3_1 = mesh.vertices[index2];
      Vector3 vector3_2 = mesh.vertices[index3];
      Vector3 vector3_3 = mesh.vertices[index4];
      Vector3 vector3_4 = mesh.normals[index2];
      Vector3 vector3_5 = mesh.normals[index3];
      Vector3 vector3_6 = mesh.normals[index4];
      float num2 = Random.Range(0.0f, 1f);
      float num3 = Random.Range(0.0f, 1f);
      float num4 = Random.Range(0.0f, 1f);
      float num5 = num2 + num3 + num4;
      float num6 = num2 / num5;
      float num7 = num3 / num5;
      float num8 = num4 / num5;
      Vector3 vector3_7 = vector3_1 * num6 + vector3_2 * num7 + vector3_3 * num8;
      Vector3 vector3_8 = vector3_4 * num6 + vector3_5 * num7 + vector3_6 * num8;
      ParticleSystem.Particle particle = new ParticleSystem.Particle();
      particle.position = vector3_7;
      particle.rotation = (float) Random.Range(45, 315);
      particle.velocity = vector3_8 * this._particleSystem.startSpeed;
      particle.size = Random.Range(this.minSize, this.maxSize);
      // ISSUE: explicit reference operation
      // ISSUE: variable of a reference type
      ParticleSystem.Particle& local = @particle;
      float startLifetime = this._particleSystem.startLifetime;
      particle.startLifetime = startLifetime;
      double num9 = (double) startLifetime;
      // ISSUE: explicit reference operation
      (^local).lifetime = (float) num9;
      particle.color = (Color32) this._particleSystem.startColor;
      this._particleSystem.Emit(particle);
    }
  }
}
