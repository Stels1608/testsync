﻿// Decompiled with JetBrains decompiler
// Type: GUISlotPlayer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GUISlotPlayer : GUISlotWithBuffs
{
  private GUIBlinker blinker;

  public bool IsBlinking
  {
    get
    {
      return this.blinker.IsRendered;
    }
    set
    {
      this.blinker.IsRendered = value;
    }
  }

  private float Health
  {
    get
    {
      return Game.Me.Stats.HullPoints / Game.Me.Stats.MaxHullPoints;
    }
  }

  private float Shields
  {
    get
    {
      return Game.Me.Stats.PowerPoints / Game.Me.Stats.MaxPowerPoints;
    }
  }

  public GUISlotPlayer(SmartRect parent)
    : base(parent)
  {
    this.NameString = Game.Me.Name;
    this.blinker = new GUIBlinker();
    this.blinker.Position = new float2(20f, -6f);
    this.blinker.Width *= 1.5f;
    this.blinker.Height *= 1.5f;
    this.InsertPanel((GUIPanel) this.blinker);
    this.IsBlinking = false;
  }

  public void SetName(string name)
  {
    this.NameString = name;
  }

  public override void Update()
  {
    base.Update();
    this.RedLevel = this.Health;
    this.BlueLevel = this.Shields;
    this.timeToUpdateBuffs -= (float) (double) Time.deltaTime;
    if ((double) this.timeToUpdateBuffs <= 0.0)
    {
      this.buffs.Clear();
      int num = 0;
      using (List<ShipBuff>.Enumerator enumerator = Game.Me.Stats.DisplayedShipBuffs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ShipBuff current = enumerator.Current;
          if ((bool) current.IsLoaded && (double) current.TimeLeft > 0.0)
          {
            this.buffs.Add(current);
            ++num;
          }
          if (num >= 10)
            break;
        }
      }
      if (this.healthLevel.IsRendered)
        this.SetHealthString((int) Game.Me.Stats.HullPoints, (int) Game.Me.Stats.MaxHullPoints);
      if (this.energyLevel.IsRendered)
        this.SetEnergyString((int) Game.Me.Stats.PowerPoints, (int) Game.Me.Stats.MaxPowerPoints);
      this.timeToUpdateBuffs = 0.1f;
    }
    this.drawBuffs = this.buffs.Count > 0;
    this.timeToUpdateLevel -= (float) (double) Time.deltaTime;
    if ((double) this.timeToUpdateLevel > 0.0)
      return;
    this.leaderMark.IsRendered = Game.Me.Party.IsLeader;
    this.LevelString = Game.Me.Level.ToString();
    this.timeToUpdateLevel = 0.1f;
  }

  protected override void UpdateGUISlotAvatar(GUISlotAvatar avatar)
  {
    if (Game.Me.ActualShip != null)
    {
      avatar.Texture = Game.Me.ActualShip.Avatar;
    }
    else
    {
      avatar.Texture = this.humanAvatar;
      if (Game.Me.Faction == Faction.Cylon)
        avatar.Texture = this.cylonAvatar;
    }
    avatar.IsRendered = true;
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey != KeyCode.Mouse1 || !this.avatar.Contains(mousePosition))
      return false;
    this.avatarClicked = true;
    return true;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (!this.avatarClicked || !this.avatar.Contains(mousePosition) || mouseKey != KeyCode.Mouse1)
      return false;
    GUISlotPopup guiSlotPopup = Game.GUIManager.Find<GUISlotManager>().Popup;
    guiSlotPopup.Clear();
    if (Game.Me.Party.HasParty)
      guiSlotPopup.AddLeave();
    guiSlotPopup.Parent = this.avatar.SmartRect;
    guiSlotPopup.PlaceRightDownCornerOf((GUIPanel) this.avatar, -20f);
    guiSlotPopup.IsRendered = true;
    if (Game.Me.ActiveShip.IsCapitalShip)
      guiSlotPopup.duel.IsRendered = false;
    this.avatarClicked = false;
    return true;
  }

  public override bool OnShowTooltip(float2 mousePosition)
  {
    if (base.OnShowTooltip(mousePosition))
      return true;
    if (!this.name.Contains(mousePosition) || !this.nameWasCut)
      return false;
    this.name.SetTooltip(Game.Me.Name);
    Game.TooltipManager.ShowTooltip(this.name.AdvancedTooltip);
    return true;
  }
}
