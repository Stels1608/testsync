﻿// Decompiled with JetBrains decompiler
// Type: TournamentCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class TournamentCard : Card
{
  private TournamentType tournamentType;
  private byte minLevel;
  private List<uint> allowedTiers;

  public List<uint> AllowedTiers
  {
    get
    {
      return this.allowedTiers;
    }
  }

  public byte MinLevel
  {
    get
    {
      return this.minLevel;
    }
  }

  public TournamentType TournamentType
  {
    get
    {
      return this.tournamentType;
    }
  }

  public TournamentCard(uint cardGuid)
    : base(cardGuid)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.tournamentType = (TournamentType) r.ReadByte();
    this.minLevel = r.ReadByte();
    this.allowedTiers = r.ReadUInt32List();
  }
}
