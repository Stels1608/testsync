﻿// Decompiled with JetBrains decompiler
// Type: CutsceneScreenFader
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CutsceneScreenFader : MonoBehaviour
{
  private Color drawColor = new Color(0.0f, 0.0f, 0.0f, 1f);
  public float FadeLevel;
  private Texture2D blackTex;

  public void Awake()
  {
    this.blackTex = ImageUtils.CreateTexture(Vector2i.one, this.drawColor);
  }

  private void OnGUI()
  {
    this.drawColor.a = this.FadeLevel;
    float height = (float) Screen.height;
    float width = (float) Screen.width;
    Color color = GUI.color;
    GUI.color = this.drawColor;
    GUI.DrawTexture(new Rect(0.0f, 0.0f, width, height), (Texture) this.blackTex);
    GUI.color = color;
  }

  public void FadeIn(float delay, float duration)
  {
    this.StartCoroutine(this.ProcessFadeIn(delay, duration));
  }

  [DebuggerHidden]
  private IEnumerator ProcessFadeIn(float delay, float duration)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CutsceneScreenFader.\u003CProcessFadeIn\u003Ec__Iterator10() { delay = delay, duration = duration, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003Eduration = duration, \u003C\u003Ef__this = this };
  }

  public void FadeOut(float delay, float duration)
  {
    this.StartCoroutine(this.ProcessFadeOut(delay, duration));
  }

  [DebuggerHidden]
  private IEnumerator ProcessFadeOut(float delay, float duration)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CutsceneScreenFader.\u003CProcessFadeOut\u003Ec__Iterator11() { delay = delay, duration = duration, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003Eduration = duration, \u003C\u003Ef__this = this };
  }

  public void TurnToBlack()
  {
    this.FadeLevel = 1f;
  }

  private void UpdateFadeInOut(float newAlpha)
  {
    this.FadeLevel = newAlpha;
  }
}
