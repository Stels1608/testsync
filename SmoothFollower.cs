﻿// Decompiled with JetBrains decompiler
// Type: SmoothFollower
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SmoothFollower
{
  private Vector3 targetPosition;
  private Vector3 position;
  private Vector3 velocity;
  private float smoothingTime;
  private float prediction;

  public SmoothFollower(float smoothingTime)
  {
    this.targetPosition = Vector3.zero;
    this.position = Vector3.zero;
    this.velocity = Vector3.zero;
    this.smoothingTime = smoothingTime;
    this.prediction = 1f;
  }

  public SmoothFollower(float smoothingTime, float prediction)
  {
    this.targetPosition = Vector3.zero;
    this.position = Vector3.zero;
    this.velocity = Vector3.zero;
    this.smoothingTime = smoothingTime;
    this.prediction = prediction;
  }

  public Vector3 Update(Vector3 targetPositionNew, float deltaTime)
  {
    Vector3 vector3 = (targetPositionNew - this.targetPosition) / deltaTime;
    this.targetPosition = targetPositionNew;
    float num = Mathf.Min(1f, deltaTime / this.smoothingTime);
    this.velocity = this.velocity * (1f - num) + (this.targetPosition + vector3 * this.prediction - this.position) * num;
    this.position += this.velocity * Time.deltaTime;
    return this.position;
  }

  public Vector3 Update(Vector3 targetPositionNew, float deltaTime, bool reset)
  {
    if (!reset)
      return this.Update(targetPositionNew, deltaTime);
    this.targetPosition = targetPositionNew;
    this.position = targetPositionNew;
    this.velocity = Vector3.zero;
    return this.position;
  }

  public Vector3 GetPosition()
  {
    return this.position;
  }

  public Vector3 GetVelocity()
  {
    return this.velocity;
  }
}
