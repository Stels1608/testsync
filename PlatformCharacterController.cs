﻿// Decompiled with JetBrains decompiler
// Type: PlatformCharacterController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlatformCharacterController : MonoBehaviour
{
  public float walkMultiplier = 0.5f;
  protected CharacterMotor motor;
  public bool defaultIsWalk;

  public virtual void Start()
  {
    this.motor = this.GetComponent(typeof (CharacterMotor)) as CharacterMotor;
    if (!((Object) this.motor == (Object) null))
      return;
    Debug.Log((object) "Motor is null!!");
  }

  public virtual void Update()
  {
    Vector3 vector3 = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0.0f);
    if ((double) vector3.y == 0.0)
    {
      this.motor.desiredMovementDirection = new Vector3();
      this.transform.Rotate(new Vector3(0.0f, 1f, 0.0f), vector3.x * 0.05f);
      if ((double) vector3.x > 0.0)
        this.GetComponent<Animation>().CrossFade("turn_right");
      else if ((double) vector3.x < 0.0)
        this.GetComponent<Animation>().CrossFade("turn_left");
      else
        this.GetComponent<Animation>().CrossFade("idle");
    }
    else
    {
      if ((Object) this.GetComponent<Animation>() != (Object) null)
        this.GetComponent<Animation>().CrossFade("run");
      if ((double) vector3.magnitude > 1.0)
        vector3 = vector3.normalized;
      vector3 = vector3.normalized * Mathf.Pow(vector3.magnitude, 2f);
      vector3 = Camera.main.transform.rotation * vector3;
      vector3 = Quaternion.FromToRotation(Camera.main.transform.forward * -1f, this.transform.up) * vector3;
      vector3 = Quaternion.Inverse(this.transform.rotation) * vector3;
      if ((double) this.walkMultiplier != 1.0 && (Input.GetKey("left shift") ? 1 : (Input.GetKey("right shift") ? 1 : 0)) != (this.defaultIsWalk ? 1 : 0))
        vector3 *= this.walkMultiplier;
      this.motor.desiredMovementDirection = vector3;
    }
  }
}
