﻿// Decompiled with JetBrains decompiler
// Type: TargetLaunchManeuver
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TargetLaunchManeuver : Maneuver
{
  protected uint launcherID;
  protected uint targetID;
  protected ushort objectPointHash;
  protected float relativeSpeed;

  public override MovementFrame NextFrame(Tick tick, MovementFrame prevFrame)
  {
    if (tick == this.startTick)
      return this.GetLaunchFrame(tick);
    return this.GetFollowFrame(tick, prevFrame);
  }

  protected MovementFrame GetLaunchFrame(Tick launchTick)
  {
    SpaceObject spaceObject = SpaceLevel.GetLevel().GetObjectRegistry().Get(this.launcherID);
    if (spaceObject == null)
      return MovementFrame.Invalid;
    MovementFrame tickFrame = spaceObject.MovementController.GetTickFrame(launchTick - 1);
    if (!tickFrame.valid)
    {
      Log.Add(spaceObject.ToString());
      Log.Add("Now " + (object) Tick.Current);
      Log.Add("Invalid frame" + (object) launchTick);
      return MovementFrame.Invalid;
    }
    Spot objectPoint = spaceObject.GetObjectPoint(this.objectPointHash);
    Vector3 vector3_1 = objectPoint == null ? Vector3.zero : objectPoint.LocalPosition;
    Quaternion quaternion = objectPoint == null ? Quaternion.identity : objectPoint.LocalRotation;
    Quaternion rotation = tickFrame.NextEuler3.rotation;
    Vector3 direction = rotation * quaternion * Vector3.forward;
    Vector3 vector3_2 = direction * this.relativeSpeed;
    return new MovementFrame(tickFrame.nextPosition + rotation * vector3_1, Euler3.Direction(direction).Normalized(false), tickFrame.linearSpeed + vector3_2, Vector3.zero, Euler3.zero);
  }

  protected MovementFrame GetFollowFrame(Tick tick, MovementFrame prevFrame)
  {
    SpaceObject spaceObject = SpaceLevel.GetLevel().GetObjectRegistry().Get(this.targetID);
    if (spaceObject == null)
      return this.Drift(prevFrame);
    Euler3 direction = Euler3.Direction(spaceObject.MovementController.GetTickFrame(tick - 1).position - prevFrame.position);
    return this.MoveToDirection(prevFrame, direction);
  }

  public override string ToString()
  {
    return string.Format("TargetaunchManeuver: launcher={0}, target={1}, objectPointHash={2}, relativeSpeed={3}", (object) this.launcherID, (object) this.targetID, (object) this.objectPointHash, (object) this.relativeSpeed);
  }

  public override void Read(BgoProtocolReader pr)
  {
    base.Read(pr);
    this.startTick = pr.ReadTick();
    this.launcherID = pr.ReadUInt32();
    this.targetID = pr.ReadUInt32();
    this.objectPointHash = pr.ReadUInt16();
    this.relativeSpeed = pr.ReadSingle();
    this.options.Read(pr);
  }
}
