﻿// Decompiled with JetBrains decompiler
// Type: DialogExitBtn
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DialogExitBtn : ButtonWidget
{
  private const string SPRITE_NAME = "btn_dialog_exit";

  public override void Start()
  {
    base.Start();
    if ((Object) this.bgSprite == (Object) null)
      this.bgSprite = this.GetComponent<UISprite>();
    this.bgSprite.spriteName = "btn_dialog_exit";
    this.bgSprite.color = ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_ANSWER_COLOR_NORMAL);
  }

  public override void OnHover(bool isOver)
  {
    base.OnHover(isOver);
    if (!((Object) this.bgSprite != (Object) null))
      return;
    this.bgSprite.color = !isOver ? ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_ANSWER_COLOR_NORMAL) : ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_ANSWER_COLOR_HOVER);
  }
}
