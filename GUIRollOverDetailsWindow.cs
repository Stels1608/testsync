﻿// Decompiled with JetBrains decompiler
// Type: GUIRollOverDetailsWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

public class GUIRollOverDetailsWindow : GUIShopTooltipBox
{
  private GUIImageNew itemImage;
  private GUILabelNew titleLabel;
  private GUILabelNew shortDescLabel;
  private GUILabelNew subtitleLabel;
  private ShipItem item;
  private AtlasCache atlasCache;

  public GUIRollOverDetailsWindow(SmartRect parent)
    : base(parent)
  {
    this.IsRendered = false;
    JWindowDescription jwindowDescription = Loaders.LoadResource("GUI/EquipBuyPanel/gui_slot_layout");
    GUIImageNew guiImageNew = new GUIImageNew(ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/StarterPack/Rollrover_Panel"));
    this.root.Width = guiImageNew.Rect.width;
    this.root.Height = guiImageNew.Rect.height;
    this.AddPanel((GUIPanel) guiImageNew);
    this.titleLabel = (jwindowDescription["title_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.titleLabel);
    this.shortDescLabel = (jwindowDescription["short_desc_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.shortDescLabel);
    this.subtitleLabel = (jwindowDescription["subtitle_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.subtitleLabel);
    this.itemImage = (jwindowDescription["item_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.itemImage);
    this.atlasCache = new AtlasCache(new float2(40f, 35f));
  }

  public void TryToShow(float2 absPos, ShipItem item)
  {
    this.item = item;
    this.Appear(absPos);
  }

  public override void Hide()
  {
    this.item = (ShipItem) null;
    this.Disappear();
  }

  public override void Update()
  {
    if (this.item != null && (bool) this.item.IsLoaded)
    {
      this.titleLabel.Text = this.item.ItemGUICard.Name;
      if (this.item is ShipSystem)
        this.subtitleLabel.Text = "%$bgo.shop.level%" + (object) (this.item as ShipSystem).Card.Level + "   %$bgo.shop.max_level%" + (object) (this.item as ShipSystem).Card.MaxLevel;
      else
        this.subtitleLabel.Text = string.Empty;
      AtlasEntry cachedEntryBy = this.atlasCache.GetCachedEntryBy(this.item.ItemGUICard.GUIAtlasTexturePath, (int) this.item.ItemGUICard.FrameIndex);
      this.itemImage.Texture = cachedEntryBy.Texture;
      this.itemImage.InnerRect = cachedEntryBy.FrameRect;
      this.itemImage.IsRendered = true;
      this.titleLabel.Text = this.item.ItemGUICard.Name;
      this.shortDescLabel.Text = this.item.ItemGUICard.ShortDescription;
    }
    base.Update();
  }
}
