﻿// Decompiled with JetBrains decompiler
// Type: DradisHelper
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public static class DradisHelper
{
  public static bool IsAlwaysInMapRange(ISpaceEntity target)
  {
    if (target.IsFriendlyHub && target.SpaceEntityType != SpaceEntityType.Outpost || target.TargetType == TargetType.SpacePosition)
      return true;
    switch (target.SpaceEntityType)
    {
      case SpaceEntityType.Asteroid:
      case SpaceEntityType.AsteroidBot:
      case SpaceEntityType.Planet:
      case SpaceEntityType.Planetoid:
      case SpaceEntityType.SectorEvent:
      case SpaceEntityType.Comet:
        return true;
      default:
        return false;
    }
  }

  public static bool IsInDetectorsMapRange(ObjectStats detectorStats, SpaceObject detector, SpaceObject target)
  {
    float detectionOuterRadius = detectorStats.DetectionOuterRadius;
    float num1 = detectionOuterRadius * detectionOuterRadius;
    float detectionVisualRadius = detectorStats.DetectionVisualRadius;
    float num2 = detectionVisualRadius * detectionVisualRadius;
    float num3 = !detector.IsMe ? (detector.Position - target.Position).sqrMagnitude : target.GetSqrDistance();
    if ((double) num3 < (double) num2)
      return true;
    if ((double) num3 > (double) num1)
      return false;
    return !target.IsCloaked;
  }

  public static bool IsInDetectorsDradisRange(ObjectStats detectorStats, SpaceObject detector, SpaceObject target)
  {
    float detectionInnerRadius = detectorStats.DetectionInnerRadius;
    float num1 = detectionInnerRadius * detectionInnerRadius;
    float detectionVisualRadius = detectorStats.DetectionVisualRadius;
    float num2 = detectionVisualRadius * detectionVisualRadius;
    float num3 = !detector.IsMe ? (detector.Position - target.Position).sqrMagnitude : target.GetSqrDistance();
    if ((double) num3 < (double) num2)
      return true;
    if ((double) num3 > (double) num1)
      return false;
    return !target.IsCloaked;
  }

  public static void UpdateObjectFlags(SpaceObject obj)
  {
    PlayerShip ship = Game.Me.Ship;
    if (ship == null)
      return;
    float magnitude = (obj.Position - ship.Position).magnitude;
    obj.SetDistance(magnitude);
    obj.IsInPlayersMapRange = DradisHelper.IsAlwaysInMapRange((ISpaceEntity) obj) || DradisHelper.IsInDetectorsMapRange((ObjectStats) Game.Me.Stats, (SpaceObject) ship, obj);
    obj.IsInDradisRange = DradisHelper.IsInDetectorsDradisRange((ObjectStats) Game.Me.Stats, (SpaceObject) ship, obj);
  }
}
