﻿// Decompiled with JetBrains decompiler
// Type: TournamentMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public class TournamentMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "TournamentMediator";
  private TournamentRankingWindowWidget rankingWindow;

  public TournamentMediator()
    : base("TournamentMediator")
  {
  }

  public static List<RankEntry> GetDummyData(ushort ownRank)
  {
    List<RankEntry> rankEntryList = new List<RankEntry>();
    rankEntryList.Add(new RankEntry((ushort) 1, (int) ownRank != 1 ? 1U : Game.Me.ServerID, (ushort) 1, "PferdImAnzug", Faction.Cylon, 30440U, 100U, 0U));
    rankEntryList.Add(new RankEntry((ushort) 2, (int) ownRank != 2 ? 2U : Game.Me.ServerID, (ushort) 2, "Osopronosomi", Faction.Colonial, 20100U, 80U, 10U));
    for (ushort index = 3; (int) index < 40; ++index)
    {
      ushort rank = 3;
      string name = "RandomPlayer" + (object) index;
      Faction faction = (double) Random.value <= 0.5 ? Faction.Colonial : Faction.Cylon;
      uint points = (uint) (10000 / (int) index + Random.Range(1, 1000));
      uint kills = 100U / (uint) index;
      uint deaths = (uint) index;
      uint playerId = (int) index != (int) ownRank ? (uint) index * 1000U : Game.Me.ServerID;
      rankEntryList.Add(new RankEntry(index, playerId, rank, name, faction, points, kills, deaths));
    }
    return rankEntryList;
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.TournamentStart);
    this.AddMessageInterest(Message.TournamentStop);
    this.AddMessageInterest(Message.ShowTournamentRanking);
    this.AddMessageInterest(Message.TournamentRankingList);
    this.AddMessageInterest(Message.TournamentOwnRank);
    this.AddMessageInterest(Message.TournamentEnterFailed);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.TournamentRankingList:
        this.ShowRanking((List<RankEntry>) message.Data);
        break;
      case Message.TournamentOwnRank:
        this.SetOwnTournamentRank((ushort) message.Data);
        break;
      case Message.TournamentEnterFailed:
        this.OnTournamentEnterFailed((TournamentValidationError) message.Data);
        break;
      case Message.TournamentStart:
        this.OnTournamentStart((TournamentCard) message.Data);
        break;
      case Message.TournamentStop:
        this.OnTournamentStop((TournamentCard) message.Data);
        break;
      case Message.UiCreated:
        this.AttachView();
        break;
      case Message.ShowTournamentRanking:
        this.RequestScoreboardPosition();
        break;
    }
  }

  private void ShowRanking(List<RankEntry> ranking)
  {
    if (!this.rankingWindow.IsOpen)
      this.rankingWindow.Open();
    this.rankingWindow.ShowRanking(ranking);
  }

  private void SetOwnTournamentRank(ushort rank)
  {
    this.rankingWindow.OwnRank = rank;
    TournamentProtocol.GetProtocol().RequestScoreboard(1U, (ushort) 5);
    TournamentProtocol.GetProtocol().RequestScoreboard((uint) this.rankingWindow.Offset, (ushort) 14);
  }

  private void RequestScoreboardPosition()
  {
    if (this.rankingWindow.IsOpen || !Game.Me.TournamentParticipant)
      return;
    TournamentProtocol.GetProtocol().RequestScoreboardPosition(Game.Me.ServerID);
  }

  private void AttachView()
  {
    this.rankingWindow = ((GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/ui_elements/tournament_ranking/TournamentRanking"))).GetComponent<TournamentRankingWindowWidget>();
    if ((Object) this.rankingWindow == (Object) null)
      Debug.LogError((object) "TournamentRankingWindowWidget was not registered correctly");
    (this.OwnerFacade.FetchDataProvider("GuiDataProvider") as GuiDataProvider).AddWindow((WindowWidget) this.rankingWindow);
  }

  private void OnTournamentStart(TournamentCard tournamentCard)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TournamentMediator.\u003COnTournamentStart\u003Ec__AnonStoreyC7 startCAnonStoreyC7 = new TournamentMediator.\u003COnTournamentStart\u003Ec__AnonStoreyC7();
    // ISSUE: reference to a compiler-generated field
    startCAnonStoreyC7.tournamentCard = tournamentCard;
    // ISSUE: reference to a compiler-generated field
    Game.Tournament.TournamentCard = startCAnonStoreyC7.tournamentCard;
    if (!GameLevel.Instance.CanAccessTournament())
      return;
    this.SetTournamentButtonActive(true);
    // ISSUE: reference to a compiler-generated field
    GuiTournamentSectorGreeting.GuiCard = (GUICard) Game.Catalogue.FetchCard(startCAnonStoreyC7.tournamentCard.CardGUID, CardView.GUI);
    // ISSUE: reference to a compiler-generated method
    Game.DelayedActions.Add(new DelayedActions.Predicate(startCAnonStoreyC7.\u003C\u003Em__1CD));
  }

  private void OnTournamentStop(TournamentCard tournamentCard)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TournamentMediator.\u003COnTournamentStop\u003Ec__AnonStoreyC8 stopCAnonStoreyC8 = new TournamentMediator.\u003COnTournamentStop\u003Ec__AnonStoreyC8();
    // ISSUE: reference to a compiler-generated field
    stopCAnonStoreyC8.tournamentCard = tournamentCard;
    this.SetTournamentButtonActive(false);
    // ISSUE: reference to a compiler-generated method
    Game.DelayedActions.Add(new DelayedActions.Predicate(stopCAnonStoreyC8.\u003C\u003Em__1CE));
  }

  private void SetTournamentButtonActive(bool active)
  {
    SpaceLevel level = SpaceLevel.GetLevel();
    if (!((Object) level == (Object) null) && !level.IsGlobal)
      return;
    SystemButtonWindow systemButtons = Game.GUIManager.SystemButtons;
    if (!((Object) systemButtons != (Object) null))
      return;
    systemButtons.ShowTournament(active);
    systemButtons.SetBlink(StoryProtocol.ControlType.Tournament, active);
  }

  private void OnTournamentEnterFailed(TournamentValidationError reason)
  {
    TournamentCard tournamentCard = Game.Tournament.TournamentCard;
    switch (reason)
    {
      case TournamentValidationError.ShipTierNotAllowed:
        if (tournamentCard.AllowedTiers.Count != 1)
        {
          this.OwnerFacade.SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.tournament.wrong_ship%", NotificationCategory.Neutral));
          break;
        }
        this.ShowTierSpecificErrorMessage((ShipRoleTier) tournamentCard.AllowedTiers[0]);
        break;
      case TournamentValidationError.CarrierNotAllowed:
      case TournamentValidationError.ConquestShipNotAllowed:
        this.ShowTierSpecificErrorMessage(ShipRoleTier.Freighter);
        break;
      case TournamentValidationError.GroupNotAllowed:
        this.OwnerFacade.SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.tournament.no_group%", NotificationCategory.Neutral));
        break;
      case TournamentValidationError.CarrierAnchoredNotAllowed:
        this.OwnerFacade.SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.tournament.no_anchored%", NotificationCategory.Neutral));
        break;
      case TournamentValidationError.InvalidTournamentSector:
        this.OwnerFacade.SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.tournament.wrong_level%", NotificationCategory.Neutral));
        break;
      default:
        this.OwnerFacade.SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("Undefined tournament error: " + (object) reason, NotificationCategory.Neutral));
        break;
    }
  }

  private void ShowTierSpecificErrorMessage(ShipRoleTier tier)
  {
    switch (tier)
    {
      case ShipRoleTier.Strike:
        this.OwnerFacade.SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.tournament.no_strike%", NotificationCategory.Neutral));
        break;
      case ShipRoleTier.Escort:
        this.OwnerFacade.SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.tournament.no_escort%", NotificationCategory.Neutral));
        break;
      case ShipRoleTier.Line:
        this.OwnerFacade.SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.tournament.no_liner%", NotificationCategory.Neutral));
        break;
      default:
        this.OwnerFacade.SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.tournament.wrong_ship%", NotificationCategory.Neutral));
        break;
    }
  }
}
