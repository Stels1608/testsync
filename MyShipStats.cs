﻿// Decompiled with JetBrains decompiler
// Type: MyShipStats
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MyShipStats : SpaceSubscribeInfo
{
  public override bool InCombat
  {
    get
    {
      return base.InCombat;
    }
    set
    {
      FacadeFactory.GetInstance().SendMessage(Message.PlayerInThreat, (object) value);
      base.InCombat = value;
    }
  }

  public MyShipStats(uint objectId)
    : base(objectId)
  {
  }

  protected override void AddBuff(ShipBuff buff)
  {
    base.AddBuff(buff);
    if (!OnScreenNotification.ShowAugmentMessages)
      return;
    using (List<ShipBuff>.Enumerator enumerator = this.ShipBuffs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipBuff current = enumerator.Current;
        if (current != buff && (int) current.AbilityGuid == (int) buff.AbilityGuid)
          return;
      }
    }
    if (buff.Ability == null)
      return;
    FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new BuffNotification(buff));
  }

  protected override void OnStatChanged(ObjectStat stat, float value)
  {
    if (stat != ObjectStat.Speed || !((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null) || SpaceLevel.GetLevel().GUISpeedControl == null)
      return;
    SpaceLevel.GetLevel().GUISpeedControl.ModifySpeed(0.0f);
  }

  protected override void OnShortCircuited(ShipBuff buff)
  {
    base.OnShortCircuited(buff);
    foreach (ShipAbility shipAbility in Game.Me.ActiveShip.AbilityToolbar)
    {
      if (shipAbility != null && shipAbility.card != null)
      {
        if (buff.Ability == null || buff.Ability.AffectedAbilityTypes == null)
          Debug.LogWarning((object) "Buff Ability is NULL");
        else if (buff.Ability.AffectedAbilityTypes.Contains(shipAbility.card.ActionType))
          shipAbility.ShortCircuited = true;
      }
    }
  }

  protected override void OnShortRemoved(uint guid)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MyShipStats.\u003COnShortRemoved\u003Ec__AnonStorey66 removedCAnonStorey66 = new MyShipStats.\u003COnShortRemoved\u003Ec__AnonStorey66();
    // ISSUE: reference to a compiler-generated field
    removedCAnonStorey66.guid = guid;
    // ISSUE: reference to a compiler-generated field
    base.OnShortRemoved(removedCAnonStorey66.guid);
    // ISSUE: reference to a compiler-generated method
    ShipBuff shipBuff = this.ShipBuffs.FirstOrDefault<ShipBuff>(new Func<ShipBuff, bool>(removedCAnonStorey66.\u003C\u003Em__39));
    if (shipBuff == null)
      return;
    foreach (ShipAbility shipAbility in Game.Me.ActiveShip.AbilityToolbar)
    {
      if (shipBuff.Ability.AffectedAbilityTypes.Contains(shipAbility.card.ActionType))
        shipAbility.ShortCircuited = false;
    }
  }

  protected override void OnShipAspectsUpdate(ShipAspects shipAspects)
  {
    Game.Me.Hangar.ActiveShip.SetShipAspects(shipAspects);
    FacadeFactory.GetInstance().SendMessage(Message.PlayershipAspectsUpdated, (object) shipAspects);
  }

  protected override void OnSlotStat(ushort slotId, ObjectStat stat, float value)
  {
    ShipSlot slot = Game.Me.ActiveShip.GetSlot(slotId);
    if (slot == null)
      return;
    slot.GameStats.SetObfuscatedFloat(stat, value);
    slot.UpdatedStats = true;
  }

  protected override void OnAddToggleBuff(ushort slotId, uint abilityGuid)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MyShipStats.\u003COnAddToggleBuff\u003Ec__AnonStorey67 buffCAnonStorey67 = new MyShipStats.\u003COnAddToggleBuff\u003Ec__AnonStorey67();
    base.OnAddToggleBuff(slotId, abilityGuid);
    // ISSUE: reference to a compiler-generated field
    buffCAnonStorey67.slot = Game.Me.ActiveShip.GetSlot(slotId);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    buffCAnonStorey67.slot.IsLoaded.AddHandler(new SignalHandler(buffCAnonStorey67.\u003C\u003Em__3A));
  }

  protected override void OnRemoveToggleBuff(ushort slotId, uint abilityGuid)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    MyShipStats.\u003COnRemoveToggleBuff\u003Ec__AnonStorey68 buffCAnonStorey68 = new MyShipStats.\u003COnRemoveToggleBuff\u003Ec__AnonStorey68();
    base.OnRemoveToggleBuff(slotId, abilityGuid);
    // ISSUE: reference to a compiler-generated field
    buffCAnonStorey68.slot = Game.Me.ActiveShip.GetSlot(slotId);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    buffCAnonStorey68.slot.IsLoaded.AddHandler(new SignalHandler(buffCAnonStorey68.\u003C\u003Em__3B));
  }
}
