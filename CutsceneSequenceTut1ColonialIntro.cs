﻿// Decompiled with JetBrains decompiler
// Type: CutsceneSequenceTut1ColonialIntro
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CutsceneSequenceTut1ColonialIntro : CutsceneSequenceBase
{
  private const string animClipName = "Tut1_Intro_Colonial";

  protected override string GetSceneSetupName
  {
    get
    {
      return "cutscene_tut1_intro_colonial";
    }
  }

  protected override CutsceneSkipMode GetSkipMode
  {
    get
    {
      return CutsceneSkipMode.ShowMenu;
    }
  }

  protected override bool ExecuteCustomPreparations()
  {
    SpaceLevel.GetLevel().PlayerActualShip.CreatingCause = CreatingCause.AlreadyExists;
    if ((Object) this.CutsceneSetup.AnimationChild == (Object) null)
    {
      Debug.LogError((object) "CutsceneTut1SectorIntroAndGalacticaJumpIn: cutscene setup's animation child is null. Aborting...");
      return false;
    }
    if (!((Object) this.CutsceneSetup.AnimationChild.GetClip(this.GetAnimClipName()) == (Object) null))
      return true;
    Debug.LogError((object) ("CutsceneTut1SectorIntroAndGalacticaJumpIn: Clip " + this.GetAnimClipName() + " doesn't exist in animation component of child \"" + this.CutsceneSetup.AnimationChild.name + "\". Aborting..."));
    return false;
  }

  protected override void StartScene()
  {
    this.CutsceneSetup.AnimationChild.Play(this.GetAnimClipName());
  }

  protected override void Cleanup()
  {
    this.ShowRealPlayerShip(true);
  }

  protected string GetAnimClipName()
  {
    return "Tut1_Intro_Colonial";
  }
}
