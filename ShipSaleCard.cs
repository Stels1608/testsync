﻿// Decompiled with JetBrains decompiler
// Type: ShipSaleCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class ShipSaleCard : Card
{
  public RewardCard[] ShipSales;
  public GUICard GUICard;

  public ShipSaleCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.GUICard = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.GUICard
    });
    if (Game.Me.Faction == Faction.Cylon)
    {
      this.ReadIgnoreRewardCards(r);
      this.ReadRewardCards(r);
    }
    else
    {
      this.ReadRewardCards(r);
      this.ReadIgnoreRewardCards(r);
    }
  }

  private void ReadRewardCards(BgoProtocolReader r)
  {
    int length = r.ReadLength();
    this.ShipSales = new RewardCard[length];
    for (int index = 0; index < length; ++index)
    {
      uint cardGUID = r.ReadGUID();
      this.ShipSales[index] = (RewardCard) Game.Catalogue.FetchCard(cardGUID, CardView.Reward);
      this.IsLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) this.ShipSales[index]
      });
    }
  }

  private void ReadIgnoreRewardCards(BgoProtocolReader r)
  {
    int num1 = r.ReadLength();
    for (int index = 0; index < num1; ++index)
    {
      int num2 = (int) r.ReadGUID();
    }
  }

  public override string ToString()
  {
    string str = "\n[ShipSaleCard]" + (object) this.GUICard;
    foreach (RewardCard shipSale in this.ShipSales)
      str = str + "\nShipSale: " + (object) shipSale;
    return str;
  }
}
