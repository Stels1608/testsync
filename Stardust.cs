﻿// Decompiled with JetBrains decompiler
// Type: Stardust
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Stardust : MonoBehaviour
{
  public float m_fastDustBase = 70f;
  public float m_cameraOffset = 100f;
  public float m_dustTriggerSpeed = 5f;
  public GameObject m_starDustPrefab;
  private GameObject m_starDustObj;
  private ParticleSystem m_starDustPS;
  private ParticleSystem m_starDustFastPS;
  private PlayerShip m_player;
  private float? m_fastDustTriggerSpeed;
  public bool initialized;

  public PlayerShip Player
  {
    get
    {
      return this.m_player ?? (this.m_player = SpaceLevel.GetLevel().GetPlayerShip());
    }
  }

  public bool Rendered
  {
    get
    {
      return this.enabled;
    }
    set
    {
      this.enabled = value;
      if (!((Object) this.m_starDustObj != (Object) null))
        return;
      this.m_starDustObj.SetActive(this.enabled);
    }
  }

  public void Initialize(Color color)
  {
    if ((Object) this.m_starDustPrefab != (Object) null)
    {
      this.m_starDustObj = Object.Instantiate((Object) this.m_starDustPrefab, Vector3.zero, Quaternion.identity) as GameObject;
      this.m_starDustPS = this.m_starDustObj.GetComponent<ParticleSystem>();
      this.m_starDustPS.Stop();
      this.m_starDustPS.startColor = this.m_starDustPS.startColor * color;
      GameObject objectInChildren = HelperUtil.FindGameObjectInChildren(this.m_starDustObj, "starDustFast");
      if ((Object) null != (Object) objectInChildren)
      {
        this.m_starDustFastPS = objectInChildren.GetComponent<ParticleSystem>();
        this.m_starDustFastPS.Stop();
      }
    }
    this.initialized = true;
  }

  private void Update()
  {
    if (SectorEditorHelper.IsEditorLevel())
      return;
    if (!this.initialized)
      this.Initialize(SpaceLevel.GetLevel().Card.DustColor);
    this.m_starDustObj.SetActive(this.enabled);
    if ((Object) this.m_starDustPrefab == (Object) null)
    {
      Debug.LogError((object) "StarDust prefab null.");
    }
    else
    {
      if (this.Player == null)
        return;
      if (this.m_player.Dead)
      {
        if (this.m_starDustFastPS.isPlaying)
          this.m_starDustFastPS.Stop();
        if (this.m_starDustPS.isPlaying)
          this.m_starDustPS.Stop();
      }
      if (!this.m_fastDustTriggerSpeed.HasValue && (bool) this.m_player.IsLoaded && (bool) this.m_player.CameraCard.IsLoaded)
        this.m_fastDustTriggerSpeed = new float?(Mathf.Max(this.m_player.CameraCard.SoftTrembleSpeed, this.m_fastDustBase));
      if ((double) this.m_player.MovementController.CurrentSpeed >= (double) this.m_dustTriggerSpeed)
      {
        if (!this.m_starDustPS.isPlaying)
          this.m_starDustPS.Play();
      }
      else if (this.m_starDustPS.isPlaying)
        this.m_starDustPS.Stop();
      if (this.m_fastDustTriggerSpeed.HasValue)
      {
        float? nullable = this.m_fastDustTriggerSpeed;
        if ((!nullable.HasValue ? 0 : ((double) this.m_player.MovementController.CurrentSpeed >= (double) nullable.Value ? 1 : 0)) != 0)
        {
          if (!this.m_starDustFastPS.isPlaying)
          {
            this.m_starDustFastPS.Play();
            goto label_26;
          }
          else
            goto label_26;
        }
      }
      if (this.m_starDustFastPS.isPlaying)
        this.m_starDustFastPS.Stop();
label_26:
      this.UpdatePosition(this.m_player);
      this.UpdateVFX();
    }
  }

  private void UpdatePosition(PlayerShip player)
  {
    if (player == null || !((Object) null != (Object) this.m_starDustObj))
      return;
    Vector3 normalized = player.MovementController.GetTickFrame(Tick.Current).linearSpeed.normalized;
    if (normalized == Vector3.zero)
      return;
    this.m_starDustObj.transform.position = this.transform.position + normalized * this.m_cameraOffset;
    this.m_starDustObj.transform.rotation = Quaternion.LookRotation(normalized);
  }

  private void UpdateVFX()
  {
    if (!((Object) null != (Object) this.m_starDustPS) || this.m_player == null)
      return;
    float num = this.m_player.MovementController.CurrentSpeed * 0.1f;
    if (this.m_starDustPS.isPlaying)
    {
      this.m_starDustPS.startSpeed = this.m_player.MovementController.CurrentSpeed * 2.5f;
      this.m_starDustPS.startLifetime = 5f / num;
      this.m_starDustPS.emissionRate = 20f * num;
    }
    if (!((Object) null != (Object) this.m_starDustFastPS) || !this.m_starDustFastPS.isPlaying)
      return;
    this.m_starDustFastPS.startSpeed = this.m_player.MovementController.CurrentSpeed * 3.5f;
  }
}
