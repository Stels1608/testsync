﻿// Decompiled with JetBrains decompiler
// Type: DutyList
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class DutyList : SmartList<Duty>
{
  private ushort selectedId;

  public ushort SelectedId
  {
    get
    {
      return this.selectedId;
    }
  }

  public Duty Selected
  {
    get
    {
      return this.GetByID(this.selectedId);
    }
  }

  public void _SetSelectedDuty(ushort ServerID)
  {
    this.selectedId = ServerID;
  }
}
