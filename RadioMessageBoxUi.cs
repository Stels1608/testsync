﻿// Decompiled with JetBrains decompiler
// Type: RadioMessageBoxUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using MessageBox;
using System.Collections.Generic;
using UnityEngine;

public abstract class RadioMessageBoxUi : MessageBoxBase
{
  private List<BoxesStackItem> stack;
  private int index;

  public void SetContent(OnMessageBoxClose onClose, BoxesStackItem i)
  {
    List<BoxesStackItem> s = new List<BoxesStackItem>() { i };
    this.SetContent(onClose, s);
  }

  public void SetContent(OnMessageBoxClose onClose, List<BoxesStackItem> s)
  {
    this.AddOnClosedAction(onClose);
    this.stack = s;
    this.index = this.stack.Count - 1;
    this.ShowCurrentIndexMessage();
  }

  protected abstract void SetPilotName(string pilotName);

  protected abstract void SetPilotPortrait(Texture2D portrait);

  protected abstract void SetHintText(string hintText);

  protected abstract void SetHintImage2D(Texture2D hintImage);

  protected abstract void ShowHintImage(bool isVisible);

  protected abstract void ShowOkButton(bool isVisible);

  protected abstract void EnableLeftNavigationButton(bool isEnabled);

  protected abstract void EnableRightNavigationButton(bool isEnabled);

  protected abstract void SetNavigationLabel(string navPageIndex);

  protected abstract void DisplayMessageText();

  private void ApplyNpcCard(GUICard npcCard)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    RadioMessageBoxUi.\u003CApplyNpcCard\u003Ec__AnonStorey126 cardCAnonStorey126 = new RadioMessageBoxUi.\u003CApplyNpcCard\u003Ec__AnonStorey126();
    // ISSUE: reference to a compiler-generated field
    cardCAnonStorey126.npcCard = npcCard;
    // ISSUE: reference to a compiler-generated field
    cardCAnonStorey126.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    if (!(bool) cardCAnonStorey126.npcCard.IsLoaded)
      this.ShowBox(false);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    cardCAnonStorey126.npcCard.IsLoaded.AddHandler(new SignalHandler(cardCAnonStorey126.\u003C\u003Em__2C3));
  }

  private void OnLoadCard(GUICard npcCard)
  {
    if (npcCard.GUIAtlasTexturePath == "player_placeholder")
    {
      AvatarSnapshot.RequestCharacterPortrait(new AvatarSnapshotCallback(this.SetPilotPortrait));
      this.SetPilotName(Game.Me.Name);
    }
    else
    {
      Texture2D portrait = (Texture2D) Resources.Load(npcCard.GUIAtlasTexturePath);
      if ((Object) portrait == (Object) null)
      {
        Debug.LogError((object) ("OnLoadCard(): Couldn't load portrait: " + npcCard.GUIAtlasTexturePath));
      }
      else
      {
        this.SetPilotPortrait(portrait);
        this.SetPilotName(npcCard.Name);
      }
    }
  }

  private void SetHintImage(string hintImagePath)
  {
    if (string.IsNullOrEmpty(hintImagePath))
    {
      this.ShowHintImage(false);
    }
    else
    {
      Texture2D hintImage = (Texture2D) Resources.Load(hintImagePath);
      this.ShowHintImage(true);
      this.SetHintImage2D(hintImage);
    }
  }

  private void ShowBox(bool isVisible)
  {
    this.gameObject.SetActive(isVisible);
  }

  public void ShowPreviousMessage()
  {
    --this.index;
    Mathf.Clamp(this.index, 0, this.stack.Count - 1);
    this.ShowCurrentIndexMessage();
  }

  public void ShowNextMessage()
  {
    ++this.index;
    Mathf.Clamp(this.index, 0, this.stack.Count - 1);
    this.ShowCurrentIndexMessage();
  }

  public void ShowCurrentIndexMessage()
  {
    this.SetNavigationLabel((this.index + 1).ToString() + "/" + (object) this.stack.Count);
    this.ShowOkButton(this.stack[this.index].showOk);
    this.SetOkButtonText(this.stack[this.index].okButtonName);
    this.SetMainText(this.stack[this.index].mainText);
    this.SetHintText(this.stack[this.index].extText);
    this.DisplayMessageText();
    this.SetHintImage(this.stack[this.index].extraImage);
    this.SetTimeToLive(this.stack[this.index].TimeToLive);
    GUICard npcCard = this.stack[this.index].card as GUICard;
    if (npcCard != null)
      this.ApplyNpcCard(npcCard);
    this.EnableLeftNavigationButton(this.index != 0);
    this.EnableRightNavigationButton(this.index != this.stack.Count - 1);
  }
}
