﻿// Decompiled with JetBrains decompiler
// Type: JSectorEvent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class JSectorEvent
{
  [JsonField(JsonName = "_id")]
  public string _id = string.Empty;
  [JsonField(JsonName = "NameCylon")]
  public string nameCylon = string.Empty;
  [JsonField(JsonName = "NameColonial")]
  public string nameColonial = string.Empty;
  [JsonField(JsonName = "DescriptionCylon")]
  public string descriptionCylon = string.Empty;
  [JsonField(JsonName = "DescriptionColonial")]
  public string descriptionColonial = string.Empty;
  private const string CONTRIBUTION_PVE = "pve";
  private const string CONTRIBUTION_PVP = "pvp";
  private const string CONTRIBUTION_TIME = "time";
  private const string CONTRIBUTION_BUFF = "buff";
  private const string CONTRIBUTION_HIT = "hit";
  private const string BALANCE_OWNER_TIER1 = "ownerTierOneWeight";
  private const string BALANCE_OWNER_TIER2 = "ownerTierTwoWeight";
  private const string BALANCE_OWNER_TIER3 = "ownerTierThreeWeight";
  private const string BALANCE_OWNER_TIER4 = "ownerTierFourWeight";
  private const string BALANCE_RIVAL_TIER1 = "rivalTierOneWeight";
  private const string BALANCE_RIVAL_TIER2 = "rivalTierTwoWeight";
  private const string BALANCE_RIVAL_TIER3 = "rivalTierThreeWeight";
  private const string BALANCE_RIVAL_TIER4 = "rivalTierFourWeight";
  [JsonField(JsonName = "GroupEvent")]
  public bool elite;
  [JsonField(JsonName = "Radius")]
  public float radius;
  [JsonField(JsonName = "Behavior")]
  public string behavior;
  [JsonField(JsonName = "Contribution")]
  public Dictionary<string, float> contribution;
  [JsonField(JsonName = "Balance")]
  public Dictionary<string, float> balance;
  [JsonField(JsonName = "ContributionTick")]
  public int contributionTick;
  [JsonField(JsonName = "BalanceTick")]
  public int balanceTick;
  [JsonField(JsonName = "Points")]
  public Dictionary<string, JTransform> points;
  [JsonField(JsonName = "OwnerSpawnPoints")]
  public Dictionary<string, JTransform> ownerSpawnPoints;
  [JsonField(JsonName = "RivalSpawnPoints")]
  public Dictionary<string, JTransform> rivalSpawnPoints;

  public float ContributionPve
  {
    get
    {
      return this.contribution["pve"];
    }
    set
    {
      this.contribution["pve"] = value;
    }
  }

  public float ContributionPvp
  {
    get
    {
      return this.contribution["pvp"];
    }
    set
    {
      this.contribution["pvp"] = value;
    }
  }

  public float ContributionTime
  {
    get
    {
      return this.contribution["time"];
    }
    set
    {
      this.contribution["time"] = value;
    }
  }

  public float ContributionBuff
  {
    get
    {
      return this.contribution["buff"];
    }
    set
    {
      this.contribution["buff"] = value;
    }
  }

  public float ContributionHit
  {
    get
    {
      return this.contribution["hit"];
    }
    set
    {
      this.contribution["hit"] = value;
    }
  }

  public float BalanceOwnerTier1
  {
    get
    {
      return this.balance["ownerTierOneWeight"];
    }
    set
    {
      this.balance["ownerTierOneWeight"] = value;
    }
  }

  public float BalanceOwnerTier2
  {
    get
    {
      return this.balance["ownerTierTwoWeight"];
    }
    set
    {
      this.balance["ownerTierTwoWeight"] = value;
    }
  }

  public float BalanceOwnerTier3
  {
    get
    {
      return this.balance["ownerTierThreeWeight"];
    }
    set
    {
      this.balance["ownerTierThreeWeight"] = value;
    }
  }

  public float BalanceOwnerTier4
  {
    get
    {
      return this.balance["ownerTierFourWeight"];
    }
    set
    {
      this.balance["ownerTierFourWeight"] = value;
    }
  }

  public float BalanceRivalTier1
  {
    get
    {
      return this.balance["rivalTierOneWeight"];
    }
    set
    {
      this.balance["rivalTierOneWeight"] = value;
    }
  }

  public float BalanceRivalTier2
  {
    get
    {
      return this.balance["rivalTierTwoWeight"];
    }
    set
    {
      this.balance["rivalTierTwoWeight"] = value;
    }
  }

  public float BalanceRivalTier3
  {
    get
    {
      return this.balance["rivalTierThreeWeight"];
    }
    set
    {
      this.balance["rivalTierThreeWeight"] = value;
    }
  }

  public float BalanceRivalTier4
  {
    get
    {
      return this.balance["rivalTierFourWeight"];
    }
    set
    {
      this.balance["rivalTierFourWeight"] = value;
    }
  }

  public void AddPoint(Transform point)
  {
    this.points.Add(point.name, new JTransform()
    {
      position = point.localPosition,
      rotation = point.localRotation
    });
  }

  public override string ToString()
  {
    return string.Format("Id: {0}, NameCylon: {1}, NameColonial: {2}, DescriptionCylon: {3}, DescriptionColonial: {4}, Elite: {5}, Radius: {6}, Behavior: {7}, ContributionTick: {8}", (object) this._id, (object) this.nameCylon, (object) this.nameColonial, (object) this.descriptionCylon, (object) this.descriptionColonial, (object) this.elite, (object) this.radius, (object) this.behavior, (object) this.contributionTick);
  }
}
