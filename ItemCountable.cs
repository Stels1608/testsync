﻿// Decompiled with JetBrains decompiler
// Type: ItemCountable
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class ItemCountable : ShipItem
{
  public uint count;
  public bool disableBuying;
  public ShipConsumableCard Card;

  public uint Count
  {
    get
    {
      return this.count;
    }
    set
    {
      this.count = value;
    }
  }

  public bool IsResource
  {
    get
    {
      if (!(bool) this.IsLoaded)
        return false;
      if (this.ShopItemCard.Category == ShopCategory.Resource)
        return true;
      uint cardGuid = this.Card.CardGUID;
      switch (cardGuid)
      {
        case 215278030:
        case 264733124:
        case 207047790:
        case 187088612:
        case 130920111:
          return true;
        default:
          return (int) cardGuid == 130762195;
      }
    }
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Count = r.ReadUInt32();
    this.Card = (ShipConsumableCard) Game.Catalogue.FetchCard(this.CardGUID, CardView.ShipConsumable);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.Card
    });
  }

  public void Use()
  {
    PlayerProtocol.GetProtocol().UseAugment(this);
  }
}
