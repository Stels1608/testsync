﻿// Decompiled with JetBrains decompiler
// Type: Price
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class Price : ILoadable, IProtocolRead
{
  private readonly Flag isLoaded = new Flag();
  public readonly Dictionary<ShipConsumableCard, float> items = new Dictionary<ShipConsumableCard, float>();

  public Flag IsLoaded
  {
    get
    {
      return this.isLoaded;
    }
  }

  public bool UsesMerits
  {
    get
    {
      using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = this.items.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if ((int) enumerator.Current.Key.CardGUID == 130920111)
            return true;
        }
      }
      return false;
    }
  }

  public float CubitsCost
  {
    get
    {
      using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = this.items.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
          if ((int) current.Key.CardGUID == 264733124)
            return current.Value;
        }
      }
      return 0.0f;
    }
  }

  public void Read(BgoProtocolReader r)
  {
    int num = r.ReadLength();
    for (int index = 0; index < num; ++index)
      this.items[(ShipConsumableCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.ShipConsumable)] = r.ReadSingle();
    this.isLoaded.Depend<ShipConsumableCard>((IEnumerable<ShipConsumableCard>) this.items.Keys);
    this.IsLoaded.Set();
  }

  public bool IsEnoughInHangar()
  {
    return this.IsEnoughInHangar(1U);
  }

  public bool IsEnoughInHangar(uint count)
  {
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        if ((double) current.Value != 0.0)
        {
          ShipItem byGuid = Game.Me.Hold.GetByGUID(current.Key.CardGUID);
          if (byGuid == null || byGuid is ItemCountable && (double) (byGuid as ItemCountable).Count < (double) current.Value * (double) count)
            return false;
        }
      }
    }
    return true;
  }

  public bool IsEnoughInHangar(ShopItemCard shopItemCard)
  {
    return this.IsEnoughInHangar(shopItemCard, 1);
  }

  public bool IsEnoughInHangar(ShopItemCard shopItemCard, int count)
  {
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        if ((double) current.Value != 0.0 && !this.IsEnoughInHangar(current, shopItemCard, count))
          return false;
      }
    }
    return true;
  }

  public bool IsEnoughInHangar(ResourceType resource, ShopItemCard shopItemCard, int count)
  {
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        if ((ResourceType) current.Key.CardGUID == resource)
          return (double) current.Value == 0.0 || this.IsEnoughInHangar(current, shopItemCard, count);
      }
    }
    return true;
  }

  private bool IsEnoughInHangar(KeyValuePair<ShipConsumableCard, float> pair, ShopItemCard shopItemCard, int count)
  {
    ShipItem byGuid = Game.Me.Hold.GetByGUID(pair.Key.CardGUID);
    if (byGuid == null)
      return false;
    float result;
    Shop.ApplyDiscount(out result, shopItemCard.CardGUID, pair.Value * (float) count);
    return !(byGuid is ItemCountable) || (double) (byGuid as ItemCountable).Count >= (double) result;
  }

  public int GetMaxCountWeCanBuy(ShopItemCard shopItemCard)
  {
    int a = int.MaxValue;
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        ShipItem byGuid = Game.Me.Hold.GetByGUID(current.Key.CardGUID);
        if (byGuid == null)
          return 0;
        float result;
        Shop.ApplyDiscount(out result, shopItemCard.CardGUID, current.Value);
        if (byGuid is ItemCountable && (double) result > 0.0)
          a = Mathf.Min(a, Mathf.FloorToInt((float) (byGuid as ItemCountable).Count / result));
      }
    }
    return a;
  }

  public List<ResourceType> GetMissingResources(int count = 1)
  {
    List<ResourceType> resourceTypeList = new List<ResourceType>();
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        uint cardGuid = current.Key.CardGUID;
        uint countByGuid = Game.Me.Hold.GetCountByGUID(cardGuid);
        float result;
        Shop.ApplyDiscount(out result, cardGuid, current.Value);
        if ((double) countByGuid < (double) result * (double) count)
          resourceTypeList.Add((ResourceType) current.Key.CardGUID);
      }
    }
    return resourceTypeList;
  }

  public Dictionary<ShipConsumableCard, uint> GetMissingResourcesAmount(int count = 1)
  {
    Dictionary<ShipConsumableCard, uint> dictionary = new Dictionary<ShipConsumableCard, uint>();
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        uint countByGuid = Game.Me.Hold.GetCountByGUID(current.Key.CardGUID);
        if ((double) countByGuid < (double) current.Value * (double) count)
        {
          float num = current.Value - (float) countByGuid;
          dictionary.Add(current.Key, (uint) num);
        }
      }
    }
    return dictionary;
  }

  public uint GetBuyForCubitsPrice()
  {
    uint num = 0;
    using (Dictionary<ShipConsumableCard, uint>.Enumerator enumerator = this.GetMissingResourcesAmount(1).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, uint> current = enumerator.Current;
        ShopItemCard shopItemCard = (ShopItemCard) Game.Catalogue.FetchCard(current.Key.CardGUID, CardView.Price);
        num += (uint) ((double) shopItemCard.BuyPrice.CubitsCost * (double) current.Value);
      }
    }
    return num;
  }

  public override string ToString()
  {
    string str = string.Empty;
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        str = str + current.Key.GUICard.Name + " " + (object) current.Value + " ";
      }
    }
    return str;
  }

  public void AddPrice(Price price, uint count = 1)
  {
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = price.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        if (!this.items.ContainsKey(current.Key))
          this.items.Add(current.Key, 0.0f);
        Dictionary<ShipConsumableCard, float> dictionary;
        ShipConsumableCard key;
        (dictionary = this.items)[key = current.Key] = dictionary[key] + current.Value * (float) count;
      }
    }
  }
}
