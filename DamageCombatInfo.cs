﻿// Decompiled with JetBrains decompiler
// Type: DamageCombatInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class DamageCombatInfo : AbstractCombatInfo
{
  private readonly float damage;
  private readonly bool criticalHit;

  public bool CriticalHit
  {
    get
    {
      return this.criticalHit;
    }
  }

  public override string DisplayString
  {
    get
    {
      return this.Damage.ToString("n0");
    }
  }

  public int Damage
  {
    get
    {
      return (int) this.damage;
    }
  }

  public DamageCombatInfo(SpaceObject target, float damage, bool criticalHit)
    : base(target)
  {
    this.damage = (float) (int) damage;
    this.criticalHit = criticalHit;
  }
}
