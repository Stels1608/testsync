﻿// Decompiled with JetBrains decompiler
// Type: BPException
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.Serialization;

public class BPException : Exception
{
  public BPException()
  {
  }

  public BPException(string message)
    : base(message)
  {
  }

  protected BPException(SerializationInfo info, StreamingContext context)
    : base(info, context)
  {
  }

  public BPException(string message, Exception innerException)
    : base(message, innerException)
  {
  }
}
