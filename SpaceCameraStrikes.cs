﻿// Decompiled with JetBrains decompiler
// Type: SpaceCameraStrikes
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class SpaceCameraStrikes : SpaceCameraBase
{
  private static readonly List<SpaceCameraBase.CameraMode> supportedCameraModes = new List<SpaceCameraBase.CameraMode>() { SpaceCameraBase.CameraMode.Chase, SpaceCameraBase.CameraMode.Nose };
  private bool leftMouseDown;

  public static List<SpaceCameraBase.CameraMode> SupportedCameraModes
  {
    get
    {
      return SpaceCameraStrikes.supportedCameraModes;
    }
  }

  public override bool HandleMouseInput
  {
    get
    {
      return true;
    }
  }

  public override bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public override bool HandleJoystickAxesInput
  {
    get
    {
      return true;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    Animation componentInChildren = this.GetComponentInChildren<Animation>();
    if ((Object) componentInChildren != (Object) null)
      Object.Destroy((Object) componentInChildren);
    this.SetCurrentBehavior((ISpaceCameraBehavior) new SpaceCameraBehaviorStrikesChase());
  }

  public override List<SpaceCameraBase.CameraMode> GetSupportedCameraModes()
  {
    return SpaceCameraStrikes.supportedCameraModes;
  }

  public override void SetCameraMode(SpaceCameraBase.CameraMode cameraMode)
  {
    base.SetCameraMode(cameraMode);
    switch (this.CurrentCameraMode)
    {
      case SpaceCameraBase.CameraMode.Chase:
        this.SetCurrentBehavior((ISpaceCameraBehavior) new SpaceCameraBehaviorStrikesChase());
        break;
      case SpaceCameraBase.CameraMode.Nose:
        this.SetCurrentBehavior((ISpaceCameraBehavior) new SpaceCameraBehaviorStrikesNose());
        break;
      default:
        this.SetCurrentBehavior((ISpaceCameraBehavior) new SpaceCameraBehaviorStrikesChase());
        break;
    }
  }

  protected override void LateUpdate()
  {
    base.LateUpdate();
    if ((Object) Camera.main == (Object) null)
    {
      BgoUtils.LogOnce("Camera.main is null. What did you do...?");
    }
    else
    {
      if (Input.GetMouseButtonDown(0))
        this.leftMouseDown = (GUIManager.MouseOverAnyGuiElement ? 1 : (GUIManager.ModalNguiWindowOpened ? 1 : 0)) == 0;
      else if (Input.GetMouseButtonUp(0))
        this.leftMouseDown = false;
      if (Input.GetMouseButtonUp(0))
        this.OnMouseUp(new float2(Input.mousePosition.x, Input.mousePosition.y), KeyCode.Mouse0);
      else if (Input.GetMouseButtonDown(1))
        this.OnMouseDown(new float2(Input.mousePosition.x, Input.mousePosition.y), KeyCode.Mouse1);
      if (SpaceLevel.GetLevel().GetPlayerShip() == null)
        return;
      this.CurrentBehavior.LateUpdate();
      this.ApplyCameraParameters(this.CurrentBehavior.CalculateCurrentCameraParameters());
    }
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse1 && this.CurrentBehavior is SpaceCameraBehaviorStrikesOrbit)
      this.LeaveOrbitMode();
    return false;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse0 && this.CurrentBehavior is SpaceCameraBehaviorStrikesOrbit)
      this.DelayedCameraSwitch();
    return false;
  }

  private void DelayedCameraSwitch()
  {
    this.CancelLeavingOrbitMode();
    this.Invoke("LeaveOrbitMode", 3f);
  }

  private void LeaveOrbitMode()
  {
    this.CancelLeavingOrbitMode();
    this.SetCameraMode(this.CurrentCameraMode);
  }

  private void CancelLeavingOrbitMode()
  {
    this.CancelInvoke("LeaveOrbitMode");
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    if (this.leftMouseDown && !Input.GetMouseButton(1) && !(this.CurrentBehavior is SpaceCameraBehaviorStrikesOrbit))
    {
      if (GUIManager.MouseOverAnyOldGuiElement || (double) (Mathf.Abs(Input.GetAxisRaw("Mouse X")) + Mathf.Abs(Input.GetAxisRaw("Mouse Y"))) < 0.28999999165535)
        return;
      this.SetCurrentBehavior((ISpaceCameraBehavior) new SpaceCameraBehaviorStrikesOrbit());
    }
    else
    {
      if (!Input.GetMouseButton(0) || !(this.CurrentBehavior is SpaceCameraBehaviorStrikesOrbit))
        return;
      this.CancelLeavingOrbitMode();
    }
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    return base.OnKeyDown(keyboardKey, action);
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    return base.OnKeyUp(keyboardKey, action);
  }

  public override void OnTargetChanged(ISpaceEntity newTarget)
  {
    if (!(this.CurrentBehavior is SpaceCameraBehaviorStrikesChase))
      return;
    ((SpaceCameraBehaviorStrikesChase) this.CurrentBehavior).OnTargetChanged(newTarget);
  }
}
