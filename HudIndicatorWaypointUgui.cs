﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorWaypointUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class HudIndicatorWaypointUgui : HudIndicatorBaseComponentUgui, IRequiresLateUpdate
{
  [SerializeField]
  private Image image;
  private CanvasGroup canvas;

  protected UIAtlas Atlas
  {
    get
    {
      return HudIndicatorsAtlasLinker.Instance.Atlas;
    }
  }

  protected override void ResetComponentStates()
  {
  }

  protected override void Awake()
  {
    base.Awake();
    this.canvas = this.image.gameObject.AddComponent<CanvasGroup>();
    this.UpdateColorAndAlpha();
  }

  protected override void SetColoring()
  {
    this.image.color = TargetColors.waypointTargetColor;
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    this.image.gameObject.SetActive(this.IsActiveWaypoint && this.InScreen);
  }

  public void RemoteLateUpdate()
  {
    if (!this.gameObject.activeSelf)
      return;
    this.AlphaFade();
  }

  private void AlphaFade()
  {
    this.canvas.alpha = Mathf.Cos((float) (1.14999997615814 * (double) Time.time % 1.57000005245209));
  }
}
