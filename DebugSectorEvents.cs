﻿// Decompiled with JetBrains decompiler
// Type: DebugSectorEvents
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugSectorEvents : DebugBehaviour<DebugSectorEvents>
{
  private Vector2 scroll = new Vector2();

  private void Start()
  {
    this.windowID = 15;
    this.SetSize(200f, 300f);
  }

  protected override void WindowFunc()
  {
    GUILayout.Label("Vital danger! Only for expert");
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.Label("Use in sector!");
    if (GUILayout.Button("Start all Sector Events"))
    {
      DebugProtocol.GetProtocol().Command("sector_events_start_all");
      DebugBehaviour<DebugSectorEvents>.Close();
    }
    GUILayout.EndScrollView();
  }
}
