﻿// Decompiled with JetBrains decompiler
// Type: ToolboxPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System.Collections.Generic;
using UnityEngine;

public class ToolboxPanel : GUIPanel
{
  public MainPanel panel;
  private string documentName;
  private string cdbDocumentName;
  private string cdbDocumentPath;

  public ToolboxPanel()
    : base((SmartRect) null)
  {
    this.root.Width = 150f;
    this.root.Height = 600f;
    try
    {
      this.documentName = PlayerPrefs.GetString("EditorGUIlayoutPath", "gui_layout.txt");
      this.cdbDocumentName = PlayerPrefs.GetString("EditorGUIlayoutCDBName", string.Empty);
      this.cdbDocumentPath = PlayerPrefs.GetString("EditorGUIlayoutCDBPath1", "/PaperdollSmall/1/");
    }
    catch (PlayerPrefsException ex)
    {
    }
  }

  public override void Draw()
  {
    GUILayout.BeginArea(this.root.AbsRect);
    GUILayout.BeginVertical();
    if (GUILayout.Button("Add Button"))
      this.panel.AddElement((CustomGUIElement) new Button(new JButton()));
    if (GUILayout.Button("Add Label"))
      this.panel.AddElement((CustomGUIElement) new Label(new JLabel()));
    if (GUILayout.Button("Add Image"))
      this.panel.AddElement((CustomGUIElement) new Image(new JImage()));
    if (GUILayout.Button("Add Pivot"))
    {
      JPivot desc = new JPivot();
      desc.position = new float2(10f, -10f);
      this.panel.AddElement((CustomGUIElement) new BsgoEditor.GUIEditor.Pivot(desc));
    }
    if (GUILayout.Button("Duplicate"))
    {
      CustomGUIElement el1 = this.panel.selectedObject;
      if (el1 == null)
        return;
      JElementBase el2 = Loaders.GrabElement(el1);
      el2.position += new float2(10f, 10f);
      this.panel.AddElement(Loaders.CreateGuiElement(el2));
    }
    if (GUILayout.Button("Remove") || Input.GetKeyDown(KeyCode.Delete))
    {
      this.panel.RemoveElement(this.panel.selectedObject);
      this.panel.selectedObject = (CustomGUIElement) null;
    }
    GUILayout.Label("Document name:");
    this.documentName = GUILayout.TextArea(this.documentName);
    if (GUILayout.Button("Load"))
    {
      this.panel.elements.Clear();
      using (List<JElementBase>.Enumerator enumerator = Loaders.Load(this.documentName).Elements.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.panel.AddElement(Loaders.CreateGuiElement(enumerator.Current));
      }
    }
    if (GUILayout.Button("Save"))
    {
      Loaders.Save(this.documentName, this.panel);
      PlayerPrefs.SetString("EditorGUIlayoutPath", this.documentName);
    }
    GUILayout.Label("CDB document name:");
    this.cdbDocumentName = GUILayout.TextArea(this.cdbDocumentName);
    GUILayout.Label("CDB path:");
    this.cdbDocumentPath = GUILayout.TextArea(this.cdbDocumentPath);
    if (GUILayout.Button("Load CDB"))
    {
      this.panel.elements.Clear();
      using (List<JElementBase>.Enumerator enumerator = Loaders.LoadFromCDB(this.cdbDocumentName, this.cdbDocumentPath).Elements.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.panel.AddElement(Loaders.CreateGuiElement(enumerator.Current));
      }
    }
    if (GUILayout.Button("Save CDB"))
    {
      Loaders.SaveToCDB(this.cdbDocumentName, this.cdbDocumentPath, this.panel);
      PlayerPrefs.SetString("EditorGUIlayoutCDBName", this.cdbDocumentName);
      PlayerPrefs.SetString("EditorGUIlayoutCDBPath1", this.cdbDocumentPath);
    }
    GUILayout.Space(20f);
    if (GUILayout.Button("Bring to Front"))
      this.panel.SendToBack(this.panel.selectedObject);
    if (GUILayout.Button("Send to Back"))
      this.panel.BringToFront(this.panel.selectedObject);
    GUILayout.EndVertical();
    GUILayout.EndArea();
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return this.Contains(mousePosition);
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return this.Contains(mousePosition);
  }

  public override void RecalculateAbsCoords()
  {
    this.root.Position = new float2((float) Screen.width - this.root.Width, this.root.Height / 2f);
    base.RecalculateAbsCoords();
  }
}
