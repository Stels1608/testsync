﻿// Decompiled with JetBrains decompiler
// Type: DamageOverlay
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class DamageOverlay : MonoBehaviour
{
  private const float ALPHA_FADE = 0.2f;
  public static bool ShowDamageOverlaySetting;
  public UnityEngine.Sprite[] cylonSprites;
  public UnityEngine.Sprite[] colonialSprites;
  public Image[] images;
  private float alpha;
  private bool isSectorMapImplementedByMartinWithVeryGoodCodeOpen;

  public void SetFaction(Faction faction)
  {
    UnityEngine.Sprite[] spriteArray = faction != Faction.Colonial ? this.cylonSprites : this.colonialSprites;
    for (int index = 0; index < spriteArray.Length; ++index)
      this.images[index].sprite = spriteArray[index];
  }

  public void Set3DSectorMapImplementedByMartinWithVeryGoodCodeOpen(bool is3DSectorMapImplementedByMartinWithVeryGoodCode)
  {
    this.isSectorMapImplementedByMartinWithVeryGoodCodeOpen = is3DSectorMapImplementedByMartinWithVeryGoodCode;
  }

  public void VisualizeDamage(DamageCombatInfo combatInfo)
  {
    if (!DamageOverlay.ShowDamageOverlaySetting && !this.isSectorMapImplementedByMartinWithVeryGoodCodeOpen)
      return;
    if (!this.enabled)
      this.enabled = true;
    SpaceObject spaceObject = (SpaceObject) Game.Me.ActualShip;
    this.alpha = 1f - spaceObject.Props.HullPoints / spaceObject.Props.MaxHullPoints;
    if (!combatInfo.CriticalHit)
      return;
    this.alpha *= 2f;
  }

  private void Update()
  {
    float num = this.alpha;
    this.alpha -= Time.deltaTime * 0.2f;
    this.alpha = Mathf.Clamp(this.alpha, 0.0f, 2f);
    this.SetAlpha(this.alpha);
    if ((double) num >= 0.00999999977648258 || (double) this.alpha >= 0.00999999977648258)
      return;
    this.SetAlpha(0.0f);
    this.enabled = false;
  }

  private void SetAlpha(float a)
  {
    Color white = Color.white;
    white.a = a;
    for (int index = 0; index < this.images.Length; ++index)
      this.images[index].color = white;
  }
}
