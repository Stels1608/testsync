﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorColorScheme
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class HudIndicatorColorScheme
{
  private static IHudIndicatorColorScheme _instance;

  public static IHudIndicatorColorScheme Instance
  {
    get
    {
      if (HudIndicatorColorScheme._instance == null)
        HudIndicatorColorScheme.SetMode(HudIndicatorColorSchemeMode.FriendOrFoeColors);
      return HudIndicatorColorScheme._instance;
    }
  }

  private HudIndicatorColorScheme()
  {
  }

  public static void SetMode(HudIndicatorColorSchemeMode schemeMode)
  {
    HudIndicatorColorScheme._instance = schemeMode != HudIndicatorColorSchemeMode.FactionColors ? (IHudIndicatorColorScheme) new HudIndicatorColorSchemeFriendOrFoe() : (IHudIndicatorColorScheme) new HudIndicatorColorSchemeFactions();
    DradisHud.SetIndicatorColorMode(schemeMode);
  }
}
