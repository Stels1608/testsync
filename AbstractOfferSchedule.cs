﻿// Decompiled with JetBrains decompiler
// Type: AbstractOfferSchedule
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;

internal class AbstractOfferSchedule
{
  protected readonly List<IShopOffer> myList;
  private List<GuiTicker.IMessage> tickerMessages;

  protected AbstractOfferSchedule(List<IShopOffer> list, string tickerMessage)
  {
    this.myList = list;
    GuiTicker ticker = Game.Ticker;
    this.tickerMessages = new List<GuiTicker.IMessage>();
    string str;
    if (!BsgoLocalization.TryGet(tickerMessage, out str))
      return;
    using (List<IShopOffer>.Enumerator enumerator = this.myList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IShopOffer current = enumerator.Current;
        UpgradeShopOffer upgradeShopOffer = current as UpgradeShopOffer;
        if (upgradeShopOffer != null && upgradeShopOffer.IsDiscounted(ShopItemType.Ship, 1))
        {
          str = BsgoLocalization.Get("bgo.notif.active_ship_upgrades_sale");
          this.tickerMessages.Add(ticker.AddPendingCountdown(str.Replace("{discount}", current.Percentage().ToString()), (long) current.Expiry()));
        }
        else
          this.tickerMessages.Add(ticker.AddPendingCountdown(str.Replace("{discount}", current.Percentage().ToString()), (long) current.Expiry()));
      }
    }
  }

  public void RemoveTickerMessages()
  {
    using (List<GuiTicker.IMessage>.Enumerator enumerator = this.tickerMessages.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Cancel();
    }
  }
}
