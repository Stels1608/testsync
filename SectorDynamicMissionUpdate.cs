﻿// Decompiled with JetBrains decompiler
// Type: SectorDynamicMissionUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class SectorDynamicMissionUpdate : GalaxyMapUpdate
{
  public byte DynamicMissionCount { get; private set; }

  public SectorDynamicMissionUpdate(Faction faction, uint sectorId, byte dynamicMissionCount)
    : base(GalaxyUpdateType.SectorDynamicMissions, faction, (int) sectorId)
  {
    this.DynamicMissionCount = dynamicMissionCount;
  }

  public override string ToString()
  {
    return "[SectorDynamicMissionUpdate] (Faction: " + (object) this.Faction + ", SectorId: " + (object) this.SectorId + ", DynamicMissionCount:" + (object) this.DynamicMissionCount + ")";
  }
}
