﻿// Decompiled with JetBrains decompiler
// Type: LegAnimator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[RequireComponent(typeof (LegController))]
[RequireComponent(typeof (AlignmentTracker))]
public class LegAnimator : MonoBehaviour
{
  public bool startAutomatically = true;
  public float minStepDistance = 0.2f;
  public float maxStepDuration = 1.5f;
  public float maxStepRotation = 160f;
  public float maxStepAcceleration = 5f;
  public float maxStepHeight = 1f;
  public float maxSlopeAngle = 60f;
  public bool enableLegParking = true;
  public LayerMask groundLayers = (LayerMask) 1;
  public float climbTiltAmount = 0.5f;
  public float accelerateTiltAmount = 0.02f;
  private bool updateStates = true;
  private Dictionary<string, TrajectoryVisualizer> trajectories = new Dictionary<string, TrajectoryVisualizer>();
  public float groundHugX;
  public float groundHugZ;
  public float climbTiltSensitivity;
  public float accelerateTiltSensitivity;
  public bool renderFootMarkers;
  public bool renderBlendingGraph;
  public bool renderCycleGraph;
  public bool renderAnimationStates;
  private bool isActive;
  private float currentTime;
  private LegController legC;
  private AlignmentTracker tr;
  private LegInfo[] legs;
  private LegState[] legStates;
  private Vector3 position;
  private Vector3 horizontalVelocity;
  private Vector3 horizontalVelocitySmoothed;
  private float speed;
  private float speedSmoothed;
  private Vector3 objectVelocity;
  private Vector3 usedObjectVelocity;
  private Quaternion rotation;
  private Vector3 up;
  private Vector3 forward;
  private float scale;
  private Vector3 baseUpGround;
  private Vector3 bodyUp;
  private Vector3 legsUp;
  private float accelerationTiltX;
  private float accelerationTiltZ;
  private AnimationState controlMotionState;
  private MotionGroupState[] motionGroupStates;
  private AnimationState[] nonGroupMotionStates;
  private float[] nonGroupMotionWeights;
  private AnimationState[] motionStates;
  private AnimationState[] cycleMotionStates;
  private float[] motionWeights;
  private float[] cycleMotionWeights;
  private float summedMotionWeight;
  private float summedCycleMotionWeight;
  private float locomotionWeight;
  private float cycleDuration;
  private float cycleDistance;
  private float normalizedTime;
  [NonSerialized]
  public GameObject ghost;
  [NonSerialized]
  private Material lineMaterial;

  [Conditional("VISUALIZE")]
  private void AddTrajectoryPoint(string name, Vector3 point)
  {
    this.trajectories[name].AddPoint(Time.time, point);
  }

  [Conditional("DEBUG")]
  private void Assert(bool condition, string text)
  {
    if (condition)
      return;
    UnityEngine.Debug.LogError((object) text);
  }

  [Conditional("DEBUG")]
  private void AssertSane(float f, string text)
  {
    if (Util.IsSaneNumber(f))
      return;
    UnityEngine.Debug.LogError((object) (text + "=" + (object) f));
  }

  [Conditional("DEBUG")]
  private void AssertSane(Vector3 vect, string text)
  {
    if (Util.IsSaneNumber(vect.x) && Util.IsSaneNumber(vect.y) && Util.IsSaneNumber(vect.z))
      return;
    UnityEngine.Debug.LogError((object) (text + "=" + (object) vect));
  }

  [Conditional("DEBUG")]
  private void AssertSane(Quaternion q, string text)
  {
    if (Util.IsSaneNumber(q.x) && Util.IsSaneNumber(q.y) && (Util.IsSaneNumber(q.z) && Util.IsSaneNumber(q.w)))
      return;
    UnityEngine.Debug.LogError((object) (text + "=" + (object) q));
  }

  private void Start()
  {
    this.tr = this.GetComponent(typeof (AlignmentTracker)) as AlignmentTracker;
    this.legC = this.GetComponent(typeof (LegController)) as LegController;
    this.legs = this.legC.legs;
    if (!this.legC.initialized)
    {
      UnityEngine.Debug.LogError((object) (this.name + ": Locomotion System has not been initialized."), (UnityEngine.Object) this);
      this.enabled = false;
    }
    this.legStates = new LegState[this.legs.Length];
    this.updateStates = true;
    this.ResetMotionStates();
    this.isActive = false;
    for (int index = 0; index < this.legs.Length; ++index)
    {
      this.trajectories.Add("leg" + (object) index + "heel", new TrajectoryVisualizer(this.legs[index].debugColor, 3f));
      this.trajectories.Add("leg" + (object) index + "toetip", new TrajectoryVisualizer(this.legs[index].debugColor, 3f));
      this.trajectories.Add("leg" + (object) index + "footbase", new TrajectoryVisualizer(this.legs[index].debugColor, 3f));
    }
  }

  private void OnEnable()
  {
    this.updateStates = true;
    if ((UnityEngine.Object) this.legC == (UnityEngine.Object) null)
      return;
    this.ResetMotionStates();
    if (this.legC.initialized)
      return;
    UnityEngine.Debug.LogError((object) (this.name + ": Locomotion System has not been initialized."), (UnityEngine.Object) this);
    this.enabled = false;
  }

  private AnimationClip GetDummyClip()
  {
    AnimationClip animationClip = new AnimationClip();
    AnimationCurve curve = new AnimationCurve(new Keyframe[2]{ new Keyframe(0.0f, 0.0f), new Keyframe(1f, 1f) });
    animationClip.SetCurve("dummy_path", typeof (Transform), "dummy_property", curve);
    return animationClip;
  }

  private void ResetMotionStates()
  {
    this.motionStates = new AnimationState[this.legC.motions.Length];
    this.cycleMotionStates = new AnimationState[this.legC.cycleMotions.Length];
    this.motionWeights = new float[this.legC.motions.Length];
    this.cycleMotionWeights = new float[this.legC.cycleMotions.Length];
    this.nonGroupMotionWeights = new float[this.legC.nonGroupMotions.Length];
    this.controlMotionState = this.GetComponent<Animation>()["LocomotionSystem"];
    if ((TrackedReference) this.controlMotionState == (TrackedReference) null)
    {
      this.GetComponent<Animation>().AddClip(this.GetDummyClip(), "LocomotionSystem");
      this.controlMotionState = this.GetComponent<Animation>()["LocomotionSystem"];
    }
    this.controlMotionState.enabled = true;
    this.controlMotionState.wrapMode = WrapMode.Loop;
    this.controlMotionState.weight = 1f;
    this.controlMotionState.layer = 10000;
    this.motionGroupStates = new MotionGroupState[this.legC.motionGroups.Length];
    int index1 = 0;
    for (int index2 = 0; index2 < this.legC.motions.Length; ++index2)
    {
      this.motionStates[index2] = this.GetComponent<Animation>()[this.legC.motions[index2].name];
      if ((TrackedReference) this.motionStates[index2] == (TrackedReference) null)
      {
        this.GetComponent<Animation>().AddClip(this.legC.motions[index2].animation, this.legC.motions[index2].name);
        this.motionStates[index2] = this.GetComponent<Animation>()[this.legC.motions[index2].name];
      }
      this.motionStates[index2].wrapMode = WrapMode.Loop;
      if (this.legC.motions[index2].motionType == MotionType.WalkCycle)
      {
        this.cycleMotionStates[index1] = this.motionStates[index2];
        this.cycleMotionStates[index1].speed = 0.0f;
        ++index1;
      }
    }
    for (int index2 = 0; index2 < this.motionGroupStates.Length; ++index2)
    {
      AnimationState animationState = this.GetComponent<Animation>()[this.legC.motionGroups[index2].name];
      if ((TrackedReference) animationState == (TrackedReference) null)
      {
        this.GetComponent<Animation>().AddClip(this.GetDummyClip(), this.legC.motionGroups[index2].name);
        animationState = this.GetComponent<Animation>()[this.legC.motionGroups[index2].name];
      }
      animationState.enabled = true;
      animationState.wrapMode = WrapMode.Loop;
      if (this.startAutomatically && index2 == 0)
        animationState.weight = 1f;
      this.motionGroupStates[index2] = new MotionGroupState();
      this.motionGroupStates[index2].controller = animationState;
      this.motionGroupStates[index2].motionStates = new AnimationState[this.legC.motionGroups[index2].motions.Length];
      this.motionGroupStates[index2].relativeWeights = new float[this.legC.motionGroups[index2].motions.Length];
      for (int index3 = 0; index3 < this.motionGroupStates[index2].motionStates.Length; ++index3)
      {
        this.motionGroupStates[index2].motionStates[index3] = this.GetComponent<Animation>()[this.legC.motionGroups[index2].motions[index3].name];
        this.motionGroupStates[index2].relativeWeights[index3] = 0.0f;
      }
      this.motionGroupStates[index2].primaryMotionIndex = 0;
    }
    this.nonGroupMotionStates = new AnimationState[this.legC.nonGroupMotions.Length];
    for (int index2 = 0; index2 < this.legC.nonGroupMotions.Length; ++index2)
    {
      this.nonGroupMotionStates[index2] = this.GetComponent<Animation>()[this.legC.nonGroupMotions[index2].name];
      if ((TrackedReference) this.nonGroupMotionStates[index2] == (TrackedReference) null)
      {
        this.GetComponent<Animation>().AddClip(this.legC.nonGroupMotions[index2].animation, this.legC.nonGroupMotions[index2].name);
        this.nonGroupMotionStates[index2] = this.GetComponent<Animation>()[this.legC.nonGroupMotions[index2].name];
        this.nonGroupMotionWeights[index2] = this.nonGroupMotionStates[index2].weight;
      }
    }
    for (int index2 = 0; index2 < this.legs.Length; ++index2)
      this.legStates[index2] = new LegState();
  }

  private void ResetSteps()
  {
    this.up = this.transform.up;
    this.forward = this.transform.forward;
    this.baseUpGround = this.up;
    this.legsUp = this.up;
    this.bodyUp = this.up;
    for (int index = 0; index < this.legs.Length; ++index)
    {
      this.legStates[index].stepFromTime = Time.time - 0.01f;
      this.legStates[index].stepToTime = Time.time;
      this.legStates[index].stepFromMatrix = this.FindGroundedBase(this.transform.TransformPoint(this.legStates[index].stancePosition / this.scale), this.transform.rotation, this.legStates[index].heelToetipVector, false);
      this.legStates[index].stepFromPosition = (Vector3) this.legStates[index].stepFromMatrix.GetColumn(3);
      this.legStates[index].stepToPosition = this.legStates[index].stepFromPosition;
      this.legStates[index].stepToMatrix = this.legStates[index].stepFromMatrix;
    }
    this.normalizedTime = 0.0f;
    this.cycleDuration = this.maxStepDuration;
    this.cycleDistance = 0.0f;
  }

  private void Update()
  {
    if ((double) Time.deltaTime == 0.0)
      return;
    this.scale = this.transform.lossyScale.z;
    this.horizontalVelocity = Util.ProjectOntoPlane(this.tr.velocity, this.up);
    this.horizontalVelocitySmoothed = Util.ProjectOntoPlane(this.tr.velocitySmoothed, this.up);
    this.speed = this.horizontalVelocity.magnitude;
    this.speedSmoothed = this.horizontalVelocitySmoothed.magnitude;
    this.objectVelocity = this.transform.InverseTransformPoint(this.horizontalVelocitySmoothed) - this.transform.InverseTransformPoint(Vector3.zero);
    bool flag1 = false;
    if ((double) (this.objectVelocity - this.usedObjectVelocity).magnitude > 1.0 / 500.0 * (double) Mathf.Min(this.objectVelocity.magnitude, this.usedObjectVelocity.magnitude) || this.updateStates)
    {
      flag1 = true;
      this.usedObjectVelocity = this.objectVelocity;
    }
    bool flag2 = false;
    float num1 = 1f / 1000f;
    for (int index1 = 0; index1 < this.legC.motionGroups.Length; ++index1)
    {
      MotionGroupState motionGroupState = this.motionGroupStates[index1];
      bool flag3 = false;
      bool flag4 = false;
      float num2 = motionGroupState.controller.weight;
      if (!motionGroupState.controller.enabled || (double) num2 < (double) num1)
        num2 = 0.0f;
      else if ((double) num2 > 1.0 - (double) num1)
        num2 = 1f;
      if ((double) Mathf.Abs(num2 - motionGroupState.weight) > (double) num1)
      {
        flag3 = true;
        flag2 = true;
        if ((double) motionGroupState.weight == 0.0 && (double) num2 > 0.0)
          flag4 = true;
        motionGroupState.weight = num2;
      }
      else if ((double) Mathf.Abs(motionGroupState.motionStates[motionGroupState.primaryMotionIndex].weight - motionGroupState.relativeWeights[motionGroupState.primaryMotionIndex] * motionGroupState.weight) > (double) num1 || motionGroupState.motionStates[motionGroupState.primaryMotionIndex].layer != motionGroupState.controller.layer)
        flag3 = true;
      if (flag1 || flag3)
      {
        if ((flag1 || flag4) && (double) motionGroupState.weight > 0.0)
        {
          flag2 = true;
          MotionGroupInfo motionGroupInfo = this.legC.motionGroups[index1];
          motionGroupState.relativeWeights = motionGroupInfo.GetMotionWeights(this.objectVelocity);
        }
        if ((double) motionGroupState.weight > 0.0)
        {
          float num3 = 0.0f;
          int layer = motionGroupState.controller.layer;
          for (int index2 = 0; index2 < motionGroupState.motionStates.Length; ++index2)
          {
            float num4 = motionGroupState.relativeWeights[index2] * motionGroupState.weight;
            motionGroupState.motionStates[index2].weight = num4;
            motionGroupState.motionStates[index2].enabled = (double) num4 > 0.0;
            motionGroupState.motionStates[index2].layer = layer;
            if ((double) num4 > (double) num3)
            {
              motionGroupState.primaryMotionIndex = index2;
              num3 = num4;
            }
          }
        }
        else
        {
          for (int index2 = 0; index2 < motionGroupState.motionStates.Length; ++index2)
          {
            motionGroupState.motionStates[index2].weight = 0.0f;
            motionGroupState.motionStates[index2].enabled = false;
          }
        }
      }
    }
    for (int index = 0; index < this.nonGroupMotionStates.Length; ++index)
    {
      float num2 = this.nonGroupMotionStates[index].weight;
      if (!this.nonGroupMotionStates[index].enabled)
        num2 = 0.0f;
      if ((double) Mathf.Abs(num2 - this.nonGroupMotionWeights[index]) > (double) num1 || (double) num2 == 0.0 && (double) this.nonGroupMotionWeights[index] != 0.0)
      {
        flag2 = true;
        this.nonGroupMotionWeights[index] = num2;
      }
    }
    bool flag5 = this.updateStates;
    if (flag2 || this.updateStates)
    {
      this.summedMotionWeight = 0.0f;
      this.summedCycleMotionWeight = 0.0f;
      int index1 = 0;
      for (int index2 = 0; index2 < this.legC.motions.Length; ++index2)
      {
        this.motionWeights[index2] = this.motionStates[index2].weight;
        this.summedMotionWeight += this.motionWeights[index2];
        if (this.legC.motions[index2].motionType == MotionType.WalkCycle)
        {
          this.cycleMotionWeights[index1] = this.motionWeights[index2];
          this.summedCycleMotionWeight += this.motionWeights[index2];
          ++index1;
        }
      }
      if ((double) this.summedMotionWeight == 0.0)
      {
        this.isActive = false;
        if (!((UnityEngine.Object) this.ghost != (UnityEngine.Object) null))
          return;
        (this.ghost.GetComponent(typeof (GhostOriginal)) as GhostOriginal).Synch();
        return;
      }
      if (!this.isActive)
        flag5 = true;
      this.isActive = true;
      for (int index2 = 0; index2 < this.legC.motions.Length; ++index2)
        this.motionWeights[index2] /= this.summedMotionWeight;
      if ((double) this.summedCycleMotionWeight > 0.0)
      {
        for (int index2 = 0; index2 < this.legC.cycleMotions.Length; ++index2)
          this.cycleMotionWeights[index2] /= this.summedCycleMotionWeight;
      }
      for (int index2 = 0; index2 < this.legs.Length; ++index2)
      {
        this.legStates[index2].stancePosition = Vector3.zero;
        this.legStates[index2].heelToetipVector = Vector3.zero;
      }
      for (int index2 = 0; index2 < this.legC.motions.Length; ++index2)
      {
        IMotionAnalyzer imotionAnalyzer = this.legC.motions[index2];
        float num2 = this.motionWeights[index2];
        if ((double) num2 > 0.0)
        {
          for (int index3 = 0; index3 < this.legs.Length; ++index3)
          {
            this.legStates[index3].stancePosition += imotionAnalyzer.cycles[index3].stancePosition * this.scale * num2;
            this.legStates[index3].heelToetipVector += imotionAnalyzer.cycles[index3].heelToetipVector * this.scale * num2;
          }
        }
      }
      if ((double) this.summedCycleMotionWeight > 0.0)
      {
        for (int index2 = 0; index2 < this.legs.Length; ++index2)
        {
          this.legStates[index2].liftTime = 0.0f;
          this.legStates[index2].liftoffTime = 0.0f;
          this.legStates[index2].postliftTime = 0.0f;
          this.legStates[index2].prelandTime = 0.0f;
          this.legStates[index2].strikeTime = 0.0f;
          this.legStates[index2].landTime = 0.0f;
        }
        for (int index2 = 0; index2 < this.legC.cycleMotions.Length; ++index2)
        {
          IMotionAnalyzer imotionAnalyzer = this.legC.cycleMotions[index2];
          float num2 = this.cycleMotionWeights[index2];
          if ((double) num2 > 0.0)
          {
            for (int index3 = 0; index3 < this.legs.Length; ++index3)
            {
              this.legStates[index3].liftTime += imotionAnalyzer.cycles[index3].liftTime * num2;
              this.legStates[index3].liftoffTime += imotionAnalyzer.cycles[index3].liftoffTime * num2;
              this.legStates[index3].postliftTime += imotionAnalyzer.cycles[index3].postliftTime * num2;
              this.legStates[index3].prelandTime += imotionAnalyzer.cycles[index3].prelandTime * num2;
              this.legStates[index3].strikeTime += imotionAnalyzer.cycles[index3].strikeTime * num2;
              this.legStates[index3].landTime += imotionAnalyzer.cycles[index3].landTime * num2;
            }
          }
        }
      }
      if ((double) this.summedCycleMotionWeight > 0.0)
      {
        for (int index2 = 0; index2 < this.legs.Length; ++index2)
        {
          Vector2 zero = Vector2.zero;
          for (int index3 = 0; index3 < this.legC.cycleMotions.Length; ++index3)
          {
            IMotionAnalyzer imotionAnalyzer = this.legC.cycleMotions[index3];
            float num2 = this.cycleMotionWeights[index3];
            if ((double) num2 > 0.0)
              zero += new Vector2(Mathf.Cos((float) ((double) imotionAnalyzer.cycles[index2].stanceTime * 2.0 * 3.14159274101257)), Mathf.Sin((float) ((double) imotionAnalyzer.cycles[index2].stanceTime * 2.0 * 3.14159274101257))) * num2;
          }
          this.legStates[index2].stanceTime = Util.Mod((float) ((double) Mathf.Atan2(zero.y, zero.x) / 2.0 / 3.14159274101257));
        }
      }
    }
    float num5 = this.controlMotionState.weight;
    if (!this.controlMotionState.enabled)
      num5 = 0.0f;
    this.locomotionWeight = Mathf.Clamp01(this.summedMotionWeight * num5);
    if (this.updateStates || flag5)
      this.ResetSteps();
    float num6 = 0.0f;
    float num7 = 0.0f;
    for (int index = 0; index < this.legC.motions.Length; ++index)
    {
      IMotionAnalyzer imotionAnalyzer = this.legC.motions[index];
      float num2 = this.motionWeights[index];
      if ((double) num2 > 0.0)
      {
        if (imotionAnalyzer.motionType == MotionType.WalkCycle)
          num6 += 1f / imotionAnalyzer.cycleDuration * num2;
        num7 += imotionAnalyzer.cycleSpeed * num2;
      }
    }
    float num8 = this.maxStepDuration;
    if ((double) num6 > 0.0)
      num8 = 1f / num6;
    float f = 1f;
    if ((double) this.speed != 0.0)
      f = num7 * this.scale / this.speed;
    if ((double) f > 0.0)
      num8 *= Mathf.Sqrt(f);
    float magnitude1 = Vector3.Project(this.tr.rotation * this.tr.angularVelocitySmoothed, this.up).magnitude;
    if ((double) magnitude1 > 0.0)
      num8 = Mathf.Min(this.maxStepRotation / magnitude1, num8);
    float magnitude2 = Util.ProjectOntoPlane(this.tr.accelerationSmoothed, this.up).magnitude;
    if ((double) magnitude2 > 0.0)
      num8 = Mathf.Clamp(this.maxStepAcceleration / magnitude2, num8 / 2f, num8);
    this.cycleDuration = Mathf.Min(num8, this.maxStepDuration);
    this.cycleDistance = this.cycleDuration * this.speed;
    bool flag6 = false;
    if (this.enableLegParking)
    {
      flag6 = true;
      for (int index = 0; index < this.legs.Length; ++index)
      {
        if (!this.legStates[index].parked)
          flag6 = false;
      }
    }
    if (!flag6)
    {
      this.normalizedTime = Util.Mod(this.normalizedTime + 1f / this.cycleDuration * Time.deltaTime);
      for (int index = 0; index < this.legC.cycleMotions.Length; ++index)
        this.cycleMotionStates[index].normalizedTime = this.legC.cycleMotions[index].GetType() != typeof (MotionAnalyzerBackwards) ? this.normalizedTime - this.legC.cycleMotions[index].cycleOffset : (float) (1.0 - ((double) this.normalizedTime - (double) this.legC.cycleMotions[index].cycleOffset));
    }
    this.updateStates = false;
    this.currentTime = Time.time;
    if (!((UnityEngine.Object) this.ghost != (UnityEngine.Object) null))
      return;
    (this.ghost.GetComponent(typeof (GhostOriginal)) as GhostOriginal).Synch();
  }

  private void FixedUpdate()
  {
    if ((double) Time.deltaTime == 0.0)
      return;
    this.tr.ControlledFixedUpdate();
  }

  private void LateUpdate()
  {
    if ((double) Time.deltaTime == 0.0)
      return;
    this.tr.ControlledLateUpdate();
    this.position = this.tr.position;
    this.rotation = this.tr.rotation;
    this.up = this.rotation * Vector3.up;
    this.forward = this.rotation * Vector3.forward;
    Vector3 vector3_1 = this.rotation * Vector3.right;
    if (!this.isActive || (double) this.currentTime != (double) Time.time)
      return;
    int layer = this.gameObject.layer;
    this.gameObject.layer = 2;
    for (int index = 0; index < this.legs.Length; ++index)
    {
      float num1 = Util.CyclicDiff(this.normalizedTime, this.legStates[index].stanceTime);
      bool flag1 = false;
      if ((double) num1 < (double) this.legStates[index].designatedCycleTimePrev - 0.5)
      {
        flag1 = true;
        ++this.legStates[index].stepNr;
        if (!this.legStates[index].parked)
        {
          this.legStates[index].stepFromTime = this.legStates[index].stepToTime;
          this.legStates[index].stepFromPosition = this.legStates[index].stepToPosition;
          this.legStates[index].stepFromMatrix = this.legStates[index].stepToMatrix;
          this.legStates[index].debugHistory.Clear();
          this.legStates[index].cycleTime = num1;
        }
        this.legStates[index].parked = false;
      }
      this.legStates[index].designatedCycleTimePrev = num1;
      this.legStates[index].stepToTime = Time.time + (1f - num1) * this.cycleDuration;
      float num2 = (this.legStates[index].strikeTime - num1) * this.cycleDuration;
      if ((double) num1 >= (double) this.legStates[index].strikeTime)
        this.legStates[index].cycleTime = num1;
      else
        this.legStates[index].cycleTime += (this.legStates[index].strikeTime - this.legStates[index].cycleTime) * Time.deltaTime / num2;
      if ((double) this.legStates[index].cycleTime >= (double) num1)
        this.legStates[index].cycleTime = num1;
      if ((double) this.legStates[index].cycleTime < (double) this.legStates[index].strikeTime)
      {
        float b = Mathf.InverseLerp(this.legStates[index].liftoffTime, this.legStates[index].strikeTime, this.legStates[index].cycleTime);
        Quaternion to = Quaternion.AngleAxis(this.tr.angularVelocitySmoothed.magnitude * (this.legStates[index].stepToTime - Time.time), this.tr.angularVelocitySmoothed) * this.tr.rotation;
        Quaternion rot;
        if ((double) this.legStates[index].cycleTime <= (double) this.legStates[index].liftoffTime)
        {
          rot = to;
        }
        else
        {
          Quaternion quaternion = Util.QuaternionFromMatrix(this.legStates[index].stepToMatrix);
          Quaternion from = Quaternion.FromToRotation(quaternion * Vector3.up, this.up) * quaternion;
          float angle = Mathf.Max(this.tr.angularVelocitySmoothed.magnitude * 3f, this.maxStepRotation / this.maxStepDuration) / b * Time.deltaTime;
          rot = Util.ConstantSlerp(from, to, angle);
        }
        float num3 = Vector3.Dot(this.tr.angularVelocitySmoothed, this.up);
        Vector3 originalVector;
        if ((double) num3 * (double) this.cycleDuration < 5.0)
        {
          originalVector = this.tr.position + rot * this.legStates[index].stancePosition + this.horizontalVelocity * (this.legStates[index].stepToTime - Time.time);
        }
        else
        {
          Vector3 vector3_2 = Vector3.Cross(this.up, this.horizontalVelocity) / (float) ((double) num3 * 3.14159274101257 / 180.0);
          Vector3 vector3_3 = vector3_2 + Quaternion.AngleAxis(num3 * (this.legStates[index].stepToTime - Time.time), this.up) * -vector3_2;
          originalVector = this.tr.position + rot * this.legStates[index].stancePosition + vector3_3;
        }
        Matrix4x4 groundedBase = this.FindGroundedBase(Util.SetHeight(originalVector, this.position + this.legC.groundPlaneHeight * this.up * this.scale, this.up), rot, this.legStates[index].heelToetipVector, true);
        Vector3 vector3_4 = (Vector3) groundedBase.GetColumn(3);
        if (flag1)
        {
          this.legStates[index].stepToPosition = vector3_4;
          this.legStates[index].stepToPositionGoal = vector3_4;
        }
        else
        {
          float num4 = Mathf.Max((float) ((double) this.speed * 3.0 + (double) this.tr.accelerationSmoothed.magnitude / 10.0), (float) ((double) this.legs[index].footLength * (double) this.scale * 3.0));
          float num5 = this.legStates[index].cycleTime / this.legStates[index].strikeTime;
          if ((double) (vector3_4 - this.legStates[index].stepToPosition).sqrMagnitude < (double) Mathf.Pow(num4 * (float) (1.0 / (double) num5 - 1.0), 2f))
            this.legStates[index].stepToPositionGoal = vector3_4;
          Vector3 vector3_2 = this.legStates[index].stepToPositionGoal - this.legStates[index].stepToPosition;
          if (vector3_2 != Vector3.zero && (double) num2 > 0.0)
          {
            float magnitude = vector3_2.magnitude;
            float num6 = Mathf.Min(magnitude, Mathf.Max(num4 / Mathf.Max(0.1f, b) * Time.deltaTime, (float) ((1.0 + 2.0 * (double) Mathf.Pow(num5 - 1f, 2f)) * ((double) Time.deltaTime / (double) num2)) * magnitude));
            this.legStates[index].stepToPosition += (this.legStates[index].stepToPositionGoal - this.legStates[index].stepToPosition) / magnitude * num6;
          }
        }
        groundedBase.SetColumn(3, (Vector4) this.legStates[index].stepToPosition);
        groundedBase[3, 3] = 1f;
        this.legStates[index].stepToMatrix = groundedBase;
      }
      if (this.enableLegParking)
      {
        bool flag2 = (double) Util.ProjectOntoPlane(this.legStates[index].stepToPosition - this.legStates[index].stepFromPosition, this.up).magnitude > (double) this.minStepDistance || (double) Vector3.Angle((Vector3) this.legStates[index].stepToMatrix.GetColumn(2), (Vector3) this.legStates[index].stepFromMatrix.GetColumn(2)) > (double) this.maxStepRotation / 2.0;
        if (flag1 && !flag2)
          this.legStates[index].parked = true;
        if (this.legStates[index].parked && (double) num1 < 0.670000016689301 && flag2)
          this.legStates[index].parked = false;
        if (this.legStates[index].parked)
          this.legStates[index].cycleTime = 0.0f;
      }
    }
    Vector3 v = Quaternion.Inverse(this.tr.rotation) * this.horizontalVelocity;
    v.y = 0.0f;
    if ((double) v.sqrMagnitude > 0.0)
      v = v.normalized;
    Vector3[] vector3Array = new Vector3[this.legs.Length];
    Vector3 zero1 = Vector3.zero;
    Vector3 zero2 = Vector3.zero;
    Vector3 zero3 = Vector3.zero;
    float num7 = 0.0f;
    for (int index = 0; index < this.legs.Length; ++index)
    {
      float num1 = (float) ((double) Mathf.Cos((float) ((double) this.legStates[index].cycleTime * 2.0 * 3.14159274101257)) / 2.0 + 0.5);
      num7 += num1 + 1f / 1000f;
      float num2 = (float) (-(double) Mathf.Cos(Mathf.InverseLerp(this.legStates[index].liftTime, this.legStates[index].landTime, this.legStates[index].cycleTime) * 3.141593f) / 2.0 + 0.5);
      Vector3 vector3_2 = this.transform.TransformDirection(-this.legStates[index].stancePosition) * this.scale;
      vector3Array[index] = (this.legStates[index].stepFromPosition + this.legStates[index].stepFromMatrix.MultiplyVector(v) * this.cycleDistance * this.legStates[index].cycleTime) * (1f - num2) + (this.legStates[index].stepToPosition + this.legStates[index].stepToMatrix.MultiplyVector(v) * this.cycleDistance * (this.legStates[index].cycleTime - 1f)) * num2;
      if (float.IsNaN(vector3Array[index].x) || float.IsNaN(vector3Array[index].y) || float.IsNaN(vector3Array[index].z))
        UnityEngine.Debug.LogError((object) ("legStates[leg].cycleTime=" + (object) this.legStates[index].cycleTime + ", strideSCurve=" + (object) num2 + ", tangentDir=" + (object) v + ", cycleDistance=" + (object) this.cycleDistance + ", legStates[leg].stepFromPosition=" + (object) this.legStates[index].stepFromPosition + ", legStates[leg].stepToPosition=" + (object) this.legStates[index].stepToPosition + ", legStates[leg].stepToMatrix.MultiplyVector(tangentDir)=" + (object) this.legStates[index].stepToMatrix.MultiplyVector(v) + ", legStates[leg].stepFromMatrix.MultiplyVector(tangentDir)=" + (object) this.legStates[index].stepFromMatrix.MultiplyVector(v)));
      zero1 += (vector3Array[index] + vector3_2) * (num1 + 1f / 1000f);
      zero3 += vector3Array[index];
      zero2 += (this.legStates[index].stepToPosition - this.legStates[index].stepFromPosition) * (float) (1.0 - (double) num1 + 1.0 / 1000.0);
    }
    Vector3 end1 = zero3 / (float) this.legs.Length;
    Vector3 vector3_5 = zero1 / num7;
    if (float.IsNaN(vector3_5.x) || float.IsNaN(vector3_5.y) || float.IsNaN(vector3_5.z))
      vector3_5 = this.position;
    Vector3 referenceHeightVector = vector3_5 + this.up * this.legC.groundPlaneHeight;
    Vector3 vector3_6 = this.up;
    if ((double) this.groundHugX >= 0.0 || (double) this.groundHugZ >= 0.0)
    {
      Vector3 lhs = this.up * 0.1f;
      for (int index = 0; index < this.legs.Length; ++index)
      {
        Vector3 vector3_2 = vector3Array[index] - end1;
        lhs += Vector3.Cross(Vector3.Cross(vector3_2, this.baseUpGround), vector3_2);
        UnityEngine.Debug.DrawLine(vector3Array[index], end1);
      }
      float num1 = Vector3.Dot(lhs, this.up);
      if ((double) num1 > 0.0)
        this.baseUpGround = lhs / num1;
      vector3_6 = (double) this.groundHugX < 1.0 || (double) this.groundHugZ < 1.0 ? (this.up + this.groundHugX * Vector3.Project(this.baseUpGround, vector3_1) + this.groundHugZ * Vector3.Project(this.baseUpGround, this.forward)).normalized : this.baseUpGround.normalized;
    }
    Vector3 lhs1 = this.up;
    if (zero2 != Vector3.zero)
      lhs1 = Vector3.Cross(zero2, Vector3.Cross(this.up, zero2));
    Vector3 vector = lhs1 / Vector3.Dot(lhs1, this.up);
    Vector3 vector3_7 = Vector3.zero;
    if ((double) this.accelerateTiltAmount * (double) this.accelerateTiltSensitivity != 0.0)
    {
      float to1 = Vector3.Dot(this.tr.accelerationSmoothed * this.accelerateTiltSensitivity * this.accelerateTiltAmount, vector3_1) * (1f - this.groundHugX);
      float to2 = Vector3.Dot(this.tr.accelerationSmoothed * this.accelerateTiltSensitivity * this.accelerateTiltAmount, this.forward) * (1f - this.groundHugZ);
      this.accelerationTiltX = Mathf.Lerp(this.accelerationTiltX, to1, Time.deltaTime * 10f);
      this.accelerationTiltZ = Mathf.Lerp(this.accelerationTiltZ, to2, Time.deltaTime * 10f);
      vector3_7 = (this.accelerationTiltX * vector3_1 + this.accelerationTiltZ * this.forward) * (float) (1.0 - 1.0 / ((double) this.speedSmoothed * (double) this.accelerateTiltSensitivity + 1.0));
    }
    Vector3 vector3_8 = Vector3.zero;
    if ((double) this.climbTiltAmount * (double) this.climbTiltAmount != 0.0)
      vector3_8 = (Vector3.Project(vector, vector3_1) * (1f - this.groundHugX) + Vector3.Project(vector, this.forward) * (1f - this.groundHugZ)) * -this.climbTiltAmount * (float) (1.0 - 1.0 / ((double) this.speedSmoothed * (double) this.climbTiltSensitivity + 1.0));
    this.bodyUp = (vector3_6 + vector3_7 + vector3_8).normalized;
    Quaternion rotation = Quaternion.AngleAxis(Vector3.Angle(this.up, this.bodyUp), Vector3.Cross(this.up, this.bodyUp));
    this.legsUp = (this.up + vector3_7).normalized;
    Quaternion quaternion1 = Quaternion.AngleAxis(Vector3.Angle(this.up, this.legsUp), Vector3.Cross(this.up, this.legsUp));
    for (int leg = 0; leg < this.legs.Length; ++leg)
    {
      float t1 = Mathf.InverseLerp(this.legStates[leg].liftoffTime, this.legStates[leg].strikeTime, this.legStates[leg].cycleTime);
      float num1 = Mathf.InverseLerp(this.legStates[leg].liftTime, this.legStates[leg].landTime, this.legStates[leg].cycleTime);
      int phase;
      float flightTime;
      if ((double) this.legStates[leg].cycleTime < (double) this.legStates[leg].liftoffTime)
      {
        phase = 0;
        flightTime = Mathf.InverseLerp(0.0f, this.legStates[leg].liftoffTime, this.legStates[leg].cycleTime);
      }
      else if ((double) this.legStates[leg].cycleTime > (double) this.legStates[leg].strikeTime)
      {
        phase = 2;
        flightTime = Mathf.InverseLerp(this.legStates[leg].strikeTime, 1f, this.legStates[leg].cycleTime);
      }
      else
      {
        phase = 1;
        flightTime = t1;
      }
      Vector3 zero4 = Vector3.zero;
      for (int index = 0; index < this.legC.motions.Length; ++index)
      {
        IMotionAnalyzer imotionAnalyzer = this.legC.motions[index];
        float num2 = this.motionWeights[index];
        if ((double) num2 > 0.0)
          zero4 += imotionAnalyzer.GetFlightFootPosition(leg, flightTime, phase) * num2;
      }
      Vector3 from = this.legStates[leg].stepFromPosition;
      Vector3 to = this.legStates[leg].stepToPosition;
      Vector3 lhs2 = this.legStates[leg].stepFromMatrix.MultiplyVector(Vector3.up);
      Vector3 lhs3 = this.legStates[leg].stepToMatrix.MultiplyVector(Vector3.up);
      float num3 = Mathf.Sin(zero4.z * 3.141593f);
      float t2 = Mathf.Sin(t1 * 3.141593f);
      this.legStates[leg].footBase = from * (1f - zero4.z) + to * zero4.z;
      Vector3 vector3_2 = this.tr.position + this.tr.rotation * this.legStates[leg].stancePosition - Vector3.Lerp(from, to, this.legStates[leg].cycleTime);
      this.legStates[leg].footBase += Util.ProjectOntoPlane(vector3_2 * num3, this.legsUp);
      Vector3 vector3_3 = (from + to) / 2f;
      float num4 = (float) ((double) Mathf.Max(Vector3.Dot(lhs2, from - vector3_3) / Vector3.Dot(lhs2, this.legsUp), Vector3.Dot(lhs3, to - vector3_3) / Vector3.Dot(lhs3, this.legsUp)) * 2.0 / 3.14159274101257);
      this.legStates[leg].footBase += num4 * num3 * this.legsUp;
      Quaternion quaternion2 = Quaternion.Slerp(Util.QuaternionFromMatrix(this.legStates[leg].stepFromMatrix), Util.QuaternionFromMatrix(this.legStates[leg].stepToMatrix), t1);
      this.legStates[leg].footBaseRotation = (double) num1 >= 0.5 ? Quaternion.Slerp(this.rotation, Util.QuaternionFromMatrix(this.legStates[leg].stepToMatrix), (float) ((double) num1 * 2.0 - 1.0)) : Quaternion.Slerp(Util.QuaternionFromMatrix(this.legStates[leg].stepFromMatrix), this.rotation, num1 * 2f);
      this.legStates[leg].footBaseRotation = Quaternion.FromToRotation(this.legStates[leg].footBaseRotation * Vector3.up, quaternion2 * Vector3.up) * this.legStates[leg].footBaseRotation;
      Vector3 lhs4 = Vector3.Lerp(this.legStates[leg].footBase, Util.SetHeight(this.legStates[leg].footBase, referenceHeightVector, this.legsUp), t2);
      if ((double) Vector3.Dot(lhs4, this.legsUp) > (double) Vector3.Dot(this.legStates[leg].footBase, this.legsUp))
        this.legStates[leg].footBase = lhs4;
      this.legStates[leg].footBase += zero4.y * this.legsUp * this.scale;
      Vector3 normalized = Vector3.Cross(this.legsUp, to - from).normalized;
      this.legStates[leg].footBase += zero4.x * normalized * this.scale;
      UnityEngine.Debug.DrawLine(this.legStates[leg].footBase, this.legStates[leg].footBase + this.legStates[leg].footBaseRotation * this.legStates[leg].heelToetipVector, this.legs[leg].debugColor);
    }
    if ((double) this.locomotionWeight < 1.0)
    {
      for (int index = 0; index < this.legs.Length; ++index)
      {
        Vector3 from = -MotionAnalyzer.GetHeelOffset(this.legs[index].ankle, this.legs[index].ankleHeelVector, this.legs[index].toe, this.legs[index].toeToetipVector, this.legStates[index].heelToetipVector, this.legStates[index].footBaseRotation) + this.legs[index].ankle.TransformPoint(this.legs[index].ankleHeelVector);
        this.legStates[index].footBase = Vector3.Lerp(from, this.legStates[index].footBase, this.locomotionWeight);
        this.legStates[index].footBaseRotation = Quaternion.Slerp(this.rotation, this.legStates[index].footBaseRotation, this.locomotionWeight);
      }
    }
    this.legC.root.transform.rotation = this.tr.rotation * Quaternion.Inverse(this.transform.rotation) * rotation * this.legC.root.transform.rotation;
    for (int index = 0; index < this.legs.Length; ++index)
      this.legs[index].hip.rotation = quaternion1 * Quaternion.Inverse(rotation) * this.legs[index].hip.rotation;
    Vector3 position = this.legC.root.transform.position;
    Vector3 start1 = this.transform.TransformPoint(this.legC.hipAverage);
    Vector3 end2 = this.transform.TransformPoint(this.legC.hipAverageGround);
    this.legC.root.transform.position = position + rotation * (position - start1) - position - start1 + quaternion1 * (start1 - end2) - start1 - end2 + this.position - this.transform.position;
    for (int index = 0; index < this.legs.Length; ++index)
    {
      this.legStates[index].hipReference = this.legs[index].hip.position;
      this.legStates[index].ankleReference = this.legs[index].ankle.position;
    }
    for (int index1 = 1; index1 <= 2; ++index1)
    {
      for (int index2 = 0; index2 < this.legs.Length; ++index2)
        this.legStates[index2].ankle = MotionAnalyzer.GetAnklePosition(this.legs[index2].ankle, this.legs[index2].ankleHeelVector, this.legs[index2].toe, this.legs[index2].toeToetipVector, this.legStates[index2].heelToetipVector, this.legStates[index2].footBase, this.legStates[index2].footBaseRotation);
      this.FindHipOffset();
      for (int leg = 0; leg < this.legs.Length; ++leg)
        this.AdjustLeg(leg, this.legStates[leg].ankle, index1 == 2);
    }
    for (int index1 = 0; index1 < this.legs.Length; ++index1)
    {
      for (int index2 = 0; index2 < this.legs[index1].legChain.Length - 1; ++index2)
        UnityEngine.Debug.DrawLine(this.legs[index1].legChain[index2].position, this.legs[index1].legChain[index2 + 1].position, this.legs[index1].debugColor);
    }
    Vector3 start2 = this.position;
    UnityEngine.Debug.DrawRay(start2, this.up / 10f, Color.white);
    UnityEngine.Debug.DrawRay(start2 - this.forward / 20f, this.forward / 10f, Color.white);
    UnityEngine.Debug.DrawLine(start1, end2, Color.white);
    UnityEngine.Debug.DrawRay(start2, vector3_6 * 2f, Color.blue);
    UnityEngine.Debug.DrawRay(start1, this.bodyUp * 2f, Color.yellow);
    this.gameObject.layer = layer;
  }

  public Matrix4x4 FindGroundedBase(Vector3 pos, Quaternion rot, Vector3 heelToToetipVector, bool avoidLedges)
  {
    Vector3 lhs1 = new Vector3();
    Vector3 lhs2 = new Vector3();
    Vector3 toDirection1 = new Vector3();
    Vector3 toDirection2 = new Vector3();
    bool flag1 = false;
    bool flag2 = false;
    bool flag3 = false;
    RaycastHit hitInfo;
    if (Physics.Raycast(pos + this.up * this.maxStepHeight, -this.up, out hitInfo, this.maxStepHeight * 2f, (int) this.groundLayers))
    {
      flag3 = true;
      lhs1 = hitInfo.point;
      if ((double) Vector3.Angle(hitInfo.normal, this.up) < (double) this.maxSlopeAngle)
      {
        toDirection1 = hitInfo.normal;
        flag1 = true;
      }
    }
    Vector3 vector3_1 = rot * heelToToetipVector;
    float magnitude = vector3_1.magnitude;
    if (Physics.Raycast(pos + this.up * this.maxStepHeight + vector3_1, -this.up, out hitInfo, this.maxStepHeight * 2f, (int) this.groundLayers))
    {
      flag3 = true;
      lhs2 = hitInfo.point;
      if ((double) Vector3.Angle(hitInfo.normal, this.up) < (double) this.maxSlopeAngle)
      {
        toDirection2 = hitInfo.normal;
        flag2 = true;
      }
    }
    if (!flag3)
    {
      Matrix4x4 identity = Matrix4x4.identity;
      identity.SetTRS(pos, rot, Vector3.one);
      return identity;
    }
    bool flag4 = false;
    if (avoidLedges)
    {
      if (!flag1 && !flag2)
        flag1 = true;
      else if (flag1 && flag2)
      {
        Vector3 normalized = (toDirection1 + toDirection2).normalized;
        float num1 = Vector3.Dot(lhs1, normalized);
        float num2 = Vector3.Dot(lhs2, normalized);
        if ((double) num1 < (double) num2)
          flag1 = false;
        if ((double) Mathf.Abs(num1 - num2) > (double) magnitude / 4.0)
          flag4 = true;
      }
      else
        flag4 = true;
    }
    Vector3 fromDirection = rot * Vector3.up;
    Vector3 p;
    if (flag1)
    {
      if (toDirection1 != Vector3.zero)
        rot = Quaternion.FromToRotation(fromDirection, toDirection1) * rot;
      p = lhs1;
      if (flag4)
      {
        Vector3 vector3_2 = rot * heelToToetipVector;
        p -= vector3_2 * 0.5f;
      }
    }
    else
    {
      if (toDirection2 != Vector3.zero)
        rot = Quaternion.FromToRotation(fromDirection, toDirection2) * rot;
      Vector3 vector3_2 = rot * heelToToetipVector;
      p = lhs2 - vector3_2;
      if (flag4)
        p += vector3_2 * 0.5f;
    }
    return Util.MatrixFromQuaternionPosition(rot, p);
  }

  public void FindHipOffset()
  {
    float num1 = float.PositiveInfinity;
    float num2 = float.PositiveInfinity;
    float num3 = 0.0f;
    for (int index = 0; index < this.legs.Length; ++index)
    {
      Vector3 v = this.legStates[index].ankleReference - this.legStates[index].hipReference;
      float magnitude1 = v.magnitude;
      float magnitude2 = Util.ProjectOntoPlane(v, this.legsUp).magnitude;
      Vector3 vector3 = Util.ProjectOntoPlane(this.legStates[index].ankle - this.legs[index].hip.position, this.legsUp);
      float num4 = vector3.magnitude - magnitude2;
      if ((double) num4 > 0.0)
      {
        float num5 = (float) ((double) this.legs[index].legLength * (double) this.scale * 0.999000012874603) - magnitude2;
        this.legStates[index].ankle = this.legStates[index].ankle - vector3 + vector3.normalized * (magnitude2 + (float) (1.0 - 1.0 / ((double) num4 / (double) num5 + 1.0)) * num5);
      }
      float[] sphereIntersections1 = Util.GetLineSphereIntersections(Vector3.zero, this.legsUp, this.legStates[index].ankle - this.legs[index].hip.position, magnitude1);
      float num6 = sphereIntersections1 == null ? Vector3.Dot(this.legStates[index].footBase - this.legs[index].hip.position, this.legsUp) : sphereIntersections1[1];
      float[] sphereIntersections2 = Util.GetLineSphereIntersections(Vector3.zero, this.legsUp, this.legStates[index].ankle - this.legs[index].hip.position, (float) ((double) this.legs[index].legLength * (double) this.scale * 0.999000012874603));
      float num7;
      if (sphereIntersections2 != null)
      {
        num7 = sphereIntersections2[1];
      }
      else
      {
        num7 = Vector3.Dot(this.legStates[index].ankle - this.legs[index].hip.position, this.legsUp);
        UnityEngine.Debug.Log((object) (this.gameObject.name + ": Line-sphere intersection failed for leg " + (object) index + ", hipMaxHeight."));
      }
      if ((double) num6 < (double) num1)
        num1 = num6;
      if ((double) num7 < (double) num2)
        num2 = num7;
      num3 += num6 / (float) this.legs.Length;
    }
    if ((double) num1 > (double) num2)
      num1 = num2;
    float num8 = num3 - num1;
    float num9 = num2 - num1;
    float num10 = num1;
    if ((double) num8 + (double) num9 > 0.0)
      num10 += (float) ((double) num8 * (double) num9 / ((double) num8 + (double) num9));
    this.legC.root.position += num10 * this.legsUp;
  }

  public void AdjustLeg(int leg, Vector3 desiredAnklePosition, bool secondPass)
  {
    LegInfo legInfo = this.legs[leg];
    LegState legState = this.legStates[leg];
    Quaternion to = secondPass ? legInfo.ankle.rotation : this.legStates[leg].footBaseRotation * Quaternion.Inverse(this.rotation) * legInfo.ankle.rotation;
    (legInfo.legChain.Length != 3 ? (IKSolver) new IKSimple() : (IKSolver) new IK1JointAnalytic()).Solve(legInfo.legChain, desiredAnklePosition);
    Vector3 position1 = legInfo.hip.position;
    Vector3 position2 = legInfo.ankle.position;
    if (!secondPass)
    {
      Quaternion from = Quaternion.FromToRotation(this.forward, Util.ProjectOntoPlane(this.legStates[leg].footBaseRotation * Vector3.forward, this.up)) * legInfo.ankle.rotation;
      legInfo.ankle.rotation = Quaternion.Slerp(from, to, 1f - legState.GetFootGrounding(legState.cycleTime));
    }
    else
    {
      Vector3 normal = position2 - position1;
      Quaternion quaternion = Quaternion.Slerp(Quaternion.identity, Quaternion.FromToRotation(Util.ProjectOntoPlane(this.forward, normal), Util.ProjectOntoPlane(this.legStates[leg].footBaseRotation * Vector3.forward, normal)), 0.5f);
      legInfo.hip.rotation = quaternion * legInfo.hip.rotation;
      legInfo.ankle.rotation = to;
    }
  }

  private void OnRenderObject()
  {
    this.CreateLineMaterial();
    this.lineMaterial.SetPass(0);
    if (!this.isActive)
      return;
    if (this.renderFootMarkers)
      this.RenderFootMarkers();
    if (this.renderBlendingGraph)
      this.RenderBlendingGraph();
    if (this.renderCycleGraph)
      this.RenderCycleGraph();
    using (Dictionary<string, TrajectoryVisualizer>.Enumerator enumerator = this.trajectories.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.Render();
    }
  }

  private void CreateLineMaterial()
  {
    if ((bool) ((UnityEngine.Object) this.lineMaterial))
      return;
    this.lineMaterial = new Material("Shader \"Lines/Colored Blended\" {SubShader { Pass { \tBindChannels { Bind \"Color\",color } \tBlend SrcAlpha OneMinusSrcAlpha \tZWrite Off Cull Off Fog { Mode Off } } } }");
    this.lineMaterial.hideFlags = HideFlags.HideAndDontSave;
    this.lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
  }

  private void DrawLine(Vector3 a, Vector3 b, Color color)
  {
    GL.Color(color);
    GL.Vertex(a);
    GL.Vertex(b);
  }

  public void RenderBlendingGraph()
  {
    Vector3 vector3_1 = this.transform.worldToLocalMatrix.MultiplyVector(this.horizontalVelocity);
    Matrix4x4 matrix = Util.CreateMatrix(this.transform.right, this.transform.forward, this.transform.up, this.transform.TransformPoint(this.legC.hipAverage));
    float num1 = (Camera.main.transform.position - this.transform.TransformPoint(this.legC.hipAverage)).magnitude / 2f;
    DrawArea drawArea = (DrawArea) new DrawArea3D(new Vector3(-num1, -num1, 0.0f), new Vector3(num1, num1, 0.0f), matrix);
    GL.Begin(7);
    drawArea.DrawRect(new Vector3(0.0f, 0.0f, 0.0f), new Vector3(1f, 1f, 0.0f), new Color(0.0f, 0.0f, 0.0f, 0.2f));
    GL.End();
    Color c1 = new Color(0.7f, 0.7f, 0.7f, 1f);
    float a = 0.0f;
    for (int index = 0; index < this.legC.motions.Length; ++index)
    {
      IMotionAnalyzer imotionAnalyzer = this.legC.motions[index];
      a = Mathf.Max(Mathf.Max(a, Mathf.Abs(imotionAnalyzer.cycleVelocity.x)), Mathf.Abs(imotionAnalyzer.cycleVelocity.z));
    }
    float num2 = (double) a != 0.0 ? a * 1.2f : 1f;
    GL.Begin(1);
    drawArea.DrawLine(new Vector3(0.5f, 0.0f, 0.0f), new Vector3(0.5f, 1f, 0.0f), c1);
    drawArea.DrawLine(new Vector3(0.0f, 0.5f, 0.0f), new Vector3(1f, 0.5f, 0.0f), c1);
    drawArea.DrawLine(new Vector3(0.0f, 0.0f, 0.0f), new Vector3(1f, 0.0f, 0.0f), c1);
    drawArea.DrawLine(new Vector3(1f, 0.0f, 0.0f), new Vector3(1f, 1f, 0.0f), c1);
    drawArea.DrawLine(new Vector3(1f, 1f, 0.0f), new Vector3(0.0f, 1f, 0.0f), c1);
    drawArea.DrawLine(new Vector3(0.0f, 1f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f), c1);
    GL.End();
    for (int index1 = 0; index1 < this.motionGroupStates.Length; ++index1)
    {
      Vector3 vector3_2 = Quaternion.AngleAxis((float) (((double) index1 + 0.5) * 360.0) / (float) this.motionGroupStates.Length, Vector3.one) * Vector3.right;
      Color c2 = new Color(vector3_2.x, vector3_2.y, vector3_2.z);
      IMotionAnalyzer[] imotionAnalyzerArray = this.legC.motionGroups[index1].motions;
      GL.Begin(7);
      Color c3 = c2 * 0.4f;
      c3.a = 0.8f;
      for (int index2 = 0; index2 < imotionAnalyzerArray.Length; ++index2)
      {
        IMotionAnalyzer imotionAnalyzer = imotionAnalyzerArray[index2];
        float num3 = (float) ((double) imotionAnalyzer.cycleVelocity.x / (double) num2 / 2.0 + 0.5);
        float num4 = (float) ((double) imotionAnalyzer.cycleVelocity.z / (double) num2 / 2.0 + 0.5);
        float num5 = 0.02f;
        drawArea.DrawDiamond(new Vector3(num3 - num5, num4 - num5, 0.0f), new Vector3(num3 + num5, num4 + num5, 0.0f), c3);
      }
      GL.End();
      if ((double) this.motionGroupStates[index1].weight != 0.0)
      {
        float[] numArray = this.motionGroupStates[index1].relativeWeights;
        GL.Begin(7);
        c2.a = this.motionGroupStates[index1].weight;
        for (int index2 = 0; index2 < imotionAnalyzerArray.Length; ++index2)
        {
          IMotionAnalyzer imotionAnalyzer = imotionAnalyzerArray[index2];
          float num3 = (float) ((double) imotionAnalyzer.cycleVelocity.x / (double) num2 / 2.0 + 0.5);
          float num4 = (float) ((double) imotionAnalyzer.cycleVelocity.z / (double) num2 / 2.0 + 0.5);
          float num5 = Mathf.Pow(numArray[index2], 0.5f) * 0.05f;
          drawArea.DrawRect(new Vector3(num3 - num5, num4 - num5, 0.0f), new Vector3(num3 + num5, num4 + num5, 0.0f), c2);
        }
        GL.End();
      }
    }
    GL.Begin(7);
    float num6 = (float) ((double) vector3_1.x / (double) num2 / 2.0 + 0.5);
    float num7 = (float) ((double) vector3_1.z / (double) num2 / 2.0 + 0.5);
    float num8 = 0.02f;
    drawArea.DrawRect(new Vector3(num6 - num8, num7 - num8, 0.0f), new Vector3(num6 + num8, num7 + num8, 0.0f), new Color(0.0f, 0.0f, 0.0f, 1f));
    float num9 = num8 / 2f;
    drawArea.DrawRect(new Vector3(num6 - num9, num7 - num9, 0.0f), new Vector3(num6 + num9, num7 + num9, 0.0f), new Color(1f, 1f, 1f, 1f));
    GL.End();
  }

  public void RenderCycleGraph()
  {
    float num1 = (float) Camera.main.pixelWidth;
    float num2 = (float) Camera.main.pixelHeight;
    Color c1 = new Color(0.7f, 0.7f, 0.7f, 1f);
    DrawArea drawArea = new DrawArea(new Vector3(num1 - 0.49f * num2, 0.01f * num2, 0.0f), new Vector3(num1 - 0.01f * num2, 0.49f * num2, 0.0f));
    drawArea.canvasMin = new Vector3(-1.1f, -1.1f, 0.0f);
    drawArea.canvasMax = new Vector3(1.1f, 1.1f, 0.0f);
    GL.Begin(7);
    drawArea.DrawRect(new Vector3(-1f, -1f, 0.0f), new Vector3(1f, 1f, 0.0f), new Color(0.0f, 0.0f, 0.0f, 0.2f));
    GL.End();
    GL.Begin(1);
    drawArea.DrawLine(-0.9f * Vector3.up, -1.1f * Vector3.up, c1);
    for (int index = 0; index < 90; ++index)
      drawArea.DrawLine(Quaternion.AngleAxis((float) ((double) index / 90.0 * 360.0), Vector3.forward) * Vector3.up, Quaternion.AngleAxis((float) ((double) (index + 1) / 90.0 * 360.0), Vector3.forward) * Vector3.up, c1);
    for (int index = 0; index < this.legs.Length; ++index)
    {
      Color c2 = this.legs[index].debugColor;
      Vector3 vector3_1 = Quaternion.AngleAxis(360f * this.legStates[index].liftoffTime, -Vector3.forward) * -Vector3.up;
      drawArea.DrawLine(vector3_1 * 0.9f, vector3_1 * 1.1f, c2);
      Vector3 vector3_2 = Quaternion.AngleAxis(360f * this.legStates[index].strikeTime, -Vector3.forward) * -Vector3.up;
      drawArea.DrawLine(vector3_2 * 0.9f, vector3_2 * 1.1f, c2);
    }
    GL.End();
    GL.Begin(7);
    for (int index = 0; index < this.legs.Length; ++index)
    {
      Color c2 = this.legs[index].debugColor;
      Vector3 vector3_1 = Quaternion.AngleAxis(360f * this.legStates[index].cycleTime, -Vector3.forward) * -Vector3.up;
      drawArea.DrawRect(vector3_1 - Vector3.one * 0.1f, vector3_1 + Vector3.one * 0.1f, c2);
      Vector3 vector3_2 = Quaternion.AngleAxis(360f * Util.CyclicDiff(this.normalizedTime, this.legStates[index].stanceTime), -Vector3.forward) * -Vector3.up * 0.8f;
      drawArea.DrawRect(vector3_2 - Vector3.one * 0.05f, vector3_2 + Vector3.one * 0.05f, c2);
    }
    GL.End();
  }

  public void RenderMotionCycles()
  {
    float num = (float) Camera.main.pixelHeight;
    Color c1 = new Color(0.7f, 0.7f, 0.7f, 1f);
    for (int index1 = 0; index1 < this.legC.cycleMotions.Length; ++index1)
    {
      DrawArea drawArea = new DrawArea(new Vector3((float) (0.00999999977648258 * (double) num + 0.200000002980232 * (double) num * (double) index1), 0.31f * num, 0.0f), new Vector3((float) (0.189999997615814 * (double) num + 0.200000002980232 * (double) num * (double) index1), 0.49f * num, 0.0f));
      drawArea.canvasMin = new Vector3(-1.1f, -1.1f, 0.0f);
      drawArea.canvasMax = new Vector3(1.1f, 1.1f, 0.0f);
      GL.Begin(1);
      drawArea.DrawLine(-0.9f * Vector3.up, -1.1f * Vector3.up, c1);
      for (int index2 = 0; index2 < 90; ++index2)
        drawArea.DrawLine(Quaternion.AngleAxis((float) ((double) index2 / 90.0 * 360.0), Vector3.forward) * Vector3.up, Quaternion.AngleAxis((float) ((double) (index2 + 1) / 90.0 * 360.0), Vector3.forward) * Vector3.up, c1);
      GL.End();
      GL.Begin(7);
      for (int index2 = 0; index2 < this.legs.Length; ++index2)
      {
        Color c2 = this.legs[index2].debugColor;
        Vector3 vector3 = Quaternion.AngleAxis(360f * (this.legC.cycleMotions[index1].cycles[index2].stanceTime - this.legC.cycleMotions[index1].cycleOffset * (float) (0.5 + 0.5 * (double) Mathf.Sin(Time.time * 2f))), -Vector3.forward) * -Vector3.up;
        drawArea.DrawRect(vector3 - Vector3.one * 0.1f, vector3 + Vector3.one * 0.1f, c2);
      }
      GL.End();
    }
  }

  public void RenderFootMarkers()
  {
    GL.Begin(1);
    GL.End();
    GL.Begin(7);
    for (int index1 = 0; index1 < this.legs.Length; ++index1)
    {
      for (int index2 = 0; index2 < 3; ++index2)
      {
        if (this.legStates[index1] != null)
        {
          Matrix4x4 matrix4x4;
          if (index2 == 0)
          {
            matrix4x4 = this.legStates[index1].stepFromMatrix;
            GL.Color(this.legs[index1].debugColor * 0.8f);
          }
          else if (index2 == 1)
          {
            matrix4x4 = this.legStates[index1].stepToMatrix;
            GL.Color(this.legs[index1].debugColor);
          }
          else
          {
            matrix4x4 = this.legStates[index1].stepToMatrix;
            GL.Color(this.legs[index1].debugColor * 0.4f);
          }
          Vector3 vector3_1 = matrix4x4.MultiplyPoint3x4(Vector3.zero);
          Vector3 vector3_2 = matrix4x4.MultiplyVector(this.legStates[index1].heelToetipVector);
          Vector3 axis = matrix4x4.MultiplyVector(Vector3.up);
          Vector3 vector3_3 = (Quaternion.AngleAxis(90f, axis) * vector3_2).normalized * this.legs[index1].footWidth * this.scale;
          Vector3 vector3_4 = vector3_1 + axis.normalized * vector3_3.magnitude / 20f;
          if (index2 == 2)
            vector3_4 += this.legStates[index1].stepToPositionGoal - this.legStates[index1].stepToPosition;
          GL.Vertex(vector3_4 + vector3_3 / 2f);
          GL.Vertex(vector3_4 - vector3_3 / 2f);
          GL.Vertex(vector3_4 - vector3_3 / 4f + vector3_2);
          GL.Vertex(vector3_4 + vector3_3 / 4f + vector3_2);
        }
      }
    }
    GL.End();
  }

  private void OnGUI()
  {
    if (!this.renderAnimationStates)
      return;
    this.RenderAnimationStates();
  }

  public void RenderAnimationStates()
  {
    int num1 = 0;
    foreach (AnimationState animationState in this.GetComponent<Animation>())
    {
      string name = animationState.name;
      float num2 = (float) (0.5 + 0.5 * (double) animationState.weight);
      GUI.color = new Color(0.0f, 0.0f, num2, 1f);
      if (animationState.enabled)
        GUI.color = new Color(num2, num2, num2, 1f);
      string text = name + " " + animationState.weight.ToString("0.000");
      GUI.Label(new Rect((float) (Screen.width - 200), (float) (10 + 20 * num1), 200f, 20f), text);
      ++num1;
    }
  }
}
