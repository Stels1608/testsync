﻿// Decompiled with JetBrains decompiler
// Type: BannerWindowNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BannerWindowNgui : BannerWindowUi
{
  [SerializeField]
  private UILabel titleLabel;
  [SerializeField]
  private UI2DSprite artworkImage;
  [SerializeField]
  private UILabel descriptionLabel;
  [SerializeField]
  private UILabel footerLabel;
  [SerializeField]
  private UILabel okBtnLabel;

  protected override void SetTitle(string title)
  {
    this.titleLabel.text = title;
  }

  protected override void SetArtwork(Texture2D artwork)
  {
    this.artworkImage.sprite2D = UnityEngine.Sprite.Create(artwork, new Rect(0.0f, 0.0f, (float) artwork.width, (float) artwork.height), new Vector2(0.5f, 0.5f));
  }

  protected override void SetMainText(string description)
  {
    this.descriptionLabel.text = description;
  }

  protected override void SetFooterText(string footerText)
  {
    this.footerLabel.text = footerText;
  }

  protected override void SetOkButtonText(string okButtonText)
  {
    this.okBtnLabel.text = okButtonText;
  }
}
