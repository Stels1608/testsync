﻿// Decompiled with JetBrains decompiler
// Type: UndockButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class UndockButton : MonoBehaviour
{
  private bool oldhaveTimer = true;
  public UnityEngine.Sprite colonialIcon;
  public UnityEngine.Sprite cylonIcon;
  public UnityEngine.Sprite backgroundNormal;
  public UnityEngine.Sprite backgroundOver;
  public UI2DSprite buttonBackground;
  public UI2DSprite icon;
  public UILabel label;
  private bool disabled;

  public void Start()
  {
    this.icon.sprite2D = Game.Me.Faction != Faction.Cylon ? this.colonialIcon : this.cylonIcon;
  }

  private void OnEnable()
  {
    this.disabled = false;
    this.OnHover(false);
  }

  public void OnHover(bool isOver)
  {
    if (this.disabled)
      return;
    this.label.color = !isOver ? Gui.Options.MouseOverColor : Gui.Options.MouseOverInvertColor;
    this.icon.color = !isOver ? Gui.Options.MouseOverColor : Gui.Options.MouseOverInvertColor;
    this.buttonBackground.sprite2D = !isOver ? this.backgroundNormal : this.backgroundOver;
    this.buttonBackground.color = !isOver ? ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR) : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_CLICK_COLOR);
  }

  public void Update()
  {
    this.disabled = Game.Me.Caps.IsRestricted(Capability.Undock);
    if (this.disabled)
      this.SetTimerText(Game.Me.Caps.Timeout(Capability.Undock));
    else if (this.oldhaveTimer != this.disabled)
      this.SetUndockText(BsgoLocalization.Get("bgo.etc.undock"));
    if (this.oldhaveTimer != this.disabled)
      this.icon.color = !this.disabled ? Color.white : Color.gray;
    this.oldhaveTimer = this.disabled;
  }

  private void SetTimerText(TimeSpan timeToUndock)
  {
    this.label.text = !(timeToUndock > TimeSpan.Zero) ? Tools.FormatTime(TimeSpan.Zero) : Tools.FormatTime(timeToUndock);
    this.label.trueTypeFont = Gui.Options.fontEurostileTBlaExt;
    this.label.fontSize = 16;
  }

  private void SetUndockText(string text)
  {
    this.label.text = text.ToUpper();
    this.label.trueTypeFont = Gui.Options.fontEurostileTRegCon;
    this.label.fontSize = 12;
  }

  public void OnClick()
  {
    if (this.disabled)
      return;
    this.Undock();
  }

  private void Undock()
  {
    if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null && Game.Me.Anchored)
      GameProtocol.GetProtocol().RequestUnanchor();
    else if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null && SpaceLevel.GetLevel().GetActualPlayerShip() != null && SpaceLevel.GetLevel().GetActualPlayerShip().ShipCardLight.ShipRoleDeprecated == ShipRoleDeprecated.Carrier)
    {
      GameProtocol.GetProtocol().RequestLaunchStrikes();
    }
    else
    {
      RoomProtocol.GetProtocol().Quit();
      this.disabled = true;
    }
  }
}
