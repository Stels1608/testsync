﻿// Decompiled with JetBrains decompiler
// Type: DebugPositions
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugPositions : DebugBehaviour<DebugPositions>
{
  private Vector2 scroll = new Vector2();
  private string playerInfo = string.Empty;
  private string npcInfo = string.Empty;

  private void Start()
  {
    this.windowID = 21;
    this.SetSize(300f, 400f);
    Timer.CreateTimer("PositionUpdateTimer", 1f, 1f, new Timer.TickHandler(this.TimerUpdate));
  }

  private void TimerUpdate()
  {
    List<PlayerShip> playerShipList = new List<PlayerShip>();
    List<SpaceObject> spaceObjectList = new List<SpaceObject>();
    this.playerInfo = string.Empty;
    this.npcInfo = string.Empty;
    if (Game.Me.ActiveShip != null && GameLevel.Instance is SpaceLevel)
    {
      foreach (SpaceObject spaceObject in SpaceLevel.GetLevel().GetObjectRegistry().GetAll())
      {
        Ship ship = spaceObject as Ship;
        if (ship != null && (bool) ship.IsLoaded)
        {
          if (ship.IsPlayer)
            playerShipList.Add(ship as PlayerShip);
          else
            spaceObjectList.Add((SpaceObject) ship);
        }
      }
    }
    using (List<PlayerShip>.Enumerator enumerator = playerShipList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerShip current = enumerator.Current;
        this.playerInfo += string.Format("{0}: \n\t[ x={1}, y={2}, z={3} ]\n", (object) current.Player.Name, (object) current.Position.x, (object) current.Position.y, (object) current.Position.z);
      }
    }
    using (List<SpaceObject>.Enumerator enumerator = spaceObjectList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SpaceObject current = enumerator.Current;
        this.npcInfo += string.Format("{0}: \n\t[ x={1}, y={2}, z={3} ]\n", (object) current.PrefabName, (object) current.Position.x, (object) current.Position.y, (object) current.Position.z);
      }
    }
  }

  protected override void WindowFunc()
  {
    TextAnchor alignment = GUI.skin.box.alignment;
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.Label("Player positions:\n");
    GUILayout.Box(this.playerInfo);
    GUILayout.Label("NPC positions:\n");
    GUILayout.Box(this.npcInfo);
    GUILayout.EndScrollView();
    GUI.skin.box.alignment = alignment;
  }
}
