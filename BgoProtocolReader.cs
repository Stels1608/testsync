﻿// Decompiled with JetBrains decompiler
// Type: BgoProtocolReader
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using ComponentAce.Compression.Libs.zlib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class BgoProtocolReader : BinaryReader
{
  public BgoProtocolReader(byte[] buffer)
    : this(new MemoryStream(buffer))
  {
  }

  public BgoProtocolReader(MemoryStream stream)
    : base((Stream) stream)
  {
  }

  public BgoProtocolReader UnZip()
  {
    int length = this.ReadLength();
    byte[] numArray = new byte[length];
    this.Read(numArray, 0, length);
    MemoryStream stream = new MemoryStream();
    ZOutputStream zoutputStream = new ZOutputStream((Stream) stream);
    zoutputStream.Write(numArray, 0, length);
    zoutputStream.Flush();
    stream.Seek(0L, SeekOrigin.Begin);
    return new BgoProtocolReader(stream);
  }

  public override string ReadString()
  {
    int length = this.ReadLength();
    if (length <= 0)
      return string.Empty;
    byte[] numArray = new byte[length];
    this.Read(numArray, 0, numArray.Length);
    return Encoding.UTF8.GetString(numArray);
  }

  public string[] ReadStringArray()
  {
    int length = this.ReadLength();
    string[] strArray = new string[length];
    for (int index = 0; index < length; ++index)
      strArray[index] = this.ReadString();
    return strArray;
  }

  public byte[] ReadByteArray()
  {
    int length = this.ReadLength();
    byte[] numArray = new byte[length];
    for (int index = 0; index < length; ++index)
      numArray[index] = this.ReadByte();
    return numArray;
  }

  public T ReadDesc<T>() where T : IProtocolRead, new()
  {
    T obj = new T();
    obj.Read(this);
    return obj;
  }

  public List<T> ReadDescList<T>() where T : IProtocolRead, new()
  {
    int num = this.ReadLength();
    List<T> objList = new List<T>();
    for (int index = 0; index < num; ++index)
      objList.Add(this.ReadDesc<T>());
    return objList;
  }

  public List<ushort> ReadUInt16List()
  {
    int num = this.ReadLength();
    List<ushort> ushortList = new List<ushort>();
    for (int index = 0; index < num; ++index)
      ushortList.Add(this.ReadUInt16());
    return ushortList;
  }

  public List<uint> ReadUInt32List()
  {
    int num = this.ReadLength();
    List<uint> uintList = new List<uint>();
    for (int index = 0; index < num; ++index)
      uintList.Add(this.ReadUInt32());
    return uintList;
  }

  public T[] ReadDescArray<T>() where T : IProtocolRead, new()
  {
    int length = this.ReadLength();
    T[] objArray = new T[length];
    for (int index = 0; index < length; ++index)
      objArray[index] = this.ReadDesc<T>();
    return objArray;
  }

  public HashSet<T> ReadSet<T>()
  {
    ushort num1 = this.ReadUInt16();
    HashSet<T> objSet = new HashSet<T>();
    int num2 = 1;
    while (num2 < 65536)
    {
      if (((int) num1 & num2) != 0)
        objSet.Add((T) Enum.ToObject(typeof (T), num2));
      num2 <<= 1;
    }
    return objSet;
  }

  public int ReadLength()
  {
    return (int) this.ReadUInt16();
  }

  public Vector2 ReadVector2()
  {
    return new Vector2(this.ReadSingle(), this.ReadSingle());
  }

  public Vector3 ReadVector3()
  {
    return new Vector3(this.ReadSingle(), this.ReadSingle(), this.ReadSingle());
  }

  public Euler3 ReadEuler()
  {
    return new Euler3(this.ReadSingle(), this.ReadSingle(), this.ReadSingle());
  }

  public Quaternion ReadQuaternion()
  {
    return new Quaternion(this.ReadSingle(), this.ReadSingle(), this.ReadSingle(), this.ReadSingle());
  }

  public Color ReadColor()
  {
    return new Color((float) this.ReadByte() / (float) byte.MaxValue, (float) this.ReadByte() / (float) byte.MaxValue, (float) this.ReadByte() / (float) byte.MaxValue, (float) this.ReadByte() / (float) byte.MaxValue);
  }

  public Tick ReadTick()
  {
    return this.ReadDesc<Tick>();
  }

  public DateTime ReadDateTime()
  {
    return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds((double) this.ReadUInt32());
  }

  public DateTime ReadLongDateTime()
  {
    ulong num = this.ReadUInt64();
    if ((long) num != 0L)
      return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds((double) num);
    return new DateTime();
  }

  public List<SectorEventTask> ReadEventTaskList()
  {
    int capacity = this.ReadLength();
    List<SectorEventTask> sectorEventTaskList = new List<SectorEventTask>(capacity);
    for (int index1 = 0; index1 < capacity; ++index1)
    {
      byte index2 = this.ReadByte();
      SectorEventState taskState = (SectorEventState) this.ReadByte();
      SectorEventTaskType sectorEventTaskType = (SectorEventTaskType) this.ReadByte();
      SectorEventTaskSubType subType = (SectorEventTaskSubType) this.ReadByte();
      if (sectorEventTaskType == SectorEventTaskType.Protect)
      {
        uint vipObjectId = this.ReadUInt32();
        DateTime endTime = this.ReadDateTime();
        sectorEventTaskList.Add((SectorEventTask) new SectorEventProtectTask(index2, subType, taskState, vipObjectId, endTime));
      }
    }
    return sectorEventTaskList;
  }

  public uint ReadGUID()
  {
    return this.ReadUInt32();
  }

  public Maneuver ReadManeuver()
  {
    long position = this.BaseStream.Position;
    ManeuverType maneuverType = (ManeuverType) this.ReadByte();
    this.BaseStream.Position = position;
    switch (maneuverType)
    {
      case ManeuverType.Pulse:
        return (Maneuver) this.ReadDesc<PulseManeuver>();
      case ManeuverType.Teleport:
        return (Maneuver) this.ReadDesc<TeleportManeuver>();
      case ManeuverType.Rest:
        return (Maneuver) this.ReadDesc<RestManeuver>();
      case ManeuverType.Warp:
        return (Maneuver) this.ReadDesc<WarpManeuver>();
      case ManeuverType.Directional:
        return (Maneuver) this.ReadDesc<DirectionalManeuver>();
      case ManeuverType.Launch:
        return (Maneuver) this.ReadDesc<LaunchManeuver>();
      case ManeuverType.Flip:
        return (Maneuver) this.ReadDesc<FlipManeuver>();
      case ManeuverType.Turn:
        return (Maneuver) this.ReadDesc<TurnManeuver>();
      case ManeuverType.Follow:
        return (Maneuver) this.ReadDesc<FollowManeuver>();
      case ManeuverType.DirectionalWithoutRoll:
        return (Maneuver) this.ReadDesc<DirectionalWithoutRollManeuver>();
      case ManeuverType.TurnQweasd:
        return (Maneuver) this.ReadDesc<TurnQweasdManeuver>();
      case ManeuverType.TurnToDirectionStrikes:
        return (Maneuver) this.ReadDesc<TurnToDirectionStrikes>();
      case ManeuverType.TurnByPitchYawStrikes:
        return (Maneuver) this.ReadDesc<TurnByPitchYawStrikes>();
      case ManeuverType.TargetLaunch:
        return (Maneuver) this.ReadDesc<TargetLaunchManeuver>();
      default:
        return (Maneuver) null;
    }
  }

  public static ushort ReadBufferSize(byte[] data)
  {
    return (ushort) ((uint) (ushort) data[0] << 8 | (uint) (ushort) data[1]);
  }

  public void ProcessLocation(uint playerID)
  {
    GameLocation location = (GameLocation) this.ReadByte();
    switch (location)
    {
      case GameLocation.Space:
      case GameLocation.Story:
      case GameLocation.Arena:
      case GameLocation.BattleSpace:
      case GameLocation.Tournament:
        uint sectorGUID1 = this.ReadGUID();
        Game.Players[playerID]._SetLocation(location, sectorGUID1, 0U);
        break;
      case GameLocation.Room:
        uint sectorGUID2 = this.ReadGUID();
        uint roomGUID = this.ReadGUID();
        Game.Players[playerID]._SetLocation(location, sectorGUID2, roomGUID);
        break;
      default:
        Game.Players[playerID]._SetLocation(location, 0U, 0U);
        break;
    }
  }
}
