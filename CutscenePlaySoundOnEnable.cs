﻿// Decompiled with JetBrains decompiler
// Type: CutscenePlaySoundOnEnable
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[RequireComponent(typeof (AudioSource))]
public class CutscenePlaySoundOnEnable : MonoBehaviour
{
  private AudioSource audioSource;
  public float Delay;

  private void Awake()
  {
    this.audioSource = this.GetComponent<AudioSource>();
  }

  private void OnEnable()
  {
    this.StartCoroutine(this.Play());
  }

  [DebuggerHidden]
  private IEnumerator Play()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CutscenePlaySoundOnEnable.\u003CPlay\u003Ec__Iterator12() { \u003C\u003Ef__this = this };
  }
}
