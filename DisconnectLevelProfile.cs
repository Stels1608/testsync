﻿// Decompiled with JetBrains decompiler
// Type: DisconnectLevelProfile
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;

public class DisconnectLevelProfile : LevelProfile
{
  private readonly string message;

  public string SceneName
  {
    get
    {
      return "DebugLevel";
    }
  }

  public string Message
  {
    get
    {
      return this.message;
    }
  }

  public bool Ready
  {
    get
    {
      return true;
    }
  }

  public IEnumerable<string> RequiredAssets
  {
    get
    {
      return Enumerable.Empty<string>();
    }
  }

  public DisconnectLevelProfile(string message)
  {
    this.message = message;
  }
}
