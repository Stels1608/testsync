﻿// Decompiled with JetBrains decompiler
// Type: MouseSetup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MouseSetup
{
  public static float2 MousePositionGui
  {
    get
    {
      float2 float2 = float2.FromV2((Vector2) Input.mousePosition);
      float2.y = (float) Screen.height - float2.y;
      return float2;
    }
  }
}
