﻿// Decompiled with JetBrains decompiler
// Type: GUISystemMap
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using Gui.Tooltips;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GUISystemMap : GUIPanel
{
  private Color colonialColor = TargetColorsLegacyBrackets.colonialColor;
  private Color cylonColor = TargetColorsLegacyBrackets.cylonColor;
  private Color ancientColor = TargetColorsLegacyBrackets.ancientColor;
  private Rect oneRect = new Rect(0.0f, 0.0f, 1f, 1f);
  private const int ATLAS_WAYPOINT_MARKER_ID = 11;
  private const float UPDATE_INTERVAL = 1.5f;
  private GUIImageNew background;
  private GUILabelNew caption;
  private GUILabelNew systemName;
  private GUILabelNew location;
  private GUICheckBox friendly;
  private GUICheckBox hostile;
  private GUICheckBox neutral;
  private GUIButtonNew exit;
  private GUISystemMap.PlayerMarker marker;
  private List<GUISystemMap.PlayerMarker> sharedDradisMarkers;
  private GUISystemMap.DirectionMarker arrow;
  private GUIImageNew area;
  private AtlasCache atlasCache;
  private List<GUISystemMap.MapIcon> ships;
  private List<GUISystemMap.MapIcon> asteroids;
  private List<GUISystemMap.MapIcon> debris;
  private List<GUISystemMap.MapIcon> jumpTargetTransponders;
  private List<GUISystemMap.MapIcon> waypoints;
  private List<GUISystemMap.MapIcon> planets;
  private List<GUISystemMap.MapIcon> comets;
  private List<GUISystemMap.MapIcon> hubs;
  private List<GUISystemMap.MapIcon> sectorEvents;
  private GUILabelNew[] gridMarkersX;
  private GUILabelNew[] gridMarkersY;
  private static Dictionary<GUISystemMap.MapIconType, string> tooltipsDictonary;
  private Color partyColor;
  private Texture2D targetSelection;
  private Texture2D partySelection;
  private Texture2D tPlanet;
  private Texture2D cylonBasePlanet;
  private Texture2D colonialBasePlanet;
  private Texture2D sectorEventRadius;
  private List<IGUIPanel> oldVisiblePanels;
  private float timeToUpdate;
  private int lastIslandID;
  private SpaceLevel spaceLevel;
  private Me player;

  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  public override bool IsRendered
  {
    set
    {
      base.IsRendered = value;
      if (this.IsRendered)
      {
        SpaceLevel level = SpaceLevel.GetLevel();
        Me me = Game.Me;
        if ((UnityEngine.Object) level != (UnityEngine.Object) null && me != null && me.Ship != null)
        {
          this.marker.MakeOrientationFrom(Vector3.forward, me.Ship.Forward);
          this.UpdateMarkerPosition();
        }
        this.oldVisiblePanels.Clear();
        using (List<IGUIPanel>.Enumerator enumerator = Game.GUIManager.panels.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            IGUIPanel current = enumerator.Current;
            if (current.IsRendered && current != this && (!(current is GalaxyMapMain) && !(current is MessageBoxManager)) && (!(current is GuiTicker) && !(current is NotificationManager)))
            {
              this.oldVisiblePanels.Add(current);
              current.IsRendered = false;
            }
          }
        }
        Game.GUIManager.Find<GUISlotManager>().IsRendered = true;
      }
      else
      {
        using (List<IGUIPanel>.Enumerator enumerator = this.oldVisiblePanels.GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.IsRendered = true;
        }
      }
    }
  }

  public override bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public GUISystemMap(AtlasCache atlasCache)
  {
    this.Name = "SystemMap";
    this.tPlanet = (Texture2D) ResourceLoader.Load("GUI/Map/SystemMap/planet");
    this.cylonBasePlanet = (Texture2D) ResourceLoader.Load("GUI/Map/SystemMap/planet");
    this.colonialBasePlanet = (Texture2D) ResourceLoader.Load("GUI/Map/SystemMap/planet");
    this.sectorEventRadius = (Texture2D) ResourceLoader.Load("GUI/Map/SystemMap/dynamic_event_radius");
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/Map/SystemMap/layout.txt");
    this.background = (jwindowDescription["background"] as JImage).CreateGUIImageNew();
    this.root.Width = this.background.Width;
    this.root.Height = this.background.Height;
    this.AddPanel((GUIPanel) this.background);
    this.FillGridMarkers();
    this.caption = (jwindowDescription["caption"] as JLabel).CreateGUILabelNew();
    this.AddPanel((GUIPanel) this.caption);
    this.area = (jwindowDescription["area"] as JImage).CreateGUIImageNew();
    this.area.IsRendered = false;
    this.AddPanel((GUIPanel) this.area);
    this.systemName = new GUILabelNew(jwindowDescription["systemName"] as JLabel);
    this.AddPanel((GUIPanel) this.systemName);
    this.location = new GUILabelNew(jwindowDescription["location"] as JLabel);
    this.AddPanel((GUIPanel) this.location);
    this.friendly = new GUICheckBox(jwindowDescription["friendly"] as JButton);
    this.friendly.Handler = (AnonymousDelegate) (() =>
    {
      DradisManager dradisManager = Game.GUIManager.Find<DradisManager>();
      if (dradisManager == null)
        return;
      dradisManager.SetObjectFilterValue(MapAlignmentFilter.Friendly, this.friendly.IsPressed);
    });
    this.AddPanel((GUIPanel) this.friendly);
    this.friendly.IsPressed = true;
    this.hostile = new GUICheckBox(jwindowDescription["hostile"] as JButton);
    this.hostile.Handler = (AnonymousDelegate) (() =>
    {
      DradisManager dradisManager = Game.GUIManager.Find<DradisManager>();
      if (dradisManager == null)
        return;
      dradisManager.SetObjectFilterValue(MapAlignmentFilter.Hostile, this.hostile.IsPressed);
    });
    this.AddPanel((GUIPanel) this.hostile);
    this.hostile.IsPressed = true;
    this.neutral = new GUICheckBox(jwindowDescription["neutral"] as JButton);
    this.neutral.Handler = (AnonymousDelegate) (() =>
    {
      DradisManager dradisManager = Game.GUIManager.Find<DradisManager>();
      if (dradisManager == null)
        return;
      dradisManager.SetObjectFilterValue(MapAlignmentFilter.Neutral, this.neutral.IsPressed);
    });
    this.AddPanel((GUIPanel) this.neutral);
    this.neutral.IsPressed = true;
    this.exit = (jwindowDescription["exit"] as JButton).CreateGUIButtonNew();
    this.exit.TextLabel.MakeCenteredRect();
    this.exit.Width = Mathf.Max(this.exit.Width, this.exit.TextLabel.Width + 8f);
    this.exit.PositionX = (float) ((double) this.background.Width / 2.0 - (20.0 + (double) this.exit.Width / 2.0));
    this.exit.TextLabel.PositionY = -1f;
    this.exit.Handler = (AnonymousDelegate) (() => this.IsRendered = false);
    this.AddPanel((GUIPanel) this.exit);
    this.marker = new GUISystemMap.PlayerMarker(Game.Me.Ship);
    this.arrow = new GUISystemMap.DirectionMarker();
    this.AddPanel((GUIPanel) this.marker);
    this.sharedDradisMarkers = new List<GUISystemMap.PlayerMarker>();
    this.marker.Parent = this.area.SmartRect;
    this.AddPanel((GUIPanel) this.arrow);
    this.arrow.Parent = this.area.SmartRect;
    this.atlasCache = atlasCache;
    this.ships = new List<GUISystemMap.MapIcon>();
    this.asteroids = new List<GUISystemMap.MapIcon>();
    this.debris = new List<GUISystemMap.MapIcon>();
    this.jumpTargetTransponders = new List<GUISystemMap.MapIcon>();
    this.waypoints = new List<GUISystemMap.MapIcon>();
    this.planets = new List<GUISystemMap.MapIcon>();
    this.comets = new List<GUISystemMap.MapIcon>();
    this.hubs = new List<GUISystemMap.MapIcon>();
    this.sectorEvents = new List<GUISystemMap.MapIcon>();
    this.targetSelection = ResourceLoader.Load<Texture2D>("GUI/Map/selection_target");
    this.partySelection = ResourceLoader.Load<Texture2D>("GUI/Map/selection_party");
    this.colonialColor.a = 1f;
    this.cylonColor.a = 1f;
    this.partyColor = Game.Me.Faction != Faction.Colonial ? TargetColorsLegacyBrackets.cylonPartyColor : TargetColorsLegacyBrackets.colonialPartyColor;
    this.oldVisiblePanels = new List<IGUIPanel>();
    this.SetTooltipsDictionary();
    this.marker.SetTooltip(GUISystemMap.tooltipsDictonary[GUISystemMap.MapIconType.YourShip]);
  }

  private void SetTooltipsDictionary()
  {
    if (GUISystemMap.tooltipsDictonary != null)
      return;
    GUISystemMap.tooltipsDictonary = new Dictionary<GUISystemMap.MapIconType, string>();
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.YourShip, "%$bgo.etc.system_map_yourship_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialOutpost, "%$bgo.etc.system_map_colonial_outpost_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialMiningShip, "%$bgo.etc.system_map_colonial_miningship_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonOutpost, "%$bgo.etc.system_map_cylon_outpost_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonMiningShip, "%$bgo.etc.system_map_cylon_miningship_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.Basestar, "%$bgo.etc.system_map_basestar_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.Galactica, "%$bgo.etc.system_map_galactica_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.Pegasus, "%$bgo.etc.system_map_pegasus_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialStrikeShip, "%$bgo.etc.system_map_colonial_strike_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialEscortShip, "%$bgo.etc.system_map_colonial_escort_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialLineShip, "%$bgo.etc.system_map_colonial_line_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialFreighterShip, "%$bgo.etc.system_map_colonial_freighter_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialCarrierShip, "%$bgo.etc.system_map_colonial_carrier_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonStrikeShip, "%$bgo.etc.system_map_cylon_strike_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonEscortShip, "%$bgo.etc.system_map_cylon_escort_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonLineShip, "%$bgo.etc.system_map_cylon_line_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonFreighterShip, "%$bgo.etc.system_map_cylon_freighter_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonCarrierShip, "%$bgo.etc.system_map_cylon_carrier_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.Drone, "%$bgo.etc.system_map_drone_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.DebrisPile, "%$bgo.etc.system_map_derbispile_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.Planetoid, "%$bgo.etc.system_map_planetoid_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.Comet, "%$bgo.etc.system_map_comet_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialJumpBeacon, "%$bgo.etc.system_map_colonial_jump_beacon_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonJumpBeacon, "%$bgo.etc.system_map_cylon_jump_beacon_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.NeutralJumpBeacon, "%$bgo.etc.system_map_neutral_jump_beacon_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialLightPlatform, "%$bgo.etc.system_map_colonial_weapon_platform_light_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialMediumPlatform, "%$bgo.etc.system_map_colonial_weapon_platform_medium_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialHeavyPlatform, "%$bgo.etc.system_map_colonial_weapon_platform_heavy_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonLightPlatform, "%$bgo.etc.system_map_cylon_weapon_platform_light_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonMediumPlatform, "%$bgo.etc.system_map_cylon_weapon_platform_medium_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonHeavyPlatform, "%$bgo.etc.system_map_cylon_weapon_platform_heavy_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.ColonialJumpTargetTransponder, "%$bgo.etc.system_map_colonial_jump_target_transponder_tooltip%");
    GUISystemMap.tooltipsDictonary.Add(GUISystemMap.MapIconType.CylonJumpTargetTransponder, "%$bgo.etc.system_map_cylon_jump_target_transponder_tooltip%");
  }

  public override bool OnShowTooltip(float2 mousePosition)
  {
    if (!this.IsRendered)
      return false;
    Vector2 v2 = mousePosition.ToV2();
    if (this.marker.OnShowTooltip(mousePosition))
      return true;
    this.advancedTooltip = (GuiAdvancedTooltipBase) null;
    int iconIndex;
    if (this.MouseOverIcon(this.ships, v2, out iconIndex))
    {
      Ship ship = this.ships[iconIndex].spaceObject as Ship;
      if (!(ship is AsteroidShip))
        this.SetTooltip(GUISystemMap.tooltipsDictonary[this.GetShipType(ref ship)]);
    }
    else if (this.MouseOverIcon(this.planets, v2, out iconIndex))
      this.SetTooltip(GUISystemMap.tooltipsDictonary[GUISystemMap.MapIconType.Planetoid]);
    else if (this.MouseOverIcon(this.comets, v2, out iconIndex))
      this.SetTooltip(GUISystemMap.tooltipsDictonary[GUISystemMap.MapIconType.Comet]);
    else if (this.MouseOverIcon(this.debris, v2, out iconIndex))
    {
      if (this.debris[iconIndex].spaceObject.WorldCard.Targetable)
        this.SetTooltip(GUISystemMap.tooltipsDictonary[GUISystemMap.MapIconType.DebrisPile]);
    }
    else if (this.MouseOverIcon(this.jumpTargetTransponders, v2, out iconIndex))
    {
      switch (this.ships[iconIndex].spaceObject.Faction)
      {
        case Faction.Colonial:
          this.SetTooltip(GUISystemMap.tooltipsDictonary[GUISystemMap.MapIconType.ColonialJumpTargetTransponder]);
          break;
        case Faction.Cylon:
          this.SetTooltip(GUISystemMap.tooltipsDictonary[GUISystemMap.MapIconType.CylonJumpTargetTransponder]);
          break;
      }
    }
    if (this.advancedTooltip == null)
      return base.OnShowTooltip(mousePosition);
    Game.TooltipManager.ShowTooltip(this.advancedTooltip);
    return true;
  }

  public void FillGridMarkers()
  {
    if (this.gridMarkersX == null)
    {
      this.gridMarkersX = new GUILabelNew[8];
      this.gridMarkersY = new GUILabelNew[8];
      for (int index = 0; index < 8; ++index)
      {
        this.gridMarkersX[index] = new GUILabelNew(((KeyCode) (97 + index)).ToString(), (SmartRect) null);
        this.AddPanel((GUIPanel) this.gridMarkersX[index]);
        this.gridMarkersY[index] = new GUILabelNew((index + 1).ToString(), (SmartRect) null);
        this.AddPanel((GUIPanel) this.gridMarkersY[index]);
      }
    }
    for (int index = 0; index < 8; ++index)
    {
      this.gridMarkersX[index].Position = new float2((float) (index * 70 - 240), -255f);
      this.gridMarkersY[index].Position = new float2(-265f, (float) (index * 70 - 230));
    }
  }

  public override void Update()
  {
    base.Update();
    this.spaceLevel = SpaceLevel.GetLevel();
    this.player = Game.Me;
    if ((UnityEngine.Object) this.spaceLevel == (UnityEngine.Object) null)
      return;
    if (this.player != null && this.player.Ship != null)
      this.marker.MakeOrientationFrom(Vector3.forward, this.player.Ship.Forward);
    this.timeToUpdate -= Time.deltaTime;
    if ((double) this.timeToUpdate > 0.0)
      return;
    if (this.player != null && this.player.Ship != null)
    {
      this.Find<GUILabelNew>("systemName").Text = this.spaceLevel.Card.GUICard.Name;
      this.UpdateMarkerPosition();
      float2 float2 = new float2();
      float2.x = this.player.Ship.Position.x / (this.spaceLevel.GetSectorSize().x / 2f);
      float2.y = this.player.Ship.Position.z / (this.spaceLevel.GetSectorSize().y / 2f);
      float2.x *= 100f;
      float2.y *= 100f;
      this.location.Text = string.Format("%$bgo.etc.system_map_location% ({0:##0.},{1:##0.})", (object) float2.x, (object) float2.y);
    }
    this.UpdateSharedDradisMarkers();
    this.PollObjects();
    this.timeToUpdate = 1.5f;
  }

  public void PollObjects()
  {
    Rect initialRect = new Rect(0.0f, 0.0f, this.atlasCache.ElementSize.x, this.atlasCache.ElementSize.y);
    this.ships.Clear();
    this.asteroids.Clear();
    this.debris.Clear();
    this.jumpTargetTransponders.Clear();
    this.waypoints.Clear();
    this.planets.Clear();
    this.comets.Clear();
    this.hubs.Clear();
    this.sectorEvents.Clear();
    Dictionary<int, GUISystemMap.MapIcon> dictionary = new Dictionary<int, GUISystemMap.MapIcon>();
    List<GUISystemMap.MapIcon> mapIconList = new List<GUISystemMap.MapIcon>();
    HashSet<Relation> relationSet = new HashSet<Relation>();
    if (this.friendly.IsPressed)
      relationSet.Add(Relation.Friend);
    if (this.hostile.IsPressed)
      relationSet.Add(Relation.Enemy);
    if (this.neutral.IsPressed)
      relationSet.Add(Relation.Neutral);
    SpaceObject playerTarget = this.spaceLevel.GetPlayerTarget();
    foreach (SpaceObject spaceObject in this.spaceLevel.GetObjectRegistry().GetAllLoaded())
    {
      if (spaceObject.MarkerType != SpaceObject.MarkObjectType.None)
        this.waypoints.Add(new GUISystemMap.MapIcon()
        {
          atlasEntry = this.atlasCache.GetCachedEntryBy(spaceObject.WorldCard.SystemMapTexture, 11),
          smartRect = new SmartRect(initialRect, this.SpaceToMap(spaceObject.Position), this.area.SmartRect),
          spaceObject = spaceObject
        });
      if (spaceObject is FighterShip || spaceObject is PlayerShip || spaceObject is AsteroidShip)
      {
        Ship ship = spaceObject as Ship;
        if (!ship.IsMe)
        {
          bool flag = ship.IsPlayer && ship.PlayerRelation == Relation.Friend;
          if ((flag || ship.IsInMapRange) && (relationSet.Contains(ship.PlayerRelation) && (int) ship.WorldCard.FrameIndex >= 0) && ((!(ship is PlayerShip) || !((PlayerShip) ship).Player.IsAnchored) && (!ship.IsCloaked || ship.IsInMapRange)))
          {
            GUISystemMap.MapIcon mapIcon = new GUISystemMap.MapIcon();
            int frameIndex = (int) ship.WorldCard.FrameIndex;
            if (ship.IsHostileCongener)
              frameIndex += ship.Faction != Faction.Colonial ? -1 : 1;
            mapIcon.atlasEntry = this.atlasCache.GetCachedEntryBy(ship.WorldCard.SystemMapTexture, frameIndex);
            mapIcon.smartRect = new SmartRect(initialRect, this.SpaceToMap(ship.Position), this.area.SmartRect);
            if (this.area.Contains(mapIcon.smartRect.AbsPosition))
            {
              mapIcon.spaceObject = (SpaceObject) ship;
              if (flag)
                mapIcon.isPartyMember = Game.Me.Party.IsMember((ship as PlayerShip).Player);
              mapIcon.isTarget = ship == playerTarget;
              this.ships.Add(mapIcon);
            }
          }
        }
      }
      else if (spaceObject is CruiserShip || spaceObject is WeaponPlatform)
      {
        CruiserShip cruiserShip = spaceObject as CruiserShip;
        if ((int) cruiserShip.WorldCard.FrameIndex >= 0)
        {
          GUISystemMap.MapIcon mapIcon = new GUISystemMap.MapIcon();
          mapIcon.atlasEntry = this.atlasCache.GetCachedEntryBy(cruiserShip.WorldCard.SystemMapTexture, (int) cruiserShip.WorldCard.FrameIndex);
          mapIcon.smartRect = new SmartRect(initialRect, this.SpaceToMap(cruiserShip.Position), this.area.SmartRect);
          if (this.area.Contains(mapIcon.smartRect.AbsPosition))
          {
            mapIcon.spaceObject = (SpaceObject) cruiserShip;
            mapIcon.isTarget = cruiserShip == playerTarget;
            this.ships.Add(mapIcon);
          }
        }
      }
      else if (spaceObject is JumpBeacon)
      {
        JumpBeacon jumpBeacon = spaceObject as JumpBeacon;
        if ((int) jumpBeacon.WorldCard.FrameIndex >= 0 && (jumpBeacon.IsInMapRange || jumpBeacon.WorldCard.ForceShowOnMap))
        {
          GUISystemMap.MapIcon mapIcon = new GUISystemMap.MapIcon();
          mapIcon.atlasEntry = this.atlasCache.GetCachedEntryBy(jumpBeacon.WorldCard.SystemMapTexture, (int) jumpBeacon.WorldCard.FrameIndex);
          mapIcon.smartRect = new SmartRect(initialRect, this.SpaceToMap(jumpBeacon.Position), this.area.SmartRect);
          if (this.area.Contains(mapIcon.smartRect.AbsPosition))
          {
            mapIcon.spaceObject = (SpaceObject) jumpBeacon;
            mapIcon.isTarget = jumpBeacon == playerTarget;
            this.ships.Add(mapIcon);
          }
        }
      }
      else if (spaceObject is MiningShip)
      {
        MiningShip miningShip = spaceObject as MiningShip;
        if (miningShip.IsInMapRange && relationSet.Contains(miningShip.PlayerRelation) && (int) miningShip.WorldCard.FrameIndex >= 0)
          this.ships.Add(new GUISystemMap.MapIcon()
          {
            atlasEntry = this.atlasCache.GetCachedEntryBy(miningShip.WorldCard.SystemMapTexture, (int) miningShip.WorldCard.FrameIndex),
            smartRect = new SmartRect(initialRect, this.SpaceToMap(miningShip.Position), this.area.SmartRect),
            spaceObject = (SpaceObject) miningShip,
            isTarget = miningShip == playerTarget
          });
      }
      else if (this.neutral.IsPressed)
      {
        if (spaceObject is Asteroid)
        {
          Asteroid asteroid = spaceObject as Asteroid;
          bool flag = asteroid == playerTarget || asteroid is Planetoid;
          if (flag || asteroid.island == -1 || !dictionary.ContainsKey(asteroid.island))
          {
            GUISystemMap.MapIcon mapIcon = new GUISystemMap.MapIcon();
            mapIcon.atlasEntry = this.atlasCache.GetCachedEntryBy(asteroid.WorldCard.SystemMapTexture, (int) asteroid.WorldCard.FrameIndex);
            mapIcon.smartRect = new SmartRect(initialRect, this.SpaceToMap(asteroid.Position), this.area.SmartRect);
            mapIcon.spaceObject = (SpaceObject) asteroid;
            mapIcon.isTarget = asteroid == playerTarget;
            if (flag)
              this.asteroids.Add(mapIcon);
            else if (asteroid.island == -1)
            {
              mapIconList.Add(mapIcon);
            }
            else
            {
              dictionary.Add(asteroid.island, mapIcon);
              this.asteroids.Add(mapIcon);
            }
          }
        }
        else if (spaceObject is DebrisPile)
        {
          DebrisPile debrisPile = spaceObject as DebrisPile;
          if ((debrisPile.IsInMapRange || debrisPile.WorldCard.ForceShowOnMap) && (int) debrisPile.WorldCard.FrameIndex >= 0)
          {
            GUISystemMap.MapIcon mapIcon = new GUISystemMap.MapIcon();
            mapIcon.atlasEntry = this.atlasCache.GetCachedEntryBy(debrisPile.WorldCard.SystemMapTexture, (int) debrisPile.WorldCard.FrameIndex);
            mapIcon.smartRect = new SmartRect(initialRect, this.SpaceToMap(debrisPile.Position), this.area.SmartRect);
            if (this.area.Contains(mapIcon.smartRect.AbsPosition))
            {
              mapIcon.spaceObject = (SpaceObject) debrisPile;
              mapIcon.isTarget = debrisPile == playerTarget;
              this.debris.Add(mapIcon);
            }
          }
        }
        else if (spaceObject is Planet)
        {
          Planet planet = spaceObject as Planet;
          GUISystemMap.MapIcon mapIcon1 = new GUISystemMap.MapIcon();
          Texture2D texture = this.tPlanet;
          if (planet.CanDock(true))
            texture = planet.Faction != Faction.Colonial ? this.cylonBasePlanet : this.colonialBasePlanet;
          mapIcon1.atlasEntry = new AtlasEntry(texture, this.oneRect);
          float num = this.SpaceToMap(planet.Radius) * 2f;
          mapIcon1.smartRect = new SmartRect(new Rect(0.0f, 0.0f, num, num), this.SpaceToMap(planet.Position), this.area.SmartRect);
          if (this.area.Contains(mapIcon1.smartRect.AbsPosition))
          {
            if ((int) planet.WorldCard.FrameIndex == 19 || (int) planet.WorldCard.FrameIndex == 20)
            {
              GUISystemMap.MapIcon mapIcon2 = new GUISystemMap.MapIcon();
              mapIcon2.atlasEntry = this.atlasCache.GetCachedEntryBy(planet.WorldCard.SystemMapTexture, (int) planet.WorldCard.FrameIndex);
              float2 position = mapIcon1.smartRect.Position - (float) ((double) num / 2.0 - 7.0) * mapIcon1.smartRect.Position.normalized;
              mapIcon2.smartRect = new SmartRect(initialRect, position, this.area.SmartRect);
              mapIcon2.spaceObject = (SpaceObject) planet;
              this.hubs.Add(mapIcon2);
            }
            mapIcon1.spaceObject = (SpaceObject) null;
            this.planets.Add(mapIcon1);
          }
        }
        else if (spaceObject is Comet)
        {
          Comet comet = spaceObject as Comet;
          this.comets.Add(new GUISystemMap.MapIcon()
          {
            atlasEntry = this.atlasCache.GetCachedEntryBy(comet.WorldCard.SystemMapTexture, (int) comet.WorldCard.FrameIndex),
            smartRect = new SmartRect(initialRect, this.SpaceToMap(comet.Position), this.area.SmartRect),
            spaceObject = (SpaceObject) comet,
            isTarget = comet == playerTarget,
            spaceObject = (SpaceObject) null
          });
        }
        else if (spaceObject is SectorEvent)
        {
          SectorEvent sectorEvent = spaceObject as SectorEvent;
          GUISystemMap.MapIcon mapIcon1 = new GUISystemMap.MapIcon();
          mapIcon1.atlasEntry = new AtlasEntry(this.sectorEventRadius, this.oneRect);
          float num = this.SpaceToMap(sectorEvent.Radius) * 2f;
          mapIcon1.smartRect = new SmartRect(new Rect(0.0f, 0.0f, num, num), this.SpaceToMap(sectorEvent.Position), this.area.SmartRect);
          if (this.area.Contains(mapIcon1.smartRect.AbsPosition))
          {
            GUISystemMap.MapIcon mapIcon2 = new GUISystemMap.MapIcon();
            Texture2D icon = sectorEvent.SectorEventCard.Icon;
            mapIcon2.atlasEntry = new AtlasEntry(icon, this.oneRect);
            mapIcon2.smartRect = new SmartRect(new Rect(0.0f, 0.0f, (float) icon.width, (float) icon.height), this.SpaceToMap(sectorEvent.Position), this.area.SmartRect);
            mapIcon1.spaceObject = mapIcon2.spaceObject = (SpaceObject) sectorEvent;
            this.sectorEvents.Add(mapIcon1);
            this.sectorEvents.Add(mapIcon2);
          }
        }
        else if (spaceObject is JumpTargetTransponder)
        {
          JumpTargetTransponder targetTransponder = spaceObject as JumpTargetTransponder;
          if ((int) targetTransponder.WorldCard.FrameIndex >= 0)
          {
            int frameIndex = (int) targetTransponder.WorldCard.FrameIndex;
            if (targetTransponder.Faction == Faction.Colonial)
              ++frameIndex;
            GUISystemMap.MapIcon mapIcon = new GUISystemMap.MapIcon();
            mapIcon.atlasEntry = this.atlasCache.GetCachedEntryBy(targetTransponder.WorldCard.SystemMapTexture, frameIndex);
            mapIcon.smartRect = new SmartRect(initialRect, this.SpaceToMap(targetTransponder.Position), this.area.SmartRect);
            if (this.area.Contains(mapIcon.smartRect.AbsPosition))
            {
              mapIcon.spaceObject = (SpaceObject) targetTransponder;
              mapIcon.isTarget = targetTransponder == playerTarget;
              this.jumpTargetTransponders.Add(mapIcon);
            }
          }
        }
      }
    }
    using (List<GUISystemMap.MapIcon>.Enumerator enumerator1 = mapIconList.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        GUISystemMap.MapIcon current1 = enumerator1.Current;
        Asteroid asteroid = current1.spaceObject as Asteroid;
        using (Dictionary<int, GUISystemMap.MapIcon>.Enumerator enumerator2 = dictionary.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            KeyValuePair<int, GUISystemMap.MapIcon> current2 = enumerator2.Current;
            if ((double) (current2.Value.smartRect.AbsPosition - current1.smartRect.AbsPosition).magnitude < 6.0)
            {
              asteroid.island = current2.Key;
              break;
            }
          }
        }
        if (asteroid.island == -1)
        {
          asteroid.island = this.lastIslandID;
          dictionary.Add(this.lastIslandID, current1);
          ++this.lastIslandID;
          this.asteroids.Add(current1);
        }
      }
    }
  }

  public override void Draw()
  {
    this.background.Draw();
    this.caption.Draw();
    this.systemName.Draw();
    this.location.Draw();
    this.friendly.Draw();
    this.hostile.Draw();
    this.neutral.Draw();
    this.exit.Draw();
    if (Event.current.type == UnityEngine.EventType.Repaint)
    {
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.planets.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          Graphics.DrawTexture(current.smartRect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.comets.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          Graphics.DrawTexture(current.smartRect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.hubs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          Graphics.DrawTexture(current.smartRect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.sectorEvents.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          Graphics.DrawTexture(current.smartRect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.asteroids.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          Graphics.DrawTexture(current.smartRect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
          if (current.isTarget)
            GUI.DrawTexture(current.smartRect.AbsRect, (Texture) this.targetSelection);
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.debris.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          Graphics.DrawTexture(current.smartRect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
          if (current.isTarget)
            GUI.DrawTexture(current.smartRect.AbsRect, (Texture) this.targetSelection);
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.jumpTargetTransponders.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          if (current.isTarget)
            GUI.DrawTexture(current.smartRect.AbsRect, (Texture) this.targetSelection);
          Graphics.DrawTexture(current.smartRect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.waypoints.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          if (current.isTarget)
            GUI.DrawTexture(current.smartRect.AbsRect, (Texture) this.targetSelection);
          Graphics.DrawTexture(current.smartRect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.ships.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          Graphics.DrawTexture(current.smartRect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
          if (current.isPartyMember && current.spaceObject.PlayerRelation == Relation.Friend)
          {
            Color color = GUI.color;
            GUI.color = this.partyColor;
            GUI.DrawTexture(current.smartRect.AbsRect, (Texture) this.partySelection);
            GUI.color = color;
          }
          if (current.isTarget)
          {
            Color color1 = current.spaceObject.Faction != Faction.Ancient ? (current.spaceObject.Faction != Faction.Colonial ? (!current.spaceObject.IsHostileCongener ? this.cylonColor : this.colonialColor) : (!current.spaceObject.IsHostileCongener ? this.colonialColor : this.cylonColor)) : this.ancientColor;
            Color color2 = GUI.color;
            GUI.color = color1;
            GUI.DrawTexture(current.smartRect.AbsRect, (Texture) this.targetSelection);
            GUI.color = color2;
          }
        }
      }
    }
    for (int index = 0; index < 8; ++index)
    {
      this.gridMarkersX[index].Draw();
      this.gridMarkersY[index].Draw();
    }
    if (this.marker.IsRendered)
      this.marker.Draw();
    else
      this.arrow.Draw();
    for (int index = 0; index < this.sharedDradisMarkers.Count; ++index)
    {
      if (this.sharedDradisMarkers[index].IsRendered)
        this.sharedDradisMarkers[index].Draw();
    }
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (!base.OnMouseUp(mousePosition, mouseKey))
      return false;
    if (mouseKey == KeyCode.Mouse0)
    {
      SpaceLevel level = SpaceLevel.GetLevel();
      SpaceObject target = (SpaceObject) null;
      float2 float2 = mousePosition - this.area.SmartRect.AbsPosition;
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.ships.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          if ((double) (float2 - current.smartRect.Position).magnitude < 5.0 && (UnityEngine.Object) level != (UnityEngine.Object) null && current.spaceObject != null)
          {
            target = current.spaceObject;
            break;
          }
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.jumpTargetTransponders.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          if ((double) (float2 - current.smartRect.Position).magnitude < 5.0 && (UnityEngine.Object) level != (UnityEngine.Object) null && current.spaceObject != null)
          {
            target = current.spaceObject;
            break;
          }
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.debris.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          if ((double) (float2 - current.smartRect.Position).magnitude < 10.0 && (UnityEngine.Object) level != (UnityEngine.Object) null && current.spaceObject != null)
          {
            target = current.spaceObject;
            break;
          }
        }
      }
      using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.hubs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISystemMap.MapIcon current = enumerator.Current;
          if ((double) (float2 - current.smartRect.Position).magnitude < 10.0 && (UnityEngine.Object) level != (UnityEngine.Object) null && current.spaceObject != null)
          {
            target = current.spaceObject;
            break;
          }
        }
      }
      if (target != null)
        TargetSelector.SelectTargetRequest(target, false);
      return target == null;
    }
    if (mouseKey == KeyCode.Mouse1)
    {
      mousePosition = MouseSetup.MousePositionGui - this.area.SmartRect.AbsPosition;
      mousePosition.x /= this.area.Width / 2f;
      mousePosition.y /= this.area.Height / 2f;
      float2 sectorSize = SpaceLevel.GetLevel().GetSectorSize();
      mousePosition.x *= sectorSize.x / 2f;
      mousePosition.y *= sectorSize.y / 2f;
      Vector3 v3Xz = mousePosition.ToV3XZ();
      v3Xz.z = -v3Xz.z;
      SpaceLevel level = SpaceLevel.GetLevel();
      Me me = Game.Me;
      if ((UnityEngine.Object) level != (UnityEngine.Object) null && level.ShipControls != null && me.Ship != null)
        level.ShipControls.Move(v3Xz - me.Ship.Position);
    }
    return true;
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (action == Action.ToggleSystemMap2D)
      return true;
    if (action <= Action.CancelJump && action >= Action.TurnOrSlideLeft || action < Action.ABILITY_MAX && action >= Action.WeaponSlot1)
      return false;
    return this.IsRendered;
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (action == Action.ToggleSystemMap2D)
    {
      this.ToggleIsRendered();
      return true;
    }
    if (action <= Action.CancelJump && action >= Action.TurnOrSlideLeft || action < Action.ABILITY_MAX && action >= Action.WeaponSlot1)
      return false;
    return this.IsRendered;
  }

  public override void RecalculateAbsCoords()
  {
    this.Position = new float2((float) Screen.width / 2f, (float) Screen.height / 2f);
    base.RecalculateAbsCoords();
    using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.hubs.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.smartRect.RecalculateAbsCoords();
    }
    using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.planets.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.smartRect.RecalculateAbsCoords();
    }
    using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.comets.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.smartRect.RecalculateAbsCoords();
    }
    using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.asteroids.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.smartRect.RecalculateAbsCoords();
    }
    using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.debris.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.smartRect.RecalculateAbsCoords();
    }
    using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.jumpTargetTransponders.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.smartRect.RecalculateAbsCoords();
    }
    using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.waypoints.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.smartRect.RecalculateAbsCoords();
    }
    using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.sectorEvents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.smartRect.RecalculateAbsCoords();
    }
    using (List<GUISystemMap.MapIcon>.Enumerator enumerator = this.ships.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.smartRect.RecalculateAbsCoords();
    }
  }

  private void UpdateMarkerPosition()
  {
    if (this.player == null || this.player.Ship == null)
      return;
    this.marker.Ship = this.player.Ship;
    this.marker.IsRendered = true;
    this.marker.Position = this.SpaceToMap(this.player.Ship.Position);
    this.marker.DetectionOuterRadius = 2f * this.SpaceToMap(Game.Me.Stats.DetectionOuterRadius);
    this.marker.DetectionInnerRadius = 2f * this.SpaceToMap(Game.Me.Stats.DetectionInnerRadius);
    if (this.area.Contains(this.marker.SmartRect.AbsPosition))
      return;
    this.marker.IsRendered = false;
    this.arrow.IsRendered = true;
    float2 areaBorder = this.ClampToAreaBorder(this.marker.Position);
    this.arrow.Position = areaBorder + -areaBorder.normalized * 5f;
  }

  public void UpdateSharedDradisMarkers()
  {
    for (int index = 0; index < this.sharedDradisMarkers.Count; ++index)
      this.area.RemovePanel((GUIPanel) this.sharedDradisMarkers[index]);
    this.sharedDradisMarkers.Clear();
    foreach (PlayerShip ship in this.spaceLevel.GetObjectRegistry().GetLoaded<PlayerShip>())
    {
      if (ship.ShipCardLight.ShipRoleDeprecated == ShipRoleDeprecated.Carrier && Game.Me.Party.IsMember(ship.Player))
      {
        float detectionVisualRadius = ship.Player.Stats.DetectionVisualRadius;
        if ((double) (ship.Position - Game.Me.Ship.Position).sqrMagnitude < (double) detectionVisualRadius * (double) detectionVisualRadius)
        {
          GUISystemMap.PlayerMarker playerMarker = new GUISystemMap.PlayerMarker(ship);
          this.sharedDradisMarkers.Add(playerMarker);
          this.area.AddPanel((GUIPanel) playerMarker);
          playerMarker.Parent = this.area.SmartRect;
          playerMarker.IsRendered = true;
          playerMarker.Position = this.SpaceToMap(ship.Position);
          playerMarker.DetectionOuterRadius = 2f * this.SpaceToMap(ship.Player.Stats.DetectionOuterRadius);
          playerMarker.DetectionInnerRadius = 2f * this.SpaceToMap(ship.Player.Stats.DetectionInnerRadius);
        }
      }
    }
  }

  private float SpaceToMap(float point)
  {
    if ((UnityEngine.Object) this.spaceLevel == (UnityEngine.Object) null)
      return 0.0f;
    float2 sectorSize = this.spaceLevel.GetSectorSize();
    return (float) ((double) point * ((double) this.area.Width / 2.0) / ((double) sectorSize.x / 2.0));
  }

  private float2 SpaceToMap(Vector3 position)
  {
    if ((UnityEngine.Object) this.spaceLevel == (UnityEngine.Object) null)
      return float2.zero;
    float2 sectorSize = this.spaceLevel.GetSectorSize();
    Matrix4x4 identity = Matrix4x4.identity;
    identity.m00 = (float) ((double) this.area.Width / 2.0 / ((double) sectorSize.x / 2.0));
    identity.m22 = (float) (-((double) this.area.Height / 2.0) / ((double) sectorSize.y / 2.0));
    return float2.FromV3XZ((Vector3) (identity * (Vector4) position));
  }

  private float2 ClampToAreaBorder(float2 position)
  {
    GUISystemMap.segment2 first = new GUISystemMap.segment2(float2.zero, position);
    float2 float2_1 = new float2((float) (-(double) this.area.SmartRect.Width / 2.0), (float) (-(double) this.area.SmartRect.Height / 2.0));
    float2 float2_2 = new float2((float) (-(double) this.area.SmartRect.Width / 2.0), this.area.SmartRect.Height / 2f);
    float2 float2_3 = new float2(this.area.SmartRect.Width / 2f, (float) (-(double) this.area.SmartRect.Height / 2.0));
    float2 float2_4 = new float2(this.area.SmartRect.Width / 2f, this.area.SmartRect.Height / 2f);
    GUISystemMap.segment2 segment2_1 = new GUISystemMap.segment2(float2_2, float2_1);
    GUISystemMap.segment2 segment2_2 = new GUISystemMap.segment2(float2_1, float2_3);
    GUISystemMap.segment2 segment2_3 = new GUISystemMap.segment2(float2_3, float2_4);
    GUISystemMap.segment2 segment2_4 = new GUISystemMap.segment2(float2_4, float2_2);
    using (List<GUISystemMap.segment2>.Enumerator enumerator = new List<GUISystemMap.segment2>() { segment2_1, segment2_2, segment2_3, segment2_4 }.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISystemMap.segment2 current = enumerator.Current;
        float2 result;
        if (this.PointOfIntersectionSegmentAndSegment(first, current, out result))
          return result;
      }
    }
    return float2.zero;
  }

  private bool PointOfIntersectionSegmentAndSegment(GUISystemMap.segment2 first, GUISystemMap.segment2 second, out float2 result)
  {
    result = new float2(0.0f, 0.0f);
    float2 float2_1 = first.end - first.begin;
    float2 float2_2 = second.end - second.begin;
    float num1 = -float2_1.y;
    float num2 = float2_1.x;
    float num3 = (float) -((double) num1 * (double) first.begin.x + (double) num2 * (double) first.begin.y);
    float num4 = -float2_2.y;
    float num5 = float2_2.x;
    float num6 = (float) -((double) num4 * (double) second.begin.x + (double) num5 * (double) second.begin.y);
    float num7 = (float) ((double) num4 * (double) first.begin.x + (double) num5 * (double) first.begin.y) + num6;
    float num8 = (float) ((double) num4 * (double) first.end.x + (double) num5 * (double) first.end.y) + num6;
    float num9 = (float) ((double) num1 * (double) second.begin.x + (double) num2 * (double) second.begin.y) + num3;
    float num10 = (float) ((double) num1 * (double) second.end.x + (double) num2 * (double) second.end.y) + num3;
    if ((double) num7 * (double) num8 >= 0.0 || (double) num9 * (double) num10 >= 0.0)
      return false;
    float num11 = num7 / (num7 - num8);
    result = first.begin + num11 * float2_1;
    return true;
  }

  private bool MouseOverIcon(List<GUISystemMap.MapIcon> mapIconList, Vector2 mousePosition, out int iconIndex)
  {
    iconIndex = 0;
    while (iconIndex < mapIconList.Count)
    {
      if (mapIconList[iconIndex].smartRect.AbsRect.Contains(mousePosition))
        return true;
      iconIndex = iconIndex + 1;
    }
    return false;
  }

  private GUISystemMap.MapIconType GetShipType(ref Ship ship)
  {
    GUISystemMap.MapIconType mapIconType = GUISystemMap.MapIconType.Drone;
    if (ship is FighterShip || ship is PlayerShip)
    {
      switch (ship.ShipCardLight.Tier)
      {
        case 1:
          mapIconType = ship.Faction != Faction.Colonial ? (ship.Faction != Faction.Cylon ? GUISystemMap.MapIconType.Drone : GUISystemMap.MapIconType.CylonStrikeShip) : GUISystemMap.MapIconType.ColonialStrikeShip;
          break;
        case 2:
          mapIconType = ship.Faction != Faction.Colonial ? (ship.Faction != Faction.Cylon ? GUISystemMap.MapIconType.Drone : GUISystemMap.MapIconType.CylonEscortShip) : GUISystemMap.MapIconType.ColonialEscortShip;
          break;
        case 3:
          mapIconType = ship.Faction != Faction.Colonial ? (ship.Faction != Faction.Cylon ? GUISystemMap.MapIconType.Drone : GUISystemMap.MapIconType.CylonLineShip) : GUISystemMap.MapIconType.ColonialLineShip;
          break;
        case 4:
          mapIconType = ship.ShipCardLight.ShipRoleDeprecated != ShipRoleDeprecated.Carrier ? (ship.Faction != Faction.Colonial ? (ship.Faction != Faction.Cylon ? GUISystemMap.MapIconType.Drone : GUISystemMap.MapIconType.CylonFreighterShip) : GUISystemMap.MapIconType.ColonialFreighterShip) : (ship.Faction != Faction.Colonial ? (ship.Faction != Faction.Cylon ? GUISystemMap.MapIconType.Drone : GUISystemMap.MapIconType.CylonCarrierShip) : GUISystemMap.MapIconType.ColonialCarrierShip);
          break;
      }
    }
    else if (ship is WeaponPlatform)
    {
      WeaponPlatform weaponPlatform = ship as WeaponPlatform;
      switch (weaponPlatform.ShipCardLight.Tier)
      {
        case 1:
          return weaponPlatform.Faction == Faction.Colonial ? GUISystemMap.MapIconType.ColonialLightPlatform : GUISystemMap.MapIconType.CylonLightPlatform;
        case 2:
          return weaponPlatform.Faction == Faction.Colonial ? GUISystemMap.MapIconType.ColonialMediumPlatform : GUISystemMap.MapIconType.CylonMediumPlatform;
        case 3:
          return weaponPlatform.Faction == Faction.Colonial ? GUISystemMap.MapIconType.ColonialHeavyPlatform : GUISystemMap.MapIconType.CylonHeavyPlatform;
        default:
          Debug.LogWarning((object) ("Unknown weapon platform tier " + (object) weaponPlatform.ShipCardLight.Tier));
          break;
      }
    }
    else if (ship is CruiserShip)
    {
      CruiserShip cruiserShip = ship as CruiserShip;
      bool flag1 = cruiserShip.WorldCard.PrefabName.Contains("galactica");
      bool flag2 = cruiserShip.WorldCard.PrefabName.Contains("basestar");
      bool flag3 = cruiserShip.WorldCard.PrefabName.Contains("pegasus");
      bool flag4 = cruiserShip.WorldCard.PrefabName.Contains("freighter");
      bool flag5 = cruiserShip is OutpostShip;
      mapIconType = !flag1 ? (!flag2 ? (!flag3 ? (!flag5 ? (!flag4 ? (cruiserShip.Faction != Faction.Colonial ? (cruiserShip.Faction != Faction.Cylon ? GUISystemMap.MapIconType.Drone : GUISystemMap.MapIconType.CylonMiningShip) : GUISystemMap.MapIconType.ColonialMiningShip) : (cruiserShip.Faction != Faction.Colonial ? (cruiserShip.Faction != Faction.Cylon ? GUISystemMap.MapIconType.Drone : GUISystemMap.MapIconType.CylonFreighterShip) : GUISystemMap.MapIconType.ColonialFreighterShip)) : (cruiserShip.Faction != Faction.Colonial ? (cruiserShip.Faction != Faction.Cylon ? GUISystemMap.MapIconType.Drone : GUISystemMap.MapIconType.CylonOutpost) : GUISystemMap.MapIconType.ColonialOutpost)) : GUISystemMap.MapIconType.Pegasus) : GUISystemMap.MapIconType.Basestar) : GUISystemMap.MapIconType.Galactica;
    }
    else if (ship is MiningShip)
      mapIconType = ship.Faction != Faction.Colonial ? (ship.Faction != Faction.Cylon ? GUISystemMap.MapIconType.Drone : GUISystemMap.MapIconType.CylonMiningShip) : GUISystemMap.MapIconType.ColonialMiningShip;
    else if (ship is AsteroidShip)
      mapIconType = GUISystemMap.MapIconType.Drone;
    else if (ship is JumpBeacon)
      mapIconType = ship.Faction != Faction.Colonial ? (ship.Faction != Faction.Cylon ? GUISystemMap.MapIconType.NeutralJumpBeacon : GUISystemMap.MapIconType.CylonJumpBeacon) : GUISystemMap.MapIconType.ColonialJumpBeacon;
    return mapIconType;
  }

  private class MapIcon
  {
    public bool isPartyMember;
    public bool isTarget;
    public AtlasEntry atlasEntry;
    public SmartRect smartRect;
    public SpaceObject spaceObject;
  }

  private class PlayerMarker : GUIPanel
  {
    private float orientation;
    private readonly GUIImageNew playerMarker;
    private readonly GUIImageNew outerCircle;
    private readonly GUIImageNew innerCircle;
    private PlayerShip ship;

    public override float2 Position
    {
      get
      {
        return base.Position;
      }
      set
      {
        base.Position = value;
        this.RecalculateAbsCoords();
      }
    }

    public PlayerShip Ship
    {
      get
      {
        return this.ship;
      }
      set
      {
        this.ship = value;
      }
    }

    public float DetectionOuterRadius
    {
      set
      {
        this.outerCircle.SmartRect.Width = value;
        this.outerCircle.SmartRect.Height = value;
      }
    }

    public float DetectionInnerRadius
    {
      set
      {
        this.innerCircle.SmartRect.Width = value;
        this.innerCircle.SmartRect.Height = value;
      }
    }

    public Rect AbsRect
    {
      get
      {
        return this.playerMarker.SmartRect.AbsRect;
      }
    }

    public PlayerMarker(PlayerShip ship)
    {
      this.playerMarker = new GUIImageNew(ResourceLoader.Load<Texture2D>("GUI/Map/SystemMap/player_mark"));
      this.playerMarker.Parent = this.root;
      Texture2D texture = ResourceLoader.Load<Texture2D>("GUI/Map/SystemMap/dradis_circle");
      this.outerCircle = new GUIImageNew(texture);
      this.outerCircle.Parent = this.playerMarker.SmartRect;
      this.AddPanel((GUIPanel) this.outerCircle);
      this.innerCircle = new GUIImageNew(texture);
      this.AddPanel((GUIPanel) this.innerCircle);
      this.ship = ship;
    }

    public override void Draw()
    {
      base.Draw();
      if (this.ship == null || !this.ship.IsMe)
        return;
      GUIUtility.RotateAroundPivot(this.orientation, this.playerMarker.SmartRect.AbsPosition.ToV2());
      this.playerMarker.Draw();
      GUI.matrix = Matrix4x4.identity;
    }

    public override void RecalculateAbsCoords()
    {
      base.RecalculateAbsCoords();
      this.playerMarker.RecalculateAbsCoords();
    }

    public void MakeOrientationFrom(Vector3 vec1, Vector3 vec2)
    {
      this.orientation = this.CalcAngle0to360CW(vec1, vec2);
    }

    public override bool OnShowTooltip(float2 mousePosition)
    {
      if (!this.IsRendered || this.advancedTooltip == null || !this.AbsRect.Contains(mousePosition.ToV2()))
        return false;
      Game.TooltipManager.ShowTooltip(this.advancedTooltip);
      return true;
    }

    private float CalcAngle0to360CW(Vector3 vector1, Vector3 vector2)
    {
      float num = Vector3.Angle(vector1, vector2);
      if ((double) Vector3.Cross(vector1, vector2).y > 0.0)
        return num;
      return 360f - num;
    }
  }

  private class DirectionMarker : GUIPanel
  {
    private float sign = 1f;
    private const float BLINK_INTERVAL = 0.5f;
    private float orientation;
    private GUIImageNew marker;
    private Color color;

    public override bool IsUpdated
    {
      get
      {
        return this.IsRendered;
      }
      set
      {
        throw new Exception("method call is prohibited");
      }
    }

    public override float2 Position
    {
      set
      {
        base.Position = value;
        this.RecalculateAbsCoords();
        this.orientation = this.CalcAngle0to360CW(Vector3.forward, new float2(this.SmartRect.Position.x, -this.SmartRect.Position.y).ToV3XZ());
      }
    }

    public DirectionMarker()
    {
      this.marker = new GUIImageNew(ResourceLoader.Load<Texture2D>("GUI/Map/SystemMap/player_direction"));
      this.marker.Parent = this.root;
      this.color = Game.Me.Faction != Faction.Colonial ? ColorUtility.MakeColorFrom0To255(201U, 46U, 71U) : ColorUtility.MakeColorFrom0To255(122U, 163U, 238U);
      this.color.a = 0.0f;
    }

    public override void Update()
    {
      base.Update();
      this.color.a += (float) ((double) this.sign * (double) Time.deltaTime * 1.0 / 0.5);
      if ((double) this.color.a > 1.0 || (double) this.color.a < 0.0)
        this.sign = -this.sign;
      this.color.a = Mathf.Clamp01(this.color.a);
    }

    public override void Draw()
    {
      base.Draw();
      GUIUtility.RotateAroundPivot(this.orientation, this.marker.SmartRect.AbsPosition.ToV2());
      if (Event.current.type == UnityEngine.EventType.Repaint)
        Graphics.DrawTexture(this.marker.SmartRect.AbsRect, (Texture) this.marker.Texture, this.marker.InnerRect, 0, 0, 0, 0, this.color);
      GUI.matrix = Matrix4x4.identity;
    }

    public override void RecalculateAbsCoords()
    {
      base.RecalculateAbsCoords();
      this.marker.RecalculateAbsCoords();
    }

    private float CalcAngle0to360CW(Vector3 vector1, Vector3 vector2)
    {
      float num = Vector3.Angle(vector1, vector2);
      if ((double) Vector3.Cross(vector1, vector2).y > 0.0)
        return num;
      return 360f - num;
    }
  }

  public struct segment2
  {
    public float2 begin;
    public float2 end;

    public segment2(float2 begin, float2 end)
    {
      this.begin = begin;
      this.end = end;
    }
  }

  private enum MapIconType : byte
  {
    YourShip,
    ColonialOutpost,
    ColonialMiningShip,
    CylonOutpost,
    CylonMiningShip,
    Basestar,
    Galactica,
    Pegasus,
    ColonialStrikeShip,
    ColonialEscortShip,
    ColonialLineShip,
    ColonialFreighterShip,
    ColonialCarrierShip,
    CylonStrikeShip,
    CylonEscortShip,
    CylonLineShip,
    CylonFreighterShip,
    CylonCarrierShip,
    Drone,
    DebrisPile,
    Planetoid,
    Comet,
    ColonialJumpBeacon,
    CylonJumpBeacon,
    NeutralJumpBeacon,
    ColonialLightPlatform,
    ColonialMediumPlatform,
    ColonialHeavyPlatform,
    CylonLightPlatform,
    CylonMediumPlatform,
    CylonHeavyPlatform,
    ColonialJumpTargetTransponder,
    CylonJumpTargetTransponder,
  }
}
