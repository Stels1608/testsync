﻿// Decompiled with JetBrains decompiler
// Type: NotificationMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using Paths;
using UnityEngine;

public class NotificationMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "NotificationMediator";
  private OnScreenNotificationView view;

  public NotificationMediator()
    : base("NotificationMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.ShowOnScreenNotification);
    this.AddMessageInterest(Message.NotificationServerError);
    this.AddMessageInterest(Message.PlayerLevel);
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.LevelLoaded);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.LoadNewLevel:
        this.view.gameObject.SetActive(false);
        break;
      case Message.LevelLoaded:
        this.view.gameObject.SetActive(true);
        break;
      case Message.UiCreated:
        this.CreateNotificationView();
        break;
      case Message.ShowOnScreenNotification:
        this.view.ShowOnScreenNotification((OnScreenNotification) message.Data);
        break;
      case Message.NotificationServerError:
        new InfoBox(Tools.ParseMessage("%$bgo.notificationerror." + (object) (uint) (int) message.Data + "%")).Show();
        break;
      case Message.PlayerLevel:
        byte num = (byte) message.Data;
        bool flag = (int) num > (int) Game.Me.Level;
        Game.Me.Level = num;
        if (!flag)
          break;
        this.SendMessage(Message.ShowOnScreenNotification, (object) new LevelUpNotification(Game.Me.Rank));
        FacadeFactory.GetInstance().SendMessage(Message.RecheckSystemButtons);
        GUISound.Instance.OnLevelUp();
        break;
    }
  }

  private void CreateNotificationView()
  {
    this.view = UguiTools.CreateChild(Resources.Load<GameObject>(Ui.Prefabs + "/StaticNotificationView"), UguiTools.GetCanvas(BsgoCanvas.Hud).transform).GetComponent<OnScreenNotificationView>();
    FacadeFactory.GetInstance().SendMessage(Message.AddToCombatGui, (object) this.view);
  }
}
