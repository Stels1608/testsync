﻿// Decompiled with JetBrains decompiler
// Type: SectorEvent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class SectorEvent : SpaceObject
{
  public AnonymousDelegate OnStateUpdate = (AnonymousDelegate) (() => {});
  private bool? isInRadius;
  private UnityEngine.Sprite _icon;

  public SectorEventCard SectorEventCard { get; private set; }

  public Faction OwnerFaction { get; private set; }

  public float Radius { get; private set; }

  public SectorEventState State { get; private set; }

  public override string ObjectName
  {
    get
    {
      return this.Name;
    }
  }

  public override string Name
  {
    get
    {
      return BsgoLocalization.Get(this.SectorEventCard.Name);
    }
  }

  public string Description
  {
    get
    {
      if (Game.Me.Faction == Faction.Cylon)
        return this.SectorEventCard.DescriptionCylon;
      if (Game.Me.Faction == Faction.Colonial)
        return this.SectorEventCard.DescriptionColonial;
      return "<no description for" + (object) Game.Me.Faction + ">";
    }
  }

  public bool IsInRadius
  {
    get
    {
      if (this.isInRadius.HasValue)
        return this.isInRadius.Value;
      return false;
    }
  }

  public override UnityEngine.Sprite Icon3dMap
  {
    get
    {
      if ((Object) this._icon == (Object) null)
      {
        Texture2D icon = this.SectorEventCard.Icon;
        this._icon = UnityEngine.Sprite.Create(icon, new Rect(0.0f, 0.0f, (float) icon.width, (float) icon.height), new Vector2(0.5f, 0.5f));
      }
      return this._icon;
    }
  }

  public override UnityEngine.Sprite IconBrackets
  {
    get
    {
      if ((Object) this._icon == (Object) null)
      {
        Texture2D icon = this.SectorEventCard.Icon;
        this._icon = UnityEngine.Sprite.Create(icon, new Rect(0.0f, 0.0f, (float) icon.width, (float) icon.height), new Vector2(0.5f, 0.5f));
      }
      return this._icon;
    }
  }

  public override bool IsTargetable
  {
    get
    {
      return false;
    }
  }

  public SectorEvent(uint objectId)
    : base(objectId)
  {
    this.AutoSubscribe = true;
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.SectorEventCard = (SectorEventCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.SectorEvent);
    this.AreCardsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.SectorEventCard
    });
    this.IsConstructed.Set();
    this.Subscribe();
  }

  public void Update(SectorEventStateUpdate eventStateUpdate)
  {
    this.Position = eventStateUpdate.Position;
    this.Radius = eventStateUpdate.Radius;
    this.MeshBoundsRaw = new Bounds(Vector3.zero, 2f * Vector3.one * this.Radius);
    this.OwnerFaction = eventStateUpdate.OwnerFaction;
    this.State = eventStateUpdate.State;
    this.OnStateUpdate();
  }

  public override void Update()
  {
    base.Update();
    this.UpdateRadius();
  }

  public void UpdateRadius()
  {
    bool flag = (double) (this.Position - SpaceLevel.GetLevel().GetPlayerPosition()).sqrMagnitude < (double) this.SectorEventCard.Radius * (double) this.SectorEventCard.Radius;
    int num1 = flag ? 1 : 0;
    bool? nullable = this.isInRadius;
    int num2 = nullable.GetValueOrDefault() ? 1 : 0;
    if ((num1 != num2 ? 1 : (!nullable.HasValue ? 1 : 0)) == 0)
      return;
    this.isInRadius = new bool?(flag);
    FacadeFactory.GetInstance().SendMessage(Message.PlayerPassedSectorEventBorder, (object) new KeyValuePair<SectorEvent, bool>(this, flag));
  }
}
