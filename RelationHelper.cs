﻿// Decompiled with JetBrains decompiler
// Type: RelationHelper
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public static class RelationHelper
{
  public static Relation GetRelation(ISpaceEntity thisObject, Faction otherFaction, FactionGroup otherFactionGroup, TargetBracketMode targetBracketmode = TargetBracketMode.Default)
  {
    if (thisObject.IsMe)
      return Relation.Self;
    if (otherFaction == Faction.Neutral || thisObject.Faction == Faction.Neutral)
      return Relation.Neutral;
    return targetBracketmode == TargetBracketMode.AllEnemy || otherFaction != thisObject.Faction || otherFactionGroup != thisObject.FactionGroup ? Relation.Enemy : Relation.Friend;
  }
}
