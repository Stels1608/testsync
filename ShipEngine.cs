﻿// Decompiled with JetBrains decompiler
// Type: ShipEngine
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class ShipEngine : MonoBehaviour
{
  public float animationRate = 4f;
  public float animationRange = 0.02f;
  public Color tintColorOverride = new Color(1f, 1f, 1f);
  private float shipSpeedScaleFactor = 1f;
  public Color overrideColorMin = new Color(0.3f, 0.2f, 0.0f);
  public Color overrideColorMax = new Color(0.1f, 0.2f, 0.5f);
  public float flareMaxStretchScale = 1f;
  public float flareMinStretchScale = 0.2f;
  public float textureAnimationRate = 1f;
  private Vector2 textureUVOffset = new Vector2(0.0f, 0.0f);
  public float EngineVolume = 1f;
  public float EngineMinDistance = 1f;
  private float fadingOutDuration = 1f;
  public float BoostVolume = 1f;
  public float DopplerLevel = 1f;
  private ShipEngine.EngineState lastState;
  public ShipEngine.EngineState State;
  public ShipEngine.EngineComponentType engineComponentType;
  public bool useColorOverride;
  public bool shipSpeedOverridesColor;
  public bool animateTextureU;
  public bool inverseTextureAnimation;
  private Color originalColor;
  private float animationTime;
  private AudioSource engineAudio;
  public AudioClip EngineClip;
  private bool fadingOut;
  private AudioSource boostAudio;
  public AudioClip BoostStartClip;
  public AudioClip BoostLoopClip;
  public AudioClip BoostEndClip;
  private float flareOriginalScale;
  private Renderer renderer;

  public float FlareOriginalScale
  {
    get
    {
      return this.flareOriginalScale;
    }
    set
    {
      this.flareOriginalScale = value;
    }
  }

  private void Start()
  {
    if (this.engineComponentType == ShipEngine.EngineComponentType.Glow)
    {
      Object.Destroy((Object) this.gameObject);
    }
    else
    {
      if (this.engineComponentType != ShipEngine.EngineComponentType.Flare)
        return;
      this.engineAudio = this.gameObject.AddComponent<AudioSource>();
      this.engineAudio.playOnAwake = false;
      this.engineAudio.clip = this.EngineClip;
      this.engineAudio.volume = this.EngineVolume;
      this.engineAudio.minDistance = this.EngineMinDistance;
      this.engineAudio.maxDistance = this.engineAudio.minDistance * 256f;
      this.engineAudio.dopplerLevel = this.DopplerLevel;
      this.engineAudio.loop = true;
      this.engineAudio.spatialBlend = 1f;
      this.boostAudio = this.gameObject.AddComponent<AudioSource>();
      this.boostAudio.playOnAwake = false;
      this.boostAudio.clip = this.BoostStartClip;
      this.boostAudio.volume = this.BoostVolume;
      this.boostAudio.minDistance = this.EngineMinDistance;
      this.boostAudio.maxDistance = this.boostAudio.minDistance * 256f;
      this.boostAudio.dopplerLevel = this.DopplerLevel;
      this.boostAudio.loop = false;
      this.boostAudio.spatialBlend = 1f;
    }
  }

  private void Awake()
  {
    this.State = ShipEngine.EngineState.Off;
    this.renderer = this.GetComponent<Renderer>();
    this.flareOriginalScale = this.transform.localScale.z;
    if (!((Object) this.renderer != (Object) null))
      return;
    this.originalColor = !this.useColorOverride ? this.renderer.material.GetColor("_TintColor") : this.tintColorOverride;
  }

  private void Update()
  {
    if (this.engineComponentType == ShipEngine.EngineComponentType.Undefined)
      return;
    switch (this.State)
    {
      case ShipEngine.EngineState.Off:
        this.shipSpeedScaleFactor = 0.0f;
        break;
      case ShipEngine.EngineState.Weak:
        this.shipSpeedScaleFactor = 0.25f;
        break;
      case ShipEngine.EngineState.Normal:
        this.shipSpeedScaleFactor = 0.5f;
        break;
      case ShipEngine.EngineState.Boost:
        this.shipSpeedScaleFactor = 1f;
        break;
    }
    this.shipSpeedScaleFactor = Mathf.Clamp01(this.shipSpeedScaleFactor);
    this.animationTime += Time.deltaTime * this.animationRate;
    this.animationTime = Mathf.Repeat(this.animationTime, 2f);
    float num = Mathf.Sin(this.animationTime * 3.141593f) * this.animationRange;
    Color color = this.originalColor;
    if (this.shipSpeedOverridesColor)
      color = new Color(Mathf.Lerp(this.overrideColorMin.r, this.overrideColorMax.r, this.shipSpeedScaleFactor), Mathf.Lerp(this.overrideColorMin.g, this.overrideColorMax.g, this.shipSpeedScaleFactor), Mathf.Lerp(this.overrideColorMin.b, this.overrideColorMax.b, this.shipSpeedScaleFactor), Mathf.Lerp(this.overrideColorMin.a, this.overrideColorMax.a, this.shipSpeedScaleFactor));
    if (this.engineComponentType == ShipEngine.EngineComponentType.Flare)
    {
      switch (this.State)
      {
        case ShipEngine.EngineState.Off:
          if (this.engineAudio.isPlaying && !this.fadingOut)
            this.StartCoroutine(this.FadeOutEngineSound());
          if (this.lastState == ShipEngine.EngineState.Boost)
          {
            this.boostAudio.clip = this.BoostEndClip;
            this.boostAudio.loop = false;
            this.boostAudio.Play();
            break;
          }
          break;
        case ShipEngine.EngineState.Weak:
        case ShipEngine.EngineState.Normal:
          if (!this.engineAudio.isPlaying)
          {
            this.engineAudio.volume = this.EngineVolume;
            this.engineAudio.Play();
          }
          if (this.lastState == ShipEngine.EngineState.Boost)
          {
            this.boostAudio.clip = this.BoostEndClip;
            this.boostAudio.loop = false;
            this.boostAudio.Play();
            break;
          }
          break;
        case ShipEngine.EngineState.Boost:
          if (!this.engineAudio.isPlaying)
          {
            this.engineAudio.volume = this.EngineVolume;
            this.engineAudio.Play();
          }
          if (this.lastState != ShipEngine.EngineState.Boost)
          {
            this.boostAudio.clip = this.BoostStartClip;
            this.boostAudio.loop = false;
            this.boostAudio.Play();
            break;
          }
          if (!this.boostAudio.isPlaying)
          {
            this.boostAudio.clip = this.BoostLoopClip;
            this.boostAudio.loop = true;
            this.boostAudio.Play();
            break;
          }
          break;
      }
      this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, (this.FlareOriginalScale + this.FlareOriginalScale * num) * Mathf.Lerp(this.flareMinStretchScale, this.flareMaxStretchScale, this.shipSpeedScaleFactor));
      if ((Object) this.renderer != (Object) null)
        this.renderer.material.SetColor("_TintColor", color);
    }
    if (this.engineComponentType == ShipEngine.EngineComponentType.Center)
      this.renderer.material.SetColor("_TintColor", color + new Color(num, num, num));
    if (this.animateTextureU)
    {
      if (!this.inverseTextureAnimation)
      {
        this.textureUVOffset.x += Time.deltaTime * this.textureAnimationRate;
        this.textureUVOffset.x = Mathf.Repeat(this.textureUVOffset.x, 1f);
      }
      else
      {
        this.textureUVOffset.x -= Time.deltaTime * this.textureAnimationRate;
        this.textureUVOffset.x = Mathf.Repeat(this.textureUVOffset.x, -1f);
      }
      this.renderer.material.mainTextureOffset = this.textureUVOffset;
    }
    this.lastState = this.State;
  }

  [DebuggerHidden]
  private IEnumerator FadeOutEngineSound()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ShipEngine.\u003CFadeOutEngineSound\u003Ec__Iterator3C() { \u003C\u003Ef__this = this };
  }

  private void OnEnable()
  {
    if (!((Object) this.renderer != (Object) null))
      return;
    this.renderer.enabled = true;
  }

  private void OnDisable()
  {
    if (!((Object) this.renderer != (Object) null))
      return;
    this.renderer.enabled = false;
  }

  public enum EngineComponentType
  {
    Undefined,
    Center,
    Flare,
    Glow,
  }

  public enum EngineState
  {
    Off,
    Weak,
    Normal,
    Boost,
  }
}
