﻿// Decompiled with JetBrains decompiler
// Type: Catalogue
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class Catalogue
{
  private readonly Dictionary<long, Card> cache = new Dictionary<long, Card>();
  private readonly CatalogueProtocol catalogueProtocol;

  public Dictionary<long, Card> DebugCache
  {
    get
    {
      return this.cache;
    }
  }

  public Catalogue()
  {
    this.catalogueProtocol = new CatalogueProtocol(this);
    Game.ProtocolManager.RegisterProtocol((BgoProtocol) this.catalogueProtocol);
  }

  public Card FetchCard(uint cardGUID, CardView cardView)
  {
    if ((int) cardGUID == 0)
      return (Card) null;
    Card card;
    if (!this.cache.TryGetValue(this.GenerateKey(cardGUID, cardView), out card))
    {
      card = CardFactory.CreateCard(cardGUID, cardView);
      this.AddCard(cardGUID, cardView, card);
      this.catalogueProtocol.RequestCard(cardGUID, cardView);
    }
    return card;
  }

  private void AddCard(uint cardGUID, CardView cardView, Card card)
  {
    this.cache[this.GenerateKey(cardGUID, cardView)] = card;
  }

  private long GenerateKey(uint cardGUID, CardView cardView)
  {
    return ((long) cardView << 32) + (long) cardGUID;
  }
}
