﻿// Decompiled with JetBrains decompiler
// Type: GuiDeathPopup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

internal class GuiDeathPopup : GuiPanel
{
  private const string mc_layout = "GUI/UpSell/Death_Feature.txt";
  private const int no_flavor_image_shrink = 220;
  private string packageName;
  public DeathDesc m_deathDesc;

  private GuiDeathPopup(GUICard guiCard, RewardCard bonusCard, uint discountAmount)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiDeathPopup.\u003CGuiDeathPopup\u003Ec__AnonStorey9C popupCAnonStorey9C = new GuiDeathPopup.\u003CGuiDeathPopup\u003Ec__AnonStorey9C();
    // ISSUE: reference to a compiler-generated field
    popupCAnonStorey9C.bonusCard = bonusCard;
    // ISSUE: explicit constructor call
    base.\u002Ector("GUI/UpSell/Death_Feature.txt");
    GuiPanel guiPanel1 = this.Find<GuiPanel>("Panel_BG");
    GuiImage guiImage = guiPanel1.Find<GuiImage>("image_flavor");
    GuiPanel guiPanel2 = guiPanel1.Find<GuiPanel>("panel_container");
    GuiLabel guiLabel1 = guiPanel1.Find<GuiLabel>("label_header");
    GuiLabel guiLabel2 = guiPanel2.Find<GuiLabel>("label_shortDescription");
    GuiLabel guiLabel3 = guiPanel2.Find<GuiLabel>("label_longDescription");
    GuiLabel guiLabel4 = guiPanel2.Find<GuiLabel>("label_discount");
    GuiButton guiButton1 = guiPanel2.Find<GuiButton>("button_buyNow");
    GuiButton guiButton2 = guiPanel2.Find<GuiButton>("button_pay");
    GuiButton guiButton3 = guiPanel1.Find<GuiButton>("button_close");
    GuiPanel guiPanel3 = guiPanel2.Find<GuiPanel>("panel_items");
    GuiLabel guiLabel5 = guiPanel2.Find<GuiLabel>("LabelName1");
    guiLabel1.Text = guiCard.Name;
    guiLabel2.Text = guiCard.ShortDescription;
    guiLabel3.Text = guiCard.Description;
    guiLabel5.Text = "%$bgo.deathpopup.contents%";
    guiButton1.Text = "%$bgo.deathpopup.buy_now%";
    if (guiCard.GUITexturePath == string.Empty)
    {
      guiPanel2.PositionY -= 220f;
      guiImage.IsRendered = false;
      guiPanel1.Find<GuiImage>("image_panel_BG").SizeY -= 220f;
      guiPanel1.SizeY -= 220f;
      this.SizeY -= 220f;
    }
    else
      guiImage.Texture = guiCard.GUITexture;
    guiLabel4.Text = BsgoLocalization.Get("%$bgo.deathpopup.discount%", (object) discountAmount);
    // ISSUE: reference to a compiler-generated field
    Game.Payment.QueryPackagePrice(popupCAnonStorey9C.bonusCard.Package, new PaymentInterface.PaymentCallback(this.OnPaymentPriceRecv));
    // ISSUE: reference to a compiler-generated field
    this.packageName = popupCAnonStorey9C.bonusCard.Package;
    guiButton3.OnClick = new AnonymousDelegate(this.Hide);
    // ISSUE: reference to a compiler-generated method
    guiButton1.OnClick = new AnonymousDelegate(popupCAnonStorey9C.\u003C\u003Em__120);
    guiButton2.OnClick = guiButton1.OnClick;
    GuiPanelVerticalScroll panelVerticalScroll = new GuiPanelVerticalScroll();
    panelVerticalScroll.Size = guiPanel3.Size;
    guiPanel3.AddChild((GuiElementBase) panelVerticalScroll);
    // ISSUE: reference to a compiler-generated field
    for (int index = 0; index < popupCAnonStorey9C.bonusCard.Items.Count; ++index)
    {
      // ISSUE: reference to a compiler-generated field
      panelVerticalScroll.AddChild((GuiElementBase) new GuiDeathPopupItem(popupCAnonStorey9C.bonusCard.Items[index]));
    }
    // ISSUE: reference to a compiler-generated field
    if (popupCAnonStorey9C.bonusCard.PackagedCubits <= 0U)
      return;
    ShipConsumableCard shipConsumableCard = Game.Catalogue.FetchCard(264733124U, CardView.ShipConsumable) as ShipConsumableCard;
    ShopItemCard shopItemCard = Game.Catalogue.FetchCard(264733124U, CardView.Price) as ShopItemCard;
    ItemCountable itemCountable = new ItemCountable();
    itemCountable.Card = shipConsumableCard;
    // ISSUE: reference to a compiler-generated field
    itemCountable.count = popupCAnonStorey9C.bonusCard.PackagedCubits;
    itemCountable.ItemGUICard = shipConsumableCard.GUICard;
    itemCountable.ShopItemCard = shopItemCard;
    panelVerticalScroll.AddChild((GuiElementBase) new GuiDeathPopupItem((ShipItem) itemCountable));
  }

  public override void Update()
  {
    base.Update();
    this.PositionCenter = new Vector2((float) Screen.width / 2f, (float) Screen.height / 2f);
  }

  public static void Show(GUICard guiCard, RewardCard bonusCard, uint discountAmount)
  {
    GuiDialogPopupManager.ShowDialog((GuiPanel) new GuiDeathPopup(guiCard, bonusCard, discountAmount), false);
  }

  public void Hide()
  {
    GuiDialogPopupManager.CloseDialog();
    if (this.m_deathDesc != null)
      PlayerRespawnDialog.Show(this.m_deathDesc);
    Game.Payment.UnregisterPackagePrice(this.packageName, new PaymentInterface.PaymentCallback(this.OnPaymentPriceRecv));
  }

  public void OnPaymentPriceRecv(object paymentPrice)
  {
    this.Find<GuiPanel>("Panel_BG").Find<GuiPanel>("panel_container").Find<GuiLabel>("label_cashValue").Text = BsgoLocalization.Get("%$bgo.deathpopup.price%", (object) (string) paymentPrice);
  }
}
