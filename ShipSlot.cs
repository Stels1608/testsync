﻿// Decompiled with JetBrains decompiler
// Type: ShipSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Paperdolls.PaperdollServerData;
using System;

public class ShipSlot : IContainer, IProtocolRead
{
  public AbilityValidation Validation = new AbilityValidation();
  public ObjectStats GameStats = new ObjectStats();
  public ushort ServerID;
  public ShipSystem System;
  public uint ConsumableGUID;
  public ShipAbility Ability;
  public bool UpdatedStats;
  public bool Inoperable;
  public Flag IsLoaded;
  private Action action;
  private HangarShip ship;
  private IContainerID containerID;

  public Action Action
  {
    get
    {
      return this.action;
    }
    set
    {
      this.action = value;
    }
  }

  public ShipSlotCard Card
  {
    get
    {
      if (this.Ship == null || this.Ship.Card == null || this.Ship.Card.Slots == null)
        return (ShipSlotCard) null;
      return this.Ship.Card.Slots.Find((Predicate<ShipSlotCard>) (slot => (int) slot.SlotId == (int) this.ServerID));
    }
  }

  public IContainerID ContainerID
  {
    get
    {
      return this.containerID;
    }
  }

  public HangarShip Ship
  {
    get
    {
      return this.ship;
    }
  }

  public bool IsWeaponTypeSlot
  {
    get
    {
      if (this.Card == null)
        return false;
      return ShipSlot.IsWeaponSlot(this.Card.SystemType);
    }
  }

  public ShipSlot(HangarShip ship, ushort slotID)
  {
    this.ServerID = slotID;
    this.IsLoaded = new Flag();
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) ship.Card
    });
    this.ship = ship;
    this.containerID = (IContainerID) new ShipSlot.SlotContainerID(ship.ServerID, slotID);
  }

  public ShipItem GetByID(ushort ID)
  {
    return (ShipItem) this.System;
  }

  public void Read(BgoProtocolReader r)
  {
    this.System = (ShipSystem) ItemFactory.ReadItem(r);
    this.ConsumableGUID = r.ReadGUID();
    this.Inoperable = r.ReadBoolean();
    if (this.System != null)
    {
      this.System.Container = (IContainer) this;
      this.IsLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) this.System.Card
      });
      this.IsLoaded.AddHandler(new SignalHandler(this.UpdateAbility));
    }
    else
      this.Ability = (ShipAbility) null;
    this.IsLoaded.Set();
  }

  public static bool IsWeaponSlot(ShipSlotType slotType)
  {
    if (slotType != ShipSlotType.weapon && slotType != ShipSlotType.launcher && (slotType != ShipSlotType.gun && slotType != ShipSlotType.defensive_weapon))
      return slotType == ShipSlotType.special_weapon;
    return true;
  }

  private void UpdateAbility()
  {
    if (this.System.Card.AbilityCards.Length > 0)
    {
      if (this.Ability == null)
        this.Ability = new ShipAbility();
      this.Ability.InjectSlot(this);
    }
    else
      this.Ability = (ShipAbility) null;
    this.Ship._UpdateToolbar();
  }

  public void SelectConsumable(ItemCountable consumable)
  {
    if ((int) this.ConsumableGUID == (int) consumable.CardGUID)
      return;
    PlayerProtocol.GetProtocol().SelectConsumable(this.Ship.ServerID, consumable.CardGUID, this.ServerID);
  }

  public override string ToString()
  {
    return string.Format("ServerId: {0}, System: {1}, ConsumableGuid: {2}, Ability: {3}, Validation: {4}, GameStats: {5}, UpdatedStats: {6}, Inoperable: {7}, IsLoaded: {8}, Ship: {9}, ContainerId: {10}", (object) this.ServerID, (object) this.System, (object) this.ConsumableGUID, (object) this.Ability, (object) this.Validation, (object) this.GameStats, (object) this.UpdatedStats, (object) this.Inoperable, (object) this.IsLoaded, (object) this.Ship, (object) this.containerID);
  }

  public class SlotContainerID : IContainerID, IProtocolWrite
  {
    private ushort ShipID;
    private ushort SlotID;

    public SlotContainerID(ushort shipID, ushort slotID)
    {
      this.ShipID = shipID;
      this.SlotID = slotID;
    }

    void IProtocolWrite.Write(BgoProtocolWriter w)
    {
      w.Write((byte) 3);
      w.Write(this.ShipID);
      w.Write(this.SlotID);
    }

    public ContainerType GetContainerType()
    {
      return ContainerType.ShipSlot;
    }
  }
}
