﻿// Decompiled with JetBrains decompiler
// Type: GUICameraModeButtonsBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System.Collections.Generic;
using UnityEngine;

public abstract class GUICameraModeButtonsBase : GUIRadioGroupNew
{
  protected readonly GUIBlinker Blinker = new GUIBlinker();
  protected List<SpaceCameraBase.CameraMode> SupportedCameraModes;
  protected GUIButtonNew NoseCamButton;
  protected GUIButtonNew FreeCamButton;
  protected GUIButtonNew ChaseCamButton;
  protected GUIButtonNew TargetCamButton;
  private int mode;

  public bool IsBlinking
  {
    get
    {
      return this.Blinker.IsRendered;
    }
    set
    {
      this.Blinker.IsRendered = value;
    }
  }

  public SpaceCameraBase.CameraMode CameraMode
  {
    set
    {
      GUIButtonNew guiButtonNew1 = this.Find<GUIButtonNew>(value.ToString().ToLower());
      using (List<GUIPanel>.Enumerator enumerator = this.Children.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUIButtonNew guiButtonNew2 = (GUIButtonNew) enumerator.Current;
          if (guiButtonNew2 != guiButtonNew1)
            guiButtonNew2.IsPressed = false;
        }
      }
      guiButtonNew1.IsPressed = true;
    }
  }

  protected GUICameraModeButtonsBase()
  {
    this.LoadSupportedCameraModes();
    this.CreateAllCameraButtons();
    this.CreateActionHandlers();
  }

  protected abstract void LoadSupportedCameraModes();

  private void CreateActionHandlers()
  {
    this.handlers.Add(Action.ToggleCamera, (AnonymousDelegate) (() =>
    {
      if (++this.mode > this.SupportedCameraModes.Count - 1)
        this.mode = 0;
      FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) this.SupportedCameraModes[this.mode], true));
    }));
    this.handlers.Add(Action.TargetCamera, (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Target, true))));
    this.handlers.Add(Action.NoseCamera, (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Nose, true))));
    this.handlers.Add(Action.FreeCamera, (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Free, true))));
    this.handlers.Add(Action.ChaseCamera, (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Chase, true))));
  }

  private GUIButtonNew CreateCameraButton(string buttonName)
  {
    float2 float2 = new float2(28f, 28f);
    GUIButtonNew guiButtonNew = new GUIButtonNew(string.Empty, ResourceLoader.Load<Texture2D>("GUI/CameraToggles/camera_" + buttonName), ResourceLoader.Load<Texture2D>("GUI/CameraToggles/camera_" + buttonName + "_over"), ResourceLoader.Load<Texture2D>("GUI/CameraToggles/camera_" + buttonName + "_click"), (SmartRect) null);
    guiButtonNew.Size = float2;
    guiButtonNew.Name = buttonName;
    return guiButtonNew;
  }

  private void CreateAllCameraButtons()
  {
    this.NoseCamButton = this.CreateCameraButton("nose");
    this.FreeCamButton = this.CreateCameraButton("free");
    this.ChaseCamButton = this.CreateCameraButton("chase");
    this.TargetCamButton = this.CreateCameraButton("target");
    this.NoseCamButton.Handler = (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Nose, true)));
    this.ChaseCamButton.Handler = (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Chase, true)));
    this.FreeCamButton.Handler = (AnonymousDelegate) (() =>
    {
      FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Free, true));
      if (!this.IsBlinking)
        return;
      StoryProtocol.GetProtocol().TriggerControl(StoryProtocol.ControlType.Camera);
    });
    this.TargetCamButton.Handler = (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Target, true)));
    this.Gap = 3f;
    this.NoseCamButton.SetTooltip("%$bgo.option_buttons.nose_camera_mode% ", Action.NoseCamera);
    this.FreeCamButton.SetTooltip("%$bgo.option_buttons.free_camera_mode% ", Action.FreeCamera);
    this.ChaseCamButton.SetTooltip("%$bgo.option_buttons.chase_camera_mode% ", Action.ChaseCamera);
    this.TargetCamButton.SetTooltip("%$bgo.option_buttons.look_at_target_camera_mode% ", Action.TargetCamera);
    this.IsBlinking = false;
    this.FreeCamButton.AddPanel((GUIPanel) this.Blinker);
    this.IsRendered = true;
  }

  protected void AddCameraModeButtons()
  {
    using (List<SpaceCameraBase.CameraMode>.Enumerator enumerator = this.SupportedCameraModes.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        switch (enumerator.Current)
        {
          case SpaceCameraBase.CameraMode.Target:
            this.AddPanel((GUIPanel) this.TargetCamButton);
            continue;
          case SpaceCameraBase.CameraMode.Chase:
            this.AddPanel((GUIPanel) this.ChaseCamButton);
            continue;
          case SpaceCameraBase.CameraMode.Free:
            this.AddPanel((GUIPanel) this.FreeCamButton);
            continue;
          case SpaceCameraBase.CameraMode.Nose:
            this.AddPanel((GUIPanel) this.NoseCamButton);
            continue;
          default:
            continue;
        }
      }
    }
  }

  public void OnCameraModeChanged(SpaceCameraBase.CameraMode cameraMode)
  {
    if (!this.SupportedCameraModes.Contains(cameraMode))
      return;
    this.CameraMode = cameraMode;
  }
}
