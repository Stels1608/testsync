﻿// Decompiled with JetBrains decompiler
// Type: ACMVersion
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

internal class ACMVersion
{
  private static List<string> availableColors = new List<string>();
  private Dictionary<string, ACMColor> colors = new Dictionary<string, ACMColor>();

  public static List<string> GetColors()
  {
    List<string> stringList = new List<string>();
    using (List<string>.Enumerator enumerator = ACMVersion.availableColors.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        stringList.Add(current);
      }
    }
    return stringList;
  }

  public void Parse(List<string> materials)
  {
    ACMVersion.availableColors.Add("grey");
    ACMVersion.availableColors.Add("white");
    ACMVersion.availableColors.Add("black");
    ACMVersion.availableColors.Add("brown");
    ACMVersion.availableColors.Add("green");
    using (List<string>.Enumerator enumerator = ACMVersion.availableColors.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        this.colors[current] = new ACMColor();
        this.colors[current].Parse(this.FindMaterialsFromColor(current, materials));
      }
    }
  }

  private List<string> FindMaterialsFromColor(string col, List<string> materials)
  {
    List<string> stringList = new List<string>();
    using (List<string>.Enumerator enumerator = materials.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        if (current.Contains(col))
          stringList.Add(current);
      }
    }
    return stringList;
  }

  public string GetMaterial(string color, int number)
  {
    if (!this.colors.ContainsKey(color))
      return string.Empty;
    return this.colors[color].GetMaterial(number);
  }

  public int GetMaterialNumber(string material)
  {
    using (Dictionary<string, ACMColor>.Enumerator enumerator = this.colors.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int materialNumber = enumerator.Current.Value.GetMaterialNumber(material);
        if (materialNumber != -1)
          return materialNumber;
      }
    }
    return -1;
  }

  public int GetMaterialsCount(string color)
  {
    ACMColor acmColor;
    if (this.colors.TryGetValue(color, out acmColor))
      return acmColor.GetMaterialsCount();
    return 0;
  }
}
