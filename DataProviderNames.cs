﻿// Decompiled with JetBrains decompiler
// Type: DataProviderNames
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class DataProviderNames
{
  public const string SETTINGS = "SettingsDataProvider";
  public const string LOCALIZATION = "LocalizationDataProvider";
  public const string GUI = "GuiDataProvider";
  public const string SHOP = "ShopDataProvider";
  public const string TARGETSELECTION = "TargetSelectionProvider";
  public const string INGAME_LEVEL_STARTED_ACTIONS = "GameLevelLoadedActionsDataProvider";
  public const string CHARACTER_SERVICE = "CharacterServiceDataProvider";
  public const string LEADERBOARD = "TournamentDataProvider";
  public const string INPUT = "InputDataProvider";
}
