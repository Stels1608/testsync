﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DFocusPoint
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class SystemMap3DFocusPoint : SpacePosition
{
  public override bool IsInMapRange
  {
    get
    {
      return true;
    }
  }

  protected override sbyte IconIndex
  {
    get
    {
      return -1;
    }
  }

  public override SpaceEntityType SpaceEntityType
  {
    get
    {
      return SpaceEntityType.SectorMap3DFocusPoint;
    }
  }
}
