﻿// Decompiled with JetBrains decompiler
// Type: InputListener
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public interface InputListener
{
  bool HandleMouseInput { get; }

  bool HandleKeyboardInput { get; }

  bool HandleJoystickAxesInput { get; }

  bool OnMouseDown(float2 mousePosition, KeyCode mouseKey);

  bool OnMouseUp(float2 mousePosition, KeyCode mouseKey);

  void OnMouseMove(float2 mousePosition);

  bool OnMouseScrollDown();

  bool OnMouseScrollUp();

  bool OnKeyDown(KeyCode keyboardKey, Action action);

  bool OnKeyUp(KeyCode keyboardKey, Action action);

  bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted);
}
