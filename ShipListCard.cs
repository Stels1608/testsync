﻿// Decompiled with JetBrains decompiler
// Type: ShipListCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class ShipListCard : Card
{
  public ShipCard[] ShipCards;
  public ShipCard[] UpgradeShipCards;
  public List<ShipCard> VariantShipCards;
  private Flag childCardsLoaded;
  private Flag variantsHandled;

  public ShipListCard(uint cardGUID)
    : base(cardGUID)
  {
    this.VariantShipCards = new List<ShipCard>();
    this.childCardsLoaded = new Flag();
    this.variantsHandled = new Flag();
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.variantsHandled
    });
  }

  public ShipCard Get(byte tier, ShipRoleDeprecated roleDeprecated)
  {
    if (this.ShipCards == null)
      return (ShipCard) null;
    foreach (ShipCard shipCard in this.ShipCards)
    {
      if ((int) shipCard.Tier == (int) tier && shipCard.ShipRoleDeprecated == roleDeprecated)
        return shipCard;
    }
    return (ShipCard) null;
  }

  public ShipCard Get(byte hangarId)
  {
    if (this.ShipCards == null)
      return (ShipCard) null;
    foreach (ShipCard shipCard in this.ShipCards)
    {
      if ((int) shipCard.HangarID == (int) hangarId)
        return shipCard;
    }
    return (ShipCard) null;
  }

  public ShipCard GetUpgraded(byte tier, ShipRoleDeprecated roleDeprecated)
  {
    if (this.ShipCards == null)
      return (ShipCard) null;
    foreach (ShipCard shipCard in this.ShipCards)
    {
      if ((int) shipCard.Tier == (int) tier && shipCard.ShipRoleDeprecated == roleDeprecated)
      {
        if ((int) shipCard.MaxLevel == 1)
          return shipCard;
        if ((int) shipCard.MaxLevel == 2)
          return shipCard.NextCard;
      }
    }
    return (ShipCard) null;
  }

  public ShipCard GetShipCard(byte tier, ShipRoleDeprecated roleDeprecated)
  {
    foreach (ShipCard shipCard in this.ShipCards)
    {
      if ((int) shipCard.Tier == (int) tier && shipCard.ShipRoleDeprecated == roleDeprecated)
        return shipCard;
    }
    return (ShipCard) null;
  }

  public ShipCard GetShipCard(byte hangarId)
  {
    foreach (ShipCard shipCard in this.ShipCards)
    {
      if ((int) shipCard.HangarID == (int) hangarId)
        return shipCard;
    }
    return (ShipCard) null;
  }

  public ShipCard GetVariantCard(byte hangarId)
  {
    using (List<ShipCard>.Enumerator enumerator = this.VariantShipCards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipCard current = enumerator.Current;
        if ((int) current.HangarID == (int) hangarId)
          return current;
      }
    }
    return (ShipCard) null;
  }

  public void AddAsVariant(ShipCard card)
  {
    this.VariantShipCards.Add(card);
  }

  private void MoveVariants()
  {
    uint num1 = 0;
    for (uint index = 0; (long) index < (long) this.ShipCards.Length; ++index)
    {
      if (this.ShipCards[(IntPtr) index].HasParent)
        ++num1;
    }
    if (num1 > 0U)
    {
      ShipCard[] shipCardArray1 = new ShipCard[(long) this.ShipCards.Length - (long) num1];
      ShipCard[] shipCardArray2 = new ShipCard[(long) this.ShipCards.Length - (long) num1];
      uint num2 = 0;
      for (uint index = 0; (long) index < (long) this.ShipCards.Length; ++index)
      {
        if (this.ShipCards[(IntPtr) index].HasParent)
        {
          this.AddAsVariant(this.ShipCards[(IntPtr) index]);
        }
        else
        {
          shipCardArray1[(IntPtr) num2] = this.ShipCards[(IntPtr) index];
          shipCardArray2[(IntPtr) num2++] = this.UpgradeShipCards[(IntPtr) index];
        }
      }
      this.ShipCards = shipCardArray1;
      this.UpgradeShipCards = shipCardArray2;
    }
    this.variantsHandled.Set();
  }

  public override void Read(BgoProtocolReader r)
  {
    int length = r.ReadLength();
    this.ShipCards = new ShipCard[length];
    this.UpgradeShipCards = new ShipCard[length];
    for (int index = 0; index < length; ++index)
    {
      this.ShipCards[index] = (ShipCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.Ship);
      this.UpgradeShipCards[index] = (ShipCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.Ship);
      this.childCardsLoaded.Depend((ILoadable) this.ShipCards[index], (ILoadable) this.UpgradeShipCards[index]);
    }
    this.childCardsLoaded.AddHandler(new SignalHandler(this.MoveVariants));
    this.childCardsLoaded.Set();
  }

  public ShipCard GetVariantShipCard(ShipCard baseShip, uint index)
  {
    if (!baseShip.HasVariant(index))
      return (ShipCard) null;
    for (int index1 = 0; index1 < this.VariantShipCards.Count; ++index1)
    {
      if (this.VariantShipCards[index1] != null && (int) this.VariantShipCards[index1].HangarID == (int) baseShip.VariantHangarIDs[(int) index])
        return this.VariantShipCards[index1];
    }
    return (ShipCard) null;
  }
}
