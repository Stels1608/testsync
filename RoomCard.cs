﻿// Decompiled with JetBrains decompiler
// Type: RoomCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RoomCard : Card
{
  public List<RoomDoor> doors = new List<RoomDoor>();
  public List<RoomNPC> NPCs = new List<RoomNPC>();
  public string music;

  public RoomCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    int num1 = (int) r.ReadUInt16();
    for (int index = 0; index < num1; ++index)
      this.doors.Add(r.ReadDesc<RoomDoor>());
    int num2 = (int) r.ReadUInt16();
    for (int index = 0; index < num2; ++index)
      this.NPCs.Add(r.ReadDesc<RoomNPC>());
    this.music = r.ReadString();
  }
}
