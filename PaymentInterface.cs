﻿// Decompiled with JetBrains decompiler
// Type: PaymentInterface
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class PaymentInterface : MonoBehaviour
{
  private Dictionary<string, PaymentInterface.PaymentCallback> m_callbackDictionary = new Dictionary<string, PaymentInterface.PaymentCallback>();

  public void OpenPaymentWindow()
  {
    ExternalApi.EvalJs("window.createPaymentSession();");
  }

  public void OpenPaymentWindow(string packageName)
  {
    ExternalApi.EvalJs("window.createSpecialPaymentSession(\"" + packageName + "\");");
  }

  public void OpenPaymentWindow(string packageName, uint itemGroup)
  {
    FacadeFactory.GetInstance().SendMessage(Message.SetFullscreen, (object) false);
    ExternalApi.EvalJs("window.createSpecialPaymentSession(\"" + packageName + "\"," + (object) itemGroup + ");");
  }

  public void OpenPaymentCampaignWindow(uint campaignGui)
  {
    FacadeFactory.GetInstance().SendMessage(Message.SetFullscreen, (object) false);
    ExternalApi.EvalJs("window.createCampaignSession(" + (object) campaignGui + ");");
  }

  public void QueryPackagePrice(string packageName, PaymentInterface.PaymentCallback callback)
  {
    ExternalApi.EvalJs("window.getPaymentItemPrice(\"" + packageName + "\");");
    PaymentInterface.PaymentCallback paymentCallback1;
    if (!this.m_callbackDictionary.TryGetValue(packageName, out paymentCallback1))
    {
      this.m_callbackDictionary.Add(packageName, callback);
    }
    else
    {
      PaymentInterface.PaymentCallback paymentCallback2 = paymentCallback1 + callback;
    }
  }

  public void ReplyPackagePrice(string packageNameAndPrice)
  {
    string[] strArray = packageNameAndPrice.Split(' ');
    string key = strArray[0];
    string str = strArray[1];
    PaymentInterface.PaymentCallback paymentCallback;
    if (!this.m_callbackDictionary.TryGetValue(key, out paymentCallback))
    {
      Debug.Log((object) ("***ReplyPackagePrice error, no callback found for packageName(" + key + ")***"));
    }
    else
    {
      if (paymentCallback == null)
        return;
      paymentCallback((object) str);
    }
  }

  public void UnregisterPackagePrice(string packageName, PaymentInterface.PaymentCallback callback)
  {
    PaymentInterface.PaymentCallback paymentCallback;
    if (!this.m_callbackDictionary.TryGetValue(packageName, out paymentCallback) || paymentCallback != null)
      return;
    this.m_callbackDictionary.Remove(packageName);
  }

  public delegate void PaymentCallback(object data);
}
