﻿// Decompiled with JetBrains decompiler
// Type: ChangeSettingAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSettingAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    ChangeSettingMessage changeSettingMessage = (ChangeSettingMessage) message;
    UserSetting setting = changeSettingMessage.Setting;
    object data = changeSettingMessage.Data;
    Message messageId = Message.None;
    switch (setting)
    {
      case UserSetting.CombatGui:
        messageId = Message.SettingChangedCombatGui;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowTutorial:
      case UserSetting.InvertedVertical:
      case UserSetting.Fullscreen:
      case UserSetting.InventoryPanelPosition:
      case UserSetting.Layout:
      case UserSetting.ShowChangeFaction:
      case UserSetting.Fullframe:
      case UserSetting.HudIndicatorShowWingNames:
      case UserSetting.ShowEnemyIndication:
      case UserSetting.ShowFriendIndication:
      case UserSetting.HudIndicatorShowMissionArrow:
      case UserSetting.HudIndicatorShowTitles:
      case UserSetting.SystemMap3DTransitionMode:
      case UserSetting.SystemMap3DCameraView:
      case UserSetting.SystemMap3DShowAsteroids:
      case UserSetting.SystemMap3DShowDynamicMissions:
      case UserSetting.SystemMap3DFormAsteroidGroups:
      case UserSetting.HudIndicatorTextSize:
      case UserSetting.HudIndicatorShowShipTierIcon:
      case UserSetting.HudIndicatorSelectionCrosshair:
      case UserSetting.HudIndicatorHealthBar:
        SettingsDataProvider settingsDataProvider = (SettingsDataProvider) this.OwnerFacade.FetchDataProvider("SettingsDataProvider");
        settingsDataProvider.CurrentSettings.Set(setting, data);
        using (List<ISettingsReceiver>.Enumerator enumerator = settingsDataProvider.SettingsReceivers.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            ISettingsReceiver current = enumerator.Current;
            if (current == null)
              Debug.LogWarning((object) "A settings receiver wasn't unregistered!");
            else
              current.ReceiveSetting(setting, data);
          }
        }
        if (messageId != Message.None)
          this.SendMessage(messageId, data);
        if (!changeSettingMessage.Save)
          break;
        this.OwnerFacade.SendMessage(Message.SaveSettings);
        break;
      case UserSetting.CameraMode:
        messageId = Message.SettingChangedCameraMode;
        goto case UserSetting.ShowTutorial;
      case UserSetting.MusicVolume:
        messageId = Message.SettingChangedMusicVolume;
        goto case UserSetting.ShowTutorial;
      case UserSetting.SoundVolume:
        messageId = Message.SettingChangedSoundVolume;
        goto case UserSetting.ShowTutorial;
      case UserSetting.StatsIndication:
        bool flag = (bool) data;
        GUISlot.ShowStats = flag;
        GUISlotManager.ShowStats = flag;
        goto case UserSetting.ShowTutorial;
      case UserSetting.HudIndicatorShowShipNames:
        TargetSelector.ShowShipNames = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.LootPanelPosition:
        messageId = Message.SettingChangedLootPanelPosition;
        goto case UserSetting.ShowTutorial;
      case UserSetting.CompletedTutorials:
        messageId = Message.SettingChangedCompletedTutorials;
        goto case UserSetting.ShowTutorial;
      case UserSetting.GraphicsQuality:
        Game.GraphicsQuality = (GraphicsQuality) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.AssignmentsCollapsed:
        messageId = Message.SettingChangedAssignmentsCollapsed;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowStarDust:
        messageId = Message.SettingChangedStarDust;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowStarFog:
        messageId = Message.SettingChangedStarFog;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ViewDistance:
        LODListener.ViewDistance = (float) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowGlowEffect:
        messageId = Message.SettingChangedGlowEffect;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ChatShowPrefix:
        messageId = Message.SettingChangedChatShowPrefix;
        goto case UserSetting.ShowTutorial;
      case UserSetting.MouseWheelBinding:
        SpaceCameraBase.MouseWheel = (MouseWheelBinding) data;
        ShipControlsBase.MouseWheelBinding = (MouseWheelBinding) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ChatViewLocal:
        messageId = Message.SettingChangedChatViewLocal;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ChatViewGlobal:
        messageId = Message.SettingChangedChatViewGlobal;
        goto case UserSetting.ShowTutorial;
      case UserSetting.AutoLoot:
        messageId = Message.SettingChangedAutoLoot;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowPopups:
        messageId = Message.SettingChangedShowPopups;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowOutpostMessages:
        OnScreenNotification.ShowOutpostMessages = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowHeavyFightingMessages:
        OnScreenNotification.ShowHeavyFightingMessages = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowAugmentMessages:
        OnScreenNotification.ShowAugmentMessages = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowMiningShipMessages:
        OnScreenNotification.ShowMiningShipMessages = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowExperienceMessages:
        OnScreenNotification.ShowExperienceMessages = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.AdvancedFlightControls:
        this.AdjustAdvancedFlightControls((bool) data);
        goto case UserSetting.ShowTutorial;
      case UserSetting.AntiAliasing:
        QualitySettings.antiAliasing = (int) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.VSync:
        QualitySettings.vSyncCount = !(bool) data ? 0 : 1;
        goto case UserSetting.ShowTutorial;
      case UserSetting.UseProceduralTextures:
        messageId = Message.SettingChangedUseProceduralTextures;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowFpsAndPing:
        messageId = Message.SettingChangedShowFps;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowBulletImpactFx:
        AbstractGun.ShowBulletImpactFx = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.CombatText:
        messageId = Message.SettingChangedCombatText;
        goto case UserSetting.ShowTutorial;
      case UserSetting.FlakFieldDensity:
        Flak.ExplosionEmissionRateMultiplier = (float) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.DeadZoneMouse:
        ShipControlsBase.DeadZoneMouseRadiusFactor = (float) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.AutomaticAmmoReload:
        GUISmallPaperdoll.AutomaticAmmoChange = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowWofConfirmation:
        messageId = Message.WofShowConfirmation;
        goto case UserSetting.ShowTutorial;
      case UserSetting.DeadZoneJoystick:
        JoystickSetup.DeadZone = (float) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.SensitivityJoystick:
        JoystickSetup.Sensitivity = (float) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.CameraZoom:
        messageId = Message.SettingChangedCameraZoom;
        goto case UserSetting.ShowTutorial;
      case UserSetting.JoystickGamepadEnabled:
        messageId = Message.ToggleJoystickGamepadEnabled;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowXbox360Buttons:
        JoystickSetup.ShowXbox360Buttons = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.HighResModels:
        messageId = Message.SettingChangedHighResModels;
        goto case UserSetting.ShowTutorial;
      case UserSetting.HighResTextures:
        QualitySettings.masterTextureLimit = !(bool) data ? 1 : 0;
        goto case UserSetting.ShowTutorial;
      case UserSetting.HighQualityParticles:
        if ((Object) (GameLevel.Instance as RoomLevel) != (Object) null)
          Object.FindObjectOfType<RoomSettings>().ShowParticleFog = (bool) data;
        CometLOD.HighQualityParticles = (bool) data;
        foreach (CometLOD cometLod in Object.FindObjectsOfType<CometLOD>())
          cometLod.ToggleHighQualityParticles((bool) data);
        goto case UserSetting.ShowTutorial;
      case UserSetting.AnisotropicFiltering:
        QualitySettings.anisotropicFiltering = !(bool) data ? AnisotropicFiltering.Enable : AnisotropicFiltering.ForceEnable;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowCutscenes:
        messageId = Message.SettingChangedShowCutscenes;
        goto case UserSetting.ShowTutorial;
      case UserSetting.MuteSound:
        messageId = Message.SettingChangedMuteSound;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowDamageOverlay:
        DamageOverlay.ShowDamageOverlaySetting = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowShipSkins:
        messageId = Message.SettingChangedShowShipSkins;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowWeaponModules:
        messageId = Message.SettingChangedShowModules;
        goto case UserSetting.ShowTutorial;
      case UserSetting.HudIndicatorMinimizeDistance:
        HudIndicatorInfo.MiniModeRangeOption = (float) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.HudIndicatorShowTargetNames:
        TargetSelector.ShowPlayerNames = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.HudIndicatorColorScheme:
        HudIndicatorColorScheme.SetMode((HudIndicatorColorSchemeMode) data);
        goto case UserSetting.ShowTutorial;
      case UserSetting.HudIndicatorBracketResizing:
        HudIndicatorBaseUgui.ResizeBracketSetting = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.HudIndicatorDescriptionDisplayDistance:
        HudIndicatorInfo.TopTextRenderRangeOption = (float) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowAssignmentMessages:
        OnScreenNotification.ShowAssignmentMessages = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.ShowXpBar:
        ExperienceBar.ShowXpBar = (bool) data;
        goto case UserSetting.ShowTutorial;
      case UserSetting.FramerateCapping:
        int num = !(bool) data ? -1 : 60;
        Application.targetFrameRate = num;
        Debug.Log((object) ("Target Framerate set to: " + (num != -1 ? num.ToString() : "Unbound")));
        goto case UserSetting.ShowTutorial;
      default:
        Debug.LogWarning((object) ("Non-Valid value was given to Changed Option " + (object) setting + " Value: " + data));
        goto case UserSetting.ShowTutorial;
    }
  }

  private void AdjustAdvancedFlightControls(bool on)
  {
    ShipControlsBase.AdvancedFlightControls = on;
    SpaceCameraBase.AdvancedFlightControls = on;
  }
}
