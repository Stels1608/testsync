﻿// Decompiled with JetBrains decompiler
// Type: WofBonusMapLayoutGamma
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class WofBonusMapLayoutGamma : WofBonusMapLayout
{
  public WofBonusMapLayoutGamma()
  {
    this.bonusMapId = 3;
    this.maxParts = 42;
    this.mapPositions = new Dictionary<ushort, Vector2>();
    this.mapPositions.Add((ushort) 1, new Vector2(9f, -2f));
    this.mapPositions.Add((ushort) 2, new Vector2(6f, -4f));
    this.mapPositions.Add((ushort) 3, new Vector2(6f, -5f));
    this.mapPositions.Add((ushort) 4, new Vector2(11f, -5f));
    this.mapPositions.Add((ushort) 5, new Vector2(8f, -4f));
    this.mapPositions.Add((ushort) 6, new Vector2(5f, -2f));
    this.mapPositions.Add((ushort) 7, new Vector2(14f, -5f));
    this.mapPositions.Add((ushort) 8, new Vector2(3f, -3f));
    this.mapPositions.Add((ushort) 9, new Vector2(1f, -4f));
    this.mapPositions.Add((ushort) 10, new Vector2(8f, -5f));
    this.mapPositions.Add((ushort) 11, new Vector2(15f, -1f));
    this.mapPositions.Add((ushort) 12, new Vector2(5f, -5f));
    this.mapPositions.Add((ushort) 13, new Vector2(6f, -1f));
    this.mapPositions.Add((ushort) 14, new Vector2(1f, -6f));
    this.mapPositions.Add((ushort) 15, new Vector2(0.0f, -2f));
    this.mapPositions.Add((ushort) 16, new Vector2(12f, -1f));
    this.mapPositions.Add((ushort) 17, new Vector2(7f, -3f));
    this.mapPositions.Add((ushort) 18, new Vector2(5f, -4f));
    this.mapPositions.Add((ushort) 19, new Vector2(15f, -3f));
    this.mapPositions.Add((ushort) 20, new Vector2(2f, -3f));
    this.mapPositions.Add((ushort) 21, new Vector2(7f, -6f));
    this.mapPositions.Add((ushort) 22, new Vector2(7f, -1f));
    this.mapPositions.Add((ushort) 23, new Vector2(7f, -4f));
    this.mapPositions.Add((ushort) 24, new Vector2(11f, -2f));
    this.mapPositions.Add((ushort) 25, new Vector2(14f, -2f));
    this.mapPositions.Add((ushort) 26, new Vector2(4f, -2f));
    this.mapPositions.Add((ushort) 27, new Vector2(12f, -6f));
    this.mapPositions.Add((ushort) 28, new Vector2(8f, -1f));
    this.mapPositions.Add((ushort) 29, new Vector2(13f, -5f));
    this.mapPositions.Add((ushort) 30, new Vector2(13f, -6f));
    this.mapPositions.Add((ushort) 31, new Vector2(14f, -4f));
    this.mapPositions.Add((ushort) 32, new Vector2(5f, -1f));
    this.mapPositions.Add((ushort) 33, new Vector2(15f, -6f));
    this.mapPositions.Add((ushort) 34, new Vector2(15f, -4f));
    this.mapPositions.Add((ushort) 35, new Vector2(10f, -3f));
    this.mapPositions.Add((ushort) 36, new Vector2(4f, -3f));
    this.mapPositions.Add((ushort) 37, new Vector2(0.0f, -5f));
    this.mapPositions.Add((ushort) 38, new Vector2(9f, -3f));
    this.mapPositions.Add((ushort) 39, new Vector2(3f, -6f));
    this.mapPositions.Add((ushort) 40, new Vector2(14f, -6f));
    this.mapPositions.Add((ushort) 41, new Vector2(9f, -5f));
    this.mapPositions.Add((ushort) 42, new Vector2(2f, -4f));
    this.difficulty = WofBonusMapDifficulty.Expert;
    this.requiredLevel = 20;
    this.mapName = Tools.ParseMessage("%$bgo.wof_bonusmap." + (object) this.bonusMapId + "%");
  }
}
