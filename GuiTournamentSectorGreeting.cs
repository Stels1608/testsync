﻿// Decompiled with JetBrains decompiler
// Type: GuiTournamentSectorGreeting
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GuiTournamentSectorGreeting : GuiDialog
{
  private const string singleSpace = "\n";
  private const string doubleSpace = "\n\n";
  private const string bulletPoint = "\t-\t";
  public static GUICard GuiCard;
  private readonly bool isStarting;
  private bool sentSeenMessage;
  private readonly GuiButton m_okayButton;

  public uint GreetingMessageID
  {
    get
    {
      return 10;
    }
  }

  public uint EndingMessageID
  {
    get
    {
      return 11;
    }
  }

  public GuiTournamentSectorGreeting(TournamentType tournamentType, bool isStarting)
  {
    ShopWindow shopWindow = Game.GUIManager.Find<ShopWindow>();
    if (shopWindow != null && shopWindow.ConfirmWindow != null)
      shopWindow.ConfirmWindow.Hide();
    this.isStarting = isStarting;
    GuiDialogPopupManager.ShowDialog((GuiPanel) this, true);
    Loaders.Load((GuiPanel) this, "GUI/Tournaments/SectorGreetingLayout");
    switch (tournamentType)
    {
      case TournamentType.Strike:
        this.BackgroundImage.TexturePath = "GUI/Events/Tournament/tournament_announcement_strike";
        break;
      case TournamentType.Escort:
        this.BackgroundImage.TexturePath = "GUI/Events/Tournament/tournament_announcement_escort";
        break;
      default:
        this.BackgroundImage.TexturePath = "GUI/Events/Tournament/tournament_announcement_strike";
        break;
    }
    this.CloseButton.Position = new Vector2(4f, -2f);
    this.RemoveChild((GuiElementBase) this.CloseButton);
    this.AddChild((GuiElementBase) this.CloseButton);
    GuiTournamentSectorGreeting tournamentSectorGreeting = this;
    AnonymousDelegate anonymousDelegate = tournamentSectorGreeting.OnClose + new AnonymousDelegate(this.CloseDialog);
    tournamentSectorGreeting.OnClose = anonymousDelegate;
    this.m_okayButton = this.Find<GuiButton>("OkayButton");
    this.m_okayButton.SizeX = 125f;
    this.m_okayButton.Pressed += new AnonymousDelegate(this.CloseDialog);
    this.Find<GuiLabel>("titleText").Text = BsgoLocalization.Get("%$bgo.tournament_greeting.title%", (object) GuiTournamentSectorGreeting.GuiCard.Name);
    this.Find<GuiScrollPanel>().FindScrollChild<GuiLabel>("bodyText").Text = BsgoLocalization.Get(!isStarting ? GuiTournamentSectorGreeting.GuiCard.ShortDescription : GuiTournamentSectorGreeting.GuiCard.Description, (object) "\t-\t", (object) "\n", (object) "\n\n");
    this.PositionCenter = new Vector2((float) (Screen.width / 2), (float) (Screen.height / 2));
  }

  private void CloseDialog()
  {
    this.IsRendered = false;
    GuiDialogPopupManager.CloseDialog();
    if (this.sentSeenMessage)
      return;
    this.sentSeenMessage = true;
    PlayerProtocol.GetProtocol().SetPopupToSeen(!this.isStarting ? this.EndingMessageID : this.GreetingMessageID);
  }

  public static void ShowGreeting(TournamentType tournamentType)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiTournamentSectorGreeting.\u003CShowGreeting\u003Ec__AnonStoreyC5 greetingCAnonStoreyC5 = new GuiTournamentSectorGreeting.\u003CShowGreeting\u003Ec__AnonStoreyC5();
    // ISSUE: reference to a compiler-generated field
    greetingCAnonStoreyC5.tournamentType = tournamentType;
    if (GuiTournamentSectorGreeting.GuiCard == null)
      return;
    // ISSUE: reference to a compiler-generated method
    GuiTournamentSectorGreeting.GuiCard.IsLoaded.AddHandler(new SignalHandler(greetingCAnonStoreyC5.\u003C\u003Em__1CB));
  }

  public static void ShowEnding(TournamentType tournamentType)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiTournamentSectorGreeting.\u003CShowEnding\u003Ec__AnonStoreyC6 endingCAnonStoreyC6 = new GuiTournamentSectorGreeting.\u003CShowEnding\u003Ec__AnonStoreyC6();
    // ISSUE: reference to a compiler-generated field
    endingCAnonStoreyC6.tournamentType = tournamentType;
    if (GuiTournamentSectorGreeting.GuiCard == null)
      return;
    // ISSUE: reference to a compiler-generated method
    GuiTournamentSectorGreeting.GuiCard.IsLoaded.AddHandler(new SignalHandler(endingCAnonStoreyC6.\u003C\u003Em__1CC));
  }
}
