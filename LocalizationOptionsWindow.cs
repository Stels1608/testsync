﻿// Decompiled with JetBrains decompiler
// Type: LocalizationOptionsWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class LocalizationOptionsWindow : MonoBehaviour
{
  [SerializeField]
  private UILabel headline;

  private void Start()
  {
    Log.Add("START LOCA OPTIONS");
    if (!((Object) null != (Object) this.headline))
      return;
    this.headline.text = Tools.ParseMessage(string.Empty);
  }
}
