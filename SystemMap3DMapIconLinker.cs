﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DMapIconLinker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Paths;
using UnityEngine;

public class SystemMap3DMapIconLinker : MonoBehaviour
{
  [SerializeField]
  private UnityEngine.Sprite mapObjectsNpcsMulti;
  [SerializeField]
  private UnityEngine.Sprite[] mapObjectsNpcs;
  [SerializeField]
  private UnityEngine.Sprite[] mapObjectsPlayers;
  [SerializeField]
  private UnityEngine.Sprite[] _mapObjectsNpcsBrackets;
  [SerializeField]
  private UnityEngine.Sprite[] _mapObjectsPlayersBrackets;
  [SerializeField]
  private UnityEngine.Sprite bracketSpriteHover;
  [SerializeField]
  private UnityEngine.Sprite bracketSpriteSelected;
  [SerializeField]
  private UnityEngine.Sprite dradisCircle;
  [SerializeField]
  private UnityEngine.Sprite partyMarker;
  [SerializeField]
  private UnityEngine.Sprite sectorEventIcon;
  [SerializeField]
  private UnityEngine.Sprite waypointIcon;
  private static SystemMap3DMapIconLinker _instance;

  public static SystemMap3DMapIconLinker Instance
  {
    get
    {
      return SystemMap3DMapIconLinker._instance ?? (SystemMap3DMapIconLinker._instance = Resources.Load<GameObject>(Ui.Prefabs + "/SystemMap3D/SystemMap3DMapIconLinker").GetComponent<SystemMap3DMapIconLinker>());
    }
  }

  public UnityEngine.Sprite[] MapObjectsNPCs
  {
    get
    {
      return this.mapObjectsNpcs;
    }
  }

  public UnityEngine.Sprite[] MapObjectsPlayers
  {
    get
    {
      return this.mapObjectsPlayers;
    }
  }

  public UnityEngine.Sprite[] MapObjectsNPCsBrackets
  {
    get
    {
      return this._mapObjectsNpcsBrackets;
    }
  }

  public UnityEngine.Sprite[] MapObjectsPlayersBrackets
  {
    get
    {
      return this._mapObjectsPlayersBrackets;
    }
  }

  public UnityEngine.Sprite BracketSpriteHover
  {
    get
    {
      return this.bracketSpriteHover;
    }
  }

  public UnityEngine.Sprite BracketSpriteSelected
  {
    get
    {
      return this.bracketSpriteSelected;
    }
  }

  public UnityEngine.Sprite DradisCircle
  {
    get
    {
      return this.dradisCircle;
    }
  }

  public UnityEngine.Sprite PartyMarker
  {
    get
    {
      return this.partyMarker;
    }
  }

  public UnityEngine.Sprite SectorEventIcon
  {
    get
    {
      return this.sectorEventIcon;
    }
  }

  public UnityEngine.Sprite WaypointIcon
  {
    get
    {
      return this.waypointIcon;
    }
  }
}
