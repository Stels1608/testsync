﻿// Decompiled with JetBrains decompiler
// Type: FlightSimPlayerShip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FlightSimPlayerShip : PlayerShip
{
  public FlightSimPlayerShip(uint objectID)
    : base(objectID)
  {
    this.InjectManeuverController((ManeuverController) new PlayerManeuverController((SpaceObject) null, (MovementCard) null));
  }

  public void InjectModel(GameObject model)
  {
    this.model = model;
    model.transform.parent = this.Root;
  }

  public void InjectMovementUpdateDeltaTime(float deltaTime)
  {
    (this.movementController as IPlayerMovementController).MovementUpdateDeltaTime = (double) deltaTime;
  }

  private void InjectManeuverController(ManeuverController maneuverController)
  {
    this.movementController = (IMovementController) maneuverController;
  }
}
