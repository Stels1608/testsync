﻿// Decompiled with JetBrains decompiler
// Type: SectorEventTask
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public abstract class SectorEventTask
{
  public byte Index { get; private set; }

  public SectorEventState TaskState { get; private set; }

  public abstract SectorEventTaskType TaskType { get; }

  public SectorEventTaskSubType SubType { get; private set; }

  public SectorEventTask(byte index, SectorEventTaskSubType subType, SectorEventState taskState)
  {
    this.Index = index;
    this.TaskState = taskState;
    this.SubType = subType;
  }
}
