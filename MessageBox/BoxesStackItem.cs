﻿// Decompiled with JetBrains decompiler
// Type: MessageBox.BoxesStackItem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace MessageBox
{
  public struct BoxesStackItem
  {
    public MessageBoxType msgBoxType;
    public string titleText;
    public bool showOk;
    public string okButtonName;
    public string cancelButtonName;
    public string mainText;
    public string extText;
    public HelpScreenType helpType;
    public uint TimeToLive;
    public Card card;
    public string extraImage;
    public System.Action<MessageBoxActionType> onPressed;

    public override string ToString()
    {
      return string.Format("MsgBoxType: {0}, TitleText: {1}, OkButtonName: {2}, CancelButtonName: {3}, MainText: {4}, ExtText: {5}, HelpType: {6}, TimeToLive: {7}, Card: {8}, ExtraImage: {9}, OnPressed: {10}", (object) this.msgBoxType, (object) this.titleText, (object) this.okButtonName, (object) this.cancelButtonName, (object) this.mainText, (object) this.extText, (object) this.helpType, (object) this.TimeToLive, (object) this.card, (object) this.extraImage, (object) this.onPressed);
    }
  }
}
