﻿// Decompiled with JetBrains decompiler
// Type: Curves
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class Curves
{
  public static List<T> DouglasPeucker<T>(List<T> knots, Curves.DistanceDelegate<T> distance, float epsilon)
  {
    T from = knots[0];
    T to = knots[knots.Count - 1];
    float num1 = 0.0f;
    int index1 = 0;
    for (int index2 = 1; index2 < knots.Count - 1; ++index2)
    {
      float num2 = distance(knots[index2], from, to);
      if ((double) num2 > (double) num1)
      {
        index1 = index2;
        num1 = num2;
      }
    }
    if ((double) num1 > (double) epsilon)
    {
      List<T> objList = Curves.DouglasPeucker<T>(knots.GetRange(0, index1 + 1), distance, epsilon);
      objList.RemoveAt(objList.Count - 1);
      objList.AddRange((IEnumerable<T>) Curves.DouglasPeucker<T>(knots.GetRange(index1, knots.Count - index1), distance, epsilon));
      return objList;
    }
    return new List<T>() { from, to };
  }

  public class Curve
  {
    public List<Curves.Curve.Knot> knots = new List<Curves.Curve.Knot>();

    public Vector3 Get(float time)
    {
      time = Mathf.Clamp01(time);
      int index = 0;
      while (index < this.knots.Count && (double) this.knots[index].t < (double) time)
        ++index;
      if (index < 1)
        index = 1;
      if (index >= this.knots.Count)
        index = this.knots.Count - 1;
      return Vector3.Lerp(this.knots[index - 1].point, this.knots[index].point, Mathf.Clamp01((float) (((double) time - (double) this.knots[index - 1].t) / ((double) this.knots[index].t - (double) this.knots[index - 1].t))));
    }

    public void AddKnot(float t, Vector3 point)
    {
      this.knots.Add(new Curves.Curve.Knot(t, point));
    }

    public void Mirror()
    {
      using (List<Curves.Curve.Knot>.Enumerator enumerator = this.knots.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Curves.Curve.Knot current = enumerator.Current;
          current.point.y = -current.point.y;
          current.point.z = -current.point.z;
        }
      }
    }

    public void Normalize(Vector3 norm)
    {
      Vector3 vector3 = Algorithm3D.NormalizeEuler(norm, this.knots[this.knots.Count - 1].point) - this.knots[this.knots.Count - 1].point;
      using (List<Curves.Curve.Knot>.Enumerator enumerator = this.knots.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Curves.Curve.Knot current = enumerator.Current;
          current.point += vector3 * current.t;
        }
      }
    }

    public void Scale(Vector3 scale)
    {
      using (List<Curves.Curve.Knot>.Enumerator enumerator = this.knots.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Curves.Curve.Knot current = enumerator.Current;
          current.point = Vector3.Scale(current.point, scale);
        }
      }
    }

    public List<Vector3> GetTickKnots(int tickCount)
    {
      List<Vector3> vector3List = new List<Vector3>();
      for (int index = 0; index <= tickCount; ++index)
        vector3List.Add(this.Get((float) index / (float) tickCount));
      return vector3List;
    }

    public List<Quaternion> GetTickRotations(int tickCount)
    {
      List<Quaternion> quaternionList = new List<Quaternion>();
      List<Vector3> tickKnots = this.GetTickKnots(tickCount);
      for (int index = 0; index < tickCount; ++index)
      {
        Vector3 euler1 = tickKnots[index];
        Vector3 euler2 = tickKnots[index + 1];
        quaternionList.Add(Quaternion.Inverse(Quaternion.Euler(euler1)) * Quaternion.Euler(euler2));
      }
      return quaternionList;
    }

    public class Knot
    {
      public float t;
      public Vector3 point;

      public Knot(float t, Vector3 point)
      {
        this.t = t;
        this.point = point;
      }
    }
  }

  public delegate float DistanceDelegate<T>(T knot, T from, T to);
}
