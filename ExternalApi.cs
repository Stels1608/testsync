﻿// Decompiled with JetBrains decompiler
// Type: ExternalApi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public static class ExternalApi
{
  public static void CallJs(string functionName, params object[] args)
  {
    string str1 = functionName + "(";
    string str2 = string.Empty;
    for (int index = 0; index < args.Length; ++index)
    {
      str1 = str1 + str2 + "\"" + args[index] + "\"";
      str2 = ",";
    }
    LauncherApi.CallJavascript(str1 + ");");
  }

  public static void EvalJs(string script)
  {
    LauncherApi.CallJavascript(script.Replace("window.", string.Empty));
  }
}
