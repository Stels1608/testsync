﻿// Decompiled with JetBrains decompiler
// Type: DebugSpiral
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugSpiral : MonoBehaviour
{
  private Vector3[] points;

  private void Awake()
  {
    List<Vector3> vector3List = new List<Vector3>();
    float count = 3f;
    float lenght = 6f;
    float r1 = 1f;
    float r2 = 10f;
    float from = 0.01f;
    float to = 0.02f;
    float num1 = 1f;
    float num2 = 0.01f;
    Vector3 vector3_1 = new Vector3();
    float t = 0.0f;
    while ((double) t < 1.0)
    {
      Vector3 vector3_2 = this.F(t, count, lenght, r1, r2);
      if ((double) (vector3_2 - vector3_1).magnitude > (double) num1)
      {
        num1 = Mathf.Lerp(from, to, Random.value);
        vector3_1 = vector3_2;
        vector3List.Add(vector3_2);
      }
      t += num2;
    }
    this.points = vector3List.ToArray();
  }

  private void Update()
  {
    BgoDebug.DrawPathWithNormal(this.points, Vector3.left, 0.1f, Color.red);
  }

  private Vector3 F(float t, float count, float lenght, float r1, float r2)
  {
    float f1 = Mathf.Lerp(0.0f, 6.283185f * count, t);
    float f2 = Mathf.Lerp(0.0f, 6.283185f * count, t);
    float num = Mathf.Lerp(r1, r2, t);
    float z = Mathf.Lerp(0.0f, lenght, t);
    return new Vector3(Mathf.Sin(f1) * num, Mathf.Cos(f2) * num, z);
  }
}
