﻿// Decompiled with JetBrains decompiler
// Type: UICamera
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[RequireComponent(typeof (Camera))]
[AddComponentMenu("NGUI/UI/NGUI Event System (UICamera)")]
[ExecuteInEditMode]
public class UICamera : MonoBehaviour
{
  public static BetterList<UICamera> list = new BetterList<UICamera>();
  public static UICamera.GetKeyStateFunc GetKeyDown = new UICamera.GetKeyStateFunc(Input.GetKeyDown);
  public static UICamera.GetKeyStateFunc GetKeyUp = new UICamera.GetKeyStateFunc(Input.GetKeyUp);
  public static UICamera.GetKeyStateFunc GetKey = new UICamera.GetKeyStateFunc(Input.GetKey);
  public static UICamera.GetAxisFunc GetAxis = new UICamera.GetAxisFunc(Input.GetAxis);
  public static bool showTooltips = true;
  public static Vector2 lastTouchPosition = Vector2.zero;
  public static Vector3 lastWorldPosition = Vector3.zero;
  public static UICamera current = (UICamera) null;
  public static Camera currentCamera = (Camera) null;
  public static UICamera.ControlScheme currentScheme = UICamera.ControlScheme.Mouse;
  public static int currentTouchID = -1;
  public static KeyCode currentKey = KeyCode.None;
  public static UICamera.MouseOrTouch currentTouch = (UICamera.MouseOrTouch) null;
  public static bool inputHasFocus = false;
  private static GameObject mCurrentSelection = (GameObject) null;
  private static GameObject mNextSelection = (GameObject) null;
  private static UICamera.ControlScheme mNextScheme = UICamera.ControlScheme.Controller;
  private static UICamera.MouseOrTouch[] mMouse = new UICamera.MouseOrTouch[3]{ new UICamera.MouseOrTouch(), new UICamera.MouseOrTouch(), new UICamera.MouseOrTouch() };
  public static UICamera.MouseOrTouch controller = new UICamera.MouseOrTouch();
  private static float mNextEvent = 0.0f;
  private static Dictionary<int, UICamera.MouseOrTouch> mTouches = new Dictionary<int, UICamera.MouseOrTouch>();
  private static int mWidth = 0;
  private static int mHeight = 0;
  public static bool isDragging = false;
  private static UICamera.DepthEntry mHit = new UICamera.DepthEntry();
  private static BetterList<UICamera.DepthEntry> mHits = new BetterList<UICamera.DepthEntry>();
  private static Plane m2DPlane = new Plane(Vector3.back, 0.0f);
  private static bool mNotifying = false;
  public UICamera.EventType eventType = UICamera.EventType.UI_3D;
  public LayerMask eventReceiverMask = (LayerMask) -1;
  public bool useMouse = true;
  public bool useTouch = true;
  public bool allowMultiTouch = true;
  public bool useKeyboard = true;
  public bool useController = true;
  public bool stickyTooltip = true;
  public float tooltipDelay = 1f;
  public float mouseDragThreshold = 4f;
  public float mouseClickThreshold = 10f;
  public float touchDragThreshold = 40f;
  public float touchClickThreshold = 40f;
  public float rangeDistance = -1f;
  public string scrollAxisName = "Mouse ScrollWheel";
  public string verticalAxisName = "Vertical";
  public string horizontalAxisName = "Horizontal";
  public KeyCode submitKey0 = KeyCode.Return;
  public KeyCode submitKey1 = KeyCode.JoystickButton0;
  public KeyCode cancelKey0 = KeyCode.Escape;
  public KeyCode cancelKey1 = KeyCode.JoystickButton1;
  public static UICamera.OnScreenResize onScreenResize;
  public bool debug;
  public static UICamera.OnCustomInput onCustomInput;
  public static RaycastHit lastHit;
  private static GameObject mGenericHandler;
  public static GameObject fallThrough;
  public static UICamera.VoidDelegate onClick;
  public static UICamera.VoidDelegate onDoubleClick;
  public static UICamera.BoolDelegate onHover;
  public static UICamera.BoolDelegate onPress;
  public static UICamera.BoolDelegate onSelect;
  public static UICamera.FloatDelegate onScroll;
  public static UICamera.VectorDelegate onDrag;
  public static UICamera.VoidDelegate onDragStart;
  public static UICamera.ObjectDelegate onDragOver;
  public static UICamera.ObjectDelegate onDragOut;
  public static UICamera.VoidDelegate onDragEnd;
  public static UICamera.ObjectDelegate onDrop;
  public static UICamera.KeyCodeDelegate onKey;
  public static UICamera.BoolDelegate onTooltip;
  public static UICamera.MoveDelegate onMouseMove;
  private static GameObject mHover;
  private GameObject mTooltip;
  private Camera mCam;
  private float mTooltipTime;
  private float mNextRaycast;
  public static GameObject hoveredObject;

  [Obsolete("Use new OnDragStart / OnDragOver / OnDragOut / OnDragEnd events instead")]
  public bool stickyPress
  {
    get
    {
      return true;
    }
  }

  public static Ray currentRay
  {
    get
    {
      if ((UnityEngine.Object) UICamera.currentCamera != (UnityEngine.Object) null && UICamera.currentTouch != null)
        return UICamera.currentCamera.ScreenPointToRay((Vector3) UICamera.currentTouch.pos);
      return new Ray();
    }
  }

  [Obsolete("Use delegates instead such as UICamera.onClick, UICamera.onHover, etc.")]
  public static GameObject genericEventHandler
  {
    get
    {
      return UICamera.mGenericHandler;
    }
    set
    {
      UICamera.mGenericHandler = value;
    }
  }

  private bool handlesEvents
  {
    get
    {
      return (UnityEngine.Object) UICamera.eventHandler == (UnityEngine.Object) this;
    }
  }

  public Camera cachedCamera
  {
    get
    {
      if ((UnityEngine.Object) this.mCam == (UnityEngine.Object) null)
        this.mCam = this.GetComponent<Camera>();
      return this.mCam;
    }
  }

  public static bool isOverUI
  {
    get
    {
      if (UICamera.currentTouch != null)
        return UICamera.currentTouch.isOverUI;
      if ((UnityEngine.Object) UICamera.hoveredObject == (UnityEngine.Object) null)
        return false;
      return (UnityEngine.Object) NGUITools.FindInParents<UIRoot>(UICamera.hoveredObject) != (UnityEngine.Object) null;
    }
  }

  public static GameObject selectedObject
  {
    get
    {
      return UICamera.mCurrentSelection;
    }
    set
    {
      UICamera.SetSelection(value, UICamera.currentScheme);
    }
  }

  public static int touchCount
  {
    get
    {
      int num = 0;
      using (Dictionary<int, UICamera.MouseOrTouch>.Enumerator enumerator = UICamera.mTouches.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if ((UnityEngine.Object) enumerator.Current.Value.pressed != (UnityEngine.Object) null)
            ++num;
        }
      }
      for (int index = 0; index < UICamera.mMouse.Length; ++index)
      {
        if ((UnityEngine.Object) UICamera.mMouse[index].pressed != (UnityEngine.Object) null)
          ++num;
      }
      if ((UnityEngine.Object) UICamera.controller.pressed != (UnityEngine.Object) null)
        ++num;
      return num;
    }
  }

  public static int dragCount
  {
    get
    {
      int num = 0;
      using (Dictionary<int, UICamera.MouseOrTouch>.Enumerator enumerator = UICamera.mTouches.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if ((UnityEngine.Object) enumerator.Current.Value.dragged != (UnityEngine.Object) null)
            ++num;
        }
      }
      for (int index = 0; index < UICamera.mMouse.Length; ++index)
      {
        if ((UnityEngine.Object) UICamera.mMouse[index].dragged != (UnityEngine.Object) null)
          ++num;
      }
      if ((UnityEngine.Object) UICamera.controller.dragged != (UnityEngine.Object) null)
        ++num;
      return num;
    }
  }

  public static Camera mainCamera
  {
    get
    {
      UICamera eventHandler = UICamera.eventHandler;
      if ((UnityEngine.Object) eventHandler != (UnityEngine.Object) null)
        return eventHandler.cachedCamera;
      return (Camera) null;
    }
  }

  public static UICamera eventHandler
  {
    get
    {
      for (int index = 0; index < UICamera.list.size; ++index)
      {
        UICamera uiCamera = UICamera.list.buffer[index];
        if (!((UnityEngine.Object) uiCamera == (UnityEngine.Object) null) && uiCamera.enabled && NGUITools.GetActive(uiCamera.gameObject))
          return uiCamera;
      }
      return (UICamera) null;
    }
  }

  public static bool IsPressed(GameObject go)
  {
    for (int index = 0; index < 3; ++index)
    {
      if ((UnityEngine.Object) UICamera.mMouse[index].pressed == (UnityEngine.Object) go)
        return true;
    }
    using (Dictionary<int, UICamera.MouseOrTouch>.Enumerator enumerator = UICamera.mTouches.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if ((UnityEngine.Object) enumerator.Current.Value.pressed == (UnityEngine.Object) go)
          return true;
      }
    }
    return (UnityEngine.Object) UICamera.controller.pressed == (UnityEngine.Object) go;
  }

  protected static void SetSelection(GameObject go, UICamera.ControlScheme scheme)
  {
    if ((UnityEngine.Object) UICamera.mNextSelection != (UnityEngine.Object) null)
    {
      UICamera.mNextSelection = go;
    }
    else
    {
      if (!((UnityEngine.Object) UICamera.mCurrentSelection != (UnityEngine.Object) go))
        return;
      UICamera.mNextSelection = go;
      UICamera.mNextScheme = scheme;
      if (UICamera.list.size <= 0)
        return;
      UICamera uiCamera = !((UnityEngine.Object) UICamera.mNextSelection != (UnityEngine.Object) null) ? UICamera.list[0] : UICamera.FindCameraForLayer(UICamera.mNextSelection.layer);
      if (!((UnityEngine.Object) uiCamera != (UnityEngine.Object) null))
        return;
      uiCamera.StartCoroutine(uiCamera.ChangeSelection());
    }
  }

  [DebuggerHidden]
  private IEnumerator ChangeSelection()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UICamera.\u003CChangeSelection\u003Ec__Iterator3() { \u003C\u003Ef__this = this };
  }

  private static int CompareFunc(UICamera a, UICamera b)
  {
    if ((double) a.cachedCamera.depth < (double) b.cachedCamera.depth)
      return 1;
    return (double) a.cachedCamera.depth > (double) b.cachedCamera.depth ? -1 : 0;
  }

  private static Rigidbody FindRootRigidbody(Transform trans)
  {
    for (; (UnityEngine.Object) trans != (UnityEngine.Object) null; trans = trans.parent)
    {
      if ((UnityEngine.Object) trans.GetComponent<UIPanel>() != (UnityEngine.Object) null)
        return (Rigidbody) null;
      Rigidbody component = trans.GetComponent<Rigidbody>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        return component;
    }
    return (Rigidbody) null;
  }

  private static Rigidbody2D FindRootRigidbody2D(Transform trans)
  {
    for (; (UnityEngine.Object) trans != (UnityEngine.Object) null; trans = trans.parent)
    {
      if ((UnityEngine.Object) trans.GetComponent<UIPanel>() != (UnityEngine.Object) null)
        return (Rigidbody2D) null;
      Rigidbody2D component = trans.GetComponent<Rigidbody2D>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        return component;
    }
    return (Rigidbody2D) null;
  }

  public static bool Raycast(Vector3 inPos)
  {
    for (int index1 = 0; index1 < UICamera.list.size; ++index1)
    {
      UICamera uiCamera = UICamera.list.buffer[index1];
      if (uiCamera.enabled && NGUITools.GetActive(uiCamera.gameObject))
      {
        UICamera.currentCamera = uiCamera.cachedCamera;
        Vector3 viewportPoint = UICamera.currentCamera.ScreenToViewportPoint(inPos);
        if (!float.IsNaN(viewportPoint.x) && !float.IsNaN(viewportPoint.y) && ((double) viewportPoint.x >= 0.0 && (double) viewportPoint.x <= 1.0) && ((double) viewportPoint.y >= 0.0 && (double) viewportPoint.y <= 1.0))
        {
          Ray ray = UICamera.currentCamera.ScreenPointToRay(inPos);
          int layerMask = UICamera.currentCamera.cullingMask & (int) uiCamera.eventReceiverMask;
          float enter = (double) uiCamera.rangeDistance <= 0.0 ? UICamera.currentCamera.farClipPlane - UICamera.currentCamera.nearClipPlane : uiCamera.rangeDistance;
          if (uiCamera.eventType == UICamera.EventType.World_3D)
          {
            if (Physics.Raycast(ray, out UICamera.lastHit, enter, layerMask))
            {
              UICamera.lastWorldPosition = UICamera.lastHit.point;
              UICamera.hoveredObject = UICamera.lastHit.collider.gameObject;
              Rigidbody rootRigidbody = UICamera.FindRootRigidbody(UICamera.hoveredObject.transform);
              if ((UnityEngine.Object) rootRigidbody != (UnityEngine.Object) null)
                UICamera.hoveredObject = rootRigidbody.gameObject;
              return true;
            }
          }
          else if (uiCamera.eventType == UICamera.EventType.UI_3D)
          {
            RaycastHit[] raycastHitArray = Physics.RaycastAll(ray, enter, layerMask);
            if (raycastHitArray.Length > 1)
            {
              for (int index2 = 0; index2 < raycastHitArray.Length; ++index2)
              {
                GameObject gameObject = raycastHitArray[index2].collider.gameObject;
                UIWidget component = gameObject.GetComponent<UIWidget>();
                if ((UnityEngine.Object) component != (UnityEngine.Object) null)
                {
                  if (!component.isVisible || component.hitCheck != null && !component.hitCheck(raycastHitArray[index2].point))
                    continue;
                }
                else
                {
                  UIRect inParents = NGUITools.FindInParents<UIRect>(gameObject);
                  if ((UnityEngine.Object) inParents != (UnityEngine.Object) null && (double) inParents.finalAlpha < 1.0 / 1000.0)
                    continue;
                }
                UICamera.mHit.depth = NGUITools.CalculateRaycastDepth(gameObject);
                if (UICamera.mHit.depth != int.MaxValue)
                {
                  UICamera.mHit.hit = raycastHitArray[index2];
                  UICamera.mHit.point = raycastHitArray[index2].point;
                  UICamera.mHit.go = raycastHitArray[index2].collider.gameObject;
                  UICamera.mHits.Add(UICamera.mHit);
                }
              }
              UICamera.mHits.Sort((BetterList<UICamera.DepthEntry>.CompareFunc) ((r1, r2) => r2.depth.CompareTo(r1.depth)));
              for (int index2 = 0; index2 < UICamera.mHits.size; ++index2)
              {
                if (UICamera.IsVisible(ref UICamera.mHits.buffer[index2]))
                {
                  UICamera.lastHit = UICamera.mHits[index2].hit;
                  UICamera.hoveredObject = UICamera.mHits[index2].go;
                  UICamera.lastWorldPosition = UICamera.mHits[index2].point;
                  UICamera.mHits.Clear();
                  return true;
                }
              }
              UICamera.mHits.Clear();
            }
            else if (raycastHitArray.Length == 1)
            {
              GameObject gameObject = raycastHitArray[0].collider.gameObject;
              UIWidget component = gameObject.GetComponent<UIWidget>();
              if ((UnityEngine.Object) component != (UnityEngine.Object) null)
              {
                if (!component.isVisible || component.hitCheck != null && !component.hitCheck(raycastHitArray[0].point))
                  continue;
              }
              else
              {
                UIRect inParents = NGUITools.FindInParents<UIRect>(gameObject);
                if ((UnityEngine.Object) inParents != (UnityEngine.Object) null && (double) inParents.finalAlpha < 1.0 / 1000.0)
                  continue;
              }
              if (UICamera.IsVisible(raycastHitArray[0].point, raycastHitArray[0].collider.gameObject))
              {
                UICamera.lastHit = raycastHitArray[0];
                UICamera.lastWorldPosition = raycastHitArray[0].point;
                UICamera.hoveredObject = UICamera.lastHit.collider.gameObject;
                return true;
              }
            }
          }
          else if (uiCamera.eventType == UICamera.EventType.World_2D)
          {
            if (UICamera.m2DPlane.Raycast(ray, out enter))
            {
              Vector3 point = ray.GetPoint(enter);
              Collider2D collider2D = Physics2D.OverlapPoint((Vector2) point, layerMask);
              if ((bool) ((UnityEngine.Object) collider2D))
              {
                UICamera.lastWorldPosition = point;
                UICamera.hoveredObject = collider2D.gameObject;
                Rigidbody2D rootRigidbody2D = UICamera.FindRootRigidbody2D(UICamera.hoveredObject.transform);
                if ((UnityEngine.Object) rootRigidbody2D != (UnityEngine.Object) null)
                  UICamera.hoveredObject = rootRigidbody2D.gameObject;
                return true;
              }
            }
          }
          else if (uiCamera.eventType == UICamera.EventType.UI_2D && UICamera.m2DPlane.Raycast(ray, out enter))
          {
            UICamera.lastWorldPosition = ray.GetPoint(enter);
            Collider2D[] collider2DArray = Physics2D.OverlapPointAll((Vector2) UICamera.lastWorldPosition, layerMask);
            if (collider2DArray.Length > 1)
            {
              for (int index2 = 0; index2 < collider2DArray.Length; ++index2)
              {
                GameObject gameObject = collider2DArray[index2].gameObject;
                UIWidget component = gameObject.GetComponent<UIWidget>();
                if ((UnityEngine.Object) component != (UnityEngine.Object) null)
                {
                  if (!component.isVisible || component.hitCheck != null && !component.hitCheck(UICamera.lastWorldPosition))
                    continue;
                }
                else
                {
                  UIRect inParents = NGUITools.FindInParents<UIRect>(gameObject);
                  if ((UnityEngine.Object) inParents != (UnityEngine.Object) null && (double) inParents.finalAlpha < 1.0 / 1000.0)
                    continue;
                }
                UICamera.mHit.depth = NGUITools.CalculateRaycastDepth(gameObject);
                if (UICamera.mHit.depth != int.MaxValue)
                {
                  UICamera.mHit.go = gameObject;
                  UICamera.mHit.point = UICamera.lastWorldPosition;
                  UICamera.mHits.Add(UICamera.mHit);
                }
              }
              UICamera.mHits.Sort((BetterList<UICamera.DepthEntry>.CompareFunc) ((r1, r2) => r2.depth.CompareTo(r1.depth)));
              for (int index2 = 0; index2 < UICamera.mHits.size; ++index2)
              {
                if (UICamera.IsVisible(ref UICamera.mHits.buffer[index2]))
                {
                  UICamera.hoveredObject = UICamera.mHits[index2].go;
                  UICamera.mHits.Clear();
                  return true;
                }
              }
              UICamera.mHits.Clear();
            }
            else if (collider2DArray.Length == 1)
            {
              GameObject gameObject = collider2DArray[0].gameObject;
              UIWidget component = gameObject.GetComponent<UIWidget>();
              if ((UnityEngine.Object) component != (UnityEngine.Object) null)
              {
                if (!component.isVisible || component.hitCheck != null && !component.hitCheck(UICamera.lastWorldPosition))
                  continue;
              }
              else
              {
                UIRect inParents = NGUITools.FindInParents<UIRect>(gameObject);
                if ((UnityEngine.Object) inParents != (UnityEngine.Object) null && (double) inParents.finalAlpha < 1.0 / 1000.0)
                  continue;
              }
              if (UICamera.IsVisible(UICamera.lastWorldPosition, gameObject))
              {
                UICamera.hoveredObject = gameObject;
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }

  private static bool IsVisible(Vector3 worldPoint, GameObject go)
  {
    for (UIPanel uiPanel = NGUITools.FindInParents<UIPanel>(go); (UnityEngine.Object) uiPanel != (UnityEngine.Object) null; uiPanel = uiPanel.parentPanel)
    {
      if (!uiPanel.IsVisible(worldPoint))
        return false;
    }
    return true;
  }

  private static bool IsVisible(ref UICamera.DepthEntry de)
  {
    for (UIPanel uiPanel = NGUITools.FindInParents<UIPanel>(de.go); (UnityEngine.Object) uiPanel != (UnityEngine.Object) null; uiPanel = uiPanel.parentPanel)
    {
      if (!uiPanel.IsVisible(de.point))
        return false;
    }
    return true;
  }

  public static bool IsHighlighted(GameObject go)
  {
    if (UICamera.currentScheme == UICamera.ControlScheme.Mouse)
      return (UnityEngine.Object) UICamera.hoveredObject == (UnityEngine.Object) go;
    if (UICamera.currentScheme == UICamera.ControlScheme.Controller)
      return (UnityEngine.Object) UICamera.selectedObject == (UnityEngine.Object) go;
    return false;
  }

  public static UICamera FindCameraForLayer(int layer)
  {
    int num = 1 << layer;
    for (int index = 0; index < UICamera.list.size; ++index)
    {
      UICamera uiCamera = UICamera.list.buffer[index];
      Camera cachedCamera = uiCamera.cachedCamera;
      if ((UnityEngine.Object) cachedCamera != (UnityEngine.Object) null && (cachedCamera.cullingMask & num) != 0)
        return uiCamera;
    }
    return (UICamera) null;
  }

  private static int GetDirection(KeyCode up, KeyCode down)
  {
    if (UICamera.GetKeyDown(up))
      return 1;
    return UICamera.GetKeyDown(down) ? -1 : 0;
  }

  private static int GetDirection(KeyCode up0, KeyCode up1, KeyCode down0, KeyCode down1)
  {
    if (UICamera.GetKeyDown(up0) || UICamera.GetKeyDown(up1))
      return 1;
    return UICamera.GetKeyDown(down0) || UICamera.GetKeyDown(down1) ? -1 : 0;
  }

  private static int GetDirection(string axis)
  {
    float time = RealTime.time;
    if ((double) UICamera.mNextEvent < (double) time && !string.IsNullOrEmpty(axis))
    {
      float num = UICamera.GetAxis(axis);
      if ((double) num > 0.75)
      {
        UICamera.mNextEvent = time + 0.25f;
        return 1;
      }
      if ((double) num < -0.75)
      {
        UICamera.mNextEvent = time + 0.25f;
        return -1;
      }
    }
    return 0;
  }

  public static void Notify(GameObject go, string funcName, object obj)
  {
    if (UICamera.mNotifying)
      return;
    UICamera.mNotifying = true;
    if (NGUITools.GetActive(go))
    {
      go.SendMessage(funcName, obj, SendMessageOptions.DontRequireReceiver);
      if ((UnityEngine.Object) UICamera.mGenericHandler != (UnityEngine.Object) null && (UnityEngine.Object) UICamera.mGenericHandler != (UnityEngine.Object) go)
        UICamera.mGenericHandler.SendMessage(funcName, obj, SendMessageOptions.DontRequireReceiver);
    }
    UICamera.mNotifying = false;
  }

  public static UICamera.MouseOrTouch GetMouse(int button)
  {
    return UICamera.mMouse[button];
  }

  public static UICamera.MouseOrTouch GetTouch(int id)
  {
    UICamera.MouseOrTouch mouseOrTouch = (UICamera.MouseOrTouch) null;
    if (id < 0)
      return UICamera.GetMouse(-id - 1);
    if (!UICamera.mTouches.TryGetValue(id, out mouseOrTouch))
    {
      mouseOrTouch = new UICamera.MouseOrTouch();
      mouseOrTouch.touchBegan = true;
      UICamera.mTouches.Add(id, mouseOrTouch);
    }
    return mouseOrTouch;
  }

  public static void RemoveTouch(int id)
  {
    UICamera.mTouches.Remove(id);
  }

  private void Awake()
  {
    UICamera.mWidth = Screen.width;
    UICamera.mHeight = Screen.height;
    if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer || (Application.platform == RuntimePlatform.WP8Player || Application.platform == RuntimePlatform.BlackBerryPlayer))
    {
      this.useMouse = false;
      this.useTouch = true;
      if (Application.platform == RuntimePlatform.IPhonePlayer)
      {
        this.useKeyboard = false;
        this.useController = false;
      }
    }
    else if (Application.platform == RuntimePlatform.PS3 || Application.platform == RuntimePlatform.XBOX360)
    {
      this.useMouse = false;
      this.useTouch = false;
      this.useKeyboard = false;
      this.useController = true;
    }
    UICamera.mMouse[0].pos = (Vector2) Input.mousePosition;
    for (int index = 1; index < 3; ++index)
    {
      UICamera.mMouse[index].pos = UICamera.mMouse[0].pos;
      UICamera.mMouse[index].lastPos = UICamera.mMouse[0].pos;
    }
    UICamera.lastTouchPosition = UICamera.mMouse[0].pos;
  }

  private void OnEnable()
  {
    UICamera.list.Add(this);
    UICamera.list.Sort(new BetterList<UICamera>.CompareFunc(UICamera.CompareFunc));
  }

  private void OnDisable()
  {
    UICamera.list.Remove(this);
  }

  private void Start()
  {
    if (this.eventType != UICamera.EventType.World_3D && this.cachedCamera.transparencySortMode != TransparencySortMode.Orthographic)
      this.cachedCamera.transparencySortMode = TransparencySortMode.Orthographic;
    if (Application.isPlaying)
      this.cachedCamera.eventMask = 0;
    if (!this.handlesEvents)
      return;
    NGUIDebug.debugRaycast = this.debug;
  }

  private void Update()
  {
    if (!this.handlesEvents)
      return;
    UICamera.current = this;
    if (this.useTouch)
      this.ProcessTouches();
    else if (this.useMouse)
      this.ProcessMouse();
    if (UICamera.onCustomInput != null)
      UICamera.onCustomInput();
    if (this.useMouse && (UnityEngine.Object) UICamera.mCurrentSelection != (UnityEngine.Object) null)
    {
      if (this.cancelKey0 != KeyCode.None && UICamera.GetKeyDown(this.cancelKey0))
      {
        UICamera.currentScheme = UICamera.ControlScheme.Controller;
        UICamera.currentKey = this.cancelKey0;
        UICamera.selectedObject = (GameObject) null;
      }
      else if (this.cancelKey1 != KeyCode.None && UICamera.GetKeyDown(this.cancelKey1))
      {
        UICamera.currentScheme = UICamera.ControlScheme.Controller;
        UICamera.currentKey = this.cancelKey1;
        UICamera.selectedObject = (GameObject) null;
      }
    }
    if ((UnityEngine.Object) UICamera.mCurrentSelection == (UnityEngine.Object) null)
      UICamera.inputHasFocus = false;
    if ((UnityEngine.Object) UICamera.mCurrentSelection != (UnityEngine.Object) null)
      this.ProcessOthers();
    if (this.useMouse && (UnityEngine.Object) UICamera.mHover != (UnityEngine.Object) null)
    {
      float delta = string.IsNullOrEmpty(this.scrollAxisName) ? 0.0f : UICamera.GetAxis(this.scrollAxisName);
      if ((double) delta != 0.0)
      {
        if (UICamera.onScroll != null)
          UICamera.onScroll(UICamera.mHover, delta);
        UICamera.Notify(UICamera.mHover, "OnScroll", (object) delta);
      }
      if (UICamera.showTooltips && (double) this.mTooltipTime != 0.0 && ((double) this.mTooltipTime < (double) RealTime.time || UICamera.GetKey(KeyCode.LeftShift) || UICamera.GetKey(KeyCode.RightShift)))
      {
        this.mTooltip = UICamera.mHover;
        this.ShowTooltip(true);
      }
    }
    UICamera.current = (UICamera) null;
  }

  private void LateUpdate()
  {
    if (!this.handlesEvents)
      return;
    int width = Screen.width;
    int height = Screen.height;
    if (width == UICamera.mWidth && height == UICamera.mHeight)
      return;
    UICamera.mWidth = width;
    UICamera.mHeight = height;
    UIRoot.Broadcast("UpdateAnchors");
    if (UICamera.onScreenResize == null)
      return;
    UICamera.onScreenResize();
  }

  public void ProcessMouse()
  {
    UICamera.lastTouchPosition = (Vector2) Input.mousePosition;
    UICamera.mMouse[0].delta = UICamera.lastTouchPosition - UICamera.mMouse[0].pos;
    UICamera.mMouse[0].pos = UICamera.lastTouchPosition;
    bool flag1 = (double) UICamera.mMouse[0].delta.sqrMagnitude > 1.0 / 1000.0;
    for (int index = 1; index < 3; ++index)
    {
      UICamera.mMouse[index].pos = UICamera.mMouse[0].pos;
      UICamera.mMouse[index].delta = UICamera.mMouse[0].delta;
    }
    bool flag2 = false;
    bool flag3 = false;
    for (int button = 0; button < 3; ++button)
    {
      if (Input.GetMouseButtonDown(button))
      {
        UICamera.currentScheme = UICamera.ControlScheme.Mouse;
        flag3 = true;
        flag2 = true;
      }
      else if (Input.GetMouseButton(button))
      {
        UICamera.currentScheme = UICamera.ControlScheme.Mouse;
        flag2 = true;
      }
    }
    if (flag2 || flag1 || (double) this.mNextRaycast < (double) RealTime.time)
    {
      this.mNextRaycast = RealTime.time + 0.02f;
      if (!UICamera.Raycast(Input.mousePosition))
        UICamera.hoveredObject = UICamera.fallThrough;
      if ((UnityEngine.Object) UICamera.hoveredObject == (UnityEngine.Object) null)
        UICamera.hoveredObject = UICamera.mGenericHandler;
      for (int index = 0; index < 3; ++index)
        UICamera.mMouse[index].current = UICamera.hoveredObject;
    }
    bool flag4 = (UnityEngine.Object) UICamera.mMouse[0].last != (UnityEngine.Object) UICamera.mMouse[0].current;
    if (flag4)
      UICamera.currentScheme = UICamera.ControlScheme.Mouse;
    if (flag2)
      this.mTooltipTime = 0.0f;
    else if (flag1 && (!this.stickyTooltip || flag4))
    {
      if ((double) this.mTooltipTime != 0.0)
        this.mTooltipTime = RealTime.time + this.tooltipDelay;
      else if ((UnityEngine.Object) this.mTooltip != (UnityEngine.Object) null)
        this.ShowTooltip(false);
    }
    if (UICamera.onMouseMove != null)
    {
      UICamera.currentTouch = UICamera.mMouse[0];
      UICamera.onMouseMove(UICamera.currentTouch.delta);
      UICamera.currentTouch = (UICamera.MouseOrTouch) null;
    }
    if ((flag3 || !flag2) && ((UnityEngine.Object) UICamera.mHover != (UnityEngine.Object) null && flag4))
    {
      UICamera.currentScheme = UICamera.ControlScheme.Mouse;
      UICamera.currentTouch = UICamera.mMouse[0];
      if ((UnityEngine.Object) this.mTooltip != (UnityEngine.Object) null)
        this.ShowTooltip(false);
      if (UICamera.onHover != null)
        UICamera.onHover(UICamera.mHover, false);
      UICamera.Notify(UICamera.mHover, "OnHover", (object) false);
      UICamera.mHover = (GameObject) null;
    }
    for (int button = 0; button < 3; ++button)
    {
      bool mouseButtonDown = Input.GetMouseButtonDown(button);
      bool mouseButtonUp = Input.GetMouseButtonUp(button);
      if (mouseButtonDown || mouseButtonUp)
        UICamera.currentScheme = UICamera.ControlScheme.Mouse;
      UICamera.currentTouch = UICamera.mMouse[button];
      UICamera.currentTouchID = -1 - button;
      UICamera.currentKey = (KeyCode) (323 + button);
      if (mouseButtonDown)
        UICamera.currentTouch.pressedCam = UICamera.currentCamera;
      else if ((UnityEngine.Object) UICamera.currentTouch.pressed != (UnityEngine.Object) null)
        UICamera.currentCamera = UICamera.currentTouch.pressedCam;
      this.ProcessTouch(mouseButtonDown, mouseButtonUp);
      UICamera.currentKey = KeyCode.None;
    }
    if (!flag2 && flag4)
    {
      UICamera.currentScheme = UICamera.ControlScheme.Mouse;
      this.mTooltipTime = RealTime.time + this.tooltipDelay;
      UICamera.mHover = UICamera.mMouse[0].current;
      UICamera.currentTouch = UICamera.mMouse[0];
      if (UICamera.onHover != null)
        UICamera.onHover(UICamera.mHover, true);
      UICamera.Notify(UICamera.mHover, "OnHover", (object) true);
    }
    UICamera.currentTouch = (UICamera.MouseOrTouch) null;
    UICamera.mMouse[0].last = UICamera.mMouse[0].current;
    for (int index = 1; index < 3; ++index)
      UICamera.mMouse[index].last = UICamera.mMouse[0].last;
  }

  public void ProcessTouches()
  {
    UICamera.currentScheme = UICamera.ControlScheme.Touch;
    for (int index = 0; index < Input.touchCount; ++index)
    {
      Touch touch = Input.GetTouch(index);
      UICamera.currentTouchID = !this.allowMultiTouch ? 1 : touch.fingerId;
      UICamera.currentTouch = UICamera.GetTouch(UICamera.currentTouchID);
      bool pressed = touch.phase == TouchPhase.Began || UICamera.currentTouch.touchBegan;
      bool unpressed = touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended;
      UICamera.currentTouch.touchBegan = false;
      UICamera.currentTouch.delta = !pressed ? touch.position - UICamera.currentTouch.pos : Vector2.zero;
      UICamera.currentTouch.pos = touch.position;
      if (!UICamera.Raycast((Vector3) UICamera.currentTouch.pos))
        UICamera.hoveredObject = UICamera.fallThrough;
      if ((UnityEngine.Object) UICamera.hoveredObject == (UnityEngine.Object) null)
        UICamera.hoveredObject = UICamera.mGenericHandler;
      UICamera.currentTouch.last = UICamera.currentTouch.current;
      UICamera.currentTouch.current = UICamera.hoveredObject;
      UICamera.lastTouchPosition = UICamera.currentTouch.pos;
      if (pressed)
        UICamera.currentTouch.pressedCam = UICamera.currentCamera;
      else if ((UnityEngine.Object) UICamera.currentTouch.pressed != (UnityEngine.Object) null)
        UICamera.currentCamera = UICamera.currentTouch.pressedCam;
      if (touch.tapCount > 1)
        UICamera.currentTouch.clickTime = RealTime.time;
      this.ProcessTouch(pressed, unpressed);
      if (unpressed)
        UICamera.RemoveTouch(UICamera.currentTouchID);
      UICamera.currentTouch.last = (GameObject) null;
      UICamera.currentTouch = (UICamera.MouseOrTouch) null;
      if (!this.allowMultiTouch)
        break;
    }
    if (Input.touchCount != 0 || !this.useMouse)
      return;
    this.ProcessMouse();
  }

  private void ProcessFakeTouches()
  {
    bool mouseButtonDown = Input.GetMouseButtonDown(0);
    bool mouseButtonUp = Input.GetMouseButtonUp(0);
    bool mouseButton = Input.GetMouseButton(0);
    if (!mouseButtonDown && !mouseButtonUp && !mouseButton)
      return;
    UICamera.currentTouchID = 1;
    UICamera.currentTouch = UICamera.mMouse[0];
    UICamera.currentTouch.touchBegan = mouseButtonDown;
    Vector2 vector2 = (Vector2) Input.mousePosition;
    UICamera.currentTouch.delta = !mouseButtonDown ? vector2 - UICamera.currentTouch.pos : Vector2.zero;
    UICamera.currentTouch.pos = vector2;
    if (!UICamera.Raycast((Vector3) UICamera.currentTouch.pos))
      UICamera.hoveredObject = UICamera.fallThrough;
    if ((UnityEngine.Object) UICamera.hoveredObject == (UnityEngine.Object) null)
      UICamera.hoveredObject = UICamera.mGenericHandler;
    UICamera.currentTouch.last = UICamera.currentTouch.current;
    UICamera.currentTouch.current = UICamera.hoveredObject;
    UICamera.lastTouchPosition = UICamera.currentTouch.pos;
    if (mouseButtonDown)
      UICamera.currentTouch.pressedCam = UICamera.currentCamera;
    else if ((UnityEngine.Object) UICamera.currentTouch.pressed != (UnityEngine.Object) null)
      UICamera.currentCamera = UICamera.currentTouch.pressedCam;
    this.ProcessTouch(mouseButtonDown, mouseButtonUp);
    if (mouseButtonUp)
      UICamera.RemoveTouch(UICamera.currentTouchID);
    UICamera.currentTouch.last = (GameObject) null;
    UICamera.currentTouch = (UICamera.MouseOrTouch) null;
  }

  public void ProcessOthers()
  {
    UICamera.currentTouchID = -100;
    UICamera.currentTouch = UICamera.controller;
    bool pressed = false;
    bool unpressed = false;
    if (this.submitKey0 != KeyCode.None && UICamera.GetKeyDown(this.submitKey0))
    {
      UICamera.currentKey = this.submitKey0;
      pressed = true;
    }
    if (this.submitKey1 != KeyCode.None && UICamera.GetKeyDown(this.submitKey1))
    {
      UICamera.currentKey = this.submitKey1;
      pressed = true;
    }
    if (this.submitKey0 != KeyCode.None && UICamera.GetKeyUp(this.submitKey0))
    {
      UICamera.currentKey = this.submitKey0;
      unpressed = true;
    }
    if (this.submitKey1 != KeyCode.None && UICamera.GetKeyUp(this.submitKey1))
    {
      UICamera.currentKey = this.submitKey1;
      unpressed = true;
    }
    if (pressed || unpressed)
    {
      UICamera.currentScheme = UICamera.ControlScheme.Controller;
      UICamera.currentTouch.last = UICamera.currentTouch.current;
      UICamera.currentTouch.current = UICamera.mCurrentSelection;
      this.ProcessTouch(pressed, unpressed);
      UICamera.currentTouch.last = (GameObject) null;
    }
    int num1 = 0;
    int num2 = 0;
    if (this.useKeyboard)
    {
      if (UICamera.inputHasFocus)
      {
        num1 += UICamera.GetDirection(KeyCode.UpArrow, KeyCode.DownArrow);
        num2 += UICamera.GetDirection(KeyCode.RightArrow, KeyCode.LeftArrow);
      }
      else
      {
        num1 += UICamera.GetDirection(KeyCode.W, KeyCode.UpArrow, KeyCode.S, KeyCode.DownArrow);
        num2 += UICamera.GetDirection(KeyCode.D, KeyCode.RightArrow, KeyCode.A, KeyCode.LeftArrow);
      }
    }
    if (this.useController)
    {
      if (!string.IsNullOrEmpty(this.verticalAxisName))
        num1 += UICamera.GetDirection(this.verticalAxisName);
      if (!string.IsNullOrEmpty(this.horizontalAxisName))
        num2 += UICamera.GetDirection(this.horizontalAxisName);
    }
    if (num1 != 0)
    {
      UICamera.currentScheme = UICamera.ControlScheme.Controller;
      KeyCode key = num1 <= 0 ? KeyCode.DownArrow : KeyCode.UpArrow;
      if (UICamera.onKey != null)
        UICamera.onKey(UICamera.mCurrentSelection, key);
      UICamera.Notify(UICamera.mCurrentSelection, "OnKey", (object) key);
    }
    if (num2 != 0)
    {
      UICamera.currentScheme = UICamera.ControlScheme.Controller;
      KeyCode key = num2 <= 0 ? KeyCode.LeftArrow : KeyCode.RightArrow;
      if (UICamera.onKey != null)
        UICamera.onKey(UICamera.mCurrentSelection, key);
      UICamera.Notify(UICamera.mCurrentSelection, "OnKey", (object) key);
    }
    if (this.useKeyboard && UICamera.GetKeyDown(KeyCode.Tab))
    {
      UICamera.currentKey = KeyCode.Tab;
      UICamera.currentScheme = UICamera.ControlScheme.Controller;
      if (UICamera.onKey != null)
        UICamera.onKey(UICamera.mCurrentSelection, KeyCode.Tab);
      UICamera.Notify(UICamera.mCurrentSelection, "OnKey", (object) KeyCode.Tab);
    }
    if (this.cancelKey0 != KeyCode.None && UICamera.GetKeyDown(this.cancelKey0))
    {
      UICamera.currentKey = this.cancelKey0;
      UICamera.currentScheme = UICamera.ControlScheme.Controller;
      if (UICamera.onKey != null)
        UICamera.onKey(UICamera.mCurrentSelection, KeyCode.Escape);
      UICamera.Notify(UICamera.mCurrentSelection, "OnKey", (object) KeyCode.Escape);
    }
    if (this.cancelKey1 != KeyCode.None && UICamera.GetKeyDown(this.cancelKey1))
    {
      UICamera.currentKey = this.cancelKey1;
      UICamera.currentScheme = UICamera.ControlScheme.Controller;
      if (UICamera.onKey != null)
        UICamera.onKey(UICamera.mCurrentSelection, KeyCode.Escape);
      UICamera.Notify(UICamera.mCurrentSelection, "OnKey", (object) KeyCode.Escape);
    }
    UICamera.currentTouch = (UICamera.MouseOrTouch) null;
    UICamera.currentKey = KeyCode.None;
  }

  public void ProcessTouch(bool pressed, bool unpressed)
  {
    bool flag1 = UICamera.currentScheme == UICamera.ControlScheme.Mouse;
    float num1 = !flag1 ? this.touchDragThreshold : this.mouseDragThreshold;
    float num2 = !flag1 ? this.touchClickThreshold : this.mouseClickThreshold;
    float num3 = num1 * num1;
    float num4 = num2 * num2;
    if (pressed)
    {
      if ((UnityEngine.Object) this.mTooltip != (UnityEngine.Object) null)
        this.ShowTooltip(false);
      UICamera.currentTouch.pressStarted = true;
      if (UICamera.onPress != null)
        UICamera.onPress(UICamera.currentTouch.pressed, false);
      UICamera.Notify(UICamera.currentTouch.pressed, "OnPress", (object) false);
      UICamera.currentTouch.pressed = UICamera.currentTouch.current;
      UICamera.currentTouch.dragged = UICamera.currentTouch.current;
      UICamera.currentTouch.clickNotification = UICamera.ClickNotification.BasedOnDelta;
      UICamera.currentTouch.totalDelta = Vector2.zero;
      UICamera.currentTouch.dragStarted = false;
      if (UICamera.onPress != null)
        UICamera.onPress(UICamera.currentTouch.pressed, true);
      UICamera.Notify(UICamera.currentTouch.pressed, "OnPress", (object) true);
      if ((UnityEngine.Object) UICamera.currentTouch.pressed != (UnityEngine.Object) UICamera.mCurrentSelection)
      {
        if ((UnityEngine.Object) this.mTooltip != (UnityEngine.Object) null)
          this.ShowTooltip(false);
        UICamera.currentScheme = UICamera.ControlScheme.Touch;
        UICamera.selectedObject = UICamera.currentTouch.pressed;
      }
    }
    else if ((UnityEngine.Object) UICamera.currentTouch.pressed != (UnityEngine.Object) null && ((double) UICamera.currentTouch.delta.sqrMagnitude != 0.0 || (UnityEngine.Object) UICamera.currentTouch.current != (UnityEngine.Object) UICamera.currentTouch.last))
    {
      UICamera.currentTouch.totalDelta += UICamera.currentTouch.delta;
      float sqrMagnitude = UICamera.currentTouch.totalDelta.sqrMagnitude;
      bool flag2 = false;
      if (!UICamera.currentTouch.dragStarted && (UnityEngine.Object) UICamera.currentTouch.last != (UnityEngine.Object) UICamera.currentTouch.current)
      {
        UICamera.currentTouch.dragStarted = true;
        UICamera.currentTouch.delta = UICamera.currentTouch.totalDelta;
        UICamera.isDragging = true;
        if (UICamera.onDragStart != null)
          UICamera.onDragStart(UICamera.currentTouch.dragged);
        UICamera.Notify(UICamera.currentTouch.dragged, "OnDragStart", (object) null);
        if (UICamera.onDragOver != null)
          UICamera.onDragOver(UICamera.currentTouch.last, UICamera.currentTouch.dragged);
        UICamera.Notify(UICamera.currentTouch.last, "OnDragOver", (object) UICamera.currentTouch.dragged);
        UICamera.isDragging = false;
      }
      else if (!UICamera.currentTouch.dragStarted && (double) num3 < (double) sqrMagnitude)
      {
        flag2 = true;
        UICamera.currentTouch.dragStarted = true;
        UICamera.currentTouch.delta = UICamera.currentTouch.totalDelta;
      }
      if (UICamera.currentTouch.dragStarted)
      {
        if ((UnityEngine.Object) this.mTooltip != (UnityEngine.Object) null)
          this.ShowTooltip(false);
        UICamera.isDragging = true;
        bool flag3 = UICamera.currentTouch.clickNotification == UICamera.ClickNotification.None;
        if (flag2)
        {
          if (UICamera.onDragStart != null)
            UICamera.onDragStart(UICamera.currentTouch.dragged);
          UICamera.Notify(UICamera.currentTouch.dragged, "OnDragStart", (object) null);
          if (UICamera.onDragOver != null)
            UICamera.onDragOver(UICamera.currentTouch.last, UICamera.currentTouch.dragged);
          UICamera.Notify(UICamera.currentTouch.current, "OnDragOver", (object) UICamera.currentTouch.dragged);
        }
        else if ((UnityEngine.Object) UICamera.currentTouch.last != (UnityEngine.Object) UICamera.currentTouch.current)
        {
          if (UICamera.onDragStart != null)
            UICamera.onDragStart(UICamera.currentTouch.dragged);
          UICamera.Notify(UICamera.currentTouch.last, "OnDragOut", (object) UICamera.currentTouch.dragged);
          if (UICamera.onDragOver != null)
            UICamera.onDragOver(UICamera.currentTouch.last, UICamera.currentTouch.dragged);
          UICamera.Notify(UICamera.currentTouch.current, "OnDragOver", (object) UICamera.currentTouch.dragged);
        }
        if (UICamera.onDrag != null)
          UICamera.onDrag(UICamera.currentTouch.dragged, UICamera.currentTouch.delta);
        UICamera.Notify(UICamera.currentTouch.dragged, "OnDrag", (object) UICamera.currentTouch.delta);
        UICamera.currentTouch.last = UICamera.currentTouch.current;
        UICamera.isDragging = false;
        if (flag3)
          UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;
        else if (UICamera.currentTouch.clickNotification == UICamera.ClickNotification.BasedOnDelta && (double) num4 < (double) sqrMagnitude)
          UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;
      }
    }
    if (!unpressed)
      return;
    UICamera.currentTouch.pressStarted = false;
    if ((UnityEngine.Object) this.mTooltip != (UnityEngine.Object) null)
      this.ShowTooltip(false);
    if ((UnityEngine.Object) UICamera.currentTouch.pressed != (UnityEngine.Object) null)
    {
      if (UICamera.currentTouch.dragStarted)
      {
        if (UICamera.onDragOut != null)
          UICamera.onDragOut(UICamera.currentTouch.last, UICamera.currentTouch.dragged);
        UICamera.Notify(UICamera.currentTouch.last, "OnDragOut", (object) UICamera.currentTouch.dragged);
        if (UICamera.onDragEnd != null)
          UICamera.onDragEnd(UICamera.currentTouch.dragged);
        UICamera.Notify(UICamera.currentTouch.dragged, "OnDragEnd", (object) null);
      }
      if (UICamera.onPress != null)
        UICamera.onPress(UICamera.currentTouch.pressed, false);
      UICamera.Notify(UICamera.currentTouch.pressed, "OnPress", (object) false);
      if (flag1)
      {
        if (UICamera.onHover != null)
          UICamera.onHover(UICamera.currentTouch.current, true);
        UICamera.Notify(UICamera.currentTouch.current, "OnHover", (object) true);
      }
      UICamera.mHover = UICamera.currentTouch.current;
      if ((UnityEngine.Object) UICamera.currentTouch.dragged == (UnityEngine.Object) UICamera.currentTouch.current || UICamera.currentScheme != UICamera.ControlScheme.Controller && UICamera.currentTouch.clickNotification != UICamera.ClickNotification.None && (double) UICamera.currentTouch.totalDelta.sqrMagnitude < (double) num3)
      {
        if ((UnityEngine.Object) UICamera.currentTouch.pressed != (UnityEngine.Object) UICamera.mCurrentSelection)
        {
          UICamera.mNextSelection = (GameObject) null;
          UICamera.mCurrentSelection = UICamera.currentTouch.pressed;
          if (UICamera.onSelect != null)
            UICamera.onSelect(UICamera.currentTouch.pressed, true);
          UICamera.Notify(UICamera.currentTouch.pressed, "OnSelect", (object) true);
        }
        else
        {
          UICamera.mNextSelection = (GameObject) null;
          UICamera.mCurrentSelection = UICamera.currentTouch.pressed;
        }
        if (UICamera.currentTouch.clickNotification != UICamera.ClickNotification.None && (UnityEngine.Object) UICamera.currentTouch.pressed == (UnityEngine.Object) UICamera.currentTouch.current)
        {
          float time = RealTime.time;
          if (UICamera.onClick != null)
            UICamera.onClick(UICamera.currentTouch.pressed);
          UICamera.Notify(UICamera.currentTouch.pressed, "OnClick", (object) null);
          if ((double) UICamera.currentTouch.clickTime + 0.349999994039536 > (double) time)
          {
            if (UICamera.onDoubleClick != null)
              UICamera.onDoubleClick(UICamera.currentTouch.pressed);
            UICamera.Notify(UICamera.currentTouch.pressed, "OnDoubleClick", (object) null);
          }
          UICamera.currentTouch.clickTime = time;
        }
      }
      else if (UICamera.currentTouch.dragStarted)
      {
        if (UICamera.onDrop != null)
          UICamera.onDrop(UICamera.currentTouch.current, UICamera.currentTouch.dragged);
        UICamera.Notify(UICamera.currentTouch.current, "OnDrop", (object) UICamera.currentTouch.dragged);
      }
    }
    UICamera.currentTouch.dragStarted = false;
    UICamera.currentTouch.pressed = (GameObject) null;
    UICamera.currentTouch.dragged = (GameObject) null;
  }

  public void ShowTooltip(bool val)
  {
    this.mTooltipTime = 0.0f;
    if (UICamera.onTooltip != null)
      UICamera.onTooltip(this.mTooltip, val);
    UICamera.Notify(this.mTooltip, "OnTooltip", (object) val);
    if (val)
      return;
    this.mTooltip = (GameObject) null;
  }

  private void OnApplicationPause()
  {
    UICamera.MouseOrTouch mouseOrTouch = UICamera.currentTouch;
    if (this.useTouch)
    {
      BetterList<int> betterList = new BetterList<int>();
      using (Dictionary<int, UICamera.MouseOrTouch>.Enumerator enumerator = UICamera.mTouches.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<int, UICamera.MouseOrTouch> current = enumerator.Current;
          if (current.Value != null && (bool) ((UnityEngine.Object) current.Value.pressed))
          {
            UICamera.currentTouch = current.Value;
            UICamera.currentTouchID = current.Key;
            UICamera.currentScheme = UICamera.ControlScheme.Touch;
            UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;
            this.ProcessTouch(false, true);
            betterList.Add(UICamera.currentTouchID);
          }
        }
      }
      for (int index = 0; index < betterList.size; ++index)
        UICamera.RemoveTouch(betterList[index]);
    }
    if (this.useMouse)
    {
      for (int index = 0; index < 3; ++index)
      {
        if ((bool) ((UnityEngine.Object) UICamera.mMouse[index].pressed))
        {
          UICamera.currentTouch = UICamera.mMouse[index];
          UICamera.currentTouchID = -1 - index;
          UICamera.currentKey = (KeyCode) (323 + index);
          UICamera.currentScheme = UICamera.ControlScheme.Mouse;
          UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;
          this.ProcessTouch(false, true);
        }
      }
    }
    if (this.useController && (bool) ((UnityEngine.Object) UICamera.controller.pressed))
    {
      UICamera.currentTouch = UICamera.controller;
      UICamera.currentTouchID = -100;
      UICamera.currentScheme = UICamera.ControlScheme.Controller;
      UICamera.currentTouch.last = UICamera.currentTouch.current;
      UICamera.currentTouch.current = UICamera.mCurrentSelection;
      UICamera.currentTouch.clickNotification = UICamera.ClickNotification.None;
      this.ProcessTouch(false, true);
      UICamera.currentTouch.last = (GameObject) null;
    }
    UICamera.currentTouch = mouseOrTouch;
  }

  public enum ControlScheme
  {
    Mouse,
    Touch,
    Controller,
  }

  public enum ClickNotification
  {
    None,
    Always,
    BasedOnDelta,
  }

  public class MouseOrTouch
  {
    public UICamera.ClickNotification clickNotification = UICamera.ClickNotification.Always;
    public bool touchBegan = true;
    public Vector2 pos;
    public Vector2 lastPos;
    public Vector2 delta;
    public Vector2 totalDelta;
    public Camera pressedCam;
    public GameObject last;
    public GameObject current;
    public GameObject pressed;
    public GameObject dragged;
    public float clickTime;
    public bool pressStarted;
    public bool dragStarted;

    public bool isOverUI
    {
      get
      {
        if ((UnityEngine.Object) this.current != (UnityEngine.Object) null)
          return (UnityEngine.Object) NGUITools.FindInParents<UIRoot>(this.current) != (UnityEngine.Object) null;
        return false;
      }
    }
  }

  public enum EventType
  {
    World_3D,
    UI_3D,
    World_2D,
    UI_2D,
  }

  private struct DepthEntry
  {
    public int depth;
    public RaycastHit hit;
    public Vector3 point;
    public GameObject go;
  }

  public delegate bool GetKeyStateFunc(KeyCode key);

  public delegate float GetAxisFunc(string name);

  public delegate void OnScreenResize();

  public delegate void OnCustomInput();

  public delegate void MoveDelegate(Vector2 delta);

  public delegate void VoidDelegate(GameObject go);

  public delegate void BoolDelegate(GameObject go, bool state);

  public delegate void FloatDelegate(GameObject go, float delta);

  public delegate void VectorDelegate(GameObject go, Vector2 delta);

  public delegate void ObjectDelegate(GameObject go, GameObject obj);

  public delegate void KeyCodeDelegate(GameObject go, KeyCode key);
}
