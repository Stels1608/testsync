﻿// Decompiled with JetBrains decompiler
// Type: TMPro.FontStyles
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace TMPro
{
  public enum FontStyles
  {
    Normal = 0,
    Bold = 1,
    Italic = 2,
    Underline = 4,
    LowerCase = 8,
    UpperCase = 16,
    SmallCaps = 32,
    Strikethrough = 64,
    Superscript = 128,
    Subscript = 256,
  }
}
