﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_TextInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TMPro
{
  [Serializable]
  public class TMP_TextInfo
  {
    private static Vector2 k_InfinityVectorPositive = new Vector2(1000000f, 1000000f);
    private static Vector2 k_InfinityVectorNegative = new Vector2(-1000000f, -1000000f);
    public TMP_Text textComponent;
    public int characterCount;
    public int spriteCount;
    public int spaceCount;
    public int wordCount;
    public int linkCount;
    public int lineCount;
    public int pageCount;
    public int materialCount;
    public TMP_CharacterInfo[] characterInfo;
    public TMP_WordInfo[] wordInfo;
    public TMP_LinkInfo[] linkInfo;
    public TMP_LineInfo[] lineInfo;
    public TMP_PageInfo[] pageInfo;
    public TMP_MeshInfo[] meshInfo;

    public TMP_TextInfo()
    {
      this.characterInfo = new TMP_CharacterInfo[8];
      this.wordInfo = new TMP_WordInfo[16];
      this.linkInfo = new TMP_LinkInfo[0];
      this.lineInfo = new TMP_LineInfo[2];
      this.pageInfo = new TMP_PageInfo[16];
      this.meshInfo = new TMP_MeshInfo[1];
    }

    public TMP_TextInfo(TMP_Text textComponent)
    {
      this.textComponent = textComponent;
      this.characterInfo = new TMP_CharacterInfo[8];
      this.wordInfo = new TMP_WordInfo[4];
      this.linkInfo = new TMP_LinkInfo[0];
      this.lineInfo = new TMP_LineInfo[2];
      this.pageInfo = new TMP_PageInfo[16];
      this.meshInfo = new TMP_MeshInfo[1];
      this.meshInfo[0].mesh = textComponent.mesh;
      this.materialCount = 1;
    }

    public void Clear()
    {
      this.characterCount = 0;
      this.spaceCount = 0;
      this.wordCount = 0;
      this.linkCount = 0;
      this.lineCount = 0;
      this.pageCount = 0;
      this.spriteCount = 0;
      for (int index = 0; index < this.meshInfo.Length; ++index)
        this.meshInfo[index].vertexCount = 0;
    }

    public void ClearMeshInfo(bool updateMesh)
    {
      for (int index = 0; index < this.meshInfo.Length; ++index)
        this.meshInfo[index].Clear(updateMesh);
    }

    public void ClearAllMeshInfo()
    {
      for (int index = 0; index < this.meshInfo.Length; ++index)
        this.meshInfo[index].Clear(true);
    }

    public void ClearUnusedVertices(MaterialReference[] materials)
    {
      for (int index = 0; index < this.meshInfo.Length; ++index)
      {
        int startIndex = 0;
        this.meshInfo[index].ClearUnusedVertices(startIndex);
      }
    }

    public void ClearLineInfo()
    {
      if (this.lineInfo == null)
        this.lineInfo = new TMP_LineInfo[2];
      for (int index = 0; index < this.lineInfo.Length; ++index)
      {
        this.lineInfo[index].characterCount = 0;
        this.lineInfo[index].spaceCount = 0;
        this.lineInfo[index].width = 0.0f;
        this.lineInfo[index].ascender = TMP_TextInfo.k_InfinityVectorNegative.x;
        this.lineInfo[index].descender = TMP_TextInfo.k_InfinityVectorPositive.x;
        this.lineInfo[index].lineExtents.min = TMP_TextInfo.k_InfinityVectorPositive;
        this.lineInfo[index].lineExtents.max = TMP_TextInfo.k_InfinityVectorNegative;
        this.lineInfo[index].maxAdvance = 0.0f;
      }
    }

    public static void Resize<T>(ref T[] array, int size)
    {
      int newSize = size <= 1024 ? Mathf.NextPowerOfTwo(size) : size + 256;
      Array.Resize<T>(ref array, newSize);
    }

    public static void Resize<T>(ref T[] array, int size, bool isBlockAllocated)
    {
      if (size <= array.Length)
        return;
      if (isBlockAllocated)
        size = size <= 1024 ? Mathf.NextPowerOfTwo(size) : size + 256;
      Array.Resize<T>(ref array, size);
    }
  }
}
