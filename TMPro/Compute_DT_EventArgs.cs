﻿// Decompiled with JetBrains decompiler
// Type: TMPro.Compute_DT_EventArgs
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TMPro
{
  public class Compute_DT_EventArgs
  {
    public Compute_DistanceTransform_EventTypes EventType;
    public float ProgressPercentage;
    public Color[] Colors;

    public Compute_DT_EventArgs(Compute_DistanceTransform_EventTypes type, float progress)
    {
      this.EventType = type;
      this.ProgressPercentage = progress;
    }

    public Compute_DT_EventArgs(Compute_DistanceTransform_EventTypes type, Color[] colors)
    {
      this.EventType = type;
      this.Colors = colors;
    }
  }
}
