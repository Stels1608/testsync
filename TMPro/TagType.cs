﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TagType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace TMPro
{
  public enum TagType
  {
    None = 0,
    NumericalValue = 1,
    StringValue = 2,
    ColorValue = 4,
  }
}
