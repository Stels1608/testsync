﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_TextUtilities
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TMPro
{
  public static class TMP_TextUtilities
  {
    private static Vector3[] m_rectWorldCorners = new Vector3[4];
    private const string k_lookupStringL = "-------------------------------- !-#$%&-()*+,-./0123456789:;<=>?@abcdefghijklmnopqrstuvwxyz[-]^_`abcdefghijklmnopqrstuvwxyz{|}~-";
    private const string k_lookupStringU = "-------------------------------- !-#$%&-()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[-]^_`ABCDEFGHIJKLMNOPQRSTUVWXYZ{|}~-";

    public static CaretInfo GetCursorInsertionIndex(TMP_Text textComponent, Vector3 position, Camera camera)
    {
      int nearestCharacter = TMP_TextUtilities.FindNearestCharacter(textComponent, position, camera, false);
      RectTransform rectTransform = textComponent.rectTransform;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle((Transform) rectTransform, (Vector2) position, camera, out position);
      TMP_CharacterInfo tmpCharacterInfo = textComponent.textInfo.characterInfo[nearestCharacter];
      Vector3 vector3_1 = rectTransform.TransformPoint(tmpCharacterInfo.bottomLeft);
      Vector3 vector3_2 = rectTransform.TransformPoint(tmpCharacterInfo.topRight);
      if (((double) position.x - (double) vector3_1.x) / ((double) vector3_2.x - (double) vector3_1.x) < 0.5)
        return new CaretInfo(nearestCharacter, CaretPosition.Left);
      return new CaretInfo(nearestCharacter, CaretPosition.Right);
    }

    public static bool IsIntersectingRectTransform(RectTransform rectTransform, Vector3 position, Camera camera)
    {
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle((Transform) rectTransform, (Vector2) position, camera, out position);
      rectTransform.GetWorldCorners(TMP_TextUtilities.m_rectWorldCorners);
      return TMP_TextUtilities.PointIntersectRectangle(position, TMP_TextUtilities.m_rectWorldCorners[0], TMP_TextUtilities.m_rectWorldCorners[1], TMP_TextUtilities.m_rectWorldCorners[2], TMP_TextUtilities.m_rectWorldCorners[3]);
    }

    public static int FindIntersectingCharacter(TextMeshProUGUI text, Vector3 position, Camera camera, bool visibleOnly)
    {
      RectTransform rectTransform = text.rectTransform;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle((Transform) rectTransform, (Vector2) position, camera, out position);
      for (int index = 0; index < text.textInfo.characterCount; ++index)
      {
        TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index];
        if (!visibleOnly || tmpCharacterInfo.isVisible)
        {
          Vector3 a = rectTransform.TransformPoint(tmpCharacterInfo.bottomLeft);
          Vector3 b = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.topRight.y, 0.0f));
          Vector3 c = rectTransform.TransformPoint(tmpCharacterInfo.topRight);
          Vector3 d = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.bottomLeft.y, 0.0f));
          if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
            return index;
        }
      }
      return -1;
    }

    public static int FindIntersectingCharacter(TextMeshPro text, Vector3 position, Camera camera, bool visibleOnly)
    {
      Transform transform = text.transform;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle(transform, (Vector2) position, camera, out position);
      for (int index = 0; index < text.textInfo.characterCount; ++index)
      {
        TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index];
        if (!visibleOnly || tmpCharacterInfo.isVisible)
        {
          Vector3 a = transform.TransformPoint(tmpCharacterInfo.bottomLeft);
          Vector3 b = transform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.topRight.y, 0.0f));
          Vector3 c = transform.TransformPoint(tmpCharacterInfo.topRight);
          Vector3 d = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.bottomLeft.y, 0.0f));
          if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
            return index;
        }
      }
      return -1;
    }

    public static int FindNearestCharacter(TMP_Text text, Vector3 position, Camera camera, bool visibleOnly)
    {
      RectTransform rectTransform = text.rectTransform;
      float num1 = float.PositiveInfinity;
      int num2 = 0;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle((Transform) rectTransform, (Vector2) position, camera, out position);
      for (int index = 0; index < text.textInfo.characterCount; ++index)
      {
        TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index];
        if (!visibleOnly || tmpCharacterInfo.isVisible)
        {
          Vector3 vector3_1 = rectTransform.TransformPoint(tmpCharacterInfo.bottomLeft);
          Vector3 vector3_2 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.topRight.y, 0.0f));
          Vector3 vector3_3 = rectTransform.TransformPoint(tmpCharacterInfo.topRight);
          Vector3 vector3_4 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.bottomLeft.y, 0.0f));
          if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_3, vector3_4))
            return index;
          float line1 = TMP_TextUtilities.DistanceToLine(vector3_1, vector3_2, position);
          float line2 = TMP_TextUtilities.DistanceToLine(vector3_2, vector3_3, position);
          float line3 = TMP_TextUtilities.DistanceToLine(vector3_3, vector3_4, position);
          float line4 = TMP_TextUtilities.DistanceToLine(vector3_4, vector3_1, position);
          float num3 = (double) line1 >= (double) line2 ? line2 : line1;
          float num4 = (double) num3 >= (double) line3 ? line3 : num3;
          float num5 = (double) num4 >= (double) line4 ? line4 : num4;
          if ((double) num1 > (double) num5)
          {
            num1 = num5;
            num2 = index;
          }
        }
      }
      return num2;
    }

    public static int FindNearestCharacter(TextMeshProUGUI text, Vector3 position, Camera camera, bool visibleOnly)
    {
      RectTransform rectTransform = text.rectTransform;
      float num1 = float.PositiveInfinity;
      int num2 = 0;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle((Transform) rectTransform, (Vector2) position, camera, out position);
      for (int index = 0; index < text.textInfo.characterCount; ++index)
      {
        TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index];
        if (!visibleOnly || tmpCharacterInfo.isVisible)
        {
          Vector3 vector3_1 = rectTransform.TransformPoint(tmpCharacterInfo.bottomLeft);
          Vector3 vector3_2 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.topRight.y, 0.0f));
          Vector3 vector3_3 = rectTransform.TransformPoint(tmpCharacterInfo.topRight);
          Vector3 vector3_4 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.bottomLeft.y, 0.0f));
          if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_3, vector3_4))
            return index;
          float line1 = TMP_TextUtilities.DistanceToLine(vector3_1, vector3_2, position);
          float line2 = TMP_TextUtilities.DistanceToLine(vector3_2, vector3_3, position);
          float line3 = TMP_TextUtilities.DistanceToLine(vector3_3, vector3_4, position);
          float line4 = TMP_TextUtilities.DistanceToLine(vector3_4, vector3_1, position);
          float num3 = (double) line1 >= (double) line2 ? line2 : line1;
          float num4 = (double) num3 >= (double) line3 ? line3 : num3;
          float num5 = (double) num4 >= (double) line4 ? line4 : num4;
          if ((double) num1 > (double) num5)
          {
            num1 = num5;
            num2 = index;
          }
        }
      }
      return num2;
    }

    public static int FindNearestCharacter(TextMeshPro text, Vector3 position, Camera camera, bool visibleOnly)
    {
      Transform transform = text.transform;
      float num1 = float.PositiveInfinity;
      int num2 = 0;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle(transform, (Vector2) position, camera, out position);
      for (int index = 0; index < text.textInfo.characterCount; ++index)
      {
        TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index];
        if (!visibleOnly || tmpCharacterInfo.isVisible)
        {
          Vector3 vector3_1 = transform.TransformPoint(tmpCharacterInfo.bottomLeft);
          Vector3 vector3_2 = transform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.topRight.y, 0.0f));
          Vector3 vector3_3 = transform.TransformPoint(tmpCharacterInfo.topRight);
          Vector3 vector3_4 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.bottomLeft.y, 0.0f));
          if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_3, vector3_4))
            return index;
          float line1 = TMP_TextUtilities.DistanceToLine(vector3_1, vector3_2, position);
          float line2 = TMP_TextUtilities.DistanceToLine(vector3_2, vector3_3, position);
          float line3 = TMP_TextUtilities.DistanceToLine(vector3_3, vector3_4, position);
          float line4 = TMP_TextUtilities.DistanceToLine(vector3_4, vector3_1, position);
          float num3 = (double) line1 >= (double) line2 ? line2 : line1;
          float num4 = (double) num3 >= (double) line3 ? line3 : num3;
          float num5 = (double) num4 >= (double) line4 ? line4 : num4;
          if ((double) num1 > (double) num5)
          {
            num1 = num5;
            num2 = index;
          }
        }
      }
      return num2;
    }

    public static int FindIntersectingWord(TextMeshProUGUI text, Vector3 position, Camera camera)
    {
      RectTransform rectTransform = text.rectTransform;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle((Transform) rectTransform, (Vector2) position, camera, out position);
      for (int index1 = 0; index1 < text.textInfo.wordCount; ++index1)
      {
        TMP_WordInfo tmpWordInfo = text.textInfo.wordInfo[index1];
        bool flag1 = false;
        Vector3 a = Vector3.zero;
        Vector3 b = Vector3.zero;
        Vector3 d = Vector3.zero;
        Vector3 c = Vector3.zero;
        float num1 = float.NegativeInfinity;
        float num2 = float.PositiveInfinity;
        for (int index2 = 0; index2 < tmpWordInfo.characterCount; ++index2)
        {
          int index3 = tmpWordInfo.firstCharacterIndex + index2;
          TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index3];
          int num3 = (int) tmpCharacterInfo.lineNumber;
          bool flag2 = index3 <= text.maxVisibleCharacters && (int) tmpCharacterInfo.lineNumber <= text.maxVisibleLines && (text.OverflowMode != TextOverflowModes.Page || (int) tmpCharacterInfo.pageNumber + 1 == text.pageToDisplay);
          num1 = Mathf.Max(num1, tmpCharacterInfo.ascender);
          num2 = Mathf.Min(num2, tmpCharacterInfo.descender);
          if (!flag1 && flag2)
          {
            flag1 = true;
            a = new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.descender, 0.0f);
            b = new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.ascender, 0.0f);
            if (tmpWordInfo.characterCount == 1)
            {
              flag1 = false;
              d = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f);
              c = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f);
              a = rectTransform.TransformPoint(new Vector3(a.x, num2, 0.0f));
              b = rectTransform.TransformPoint(new Vector3(b.x, num1, 0.0f));
              c = rectTransform.TransformPoint(new Vector3(c.x, num1, 0.0f));
              d = rectTransform.TransformPoint(new Vector3(d.x, num2, 0.0f));
              if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
                return index1;
            }
          }
          if (flag1 && index2 == tmpWordInfo.characterCount - 1)
          {
            flag1 = false;
            d = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f);
            c = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f);
            a = rectTransform.TransformPoint(new Vector3(a.x, num2, 0.0f));
            b = rectTransform.TransformPoint(new Vector3(b.x, num1, 0.0f));
            c = rectTransform.TransformPoint(new Vector3(c.x, num1, 0.0f));
            d = rectTransform.TransformPoint(new Vector3(d.x, num2, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
              return index1;
          }
          else if (flag1 && num3 != (int) text.textInfo.characterInfo[index3 + 1].lineNumber)
          {
            flag1 = false;
            d = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f);
            c = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f);
            a = rectTransform.TransformPoint(new Vector3(a.x, num2, 0.0f));
            b = rectTransform.TransformPoint(new Vector3(b.x, num1, 0.0f));
            c = rectTransform.TransformPoint(new Vector3(c.x, num1, 0.0f));
            d = rectTransform.TransformPoint(new Vector3(d.x, num2, 0.0f));
            num1 = float.NegativeInfinity;
            num2 = float.PositiveInfinity;
            if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
              return index1;
          }
        }
      }
      return -1;
    }

    public static int FindIntersectingWord(TextMeshPro text, Vector3 position, Camera camera)
    {
      Transform transform = text.transform;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle(transform, (Vector2) position, camera, out position);
      for (int index1 = 0; index1 < text.textInfo.wordCount; ++index1)
      {
        TMP_WordInfo tmpWordInfo = text.textInfo.wordInfo[index1];
        bool flag1 = false;
        Vector3 a = Vector3.zero;
        Vector3 b = Vector3.zero;
        Vector3 d = Vector3.zero;
        Vector3 c = Vector3.zero;
        float num1 = float.NegativeInfinity;
        float num2 = float.PositiveInfinity;
        for (int index2 = 0; index2 < tmpWordInfo.characterCount; ++index2)
        {
          int index3 = tmpWordInfo.firstCharacterIndex + index2;
          TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index3];
          int num3 = (int) tmpCharacterInfo.lineNumber;
          bool flag2 = index3 <= text.maxVisibleCharacters && (int) tmpCharacterInfo.lineNumber <= text.maxVisibleLines && (text.OverflowMode != TextOverflowModes.Page || (int) tmpCharacterInfo.pageNumber + 1 == text.pageToDisplay);
          num1 = Mathf.Max(num1, tmpCharacterInfo.ascender);
          num2 = Mathf.Min(num2, tmpCharacterInfo.descender);
          if (!flag1 && flag2)
          {
            flag1 = true;
            a = new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.descender, 0.0f);
            b = new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.ascender, 0.0f);
            if (tmpWordInfo.characterCount == 1)
            {
              flag1 = false;
              d = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f);
              c = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f);
              a = transform.TransformPoint(new Vector3(a.x, num2, 0.0f));
              b = transform.TransformPoint(new Vector3(b.x, num1, 0.0f));
              c = transform.TransformPoint(new Vector3(c.x, num1, 0.0f));
              d = transform.TransformPoint(new Vector3(d.x, num2, 0.0f));
              if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
                return index1;
            }
          }
          if (flag1 && index2 == tmpWordInfo.characterCount - 1)
          {
            flag1 = false;
            d = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f);
            c = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f);
            a = transform.TransformPoint(new Vector3(a.x, num2, 0.0f));
            b = transform.TransformPoint(new Vector3(b.x, num1, 0.0f));
            c = transform.TransformPoint(new Vector3(c.x, num1, 0.0f));
            d = transform.TransformPoint(new Vector3(d.x, num2, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
              return index1;
          }
          else if (flag1 && num3 != (int) text.textInfo.characterInfo[index3 + 1].lineNumber)
          {
            flag1 = false;
            d = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f);
            c = new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f);
            a = transform.TransformPoint(new Vector3(a.x, num2, 0.0f));
            b = transform.TransformPoint(new Vector3(b.x, num1, 0.0f));
            c = transform.TransformPoint(new Vector3(c.x, num1, 0.0f));
            d = transform.TransformPoint(new Vector3(d.x, num2, 0.0f));
            num1 = float.NegativeInfinity;
            num2 = float.PositiveInfinity;
            if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
              return index1;
          }
        }
      }
      return -1;
    }

    public static int FindNearestWord(TextMeshProUGUI text, Vector3 position, Camera camera)
    {
      RectTransform rectTransform = text.rectTransform;
      float num1 = float.PositiveInfinity;
      int num2 = 0;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle((Transform) rectTransform, (Vector2) position, camera, out position);
      for (int index1 = 0; index1 < text.textInfo.wordCount; ++index1)
      {
        TMP_WordInfo tmpWordInfo = text.textInfo.wordInfo[index1];
        bool flag1 = false;
        Vector3 vector3_1 = Vector3.zero;
        Vector3 vector3_2 = Vector3.zero;
        Vector3 vector3_3 = Vector3.zero;
        Vector3 vector3_4 = Vector3.zero;
        for (int index2 = 0; index2 < tmpWordInfo.characterCount; ++index2)
        {
          int index3 = tmpWordInfo.firstCharacterIndex + index2;
          TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index3];
          int num3 = (int) tmpCharacterInfo.lineNumber;
          bool flag2 = index3 <= text.maxVisibleCharacters && (int) tmpCharacterInfo.lineNumber <= text.maxVisibleLines && (text.OverflowMode != TextOverflowModes.Page || (int) tmpCharacterInfo.pageNumber + 1 == text.pageToDisplay);
          if (!flag1 && flag2)
          {
            flag1 = true;
            vector3_1 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.descender, 0.0f));
            vector3_2 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.ascender, 0.0f));
            if (tmpWordInfo.characterCount == 1)
            {
              flag1 = false;
              vector3_3 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
              vector3_4 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
              if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
                return index1;
            }
          }
          if (flag1 && index2 == tmpWordInfo.characterCount - 1)
          {
            flag1 = false;
            vector3_3 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            vector3_4 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
              return index1;
          }
          else if (flag1 && num3 != (int) text.textInfo.characterInfo[index3 + 1].lineNumber)
          {
            flag1 = false;
            vector3_3 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            vector3_4 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
              return index1;
          }
        }
        float line1 = TMP_TextUtilities.DistanceToLine(vector3_1, vector3_2, position);
        float line2 = TMP_TextUtilities.DistanceToLine(vector3_2, vector3_4, position);
        float line3 = TMP_TextUtilities.DistanceToLine(vector3_4, vector3_3, position);
        float line4 = TMP_TextUtilities.DistanceToLine(vector3_3, vector3_1, position);
        float num4 = (double) line1 >= (double) line2 ? line2 : line1;
        float num5 = (double) num4 >= (double) line3 ? line3 : num4;
        float num6 = (double) num5 >= (double) line4 ? line4 : num5;
        if ((double) num1 > (double) num6)
        {
          num1 = num6;
          num2 = index1;
        }
      }
      return num2;
    }

    public static int FindNearestWord(TextMeshPro text, Vector3 position, Camera camera)
    {
      Transform transform = text.transform;
      float num1 = float.PositiveInfinity;
      int num2 = 0;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle(transform, (Vector2) position, camera, out position);
      for (int index1 = 0; index1 < text.textInfo.wordCount; ++index1)
      {
        TMP_WordInfo tmpWordInfo = text.textInfo.wordInfo[index1];
        bool flag1 = false;
        Vector3 vector3_1 = Vector3.zero;
        Vector3 vector3_2 = Vector3.zero;
        Vector3 vector3_3 = Vector3.zero;
        Vector3 vector3_4 = Vector3.zero;
        for (int index2 = 0; index2 < tmpWordInfo.characterCount; ++index2)
        {
          int index3 = tmpWordInfo.firstCharacterIndex + index2;
          TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index3];
          int num3 = (int) tmpCharacterInfo.lineNumber;
          bool flag2 = index3 <= text.maxVisibleCharacters && (int) tmpCharacterInfo.lineNumber <= text.maxVisibleLines && (text.OverflowMode != TextOverflowModes.Page || (int) tmpCharacterInfo.pageNumber + 1 == text.pageToDisplay);
          if (!flag1 && flag2)
          {
            flag1 = true;
            vector3_1 = transform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.descender, 0.0f));
            vector3_2 = transform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.ascender, 0.0f));
            if (tmpWordInfo.characterCount == 1)
            {
              flag1 = false;
              vector3_3 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
              vector3_4 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
              if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
                return index1;
            }
          }
          if (flag1 && index2 == tmpWordInfo.characterCount - 1)
          {
            flag1 = false;
            vector3_3 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            vector3_4 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
              return index1;
          }
          else if (flag1 && num3 != (int) text.textInfo.characterInfo[index3 + 1].lineNumber)
          {
            flag1 = false;
            vector3_3 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            vector3_4 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
              return index1;
          }
        }
        float line1 = TMP_TextUtilities.DistanceToLine(vector3_1, vector3_2, position);
        float line2 = TMP_TextUtilities.DistanceToLine(vector3_2, vector3_4, position);
        float line3 = TMP_TextUtilities.DistanceToLine(vector3_4, vector3_3, position);
        float line4 = TMP_TextUtilities.DistanceToLine(vector3_3, vector3_1, position);
        float num4 = (double) line1 >= (double) line2 ? line2 : line1;
        float num5 = (double) num4 >= (double) line3 ? line3 : num4;
        float num6 = (double) num5 >= (double) line4 ? line4 : num5;
        if ((double) num1 > (double) num6)
        {
          num1 = num6;
          num2 = index1;
        }
      }
      return num2;
    }

    public static int FindIntersectingLink(TextMeshProUGUI text, Vector3 position, Camera camera)
    {
      Transform transform = text.transform;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle(transform, (Vector2) position, camera, out position);
      for (int index1 = 0; index1 < text.textInfo.linkCount; ++index1)
      {
        TMP_LinkInfo tmpLinkInfo = text.textInfo.linkInfo[index1];
        bool flag = false;
        Vector3 a = Vector3.zero;
        Vector3 b = Vector3.zero;
        Vector3 zero1 = Vector3.zero;
        Vector3 zero2 = Vector3.zero;
        for (int index2 = 0; index2 < tmpLinkInfo.linkTextLength; ++index2)
        {
          int index3 = tmpLinkInfo.linkTextfirstCharacterIndex + index2;
          TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index3];
          int num = (int) tmpCharacterInfo.lineNumber;
          if (!flag)
          {
            flag = true;
            a = transform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.descender, 0.0f));
            b = transform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.ascender, 0.0f));
            if (tmpLinkInfo.linkTextLength == 1)
            {
              flag = false;
              Vector3 d = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
              Vector3 c = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
              if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
                return index1;
            }
          }
          if (flag && index2 == tmpLinkInfo.linkTextLength - 1)
          {
            flag = false;
            Vector3 d = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            Vector3 c = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
              return index1;
          }
          else if (flag && num != (int) text.textInfo.characterInfo[index3 + 1].lineNumber)
          {
            flag = false;
            Vector3 d = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            Vector3 c = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
              return index1;
          }
        }
      }
      return -1;
    }

    public static int FindIntersectingLink(TextMeshPro text, Vector3 position, Camera camera)
    {
      Transform transform = text.transform;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle(transform, (Vector2) position, camera, out position);
      for (int index1 = 0; index1 < text.textInfo.linkCount; ++index1)
      {
        TMP_LinkInfo tmpLinkInfo = text.textInfo.linkInfo[index1];
        bool flag = false;
        Vector3 a = Vector3.zero;
        Vector3 b = Vector3.zero;
        Vector3 zero1 = Vector3.zero;
        Vector3 zero2 = Vector3.zero;
        for (int index2 = 0; index2 < tmpLinkInfo.linkTextLength; ++index2)
        {
          int index3 = tmpLinkInfo.linkTextfirstCharacterIndex + index2;
          TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index3];
          int num = (int) tmpCharacterInfo.lineNumber;
          if (!flag)
          {
            flag = true;
            a = transform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.descender, 0.0f));
            b = transform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.ascender, 0.0f));
            if (tmpLinkInfo.linkTextLength == 1)
            {
              flag = false;
              Vector3 d = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
              Vector3 c = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
              if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
                return index1;
            }
          }
          if (flag && index2 == tmpLinkInfo.linkTextLength - 1)
          {
            flag = false;
            Vector3 d = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            Vector3 c = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
              return index1;
          }
          else if (flag && num != (int) text.textInfo.characterInfo[index3 + 1].lineNumber)
          {
            flag = false;
            Vector3 d = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            Vector3 c = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, a, b, c, d))
              return index1;
          }
        }
      }
      return -1;
    }

    public static int FindNearestLink(TextMeshProUGUI text, Vector3 position, Camera camera)
    {
      RectTransform rectTransform = text.rectTransform;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle((Transform) rectTransform, (Vector2) position, camera, out position);
      float num1 = float.PositiveInfinity;
      int num2 = 0;
      for (int index1 = 0; index1 < text.textInfo.linkCount; ++index1)
      {
        TMP_LinkInfo tmpLinkInfo = text.textInfo.linkInfo[index1];
        bool flag = false;
        Vector3 vector3_1 = Vector3.zero;
        Vector3 vector3_2 = Vector3.zero;
        Vector3 vector3_3 = Vector3.zero;
        Vector3 vector3_4 = Vector3.zero;
        for (int index2 = 0; index2 < tmpLinkInfo.linkTextLength; ++index2)
        {
          int index3 = tmpLinkInfo.linkTextfirstCharacterIndex + index2;
          TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index3];
          int num3 = (int) tmpCharacterInfo.lineNumber;
          if (!flag)
          {
            flag = true;
            vector3_1 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.descender, 0.0f));
            vector3_2 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.ascender, 0.0f));
            if (tmpLinkInfo.linkTextLength == 1)
            {
              flag = false;
              vector3_3 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
              vector3_4 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
              if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
                return index1;
            }
          }
          if (flag && index2 == tmpLinkInfo.linkTextLength - 1)
          {
            flag = false;
            vector3_3 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            vector3_4 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
              return index1;
          }
          else if (flag && num3 != (int) text.textInfo.characterInfo[index3 + 1].lineNumber)
          {
            flag = false;
            vector3_3 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            vector3_4 = rectTransform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
              return index1;
          }
        }
        float line1 = TMP_TextUtilities.DistanceToLine(vector3_1, vector3_2, position);
        float line2 = TMP_TextUtilities.DistanceToLine(vector3_2, vector3_4, position);
        float line3 = TMP_TextUtilities.DistanceToLine(vector3_4, vector3_3, position);
        float line4 = TMP_TextUtilities.DistanceToLine(vector3_3, vector3_1, position);
        float num4 = (double) line1 >= (double) line2 ? line2 : line1;
        float num5 = (double) num4 >= (double) line3 ? line3 : num4;
        float num6 = (double) num5 >= (double) line4 ? line4 : num5;
        if ((double) num1 > (double) num6)
        {
          num1 = num6;
          num2 = index1;
        }
      }
      return num2;
    }

    public static int FindNearestLink(TextMeshPro text, Vector3 position, Camera camera)
    {
      Transform transform = text.transform;
      TMP_TextUtilities.ScreenPointToWorldPointInRectangle(transform, (Vector2) position, camera, out position);
      float num1 = float.PositiveInfinity;
      int num2 = 0;
      for (int index1 = 0; index1 < text.textInfo.linkCount; ++index1)
      {
        TMP_LinkInfo tmpLinkInfo = text.textInfo.linkInfo[index1];
        bool flag = false;
        Vector3 vector3_1 = Vector3.zero;
        Vector3 vector3_2 = Vector3.zero;
        Vector3 vector3_3 = Vector3.zero;
        Vector3 vector3_4 = Vector3.zero;
        for (int index2 = 0; index2 < tmpLinkInfo.linkTextLength; ++index2)
        {
          int index3 = tmpLinkInfo.linkTextfirstCharacterIndex + index2;
          TMP_CharacterInfo tmpCharacterInfo = text.textInfo.characterInfo[index3];
          int num3 = (int) tmpCharacterInfo.lineNumber;
          if (!flag)
          {
            flag = true;
            vector3_1 = transform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.descender, 0.0f));
            vector3_2 = transform.TransformPoint(new Vector3(tmpCharacterInfo.bottomLeft.x, tmpCharacterInfo.ascender, 0.0f));
            if (tmpLinkInfo.linkTextLength == 1)
            {
              flag = false;
              vector3_3 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
              vector3_4 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
              if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
                return index1;
            }
          }
          if (flag && index2 == tmpLinkInfo.linkTextLength - 1)
          {
            flag = false;
            vector3_3 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            vector3_4 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
              return index1;
          }
          else if (flag && num3 != (int) text.textInfo.characterInfo[index3 + 1].lineNumber)
          {
            flag = false;
            vector3_3 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.descender, 0.0f));
            vector3_4 = transform.TransformPoint(new Vector3(tmpCharacterInfo.topRight.x, tmpCharacterInfo.ascender, 0.0f));
            if (TMP_TextUtilities.PointIntersectRectangle(position, vector3_1, vector3_2, vector3_4, vector3_3))
              return index1;
          }
        }
        float line1 = TMP_TextUtilities.DistanceToLine(vector3_1, vector3_2, position);
        float line2 = TMP_TextUtilities.DistanceToLine(vector3_2, vector3_4, position);
        float line3 = TMP_TextUtilities.DistanceToLine(vector3_4, vector3_3, position);
        float line4 = TMP_TextUtilities.DistanceToLine(vector3_3, vector3_1, position);
        float num4 = (double) line1 >= (double) line2 ? line2 : line1;
        float num5 = (double) num4 >= (double) line3 ? line3 : num4;
        float num6 = (double) num5 >= (double) line4 ? line4 : num5;
        if ((double) num1 > (double) num6)
        {
          num1 = num6;
          num2 = index1;
        }
      }
      return num2;
    }

    private static bool PointIntersectRectangle(Vector3 m, Vector3 a, Vector3 b, Vector3 c, Vector3 d)
    {
      Vector3 vector3_1 = b - a;
      Vector3 rhs1 = m - a;
      Vector3 vector3_2 = c - b;
      Vector3 rhs2 = m - b;
      float num1 = Vector3.Dot(vector3_1, rhs1);
      float num2 = Vector3.Dot(vector3_2, rhs2);
      if (0.0 <= (double) num1 && (double) num1 <= (double) Vector3.Dot(vector3_1, vector3_1) && 0.0 <= (double) num2)
        return (double) num2 <= (double) Vector3.Dot(vector3_2, vector3_2);
      return false;
    }

    public static bool ScreenPointToWorldPointInRectangle(Transform transform, Vector2 screenPoint, Camera cam, out Vector3 worldPoint)
    {
      worldPoint = (Vector3) Vector2.zero;
      Ray ray = RectTransformUtility.ScreenPointToRay(cam, screenPoint);
      float enter;
      if (!new Plane(transform.rotation * Vector3.back, transform.position).Raycast(ray, out enter))
        return false;
      worldPoint = ray.GetPoint(enter);
      return true;
    }

    private static bool IntersectLinePlane(TMP_TextUtilities.LineSegment line, Vector3 point, Vector3 normal, out Vector3 intersectingPoint)
    {
      intersectingPoint = Vector3.zero;
      Vector3 rhs1 = line.Point2 - line.Point1;
      Vector3 rhs2 = line.Point1 - point;
      float f = Vector3.Dot(normal, rhs1);
      float num1 = -Vector3.Dot(normal, rhs2);
      if ((double) Mathf.Abs(f) < (double) Mathf.Epsilon)
        return (double) num1 == 0.0;
      float num2 = num1 / f;
      if ((double) num2 < 0.0 || (double) num2 > 1.0)
        return false;
      intersectingPoint = line.Point1 + num2 * rhs1;
      return true;
    }

    public static float DistanceToLine(Vector3 a, Vector3 b, Vector3 point)
    {
      Vector3 vector3_1 = b - a;
      Vector3 vector3_2 = a - point;
      float num = Vector3.Dot(vector3_1, vector3_2);
      if ((double) num > 0.0)
        return Vector3.Dot(vector3_2, vector3_2);
      Vector3 vector3_3 = point - b;
      if ((double) Vector3.Dot(vector3_1, vector3_3) > 0.0)
        return Vector3.Dot(vector3_3, vector3_3);
      Vector3 vector3_4 = vector3_2 - vector3_1 * (num / Vector3.Dot(vector3_1, vector3_1));
      return Vector3.Dot(vector3_4, vector3_4);
    }

    public static char ToLowerFast(char c)
    {
      return "-------------------------------- !-#$%&-()*+,-./0123456789:;<=>?@abcdefghijklmnopqrstuvwxyz[-]^_`abcdefghijklmnopqrstuvwxyz{|}~-"[(int) c];
    }

    public static char ToUpperFast(char c)
    {
      return "-------------------------------- !-#$%&-()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[-]^_`ABCDEFGHIJKLMNOPQRSTUVWXYZ{|}~-"[(int) c];
    }

    public static int GetSimpleHashCode(string s)
    {
      int num = 0;
      for (int index = 0; index < s.Length; ++index)
        num = (num << 5) + num ^ (int) s[index];
      return num;
    }

    public static uint GetSimpleHashCodeLowercase(string s)
    {
      uint num = 5381;
      for (int index = 0; index < s.Length; ++index)
        num = (num << 5) + num ^ (uint) TMP_TextUtilities.ToLowerFast(s[index]);
      return num;
    }

    private struct LineSegment
    {
      public Vector3 Point1;
      public Vector3 Point2;

      public LineSegment(Vector3 p1, Vector3 p2)
      {
        this.Point1 = p1;
        this.Point2 = p2;
      }
    }
  }
}
