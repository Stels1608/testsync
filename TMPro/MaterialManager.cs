﻿// Decompiled with JetBrains decompiler
// Type: TMPro.MaterialManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace TMPro
{
  public static class MaterialManager
  {
    private static List<MaterialManager.MaskingMaterial> m_materialList = new List<MaterialManager.MaskingMaterial>();
    private static Mask[] m_maskComponents;

    public static Material GetStencilMaterial(Material baseMaterial, int stencilID)
    {
      if (!baseMaterial.HasProperty(ShaderUtilities.ID_StencilID))
      {
        Debug.LogWarning((object) "Selected Shader does not support Stencil Masking. Please select the Distance Field or Mobile Distance Field Shader.");
        return baseMaterial;
      }
      int instanceId = baseMaterial.GetInstanceID();
      for (int index = 0; index < MaterialManager.m_materialList.Count; ++index)
      {
        if (MaterialManager.m_materialList[index].baseMaterial.GetInstanceID() == instanceId && MaterialManager.m_materialList[index].stencilID == stencilID)
        {
          ++MaterialManager.m_materialList[index].count;
          return MaterialManager.m_materialList[index].stencilMaterial;
        }
      }
      Material material1 = new Material(baseMaterial);
      material1.hideFlags = HideFlags.HideAndDontSave;
      Material material2 = material1;
      string str = material2.name + " Masking ID:" + (object) stencilID;
      material2.name = str;
      material1.shaderKeywords = baseMaterial.shaderKeywords;
      ShaderUtilities.GetShaderPropertyIDs();
      material1.SetFloat(ShaderUtilities.ID_StencilID, (float) stencilID);
      material1.SetFloat(ShaderUtilities.ID_StencilComp, 4f);
      MaterialManager.m_materialList.Add(new MaterialManager.MaskingMaterial()
      {
        baseMaterial = baseMaterial,
        stencilMaterial = material1,
        stencilID = stencilID,
        count = 1
      });
      return material1;
    }

    public static void ReleaseStencilMaterial(Material stencilMaterial)
    {
      int instanceId = stencilMaterial.GetInstanceID();
      for (int index = 0; index < MaterialManager.m_materialList.Count; ++index)
      {
        if (MaterialManager.m_materialList[index].stencilMaterial.GetInstanceID() == instanceId)
        {
          if (MaterialManager.m_materialList[index].count > 1)
          {
            --MaterialManager.m_materialList[index].count;
            break;
          }
          UnityEngine.Object.DestroyImmediate((UnityEngine.Object) MaterialManager.m_materialList[index].stencilMaterial);
          MaterialManager.m_materialList.RemoveAt(index);
          stencilMaterial = (Material) null;
          break;
        }
      }
    }

    public static Material GetBaseMaterial(Material stencilMaterial)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      int index = MaterialManager.m_materialList.FindIndex(new Predicate<MaterialManager.MaskingMaterial>(new MaterialManager.\u003CGetBaseMaterial\u003Ec__AnonStorey11F() { stencilMaterial = stencilMaterial }.\u003C\u003Em__2B4));
      if (index == -1)
        return (Material) null;
      return MaterialManager.m_materialList[index].baseMaterial;
    }

    public static Material SetStencil(Material material, int stencilID)
    {
      material.SetFloat(ShaderUtilities.ID_StencilID, (float) stencilID);
      if (stencilID == 0)
        material.SetFloat(ShaderUtilities.ID_StencilComp, 8f);
      else
        material.SetFloat(ShaderUtilities.ID_StencilComp, 4f);
      return material;
    }

    public static void AddMaskingMaterial(Material baseMaterial, Material stencilMaterial, int stencilID)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      MaterialManager.\u003CAddMaskingMaterial\u003Ec__AnonStorey120 materialCAnonStorey120 = new MaterialManager.\u003CAddMaskingMaterial\u003Ec__AnonStorey120();
      // ISSUE: reference to a compiler-generated field
      materialCAnonStorey120.stencilMaterial = stencilMaterial;
      // ISSUE: reference to a compiler-generated method
      int index = MaterialManager.m_materialList.FindIndex(new Predicate<MaterialManager.MaskingMaterial>(materialCAnonStorey120.\u003C\u003Em__2B5));
      if (index == -1)
      {
        // ISSUE: reference to a compiler-generated field
        MaterialManager.m_materialList.Add(new MaterialManager.MaskingMaterial()
        {
          baseMaterial = baseMaterial,
          stencilMaterial = materialCAnonStorey120.stencilMaterial,
          stencilID = stencilID,
          count = 1
        });
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        materialCAnonStorey120.stencilMaterial = MaterialManager.m_materialList[index].stencilMaterial;
        ++MaterialManager.m_materialList[index].count;
      }
    }

    public static void RemoveStencilMaterial(Material stencilMaterial)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      int index = MaterialManager.m_materialList.FindIndex(new Predicate<MaterialManager.MaskingMaterial>(new MaterialManager.\u003CRemoveStencilMaterial\u003Ec__AnonStorey121() { stencilMaterial = stencilMaterial }.\u003C\u003Em__2B6));
      if (index == -1)
        return;
      MaterialManager.m_materialList.RemoveAt(index);
    }

    public static void ReleaseBaseMaterial(Material baseMaterial)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      MaterialManager.\u003CReleaseBaseMaterial\u003Ec__AnonStorey122 materialCAnonStorey122 = new MaterialManager.\u003CReleaseBaseMaterial\u003Ec__AnonStorey122();
      // ISSUE: reference to a compiler-generated field
      materialCAnonStorey122.baseMaterial = baseMaterial;
      // ISSUE: reference to a compiler-generated method
      int index = MaterialManager.m_materialList.FindIndex(new Predicate<MaterialManager.MaskingMaterial>(materialCAnonStorey122.\u003C\u003Em__2B7));
      if (index == -1)
      {
        // ISSUE: reference to a compiler-generated field
        Debug.Log((object) ("No Masking Material exists for " + materialCAnonStorey122.baseMaterial.name));
      }
      else if (MaterialManager.m_materialList[index].count > 1)
      {
        --MaterialManager.m_materialList[index].count;
        Debug.Log((object) ("Removed (1) reference to " + MaterialManager.m_materialList[index].stencilMaterial.name + ". There are " + (object) MaterialManager.m_materialList[index].count + " references left."));
      }
      else
      {
        Debug.Log((object) ("Removed last reference to " + MaterialManager.m_materialList[index].stencilMaterial.name + " with ID " + (object) MaterialManager.m_materialList[index].stencilMaterial.GetInstanceID()));
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) MaterialManager.m_materialList[index].stencilMaterial);
        MaterialManager.m_materialList.RemoveAt(index);
      }
    }

    public static void ClearMaterials()
    {
      if (MaterialManager.m_materialList.Count<MaterialManager.MaskingMaterial>() == 0)
      {
        Debug.Log((object) "Material List has already been cleared.");
      }
      else
      {
        for (int index = 0; index < MaterialManager.m_materialList.Count<MaterialManager.MaskingMaterial>(); ++index)
        {
          UnityEngine.Object.DestroyImmediate((UnityEngine.Object) MaterialManager.m_materialList[index].stencilMaterial);
          MaterialManager.m_materialList.RemoveAt(index);
        }
      }
    }

    public static int GetStencilID(GameObject obj)
    {
      int num = 0;
      MaterialManager.m_maskComponents = obj.GetComponentsInParent<Mask>();
      for (int index = 0; index < MaterialManager.m_maskComponents.Length; ++index)
      {
        if (MaterialManager.m_maskComponents[index].MaskEnabled())
          ++num;
      }
      return Mathf.Min((1 << num) - 1, (int) byte.MaxValue);
    }

    private class MaskingMaterial
    {
      public Material baseMaterial;
      public Material stencilMaterial;
      public int count;
      public int stencilID;
    }
  }
}
