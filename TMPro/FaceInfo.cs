﻿// Decompiled with JetBrains decompiler
// Type: TMPro.FaceInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

namespace TMPro
{
  [Serializable]
  public class FaceInfo
  {
    public string Name;
    public float PointSize;
    public float Scale;
    public int CharacterCount;
    public float LineHeight;
    public float Baseline;
    public float Ascender;
    public float Descender;
    public float CenterLine;
    public float SuperscriptOffset;
    public float SubscriptOffset;
    public float SubSize;
    public float Underline;
    public float UnderlineThickness;
    public float TabWidth;
    public float Padding;
    public float AtlasWidth;
    public float AtlasHeight;
  }
}
