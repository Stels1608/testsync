﻿// Decompiled with JetBrains decompiler
// Type: TMPro.InlineGraphic
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace TMPro
{
  public class InlineGraphic : MaskableGraphic
  {
    public Texture texture;
    private InlineGraphicManager m_manager;
    private RectTransform m_RectTransform;
    private RectTransform m_ParentRectTransform;

    public override Texture mainTexture
    {
      get
      {
        if ((Object) this.texture == (Object) null)
          return (Texture) Graphic.s_WhiteTexture;
        return this.texture;
      }
    }

    protected override void Awake()
    {
      this.m_manager = this.GetComponentInParent<InlineGraphicManager>();
    }

    protected override void OnEnable()
    {
      if ((Object) this.m_RectTransform == (Object) null)
        this.m_RectTransform = this.gameObject.GetComponent<RectTransform>();
      if (!((Object) this.m_manager != (Object) null) || !((Object) this.m_manager.spriteAsset != (Object) null))
        return;
      this.texture = this.m_manager.spriteAsset.spriteSheet;
    }

    protected override void OnDisable()
    {
      base.OnDisable();
    }

    protected override void OnTransformParentChanged()
    {
    }

    protected override void OnRectTransformDimensionsChange()
    {
      if ((Object) this.m_RectTransform == (Object) null)
        this.m_RectTransform = this.gameObject.GetComponent<RectTransform>();
      if ((Object) this.m_ParentRectTransform == (Object) null)
        this.m_ParentRectTransform = this.m_RectTransform.parent.GetComponent<RectTransform>();
      if (!(this.m_RectTransform.pivot != this.m_ParentRectTransform.pivot))
        return;
      this.m_RectTransform.pivot = this.m_ParentRectTransform.pivot;
    }

    public new void UpdateMaterial()
    {
      base.UpdateMaterial();
    }

    protected override void UpdateGeometry()
    {
    }
  }
}
