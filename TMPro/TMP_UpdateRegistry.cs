﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_UpdateRegistry
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TMPro
{
  public class TMP_UpdateRegistry
  {
    private readonly List<ICanvasElement> m_LayoutRebuildQueue = new List<ICanvasElement>();
    private Dictionary<int, int> m_LayoutQueueLookup = new Dictionary<int, int>();
    private readonly List<ICanvasElement> m_GraphicRebuildQueue = new List<ICanvasElement>();
    private Dictionary<int, int> m_GraphicQueueLookup = new Dictionary<int, int>();
    private static TMP_UpdateRegistry s_Instance;

    public static TMP_UpdateRegistry instance
    {
      get
      {
        if (TMP_UpdateRegistry.s_Instance == null)
          TMP_UpdateRegistry.s_Instance = new TMP_UpdateRegistry();
        return TMP_UpdateRegistry.s_Instance;
      }
    }

    protected TMP_UpdateRegistry()
    {
      Canvas.willRenderCanvases += new Canvas.WillRenderCanvases(this.PerformUpdateForCanvasRendererObjects);
    }

    public static void RegisterCanvasElementForLayoutRebuild(ICanvasElement element)
    {
      TMP_UpdateRegistry.instance.InternalRegisterCanvasElementForLayoutRebuild(element);
    }

    private bool InternalRegisterCanvasElementForLayoutRebuild(ICanvasElement element)
    {
      int instanceId = (element as Object).GetInstanceID();
      if (this.m_LayoutQueueLookup.ContainsKey(instanceId))
        return false;
      this.m_LayoutQueueLookup[instanceId] = instanceId;
      this.m_LayoutRebuildQueue.Add(element);
      return true;
    }

    public static void RegisterCanvasElementForGraphicRebuild(ICanvasElement element)
    {
      TMP_UpdateRegistry.instance.InternalRegisterCanvasElementForGraphicRebuild(element);
    }

    private bool InternalRegisterCanvasElementForGraphicRebuild(ICanvasElement element)
    {
      int instanceId = (element as Object).GetInstanceID();
      if (this.m_GraphicQueueLookup.ContainsKey(instanceId))
        return false;
      this.m_GraphicQueueLookup[instanceId] = instanceId;
      this.m_GraphicRebuildQueue.Add(element);
      return true;
    }

    private void PerformUpdateForCanvasRendererObjects()
    {
      for (int index = 0; index < this.m_LayoutRebuildQueue.Count; ++index)
        TMP_UpdateRegistry.instance.m_LayoutRebuildQueue[index].Rebuild(CanvasUpdate.Prelayout);
      if (this.m_LayoutRebuildQueue.Count > 0)
      {
        this.m_LayoutRebuildQueue.Clear();
        this.m_LayoutQueueLookup.Clear();
      }
      for (int index = 0; index < this.m_GraphicRebuildQueue.Count; ++index)
        TMP_UpdateRegistry.instance.m_GraphicRebuildQueue[index].Rebuild(CanvasUpdate.PreRender);
      if (this.m_GraphicRebuildQueue.Count <= 0)
        return;
      this.m_GraphicRebuildQueue.Clear();
      this.m_GraphicQueueLookup.Clear();
    }

    private void PerformUpdateForMeshRendererObjects()
    {
      Debug.Log((object) "Perform update of MeshRenderer objects.");
    }

    public static void UnRegisterCanvasElementForRebuild(ICanvasElement element)
    {
      TMP_UpdateRegistry.instance.InternalUnRegisterCanvasElementForLayoutRebuild(element);
      TMP_UpdateRegistry.instance.InternalUnRegisterCanvasElementForGraphicRebuild(element);
    }

    private void InternalUnRegisterCanvasElementForLayoutRebuild(ICanvasElement element)
    {
      int instanceId = (element as Object).GetInstanceID();
      TMP_UpdateRegistry.instance.m_LayoutRebuildQueue.Remove(element);
      this.m_GraphicQueueLookup.Remove(instanceId);
    }

    private void InternalUnRegisterCanvasElementForGraphicRebuild(ICanvasElement element)
    {
      int instanceId = (element as Object).GetInstanceID();
      TMP_UpdateRegistry.instance.m_GraphicRebuildQueue.Remove(element);
      this.m_LayoutQueueLookup.Remove(instanceId);
    }
  }
}
