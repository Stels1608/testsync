﻿// Decompiled with JetBrains decompiler
// Type: TMPro.FastAction`1
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace TMPro
{
  public class FastAction<A>
  {
    private LinkedList<Action<A>> delegates = new LinkedList<Action<A>>();
    private Dictionary<Action<A>, LinkedListNode<Action<A>>> lookup = new Dictionary<Action<A>, LinkedListNode<Action<A>>>();

    public void Add(Action<A> rhs)
    {
      if (this.lookup.ContainsKey(rhs))
        return;
      this.lookup[rhs] = this.delegates.AddLast(rhs);
    }

    public void Remove(Action<A> rhs)
    {
      LinkedListNode<Action<A>> node;
      if (!this.lookup.TryGetValue(rhs, out node))
        return;
      this.lookup.Remove(rhs);
      this.delegates.Remove(node);
    }

    public void Call(A a)
    {
      for (LinkedListNode<Action<A>> linkedListNode = this.delegates.First; linkedListNode != null; linkedListNode = linkedListNode.Next)
        linkedListNode.Value(a);
    }
  }
}
