﻿// Decompiled with JetBrains decompiler
// Type: TMPro.InlineGraphicManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TMPro
{
  [ExecuteInEditMode]
  [AddComponentMenu("UI/Inline Graphics Manager", 13)]
  public class InlineGraphicManager : MonoBehaviour
  {
    [SerializeField]
    private TMP_SpriteAsset m_spriteAsset;
    [HideInInspector]
    [SerializeField]
    private InlineGraphic m_inlineGraphic;
    [HideInInspector]
    [SerializeField]
    private CanvasRenderer m_inlineGraphicCanvasRenderer;
    private UIVertex[] m_uiVertex;
    private RectTransform m_inlineGraphicRectTransform;
    private TMP_Text m_textComponent;
    private bool m_isInitialized;

    public TMP_SpriteAsset spriteAsset
    {
      get
      {
        return this.m_spriteAsset;
      }
      set
      {
        this.LoadSpriteAsset(value);
      }
    }

    public InlineGraphic inlineGraphic
    {
      get
      {
        return this.m_inlineGraphic;
      }
      set
      {
        if (!((UnityEngine.Object) this.m_inlineGraphic != (UnityEngine.Object) value))
          return;
        this.m_inlineGraphic = value;
      }
    }

    public CanvasRenderer canvasRenderer
    {
      get
      {
        return this.m_inlineGraphicCanvasRenderer;
      }
    }

    public UIVertex[] uiVertex
    {
      get
      {
        return this.m_uiVertex;
      }
    }

    private void Awake()
    {
      Debug.LogWarning((object) ("InlineGraphicManager component is now Obsolete and can safely be removed from Object [" + this.gameObject.name + "]."), (UnityEngine.Object) this);
    }

    private void OnEnable()
    {
      this.enabled = false;
    }

    private void OnDisable()
    {
    }

    private void OnDestroy()
    {
    }

    private void LoadSpriteAsset(TMP_SpriteAsset spriteAsset)
    {
      if ((UnityEngine.Object) spriteAsset == (UnityEngine.Object) null)
      {
        TMP_Settings tmpSettings = Resources.Load("TMP Settings") as TMP_Settings;
        spriteAsset = !((UnityEngine.Object) tmpSettings != (UnityEngine.Object) null) || !((UnityEngine.Object) tmpSettings.spriteAsset != (UnityEngine.Object) null) ? Resources.Load("Sprite Assets/Default Sprite Asset") as TMP_SpriteAsset : tmpSettings.spriteAsset;
      }
      this.m_spriteAsset = spriteAsset;
      this.m_inlineGraphic.texture = this.m_spriteAsset.spriteSheet;
      if (!((UnityEngine.Object) this.m_textComponent != (UnityEngine.Object) null) || !this.m_isInitialized)
        return;
      this.m_textComponent.havePropertiesChanged = true;
      this.m_textComponent.SetVerticesDirty();
    }

    public void AddInlineGraphicsChild()
    {
      if ((UnityEngine.Object) this.m_inlineGraphic != (UnityEngine.Object) null)
        return;
      GameObject gameObject = new GameObject("Inline Graphic");
      this.m_inlineGraphic = gameObject.AddComponent<InlineGraphic>();
      this.m_inlineGraphicRectTransform = gameObject.GetComponent<RectTransform>();
      this.m_inlineGraphicCanvasRenderer = gameObject.GetComponent<CanvasRenderer>();
      this.m_inlineGraphicRectTransform.SetParent(this.transform, false);
      this.m_inlineGraphicRectTransform.localPosition = Vector3.zero;
      this.m_inlineGraphicRectTransform.anchoredPosition3D = Vector3.zero;
      this.m_inlineGraphicRectTransform.sizeDelta = Vector2.zero;
      this.m_inlineGraphicRectTransform.anchorMin = Vector2.zero;
      this.m_inlineGraphicRectTransform.anchorMax = Vector2.one;
      this.m_textComponent = this.GetComponent<TMP_Text>();
    }

    public void AllocatedVertexBuffers(int size)
    {
      if ((UnityEngine.Object) this.m_inlineGraphic == (UnityEngine.Object) null)
      {
        this.AddInlineGraphicsChild();
        this.LoadSpriteAsset(this.m_spriteAsset);
      }
      if (this.m_uiVertex == null)
        this.m_uiVertex = new UIVertex[4];
      int num = size * 4;
      if (num <= this.m_uiVertex.Length)
        return;
      this.m_uiVertex = new UIVertex[Mathf.NextPowerOfTwo(num)];
    }

    public void UpdatePivot(Vector2 pivot)
    {
      if ((UnityEngine.Object) this.m_inlineGraphicRectTransform == (UnityEngine.Object) null)
        this.m_inlineGraphicRectTransform = this.m_inlineGraphic.GetComponent<RectTransform>();
      this.m_inlineGraphicRectTransform.pivot = pivot;
    }

    public void ClearUIVertex()
    {
      if (this.uiVertex == null || this.uiVertex.Length <= 0)
        return;
      Array.Clear((Array) this.uiVertex, 0, this.uiVertex.Length);
      this.m_inlineGraphicCanvasRenderer.Clear();
    }

    public void DrawSprite(UIVertex[] uiVertices, int spriteCount)
    {
      if ((UnityEngine.Object) this.m_inlineGraphicCanvasRenderer == (UnityEngine.Object) null)
        this.m_inlineGraphicCanvasRenderer = this.m_inlineGraphic.GetComponent<CanvasRenderer>();
      this.m_inlineGraphicCanvasRenderer.SetVertices(uiVertices, spriteCount * 4);
      this.m_inlineGraphic.UpdateMaterial();
    }

    public TMP_Sprite GetSprite(int index)
    {
      if ((UnityEngine.Object) this.m_spriteAsset == (UnityEngine.Object) null)
      {
        Debug.LogWarning((object) "No Sprite Asset is assigned.", (UnityEngine.Object) this);
        return (TMP_Sprite) null;
      }
      if (this.m_spriteAsset.spriteInfoList != null && index <= this.m_spriteAsset.spriteInfoList.Count - 1)
        return this.m_spriteAsset.spriteInfoList[index];
      Debug.LogWarning((object) "Sprite index exceeds the number of sprites in this Sprite Asset.", (UnityEngine.Object) this);
      return (TMP_Sprite) null;
    }

    public int GetSpriteIndexByHashCode(int hashCode)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      InlineGraphicManager.\u003CGetSpriteIndexByHashCode\u003Ec__AnonStorey11D codeCAnonStorey11D = new InlineGraphicManager.\u003CGetSpriteIndexByHashCode\u003Ec__AnonStorey11D();
      // ISSUE: reference to a compiler-generated field
      codeCAnonStorey11D.hashCode = hashCode;
      if (!((UnityEngine.Object) this.m_spriteAsset == (UnityEngine.Object) null) && this.m_spriteAsset.spriteInfoList != null)
      {
        // ISSUE: reference to a compiler-generated method
        return this.m_spriteAsset.spriteInfoList.FindIndex(new Predicate<TMP_Sprite>(codeCAnonStorey11D.\u003C\u003Em__2B2));
      }
      Debug.LogWarning((object) "No Sprite Asset is assigned.", (UnityEngine.Object) this);
      return -1;
    }

    public int GetSpriteIndexByIndex(int index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      InlineGraphicManager.\u003CGetSpriteIndexByIndex\u003Ec__AnonStorey11E indexCAnonStorey11E = new InlineGraphicManager.\u003CGetSpriteIndexByIndex\u003Ec__AnonStorey11E();
      // ISSUE: reference to a compiler-generated field
      indexCAnonStorey11E.index = index;
      if (!((UnityEngine.Object) this.m_spriteAsset == (UnityEngine.Object) null) && this.m_spriteAsset.spriteInfoList != null)
      {
        // ISSUE: reference to a compiler-generated method
        return this.m_spriteAsset.spriteInfoList.FindIndex(new Predicate<TMP_Sprite>(indexCAnonStorey11E.\u003C\u003Em__2B3));
      }
      Debug.LogWarning((object) "No Sprite Asset is assigned.", (UnityEngine.Object) this);
      return -1;
    }

    public void SetUIVertex(UIVertex[] uiVertex)
    {
      this.m_uiVertex = uiVertex;
    }
  }
}
