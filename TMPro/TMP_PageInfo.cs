﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_PageInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace TMPro
{
  public struct TMP_PageInfo
  {
    public int firstCharacterIndex;
    public int lastCharacterIndex;
    public float ascender;
    public float baseLine;
    public float descender;
  }
}
