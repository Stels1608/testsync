﻿// Decompiled with JetBrains decompiler
// Type: TMPro.ShaderUtilities
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TMPro
{
  public static class ShaderUtilities
  {
    public static string Keyword_Bevel = "BEVEL_ON";
    public static string Keyword_Glow = "GLOW_ON";
    public static string Keyword_Underlay = "UNDERLAY_ON";
    public static string Keyword_Ratios = "RATIOS_OFF";
    public static string Keyword_MASK_SOFT = "MASK_SOFT";
    public static string Keyword_MASK_HARD = "MASK_HARD";
    public static string Keyword_MASK_TEX = "MASK_TEX";
    public static string ShaderTag_ZTestMode = "unity_GUIZTestMode";
    public static string ShaderTag_CullMode = "_CullMode";
    private static float m_clamp = 1f;
    public static int ID_MainTex;
    public static int ID_FaceTex;
    public static int ID_FaceColor;
    public static int ID_FaceDilate;
    public static int ID_Shininess;
    public static int ID_UnderlayColor;
    public static int ID_UnderlayOffsetX;
    public static int ID_UnderlayOffsetY;
    public static int ID_UnderlayDilate;
    public static int ID_UnderlaySoftness;
    public static int ID_WeightNormal;
    public static int ID_WeightBold;
    public static int ID_OutlineTex;
    public static int ID_OutlineWidth;
    public static int ID_OutlineSoftness;
    public static int ID_OutlineColor;
    public static int ID_GradientScale;
    public static int ID_ScaleX;
    public static int ID_ScaleY;
    public static int ID_PerspectiveFilter;
    public static int ID_TextureWidth;
    public static int ID_TextureHeight;
    public static int ID_BevelAmount;
    public static int ID_GlowColor;
    public static int ID_GlowOffset;
    public static int ID_GlowPower;
    public static int ID_GlowOuter;
    public static int ID_LightAngle;
    public static int ID_EnvMap;
    public static int ID_EnvMatrix;
    public static int ID_EnvMatrixRotation;
    public static int ID_MaskCoord;
    public static int ID_ClipRect;
    public static int ID_MaskSoftnessX;
    public static int ID_MaskSoftnessY;
    public static int ID_VertexOffsetX;
    public static int ID_VertexOffsetY;
    public static int ID_UseClipRect;
    public static int ID_StencilID;
    public static int ID_StencilOp;
    public static int ID_StencilComp;
    public static int ID_StencilReadMask;
    public static int ID_StencilWriteMask;
    public static int ID_ShaderFlags;
    public static int ID_ScaleRatio_A;
    public static int ID_ScaleRatio_B;
    public static int ID_ScaleRatio_C;
    public static bool isInitialized;

    public static void GetShaderPropertyIDs()
    {
      if (ShaderUtilities.isInitialized)
        return;
      ShaderUtilities.isInitialized = true;
      ShaderUtilities.ID_MainTex = Shader.PropertyToID("_MainTex");
      ShaderUtilities.ID_FaceTex = Shader.PropertyToID("_FaceTex");
      ShaderUtilities.ID_FaceColor = Shader.PropertyToID("_FaceColor");
      ShaderUtilities.ID_FaceDilate = Shader.PropertyToID("_FaceDilate");
      ShaderUtilities.ID_Shininess = Shader.PropertyToID("_FaceShininess");
      ShaderUtilities.ID_UnderlayColor = Shader.PropertyToID("_UnderlayColor");
      ShaderUtilities.ID_UnderlayOffsetX = Shader.PropertyToID("_UnderlayOffsetX");
      ShaderUtilities.ID_UnderlayOffsetY = Shader.PropertyToID("_UnderlayOffsetY");
      ShaderUtilities.ID_UnderlayDilate = Shader.PropertyToID("_UnderlayDilate");
      ShaderUtilities.ID_UnderlaySoftness = Shader.PropertyToID("_UnderlaySoftness");
      ShaderUtilities.ID_WeightNormal = Shader.PropertyToID("_WeightNormal");
      ShaderUtilities.ID_WeightBold = Shader.PropertyToID("_WeightBold");
      ShaderUtilities.ID_OutlineTex = Shader.PropertyToID("_OutlineTex");
      ShaderUtilities.ID_OutlineWidth = Shader.PropertyToID("_OutlineWidth");
      ShaderUtilities.ID_OutlineSoftness = Shader.PropertyToID("_OutlineSoftness");
      ShaderUtilities.ID_OutlineColor = Shader.PropertyToID("_OutlineColor");
      ShaderUtilities.ID_GradientScale = Shader.PropertyToID("_GradientScale");
      ShaderUtilities.ID_ScaleX = Shader.PropertyToID("_ScaleX");
      ShaderUtilities.ID_ScaleY = Shader.PropertyToID("_ScaleY");
      ShaderUtilities.ID_PerspectiveFilter = Shader.PropertyToID("_PerspectiveFilter");
      ShaderUtilities.ID_TextureWidth = Shader.PropertyToID("_TextureWidth");
      ShaderUtilities.ID_TextureHeight = Shader.PropertyToID("_TextureHeight");
      ShaderUtilities.ID_BevelAmount = Shader.PropertyToID("_Bevel");
      ShaderUtilities.ID_LightAngle = Shader.PropertyToID("_LightAngle");
      ShaderUtilities.ID_EnvMap = Shader.PropertyToID("_Cube");
      ShaderUtilities.ID_EnvMatrix = Shader.PropertyToID("_EnvMatrix");
      ShaderUtilities.ID_EnvMatrixRotation = Shader.PropertyToID("_EnvMatrixRotation");
      ShaderUtilities.ID_GlowColor = Shader.PropertyToID("_GlowColor");
      ShaderUtilities.ID_GlowOffset = Shader.PropertyToID("_GlowOffset");
      ShaderUtilities.ID_GlowPower = Shader.PropertyToID("_GlowPower");
      ShaderUtilities.ID_GlowOuter = Shader.PropertyToID("_GlowOuter");
      ShaderUtilities.ID_MaskCoord = Shader.PropertyToID("_MaskCoord");
      ShaderUtilities.ID_ClipRect = Shader.PropertyToID("_ClipRect");
      ShaderUtilities.ID_UseClipRect = Shader.PropertyToID("_UseClipRect");
      ShaderUtilities.ID_MaskSoftnessX = Shader.PropertyToID("_MaskSoftnessX");
      ShaderUtilities.ID_MaskSoftnessY = Shader.PropertyToID("_MaskSoftnessY");
      ShaderUtilities.ID_VertexOffsetX = Shader.PropertyToID("_VertexOffsetX");
      ShaderUtilities.ID_VertexOffsetY = Shader.PropertyToID("_VertexOffsetY");
      ShaderUtilities.ID_StencilID = Shader.PropertyToID("_Stencil");
      ShaderUtilities.ID_StencilOp = Shader.PropertyToID("_StencilOp");
      ShaderUtilities.ID_StencilComp = Shader.PropertyToID("_StencilComp");
      ShaderUtilities.ID_StencilReadMask = Shader.PropertyToID("_StencilReadMask");
      ShaderUtilities.ID_StencilWriteMask = Shader.PropertyToID("_StencilWriteMask");
      ShaderUtilities.ID_ShaderFlags = Shader.PropertyToID("_ShaderFlags");
      ShaderUtilities.ID_ScaleRatio_A = Shader.PropertyToID("_ScaleRatioA");
      ShaderUtilities.ID_ScaleRatio_B = Shader.PropertyToID("_ScaleRatioB");
      ShaderUtilities.ID_ScaleRatio_C = Shader.PropertyToID("_ScaleRatioC");
    }

    public static void UpdateShaderRatios(Material mat, bool isBold)
    {
      bool flag = !((IEnumerable<string>) mat.shaderKeywords).Contains<string>(ShaderUtilities.Keyword_Ratios);
      float float1 = mat.GetFloat(ShaderUtilities.ID_GradientScale);
      float float2 = mat.GetFloat(ShaderUtilities.ID_FaceDilate);
      float float3 = mat.GetFloat(ShaderUtilities.ID_OutlineWidth);
      float float4 = mat.GetFloat(ShaderUtilities.ID_OutlineSoftness);
      float num1 = isBold ? mat.GetFloat(ShaderUtilities.ID_WeightBold) * 2f / float1 : mat.GetFloat(ShaderUtilities.ID_WeightNormal) * 2f / float1;
      float num2 = Mathf.Max(1f, num1 + float2 + float3 + float4);
      float num3 = !flag ? 1f : (float) (((double) float1 - (double) ShaderUtilities.m_clamp) / ((double) float1 * (double) num2));
      mat.SetFloat(ShaderUtilities.ID_ScaleRatio_A, num3);
      if (mat.HasProperty(ShaderUtilities.ID_GlowOffset))
      {
        float float5 = mat.GetFloat(ShaderUtilities.ID_GlowOffset);
        float float6 = mat.GetFloat(ShaderUtilities.ID_GlowOuter);
        float num4 = (float) (((double) num1 + (double) float2) * ((double) float1 - (double) ShaderUtilities.m_clamp));
        float num5 = Mathf.Max(1f, float5 + float6);
        float num6 = !flag ? 1f : Mathf.Max(0.0f, float1 - ShaderUtilities.m_clamp - num4) / (float1 * num5);
        mat.SetFloat(ShaderUtilities.ID_ScaleRatio_B, num6);
      }
      if (!mat.HasProperty(ShaderUtilities.ID_UnderlayOffsetX))
        return;
      float float7 = mat.GetFloat(ShaderUtilities.ID_UnderlayOffsetX);
      float float8 = mat.GetFloat(ShaderUtilities.ID_UnderlayOffsetY);
      float float9 = mat.GetFloat(ShaderUtilities.ID_UnderlayDilate);
      float float10 = mat.GetFloat(ShaderUtilities.ID_UnderlaySoftness);
      float num7 = (float) (((double) num1 + (double) float2) * ((double) float1 - (double) ShaderUtilities.m_clamp));
      float num8 = Mathf.Max(1f, Mathf.Max(Mathf.Abs(float7), Mathf.Abs(float8)) + float9 + float10);
      float num9 = !flag ? 1f : Mathf.Max(0.0f, float1 - ShaderUtilities.m_clamp - num7) / (float1 * num8);
      mat.SetFloat(ShaderUtilities.ID_ScaleRatio_C, num9);
    }

    public static Vector4 GetFontExtent(Material material)
    {
      return Vector4.zero;
    }

    public static bool IsMaskingEnabled(Material material)
    {
      return !((Object) material == (Object) null) && material.HasProperty(ShaderUtilities.ID_ClipRect) && (((IEnumerable<string>) material.shaderKeywords).Contains<string>(ShaderUtilities.Keyword_MASK_SOFT) || ((IEnumerable<string>) material.shaderKeywords).Contains<string>(ShaderUtilities.Keyword_MASK_HARD) || ((IEnumerable<string>) material.shaderKeywords).Contains<string>(ShaderUtilities.Keyword_MASK_TEX));
    }

    public static float GetPadding(Material material, bool enableExtraPadding, bool isBold)
    {
      if (!ShaderUtilities.isInitialized)
        ShaderUtilities.GetShaderPropertyIDs();
      if ((Object) material == (Object) null)
        return 0.0f;
      int num1 = !enableExtraPadding ? 0 : 4;
      if (!material.HasProperty(ShaderUtilities.ID_GradientScale))
        return (float) num1;
      Vector4 zero1 = Vector4.zero;
      Vector4 zero2 = Vector4.zero;
      float num2 = 0.0f;
      float num3 = 0.0f;
      float num4 = 0.0f;
      float num5 = 0.0f;
      float num6 = 0.0f;
      float num7 = 0.0f;
      float num8 = 0.0f;
      float num9 = 0.0f;
      ShaderUtilities.UpdateShaderRatios(material, isBold);
      string[] shaderKeywords = material.shaderKeywords;
      if (material.HasProperty(ShaderUtilities.ID_ScaleRatio_A))
        num5 = material.GetFloat(ShaderUtilities.ID_ScaleRatio_A);
      if (material.HasProperty(ShaderUtilities.ID_FaceDilate))
        num2 = material.GetFloat(ShaderUtilities.ID_FaceDilate) * num5;
      if (material.HasProperty(ShaderUtilities.ID_OutlineSoftness))
        num3 = material.GetFloat(ShaderUtilities.ID_OutlineSoftness) * num5;
      if (material.HasProperty(ShaderUtilities.ID_OutlineWidth))
        num4 = material.GetFloat(ShaderUtilities.ID_OutlineWidth) * num5;
      float a = num4 + num3 + num2;
      if (material.HasProperty(ShaderUtilities.ID_GlowOffset) && ((IEnumerable<string>) shaderKeywords).Contains<string>(ShaderUtilities.Keyword_Glow))
      {
        if (material.HasProperty(ShaderUtilities.ID_ScaleRatio_B))
          num6 = material.GetFloat(ShaderUtilities.ID_ScaleRatio_B);
        num8 = material.GetFloat(ShaderUtilities.ID_GlowOffset) * num6;
        num9 = material.GetFloat(ShaderUtilities.ID_GlowOuter) * num6;
      }
      float b1 = Mathf.Max(a, num2 + num8 + num9);
      if (material.HasProperty(ShaderUtilities.ID_UnderlaySoftness) && ((IEnumerable<string>) shaderKeywords).Contains<string>(ShaderUtilities.Keyword_Underlay))
      {
        if (material.HasProperty(ShaderUtilities.ID_ScaleRatio_C))
          num7 = material.GetFloat(ShaderUtilities.ID_ScaleRatio_C);
        float num10 = material.GetFloat(ShaderUtilities.ID_UnderlayOffsetX) * num7;
        float num11 = material.GetFloat(ShaderUtilities.ID_UnderlayOffsetY) * num7;
        float num12 = material.GetFloat(ShaderUtilities.ID_UnderlayDilate) * num7;
        float num13 = material.GetFloat(ShaderUtilities.ID_UnderlaySoftness) * num7;
        zero1.x = Mathf.Max(zero1.x, num2 + num12 + num13 - num10);
        zero1.y = Mathf.Max(zero1.y, num2 + num12 + num13 - num11);
        zero1.z = Mathf.Max(zero1.z, num2 + num12 + num13 + num10);
        zero1.w = Mathf.Max(zero1.w, num2 + num12 + num13 + num11);
      }
      zero1.x = Mathf.Max(zero1.x, b1);
      zero1.y = Mathf.Max(zero1.y, b1);
      zero1.z = Mathf.Max(zero1.z, b1);
      zero1.w = Mathf.Max(zero1.w, b1);
      zero1.x += (float) num1;
      zero1.y += (float) num1;
      zero1.z += (float) num1;
      zero1.w += (float) num1;
      zero1.x = Mathf.Min(zero1.x, 1f);
      zero1.y = Mathf.Min(zero1.y, 1f);
      zero1.z = Mathf.Min(zero1.z, 1f);
      zero1.w = Mathf.Min(zero1.w, 1f);
      zero2.x = (double) zero2.x >= (double) zero1.x ? zero2.x : zero1.x;
      zero2.y = (double) zero2.y >= (double) zero1.y ? zero2.y : zero1.y;
      zero2.z = (double) zero2.z >= (double) zero1.z ? zero2.z : zero1.z;
      zero2.w = (double) zero2.w >= (double) zero1.w ? zero2.w : zero1.w;
      float @float = material.GetFloat(ShaderUtilities.ID_GradientScale);
      zero1 *= @float;
      float b2 = Mathf.Max(zero1.x, zero1.y);
      float b3 = Mathf.Max(zero1.z, b2);
      return Mathf.Max(zero1.w, b3) + 0.5f;
    }

    public static float GetPadding(Material[] materials, bool enableExtraPadding, bool isBold)
    {
      if (!ShaderUtilities.isInitialized)
        ShaderUtilities.GetShaderPropertyIDs();
      if (materials == null)
        return 0.0f;
      int num1 = !enableExtraPadding ? 0 : 4;
      if (!materials[0].HasProperty(ShaderUtilities.ID_GradientScale))
        return (float) num1;
      Vector4 zero1 = Vector4.zero;
      Vector4 zero2 = Vector4.zero;
      float num2 = 0.0f;
      float num3 = 0.0f;
      float num4 = 0.0f;
      float num5 = 0.0f;
      float num6 = 0.0f;
      float num7 = 0.0f;
      float num8 = 0.0f;
      float num9 = 0.0f;
      for (int index = 0; index < materials.Length; ++index)
      {
        ShaderUtilities.UpdateShaderRatios(materials[index], isBold);
        string[] shaderKeywords = materials[index].shaderKeywords;
        if (materials[index].HasProperty(ShaderUtilities.ID_ScaleRatio_A))
          num5 = materials[index].GetFloat(ShaderUtilities.ID_ScaleRatio_A);
        if (materials[index].HasProperty(ShaderUtilities.ID_FaceDilate))
          num2 = materials[index].GetFloat(ShaderUtilities.ID_FaceDilate) * num5;
        if (materials[index].HasProperty(ShaderUtilities.ID_OutlineSoftness))
          num3 = materials[index].GetFloat(ShaderUtilities.ID_OutlineSoftness) * num5;
        if (materials[index].HasProperty(ShaderUtilities.ID_OutlineWidth))
          num4 = materials[index].GetFloat(ShaderUtilities.ID_OutlineWidth) * num5;
        float a = num4 + num3 + num2;
        if (materials[index].HasProperty(ShaderUtilities.ID_GlowOffset) && ((IEnumerable<string>) shaderKeywords).Contains<string>(ShaderUtilities.Keyword_Glow))
        {
          if (materials[index].HasProperty(ShaderUtilities.ID_ScaleRatio_B))
            num6 = materials[index].GetFloat(ShaderUtilities.ID_ScaleRatio_B);
          num8 = materials[index].GetFloat(ShaderUtilities.ID_GlowOffset) * num6;
          num9 = materials[index].GetFloat(ShaderUtilities.ID_GlowOuter) * num6;
        }
        float b = Mathf.Max(a, num2 + num8 + num9);
        if (materials[index].HasProperty(ShaderUtilities.ID_UnderlaySoftness) && ((IEnumerable<string>) shaderKeywords).Contains<string>(ShaderUtilities.Keyword_Underlay))
        {
          if (materials[index].HasProperty(ShaderUtilities.ID_ScaleRatio_C))
            num7 = materials[index].GetFloat(ShaderUtilities.ID_ScaleRatio_C);
          float num10 = materials[index].GetFloat(ShaderUtilities.ID_UnderlayOffsetX) * num7;
          float num11 = materials[index].GetFloat(ShaderUtilities.ID_UnderlayOffsetY) * num7;
          float num12 = materials[index].GetFloat(ShaderUtilities.ID_UnderlayDilate) * num7;
          float num13 = materials[index].GetFloat(ShaderUtilities.ID_UnderlaySoftness) * num7;
          zero1.x = Mathf.Max(zero1.x, num2 + num12 + num13 - num10);
          zero1.y = Mathf.Max(zero1.y, num2 + num12 + num13 - num11);
          zero1.z = Mathf.Max(zero1.z, num2 + num12 + num13 + num10);
          zero1.w = Mathf.Max(zero1.w, num2 + num12 + num13 + num11);
        }
        zero1.x = Mathf.Max(zero1.x, b);
        zero1.y = Mathf.Max(zero1.y, b);
        zero1.z = Mathf.Max(zero1.z, b);
        zero1.w = Mathf.Max(zero1.w, b);
        zero1.x += (float) num1;
        zero1.y += (float) num1;
        zero1.z += (float) num1;
        zero1.w += (float) num1;
        zero1.x = Mathf.Min(zero1.x, 1f);
        zero1.y = Mathf.Min(zero1.y, 1f);
        zero1.z = Mathf.Min(zero1.z, 1f);
        zero1.w = Mathf.Min(zero1.w, 1f);
        zero2.x = (double) zero2.x >= (double) zero1.x ? zero2.x : zero1.x;
        zero2.y = (double) zero2.y >= (double) zero1.y ? zero2.y : zero1.y;
        zero2.z = (double) zero2.z >= (double) zero1.z ? zero2.z : zero1.z;
        zero2.w = (double) zero2.w >= (double) zero1.w ? zero2.w : zero1.w;
      }
      float @float = materials[0].GetFloat(ShaderUtilities.ID_GradientScale);
      zero1 *= @float;
      float b1 = Mathf.Max(zero1.x, zero1.y);
      float b2 = Mathf.Max(zero1.z, b1);
      return Mathf.Max(zero1.w, b2) + 0.25f;
    }
  }
}
