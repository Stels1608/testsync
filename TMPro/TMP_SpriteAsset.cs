﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_SpriteAsset
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TMPro
{
  public class TMP_SpriteAsset : TMP_Asset
  {
    public static TMP_SpriteAsset m_defaultSpriteAsset;
    public Texture spriteSheet;
    public List<TMP_Sprite> spriteInfoList;
    private List<Sprite> m_sprites;

    public static TMP_SpriteAsset defaultSpriteAsset
    {
      get
      {
        if ((Object) TMP_SpriteAsset.m_defaultSpriteAsset == (Object) null)
          TMP_SpriteAsset.m_defaultSpriteAsset = Resources.Load<TMP_SpriteAsset>("Sprite Assets/Default Sprite Asset");
        return TMP_SpriteAsset.m_defaultSpriteAsset;
      }
    }

    private void OnEnable()
    {
    }

    private void OnValidate()
    {
      TMPro_EventManager.ON_SPRITE_ASSET_PROPERTY_CHANGED(true, (Object) this);
    }

    private Material GetDefaultSpriteMaterial()
    {
      ShaderUtilities.GetShaderPropertyIDs();
      Material material = new Material(Shader.Find("TMPro/Sprite"));
      material.SetTexture(ShaderUtilities.ID_MainTex, this.spriteSheet);
      material.hideFlags = HideFlags.HideInHierarchy;
      return material;
    }

    public int GetSpriteIndex(int hashCode)
    {
      for (int index = 0; index < this.spriteInfoList.Count; ++index)
      {
        if (this.spriteInfoList[index].hashCode == hashCode)
          return index;
      }
      return -1;
    }
  }
}
