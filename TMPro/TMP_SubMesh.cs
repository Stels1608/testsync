﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_SubMesh
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TMPro
{
  [RequireComponent(typeof (MeshRenderer))]
  [ExecuteInEditMode]
  [RequireComponent(typeof (MeshFilter))]
  public class TMP_SubMesh : MonoBehaviour
  {
    [SerializeField]
    private TMP_FontAsset m_fontAsset;
    [SerializeField]
    private TMP_SpriteAsset m_spriteAsset;
    [SerializeField]
    private Material m_material;
    [SerializeField]
    private Material m_sharedMaterial;
    [SerializeField]
    private bool m_isDefaultMaterial;
    [SerializeField]
    private float m_padding;
    [SerializeField]
    private Renderer m_renderer;
    [SerializeField]
    private MeshFilter m_meshFilter;
    private Mesh m_mesh;
    [SerializeField]
    private TextMeshPro m_TextComponent;
    [NonSerialized]
    private bool m_isRegisteredForEvents;

    public TMP_FontAsset fontAsset
    {
      get
      {
        return this.m_fontAsset;
      }
      set
      {
        this.m_fontAsset = value;
      }
    }

    public TMP_SpriteAsset spriteAsset
    {
      get
      {
        return this.m_spriteAsset;
      }
      set
      {
        this.m_spriteAsset = value;
      }
    }

    public Material material
    {
      get
      {
        return this.GetMaterial(this.m_sharedMaterial);
      }
      set
      {
        if (this.m_sharedMaterial.GetInstanceID() == value.GetInstanceID())
          return;
        this.m_sharedMaterial = value;
        this.m_padding = this.GetPaddingForMaterial();
        this.SetVerticesDirty();
        this.SetMaterialDirty();
      }
    }

    public Material sharedMaterial
    {
      get
      {
        return this.GetSharedMaterial();
      }
      set
      {
        this.SetSharedMaterial(value);
      }
    }

    public bool isDefaultMaterial
    {
      get
      {
        return this.m_isDefaultMaterial;
      }
      set
      {
        this.m_isDefaultMaterial = value;
      }
    }

    public float padding
    {
      get
      {
        return this.m_padding;
      }
      set
      {
        this.m_padding = value;
      }
    }

    public Renderer renderer
    {
      get
      {
        if ((UnityEngine.Object) this.m_renderer == (UnityEngine.Object) null)
          this.m_renderer = this.GetComponent<Renderer>();
        return this.m_renderer;
      }
    }

    public MeshFilter meshFilter
    {
      get
      {
        if ((UnityEngine.Object) this.m_meshFilter == (UnityEngine.Object) null)
          this.m_meshFilter = this.GetComponent<MeshFilter>();
        return this.m_meshFilter;
      }
    }

    public Mesh mesh
    {
      get
      {
        if ((UnityEngine.Object) this.m_mesh == (UnityEngine.Object) null)
        {
          this.m_mesh = new Mesh();
          this.m_mesh.hideFlags = HideFlags.HideAndDontSave;
          this.meshFilter.mesh = this.m_mesh;
        }
        return this.m_mesh;
      }
      set
      {
        this.m_mesh = value;
      }
    }

    private void Awake()
    {
    }

    private void OnEnable()
    {
      if (this.m_isRegisteredForEvents)
        return;
      this.m_isRegisteredForEvents = true;
    }

    private void OnDestroy()
    {
      if ((UnityEngine.Object) this.m_mesh != (UnityEngine.Object) null)
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_mesh);
      this.m_isRegisteredForEvents = false;
    }

    public static TMP_SubMesh AddSubTextObject(TextMeshPro textComponent, MaterialReference materialReference)
    {
      GameObject gameObject = new GameObject("TMP SubMesh [" + materialReference.material.name + "]");
      TMP_SubMesh tmpSubMesh = gameObject.AddComponent<TMP_SubMesh>();
      gameObject.transform.SetParent(textComponent.transform, false);
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localRotation = Quaternion.identity;
      gameObject.transform.localScale = Vector3.one;
      gameObject.layer = textComponent.gameObject.layer;
      tmpSubMesh.m_meshFilter = gameObject.GetComponent<MeshFilter>();
      tmpSubMesh.m_TextComponent = textComponent;
      tmpSubMesh.m_fontAsset = materialReference.fontAsset;
      tmpSubMesh.m_spriteAsset = materialReference.spriteAsset;
      tmpSubMesh.m_isDefaultMaterial = materialReference.isDefaultMaterial;
      tmpSubMesh.SetSharedMaterial(materialReference.material);
      tmpSubMesh.renderer.sortingLayerID = textComponent.renderer.sortingLayerID;
      tmpSubMesh.renderer.sortingOrder = textComponent.renderer.sortingOrder;
      return tmpSubMesh;
    }

    private Material GetMaterial(Material mat)
    {
      if ((UnityEngine.Object) this.m_renderer == (UnityEngine.Object) null)
        this.m_renderer = this.GetComponent<Renderer>();
      if ((UnityEngine.Object) this.m_material == (UnityEngine.Object) null || this.m_material.GetInstanceID() != mat.GetInstanceID())
        this.m_material = this.CreateMaterialInstance(mat);
      this.m_sharedMaterial = this.m_material;
      this.m_padding = this.GetPaddingForMaterial();
      this.SetVerticesDirty();
      this.SetMaterialDirty();
      return this.m_sharedMaterial;
    }

    private Material CreateMaterialInstance(Material source)
    {
      Material material = new Material(source);
      material.shaderKeywords = source.shaderKeywords;
      material.name += " (Instance)";
      return material;
    }

    private Material GetSharedMaterial()
    {
      if ((UnityEngine.Object) this.m_renderer == (UnityEngine.Object) null)
        this.m_renderer = this.GetComponent<Renderer>();
      return this.m_renderer.sharedMaterial;
    }

    private void SetSharedMaterial(Material mat)
    {
      this.m_sharedMaterial = mat;
      this.m_padding = this.GetPaddingForMaterial();
      this.SetMaterialDirty();
    }

    public float GetPaddingForMaterial()
    {
      return ShaderUtilities.GetPadding(this.m_sharedMaterial, this.m_TextComponent.extraPadding, this.m_TextComponent.isUsingBold);
    }

    public void UpdateMeshPadding(bool isExtraPadding, bool isUsingBold)
    {
      this.m_padding = ShaderUtilities.GetPadding(this.m_sharedMaterial, isExtraPadding, isUsingBold);
    }

    public void SetVerticesDirty()
    {
      if (!this.enabled || !((UnityEngine.Object) this.m_TextComponent != (UnityEngine.Object) null))
        return;
      this.m_TextComponent.havePropertiesChanged = true;
      this.m_TextComponent.SetVerticesDirty();
    }

    public void SetMaterialDirty()
    {
      if (!this.enabled)
        return;
      this.UpdateMaterial();
    }

    protected void UpdateMaterial()
    {
      if (!this.enabled)
        return;
      if ((UnityEngine.Object) this.m_renderer == (UnityEngine.Object) null)
        this.m_renderer = this.renderer;
      this.m_renderer.sharedMaterial = this.m_sharedMaterial;
    }
  }
}
