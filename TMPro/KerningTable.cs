﻿// Decompiled with JetBrains decompiler
// Type: TMPro.KerningTable
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;

namespace TMPro
{
  [Serializable]
  public class KerningTable
  {
    public List<KerningPair> kerningPairs;

    public KerningTable()
    {
      this.kerningPairs = new List<KerningPair>();
    }

    public void AddKerningPair()
    {
      if (this.kerningPairs.Count == 0)
        this.kerningPairs.Add(new KerningPair(0, 0, 0.0f));
      else
        this.kerningPairs.Add(new KerningPair(this.kerningPairs.Last<KerningPair>().AscII_Left, this.kerningPairs.Last<KerningPair>().AscII_Right, this.kerningPairs.Last<KerningPair>().XadvanceOffset));
    }

    public int AddKerningPair(int left, int right, float offset)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      KerningTable.\u003CAddKerningPair\u003Ec__AnonStorey123 pairCAnonStorey123 = new KerningTable.\u003CAddKerningPair\u003Ec__AnonStorey123();
      // ISSUE: reference to a compiler-generated field
      pairCAnonStorey123.left = left;
      // ISSUE: reference to a compiler-generated field
      pairCAnonStorey123.right = right;
      // ISSUE: reference to a compiler-generated method
      if (this.kerningPairs.FindIndex(new Predicate<KerningPair>(pairCAnonStorey123.\u003C\u003Em__2B9)) != -1)
        return -1;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      this.kerningPairs.Add(new KerningPair(pairCAnonStorey123.left, pairCAnonStorey123.right, offset));
      return 0;
    }

    public void RemoveKerningPair(int left, int right)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      int index = this.kerningPairs.FindIndex(new Predicate<KerningPair>(new KerningTable.\u003CRemoveKerningPair\u003Ec__AnonStorey124() { left = left, right = right }.\u003C\u003Em__2BA));
      if (index == -1)
        return;
      this.kerningPairs.RemoveAt(index);
    }

    public void RemoveKerningPair(int index)
    {
      this.kerningPairs.RemoveAt(index);
    }

    public void SortKerningPairs()
    {
      if (this.kerningPairs.Count <= 0)
        return;
      this.kerningPairs = this.kerningPairs.OrderBy<KerningPair, int>((Func<KerningPair, int>) (s => s.AscII_Left)).ThenBy<KerningPair, int>((Func<KerningPair, int>) (s => s.AscII_Right)).ToList<KerningPair>();
    }
  }
}
