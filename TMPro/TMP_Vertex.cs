﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_Vertex
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TMPro
{
  public struct TMP_Vertex
  {
    public Vector3 position;
    public Vector2 uv;
    public Vector2 uv2;
    public Vector2 uv4;
    public Color32 color;
  }
}
