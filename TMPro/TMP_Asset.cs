﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_Asset
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TMPro
{
  [Serializable]
  public class TMP_Asset : ScriptableObject
  {
    public int hashCode;
    public Material material;
    public int materialHashCode;
  }
}
