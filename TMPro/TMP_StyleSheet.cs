﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_StyleSheet
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace TMPro
{
  [Serializable]
  public class TMP_StyleSheet : ScriptableObject
  {
    [SerializeField]
    private List<TMP_Style> m_StyleList = new List<TMP_Style>(1);
    private Dictionary<int, TMP_Style> m_StyleDictionary = new Dictionary<int, TMP_Style>();
    private static TMP_StyleSheet s_Instance;

    public static TMP_StyleSheet instance
    {
      get
      {
        if ((UnityEngine.Object) TMP_StyleSheet.s_Instance == (UnityEngine.Object) null)
        {
          TMP_Settings tmpSettings = TMP_Settings.LoadDefaultSettings();
          if ((UnityEngine.Object) tmpSettings == (UnityEngine.Object) null)
            return (TMP_StyleSheet) null;
          TMP_StyleSheet.s_Instance = tmpSettings.styleSheet;
          if ((UnityEngine.Object) TMP_StyleSheet.s_Instance == (UnityEngine.Object) null)
            TMP_StyleSheet.s_Instance = Resources.Load("Style Sheets/TMP Default Style Sheet") as TMP_StyleSheet;
          TMP_StyleSheet.s_Instance.LoadStyleDictionaryInternal();
        }
        return TMP_StyleSheet.s_Instance;
      }
    }

    public static TMP_StyleSheet LoadDefaultStyleSheet()
    {
      return TMP_StyleSheet.instance;
    }

    public static TMP_Style GetStyle(int hashCode)
    {
      return TMP_StyleSheet.s_Instance.GetStyleInternal(hashCode);
    }

    private TMP_Style GetStyleInternal(int hashCode)
    {
      TMP_Style tmpStyle;
      if (this.m_StyleDictionary.TryGetValue(hashCode, out tmpStyle))
        return tmpStyle;
      return (TMP_Style) null;
    }

    public void UpdateStyleDictionaryKey(int old_key, int new_key)
    {
      if (!this.m_StyleDictionary.ContainsKey(old_key))
        return;
      TMP_Style tmpStyle = this.m_StyleDictionary[old_key];
      this.m_StyleDictionary.Add(new_key, tmpStyle);
      this.m_StyleDictionary.Remove(old_key);
    }

    public static void RefreshStyles()
    {
      TMP_StyleSheet.s_Instance.LoadStyleDictionaryInternal();
    }

    private void LoadStyleDictionaryInternal()
    {
      this.m_StyleDictionary.Clear();
      for (int index = 0; index < this.m_StyleList.Count; ++index)
      {
        this.m_StyleList[index].RefreshStyle();
        if (!this.m_StyleDictionary.ContainsKey(this.m_StyleList[index].hashCode))
          this.m_StyleDictionary.Add(this.m_StyleList[index].hashCode, this.m_StyleList[index]);
      }
    }
  }
}
