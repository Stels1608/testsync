﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_LineInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace TMPro
{
  public struct TMP_LineInfo
  {
    public int characterCount;
    public int spaceCount;
    public int wordCount;
    public int firstCharacterIndex;
    public int firstVisibleCharacterIndex;
    public int lastCharacterIndex;
    public int lastVisibleCharacterIndex;
    public float length;
    public float lineHeight;
    public float ascender;
    public float baseline;
    public float descender;
    public float maxAdvance;
    public float width;
    public float marginLeft;
    public float marginRight;
    public float maxScale;
    public TextAlignmentOptions alignment;
    public Extents lineExtents;
  }
}
