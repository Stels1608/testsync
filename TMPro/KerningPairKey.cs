﻿// Decompiled with JetBrains decompiler
// Type: TMPro.KerningPairKey
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace TMPro
{
  public struct KerningPairKey
  {
    public int ascii_Left;
    public int ascii_Right;
    public int key;

    public KerningPairKey(int ascii_left, int ascii_right)
    {
      this.ascii_Left = ascii_left;
      this.ascii_Right = ascii_right;
      this.key = (ascii_right << 16) + ascii_left;
    }
  }
}
