﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMPro_EventManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TMPro
{
  public static class TMPro_EventManager
  {
    public static readonly FastAction<object, Compute_DT_EventArgs> COMPUTE_DT_EVENT = new FastAction<object, Compute_DT_EventArgs>();
    public static readonly FastAction<bool, Material> MATERIAL_PROPERTY_EVENT = new FastAction<bool, Material>();
    public static readonly FastAction<bool, TMP_FontAsset> FONT_PROPERTY_EVENT = new FastAction<bool, TMP_FontAsset>();
    public static readonly FastAction<bool, Object> SPRITE_ASSET_PROPERTY_EVENT = new FastAction<bool, Object>();
    public static readonly FastAction<bool, TextMeshPro> TEXTMESHPRO_PROPERTY_EVENT = new FastAction<bool, TextMeshPro>();
    public static readonly FastAction<GameObject, Material, Material> DRAG_AND_DROP_MATERIAL_EVENT = new FastAction<GameObject, Material, Material>();
    public static readonly FastAction<bool> TEXT_STYLE_PROPERTY_EVENT = new FastAction<bool>();
    public static readonly FastAction TMP_SETTINGS_PROPERTY_EVENT = new FastAction();
    public static readonly FastAction<bool, TextMeshProUGUI> TEXTMESHPRO_UGUI_PROPERTY_EVENT = new FastAction<bool, TextMeshProUGUI>();
    public static readonly FastAction<Material> BASE_MATERIAL_EVENT = new FastAction<Material>();
    public static readonly FastAction OnPreRenderObject_Event = new FastAction();
    public static readonly FastAction<Object> TEXT_CHANGED_EVENT = new FastAction<Object>();
    public static readonly FastAction WILL_RENDER_CANVASES = new FastAction();

    public static void ON_PRE_RENDER_OBJECT_CHANGED()
    {
      TMPro_EventManager.OnPreRenderObject_Event.Call();
    }

    public static void ON_MATERIAL_PROPERTY_CHANGED(bool isChanged, Material mat)
    {
      TMPro_EventManager.MATERIAL_PROPERTY_EVENT.Call(isChanged, mat);
    }

    public static void ON_FONT_PROPERTY_CHANGED(bool isChanged, TMP_FontAsset font)
    {
      TMPro_EventManager.FONT_PROPERTY_EVENT.Call(isChanged, font);
    }

    public static void ON_SPRITE_ASSET_PROPERTY_CHANGED(bool isChanged, Object obj)
    {
      TMPro_EventManager.SPRITE_ASSET_PROPERTY_EVENT.Call(isChanged, obj);
    }

    public static void ON_TEXTMESHPRO_PROPERTY_CHANGED(bool isChanged, TextMeshPro obj)
    {
      TMPro_EventManager.TEXTMESHPRO_PROPERTY_EVENT.Call(isChanged, obj);
    }

    public static void ON_DRAG_AND_DROP_MATERIAL_CHANGED(GameObject sender, Material currentMaterial, Material newMaterial)
    {
      TMPro_EventManager.DRAG_AND_DROP_MATERIAL_EVENT.Call(sender, currentMaterial, newMaterial);
    }

    public static void ON_TEXT_STYLE_PROPERTY_CHANGED(bool isChanged)
    {
      TMPro_EventManager.TEXT_STYLE_PROPERTY_EVENT.Call(isChanged);
    }

    public static void ON_TEXT_CHANGED(Object obj)
    {
      TMPro_EventManager.TEXT_CHANGED_EVENT.Call(obj);
    }

    public static void ON_TMP_SETTINGS_CHANGED()
    {
      TMPro_EventManager.TMP_SETTINGS_PROPERTY_EVENT.Call();
    }

    public static void ON_TEXTMESHPRO_UGUI_PROPERTY_CHANGED(bool isChanged, TextMeshProUGUI obj)
    {
      TMPro_EventManager.TEXTMESHPRO_UGUI_PROPERTY_EVENT.Call(isChanged, obj);
    }

    public static void ON_BASE_MATERIAL_CHANGED(Material mat)
    {
      TMPro_EventManager.BASE_MATERIAL_EVENT.Call(mat);
    }

    public static void ON_COMPUTE_DT_EVENT(object Sender, Compute_DT_EventArgs e)
    {
      TMPro_EventManager.COMPUTE_DT_EVENT.Call(Sender, e);
    }
  }
}
