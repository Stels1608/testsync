﻿// Decompiled with JetBrains decompiler
// Type: TMPro.KerningPair
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

namespace TMPro
{
  [Serializable]
  public class KerningPair
  {
    public int AscII_Left;
    public int AscII_Right;
    public float XadvanceOffset;

    public KerningPair(int left, int right, float offset)
    {
      this.AscII_Left = left;
      this.AscII_Right = right;
      this.XadvanceOffset = offset;
    }
  }
}
