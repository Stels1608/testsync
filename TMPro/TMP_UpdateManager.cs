﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_UpdateManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TMPro
{
  public class TMP_UpdateManager
  {
    private readonly List<TMP_Text> m_LayoutRebuildQueue = new List<TMP_Text>();
    private Dictionary<int, int> m_LayoutQueueLookup = new Dictionary<int, int>();
    private readonly List<TMP_Text> m_GraphicRebuildQueue = new List<TMP_Text>();
    private Dictionary<int, int> m_GraphicQueueLookup = new Dictionary<int, int>();
    private static TMP_UpdateManager s_Instance;

    public static TMP_UpdateManager instance
    {
      get
      {
        if (TMP_UpdateManager.s_Instance == null)
          TMP_UpdateManager.s_Instance = new TMP_UpdateManager();
        return TMP_UpdateManager.s_Instance;
      }
    }

    protected TMP_UpdateManager()
    {
      Camera.onPreRender += new Camera.CameraCallback(this.OnCameraPreRender);
    }

    public static void RegisterTextElementForLayoutRebuild(TMP_Text element)
    {
      TMP_UpdateManager.instance.InternalRegisterTextElementForLayoutRebuild(element);
    }

    private bool InternalRegisterTextElementForLayoutRebuild(TMP_Text element)
    {
      int instanceId = element.GetInstanceID();
      if (this.m_LayoutQueueLookup.ContainsKey(instanceId))
        return false;
      this.m_LayoutQueueLookup[instanceId] = instanceId;
      this.m_LayoutRebuildQueue.Add(element);
      return true;
    }

    public static void RegisterTextElementForGraphicRebuild(TMP_Text element)
    {
      TMP_UpdateManager.instance.InternalRegisterTextElementForGraphicRebuild(element);
    }

    private bool InternalRegisterTextElementForGraphicRebuild(TMP_Text element)
    {
      int instanceId = element.GetInstanceID();
      if (this.m_GraphicQueueLookup.ContainsKey(instanceId))
        return false;
      this.m_GraphicQueueLookup[instanceId] = instanceId;
      this.m_GraphicRebuildQueue.Add(element);
      return true;
    }

    private void OnCameraPreRender(Camera cam)
    {
      for (int index = 0; index < this.m_LayoutRebuildQueue.Count; ++index)
        this.m_LayoutRebuildQueue[index].Rebuild(CanvasUpdate.Prelayout);
      if (this.m_LayoutRebuildQueue.Count > 0)
      {
        this.m_LayoutRebuildQueue.Clear();
        this.m_LayoutQueueLookup.Clear();
      }
      for (int index = 0; index < this.m_GraphicRebuildQueue.Count; ++index)
        this.m_GraphicRebuildQueue[index].Rebuild(CanvasUpdate.PreRender);
      if (this.m_GraphicRebuildQueue.Count <= 0)
        return;
      this.m_GraphicRebuildQueue.Clear();
      this.m_GraphicQueueLookup.Clear();
    }

    public static void UnRegisterTextElementForRebuild(TMP_Text element)
    {
      TMP_UpdateManager.instance.InternalUnRegisterTextElementForGraphicRebuild(element);
      TMP_UpdateManager.instance.InternalUnRegisterTextElementForLayoutRebuild(element);
    }

    private void InternalUnRegisterTextElementForGraphicRebuild(TMP_Text element)
    {
      int instanceId = element.GetInstanceID();
      TMP_UpdateManager.instance.m_GraphicRebuildQueue.Remove(element);
      this.m_GraphicQueueLookup.Remove(instanceId);
    }

    private void InternalUnRegisterTextElementForLayoutRebuild(TMP_Text element)
    {
      int instanceId = element.GetInstanceID();
      TMP_UpdateManager.instance.m_LayoutRebuildQueue.Remove(element);
      this.m_LayoutQueueLookup.Remove(instanceId);
    }
  }
}
