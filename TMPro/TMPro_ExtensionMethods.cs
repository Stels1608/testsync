﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMPro_ExtensionMethods
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TMPro
{
  public static class TMPro_ExtensionMethods
  {
    public static string ArrayToString(this char[] chars)
    {
      string str = string.Empty;
      for (int index = 0; index < chars.Length && (int) chars[index] != 0; ++index)
        str += (string) (object) chars[index];
      return str;
    }

    public static int FindInstanceID<T>(this List<T> list, T target) where T : Object
    {
      int instanceId = target.GetInstanceID();
      for (int index = 0; index < list.Count; ++index)
      {
        if (list[index].GetInstanceID() == instanceId)
          return index;
      }
      return -1;
    }

    public static bool Compare(this Color32 a, Color32 b)
    {
      if ((int) a.r == (int) b.r && (int) a.g == (int) b.g && (int) a.b == (int) b.b)
        return (int) a.a == (int) b.a;
      return false;
    }

    public static bool CompareRGB(this Color32 a, Color32 b)
    {
      if ((int) a.r == (int) b.r && (int) a.g == (int) b.g)
        return (int) a.b == (int) b.b;
      return false;
    }

    public static bool Compare(this Color a, Color b)
    {
      if ((double) a.r == (double) b.r && (double) a.g == (double) b.g && (double) a.b == (double) b.b)
        return (double) a.a == (double) b.a;
      return false;
    }

    public static bool CompareRGB(this Color a, Color b)
    {
      if ((double) a.r == (double) b.r && (double) a.g == (double) b.g)
        return (double) a.b == (double) b.b;
      return false;
    }

    public static Color32 Multiply(this Color32 c1, Color32 c2)
    {
      return new Color32((byte) ((double) c1.r / (double) byte.MaxValue * ((double) c2.r / (double) byte.MaxValue) * (double) byte.MaxValue), (byte) ((double) c1.g / (double) byte.MaxValue * ((double) c2.g / (double) byte.MaxValue) * (double) byte.MaxValue), (byte) ((double) c1.b / (double) byte.MaxValue * ((double) c2.b / (double) byte.MaxValue) * (double) byte.MaxValue), (byte) ((double) c1.a / (double) byte.MaxValue * ((double) c2.a / (double) byte.MaxValue) * (double) byte.MaxValue));
    }

    public static Color32 Tint(this Color32 c1, Color32 c2)
    {
      return new Color32((byte) ((double) c1.r / (double) byte.MaxValue * ((double) c2.r / (double) byte.MaxValue) * (double) byte.MaxValue), (byte) ((double) c1.g / (double) byte.MaxValue * ((double) c2.g / (double) byte.MaxValue) * (double) byte.MaxValue), (byte) ((double) c1.b / (double) byte.MaxValue * ((double) c2.b / (double) byte.MaxValue) * (double) byte.MaxValue), (byte) ((double) c1.a / (double) byte.MaxValue * ((double) c2.a / (double) byte.MaxValue) * (double) byte.MaxValue));
    }

    public static Color32 Tint(this Color32 c1, float tint)
    {
      return new Color32((byte) Mathf.Clamp((float) ((double) c1.r / (double) byte.MaxValue * (double) tint * (double) byte.MaxValue), 0.0f, (float) byte.MaxValue), (byte) Mathf.Clamp((float) ((double) c1.g / (double) byte.MaxValue * (double) tint * (double) byte.MaxValue), 0.0f, (float) byte.MaxValue), (byte) Mathf.Clamp((float) ((double) c1.b / (double) byte.MaxValue * (double) tint * (double) byte.MaxValue), 0.0f, (float) byte.MaxValue), (byte) Mathf.Clamp((float) ((double) c1.a / (double) byte.MaxValue * (double) tint * (double) byte.MaxValue), 0.0f, (float) byte.MaxValue));
    }

    public static bool Compare(this Vector3 v1, Vector3 v2, int accuracy)
    {
      bool flag1 = (int) ((double) v1.x * (double) accuracy) == (int) ((double) v2.x * (double) accuracy);
      bool flag2 = (int) ((double) v1.y * (double) accuracy) == (int) ((double) v2.y * (double) accuracy);
      bool flag3 = (int) ((double) v1.z * (double) accuracy) == (int) ((double) v2.z * (double) accuracy);
      if (flag1 && flag2)
        return flag3;
      return false;
    }

    public static bool Compare(this Quaternion q1, Quaternion q2, int accuracy)
    {
      bool flag1 = (int) ((double) q1.x * (double) accuracy) == (int) ((double) q2.x * (double) accuracy);
      bool flag2 = (int) ((double) q1.y * (double) accuracy) == (int) ((double) q2.y * (double) accuracy);
      bool flag3 = (int) ((double) q1.z * (double) accuracy) == (int) ((double) q2.z * (double) accuracy);
      bool flag4 = (int) ((double) q1.w * (double) accuracy) == (int) ((double) q2.w * (double) accuracy);
      if (flag1 && flag2 && flag3)
        return flag4;
      return false;
    }
  }
}
