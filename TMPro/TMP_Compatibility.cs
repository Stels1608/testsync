﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_Compatibility
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace TMPro
{
  public static class TMP_Compatibility
  {
    public enum AnchorPositions
    {
      TopLeft,
      Top,
      TopRight,
      Left,
      Center,
      Right,
      BottomLeft,
      Bottom,
      BottomRight,
      BaseLine,
      None,
    }
  }
}
