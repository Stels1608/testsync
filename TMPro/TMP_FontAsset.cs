﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_FontAsset
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TMPro
{
  [Serializable]
  public class TMP_FontAsset : TMP_Asset
  {
    public TMP_FontWeights[] fontWeights = new TMP_FontWeights[10];
    public float boldStyle = 0.75f;
    public float boldSpacing = 7f;
    public byte italicStyle = 35;
    public byte tabSize = 10;
    private static TMP_FontAsset s_defaultFontAsset;
    public TMP_FontAsset.FontAssetTypes fontAssetType;
    [SerializeField]
    private FaceInfo m_fontInfo;
    [SerializeField]
    public Texture2D atlas;
    [SerializeField]
    private List<TMP_Glyph> m_glyphInfoList;
    private Dictionary<int, TMP_Glyph> m_characterDictionary;
    private Dictionary<int, KerningPair> m_kerningDictionary;
    [SerializeField]
    private KerningTable m_kerningInfo;
    [SerializeField]
    private KerningPair m_kerningPair;
    [SerializeField]
    private LineBreakingTable m_lineBreakingInfo;
    [SerializeField]
    public FontCreationSetting fontCreationSettings;
    private int[] m_characterSet;
    public float normalStyle;
    public float normalSpacingOffset;
    private byte m_oldTabSize;

    public static TMP_FontAsset defaultFontAsset
    {
      get
      {
        if ((UnityEngine.Object) TMP_FontAsset.s_defaultFontAsset == (UnityEngine.Object) null)
          TMP_FontAsset.s_defaultFontAsset = Resources.Load<TMP_FontAsset>("Fonts & Materials/ARIAL SDF");
        return TMP_FontAsset.s_defaultFontAsset;
      }
    }

    public FaceInfo fontInfo
    {
      get
      {
        return this.m_fontInfo;
      }
    }

    public Dictionary<int, TMP_Glyph> characterDictionary
    {
      get
      {
        return this.m_characterDictionary;
      }
    }

    public Dictionary<int, KerningPair> kerningDictionary
    {
      get
      {
        return this.m_kerningDictionary;
      }
    }

    public KerningTable kerningInfo
    {
      get
      {
        return this.m_kerningInfo;
      }
    }

    public LineBreakingTable lineBreakingInfo
    {
      get
      {
        return this.m_lineBreakingInfo;
      }
    }

    private void OnEnable()
    {
      if (this.m_characterDictionary != null)
        return;
      this.ReadFontDefinition();
    }

    private void OnDisable()
    {
    }

    private void OnValidate()
    {
      if ((int) this.m_oldTabSize == (int) this.tabSize)
        return;
      this.m_oldTabSize = this.tabSize;
      this.ReadFontDefinition();
    }

    public void AddFaceInfo(FaceInfo faceInfo)
    {
      this.m_fontInfo = faceInfo;
    }

    public void AddGlyphInfo(TMP_Glyph[] glyphInfo)
    {
      this.m_glyphInfoList = new List<TMP_Glyph>();
      int length = glyphInfo.Length;
      this.m_fontInfo.CharacterCount = length;
      this.m_characterSet = new int[length];
      for (int index = 0; index < length; ++index)
      {
        TMP_Glyph tmpGlyph = new TMP_Glyph();
        tmpGlyph.id = glyphInfo[index].id;
        tmpGlyph.x = glyphInfo[index].x;
        tmpGlyph.y = glyphInfo[index].y;
        tmpGlyph.width = glyphInfo[index].width;
        tmpGlyph.height = glyphInfo[index].height;
        tmpGlyph.xOffset = glyphInfo[index].xOffset;
        tmpGlyph.yOffset = glyphInfo[index].yOffset + this.m_fontInfo.Padding;
        tmpGlyph.xAdvance = glyphInfo[index].xAdvance;
        this.m_glyphInfoList.Add(tmpGlyph);
        this.m_characterSet[index] = tmpGlyph.id;
      }
      this.m_glyphInfoList = this.m_glyphInfoList.OrderBy<TMP_Glyph, int>((Func<TMP_Glyph, int>) (s => s.id)).ToList<TMP_Glyph>();
    }

    public void AddKerningInfo(KerningTable kerningTable)
    {
      this.m_kerningInfo = kerningTable;
    }

    public void ReadFontDefinition()
    {
      if (this.m_fontInfo == null)
        return;
      this.m_characterDictionary = new Dictionary<int, TMP_Glyph>();
      using (List<TMP_Glyph>.Enumerator enumerator = this.m_glyphInfoList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          TMP_Glyph current = enumerator.Current;
          if (!this.m_characterDictionary.ContainsKey(current.id))
            this.m_characterDictionary.Add(current.id, current);
        }
      }
      TMP_Glyph tmpGlyph1 = new TMP_Glyph();
      if (this.m_characterDictionary.ContainsKey(32))
      {
        this.m_characterDictionary[32].width = this.m_characterDictionary[32].xAdvance;
        this.m_characterDictionary[32].height = this.m_fontInfo.Ascender - this.m_fontInfo.Descender;
        this.m_characterDictionary[32].yOffset = this.m_fontInfo.Ascender;
      }
      else
      {
        TMP_Glyph tmpGlyph2 = new TMP_Glyph();
        tmpGlyph2.id = 32;
        tmpGlyph2.x = 0.0f;
        tmpGlyph2.y = 0.0f;
        tmpGlyph2.width = this.m_fontInfo.Ascender / 5f;
        tmpGlyph2.height = this.m_fontInfo.Ascender - this.m_fontInfo.Descender;
        tmpGlyph2.xOffset = 0.0f;
        tmpGlyph2.yOffset = this.m_fontInfo.Ascender;
        tmpGlyph2.xAdvance = this.m_fontInfo.PointSize / 4f;
        this.m_characterDictionary.Add(32, tmpGlyph2);
      }
      if (!this.m_characterDictionary.ContainsKey(160))
        this.m_characterDictionary.Add(160, TMP_Glyph.Clone(this.m_characterDictionary[32]));
      if (!this.m_characterDictionary.ContainsKey(8203))
      {
        TMP_Glyph tmpGlyph2 = TMP_Glyph.Clone(this.m_characterDictionary[32]);
        tmpGlyph2.width = 0.0f;
        tmpGlyph2.xAdvance = 0.0f;
        this.m_characterDictionary.Add(8203, tmpGlyph2);
      }
      if (!this.m_characterDictionary.ContainsKey(10))
      {
        TMP_Glyph tmpGlyph2 = new TMP_Glyph();
        tmpGlyph2.id = 10;
        tmpGlyph2.x = 0.0f;
        tmpGlyph2.y = 0.0f;
        tmpGlyph2.width = 10f;
        tmpGlyph2.height = this.m_characterDictionary[32].height;
        tmpGlyph2.xOffset = 0.0f;
        tmpGlyph2.yOffset = this.m_characterDictionary[32].yOffset;
        tmpGlyph2.xAdvance = 0.0f;
        this.m_characterDictionary.Add(10, tmpGlyph2);
        if (!this.m_characterDictionary.ContainsKey(13))
          this.m_characterDictionary.Add(13, tmpGlyph2);
      }
      if (!this.m_characterDictionary.ContainsKey(9))
      {
        TMP_Glyph tmpGlyph2 = new TMP_Glyph();
        tmpGlyph2.id = 9;
        tmpGlyph2.x = this.m_characterDictionary[32].x;
        tmpGlyph2.y = this.m_characterDictionary[32].y;
        tmpGlyph2.width = (float) ((double) this.m_characterDictionary[32].width * (double) this.tabSize + ((double) this.m_characterDictionary[32].xAdvance - (double) this.m_characterDictionary[32].width) * (double) ((int) this.tabSize - 1));
        tmpGlyph2.height = this.m_characterDictionary[32].height;
        tmpGlyph2.xOffset = this.m_characterDictionary[32].xOffset;
        tmpGlyph2.yOffset = this.m_characterDictionary[32].yOffset;
        tmpGlyph2.xAdvance = this.m_characterDictionary[32].xAdvance * (float) this.tabSize;
        this.m_characterDictionary.Add(9, tmpGlyph2);
      }
      this.m_fontInfo.TabWidth = this.m_characterDictionary[9].xAdvance;
      if ((double) this.m_fontInfo.Scale == 0.0)
        this.m_fontInfo.Scale = 1f;
      this.m_kerningDictionary = new Dictionary<int, KerningPair>();
      List<KerningPair> kerningPairList = this.m_kerningInfo.kerningPairs;
      for (int index = 0; index < kerningPairList.Count; ++index)
      {
        KerningPair kerningPair = kerningPairList[index];
        KerningPairKey kerningPairKey = new KerningPairKey(kerningPair.AscII_Left, kerningPair.AscII_Right);
        if (!this.m_kerningDictionary.ContainsKey(kerningPairKey.key))
          this.m_kerningDictionary.Add(kerningPairKey.key, kerningPair);
        else
          Debug.Log((object) ("Kerning Key for [" + (object) kerningPairKey.ascii_Left + "] and [" + (object) kerningPairKey.ascii_Right + "] already exists."));
      }
      this.m_lineBreakingInfo = new LineBreakingTable();
      TextAsset file1 = Resources.Load("LineBreaking Leading Characters", typeof (TextAsset)) as TextAsset;
      if ((UnityEngine.Object) file1 != (UnityEngine.Object) null)
        this.m_lineBreakingInfo.leadingCharacters = this.GetCharacters(file1);
      TextAsset file2 = Resources.Load("LineBreaking Following Characters", typeof (TextAsset)) as TextAsset;
      if ((UnityEngine.Object) file2 != (UnityEngine.Object) null)
        this.m_lineBreakingInfo.followingCharacters = this.GetCharacters(file2);
      this.hashCode = TMP_TextUtilities.GetSimpleHashCode(this.name);
      this.materialHashCode = TMP_TextUtilities.GetSimpleHashCode(this.material.name);
    }

    private Dictionary<int, char> GetCharacters(TextAsset file)
    {
      Dictionary<int, char> dictionary = new Dictionary<int, char>();
      foreach (char ch in file.text)
      {
        if (!dictionary.ContainsKey((int) ch))
          dictionary.Add((int) ch, ch);
      }
      return dictionary;
    }

    public bool HasCharacter(int character)
    {
      return this.m_characterDictionary != null && this.m_characterDictionary.ContainsKey(character);
    }

    public bool HasCharacter(char character)
    {
      return this.m_characterDictionary != null && this.m_characterDictionary.ContainsKey((int) character);
    }

    public bool HasCharacters(string text, out List<char> missingCharacters)
    {
      if (this.m_characterDictionary == null)
      {
        missingCharacters = (List<char>) null;
        return false;
      }
      missingCharacters = new List<char>();
      for (int index = 0; index < text.Length; ++index)
      {
        if (!this.m_characterDictionary.ContainsKey((int) text[index]))
          missingCharacters.Add(text[index]);
      }
      return missingCharacters.Count == 0;
    }

    public enum FontAssetTypes
    {
      None,
      SDF,
      Bitmap,
    }
  }
}
