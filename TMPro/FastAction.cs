﻿// Decompiled with JetBrains decompiler
// Type: TMPro.FastAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace TMPro
{
  public class FastAction
  {
    private LinkedList<Action> delegates = new LinkedList<Action>();
    private Dictionary<Action, LinkedListNode<Action>> lookup = new Dictionary<Action, LinkedListNode<Action>>();

    public void Add(Action rhs)
    {
      if (this.lookup.ContainsKey(rhs))
        return;
      this.lookup[rhs] = this.delegates.AddLast(rhs);
    }

    public void Remove(Action rhs)
    {
      LinkedListNode<Action> node;
      if (!this.lookup.TryGetValue(rhs, out node))
        return;
      this.lookup.Remove(rhs);
      this.delegates.Remove(node);
    }

    public void Call()
    {
      for (LinkedListNode<Action> linkedListNode = this.delegates.First; linkedListNode != null; linkedListNode = linkedListNode.Next)
        linkedListNode.Value();
    }
  }
}
