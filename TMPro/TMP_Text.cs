﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_Text
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace TMPro
{
  public class TMP_Text : MaskableGraphic
  {
    protected static Color32 s_colorWhite = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
    protected static Vector2 k_InfinityVectorPositive = new Vector2(1000000f, 1000000f);
    protected static Vector2 k_InfinityVectorNegative = new Vector2(-1000000f, -1000000f);
    protected MaterialReference[] m_materialReferences = new MaterialReference[32];
    protected Dictionary<int, int> m_materialReferenceIndexLookup = new Dictionary<int, int>();
    protected TMP_XmlTagStack<MaterialReference> m_materialReferenceStack = new TMP_XmlTagStack<MaterialReference>(new MaterialReference[16]);
    [SerializeField]
    [FormerlySerializedAs("m_fontColor")]
    protected Color32 m_fontColor32 = (Color32) Color.white;
    [SerializeField]
    protected Color m_fontColor = Color.white;
    [SerializeField]
    protected VertexGradient m_fontColorGradient = new VertexGradient(Color.white);
    [SerializeField]
    protected Color32 m_faceColor = (Color32) Color.white;
    [SerializeField]
    protected Color32 m_outlineColor = (Color32) Color.black;
    [SerializeField]
    protected float m_fontSize = 36f;
    [SerializeField]
    protected float m_fontSizeBase = 36f;
    protected TMP_XmlTagStack<float> m_sizeStack = new TMP_XmlTagStack<float>(new float[16]);
    [SerializeField]
    protected int m_fontWeight = 400;
    protected TMP_XmlTagStack<int> m_fontWeightStack = new TMP_XmlTagStack<int>(new int[16]);
    protected Vector3[] m_textContainerLocalCorners = new Vector3[4];
    [SerializeField]
    protected float m_wordWrappingRatios = 0.4f;
    [SerializeField]
    protected bool m_isRichText = true;
    protected bool m_parseCtrlCharacters = true;
    [SerializeField]
    protected bool m_ignoreCulling = true;
    protected int m_maxVisibleCharacters = 99999;
    protected int m_maxVisibleWords = 99999;
    protected int m_maxVisibleLines = 99999;
    [SerializeField]
    protected int m_pageToDisplay = 1;
    [SerializeField]
    protected Vector4 m_margin = new Vector4(0.0f, 0.0f, 0.0f, 0.0f);
    protected float m_width = -1f;
    protected float m_flexibleHeight = -1f;
    protected float m_flexibleWidth = -1f;
    protected float m_preferredWidth = 9999f;
    protected float m_preferredHeight = 9999f;
    protected char[] m_htmlTag = new char[64];
    protected XML_TagAttribute[] m_xmlAttribute = new XML_TagAttribute[8];
    protected TMP_XmlTagStack<float> m_indentStack = new TMP_XmlTagStack<float>(new float[16]);
    protected TMP_LinkInfo tag_LinkInfo = new TMP_LinkInfo();
    protected char[] m_input_CharArray = new char[256];
    protected Color32 m_htmlColor = (Color32) new Color((float) byte.MaxValue, (float) byte.MaxValue, (float) byte.MaxValue, 128f);
    protected TMP_XmlTagStack<Color32> m_colorStack = new TMP_XmlTagStack<Color32>(new Color32[16]);
    protected TMP_XmlTagStack<int> m_styleStack = new TMP_XmlTagStack<int>(new int[16]);
    protected TMP_XmlTagStack<int> m_actionStack = new TMP_XmlTagStack<int>(new int[16]);
    private readonly float[] k_Power = new float[10]{ 0.5f, 0.05f, 0.005f, 0.0005f, 5E-05f, 5E-06f, 5E-07f, 5E-08f, 5E-09f, 5E-10f };
    [SerializeField]
    protected string m_text;
    [SerializeField]
    protected TMP_FontAsset m_fontAsset;
    protected TMP_FontAsset m_currentFontAsset;
    protected bool m_isSDFShader;
    [SerializeField]
    protected Material m_sharedMaterial;
    protected Material m_currentMaterial;
    protected int m_currentMaterialIndex;
    protected int m_sharedMaterialHashCode;
    [SerializeField]
    protected Material[] m_fontSharedMaterials;
    [SerializeField]
    protected Material m_fontMaterial;
    [SerializeField]
    protected Material[] m_fontMaterials;
    protected bool m_isMaterialDirty;
    [SerializeField]
    protected bool m_enableVertexGradient;
    protected TMP_SpriteAsset m_spriteAsset;
    [SerializeField]
    protected bool m_tintAllSprites;
    protected bool m_tintSprite;
    protected Color32 m_spriteColor;
    [SerializeField]
    protected bool m_overrideHtmlColors;
    protected float m_outlineWidth;
    protected float m_currentFontSize;
    protected int m_fontWeightInternal;
    [SerializeField]
    protected bool m_enableAutoSizing;
    protected float m_maxFontSize;
    protected float m_minFontSize;
    [SerializeField]
    protected float m_fontSizeMin;
    [SerializeField]
    protected float m_fontSizeMax;
    [SerializeField]
    protected FontStyles m_fontStyle;
    protected FontStyles m_style;
    protected bool m_isUsingBold;
    [SerializeField]
    [FormerlySerializedAs("m_lineJustification")]
    protected TextAlignmentOptions m_textAlignment;
    protected TextAlignmentOptions m_lineJustification;
    [SerializeField]
    protected float m_characterSpacing;
    protected float m_cSpacing;
    protected float m_monoSpacing;
    [SerializeField]
    protected float m_lineSpacing;
    protected float m_lineSpacingDelta;
    protected float m_lineHeight;
    [SerializeField]
    protected float m_lineSpacingMax;
    [SerializeField]
    protected float m_paragraphSpacing;
    [SerializeField]
    protected float m_charWidthMaxAdj;
    protected float m_charWidthAdjDelta;
    [SerializeField]
    protected bool m_enableWordWrapping;
    protected bool m_isCharacterWrappingEnabled;
    protected bool m_isNonBreakingSpace;
    protected bool m_isIgnoringAlignment;
    [SerializeField]
    protected TextOverflowModes m_overflowMode;
    protected bool m_isTextTruncated;
    [SerializeField]
    protected bool m_enableKerning;
    [SerializeField]
    protected bool m_enableExtraPadding;
    [SerializeField]
    protected bool checkPaddingRequired;
    protected bool m_isOverlay;
    [SerializeField]
    protected bool m_isOrthographic;
    [SerializeField]
    protected bool m_isCullingEnabled;
    [SerializeField]
    protected TextureMappingOptions m_horizontalMapping;
    [SerializeField]
    protected TextureMappingOptions m_verticalMapping;
    protected TextRenderFlags m_renderMode;
    protected bool m_isNewPage;
    protected float m_marginLeft;
    protected float m_marginRight;
    protected float m_marginWidth;
    protected float m_marginHeight;
    protected TMP_TextInfo m_textInfo;
    [SerializeField]
    protected bool m_havePropertiesChanged;
    [SerializeField]
    protected bool m_isUsingLegacyAnimationComponent;
    protected Transform m_transform;
    protected RectTransform m_rectTransform;
    protected Mesh m_mesh;
    protected float m_minHeight;
    protected float m_minWidth;
    protected float m_renderedWidth;
    protected float m_renderedHeight;
    protected int m_layoutPriority;
    protected bool m_isCalculateSizeRequired;
    protected bool m_isLayoutDirty;
    protected bool m_verticesAlreadyDirty;
    protected bool m_layoutAlreadyDirty;
    [SerializeField]
    protected bool m_isInputParsingRequired;
    [SerializeField]
    protected bool m_isRightToLeft;
    [SerializeField]
    protected TMP_Text.TextInputSources m_inputSource;
    protected string old_text;
    protected float old_arg0;
    protected float old_arg1;
    protected float old_arg2;
    protected float m_fontScale;
    protected float m_fontScaleMultiplier;
    protected float tag_LineIndent;
    protected float tag_Indent;
    protected bool tag_NoParsing;
    protected bool m_isParsingText;
    protected int[] m_char_buffer;
    private TMP_CharacterInfo[] m_internalCharacterInfo;
    private int m_charArray_Length;
    protected int m_totalCharacterCount;
    protected TMP_Settings m_settings;
    protected bool m_warningsDisabled;
    protected int m_characterCount;
    protected int m_visibleCharacterCount;
    protected int m_visibleSpriteCount;
    protected int m_firstCharacterOfLine;
    protected int m_firstVisibleCharacterOfLine;
    protected int m_lastCharacterOfLine;
    protected int m_lastVisibleCharacterOfLine;
    protected int m_lineNumber;
    protected int m_pageNumber;
    protected float m_maxAscender;
    protected float m_maxDescender;
    protected float m_maxLineAscender;
    protected float m_maxLineDescender;
    protected float m_startOfLineAscender;
    protected float m_lineOffset;
    protected Extents m_meshExtents;
    protected float m_tabSpacing;
    protected float m_spacing;
    protected bool IsRectTransformDriven;
    protected float m_padding;
    protected float m_baselineOffset;
    protected float m_xAdvance;
    protected TMP_TextElementType m_textElementType;
    protected TMP_TextElement m_cached_TextElement;
    protected TMP_Glyph m_cached_Underline_GlyphInfo;
    protected TMP_Glyph m_cached_Ellipsis_GlyphInfo;
    protected TMP_SpriteAsset m_defaultSpriteAsset;
    protected TMP_SpriteAsset m_currentSpriteAsset;
    protected int m_spriteCount;
    protected int m_spriteIndex;
    protected InlineGraphicManager m_inlineGraphics;

    public string text
    {
      get
      {
        return this.m_text;
      }
      set
      {
        if (this.m_text == value)
          return;
        this.m_text = value;
        this.m_inputSource = TMP_Text.TextInputSources.Text;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.m_isInputParsingRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public TMP_FontAsset font
    {
      get
      {
        return this.m_fontAsset;
      }
      set
      {
        if ((UnityEngine.Object) this.m_fontAsset == (UnityEngine.Object) value)
          return;
        this.m_fontAsset = value;
        this.LoadFontAsset();
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public virtual Material fontSharedMaterial
    {
      get
      {
        return this.m_sharedMaterial;
      }
      set
      {
        if ((UnityEngine.Object) this.m_sharedMaterial == (UnityEngine.Object) value)
          return;
        this.SetSharedMaterial(value);
        this.m_havePropertiesChanged = true;
        this.SetVerticesDirty();
        this.SetMaterialDirty();
      }
    }

    public virtual Material[] fontSharedMaterials
    {
      get
      {
        return this.GetSharedMaterials();
      }
      set
      {
        this.SetSharedMaterials(value);
        this.m_havePropertiesChanged = true;
        this.SetVerticesDirty();
        this.SetMaterialDirty();
      }
    }

    public Material fontMaterial
    {
      get
      {
        return this.GetMaterial(this.m_sharedMaterial);
      }
      set
      {
        if ((UnityEngine.Object) this.m_sharedMaterial != (UnityEngine.Object) null && this.m_sharedMaterial.GetInstanceID() == value.GetInstanceID())
          return;
        this.m_sharedMaterial = value;
        this.m_padding = this.GetPaddingForMaterial();
        this.m_havePropertiesChanged = true;
        this.SetVerticesDirty();
        this.SetMaterialDirty();
      }
    }

    public virtual Material[] fontMaterials
    {
      get
      {
        return this.GetMaterials(this.m_fontSharedMaterials);
      }
      set
      {
        this.SetSharedMaterials(value);
        this.m_havePropertiesChanged = true;
        this.SetVerticesDirty();
        this.SetMaterialDirty();
      }
    }

    public new Color color
    {
      get
      {
        return this.m_fontColor;
      }
      set
      {
        if (this.m_fontColor == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_fontColor = value;
        this.SetVerticesDirty();
      }
    }

    public float alpha
    {
      get
      {
        return this.m_fontColor.a;
      }
      set
      {
        if ((double) this.m_fontColor.a == (double) value)
          return;
        this.m_fontColor.a = value;
        this.m_havePropertiesChanged = true;
        this.SetVerticesDirty();
      }
    }

    public bool enableVertexGradient
    {
      get
      {
        return this.m_enableVertexGradient;
      }
      set
      {
        if (this.m_enableVertexGradient == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_enableVertexGradient = value;
        this.SetVerticesDirty();
      }
    }

    public VertexGradient colorGradient
    {
      get
      {
        return this.m_fontColorGradient;
      }
      set
      {
        this.m_havePropertiesChanged = true;
        this.m_fontColorGradient = value;
        this.SetVerticesDirty();
      }
    }

    public TMP_SpriteAsset spriteAsset
    {
      get
      {
        return this.m_spriteAsset;
      }
      set
      {
        this.m_spriteAsset = value;
      }
    }

    public bool tintAllSprites
    {
      get
      {
        return this.m_tintAllSprites;
      }
      set
      {
        if (this.m_tintAllSprites == value)
          return;
        this.m_tintAllSprites = value;
        this.m_havePropertiesChanged = true;
        this.SetVerticesDirty();
      }
    }

    public bool overrideColorTags
    {
      get
      {
        return this.m_overrideHtmlColors;
      }
      set
      {
        if (this.m_overrideHtmlColors == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_overrideHtmlColors = value;
        this.SetVerticesDirty();
      }
    }

    public Color32 faceColor
    {
      get
      {
        if ((UnityEngine.Object) this.m_sharedMaterial == (UnityEngine.Object) null)
          return this.m_faceColor;
        this.m_faceColor = (Color32) this.m_sharedMaterial.GetColor(ShaderUtilities.ID_FaceColor);
        return this.m_faceColor;
      }
      set
      {
        if (this.m_faceColor.Compare(value))
          return;
        this.SetFaceColor(value);
        this.m_havePropertiesChanged = true;
        this.m_faceColor = value;
        this.SetVerticesDirty();
        this.SetMaterialDirty();
      }
    }

    public Color32 outlineColor
    {
      get
      {
        if ((UnityEngine.Object) this.m_sharedMaterial == (UnityEngine.Object) null)
          return this.m_outlineColor;
        this.m_outlineColor = (Color32) this.m_sharedMaterial.GetColor(ShaderUtilities.ID_OutlineColor);
        return this.m_outlineColor;
      }
      set
      {
        if (this.m_outlineColor.Compare(value))
          return;
        this.SetOutlineColor(value);
        this.m_havePropertiesChanged = true;
        this.m_outlineColor = value;
        this.SetVerticesDirty();
      }
    }

    public float outlineWidth
    {
      get
      {
        if ((UnityEngine.Object) this.m_sharedMaterial == (UnityEngine.Object) null)
          return this.m_outlineWidth;
        this.m_outlineWidth = this.m_sharedMaterial.GetFloat(ShaderUtilities.ID_OutlineWidth);
        return this.m_outlineWidth;
      }
      set
      {
        if ((double) this.m_outlineWidth == (double) value)
          return;
        this.SetOutlineThickness(value);
        this.m_havePropertiesChanged = true;
        this.m_outlineWidth = value;
        this.SetVerticesDirty();
      }
    }

    public float fontSize
    {
      get
      {
        return this.m_fontSize;
      }
      set
      {
        if ((double) this.m_fontSize == (double) value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
        this.m_fontSize = value;
        if (this.m_enableAutoSizing)
          return;
        this.m_fontSizeBase = this.m_fontSize;
      }
    }

    public float fontScale
    {
      get
      {
        return this.m_fontScale;
      }
    }

    public int fontWeight
    {
      get
      {
        return this.m_fontWeight;
      }
      set
      {
        if (this.m_fontWeight == value)
          return;
        this.m_fontWeight = value;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public float pixelsPerUnit
    {
      get
      {
        Canvas canvas = this.canvas;
        if (!(bool) ((UnityEngine.Object) canvas))
          return 1f;
        if (!(bool) ((UnityEngine.Object) this.font))
          return canvas.scaleFactor;
        if ((UnityEngine.Object) this.m_currentFontAsset == (UnityEngine.Object) null || (double) this.m_currentFontAsset.fontInfo.PointSize <= 0.0 || (double) this.m_fontSize <= 0.0)
          return 1f;
        return this.m_fontSize / this.m_currentFontAsset.fontInfo.PointSize;
      }
    }

    public bool enableAutoSizing
    {
      get
      {
        return this.m_enableAutoSizing;
      }
      set
      {
        if (this.m_enableAutoSizing == value)
          return;
        this.m_enableAutoSizing = value;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public float fontSizeMin
    {
      get
      {
        return this.m_fontSizeMin;
      }
      set
      {
        if ((double) this.m_fontSizeMin == (double) value)
          return;
        this.m_fontSizeMin = value;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public float fontSizeMax
    {
      get
      {
        return this.m_fontSizeMax;
      }
      set
      {
        if ((double) this.m_fontSizeMax == (double) value)
          return;
        this.m_fontSizeMax = value;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public FontStyles fontStyle
    {
      get
      {
        return this.m_fontStyle;
      }
      set
      {
        if (this.m_fontStyle == value)
          return;
        this.m_fontStyle = value;
        this.m_havePropertiesChanged = true;
        this.checkPaddingRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public bool isUsingBold
    {
      get
      {
        return this.m_isUsingBold;
      }
    }

    public TextAlignmentOptions alignment
    {
      get
      {
        return this.m_textAlignment;
      }
      set
      {
        if (this.m_textAlignment == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_textAlignment = value;
        this.SetVerticesDirty();
      }
    }

    public float characterSpacing
    {
      get
      {
        return this.m_characterSpacing;
      }
      set
      {
        if ((double) this.m_characterSpacing == (double) value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
        this.m_characterSpacing = value;
      }
    }

    public float lineSpacing
    {
      get
      {
        return this.m_lineSpacing;
      }
      set
      {
        if ((double) this.m_lineSpacing == (double) value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
        this.m_lineSpacing = value;
      }
    }

    public float paragraphSpacing
    {
      get
      {
        return this.m_paragraphSpacing;
      }
      set
      {
        if ((double) this.m_paragraphSpacing == (double) value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
        this.m_paragraphSpacing = value;
      }
    }

    public float characterWidthAdjustment
    {
      get
      {
        return this.m_charWidthMaxAdj;
      }
      set
      {
        if ((double) this.m_charWidthMaxAdj == (double) value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
        this.m_charWidthMaxAdj = value;
      }
    }

    public bool enableWordWrapping
    {
      get
      {
        return this.m_enableWordWrapping;
      }
      set
      {
        if (this.m_enableWordWrapping == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_isInputParsingRequired = true;
        this.m_isCalculateSizeRequired = true;
        this.m_enableWordWrapping = value;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public float wordWrappingRatios
    {
      get
      {
        return this.m_wordWrappingRatios;
      }
      set
      {
        if ((double) this.m_wordWrappingRatios == (double) value)
          return;
        this.m_wordWrappingRatios = value;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public TextOverflowModes OverflowMode
    {
      get
      {
        return this.m_overflowMode;
      }
      set
      {
        if (this.m_overflowMode == value)
          return;
        this.m_overflowMode = value;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public bool enableKerning
    {
      get
      {
        return this.m_enableKerning;
      }
      set
      {
        if (this.m_enableKerning == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
        this.m_enableKerning = value;
      }
    }

    public bool extraPadding
    {
      get
      {
        return this.m_enableExtraPadding;
      }
      set
      {
        if (this.m_enableExtraPadding == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_enableExtraPadding = value;
        this.UpdateMeshPadding();
        this.SetVerticesDirty();
      }
    }

    public bool richText
    {
      get
      {
        return this.m_isRichText;
      }
      set
      {
        if (this.m_isRichText == value)
          return;
        this.m_isRichText = value;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
        this.m_isInputParsingRequired = true;
      }
    }

    public bool parseCtrlCharacters
    {
      get
      {
        return this.m_parseCtrlCharacters;
      }
      set
      {
        if (this.m_parseCtrlCharacters == value)
          return;
        this.m_parseCtrlCharacters = value;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
        this.m_isInputParsingRequired = true;
      }
    }

    public bool isOverlay
    {
      get
      {
        return this.m_isOverlay;
      }
      set
      {
        if (this.m_isOverlay == value)
          return;
        this.m_isOverlay = value;
        this.SetShaderDepth();
        this.m_havePropertiesChanged = true;
        this.SetVerticesDirty();
      }
    }

    public bool isOrthographic
    {
      get
      {
        return this.m_isOrthographic;
      }
      set
      {
        if (this.m_isOrthographic == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_isOrthographic = value;
        this.SetVerticesDirty();
      }
    }

    public bool enableCulling
    {
      get
      {
        return this.m_isCullingEnabled;
      }
      set
      {
        if (this.m_isCullingEnabled == value)
          return;
        this.m_isCullingEnabled = value;
        this.SetCulling();
        this.m_havePropertiesChanged = true;
      }
    }

    public bool ignoreVisibility
    {
      get
      {
        return this.m_ignoreCulling;
      }
      set
      {
        if (this.m_ignoreCulling == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_ignoreCulling = value;
      }
    }

    public TextureMappingOptions horizontalMapping
    {
      get
      {
        return this.m_horizontalMapping;
      }
      set
      {
        if (this.m_horizontalMapping == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_horizontalMapping = value;
        this.SetVerticesDirty();
      }
    }

    public TextureMappingOptions verticalMapping
    {
      get
      {
        return this.m_verticalMapping;
      }
      set
      {
        if (this.m_verticalMapping == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_verticalMapping = value;
        this.SetVerticesDirty();
      }
    }

    public TextRenderFlags renderMode
    {
      get
      {
        return this.m_renderMode;
      }
      set
      {
        if (this.m_renderMode == value)
          return;
        this.m_renderMode = value;
        this.m_havePropertiesChanged = true;
      }
    }

    public int maxVisibleCharacters
    {
      get
      {
        return this.m_maxVisibleCharacters;
      }
      set
      {
        if (this.m_maxVisibleCharacters == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_maxVisibleCharacters = value;
        this.SetVerticesDirty();
      }
    }

    public int maxVisibleWords
    {
      get
      {
        return this.m_maxVisibleWords;
      }
      set
      {
        if (this.m_maxVisibleWords == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_maxVisibleWords = value;
        this.SetVerticesDirty();
      }
    }

    public int maxVisibleLines
    {
      get
      {
        return this.m_maxVisibleLines;
      }
      set
      {
        if (this.m_maxVisibleLines == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_isInputParsingRequired = true;
        this.m_maxVisibleLines = value;
        this.SetVerticesDirty();
      }
    }

    public int pageToDisplay
    {
      get
      {
        return this.m_pageToDisplay;
      }
      set
      {
        if (this.m_pageToDisplay == value)
          return;
        this.m_havePropertiesChanged = true;
        this.m_pageToDisplay = value;
        this.SetVerticesDirty();
      }
    }

    public virtual Vector4 margin
    {
      get
      {
        return this.m_margin;
      }
      set
      {
        if (this.m_margin == value)
          return;
        this.m_margin = value;
        this.ComputeMarginSize();
        this.m_havePropertiesChanged = true;
        this.SetVerticesDirty();
      }
    }

    public TMP_TextInfo textInfo
    {
      get
      {
        return this.m_textInfo;
      }
    }

    public bool havePropertiesChanged
    {
      get
      {
        return this.m_havePropertiesChanged;
      }
      set
      {
        if (this.m_havePropertiesChanged == value)
          return;
        this.m_havePropertiesChanged = value;
        this.SetVerticesDirty();
        this.SetLayoutDirty();
      }
    }

    public bool isUsingLegacyAnimationComponent
    {
      get
      {
        return this.m_isUsingLegacyAnimationComponent;
      }
      set
      {
        this.m_isUsingLegacyAnimationComponent = value;
      }
    }

    public new Transform transform
    {
      get
      {
        if ((UnityEngine.Object) this.m_transform == (UnityEngine.Object) null)
          this.m_transform = this.GetComponent<Transform>();
        return this.m_transform;
      }
    }

    public new RectTransform rectTransform
    {
      get
      {
        if ((UnityEngine.Object) this.m_rectTransform == (UnityEngine.Object) null)
          this.m_rectTransform = this.GetComponent<RectTransform>();
        return this.m_rectTransform;
      }
    }

    public virtual bool autoSizeTextContainer { get; set; }

    public virtual Mesh mesh
    {
      get
      {
        return this.m_mesh;
      }
    }

    public float flexibleHeight
    {
      get
      {
        return this.m_flexibleHeight;
      }
    }

    public float flexibleWidth
    {
      get
      {
        return this.m_flexibleWidth;
      }
    }

    public float minHeight
    {
      get
      {
        return this.m_minHeight;
      }
    }

    public float minWidth
    {
      get
      {
        return this.m_minWidth;
      }
    }

    public virtual float preferredWidth
    {
      get
      {
        if ((double) this.m_preferredWidth == 9999.0)
          return this.GetPreferredWidth();
        return this.m_preferredWidth;
      }
    }

    public virtual float preferredHeight
    {
      get
      {
        if ((double) this.m_preferredHeight == 9999.0)
          return this.GetPreferredHeight();
        return this.m_preferredHeight;
      }
    }

    public int layoutPriority
    {
      get
      {
        return this.m_layoutPriority;
      }
    }

    protected virtual void LoadFontAsset()
    {
    }

    protected virtual void SetSharedMaterial(Material mat)
    {
    }

    protected virtual Material GetMaterial(Material mat)
    {
      return (Material) null;
    }

    protected virtual void SetFontBaseMaterial(Material mat)
    {
    }

    protected virtual Material[] GetSharedMaterials()
    {
      return (Material[]) null;
    }

    protected virtual void SetSharedMaterials(Material[] materials)
    {
    }

    protected virtual Material[] GetMaterials(Material[] mats)
    {
      return (Material[]) null;
    }

    protected virtual Material CreateMaterialInstance(Material source)
    {
      Material material = new Material(source);
      material.shaderKeywords = source.shaderKeywords;
      material.name += " (Instance)";
      return material;
    }

    protected virtual void SetFaceColor(Color32 color)
    {
    }

    protected virtual void SetOutlineColor(Color32 color)
    {
    }

    protected virtual void SetOutlineThickness(float thickness)
    {
    }

    protected virtual void SetShaderDepth()
    {
    }

    protected virtual void SetCulling()
    {
    }

    protected virtual float GetPaddingForMaterial()
    {
      return 0.0f;
    }

    protected virtual float GetPaddingForMaterial(Material mat)
    {
      return 0.0f;
    }

    protected virtual Vector3[] GetTextContainerLocalCorners()
    {
      return (Vector3[]) null;
    }

    public virtual void ForceMeshUpdate()
    {
    }

    public virtual void UpdateGeometry(Mesh mesh, int index)
    {
    }

    public virtual void UpdateVertexData()
    {
    }

    public virtual void UpdateMeshPadding()
    {
    }

    public void SetText(string text)
    {
      this.StringToCharArray(text, ref this.m_char_buffer);
      this.m_inputSource = TMP_Text.TextInputSources.SetCharArray;
      this.m_isInputParsingRequired = true;
      this.m_havePropertiesChanged = true;
      this.m_isCalculateSizeRequired = true;
      this.SetVerticesDirty();
      this.SetLayoutDirty();
    }

    public void SetText(string text, float arg0)
    {
      this.SetText(text, arg0, (float) byte.MaxValue, (float) byte.MaxValue);
    }

    public void SetText(string text, float arg0, float arg1)
    {
      this.SetText(text, arg0, arg1, (float) byte.MaxValue);
    }

    public void SetText(string text, float arg0, float arg1, float arg2)
    {
      if (text == this.old_text && (double) arg0 == (double) this.old_arg0 && ((double) arg1 == (double) this.old_arg1 && (double) arg2 == (double) this.old_arg2))
        return;
      this.old_text = text;
      this.old_arg1 = (float) byte.MaxValue;
      this.old_arg2 = (float) byte.MaxValue;
      int precision = 0;
      int index1 = 0;
      for (int index2 = 0; index2 < text.Length; ++index2)
      {
        char ch = text[index2];
        if ((int) ch == 123)
        {
          if ((int) text[index2 + 2] == 58)
            precision = (int) text[index2 + 3] - 48;
          switch (text[index2 + 1])
          {
            case '0':
              this.old_arg0 = arg0;
              this.AddFloatToCharArray(arg0, ref index1, precision);
              break;
            case '1':
              this.old_arg1 = arg1;
              this.AddFloatToCharArray(arg1, ref index1, precision);
              break;
            case '2':
              this.old_arg2 = arg2;
              this.AddFloatToCharArray(arg2, ref index1, precision);
              break;
          }
          if ((int) text[index2 + 2] == 58)
            index2 += 4;
          else
            index2 += 2;
        }
        else
        {
          this.m_input_CharArray[index1] = ch;
          ++index1;
        }
      }
      this.m_input_CharArray[index1] = char.MinValue;
      this.m_charArray_Length = index1;
      this.m_inputSource = TMP_Text.TextInputSources.SetText;
      this.m_isInputParsingRequired = true;
      this.m_havePropertiesChanged = true;
      this.m_isCalculateSizeRequired = true;
      this.SetVerticesDirty();
      this.SetLayoutDirty();
    }

    public void SetText(StringBuilder text)
    {
      this.StringBuilderToIntArray(text, ref this.m_char_buffer);
      this.m_inputSource = TMP_Text.TextInputSources.SetCharArray;
      this.m_isInputParsingRequired = true;
      this.m_havePropertiesChanged = true;
      this.m_isCalculateSizeRequired = true;
      this.SetVerticesDirty();
      this.SetLayoutDirty();
    }

    public void SetCharArray(char[] charArray)
    {
      if (charArray == null || charArray.Length == 0)
        return;
      if (this.m_char_buffer.Length <= charArray.Length)
        this.m_char_buffer = new int[Mathf.NextPowerOfTwo(charArray.Length + 1)];
      int index1 = 0;
      for (int index2 = 0; index2 < charArray.Length; ++index2)
      {
        if ((int) charArray[index2] == 92 && index2 < charArray.Length - 1)
        {
          int num = (int) charArray[index2 + 1];
          switch (num)
          {
            case 114:
              this.m_char_buffer[index1] = 13;
              ++index2;
              ++index1;
              continue;
            case 116:
              this.m_char_buffer[index1] = 9;
              ++index2;
              ++index1;
              continue;
            default:
              if (num == 110)
              {
                this.m_char_buffer[index1] = 10;
                ++index2;
                ++index1;
                continue;
              }
              break;
          }
        }
        this.m_char_buffer[index1] = (int) charArray[index2];
        ++index1;
      }
      this.m_char_buffer[index1] = 0;
      this.m_inputSource = TMP_Text.TextInputSources.SetCharArray;
      this.m_havePropertiesChanged = true;
      this.m_isInputParsingRequired = true;
    }

    protected void SetTextArrayToCharArray(char[] charArray, ref int[] charBuffer)
    {
      if (charArray == null || this.m_charArray_Length == 0)
        return;
      if (charBuffer.Length <= this.m_charArray_Length)
      {
        int length = this.m_charArray_Length <= 1024 ? Mathf.NextPowerOfTwo(this.m_charArray_Length + 1) : this.m_charArray_Length + 256;
        charBuffer = new int[length];
      }
      int index1 = 0;
      for (int index2 = 0; index2 < this.m_charArray_Length; ++index2)
      {
        if (char.IsHighSurrogate(charArray[index2]) && char.IsLowSurrogate(charArray[index2 + 1]))
        {
          charBuffer[index1] = char.ConvertToUtf32(charArray[index2], charArray[index2 + 1]);
          ++index2;
          ++index1;
        }
        else
        {
          charBuffer[index1] = (int) charArray[index2];
          ++index1;
        }
      }
      charBuffer[index1] = 0;
    }

    protected void StringToCharArray(string text, ref int[] chars)
    {
      if (text == null)
      {
        chars[0] = 0;
      }
      else
      {
        if (chars == null || chars.Length <= text.Length)
        {
          int length = text.Length <= 1024 ? Mathf.NextPowerOfTwo(text.Length + 1) : text.Length + 256;
          chars = new int[length];
        }
        int index1 = 0;
        for (int index2 = 0; index2 < text.Length; ++index2)
        {
          if (this.m_parseCtrlCharacters && (int) text[index2] == 92 && text.Length > index2 + 1)
          {
            int num = (int) text[index2 + 1];
            switch (num)
            {
              case 110:
                chars[index1] = 10;
                ++index2;
                ++index1;
                continue;
              case 114:
                chars[index1] = 13;
                ++index2;
                ++index1;
                continue;
              case 116:
                chars[index1] = 9;
                ++index2;
                ++index1;
                continue;
              case 117:
                if (text.Length > index2 + 5)
                {
                  chars[index1] = (int) (ushort) this.GetUTF16(index2 + 2);
                  index2 += 5;
                  ++index1;
                  continue;
                }
                break;
              default:
                if (num != 85)
                {
                  if (num == 92 && text.Length > index2 + 2)
                  {
                    chars[index1] = (int) text[index2 + 1];
                    chars[index1 + 1] = (int) text[index2 + 2];
                    index2 += 2;
                    index1 += 2;
                    continue;
                  }
                  break;
                }
                if (text.Length > index2 + 9)
                {
                  chars[index1] = this.GetUTF32(index2 + 2);
                  index2 += 9;
                  ++index1;
                  continue;
                }
                break;
            }
          }
          if (char.IsHighSurrogate(text[index2]) && char.IsLowSurrogate(text[index2 + 1]))
          {
            chars[index1] = char.ConvertToUtf32(text[index2], text[index2 + 1]);
            ++index2;
            ++index1;
          }
          else
          {
            chars[index1] = (int) text[index2];
            ++index1;
          }
        }
        chars[index1] = 0;
      }
    }

    protected void StringBuilderToIntArray(StringBuilder text, ref int[] chars)
    {
      if (text == null)
      {
        chars[0] = 0;
      }
      else
      {
        if (chars == null || chars.Length <= text.Length)
        {
          int length = text.Length <= 1024 ? Mathf.NextPowerOfTwo(text.Length + 1) : text.Length + 256;
          chars = new int[length];
        }
        int index1 = 0;
        for (int index2 = 0; index2 < text.Length; ++index2)
        {
          if (this.m_parseCtrlCharacters && (int) text[index2] == 92 && text.Length > index2 + 1)
          {
            int num = (int) text[index2 + 1];
            switch (num)
            {
              case 110:
                chars[index1] = 10;
                ++index2;
                ++index1;
                continue;
              case 114:
                chars[index1] = 13;
                ++index2;
                ++index1;
                continue;
              case 116:
                chars[index1] = 9;
                ++index2;
                ++index1;
                continue;
              case 117:
                if (text.Length > index2 + 5)
                {
                  chars[index1] = (int) (ushort) this.GetUTF16(index2 + 2);
                  index2 += 5;
                  ++index1;
                  continue;
                }
                break;
              default:
                if (num != 85)
                {
                  if (num == 92 && text.Length > index2 + 2)
                  {
                    chars[index1] = (int) text[index2 + 1];
                    chars[index1 + 1] = (int) text[index2 + 2];
                    index2 += 2;
                    index1 += 2;
                    continue;
                  }
                  break;
                }
                if (text.Length > index2 + 9)
                {
                  chars[index1] = this.GetUTF32(index2 + 2);
                  index2 += 9;
                  ++index1;
                  continue;
                }
                break;
            }
          }
          if (char.IsHighSurrogate(text[index2]) && char.IsLowSurrogate(text[index2 + 1]))
          {
            chars[index1] = char.ConvertToUtf32(text[index2], text[index2 + 1]);
            ++index2;
            ++index1;
          }
          else
          {
            chars[index1] = (int) text[index2];
            ++index1;
          }
        }
        chars[index1] = 0;
      }
    }

    protected void AddFloatToCharArray(float number, ref int index, int precision)
    {
      if ((double) number < 0.0)
      {
        char[] chArray = this.m_input_CharArray;
        int num1;
        index = (num1 = index) + 1;
        int index1 = num1;
        int num2 = 45;
        chArray[index1] = (char) num2;
        number = -number;
      }
      number += this.k_Power[Mathf.Min(9, precision)];
      int number1 = (int) number;
      this.AddIntToCharArray(number1, ref index, precision);
      if (precision <= 0)
        return;
      char[] chArray1 = this.m_input_CharArray;
      int num3;
      index = (num3 = index) + 1;
      int index2 = num3;
      int num4 = 46;
      chArray1[index2] = (char) num4;
      number -= (float) number1;
      for (int index1 = 0; index1 < precision; ++index1)
      {
        number *= 10f;
        int num1 = (int) number;
        char[] chArray2 = this.m_input_CharArray;
        int num2;
        index = (num2 = index) + 1;
        int index3 = num2;
        int num5 = (int) (ushort) (num1 + 48);
        chArray2[index3] = (char) num5;
        number -= (float) num1;
      }
    }

    protected void AddIntToCharArray(int number, ref int index, int precision)
    {
      if (number < 0)
      {
        char[] chArray = this.m_input_CharArray;
        int num1;
        index = (num1 = index) + 1;
        int index1 = num1;
        int num2 = 45;
        chArray[index1] = (char) num2;
        number = -number;
      }
      int index2 = index;
      do
      {
        this.m_input_CharArray[index2++] = (char) (number % 10 + 48);
        number /= 10;
      }
      while (number > 0);
      int num = index2;
      while (index + 1 < index2)
      {
        --index2;
        char ch = this.m_input_CharArray[index];
        this.m_input_CharArray[index] = this.m_input_CharArray[index2];
        this.m_input_CharArray[index2] = ch;
        index = index + 1;
      }
      index = num;
    }

    protected virtual int SetArraySizes(int[] chars)
    {
      return 0;
    }

    protected void ParseInputText()
    {
      this.m_isInputParsingRequired = false;
      switch (this.m_inputSource)
      {
        case TMP_Text.TextInputSources.Text:
          this.StringToCharArray(this.m_text, ref this.m_char_buffer);
          break;
        case TMP_Text.TextInputSources.SetText:
          this.SetTextArrayToCharArray(this.m_input_CharArray, ref this.m_char_buffer);
          break;
      }
      this.SetArraySizes(this.m_char_buffer);
    }

    protected virtual void GenerateTextMesh()
    {
    }

    public Vector2 GetPreferredValues()
    {
      if (this.m_isInputParsingRequired || this.m_isTextTruncated)
        this.ParseInputText();
      return new Vector2(this.GetPreferredWidth(), this.GetPreferredHeight());
    }

    public Vector2 GetPreferredValues(float width, float height)
    {
      if (this.m_isInputParsingRequired || this.m_isTextTruncated)
        this.ParseInputText();
      Vector2 margin = new Vector2(width, height);
      return new Vector2(this.GetPreferredWidth(margin), this.GetPreferredHeight(margin));
    }

    public Vector2 GetPreferredValues(string text)
    {
      this.StringToCharArray(text, ref this.m_char_buffer);
      this.SetArraySizes(this.m_char_buffer);
      Vector2 margin = new Vector2(float.PositiveInfinity, float.PositiveInfinity);
      return new Vector2(this.GetPreferredWidth(margin), this.GetPreferredHeight(margin));
    }

    public Vector2 GetPreferredValues(string text, float width, float height)
    {
      this.StringToCharArray(text, ref this.m_char_buffer);
      this.SetArraySizes(this.m_char_buffer);
      Vector2 margin = new Vector2(width, height);
      return new Vector2(this.GetPreferredWidth(margin), this.GetPreferredHeight(margin));
    }

    protected float GetPreferredWidth()
    {
      float defaultFontSize = !this.m_enableAutoSizing ? this.m_fontSize : this.m_fontSizeMax;
      Vector2 marginSize = new Vector2(float.PositiveInfinity, float.PositiveInfinity);
      if (this.m_isInputParsingRequired || this.m_isTextTruncated)
        this.ParseInputText();
      return this.CalculatePreferredValues(defaultFontSize, marginSize).x;
    }

    protected float GetPreferredWidth(Vector2 margin)
    {
      return this.CalculatePreferredValues(!this.m_enableAutoSizing ? this.m_fontSize : this.m_fontSizeMax, margin).x;
    }

    protected float GetPreferredHeight()
    {
      float defaultFontSize = !this.m_enableAutoSizing ? this.m_fontSize : this.m_fontSizeMax;
      Vector2 marginSize = new Vector2((double) this.m_marginWidth == 0.0 ? float.PositiveInfinity : this.m_marginWidth, float.PositiveInfinity);
      if (this.m_isInputParsingRequired || this.m_isTextTruncated)
        this.ParseInputText();
      return this.CalculatePreferredValues(defaultFontSize, marginSize).y;
    }

    protected float GetPreferredHeight(Vector2 margin)
    {
      return this.CalculatePreferredValues(!this.m_enableAutoSizing ? this.m_fontSize : this.m_fontSizeMax, margin).y;
    }

    protected virtual Vector2 CalculatePreferredValues(float defaultFontSize, Vector2 marginSize)
    {
      if ((UnityEngine.Object) this.m_fontAsset == (UnityEngine.Object) null || this.m_fontAsset.characterDictionary == null)
      {
        Debug.LogWarning((object) ("Can't Generate Mesh! No Font Asset has been assigned to Object ID: " + (object) this.GetInstanceID()));
        return Vector2.zero;
      }
      if (this.m_char_buffer == null || this.m_char_buffer.Length == 0 || this.m_char_buffer[0] == 0)
        return Vector2.zero;
      this.m_currentFontAsset = this.m_fontAsset;
      this.m_currentMaterial = this.m_sharedMaterial;
      this.m_currentMaterialIndex = 0;
      this.m_materialReferenceStack.SetDefault(new MaterialReference(0, this.m_currentFontAsset, (TMP_SpriteAsset) null, this.m_currentMaterial, this.m_padding));
      int num1 = this.m_totalCharacterCount;
      if (this.m_internalCharacterInfo == null || num1 > this.m_internalCharacterInfo.Length)
        this.m_internalCharacterInfo = new TMP_CharacterInfo[num1 <= 1024 ? Mathf.NextPowerOfTwo(num1) : num1 + 256];
      this.m_fontScale = (float) ((double) defaultFontSize / (double) this.m_currentFontAsset.fontInfo.PointSize * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
      this.m_fontScaleMultiplier = 1f;
      float num2 = (float) ((double) defaultFontSize / (double) this.m_fontAsset.fontInfo.PointSize * (double) this.m_fontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
      float num3 = this.m_fontScale;
      this.m_currentFontSize = defaultFontSize;
      this.m_sizeStack.SetDefault(this.m_currentFontSize);
      this.m_style = this.m_fontStyle;
      this.m_baselineOffset = 0.0f;
      this.m_styleStack.Clear();
      this.m_lineOffset = 0.0f;
      this.m_lineHeight = 0.0f;
      float num4 = this.m_currentFontAsset.fontInfo.LineHeight - (this.m_currentFontAsset.fontInfo.Ascender - this.m_currentFontAsset.fontInfo.Descender);
      this.m_cSpacing = 0.0f;
      this.m_monoSpacing = 0.0f;
      this.m_xAdvance = 0.0f;
      float a1 = 0.0f;
      this.tag_LineIndent = 0.0f;
      this.tag_Indent = 0.0f;
      this.m_indentStack.SetDefault(0.0f);
      this.tag_NoParsing = false;
      this.m_characterCount = 0;
      this.m_firstCharacterOfLine = 0;
      this.m_maxLineAscender = float.NegativeInfinity;
      this.m_maxLineDescender = float.PositiveInfinity;
      this.m_lineNumber = 0;
      float num5 = marginSize.x;
      this.m_marginLeft = 0.0f;
      this.m_marginRight = 0.0f;
      this.m_width = -1f;
      float num6 = 0.0f;
      float a2 = 0.0f;
      this.m_maxAscender = 0.0f;
      this.m_maxDescender = 0.0f;
      bool flag1 = true;
      bool flag2 = false;
      WordWrapState state1 = new WordWrapState();
      this.SaveWordWrappingState(ref state1, 0, 0);
      WordWrapState state2 = new WordWrapState();
      int num7 = 0;
      int endIndex = 0;
      for (int index = 0; this.m_char_buffer[index] != 0; ++index)
      {
        int num8 = this.m_char_buffer[index];
        this.m_textElementType = TMP_TextElementType.Character;
        this.m_currentMaterialIndex = this.m_textInfo.characterInfo[this.m_characterCount].materialReferenceIndex;
        this.m_currentFontAsset = this.m_materialReferences[this.m_currentMaterialIndex].fontAsset;
        int num9 = this.m_currentMaterialIndex;
        if (this.m_isRichText && num8 == 60)
        {
          this.m_isParsingText = true;
          if (this.ValidateHtmlTag(this.m_char_buffer, index + 1, out endIndex))
          {
            index = endIndex;
            if (this.m_textElementType == TMP_TextElementType.Character)
              continue;
          }
        }
        this.m_isParsingText = false;
        float num10 = 1f;
        if (this.m_textElementType == TMP_TextElementType.Character)
        {
          if ((this.m_style & FontStyles.UpperCase) == FontStyles.UpperCase)
          {
            if (char.IsLower((char) num8))
              num8 = (int) char.ToUpper((char) num8);
          }
          else if ((this.m_style & FontStyles.LowerCase) == FontStyles.LowerCase)
          {
            if (char.IsUpper((char) num8))
              num8 = (int) char.ToLower((char) num8);
          }
          else if (((this.m_fontStyle & FontStyles.SmallCaps) == FontStyles.SmallCaps || (this.m_style & FontStyles.SmallCaps) == FontStyles.SmallCaps) && char.IsLower((char) num8))
          {
            num10 = 0.8f;
            num8 = (int) char.ToUpper((char) num8);
          }
        }
        if (this.m_textElementType == TMP_TextElementType.Sprite)
        {
          TMP_Sprite tmpSprite = this.m_currentSpriteAsset.spriteInfoList[this.m_spriteIndex];
          if (tmpSprite != null)
          {
            num8 = 57344 + this.m_spriteIndex;
            this.m_cached_TextElement = (TMP_TextElement) tmpSprite;
            num3 = this.m_fontAsset.fontInfo.Ascender / tmpSprite.height * tmpSprite.scale * num2;
            this.m_internalCharacterInfo[this.m_characterCount].elementType = TMP_TextElementType.Sprite;
            this.m_currentMaterialIndex = num9;
          }
          else
            continue;
        }
        else if (this.m_textElementType == TMP_TextElementType.Character)
        {
          this.m_cached_TextElement = this.m_textInfo.characterInfo[this.m_characterCount].textElement;
          this.m_currentFontAsset = this.m_textInfo.characterInfo[this.m_characterCount].fontAsset;
          this.m_currentMaterialIndex = this.m_textInfo.characterInfo[this.m_characterCount].materialReferenceIndex;
          this.m_fontScale = (float) ((double) this.m_currentFontSize * (double) num10 / (double) this.m_currentFontAsset.fontInfo.PointSize * (double) this.m_currentFontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
          num3 = this.m_fontScale * this.m_fontScaleMultiplier;
          this.m_internalCharacterInfo[this.m_characterCount].elementType = TMP_TextElementType.Character;
        }
        this.m_internalCharacterInfo[this.m_characterCount].character = (char) num8;
        if (this.m_enableKerning && this.m_characterCount >= 1)
        {
          KerningPair kerningPair;
          this.m_currentFontAsset.kerningDictionary.TryGetValue(new KerningPairKey((int) this.m_internalCharacterInfo[this.m_characterCount - 1].character, num8).key, out kerningPair);
          if (kerningPair != null)
            this.m_xAdvance += kerningPair.XadvanceOffset * num3;
        }
        float num11 = 0.0f;
        if ((double) this.m_monoSpacing != 0.0)
        {
          num11 = (float) ((double) this.m_monoSpacing / 2.0 - ((double) this.m_cached_TextElement.width / 2.0 + (double) this.m_cached_TextElement.xOffset) * (double) num3);
          this.m_xAdvance += num11;
        }
        float num12 = (this.m_style & FontStyles.Bold) == FontStyles.Bold || (this.m_fontStyle & FontStyles.Bold) == FontStyles.Bold ? (float) (1.0 + (double) this.m_currentFontAsset.boldSpacing * 0.00999999977648258) : 1f;
        this.m_internalCharacterInfo[this.m_characterCount].baseLine = 0.0f - this.m_lineOffset + this.m_baselineOffset;
        float num13 = this.m_currentFontAsset.fontInfo.Ascender * (this.m_textElementType != TMP_TextElementType.Character ? num2 : num3) + this.m_baselineOffset;
        this.m_internalCharacterInfo[this.m_characterCount].ascender = num13 - this.m_lineOffset;
        this.m_maxLineAscender = (double) num13 <= (double) this.m_maxLineAscender ? this.m_maxLineAscender : num13;
        float num14 = this.m_currentFontAsset.fontInfo.Descender * (this.m_textElementType != TMP_TextElementType.Character ? num2 : num3) + this.m_baselineOffset;
        float num15 = this.m_internalCharacterInfo[this.m_characterCount].descender = num14 - this.m_lineOffset;
        this.m_maxLineDescender = (double) num14 >= (double) this.m_maxLineDescender ? this.m_maxLineDescender : num14;
        if ((this.m_style & FontStyles.Subscript) == FontStyles.Subscript || (this.m_style & FontStyles.Superscript) == FontStyles.Superscript)
        {
          float num16 = (num13 - this.m_baselineOffset) / this.m_currentFontAsset.fontInfo.SubSize;
          num13 = this.m_maxLineAscender;
          this.m_maxLineAscender = (double) num16 <= (double) this.m_maxLineAscender ? this.m_maxLineAscender : num16;
          float num17 = (num14 - this.m_baselineOffset) / this.m_currentFontAsset.fontInfo.SubSize;
          float num18 = this.m_maxLineDescender;
          this.m_maxLineDescender = (double) num17 >= (double) this.m_maxLineDescender ? this.m_maxLineDescender : num17;
        }
        if (this.m_lineNumber == 0)
          this.m_maxAscender = (double) this.m_maxAscender <= (double) num13 ? num13 : this.m_maxAscender;
        if (num8 == 9 || !char.IsWhiteSpace((char) num8) || this.m_textElementType == TMP_TextElementType.Sprite)
        {
          float num16 = (double) this.m_width == -1.0 ? num5 + 0.0001f - this.m_marginLeft - this.m_marginRight : Mathf.Min(num5 + 0.0001f - this.m_marginLeft - this.m_marginRight, this.m_width);
          if ((double) this.m_xAdvance + (double) this.m_cached_TextElement.xAdvance * (double) num3 > (double) num16 && this.enableWordWrapping && this.m_characterCount != this.m_firstCharacterOfLine)
          {
            if (num7 == state2.previous_WordBreak || flag1)
            {
              if (!this.m_isCharacterWrappingEnabled)
                this.m_isCharacterWrappingEnabled = true;
              else
                flag2 = true;
            }
            index = this.RestoreWordWrappingState(ref state2);
            num7 = index;
            if (this.m_lineNumber > 0 && !TMP_Math.Approximately(this.m_maxLineAscender, this.m_startOfLineAscender) && (double) this.m_lineHeight == 0.0)
            {
              float offset = this.m_maxLineAscender - this.m_startOfLineAscender;
              this.AdjustLineOffset(this.m_firstCharacterOfLine, this.m_characterCount, offset);
              this.m_lineOffset += offset;
              state2.lineOffset = this.m_lineOffset;
              state2.previousLineAscender = this.m_maxLineAscender;
            }
            float num17 = this.m_maxLineAscender - this.m_lineOffset;
            float num18 = this.m_maxLineDescender - this.m_lineOffset;
            this.m_maxDescender = (double) this.m_maxDescender >= (double) num18 ? num18 : this.m_maxDescender;
            this.m_firstCharacterOfLine = this.m_characterCount;
            num6 += this.m_xAdvance;
            a2 = !this.m_enableWordWrapping ? Mathf.Max(a2, num17 - num18) : this.m_maxAscender - this.m_maxDescender;
            this.SaveWordWrappingState(ref state1, index, this.m_characterCount - 1);
            ++this.m_lineNumber;
            if ((double) this.m_lineHeight == 0.0)
            {
              float num19 = this.m_internalCharacterInfo[this.m_characterCount].ascender - this.m_internalCharacterInfo[this.m_characterCount].baseLine;
              this.m_lineOffset += (float) (0.0 - (double) this.m_maxLineDescender + (double) num19 + ((double) num4 + (double) this.m_lineSpacing + (double) this.m_lineSpacingDelta) * (double) num2);
              this.m_startOfLineAscender = num19;
            }
            else
              this.m_lineOffset += this.m_lineHeight + this.m_lineSpacing * num2;
            this.m_maxLineAscender = float.NegativeInfinity;
            this.m_maxLineDescender = float.PositiveInfinity;
            this.m_xAdvance = 0.0f + this.tag_Indent;
            continue;
          }
        }
        if (this.m_lineNumber > 0 && !TMP_Math.Approximately(this.m_maxLineAscender, this.m_startOfLineAscender) && ((double) this.m_lineHeight == 0.0 && !this.m_isNewPage))
        {
          float offset = this.m_maxLineAscender - this.m_startOfLineAscender;
          this.AdjustLineOffset(this.m_firstCharacterOfLine, this.m_characterCount, offset);
          num15 -= offset;
          this.m_lineOffset += offset;
          this.m_startOfLineAscender += offset;
          state2.lineOffset = this.m_lineOffset;
          state2.previousLineAscender = this.m_startOfLineAscender;
        }
        if (num8 == 9)
          this.m_xAdvance += this.m_currentFontAsset.fontInfo.TabWidth * num3;
        else if ((double) this.m_monoSpacing != 0.0)
          this.m_xAdvance += (float) ((double) this.m_monoSpacing - (double) num11 + ((double) this.m_characterSpacing + (double) this.m_currentFontAsset.normalSpacingOffset) * (double) num3) + this.m_cSpacing;
        else
          this.m_xAdvance += (this.m_cached_TextElement.xAdvance * num12 + this.m_characterSpacing + this.m_currentFontAsset.normalSpacingOffset) * num3 + this.m_cSpacing;
        if (num8 == 13)
        {
          a1 = Mathf.Max(a1, num6 + this.m_xAdvance);
          num6 = 0.0f;
          this.m_xAdvance = 0.0f + this.tag_Indent;
        }
        if (num8 == 10 || this.m_characterCount == num1 - 1)
        {
          if (this.m_lineNumber > 0 && !TMP_Math.Approximately(this.m_maxLineAscender, this.m_startOfLineAscender) && (double) this.m_lineHeight == 0.0)
          {
            float offset = this.m_maxLineAscender - this.m_startOfLineAscender;
            this.AdjustLineOffset(this.m_firstCharacterOfLine, this.m_characterCount, offset);
            float num16 = num15 - offset;
            this.m_lineOffset += offset;
          }
          float num17 = this.m_maxLineDescender - this.m_lineOffset;
          this.m_maxDescender = (double) this.m_maxDescender >= (double) num17 ? num17 : this.m_maxDescender;
          this.m_firstCharacterOfLine = this.m_characterCount + 1;
          if (num8 == 10 && this.m_characterCount != num1 - 1)
          {
            a1 = Mathf.Max(a1, num6 + this.m_xAdvance);
            num6 = 0.0f;
          }
          else
            num6 = Mathf.Max(a1, num6 + this.m_xAdvance);
          a2 = this.m_maxAscender - this.m_maxDescender;
          if (num8 == 10)
          {
            this.SaveWordWrappingState(ref state1, index, this.m_characterCount);
            this.SaveWordWrappingState(ref state2, index, this.m_characterCount);
            ++this.m_lineNumber;
            if ((double) this.m_lineHeight == 0.0)
              this.m_lineOffset += (float) (0.0 - (double) this.m_maxLineDescender + (double) num13 + ((double) num4 + (double) this.m_lineSpacing + (double) this.m_paragraphSpacing + (double) this.m_lineSpacingDelta) * (double) num2);
            else
              this.m_lineOffset += this.m_lineHeight + (this.m_lineSpacing + this.m_paragraphSpacing) * num2;
            this.m_maxLineAscender = float.NegativeInfinity;
            this.m_maxLineDescender = float.PositiveInfinity;
            this.m_startOfLineAscender = num13;
            this.m_xAdvance = 0.0f + this.tag_LineIndent + this.tag_Indent;
          }
        }
        if (this.m_enableWordWrapping || this.m_overflowMode == TextOverflowModes.Truncate || this.m_overflowMode == TextOverflowModes.Ellipsis)
        {
          if ((num8 == 9 || num8 == 32) && !this.m_isNonBreakingSpace)
          {
            this.SaveWordWrappingState(ref state2, index, this.m_characterCount);
            this.m_isCharacterWrappingEnabled = false;
            flag1 = false;
          }
          else if (num8 > 11904 && num8 < 40959)
          {
            if (!this.m_currentFontAsset.lineBreakingInfo.leadingCharacters.ContainsKey(num8) && this.m_characterCount < num1 - 1 && !this.m_currentFontAsset.lineBreakingInfo.followingCharacters.ContainsKey((int) this.m_internalCharacterInfo[this.m_characterCount + 1].character))
            {
              this.SaveWordWrappingState(ref state2, index, this.m_characterCount);
              this.m_isCharacterWrappingEnabled = false;
              flag1 = false;
            }
          }
          else if (flag1 || this.m_isCharacterWrappingEnabled || flag2)
            this.SaveWordWrappingState(ref state2, index, this.m_characterCount);
        }
        ++this.m_characterCount;
      }
      this.m_isCharacterWrappingEnabled = false;
      return new Vector2((float) ((double) num6 + ((double) this.m_margin.x <= 0.0 ? 0.0 : (double) this.m_margin.x) + ((double) this.m_margin.z <= 0.0 ? 0.0 : (double) this.m_margin.z)), (float) ((double) a2 + ((double) this.m_margin.y <= 0.0 ? 0.0 : (double) this.m_margin.y) + ((double) this.m_margin.w <= 0.0 ? 0.0 : (double) this.m_margin.w)));
    }

    protected virtual void AdjustLineOffset(int startIndex, int endIndex, float offset)
    {
    }

    protected void ResizeLineExtents(int size)
    {
      size = size <= 1024 ? Mathf.NextPowerOfTwo(size + 1) : size + 256;
      TMP_LineInfo[] tmpLineInfoArray = new TMP_LineInfo[size];
      for (int index = 0; index < size; ++index)
      {
        if (index < this.m_textInfo.lineInfo.Length)
        {
          tmpLineInfoArray[index] = this.m_textInfo.lineInfo[index];
        }
        else
        {
          tmpLineInfoArray[index].lineExtents.min = TMP_Text.k_InfinityVectorPositive;
          tmpLineInfoArray[index].lineExtents.max = TMP_Text.k_InfinityVectorNegative;
          tmpLineInfoArray[index].ascender = TMP_Text.k_InfinityVectorNegative.x;
          tmpLineInfoArray[index].descender = TMP_Text.k_InfinityVectorPositive.x;
        }
      }
      this.m_textInfo.lineInfo = tmpLineInfoArray;
    }

    public TMP_TextInfo GetTextInfo(string text)
    {
      this.StringToCharArray(text, ref this.m_char_buffer);
      this.SetArraySizes(this.m_char_buffer);
      this.m_renderMode = TextRenderFlags.DontRender;
      this.ComputeMarginSize();
      this.GenerateTextMesh();
      this.m_renderMode = TextRenderFlags.Render;
      return this.textInfo;
    }

    protected virtual void ComputeMarginSize()
    {
    }

    protected int GetArraySizes(int[] chars)
    {
      int endIndex = 0;
      this.m_totalCharacterCount = 0;
      this.m_isUsingBold = false;
      this.m_isParsingText = false;
      for (int index = 0; chars[index] != 0; ++index)
      {
        int num = chars[index];
        if (this.m_isRichText && num == 60 && this.ValidateHtmlTag(chars, index + 1, out endIndex))
        {
          index = endIndex;
          if ((this.m_style & FontStyles.Bold) == FontStyles.Bold)
            this.m_isUsingBold = true;
        }
        else
        {
          if (char.IsWhiteSpace((char) num))
            ;
          ++this.m_totalCharacterCount;
        }
      }
      return this.m_totalCharacterCount;
    }

    protected void SaveWordWrappingState(ref WordWrapState state, int index, int count)
    {
      state.currentFontAsset = this.m_currentFontAsset;
      state.currentSpriteAsset = this.m_currentSpriteAsset;
      state.currentMaterial = this.m_currentMaterial;
      state.currentMaterialIndex = this.m_currentMaterialIndex;
      state.previous_WordBreak = index;
      state.total_CharacterCount = count;
      state.visible_CharacterCount = this.m_visibleCharacterCount;
      state.visible_SpriteCount = this.m_visibleSpriteCount;
      state.visible_LinkCount = this.m_textInfo.linkCount;
      state.firstCharacterIndex = this.m_firstCharacterOfLine;
      state.firstVisibleCharacterIndex = this.m_firstVisibleCharacterOfLine;
      state.lastVisibleCharIndex = this.m_lastVisibleCharacterOfLine;
      state.fontStyle = this.m_style;
      state.fontScale = this.m_fontScale;
      state.fontScaleMultiplier = this.m_fontScaleMultiplier;
      state.currentFontSize = this.m_currentFontSize;
      state.xAdvance = this.m_xAdvance;
      state.maxAscender = this.m_maxAscender;
      state.maxDescender = this.m_maxDescender;
      state.maxLineAscender = this.m_maxLineAscender;
      state.maxLineDescender = this.m_maxLineDescender;
      state.previousLineAscender = this.m_startOfLineAscender;
      state.preferredWidth = this.m_preferredWidth;
      state.preferredHeight = this.m_preferredHeight;
      state.meshExtents = this.m_meshExtents;
      state.lineNumber = this.m_lineNumber;
      state.lineOffset = this.m_lineOffset;
      state.baselineOffset = this.m_baselineOffset;
      state.vertexColor = this.m_htmlColor;
      state.tagNoParsing = this.tag_NoParsing;
      state.colorStack = this.m_colorStack;
      state.sizeStack = this.m_sizeStack;
      state.fontWeightStack = this.m_fontWeightStack;
      state.styleStack = this.m_styleStack;
      state.materialReferenceStack = this.m_materialReferenceStack;
      if (this.m_lineNumber >= this.m_textInfo.lineInfo.Length)
        return;
      state.lineInfo = this.m_textInfo.lineInfo[this.m_lineNumber];
    }

    protected int RestoreWordWrappingState(ref WordWrapState state)
    {
      int num = state.previous_WordBreak;
      this.m_currentFontAsset = state.currentFontAsset;
      this.m_currentSpriteAsset = state.currentSpriteAsset;
      this.m_currentMaterial = state.currentMaterial;
      this.m_currentMaterialIndex = state.currentMaterialIndex;
      this.m_characterCount = state.total_CharacterCount + 1;
      this.m_visibleCharacterCount = state.visible_CharacterCount;
      this.m_visibleSpriteCount = state.visible_SpriteCount;
      this.m_textInfo.linkCount = state.visible_LinkCount;
      this.m_firstCharacterOfLine = state.firstCharacterIndex;
      this.m_firstVisibleCharacterOfLine = state.firstVisibleCharacterIndex;
      this.m_lastVisibleCharacterOfLine = state.lastVisibleCharIndex;
      this.m_style = state.fontStyle;
      this.m_fontScale = state.fontScale;
      this.m_fontScaleMultiplier = state.fontScaleMultiplier;
      this.m_currentFontSize = state.currentFontSize;
      this.m_xAdvance = state.xAdvance;
      this.m_maxAscender = state.maxAscender;
      this.m_maxDescender = state.maxDescender;
      this.m_maxLineAscender = state.maxLineAscender;
      this.m_maxLineDescender = state.maxLineDescender;
      this.m_startOfLineAscender = state.previousLineAscender;
      this.m_preferredWidth = state.preferredWidth;
      this.m_preferredHeight = state.preferredHeight;
      this.m_meshExtents = state.meshExtents;
      this.m_lineNumber = state.lineNumber;
      this.m_lineOffset = state.lineOffset;
      this.m_baselineOffset = state.baselineOffset;
      this.m_htmlColor = state.vertexColor;
      this.tag_NoParsing = state.tagNoParsing;
      this.m_colorStack = state.colorStack;
      this.m_sizeStack = state.sizeStack;
      this.m_fontWeightStack = state.fontWeightStack;
      this.m_styleStack = state.styleStack;
      this.m_materialReferenceStack = state.materialReferenceStack;
      if (this.m_lineNumber < this.m_textInfo.lineInfo.Length)
        this.m_textInfo.lineInfo[this.m_lineNumber] = state.lineInfo;
      return num;
    }

    protected virtual void SaveGlyphVertexInfo(float padding, float style_padding, Color32 vertexColor)
    {
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.position = this.m_textInfo.characterInfo[this.m_characterCount].bottomLeft;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.position = this.m_textInfo.characterInfo[this.m_characterCount].topLeft;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.position = this.m_textInfo.characterInfo[this.m_characterCount].topRight;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.position = this.m_textInfo.characterInfo[this.m_characterCount].bottomRight;
      vertexColor.a = (int) this.m_fontColor32.a >= (int) vertexColor.a ? vertexColor.a : this.m_fontColor32.a;
      if (!this.m_enableVertexGradient)
      {
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = vertexColor;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = vertexColor;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = vertexColor;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = vertexColor;
      }
      else if (!this.m_overrideHtmlColors && !this.m_htmlColor.CompareRGB(this.m_fontColor32))
      {
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = vertexColor;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = vertexColor;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = vertexColor;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = vertexColor;
      }
      else
      {
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = (Color32) (this.m_fontColorGradient.bottomLeft * (Color) vertexColor);
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = (Color32) (this.m_fontColorGradient.topLeft * (Color) vertexColor);
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = (Color32) (this.m_fontColorGradient.topRight * (Color) vertexColor);
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = (Color32) (this.m_fontColorGradient.bottomRight * (Color) vertexColor);
      }
      if (!this.m_isSDFShader)
        style_padding = 0.0f;
      FaceInfo fontInfo = this.m_currentFontAsset.fontInfo;
      Vector2 vector2_1;
      vector2_1.x = (this.m_cached_TextElement.x - padding - style_padding) / fontInfo.AtlasWidth;
      vector2_1.y = (float) (1.0 - ((double) this.m_cached_TextElement.y + (double) padding + (double) style_padding + (double) this.m_cached_TextElement.height) / (double) fontInfo.AtlasHeight);
      Vector2 vector2_2;
      vector2_2.x = vector2_1.x;
      vector2_2.y = (float) (1.0 - ((double) this.m_cached_TextElement.y - (double) padding - (double) style_padding) / (double) fontInfo.AtlasHeight);
      Vector2 vector2_3;
      vector2_3.x = (this.m_cached_TextElement.x + padding + style_padding + this.m_cached_TextElement.width) / fontInfo.AtlasWidth;
      vector2_3.y = vector2_2.y;
      Vector2 vector2_4;
      vector2_4.x = vector2_3.x;
      vector2_4.y = vector2_1.y;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.uv = vector2_1;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.uv = vector2_2;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.uv = vector2_3;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.uv = vector2_4;
    }

    protected virtual void SaveSpriteVertexInfo(Color32 vertexColor)
    {
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.position = this.m_textInfo.characterInfo[this.m_characterCount].bottomLeft;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.position = this.m_textInfo.characterInfo[this.m_characterCount].topLeft;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.position = this.m_textInfo.characterInfo[this.m_characterCount].topRight;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.position = this.m_textInfo.characterInfo[this.m_characterCount].bottomRight;
      if (this.m_tintAllSprites)
        this.m_tintSprite = true;
      Color32 color32 = !this.m_tintSprite ? this.m_spriteColor : this.m_spriteColor.Multiply(vertexColor);
      color32.a = (int) color32.a >= (int) this.m_fontColor32.a ? this.m_fontColor32.a : (color32.a = (int) color32.a >= (int) vertexColor.a ? vertexColor.a : color32.a);
      if (!this.m_enableVertexGradient)
      {
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = color32;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = color32;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = color32;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = color32;
      }
      else if (!this.m_overrideHtmlColors && !this.m_htmlColor.CompareRGB(this.m_fontColor32))
      {
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = color32;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = color32;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = color32;
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = color32;
      }
      else
      {
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = (Color32) (this.m_fontColorGradient.bottomLeft * (Color) color32);
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = (Color32) (this.m_fontColorGradient.topLeft * (Color) color32);
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = (Color32) (this.m_fontColorGradient.topRight * (Color) color32);
        this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = (Color32) (this.m_fontColorGradient.bottomRight * (Color) color32);
      }
      Vector2 vector2_1 = new Vector2(this.m_cached_TextElement.x / (float) this.m_currentSpriteAsset.spriteSheet.width, this.m_cached_TextElement.y / (float) this.m_currentSpriteAsset.spriteSheet.height);
      Vector2 vector2_2 = new Vector2(vector2_1.x, (this.m_cached_TextElement.y + this.m_cached_TextElement.height) / (float) this.m_currentSpriteAsset.spriteSheet.height);
      Vector2 vector2_3 = new Vector2((this.m_cached_TextElement.x + this.m_cached_TextElement.width) / (float) this.m_currentSpriteAsset.spriteSheet.width, vector2_2.y);
      Vector2 vector2_4 = new Vector2(vector2_3.x, vector2_1.y);
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.uv = vector2_1;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.uv = vector2_2;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.uv = vector2_3;
      this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.uv = vector2_4;
    }

    protected virtual void FillCharacterVertexBuffers(int i, int index_X4)
    {
      int index = this.m_textInfo.characterInfo[i].materialReferenceIndex;
      index_X4 = this.m_textInfo.meshInfo[index].vertexCount;
      TMP_CharacterInfo[] tmpCharacterInfoArray = this.m_textInfo.characterInfo;
      this.m_textInfo.characterInfo[i].vertexIndex = (short) index_X4;
      this.m_textInfo.meshInfo[index].vertices[0 + index_X4] = tmpCharacterInfoArray[i].vertex_BL.position;
      this.m_textInfo.meshInfo[index].vertices[1 + index_X4] = tmpCharacterInfoArray[i].vertex_TL.position;
      this.m_textInfo.meshInfo[index].vertices[2 + index_X4] = tmpCharacterInfoArray[i].vertex_TR.position;
      this.m_textInfo.meshInfo[index].vertices[3 + index_X4] = tmpCharacterInfoArray[i].vertex_BR.position;
      this.m_textInfo.meshInfo[index].uvs0[0 + index_X4] = tmpCharacterInfoArray[i].vertex_BL.uv;
      this.m_textInfo.meshInfo[index].uvs0[1 + index_X4] = tmpCharacterInfoArray[i].vertex_TL.uv;
      this.m_textInfo.meshInfo[index].uvs0[2 + index_X4] = tmpCharacterInfoArray[i].vertex_TR.uv;
      this.m_textInfo.meshInfo[index].uvs0[3 + index_X4] = tmpCharacterInfoArray[i].vertex_BR.uv;
      this.m_textInfo.meshInfo[index].uvs2[0 + index_X4] = tmpCharacterInfoArray[i].vertex_BL.uv2;
      this.m_textInfo.meshInfo[index].uvs2[1 + index_X4] = tmpCharacterInfoArray[i].vertex_TL.uv2;
      this.m_textInfo.meshInfo[index].uvs2[2 + index_X4] = tmpCharacterInfoArray[i].vertex_TR.uv2;
      this.m_textInfo.meshInfo[index].uvs2[3 + index_X4] = tmpCharacterInfoArray[i].vertex_BR.uv2;
      this.m_textInfo.meshInfo[index].colors32[0 + index_X4] = tmpCharacterInfoArray[i].vertex_BL.color;
      this.m_textInfo.meshInfo[index].colors32[1 + index_X4] = tmpCharacterInfoArray[i].vertex_TL.color;
      this.m_textInfo.meshInfo[index].colors32[2 + index_X4] = tmpCharacterInfoArray[i].vertex_TR.color;
      this.m_textInfo.meshInfo[index].colors32[3 + index_X4] = tmpCharacterInfoArray[i].vertex_BR.color;
      this.m_textInfo.meshInfo[index].vertexCount = index_X4 + 4;
    }

    protected virtual void FillSpriteVertexBuffers(int i, int index_X4)
    {
      int index = this.m_textInfo.characterInfo[i].materialReferenceIndex;
      index_X4 = this.m_textInfo.meshInfo[index].vertexCount;
      TMP_CharacterInfo[] tmpCharacterInfoArray = this.m_textInfo.characterInfo;
      this.m_textInfo.characterInfo[i].vertexIndex = (short) index_X4;
      this.m_textInfo.meshInfo[index].vertices[0 + index_X4] = tmpCharacterInfoArray[i].vertex_BL.position;
      this.m_textInfo.meshInfo[index].vertices[1 + index_X4] = tmpCharacterInfoArray[i].vertex_TL.position;
      this.m_textInfo.meshInfo[index].vertices[2 + index_X4] = tmpCharacterInfoArray[i].vertex_TR.position;
      this.m_textInfo.meshInfo[index].vertices[3 + index_X4] = tmpCharacterInfoArray[i].vertex_BR.position;
      this.m_textInfo.meshInfo[index].uvs0[0 + index_X4] = tmpCharacterInfoArray[i].vertex_BL.uv;
      this.m_textInfo.meshInfo[index].uvs0[1 + index_X4] = tmpCharacterInfoArray[i].vertex_TL.uv;
      this.m_textInfo.meshInfo[index].uvs0[2 + index_X4] = tmpCharacterInfoArray[i].vertex_TR.uv;
      this.m_textInfo.meshInfo[index].uvs0[3 + index_X4] = tmpCharacterInfoArray[i].vertex_BR.uv;
      this.m_textInfo.meshInfo[index].uvs2[0 + index_X4] = tmpCharacterInfoArray[i].vertex_BL.uv2;
      this.m_textInfo.meshInfo[index].uvs2[1 + index_X4] = tmpCharacterInfoArray[i].vertex_TL.uv2;
      this.m_textInfo.meshInfo[index].uvs2[2 + index_X4] = tmpCharacterInfoArray[i].vertex_TR.uv2;
      this.m_textInfo.meshInfo[index].uvs2[3 + index_X4] = tmpCharacterInfoArray[i].vertex_BR.uv2;
      this.m_textInfo.meshInfo[index].colors32[0 + index_X4] = tmpCharacterInfoArray[i].vertex_BL.color;
      this.m_textInfo.meshInfo[index].colors32[1 + index_X4] = tmpCharacterInfoArray[i].vertex_TL.color;
      this.m_textInfo.meshInfo[index].colors32[2 + index_X4] = tmpCharacterInfoArray[i].vertex_TR.color;
      this.m_textInfo.meshInfo[index].colors32[3 + index_X4] = tmpCharacterInfoArray[i].vertex_BR.color;
      this.m_textInfo.meshInfo[index].vertexCount = index_X4 + 4;
    }

    protected virtual void DrawUnderlineMesh(Vector3 start, Vector3 end, ref int index, float startScale, float endScale, float maxScale, Color32 underlineColor)
    {
      if (this.m_cached_Underline_GlyphInfo == null)
      {
        if (this.m_warningsDisabled)
          return;
        Debug.LogWarning((object) "Unable to add underline since the Font Asset doesn't contain the underline character.", (UnityEngine.Object) this);
      }
      else
      {
        int num1 = index + 12;
        if (num1 > this.m_textInfo.meshInfo[0].vertices.Length)
          this.m_textInfo.meshInfo[0].ResizeMeshInfo(num1 / 4);
        start.y = Mathf.Min(start.y, end.y);
        end.y = Mathf.Min(start.y, end.y);
        float x1 = this.m_cached_Underline_GlyphInfo.width / 2f * maxScale;
        if ((double) end.x - (double) start.x < (double) this.m_cached_Underline_GlyphInfo.width * (double) maxScale)
          x1 = (float) (((double) end.x - (double) start.x) / 2.0);
        float num2 = this.m_padding * startScale / maxScale;
        float num3 = this.m_padding * endScale / maxScale;
        float num4 = this.m_cached_Underline_GlyphInfo.height;
        Vector3[] vector3Array = this.m_textInfo.meshInfo[0].vertices;
        vector3Array[index] = start + new Vector3(0.0f, (float) (0.0 - ((double) num4 + (double) this.m_padding) * (double) maxScale), 0.0f);
        vector3Array[index + 1] = start + new Vector3(0.0f, this.m_padding * maxScale, 0.0f);
        vector3Array[index + 2] = vector3Array[index + 1] + new Vector3(x1, 0.0f, 0.0f);
        vector3Array[index + 3] = vector3Array[index] + new Vector3(x1, 0.0f, 0.0f);
        vector3Array[index + 4] = vector3Array[index + 3];
        vector3Array[index + 5] = vector3Array[index + 2];
        vector3Array[index + 6] = end + new Vector3(-x1, this.m_padding * maxScale, 0.0f);
        vector3Array[index + 7] = end + new Vector3(-x1, (float) -((double) num4 + (double) this.m_padding) * maxScale, 0.0f);
        vector3Array[index + 8] = vector3Array[index + 7];
        vector3Array[index + 9] = vector3Array[index + 6];
        vector3Array[index + 10] = end + new Vector3(0.0f, this.m_padding * maxScale, 0.0f);
        vector3Array[index + 11] = end + new Vector3(0.0f, (float) -((double) num4 + (double) this.m_padding) * maxScale, 0.0f);
        Vector2[] vector2Array1 = this.m_textInfo.meshInfo[0].uvs0;
        Vector2 vector2_1 = new Vector2((this.m_cached_Underline_GlyphInfo.x - num2) / this.m_fontAsset.fontInfo.AtlasWidth, (float) (1.0 - ((double) this.m_cached_Underline_GlyphInfo.y + (double) this.m_padding + (double) this.m_cached_Underline_GlyphInfo.height) / (double) this.m_fontAsset.fontInfo.AtlasHeight));
        Vector2 vector2_2 = new Vector2(vector2_1.x, (float) (1.0 - ((double) this.m_cached_Underline_GlyphInfo.y - (double) this.m_padding) / (double) this.m_fontAsset.fontInfo.AtlasHeight));
        Vector2 vector2_3 = new Vector2((float) ((double) this.m_cached_Underline_GlyphInfo.x - (double) num2 + (double) this.m_cached_Underline_GlyphInfo.width / 2.0) / this.m_fontAsset.fontInfo.AtlasWidth, vector2_2.y);
        Vector2 vector2_4 = new Vector2(vector2_3.x, vector2_1.y);
        Vector2 vector2_5 = new Vector2((float) ((double) this.m_cached_Underline_GlyphInfo.x + (double) num3 + (double) this.m_cached_Underline_GlyphInfo.width / 2.0) / this.m_fontAsset.fontInfo.AtlasWidth, vector2_2.y);
        Vector2 vector2_6 = new Vector2(vector2_5.x, vector2_1.y);
        Vector2 vector2_7 = new Vector2((this.m_cached_Underline_GlyphInfo.x + num3 + this.m_cached_Underline_GlyphInfo.width) / this.m_fontAsset.fontInfo.AtlasWidth, vector2_2.y);
        Vector2 vector2_8 = new Vector2(vector2_7.x, vector2_1.y);
        vector2Array1[0 + index] = vector2_1;
        vector2Array1[1 + index] = vector2_2;
        vector2Array1[2 + index] = vector2_3;
        vector2Array1[3 + index] = vector2_4;
        vector2Array1[4 + index] = new Vector2(vector2_3.x - vector2_3.x * (1f / 1000f), vector2_1.y);
        vector2Array1[5 + index] = new Vector2(vector2_3.x - vector2_3.x * (1f / 1000f), vector2_2.y);
        vector2Array1[6 + index] = new Vector2(vector2_3.x + vector2_3.x * (1f / 1000f), vector2_2.y);
        vector2Array1[7 + index] = new Vector2(vector2_3.x + vector2_3.x * (1f / 1000f), vector2_1.y);
        vector2Array1[8 + index] = vector2_6;
        vector2Array1[9 + index] = vector2_5;
        vector2Array1[10 + index] = vector2_7;
        vector2Array1[11 + index] = vector2_8;
        float x2 = (float) (((double) vector3Array[index + 2].x - (double) start.x) / ((double) end.x - (double) start.x));
        float scale1 = (double) maxScale * (double) this.m_rectTransform.lossyScale.y != 0.0 ? this.m_rectTransform.lossyScale.y : 1f;
        float scale2 = scale1;
        Vector2[] vector2Array2 = this.m_textInfo.meshInfo[0].uvs2;
        vector2Array2[0 + index] = this.PackUV(0.0f, 0.0f, scale1);
        vector2Array2[1 + index] = this.PackUV(0.0f, 1f, scale1);
        vector2Array2[2 + index] = this.PackUV(x2, 1f, scale1);
        vector2Array2[3 + index] = this.PackUV(x2, 0.0f, scale1);
        float x3 = (float) (((double) vector3Array[index + 4].x - (double) start.x) / ((double) end.x - (double) start.x));
        float x4 = (float) (((double) vector3Array[index + 6].x - (double) start.x) / ((double) end.x - (double) start.x));
        vector2Array2[4 + index] = this.PackUV(x3, 0.0f, scale2);
        vector2Array2[5 + index] = this.PackUV(x3, 1f, scale2);
        vector2Array2[6 + index] = this.PackUV(x4, 1f, scale2);
        vector2Array2[7 + index] = this.PackUV(x4, 0.0f, scale2);
        float x5 = (float) (((double) vector3Array[index + 8].x - (double) start.x) / ((double) end.x - (double) start.x));
        float num5 = (float) (((double) vector3Array[index + 6].x - (double) start.x) / ((double) end.x - (double) start.x));
        vector2Array2[8 + index] = this.PackUV(x5, 0.0f, scale1);
        vector2Array2[9 + index] = this.PackUV(x5, 1f, scale1);
        vector2Array2[10 + index] = this.PackUV(1f, 1f, scale1);
        vector2Array2[11 + index] = this.PackUV(1f, 0.0f, scale1);
        Color32[] color32Array = this.m_textInfo.meshInfo[0].colors32;
        color32Array[0 + index] = underlineColor;
        color32Array[1 + index] = underlineColor;
        color32Array[2 + index] = underlineColor;
        color32Array[3 + index] = underlineColor;
        color32Array[4 + index] = underlineColor;
        color32Array[5 + index] = underlineColor;
        color32Array[6 + index] = underlineColor;
        color32Array[7 + index] = underlineColor;
        color32Array[8 + index] = underlineColor;
        color32Array[9 + index] = underlineColor;
        color32Array[10 + index] = underlineColor;
        color32Array[11 + index] = underlineColor;
        index = index + 12;
      }
    }

    protected void GetSpecialCharacters(TMP_FontAsset fontAsset)
    {
      if (!fontAsset.characterDictionary.TryGetValue(95, out this.m_cached_Underline_GlyphInfo) && (UnityEngine.Object) this.m_settings != (UnityEngine.Object) null && !this.m_settings.warningsDisabled)
        Debug.LogWarning((object) ("Underscore character wasn't found in the current Font Asset [" + fontAsset.name + "]. No characters assigned for Underline."), (UnityEngine.Object) this);
      if (fontAsset.characterDictionary.TryGetValue(8230, out this.m_cached_Ellipsis_GlyphInfo) || !((UnityEngine.Object) this.m_settings != (UnityEngine.Object) null) || this.m_settings.warningsDisabled)
        return;
      Debug.LogWarning((object) ("Ellipsis character wasn't found in the current Font Asset [" + fontAsset.name + "]. No characters assigned for Ellipsis."), (UnityEngine.Object) this);
    }

    protected int GetMaterialReferenceForFontWeight()
    {
      this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentFontAsset.fontWeights[0].italicTypeface.material, this.m_currentFontAsset.fontWeights[0].italicTypeface, this.m_materialReferences, this.m_materialReferenceIndexLookup);
      return 0;
    }

    protected TMP_FontAsset GetAlternativeFontAsset()
    {
      bool flag = (this.m_style & FontStyles.Italic) == FontStyles.Italic || (this.m_fontStyle & FontStyles.Italic) == FontStyles.Italic;
      int index = this.m_fontWeightInternal / 100;
      TMP_FontAsset tmpFontAsset = !flag ? this.m_currentFontAsset.fontWeights[index].regularTypeface : this.m_currentFontAsset.fontWeights[index].italicTypeface;
      if ((UnityEngine.Object) tmpFontAsset == (UnityEngine.Object) null)
        return this.m_currentFontAsset;
      this.m_currentFontAsset = tmpFontAsset;
      return this.m_currentFontAsset;
    }

    protected Vector2 PackUV(float x, float y, float scale)
    {
      Vector2 vector2;
      vector2.x = Mathf.Floor(x * 4095f);
      vector2.y = Mathf.Floor(y * 4095f);
      vector2.x = vector2.x * 4096f + vector2.y;
      vector2.y = scale;
      return vector2;
    }

    protected float PackUV(float x, float y)
    {
      return (float) (Math.Floor((double) x * 4095.0) * 4096.0 + Math.Floor((double) y * 4095.0));
    }

    protected int HexToInt(char hex)
    {
      char ch = hex;
      switch (ch)
      {
        case '0':
          return 0;
        case '1':
          return 1;
        case '2':
          return 2;
        case '3':
          return 3;
        case '4':
          return 4;
        case '5':
          return 5;
        case '6':
          return 6;
        case '7':
          return 7;
        case '8':
          return 8;
        case '9':
          return 9;
        case 'A':
          return 10;
        case 'B':
          return 11;
        case 'C':
          return 12;
        case 'D':
          return 13;
        case 'E':
          return 14;
        case 'F':
          return 15;
        default:
          switch (ch)
          {
            case 'a':
              return 10;
            case 'b':
              return 11;
            case 'c':
              return 12;
            case 'd':
              return 13;
            case 'e':
              return 14;
            case 'f':
              return 15;
            default:
              return 15;
          }
      }
    }

    protected int GetUTF16(int i)
    {
      return this.HexToInt(this.m_text[i]) * 4096 + this.HexToInt(this.m_text[i + 1]) * 256 + this.HexToInt(this.m_text[i + 2]) * 16 + this.HexToInt(this.m_text[i + 3]);
    }

    protected int GetUTF32(int i)
    {
      return 0 + this.HexToInt(this.m_text[i]) * 268435456 + this.HexToInt(this.m_text[i + 1]) * 16777216 + this.HexToInt(this.m_text[i + 2]) * 1048576 + this.HexToInt(this.m_text[i + 3]) * 65536 + this.HexToInt(this.m_text[i + 4]) * 4096 + this.HexToInt(this.m_text[i + 5]) * 256 + this.HexToInt(this.m_text[i + 6]) * 16 + this.HexToInt(this.m_text[i + 7]);
    }

    protected Color32 HexCharsToColor(char[] hexChars, int tagCount)
    {
      if (tagCount == 7)
        return new Color32((byte) (this.HexToInt(hexChars[1]) * 16 + this.HexToInt(hexChars[2])), (byte) (this.HexToInt(hexChars[3]) * 16 + this.HexToInt(hexChars[4])), (byte) (this.HexToInt(hexChars[5]) * 16 + this.HexToInt(hexChars[6])), byte.MaxValue);
      if (tagCount == 9)
        return new Color32((byte) (this.HexToInt(hexChars[1]) * 16 + this.HexToInt(hexChars[2])), (byte) (this.HexToInt(hexChars[3]) * 16 + this.HexToInt(hexChars[4])), (byte) (this.HexToInt(hexChars[5]) * 16 + this.HexToInt(hexChars[6])), (byte) (this.HexToInt(hexChars[7]) * 16 + this.HexToInt(hexChars[8])));
      if (tagCount == 13)
        return new Color32((byte) (this.HexToInt(hexChars[7]) * 16 + this.HexToInt(hexChars[8])), (byte) (this.HexToInt(hexChars[9]) * 16 + this.HexToInt(hexChars[10])), (byte) (this.HexToInt(hexChars[11]) * 16 + this.HexToInt(hexChars[12])), byte.MaxValue);
      if (tagCount == 15)
        return new Color32((byte) (this.HexToInt(hexChars[7]) * 16 + this.HexToInt(hexChars[8])), (byte) (this.HexToInt(hexChars[9]) * 16 + this.HexToInt(hexChars[10])), (byte) (this.HexToInt(hexChars[11]) * 16 + this.HexToInt(hexChars[12])), (byte) (this.HexToInt(hexChars[13]) * 16 + this.HexToInt(hexChars[14])));
      return new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
    }

    protected Color32 HexCharsToColor(char[] hexChars, int startIndex, int length)
    {
      if (length == 7)
        return new Color32((byte) (this.HexToInt(hexChars[startIndex + 1]) * 16 + this.HexToInt(hexChars[startIndex + 2])), (byte) (this.HexToInt(hexChars[startIndex + 3]) * 16 + this.HexToInt(hexChars[startIndex + 4])), (byte) (this.HexToInt(hexChars[startIndex + 5]) * 16 + this.HexToInt(hexChars[startIndex + 6])), byte.MaxValue);
      if (length == 9)
        return new Color32((byte) (this.HexToInt(hexChars[startIndex + 1]) * 16 + this.HexToInt(hexChars[startIndex + 2])), (byte) (this.HexToInt(hexChars[startIndex + 3]) * 16 + this.HexToInt(hexChars[startIndex + 4])), (byte) (this.HexToInt(hexChars[startIndex + 5]) * 16 + this.HexToInt(hexChars[startIndex + 6])), (byte) (this.HexToInt(hexChars[startIndex + 7]) * 16 + this.HexToInt(hexChars[startIndex + 8])));
      return new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
    }

    protected float ConvertToFloat(char[] chars, int startIndex, int length, int decimalPointIndex)
    {
      if (startIndex == 0)
        return -9999f;
      int num1 = startIndex + length - 1;
      float num2 = 0.0f;
      float num3 = 1f;
      decimalPointIndex = decimalPointIndex <= 0 ? num1 + 1 : decimalPointIndex;
      if ((int) chars[startIndex] == 45)
      {
        ++startIndex;
        num3 = -1f;
      }
      if ((int) chars[startIndex] == 43 || (int) chars[startIndex] == 37)
        ++startIndex;
      for (int index = startIndex; index < num1 + 1; ++index)
      {
        if (!char.IsDigit(chars[index]) && (int) chars[index] != 46)
          return -9999f;
        switch (decimalPointIndex - index + 3)
        {
          case 0:
            num2 += (float) ((int) chars[index] - 48) * (1f / 1000f);
            break;
          case 1:
            num2 += (float) ((int) chars[index] - 48) * 0.01f;
            break;
          case 2:
            num2 += (float) ((int) chars[index] - 48) * 0.1f;
            break;
          case 4:
            num2 += (float) ((int) chars[index] - 48);
            break;
          case 5:
            num2 += (float) (((int) chars[index] - 48) * 10);
            break;
          case 6:
            num2 += (float) (((int) chars[index] - 48) * 100);
            break;
          case 7:
            num2 += (float) (((int) chars[index] - 48) * 1000);
            break;
        }
      }
      return num2 * num3;
    }

    protected bool ValidateHtmlTag(int[] chars, int startIndex, out int endIndex)
    {
      int tagCount = 0;
      byte num1 = 0;
      TagUnits tagUnits = TagUnits.Pixels;
      TagType tagType = TagType.None;
      int index1 = 0;
      this.m_xmlAttribute[index1].nameHashCode = 0;
      this.m_xmlAttribute[index1].valueType = TagType.None;
      this.m_xmlAttribute[index1].valueHashCode = 0;
      this.m_xmlAttribute[index1].valueStartIndex = 0;
      this.m_xmlAttribute[index1].valueLength = 0;
      this.m_xmlAttribute[index1].valueDecimalIndex = 0;
      endIndex = startIndex;
      bool flag1 = false;
      bool flag2 = false;
      for (int index2 = startIndex; index2 < chars.Length && chars[index2] != 0 && (tagCount < this.m_htmlTag.Length && chars[index2] != 60); ++index2)
      {
        if (chars[index2] == 62)
        {
          flag2 = true;
          endIndex = index2;
          this.m_htmlTag[tagCount] = char.MinValue;
          break;
        }
        this.m_htmlTag[tagCount] = (char) chars[index2];
        ++tagCount;
        if ((int) num1 == 1)
        {
          if (this.m_xmlAttribute[index1].valueStartIndex == 0)
          {
            if (chars[index2] == 43 || chars[index2] == 45 || char.IsDigit((char) chars[index2]))
            {
              tagType = TagType.NumericalValue;
              this.m_xmlAttribute[index1].valueType = TagType.NumericalValue;
              this.m_xmlAttribute[index1].valueStartIndex = tagCount - 1;
              ++this.m_xmlAttribute[index1].valueLength;
            }
            else if (chars[index2] == 35)
            {
              tagType = TagType.ColorValue;
              this.m_xmlAttribute[index1].valueType = TagType.ColorValue;
              this.m_xmlAttribute[index1].valueStartIndex = tagCount - 1;
              ++this.m_xmlAttribute[index1].valueLength;
            }
            else if (chars[index2] != 34)
            {
              tagType = TagType.StringValue;
              this.m_xmlAttribute[index1].valueType = TagType.StringValue;
              this.m_xmlAttribute[index1].valueStartIndex = tagCount - 1;
              this.m_xmlAttribute[index1].valueHashCode = (this.m_xmlAttribute[index1].valueHashCode << 5) + this.m_xmlAttribute[index1].valueHashCode ^ chars[index2];
              ++this.m_xmlAttribute[index1].valueLength;
            }
          }
          else if (tagType == TagType.NumericalValue)
          {
            if (chars[index2] == 46)
              this.m_xmlAttribute[index1].valueDecimalIndex = tagCount - 1;
            if (chars[index2] == 112 || chars[index2] == 101 || (chars[index2] == 37 || chars[index2] == 32))
            {
              num1 = (byte) 2;
              tagType = TagType.None;
              ++index1;
              this.m_xmlAttribute[index1].nameHashCode = 0;
              this.m_xmlAttribute[index1].valueType = TagType.None;
              this.m_xmlAttribute[index1].valueHashCode = 0;
              this.m_xmlAttribute[index1].valueStartIndex = 0;
              this.m_xmlAttribute[index1].valueLength = 0;
              this.m_xmlAttribute[index1].valueDecimalIndex = 0;
              if (chars[index2] == 101)
                tagUnits = TagUnits.FontUnits;
              else if (chars[index2] == 37)
                tagUnits = TagUnits.Percentage;
            }
            else if ((int) num1 != 2)
              ++this.m_xmlAttribute[index1].valueLength;
          }
          else if (tagType == TagType.ColorValue)
          {
            if (chars[index2] != 32)
            {
              ++this.m_xmlAttribute[index1].valueLength;
            }
            else
            {
              num1 = (byte) 2;
              tagType = TagType.None;
              ++index1;
              this.m_xmlAttribute[index1].nameHashCode = 0;
              this.m_xmlAttribute[index1].valueType = TagType.None;
              this.m_xmlAttribute[index1].valueHashCode = 0;
              this.m_xmlAttribute[index1].valueStartIndex = 0;
              this.m_xmlAttribute[index1].valueLength = 0;
              this.m_xmlAttribute[index1].valueDecimalIndex = 0;
            }
          }
          else if (tagType == TagType.StringValue)
          {
            if (chars[index2] != 34)
            {
              this.m_xmlAttribute[index1].valueHashCode = (this.m_xmlAttribute[index1].valueHashCode << 5) + this.m_xmlAttribute[index1].valueHashCode ^ chars[index2];
              ++this.m_xmlAttribute[index1].valueLength;
            }
            else
            {
              num1 = (byte) 2;
              tagType = TagType.None;
              ++index1;
              this.m_xmlAttribute[index1].nameHashCode = 0;
              this.m_xmlAttribute[index1].valueType = TagType.None;
              this.m_xmlAttribute[index1].valueHashCode = 0;
              this.m_xmlAttribute[index1].valueStartIndex = 0;
              this.m_xmlAttribute[index1].valueLength = 0;
              this.m_xmlAttribute[index1].valueDecimalIndex = 0;
            }
          }
        }
        if (chars[index2] == 61)
          num1 = (byte) 1;
        if ((int) num1 == 0 && chars[index2] == 32)
        {
          if (flag1)
            return false;
          flag1 = true;
          num1 = (byte) 2;
          tagType = TagType.None;
          ++index1;
          this.m_xmlAttribute[index1].nameHashCode = 0;
          this.m_xmlAttribute[index1].valueType = TagType.None;
          this.m_xmlAttribute[index1].valueHashCode = 0;
          this.m_xmlAttribute[index1].valueStartIndex = 0;
          this.m_xmlAttribute[index1].valueLength = 0;
          this.m_xmlAttribute[index1].valueDecimalIndex = 0;
        }
        if ((int) num1 == 0)
          this.m_xmlAttribute[index1].nameHashCode = (this.m_xmlAttribute[index1].nameHashCode << 3) - this.m_xmlAttribute[index1].nameHashCode + chars[index2];
        if ((int) num1 == 2 && chars[index2] == 32)
          num1 = (byte) 0;
      }
      if (!flag2 || this.tag_NoParsing && this.m_xmlAttribute[0].nameHashCode != 53822163)
        return false;
      if (this.m_xmlAttribute[0].nameHashCode == 53822163)
      {
        this.tag_NoParsing = false;
        return true;
      }
      if ((int) this.m_htmlTag[0] == 35 && tagCount == 7)
      {
        this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, tagCount);
        this.m_colorStack.Add(this.m_htmlColor);
        return true;
      }
      if ((int) this.m_htmlTag[0] == 35 && tagCount == 9)
      {
        this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, tagCount);
        this.m_colorStack.Add(this.m_htmlColor);
        return true;
      }
      int num2 = this.m_xmlAttribute[0].nameHashCode;
      switch (num2)
      {
        case 115:
          this.m_style |= FontStyles.Strikethrough;
          return true;
        case 117:
          this.m_style |= FontStyles.Underline;
          return true;
        default:
          switch (num2 - 444)
          {
            case 0:
              if ((this.m_fontStyle & FontStyles.Strikethrough) != FontStyles.Strikethrough)
                this.m_style &= ~FontStyles.Strikethrough;
              return true;
            case 2:
              if ((this.m_fontStyle & FontStyles.Underline) != FontStyles.Underline)
                this.m_style &= ~FontStyles.Underline;
              return true;
            default:
              if (num2 == 426)
                return true;
              if (num2 != 427)
              {
                if (num2 != -1885698441)
                {
                  if (num2 != -1668324918)
                  {
                    if (num2 != -1632103439)
                    {
                      if (num2 != -1616441709)
                      {
                        if (num2 != -884817987)
                        {
                          if (num2 != -445573839)
                          {
                            if (num2 != -445537194)
                            {
                              if (num2 != -330774850)
                              {
                                if (num2 != 98)
                                {
                                  if (num2 != 105)
                                  {
                                    if (num2 != 434)
                                    {
                                      if (num2 != 6380)
                                      {
                                        if (num2 != 6552)
                                        {
                                          if (num2 != 6566)
                                          {
                                            if (num2 != 22501)
                                            {
                                              if (num2 != 22673)
                                              {
                                                if (num2 != 22687)
                                                {
                                                  if (num2 != 41311)
                                                  {
                                                    if (num2 != 43066)
                                                    {
                                                      if (num2 != 43969)
                                                      {
                                                        if (num2 != 43991)
                                                        {
                                                          if (num2 != 45545)
                                                          {
                                                            if (num2 != 154158)
                                                            {
                                                              if (num2 != 155913)
                                                              {
                                                                if (num2 != 156816)
                                                                {
                                                                  if (num2 != 158392)
                                                                  {
                                                                    if (num2 != 275917)
                                                                    {
                                                                      if (num2 != 276254)
                                                                      {
                                                                        if (num2 == 280416)
                                                                          return false;
                                                                        if (num2 != 281955)
                                                                        {
                                                                          if (num2 != 320078)
                                                                          {
                                                                            if (num2 != 322689)
                                                                            {
                                                                              if (num2 != 327550)
                                                                              {
                                                                                if (num2 != 1065846)
                                                                                {
                                                                                  if (num2 != 1071884)
                                                                                  {
                                                                                    if (num2 != 1112618)
                                                                                    {
                                                                                      if (num2 != 1117479)
                                                                                      {
                                                                                        if (num2 == 1750458 || num2 == 1913798)
                                                                                          return false;
                                                                                        if (num2 != 1983971)
                                                                                        {
                                                                                          if (num2 != 2068980)
                                                                                          {
                                                                                            if (num2 != 2109854)
                                                                                            {
                                                                                              if (num2 != 2152041)
                                                                                              {
                                                                                                if (num2 != 2246877)
                                                                                                {
                                                                                                  if (num2 == 7443301)
                                                                                                    return false;
                                                                                                  if (num2 != 7513474)
                                                                                                  {
                                                                                                    if (num2 != 7598483)
                                                                                                    {
                                                                                                      if (num2 != 7639357)
                                                                                                      {
                                                                                                        if (num2 != 7681544)
                                                                                                        {
                                                                                                          if (num2 != 13526026)
                                                                                                          {
                                                                                                            if (num2 != 15115642)
                                                                                                            {
                                                                                                              if (num2 != 16034505)
                                                                                                              {
                                                                                                                if (num2 != 52232547)
                                                                                                                {
                                                                                                                  if (num2 != 54741026)
                                                                                                                  {
                                                                                                                    if (num2 != 730022849)
                                                                                                                    {
                                                                                                                      if (num2 != 766244328)
                                                                                                                      {
                                                                                                                        if (num2 != 781906058)
                                                                                                                        {
                                                                                                                          if (num2 != 1100728678)
                                                                                                                          {
                                                                                                                            if (num2 != 1109349752)
                                                                                                                            {
                                                                                                                              if (num2 != 1109386397)
                                                                                                                                return false;
                                                                                                                              float @float = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                                                                              if ((double) @float == -9999.0 || (double) @float == 0.0)
                                                                                                                                return false;
                                                                                                                              switch (tagUnits)
                                                                                                                              {
                                                                                                                                case TagUnits.Pixels:
                                                                                                                                  this.tag_LineIndent = @float;
                                                                                                                                  break;
                                                                                                                                case TagUnits.FontUnits:
                                                                                                                                  this.tag_LineIndent = @float;
                                                                                                                                  this.tag_LineIndent *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float) this.m_fontAsset.tabSize;
                                                                                                                                  break;
                                                                                                                                case TagUnits.Percentage:
                                                                                                                                  this.tag_LineIndent = (float) ((double) this.m_marginWidth * (double) @float / 100.0);
                                                                                                                                  break;
                                                                                                                              }
                                                                                                                              this.m_xAdvance += this.tag_LineIndent;
                                                                                                                              return true;
                                                                                                                            }
                                                                                                                            float float1 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                                                                            if ((double) float1 == -9999.0 || (double) float1 == 0.0)
                                                                                                                              return false;
                                                                                                                            this.m_lineHeight = float1;
                                                                                                                            switch (tagUnits)
                                                                                                                            {
                                                                                                                              case TagUnits.FontUnits:
                                                                                                                                this.m_lineHeight *= this.m_fontAsset.fontInfo.LineHeight * this.m_fontScale;
                                                                                                                                break;
                                                                                                                              case TagUnits.Percentage:
                                                                                                                                this.m_lineHeight = (float) ((double) this.m_fontAsset.fontInfo.LineHeight * (double) this.m_lineHeight / 100.0) * this.m_fontScale;
                                                                                                                                break;
                                                                                                                            }
                                                                                                                            return true;
                                                                                                                          }
                                                                                                                          float float2 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                                                                          if ((double) float2 == -9999.0 || (double) float2 == 0.0)
                                                                                                                            return false;
                                                                                                                          this.m_marginLeft = float2;
                                                                                                                          switch (tagUnits)
                                                                                                                          {
                                                                                                                            case TagUnits.FontUnits:
                                                                                                                              this.m_marginLeft *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float) this.m_fontAsset.tabSize;
                                                                                                                              break;
                                                                                                                            case TagUnits.Percentage:
                                                                                                                              this.m_marginLeft = (float) (((double) this.m_marginWidth - ((double) this.m_width == -1.0 ? 0.0 : (double) this.m_width)) * (double) this.m_marginLeft / 100.0);
                                                                                                                              break;
                                                                                                                          }
                                                                                                                          this.m_marginLeft = (double) this.m_marginLeft < 0.0 ? 0.0f : this.m_marginLeft;
                                                                                                                          return true;
                                                                                                                        }
                                                                                                                      }
                                                                                                                      else
                                                                                                                      {
                                                                                                                        this.m_style |= FontStyles.SmallCaps;
                                                                                                                        return true;
                                                                                                                      }
                                                                                                                    }
                                                                                                                    else
                                                                                                                    {
                                                                                                                      this.m_style |= FontStyles.LowerCase;
                                                                                                                      return true;
                                                                                                                    }
                                                                                                                  }
                                                                                                                  else
                                                                                                                  {
                                                                                                                    this.m_baselineOffset = 0.0f;
                                                                                                                    return true;
                                                                                                                  }
                                                                                                                }
                                                                                                                else
                                                                                                                  goto label_345;
                                                                                                              }
                                                                                                              else
                                                                                                              {
                                                                                                                float @float = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                                                                if ((double) @float == -9999.0 || (double) @float == 0.0)
                                                                                                                  return false;
                                                                                                                switch (tagUnits)
                                                                                                                {
                                                                                                                  case TagUnits.Pixels:
                                                                                                                    this.m_baselineOffset = @float;
                                                                                                                    return true;
                                                                                                                  case TagUnits.FontUnits:
                                                                                                                    this.m_baselineOffset = @float * this.m_fontScale * this.m_fontAsset.fontInfo.Ascender;
                                                                                                                    return true;
                                                                                                                  case TagUnits.Percentage:
                                                                                                                    return false;
                                                                                                                  default:
                                                                                                                    return false;
                                                                                                                }
                                                                                                              }
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                              this.tag_NoParsing = true;
                                                                                                              return true;
                                                                                                            }
                                                                                                          }
                                                                                                          this.m_style |= FontStyles.UpperCase;
                                                                                                          return true;
                                                                                                        }
                                                                                                        this.m_monoSpacing = 0.0f;
                                                                                                        return true;
                                                                                                      }
                                                                                                      this.m_marginLeft = 0.0f;
                                                                                                      this.m_marginRight = 0.0f;
                                                                                                      return true;
                                                                                                    }
                                                                                                    this.tag_Indent = this.m_indentStack.Remove();
                                                                                                    return true;
                                                                                                  }
                                                                                                  this.m_cSpacing = 0.0f;
                                                                                                  return true;
                                                                                                }
                                                                                                int hashCode = this.m_xmlAttribute[0].valueHashCode;
                                                                                                if (this.m_xmlAttribute[0].valueType == TagType.None || this.m_xmlAttribute[0].valueType == TagType.NumericalValue)
                                                                                                {
                                                                                                  if ((UnityEngine.Object) this.m_defaultSpriteAsset == (UnityEngine.Object) null)
                                                                                                  {
                                                                                                    if ((UnityEngine.Object) this.m_settings == (UnityEngine.Object) null)
                                                                                                      this.m_settings = TMP_Settings.LoadDefaultSettings();
                                                                                                    this.m_defaultSpriteAsset = !((UnityEngine.Object) this.m_settings != (UnityEngine.Object) null) || !((UnityEngine.Object) this.m_settings.spriteAsset != (UnityEngine.Object) null) ? Resources.Load<TMP_SpriteAsset>("Sprite Assets/Default Sprite Asset") : this.m_settings.spriteAsset;
                                                                                                  }
                                                                                                  this.m_currentSpriteAsset = this.m_defaultSpriteAsset;
                                                                                                  if ((UnityEngine.Object) this.m_currentSpriteAsset == (UnityEngine.Object) null)
                                                                                                    return false;
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                  TMP_SpriteAsset spriteAsset;
                                                                                                  if (MaterialReferenceManager.TryGetSpriteAsset(hashCode, out spriteAsset))
                                                                                                  {
                                                                                                    this.m_currentSpriteAsset = spriteAsset;
                                                                                                  }
                                                                                                  else
                                                                                                  {
                                                                                                    if ((UnityEngine.Object) spriteAsset == (UnityEngine.Object) null)
                                                                                                      spriteAsset = Resources.Load<TMP_SpriteAsset>("Sprites/" + new string(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength));
                                                                                                    if ((UnityEngine.Object) spriteAsset == (UnityEngine.Object) null)
                                                                                                      return false;
                                                                                                    MaterialReferenceManager.AddSpriteAsset(hashCode, spriteAsset);
                                                                                                    this.m_currentSpriteAsset = spriteAsset;
                                                                                                  }
                                                                                                }
                                                                                                if (this.m_xmlAttribute[0].valueType == TagType.NumericalValue)
                                                                                                {
                                                                                                  int num3 = (int) this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                                                  if (num3 == -9999)
                                                                                                    return false;
                                                                                                  this.m_spriteIndex = num3;
                                                                                                }
                                                                                                else if (this.m_xmlAttribute[1].nameHashCode == 43347)
                                                                                                {
                                                                                                  int spriteIndex = this.m_currentSpriteAsset.GetSpriteIndex(this.m_xmlAttribute[1].valueHashCode);
                                                                                                  if (spriteIndex == -1)
                                                                                                    return false;
                                                                                                  this.m_spriteIndex = spriteIndex;
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                  if (this.m_xmlAttribute[1].nameHashCode != 295562)
                                                                                                    return false;
                                                                                                  int num3 = (int) this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[1].valueStartIndex, this.m_xmlAttribute[1].valueLength, this.m_xmlAttribute[1].valueDecimalIndex);
                                                                                                  if (num3 == -9999)
                                                                                                    return false;
                                                                                                  this.m_spriteIndex = num3;
                                                                                                  if (this.m_spriteIndex > this.m_currentSpriteAsset.spriteInfoList.Count - 1)
                                                                                                    return false;
                                                                                                }
                                                                                                this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentSpriteAsset.material, this.m_currentSpriteAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
                                                                                                this.m_spriteColor = TMP_Text.s_colorWhite;
                                                                                                this.m_tintSprite = false;
                                                                                                if (this.m_xmlAttribute[1].nameHashCode == 45819)
                                                                                                  this.m_tintSprite = (double) this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[1].valueStartIndex, this.m_xmlAttribute[1].valueLength, this.m_xmlAttribute[1].valueDecimalIndex) != 0.0;
                                                                                                else if (this.m_xmlAttribute[2].nameHashCode == 45819)
                                                                                                  this.m_tintSprite = (double) this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[2].valueStartIndex, this.m_xmlAttribute[2].valueLength, this.m_xmlAttribute[2].valueDecimalIndex) != 0.0;
                                                                                                if (this.m_xmlAttribute[1].nameHashCode == 281955)
                                                                                                  this.m_spriteColor = this.HexCharsToColor(this.m_htmlTag, this.m_xmlAttribute[1].valueStartIndex, this.m_xmlAttribute[1].valueLength);
                                                                                                else if (this.m_xmlAttribute[2].nameHashCode == 281955)
                                                                                                  this.m_spriteColor = this.HexCharsToColor(this.m_htmlTag, this.m_xmlAttribute[2].valueStartIndex, this.m_xmlAttribute[2].valueLength);
                                                                                                this.m_xmlAttribute[1].nameHashCode = 0;
                                                                                                this.m_xmlAttribute[2].nameHashCode = 0;
                                                                                                this.m_textElementType = TMP_TextElementType.Sprite;
                                                                                                return true;
                                                                                              }
                                                                                              float float3 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                                              if ((double) float3 == -9999.0 || (double) float3 == 0.0)
                                                                                                return false;
                                                                                              switch (tagUnits)
                                                                                              {
                                                                                                case TagUnits.Pixels:
                                                                                                  this.m_monoSpacing = float3;
                                                                                                  break;
                                                                                                case TagUnits.FontUnits:
                                                                                                  this.m_monoSpacing = float3;
                                                                                                  this.m_monoSpacing *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float) this.m_fontAsset.tabSize;
                                                                                                  break;
                                                                                                case TagUnits.Percentage:
                                                                                                  return false;
                                                                                              }
                                                                                              return true;
                                                                                            }
                                                                                            float float4 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                                            if ((double) float4 == -9999.0 || (double) float4 == 0.0)
                                                                                              return false;
                                                                                            this.m_marginLeft = float4;
                                                                                            switch (tagUnits)
                                                                                            {
                                                                                              case TagUnits.FontUnits:
                                                                                                this.m_marginLeft *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float) this.m_fontAsset.tabSize;
                                                                                                break;
                                                                                              case TagUnits.Percentage:
                                                                                                this.m_marginLeft = (float) (((double) this.m_marginWidth - ((double) this.m_width == -1.0 ? 0.0 : (double) this.m_width)) * (double) this.m_marginLeft / 100.0);
                                                                                                break;
                                                                                            }
                                                                                            this.m_marginLeft = (double) this.m_marginLeft < 0.0 ? 0.0f : this.m_marginLeft;
                                                                                            this.m_marginRight = this.m_marginLeft;
                                                                                            return true;
                                                                                          }
                                                                                          float float5 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                                          if ((double) float5 == -9999.0 || (double) float5 == 0.0)
                                                                                            return false;
                                                                                          switch (tagUnits)
                                                                                          {
                                                                                            case TagUnits.Pixels:
                                                                                              this.tag_Indent = float5;
                                                                                              break;
                                                                                            case TagUnits.FontUnits:
                                                                                              this.tag_Indent = float5;
                                                                                              this.tag_Indent *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float) this.m_fontAsset.tabSize;
                                                                                              break;
                                                                                            case TagUnits.Percentage:
                                                                                              this.tag_Indent = (float) ((double) this.m_marginWidth * (double) float5 / 100.0);
                                                                                              break;
                                                                                          }
                                                                                          this.m_indentStack.Add(this.tag_Indent);
                                                                                          this.m_xAdvance = this.tag_Indent;
                                                                                          return true;
                                                                                        }
                                                                                        float float6 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                                        if ((double) float6 == -9999.0 || (double) float6 == 0.0)
                                                                                          return false;
                                                                                        switch (tagUnits)
                                                                                        {
                                                                                          case TagUnits.Pixels:
                                                                                            this.m_cSpacing = float6;
                                                                                            break;
                                                                                          case TagUnits.FontUnits:
                                                                                            this.m_cSpacing = float6;
                                                                                            this.m_cSpacing *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float) this.m_fontAsset.tabSize;
                                                                                            break;
                                                                                          case TagUnits.Percentage:
                                                                                            return false;
                                                                                        }
                                                                                        return true;
                                                                                      }
                                                                                      this.m_width = -1f;
                                                                                      return true;
                                                                                    }
                                                                                    TMP_Style tmpStyle = TMP_StyleSheet.GetStyle(this.m_xmlAttribute[0].valueHashCode) ?? TMP_StyleSheet.GetStyle(this.m_styleStack.Remove());
                                                                                    if (tmpStyle == null)
                                                                                      return false;
                                                                                    for (int endIndex1 = 0; endIndex1 < tmpStyle.styleClosingTagArray.Length; ++endIndex1)
                                                                                    {
                                                                                      if (tmpStyle.styleClosingTagArray[endIndex1] == 60)
                                                                                        this.ValidateHtmlTag(tmpStyle.styleClosingTagArray, endIndex1 + 1, out endIndex1);
                                                                                    }
                                                                                    return true;
                                                                                  }
                                                                                  this.m_htmlColor = this.m_colorStack.Remove();
                                                                                  return true;
                                                                                }
                                                                                this.m_lineJustification = this.m_textAlignment;
                                                                                return true;
                                                                              }
                                                                              float float7 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                              if ((double) float7 == -9999.0 || (double) float7 == 0.0)
                                                                                return false;
                                                                              switch (tagUnits)
                                                                              {
                                                                                case TagUnits.Pixels:
                                                                                  this.m_width = float7;
                                                                                  break;
                                                                                case TagUnits.FontUnits:
                                                                                  return false;
                                                                                case TagUnits.Percentage:
                                                                                  this.m_width = (float) ((double) this.m_marginWidth * (double) float7 / 100.0);
                                                                                  break;
                                                                              }
                                                                              return true;
                                                                            }
                                                                            TMP_Style style = TMP_StyleSheet.GetStyle(this.m_xmlAttribute[0].valueHashCode);
                                                                            if (style == null)
                                                                              return false;
                                                                            this.m_styleStack.Add(style.hashCode);
                                                                            for (int endIndex1 = 0; endIndex1 < style.styleOpeningTagArray.Length; ++endIndex1)
                                                                            {
                                                                              if (style.styleOpeningTagArray[endIndex1] == 60)
                                                                                this.ValidateHtmlTag(style.styleOpeningTagArray, endIndex1 + 1, out endIndex1);
                                                                            }
                                                                            return true;
                                                                          }
                                                                          float float8 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                                          if ((double) float8 == -9999.0 || (double) float8 == 0.0)
                                                                            return false;
                                                                          switch (tagUnits)
                                                                          {
                                                                            case TagUnits.Pixels:
                                                                              this.m_xAdvance += float8;
                                                                              return true;
                                                                            case TagUnits.FontUnits:
                                                                              this.m_xAdvance += float8 * this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float) this.m_fontAsset.tabSize;
                                                                              return true;
                                                                            case TagUnits.Percentage:
                                                                              return false;
                                                                            default:
                                                                              return false;
                                                                          }
                                                                        }
                                                                        else
                                                                        {
                                                                          if ((int) this.m_htmlTag[6] == 35 && tagCount == 13)
                                                                          {
                                                                            this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, tagCount);
                                                                            this.m_colorStack.Add(this.m_htmlColor);
                                                                            return true;
                                                                          }
                                                                          if ((int) this.m_htmlTag[6] == 35 && tagCount == 15)
                                                                          {
                                                                            this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, tagCount);
                                                                            this.m_colorStack.Add(this.m_htmlColor);
                                                                            return true;
                                                                          }
                                                                          switch (this.m_xmlAttribute[0].valueHashCode)
                                                                          {
                                                                            case -36881330:
                                                                              this.m_htmlColor = new Color32((byte) 160, (byte) 32, (byte) 240, byte.MaxValue);
                                                                              this.m_colorStack.Add(this.m_htmlColor);
                                                                              return true;
                                                                            case 125395:
                                                                              this.m_htmlColor = (Color32) Color.red;
                                                                              this.m_colorStack.Add(this.m_htmlColor);
                                                                              return true;
                                                                            case 3573310:
                                                                              this.m_htmlColor = (Color32) Color.blue;
                                                                              this.m_colorStack.Add(this.m_htmlColor);
                                                                              return true;
                                                                            case 26556144:
                                                                              this.m_htmlColor = new Color32(byte.MaxValue, (byte) 128, (byte) 0, byte.MaxValue);
                                                                              this.m_colorStack.Add(this.m_htmlColor);
                                                                              return true;
                                                                            case 117905991:
                                                                              this.m_htmlColor = (Color32) Color.black;
                                                                              this.m_colorStack.Add(this.m_htmlColor);
                                                                              return true;
                                                                            case 121463835:
                                                                              this.m_htmlColor = (Color32) Color.green;
                                                                              this.m_colorStack.Add(this.m_htmlColor);
                                                                              return true;
                                                                            case 140357351:
                                                                              this.m_htmlColor = (Color32) Color.white;
                                                                              this.m_colorStack.Add(this.m_htmlColor);
                                                                              return true;
                                                                            case 554054276:
                                                                              this.m_htmlColor = (Color32) Color.yellow;
                                                                              this.m_colorStack.Add(this.m_htmlColor);
                                                                              return true;
                                                                            default:
                                                                              return false;
                                                                          }
                                                                        }
                                                                      }
                                                                      else
                                                                      {
                                                                        if (this.m_xmlAttribute[0].valueLength != 3)
                                                                          return false;
                                                                        this.m_htmlColor.a = (byte) (this.HexToInt(this.m_htmlTag[7]) * 16 + this.HexToInt(this.m_htmlTag[8]));
                                                                        return true;
                                                                      }
                                                                    }
                                                                    else
                                                                    {
                                                                      switch (this.m_xmlAttribute[0].valueHashCode)
                                                                      {
                                                                        case -523808257:
                                                                          this.m_lineJustification = TextAlignmentOptions.Justified;
                                                                          return true;
                                                                        case -458210101:
                                                                          this.m_lineJustification = TextAlignmentOptions.Center;
                                                                          return true;
                                                                        case 3774683:
                                                                          this.m_lineJustification = TextAlignmentOptions.Left;
                                                                          return true;
                                                                        case 136703040:
                                                                          this.m_lineJustification = TextAlignmentOptions.Right;
                                                                          return true;
                                                                        default:
                                                                          return false;
                                                                      }
                                                                    }
                                                                  }
                                                                  else
                                                                  {
                                                                    this.m_currentFontSize = this.m_sizeStack.Remove();
                                                                    this.m_fontScale = (float) ((double) this.m_currentFontSize / (double) this.m_currentFontAsset.fontInfo.PointSize * (double) this.m_currentFontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
                                                                    return true;
                                                                  }
                                                                }
                                                                else
                                                                {
                                                                  this.m_isNonBreakingSpace = false;
                                                                  return true;
                                                                }
                                                              }
                                                              else
                                                              {
                                                                if (this.m_isParsingText)
                                                                {
                                                                  this.tag_LinkInfo.linkTextLength = this.m_characterCount - this.tag_LinkInfo.linkTextfirstCharacterIndex;
                                                                  int length = this.m_textInfo.linkInfo.Length;
                                                                  if (this.m_textInfo.linkCount + 1 > length)
                                                                    TMP_TextInfo.Resize<TMP_LinkInfo>(ref this.m_textInfo.linkInfo, length + 1);
                                                                  this.m_textInfo.linkInfo[this.m_textInfo.linkCount] = this.tag_LinkInfo;
                                                                  ++this.m_textInfo.linkCount;
                                                                }
                                                                return true;
                                                              }
                                                            }
                                                            else
                                                            {
                                                              MaterialReference materialReference = this.m_materialReferenceStack.Remove();
                                                              this.m_currentFontAsset = materialReference.fontAsset;
                                                              this.m_currentMaterial = materialReference.material;
                                                              this.m_currentMaterialIndex = materialReference.index;
                                                              this.m_fontScale = (float) ((double) this.m_currentFontSize / (double) this.m_currentFontAsset.fontInfo.PointSize * (double) this.m_currentFontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
                                                              return true;
                                                            }
                                                          }
                                                          else
                                                          {
                                                            float @float = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                                            if ((double) @float == -9999.0 || (double) @float == 0.0)
                                                              return false;
                                                            switch (tagUnits)
                                                            {
                                                              case TagUnits.Pixels:
                                                                if ((int) this.m_htmlTag[5] == 43)
                                                                {
                                                                  this.m_currentFontSize = this.m_fontSize + @float;
                                                                  this.m_sizeStack.Add(this.m_currentFontSize);
                                                                  this.m_fontScale = (float) ((double) this.m_currentFontSize / (double) this.m_currentFontAsset.fontInfo.PointSize * (double) this.m_currentFontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
                                                                  return true;
                                                                }
                                                                if ((int) this.m_htmlTag[5] == 45)
                                                                {
                                                                  this.m_currentFontSize = this.m_fontSize + @float;
                                                                  this.m_sizeStack.Add(this.m_currentFontSize);
                                                                  this.m_fontScale = (float) ((double) this.m_currentFontSize / (double) this.m_currentFontAsset.fontInfo.PointSize * (double) this.m_currentFontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
                                                                  return true;
                                                                }
                                                                this.m_currentFontSize = @float;
                                                                this.m_sizeStack.Add(this.m_currentFontSize);
                                                                this.m_fontScale = (float) ((double) this.m_currentFontSize / (double) this.m_currentFontAsset.fontInfo.PointSize * (double) this.m_currentFontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
                                                                return true;
                                                              case TagUnits.FontUnits:
                                                                this.m_currentFontSize = this.m_fontSize * @float;
                                                                this.m_sizeStack.Add(this.m_currentFontSize);
                                                                this.m_fontScale = (float) ((double) this.m_currentFontSize / (double) this.m_currentFontAsset.fontInfo.PointSize * (double) this.m_currentFontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
                                                                return true;
                                                              case TagUnits.Percentage:
                                                                this.m_currentFontSize = (float) ((double) this.m_fontSize * (double) @float / 100.0);
                                                                this.m_sizeStack.Add(this.m_currentFontSize);
                                                                this.m_fontScale = (float) ((double) this.m_currentFontSize / (double) this.m_currentFontAsset.fontInfo.PointSize * (double) this.m_currentFontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
                                                                return true;
                                                              default:
                                                                return false;
                                                            }
                                                          }
                                                        }
                                                        else
                                                        {
                                                          if (this.m_overflowMode == TextOverflowModes.Page)
                                                          {
                                                            this.m_xAdvance = 0.0f + this.tag_LineIndent + this.tag_Indent;
                                                            this.m_lineOffset = 0.0f;
                                                            ++this.m_pageNumber;
                                                            this.m_isNewPage = true;
                                                          }
                                                          return true;
                                                        }
                                                      }
                                                      else
                                                      {
                                                        this.m_isNonBreakingSpace = true;
                                                        return true;
                                                      }
                                                    }
                                                    else
                                                    {
                                                      if (this.m_isParsingText)
                                                      {
                                                        this.tag_LinkInfo.textComponent = this;
                                                        this.tag_LinkInfo.hashCode = this.m_xmlAttribute[0].valueHashCode;
                                                        this.tag_LinkInfo.linkTextfirstCharacterIndex = this.m_characterCount;
                                                        this.tag_LinkInfo.linkIdFirstCharacterIndex = startIndex + this.m_xmlAttribute[0].valueStartIndex;
                                                        this.tag_LinkInfo.linkIdLength = this.m_xmlAttribute[0].valueLength;
                                                      }
                                                      return true;
                                                    }
                                                  }
                                                  else
                                                  {
                                                    int hashCode1 = this.m_xmlAttribute[0].valueHashCode;
                                                    int num3 = this.m_xmlAttribute[1].nameHashCode;
                                                    int hashCode2 = this.m_xmlAttribute[1].valueHashCode;
                                                    if (hashCode1 == 764638571 || hashCode1 == 523367755)
                                                    {
                                                      this.m_currentFontAsset = this.m_materialReferences[0].fontAsset;
                                                      this.m_currentMaterial = this.m_materialReferences[0].material;
                                                      this.m_currentMaterialIndex = 0;
                                                      this.m_fontScale = (float) ((double) this.m_currentFontSize / (double) this.m_currentFontAsset.fontInfo.PointSize * (double) this.m_currentFontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
                                                      this.m_materialReferenceStack.Add(this.m_materialReferences[0]);
                                                      return true;
                                                    }
                                                    TMP_FontAsset fontAsset;
                                                    if (!MaterialReferenceManager.TryGetFontAsset(hashCode1, out fontAsset))
                                                    {
                                                      fontAsset = Resources.Load<TMP_FontAsset>("Fonts & Materials/" + new string(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength));
                                                      if ((UnityEngine.Object) fontAsset == (UnityEngine.Object) null)
                                                        return false;
                                                      MaterialReferenceManager.AddFontAsset(fontAsset);
                                                    }
                                                    if (num3 == 0 && hashCode2 == 0)
                                                    {
                                                      this.m_currentMaterial = fontAsset.material;
                                                      this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentMaterial, fontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
                                                      this.m_materialReferenceStack.Add(this.m_materialReferences[this.m_currentMaterialIndex]);
                                                    }
                                                    else
                                                    {
                                                      if (num3 != 103415287)
                                                        return false;
                                                      Material material;
                                                      if (MaterialReferenceManager.TryGetMaterial(hashCode2, out material))
                                                      {
                                                        this.m_currentMaterial = material;
                                                        this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentMaterial, fontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
                                                        this.m_materialReferenceStack.Add(this.m_materialReferences[this.m_currentMaterialIndex]);
                                                      }
                                                      else
                                                      {
                                                        material = Resources.Load<Material>("Fonts & Materials/" + new string(this.m_htmlTag, this.m_xmlAttribute[1].valueStartIndex, this.m_xmlAttribute[1].valueLength));
                                                        if ((UnityEngine.Object) material == (UnityEngine.Object) null)
                                                          return false;
                                                        MaterialReferenceManager.AddFontMaterial(hashCode2, material);
                                                        this.m_currentMaterial = material;
                                                        this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentMaterial, fontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
                                                        this.m_materialReferenceStack.Add(this.m_materialReferences[this.m_currentMaterialIndex]);
                                                      }
                                                    }
                                                    this.m_currentFontAsset = fontAsset;
                                                    this.m_fontScale = (float) ((double) this.m_currentFontSize / (double) this.m_currentFontAsset.fontInfo.PointSize * (double) this.m_currentFontAsset.fontInfo.Scale * (!this.m_isOrthographic ? 0.100000001490116 : 1.0));
                                                    return true;
                                                  }
                                                }
                                                else
                                                {
                                                  if ((this.m_style & FontStyles.Superscript) == FontStyles.Superscript)
                                                  {
                                                    if ((this.m_style & FontStyles.Subscript) == FontStyles.Subscript)
                                                    {
                                                      this.m_fontScaleMultiplier = (double) this.m_currentFontAsset.fontInfo.SubSize <= 0.0 ? 1f : this.m_currentFontAsset.fontInfo.SubSize;
                                                      this.m_baselineOffset = this.m_currentFontAsset.fontInfo.SubscriptOffset * this.m_fontScale * this.m_fontScaleMultiplier;
                                                    }
                                                    else
                                                    {
                                                      this.m_baselineOffset = 0.0f;
                                                      this.m_fontScaleMultiplier = 1f;
                                                    }
                                                    this.m_style &= ~FontStyles.Superscript;
                                                  }
                                                  return true;
                                                }
                                              }
                                              else
                                              {
                                                if ((this.m_style & FontStyles.Subscript) == FontStyles.Subscript)
                                                {
                                                  if ((this.m_style & FontStyles.Superscript) == FontStyles.Superscript)
                                                  {
                                                    this.m_fontScaleMultiplier = (double) this.m_currentFontAsset.fontInfo.SubSize <= 0.0 ? 1f : this.m_currentFontAsset.fontInfo.SubSize;
                                                    this.m_baselineOffset = this.m_currentFontAsset.fontInfo.SuperscriptOffset * this.m_fontScale * this.m_fontScaleMultiplier;
                                                  }
                                                  else
                                                  {
                                                    this.m_baselineOffset = 0.0f;
                                                    this.m_fontScaleMultiplier = 1f;
                                                  }
                                                  this.m_style &= ~FontStyles.Subscript;
                                                }
                                                return true;
                                              }
                                            }
                                            else
                                            {
                                              this.m_isIgnoringAlignment = false;
                                              return true;
                                            }
                                          }
                                          else
                                          {
                                            this.m_fontScaleMultiplier = (double) this.m_currentFontAsset.fontInfo.SubSize <= 0.0 ? 1f : this.m_currentFontAsset.fontInfo.SubSize;
                                            this.m_baselineOffset = this.m_currentFontAsset.fontInfo.SuperscriptOffset * this.m_fontScale * this.m_fontScaleMultiplier;
                                            this.m_style |= FontStyles.Superscript;
                                            return true;
                                          }
                                        }
                                        else
                                        {
                                          this.m_fontScaleMultiplier = (double) this.m_currentFontAsset.fontInfo.SubSize <= 0.0 ? 1f : this.m_currentFontAsset.fontInfo.SubSize;
                                          this.m_baselineOffset = this.m_currentFontAsset.fontInfo.SubscriptOffset * this.m_fontScale * this.m_fontScaleMultiplier;
                                          this.m_style |= FontStyles.Subscript;
                                          return true;
                                        }
                                      }
                                      else
                                      {
                                        float @float = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                        if ((double) @float == -9999.0)
                                          return false;
                                        switch (tagUnits)
                                        {
                                          case TagUnits.Pixels:
                                            this.m_xAdvance = @float;
                                            return true;
                                          case TagUnits.FontUnits:
                                            this.m_xAdvance = @float * this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float) this.m_fontAsset.tabSize;
                                            return true;
                                          case TagUnits.Percentage:
                                            this.m_xAdvance = (float) ((double) this.m_marginWidth * (double) @float / 100.0);
                                            return true;
                                          default:
                                            return false;
                                        }
                                      }
                                    }
                                    else
                                    {
                                      this.m_style &= ~FontStyles.Italic;
                                      return true;
                                    }
                                  }
                                  else
                                  {
                                    this.m_style |= FontStyles.Italic;
                                    return true;
                                  }
                                }
                                else
                                {
                                  this.m_style |= FontStyles.Bold;
                                  this.m_fontWeightInternal = 700;
                                  this.m_fontWeightStack.Add(700);
                                  return true;
                                }
                              }
                              else
                              {
                                float @float = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                                if ((double) @float == -9999.0 || (double) @float == 0.0)
                                  return false;
                                if ((this.m_fontStyle & FontStyles.Bold) == FontStyles.Bold)
                                  return true;
                                this.m_style &= ~FontStyles.Bold;
                                switch ((int) @float)
                                {
                                  case 100:
                                    this.m_fontWeightInternal = 100;
                                    break;
                                  case 200:
                                    this.m_fontWeightInternal = 200;
                                    break;
                                  case 300:
                                    this.m_fontWeightInternal = 300;
                                    break;
                                  case 400:
                                    this.m_fontWeightInternal = 400;
                                    break;
                                  case 500:
                                    this.m_fontWeightInternal = 500;
                                    break;
                                  case 600:
                                    this.m_fontWeightInternal = 600;
                                    break;
                                  case 700:
                                    this.m_fontWeightInternal = 700;
                                    this.m_style |= FontStyles.Bold;
                                    break;
                                  case 800:
                                    this.m_fontWeightInternal = 800;
                                    break;
                                  case 900:
                                    this.m_fontWeightInternal = 900;
                                    break;
                                }
                                this.m_fontWeightStack.Add(this.m_fontWeightInternal);
                                return true;
                              }
                            }
                            else
                            {
                              this.tag_LineIndent = 0.0f;
                              return true;
                            }
                          }
                          else
                          {
                            this.m_lineHeight = 0.0f;
                            return true;
                          }
                        }
                        else
                        {
                          float @float = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength, this.m_xmlAttribute[0].valueDecimalIndex);
                          if ((double) @float == -9999.0 || (double) @float == 0.0)
                            return false;
                          this.m_marginRight = @float;
                          switch (tagUnits)
                          {
                            case TagUnits.FontUnits:
                              this.m_marginRight *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float) this.m_fontAsset.tabSize;
                              break;
                            case TagUnits.Percentage:
                              this.m_marginRight = (float) (((double) this.m_marginWidth - ((double) this.m_width == -1.0 ? 0.0 : (double) this.m_width)) * (double) this.m_marginRight / 100.0);
                              break;
                          }
                          this.m_marginRight = (double) this.m_marginRight < 0.0 ? 0.0f : this.m_marginRight;
                          return true;
                        }
                      }
label_345:
                      this.m_style &= ~FontStyles.UpperCase;
                      return true;
                    }
                    this.m_style &= ~FontStyles.SmallCaps;
                    return true;
                  }
                  this.m_style &= ~FontStyles.LowerCase;
                  return true;
                }
                this.m_fontWeightInternal = this.m_fontWeightStack.Remove();
                return true;
              }
              if ((this.m_fontStyle & FontStyles.Bold) != FontStyles.Bold)
              {
                this.m_style &= ~FontStyles.Bold;
                this.m_fontWeightInternal = this.m_fontWeightStack.Remove();
              }
              return true;
          }
      }
    }

    protected enum TextInputSources
    {
      Text,
      SetText,
      SetCharArray,
    }
  }
}
