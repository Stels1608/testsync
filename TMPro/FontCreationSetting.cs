﻿// Decompiled with JetBrains decompiler
// Type: TMPro.FontCreationSetting
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

namespace TMPro
{
  [Serializable]
  public struct FontCreationSetting
  {
    public string fontSourcePath;
    public int fontSizingMode;
    public int fontSize;
    public int fontPadding;
    public int fontPackingMode;
    public int fontAtlasWidth;
    public int fontAtlasHeight;
    public int fontCharacterSet;
    public int fontStyle;
    public float fontStlyeModifier;
    public int fontRenderMode;
    public bool fontKerning;
  }
}
