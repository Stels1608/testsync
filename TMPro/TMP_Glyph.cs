﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_Glyph
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

namespace TMPro
{
  [Serializable]
  public class TMP_Glyph : TMP_TextElement
  {
    public static TMP_Glyph Clone(TMP_Glyph source)
    {
      TMP_Glyph tmpGlyph = new TMP_Glyph();
      tmpGlyph.id = source.id;
      tmpGlyph.x = source.x;
      tmpGlyph.y = source.y;
      tmpGlyph.width = source.width;
      tmpGlyph.height = source.height;
      tmpGlyph.xOffset = source.xOffset;
      tmpGlyph.yOffset = source.yOffset;
      tmpGlyph.xAdvance = source.xAdvance;
      return tmpGlyph;
    }
  }
}
