﻿// Decompiled with JetBrains decompiler
// Type: TMPro.Mesh_Extents
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TMPro
{
  [Serializable]
  public struct Mesh_Extents
  {
    public Vector2 min;
    public Vector2 max;

    public Mesh_Extents(Vector2 min, Vector2 max)
    {
      this.min = min;
      this.max = max;
    }

    public override string ToString()
    {
      return "Min (" + this.min.x.ToString("f2") + ", " + this.min.y.ToString("f2") + ")   Max (" + this.max.x.ToString("f2") + ", " + this.max.y.ToString("f2") + ")";
    }
  }
}
