﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_Settings
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace TMPro
{
  [ExecuteInEditMode]
  [Serializable]
  public class TMP_Settings : ScriptableObject
  {
    public static TMP_Settings s_Instance;
    public bool enableWordWrapping;
    public bool enableKerning;
    public bool enableExtraPadding;
    public bool enableTintAllSprites;
    public bool warningsDisabled;
    public TMP_FontAsset fontAsset;
    public List<TMP_FontAsset> fallbackFontAssets;
    public TMP_SpriteAsset spriteAsset;
    public TMP_StyleSheet styleSheet;

    public static TMP_Settings instance
    {
      get
      {
        if ((UnityEngine.Object) TMP_Settings.s_Instance == (UnityEngine.Object) null)
          TMP_Settings.s_Instance = Resources.Load("TMP Settings") as TMP_Settings;
        return TMP_Settings.s_Instance;
      }
    }

    public static TMP_Settings LoadDefaultSettings()
    {
      if ((UnityEngine.Object) TMP_Settings.s_Instance == (UnityEngine.Object) null)
      {
        TMP_Settings tmpSettings = Resources.Load("TMP Settings") as TMP_Settings;
        if ((UnityEngine.Object) tmpSettings != (UnityEngine.Object) null)
          TMP_Settings.s_Instance = tmpSettings;
      }
      return TMP_Settings.s_Instance;
    }

    public static TMP_Settings GetSettings()
    {
      if ((UnityEngine.Object) TMP_Settings.instance == (UnityEngine.Object) null)
        return (TMP_Settings) null;
      return TMP_Settings.instance;
    }

    public static TMP_FontAsset GetFontAsset()
    {
      if ((UnityEngine.Object) TMP_Settings.instance == (UnityEngine.Object) null)
        return (TMP_FontAsset) null;
      return TMP_Settings.instance.fontAsset;
    }

    public static TMP_SpriteAsset GetSpriteAsset()
    {
      if ((UnityEngine.Object) TMP_Settings.instance == (UnityEngine.Object) null)
        return (TMP_SpriteAsset) null;
      return TMP_Settings.instance.spriteAsset;
    }

    public static TMP_StyleSheet GetStyleSheet()
    {
      if ((UnityEngine.Object) TMP_Settings.instance == (UnityEngine.Object) null)
        return (TMP_StyleSheet) null;
      return TMP_Settings.instance.styleSheet;
    }
  }
}
