﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_MeshInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TMPro
{
  public struct TMP_MeshInfo
  {
    private static readonly Color32 s_DefaultColor = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
    private static readonly Vector3 s_DefaultNormal = new Vector3(0.0f, 0.0f, -1f);
    private static readonly Vector4 s_DefaultTangent = new Vector4(-1f, 0.0f, 0.0f, 1f);
    public Mesh mesh;
    public int vertexCount;
    public Vector3[] vertices;
    public Vector3[] normals;
    public Vector4[] tangents;
    public Vector2[] uvs0;
    public Vector2[] uvs2;
    public Color32[] colors32;
    public int[] triangles;
    public UIVertex[] uiVertices;

    public TMP_MeshInfo(Mesh mesh, int size)
    {
      if ((UnityEngine.Object) mesh == (UnityEngine.Object) null)
        mesh = new Mesh();
      else
        mesh.Clear();
      this.mesh = mesh;
      int length1 = size * 4;
      int length2 = size * 6;
      this.vertexCount = 0;
      this.vertices = new Vector3[length1];
      this.uvs0 = new Vector2[length1];
      this.uvs2 = new Vector2[length1];
      this.colors32 = new Color32[length1];
      this.normals = new Vector3[length1];
      this.tangents = new Vector4[length1];
      this.triangles = new int[length2];
      this.uiVertices = new UIVertex[length1];
      int index1 = 0;
      int num = 0;
      while (num / 4 < size)
      {
        for (int index2 = 0; index2 < 4; ++index2)
        {
          this.vertices[num + index2] = Vector3.zero;
          this.uvs0[num + index2] = Vector2.zero;
          this.uvs2[num + index2] = Vector2.zero;
          this.colors32[num + index2] = TMP_MeshInfo.s_DefaultColor;
          this.normals[num + index2] = TMP_MeshInfo.s_DefaultNormal;
          this.tangents[num + index2] = TMP_MeshInfo.s_DefaultTangent;
        }
        this.triangles[index1] = num;
        this.triangles[index1 + 1] = num + 1;
        this.triangles[index1 + 2] = num + 2;
        this.triangles[index1 + 3] = num + 2;
        this.triangles[index1 + 4] = num + 3;
        this.triangles[index1 + 5] = num;
        num += 4;
        index1 += 6;
      }
      this.mesh.vertices = this.vertices;
      this.mesh.normals = this.normals;
      this.mesh.tangents = this.tangents;
      this.mesh.triangles = this.triangles;
      this.mesh.bounds = new Bounds(Vector3.zero, new Vector3(3840f, 2160f, 0.0f));
    }

    public void ResizeMeshInfo(int size)
    {
      int newSize1 = size * 4;
      int newSize2 = size * 6;
      int num1 = this.vertices.Length / 4;
      Array.Resize<Vector3>(ref this.vertices, newSize1);
      Array.Resize<Vector3>(ref this.normals, newSize1);
      Array.Resize<Vector4>(ref this.tangents, newSize1);
      Array.Resize<Vector2>(ref this.uvs0, newSize1);
      Array.Resize<Vector2>(ref this.uvs2, newSize1);
      Array.Resize<Color32>(ref this.colors32, newSize1);
      Array.Resize<int>(ref this.triangles, newSize2);
      Array.Resize<UIVertex>(ref this.uiVertices, newSize1);
      if (size <= num1)
        return;
      for (int index = num1; index < size; ++index)
      {
        int num2 = index * 4;
        int num3 = index * 6;
        this.normals[0 + num2] = TMP_MeshInfo.s_DefaultNormal;
        this.normals[1 + num2] = TMP_MeshInfo.s_DefaultNormal;
        this.normals[2 + num2] = TMP_MeshInfo.s_DefaultNormal;
        this.normals[3 + num2] = TMP_MeshInfo.s_DefaultNormal;
        this.tangents[0 + num2] = TMP_MeshInfo.s_DefaultTangent;
        this.tangents[1 + num2] = TMP_MeshInfo.s_DefaultTangent;
        this.tangents[2 + num2] = TMP_MeshInfo.s_DefaultTangent;
        this.tangents[3 + num2] = TMP_MeshInfo.s_DefaultTangent;
        this.triangles[0 + num3] = 0 + num2;
        this.triangles[1 + num3] = 1 + num2;
        this.triangles[2 + num3] = 2 + num2;
        this.triangles[3 + num3] = 2 + num2;
        this.triangles[4 + num3] = 3 + num2;
        this.triangles[5 + num3] = 0 + num2;
      }
      this.mesh.vertices = this.vertices;
      this.mesh.normals = this.normals;
      this.mesh.tangents = this.tangents;
      this.mesh.triangles = this.triangles;
    }

    public void Clear()
    {
      if (this.vertices == null)
        return;
      Array.Clear((Array) this.vertices, 0, this.vertices.Length);
      this.vertexCount = 0;
      if (!((UnityEngine.Object) this.mesh != (UnityEngine.Object) null))
        return;
      this.mesh.vertices = this.vertices;
    }

    public void Clear(bool uploadChanges)
    {
      if (this.vertices == null)
        return;
      Array.Clear((Array) this.vertices, 0, this.vertices.Length);
      this.vertexCount = 0;
      if (!uploadChanges || !((UnityEngine.Object) this.mesh != (UnityEngine.Object) null))
        return;
      this.mesh.vertices = this.vertices;
    }

    public void ClearUnusedVertices()
    {
      int length = this.vertices.Length - this.vertexCount;
      if (length <= 0)
        return;
      Array.Clear((Array) this.vertices, this.vertexCount, length);
    }

    public void ClearUnusedVertices(int startIndex)
    {
      int length = this.vertices.Length - startIndex;
      if (length <= 0)
        return;
      Array.Clear((Array) this.vertices, startIndex, length);
    }

    public void ClearUnusedVertices(int startIndex, bool updateMesh)
    {
      int length = this.vertices.Length - startIndex;
      if (length > 0)
        Array.Clear((Array) this.vertices, startIndex, length);
      if (!updateMesh || !((UnityEngine.Object) this.mesh != (UnityEngine.Object) null))
        return;
      this.mesh.vertices = this.vertices;
    }

    public UIVertex[] GetUiVertex()
    {
      int length = this.vertices.Length;
      if (this.uiVertices == null)
        this.uiVertices = new UIVertex[length];
      if (this.uiVertices.Length != length)
        Array.Resize<UIVertex>(ref this.uiVertices, length);
      for (int index = 0; index < length; ++index)
      {
        this.uiVertices[index].position = this.vertices[index];
        this.uiVertices[index].uv0 = this.uvs0[index];
        this.uiVertices[index].uv1 = this.uvs2[index];
        this.uiVertices[index].color = this.colors32[index];
        this.uiVertices[index].normal = this.normals[index];
        this.uiVertices[index].tangent = this.tangents[index];
      }
      return this.uiVertices;
    }
  }
}
