﻿// Decompiled with JetBrains decompiler
// Type: TMPro.CaretInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace TMPro
{
  public struct CaretInfo
  {
    public int index;
    public CaretPosition position;

    public CaretInfo(int index, CaretPosition position)
    {
      this.index = index;
      this.position = position;
    }
  }
}
