﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_LinkInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace TMPro
{
  public struct TMP_LinkInfo
  {
    public TMP_Text textComponent;
    public int hashCode;
    public int linkIdFirstCharacterIndex;
    public int linkIdLength;
    public int linkTextfirstCharacterIndex;
    public int linkTextLength;

    public string GetLinkText()
    {
      string str = string.Empty;
      TMP_TextInfo textInfo = this.textComponent.textInfo;
      for (int index = this.linkTextfirstCharacterIndex; index < this.linkTextfirstCharacterIndex + this.linkTextLength; ++index)
        str += (string) (object) textInfo.characterInfo[index].character;
      return str;
    }

    public string GetLinkID()
    {
      if ((Object) this.textComponent == (Object) null)
        return string.Empty;
      return this.textComponent.text.Substring(this.linkIdFirstCharacterIndex, this.linkIdLength);
    }
  }
}
