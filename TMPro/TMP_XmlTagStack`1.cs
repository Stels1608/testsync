﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_XmlTagStack`1
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace TMPro
{
  public struct TMP_XmlTagStack<T>
  {
    public T[] itemStack;
    public int index;

    public TMP_XmlTagStack(T[] tagStack)
    {
      this.itemStack = tagStack;
      this.index = 0;
    }

    public void Clear()
    {
      this.index = 0;
    }

    public void SetDefault(T item)
    {
      this.itemStack[0] = item;
      this.index = 1;
    }

    public void Add(T item)
    {
      if (this.index >= this.itemStack.Length)
        return;
      this.itemStack[this.index] = item;
      ++this.index;
    }

    public T Remove()
    {
      --this.index;
      if (this.index > 0)
        return this.itemStack[this.index - 1];
      this.index = 0;
      return this.itemStack[0];
    }
  }
}
