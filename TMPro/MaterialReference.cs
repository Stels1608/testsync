﻿// Decompiled with JetBrains decompiler
// Type: TMPro.MaterialReference
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace TMPro
{
  public struct MaterialReference
  {
    public int index;
    public TMP_FontAsset fontAsset;
    public TMP_SpriteAsset spriteAsset;
    public Material material;
    public bool isDefaultMaterial;
    public float padding;
    public int referenceCount;

    public MaterialReference(int index, TMP_FontAsset fontAsset, TMP_SpriteAsset spriteAsset, Material material, float padding)
    {
      this.index = index;
      this.fontAsset = fontAsset;
      this.spriteAsset = spriteAsset;
      this.material = material;
      this.isDefaultMaterial = material.GetInstanceID() == fontAsset.material.GetInstanceID();
      this.padding = padding;
      this.referenceCount = 0;
    }

    public static bool Contains(MaterialReference[] materialReferences, TMP_FontAsset fontAsset)
    {
      int instanceId = fontAsset.GetInstanceID();
      for (int index = 0; index < materialReferences.Length && (Object) materialReferences[index].fontAsset != (Object) null; ++index)
      {
        if (materialReferences[index].fontAsset.GetInstanceID() == instanceId)
          return true;
      }
      return false;
    }

    public static int AddMaterialReference(Material material, TMP_FontAsset fontAsset, MaterialReference[] materialReferences, Dictionary<int, int> materialReferenceIndexLookup)
    {
      int instanceId = material.GetInstanceID();
      int num = 0;
      if (materialReferenceIndexLookup.TryGetValue(instanceId, out num))
        return num;
      int count = materialReferenceIndexLookup.Count;
      materialReferenceIndexLookup[instanceId] = count;
      materialReferences[count].index = count;
      materialReferences[count].fontAsset = fontAsset;
      materialReferences[count].spriteAsset = (TMP_SpriteAsset) null;
      materialReferences[count].material = material;
      materialReferences[count].isDefaultMaterial = instanceId == fontAsset.material.GetInstanceID();
      materialReferences[count].referenceCount = 0;
      return count;
    }

    public static int AddMaterialReference(Material material, TMP_SpriteAsset spriteAsset, MaterialReference[] materialReferences, Dictionary<int, int> materialReferenceIndexLookup)
    {
      int instanceId = material.GetInstanceID();
      int num = 0;
      if (materialReferenceIndexLookup.TryGetValue(instanceId, out num))
        return num;
      int count = materialReferenceIndexLookup.Count;
      materialReferenceIndexLookup[instanceId] = count;
      materialReferences[count].index = count;
      materialReferences[count].fontAsset = materialReferences[0].fontAsset;
      materialReferences[count].spriteAsset = spriteAsset;
      materialReferences[count].material = material;
      materialReferences[count].isDefaultMaterial = true;
      materialReferences[count].referenceCount = 0;
      return count;
    }
  }
}
