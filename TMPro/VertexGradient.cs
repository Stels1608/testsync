﻿// Decompiled with JetBrains decompiler
// Type: TMPro.VertexGradient
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TMPro
{
  [Serializable]
  public struct VertexGradient
  {
    public Color topLeft;
    public Color topRight;
    public Color bottomLeft;
    public Color bottomRight;

    public VertexGradient(Color color)
    {
      this.topLeft = color;
      this.topRight = color;
      this.bottomLeft = color;
      this.bottomRight = color;
    }

    public VertexGradient(Color color0, Color color1, Color color2, Color color3)
    {
      this.topLeft = color0;
      this.topRight = color1;
      this.bottomLeft = color2;
      this.bottomRight = color3;
    }
  }
}
