﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TextMeshProUGUI
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.UI;

namespace TMPro
{
  [ExecuteInEditMode]
  [DisallowMultipleComponent]
  [RequireComponent(typeof (CanvasRenderer))]
  [AddComponentMenu("UI/TextMeshPro Text", 12)]
  [RequireComponent(typeof (RectTransform))]
  public class TextMeshProUGUI : TMP_Text, ILayoutElement
  {
    [SerializeField]
    private Vector2 m_uvOffset = Vector2.zero;
    [SerializeField]
    protected TMP_SubMeshUI[] m_subTextObjects = new TMP_SubMeshUI[8];
    private Vector3[] m_RectTransformCorners = new Vector3[4];
    private int m_max_characters = 8;
    private WordWrapState m_SavedWordWrapState = new WordWrapState();
    private WordWrapState m_SavedLineState = new WordWrapState();
    private Matrix4x4 m_EnvMapMatrix = new Matrix4x4();
    [SerializeField]
    private float m_uvLineOffset;
    [SerializeField]
    private bool m_hasFontAssetChanged;
    private Vector3 m_previousLossyScale;
    private CanvasRenderer m_canvasRenderer;
    private Canvas m_canvas;
    private bool m_isFirstAllocation;
    private bool m_isMaskingEnabled;
    [SerializeField]
    private Material m_baseMaterial;
    private bool m_isScrollRegionSet;
    private int m_stencilID;
    [SerializeField]
    private Vector4 m_maskOffset;
    private bool m_isAwake;
    [NonSerialized]
    private bool m_isRegisteredForEvents;
    private int m_recursiveCount;
    private int m_recursiveCountA;
    private int loopCountA;
    private bool m_isRebuildingLayout;

    public string textWithoutRelayouting
    {
      get
      {
        return this.m_text;
      }
      set
      {
        this.m_text = value;
        this.m_inputSource = TMP_Text.TextInputSources.Text;
        this.m_havePropertiesChanged = true;
        this.m_isInputParsingRequired = true;
        this.SetVerticesDirty();
      }
    }

    public override Material materialForRendering
    {
      get
      {
        if ((UnityEngine.Object) this.m_sharedMaterial == (UnityEngine.Object) null)
          return (Material) null;
        return this.GetModifiedMaterial(this.m_sharedMaterial);
      }
    }

    public override Mesh mesh
    {
      get
      {
        return this.m_mesh;
      }
    }

    public new CanvasRenderer canvasRenderer
    {
      get
      {
        if ((UnityEngine.Object) this.m_canvasRenderer == (UnityEngine.Object) null)
          this.m_canvasRenderer = this.GetComponent<CanvasRenderer>();
        return this.m_canvasRenderer;
      }
    }

    public InlineGraphicManager inlineGraphicManager
    {
      get
      {
        return this.m_inlineGraphics;
      }
    }

    public Bounds bounds
    {
      get
      {
        if ((UnityEngine.Object) this.m_mesh != (UnityEngine.Object) null)
          return this.m_mesh.bounds;
        return new Bounds();
      }
    }

    public Vector4 maskOffset
    {
      get
      {
        return this.m_maskOffset;
      }
      set
      {
        this.m_maskOffset = value;
        this.UpdateMask();
        this.m_havePropertiesChanged = true;
      }
    }

    protected override void Awake()
    {
      this.m_isAwake = true;
      this.m_canvas = this.canvas;
      this.m_isOrthographic = true;
      this.m_rectTransform = this.gameObject.GetComponent<RectTransform>();
      if ((UnityEngine.Object) this.m_rectTransform == (UnityEngine.Object) null)
        this.m_rectTransform = this.gameObject.AddComponent<RectTransform>();
      this.m_canvasRenderer = this.GetComponent<CanvasRenderer>();
      if ((UnityEngine.Object) this.m_canvasRenderer == (UnityEngine.Object) null)
        this.m_canvasRenderer = this.gameObject.AddComponent<CanvasRenderer>();
      if ((UnityEngine.Object) this.m_mesh == (UnityEngine.Object) null)
      {
        this.m_mesh = new Mesh();
        this.m_mesh.hideFlags = HideFlags.HideAndDontSave;
      }
      if ((UnityEngine.Object) this.m_settings == (UnityEngine.Object) null)
        this.m_settings = TMP_Settings.LoadDefaultSettings();
      if ((UnityEngine.Object) this.m_settings != (UnityEngine.Object) null)
      {
        if (this.m_text == null)
        {
          this.m_enableWordWrapping = this.m_settings.enableWordWrapping;
          this.m_enableKerning = this.m_settings.enableKerning;
          this.m_enableExtraPadding = this.m_settings.enableExtraPadding;
          this.m_tintAllSprites = this.m_settings.enableTintAllSprites;
        }
        this.m_warningsDisabled = this.m_settings.warningsDisabled;
      }
      this.LoadFontAsset();
      TMP_StyleSheet.LoadDefaultStyleSheet();
      this.m_char_buffer = new int[this.m_max_characters];
      this.m_cached_TextElement = (TMP_TextElement) new TMP_Glyph();
      this.m_isFirstAllocation = true;
      this.m_textInfo = new TMP_TextInfo((TMP_Text) this);
      if ((UnityEngine.Object) this.m_fontAsset == (UnityEngine.Object) null)
      {
        Debug.LogWarning((object) ("Please assign a Font Asset to this " + this.transform.name + " gameobject."), (UnityEngine.Object) this);
      }
      else
      {
        if ((double) this.m_fontSizeMin == 0.0)
          this.m_fontSizeMin = this.m_fontSize / 2f;
        if ((double) this.m_fontSizeMax == 0.0)
          this.m_fontSizeMax = this.m_fontSize * 2f;
        this.m_isInputParsingRequired = true;
        this.m_havePropertiesChanged = true;
        this.m_isCalculateSizeRequired = true;
      }
    }

    protected override void OnEnable()
    {
      if (!this.m_isRegisteredForEvents)
        this.m_isRegisteredForEvents = true;
      GraphicRegistry.RegisterGraphicForCanvas(this.canvas, (Graphic) this);
      if ((UnityEngine.Object) this.m_canvas == (UnityEngine.Object) null)
        this.m_canvas = this.canvas;
      this.ComputeMarginSize();
      this.m_verticesAlreadyDirty = false;
      this.m_layoutAlreadyDirty = false;
      this.m_ShouldRecalculate = true;
      this.SetAllDirty();
    }

    protected override void OnDisable()
    {
      if ((UnityEngine.Object) this.m_MaskMaterial != (UnityEngine.Object) null)
      {
        MaterialManager.ReleaseStencilMaterial(this.m_MaskMaterial);
        this.m_MaskMaterial = (Material) null;
      }
      GraphicRegistry.UnregisterGraphicForCanvas(this.canvas, (Graphic) this);
      CanvasUpdateRegistry.UnRegisterCanvasElementForRebuild((ICanvasElement) this);
      this.canvasRenderer.Clear();
      LayoutRebuilder.MarkLayoutForRebuild(this.m_rectTransform);
    }

    protected override void OnDestroy()
    {
      GraphicRegistry.UnregisterGraphicForCanvas(this.canvas, (Graphic) this);
      if ((UnityEngine.Object) this.m_mesh != (UnityEngine.Object) null)
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_mesh);
      if ((UnityEngine.Object) this.m_MaskMaterial != (UnityEngine.Object) null)
        MaterialManager.ReleaseStencilMaterial(this.m_MaskMaterial);
      this.m_isRegisteredForEvents = false;
    }

    protected override void LoadFontAsset()
    {
      ShaderUtilities.GetShaderPropertyIDs();
      if ((UnityEngine.Object) this.m_settings == (UnityEngine.Object) null)
        this.m_settings = TMP_Settings.LoadDefaultSettings();
      if ((UnityEngine.Object) this.m_fontAsset == (UnityEngine.Object) null)
      {
        if ((UnityEngine.Object) this.m_settings != (UnityEngine.Object) null && (UnityEngine.Object) this.m_settings.fontAsset != (UnityEngine.Object) null)
          this.m_fontAsset = this.m_settings.fontAsset;
        else
          this.m_fontAsset = Resources.Load("Fonts & Materials/ARIAL SDF", typeof (TMP_FontAsset)) as TMP_FontAsset;
        if ((UnityEngine.Object) this.m_fontAsset == (UnityEngine.Object) null)
        {
          Debug.LogWarning((object) ("The ARIAL SDF Font Asset was not found. There is no Font Asset assigned to " + this.gameObject.name + "."), (UnityEngine.Object) this);
          return;
        }
        if (this.m_fontAsset.characterDictionary == null)
          Debug.Log((object) "Dictionary is Null!");
        this.m_sharedMaterial = this.m_fontAsset.material;
      }
      else
      {
        if (this.m_fontAsset.characterDictionary == null)
          this.m_fontAsset.ReadFontDefinition();
        if ((UnityEngine.Object) this.m_sharedMaterial == (UnityEngine.Object) null && (UnityEngine.Object) this.m_baseMaterial != (UnityEngine.Object) null)
        {
          this.m_sharedMaterial = this.m_baseMaterial;
          this.m_baseMaterial = (Material) null;
        }
        if ((UnityEngine.Object) this.m_sharedMaterial == (UnityEngine.Object) null || (UnityEngine.Object) this.m_sharedMaterial.mainTexture == (UnityEngine.Object) null || this.m_fontAsset.atlas.GetInstanceID() != this.m_sharedMaterial.mainTexture.GetInstanceID())
        {
          if ((UnityEngine.Object) this.m_fontAsset.material == (UnityEngine.Object) null)
            Debug.LogWarning((object) ("The Font Atlas Texture of the Font Asset " + this.m_fontAsset.name + " assigned to " + this.gameObject.name + " is missing."), (UnityEngine.Object) this);
          else
            this.m_sharedMaterial = this.m_fontAsset.material;
        }
      }
      this.GetSpecialCharacters(this.m_fontAsset);
      this.m_padding = this.GetPaddingForMaterial();
      this.SetMaterialDirty();
    }

    private void UpdateEnvMapMatrix()
    {
      if (!this.m_sharedMaterial.HasProperty(ShaderUtilities.ID_EnvMap) || (UnityEngine.Object) this.m_sharedMaterial.GetTexture(ShaderUtilities.ID_EnvMap) == (UnityEngine.Object) null)
        return;
      this.m_EnvMapMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler((Vector3) this.m_sharedMaterial.GetVector(ShaderUtilities.ID_EnvMatrixRotation)), Vector3.one);
      this.m_sharedMaterial.SetMatrix(ShaderUtilities.ID_EnvMatrix, this.m_EnvMapMatrix);
    }

    private void EnableMasking()
    {
      if ((UnityEngine.Object) this.m_fontMaterial == (UnityEngine.Object) null)
      {
        this.m_fontMaterial = this.CreateMaterialInstance(this.m_sharedMaterial);
        this.m_canvasRenderer.SetMaterial(this.m_fontMaterial, this.m_sharedMaterial.mainTexture);
      }
      this.m_sharedMaterial = this.m_fontMaterial;
      if (this.m_sharedMaterial.HasProperty(ShaderUtilities.ID_ClipRect))
      {
        this.m_sharedMaterial.EnableKeyword(ShaderUtilities.Keyword_MASK_SOFT);
        this.m_sharedMaterial.DisableKeyword(ShaderUtilities.Keyword_MASK_HARD);
        this.m_sharedMaterial.DisableKeyword(ShaderUtilities.Keyword_MASK_TEX);
        this.UpdateMask();
      }
      this.m_isMaskingEnabled = true;
    }

    private void DisableMasking()
    {
      if ((UnityEngine.Object) this.m_fontMaterial != (UnityEngine.Object) null)
      {
        if (this.m_stencilID > 0)
          this.m_sharedMaterial = this.m_MaskMaterial;
        this.m_canvasRenderer.SetMaterial(this.m_sharedMaterial, this.m_sharedMaterial.mainTexture);
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_fontMaterial);
      }
      this.m_isMaskingEnabled = false;
    }

    private void UpdateMask()
    {
      if (!((UnityEngine.Object) this.m_rectTransform != (UnityEngine.Object) null))
        return;
      if (!ShaderUtilities.isInitialized)
        ShaderUtilities.GetShaderPropertyIDs();
      this.m_isScrollRegionSet = true;
      float num1 = Mathf.Min(Mathf.Min(this.m_margin.x, this.m_margin.z), this.m_sharedMaterial.GetFloat(ShaderUtilities.ID_MaskSoftnessX));
      float num2 = Mathf.Min(Mathf.Min(this.m_margin.y, this.m_margin.w), this.m_sharedMaterial.GetFloat(ShaderUtilities.ID_MaskSoftnessY));
      float num3 = (double) num1 <= 0.0 ? 0.0f : num1;
      float num4 = (double) num2 <= 0.0 ? 0.0f : num2;
      float z = (float) (((double) this.m_rectTransform.rect.width - (double) Mathf.Max(this.m_margin.x, 0.0f) - (double) Mathf.Max(this.m_margin.z, 0.0f)) / 2.0) + num3;
      float w = (float) (((double) this.m_rectTransform.rect.height - (double) Mathf.Max(this.m_margin.y, 0.0f) - (double) Mathf.Max(this.m_margin.w, 0.0f)) / 2.0) + num4;
      Vector2 vector2 = (Vector2) (this.m_rectTransform.localPosition + new Vector3((float) ((0.5 - (double) this.m_rectTransform.pivot.x) * (double) this.m_rectTransform.rect.width + ((double) Mathf.Max(this.m_margin.x, 0.0f) - (double) Mathf.Max(this.m_margin.z, 0.0f)) / 2.0), (float) ((0.5 - (double) this.m_rectTransform.pivot.y) * (double) this.m_rectTransform.rect.height + (-(double) Mathf.Max(this.m_margin.y, 0.0f) + (double) Mathf.Max(this.m_margin.w, 0.0f)) / 2.0)));
      Vector4 vector = new Vector4(vector2.x, vector2.y, z, w);
      this.m_sharedMaterial.SetVector(ShaderUtilities.ID_ClipRect, vector);
    }

    protected override Material GetMaterial(Material mat)
    {
      ShaderUtilities.GetShaderPropertyIDs();
      if ((UnityEngine.Object) this.m_fontMaterial == (UnityEngine.Object) null || this.m_fontMaterial.GetInstanceID() != mat.GetInstanceID())
        this.m_fontMaterial = this.CreateMaterialInstance(mat);
      this.m_sharedMaterial = this.m_fontMaterial;
      this.m_padding = this.GetPaddingForMaterial();
      this.SetVerticesDirty();
      this.SetMaterialDirty();
      return this.m_sharedMaterial;
    }

    protected override Material[] GetMaterials(Material[] mats)
    {
      int size = this.m_textInfo.materialCount;
      if (this.m_fontMaterials == null)
        this.m_fontMaterials = new Material[size];
      else if (this.m_fontMaterials.Length != size)
        TMP_TextInfo.Resize<Material>(ref this.m_fontMaterials, size, false);
      for (int index = 0; index < size; ++index)
      {
        if (index == 0)
          this.m_fontMaterials[index] = this.fontMaterial;
        else
          this.m_fontMaterials[index] = this.m_subTextObjects[index].material;
      }
      this.m_fontSharedMaterials = this.m_fontMaterials;
      return this.m_fontMaterials;
    }

    protected override void SetSharedMaterial(Material mat)
    {
      this.m_sharedMaterial = mat;
      this.m_padding = this.GetPaddingForMaterial();
      this.SetMaterialDirty();
    }

    protected override Material[] GetSharedMaterials()
    {
      int size = this.m_textInfo.materialCount;
      if (this.m_fontSharedMaterials == null)
        this.m_fontSharedMaterials = new Material[size];
      else if (this.m_fontSharedMaterials.Length != size)
        TMP_TextInfo.Resize<Material>(ref this.m_fontSharedMaterials, size, false);
      for (int index = 0; index < size; ++index)
      {
        if (index == 0)
          this.m_fontSharedMaterials[index] = this.m_sharedMaterial;
        else
          this.m_fontSharedMaterials[index] = this.m_subTextObjects[index].sharedMaterial;
      }
      return this.m_fontSharedMaterials;
    }

    protected override void SetSharedMaterials(Material[] materials)
    {
      int size = this.m_textInfo.materialCount;
      if (this.m_fontSharedMaterials == null)
        this.m_fontSharedMaterials = new Material[size];
      else if (this.m_fontSharedMaterials.Length != size)
        TMP_TextInfo.Resize<Material>(ref this.m_fontSharedMaterials, size, false);
      for (int index = 0; index < size; ++index)
      {
        if (index == 0)
        {
          if (!((UnityEngine.Object) materials[index].mainTexture == (UnityEngine.Object) null) && materials[index].mainTexture.GetInstanceID() == this.m_sharedMaterial.mainTexture.GetInstanceID())
          {
            this.m_sharedMaterial = this.m_fontSharedMaterials[index] = materials[index];
            this.m_padding = this.GetPaddingForMaterial(this.m_sharedMaterial);
          }
        }
        else if (!((UnityEngine.Object) materials[index].mainTexture == (UnityEngine.Object) null) && materials[index].mainTexture.GetInstanceID() == this.m_subTextObjects[index].sharedMaterial.mainTexture.GetInstanceID() && this.m_subTextObjects[index].isDefaultMaterial)
          this.m_subTextObjects[index].sharedMaterial = this.m_fontSharedMaterials[index] = materials[index];
      }
    }

    protected override void SetOutlineThickness(float thickness)
    {
      if ((UnityEngine.Object) this.m_fontMaterial != (UnityEngine.Object) null && this.m_sharedMaterial.GetInstanceID() != this.m_fontMaterial.GetInstanceID())
      {
        this.m_sharedMaterial = this.m_fontMaterial;
        this.m_canvasRenderer.SetMaterial(this.m_sharedMaterial, this.m_sharedMaterial.mainTexture);
      }
      else if ((UnityEngine.Object) this.m_fontMaterial == (UnityEngine.Object) null)
      {
        this.m_fontMaterial = this.CreateMaterialInstance(this.m_sharedMaterial);
        this.m_sharedMaterial = this.m_fontMaterial;
        this.m_canvasRenderer.SetMaterial(this.m_sharedMaterial, this.m_sharedMaterial.mainTexture);
      }
      thickness = Mathf.Clamp01(thickness);
      this.m_sharedMaterial.SetFloat(ShaderUtilities.ID_OutlineWidth, thickness);
      this.m_padding = this.GetPaddingForMaterial();
    }

    protected override void SetFaceColor(Color32 color)
    {
      if ((UnityEngine.Object) this.m_fontMaterial == (UnityEngine.Object) null)
        this.m_fontMaterial = this.CreateMaterialInstance(this.m_sharedMaterial);
      this.m_sharedMaterial = this.m_fontMaterial;
      this.m_padding = this.GetPaddingForMaterial();
      this.m_sharedMaterial.SetColor(ShaderUtilities.ID_FaceColor, (Color) color);
    }

    protected override void SetOutlineColor(Color32 color)
    {
      if ((UnityEngine.Object) this.m_fontMaterial == (UnityEngine.Object) null)
        this.m_fontMaterial = this.CreateMaterialInstance(this.m_sharedMaterial);
      this.m_sharedMaterial = this.m_fontMaterial;
      this.m_padding = this.GetPaddingForMaterial();
      this.m_sharedMaterial.SetColor(ShaderUtilities.ID_OutlineColor, (Color) color);
    }

    protected override void SetShaderDepth()
    {
      if ((UnityEngine.Object) this.m_canvas == (UnityEngine.Object) null || (UnityEngine.Object) this.m_sharedMaterial == (UnityEngine.Object) null)
        return;
      if (this.m_canvas.renderMode == RenderMode.ScreenSpaceOverlay || this.m_isOverlay)
        this.m_sharedMaterial.SetFloat(ShaderUtilities.ShaderTag_ZTestMode, 0.0f);
      else
        this.m_sharedMaterial.SetFloat(ShaderUtilities.ShaderTag_ZTestMode, 4f);
    }

    protected override void SetCulling()
    {
      if (this.m_isCullingEnabled)
        this.m_canvasRenderer.GetMaterial().SetFloat("_CullMode", 2f);
      else
        this.m_canvasRenderer.GetMaterial().SetFloat("_CullMode", 0.0f);
    }

    private void SetPerspectiveCorrection()
    {
      if (this.m_isOrthographic)
        this.m_sharedMaterial.SetFloat(ShaderUtilities.ID_PerspectiveFilter, 0.0f);
      else
        this.m_sharedMaterial.SetFloat(ShaderUtilities.ID_PerspectiveFilter, 0.875f);
    }

    protected override float GetPaddingForMaterial(Material mat)
    {
      this.m_padding = ShaderUtilities.GetPadding(mat, this.m_enableExtraPadding, this.m_isUsingBold);
      this.m_isMaskingEnabled = ShaderUtilities.IsMaskingEnabled(this.m_sharedMaterial);
      this.m_isSDFShader = mat.HasProperty(ShaderUtilities.ID_WeightNormal);
      return this.m_padding;
    }

    protected override float GetPaddingForMaterial()
    {
      ShaderUtilities.GetShaderPropertyIDs();
      this.m_padding = ShaderUtilities.GetPadding(this.m_sharedMaterial, this.m_enableExtraPadding, this.m_isUsingBold);
      this.m_isMaskingEnabled = ShaderUtilities.IsMaskingEnabled(this.m_sharedMaterial);
      this.m_isSDFShader = this.m_sharedMaterial.HasProperty(ShaderUtilities.ID_WeightNormal);
      return this.m_padding;
    }

    private void SetMeshArrays(int size)
    {
      this.m_textInfo.meshInfo[0].ResizeMeshInfo(size);
      UIVertex[] uiVertex = this.m_textInfo.meshInfo[0].GetUiVertex();
      this.m_canvasRenderer.SetVertices(uiVertex, uiVertex.Length);
    }

    protected override int SetArraySizes(int[] chars)
    {
      int endIndex = 0;
      int num1 = 0;
      this.m_totalCharacterCount = 0;
      this.m_isUsingBold = false;
      this.m_isParsingText = false;
      this.m_style = this.m_fontStyle;
      this.m_currentFontAsset = this.m_fontAsset;
      this.m_currentMaterial = this.m_sharedMaterial;
      this.m_currentMaterialIndex = 0;
      this.m_materialReferenceStack.SetDefault(new MaterialReference(0, this.m_currentFontAsset, (TMP_SpriteAsset) null, this.m_currentMaterial, this.m_padding));
      this.m_materialReferenceIndexLookup.Clear();
      MaterialReference.AddMaterialReference(this.m_currentMaterial, this.m_currentFontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
      if (this.m_textInfo == null)
        this.m_textInfo = new TMP_TextInfo();
      this.m_textElementType = TMP_TextElementType.Character;
      for (int index1 = 0; chars[index1] != 0; ++index1)
      {
        if (this.m_textInfo.characterInfo == null || this.m_totalCharacterCount >= this.m_textInfo.characterInfo.Length)
          TMP_TextInfo.Resize<TMP_CharacterInfo>(ref this.m_textInfo.characterInfo, this.m_totalCharacterCount + 1, true);
        int key = chars[index1];
        if (this.m_isRichText && key == 60)
        {
          int num2 = this.m_currentMaterialIndex;
          if (this.ValidateHtmlTag(chars, index1 + 1, out endIndex))
          {
            index1 = endIndex;
            if ((this.m_style & FontStyles.Bold) == FontStyles.Bold)
              this.m_isUsingBold = true;
            if (this.m_textElementType == TMP_TextElementType.Sprite)
            {
              ++this.m_materialReferences[this.m_currentMaterialIndex].referenceCount;
              this.m_textInfo.characterInfo[this.m_totalCharacterCount].character = (char) (57344 + this.m_spriteIndex);
              this.m_textInfo.characterInfo[this.m_totalCharacterCount].fontAsset = this.m_currentFontAsset;
              this.m_textInfo.characterInfo[this.m_totalCharacterCount].materialReferenceIndex = this.m_currentMaterialIndex;
              this.m_textElementType = TMP_TextElementType.Character;
              this.m_currentMaterialIndex = num2;
              ++num1;
              ++this.m_totalCharacterCount;
              continue;
            }
            continue;
          }
        }
        bool flag1 = false;
        bool flag2 = false;
        TMP_FontAsset tmpFontAsset = this.m_currentFontAsset;
        Material material = this.m_currentMaterial;
        int num3 = this.m_currentMaterialIndex;
        if (this.m_textElementType == TMP_TextElementType.Character)
        {
          if ((this.m_style & FontStyles.UpperCase) == FontStyles.UpperCase)
          {
            if (char.IsLower((char) key))
              key = (int) char.ToUpper((char) key);
          }
          else if ((this.m_style & FontStyles.LowerCase) == FontStyles.LowerCase)
          {
            if (char.IsUpper((char) key))
              key = (int) char.ToLower((char) key);
          }
          else if (((this.m_fontStyle & FontStyles.SmallCaps) == FontStyles.SmallCaps || (this.m_style & FontStyles.SmallCaps) == FontStyles.SmallCaps) && char.IsLower((char) key))
            key = (int) char.ToUpper((char) key);
        }
        TMP_Glyph tmpGlyph;
        this.m_currentFontAsset.characterDictionary.TryGetValue(key, out tmpGlyph);
        if (tmpGlyph == null)
        {
          if ((UnityEngine.Object) this.m_settings != (UnityEngine.Object) null && this.m_settings.fallbackFontAssets != null)
          {
            for (int index2 = 0; index2 < this.m_settings.fallbackFontAssets.Count; ++index2)
            {
              TMP_FontAsset fontAsset = this.m_settings.fallbackFontAssets[index2];
              if (!((UnityEngine.Object) fontAsset == (UnityEngine.Object) null) && fontAsset.characterDictionary.TryGetValue(key, out tmpGlyph))
              {
                flag1 = true;
                this.m_currentFontAsset = fontAsset;
                this.m_currentMaterial = fontAsset.material;
                this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentMaterial, fontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
                break;
              }
            }
          }
          if (tmpGlyph == null)
          {
            if (char.IsLower((char) key))
            {
              if (this.m_currentFontAsset.characterDictionary.TryGetValue((int) char.ToUpper((char) key), out tmpGlyph))
                key = chars[index1] = (int) char.ToUpper((char) key);
            }
            else if (char.IsUpper((char) key) && this.m_currentFontAsset.characterDictionary.TryGetValue((int) char.ToLower((char) key), out tmpGlyph))
              key = chars[index1] = (int) char.ToLower((char) key);
          }
          if (tmpGlyph == null)
          {
            this.m_currentFontAsset.characterDictionary.TryGetValue(9633, out tmpGlyph);
            if (tmpGlyph != null)
            {
              if (!this.m_warningsDisabled)
                Debug.LogWarning((object) ("Character with ASCII value of " + (object) key + " was not found in the Font Asset Glyph Table."), (UnityEngine.Object) this);
              key = chars[index1] = 9633;
            }
            else
            {
              TMP_FontAsset defaultFontAsset = TMP_FontAsset.defaultFontAsset;
              if ((UnityEngine.Object) defaultFontAsset != (UnityEngine.Object) null && defaultFontAsset.characterDictionary.TryGetValue(9633, out tmpGlyph))
              {
                if (!this.m_warningsDisabled)
                  Debug.LogWarning((object) ("Character with ASCII value of " + (object) key + " was not found in the Font Asset Glyph Table."), (UnityEngine.Object) this);
                key = chars[index1] = 9633;
                flag1 = true;
                this.m_currentFontAsset = defaultFontAsset;
                this.m_currentMaterial = defaultFontAsset.material;
                this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentMaterial, defaultFontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
              }
            }
          }
        }
        this.m_textInfo.characterInfo[this.m_totalCharacterCount].textElement = (TMP_TextElement) tmpGlyph;
        this.m_textInfo.characterInfo[this.m_totalCharacterCount].character = (char) key;
        this.m_textInfo.characterInfo[this.m_totalCharacterCount].fontAsset = this.m_currentFontAsset;
        this.m_textInfo.characterInfo[this.m_totalCharacterCount].material = this.m_currentMaterial;
        this.m_textInfo.characterInfo[this.m_totalCharacterCount].materialReferenceIndex = this.m_currentMaterialIndex;
        if (!char.IsWhiteSpace((char) key))
        {
          ++this.m_materialReferences[this.m_currentMaterialIndex].referenceCount;
          if (flag2)
            this.m_currentMaterialIndex = num3;
          if (flag1)
          {
            this.m_currentFontAsset = tmpFontAsset;
            this.m_currentMaterial = material;
            this.m_currentMaterialIndex = num3;
          }
        }
        ++this.m_totalCharacterCount;
      }
      this.m_textInfo.spriteCount = num1;
      int size = this.m_textInfo.materialCount = this.m_materialReferenceIndexLookup.Count;
      if (size > this.m_textInfo.meshInfo.Length)
        TMP_TextInfo.Resize<TMP_MeshInfo>(ref this.m_textInfo.meshInfo, size, false);
      for (int index = 0; index < size; ++index)
      {
        if (index > 0)
        {
          if ((UnityEngine.Object) this.m_subTextObjects[index] == (UnityEngine.Object) null)
          {
            this.m_subTextObjects[index] = TMP_SubMeshUI.AddSubTextObject(this, this.m_materialReferences[index]);
            this.m_textInfo.meshInfo[index].vertices = (Vector3[]) null;
          }
          if (this.m_rectTransform.pivot != this.m_subTextObjects[index].rectTransform.pivot)
            this.m_subTextObjects[index].rectTransform.pivot = this.m_rectTransform.pivot;
          if ((UnityEngine.Object) this.m_subTextObjects[index].sharedMaterial == (UnityEngine.Object) null || this.m_subTextObjects[index].sharedMaterial.GetInstanceID() != this.m_materialReferences[index].material.GetInstanceID())
          {
            bool flag = this.m_materialReferences[index].isDefaultMaterial;
            this.m_subTextObjects[index].isDefaultMaterial = flag;
            if (!flag || (UnityEngine.Object) this.m_subTextObjects[index].sharedMaterial == (UnityEngine.Object) null || this.m_subTextObjects[index].sharedMaterial.mainTexture.GetInstanceID() != this.m_materialReferences[index].material.mainTexture.GetInstanceID())
              this.m_subTextObjects[index].sharedMaterial = this.m_materialReferences[index].material;
          }
        }
        int num2 = this.m_materialReferences[index].referenceCount;
        if (this.m_textInfo.meshInfo[index].vertices == null || this.m_textInfo.meshInfo[index].vertices.Length < num2 * 4)
        {
          if (this.m_textInfo.meshInfo[index].vertices == null)
          {
            if (index == 0)
              this.m_textInfo.meshInfo[index] = new TMP_MeshInfo(this.m_mesh, num2 + 1);
            else
              this.m_textInfo.meshInfo[index] = new TMP_MeshInfo(this.m_subTextObjects[index].mesh, num2 + 1);
          }
          else
            this.m_textInfo.meshInfo[index].ResizeMeshInfo(num2 <= 1024 ? Mathf.NextPowerOfTwo(num2) : num2 + 256);
        }
      }
      for (int index = size; index < this.m_subTextObjects.Length + 1 && (UnityEngine.Object) this.m_subTextObjects[index] != (UnityEngine.Object) null; ++index)
      {
        if (index < this.m_textInfo.meshInfo.Length)
          this.m_subTextObjects[index].canvasRenderer.SetVertices((UIVertex[]) null, 0);
      }
      return this.m_totalCharacterCount;
    }

    protected override void ComputeMarginSize()
    {
      if (!((UnityEngine.Object) this.rectTransform != (UnityEngine.Object) null))
        return;
      this.m_marginWidth = this.m_rectTransform.rect.width - this.m_margin.x - this.m_margin.z;
      this.m_marginHeight = this.m_rectTransform.rect.height - this.m_margin.y - this.m_margin.w;
      this.m_RectTransformCorners = this.GetTextContainerLocalCorners();
    }

    protected override void OnDidApplyAnimationProperties()
    {
      this.m_havePropertiesChanged = true;
      this.SetVerticesDirty();
      this.SetLayoutDirty();
    }

    protected override void OnTransformParentChanged()
    {
      base.OnTransformParentChanged();
      this.m_canvas = this.canvas;
      this.ComputeMarginSize();
      this.m_havePropertiesChanged = true;
    }

    protected override void OnRectTransformDimensionsChange()
    {
      if (!this.gameObject.activeInHierarchy)
        return;
      this.ComputeMarginSize();
      this.UpdateSubObjectPivot();
      this.SetVerticesDirty();
      this.SetLayoutDirty();
    }

    private void LateUpdate()
    {
      if (!this.m_rectTransform.hasChanged)
        return;
      Vector3 lossyScale = this.m_rectTransform.lossyScale;
      if (!this.m_havePropertiesChanged && (double) lossyScale.y != (double) this.m_previousLossyScale.y && (this.m_text != string.Empty && this.m_text != null))
      {
        this.UpdateSDFScale(lossyScale.y);
        this.m_previousLossyScale = lossyScale;
      }
      this.m_rectTransform.hasChanged = false;
    }

    private void OnPreRenderCanvas()
    {
      if (!this.IsActive())
        return;
      if ((UnityEngine.Object) this.m_canvas == (UnityEngine.Object) null)
      {
        this.m_canvas = this.canvas;
        if ((UnityEngine.Object) this.m_canvas == (UnityEngine.Object) null)
          return;
      }
      this.loopCountA = 0;
      if (!this.m_havePropertiesChanged && !this.m_isLayoutDirty)
        return;
      if (this.checkPaddingRequired)
        this.UpdateMeshPadding();
      if (this.m_isInputParsingRequired || this.m_isTextTruncated)
        this.ParseInputText();
      if (this.m_enableAutoSizing)
        this.m_fontSize = Mathf.Clamp(this.m_fontSize, this.m_fontSizeMin, this.m_fontSizeMax);
      this.m_maxFontSize = this.m_fontSizeMax;
      this.m_minFontSize = this.m_fontSizeMin;
      this.m_lineSpacingDelta = 0.0f;
      this.m_charWidthAdjDelta = 0.0f;
      this.m_recursiveCount = 0;
      this.m_isCharacterWrappingEnabled = false;
      this.m_isTextTruncated = false;
      this.m_isLayoutDirty = false;
      this.GenerateTextMesh();
      this.m_havePropertiesChanged = false;
    }

    protected override void GenerateTextMesh()
    {
      if ((UnityEngine.Object) this.m_fontAsset == (UnityEngine.Object) null || this.m_fontAsset.characterDictionary == null)
      {
        Debug.LogWarning((object) ("Can't Generate Mesh! No Font Asset has been assigned to Object ID: " + (object) this.GetInstanceID()));
      }
      else
      {
        if (this.m_textInfo != null)
          this.m_textInfo.Clear();
        if (this.m_char_buffer == null || this.m_char_buffer.Length == 0 || this.m_char_buffer[0] == 0)
        {
          this.ClearMesh();
          this.m_preferredWidth = 0.0f;
          this.m_preferredHeight = 0.0f;
        }
        else
        {
          this.m_currentFontAsset = this.m_fontAsset;
          this.m_currentMaterial = this.m_sharedMaterial;
          this.m_currentMaterialIndex = 0;
          this.m_materialReferenceStack.SetDefault(new MaterialReference(0, this.m_currentFontAsset, (TMP_SpriteAsset) null, this.m_currentMaterial, this.m_padding));
          this.m_currentSpriteAsset = this.m_spriteAsset;
          int num1 = this.m_totalCharacterCount;
          this.m_fontScale = this.m_fontSize / this.m_currentFontAsset.fontInfo.PointSize;
          float num2 = this.m_fontSize / this.m_fontAsset.fontInfo.PointSize * this.m_fontAsset.fontInfo.Scale;
          float num3 = this.m_fontScale;
          this.m_fontScaleMultiplier = 1f;
          this.m_currentFontSize = this.m_fontSize;
          this.m_sizeStack.SetDefault(this.m_currentFontSize);
          this.m_style = this.m_fontStyle;
          this.m_lineJustification = this.m_textAlignment;
          float padding = 0.0f;
          float num4 = 1f;
          this.m_baselineOffset = 0.0f;
          bool flag1 = false;
          Vector3 start1 = Vector3.zero;
          Vector3 end1 = Vector3.zero;
          bool flag2 = false;
          Vector3 start2 = Vector3.zero;
          Vector3 end2 = Vector3.zero;
          this.m_fontColor32 = (Color32) this.m_fontColor;
          this.m_htmlColor = this.m_fontColor32;
          this.m_colorStack.SetDefault(this.m_htmlColor);
          this.m_styleStack.Clear();
          this.m_lineOffset = 0.0f;
          this.m_lineHeight = 0.0f;
          float num5 = this.m_currentFontAsset.fontInfo.LineHeight - (this.m_currentFontAsset.fontInfo.Ascender - this.m_currentFontAsset.fontInfo.Descender);
          this.m_cSpacing = 0.0f;
          this.m_monoSpacing = 0.0f;
          this.m_xAdvance = 0.0f;
          this.tag_LineIndent = 0.0f;
          this.tag_Indent = 0.0f;
          this.m_indentStack.SetDefault(0.0f);
          this.tag_NoParsing = false;
          this.m_characterCount = 0;
          this.m_visibleCharacterCount = 0;
          this.m_firstCharacterOfLine = 0;
          this.m_lastCharacterOfLine = 0;
          this.m_firstVisibleCharacterOfLine = 0;
          this.m_lastVisibleCharacterOfLine = 0;
          this.m_maxLineAscender = float.NegativeInfinity;
          this.m_maxLineDescender = float.PositiveInfinity;
          this.m_lineNumber = 0;
          bool flag3 = true;
          this.m_pageNumber = 0;
          int index1 = Mathf.Clamp(this.m_pageToDisplay - 1, 0, this.m_textInfo.pageInfo.Length - 1);
          int index2 = 0;
          Vector4 vector4 = this.m_margin;
          float num6 = this.m_marginWidth;
          float num7 = this.m_marginHeight;
          this.m_marginLeft = 0.0f;
          this.m_marginRight = 0.0f;
          this.m_width = -1f;
          this.m_meshExtents.min = TMP_Text.k_InfinityVectorPositive;
          this.m_meshExtents.max = TMP_Text.k_InfinityVectorNegative;
          this.m_textInfo.ClearLineInfo();
          this.m_maxAscender = 0.0f;
          this.m_maxDescender = 0.0f;
          float num8 = 0.0f;
          float num9 = 0.0f;
          bool flag4 = false;
          this.m_isNewPage = false;
          bool flag5 = true;
          bool flag6 = false;
          int num10 = 0;
          ++this.loopCountA;
          int endIndex = 0;
          for (int index3 = 0; this.m_char_buffer[index3] != 0; ++index3)
          {
            int num11 = this.m_char_buffer[index3];
            this.m_textElementType = TMP_TextElementType.Character;
            this.m_currentMaterialIndex = this.m_textInfo.characterInfo[this.m_characterCount].materialReferenceIndex;
            this.m_currentFontAsset = this.m_materialReferences[this.m_currentMaterialIndex].fontAsset;
            int num12 = this.m_currentMaterialIndex;
            if (this.m_isRichText && num11 == 60)
            {
              this.m_isParsingText = true;
              if (this.ValidateHtmlTag(this.m_char_buffer, index3 + 1, out endIndex))
              {
                index3 = endIndex;
                if (this.m_textElementType == TMP_TextElementType.Character)
                  continue;
              }
            }
            this.m_isParsingText = false;
            bool flag7 = false;
            bool flag8 = false;
            float num13 = 1f;
            if (this.m_textElementType == TMP_TextElementType.Character)
            {
              if ((this.m_style & FontStyles.UpperCase) == FontStyles.UpperCase)
              {
                if (char.IsLower((char) num11))
                  num11 = (int) char.ToUpper((char) num11);
              }
              else if ((this.m_style & FontStyles.LowerCase) == FontStyles.LowerCase)
              {
                if (char.IsUpper((char) num11))
                  num11 = (int) char.ToLower((char) num11);
              }
              else if (((this.m_fontStyle & FontStyles.SmallCaps) == FontStyles.SmallCaps || (this.m_style & FontStyles.SmallCaps) == FontStyles.SmallCaps) && char.IsLower((char) num11))
              {
                num13 = 0.8f;
                num11 = (int) char.ToUpper((char) num11);
              }
            }
            if (this.m_textElementType == TMP_TextElementType.Sprite)
            {
              TMP_Sprite tmpSprite = this.m_currentSpriteAsset.spriteInfoList[this.m_spriteIndex];
              num11 = 57344 + this.m_spriteIndex;
              this.m_currentFontAsset = this.m_fontAsset;
              float num14 = this.m_currentFontSize / this.m_fontAsset.fontInfo.PointSize * this.m_fontAsset.fontInfo.Scale;
              num3 = this.m_fontAsset.fontInfo.Ascender / tmpSprite.height * tmpSprite.scale * num14;
              this.m_cached_TextElement = (TMP_TextElement) tmpSprite;
              this.m_textInfo.characterInfo[this.m_characterCount].elementType = TMP_TextElementType.Sprite;
              this.m_textInfo.characterInfo[this.m_characterCount].spriteAsset = this.m_currentSpriteAsset;
              this.m_textInfo.characterInfo[this.m_characterCount].fontAsset = this.m_currentFontAsset;
              this.m_textInfo.characterInfo[this.m_characterCount].materialReferenceIndex = this.m_currentMaterialIndex;
              this.m_currentMaterialIndex = num12;
              padding = 0.0f;
            }
            else if (this.m_textElementType == TMP_TextElementType.Character)
            {
              this.m_cached_TextElement = this.m_textInfo.characterInfo[this.m_characterCount].textElement;
              this.m_currentFontAsset = this.m_textInfo.characterInfo[this.m_characterCount].fontAsset;
              this.m_currentMaterial = this.m_textInfo.characterInfo[this.m_characterCount].material;
              this.m_currentMaterialIndex = this.m_textInfo.characterInfo[this.m_characterCount].materialReferenceIndex;
              this.m_fontScale = this.m_currentFontSize * num13 / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale;
              num3 = this.m_fontScale * this.m_fontScaleMultiplier;
              this.m_textInfo.characterInfo[this.m_characterCount].elementType = TMP_TextElementType.Character;
              padding = this.m_currentMaterialIndex != 0 ? this.m_subTextObjects[this.m_currentMaterialIndex].padding : this.m_padding;
            }
            if (this.m_isRightToLeft)
              this.m_xAdvance -= (float) ((((double) this.m_cached_TextElement.xAdvance * (double) num4 + (double) this.m_characterSpacing + (double) this.m_currentFontAsset.normalSpacingOffset) * (double) num3 + (double) this.m_cSpacing) * (1.0 - (double) this.m_charWidthAdjDelta));
            this.m_textInfo.characterInfo[this.m_characterCount].character = (char) num11;
            this.m_textInfo.characterInfo[this.m_characterCount].pointSize = this.m_currentFontSize;
            this.m_textInfo.characterInfo[this.m_characterCount].color = this.m_htmlColor;
            this.m_textInfo.characterInfo[this.m_characterCount].style = this.m_style;
            this.m_textInfo.characterInfo[this.m_characterCount].index = (short) index3;
            if (this.m_enableKerning && this.m_characterCount >= 1)
            {
              KerningPair kerningPair;
              this.m_currentFontAsset.kerningDictionary.TryGetValue(new KerningPairKey((int) this.m_textInfo.characterInfo[this.m_characterCount - 1].character, num11).key, out kerningPair);
              if (kerningPair != null)
                this.m_xAdvance += (float) ((double) kerningPair.XadvanceOffset * (double) num3);
            }
            float num15 = 0.0f;
            if ((double) this.m_monoSpacing != 0.0)
            {
              num15 = (float) (((double) this.m_monoSpacing / 2.0 - ((double) this.m_cached_TextElement.width / 2.0 + (double) this.m_cached_TextElement.xOffset) * (double) num3) * (1.0 - (double) this.m_charWidthAdjDelta));
              this.m_xAdvance += (float) (double) num15;
            }
            float style_padding;
            if (!flag8 && (this.m_style & FontStyles.Bold) == FontStyles.Bold || (this.m_fontStyle & FontStyles.Bold) == FontStyles.Bold)
            {
              style_padding = this.m_currentFontAsset.boldStyle * 2f;
              num4 = (float) (1.0 + (double) this.m_currentFontAsset.boldSpacing * 0.00999999977648258);
            }
            else
            {
              style_padding = this.m_currentFontAsset.normalStyle * 2f;
              num4 = 1f;
            }
            float num16 = this.m_currentFontAsset.fontInfo.Baseline;
            Vector3 vector3_1;
            vector3_1.x = this.m_xAdvance + (float) (((double) this.m_cached_TextElement.xOffset - (double) padding - (double) style_padding) * (double) num3 * (1.0 - (double) this.m_charWidthAdjDelta));
            vector3_1.y = (num16 + this.m_cached_TextElement.yOffset + padding) * num3 - this.m_lineOffset + this.m_baselineOffset;
            vector3_1.z = 0.0f;
            Vector3 vector3_2;
            vector3_2.x = vector3_1.x;
            vector3_2.y = vector3_1.y - (this.m_cached_TextElement.height + padding * 2f) * num3;
            vector3_2.z = 0.0f;
            Vector3 vector3_3;
            vector3_3.x = vector3_2.x + (float) (((double) this.m_cached_TextElement.width + (double) padding * 2.0 + (double) style_padding * 2.0) * (double) num3 * (1.0 - (double) this.m_charWidthAdjDelta));
            vector3_3.y = vector3_1.y;
            vector3_3.z = 0.0f;
            Vector3 vector3_4;
            vector3_4.x = vector3_3.x;
            vector3_4.y = vector3_2.y;
            vector3_4.z = 0.0f;
            if (!flag8 && (this.m_style & FontStyles.Italic) == FontStyles.Italic || (this.m_fontStyle & FontStyles.Italic) == FontStyles.Italic)
            {
              float num14 = (float) this.m_currentFontAsset.italicStyle * 0.01f;
              Vector3 vector3_5 = new Vector3(num14 * ((this.m_cached_TextElement.yOffset + padding + style_padding) * num3), 0.0f, 0.0f);
              Vector3 vector3_6 = new Vector3(num14 * ((this.m_cached_TextElement.yOffset - this.m_cached_TextElement.height - padding - style_padding) * num3), 0.0f, 0.0f);
              vector3_1 += vector3_5;
              vector3_2 += vector3_6;
              vector3_3 += vector3_5;
              vector3_4 += vector3_6;
            }
            this.m_textInfo.characterInfo[this.m_characterCount].bottomLeft = vector3_2;
            this.m_textInfo.characterInfo[this.m_characterCount].topLeft = vector3_1;
            this.m_textInfo.characterInfo[this.m_characterCount].topRight = vector3_3;
            this.m_textInfo.characterInfo[this.m_characterCount].bottomRight = vector3_4;
            this.m_textInfo.characterInfo[this.m_characterCount].scale = num3;
            this.m_textInfo.characterInfo[this.m_characterCount].origin = this.m_xAdvance;
            this.m_textInfo.characterInfo[this.m_characterCount].baseLine = 0.0f - this.m_lineOffset + this.m_baselineOffset;
            this.m_textInfo.characterInfo[this.m_characterCount].aspectRatio = this.m_cached_TextElement.width / this.m_cached_TextElement.height;
            float num17 = this.m_currentFontAsset.fontInfo.Ascender * (this.m_textElementType != TMP_TextElementType.Character ? num2 : num3) + this.m_baselineOffset;
            this.m_textInfo.characterInfo[this.m_characterCount].ascender = num17 - this.m_lineOffset;
            this.m_maxLineAscender = (double) num17 <= (double) this.m_maxLineAscender ? this.m_maxLineAscender : num17;
            float num18 = this.m_currentFontAsset.fontInfo.Descender * (this.m_textElementType != TMP_TextElementType.Character ? num2 : num3) + this.m_baselineOffset;
            float num19 = this.m_textInfo.characterInfo[this.m_characterCount].descender = num18 - this.m_lineOffset;
            this.m_maxLineDescender = (double) num18 >= (double) this.m_maxLineDescender ? this.m_maxLineDescender : num18;
            if ((this.m_style & FontStyles.Subscript) == FontStyles.Subscript || (this.m_style & FontStyles.Superscript) == FontStyles.Superscript)
            {
              float num14 = (num17 - this.m_baselineOffset) / this.m_currentFontAsset.fontInfo.SubSize;
              num17 = this.m_maxLineAscender;
              this.m_maxLineAscender = (double) num14 <= (double) this.m_maxLineAscender ? this.m_maxLineAscender : num14;
              float num20 = (num18 - this.m_baselineOffset) / this.m_currentFontAsset.fontInfo.SubSize;
              num18 = this.m_maxLineDescender;
              this.m_maxLineDescender = (double) num20 >= (double) this.m_maxLineDescender ? this.m_maxLineDescender : num20;
            }
            if (this.m_lineNumber == 0)
              this.m_maxAscender = (double) this.m_maxAscender <= (double) num17 ? num17 : this.m_maxAscender;
            if ((double) this.m_lineOffset == 0.0)
              num8 = (double) num8 <= (double) num17 ? num17 : num8;
            this.m_textInfo.characterInfo[this.m_characterCount].isVisible = false;
            if (num11 == 9 || !char.IsWhiteSpace((char) num11) || this.m_textElementType == TMP_TextElementType.Sprite)
            {
              this.m_textInfo.characterInfo[this.m_characterCount].isVisible = true;
              float num14 = (double) this.m_width == -1.0 ? num6 + 0.0001f - this.m_marginLeft - this.m_marginRight : Mathf.Min(num6 + 0.0001f - this.m_marginLeft - this.m_marginRight, this.m_width);
              this.m_textInfo.lineInfo[this.m_lineNumber].width = num14;
              this.m_textInfo.lineInfo[this.m_lineNumber].marginLeft = this.m_marginLeft;
              if ((double) Mathf.Abs(this.m_xAdvance) + (this.m_isRightToLeft ? 0.0 : (double) this.m_cached_TextElement.xAdvance) * (1.0 - (double) this.m_charWidthAdjDelta) * (double) num3 > (double) num14)
              {
                index2 = this.m_characterCount - 1;
                if (this.enableWordWrapping && this.m_characterCount != this.m_firstCharacterOfLine)
                {
                  if (num10 == this.m_SavedWordWrapState.previous_WordBreak || flag5)
                  {
                    if (this.m_enableAutoSizing && (double) this.m_fontSize > (double) this.m_fontSizeMin)
                    {
                      if ((double) this.m_charWidthAdjDelta < (double) this.m_charWidthMaxAdj / 100.0)
                      {
                        this.loopCountA = 0;
                        this.m_charWidthAdjDelta += 0.01f;
                        this.GenerateTextMesh();
                        return;
                      }
                      this.m_maxFontSize = this.m_fontSize;
                      this.m_fontSize -= (float) (double) Mathf.Max((float) (((double) this.m_fontSize - (double) this.m_minFontSize) / 2.0), 0.05f);
                      this.m_fontSize = (float) (int) ((double) Mathf.Max(this.m_fontSize, this.m_fontSizeMin) * 20.0 + 0.5) / 20f;
                      if (this.loopCountA > 20)
                        return;
                      this.GenerateTextMesh();
                      return;
                    }
                    if (!this.m_isCharacterWrappingEnabled)
                      this.m_isCharacterWrappingEnabled = true;
                    else
                      flag6 = true;
                    ++this.m_recursiveCount;
                    if (this.m_recursiveCount > 20)
                      continue;
                  }
                  index3 = this.RestoreWordWrappingState(ref this.m_SavedWordWrapState);
                  num10 = index3;
                  if (this.m_lineNumber > 0 && !TMP_Math.Approximately(this.m_maxLineAscender, this.m_startOfLineAscender) && ((double) this.m_lineHeight == 0.0 && !this.m_isNewPage))
                  {
                    float offset = this.m_maxLineAscender - this.m_startOfLineAscender;
                    this.AdjustLineOffset(this.m_firstCharacterOfLine, this.m_characterCount, offset);
                    this.m_lineOffset += (float) (double) offset;
                    this.m_SavedWordWrapState.lineOffset = this.m_lineOffset;
                    this.m_SavedWordWrapState.previousLineAscender = this.m_maxLineAscender;
                  }
                  this.m_isNewPage = false;
                  float y1 = this.m_maxLineAscender - this.m_lineOffset;
                  float y2 = this.m_maxLineDescender - this.m_lineOffset;
                  this.m_maxDescender = (double) this.m_maxDescender >= (double) y2 ? y2 : this.m_maxDescender;
                  if (!flag4)
                    num9 = this.m_maxDescender;
                  if (this.m_characterCount >= this.m_maxVisibleCharacters || this.m_lineNumber >= this.m_maxVisibleLines)
                    flag4 = true;
                  this.m_textInfo.lineInfo[this.m_lineNumber].firstCharacterIndex = this.m_firstCharacterOfLine;
                  this.m_textInfo.lineInfo[this.m_lineNumber].firstVisibleCharacterIndex = this.m_firstVisibleCharacterOfLine;
                  this.m_textInfo.lineInfo[this.m_lineNumber].lastCharacterIndex = this.m_characterCount - 1 <= 0 ? 0 : this.m_characterCount - 1;
                  this.m_textInfo.lineInfo[this.m_lineNumber].lastVisibleCharacterIndex = this.m_lastVisibleCharacterOfLine;
                  this.m_textInfo.lineInfo[this.m_lineNumber].characterCount = this.m_textInfo.lineInfo[this.m_lineNumber].lastCharacterIndex - this.m_textInfo.lineInfo[this.m_lineNumber].firstCharacterIndex + 1;
                  this.m_textInfo.lineInfo[this.m_lineNumber].lineExtents.min = new Vector2(this.m_textInfo.characterInfo[this.m_firstVisibleCharacterOfLine].bottomLeft.x, y2);
                  this.m_textInfo.lineInfo[this.m_lineNumber].lineExtents.max = new Vector2(this.m_textInfo.characterInfo[this.m_lastVisibleCharacterOfLine].topRight.x, y1);
                  this.m_textInfo.lineInfo[this.m_lineNumber].length = this.m_textInfo.lineInfo[this.m_lineNumber].lineExtents.max.x;
                  this.m_textInfo.lineInfo[this.m_lineNumber].maxAdvance = this.m_textInfo.characterInfo[this.m_lastVisibleCharacterOfLine].xAdvance - (this.m_characterSpacing + this.m_currentFontAsset.normalSpacingOffset) * num3;
                  this.m_textInfo.lineInfo[this.m_lineNumber].baseline = 0.0f - this.m_lineOffset;
                  this.m_textInfo.lineInfo[this.m_lineNumber].ascender = y1;
                  this.m_textInfo.lineInfo[this.m_lineNumber].descender = y2;
                  this.m_firstCharacterOfLine = this.m_characterCount;
                  this.SaveWordWrappingState(ref this.m_SavedLineState, index3, this.m_characterCount - 1);
                  ++this.m_lineNumber;
                  flag3 = true;
                  if (this.m_lineNumber >= this.m_textInfo.lineInfo.Length)
                    this.ResizeLineExtents(this.m_lineNumber);
                  if ((double) this.m_lineHeight == 0.0)
                  {
                    float num20 = this.m_textInfo.characterInfo[this.m_characterCount].ascender - this.m_textInfo.characterInfo[this.m_characterCount].baseLine;
                    this.m_lineOffset += (float) (double) (float) (0.0 - (double) this.m_maxLineDescender + (double) num20 + ((double) num5 + (double) this.m_lineSpacing + (double) this.m_lineSpacingDelta) * (double) num2);
                    this.m_startOfLineAscender = num20;
                  }
                  else
                    this.m_lineOffset += (float) ((double) this.m_lineHeight + (double) this.m_lineSpacing * (double) num2);
                  this.m_maxLineAscender = float.NegativeInfinity;
                  this.m_maxLineDescender = float.PositiveInfinity;
                  this.m_xAdvance = 0.0f + this.tag_Indent;
                  continue;
                }
                if (this.m_enableAutoSizing && (double) this.m_fontSize > (double) this.m_fontSizeMin)
                {
                  if ((double) this.m_charWidthAdjDelta < (double) this.m_charWidthMaxAdj / 100.0)
                  {
                    this.loopCountA = 0;
                    this.m_charWidthAdjDelta += 0.01f;
                    this.GenerateTextMesh();
                    return;
                  }
                  this.m_maxFontSize = this.m_fontSize;
                  this.m_fontSize -= (float) (double) Mathf.Max((float) (((double) this.m_fontSize - (double) this.m_minFontSize) / 2.0), 0.05f);
                  this.m_fontSize = (float) (int) ((double) Mathf.Max(this.m_fontSize, this.m_fontSizeMin) * 20.0 + 0.5) / 20f;
                  this.m_recursiveCount = 0;
                  if (this.loopCountA > 20)
                    return;
                  this.GenerateTextMesh();
                  return;
                }
                switch (this.m_overflowMode)
                {
                  case TextOverflowModes.Overflow:
                    if (this.m_isMaskingEnabled)
                    {
                      this.DisableMasking();
                      break;
                    }
                    break;
                  case TextOverflowModes.Ellipsis:
                    if (this.m_isMaskingEnabled)
                      this.DisableMasking();
                    this.m_isTextTruncated = true;
                    if (this.m_characterCount < 1)
                    {
                      this.m_textInfo.characterInfo[this.m_characterCount].isVisible = false;
                      this.m_visibleCharacterCount = 0;
                      break;
                    }
                    this.m_char_buffer[index3 - 1] = 8230;
                    this.m_char_buffer[index3] = 0;
                    if (this.m_cached_Ellipsis_GlyphInfo != null)
                    {
                      this.m_textInfo.characterInfo[index2].character = '…';
                      this.m_textInfo.characterInfo[index2].textElement = (TMP_TextElement) this.m_cached_Ellipsis_GlyphInfo;
                      this.m_textInfo.characterInfo[index2].fontAsset = this.m_materialReferences[0].fontAsset;
                      this.m_textInfo.characterInfo[index2].material = this.m_materialReferences[0].material;
                      this.m_textInfo.characterInfo[index2].materialReferenceIndex = 0;
                    }
                    this.m_totalCharacterCount = index2 + 1;
                    this.GenerateTextMesh();
                    return;
                  case TextOverflowModes.Masking:
                    if (!this.m_isMaskingEnabled)
                    {
                      this.EnableMasking();
                      break;
                    }
                    break;
                  case TextOverflowModes.Truncate:
                    if (this.m_isMaskingEnabled)
                      this.DisableMasking();
                    this.m_textInfo.characterInfo[this.m_characterCount].isVisible = false;
                    break;
                  case TextOverflowModes.ScrollRect:
                    if (!this.m_isMaskingEnabled)
                    {
                      this.EnableMasking();
                      break;
                    }
                    break;
                }
              }
              if (num11 != 9)
              {
                Color32 vertexColor = !flag7 ? (!this.m_overrideHtmlColors ? this.m_htmlColor : this.m_fontColor32) : (Color32) Color.red;
                if (this.m_textElementType == TMP_TextElementType.Character)
                  this.SaveGlyphVertexInfo(padding, style_padding, vertexColor);
                else if (this.m_textElementType == TMP_TextElementType.Sprite)
                  this.SaveSpriteVertexInfo(vertexColor);
              }
              else
              {
                this.m_textInfo.characterInfo[this.m_characterCount].isVisible = false;
                this.m_lastVisibleCharacterOfLine = this.m_characterCount;
                ++this.m_textInfo.lineInfo[this.m_lineNumber].spaceCount;
                ++this.m_textInfo.spaceCount;
              }
              if (this.m_textInfo.characterInfo[this.m_characterCount].isVisible)
              {
                if (flag3)
                {
                  flag3 = false;
                  this.m_firstVisibleCharacterOfLine = this.m_characterCount;
                }
                ++this.m_visibleCharacterCount;
                this.m_lastVisibleCharacterOfLine = this.m_characterCount;
              }
            }
            else if (char.IsSeparator((char) num11))
            {
              ++this.m_textInfo.lineInfo[this.m_lineNumber].spaceCount;
              ++this.m_textInfo.spaceCount;
            }
            if (this.m_lineNumber > 0 && !TMP_Math.Approximately(this.m_maxLineAscender, this.m_startOfLineAscender) && ((double) this.m_lineHeight == 0.0 && !this.m_isNewPage))
            {
              float offset = this.m_maxLineAscender - this.m_startOfLineAscender;
              this.AdjustLineOffset(this.m_firstCharacterOfLine, this.m_characterCount, offset);
              num19 -= offset;
              this.m_lineOffset += (float) (double) offset;
              this.m_startOfLineAscender += (float) (double) offset;
              this.m_SavedWordWrapState.lineOffset = this.m_lineOffset;
              this.m_SavedWordWrapState.previousLineAscender = this.m_startOfLineAscender;
            }
            this.m_textInfo.characterInfo[this.m_characterCount].lineNumber = (short) this.m_lineNumber;
            this.m_textInfo.characterInfo[this.m_characterCount].pageNumber = (short) this.m_pageNumber;
            if (num11 != 10 && num11 != 13 && num11 != 8230 || this.m_textInfo.lineInfo[this.m_lineNumber].characterCount == 1)
              this.m_textInfo.lineInfo[this.m_lineNumber].alignment = this.m_lineJustification;
            if ((double) this.m_maxAscender - (double) num19 > (double) num7 + 9.99999974737875E-05)
            {
              if (this.m_enableAutoSizing && (double) this.m_lineSpacingDelta > (double) this.m_lineSpacingMax && this.m_lineNumber > 0)
              {
                --this.m_lineSpacingDelta;
                this.GenerateTextMesh();
                return;
              }
              if (this.m_enableAutoSizing && (double) this.m_fontSize > (double) this.m_fontSizeMin)
              {
                this.m_maxFontSize = this.m_fontSize;
                this.m_fontSize -= (float) (double) Mathf.Max((float) (((double) this.m_fontSize - (double) this.m_minFontSize) / 2.0), 0.05f);
                this.m_fontSize = (float) (int) ((double) Mathf.Max(this.m_fontSize, this.m_fontSizeMin) * 20.0 + 0.5) / 20f;
                this.m_recursiveCount = 0;
                if (this.loopCountA > 20)
                  return;
                this.GenerateTextMesh();
                return;
              }
              switch (this.m_overflowMode)
              {
                case TextOverflowModes.Overflow:
                  if (this.m_isMaskingEnabled)
                  {
                    this.DisableMasking();
                    break;
                  }
                  break;
                case TextOverflowModes.Ellipsis:
                  if (this.m_isMaskingEnabled)
                    this.DisableMasking();
                  if (this.m_lineNumber > 0)
                  {
                    this.m_char_buffer[(int) this.m_textInfo.characterInfo[index2].index] = 8230;
                    this.m_char_buffer[(int) this.m_textInfo.characterInfo[index2].index + 1] = 0;
                    if (this.m_cached_Ellipsis_GlyphInfo != null)
                    {
                      this.m_textInfo.characterInfo[index2].character = '…';
                      this.m_textInfo.characterInfo[index2].textElement = (TMP_TextElement) this.m_cached_Ellipsis_GlyphInfo;
                      this.m_textInfo.characterInfo[index2].fontAsset = this.m_materialReferences[0].fontAsset;
                      this.m_textInfo.characterInfo[index2].material = this.m_materialReferences[0].material;
                      this.m_textInfo.characterInfo[index2].materialReferenceIndex = 0;
                    }
                    this.m_totalCharacterCount = index2 + 1;
                    this.GenerateTextMesh();
                    this.m_isTextTruncated = true;
                    return;
                  }
                  this.ClearMesh();
                  return;
                case TextOverflowModes.Masking:
                  if (!this.m_isMaskingEnabled)
                  {
                    this.EnableMasking();
                    break;
                  }
                  break;
                case TextOverflowModes.Truncate:
                  if (this.m_isMaskingEnabled)
                    this.DisableMasking();
                  if (this.m_lineNumber > 0)
                  {
                    this.m_char_buffer[(int) this.m_textInfo.characterInfo[index2].index + 1] = 0;
                    this.m_totalCharacterCount = index2 + 1;
                    this.GenerateTextMesh();
                    this.m_isTextTruncated = true;
                    return;
                  }
                  this.ClearMesh();
                  return;
                case TextOverflowModes.ScrollRect:
                  if (!this.m_isMaskingEnabled)
                  {
                    this.EnableMasking();
                    break;
                  }
                  break;
                case TextOverflowModes.Page:
                  if (this.m_isMaskingEnabled)
                    this.DisableMasking();
                  if (num11 != 13 && num11 != 10)
                  {
                    index3 = this.RestoreWordWrappingState(ref this.m_SavedLineState);
                    if (index3 == 0)
                    {
                      this.ClearMesh();
                      return;
                    }
                    this.m_isNewPage = true;
                    this.m_xAdvance = 0.0f + this.tag_Indent;
                    this.m_lineOffset = 0.0f;
                    ++this.m_lineNumber;
                    ++this.m_pageNumber;
                    continue;
                  }
                  break;
              }
            }
            if (num11 == 9)
              this.m_xAdvance += (float) ((double) this.m_currentFontAsset.fontInfo.TabWidth * (double) num3);
            else if ((double) this.m_monoSpacing != 0.0)
              this.m_xAdvance += (float) (((double) this.m_monoSpacing - (double) num15 + ((double) this.m_characterSpacing + (double) this.m_currentFontAsset.normalSpacingOffset) * (double) num3 + (double) this.m_cSpacing) * (1.0 - (double) this.m_charWidthAdjDelta));
            else if (!this.m_isRightToLeft)
              this.m_xAdvance += (float) ((((double) this.m_cached_TextElement.xAdvance * (double) num4 + (double) this.m_characterSpacing + (double) this.m_currentFontAsset.normalSpacingOffset) * (double) num3 + (double) this.m_cSpacing) * (1.0 - (double) this.m_charWidthAdjDelta));
            this.m_textInfo.characterInfo[this.m_characterCount].xAdvance = this.m_xAdvance;
            if (num11 == 13)
              this.m_xAdvance = 0.0f + this.tag_Indent;
            if (num11 == 10 || this.m_characterCount == num1 - 1)
            {
              if (this.m_lineNumber > 0 && !TMP_Math.Approximately(this.m_maxLineAscender, this.m_startOfLineAscender) && ((double) this.m_lineHeight == 0.0 && !this.m_isNewPage))
              {
                float offset = this.m_maxLineAscender - this.m_startOfLineAscender;
                this.AdjustLineOffset(this.m_firstCharacterOfLine, this.m_characterCount, offset);
                float num14 = num19 - offset;
                this.m_lineOffset += (float) (double) offset;
              }
              this.m_isNewPage = false;
              float y1 = this.m_maxLineAscender - this.m_lineOffset;
              float y2 = this.m_maxLineDescender - this.m_lineOffset;
              this.m_maxDescender = (double) this.m_maxDescender >= (double) y2 ? y2 : this.m_maxDescender;
              if (!flag4)
                num9 = this.m_maxDescender;
              if (this.m_characterCount >= this.m_maxVisibleCharacters || this.m_lineNumber >= this.m_maxVisibleLines)
                flag4 = true;
              this.m_textInfo.lineInfo[this.m_lineNumber].firstCharacterIndex = this.m_firstCharacterOfLine;
              this.m_textInfo.lineInfo[this.m_lineNumber].firstVisibleCharacterIndex = this.m_firstVisibleCharacterOfLine;
              this.m_textInfo.lineInfo[this.m_lineNumber].lastCharacterIndex = this.m_characterCount;
              this.m_textInfo.lineInfo[this.m_lineNumber].lastVisibleCharacterIndex = this.m_lastVisibleCharacterOfLine < this.m_firstVisibleCharacterOfLine ? this.m_firstVisibleCharacterOfLine : this.m_lastVisibleCharacterOfLine;
              this.m_textInfo.lineInfo[this.m_lineNumber].characterCount = this.m_textInfo.lineInfo[this.m_lineNumber].lastCharacterIndex - this.m_textInfo.lineInfo[this.m_lineNumber].firstCharacterIndex + 1;
              this.m_textInfo.lineInfo[this.m_lineNumber].lineExtents.min = new Vector2(this.m_textInfo.characterInfo[this.m_firstVisibleCharacterOfLine].bottomLeft.x, y2);
              this.m_textInfo.lineInfo[this.m_lineNumber].lineExtents.max = new Vector2(this.m_textInfo.characterInfo[this.m_lastVisibleCharacterOfLine].topRight.x, y1);
              this.m_textInfo.lineInfo[this.m_lineNumber].length = this.m_textInfo.lineInfo[this.m_lineNumber].lineExtents.max.x - padding * num3;
              this.m_textInfo.lineInfo[this.m_lineNumber].maxAdvance = this.m_textInfo.characterInfo[this.m_lastVisibleCharacterOfLine].xAdvance - (this.m_characterSpacing + this.m_currentFontAsset.normalSpacingOffset) * num3;
              this.m_textInfo.lineInfo[this.m_lineNumber].baseline = 0.0f - this.m_lineOffset;
              this.m_textInfo.lineInfo[this.m_lineNumber].ascender = y1;
              this.m_textInfo.lineInfo[this.m_lineNumber].descender = y2;
              this.m_firstCharacterOfLine = this.m_characterCount + 1;
              if (num11 == 10)
              {
                this.SaveWordWrappingState(ref this.m_SavedLineState, index3, this.m_characterCount);
                this.SaveWordWrappingState(ref this.m_SavedWordWrapState, index3, this.m_characterCount);
                ++this.m_lineNumber;
                flag3 = true;
                if (this.m_lineNumber >= this.m_textInfo.lineInfo.Length)
                  this.ResizeLineExtents(this.m_lineNumber);
                if ((double) this.m_lineHeight == 0.0)
                  this.m_lineOffset += (float) (double) (float) (0.0 - (double) this.m_maxLineDescender + (double) num17 + ((double) num5 + (double) this.m_lineSpacing + (double) this.m_paragraphSpacing + (double) this.m_lineSpacingDelta) * (double) num2);
                else
                  this.m_lineOffset += (float) ((double) this.m_lineHeight + ((double) this.m_lineSpacing + (double) this.m_paragraphSpacing) * (double) num2);
                this.m_maxLineAscender = float.NegativeInfinity;
                this.m_maxLineDescender = float.PositiveInfinity;
                this.m_startOfLineAscender = num17;
                this.m_xAdvance = 0.0f + this.tag_LineIndent + this.tag_Indent;
                index2 = this.m_characterCount - 1;
              }
            }
            if (this.m_textInfo.characterInfo[this.m_characterCount].isVisible)
            {
              this.m_meshExtents.min.x = Mathf.Min(this.m_meshExtents.min.x, this.m_textInfo.characterInfo[this.m_characterCount].bottomLeft.x);
              this.m_meshExtents.min.y = Mathf.Min(this.m_meshExtents.min.y, this.m_textInfo.characterInfo[this.m_characterCount].bottomLeft.y);
              this.m_meshExtents.max.x = Mathf.Max(this.m_meshExtents.max.x, this.m_textInfo.characterInfo[this.m_characterCount].topRight.x);
              this.m_meshExtents.max.y = Mathf.Max(this.m_meshExtents.max.y, this.m_textInfo.characterInfo[this.m_characterCount].topRight.y);
            }
            if (this.m_overflowMode == TextOverflowModes.Page && num11 != 13 && (num11 != 10 && this.m_pageNumber < 16))
            {
              this.m_textInfo.pageInfo[this.m_pageNumber].ascender = num8;
              this.m_textInfo.pageInfo[this.m_pageNumber].descender = (double) num18 >= (double) this.m_textInfo.pageInfo[this.m_pageNumber].descender ? this.m_textInfo.pageInfo[this.m_pageNumber].descender : num18;
              if (this.m_pageNumber == 0 && this.m_characterCount == 0)
                this.m_textInfo.pageInfo[this.m_pageNumber].firstCharacterIndex = this.m_characterCount;
              else if (this.m_characterCount > 0 && this.m_pageNumber != (int) this.m_textInfo.characterInfo[this.m_characterCount - 1].pageNumber)
              {
                this.m_textInfo.pageInfo[this.m_pageNumber - 1].lastCharacterIndex = this.m_characterCount - 1;
                this.m_textInfo.pageInfo[this.m_pageNumber].firstCharacterIndex = this.m_characterCount;
              }
              else if (this.m_characterCount == num1 - 1)
                this.m_textInfo.pageInfo[this.m_pageNumber].lastCharacterIndex = this.m_characterCount;
            }
            if (this.m_enableWordWrapping || this.m_overflowMode == TextOverflowModes.Truncate || this.m_overflowMode == TextOverflowModes.Ellipsis)
            {
              if (char.IsWhiteSpace((char) num11) && !this.m_isNonBreakingSpace)
              {
                this.SaveWordWrappingState(ref this.m_SavedWordWrapState, index3, this.m_characterCount);
                this.m_isCharacterWrappingEnabled = false;
                flag5 = false;
              }
              else if (num11 > 11904 && num11 < 40959 && !this.m_isNonBreakingSpace)
              {
                if (!this.m_currentFontAsset.lineBreakingInfo.leadingCharacters.ContainsKey(num11) && this.m_characterCount < num1 - 1 && !this.m_currentFontAsset.lineBreakingInfo.followingCharacters.ContainsKey((int) this.m_textInfo.characterInfo[this.m_characterCount + 1].character))
                {
                  this.SaveWordWrappingState(ref this.m_SavedWordWrapState, index3, this.m_characterCount);
                  this.m_isCharacterWrappingEnabled = false;
                  flag5 = false;
                }
              }
              else if (flag5 || this.m_isCharacterWrappingEnabled || flag6)
                this.SaveWordWrappingState(ref this.m_SavedWordWrapState, index3, this.m_characterCount);
            }
            ++this.m_characterCount;
          }
          if (!this.m_isCharacterWrappingEnabled && this.m_enableAutoSizing && ((double) (this.m_maxFontSize - this.m_minFontSize) > 0.0509999990463257 && (double) this.m_fontSize < (double) this.m_fontSizeMax))
          {
            this.m_minFontSize = this.m_fontSize;
            this.m_fontSize += (float) (double) Mathf.Max((float) (((double) this.m_maxFontSize - (double) this.m_fontSize) / 2.0), 0.05f);
            this.m_fontSize = (float) (int) ((double) Mathf.Min(this.m_fontSize, this.m_fontSizeMax) * 20.0 + 0.5) / 20f;
            if (this.loopCountA > 20)
              return;
            this.GenerateTextMesh();
          }
          else
          {
            this.m_isCharacterWrappingEnabled = false;
            if (this.m_visibleCharacterCount == 0 && this.m_visibleSpriteCount == 0)
            {
              this.ClearMesh();
            }
            else
            {
              int index3 = this.m_materialReferences[0].referenceCount * 4;
              this.m_textInfo.meshInfo[0].Clear(false);
              Vector3 vector3_1 = Vector3.zero;
              Vector3[] vector3Array = this.m_RectTransformCorners;
              switch (this.m_textAlignment)
              {
                case TextAlignmentOptions.TopLeft:
                case TextAlignmentOptions.Top:
                case TextAlignmentOptions.TopRight:
                case TextAlignmentOptions.TopJustified:
                  vector3_1 = this.m_overflowMode == TextOverflowModes.Page ? vector3Array[1] + new Vector3(0.0f + vector4.x, 0.0f - this.m_textInfo.pageInfo[index1].ascender - vector4.y, 0.0f) : vector3Array[1] + new Vector3(0.0f + vector4.x, 0.0f - this.m_maxAscender - vector4.y, 0.0f);
                  break;
                case TextAlignmentOptions.Left:
                case TextAlignmentOptions.Center:
                case TextAlignmentOptions.Right:
                case TextAlignmentOptions.Justified:
                  vector3_1 = this.m_overflowMode == TextOverflowModes.Page ? (vector3Array[0] + vector3Array[1]) / 2f + new Vector3(0.0f + vector4.x, (float) (0.0 - ((double) this.m_textInfo.pageInfo[index1].ascender + (double) vector4.y + (double) this.m_textInfo.pageInfo[index1].descender - (double) vector4.w) / 2.0), 0.0f) : (vector3Array[0] + vector3Array[1]) / 2f + new Vector3(0.0f + vector4.x, (float) (0.0 - ((double) this.m_maxAscender + (double) vector4.y + (double) num9 - (double) vector4.w) / 2.0), 0.0f);
                  break;
                case TextAlignmentOptions.BottomLeft:
                case TextAlignmentOptions.Bottom:
                case TextAlignmentOptions.BottomRight:
                case TextAlignmentOptions.BottomJustified:
                  vector3_1 = this.m_overflowMode == TextOverflowModes.Page ? vector3Array[0] + new Vector3(0.0f + vector4.x, 0.0f - this.m_textInfo.pageInfo[index1].descender + vector4.w, 0.0f) : vector3Array[0] + new Vector3(0.0f + vector4.x, 0.0f - num9 + vector4.w, 0.0f);
                  break;
                case TextAlignmentOptions.BaselineLeft:
                case TextAlignmentOptions.Baseline:
                case TextAlignmentOptions.BaselineRight:
                case TextAlignmentOptions.BaselineJustified:
                  vector3_1 = (vector3Array[0] + vector3Array[1]) / 2f + new Vector3(0.0f + vector4.x, 0.0f, 0.0f);
                  break;
                case TextAlignmentOptions.MidlineLeft:
                case TextAlignmentOptions.Midline:
                case TextAlignmentOptions.MidlineRight:
                case TextAlignmentOptions.MidlineJustified:
                  vector3_1 = (vector3Array[0] + vector3Array[1]) / 2f + new Vector3(0.0f + vector4.x, (float) (0.0 - ((double) this.m_meshExtents.max.y + (double) vector4.y + (double) this.m_meshExtents.min.y - (double) vector4.w) / 2.0), 0.0f);
                  break;
              }
              Vector3 vector3_2 = Vector3.zero;
              Vector3 zero = Vector3.zero;
              int index_X4_1 = 0;
              int index_X4_2 = 0;
              int num11 = 0;
              int num12 = 0;
              int index4 = 0;
              bool flag7 = false;
              int num13 = 0;
              bool flag8 = !((UnityEngine.Object) this.m_canvas.worldCamera == (UnityEngine.Object) null);
              float num14 = (double) this.m_rectTransform.lossyScale.y == 0.0 ? 1f : this.m_rectTransform.lossyScale.y;
              RenderMode renderMode = this.m_canvas.renderMode;
              float scaleFactor = this.m_canvas.scaleFactor;
              Color32 underlineColor1 = (Color32) Color.white;
              Color32 underlineColor2 = (Color32) Color.white;
              float startScale = 0.0f;
              float num15 = 0.0f;
              float y1 = float.PositiveInfinity;
              int num16 = 0;
              float num17 = 0.0f;
              float num18 = 0.0f;
              float b = 0.0f;
              TMP_CharacterInfo[] tmpCharacterInfoArray = this.m_textInfo.characterInfo;
              for (int i = 0; i < this.m_characterCount; ++i)
              {
                char c = tmpCharacterInfoArray[i].character;
                int index5 = (int) tmpCharacterInfoArray[i].lineNumber;
                TMP_LineInfo tmpLineInfo = this.m_textInfo.lineInfo[index5];
                num12 = index5 + 1;
                switch (tmpLineInfo.alignment)
                {
                  case TextAlignmentOptions.TopLeft:
                  case TextAlignmentOptions.Left:
                  case TextAlignmentOptions.BottomLeft:
                  case TextAlignmentOptions.BaselineLeft:
                  case TextAlignmentOptions.MidlineLeft:
                    vector3_2 = this.m_isRightToLeft ? new Vector3(0.0f - tmpLineInfo.maxAdvance, 0.0f, 0.0f) : new Vector3(0.0f + tmpLineInfo.marginLeft, 0.0f, 0.0f);
                    break;
                  case TextAlignmentOptions.Top:
                  case TextAlignmentOptions.Center:
                  case TextAlignmentOptions.Bottom:
                  case TextAlignmentOptions.Baseline:
                  case TextAlignmentOptions.Midline:
                    vector3_2 = new Vector3((float) ((double) tmpLineInfo.marginLeft + (double) tmpLineInfo.width / 2.0 - (double) tmpLineInfo.maxAdvance / 2.0), 0.0f, 0.0f);
                    break;
                  case TextAlignmentOptions.TopRight:
                  case TextAlignmentOptions.Right:
                  case TextAlignmentOptions.BottomRight:
                  case TextAlignmentOptions.BaselineRight:
                  case TextAlignmentOptions.MidlineRight:
                    vector3_2 = this.m_isRightToLeft ? new Vector3(tmpLineInfo.marginLeft + tmpLineInfo.width, 0.0f, 0.0f) : new Vector3(tmpLineInfo.marginLeft + tmpLineInfo.width - tmpLineInfo.maxAdvance, 0.0f, 0.0f);
                    break;
                  case TextAlignmentOptions.TopJustified:
                  case TextAlignmentOptions.Justified:
                  case TextAlignmentOptions.BottomJustified:
                  case TextAlignmentOptions.BaselineJustified:
                  case TextAlignmentOptions.MidlineJustified:
                    if (!char.IsControl(tmpCharacterInfoArray[tmpLineInfo.lastCharacterIndex].character) && index5 < this.m_lineNumber)
                    {
                      float num19 = tmpLineInfo.width - tmpLineInfo.maxAdvance;
                      float num20 = tmpLineInfo.spaceCount <= 2 ? 1f : this.m_wordWrappingRatios;
                      if (index5 != index4 || i == 0)
                      {
                        vector3_2 = new Vector3(tmpLineInfo.marginLeft, 0.0f, 0.0f);
                        break;
                      }
                      if ((int) c == 9 || char.IsSeparator(c))
                      {
                        int num21 = tmpLineInfo.spaceCount - 1 <= 0 ? 1 : tmpLineInfo.spaceCount - 1;
                        vector3_2 += new Vector3(num19 * (1f - num20) / (float) num21, 0.0f, 0.0f);
                        break;
                      }
                      vector3_2 += new Vector3(num19 * num20 / (float) (tmpLineInfo.characterCount - tmpLineInfo.spaceCount - 1), 0.0f, 0.0f);
                      break;
                    }
                    vector3_2 = new Vector3(tmpLineInfo.marginLeft, 0.0f, 0.0f);
                    break;
                }
                Vector3 vector3_3 = vector3_1 + vector3_2;
                bool flag9 = tmpCharacterInfoArray[i].isVisible;
                if (flag9)
                {
                  TMP_TextElementType tmpTextElementType = tmpCharacterInfoArray[i].elementType;
                  switch (tmpTextElementType)
                  {
                    case TMP_TextElementType.Character:
                      Extents extents = tmpLineInfo.lineExtents;
                      float num19 = (float) ((double) this.m_uvLineOffset * (double) index5 % 1.0) + this.m_uvOffset.x;
                      switch (this.m_horizontalMapping)
                      {
                        case TextureMappingOptions.Character:
                          tmpCharacterInfoArray[i].vertex_BL.uv2.x = 0.0f + this.m_uvOffset.x;
                          tmpCharacterInfoArray[i].vertex_TL.uv2.x = 0.0f + this.m_uvOffset.x;
                          tmpCharacterInfoArray[i].vertex_TR.uv2.x = 1f + this.m_uvOffset.x;
                          tmpCharacterInfoArray[i].vertex_BR.uv2.x = 1f + this.m_uvOffset.x;
                          break;
                        case TextureMappingOptions.Line:
                          if (this.m_textAlignment != TextAlignmentOptions.Justified)
                          {
                            tmpCharacterInfoArray[i].vertex_BL.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_BL.position.x - (double) extents.min.x) / ((double) extents.max.x - (double) extents.min.x)) + num19;
                            tmpCharacterInfoArray[i].vertex_TL.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_TL.position.x - (double) extents.min.x) / ((double) extents.max.x - (double) extents.min.x)) + num19;
                            tmpCharacterInfoArray[i].vertex_TR.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_TR.position.x - (double) extents.min.x) / ((double) extents.max.x - (double) extents.min.x)) + num19;
                            tmpCharacterInfoArray[i].vertex_BR.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_BR.position.x - (double) extents.min.x) / ((double) extents.max.x - (double) extents.min.x)) + num19;
                            break;
                          }
                          tmpCharacterInfoArray[i].vertex_BL.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_BL.position.x + (double) vector3_2.x - (double) this.m_meshExtents.min.x) / ((double) this.m_meshExtents.max.x - (double) this.m_meshExtents.min.x)) + num19;
                          tmpCharacterInfoArray[i].vertex_TL.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_TL.position.x + (double) vector3_2.x - (double) this.m_meshExtents.min.x) / ((double) this.m_meshExtents.max.x - (double) this.m_meshExtents.min.x)) + num19;
                          tmpCharacterInfoArray[i].vertex_TR.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_TR.position.x + (double) vector3_2.x - (double) this.m_meshExtents.min.x) / ((double) this.m_meshExtents.max.x - (double) this.m_meshExtents.min.x)) + num19;
                          tmpCharacterInfoArray[i].vertex_BR.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_BR.position.x + (double) vector3_2.x - (double) this.m_meshExtents.min.x) / ((double) this.m_meshExtents.max.x - (double) this.m_meshExtents.min.x)) + num19;
                          break;
                        case TextureMappingOptions.Paragraph:
                          tmpCharacterInfoArray[i].vertex_BL.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_BL.position.x + (double) vector3_2.x - (double) this.m_meshExtents.min.x) / ((double) this.m_meshExtents.max.x - (double) this.m_meshExtents.min.x)) + num19;
                          tmpCharacterInfoArray[i].vertex_TL.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_TL.position.x + (double) vector3_2.x - (double) this.m_meshExtents.min.x) / ((double) this.m_meshExtents.max.x - (double) this.m_meshExtents.min.x)) + num19;
                          tmpCharacterInfoArray[i].vertex_TR.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_TR.position.x + (double) vector3_2.x - (double) this.m_meshExtents.min.x) / ((double) this.m_meshExtents.max.x - (double) this.m_meshExtents.min.x)) + num19;
                          tmpCharacterInfoArray[i].vertex_BR.uv2.x = (float) (((double) tmpCharacterInfoArray[i].vertex_BR.position.x + (double) vector3_2.x - (double) this.m_meshExtents.min.x) / ((double) this.m_meshExtents.max.x - (double) this.m_meshExtents.min.x)) + num19;
                          break;
                        case TextureMappingOptions.MatchAspect:
                          switch (this.m_verticalMapping)
                          {
                            case TextureMappingOptions.Character:
                              tmpCharacterInfoArray[i].vertex_BL.uv2.y = 0.0f + this.m_uvOffset.y;
                              tmpCharacterInfoArray[i].vertex_TL.uv2.y = 1f + this.m_uvOffset.y;
                              tmpCharacterInfoArray[i].vertex_TR.uv2.y = 0.0f + this.m_uvOffset.y;
                              tmpCharacterInfoArray[i].vertex_BR.uv2.y = 1f + this.m_uvOffset.y;
                              break;
                            case TextureMappingOptions.Line:
                              tmpCharacterInfoArray[i].vertex_BL.uv2.y = (float) (((double) tmpCharacterInfoArray[i].vertex_BL.position.y - (double) extents.min.y) / ((double) extents.max.y - (double) extents.min.y)) + num19;
                              tmpCharacterInfoArray[i].vertex_TL.uv2.y = (float) (((double) tmpCharacterInfoArray[i].vertex_TL.position.y - (double) extents.min.y) / ((double) extents.max.y - (double) extents.min.y)) + num19;
                              tmpCharacterInfoArray[i].vertex_TR.uv2.y = tmpCharacterInfoArray[i].vertex_BL.uv2.y;
                              tmpCharacterInfoArray[i].vertex_BR.uv2.y = tmpCharacterInfoArray[i].vertex_TL.uv2.y;
                              break;
                            case TextureMappingOptions.Paragraph:
                              tmpCharacterInfoArray[i].vertex_BL.uv2.y = (float) (((double) tmpCharacterInfoArray[i].vertex_BL.position.y - (double) this.m_meshExtents.min.y) / ((double) this.m_meshExtents.max.y - (double) this.m_meshExtents.min.y)) + num19;
                              tmpCharacterInfoArray[i].vertex_TL.uv2.y = (float) (((double) tmpCharacterInfoArray[i].vertex_TL.position.y - (double) this.m_meshExtents.min.y) / ((double) this.m_meshExtents.max.y - (double) this.m_meshExtents.min.y)) + num19;
                              tmpCharacterInfoArray[i].vertex_TR.uv2.y = tmpCharacterInfoArray[i].vertex_BL.uv2.y;
                              tmpCharacterInfoArray[i].vertex_BR.uv2.y = tmpCharacterInfoArray[i].vertex_TL.uv2.y;
                              break;
                            case TextureMappingOptions.MatchAspect:
                              Debug.Log((object) "ERROR: Cannot Match both Vertical & Horizontal.");
                              break;
                          }
                          float num20 = (float) ((1.0 - ((double) tmpCharacterInfoArray[i].vertex_BL.uv2.y + (double) tmpCharacterInfoArray[i].vertex_TL.uv2.y) * (double) tmpCharacterInfoArray[i].aspectRatio) / 2.0);
                          tmpCharacterInfoArray[i].vertex_BL.uv2.x = tmpCharacterInfoArray[i].vertex_BL.uv2.y * tmpCharacterInfoArray[i].aspectRatio + num20 + num19;
                          tmpCharacterInfoArray[i].vertex_TL.uv2.x = tmpCharacterInfoArray[i].vertex_BL.uv2.x;
                          tmpCharacterInfoArray[i].vertex_TR.uv2.x = tmpCharacterInfoArray[i].vertex_TL.uv2.y * tmpCharacterInfoArray[i].aspectRatio + num20 + num19;
                          tmpCharacterInfoArray[i].vertex_BR.uv2.x = tmpCharacterInfoArray[i].vertex_TR.uv2.x;
                          break;
                      }
                      switch (this.m_verticalMapping)
                      {
                        case TextureMappingOptions.Character:
                          tmpCharacterInfoArray[i].vertex_BL.uv2.y = 0.0f + this.m_uvOffset.y;
                          tmpCharacterInfoArray[i].vertex_TL.uv2.y = 1f + this.m_uvOffset.y;
                          tmpCharacterInfoArray[i].vertex_TR.uv2.y = 1f + this.m_uvOffset.y;
                          tmpCharacterInfoArray[i].vertex_BR.uv2.y = 0.0f + this.m_uvOffset.y;
                          break;
                        case TextureMappingOptions.Line:
                          tmpCharacterInfoArray[i].vertex_BL.uv2.y = (float) (((double) tmpCharacterInfoArray[i].vertex_BL.position.y - (double) tmpLineInfo.descender) / ((double) tmpLineInfo.ascender - (double) tmpLineInfo.descender)) + this.m_uvOffset.y;
                          tmpCharacterInfoArray[i].vertex_TL.uv2.y = (float) (((double) tmpCharacterInfoArray[i].vertex_TL.position.y - (double) tmpLineInfo.descender) / ((double) tmpLineInfo.ascender - (double) tmpLineInfo.descender)) + this.m_uvOffset.y;
                          tmpCharacterInfoArray[i].vertex_BR.uv2.y = tmpCharacterInfoArray[i].vertex_BL.uv2.y;
                          tmpCharacterInfoArray[i].vertex_TR.uv2.y = tmpCharacterInfoArray[i].vertex_TL.uv2.y;
                          break;
                        case TextureMappingOptions.Paragraph:
                          tmpCharacterInfoArray[i].vertex_BL.uv2.y = (float) (((double) tmpCharacterInfoArray[i].vertex_BL.position.y - (double) this.m_meshExtents.min.y) / ((double) this.m_meshExtents.max.y - (double) this.m_meshExtents.min.y)) + this.m_uvOffset.y;
                          tmpCharacterInfoArray[i].vertex_TL.uv2.y = (float) (((double) tmpCharacterInfoArray[i].vertex_TL.position.y - (double) this.m_meshExtents.min.y) / ((double) this.m_meshExtents.max.y - (double) this.m_meshExtents.min.y)) + this.m_uvOffset.y;
                          tmpCharacterInfoArray[i].vertex_TR.uv2.y = tmpCharacterInfoArray[i].vertex_TL.uv2.y;
                          tmpCharacterInfoArray[i].vertex_BR.uv2.y = tmpCharacterInfoArray[i].vertex_BL.uv2.y;
                          break;
                        case TextureMappingOptions.MatchAspect:
                          float num21 = (float) ((1.0 - ((double) tmpCharacterInfoArray[i].vertex_BL.uv2.x + (double) tmpCharacterInfoArray[i].vertex_TR.uv2.x) / (double) tmpCharacterInfoArray[i].aspectRatio) / 2.0);
                          tmpCharacterInfoArray[i].vertex_BL.uv2.y = num21 + tmpCharacterInfoArray[i].vertex_BL.uv2.x / tmpCharacterInfoArray[i].aspectRatio + this.m_uvOffset.y;
                          tmpCharacterInfoArray[i].vertex_TL.uv2.y = num21 + tmpCharacterInfoArray[i].vertex_TR.uv2.x / tmpCharacterInfoArray[i].aspectRatio + this.m_uvOffset.y;
                          tmpCharacterInfoArray[i].vertex_BR.uv2.y = tmpCharacterInfoArray[i].vertex_BL.uv2.y;
                          tmpCharacterInfoArray[i].vertex_TR.uv2.y = tmpCharacterInfoArray[i].vertex_TL.uv2.y;
                          break;
                      }
                      float num22 = tmpCharacterInfoArray[i].scale * (1f - this.m_charWidthAdjDelta);
                      if ((tmpCharacterInfoArray[i].style & FontStyles.Bold) == FontStyles.Bold)
                        num22 *= -1f;
                      switch (renderMode)
                      {
                        case RenderMode.ScreenSpaceOverlay:
                          num22 *= num14 / scaleFactor;
                          break;
                        case RenderMode.ScreenSpaceCamera:
                          num22 *= !flag8 ? 1f : num14;
                          break;
                        case RenderMode.WorldSpace:
                          num22 *= num14;
                          break;
                      }
                      float f1 = tmpCharacterInfoArray[i].vertex_BL.uv2.x;
                      float f2 = tmpCharacterInfoArray[i].vertex_BL.uv2.y;
                      float num23 = tmpCharacterInfoArray[i].vertex_TR.uv2.x;
                      float num24 = tmpCharacterInfoArray[i].vertex_TR.uv2.y;
                      float num25 = Mathf.Floor(f1);
                      float num26 = Mathf.Floor(f2);
                      float x1 = f1 - num25;
                      float x2 = num23 - num25;
                      float y2 = f2 - num26;
                      float y3 = num24 - num26;
                      tmpCharacterInfoArray[i].vertex_BL.uv2.x = this.PackUV(x1, y2);
                      tmpCharacterInfoArray[i].vertex_BL.uv2.y = num22;
                      tmpCharacterInfoArray[i].vertex_TL.uv2.x = this.PackUV(x1, y3);
                      tmpCharacterInfoArray[i].vertex_TL.uv2.y = num22;
                      tmpCharacterInfoArray[i].vertex_TR.uv2.x = this.PackUV(x2, y3);
                      tmpCharacterInfoArray[i].vertex_TR.uv2.y = num22;
                      tmpCharacterInfoArray[i].vertex_BR.uv2.x = this.PackUV(x2, y2);
                      tmpCharacterInfoArray[i].vertex_BR.uv2.y = num22;
                      break;
                  }
                  if (i < this.m_maxVisibleCharacters && index5 < this.m_maxVisibleLines && this.m_overflowMode != TextOverflowModes.Page)
                  {
                    tmpCharacterInfoArray[i].vertex_BL.position += vector3_3;
                    tmpCharacterInfoArray[i].vertex_TL.position += vector3_3;
                    tmpCharacterInfoArray[i].vertex_TR.position += vector3_3;
                    tmpCharacterInfoArray[i].vertex_BR.position += vector3_3;
                  }
                  else if (i < this.m_maxVisibleCharacters && index5 < this.m_maxVisibleLines && (this.m_overflowMode == TextOverflowModes.Page && (int) tmpCharacterInfoArray[i].pageNumber == index1))
                  {
                    tmpCharacterInfoArray[i].vertex_BL.position += vector3_3;
                    tmpCharacterInfoArray[i].vertex_TL.position += vector3_3;
                    tmpCharacterInfoArray[i].vertex_TR.position += vector3_3;
                    tmpCharacterInfoArray[i].vertex_BR.position += vector3_3;
                  }
                  else
                  {
                    tmpCharacterInfoArray[i].vertex_BL.position = Vector3.zero;
                    tmpCharacterInfoArray[i].vertex_TL.position = Vector3.zero;
                    tmpCharacterInfoArray[i].vertex_TR.position = Vector3.zero;
                    tmpCharacterInfoArray[i].vertex_BR.position = Vector3.zero;
                  }
                  if (tmpTextElementType == TMP_TextElementType.Character)
                    this.FillCharacterVertexBuffers(i, index_X4_1);
                  else if (tmpTextElementType == TMP_TextElementType.Sprite)
                    this.FillSpriteVertexBuffers(i, index_X4_2);
                }
                this.m_textInfo.characterInfo[i].bottomLeft += vector3_3;
                this.m_textInfo.characterInfo[i].topLeft += vector3_3;
                this.m_textInfo.characterInfo[i].topRight += vector3_3;
                this.m_textInfo.characterInfo[i].bottomRight += vector3_3;
                this.m_textInfo.characterInfo[i].origin += vector3_3.x;
                this.m_textInfo.characterInfo[i].ascender += vector3_3.y;
                this.m_textInfo.characterInfo[i].descender += vector3_3.y;
                this.m_textInfo.characterInfo[i].baseLine += vector3_3.y;
                if (!flag9)
                  ;
                if (index5 != index4 || i == this.m_characterCount - 1)
                {
                  if (index5 != index4)
                  {
                    this.m_textInfo.lineInfo[index4].lineExtents.min = new Vector2(this.m_textInfo.characterInfo[this.m_textInfo.lineInfo[index4].firstCharacterIndex].bottomLeft.x, this.m_textInfo.lineInfo[index4].descender);
                    this.m_textInfo.lineInfo[index4].lineExtents.max = new Vector2(this.m_textInfo.characterInfo[this.m_textInfo.lineInfo[index4].lastVisibleCharacterIndex].topRight.x, this.m_textInfo.lineInfo[index4].ascender);
                    this.m_textInfo.lineInfo[index4].baseline += vector3_3.y;
                    this.m_textInfo.lineInfo[index4].ascender += vector3_3.y;
                    this.m_textInfo.lineInfo[index4].descender += vector3_3.y;
                  }
                  if (i == this.m_characterCount - 1)
                  {
                    this.m_textInfo.lineInfo[index5].lineExtents.min = new Vector2(this.m_textInfo.characterInfo[this.m_textInfo.lineInfo[index5].firstCharacterIndex].bottomLeft.x, this.m_textInfo.lineInfo[index5].descender);
                    this.m_textInfo.lineInfo[index5].lineExtents.max = new Vector2(this.m_textInfo.characterInfo[this.m_textInfo.lineInfo[index5].lastVisibleCharacterIndex].topRight.x, this.m_textInfo.lineInfo[index5].ascender);
                    this.m_textInfo.lineInfo[index5].baseline += vector3_3.y;
                    this.m_textInfo.lineInfo[index5].ascender += vector3_3.y;
                    this.m_textInfo.lineInfo[index5].descender += vector3_3.y;
                  }
                }
                if (char.IsLetterOrDigit(c) || (int) c == 39 || (int) c == 8217)
                {
                  if (!flag7)
                  {
                    flag7 = true;
                    num13 = i;
                  }
                  if (flag7 && i == this.m_characterCount - 1)
                  {
                    int length = this.m_textInfo.wordInfo.Length;
                    int index6 = this.m_textInfo.wordCount;
                    if (this.m_textInfo.wordCount + 1 > length)
                      TMP_TextInfo.Resize<TMP_WordInfo>(ref this.m_textInfo.wordInfo, length + 1);
                    int num19 = i;
                    this.m_textInfo.wordInfo[index6].firstCharacterIndex = num13;
                    this.m_textInfo.wordInfo[index6].lastCharacterIndex = num19;
                    this.m_textInfo.wordInfo[index6].characterCount = num19 - num13 + 1;
                    this.m_textInfo.wordInfo[index6].textComponent = (TMP_Text) this;
                    ++num11;
                    ++this.m_textInfo.wordCount;
                    ++this.m_textInfo.lineInfo[index5].wordCount;
                  }
                }
                else if (flag7 || i == 0 && (!char.IsPunctuation(c) || char.IsWhiteSpace(c) || i == this.m_characterCount - 1))
                {
                  int num19 = i != this.m_characterCount - 1 || !char.IsLetterOrDigit(c) ? i - 1 : i;
                  flag7 = false;
                  int length = this.m_textInfo.wordInfo.Length;
                  int index6 = this.m_textInfo.wordCount;
                  if (this.m_textInfo.wordCount + 1 > length)
                    TMP_TextInfo.Resize<TMP_WordInfo>(ref this.m_textInfo.wordInfo, length + 1);
                  this.m_textInfo.wordInfo[index6].firstCharacterIndex = num13;
                  this.m_textInfo.wordInfo[index6].lastCharacterIndex = num19;
                  this.m_textInfo.wordInfo[index6].characterCount = num19 - num13 + 1;
                  this.m_textInfo.wordInfo[index6].textComponent = (TMP_Text) this;
                  ++num11;
                  ++this.m_textInfo.wordCount;
                  ++this.m_textInfo.lineInfo[index5].wordCount;
                }
                if ((this.m_textInfo.characterInfo[i].style & FontStyles.Underline) == FontStyles.Underline)
                {
                  bool flag10 = true;
                  int num19 = (int) this.m_textInfo.characterInfo[i].pageNumber;
                  if (i > this.m_maxVisibleCharacters || index5 > this.m_maxVisibleLines || this.m_overflowMode == TextOverflowModes.Page && num19 + 1 != this.m_pageToDisplay)
                    flag10 = false;
                  if (!char.IsWhiteSpace(c))
                  {
                    num15 = Mathf.Max(num15, this.m_textInfo.characterInfo[i].scale);
                    y1 = Mathf.Min(num19 != num16 ? float.PositiveInfinity : y1, this.m_textInfo.characterInfo[i].baseLine + this.font.fontInfo.Underline * num15);
                    num16 = num19;
                  }
                  if (!flag1 && flag10 && (i <= tmpLineInfo.lastVisibleCharacterIndex && (int) c != 10) && ((int) c != 13 && (i != tmpLineInfo.lastVisibleCharacterIndex || !char.IsSeparator(c))))
                  {
                    flag1 = true;
                    startScale = this.m_textInfo.characterInfo[i].scale;
                    if ((double) num15 == 0.0)
                      num15 = startScale;
                    start1 = new Vector3(this.m_textInfo.characterInfo[i].bottomLeft.x, y1, 0.0f);
                    underlineColor1 = this.m_textInfo.characterInfo[i].color;
                  }
                  if (flag1 && this.m_characterCount == 1)
                  {
                    flag1 = false;
                    end1 = new Vector3(this.m_textInfo.characterInfo[i].topRight.x, y1, 0.0f);
                    float endScale = this.m_textInfo.characterInfo[i].scale;
                    this.DrawUnderlineMesh(start1, end1, ref index3, startScale, endScale, num15, underlineColor1);
                    num15 = 0.0f;
                    y1 = float.PositiveInfinity;
                  }
                  else if (flag1 && (i == tmpLineInfo.lastCharacterIndex || i >= tmpLineInfo.lastVisibleCharacterIndex))
                  {
                    float endScale;
                    if (char.IsWhiteSpace(c))
                    {
                      int index6 = tmpLineInfo.lastVisibleCharacterIndex;
                      end1 = new Vector3(this.m_textInfo.characterInfo[index6].topRight.x, y1, 0.0f);
                      endScale = this.m_textInfo.characterInfo[index6].scale;
                    }
                    else
                    {
                      end1 = new Vector3(this.m_textInfo.characterInfo[i].topRight.x, y1, 0.0f);
                      endScale = this.m_textInfo.characterInfo[i].scale;
                    }
                    flag1 = false;
                    this.DrawUnderlineMesh(start1, end1, ref index3, startScale, endScale, num15, underlineColor1);
                    num15 = 0.0f;
                    y1 = float.PositiveInfinity;
                  }
                  else if (flag1 && !flag10)
                  {
                    flag1 = false;
                    end1 = new Vector3(this.m_textInfo.characterInfo[i - 1].topRight.x, y1, 0.0f);
                    float endScale = this.m_textInfo.characterInfo[i - 1].scale;
                    this.DrawUnderlineMesh(start1, end1, ref index3, startScale, endScale, num15, underlineColor1);
                    num15 = 0.0f;
                    y1 = float.PositiveInfinity;
                  }
                }
                else if (flag1)
                {
                  flag1 = false;
                  end1 = new Vector3(this.m_textInfo.characterInfo[i - 1].topRight.x, y1, 0.0f);
                  float endScale = this.m_textInfo.characterInfo[i - 1].scale;
                  this.DrawUnderlineMesh(start1, end1, ref index3, startScale, endScale, num15, underlineColor1);
                  num15 = 0.0f;
                  y1 = float.PositiveInfinity;
                }
                if ((this.m_textInfo.characterInfo[i].style & FontStyles.Strikethrough) == FontStyles.Strikethrough)
                {
                  bool flag10 = true;
                  if (i > this.m_maxVisibleCharacters || index5 > this.m_maxVisibleLines || this.m_overflowMode == TextOverflowModes.Page && (int) this.m_textInfo.characterInfo[i].pageNumber + 1 != this.m_pageToDisplay)
                    flag10 = false;
                  if (!flag2 && flag10 && (i <= tmpLineInfo.lastVisibleCharacterIndex && (int) c != 10) && ((int) c != 13 && (i != tmpLineInfo.lastVisibleCharacterIndex || !char.IsSeparator(c))))
                  {
                    flag2 = true;
                    num17 = this.m_textInfo.characterInfo[i].pointSize;
                    num18 = this.m_textInfo.characterInfo[i].scale;
                    start2 = new Vector3(this.m_textInfo.characterInfo[i].bottomLeft.x, this.m_textInfo.characterInfo[i].baseLine + (float) (((double) this.font.fontInfo.Ascender + (double) this.font.fontInfo.Descender) / 2.75) * num18, 0.0f);
                    underlineColor2 = this.m_textInfo.characterInfo[i].color;
                    b = this.m_textInfo.characterInfo[i].baseLine;
                  }
                  if (flag2 && this.m_characterCount == 1)
                  {
                    flag2 = false;
                    end2 = new Vector3(this.m_textInfo.characterInfo[i].topRight.x, this.m_textInfo.characterInfo[i].baseLine + (float) (((double) this.font.fontInfo.Ascender + (double) this.font.fontInfo.Descender) / 2.0) * num18, 0.0f);
                    this.DrawUnderlineMesh(start2, end2, ref index3, num18, num18, num18, underlineColor2);
                  }
                  else if (flag2 && i == tmpLineInfo.lastCharacterIndex)
                  {
                    if (char.IsWhiteSpace(c))
                    {
                      int index6 = tmpLineInfo.lastVisibleCharacterIndex;
                      end2 = new Vector3(this.m_textInfo.characterInfo[index6].topRight.x, this.m_textInfo.characterInfo[index6].baseLine + (float) (((double) this.font.fontInfo.Ascender + (double) this.font.fontInfo.Descender) / 2.0) * num18, 0.0f);
                    }
                    else
                      end2 = new Vector3(this.m_textInfo.characterInfo[i].topRight.x, this.m_textInfo.characterInfo[i].baseLine + (float) (((double) this.font.fontInfo.Ascender + (double) this.font.fontInfo.Descender) / 2.0) * num18, 0.0f);
                    flag2 = false;
                    this.DrawUnderlineMesh(start2, end2, ref index3, num18, num18, num18, underlineColor2);
                  }
                  else if (flag2 && i < this.m_characterCount && ((double) this.m_textInfo.characterInfo[i + 1].pointSize != (double) num17 || !TMP_Math.Approximately(this.m_textInfo.characterInfo[i + 1].baseLine + vector3_3.y, b)))
                  {
                    flag2 = false;
                    int index6 = tmpLineInfo.lastVisibleCharacterIndex;
                    end2 = i <= index6 ? new Vector3(this.m_textInfo.characterInfo[i].topRight.x, this.m_textInfo.characterInfo[i].baseLine + (float) (((double) this.font.fontInfo.Ascender + (double) this.font.fontInfo.Descender) / 2.0) * num18, 0.0f) : new Vector3(this.m_textInfo.characterInfo[index6].topRight.x, this.m_textInfo.characterInfo[index6].baseLine + (float) (((double) this.font.fontInfo.Ascender + (double) this.font.fontInfo.Descender) / 2.0) * num18, 0.0f);
                    this.DrawUnderlineMesh(start2, end2, ref index3, num18, num18, num18, underlineColor2);
                  }
                  else if (flag2 && !flag10)
                  {
                    flag2 = false;
                    end2 = new Vector3(this.m_textInfo.characterInfo[i - 1].topRight.x, this.m_textInfo.characterInfo[i - 1].baseLine + (float) (((double) this.font.fontInfo.Ascender + (double) this.font.fontInfo.Descender) / 2.0) * num18, 0.0f);
                    this.DrawUnderlineMesh(start2, end2, ref index3, num18, num18, num18, underlineColor2);
                  }
                }
                else if (flag2)
                {
                  flag2 = false;
                  end2 = new Vector3(this.m_textInfo.characterInfo[i - 1].topRight.x, this.m_textInfo.characterInfo[i - 1].baseLine + (float) (((double) this.font.fontInfo.Ascender + (double) this.font.fontInfo.Descender) / 2.0) * this.m_fontScale, 0.0f);
                  this.DrawUnderlineMesh(start2, end2, ref index3, num18, num18, num18, underlineColor2);
                }
                index4 = index5;
              }
              this.m_textInfo.characterCount = (int) (short) this.m_characterCount;
              this.m_textInfo.spriteCount = this.m_spriteCount;
              this.m_textInfo.lineCount = (int) (short) num12;
              this.m_textInfo.wordCount = num11 == 0 || this.m_characterCount <= 0 ? 1 : (int) (short) num11;
              this.m_textInfo.pageCount = this.m_pageNumber + 1;
              if (this.m_renderMode != TextRenderFlags.Render)
                return;
              UIVertex[] uiVertex1 = this.m_textInfo.meshInfo[0].GetUiVertex();
              this.m_canvasRenderer.SetVertices(uiVertex1, uiVertex1.Length);
              for (int index5 = 1; index5 < this.m_textInfo.materialCount; ++index5)
              {
                this.m_textInfo.meshInfo[index5].ClearUnusedVertices();
                this.m_subTextObjects[index5].mesh.MarkDynamic();
                this.m_subTextObjects[index5].mesh.vertices = this.m_textInfo.meshInfo[index5].vertices;
                this.m_subTextObjects[index5].mesh.uv = this.m_textInfo.meshInfo[index5].uvs0;
                this.m_subTextObjects[index5].mesh.uv2 = this.m_textInfo.meshInfo[index5].uvs2;
                this.m_subTextObjects[index5].mesh.colors32 = this.m_textInfo.meshInfo[index5].colors32;
                this.m_subTextObjects[index5].mesh.RecalculateBounds();
                UIVertex[] uiVertex2 = this.m_textInfo.meshInfo[index5].GetUiVertex();
                this.m_subTextObjects[index5].canvasRenderer.SetVertices(uiVertex2, uiVertex2.Length);
              }
            }
          }
        }
      }
    }

    protected override Vector3[] GetTextContainerLocalCorners()
    {
      if ((UnityEngine.Object) this.m_rectTransform == (UnityEngine.Object) null)
        this.m_rectTransform = this.rectTransform;
      this.m_rectTransform.GetLocalCorners(this.m_RectTransformCorners);
      return this.m_RectTransformCorners;
    }

    private void ClearMesh()
    {
      this.m_canvasRenderer.SetVertices((UIVertex[]) null, 0);
      int count = this.m_materialReferenceIndexLookup.Count;
      for (int index = 1; index < count; ++index)
        this.m_subTextObjects[index].canvasRenderer.SetVertices((UIVertex[]) null, 0);
    }

    private void UpdateSDFScale(float lossyScale)
    {
      lossyScale = (double) lossyScale != 0.0 ? lossyScale : 1f;
      float scaleFactor = this.m_canvas.scaleFactor;
      float num1 = this.m_canvas.renderMode != RenderMode.ScreenSpaceOverlay ? (this.m_canvas.renderMode != RenderMode.ScreenSpaceCamera ? lossyScale : (!((UnityEngine.Object) this.m_canvas.worldCamera != (UnityEngine.Object) null) ? 1f : lossyScale)) : lossyScale / scaleFactor;
      for (int index1 = 0; index1 < this.m_textInfo.characterCount; ++index1)
      {
        if (this.m_textInfo.characterInfo[index1].isVisible)
        {
          float num2 = (float) ((double) num1 * (double) this.m_textInfo.characterInfo[index1].scale * (1.0 - (double) this.m_charWidthAdjDelta));
          if ((this.m_textInfo.characterInfo[index1].style & FontStyles.Bold) == FontStyles.Bold)
            num2 *= -1f;
          int index2 = this.m_textInfo.characterInfo[index1].materialReferenceIndex;
          int index3 = (int) this.m_textInfo.characterInfo[index1].vertexIndex;
          this.m_textInfo.meshInfo[index2].uvs2[index3].y = num2;
          this.m_textInfo.meshInfo[index2].uvs2[index3 + 1].y = num2;
          this.m_textInfo.meshInfo[index2].uvs2[index3 + 2].y = num2;
          this.m_textInfo.meshInfo[index2].uvs2[index3 + 3].y = num2;
        }
      }
      for (int index = 0; index < this.m_textInfo.materialCount; ++index)
      {
        if (index == 0)
        {
          UIVertex[] uiVertex = this.m_textInfo.meshInfo[0].GetUiVertex();
          this.m_canvasRenderer.SetVertices(uiVertex, uiVertex.Length);
        }
        else
        {
          UIVertex[] uiVertex = this.m_textInfo.meshInfo[index].GetUiVertex();
          this.m_subTextObjects[index].canvasRenderer.SetVertices(uiVertex, uiVertex.Length);
        }
      }
    }

    protected override void AdjustLineOffset(int startIndex, int endIndex, float offset)
    {
      Vector3 vector3 = new Vector3(0.0f, offset, 0.0f);
      for (int index = startIndex; index <= endIndex; ++index)
      {
        this.m_textInfo.characterInfo[index].bottomLeft -= vector3;
        this.m_textInfo.characterInfo[index].topLeft -= vector3;
        this.m_textInfo.characterInfo[index].topRight -= vector3;
        this.m_textInfo.characterInfo[index].bottomRight -= vector3;
        this.m_textInfo.characterInfo[index].ascender -= vector3.y;
        this.m_textInfo.characterInfo[index].baseLine -= vector3.y;
        this.m_textInfo.characterInfo[index].descender -= vector3.y;
        if (this.m_textInfo.characterInfo[index].isVisible)
        {
          this.m_textInfo.characterInfo[index].vertex_BL.position -= vector3;
          this.m_textInfo.characterInfo[index].vertex_TL.position -= vector3;
          this.m_textInfo.characterInfo[index].vertex_TR.position -= vector3;
          this.m_textInfo.characterInfo[index].vertex_BR.position -= vector3;
        }
      }
    }

    public void CalculateLayoutInputHorizontal()
    {
      if (!this.gameObject.activeInHierarchy || !this.m_isCalculateSizeRequired && !this.m_rectTransform.hasChanged)
        return;
      this.m_preferredWidth = this.GetPreferredWidth();
      this.ComputeMarginSize();
      this.m_isLayoutDirty = true;
    }

    public void CalculateLayoutInputVertical()
    {
      if (!this.gameObject.activeInHierarchy)
        return;
      if (this.m_isCalculateSizeRequired || this.m_rectTransform.hasChanged)
      {
        this.m_preferredHeight = this.GetPreferredHeight();
        this.ComputeMarginSize();
        this.m_isLayoutDirty = true;
      }
      this.m_isCalculateSizeRequired = false;
    }

    public override void SetVerticesDirty()
    {
      if (this.m_verticesAlreadyDirty || !this.IsActive())
        return;
      this.m_verticesAlreadyDirty = true;
      CanvasUpdateRegistry.RegisterCanvasElementForGraphicRebuild((ICanvasElement) this);
    }

    public override void SetLayoutDirty()
    {
      if (this.m_layoutAlreadyDirty || !this.IsActive())
        return;
      this.m_layoutAlreadyDirty = true;
      LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
      this.m_isLayoutDirty = true;
    }

    public override void SetMaterialDirty()
    {
      if (!this.IsActive())
        return;
      this.m_isMaterialDirty = true;
      CanvasUpdateRegistry.RegisterCanvasElementForGraphicRebuild((ICanvasElement) this);
    }

    public override void SetAllDirty()
    {
      this.SetLayoutDirty();
      this.SetVerticesDirty();
      this.SetMaterialDirty();
    }

    public override void Rebuild(CanvasUpdate update)
    {
      if (update != CanvasUpdate.PreRender)
        return;
      this.OnPreRenderCanvas();
      this.m_verticesAlreadyDirty = false;
      this.m_layoutAlreadyDirty = false;
      if (!this.m_isMaterialDirty)
        return;
      this.UpdateMaterial();
      this.m_isMaterialDirty = false;
    }

    private void UpdateSubObjectPivot()
    {
      if (this.m_textInfo == null)
        return;
      for (int index = 1; index < this.m_textInfo.materialCount; ++index)
        this.m_subTextObjects[index].SetPivotDirty();
    }

    public Material GetModifiedMaterial(Material baseMaterial)
    {
      Material material = baseMaterial;
      if (this.m_ShouldRecalculate)
      {
        this.m_stencilID = MaterialManager.GetStencilID(this.gameObject);
        this.m_ShouldRecalculate = false;
      }
      if (this.m_stencilID > 0)
      {
        material = MaterialManager.GetStencilMaterial(baseMaterial, this.m_stencilID);
        if ((UnityEngine.Object) this.m_MaskMaterial != (UnityEngine.Object) null)
          MaterialManager.ReleaseStencilMaterial(this.m_MaskMaterial);
        this.m_MaskMaterial = material;
      }
      return material;
    }

    protected override void UpdateMaterial()
    {
      if (!this.IsActive())
        return;
      if ((UnityEngine.Object) this.m_canvasRenderer == (UnityEngine.Object) null)
        this.m_canvasRenderer = this.canvasRenderer;
      this.m_canvasRenderer.SetMaterial(this.materialForRendering, this.m_sharedMaterial.mainTexture);
    }

    public override void ParentMaskStateChanged()
    {
      this.m_ShouldRecalculate = true;
      this.SetMaterialDirty();
    }

    public override void UpdateMeshPadding()
    {
      this.m_padding = ShaderUtilities.GetPadding(this.m_sharedMaterial, this.m_enableExtraPadding, this.m_isUsingBold);
      this.m_isMaskingEnabled = ShaderUtilities.IsMaskingEnabled(this.m_sharedMaterial);
      this.m_havePropertiesChanged = true;
      this.checkPaddingRequired = false;
      for (int index = 1; index < this.m_textInfo.materialCount; ++index)
        this.m_subTextObjects[index].UpdateMeshPadding(this.m_enableExtraPadding, this.m_isUsingBold);
    }

    public override void ForceMeshUpdate()
    {
      this.OnPreRenderCanvas();
    }

    public override void UpdateGeometry(Mesh mesh, int index)
    {
      mesh.RecalculateBounds();
      if (index == 0)
      {
        UIVertex[] uiVertex = this.m_textInfo.meshInfo[0].GetUiVertex();
        this.m_canvasRenderer.SetVertices(uiVertex, uiVertex.Length);
      }
      else
      {
        UIVertex[] uiVertex = this.m_textInfo.meshInfo[index].GetUiVertex();
        this.m_subTextObjects[index].canvasRenderer.SetVertices(uiVertex, uiVertex.Length);
      }
    }

    public override void UpdateVertexData()
    {
      int num = this.m_textInfo.materialCount;
      for (int index = 0; index < num; ++index)
      {
        if (index == 0)
        {
          UIVertex[] uiVertex = this.m_textInfo.meshInfo[0].GetUiVertex();
          this.m_canvasRenderer.SetVertices(uiVertex, uiVertex.Length);
        }
        else
        {
          UIVertex[] uiVertex = this.m_textInfo.meshInfo[index].GetUiVertex();
          this.m_subTextObjects[index].canvasRenderer.SetVertices(uiVertex, uiVertex.Length);
        }
      }
    }

    public void UpdateFontAsset()
    {
      this.LoadFontAsset();
    }

    float ILayoutElement.get_minWidth()
    {
      return this.minWidth;
    }

    float ILayoutElement.get_flexibleWidth()
    {
      return this.flexibleWidth;
    }

    float ILayoutElement.get_minHeight()
    {
      return this.minHeight;
    }

    float ILayoutElement.get_flexibleHeight()
    {
      return this.flexibleHeight;
    }

    int ILayoutElement.get_layoutPriority()
    {
      return this.layoutPriority;
    }
  }
}
