﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_Style
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace TMPro
{
  [Serializable]
  public class TMP_Style
  {
    [SerializeField]
    private string m_Name;
    [SerializeField]
    private int m_HashCode;
    [SerializeField]
    private string m_OpeningDefinition;
    [SerializeField]
    private string m_ClosingDefinition;
    [SerializeField]
    private int[] m_OpeningTagArray;
    [SerializeField]
    private int[] m_ClosingTagArray;

    public string name
    {
      get
      {
        return this.m_Name;
      }
      set
      {
        if (!(value != this.m_Name))
          return;
        this.m_Name = value;
      }
    }

    public int hashCode
    {
      get
      {
        return this.m_HashCode;
      }
      set
      {
        if (value == this.m_HashCode)
          return;
        this.m_HashCode = value;
      }
    }

    public string styleOpeningDefinition
    {
      get
      {
        return this.m_OpeningDefinition;
      }
    }

    public string styleClosingDefinition
    {
      get
      {
        return this.m_ClosingDefinition;
      }
    }

    public int[] styleOpeningTagArray
    {
      get
      {
        return this.m_OpeningTagArray;
      }
    }

    public int[] styleClosingTagArray
    {
      get
      {
        return this.m_ClosingTagArray;
      }
    }

    public void RefreshStyle()
    {
      this.m_HashCode = TMP_TextUtilities.GetSimpleHashCode(this.m_Name);
      this.m_OpeningTagArray = new int[this.m_OpeningDefinition.Length];
      for (int index = 0; index < this.m_OpeningDefinition.Length; ++index)
        this.m_OpeningTagArray[index] = (int) this.m_OpeningDefinition[index];
      this.m_ClosingTagArray = new int[this.m_ClosingDefinition.Length];
      for (int index = 0; index < this.m_ClosingDefinition.Length; ++index)
        this.m_ClosingTagArray[index] = (int) this.m_ClosingDefinition[index];
      TMPro_EventManager.ON_TEXT_STYLE_PROPERTY_CHANGED(true);
    }
  }
}
