﻿// Decompiled with JetBrains decompiler
// Type: TMPro.FastAction`2
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace TMPro
{
  public class FastAction<A, B>
  {
    private LinkedList<Action<A, B>> delegates = new LinkedList<Action<A, B>>();
    private Dictionary<Action<A, B>, LinkedListNode<Action<A, B>>> lookup = new Dictionary<Action<A, B>, LinkedListNode<Action<A, B>>>();

    public void Add(Action<A, B> rhs)
    {
      if (this.lookup.ContainsKey(rhs))
        return;
      this.lookup[rhs] = this.delegates.AddLast(rhs);
    }

    public void Remove(Action<A, B> rhs)
    {
      LinkedListNode<Action<A, B>> node;
      if (!this.lookup.TryGetValue(rhs, out node))
        return;
      this.lookup.Remove(rhs);
      this.delegates.Remove(node);
    }

    public void Call(A a, B b)
    {
      for (LinkedListNode<Action<A, B>> linkedListNode = this.delegates.First; linkedListNode != null; linkedListNode = linkedListNode.Next)
        linkedListNode.Value(a, b);
    }
  }
}
