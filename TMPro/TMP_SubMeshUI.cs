﻿// Decompiled with JetBrains decompiler
// Type: TMPro.TMP_SubMeshUI
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.UI;

namespace TMPro
{
  [ExecuteInEditMode]
  public class TMP_SubMeshUI : MaskableGraphic
  {
    [SerializeField]
    private TMP_FontAsset m_fontAsset;
    [SerializeField]
    private TMP_SpriteAsset m_spriteAsset;
    [SerializeField]
    private Material m_material;
    [SerializeField]
    private Material m_sharedMaterial;
    [SerializeField]
    private bool m_isDefaultMaterial;
    [SerializeField]
    private float m_padding;
    [SerializeField]
    private CanvasRenderer m_canvasRenderer;
    private Mesh m_mesh;
    [SerializeField]
    private TextMeshProUGUI m_TextComponent;
    [NonSerialized]
    private bool m_isRegisteredForEvents;
    private bool m_materialDirty;
    [SerializeField]
    private int m_materialReferenceIndex;

    public TMP_FontAsset fontAsset
    {
      get
      {
        return this.m_fontAsset;
      }
      set
      {
        this.m_fontAsset = value;
      }
    }

    public TMP_SpriteAsset spriteAsset
    {
      get
      {
        return this.m_spriteAsset;
      }
      set
      {
        this.m_spriteAsset = value;
      }
    }

    public override Texture mainTexture
    {
      get
      {
        if ((UnityEngine.Object) this.sharedMaterial != (UnityEngine.Object) null)
          return this.sharedMaterial.mainTexture;
        return (Texture) null;
      }
    }

    public override Material material
    {
      get
      {
        return this.GetMaterial(this.m_sharedMaterial);
      }
      set
      {
        if (this.m_sharedMaterial.GetInstanceID() == value.GetInstanceID())
          return;
        this.m_sharedMaterial = value;
        this.m_padding = this.GetPaddingForMaterial();
        this.SetVerticesDirty();
        this.SetMaterialDirty();
      }
    }

    public Material sharedMaterial
    {
      get
      {
        return this.m_sharedMaterial;
      }
      set
      {
        this.SetSharedMaterial(value);
      }
    }

    public override Material materialForRendering
    {
      get
      {
        if ((UnityEngine.Object) this.m_sharedMaterial == (UnityEngine.Object) null)
          return (Material) null;
        return this.GetModifiedMaterial(this.m_sharedMaterial);
      }
    }

    public bool isDefaultMaterial
    {
      get
      {
        return this.m_isDefaultMaterial;
      }
      set
      {
        this.m_isDefaultMaterial = value;
      }
    }

    public float padding
    {
      get
      {
        return this.m_padding;
      }
      set
      {
        this.m_padding = value;
      }
    }

    public new CanvasRenderer canvasRenderer
    {
      get
      {
        if ((UnityEngine.Object) this.m_canvasRenderer == (UnityEngine.Object) null)
          this.m_canvasRenderer = this.GetComponent<CanvasRenderer>();
        return this.m_canvasRenderer;
      }
    }

    public Mesh mesh
    {
      get
      {
        if ((UnityEngine.Object) this.m_mesh == (UnityEngine.Object) null)
        {
          this.m_mesh = new Mesh();
          this.m_mesh.hideFlags = HideFlags.HideAndDontSave;
        }
        return this.m_mesh;
      }
      set
      {
        this.m_mesh = value;
      }
    }

    public static TMP_SubMeshUI AddSubTextObject(TextMeshProUGUI textComponent, MaterialReference materialReference)
    {
      GameObject gameObject = new GameObject("TMP UI SubObject [" + materialReference.material.name + "]");
      gameObject.transform.SetParent(textComponent.transform, false);
      gameObject.layer = textComponent.gameObject.layer;
      RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
      rectTransform.anchorMin = Vector2.zero;
      rectTransform.anchorMax = Vector2.one;
      rectTransform.sizeDelta = Vector2.zero;
      rectTransform.pivot = textComponent.rectTransform.pivot;
      TMP_SubMeshUI tmpSubMeshUi = gameObject.AddComponent<TMP_SubMeshUI>();
      tmpSubMeshUi.m_canvasRenderer = tmpSubMeshUi.canvasRenderer;
      tmpSubMeshUi.m_TextComponent = textComponent;
      tmpSubMeshUi.m_materialReferenceIndex = materialReference.index;
      tmpSubMeshUi.m_fontAsset = materialReference.fontAsset;
      tmpSubMeshUi.m_spriteAsset = materialReference.spriteAsset;
      tmpSubMeshUi.m_isDefaultMaterial = materialReference.isDefaultMaterial;
      tmpSubMeshUi.SetSharedMaterial(materialReference.material);
      return tmpSubMeshUi;
    }

    protected override void OnEnable()
    {
      if (!this.m_isRegisteredForEvents)
        this.m_isRegisteredForEvents = true;
      this.m_ShouldRecalculate = true;
      this.ParentMaskStateChanged();
    }

    protected override void OnDisable()
    {
      TMP_UpdateRegistry.UnRegisterCanvasElementForRebuild((ICanvasElement) this);
      if ((UnityEngine.Object) this.m_MaskMaterial != (UnityEngine.Object) null)
      {
        MaterialManager.ReleaseStencilMaterial(this.m_MaskMaterial);
        this.m_MaskMaterial = (Material) null;
      }
      base.OnDisable();
    }

    protected override void OnDestroy()
    {
      if ((UnityEngine.Object) this.m_mesh != (UnityEngine.Object) null)
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.m_mesh);
      if ((UnityEngine.Object) this.m_MaskMaterial != (UnityEngine.Object) null)
        MaterialManager.ReleaseStencilMaterial(this.m_MaskMaterial);
      this.m_isRegisteredForEvents = false;
    }

    protected override void OnTransformParentChanged()
    {
      if (!this.IsActive())
        return;
      this.m_ShouldRecalculate = true;
      this.ParentMaskStateChanged();
    }

    public Material GetModifiedMaterial(Material baseMaterial)
    {
      Material material = baseMaterial;
      if (this.m_ShouldRecalculate)
      {
        this.m_StencilValue = MaterialManager.GetStencilID(this.gameObject);
        this.m_ShouldRecalculate = false;
      }
      if (this.m_StencilValue > 0)
      {
        material = MaterialManager.GetStencilMaterial(baseMaterial, this.m_StencilValue);
        if ((UnityEngine.Object) this.m_MaskMaterial != (UnityEngine.Object) null)
          MaterialManager.ReleaseStencilMaterial(this.m_MaskMaterial);
        this.m_MaskMaterial = material;
      }
      return material;
    }

    public float GetPaddingForMaterial()
    {
      return ShaderUtilities.GetPadding(this.m_sharedMaterial, this.m_TextComponent.extraPadding, this.m_TextComponent.isUsingBold);
    }

    public float GetPaddingForMaterial(Material mat)
    {
      return ShaderUtilities.GetPadding(mat, this.m_TextComponent.extraPadding, this.m_TextComponent.isUsingBold);
    }

    public void UpdateMeshPadding(bool isExtraPadding, bool isUsingBold)
    {
      this.m_padding = ShaderUtilities.GetPadding(this.m_sharedMaterial, isExtraPadding, isUsingBold);
    }

    public override void SetAllDirty()
    {
    }

    public override void SetVerticesDirty()
    {
      if (!this.IsActive() || !((UnityEngine.Object) this.m_TextComponent != (UnityEngine.Object) null))
        return;
      this.m_TextComponent.havePropertiesChanged = true;
      this.m_TextComponent.SetVerticesDirty();
    }

    public override void SetLayoutDirty()
    {
    }

    public override void SetMaterialDirty()
    {
      if (!this.IsActive())
        return;
      this.m_materialDirty = true;
      this.UpdateMaterial();
    }

    public void SetPivotDirty()
    {
      if (!this.IsActive())
        return;
      this.rectTransform.pivot = this.m_TextComponent.rectTransform.pivot;
    }

    protected override void UpdateGeometry()
    {
    }

    public override void Rebuild(CanvasUpdate update)
    {
      if (update != CanvasUpdate.PreRender || !this.m_materialDirty)
        return;
      this.UpdateMaterial();
      this.m_materialDirty = false;
    }

    public void RefreshMaterial()
    {
      this.UpdateMaterial();
    }

    protected override void UpdateMaterial()
    {
      if (!this.IsActive())
        return;
      if ((UnityEngine.Object) this.m_canvasRenderer == (UnityEngine.Object) null)
        this.m_canvasRenderer = this.canvasRenderer;
      this.m_canvasRenderer.SetMaterial(this.materialForRendering, this.mainTexture);
    }

    public override void ParentMaskStateChanged()
    {
      this.m_ShouldRecalculate = true;
      this.SetMaterialDirty();
    }

    private Material GetMaterial()
    {
      return this.m_sharedMaterial;
    }

    private Material GetMaterial(Material mat)
    {
      if ((UnityEngine.Object) this.m_material == (UnityEngine.Object) null || this.m_material.GetInstanceID() != mat.GetInstanceID())
        this.m_material = this.CreateMaterialInstance(mat);
      this.m_sharedMaterial = this.m_material;
      this.m_padding = this.GetPaddingForMaterial();
      this.SetVerticesDirty();
      this.SetMaterialDirty();
      return this.m_sharedMaterial;
    }

    private Material CreateMaterialInstance(Material source)
    {
      Material material = new Material(source);
      material.shaderKeywords = source.shaderKeywords;
      material.name += " (Instance)";
      return material;
    }

    private Material GetSharedMaterial()
    {
      if ((UnityEngine.Object) this.m_canvasRenderer == (UnityEngine.Object) null)
        this.m_canvasRenderer = this.GetComponent<CanvasRenderer>();
      return this.m_canvasRenderer.GetMaterial();
    }

    private void SetSharedMaterial(Material mat)
    {
      this.m_sharedMaterial = mat;
      this.m_Material = this.m_sharedMaterial;
      this.m_padding = this.GetPaddingForMaterial();
      this.SetMaterialDirty();
    }
  }
}
