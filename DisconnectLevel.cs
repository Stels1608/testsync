﻿// Decompiled with JetBrains decompiler
// Type: DisconnectLevel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DisconnectLevel : GameLevel
{
  private string message;

  public override GameLevelType LevelType
  {
    get
    {
      return GameLevelType.DisconnectLevel;
    }
  }

  public override bool IsIngameLevel
  {
    get
    {
      return false;
    }
  }

  public static DisconnectLevel GetLevel()
  {
    return GameLevel.Instance as DisconnectLevel;
  }

  protected override void AddLoadingScreenDependencies()
  {
  }

  public override void Initialize(LevelProfile levelProfile)
  {
    base.Initialize(levelProfile);
    this.message = (levelProfile as DisconnectLevelProfile).Message;
  }

  protected override void OnGUI()
  {
    base.OnGUI();
    GUI.Box(new Rect((float) ((Screen.width - 600) / 2), (float) ((Screen.height - 400) / 2), 600f, 400f), this.message, new GUIStyle(GUI.skin.box)
    {
      wordWrap = true,
      alignment = TextAnchor.UpperCenter
    });
  }
}
