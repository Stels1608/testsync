﻿// Decompiled with JetBrains decompiler
// Type: CutsceneAssetSpawnerRemote
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public static class CutsceneAssetSpawnerRemote
{
  public static bool SpawnAssetAtLocator(string prefabName, GameObject locator, bool spawnWithJumpIn = false, bool playJumpInSound = false, JumpEffectNew.JumpEffectSize jumpEffectSize = JumpEffectNew.JumpEffectSize.Undefined)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CutsceneAssetSpawnerRemote.\u003CSpawnAssetAtLocator\u003Ec__AnonStorey5E locatorCAnonStorey5E = new CutsceneAssetSpawnerRemote.\u003CSpawnAssetAtLocator\u003Ec__AnonStorey5E();
    // ISSUE: reference to a compiler-generated field
    locatorCAnonStorey5E.jumpEffectSize = jumpEffectSize;
    // ISSUE: reference to a compiler-generated field
    locatorCAnonStorey5E.playJumpInSound = playJumpInSound;
    // ISSUE: reference to a compiler-generated field
    locatorCAnonStorey5E.spawnWithJumpIn = spawnWithJumpIn;
    if ((Object) locator == (Object) null)
    {
      Debug.LogError((object) "CutsceneShipSpawner.SpawnShipAtLocator(): given locator is null");
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    locatorCAnonStorey5E.rootScript = new GameObject("Cutscene_" + prefabName)
    {
      transform = {
        parent = locator.transform,
        localScale = Vector3.one,
        localPosition = Vector3.zero,
        localRotation = Quaternion.identity
      }
    }.AddComponent<CutsceneRootScript>();
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    locatorCAnonStorey5E.rootScript.ModelWasSetCallback += new AnonymousDelegate(locatorCAnonStorey5E.\u003C\u003Em__2C);
    // ISSUE: reference to a compiler-generated field
    locatorCAnonStorey5E.rootScript.Construct(prefabName);
    return true;
  }
}
