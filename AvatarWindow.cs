﻿// Decompiled with JetBrains decompiler
// Type: AvatarWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public abstract class AvatarWindow : MonoBehaviour, ILoadable
{
  private int humanHairbeardMatCount = 5;
  private int humanHairbeardMatIndex = -1;
  protected readonly Flag avatarLoaded = new Flag();
  private const string CUSTOMIZE_PROPERTY_PREFAB = "CharacterMenu/CustomizeAvatarOption";
  protected Avatar avatar;
  public GameObject cameraCylon;
  public GameObject cameraHuman;
  protected Faction faction;
  public Toggle femaleToggleButton;
  private Gender gender;
  public GameObject genderPanel;
  public GameObject lightCylon;
  public GameObject lightHuman;
  public Toggle maleToggleButton;
  public Transform propertyAnchor;

  public Flag IsLoaded
  {
    get
    {
      return this.avatarLoaded;
    }
  }

  protected abstract void Localize();

  protected abstract IEnumerator Resize();

  protected virtual void Start()
  {
    this.femaleToggleButton.onValueChanged.AddListener(new UnityAction<bool>(this.OnFemaleSelected));
    this.maleToggleButton.onValueChanged.AddListener(new UnityAction<bool>(this.OnMaleSelected));
  }

  public void OnRoomPrefabLoaded(Faction faction)
  {
    this.faction = faction;
    if (faction == Faction.Colonial)
    {
      this.lightHuman.SetActive(true);
      this.cameraHuman.SetActive(true);
    }
    else if (faction == Faction.Cylon)
    {
      this.lightCylon.SetActive(true);
      this.cameraCylon.SetActive(true);
    }
    else
      Debug.LogError((object) ("Invalid faction in character creation: " + (object) faction));
    Flag flag = new Flag();
    flag.Depend(new ILoadable[1]
    {
      (ILoadable) StaticCards.Avatar
    });
    flag.Set();
    flag.IsLoaded.AddHandler(new SignalHandler(this.Init));
    this.Localize();
  }

  private void Update()
  {
    if (this.avatar == null)
      return;
    this.avatar.Update();
  }

  protected void LockUi(bool locked)
  {
    foreach (Selectable componentsInChild in this.GetComponentsInChildren<Selectable>())
      componentsInChild.interactable = !locked;
  }

  protected virtual void OnAvatarLoaded()
  {
    this.avatarLoaded.Set();
    this.LockUi(false);
  }

  protected CustomizeAvatarOption CreateCustomizeUiElement()
  {
    return UguiTools.CreateChild<CustomizeAvatarOption>("CharacterMenu/CustomizeAvatarOption", this.propertyAnchor);
  }

  private void OnFemaleSelected(bool selected)
  {
    if (!selected)
      return;
    this.gender = Gender.Female;
    this.avatar.Destroy();
    this.CreateHuman("female");
  }

  private void OnMaleSelected(bool selected)
  {
    if (!selected)
      return;
    this.gender = Gender.Male;
    this.avatar.Destroy();
    this.CreateHuman("male");
  }

  private void CreateHuman(string gender)
  {
    this.avatarLoaded.Unset();
    this.avatar = AvatarSelector.Create("human", gender, Vector3.zero, Quaternion.identity);
    this.avatar.loadedCallback = new System.Action(this.OnAvatarLoaded);
    AvatarSelector.ClearAvatar(this.avatar);
    this.HumanCustomize();
    this.humanHairbeardMatCount = AvatarSelector.GetPropertyCount(this.avatar.Race, this.avatar.Sex, AvatarInfo.GetMaterialFromItem("hair"));
    this.genderPanel.SetActive(true);
  }

  private void CreateCylon()
  {
    this.avatarLoaded.Unset();
    this.avatar = AvatarSelector.Create("cylon", "centurion", Vector3.zero, Quaternion.AngleAxis(57.29578f, Vector3.up));
    this.avatar.loadedCallback = new System.Action(this.OnAvatarLoaded);
    AvatarSelector.ClearAvatar(this.avatar);
    this.CylonCustomize();
    this.genderPanel.SetActive(false);
  }

  protected static string GetTextForProp(string prop)
  {
    string key1 = (string) null;
    string key2 = prop;
    if (key2 != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AvatarWindow.\u003C\u003Ef__switch\u0024map6 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AvatarWindow.\u003C\u003Ef__switch\u0024map6 = new Dictionary<string, int>(16)
        {
          {
            "hair",
            0
          },
          {
            "hair_color",
            1
          },
          {
            "head",
            2
          },
          {
            "skin",
            3
          },
          {
            "suit",
            4
          },
          {
            "helmet",
            5
          },
          {
            "glasses",
            6
          },
          {
            "beard",
            7
          },
          {
            "head_style",
            8
          },
          {
            "head_color",
            9
          },
          {
            "arms_style",
            10
          },
          {
            "arms_color",
            11
          },
          {
            "body_style",
            12
          },
          {
            "body_color",
            13
          },
          {
            "legs_style",
            14
          },
          {
            "legs_color",
            15
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AvatarWindow.\u003C\u003Ef__switch\u0024map6.TryGetValue(key2, out num))
      {
        switch (num)
        {
          case 0:
            key1 = "bgo.gui_avatar_human.label";
            break;
          case 1:
            key1 = "bgo.gui_avatar_human.label_2";
            break;
          case 2:
            key1 = "bgo.gui_avatar_human.label_3";
            break;
          case 3:
            key1 = "bgo.gui_avatar_human.label_4";
            break;
          case 4:
            key1 = "bgo.gui_avatar_human.label_5";
            break;
          case 5:
            key1 = "bgo.gui_avatar_human.label_6";
            break;
          case 6:
            key1 = "bgo.gui_avatar_human.label_7";
            break;
          case 7:
            key1 = "bgo.gui_avatar_human.beard_label";
            break;
          case 8:
            key1 = "bgo.gui_avatar_cylon.label_7";
            break;
          case 9:
            key1 = "bgo.gui_avatar_cylon.label_6";
            break;
          case 10:
            key1 = "bgo.gui_avatar_cylon.label";
            break;
          case 11:
            key1 = "bgo.gui_avatar_cylon.label_2";
            break;
          case 12:
            key1 = "bgo.gui_avatar_cylon.label_3";
            break;
          case 13:
            key1 = "bgo.gui_avatar_cylon.label_4";
            break;
          case 14:
            key1 = "bgo.gui_avatar_cylon.label_5";
            break;
          case 15:
            key1 = "bgo.gui_avatar_cylon.beard_label";
            break;
        }
      }
    }
    return BsgoLocalization.Get(key1);
  }

  protected virtual void Init()
  {
    switch (this.faction)
    {
      case Faction.Colonial:
        this.CreateHuman("male");
        Avatar.LoadObj("human", "female");
        break;
      case Faction.Cylon:
        this.CreateCylon();
        break;
      default:
        Debug.LogError((object) ("Invalid faction in character creation: " + (object) this.faction));
        break;
    }
    this.StartCoroutine(this.Resize());
  }

  private void HumanMakeButtonsForProp(string prop, CustomizeAvatarOption uiElement)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AvatarWindow.\u003CHumanMakeButtonsForProp\u003Ec__AnonStorey73 propCAnonStorey73 = new AvatarWindow.\u003CHumanMakeButtonsForProp\u003Ec__AnonStorey73();
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey73.prop = prop;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey73.uiElement = uiElement;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey73.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey73.i = 0;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey73.size = AvatarSelector.GetPropertyCount(this.avatar.Race, this.avatar.Sex, propCAnonStorey73.prop);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey73.uiElement.UpdateIndex(1, propCAnonStorey73.size);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey73.uiElement.propertyNameText.text = AvatarWindow.GetTextForProp(propCAnonStorey73.prop);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    propCAnonStorey73.uiElement.rightButton.onClick.AddListener(new UnityAction(propCAnonStorey73.\u003C\u003Em__5C));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    propCAnonStorey73.uiElement.leftButton.onClick.AddListener(new UnityAction(propCAnonStorey73.\u003C\u003Em__5D));
  }

  private void HumanMakeButtonsForProp(string prop, string subProp1, string subProp2, CustomizeAvatarOption uiElement)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AvatarWindow.\u003CHumanMakeButtonsForProp\u003Ec__AnonStorey74 propCAnonStorey74 = new AvatarWindow.\u003CHumanMakeButtonsForProp\u003Ec__AnonStorey74();
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey74.subProp1 = subProp1;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey74.subProp2 = subProp2;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey74.uiElement = uiElement;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey74.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey74.i = 0;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey74.size = AvatarSelector.GetPropertyCount(this.avatar.Race, this.avatar.Sex, propCAnonStorey74.subProp1);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey74.uiElement.UpdateIndex(1, propCAnonStorey74.size);
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey74.uiElement.propertyNameText.text = AvatarWindow.GetTextForProp(prop);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    propCAnonStorey74.uiElement.rightButton.onClick.AddListener(new UnityAction(propCAnonStorey74.\u003C\u003Em__5E));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    propCAnonStorey74.uiElement.leftButton.onClick.AddListener(new UnityAction(propCAnonStorey74.\u003C\u003Em__5F));
  }

  private void ClearCustomizeButtons()
  {
    foreach (Component component in this.propertyAnchor)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    this.propertyAnchor.DetachChildren();
  }

  protected void HumanCustomize()
  {
    this.ClearCustomizeButtons();
    this.HumanMakeButtonsForProp("hair", this.CreateCustomizeUiElement());
    this.HumanMakeButtonsForProp("hair_color", AvatarInfo.GetMaterialFromItem("hair"), AvatarInfo.GetMaterialFromItem("beard"), this.CreateCustomizeUiElement());
    this.HumanMakeButtonsForProp("head", this.CreateCustomizeUiElement());
    this.HumanMakeButtonsForProp("skin", AvatarInfo.GetTextureFromItem("faces"), AvatarInfo.GetTextureFromItem("hands"), this.CreateCustomizeUiElement());
    this.HumanMakeButtonsForProp("suit", this.CreateCustomizeUiElement());
    this.HumanMakeButtonsForProp("helmet", this.CreateCustomizeUiElement());
    this.HumanMakeButtonsForProp("glasses", this.CreateCustomizeUiElement());
    if (this.gender != Gender.Male)
      return;
    this.HumanMakeButtonsForProp("beard", this.CreateCustomizeUiElement());
  }

  private void CylonMakeButtonsForProp(string prop, string propType, int size, CustomizeAvatarOption uiElement)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    AvatarWindow.\u003CCylonMakeButtonsForProp\u003Ec__AnonStorey75 propCAnonStorey75 = new AvatarWindow.\u003CCylonMakeButtonsForProp\u003Ec__AnonStorey75();
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey75.size = size;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey75.propType = propType;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey75.prop = prop;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey75.uiElement = uiElement;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey75.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey75.i = 0;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey75.uiElement.UpdateIndex(1, propCAnonStorey75.size);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    propCAnonStorey75.uiElement.propertyNameText.text = AvatarWindow.GetTextForProp(propCAnonStorey75.prop + "_" + propCAnonStorey75.propType);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    propCAnonStorey75.uiElement.rightButton.onClick.AddListener(new UnityAction(propCAnonStorey75.\u003C\u003Em__60));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    propCAnonStorey75.uiElement.leftButton.onClick.AddListener(new UnityAction(propCAnonStorey75.\u003C\u003Em__61));
  }

  private void CylonChangeItem(string propType, string property, bool next)
  {
    this.avatar.Lock();
    if (propType == "item")
    {
      this.ChangeItem(property, next);
      AvatarCylonCustomizer.UpdateItemsVersion();
    }
    else if (propType == "color")
      AvatarCylonCustomizer.NextItemColor(property, next);
    else if (propType == "texture")
      AvatarCylonCustomizer.NextItemMaterial(property, next);
    else if (propType == "style" && AvatarCylonCustomizer.NextItemMaterial(property, next))
    {
      this.ChangeItem(property, next);
      AvatarCylonCustomizer.UpdateItemsVersion();
    }
    AvatarCylonCustomizer.UpdateMaterials();
    this.avatar.Unlock();
  }

  protected void CylonCustomize()
  {
    this.ClearCustomizeButtons();
    this.CylonMakeButtonsForProp("head", "style", 8, this.CreateCustomizeUiElement());
    this.CylonMakeButtonsForProp("head", "color", 5, this.CreateCustomizeUiElement());
    this.CylonMakeButtonsForProp("arms", "style", 8, this.CreateCustomizeUiElement());
    this.CylonMakeButtonsForProp("arms", "color", 5, this.CreateCustomizeUiElement());
    this.CylonMakeButtonsForProp("body", "style", 8, this.CreateCustomizeUiElement());
    this.CylonMakeButtonsForProp("body", "color", 5, this.CreateCustomizeUiElement());
    this.CylonMakeButtonsForProp("legs", "style", 2, this.CreateCustomizeUiElement());
    this.CylonMakeButtonsForProp("legs", "color", 5, this.CreateCustomizeUiElement());
  }

  private void HumanChangeItem(string property, bool next)
  {
    if (property.Length == 0)
      return;
    this.avatar.Lock();
    if (AvatarInfo.GetMaterialsOfRace(this.avatar.Race).Contains(property) && AvatarInfo.GetItemFromMaterial(property) == "hair")
    {
      if (next)
        ++this.humanHairbeardMatIndex;
      else
        --this.humanHairbeardMatIndex;
      if (this.humanHairbeardMatIndex < 0)
        this.humanHairbeardMatIndex = this.humanHairbeardMatCount - 1;
      if (this.humanHairbeardMatIndex > this.humanHairbeardMatCount - 1)
        this.humanHairbeardMatIndex = 0;
    }
    this.ChangeItem(property, next);
    if (!AvatarInfo.IsItem(this.avatar.Race, property))
    {
      this.avatar.Unlock();
    }
    else
    {
      using (List<string>.Enumerator enumerator = AvatarInfo.GetMaterialsOfRace(this.avatar.Race).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          string itemFromMaterial = AvatarInfo.GetItemFromMaterial(current);
          string property1 = this.avatar.GetProperty(itemFromMaterial);
          if (!property1.Contains("empty") && itemFromMaterial == property)
          {
            int index = AvatarSelector.GetPropertyIndex(this.avatar.Race, this.avatar.Sex, current, this.avatar.GetProperty(current));
            if (itemFromMaterial == "hair" || itemFromMaterial == "beard")
              index = this.humanHairbeardMatIndex;
            string property2 = AvatarSelector.GetProperty(this.avatar.Race, this.avatar.Sex, current, property1, index);
            this.avatar.SetProperty(current, property2);
          }
        }
      }
      using (List<string>.Enumerator enumerator = AvatarInfo.GetTexturesOfRace(this.avatar.Race).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          int propertyIndex = AvatarSelector.GetPropertyIndex(this.avatar.Race, this.avatar.Sex, current, this.avatar.GetProperty(current));
          string property1 = AvatarSelector.GetProperty(this.avatar.Race, this.avatar.Sex, current, string.Empty, propertyIndex);
          this.avatar.SetProperty(current, property1);
        }
      }
      this.avatar.Unlock();
    }
  }

  private void ChangeItem(string property, bool next)
  {
    int propertyCount = AvatarSelector.GetPropertyCount(this.avatar.Race, this.avatar.Sex, property);
    int propertyIndex = AvatarSelector.GetPropertyIndex(this.avatar.Race, this.avatar.Sex, property, this.avatar.GetProperty(property));
    int index = !next ? propertyIndex - 1 : propertyIndex + 1;
    if (index == propertyCount)
      index = 0;
    else if (index < 0)
      index = propertyCount - 1;
    if (AvatarInfo.GetMaterialsOfRace(this.avatar.Race).Contains(property))
    {
      string itemFromMaterial = AvatarInfo.GetItemFromMaterial(property);
      if (itemFromMaterial == "hair" || itemFromMaterial == "beard")
        index = this.humanHairbeardMatIndex;
    }
    string property1 = this.avatar.GetProperty(AvatarInfo.GetItemFromMaterial(property));
    string property2 = AvatarSelector.GetProperty(this.avatar.Race, this.avatar.Sex, property, property1, index);
    this.avatar.SetProperty(property, property2);
  }
}
