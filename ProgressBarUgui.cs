﻿// Decompiled with JetBrains decompiler
// Type: ProgressBarUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarUgui : MonoBehaviour
{
  [SerializeField]
  private Image progressBarFg;
  [SerializeField]
  private TextMeshProUGUI progressText;

  public void SetProgress01(float progress)
  {
    this.progressBarFg.fillAmount = progress;
    this.progressText.text = ((int) ((double) progress * 100.0)).ToString() + "%";
    RectTransform rectTransform = this.transform as RectTransform;
    this.progressText.color = (double) progress <= 0.5 ? Color.white : Color.black;
  }
}
