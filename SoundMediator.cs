﻿// Decompiled with JetBrains decompiler
// Type: SoundMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class SoundMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "SoundMediator";
  private MusicBox musicBox;
  private bool soundMuted;
  private float musicVolume;

  public SoundMediator()
    : base("SoundMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.SettingChangedSoundVolume);
    this.AddMessageInterest(Message.SettingChangedMusicVolume);
    this.AddMessageInterest(Message.LevelLoaded);
    this.AddMessageInterest(Message.SettingChangedMuteSound);
    this.AddMessageInterest(Message.CutsceneMuteGameMusic);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.SettingChangedSoundVolume:
        this.AdjustSoundVolume((float) message.Data);
        break;
      case Message.SettingChangedMusicVolume:
        this.AdjustMusicVolume((float) message.Data);
        break;
      case Message.SettingChangedMuteSound:
        this.soundMuted = (bool) message.Data;
        this.ApplyCurrentValues();
        break;
      case Message.CutsceneMuteGameMusic:
        this.MuteMusic((bool) message.Data);
        break;
      case Message.LevelLoaded:
        this.OnLevelLoaded((GameLevel) message.Data);
        break;
    }
  }

  private void OnLevelLoaded(GameLevel level)
  {
    this.CreateMusicBox(level);
    if ((Object) this.musicBox == (Object) null)
      return;
    this.ApplyCurrentValues();
  }

  private void ApplyCurrentValues()
  {
    SettingsDataProvider settingsDataProvider = this.OwnerFacade.FetchDataProvider("SettingsDataProvider") as SettingsDataProvider;
    this.AdjustSoundVolume(settingsDataProvider.CurrentSettings.SoundVolume);
    this.AdjustMusicVolume(settingsDataProvider.CurrentSettings.MusicVolume);
  }

  private void CreateMusicBox(GameLevel level)
  {
    this.musicBox = (MusicBox) null;
    if (level is SpaceLevel)
    {
      this.musicBox = Object.Instantiate<MusicBox>(SpaceExposer.GetInstance().MusicBoxPrefab);
      this.musicBox.transform.parent = Object.FindObjectOfType<AudioListener>().transform;
      this.musicBox.transform.localPosition = Vector3.zero;
    }
    else if (level is RoomLevel)
    {
      GameObject gameObject = Resources.Load("Fx/" + ((RoomLevel) level).Card.music, typeof (GameObject)) as GameObject;
      if ((Object) gameObject != (Object) null)
        this.musicBox = Object.Instantiate<MusicBox>(gameObject.GetComponent<MusicBox>());
    }
    if ((Object) this.musicBox == (Object) null)
      Debug.LogError((object) ("Couldn't create music box for level: " + level.name));
    else
      this.AdjustMusicVolume(this.musicVolume);
  }

  private void AdjustSoundVolume(float soundVolume)
  {
    if (this.soundMuted)
      soundVolume = 0.0f;
    AudioListener.volume = soundVolume;
  }

  private void AdjustMusicVolume(float newMusicVolume)
  {
    this.musicVolume = newMusicVolume;
    if ((Object) this.musicBox == (Object) null)
      return;
    if (this.soundMuted)
      this.musicVolume = 0.0f;
    this.musicBox.UserVolume = this.musicVolume;
  }

  private void MuteMusic(bool muteMusic)
  {
    this.musicBox.Mute(muteMusic);
  }
}
