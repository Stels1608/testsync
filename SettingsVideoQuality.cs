﻿// Decompiled with JetBrains decompiler
// Type: SettingsVideoQuality
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class SettingsVideoQuality : SettingsVideoGroup
{
  [SerializeField]
  private UILabel optionsHeadlineLabel;
  [SerializeField]
  private UILabel informationLabel;
  [SerializeField]
  private OptionsRadioElement qualitySettings;
  [SerializeField]
  private UILabel lowLabel;
  [SerializeField]
  private UILabel mediumLabel;
  [SerializeField]
  private UILabel highLabel;
  [SerializeField]
  private UILabel ultraLabel;
  [SerializeField]
  private UILabel customLabel;

  private void Awake()
  {
    this.CoveredSettings.Add(UserSetting.GraphicsQuality);
    this.controls.Add(UserSetting.GraphicsQuality, (OptionsElement) this.qualitySettings);
  }

  private void Start()
  {
    this.ReloadLanguageData();
  }

  public void ReloadLanguageData()
  {
    this.optionsHeadlineLabel.text = Tools.ParseMessage("%$bgo.ui.options.video.quality%");
    this.informationLabel.text = Tools.ParseMessage("%$bgo.ui.options.video.quality.infomationtext%");
    this.lowLabel.text = Tools.ParseMessage("%$bgo.etc.video_quality.low%");
    this.mediumLabel.text = Tools.ParseMessage("%$bgo.etc.video_quality.medium%");
    this.highLabel.text = Tools.ParseMessage("%$bgo.etc.video_quality.high%");
    this.ultraLabel.text = Tools.ParseMessage("%$bgo.etc.video_quality.ultra%");
    this.customLabel.text = Tools.ParseMessage("%$bgo.etc.video_quality.custom%");
  }
}
