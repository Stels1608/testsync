﻿// Decompiled with JetBrains decompiler
// Type: DictionaryHolder
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DictionaryHolder : ScriptableObject
{
  [SerializeField]
  private string[] keys = new string[0];
  [SerializeField]
  private string[] values = new string[0];

  public void ToArrays(Dictionary<string, string> dict)
  {
    this.keys = new string[dict.Count];
    this.values = new string[dict.Count];
    int index = 0;
    using (Dictionary<string, string>.Enumerator enumerator = dict.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        this.keys[index] = current.Key;
        this.values[index] = current.Value;
        ++index;
      }
    }
  }

  public Dictionary<string, string> ToDictionary()
  {
    Dictionary<string, string> dictionary = new Dictionary<string, string>();
    for (int index = 0; index < this.keys.Length; ++index)
      dictionary.Add(this.keys[index], this.values[index]);
    return dictionary;
  }
}
