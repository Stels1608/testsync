﻿// Decompiled with JetBrains decompiler
// Type: CharacterCreationMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class CharacterCreationMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "CharacterCreationMediator";
  private CharacterCreationWindow view;

  public CharacterCreationMediator()
    : base("CharacterCreationMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.CharacterCreationStarted);
    this.AddMessageInterest(Message.NameAccepted);
    this.AddMessageInterest(Message.NameRejected);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.CharacterCreationStarted:
        this.view = Object.FindObjectOfType<CharacterCreationWindow>();
        if (!((Object) this.view == (Object) null))
          break;
        Debug.LogError((object) "Character Creation window not found!");
        break;
      case Message.NameRejected:
        this.view.OnNameRejected();
        break;
      case Message.NameAccepted:
        Debug.Log((object) "Name accepted, sending Request.ChooseName and Request.CreateAvatar");
        this.view.OnNameAccepted();
        this.OwnerFacade.DetachView("CharacterCreationMediator");
        break;
    }
  }
}
