﻿// Decompiled with JetBrains decompiler
// Type: Body
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("+LOD/Body")]
public class Body : MultiLODListener
{
  public Renderer[] Renderers;
  private Renderer activeRenderer;

  public Renderer ActiveRenderer
  {
    get
    {
      return this.activeRenderer;
    }
  }

  private void DisableRenderers()
  {
    if (this.Renderers == null)
      return;
    for (int index = 0; index < this.Renderers.Length; ++index)
      this.UpdateRendersAndChilderen(this.Renderers[index], false);
  }

  private void UpdateRendersAndChilderen(Renderer render, bool isEnabled)
  {
    if ((Object) null == (Object) render)
      return;
    render.enabled = isEnabled;
    if (render.transform.childCount == 0)
      return;
    Renderer[] componentsInChildren = render.transform.GetComponentsInChildren<Renderer>();
    if (componentsInChildren == null)
      return;
    for (int index = 0; index <= componentsInChildren.Length - 1; ++index)
      componentsInChildren[index].GetComponent<Renderer>().enabled = isEnabled;
  }

  protected override void Awake()
  {
    this.DisableRenderers();
    base.Awake();
  }

  public void ReloadSkin()
  {
    this.OnLevelChanged();
  }

  protected override void OnLevelChanged()
  {
    if ((Object) this.activeRenderer != (Object) null)
    {
      this.UpdateRendersAndChilderen(this.activeRenderer, false);
      this.activeRenderer = (Renderer) null;
      this.SendMessage("OnRendererNulled", SendMessageOptions.DontRequireReceiver);
    }
    if (this.Level >= this.Renderers.Length)
      return;
    this.UpdateRendersAndChilderen(this.Renderers[this.Level], true);
    this.activeRenderer = this.Renderers[this.Level];
    this.activeRenderer.enabled = true;
    this.SendMessage("OnRendererChanged", (object) new Renderer[1]
    {
      this.activeRenderer
    }, SendMessageOptions.DontRequireReceiver);
  }

  protected override void Reset()
  {
    Log.DebugInfo((object) ("** Resetting: " + this.gameObject.name));
    this.Renderers = this.FindLODs();
    if (this.Renderers.Length == 0)
      this.Renderers = new Renderer[1];
    this.LevelLimits = new float[this.Renderers.Length];
    for (int index = 0; index < this.LevelLimits.Length; ++index)
      this.LevelLimits[index] = 1000f * (float) (index + 1);
  }

  private Renderer[] FindLODs()
  {
    List<Renderer> rendererList = new List<Renderer>();
    for (int index = 0; index < this.transform.childCount; ++index)
    {
      GameObject gameObject = this.transform.GetChild(index).gameObject;
      if (gameObject.name.ToLower().Contains("_lod"))
      {
        Renderer componentInChildren = gameObject.GetComponentInChildren<Renderer>();
        rendererList.Add(componentInChildren);
      }
    }
    return rendererList.ToArray();
  }
}
