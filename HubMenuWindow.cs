﻿// Decompiled with JetBrains decompiler
// Type: HubMenuWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.DamageWindow;
using UnityEngine;

public class HubMenuWindow : WindowWidget
{
  private const int SHIP_SELECTION = 0;
  private const int SHIP_CUSTOMIZATION = 1;
  private const int ITEM_SHOP = 2;
  private const int REPAIR_SHOP = 3;
  private const int DRADIS_CONTACT = 4;
  private HubMenuButton[] buttons;
  private HubMenuButton selectedButton;

  private void Awake()
  {
    this.buttons = this.GetComponentsInChildren<HubMenuButton>();
    this.buttons[0].OnClicked = new AnonymousDelegate(this.OnShipSelection);
    this.buttons[1].OnClicked = new AnonymousDelegate(this.OnShipCustomization);
    this.buttons[2].OnClicked = new AnonymousDelegate(this.OnItemShop);
    this.buttons[3].OnClicked = new AnonymousDelegate(this.OnRepairShop);
    this.buttons[4].OnClicked = new AnonymousDelegate(this.OnDradisContact);
  }

  private void SelectButton(HubMenuButton menuButton, HubMenuWindow.WindowType associatedWindow)
  {
    if ((Object) this.selectedButton != (Object) menuButton)
    {
      this.ActivateButton(menuButton);
      this.ShowWindow(associatedWindow);
    }
    else
    {
      this.ActivateButton((HubMenuButton) null);
      this.ShowWindow(HubMenuWindow.WindowType.None);
    }
  }

  private void OnShipSelection()
  {
    this.SelectButton(this.buttons[0], HubMenuWindow.WindowType.ShipSelection);
  }

  private void OnShipCustomization()
  {
    this.SelectButton(this.buttons[1], HubMenuWindow.WindowType.ShipCustomization);
  }

  private void OnItemShop()
  {
    this.SelectButton(this.buttons[2], HubMenuWindow.WindowType.ItemShop);
  }

  private void OnRepairShop()
  {
    this.SelectButton(this.buttons[3], HubMenuWindow.WindowType.RepairShop);
  }

  private void OnDradisContact()
  {
    this.SelectButton(this.buttons[4], HubMenuWindow.WindowType.DradisContact);
  }

  private void ResetToggleBarState()
  {
    this.ActivateButton((HubMenuButton) null);
  }

  private void ActivateButton(HubMenuButton activeButton)
  {
    foreach (HubMenuButton button in this.buttons)
      button.Selected = (Object) button == (Object) activeButton;
    this.selectedButton = activeButton;
  }

  public override void Close()
  {
    base.Close();
    this.ActivateButton((HubMenuButton) null);
  }

  private void ShowWindow(HubMenuWindow.WindowType window)
  {
    Gui.ShipShop.ShipShop shipShop = RoomLevel.GetLevel().shipShop;
    shipShop.IsRendered = window == HubMenuWindow.WindowType.ShipSelection;
    shipShop.OnCloseOuter = new AnonymousDelegate(this.ResetToggleBarState);
    DWWindow dwWindow = RoomLevel.GetLevel().repairGui;
    dwWindow.IsRendered = window == HubMenuWindow.WindowType.RepairShop;
    dwWindow.HandlerClose = new AnonymousDelegate(this.ResetToggleBarState);
    ShipCustomizationWindow customizationWindow = RoomLevel.GetLevel().shipCustomizationWindow;
    customizationWindow.IsRendered = window == HubMenuWindow.WindowType.ShipCustomization;
    customizationWindow.HandlerClose = new ShipCustomizationWindow.dHandlerClose(this.ResetToggleBarState);
    ShopWindow shopWindow = RoomLevel.GetLevel().shopWindow;
    shopWindow.IsRendered = window == HubMenuWindow.WindowType.ItemShop;
    shopWindow.HandlerClose = new ShopWindow.Handlr(this.ResetToggleBarState);
    FacadeFactory.GetInstance().SendMessage(Message.GuiContentWindowVisibility, (object) Tuple.New<WindowTypes, bool>(WindowTypes.WoFWindow, window == HubMenuWindow.WindowType.DradisContact));
    if (window != HubMenuWindow.WindowType.DradisContact)
      return;
    WoFWindowWidget woFwindowWidget = (WoFWindowWidget) ((GuiMediator) FacadeFactory.GetInstance().FetchView("GuiMediator")).Manager.GetContentWindowByType(WindowTypes.WoFWindow);
    if (!((Object) woFwindowWidget != (Object) null))
      return;
    woFwindowWidget.closeDelegate = new AnonymousDelegate(this.ResetToggleBarState);
  }

  public override void ReloadLanguageData()
  {
    this.buttons[0].SetText(BsgoLocalization.Get("bgo.hub.menu.ship_hangar"));
    this.buttons[1].SetText(BsgoLocalization.Get("bgo.hub.menu.ship_customization"));
    this.buttons[2].SetText(BsgoLocalization.Get("bgo.hub.menu.ship_item_shop"));
    this.buttons[3].SetText(BsgoLocalization.Get("bgo.hub.menu.ship_repair"));
    this.buttons[4].SetText(BsgoLocalization.Get("bgo.hub.menu.dradis"));
  }

  private enum WindowType
  {
    None = -1,
    ShipSelection = 0,
    RepairShop = 1,
    ShipCustomization = 2,
    ItemShop = 3,
    DradisContact = 4,
  }
}
