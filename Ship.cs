﻿// Decompiled with JetBrains decompiler
// Type: Ship
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class Ship : SpaceObject
{
  private ShipCardLight shipCardLight;
  private ShipBindings bindings;
  private ShipAspects aspects;
  private CustomExplosion customExplosion;
  public Flag IsReadyForJumpIn;

  public ShipCardLight ShipCardLight
  {
    get
    {
      return this.shipCardLight;
    }
  }

  public ShipBindings Bindings
  {
    get
    {
      return this.bindings;
    }
  }

  public override string PrefabName
  {
    get
    {
      if (this.ShowSkin && this.HasModelSkinInstalled)
        return this.Bindings.paintSystem.PrefabName;
      return base.PrefabName;
    }
  }

  public bool HasModelSkinInstalled
  {
    get
    {
      if (this.Bindings.paintSystem != null)
        return this.Bindings.paintSystem.PrefabName != base.PrefabName;
      return false;
    }
  }

  public override bool HideStats
  {
    get
    {
      if (!base.HideStats)
        return this.HasAspect(ShipAspect.StatsScrambler);
      return true;
    }
    set
    {
      base.HideStats = value;
    }
  }

  public override byte Level
  {
    get
    {
      return this.OwnerCard.Level;
    }
  }

  public Ship(uint objectID)
    : base(objectID)
  {
    this.aspects = new ShipAspects();
    this.IsReadyForJumpIn = new Flag();
    this.IsReadyForJumpIn.Depend(new ILoadable[1]
    {
      (ILoadable) this.IsAnyResLoaded
    });
    this.IsReadyForJumpIn.Set();
  }

  public bool HasAspect(ShipAspect aspect)
  {
    return this.aspects.ContainsAspect(aspect);
  }

  protected void ReadBindings(BgoProtocolReader r)
  {
    this.bindings = r.ReadDesc<ShipBindings>();
    this.AreCardsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) StaticCards.Instance.Stickers
    });
    this.AreCardsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.Bindings.AreLoaded
    });
  }

  protected void ReadAspects(BgoProtocolReader r)
  {
    this.aspects = r.ReadDesc<ShipAspects>();
  }

  protected void LoadCards()
  {
    this.shipCardLight = (ShipCardLight) Game.Catalogue.FetchCard(this.objectGUID, CardView.ShipLight);
    this.AreCardsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.shipCardLight
    });
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.ReadBindings(r);
    this.ReadAspects(r);
    this.LoadCards();
  }

  public override void OnDestroyed()
  {
    Ship.ShipExplosion(this);
  }

  public override void Remove(RemovingCause removingCause)
  {
    if ((Object) this.Model == (Object) null || removingCause != RemovingCause.Death || (Object) (this.customExplosion = this.Root.GetComponentInChildren<CustomExplosion>()) == (Object) null)
    {
      base.Remove(removingCause);
    }
    else
    {
      this.customExplosion.Play();
      if ((Object) this.Root != (Object) null)
        Object.Destroy((Object) this.Root.gameObject, this.customExplosion.modelLifeTime);
      this.UnSubscribe();
      if ((Object) SpaceLevel.GetLevel() != (Object) null)
        SpaceLevel.GetLevel().PaintedTargets.Remove((SpaceObject) this);
      this.dead = true;
    }
  }

  protected override IMovementController CreateMovementController()
  {
    return (IMovementController) new ManeuverController((SpaceObject) this, (MovementCard) Game.Catalogue.FetchCard(this.WorldCard.CardGUID, CardView.Movement));
  }

  public virtual void JumpOut()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Ship.\u003CJumpOut\u003Ec__AnonStorey110 outCAnonStorey110 = new Ship.\u003CJumpOut\u003Ec__AnonStorey110();
    // ISSUE: reference to a compiler-generated field
    outCAnonStorey110.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    outCAnonStorey110.meIsJumpingOut = this.IsMe || (int) this.ObjectID == (int) Game.Me.AnchorTarget;
    // ISSUE: reference to a compiler-generated field
    if (outCAnonStorey110.meIsJumpingOut)
      FacadeFactory.GetInstance().SendMessage(Message.MyJumpOutAnimStarted);
    // ISSUE: reference to a compiler-generated method
    this.PlayJumpOutEffect(new AnonymousDelegate(outCAnonStorey110.\u003C\u003Em__281));
  }

  protected void PlayJumpOutEffect(AnonymousDelegate jumpCompleted)
  {
    this.SetModelVisibility(true);
    JumpEffectNew modelScript = this.GetModelScript<JumpEffectNew>();
    if ((Object) modelScript != (Object) null && (bool) modelScript.IsJumpEffectSetUp)
    {
      modelScript.JumpOut();
      JumpEffectController jumpEffectController;
      if (!modelScript.TryGetRunningJumpEffect(out jumpEffectController))
        return;
      jumpEffectController.OnFinished += jumpCompleted;
    }
    else
      jumpCompleted();
  }

  protected void PlayJumpInEffect(AnonymousDelegate jumpCompleted)
  {
    this.SetModelVisibility(true);
    JumpEffectNew modelScript = this.GetModelScript<JumpEffectNew>();
    if ((Object) modelScript != (Object) null && (bool) modelScript.IsJumpEffectSetUp)
    {
      modelScript.JumpIn();
      JumpEffectController jumpEffectController;
      if (!modelScript.TryGetRunningJumpEffect(out jumpEffectController))
        return;
      jumpEffectController.OnFinished += jumpCompleted;
    }
    else
      jumpCompleted();
  }

  protected void SetModelVisibility(bool isVisible)
  {
    if (!((Object) this.Model != (Object) null))
      return;
    this.Model.SetActive(isVisible);
  }

  public void Dock()
  {
    if (this.IsMe)
      SpaceLevel.GetLevel().StopSector();
    else
      SpaceLevel.GetLevel().GetObjectRegistry().Remove(this.ObjectID, RemovingCause.Dock);
  }

  public static void ShipExplosion(Ship ship)
  {
    if ((int) ship.ObjectID == (int) Game.Me.AnchorTarget)
      GuiDockUndock.HideUndock();
    if (!(bool) ship.IsLoaded || (Object) ship.customExplosion != (Object) null)
      return;
    FXManager.Instance.PutEffect((EffectState) new ExplosionEffectState(ship.Position, ship.Rotation, (ExplosionArgs) new ShipExplosionArgs()
    {
      Faction = ship.Faction,
      Tier = (int) ship.ShipCardLight.Tier
    }, ship.MovementController.CurrentSpeed));
  }

  public override void InitModelScripts()
  {
    base.InitModelScripts();
    JumpEffectNew jumpEffectNew = this.GetModelScript<JumpEffectNew>();
    if ((Object) jumpEffectNew == (Object) null)
      jumpEffectNew = this.AddModelScript<JumpEffectNew>();
    jumpEffectNew.SetupJumpEffect((SpaceObject) this);
    if (this.HasJumpInFxTriggered)
      return;
    this.IsReadyForJumpIn.AddHandler(new SignalHandler(jumpEffectNew.JumpIn));
  }
}
