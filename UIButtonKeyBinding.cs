﻿// Decompiled with JetBrains decompiler
// Type: UIButtonKeyBinding
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("Game/UI/Button Key Binding")]
public class UIButtonKeyBinding : MonoBehaviour
{
  public KeyCode keyCode;

  private void Update()
  {
    if (UICamera.inputHasFocus || this.keyCode == KeyCode.None)
      return;
    if (Input.GetKeyDown(this.keyCode))
      this.SendMessage("OnPress", (object) true, SendMessageOptions.DontRequireReceiver);
    if (!Input.GetKeyUp(this.keyCode))
      return;
    this.SendMessage("OnPress", (object) false, SendMessageOptions.DontRequireReceiver);
    this.SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
  }
}
