﻿// Decompiled with JetBrains decompiler
// Type: DelayedAudioPlay
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DelayedAudioPlay : MonoBehaviour
{
  public AudioSource AudioSource;
  public string FxClip;
  public float StartDelay;
  private AudioClip audioClip;
  private bool muted;

  public bool DebugPlayOnEnable { get; set; }

  private void Awake()
  {
    this.audioClip = Resources.Load<AudioClip>(this.FxClip);
    if (!((Object) this.audioClip == (Object) null))
      return;
    UnityEngine.Debug.LogError((object) ("Audio Clip is null: " + this.FxClip));
  }

  private void Start()
  {
    this.AudioSource.clip = this.audioClip;
    this.StartCoroutine(this.Play(this.StartDelay));
  }

  private void OnEnable()
  {
    if (!this.DebugPlayOnEnable)
      return;
    this.StartCoroutine(this.Play(this.StartDelay));
  }

  [DebuggerHidden]
  private IEnumerator Play(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DelayedAudioPlay.\u003CPlay\u003Ec__Iterator35() { delay = delay, \u003C\u0024\u003Edelay = delay, \u003C\u003Ef__this = this };
  }

  public void Mute()
  {
    this.muted = true;
  }
}
