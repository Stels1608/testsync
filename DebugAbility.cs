﻿// Decompiled with JetBrains decompiler
// Type: DebugAbility
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Text;
using UnityEngine;

public class DebugAbility : DebugBehaviour<DebugAbility>
{
  private Vector2 scroll = new Vector2();

  private void Start()
  {
    this.windowID = 18;
    this.SetSize(300f, 400f);
  }

  protected override void WindowFunc()
  {
    TextAnchor alignment = GUI.skin.box.alignment;
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    if (Game.Me.ActiveShip != null)
    {
      int num = 0;
      foreach (ShipSlot slot in Game.Me.ActiveShip.Slots)
      {
        if (slot.GameStats.Available)
          ++num;
      }
      GUILayout.Label(string.Format("ValidationCount {0}/{1}", (object) num, (object) Game.Me.ActiveShip.Slots.Count));
      foreach (ShipSlot slot in Game.Me.ActiveShip.Slots)
      {
        if ((bool) slot.IsLoaded && slot.GameStats.Available && slot.Ability != null)
        {
          GUILayout.BeginHorizontal();
          if (GUILayout.Button("Use", new GUILayoutOption[1]{ GUILayout.Width(50f) }))
            slot.Ability.Cast();
          GUILayout.Label(string.Format("{0}-{1}({2})", (object) slot.ServerID, (object) slot.Ability.card.GUICard.Key, (object) slot.Ability.card.GUICard.Level));
          GUILayout.EndHorizontal();
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.AppendFormat("Cons-{0} = {1} count\n", (object) slot.Ability.consumableGuid, (object) slot.Ability.ConsumableCount);
          stringBuilder.AppendFormat("Angle = {0}, ", (object) slot.GameStats.Angle);
          stringBuilder.AppendFormat("PPCost = {0}\n", (object) slot.GameStats.PowerPointCost);
          stringBuilder.AppendFormat("Range = {0} - {1}\n", (object) slot.GameStats.MinRange, (object) slot.GameStats.MaxRange);
          stringBuilder.AppendFormat("Cooldown = {0:0.0}/{1}\n", (object) Mathf.Clamp(Time.time - slot.Validation.CastedTime, 0.0f, slot.GameStats.Cooldown), (object) slot.GameStats.Cooldown);
          stringBuilder.AppendFormat("IsValid = {0},", (object) slot.Ability.IsValid);
          stringBuilder.AppendFormat("IsBroken = {0}\n", (object) slot.Ability.IsBroken);
          stringBuilder.AppendFormat("On = {0}, ", (object) slot.Validation.On);
          stringBuilder.AppendFormat("Target = {0}", (object) (slot.Validation.Target != null));
          GUILayout.Box(stringBuilder.ToString());
          GUILayout.Width(10f);
        }
      }
    }
    GUILayout.EndScrollView();
    GUI.skin.box.alignment = alignment;
  }
}
