﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorMultiTargetUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HudIndicatorMultiTargetUgui : HudIndicatorBaseComponentUgui, IReceivesInputBindings, IReceivesSpaceObject
{
  private readonly List<ShipAbility> weaponAbilities = new List<ShipAbility>();
  [SerializeField]
  private Image multiTargetIcon;
  [SerializeField]
  private TextMeshProUGUI weaponNumberLabel;
  private SpaceObject spaceObject;
  private InputBinder inputBinder;

  protected override void Awake()
  {
    base.Awake();
    Image image = this.multiTargetIcon;
    Color color1 = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_SPECIAL);
    this.weaponNumberLabel.color = color1;
    Color color2 = color1;
    image.color = color2;
  }

  protected override void ResetComponentStates()
  {
    this.spaceObject = (SpaceObject) null;
    this.weaponAbilities.Clear();
    if (!((UnityEngine.Object) this.weaponNumberLabel != (UnityEngine.Object) null))
      return;
    this.weaponNumberLabel.text = string.Empty;
  }

  private void UpdateHotkeyLabel()
  {
    List<string> stringList = new List<string>();
    using (List<ShipAbility>.Enumerator enumerator = this.weaponAbilities.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipAbility current = enumerator.Current;
        IInputBinding binding = this.inputBinder.TryGetBinding(InputDevice.KeyboardMouse, current.slot.Action);
        if (binding != null && !binding.IsUnbound)
        {
          string triggerAlias = binding.TriggerAlias;
          stringList.Add(triggerAlias);
        }
        else
          stringList.Add("*" + ((int) current.slot.ServerID + 1).ToString());
      }
    }
    stringList.Sort((Comparison<string>) ((p1, p2) =>
    {
      if (p1.StartsWith("*") && !p2.StartsWith("*"))
        return 1;
      if (!p1.StartsWith("*") && p2.StartsWith("*"))
        return -1;
      Regex regex = new Regex("\\*?(\\d+)");
      Match match1 = regex.Match(p1);
      Match match2 = regex.Match(p2);
      if (match1.Success && match2.Success)
        return int.Parse(match1.Groups[1].Value).CompareTo(int.Parse(match2.Groups[1].Value));
      return p1.CompareTo(p2);
    }));
    this.weaponNumberLabel.text = string.Join(" ", stringList.ToArray());
  }

  public void OnSpaceObjectInjection(SpaceObject spaceObject)
  {
    this.spaceObject = spaceObject;
    this.UpdateColorAndAlpha();
  }

  public void OnInputBinderInjection(InputBinder inputBinder)
  {
    this.inputBinder = inputBinder;
    this.UpdateView();
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  public override void OnMultiTargetSelectionChange(bool isAbilityTarget, ShipAbility ability)
  {
    if (isAbilityTarget)
      this.OnMultiTargetSelected(ability);
    else
      this.OnMultiTargetDeselected(ability);
    base.OnMultiTargetSelectionChange(isAbilityTarget, ability);
  }

  private void OnMultiTargetSelected(ShipAbility ability)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    HudIndicatorMultiTargetUgui.\u003COnMultiTargetSelected\u003Ec__AnonStoreyCE selectedCAnonStoreyCe = new HudIndicatorMultiTargetUgui.\u003COnMultiTargetSelected\u003Ec__AnonStoreyCE();
    // ISSUE: reference to a compiler-generated field
    selectedCAnonStoreyCe.ability = ability;
    // ISSUE: reference to a compiler-generated field
    selectedCAnonStoreyCe.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated method
    if (this.weaponAbilities.Any<ShipAbility>(new Func<ShipAbility, bool>(selectedCAnonStoreyCe.\u003C\u003Em__1F1)))
      return;
    // ISSUE: reference to a compiler-generated field
    this.weaponAbilities.Add(selectedCAnonStoreyCe.ability);
  }

  private void OnMultiTargetDeselected(ShipAbility ability)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.weaponAbilities.RemoveAll(new Predicate<ShipAbility>(new HudIndicatorMultiTargetUgui.\u003COnMultiTargetDeselected\u003Ec__AnonStoreyCF()
    {
      ability = ability,
      \u003C\u003Ef__this = this
    }.\u003C\u003Em__1F2));
  }

  private bool IsSameAbility(ShipAbility ability1, ShipAbility ability2)
  {
    return (int) ability1.ServerID == (int) ability2.ServerID;
  }

  protected override void UpdateView()
  {
    this.UpdateHotkeyLabel();
    this.gameObject.SetActive(this.IsMultiselected && this.InScreen);
  }
}
