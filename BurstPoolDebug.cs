﻿// Decompiled with JetBrains decompiler
// Type: BurstPoolDebug
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BurstPoolDebug : MonoBehaviour
{
  public BurstPool BurstPool;
  public int BurstCount;
  public int PoolCount;
  public int MeshCount;

  private void Update()
  {
    if (this.BurstPool == null)
      return;
    this.BurstCount = this.BurstPool.BurstCount;
    this.PoolCount = this.BurstPool.PoolCount;
    this.MeshCount = this.BurstPool.MeshCount;
  }
}
