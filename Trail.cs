﻿// Decompiled with JetBrains decompiler
// Type: Trail
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class Trail : MonoBehaviour
{
  public bool emit = true;
  public float lifeTime = 0.5f;
  private float lifeTimeRatio = 1f;
  public float maxAngle = 2f;
  public float minVertexDistance = 0.1f;
  public float maxVertexDistance = 1f;
  private Trail.Point[] points = new Trail.Point[100];
  private Vector3[] vertices = new Vector3[200];
  private Vector2[] uvs = new Vector2[200];
  private int[] triangles = new int[594];
  private Color[] meshColors = new Color[200];
  public int maxPointCount = 99;
  private const int COUNT = 100;
  public Material material;
  private Material instanceMaterial;
  private float fadeOutRatio;
  public Color[] colors;
  public float[] widths;
  private GameObject trailObj;
  private Mesh mesh;
  private int pointCnt;
  private int lastPointCnt;

  private void Start()
  {
    this.trailObj = new GameObject("Trail");
    this.trailObj.transform.parent = (Transform) null;
    this.trailObj.transform.position = Vector3.zero;
    this.trailObj.transform.rotation = Quaternion.identity;
    this.trailObj.transform.localScale = Vector3.one;
    this.mesh = this.trailObj.AddComponent<MeshFilter>().mesh;
    this.trailObj.AddComponent(typeof (MeshRenderer));
    this.instanceMaterial = new Material(this.material);
    this.fadeOutRatio = 1f / this.instanceMaterial.GetColor("_TintColor").a;
    this.trailObj.GetComponent<Renderer>().material = this.instanceMaterial;
  }

  public void RemoveTrailObj()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.trailObj);
  }

  private void LateUpdate()
  {
    if (this.pointCnt == this.maxPointCount)
    {
      this.points[this.pointCnt] = (Trail.Point) null;
      --this.pointCnt;
    }
    else
    {
      for (int index = this.pointCnt - 1; index >= 0 && (double) this.points[index].timeAlive > (double) this.lifeTime; --index)
      {
        this.points[index] = (Trail.Point) null;
        --this.pointCnt;
      }
    }
    if (this.emit)
    {
      if (this.pointCnt == 0)
      {
        this.points[this.pointCnt++] = new Trail.Point(this.transform);
        this.points[this.pointCnt++] = new Trail.Point(this.transform);
      }
      if (this.pointCnt == 1)
        this.insertPoint();
      bool flag = false;
      float sqrMagnitude = (this.points[1].position - this.transform.position).sqrMagnitude;
      if ((double) sqrMagnitude > (double) this.minVertexDistance * (double) this.minVertexDistance)
      {
        if ((double) sqrMagnitude > (double) this.maxVertexDistance * (double) this.maxVertexDistance)
          flag = true;
        else if ((double) Quaternion.Angle(this.transform.rotation, this.points[1].rotation) > (double) this.maxAngle)
          flag = true;
      }
      if (flag)
      {
        if (this.pointCnt < this.maxPointCount)
          this.insertPoint();
        else
          flag = false;
      }
      if (!flag)
        this.points[0].update(this.transform);
    }
    if (this.pointCnt < 2)
    {
      this.trailObj.GetComponent<Renderer>().enabled = false;
    }
    else
    {
      this.trailObj.GetComponent<Renderer>().enabled = true;
      this.lifeTimeRatio = 1f / this.lifeTime;
      if (!this.emit)
      {
        if (this.pointCnt == 0)
          return;
        Color color = this.instanceMaterial.GetColor("_TintColor");
        color.a -= this.fadeOutRatio * this.lifeTimeRatio * Time.deltaTime;
        if ((double) color.a > 0.0)
        {
          this.instanceMaterial.SetColor("_TintColor", color);
        }
        else
        {
          UnityEngine.Object.Destroy((UnityEngine.Object) this.trailObj);
          UnityEngine.Object.Destroy((UnityEngine.Object) this);
        }
      }
      else
      {
        Quaternion quaternion = Quaternion.identity;
        float num1 = (float) (1.0 / ((double) this.points[this.pointCnt - 1].timeAlive - (double) this.points[0].timeAlive));
        for (int index1 = 0; index1 < this.pointCnt; ++index1)
        {
          Trail.Point point = this.points[index1];
          float t = point.timeAlive * this.lifeTimeRatio;
          Color color = Color.Lerp(this.colors[0], this.colors[1], t);
          this.meshColors[index1 * 2] = color;
          this.meshColors[index1 * 2 + 1] = color;
          float num2 = Mathf.Lerp(this.widths[0], this.widths[1], t);
          this.trailObj.transform.position = point.position;
          if (index1 == 0)
          {
            this.trailObj.transform.rotation = point.rotation;
            Vector3 forward = this.trailObj.transform.forward;
            Vector3 tangent = point.position - Camera.main.transform.position;
            Vector3 up = Vector3.up;
            Vector3.OrthoNormalize(ref forward, ref tangent, ref up);
            quaternion = Quaternion.LookRotation(forward, up);
          }
          this.trailObj.transform.rotation = quaternion;
          this.vertices[index1 * 2] = this.trailObj.transform.TransformPoint(0.0f, num2 * 0.5f, 0.0f);
          this.vertices[index1 * 2 + 1] = this.trailObj.transform.TransformPoint(0.0f, (float) (-(double) num2 * 0.5), 0.0f);
          float x = (point.timeAlive - this.points[0].timeAlive) * num1;
          this.uvs[index1 * 2] = new Vector2(x, 0.0f);
          this.uvs[index1 * 2 + 1] = new Vector2(x, 1f);
          if (index1 > 0 && index1 >= this.lastPointCnt)
          {
            int index2 = (index1 - 1) * 6;
            int num3 = index1 * 2;
            this.triangles[index2] = num3 - 2;
            this.triangles[index2 + 1] = num3 - 1;
            this.triangles[index2 + 2] = num3;
            this.triangles[index2 + 3] = num3 + 1;
            this.triangles[index2 + 4] = num3;
            this.triangles[index2 + 5] = num3 - 1;
          }
        }
        for (int index1 = this.pointCnt; index1 < this.lastPointCnt; ++index1)
        {
          this.meshColors[index1 * 2] = Color.clear;
          this.meshColors[index1 * 2 + 1] = Color.clear;
          this.vertices[index1 * 2] = Vector3.zero;
          this.vertices[index1 * 2 + 1] = Vector3.zero;
          if (index1 > 0)
          {
            int index2 = (index1 - 1) * 6;
            this.triangles[index2] = 0;
            this.triangles[index2 + 1] = 0;
            this.triangles[index2 + 2] = 0;
            this.triangles[index2 + 3] = 0;
            this.triangles[index2 + 4] = 0;
            this.triangles[index2 + 5] = 0;
          }
        }
        this.trailObj.transform.position = Vector3.zero;
        this.trailObj.transform.rotation = Quaternion.identity;
        this.mesh.Clear();
        this.mesh.vertices = this.vertices;
        this.mesh.colors = this.meshColors;
        this.mesh.uv = this.uvs;
        this.mesh.triangles = this.triangles;
        this.lastPointCnt = this.pointCnt;
      }
    }
  }

  private void insertPoint()
  {
    Array.Copy((Array) this.points, 0, (Array) this.points, 1, this.pointCnt);
    this.points[0] = new Trail.Point(this.transform);
    ++this.pointCnt;
  }

  private class Point
  {
    public Vector3 position = Vector3.zero;
    public Quaternion rotation = Quaternion.identity;
    public float timeCreated;
    public float fadeAlpha;

    public float timeAlive
    {
      get
      {
        return Time.time - this.timeCreated;
      }
    }

    public Point(Transform trans)
    {
      this.position = trans.position;
      this.rotation = trans.rotation;
      this.timeCreated = Time.time;
    }

    public void update(Transform trans)
    {
      this.position = trans.position;
      this.rotation = trans.rotation;
      this.timeCreated = Time.time;
    }
  }
}
