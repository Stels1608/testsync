﻿// Decompiled with JetBrains decompiler
// Type: InputBindingUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class InputBindingUi : MonoBehaviour, ILocalizeable
{
  private readonly List<InputBindingUi.BindingGroupData> bindingGroups = new List<InputBindingUi.BindingGroupData>();
  private readonly List<InputBindingGroup> inputBindingGroups = new List<InputBindingGroup>();
  [SerializeField]
  private UILabel title;
  [SerializeField]
  private GameObject inputBindingElementObject;
  private InputDevice inputDevice;

  public void Start()
  {
    this.ReloadLanguageData();
  }

  protected List<Action> GetActionsCombatJoystick()
  {
    List<Action> actionsCombat = this.GetActionsCombat();
    int index = actionsCombat.FindIndex((Predicate<Action>) (action => action == Action.ToggleGuns)) + 1;
    actionsCombat.Insert(index, Action.ToggleGunsOnHold);
    return actionsCombat;
  }

  protected List<Action> GetActionsCombat()
  {
    return new List<Action>() { Action.NearestEnemy, Action.NearestFriendly, Action.SelectScreenCenterObject, Action.CancelTarget, Action.SelectNearestMissile, Action.SelectNearestMine, Action.ToggleGuns, Action.ToggleFlak, Action.TogglePointDefence, Action.FireMissiles, Action.WeaponSlot1, Action.WeaponSlot2, Action.WeaponSlot3, Action.WeaponSlot4, Action.WeaponSlot5, Action.WeaponSlot6, Action.WeaponSlot7, Action.WeaponSlot8, Action.WeaponSlot9, Action.WeaponSlot10, Action.WeaponSlot11, Action.WeaponSlot12, Action.AbilitySlot1, Action.AbilitySlot2, Action.AbilitySlot3, Action.AbilitySlot4, Action.AbilitySlot5, Action.AbilitySlot6, Action.AbilitySlot7, Action.AbilitySlot8, Action.AbilitySlot9, Action.AbilitySlot10 };
  }

  protected List<Action> GetActionsSteeringMouseKeyboard()
  {
    return new List<Action>() { Action.SlopeForwardOrSlideUp, Action.SlopeBackwardOrSlideDown, Action.TurnOrSlideLeft, Action.TurnOrSlideRight, Action.RollLeft, Action.RollRight, Action.ToggleMovementMode, Action.ToggleAdvancedFlightControls };
  }

  protected List<Action> GetActionsSteeringJoystickGamepad()
  {
    return new List<Action>() { Action.JoystickTurnLeft, Action.JoystickTurnRight, Action.JoystickTurnUp, Action.JoystickTurnDown, Action.JoystickRollLeft, Action.JoystickRollRight, Action.JoystickSpeedController, Action.JoystickLookLeft, Action.JoystickLookRight, Action.JoystickLookUp, Action.JoystickLookDown, Action.JoystickStrafeLeft, Action.JoystickStrafeRight, Action.JoystickStrafeUp, Action.JoystickStrafeDown };
  }

  protected List<Action> GetActionsFlight()
  {
    return new List<Action>() { Action.Jump, Action.CancelJump, Action.Boost, Action.ToggleBoost, Action.FullSpeed, Action.Stop, Action.SpeedUp, Action.SlowDown, Action.MatchSpeed, Action.Follow, Action.AlignToHorizon };
  }

  protected List<Action> GetActionsCamera()
  {
    return new List<Action>() { Action.TargetCamera, Action.ChaseCamera, Action.FreeCamera, Action.NoseCamera, Action.ToggleCamera };
  }

  protected List<Action> GetActionsSocial()
  {
    return new List<Action>() { Action.FocusChat, Action.UnfocusChat, Action.Reply };
  }

  protected List<Action> GetActionsUi()
  {
    return new List<Action>() { Action.ToggleWindowInventory, Action.ToggleWindowPilotLog, Action.ToggleWindowOptions, Action.ToggleWindowLeaderboard, Action.ToggleWindowWingRoster, Action.ToggleWindowGalaxyMap, Action.ToggleWindowStatusAssignments, Action.ToggleWindowDuties, Action.ToggleWindowSkills, Action.ToggleWindowShipStatus, Action.ToggleWindowInFlightSupply, Action.ToggleWindowTutorial, Action.ToggleTournamentRanking, Action.ToggleSystemMap2D, Action.ToggleSystemMap3D, Action.Map3DBackToOverview, Action.Map3DFocusYourShip };
  }

  protected List<Action> GetActionsHud()
  {
    return new List<Action>() { Action.ToggleShipName, Action.ToggleCombatGUI, Action.ToggleSquadWindow };
  }

  protected List<Action> GetActionsMisc()
  {
    return new List<Action>() { Action.ToggleFps, Action.ToggleFullscreen };
  }

  public void Setup(InputDevice device)
  {
    if (this.bindingGroups.Count > 0)
      this.bindingGroups.Clear();
    this.inputDevice = device;
    switch (device)
    {
      case InputDevice.KeyboardMouse:
        this.InitBindingGroupsKeyboardMouse(device);
        break;
      case InputDevice.Joystick:
        this.InitBindingGroupsJoystickGamepad(device);
        break;
      default:
        Debug.LogError((object) "No valid device given for input binding");
        break;
    }
    this.AddInputBindingGroups();
  }

  private void InitBindingGroupsJoystickGamepad(InputDevice groupDevice)
  {
    this.bindingGroups.Add(new InputBindingUi.BindingGroupData()
    {
      GroupName = "%$bgo.ui.options.control.controls.combat%",
      BindableActions = this.GetActionsCombatJoystick(),
      InputDevice = groupDevice
    });
    List<Action> steeringJoystickGamepad = this.GetActionsSteeringJoystickGamepad();
    steeringJoystickGamepad.AddRange((IEnumerable<Action>) this.GetActionsFlight());
    this.bindingGroups.Add(new InputBindingUi.BindingGroupData()
    {
      GroupName = "%$bgo.ui.options.control.controls.flight%",
      BindableActions = steeringJoystickGamepad,
      InputDevice = groupDevice
    });
    this.bindingGroups.Add(new InputBindingUi.BindingGroupData()
    {
      GroupName = "%$bgo.ui.options.control.controls.misc%",
      BindableActions = this.GetActionsMisc(),
      InputDevice = groupDevice
    });
  }

  private void InitBindingGroupsKeyboardMouse(InputDevice groupDevice)
  {
    this.bindingGroups.Add(new InputBindingUi.BindingGroupData()
    {
      GroupName = "%$bgo.ui.options.control.controls.combat%",
      BindableActions = this.GetActionsCombat(),
      InputDevice = groupDevice
    });
    List<Action> steeringMouseKeyboard = this.GetActionsSteeringMouseKeyboard();
    steeringMouseKeyboard.AddRange((IEnumerable<Action>) this.GetActionsFlight());
    this.bindingGroups.Add(new InputBindingUi.BindingGroupData()
    {
      GroupName = "%$bgo.ui.options.control.controls.flight%",
      BindableActions = steeringMouseKeyboard,
      InputDevice = groupDevice
    });
    this.bindingGroups.Add(new InputBindingUi.BindingGroupData()
    {
      GroupName = "%$bgo.ui.options.control.controls.camera%",
      BindableActions = this.GetActionsCamera(),
      InputDevice = groupDevice
    });
    this.bindingGroups.Add(new InputBindingUi.BindingGroupData()
    {
      GroupName = "%$bgo.ui.options.control.controls.social%",
      BindableActions = this.GetActionsSocial(),
      InputDevice = groupDevice
    });
    this.bindingGroups.Add(new InputBindingUi.BindingGroupData()
    {
      GroupName = "%$bgo.ui.options.control.controls.ui%",
      BindableActions = this.GetActionsUi(),
      InputDevice = groupDevice
    });
    this.bindingGroups.Add(new InputBindingUi.BindingGroupData()
    {
      GroupName = "%$bgo.ui.options.control.controls.hud%",
      BindableActions = this.GetActionsHud(),
      InputDevice = groupDevice
    });
    this.bindingGroups.Add(new InputBindingUi.BindingGroupData()
    {
      GroupName = "%$bgo.ui.options.control.controls.misc%",
      BindableActions = this.GetActionsMisc(),
      InputDevice = groupDevice
    });
  }

  public void AddInputBindingGroups()
  {
    GameObject prefab = (GameObject) Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingGroup");
    int num = 0;
    using (List<InputBindingUi.BindingGroupData>.Enumerator enumerator = this.bindingGroups.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InputBindingUi.BindingGroupData current = enumerator.Current;
        GameObject gameObject = NGUITools.AddChild(this.inputBindingElementObject, prefab);
        gameObject.name = num.ToString() + "_" + current.GroupName;
        InputBindingGroup component = gameObject.GetComponent<InputBindingGroup>();
        if ((UnityEngine.Object) component == (UnityEngine.Object) null)
          Debug.LogError((object) "group is null");
        component.Init(current.GroupName, current.BindableActions, current.InputDevice, this.inputBindingElementObject);
        this.inputBindingGroups.Add(component);
        ++num;
      }
    }
  }

  public void ReloadLanguageData()
  {
    switch (this.inputDevice)
    {
      case InputDevice.KeyboardMouse:
        this.title.text = BsgoLocalization.Get("%$bgo.ui.options.control.controls.headline.keyboard%");
        break;
      case InputDevice.Joystick:
        this.title.text = BsgoLocalization.Get("%$bgo.ui.options.control.controls.headline.joystick%");
        break;
      default:
        Debug.LogError((object) ("No valid device given: " + (object) this.inputDevice));
        break;
    }
  }

  public void UpdateBindingButtons(List<IInputBinding> list)
  {
    using (List<InputBindingGroup>.Enumerator enumerator = this.inputBindingGroups.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateBindingButtons(list);
    }
    this.inputBindingElementObject.GetComponent<UITable>().Reposition();
  }

  public void ShowBindingElements(bool isEnabled)
  {
    using (List<InputBindingGroup>.Enumerator enumerator = this.inputBindingGroups.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.ShowBindingButtons(isEnabled);
    }
  }

  public void RepaintBindingElements()
  {
    using (List<InputBindingGroup>.Enumerator enumerator = this.inputBindingGroups.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.RepaintBindingElements();
    }
  }

  private struct BindingGroupData
  {
    public InputDevice InputDevice;
    public string GroupName;
    public List<Action> BindableActions;
  }
}
