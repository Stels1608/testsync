﻿// Decompiled with JetBrains decompiler
// Type: DialogEntity
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DialogEntity : MonoBehaviour
{
  public DialogIndexClicked clickDelegate;
  [SerializeField]
  private UILabel dialogText;
  [SerializeField]
  private UISprite dialogTypeSprite;
  [SerializeField]
  private UISprite backgroundSprite;
  private int index;

  public virtual void Init(string text, int i, string type = "")
  {
    this.index = i;
    string str = "dialog_";
    this.dialogText.text = text;
    this.dialogTypeSprite.spriteName = !(type != string.Empty) ? str + "info_dialog" : str + type;
    this.backgroundSprite.spriteName = "transparent";
  }

  public void OnClick()
  {
    if (this.clickDelegate == null)
      return;
    this.clickDelegate(this.index);
  }

  public static GameObject Create(GameObject parent)
  {
    GameObject gameObject1 = new GameObject("dialog_entity");
    PositionUtils.CorrectTransform(parent, gameObject1);
    DialogEntity dialogEntity = gameObject1.AddComponent<DialogEntity>();
    GameObject gameObject2 = new GameObject("_text");
    PositionUtils.CorrectTransform(gameObject1, gameObject2);
    UILabel uiLabel = GuiWidgetsFactory.AddLabel(gameObject2, new Vector2(945f, 16f), ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_ANSWER_COLOR_NORMAL), 5, 16, UIWidget.Pivot.TopLeft, UILabel.Overflow.ResizeHeight);
    uiLabel.effectStyle = UILabel.Effect.Outline;
    uiLabel.effectDistance = new Vector2(1.5f, 1.5f);
    gameObject2.transform.localPosition = new Vector3(37f, -6f, 0.0f);
    UIButton uiButton1 = gameObject1.AddComponent<UIButton>();
    uiButton1.defaultColor = ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_NPC_NAME);
    uiButton1.hover = ImageUtils.HexStringToColor("f3c960");
    uiButton1.pressed = ImageUtils.HexStringToColor("f3c960");
    uiButton1.tweenTarget = gameObject2;
    uiButton1.duration = 0.1f;
    GameObject gameObject3 = Resources.Load("GUI/gui_2013/gui_2013", typeof (GameObject)) as GameObject;
    if ((Object) gameObject3 == (Object) null)
      Debug.LogError((object) "Atlas not found: GUI/gui_2013/ gui_2013");
    UIAtlas component = gameObject3.GetComponent<UIAtlas>();
    GameObject child1 = new GameObject("_dialogType");
    PositionUtils.CorrectTransform(gameObject1, child1);
    GameObject child2 = new GameObject("_hoverBackground");
    PositionUtils.CorrectTransform(gameObject1, child2);
    UISprite uiSprite1 = child1.AddComponent<UISprite>();
    uiSprite1.type = UIBasicSprite.Type.Simple;
    uiSprite1.pivot = UIWidget.Pivot.TopLeft;
    uiSprite1.width = 28;
    uiSprite1.height = 28;
    uiSprite1.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    uiSprite1.atlas = component;
    uiSprite1.pivot = UIWidget.Pivot.TopLeft;
    UISprite uiSprite2 = child2.AddComponent<UISprite>();
    uiSprite2.type = UIBasicSprite.Type.Simple;
    uiSprite2.pivot = UIWidget.Pivot.TopLeft;
    uiSprite2.width = 300;
    uiSprite2.height = 28;
    uiSprite2.transform.localPosition = new Vector3(30f, 0.0f, 0.0f);
    uiSprite2.atlas = component;
    uiSprite2.pivot = UIWidget.Pivot.TopLeft;
    UIButton uiButton2 = gameObject1.AddComponent<UIButton>();
    UIButton uiButton3 = uiButton2;
    Color color1 = ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_ANSWER_COLOR_NORMAL);
    uiButton1.defaultColor = color1;
    Color color2 = color1;
    uiButton3.defaultColor = color2;
    uiButton2.hover = ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_ANSWER_COLOR_HOVER);
    uiButton2.tweenTarget = child2;
    uiButton2.duration = 0.1f;
    uiButton2.normalSprite = "transparent";
    uiButton2.hoverSprite = "dialogue_hover_bg";
    UIButton uiButton4 = gameObject1.AddComponent<UIButton>();
    UIButton uiButton5 = uiButton4;
    Color color3 = ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_ANSWER_COLOR_NORMAL);
    uiButton1.defaultColor = color3;
    Color color4 = color3;
    uiButton5.defaultColor = color4;
    uiButton4.hover = uiButton1.hover = uiButton4.pressed = uiButton1.pressed = ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_ANSWER_COLOR_HOVER);
    uiButton4.tweenTarget = child1;
    uiButton4.duration = 0.1f;
    NGUITools.AddWidgetCollider(gameObject1);
    uiButton1.enabled = true;
    uiButton4.enabled = true;
    dialogEntity.dialogText = uiLabel;
    dialogEntity.dialogTypeSprite = uiSprite1;
    dialogEntity.backgroundSprite = uiSprite2;
    return gameObject1;
  }
}
