﻿// Decompiled with JetBrains decompiler
// Type: TabButtonWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class TabButtonWidget : ToggleButtonWidget
{
  [HideInInspector]
  [SerializeField]
  public TabPanelWidget tabPanelWidget;
  [SerializeField]
  public GameObject tabContentObject;
  public int tabPosition;

  public override void Start()
  {
    base.Start();
    this.ActiveIsOverlay = false;
  }

  public override void OnEnable()
  {
    base.OnEnable();
    if ((UnityEngine.Object) this.tabPanelWidget.activeTabButton != (UnityEngine.Object) this)
    {
      this.HideTabContent();
    }
    else
    {
      this.activeState.SetActive(true);
      this.ShowTabContent();
    }
    this.Set();
  }

  public override void OnClick()
  {
    if (this.IsActive)
      return;
    this.HoverSubSprites(false);
    this.tabPanelWidget.OnTabClick(this);
    if ((UnityEngine.Object) this.hoverState != (UnityEngine.Object) null)
      this.hoverState.SetActive(false);
    this.Set();
  }

  public virtual void HideTabContent()
  {
    if (!((UnityEngine.Object) this.tabContentObject != (UnityEngine.Object) null))
      return;
    this.tabContentObject.SetActive(false);
  }

  public virtual void ShowTabContent()
  {
    if (!((UnityEngine.Object) this.tabContentObject != (UnityEngine.Object) null))
      return;
    this.tabContentObject.SetActive(true);
  }
}
