﻿// Decompiled with JetBrains decompiler
// Type: DotArea
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[JsonClassDesc("speedDebuff", JsonName = "speedDebuff")]
[JsonClassDesc("dradisRange", JsonName = "dradisRange")]
[JsonClassDesc("energyDrain", JsonName = "energyDrain")]
[JsonClassDesc("gameObject/transform/position", JsonName = "position")]
[JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
[JsonClassDesc("dmgPerTick", JsonName = "dmgPerTick")]
public class DotArea
{
  public Vector3 position = Vector3.zero;
  public float dmgPerTick = 50f;
  public float speedDebuff = 10f;
  public float energyDrain = 10f;
  public float dradisRange = 15f;
  [JsonField(JsonName = "key")]
  public string key = string.Empty;
  public GameObject gameObject;

  public DotArea()
  {
    this.Init();
  }

  public DotArea(string _key)
  {
    this.key = _key;
    this.Init();
  }

  public void Init()
  {
    Log.Add("Key of Element == " + this.key);
    this.gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
    this.gameObject.name = this.key;
    this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
    this.gameObject.GetComponent<Renderer>().material.color = Color.red;
    this.gameObject.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
    this.gameObject.transform.position = this.position;
    Object.Destroy((Object) this.gameObject.GetComponent<Collider>());
  }
}
