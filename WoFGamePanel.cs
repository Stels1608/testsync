﻿// Decompiled with JetBrains decompiler
// Type: WoFGamePanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class WoFGamePanel : NguiWidget, ILocalizeable
{
  [SerializeField]
  private List<WofGameNukeState> nukes = new List<WofGameNukeState>();
  private bool showWofConfirmation = true;
  private readonly List<Vector3> buttonPositions = new List<Vector3>() { new Vector3(5f, 1f), new Vector3(2f, 0.0f), new Vector3(8f, 0.0f), new Vector3(0.0f, 1f), new Vector3(10f, 1f), new Vector3(2f, 3f), new Vector3(5f, 3f), new Vector3(8f, 3f), new Vector3(1f, 5f), new Vector3(9f, 5f), new Vector3(4f, 6f), new Vector3(6f, 6f), new Vector3(5f, 8f) };
  private Dictionary<int, string> mapIdFrameIndex = new Dictionary<int, string>();
  private string TARGETS = "%$bgo.wof.select_targets%";
  private string FIRE = "%$bgo.wof.dradis_fire%";
  private string COSTS_FIRST = "%$bgo.wof.total_cost%";
  private string COSTS_LAST = "%$bgo.common.cubits_c%";
  private string RELOAD = "%$bgo.wof.dradis_reload%";
  private string FREE = "%$bgo.wof.free%";
  private const float FIELD_SIZE = 40f;
  [SerializeField]
  private UILabel title;
  [SerializeField]
  private List<WoFGameTargetButton> allTargets;
  [SerializeField]
  private List<WoFGameTargetButton> inactiveTargets;
  [SerializeField]
  public List<WoFGameTargetButton> activeTargets;
  [SerializeField]
  private ButtonWidget fireButton;
  [SerializeField]
  private UILabel costLabel;
  [SerializeField]
  private GameObject enemyTargets;
  [SerializeField]
  private ButtonWidget helpButton;
  [SerializeField]
  private UILabel cubitsLabel;
  [SerializeField]
  private UILabel targetCountLabel;
  [SerializeField]
  private UILabel cubitsDescLabel;
  [SerializeField]
  private UILabel targetsDescLabel;
  [SerializeField]
  private UILabel roundsLabel;
  [SerializeField]
  private UILabel jackpotLabel;
  [SerializeField]
  private UILabel jackpotName;
  [SerializeField]
  private ItemIcon jackpotItem;
  [SerializeField]
  private UIPopupList dropDown;
  public int roundCount;
  public bool forceStop;
  private bool activeGame;
  private List<int> costsPerStep;
  private int currentPrice;
  private bool isWofFree;
  private uint cubitCount;

  public void ReloadLanguageData()
  {
    this.title.text = Tools.ParseMessage("%$bgo.wof.dradis%").ToUpper();
    this.TARGETS = Tools.ParseMessage(this.TARGETS).ToUpper();
    this.FIRE = Tools.ParseMessage(this.FIRE);
    this.COSTS_FIRST = Tools.ParseMessage(this.COSTS_FIRST);
    this.COSTS_LAST = Tools.ParseMessage(this.COSTS_LAST);
    this.RELOAD = Tools.ParseMessage(this.RELOAD);
    this.FREE = Tools.ParseMessage(this.FREE);
    this.cubitsDescLabel.text = Tools.ParseMessage("%$bgo.common.cubits_c%").ToUpper();
    this.targetsDescLabel.text = Tools.ParseMessage("%$bgo.wof.dradis_selected_target%").ToUpper();
    this.jackpotLabel.text = Tools.ParseMessage("%$bgo.wof.jackpot_headline%").ToUpper();
    this.roundsLabel.text = Tools.ParseMessage("%$bgo.wof.rounds%");
    this.fireButton.TextLabel.text = this.TARGETS;
    this.costLabel.text = this.TARGETS;
  }

  public override void Start()
  {
    base.Start();
    this.InitPanel();
    this.helpButton.handleClick = new AnonymousDelegate(this.ShowHelp);
    this.ReloadLanguageData();
  }

  public void OnEnable()
  {
    this.fireButton.IsEnabled = false;
  }

  public void InitPanel()
  {
    this.allTargets = new List<WoFGameTargetButton>();
    this.activeTargets = new List<WoFGameTargetButton>();
    this.fireButton.handleClick = new AnonymousDelegate(this.CheckCubits);
    this.fireButton.IsEnabled = false;
    this.targetCountLabel.text = "0/6";
    GameObject prefab = (GameObject) Resources.Load("GUI/gui_2013/ui_elements/WoF/WoFGameTargetButton");
    if (!((UnityEngine.Object) prefab != (UnityEngine.Object) null))
      return;
    for (int index = 0; index < this.buttonPositions.Count; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.enemyTargets, prefab);
      gameObject.SetActive(true);
      gameObject.transform.localPosition = new Vector3(this.buttonPositions[index].x * 40f, (float) ((double) this.buttonPositions[index].y * 40.0 * -1.0));
      WoFGameTargetButton component = gameObject.GetComponent<WoFGameTargetButton>();
      component.gamePanel = this;
      if (Game.Me.Faction == Faction.Colonial)
      {
        component.iconName = "dradis_cylon_icon";
        component.iconActivName = "dradis_cylon_icon_activ";
      }
      else
      {
        component.iconName = "dradis_human_icon";
        component.iconActivName = "dradis_human_icon_activ";
      }
      this.allTargets.Add(component);
    }
    this.inactiveTargets = new List<WoFGameTargetButton>((IEnumerable<WoFGameTargetButton>) this.allTargets);
  }

  public void AddActiveTarget(WoFGameTargetButton target)
  {
    this.activeTargets.Add(target);
    this.inactiveTargets.Remove(target);
    this.targetCountLabel.text = this.activeTargets.Count.ToString() + "/6";
    this.OnSelectionChange();
    this.fireButton.TextLabel.text = this.FIRE;
    if (!this.fireButton.IsEnabled)
      this.fireButton.IsEnabled = true;
    this.CheckActiveCount();
    this.nukes[this.activeTargets.Count - 1].SetActive();
  }

  public void OnSelectionChange()
  {
    if (this.activeTargets.Count > 0)
    {
      int int32 = Convert.ToInt32(this.dropDown.value.Replace("x", string.Empty));
      this.roundCount = int32;
      this.currentPrice = 0;
      for (int index = 0; index < int32; ++index)
        this.currentPrice += this.costsPerStep[this.activeTargets.Count - 1];
      if (this.isWofFree && this.activeTargets.Count == 1)
        this.currentPrice -= this.costsPerStep[this.activeTargets.Count - 1];
      else if (this.isWofFree && this.activeTargets.Count > 1)
        this.currentPrice -= this.costsPerStep[this.activeTargets.Count - 1] - this.costsPerStep[this.activeTargets.Count - 2];
      if (this.currentPrice == 0)
        this.costLabel.text = this.FREE;
      else
        this.costLabel.text = this.COSTS_FIRST + (object) this.currentPrice + " " + this.COSTS_LAST;
    }
    else
      this.costLabel.text = this.TARGETS;
  }

  public void RemoveActiveTarget(WoFGameTargetButton target)
  {
    this.activeTargets.Remove(target);
    this.inactiveTargets.Add(target);
    this.targetCountLabel.text = this.activeTargets.Count.ToString() + "/6";
    this.OnSelectionChange();
    if (this.activeTargets.Count <= 0)
    {
      this.fireButton.IsEnabled = false;
      this.fireButton.TextLabel.text = this.TARGETS;
    }
    this.CheckActiveCount();
    this.nukes[this.activeTargets.Count].SetDefault();
  }

  public void CheckActiveCount()
  {
    if (this.activeTargets.Count == 6)
    {
      using (List<WoFGameTargetButton>.Enumerator enumerator = this.inactiveTargets.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.IsEnabled = false;
      }
    }
    else
    {
      if (this.inactiveTargets[0].IsEnabled)
        return;
      using (List<WoFGameTargetButton>.Enumerator enumerator = this.inactiveTargets.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.IsEnabled = true;
      }
    }
  }

  public void CheckCubits()
  {
    this.forceStop = false;
    if ((long) this.cubitCount < (long) this.currentPrice)
      DialogBoxFactoryNgui.CreateNotEnoughCubitsDialogBox(new AnonymousDelegate(this.ResetGame));
    else if (this.showWofConfirmation)
      DialogBoxFactoryNgui.CreateSimpleToggleSettingDialog(Tools.ParseMessage("%$bgo.wof.confirmation_text%", (object) this.currentPrice, (object) Convert.ToInt32(this.dropDown.value.Replace("x", string.Empty))), new AnonymousDelegate(this.StartGame), new AnonymousDelegate(this.ResetGame), UserSetting.ShowWofConfirmation, Tools.ParseMessage("%$bgo.wof.headline%"), (string) null, (string) null);
    else
      this.StartGame();
  }

  public void StartGame()
  {
    if (!this.forceStop)
    {
      this.fireButton.handleClick = new AnonymousDelegate(this.ResetGame);
      this.fireButton.TextLabel.text = this.RELOAD;
      this.fireButton.IsEnabled = false;
      this.dropDown.gameObject.GetComponent<DropDownWidget>().IsEnabled = false;
      this.dropDown.enabled = false;
      this.dropDown.GetComponent<Collider>().enabled = false;
      using (List<WoFGameTargetButton>.Enumerator enumerator = this.activeTargets.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Vanish();
      }
      using (List<WoFGameTargetButton>.Enumerator enumerator = this.allTargets.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.IsEnabled = false;
      }
      using (List<WofGameNukeState>.Enumerator enumerator = this.nukes.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.SetUsed();
      }
      WofProtocol.GetProtocol().RequestWofDraw(this.activeTargets.Count);
      if (this.isWofFree)
        this.isWofFree = false;
      this.activeGame = true;
    }
    else
    {
      this.forceStop = false;
      this.ResetGame();
    }
  }

  public void ResetGame()
  {
    this.fireButton.handleClick = new AnonymousDelegate(this.CheckCubits);
    this.fireButton.TextLabel.text = this.TARGETS;
    this.fireButton.IsEnabled = false;
    this.dropDown.gameObject.GetComponent<DropDownWidget>().IsEnabled = true;
    this.dropDown.enabled = true;
    this.dropDown.GetComponent<Collider>().enabled = true;
    using (List<WoFGameTargetButton>.Enumerator enumerator = this.allTargets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WoFGameTargetButton current = enumerator.Current;
        current.ResetToDefault();
        current.IsEnabled = true;
      }
    }
    using (List<WofGameNukeState>.Enumerator enumerator = this.nukes.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetDefault();
    }
    this.activeTargets.Clear();
    this.inactiveTargets = new List<WoFGameTargetButton>((IEnumerable<WoFGameTargetButton>) this.allTargets);
    this.currentPrice = 0;
    this.costLabel.text = this.TARGETS;
    this.targetCountLabel.text = "0/6";
    this.dropDown.value = "1x";
    this.activeGame = false;
  }

  public void SetGameDetails(WofJackpotContainer jackpot, bool free, List<int> costs)
  {
    this.costsPerStep = costs;
    this.isWofFree = free;
    this.SetJackpot(jackpot);
  }

  private void ShowHelp()
  {
    FacadeFactory.GetInstance().SendMessage(Message.ShowHelpScreen, (object) HelpScreenType.DradisContact);
  }

  public void SetJackpot(WofJackpotContainer jackpot)
  {
    this.jackpotName.text = jackpot.type != WofProtocol.JackpotType.Item ? Tools.ParseMessage(jackpot.guiCard.Name) + " " + Tools.ParseMessage("%$bgo.wof.jackpot_map%", (object) jackpot.amount) : Tools.ParseMessage(jackpot.guiCard.Name) + " x " + (object) jackpot.amount;
    this.jackpotItem.SetGUICard(jackpot.guiCard);
  }

  public void ShowReward(WofDrawReply reply)
  {
    if (!this.gameObject.activeSelf || this.activeTargets.Count == 0)
      return;
    this.SetJackpot(reply.jackpot);
    this.isWofFree = reply.freeWofGame;
    int num = 0;
    bool flag = false;
    for (int index = 0; index < reply.rewardItems.Count; ++index)
    {
      this.activeTargets[index].item.SetGUICard(reply.rewardItems[index].itemCard);
      num = index + 1;
    }
    for (int index = 0; index < reply.bonusMapCards.Count; ++index)
    {
      if (!reply.bonusMapCards[index].JackpotItem)
        this.activeTargets[index + num].item.SetGUICard(reply.bonusMapCards[index].Guicard);
      else
        flag = true;
    }
    if (flag)
      this.activeTargets[this.activeTargets.Count - 1].item.SetGUICard(reply.jackpot.guiCard);
    using (List<WoFGameTargetButton>.Enumerator enumerator = this.activeTargets.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.StartShowItem();
    }
    if (this.roundCount > 1)
    {
      --this.roundCount;
      this.Invoke("StartNextRound", 2f);
    }
    else
    {
      this.forceStop = true;
      this.Invoke("EnableButton", 2.5f);
    }
  }

  private void StartNextRound()
  {
    using (List<WoFGameTargetButton>.Enumerator enumerator = this.activeTargets.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WoFGameTargetButton current = enumerator.Current;
        current.ResetToDefault();
        current.IsActive = true;
      }
    }
    this.Invoke("StartGame", 1f);
  }

  private void EnableButton()
  {
    this.fireButton.IsEnabled = true;
    this.activeGame = false;
  }

  public void UpdateCubits(uint cubits)
  {
    this.cubitCount = cubits;
    this.cubitsLabel.text = this.cubitCount.ToString();
  }

  public void UpdateMapInformation(Dictionary<int, GUICard> dictionary)
  {
    this.mapIdFrameIndex.Clear();
    using (Dictionary<int, GUICard>.Enumerator enumerator = dictionary.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, GUICard> current = enumerator.Current;
        this.mapIdFrameIndex.Add(current.Key, current.Value.FrameIndex.ToString());
      }
    }
  }

  public void SetWofConfirmation(bool p)
  {
    this.showWofConfirmation = p;
  }

  public bool ActiveGame()
  {
    return this.activeGame;
  }
}
