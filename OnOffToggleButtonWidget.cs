﻿// Decompiled with JetBrains decompiler
// Type: OnOffToggleButtonWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class OnOffToggleButtonWidget : ButtonWidget, ILocalizeable
{
  private string onText = "%$bgo.etc.opt_auto_loot_on%";
  private string offText = "%$bgo.etc.opt_auto_loot_off%";
  private const int BORDER_SPACE = 20;
  [HideInInspector]
  [SerializeField]
  protected bool isActive;
  [SerializeField]
  public GameObject defaultState;
  [SerializeField]
  public GameObject activeState;
  [SerializeField]
  private UISprite replacer;
  [SerializeField]
  private UISprite grips;
  [SerializeField]
  private UISprite activeSprite;
  [SerializeField]
  private UISprite defaultSprite;
  [SerializeField]
  private UILabel activeLabel;
  [SerializeField]
  private UILabel defaultLabel;
  [SerializeField]
  private GameObject replaceObject;
  protected UserSetting setting;
  protected bool initValue;
  [HideInInspector]
  public OnSettingChanged OnSettingChanged;

  public bool IsActive
  {
    get
    {
      return this.isActive;
    }
    set
    {
      this.isActive = value;
      this.SetReplacerPosition(value);
      this.Set();
    }
  }

  public override void Start()
  {
    base.Start();
    this.ReloadLanguageData();
    this.activeLabel.color = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_RUNNING);
    this.defaultLabel.color = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_RUNNING);
  }

  public override void OnClick()
  {
    if (this.IsEnabled)
    {
      this.IsActive = !this.IsActive;
      this.SetReplacerPosition(this.IsActive);
      this.OnSettingChanged(this.setting, (object) this.IsActive);
    }
    base.OnClick();
  }

  public void Init(UserSetting sett, bool value)
  {
    this.setting = sett;
    this.initValue = value;
    this.IsActive = value;
  }

  public void ButtonText(string onTex, string offTex)
  {
    this.onText = onTex;
    this.offText = offTex;
  }

  private void InitButton()
  {
    int num = 20;
    if (this.activeLabel.width > num)
      num = this.activeLabel.width;
    else if (this.defaultLabel.width > num)
      num = this.defaultLabel.width;
    this.activeSprite.width = num + 20;
    this.defaultSprite.width = num + 20;
    this.activeLabel.transform.localPosition = new Vector3((float) (this.activeSprite.width / 2), (float) (this.activeSprite.height / -2), 0.0f);
    this.defaultLabel.transform.localPosition = new Vector3((float) (this.defaultSprite.width / 2), (float) (this.defaultSprite.height / -2), 0.0f);
    this.replacer.MakePixelPerfect();
    this.replacer.width = num + 20 - 2;
    this.Invoke("DelayedReposition", 1f / 1000f);
  }

  private void DelayedReposition()
  {
    BoxCollider component = this.GetComponent<BoxCollider>();
    component.center = new Vector3((float) this.activeSprite.width, (float) (this.activeSprite.height / -2 - 26), -4f);
    component.size = new Vector3((float) (this.activeSprite.width * 2), (float) this.activeSprite.height, 0.0f);
    this.IsActive = this.initValue;
  }

  public void SetReplacerPosition(bool activex)
  {
    this.replaceObject.transform.localPosition = !activex ? this.activeState.transform.localPosition : this.defaultState.transform.localPosition;
    this.grips.transform.localPosition = new Vector3(this.replacer.transform.localPosition.x + (float) (this.replacer.width / 2), this.replacer.transform.localPosition.y - (float) (this.replacer.height / 2));
  }

  protected virtual void Set()
  {
    if ((Object) this.defaultLabel != (Object) null)
      this.defaultLabel.gameObject.SetActive(!this.isActive);
    if (!((Object) this.activeLabel != (Object) null))
      return;
    this.activeLabel.gameObject.SetActive(this.isActive);
  }

  public void ReloadLanguageData()
  {
    this.activeLabel.text = BsgoLocalization.Get(this.onText);
    this.defaultLabel.text = BsgoLocalization.Get(this.offText);
    this.InitButton();
  }
}
