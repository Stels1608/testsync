﻿// Decompiled with JetBrains decompiler
// Type: NummericStepperWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class NummericStepperWidget : NguiWidget
{
  public UIInput input;
  public ButtonWidget upButton;
  public ButtonWidget downButton;
  public double maxValue;
  public double minValue;
  public double increment;

  public override void Start()
  {
    base.Start();
    this.input.value = 0.ToString();
  }

  public void OnSubmitNummericValue(string text)
  {
    double result;
    if (!double.TryParse(text, out result))
      result = 0.0;
    if (result % this.increment != 0.0)
      result -= result % this.increment;
    if (result > this.maxValue)
      result = this.maxValue;
    if (result < this.minValue)
      result = this.minValue;
    this.input.value = result.ToString();
  }

  public void OnNummericButtonClick(string text)
  {
    double num = 0.0;
    if (string.CompareOrdinal(text, "up") == 0)
    {
      num = double.Parse(this.input.value) + this.increment;
      if (num > this.maxValue)
        num = this.maxValue;
    }
    else if (string.CompareOrdinal(text, "down") == 0)
    {
      num = double.Parse(this.input.value) - this.increment;
      if (num < this.minValue)
        num = this.minValue;
    }
    this.input.value = num.ToString();
  }
}
