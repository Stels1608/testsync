﻿// Decompiled with JetBrains decompiler
// Type: CutsceneSequenceDockingBasestarTier2
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class CutsceneSequenceDockingBasestarTier2 : CutsceneSequenceDockingBase
{
  private const string animClipName = "BasestarLandingAnimTier2Var2";

  protected override string GetSceneSetupName
  {
    get
    {
      return this.basestarStandardName;
    }
  }

  protected override CutsceneSkipMode GetSkipMode
  {
    get
    {
      return CutsceneSkipMode.SkipImmediately;
    }
  }

  protected override bool InitAdditionalNeededObjects()
  {
    CutsceneAssetSpawnerRemote.SpawnAssetAtLocator(PrefabNames.GetShipPrefabName(ShipType.HeavyRaider), this.CutsceneSetup.OtherShipLocators[0], false, false, JumpEffectNew.JumpEffectSize.Undefined);
    return true;
  }

  protected override string GetDockingTargetPrefabName()
  {
    return this.basestarPrefabName;
  }

  protected override string GetAnimClipName()
  {
    return "BasestarLandingAnimTier2Var2";
  }

  protected override void ShowRandomDockingSubtitles()
  {
    switch (Random.Range(0, 3))
    {
      case 0:
        CutsceneVideoFx.ShowSubtitleText(this.UiHandler, 1f, BsgoLocalization.Get("%$bgo.cutscenes.radio_chatter.landing_basestar_t2_var1.1%", (object) Game.Me.Name));
        CutsceneVideoFx.ShowSubtitleText(this.UiHandler, 6f, BsgoLocalization.Get("%$bgo.cutscenes.radio_chatter.landing_basestar_t2_var1.2%"));
        break;
      case 1:
        CutsceneVideoFx.ShowSubtitleText(this.UiHandler, 1f, BsgoLocalization.Get("%$bgo.cutscenes.radio_chatter.landing_basestar_t2_var2.1%", (object) Game.Me.Name));
        CutsceneVideoFx.ShowSubtitleText(this.UiHandler, 3.5f, BsgoLocalization.Get("%$bgo.cutscenes.radio_chatter.landing_basestar_t2_var2.2%"));
        CutsceneVideoFx.ShowSubtitleText(this.UiHandler, 8f, BsgoLocalization.Get("%$bgo.cutscenes.radio_chatter.landing_basestar_t2_var2.3%"));
        break;
      case 2:
        CutsceneVideoFx.ShowSubtitleText(this.UiHandler, 1f, BsgoLocalization.Get("%$bgo.cutscenes.radio_chatter.landing_basestar_t2_var3.1%", (object) Game.Me.Name.ToUpper()));
        CutsceneVideoFx.ShowSubtitleText(this.UiHandler, 4.5f, BsgoLocalization.Get("%$bgo.cutscenes.radio_chatter.landing_basestar_t2_var3.2%", (object) Game.Me.RankShort));
        CutsceneVideoFx.ShowSubtitleText(this.UiHandler, 7.5f, BsgoLocalization.Get("%$bgo.cutscenes.radio_chatter.landing_basestar_t2_var3.3%", (object) Game.Me.Name.ToUpper()));
        break;
    }
  }
}
