﻿// Decompiled with JetBrains decompiler
// Type: GuildInfoMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System;
using System.Collections.Generic;

public class GuildInfoMessage : IMessage<Message>
{
  public uint GuildId { get; private set; }

  public string GuildName { get; private set; }

  public List<RankDefinition> Ranks { get; private set; }

  public List<GuildMemberInfo> Members { get; private set; }

  public object Data
  {
    get
    {
      throw new NotImplementedException();
    }
    set
    {
      throw new NotImplementedException();
    }
  }

  public Message Id
  {
    get
    {
      return Message.GuildInfo;
    }
  }

  public GuildInfoMessage(uint guildId, string guildName, List<RankDefinition> ranks, List<GuildMemberInfo> members)
  {
    this.GuildId = guildId;
    this.GuildName = guildName;
    this.Ranks = ranks;
    this.Members = members;
  }
}
