﻿// Decompiled with JetBrains decompiler
// Type: SectorBeaconStateUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class SectorBeaconStateUpdate : GalaxyMapUpdate
{
  public float State { get; private set; }

  public SectorBeaconStateUpdate(Faction faction, uint sectorId, float state)
    : base(GalaxyUpdateType.SectorBeaconState, faction, (int) sectorId)
  {
    this.State = state;
  }

  public override string ToString()
  {
    return "[SectorBeaconStateUpdate] (Faction: " + (object) this.Faction + ", SectorId: " + (object) this.SectorId + ", State:" + (object) this.State + ")";
  }
}
