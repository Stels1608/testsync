﻿// Decompiled with JetBrains decompiler
// Type: InputDispatcher
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class InputDispatcher
{
  private float timeToLogout = 1800f;
  private const float LOGOUT_DELAY = 1800f;
  private readonly List<InputListener> listeners;
  private ushort lastInput;
  private NotifyBoxUi inactiveNotificationBox;
  private InputListener focused;
  private readonly SettingsDataProvider settingsDataProvider;

  public InputListener Focused
  {
    get
    {
      return this.focused;
    }
    set
    {
      this.focused = value;
    }
  }

  public InputDispatcher()
  {
    this.listeners = new List<InputListener>();
    this.settingsDataProvider = FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider") as SettingsDataProvider;
  }

  public void UpdateInactiveNotification()
  {
    this.timeToLogout -= Time.deltaTime;
    if ((double) this.timeToLogout < 0.0)
      Game.DoLogoutEx("User was logged out due to prolonged inactivity.");
    if ((double) this.timeToLogout <= 15.0 && (double) this.timeToLogout >= 0.0)
    {
      if (!((UnityEngine.Object) this.inactiveNotificationBox == (UnityEngine.Object) null))
        return;
      this.inactiveNotificationBox = MessageBoxFactory.CreateNotifyBox(BsgoCanvas.Windows);
      this.inactiveNotificationBox.SetContent((OnMessageBoxClose) (param0 => this.timeToLogout = 1800f), "%$bgo.InactiveTimer.layout.title%", "%$bgo.InactiveTimer.layout.text%", 15U);
      this.inactiveNotificationBox.ShowTimer(true);
    }
    else
    {
      if (!((UnityEngine.Object) this.inactiveNotificationBox != (UnityEngine.Object) null))
        return;
      this.inactiveNotificationBox.CloseWindow(MessageBoxActionType.Ok);
    }
  }

  public void ResetInactiveTimer(KeyCode keyCode)
  {
    this.ResetInactiveTimer((ushort) keyCode);
  }

  public void ResetInactiveTimer(ushort inputCode)
  {
    if ((int) inputCode == (int) this.lastInput)
      return;
    this.lastInput = inputCode;
    this.timeToLogout = 1800f;
  }

  private Action ApplyInvertedVerticalOption(Action action)
  {
    if (this.settingsDataProvider.CurrentSettings.InvertedVertical && (UnityEngine.Object) RoomLevel.GetLevel() == (UnityEngine.Object) null)
    {
      switch (action)
      {
        case Action.SlopeForwardOrSlideUp:
          action = Action.SlopeBackwardOrSlideDown;
          break;
        case Action.SlopeBackwardOrSlideDown:
          action = Action.SlopeForwardOrSlideUp;
          break;
        case Action.JoystickTurnUp:
          action = Action.JoystickTurnDown;
          break;
        case Action.JoystickTurnDown:
          action = Action.JoystickTurnUp;
          break;
      }
    }
    return action;
  }

  public void SendActionUp(KeyCode keyCode, Action action)
  {
    action = this.ApplyInvertedVerticalOption(action);
    bool flag = !InputDispatcher.IsMovementAction(action);
    if (this.focused != null && (!(this.focused is GUIChatNew) || flag) && this.focused.OnKeyUp(keyCode, action))
      return;
    using (List<InputListener>.Enumerator enumerator = this.listeners.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InputListener current = enumerator.Current;
        if (current.HandleKeyboardInput && current.OnKeyUp(keyCode, action) && flag)
          break;
      }
    }
  }

  public void SendActionDown(KeyCode keyCode, Action action)
  {
    action = this.ApplyInvertedVerticalOption(action);
    bool flag = !InputDispatcher.IsMovementAction(action);
    if (this.focused != null && (!(this.focused is GUIChatNew) || flag) && this.focused.OnKeyDown(keyCode, action))
      return;
    using (List<InputListener>.Enumerator enumerator = this.listeners.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InputListener current = enumerator.Current;
        if (current.HandleKeyboardInput && current.OnKeyDown(keyCode, action) && flag)
          break;
      }
    }
  }

  public void SendJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    this.ResetInactiveTimer((ushort) triggerCode);
    action = this.ApplyInvertedVerticalOption(action);
    for (int index = 0; index < this.listeners.Count; ++index)
    {
      InputListener nputListener = this.listeners[index];
      if (nputListener.HandleJoystickAxesInput && nputListener.OnJoystickAxesInput(triggerCode, modifierCode, action, magnitude, delta, isAxisInverted))
        break;
    }
  }

  public void SendMouseDown(float2 mousePosition, KeyCode keyCode)
  {
    this.ResetInactiveTimer(keyCode);
    if (Game.TooltipManager.MouseDown(mousePosition, keyCode) || this.focused != null && this.focused.OnMouseDown(mousePosition, keyCode))
      return;
    foreach (InputListener nputListener in this.listeners.ToArray())
    {
      if (nputListener.HandleMouseInput && nputListener.OnMouseDown(mousePosition, keyCode))
        break;
    }
  }

  public void SendMouseUp(float2 mousePosition, KeyCode keyCode)
  {
    this.ResetInactiveTimer(keyCode);
    if (Game.TooltipManager.MouseUp(mousePosition, keyCode) || this.focused != null && this.focused.OnMouseUp(mousePosition, keyCode))
      return;
    using (List<InputListener>.Enumerator enumerator = this.listeners.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InputListener current = enumerator.Current;
        if (current.HandleMouseInput && current.OnMouseUp(mousePosition, keyCode))
          break;
      }
    }
  }

  public void SendMouseScrollDown()
  {
    this.ResetInactiveTimer(KeyCode.None);
    if (this.focused != null && this.focused.OnMouseScrollDown())
      return;
    using (List<InputListener>.Enumerator enumerator = this.listeners.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InputListener current = enumerator.Current;
        if (current != null && current.HandleMouseInput && current.OnMouseScrollDown())
          break;
      }
    }
  }

  public void SendMouseScrollUp()
  {
    this.ResetInactiveTimer(KeyCode.None);
    if (this.focused != null && this.focused.OnMouseScrollUp())
      return;
    using (List<InputListener>.Enumerator enumerator = this.listeners.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InputListener current = enumerator.Current;
        if (current.HandleMouseInput && current.OnMouseScrollUp())
          break;
      }
    }
  }

  public void SendMousePosition(float2 newMousePosition)
  {
    Game.TooltipManager.MouseMove(newMousePosition);
    using (List<InputListener>.Enumerator enumerator = this.listeners.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InputListener current = enumerator.Current;
        if (current is GUIPanel && (current as GUIPanel).IsRendered)
        {
          if ((current as GUIPanel).OnShowTooltip(newMousePosition))
            break;
        }
      }
    }
    if (this.focused != null)
      this.focused.OnMouseMove(newMousePosition);
    for (int index = 0; index < this.listeners.Count; ++index)
    {
      InputListener nputListener = this.listeners[index];
      if (nputListener != this.focused && nputListener.HandleMouseInput)
        nputListener.OnMouseMove(newMousePosition);
    }
  }

  public void AddListener(InputListener listener)
  {
    if (listener == null)
      Debug.LogWarning((object) "Adding NULL inputListener");
    this.listeners.Add(listener);
    SpaceCameraBase spaceCamera = this.GetSpaceCamera();
    if (!((UnityEngine.Object) spaceCamera != (UnityEngine.Object) null))
      return;
    this.listeners.Remove((InputListener) spaceCamera);
    this.listeners.Add((InputListener) spaceCamera);
  }

  public ReadOnlyCollection<InputListener> GetListeners()
  {
    return this.listeners.AsReadOnly();
  }

  public void AddListenerToFront(InputListener listener)
  {
    this.listeners.Insert(0, listener);
  }

  public void RemoveListener(InputListener listener)
  {
    this.listeners.Remove(listener);
  }

  public void RemoveAllListeners(Predicate<InputListener> match)
  {
    this.listeners.RemoveAll(match);
  }

  public void ApplyMacChatHack()
  {
    if (Application.platform != RuntimePlatform.OSXWebPlayer)
      return;
    GUIChatNew guiChatNew = Game.GUIManager.Find<GUIChatNew>();
    if (guiChatNew == null || !guiChatNew.IsFocused || (Event.current == null || Event.current.type != UnityEngine.EventType.Used) || (Event.current.keyCode != KeyCode.Return || guiChatNew.Current == Game.ChatStorage.CombatLog))
      return;
    guiChatNew.Send();
  }

  public SpaceCameraBase GetSpaceCamera()
  {
    return this.listeners.Find((Predicate<InputListener>) (l => l is SpaceCameraBase)) as SpaceCameraBase;
  }

  public static bool IsMovementAction(Action action)
  {
    if (action != Action.SlopeForwardOrSlideUp && action != Action.SlopeBackwardOrSlideDown && (action != Action.TurnOrSlideLeft && action != Action.TurnOrSlideRight) && (action != Action.RollLeft && action != Action.RollRight))
      return action == Action.Boost;
    return true;
  }
}
