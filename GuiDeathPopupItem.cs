﻿// Decompiled with JetBrains decompiler
// Type: GuiDeathPopupItem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GuiDeathPopupItem : GuiPanel
{
  private readonly GuiLabel m_itemDescription;
  private readonly GuiLabel m_shortDescription;

  public GuiDeathPopupItem(ShipItem item)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiDeathPopupItem.\u003CGuiDeathPopupItem\u003Ec__AnonStorey9B itemCAnonStorey9B = new GuiDeathPopupItem.\u003CGuiDeathPopupItem\u003Ec__AnonStorey9B();
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey9B.item = item;
    // ISSUE: explicit constructor call
    base.\u002Ector();
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey9B.\u003C\u003Ef__this = this;
    this.Size = new Vector2(300f, 40f);
    // ISSUE: reference to a compiler-generated field
    this.SetTooltip(itemCAnonStorey9B.item);
    this.m_itemDescription = new GuiLabel();
    this.m_itemDescription.Size = this.Size;
    this.m_itemDescription.PositionX = 65f;
    this.m_itemDescription.WordWrap = true;
    this.m_itemDescription.Alignment = TextAnchor.UpperLeft;
    this.m_itemDescription.Font = Gui.Options.FontBGM_BT;
    this.m_itemDescription.FontSize = 12;
    this.AddChild((GuiElementBase) this.m_itemDescription);
    this.m_shortDescription = new GuiLabel();
    this.m_shortDescription.Size = this.Size;
    this.m_shortDescription.PositionX = 65f;
    this.m_shortDescription.PositionY = 17f;
    this.m_shortDescription.Alignment = TextAnchor.MiddleLeft;
    this.m_shortDescription.Font = Gui.Options.FontBGM_BT;
    this.m_shortDescription.FontSize = 10;
    this.m_shortDescription.NormalColor = Color.grey;
    this.m_shortDescription.OverColor = Color.grey;
    this.AddChild((GuiElementBase) this.m_shortDescription);
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey9B.m_atlasCache = new AtlasCache(new float2(40f, 35f));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    itemCAnonStorey9B.item.ItemGUICard.IsLoaded.AddHandler(new SignalHandler(itemCAnonStorey9B.\u003C\u003Em__11F));
  }
}
