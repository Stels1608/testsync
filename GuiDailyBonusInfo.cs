﻿// Decompiled with JetBrains decompiler
// Type: GuiDailyBonusInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GuiDailyBonusInfo : GuiDialog
{
  public GuiDailyBonusInfo(int day, List<RewardCard> rewards)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiDailyBonusInfo.\u003CGuiDailyBonusInfo\u003Ec__AnonStorey82 infoCAnonStorey82 = new GuiDailyBonusInfo.\u003CGuiDailyBonusInfo\u003Ec__AnonStorey82();
    // ISSUE: reference to a compiler-generated field
    infoCAnonStorey82.rewards = rewards;
    // ISSUE: explicit constructor call
    base.\u002Ector();
    GuiDialogPopupManager.ShowDialog((GuiPanel) this, true);
    this.Align = Align.MiddleCenter;
    Texture2D texture2D = ResourceLoader.Load<Texture2D>("GUI/DailyBonus/background");
    this.m_backgroundImage.Texture = texture2D;
    this.Size = new Vector2((float) texture2D.width, (float) texture2D.height);
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.daily_bonus.1_title%", Gui.Options.FontBGM_BT, 20), Align.UpCenter, new Vector2(0.0f, 10f));
    if (day < 5)
    {
      this.AddChild((GuiElementBase) new GuiLabel("%$bgo.daily_bonus.1_today%", Gui.Options.FontBGM_BT, 15), Align.UpCenter, new Vector2((float) ((day - 1) * 160 - 314), 80f));
      this.AddChild((GuiElementBase) new GuiLabel("%$bgo.daily_bonus.1_tomorrow%", Gui.Options.FontBGM_BT, 15), Align.UpCenter, new Vector2((float) ((day - 1) * 160 - 150), 80f));
    }
    else
      this.AddChild((GuiElementBase) new GuiLabel("%$bgo.daily_bonus.1_today_and_tomorrow%", Gui.Options.FontBGM_BT, 15)
      {
        Alignment = TextAnchor.LowerCenter
      }, Align.UpCenter, new Vector2(322f, 64f));
    this.AddChild((GuiElementBase) new GuiImage("GUI/DailyBonus/bar" + day.ToString()), Align.UpCenter, new Vector2(-27f, 114f));
    this.AddChild((GuiElementBase) new GuiImage("GUI/DailyBonus/numbers"), Align.UpCenter, new Vector2(5f, 118f));
    this.AddChild((GuiElementBase) new GuiImage("GUI/DailyBonus/ring"), Align.UpCenter, new Vector2((float) ((day - 1) * 160 - 317), 100f));
    this.AddChild((GuiElementBase) new GuiImage("GUI/DailyBonus/award"), Align.UpCenter, new Vector2(10f, 170f));
    GuiButton guiButton = new GuiButton("%$bgo.daily_bonus.1_button%");
    guiButton.SizeX *= 1.3f;
    guiButton.SizeY = 30f;
    guiButton.Label.Font = Gui.Options.FontBGM_BT;
    guiButton.Label.FontSize = 14;
    // ISSUE: reference to a compiler-generated method
    guiButton.Pressed = new AnonymousDelegate(infoCAnonStorey82.\u003C\u003Em__B5);
    this.AddChild((GuiElementBase) guiButton, Align.DownCenter, new Vector2(0.0f, -15f));
    this.OnClose = (AnonymousDelegate) (() => GuiDialogPopupManager.CloseDialog());
  }

  [Gui2Editor]
  public static void Show(int day)
  {
    GuiDailyBonusInfo guiDailyBonusInfo = new GuiDailyBonusInfo(day, new List<RewardCard>());
  }

  public static void Show(int day, List<RewardCard> rewards)
  {
    GuiDailyBonusInfo guiDailyBonusInfo = new GuiDailyBonusInfo(day, rewards);
  }

  [Gui2Editor]
  public static void ShowReward()
  {
    GuiDailyBonusReward dailyBonusReward = new GuiDailyBonusReward(new List<RewardCard>());
  }
}
