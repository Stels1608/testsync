﻿// Decompiled with JetBrains decompiler
// Type: MotionAnalyzerDrawOptions
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class MotionAnalyzerDrawOptions
{
  [NonSerialized]
  public bool drawHeelToe = true;
  [NonSerialized]
  public bool drawTrajectories = true;
  [NonSerialized]
  public bool drawGraph = true;
  [NonSerialized]
  public bool normalizeGraph = true;
  [NonSerialized]
  public bool isolateHorisontal = true;
  [NonSerialized]
  public bool drawFootPrints = true;
  [NonSerialized]
  public int currentLeg;
  [NonSerialized]
  public bool drawAllFeet;
  [NonSerialized]
  public bool drawFootBase;
  [NonSerialized]
  public bool drawTrajectoriesProjected;
  [NonSerialized]
  public bool drawThreePoints;
  [NonSerialized]
  public bool drawStanceMarkers;
  [NonSerialized]
  public bool drawBalanceCurve;
  [NonSerialized]
  public bool drawLiftedCurve;
  [NonSerialized]
  public bool isolateVertical;
  [NonSerialized]
  public float graphScaleH;
  [NonSerialized]
  public float graphScaleV;
}
