﻿// Decompiled with JetBrains decompiler
// Type: FXManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class FXManager : MonoBehaviour
{
  public List<EffectState> effects = new List<EffectState>();
  private Dictionary<EffectCategory, List<EffectState>> categorizedEffects = new Dictionary<EffectCategory, List<EffectState>>();
  private EffectCategory enabledCategories = EffectCategory.Any;
  private static FXManager instance;

  public static FXManager Instance
  {
    get
    {
      return FXManager.instance;
    }
  }

  public Dictionary<EffectCategory, List<EffectState>> CategorizedEffects
  {
    get
    {
      return this.categorizedEffects;
    }
  }

  public void PutEffect(EffectState effect)
  {
    if ((this.enabledCategories & effect.Category) == EffectCategory.Nil || this.effects.Contains(effect))
      return;
    this.effects.Add(effect);
    List<EffectState> effectStateList;
    if (!this.categorizedEffects.TryGetValue(effect.Category, out effectStateList))
    {
      effectStateList = new List<EffectState>();
      this.categorizedEffects.Add(effect.Category, effectStateList);
    }
    if (effectStateList.Contains(effect))
      return;
    effectStateList.Add(effect);
  }

  private void Awake()
  {
    FXManager.instance = this;
  }

  private void Update()
  {
    using (List<EffectState>.Enumerator enumerator = new List<EffectState>((IEnumerable<EffectState>) this.effects).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EffectState current = enumerator.Current;
        if (current.New)
        {
          current.Launch();
          current.Quality = current.EvaluateQuality();
        }
        if (current.Terminated)
          this.RemoveEffect(current);
        if (current.Active)
          current.Update();
      }
    }
  }

  private void OnDisable()
  {
    FXManager.instance = (FXManager) null;
  }

  private void OnCategoryDisabled(EffectCategory categoryFlag)
  {
    List<EffectState> effectStateList;
    if (!this.categorizedEffects.TryGetValue(categoryFlag, out effectStateList))
      return;
    using (List<EffectState>.Enumerator enumerator = effectStateList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Terminate();
    }
  }

  private void RemoveEffect(EffectState effect)
  {
    this.effects.Remove(effect);
    List<EffectState> effectStateList;
    if (!this.categorizedEffects.TryGetValue(effect.Category, out effectStateList))
      return;
    effectStateList.Remove(effect);
  }

  public bool HaveCategory(EffectCategory category)
  {
    return (this.enabledCategories & category) != EffectCategory.Nil;
  }

  public void AddCategory(EffectCategory category)
  {
    this.enabledCategories |= category;
  }

  public void RemoveCategory(EffectCategory category)
  {
    this.enabledCategories &= ~category;
    this.OnCategoryDisabled(category);
  }
}
