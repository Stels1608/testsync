﻿// Decompiled with JetBrains decompiler
// Type: CutsceneLoadingScreen
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class CutsceneLoadingScreen : MonoBehaviour
{
  private VideoUiHandler uiHandler;
  private GUIStyle loadingTextStyle;
  private string loadingText;

  private void Awake()
  {
    this.uiHandler = this.gameObject.AddComponent<VideoUiHandler>();
    CutsceneVideoFx.TurnToBlack(this.uiHandler);
    this.loadingTextStyle = new GUIStyle(GUIStyle.none);
    this.loadingTextStyle.font = Gui.Options.fontEurostileTRegCon;
    this.loadingTextStyle.normal.textColor = Color.white;
    this.loadingTextStyle.fontSize = 14;
    this.loadingTextStyle.alignment = TextAnchor.LowerLeft;
    this.loadingTextStyle.padding = new RectOffset(0, 30, 20, 20);
    this.loadingTextStyle.wordWrap = true;
    this.loadingText = Tools.ParseMessage("%$bgo.cutscenes.loading_screen.loading%");
  }

  private void OnGUI()
  {
    this.loadingTextStyle.fontSize = Mathf.RoundToInt(Mathf.Clamp(0.01822917f * (float) Screen.height, 14f, 18f));
    float width = this.loadingTextStyle.CalcSize(new GUIContent(this.loadingText + "...")).x;
    Rect position = new Rect((float) Screen.width - width, 0.0f, width, (float) Screen.height);
    int num = (int) ((double) Time.time * 4.0) % 4;
    string text = this.loadingText;
    for (int index = 0; index < num; ++index)
      text += ".";
    GUI.depth = 0;
    GUI.Label(position, text, this.loadingTextStyle);
  }
}
