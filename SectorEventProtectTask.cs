﻿// Decompiled with JetBrains decompiler
// Type: SectorEventProtectTask
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class SectorEventProtectTask : SectorEventTask
{
  public uint VipObjectId { get; private set; }

  public DateTime EndTime { get; private set; }

  public override SectorEventTaskType TaskType
  {
    get
    {
      return SectorEventTaskType.Protect;
    }
  }

  public SectorEventProtectTask(byte index, SectorEventTaskSubType subType, SectorEventState taskState, uint vipObjectId, DateTime endTime)
    : base(index, subType, taskState)
  {
    this.VipObjectId = vipObjectId;
    this.EndTime = endTime;
  }
}
