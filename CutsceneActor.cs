﻿// Decompiled with JetBrains decompiler
// Type: CutsceneActor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CutsceneActor : MonoBehaviour
{
  public string ActorId;

  public static GameObject FindActorById(string actorId)
  {
    CutsceneActor[] objectsOfType = Object.FindObjectsOfType<CutsceneActor>();
    for (int index = 0; index < objectsOfType.Length; ++index)
    {
      if (objectsOfType[index].ActorId.ToLower() == actorId.ToLower())
        return objectsOfType[index].gameObject;
    }
    Debug.LogError((object) ("FindActorById(): actorId " + actorId + " not found."));
    return (GameObject) null;
  }
}
