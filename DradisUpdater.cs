﻿// Decompiled with JetBrains decompiler
// Type: DradisUpdater
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class DradisUpdater
{
  private readonly ObjectRegistry registry;

  public DradisUpdater(SpaceLevel spaceLevel)
  {
    this.registry = spaceLevel.GetObjectRegistry();
    Timer.CreateTimer("CalcDistancesTimer", 1f, 1f, new Timer.TickHandler(this.UpdateAllObjectsFlags));
    Timer.CreateTimer("StartSharedDradisVisionTimer", 0.5f, 1f, new Timer.TickHandler(this.CalcSharedDradisVision));
    Timer.CreateTimer("SendDradis", 0.5f, 60f, new Timer.TickHandler(this.SendDradis));
  }

  private void UpdateAllObjectsFlags()
  {
    foreach (SpaceObject spaceObject in this.registry.GetAllLoaded())
      DradisHelper.UpdateObjectFlags(spaceObject);
  }

  private void CalcSharedDradisVision()
  {
    foreach (SpaceObject target in this.registry.GetAllLoaded())
    {
      bool flag = false;
      foreach (Player member in Game.Me.Party.Members)
      {
        PlayerShip playerShip = member.PlayerShip;
        if (member.IsSameSector && playerShip != null && (playerShip.IsInPlayersMapRange && playerShip.ShipCardLight.ShipRoleDeprecated == ShipRoleDeprecated.Carrier) && member.IsSameSector)
          flag |= DradisHelper.IsInDetectorsMapRange((ObjectStats) member.Stats, (SpaceObject) playerShip, target);
      }
      target.IsInSharedMapRange = flag;
    }
  }

  private void SendDradis()
  {
    PlayerProtocol.GetProtocol().SendDradisData();
  }
}
