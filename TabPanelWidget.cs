﻿// Decompiled with JetBrains decompiler
// Type: TabPanelWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class TabPanelWidget : NguiWidget
{
  [SerializeField]
  protected List<TabButtonWidget> buttons;
  [SerializeField]
  public TabButtonWidget activeTabButton;
  [SerializeField]
  public int activeButtonIndex;
  public bool sortByName;
  public AnonymousDelegate handleStart;

  public List<TabButtonWidget> Buttons
  {
    get
    {
      return this.buttons;
    }
    set
    {
      this.buttons = value;
    }
  }

  public void Awake()
  {
    this.Buttons = new List<TabButtonWidget>();
    this.InitializeTabs();
    List<TabButtonWidget> tabButtonWidgetList = new List<TabButtonWidget>();
    this.Buttons = !this.sortByName ? this.Buttons.OrderBy<TabButtonWidget, int>((Func<TabButtonWidget, int>) (v => v.tabPosition)).ToList<TabButtonWidget>() : this.Buttons.OrderBy<TabButtonWidget, string>((Func<TabButtonWidget, string>) (v => v.name)).ToList<TabButtonWidget>();
  }

  public override void Start()
  {
    base.Start();
    if (!((UnityEngine.Object) this.activeTabButton == (UnityEngine.Object) null))
      return;
    this.SetActiveButton(this.activeButtonIndex);
  }

  public void OnAfterEnable()
  {
    this.SetActiveButton(this.activeButtonIndex);
  }

  protected void InitializeTabs()
  {
    this.Buttons.Clear();
    for (int index = 0; index < this.transform.childCount; ++index)
    {
      TabButtonWidget component = this.transform.GetChild(index).gameObject.GetComponent<TabButtonWidget>();
      if ((UnityEngine.Object) null != (UnityEngine.Object) component)
        this.AddTabButton(component);
    }
  }

  public void SetActiveButton(int index)
  {
    if (this.Buttons.Count <= 0 || index > this.Buttons.Count)
      return;
    using (List<TabButtonWidget>.Enumerator enumerator = this.Buttons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TabButtonWidget current = enumerator.Current;
        current.IsActive = false;
        current.HideTabContent();
      }
    }
    this.activeButtonIndex = index;
    this.OnTabClick(this.Buttons[this.activeButtonIndex]);
  }

  public void SetAllActive()
  {
    if (this.Buttons.Count <= 0)
      return;
    using (List<TabButtonWidget>.Enumerator enumerator = this.Buttons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.IsActive = true;
    }
  }

  public void AddTabButton(TabButtonWidget tabButton)
  {
    this.Buttons.Add(tabButton);
    tabButton.tabPanelWidget = this;
  }

  private void RemoveTabButton(TabButtonWidget tabButton)
  {
    if (!this.Buttons.Contains(tabButton))
      return;
    this.Buttons.Remove(tabButton);
  }

  public virtual void OnTabClick(TabButtonWidget tabButton)
  {
    if (!((UnityEngine.Object) tabButton != (UnityEngine.Object) this.activeTabButton))
      return;
    if ((UnityEngine.Object) this.activeTabButton != (UnityEngine.Object) null)
    {
      this.activeTabButton.IsActive = false;
      this.activeTabButton.HideTabContent();
    }
    tabButton.IsActive = true;
    tabButton.ShowTabContent();
    this.activeTabButton = tabButton;
  }
}
