﻿// Decompiled with JetBrains decompiler
// Type: RenderAreaHelper
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RenderAreaHelper : MonoBehaviour
{
  [HideInInspector]
  [SerializeField]
  protected Color materialColor = Color.white;
  [HideInInspector]
  [SerializeField]
  protected float renderDistance = 5f;
  [SerializeField]
  [HideInInspector]
  protected float fieldOfView = 60f;
  protected const string DEFAULT_SHADER = "Unlit/Transparent Colored";
  protected const string NAME_CLONE = "RenderArea Object";
  protected const string NAME_CAMERA = "RenderArea Camera";
  protected const string NAME_WIDGET_TEXTURE = "UITexture";
  protected const string NAME_RENDER_TEXTURE = "Render Texture";
  protected bool initialized;
  [SerializeField]
  [HideInInspector]
  protected GameObject targetObject;
  [HideInInspector]
  [SerializeField]
  protected Shader materialShader;
  [SerializeField]
  [HideInInspector]
  protected int textureSizeX;
  [HideInInspector]
  [SerializeField]
  protected int textureSizeY;
  [HideInInspector]
  [SerializeField]
  protected LayerMask workLayer;
  [HideInInspector]
  [SerializeField]
  private UITexture widget;
  [HideInInspector]
  [SerializeField]
  private RenderTexture renderTexture;
  [HideInInspector]
  [SerializeField]
  private GameObject camObject;
  [HideInInspector]
  [SerializeField]
  private GameObject cloneObject;

  [HideInInspector]
  public GameObject TargetObject
  {
    get
    {
      return this.targetObject;
    }
    set
    {
      this.Clear();
      this.targetObject = value;
      this.Setup();
    }
  }

  [HideInInspector]
  public Color MaterialColor
  {
    get
    {
      return this.materialColor;
    }
    set
    {
      this.materialColor = value;
      if (!((Object) this.widget != (Object) null) || !((Object) this.widget.material != (Object) null))
        return;
      this.widget.color = this.materialColor;
    }
  }

  [HideInInspector]
  public Shader MaterialShader
  {
    get
    {
      return this.materialShader;
    }
    set
    {
      this.materialShader = value;
      if (!((Object) this.widget != (Object) null) || !((Object) this.widget.material != (Object) null))
        return;
      this.widget.material.shader = this.materialShader;
    }
  }

  [HideInInspector]
  public int TextureSizeX
  {
    get
    {
      return this.textureSizeX;
    }
    set
    {
      this.Clear();
      this.textureSizeX = value;
      this.Setup();
    }
  }

  [HideInInspector]
  public int TextureSizeY
  {
    get
    {
      return this.textureSizeY;
    }
    set
    {
      this.Clear();
      this.textureSizeY = value;
      this.Setup();
    }
  }

  [HideInInspector]
  public float RenderDistance
  {
    get
    {
      return this.renderDistance;
    }
    set
    {
      this.renderDistance = value;
      this.HandleCameraCalculations();
    }
  }

  [HideInInspector]
  public float FieldOfView
  {
    get
    {
      return this.fieldOfView;
    }
    set
    {
      this.fieldOfView = value;
      this.HandleCameraCalculations();
    }
  }

  [HideInInspector]
  public LayerMask WorkLayer
  {
    get
    {
      return this.workLayer;
    }
    set
    {
      this.workLayer = value;
    }
  }

  public void Start()
  {
    this.initialized = true;
    if (!((Object) this.TargetObject != (Object) null))
      return;
    if ((Object) this.materialShader == (Object) null)
      this.materialShader = Shader.Find("Unlit/Transparent Colored");
    this.Clear();
    this.Setup();
  }

  public void OnDestroy()
  {
    this.Clear();
    this.initialized = false;
  }

  public void Update()
  {
    if (!((Object) this.cloneObject != (Object) null) || !((Object) this.TargetObject != (Object) null))
      return;
    this.cloneObject.transform.rotation = this.TargetObject.transform.rotation;
  }

  protected void Clear()
  {
    if (!this.initialized)
      return;
    if ((Object) this.widget != (Object) null && (Object) this.widget.material != (Object) null)
      Object.Destroy((Object) this.widget.material);
    if ((Object) this.widget != (Object) null)
      Object.Destroy((Object) this.widget.gameObject);
    if ((Object) this.renderTexture != (Object) null)
      Object.Destroy((Object) this.renderTexture);
    if ((Object) this.camObject != (Object) null)
      Object.Destroy((Object) this.camObject);
    if (!((Object) this.cloneObject != (Object) null))
      return;
    Object.Destroy((Object) this.cloneObject);
  }

  protected void Setup()
  {
    if (!this.initialized || (int) this.workLayer == -1 || !((Object) this.TargetObject != (Object) null))
      return;
    this.cloneObject = new GameObject("RenderArea Object");
    this.cloneObject.layer = (int) this.workLayer;
    this.HandleObjectClone();
    this.camObject = new GameObject("RenderArea Camera");
    this.camObject.layer = (int) this.workLayer;
    this.cloneObject.transform.parent = this.TargetObject.transform.parent;
    this.camObject.transform.parent = this.TargetObject.transform.parent;
    this.HandleWidgetCreation();
    this.HandleTextureCreation(this.TextureSizeX, this.TextureSizeY);
    this.widget.MakePixelPerfect();
    this.HandleCameraCalculations();
    this.camObject.transform.LookAt(this.cloneObject.transform);
    this.camObject.GetComponent<Camera>().Render();
  }

  protected void HandleObjectClone()
  {
    this.cloneObject.AddComponent<MeshFilter>().mesh = this.TargetObject.GetComponent<MeshFilter>().mesh;
    this.cloneObject.AddComponent<MeshRenderer>();
    this.cloneObject.GetComponent<Renderer>().materials = this.targetObject.GetComponent<Renderer>().materials;
    this.cloneObject.transform.position = this.TargetObject.transform.position;
    this.cloneObject.transform.rotation = this.TargetObject.transform.rotation;
  }

  protected void HandleWidgetCreation()
  {
    this.widget = NGUITools.AddWidget<UITexture>(this.gameObject);
    this.widget.name = "UITexture";
  }

  protected void HandleTextureCreation(int sizeX, int sizeY)
  {
    this.renderTexture = new RenderTexture(sizeX, sizeY, 1);
    if ((Object) this.widget.material == (Object) null)
      this.widget.material = new Material(this.materialShader);
    this.widget.material.name = "Render Texture";
    this.widget.material.mainTexture = (Texture) this.renderTexture;
    this.widget.color = this.MaterialColor;
  }

  protected void HandleCameraCalculations()
  {
    if (!this.initialized || !((Object) this.cloneObject != (Object) null))
      return;
    Camera camera = this.camObject.GetComponent<Camera>();
    if ((Object) camera == (Object) null)
    {
      camera = this.camObject.AddComponent<Camera>();
      camera.cullingMask = 1 << this.cloneObject.layer;
      camera.clearFlags = CameraClearFlags.Color;
      camera.targetTexture = this.renderTexture;
    }
    Vector3 position = this.cloneObject.transform.position;
    position.z -= this.RenderDistance;
    this.camObject.transform.position = position;
    camera.fieldOfView = this.FieldOfView;
    camera.nearClipPlane = 0.1f;
    camera.farClipPlane = this.RenderDistance * 2f;
  }
}
