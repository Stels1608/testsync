﻿// Decompiled with JetBrains decompiler
// Type: ISpaceEntity
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public interface ISpaceEntity
{
  Vector3 Position { get; set; }

  TargetType TargetType { get; }

  bool IsInMapRange { get; }

  string ObjectName { get; }

  string Name { get; }

  bool IsMe { get; }

  bool IsTargetable { get; }

  bool IsPlayer { get; }

  bool IsHostileCongener { get; }

  Faction Faction { get; }

  Relation PlayerRelation { get; }

  FactionGroup FactionGroup { get; }

  bool Dead { get; }

  UnityEngine.Sprite Icon3dMap { get; }

  UnityEngine.Sprite IconBrackets { get; }

  Transform Root { get; }

  SpaceEntityType SpaceEntityType { get; }

  Bounds MeshBoundsRaw { get; }

  bool ShowIndicatorWhenInRange { get; }

  bool ForceShowOnMap { get; }

  bool IsFriendlyHub { get; }

  SpaceObject.MarkObjectType MarkerType { get; }

  float GetDistance();
}
