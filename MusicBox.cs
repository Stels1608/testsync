﻿// Decompiled with JetBrains decompiler
// Type: MusicBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using Unity5AssetHandling;
using UnityEngine;

public class MusicBox : MonoBehaviour
{
  public float DesignVolume = 1f;
  private float muteVolume = 1f;
  public float TrackFadeLength = 20f;
  public float MoodFadeLength = 5f;
  private bool playing = true;
  private readonly Dictionary<string, AudioClip> clipCache = new Dictionary<string, AudioClip>();
  public float UserVolume;
  public MusicBoxRecord[] CalmRecords;
  public MusicBoxRecord[] TensionRecords;
  public MusicBoxRecord[] BattleRecords;
  public int[] CalmRecordsOrder;
  public int[] TensionRecordsOrder;
  public int[] BattleRecordsOrder;
  public float SilenceDuration;
  private float fadeLength;
  private MusicBoxMood mood;
  private static uint calmIndex;
  private static uint tensionIndex;
  private static uint battleIndex;
  private float loopStartTime;
  private float loopDuration;
  private AudioSource mainAudio;
  private AudioSource extraAudio;
  private AudioSource audio1;
  private AudioSource audio2;
  private bool silence;
  private MusicBoxRecord currentRecord;
  private AssetRequest request;
  private int muteTweenId;

  public MusicBoxMood Mood
  {
    get
    {
      return this.mood;
    }
    set
    {
      if (this.mood == value)
        return;
      this.mood = value;
      if (this.mood == MusicBoxMood.Calm)
      {
        this.silence = false;
        this.loopStartTime = float.NegativeInfinity;
        this.loopDuration = 0.0f;
      }
      else
      {
        this.fadeLength = this.MoodFadeLength;
        this.Reset();
      }
    }
  }

  private float Volume()
  {
    return this.UserVolume * this.DesignVolume * this.muteVolume;
  }

  private uint ValidCalmIndex()
  {
    return MusicBox.calmIndex % (uint) this.CalmRecordsOrder.Length;
  }

  private uint ValidTensionIndex()
  {
    return MusicBox.tensionIndex % (uint) this.TensionRecordsOrder.Length;
  }

  private uint ValidBattleIndex()
  {
    return MusicBox.battleIndex % (uint) this.BattleRecordsOrder.Length;
  }

  private void Awake()
  {
    this.audio1 = this.gameObject.AddComponent<AudioSource>();
    this.audio2 = this.gameObject.AddComponent<AudioSource>();
    this.audio1.ignoreListenerVolume = true;
    this.audio1.loop = true;
    this.audio2.ignoreListenerVolume = true;
    this.audio2.loop = true;
    this.mainAudio = this.audio1;
    this.extraAudio = this.audio2;
    this.fadeLength = this.TrackFadeLength;
    this.Reset();
  }

  private void Start()
  {
    if (this.CalmRecords.Length > 0 && this.CalmRecordsOrder.Length > 0)
      this.GetClip(this.CalmRecords[this.CalmRecordsOrder[(IntPtr) this.ValidCalmIndex()]].ClipName);
    if (this.TensionRecords.Length > 0 && this.TensionRecordsOrder.Length > 0)
      this.GetClip(this.TensionRecords[this.TensionRecordsOrder[(IntPtr) this.ValidTensionIndex()]].ClipName);
    if (this.BattleRecords.Length <= 0 || this.BattleRecordsOrder.Length <= 0)
      return;
    this.GetClip(this.BattleRecords[this.BattleRecordsOrder[(IntPtr) this.ValidBattleIndex()]].ClipName);
  }

  private void Update()
  {
    float max = this.Volume();
    this.mainAudio.volume = Mathf.Clamp(this.mainAudio.volume + Time.deltaTime * max / this.fadeLength, 0.0f, max);
    this.extraAudio.volume = Mathf.Clamp(this.extraAudio.volume - Time.deltaTime * max / this.fadeLength, 0.0f, max);
    if ((double) this.extraAudio.volume == 0.0)
      this.extraAudio.Stop();
    if (!this.playing)
      return;
    if ((double) Time.time > (double) this.loopStartTime + (double) this.loopDuration)
    {
      this.loopStartTime = Time.time;
      if (this.silence)
      {
        this.silence = false;
        this.currentRecord = this.NextRecord();
        Log.Add("Current track: " + this.currentRecord.ClipName);
        this.ChangeClip(this.GetClip(this.currentRecord.ClipName));
        this.loopDuration = this.currentRecord.LoopDuration;
      }
      else
      {
        this.silence = true;
        this.fadeLength = this.TrackFadeLength;
        this.ChangeClip((AudioClip) null);
        this.loopDuration = this.SilenceDuration;
      }
    }
    else
    {
      if (this.silence || !((UnityEngine.Object) this.mainAudio.clip == (UnityEngine.Object) null) || (double) Time.time >= (double) this.loopStartTime + (double) this.loopDuration / 2.0)
        return;
      AudioClip clip = this.GetClip(this.currentRecord.ClipName);
      if (!((UnityEngine.Object) clip != (UnityEngine.Object) null))
        return;
      Log.Add("Afterset: " + this.currentRecord.ClipName);
      this.mainAudio.clip = clip;
      this.mainAudio.volume = 0.0f;
      this.mainAudio.Play();
    }
  }

  private AudioClip GetClip(string clipName)
  {
    clipName = clipName.ToLowerInvariant() + ".wav";
    if (this.clipCache.ContainsKey(clipName))
      return this.clipCache[clipName];
    if (this.request != null && this.request.AssetName == clipName)
    {
      if (this.request.IsDone)
        return this.CacheClip(clipName, (AudioClip) this.request.Asset);
      return (AudioClip) null;
    }
    this.request = AssetCatalogue.Instance.Request(clipName, false);
    if (!this.request.IsDone)
      return (AudioClip) null;
    AudioClip audioClip = (AudioClip) this.request.Asset;
    if ((UnityEngine.Object) audioClip == (UnityEngine.Object) null)
      Debug.LogError((object) "AudioClip claimed downloaded but is still NULL!!!");
    return this.CacheClip(clipName, audioClip);
  }

  private AudioClip CacheClip(string clipName, AudioClip audioClip)
  {
    this.clipCache.Add(clipName, audioClip);
    this.request = (AssetRequest) null;
    return audioClip;
  }

  private MusicBoxRecord NextRecord()
  {
    MusicBoxRecord musicBoxRecord = (MusicBoxRecord) null;
    string clipName = (string) null;
    switch (this.mood)
    {
      case MusicBoxMood.Calm:
        musicBoxRecord = this.CalmRecords[this.CalmRecordsOrder[(IntPtr) this.ValidCalmIndex()]];
        ++MusicBox.calmIndex;
        clipName = this.CalmRecords[this.CalmRecordsOrder[(IntPtr) this.ValidCalmIndex()]].ClipName;
        break;
      case MusicBoxMood.Tension:
        musicBoxRecord = this.TensionRecords[this.TensionRecordsOrder[(IntPtr) this.ValidTensionIndex()]];
        ++MusicBox.tensionIndex;
        clipName = this.TensionRecords[this.TensionRecordsOrder[(IntPtr) this.ValidTensionIndex()]].ClipName;
        break;
      case MusicBoxMood.Battle:
        musicBoxRecord = this.BattleRecords[this.BattleRecordsOrder[(IntPtr) this.ValidBattleIndex()]];
        ++MusicBox.battleIndex;
        clipName = this.BattleRecords[this.BattleRecordsOrder[(IntPtr) this.ValidBattleIndex()]].ClipName;
        break;
    }
    this.GetClip(clipName);
    return musicBoxRecord;
  }

  private void ChangeClip(AudioClip clip)
  {
    this.SwapAudios();
    this.mainAudio.clip = clip;
    this.mainAudio.volume = 0.0f;
    if (!((UnityEngine.Object) clip != (UnityEngine.Object) null))
      return;
    this.mainAudio.Play();
  }

  private void SwapAudios()
  {
    if ((UnityEngine.Object) this.mainAudio == (UnityEngine.Object) this.audio1)
    {
      this.mainAudio = this.audio2;
      this.extraAudio = this.audio1;
    }
    else
    {
      this.mainAudio = this.audio1;
      this.extraAudio = this.audio2;
    }
  }

  private void Reset()
  {
    this.silence = true;
    this.loopStartTime = float.NegativeInfinity;
    this.loopDuration = 0.0f;
  }

  public void Play()
  {
    this.playing = true;
    this.fadeLength = this.TrackFadeLength;
    this.Reset();
  }

  public void Stop()
  {
    this.playing = false;
    this.fadeLength = this.MoodFadeLength;
    this.ChangeClip((AudioClip) null);
  }

  public void Mute(bool mute)
  {
    float to = !mute ? 1f : 0.0f;
    LeanTween.cancel(this.gameObject, this.muteTweenId);
    this.muteTweenId = LeanTween.value(this.gameObject, (System.Action<float>) (f => this.muteVolume = f), this.muteVolume, to, 2f).uniqueId;
  }
}
