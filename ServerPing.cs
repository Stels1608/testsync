﻿// Decompiled with JetBrains decompiler
// Type: ServerPing
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

internal class ServerPing
{
  private Socket socket;
  private float timeBegin;

  private float currentTime
  {
    get
    {
      return (float) (DateTime.Now.TimeOfDay.TotalMilliseconds / 1000.0);
    }
  }

  public void Ping(LoginScreen.Server server)
  {
    this.Work(server);
  }

  private void Disconnect()
  {
    if (this.socket == null)
      return;
    this.socket.Close();
    this.socket = (Socket) null;
  }

  private void Work(LoginScreen.Server server)
  {
    string hostNameOrAddress = server.ip;
    string s = server.port;
    float currentTime = this.currentTime;
    IPAddress address = IPAddress.None;
    if (hostNameOrAddress != null && 0 < hostNameOrAddress.ToString().Length)
    {
      IPAddress[] hostAddresses = Dns.GetHostAddresses(hostNameOrAddress);
      if (hostAddresses != null && 0 < hostAddresses.Length)
        address = Dns.GetHostAddresses(hostNameOrAddress)[0];
    }
    this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    this.socket.NoDelay = true;
    IAsyncResult asyncResult = this.socket.BeginConnect((EndPoint) new IPEndPoint(address, int.Parse(s)), (AsyncCallback) (ar => this.socket.EndConnect(ar)), (object) null);
    while (!asyncResult.IsCompleted)
    {
      if ((double) currentTime + 5.0 < (double) this.currentTime)
      {
        server.ping = -1;
        return;
      }
      Thread.Sleep(0);
    }
    if (!this.socket.Connected)
    {
      server.ping = -1;
    }
    else
    {
      while (this.socket.Available < 5)
      {
        if ((double) currentTime + 5.0 < (double) this.currentTime)
        {
          server.ping = -1;
          return;
        }
        Thread.Sleep(0);
      }
      byte[] buffer1 = new byte[5];
      if (this.socket.Receive(buffer1, 0, buffer1.Length, SocketFlags.None) != buffer1.Length)
      {
        server.ping = -1;
      }
      else
      {
        this.timeBegin = this.currentTime;
        byte[] numArray = new byte[5];
        numArray[1] = (byte) 3;
        numArray[3] = (byte) 5;
        byte[] buffer2 = numArray;
        this.socket.Send(buffer2, 0, buffer2.Length, SocketFlags.None);
        while (this.socket.Available < 5)
        {
          if ((double) currentTime + 5.0 < (double) this.currentTime)
          {
            server.ping = -1;
            return;
          }
          Thread.Sleep(0);
        }
        byte[] buffer3 = new byte[5];
        if (this.socket.Receive(buffer3, 0, buffer3.Length, SocketFlags.None) != buffer3.Length)
        {
          server.ping = -1;
        }
        else
        {
          server.ping = (int) (((double) this.currentTime - (double) this.timeBegin) * 1000.0);
          this.Disconnect();
        }
      }
    }
  }
}
