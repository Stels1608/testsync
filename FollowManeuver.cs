﻿// Decompiled with JetBrains decompiler
// Type: FollowManeuver
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class FollowManeuver : Maneuver
{
  protected uint targetID;

  public override string ToString()
  {
    return string.Format("FollowManeuver: target={0}", (object) this.targetID);
  }

  public override MovementFrame NextFrame(Tick tick, MovementFrame prevFrame)
  {
    SpaceObject spaceObject = SpaceLevel.GetLevel().GetObjectRegistry().Get(this.targetID);
    if (spaceObject == null)
      return this.Drift(prevFrame);
    Euler3 direction = Euler3.Direction(spaceObject.MovementController.GetTickFrame(tick - 1).position - prevFrame.position);
    return this.MoveToDirection(prevFrame, direction);
  }

  public override void Read(BgoProtocolReader pr)
  {
    base.Read(pr);
    this.startTick = pr.ReadTick();
    this.targetID = pr.ReadUInt32();
    this.options.Read(pr);
  }
}
