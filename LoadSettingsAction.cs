﻿// Decompiled with JetBrains decompiler
// Type: LoadSettingsAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System;
using UnityEngine;

public class LoadSettingsAction : Bigpoint.Core.Mvc.Controller.Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    BgoProtocolReader protocolReader = (BgoProtocolReader) message.Data;
    SettingsDataProvider settingsDataProvider = (SettingsDataProvider) FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider");
    try
    {
      settingsDataProvider.Load(protocolReader);
    }
    catch (Exception ex)
    {
      Debug.LogError((object) "Error while reading server settings; reverting to default settings");
      settingsDataProvider.CurrentSettings = UserSettings.DefaultSettings;
    }
    this.OwnerFacade.SendMessage(Message.ApplySettings);
  }
}
