﻿// Decompiled with JetBrains decompiler
// Type: DebugSceneStory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugSceneStory : DebugBehaviour<DebugSceneStory>
{
  private Vector2 scroll = new Vector2();
  private string story = "story_uknown";
  private string[] cylon_story = new string[18]{ "story_cylon_tutorial_1", "story_cylon_200", "story_cylon_100", "story_cylon_101", "story_cylon_102", "story_cylon_103", "story_cylon_104", "story_cylon_105", "story_cylon_5", "story_cylon_6", "story_cylon_7", "story_cylon_8", "story_cylon_9", "story_cylon_10", "story_wave_alpha_cylon", "story_wave_beta_cylon", "story_wave_gamma_cylon", "story_wave_delta_cylon" };
  private string[] human_story = new string[18]{ "story_human_tutorial_1", "story_human_200", "story_human_100", "story_human_101", "story_human_102", "story_human_103", "story_human_104", "story_human_105", "story_human_5", "story_human_6", "story_human_7", "story_human_8", "story_human_9", "story_human_10", "story_wave_alpha_colonial", "story_wave_beta_colonial", "story_wave_gamma_colonial", "story_wave_delta_colonial" };

  private void Start()
  {
    this.windowID = 12;
    this.SetSize(200f, 230f);
  }

  protected override void WindowFunc()
  {
    GameLevel instance = GameLevel.Instance;
    if (instance is LoginLevel || (Object) instance == (Object) null)
      GUILayout.Box("No commands \nfor this level.");
    else if (instance is SpaceLevel && (instance as SpaceLevel).IsStory)
    {
      if (!(instance as SpaceLevel).IsStory || !GUILayout.Button("Complete story"))
        return;
      DebugProtocol.GetProtocol().Command("complete_story");
      DebugBehaviour<DebugSceneStory>.Close();
    }
    else
    {
      this.scroll = GUILayout.BeginScrollView(this.scroll);
      switch (Game.Me.Faction)
      {
        case Faction.Colonial:
          foreach (string str in this.human_story)
          {
            if (GUILayout.Button(str))
            {
              this.StartStory(str);
              DebugBehaviour<DebugSceneStory>.Close();
            }
          }
          break;
        case Faction.Cylon:
          foreach (string str in this.cylon_story)
          {
            if (GUILayout.Button(str))
            {
              this.StartStory(str);
              DebugBehaviour<DebugSceneStory>.Close();
            }
          }
          break;
      }
      GUILayout.EndScrollView();
      this.story = GUILayout.TextArea(this.story);
      if (!GUILayout.Button(this.story))
        return;
      this.StartStory(this.story);
      DebugBehaviour<DebugSceneStory>.Close();
    }
  }

  private void StartStory(string name)
  {
    DebugProtocol.GetProtocol().Command("start_story", name);
  }
}
