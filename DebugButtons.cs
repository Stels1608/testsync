﻿// Decompiled with JetBrains decompiler
// Type: DebugButtons
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugButtons : DebugBehaviour<DebugButtons>
{
  private void Start()
  {
    this.windowID = 10;
    this.SetSize(400f, 210f);
    this.resizable = false;
  }

  protected override void WindowFunc()
  {
    GUILayout.Label("Traveling");
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Story"))
    {
      DebugBehaviour<DebugSceneStory>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Sector"))
    {
      DebugBehaviour<DebugSceneSector>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Room"))
    {
      DebugBehaviour<DebugSceneRoom>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    GUILayout.EndHorizontal();
    GUILayout.Label("Management");
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Slots"))
    {
      DebugBehaviour<DebugSlots>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Items"))
    {
      DebugBehaviour<DebugItems>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Counters"))
    {
      DebugBehaviour<DebugCounters>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Skills"))
    {
      DebugBehaviour<DebugSkills>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Ability"))
    {
      DebugBehaviour<DebugAbility>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Server"))
    {
      DebugBehaviour<DebugServer>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Sector Events"))
    {
      DebugBehaviour<DebugSectorEvents>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Console"))
    {
      DebugBehaviour<DebugConsole>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Toggle Happy Hour"))
    {
      DebugHappyHour.ToggleHappyHour();
      DebugBehaviour<DebugButtons>.Close();
    }
    GUILayout.EndHorizontal();
    GUILayout.Label("View");
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Traffic"))
    {
      DebugBehaviour<DebugTraffic>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Protocol"))
    {
      DebugBehaviour<DebugProtocolView>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Stats"))
    {
      DebugBehaviour<DebugStatsView>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Sector"))
    {
      DebugBehaviour<DebugSectorInfo>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Cards"))
    {
      DebugBehaviour<DebugCardInfo>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Player"))
    {
      DebugBehaviour<DebugPlayerInfo>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Process state"))
    {
      DebugBehaviour<DebugProcessStateView>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Positions"))
    {
      DebugBehaviour<DebugPositions>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    if (GUILayout.Button("Models"))
    {
      DebugBehaviour<DebugModels>.Toggle();
      DebugBehaviour<DebugButtons>.Close();
    }
    GUILayout.EndHorizontal();
    GUILayout.Space(20f);
    if (!GUILayout.Button("Substance"))
      return;
    DebugBehaviour<DebugSubstances>.Toggle();
    DebugBehaviour<DebugButtons>.Close();
  }
}
