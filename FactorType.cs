﻿// Decompiled with JetBrains decompiler
// Type: FactorType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum FactorType
{
  Experience = 1,
  SkillLearning = 2,
  Loot = 3,
  AsteroidYield = 4,
  MeritIncome = 5,
  MeritCapacity = 6,
  PVP = 7,
  PVE = 8,
  Reward = 9,
  Duty = 10,
  MissionReward = 11,
  WaterCapacity = 12,
}
