﻿// Decompiled with JetBrains decompiler
// Type: FtlModelWarpEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class FtlModelWarpEffect : MonoBehaviour
{
  public float StartDelay = 0.5f;
  public float WarpShifting = 1200f;
  public Transform _modelToWarp;
  public FtlModelWarpEffect.FtlModelWarpEffectType EffectType;
  private Vector3 svLocalPos;

  public bool DebugPlayOnEnable { get; set; }

  private Transform ModelToWarp
  {
    get
    {
      return this._modelToWarp;
    }
  }

  private void OnEnable()
  {
    if (!this.DebugPlayOnEnable)
      return;
    UnityEngine.Debug.LogWarning((object) ("FtlModelWarpEffect: DebugPlay + " + (object) this.gameObject));
    if (!((Object) this._modelToWarp != (Object) null))
      return;
    this.Play(this._modelToWarp);
  }

  public void SetModelToWarp(Transform model)
  {
    this._modelToWarp = model;
    this.svLocalPos = model.localPosition;
  }

  public void Play(Transform modelToWarp)
  {
    this.SetModelToWarp(modelToWarp);
    switch (this.EffectType)
    {
      case FtlModelWarpEffect.FtlModelWarpEffectType.WarpIn:
        this._modelToWarp.localScale = Vector3.zero;
        this.StartCoroutine(this.GrowAndMove(0.15f));
        break;
      case FtlModelWarpEffect.FtlModelWarpEffectType.WarpOut:
        this._modelToWarp.localScale = Vector3.one;
        this.StartCoroutine(this.ShrinkAndMove(0.15f));
        break;
    }
  }

  [DebuggerHidden]
  private IEnumerator ShrinkAndMove(float shrinkTime = 0.15f)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new FtlModelWarpEffect.\u003CShrinkAndMove\u003Ec__Iterator37() { shrinkTime = shrinkTime, \u003C\u0024\u003EshrinkTime = shrinkTime, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator GrowAndMove(float growTime = 0.15f)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new FtlModelWarpEffect.\u003CGrowAndMove\u003Ec__Iterator38() { growTime = growTime, \u003C\u0024\u003EgrowTime = growTime, \u003C\u003Ef__this = this };
  }

  public enum FtlModelWarpEffectType
  {
    WarpIn,
    WarpOut,
  }
}
