﻿// Decompiled with JetBrains decompiler
// Type: StoryLookAtChecker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StoryLookAtChecker
{
  private const float LOOK_AT_ANGLE = 11f;
  private const float LOOK_AT_TIME = 1f;
  private const float POLLING_INTERVAL = 0.1f;
  private float aggregatedLookatTime;
  private Timer pollingTimer;

  private StoryLookAtChecker()
  {
  }

  public StoryLookAtChecker(SpaceObject spaceObject)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    StoryLookAtChecker.\u003CStoryLookAtChecker\u003Ec__AnonStorey10C checkerCAnonStorey10C = new StoryLookAtChecker.\u003CStoryLookAtChecker\u003Ec__AnonStorey10C();
    // ISSUE: reference to a compiler-generated field
    checkerCAnonStorey10C.spaceObject = spaceObject;
    // ISSUE: explicit constructor call
    base.\u002Ector();
    // ISSUE: reference to a compiler-generated field
    checkerCAnonStorey10C.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    if (checkerCAnonStorey10C.spaceObject == null)
    {
      Debug.LogWarning((object) "StoryLookAtChecker.NotifyServerWhenLookingAtSpaceObject(): spaceObject null. Will notifying server anyway to prevent stalling.");
      this.NotifyServer();
    }
    else
    {
      // ISSUE: reference to a compiler-generated method
      this.pollingTimer = Timer.CreateTimer("lookAtChecker", 0.1f, 0.1f, new Timer.TickHandler(checkerCAnonStorey10C.\u003C\u003Em__27B));
    }
  }

  private void PollTarget(SpaceObject so)
  {
    if (this.PlayerIsLookingAtSpaceObject(so))
      this.aggregatedLookatTime += 0.1f;
    else
      this.aggregatedLookatTime = 0.0f;
    if ((double) this.aggregatedLookatTime <= 1.0)
      return;
    this.NotifyServer();
  }

  private void NotifyServer()
  {
    if ((Object) this.pollingTimer != (Object) null)
      Object.Destroy((Object) this.pollingTimer);
    StoryProtocol.GetProtocol().PlayerIsLookingAtTrigger();
  }

  private bool PlayerIsLookingAtSpaceObject(SpaceObject spaceObject)
  {
    if (spaceObject == null)
      return true;
    return (double) Vector3.Angle(Game.Me.ActualShip.Rotation * Vector3.forward, spaceObject.Position - Game.Me.ActualShip.Position) < 11.0;
  }
}
