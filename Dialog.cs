﻿// Decompiled with JetBrains decompiler
// Type: Dialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class Dialog : IDialog
{
  public HandleDialogAction actionHandler;
  public string npc;

  public Dialog(string NPC)
  {
    this.npc = NPC;
    FacadeFactory.GetInstance().SendMessage(Message.DialogRegister, (object) this);
  }

  public void Say(int index)
  {
    FacadeFactory.GetInstance().SendMessage(Message.DialogClickedIndex, (object) index);
  }

  public void Advance()
  {
    FacadeFactory.GetInstance().SendMessage(Message.DialogAdvance);
  }

  public void NpcRemark(Remark npcRemark)
  {
  }

  public void PcRemarks(List<Remark> answers)
  {
  }

  public void ReceiveStopped()
  {
  }

  public void ReceiveAction(DialogAction action)
  {
    if (this.actionHandler != null)
      this.actionHandler(action);
    else
      Debug.LogError((object) "NO ACTION HANDLER AVAIBLE");
  }

  public string NpcName()
  {
    this.npc = this.npc.ToLower();
    if (this.npc == "leoben")
      this.npc = "no2";
    else if (this.npc == "sharon")
      this.npc = "no8";
    else if (this.npc == "officer")
      this.npc = "humanoutpost";
    return Tools.ParseMessage("%$bgo.npc_" + this.npc + ".Name%") + " " + Tools.ParseMessage("%$bgo.npc_" + this.npc + ".Description%");
  }
}
