﻿// Decompiled with JetBrains decompiler
// Type: GroupBuff
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class GroupBuff : ClientBuffIcon
{
  private static ShipBuff groupBuff;

  private static ShipBuff GroupShipBuff
  {
    get
    {
      if (GroupBuff.groupBuff == null)
      {
        GroupBuff.groupBuff = new ShipBuff();
        GroupBuff.groupBuff.ServerID = GroupBuff.groupBuff.AbilityGuid = 12924519U;
        GroupBuff.groupBuff.GuiCard = (GUICard) Game.Catalogue.FetchCard(12924519U, CardView.GUI);
        GroupBuff.groupBuff.MaxTime = float.MaxValue;
        GroupBuff.groupBuff.EndTime = float.MaxValue;
        GroupBuff.groupBuff.IsLoaded.Depend(new ILoadable[1]
        {
          (ILoadable) GroupBuff.groupBuff.GuiCard
        });
        GroupBuff.groupBuff.IsLoaded.Set();
      }
      return GroupBuff.groupBuff;
    }
  }

  public override ShipBuff ClientBuff
  {
    get
    {
      return GroupBuff.GroupShipBuff;
    }
  }

  public override bool Applies(SpaceObject spaceObject)
  {
    PlayerShip playerShip = spaceObject as PlayerShip;
    if (playerShip == null || !Game.Me.Party.HasParty)
      return false;
    return Game.Me.Party.IsIdMemberOrMe(playerShip.Player.ServerID);
  }
}
