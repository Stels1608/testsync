﻿// Decompiled with JetBrains decompiler
// Type: AbilityActionType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum AbilityActionType
{
  None,
  FireMissle,
  FireCannon,
  DropFlare,
  Buff,
  RestoreBuff,
  ResourceScan,
  Debuff,
  FireMining,
  Flak,
  PointDefence,
  DispellVirus,
  Follow,
  ManeuverFlip,
  Slide,
  ActivatePaintTheTarget,
  FollowFriend,
  ActivateJumpTargetTransponder,
  ToggleStealth,
  FireTorpedo,
  ToggleSystem,
  FireLightMissile,
  FireHeavyMissile,
  FireShotgun,
  FireKillCannon,
  FireMachineGun,
  Fortify,
  DevBuff,
  ShortCircuit,
  DropAntiStealthMine,
  DeflectMissile,
}
