﻿// Decompiled with JetBrains decompiler
// Type: EnumHelper
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.ComponentModel;
using System.Reflection;

public static class EnumHelper
{
  public static string GetDescription(Enum en)
  {
    MemberInfo[] member = en.GetType().GetMember(en.ToString());
    if (member != null && member.Length > 0)
    {
      object[] customAttributes = member[0].GetCustomAttributes(typeof (DescriptionAttribute), false);
      if (customAttributes != null && customAttributes.Length > 0)
        return ((DescriptionAttribute) customAttributes[0]).Description;
    }
    return en.ToString();
  }
}
