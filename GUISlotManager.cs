﻿// Decompiled with JetBrains decompiler
// Type: GUISlotManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GUISlotManager : GUIPanel
{
  public GUISlotPopup Popup = new GUISlotPopup();
  public GUISlotPlayer player;
  public GUISlotSpaceObject target;
  private List<GUISlotPartyMember> party;
  private List<float2> membersPosition;
  private GUIPanel partyPanel;

  public static bool ShowStats { get; set; }

  private bool RenderStats
  {
    set
    {
      this.player.RenderHealthLevel = value;
      this.player.RenderEnergyLevel = value;
      this.target.RenderHealthLevel = value;
      this.target.RenderEnergyLevel = value;
      using (List<GUISlotPartyMember>.Enumerator enumerator = this.party.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISlotPartyMember current = enumerator.Current;
          current.RenderHealthLevel = value;
          current.RenderEnergyLevel = value;
        }
      }
    }
  }

  public GUISlotManager()
    : base((SmartRect) null)
  {
    this.player = new GUISlotPlayer(this.root);
    this.player.Position = new float2((float) ((double) this.player.Background.Rect.width / 2.0 + 20.0), this.player.Background.Rect.height + 20f);
    this.player.IsRendered = true;
    this.AddPanel((GUIPanel) this.player);
    this.party = new List<GUISlotPartyMember>();
    this.partyPanel = new GUIPanel();
    for (int index = 0; index < 10; ++index)
    {
      GUISlotPartyMember guiSlotPartyMember = new GUISlotPartyMember((SmartRect) null);
      this.party.Add(guiSlotPartyMember);
      this.partyPanel.AddPanel((GUIPanel) guiSlotPartyMember);
    }
    this.partyPanel.IsRendered = true;
    this.AddPanel(this.partyPanel);
    this.membersPosition = new List<float2>();
    float2 position1 = this.player.Position;
    this.membersPosition.Add(position1);
    for (int index = 0; index < 4; ++index)
    {
      position1.y += 65f;
      this.membersPosition.Add(position1);
    }
    float2 position2 = this.player.Position;
    position2.x += 265f;
    for (int index = 0; index < 5; ++index)
    {
      this.membersPosition.Add(position2);
      position2.y += 65f;
    }
    for (int index = 1; index < this.membersPosition.Count; ++index)
      this.party[index - 1].Position = this.membersPosition[index];
    this.target = new GUISlotSpaceObject((SmartRect) null);
    this.target.IsRendered = false;
    this.target.Position = new float2(this.membersPosition[6].x + 45f + this.target.SmartRect.Width, this.player.Position.y);
    this.AddPanel((GUIPanel) this.target);
    this.AddPanel((GUIPanel) this.Popup);
    this.handlers.Add(Action.ToggleSquadWindow, (AnonymousDelegate) (() => this.partyPanel.IsRendered = !this.partyPanel.IsRendered));
  }

  public override void Update()
  {
    SpaceObject playerTarget = SpaceLevel.GetLevel().GetPlayerTarget();
    if (playerTarget != null && !playerTarget.Dead && playerTarget != this.target.SpaceObject)
      this.target.SpaceObject = playerTarget;
    else if (playerTarget == null)
    {
      if (this.Popup.IsRendered)
      {
        if (Game.Me.ActiveShip.IsCapitalShip)
          this.Popup.duel.IsRendered = false;
        if (this.Popup.Parent == this.target.SmartRect)
        {
          this.target.RemovePanel((GUIPanel) this.Popup);
          this.Popup.IsRendered = false;
        }
      }
      this.target.SpaceObject = (SpaceObject) null;
    }
    this.target.IsRendered = this.target.SpaceObject != null && !this.target.SpaceObject.Dead;
    this.target.IsUpdated = this.target.IsRendered;
    if (this.target.IsUpdated)
      this.target.Update();
    this.RenderStats = GUISlotManager.ShowStats;
    uint num = 0;
    int index1 = 0;
    int index2;
    if (Game.Me.Anchored)
    {
      PlayerShip playerShip = SpaceLevel.GetLevel().GetObjectRegistry().Get(Game.Me.AnchorTarget) as PlayerShip;
      if (playerShip == null)
        return;
      num = playerShip.Player.ServerID;
      this.PlaceSlot(num, index1, false);
      int index3 = index1 + 1;
      this.PlaceSlot(Game.Me.ServerID, index3, true);
      index2 = index3 + 1;
      List<uint> uintList = new List<uint>();
      if (Game.Me.Party.Anchored.TryGetValue(num, out uintList))
      {
        using (List<uint>.Enumerator enumerator = uintList.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            uint current = enumerator.Current;
            if ((int) Game.Me.ServerID != (int) current)
            {
              this.PlaceSlot(current, index2, true);
              ++index2;
            }
          }
        }
      }
    }
    else
    {
      this.PlaceSlot(Game.Me.ServerID, index1, false);
      index2 = index1 + 1;
      List<uint> uintList;
      if (Game.Me.Ship != null && Game.Me.Ship.ShipCardLight != null && (Game.Me.Ship.ShipCardLight.ShipRoleDeprecated == ShipRoleDeprecated.Carrier && Game.Me.Party.Anchored.TryGetValue(Game.Me.ServerID, out uintList)))
      {
        using (List<uint>.Enumerator enumerator = uintList.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            this.PlaceSlot(enumerator.Current, index2, true);
            ++index2;
          }
        }
      }
    }
    using (Dictionary<uint, List<uint>>.Enumerator enumerator1 = Game.Me.Party.Anchored.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<uint, List<uint>> current = enumerator1.Current;
        if ((!Game.Me.Anchored || (int) num != (int) current.Key) && (int) Game.Me.ServerID != (int) current.Key)
        {
          this.PlaceSlot(current.Key, index2, false);
          ++index2;
          using (List<uint>.Enumerator enumerator2 = current.Value.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              this.PlaceSlot(enumerator2.Current, index2, true);
              ++index2;
            }
          }
        }
      }
    }
    using (List<uint>.Enumerator enumerator = Game.Me.Party.NonAnchored.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        this.PlaceSlot(enumerator.Current, index2, false);
        ++index2;
      }
    }
    for (int index3 = index2 - 1; index3 >= 0 && index3 < this.party.Count; ++index3)
    {
      this.party[index3].IsRendered = false;
      this.party[index3].Player = (Player) null;
    }
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    bool isRendered = this.Popup.IsRendered;
    SmartRect parent = this.Popup.Parent;
    bool flag = base.OnMouseUp(mousePosition, mouseKey);
    if (this.Popup.Parent == parent && this.Popup.IsRendered && isRendered)
      this.Popup.IsRendered = false;
    return flag;
  }

  private void PlaceSlot(uint id, int index, bool indent)
  {
    if (index >= this.party.Count || index >= this.membersPosition.Count || index >= 10)
      Debug.LogError((object) ("ERROR! " + (object) index + " " + (object) this.party.Count + " " + (object) this.membersPosition.Count));
    else if ((int) Game.Me.ServerID == (int) id)
    {
      this.PlaceSlotHelper((GUISlotWithBuffs) this.player, id, index, indent);
    }
    else
    {
      GUISlotPartyMember guiSlotPartyMember = this.party[Math.Max(index - 1, 0)];
      guiSlotPartyMember.Player = Game.Players[id];
      this.PlaceSlotHelper((GUISlotWithBuffs) guiSlotPartyMember, id, index, indent);
    }
  }

  private void PlaceSlotHelper(GUISlotWithBuffs slot, uint id, int index, bool indent)
  {
    slot.Position = this.membersPosition[index] + (!indent ? new float2(0.0f, 0.0f) : new float2(10f, 0.0f));
    slot.RecalculateAbsCoords();
    slot.IsRendered = true;
    slot.Update();
  }
}
