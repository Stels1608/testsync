﻿// Decompiled with JetBrains decompiler
// Type: CutsceneSequenceGeneric
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CutsceneSequenceGeneric : CutsceneSequenceBase
{
  private Flag isDataSet = new Flag();
  private string animClipName;
  private string sceneSetupName;
  private bool fadeInOnStart;
  private CutsceneSkipMode skipMode;

  protected override string GetSceneSetupName
  {
    get
    {
      return this.sceneSetupName;
    }
  }

  protected override CutsceneSkipMode GetSkipMode
  {
    get
    {
      return this.skipMode;
    }
  }

  public new void Setup(CutscenePlayRequestData playRequestData)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.isDataSet.AddHandler(new SignalHandler(new CutsceneSequenceGeneric.\u003CSetup\u003Ec__AnonStorey61()
    {
      playRequestData = playRequestData,
      \u003C\u003Ef__this = this
    }.\u003C\u003Em__33));
  }

  protected override bool ExecuteCustomPreparations()
  {
    this.ShowRealPlayerShip(false);
    if ((Object) this.CutsceneSetup.AnimationChild == (Object) null)
    {
      Debug.LogError((object) "CutsceneSequenceGeneric: cutscene setup's animation child is null. Aborting...");
      return false;
    }
    if (!((Object) this.CutsceneSetup.AnimationChild.GetClip(this.GetAnimClipName()) == (Object) null))
      return true;
    Debug.LogError((object) ("CutsceneSequenceGeneric: Clip " + this.GetAnimClipName() + " doesn't exist in animation component of child \"" + this.CutsceneSetup.AnimationChild.name + "\". Aborting..."));
    return false;
  }

  protected override void StartScene()
  {
    this.CutsceneSetup.AnimationChild.Play(this.GetAnimClipName());
    if (!this.fadeInOnStart)
      return;
    CutsceneVideoFx.FadeIn(this.CutsceneSetup.GetComponentInChildren<VideoUiHandler>(), 2f, 0.0f);
  }

  protected override void Cleanup()
  {
    this.ShowRealPlayerShip(true);
  }

  protected string GetAnimClipName()
  {
    return this.animClipName;
  }

  public void SetSequenceData(string sceneSetupName, string clipName, bool fadeInOnStart, CutsceneSkipMode skipMode)
  {
    this.animClipName = clipName;
    this.sceneSetupName = sceneSetupName;
    this.fadeInOnStart = fadeInOnStart;
    this.skipMode = skipMode;
    this.isDataSet.Set();
  }
}
