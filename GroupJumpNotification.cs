﻿// Decompiled with JetBrains decompiler
// Type: GroupJumpNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class GroupJumpNotification : OnScreenNotification
{
  private readonly string message;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.GroupJump;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Neutral;
    }
  }

  public override string TextMessage
  {
    get
    {
      return this.message;
    }
  }

  public override bool Show
  {
    get
    {
      return true;
    }
  }

  public GroupJumpNotification(GroupJumpNotification.GroupJumpState jumpState, object messageParam = null)
  {
    switch (jumpState)
    {
      case GroupJumpNotification.GroupJumpState.NoMembers:
        this.message = BsgoLocalization.Get("%$bgo.group_jump.notify_no_members%");
        break;
      case GroupJumpNotification.GroupJumpState.NotifyIncluded:
        this.message = BsgoLocalization.Get("%$bgo.group_jump.notify_included%");
        break;
      case GroupJumpNotification.GroupJumpState.NotifyDestination:
        this.message = BsgoLocalization.Get("%$bgo.group_jump.notify_destination%", (object) ((SectorCard) messageParam).GUICard.Name);
        break;
      case GroupJumpNotification.GroupJumpState.NotifyMemberCancel:
        this.message = BsgoLocalization.Get("%$bgo.group_jump.notify_member_cancel%", (object) (string) messageParam);
        break;
      case GroupJumpNotification.GroupJumpState.NotifyLeaderCancel:
        this.message = BsgoLocalization.Get("%$bgo.group_jump.notify_leader_cancel%");
        break;
      case GroupJumpNotification.GroupJumpState.NotifyNeedTylium:
        this.message = BsgoLocalization.Get("%$bgo.group_jump.notify_need_tylium%", messageParam);
        break;
    }
  }

  public enum GroupJumpState
  {
    NoMembers,
    NotifyIncluded,
    NotifyDestination,
    NotifyMemberCancel,
    NotifyLeaderCancel,
    NotifyNeedTylium,
  }
}
