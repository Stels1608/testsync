﻿// Decompiled with JetBrains decompiler
// Type: LeaderboardTab
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LeaderboardTab : LeaderboardToggle
{
  private static UnityEngine.Sprite selectedGfx;
  private FactionColoredUguiWidget coloredWidget;

  public static void LoadIcons()
  {
    LeaderboardTab.selectedGfx = Resources.Load<UnityEngine.Sprite>("GUI/Leaderboard/tab_active_ver2");
  }

  protected override void OnAwake()
  {
    this.NotSelectedGfx = this.image.sprite;
    this.SelectedGfx = LeaderboardTab.selectedGfx;
    this.coloredWidget = this.gameObject.GetComponent<FactionColoredUguiWidget>();
  }

  protected override void OnSelectedChanged(bool newState)
  {
    base.OnSelectedChanged(newState);
    if ((Object) this.coloredWidget != (Object) null)
      this.coloredWidget.Colorize();
    this.label.color = !newState ? Color.white : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
  }
}
