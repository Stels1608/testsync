﻿// Decompiled with JetBrains decompiler
// Type: ChatTextWhisperedToYou
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

internal class ChatTextWhisperedToYou : ChatProtocolParser
{
  public string text;
  public string userName;

  public ChatTextWhisperedToYou()
  {
    this.type = ChatProtocolParserType.TextWhisperToYou;
  }

  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split(new char[3]{ '%', '@', '#' });
    if (strArray.Length != 4 || strArray[0] != "cv")
      return false;
    this.userName = strArray[1];
    this.text = strArray[2];
    return true;
  }
}
