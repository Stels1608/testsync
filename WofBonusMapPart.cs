﻿// Decompiled with JetBrains decompiler
// Type: WofBonusMapPart
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class WofBonusMapPart
{
  private bool jackpotItem;
  private int bonusMapId;
  private GUICard guicard;
  private List<int> bonusMapCardList;

  public int BonusMapId
  {
    get
    {
      return this.bonusMapId;
    }
  }

  public List<int> BonusMapCardList
  {
    get
    {
      return this.bonusMapCardList;
    }
  }

  public bool JackpotItem
  {
    get
    {
      return this.jackpotItem;
    }
  }

  public GUICard Guicard
  {
    get
    {
      return this.guicard;
    }
  }

  public WofBonusMapPart(bool jackp, int map, List<int> cards, GUICard card)
  {
    this.bonusMapCardList = cards;
    this.bonusMapId = map;
    this.jackpotItem = jackp;
    this.guicard = card;
  }

  public override string ToString()
  {
    string str = string.Empty;
    using (List<int>.Enumerator enumerator = this.bonusMapCardList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        str = str + (object) current + ", ";
      }
    }
    return string.Format("JackpotItem: {0}, BonusMapId: {1}, BonusMapCardList: {2}", (object) this.jackpotItem, (object) this.bonusMapId, (object) str);
  }
}
