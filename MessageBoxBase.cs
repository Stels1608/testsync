﻿// Decompiled with JetBrains decompiler
// Type: MessageBoxBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public abstract class MessageBoxBase : Window
{
  private OnMessageBoxClose onClosed = (OnMessageBoxClose) (param0 => {});

  public override bool IsBigWindow
  {
    get
    {
      return false;
    }
  }

  protected abstract void SetOkButtonText(string text);

  protected abstract void SetMainText(string mainText);

  protected virtual void OnTimeLeftUpdated(string mainText)
  {
  }

  [DebuggerHidden]
  private IEnumerator AutoCloser(uint timeout)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MessageBoxBase.\u003CAutoCloser\u003Ec__Iterator23() { timeout = timeout, \u003C\u0024\u003Etimeout = timeout, \u003C\u003Ef__this = this };
  }

  public void CloseWindow(MessageBoxActionType actionType)
  {
    if (this.onClosed != null)
      this.onClosed(actionType);
    this.DestroyWindow();
  }

  public void OnCloseWindow()
  {
    this.CloseWindow(MessageBoxActionType.Cancel);
  }

  public void OnOkPressed()
  {
    this.CloseWindow(MessageBoxActionType.Ok);
  }

  public void OnCollapsePressed()
  {
  }

  public void OnUncollapsePressed()
  {
  }

  protected void SetTimeToLive(uint timeToLive)
  {
    if (timeToLive <= 0U)
      return;
    this.StartCoroutine(this.AutoCloser(timeToLive));
  }

  protected void AddOnClosedAction(OnMessageBoxClose onPressed)
  {
    this.onClosed += onPressed;
  }

  protected virtual void DestroyWindow()
  {
    Object.Destroy((Object) this.gameObject);
  }
}
