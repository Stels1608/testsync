﻿// Decompiled with JetBrains decompiler
// Type: ShipConsumableCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class ShipConsumableCard : Card
{
  public ushort ConsumableType;
  public ObjectStats ItemBuffMultiply;
  public ObjectStats ItemBuffAdd;
  public byte Tier;
  public AugmentActionType Action;
  public GUICard GUICard;
  public bool IsAugment;
  public bool AutoConsume;
  public bool Trashable;
  public uint buyCount;
  public ConsumableAttribute[] sortingAttributes;
  public ConsumableEffectType effectType;

  public ShipConsumableCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.ConsumableType = r.ReadUInt16();
    this.Tier = r.ReadByte();
    this.ItemBuffMultiply = r.ReadDesc<ObjectStats>();
    this.ItemBuffAdd = r.ReadDesc<ObjectStats>();
    this.Action = (AugmentActionType) r.ReadByte();
    this.IsAugment = r.ReadBoolean();
    this.AutoConsume = r.ReadBoolean();
    this.Trashable = r.ReadBoolean();
    this.buyCount = (uint) r.ReadUInt16();
    string[] strArray = r.ReadStringArray();
    this.sortingAttributes = new ConsumableAttribute[strArray.Length];
    for (int index = 0; index < strArray.Length; ++index)
      this.sortingAttributes[index] = (ConsumableAttribute) strArray[index];
    this.effectType = (ConsumableEffectType) r.ReadByte();
    this.GUICard = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.GUICard
    });
    this.IsLoaded.Set();
  }

  public override string ToString()
  {
    return string.Format("ConsumableType: {0}, Tier: {1}, Action: {2}, IsAugment: {3}, AutoConsume: {4}, Trashable: {5}, BuyCount: {6}, SortingAttributes: {7}, EffectType: {8}", (object) this.ConsumableType, (object) this.Tier, (object) this.Action, (object) this.IsAugment, (object) this.AutoConsume, (object) this.Trashable, (object) this.buyCount, (object) this.sortingAttributes, (object) this.effectType);
  }

  public bool HasSortingAttribute(string sortingAttribute)
  {
    if (this.sortingAttributes == null)
      return false;
    foreach (ConsumableAttribute sortingAttribute1 in this.sortingAttributes)
    {
      if (sortingAttribute1.Attribute == sortingAttribute)
        return true;
    }
    return false;
  }
}
