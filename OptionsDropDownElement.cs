﻿// Decompiled with JetBrains decompiler
// Type: OptionsDropDownElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class OptionsDropDownElement : OptionsElement
{
  private List<string> textList = new List<string>();
  private List<object> valueList = new List<object>();
  private bool initialized;
  [SerializeField]
  private DropDownWidget dropDownWidget;
  private UserSetting setting;
  private OnSettingChanged onSettingChanged;

  public override OnSettingChanged OnSettingChanged
  {
    get
    {
      return this.onSettingChanged;
    }
    set
    {
      this.onSettingChanged = value;
    }
  }

  private void Changed()
  {
    if (!this.initialized)
      return;
    if (this.textList == null)
    {
      Debug.LogError((object) "VALUE WRAPPER IS NOT HERE");
    }
    else
    {
      int index = this.textList.IndexOf(this.dropDownWidget.dropDown.value);
      if (this.OnSettingChanged == null)
        return;
      this.OnSettingChanged(this.setting, this.valueList[index]);
    }
  }

  private void LateAdd()
  {
    EventDelegate.Add(this.dropDownWidget.dropDown.onChange, new EventDelegate.Callback(this.Changed));
  }

  public override void Init(UserSetting sett, object value)
  {
    this.dropDownWidget.dropDown.onChange.Clear();
    this.setting = sett;
    this.optionDescLabel.text = Tools.GetLocalized(this.setting);
    if (value is OptionsEnumWrapper)
    {
      object obj = ((OptionsEnumWrapper) value).actualValue;
      this.textList = new List<string>((IEnumerable<string>) ((OptionsEnumWrapper) value).valueText);
      this.valueList = new List<object>((IEnumerable<object>) ((OptionsEnumWrapper) value).valueList);
      this.dropDownWidget.Items = new List<string>((IEnumerable<string>) this.textList);
      int index = this.valueList.IndexOf(obj);
      if (this.textList.Count > index && index > -1)
        this.dropDownWidget.Selection = this.textList[index];
      else
        Debug.LogError((object) ("FoundIndex is wrong " + (object) index));
    }
    else
      Debug.LogError((object) "Given Value is NOT an OPTIONS ENUM WRAPPER");
    this.initialized = true;
    this.Invoke("LateAdd", 0.5f);
  }
}
