﻿// Decompiled with JetBrains decompiler
// Type: GUIShopUpgradeBarTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System.Collections.Generic;
using UnityEngine;

public class GUIShopUpgradeBarTooltip : GUIShopTooltipBox
{
  private GUILabelNew textLabel;
  private ShipSystem system;
  private int level;

  public GUIShopUpgradeBarTooltip(SmartRect parent)
    : base(parent)
  {
    JWindowDescription jwindowDescription = Loaders.LoadResource("GUI/EquipBuyPanel/DetailsWindow/gui_upgradebar_tooltip_layout");
    GUIImageNew guiImageNew = (jwindowDescription["background_image"] as JImage).CreateGUIImageNew(this.root);
    this.root.Width = guiImageNew.Width;
    this.root.Height = guiImageNew.Height;
    this.AddPanel((GUIPanel) guiImageNew);
    this.textLabel = (jwindowDescription["text_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.textLabel);
  }

  public void TryToShow(float2 absPos, ShipSystem system, int level)
  {
    this.system = system;
    this.level = level;
    this.Appear(absPos);
  }

  public override void Hide()
  {
    this.system = (ShipSystem) null;
    this.Disappear();
  }

  public override void Update()
  {
    if (this.system != null && (bool) this.system.IsLoaded)
    {
      int skillLevel = 0;
      List<PlayerSkill> needSkills = (List<PlayerSkill>) null;
      Color color1 = Color.white;
      if (!Game.Me.RequiresSkillsForSystem(this.system, this.level, out skillLevel, out needSkills) && needSkills.Count != 0)
      {
        this.textLabel.Text = "%$bgo.EquipBuyPanel.DetailsWindow.gui_upgradebar_tooltip_layout.text_label%%br%" + needSkills[0].Name + " - " + (object) skillLevel;
        color1 = new Color(0.96f, 0.13f, 0.13f);
      }
      else if (needSkills.Count != 0)
        this.textLabel.Text = "%$bgo.EquipBuyPanel.DetailsWindow.gui_upgradebar_tooltip_layout.text_label%%br%" + needSkills[0].Name + " - " + (object) skillLevel;
      else
        this.textLabel.Text = "%$bgo.shop.no_requires_skill%";
      GUILabelNew guiLabelNew = this.textLabel;
      Color color2 = color1;
      this.textLabel.OverColor = color2;
      Color color3 = color2;
      guiLabelNew.NormalColor = color3;
    }
    base.Update();
  }
}
