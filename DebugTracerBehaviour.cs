﻿// Decompiled with JetBrains decompiler
// Type: DebugTracerBehaviour
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugTracerBehaviour : MonoBehaviour
{
  public string path = "debugTracer.txt";
  private DebugTracer dt;
  private float time;
  private float maxtime;

  private void Start()
  {
    this.dt = new DebugTracer();
    this.dt.Load(this.path);
    this.maxtime = this.dt.GetMaxTime();
    Camera.main.transform.LookAt(new GameObject("123")
    {
      transform = {
        position = this.dt.GetStartPoint()
      }
    }.transform.position);
  }

  private void Update()
  {
    this.dt.Draw(this.dt.GetCreateTime(), this.time);
  }

  private void OnGUI()
  {
    GUILayout.BeginArea(new Rect(0.0f, 10f, (float) Screen.width, 50f));
    this.time = GUILayout.HorizontalSlider(this.time, this.dt.GetCreateTime(), this.maxtime);
    GUILayout.EndArea();
  }
}
