﻿// Decompiled with JetBrains decompiler
// Type: CamsHudMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Paths;
using UnityEngine;

public class CamsHudMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "GuiHudStrikesMediator";
  private SettingsDataProvider settingsDataProvider;
  private bool autoShipTurnIsActive;
  private bool manualShipTurnIsActive;
  private bool playerDead;
  private ISpaceCameraBehavior currentSpaceCameraBehavior;
  private HudStrikesTargettingCircleUi strikesTargettingCircle;
  private bool _uiDisabled;

  private bool UiDisabled
  {
    get
    {
      return this._uiDisabled;
    }
    set
    {
      this._uiDisabled = value;
      if (!this._uiDisabled)
        return;
      this.DeactivateAll();
    }
  }

  public CamsHudMediator()
    : base("GuiHudStrikesMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.Death);
    this.AddMessageInterest(Message.ShipInputMouseAutoTurnToggle);
    this.AddMessageInterest(Message.ShipInputMouseManualTurnToggle);
    this.AddMessageInterest(Message.CameraBehaviorChanged);
    this.AddMessageInterest(Message.MyShipCreated);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.DockAtNpcShipRequest);
    this.AddMessageInterest(Message.ToggleOldUi);
    this.AddMessageInterest(Message.ApplyCombatGui);
    this.AddMessageInterest(Message.SettingChangedCombatGui);
    this.settingsDataProvider = this.OwnerFacade.FetchDataProvider("SettingsDataProvider") as SettingsDataProvider;
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    if ((Object) SpaceLevel.GetLevel() == (Object) null)
      this.DestroyAll();
    else if (SpaceLevel.GetLevel().ShipRoleSpecificsFactory.DetermineFlightModel() != FlightMode.Cams)
    {
      this.DeactivateAll();
    }
    else
    {
      Message id = message.Id;
      switch (id)
      {
        case Message.Death:
          GuiHudStrikesMouseMovementIndicators.Instance.Deactivate();
          this.playerDead = true;
          break;
        case Message.MyShipCreated:
        case Message.DockAtNpcShipRequest:
          this.playerDead = false;
          this.autoShipTurnIsActive = this.manualShipTurnIsActive = false;
          break;
        case Message.CameraBehaviorChanged:
          this.currentSpaceCameraBehavior = ((SpaceCameraBehaviorChangeMessage) message).NewBehavior;
          break;
        case Message.ShipInputMouseAutoTurnToggle:
          this.autoShipTurnIsActive = (bool) message.Data;
          break;
        case Message.ShipInputMouseManualTurnToggle:
          this.manualShipTurnIsActive = (bool) message.Data;
          break;
        default:
          if (id != Message.LoadNewLevel)
          {
            if (id != Message.LevelLoaded)
            {
              if (id != Message.SettingChangedCombatGui)
              {
                if (id != Message.ApplyCombatGui)
                {
                  if (id == Message.ToggleOldUi && !(bool) message.Data)
                  {
                    this.autoShipTurnIsActive = this.manualShipTurnIsActive = false;
                    break;
                  }
                  break;
                }
                this.UiDisabled = !this.settingsDataProvider.CurrentSettings.CombatGui;
                break;
              }
              this.UiDisabled = !(bool) message.Data;
              break;
            }
            goto case Message.MyShipCreated;
          }
          else
          {
            this.DeactivateAll();
            return;
          }
      }
      if (this.UiDisabled)
        return;
      this.ReInit();
      this.HandleMovement();
      this.HandleCameraBehaviour();
      this.HandleScene();
      this.HandleDeath();
    }
  }

  private void ReInit()
  {
    if ((Object) this.strikesTargettingCircle == (Object) null)
      this.strikesTargettingCircle = NGUITools.AddChild(NGUIToolsExtension.GetUiRoot.gameObject, Resources.Load<GameObject>(Ui.Prefabs + "/TargettingCircleNgui")).GetComponent<HudStrikesTargettingCircleUi>();
    else
      this.strikesTargettingCircle.IsRendered = true;
  }

  private void HandleMovement()
  {
    if (this.autoShipTurnIsActive || this.manualShipTurnIsActive)
      GuiHudStrikesMouseMovementIndicators.Instance.Activate();
    else
      GuiHudStrikesMouseMovementIndicators.Instance.Deactivate();
  }

  private void HandleCameraBehaviour()
  {
    if (this.currentSpaceCameraBehavior == null || this.currentSpaceCameraBehavior.ShowStrikesHud)
      return;
    this.DeactivateAll();
  }

  private void HandleScene()
  {
    if ((Object) SpaceLevel.GetLevel() == (Object) null)
      ;
  }

  private void HandleDeath()
  {
    if (!this.playerDead)
      return;
    this.DeactivateAll();
  }

  private void DeactivateAll()
  {
    GuiHudStrikesMouseMovementIndicators.Instance.Deactivate();
    if (!((Object) this.strikesTargettingCircle != (Object) null))
      return;
    this.strikesTargettingCircle.IsRendered = false;
  }

  private void DestroyAll()
  {
    if (!((Object) this.strikesTargettingCircle != (Object) null))
      return;
    Object.Destroy((Object) this.strikesTargettingCircle.gameObject);
    this.strikesTargettingCircle = (HudStrikesTargettingCircleUi) null;
  }
}
