﻿// Decompiled with JetBrains decompiler
// Type: InputReceiverJoystick
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class InputReceiverJoystick
{
  private readonly Dictionary<JoystickButtonCode, InputReceiverJoystick.JoystickInputValues> joystickInputValues;
  private readonly List<Action> joystickActionsDown;
  private readonly Dictionary<JoystickButtonCode, Action> joystickAxesActions;
  private readonly JoystickSetup joystickSetup;
  private readonly InputDispatcher inputDispatcher;
  private readonly SettingsDataProvider settingsDataProvider;

  public JoystickSetup JoystickSetup
  {
    get
    {
      return this.joystickSetup;
    }
  }

  public InputReceiverJoystick(InputDispatcher inputDispatcher)
  {
    this.inputDispatcher = inputDispatcher;
    this.joystickInputValues = new Dictionary<JoystickButtonCode, InputReceiverJoystick.JoystickInputValues>((IEqualityComparer<JoystickButtonCode>) JoystickButtonCodeComparer.Instance);
    this.joystickActionsDown = new List<Action>();
    this.joystickAxesActions = new Dictionary<JoystickButtonCode, Action>((IEqualityComparer<JoystickButtonCode>) JoystickButtonCodeComparer.Instance);
    this.joystickSetup = new JoystickSetup();
    this.settingsDataProvider = FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider") as SettingsDataProvider;
  }

  public void Update()
  {
    if (!JoystickSetup.Enabled)
      return;
    this.UpdateJoystickInputs();
  }

  private void UpdateJoystickInputs()
  {
    if (!JoystickSetup.Enabled)
      return;
    List<JoystickButtonCode> joystickButtonCodeList = new List<JoystickButtonCode>();
    using (Dictionary<JoystickButtonCode, string>.Enumerator enumerator = this.joystickSetup.InputStringMapping.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<JoystickButtonCode, string> current = enumerator.Current;
        JoystickButtonCode key = current.Key;
        float num = Input.GetAxis(current.Value);
        if (!JoystickSetup.DualAxisList.Contains(key) && JoystickSetup.GetJoystickInputType(key) == JoystickInputType.Axis)
        {
          if (JoystickSetup.IsAxisNegative(key) && (double) num > 0.0 || !JoystickSetup.IsAxisNegative(key) && (double) num < 0.0)
            num = 0.0f;
          num = Mathf.Abs(num);
        }
        this.RefreshJoystickInputValues(key, num);
        if (this.joystickInputValues[key].ButtonStateChanged && this.joystickInputValues[key].ButtonState == KeyState.Up)
          joystickButtonCodeList.Add(key);
      }
    }
    using (Dictionary<JoystickButtonCode, InputReceiverJoystick.JoystickInputValues>.Enumerator enumerator = this.joystickInputValues.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<JoystickButtonCode, InputReceiverJoystick.JoystickInputValues> current = enumerator.Current;
        JoystickButtonCode key = current.Key;
        InputReceiverJoystick.JoystickInputValues joystickInputValues = current.Value;
        if (joystickInputValues.AxisChanged)
          this.NotifyAboutJoystickAxisInput(key, this.joystickInputValues[key].Magnitude, this.joystickInputValues[key].Delta);
        if (joystickInputValues.ButtonStateChanged)
          this.NotifyAboutJoystickButtonTriggered(key, this.joystickInputValues[key].ButtonState);
      }
    }
  }

  private void RefreshJoystickInputValues(JoystickButtonCode code, float magnitude)
  {
    InputReceiverJoystick.JoystickInputValues joystickInputValues1;
    if (this.joystickInputValues.TryGetValue(code, out joystickInputValues1))
    {
      float f = Mathf.Abs(magnitude) - Mathf.Abs(joystickInputValues1.Magnitude);
      InputReceiverJoystick.JoystickInputValues joystickInputValues2 = this.joystickInputValues[code];
      joystickInputValues2.AxisChanged = (double) Mathf.Abs(f) > 0.00499999988824129;
      joystickInputValues2.ButtonStateChanged = false;
      if (joystickInputValues2.AxisChanged)
      {
        joystickInputValues2.Delta = f;
        joystickInputValues2.ButtonDeltaBuffer += f;
        joystickInputValues2.Magnitude = magnitude;
        KeyState keyState = joystickInputValues2.ButtonState;
        if (joystickInputValues2.ButtonState == KeyState.Down && (double) joystickInputValues2.ButtonDeltaBuffer < -0.300000011920929)
          keyState = KeyState.Up;
        if (joystickInputValues2.ButtonState == KeyState.Up && (double) joystickInputValues2.ButtonDeltaBuffer > 0.300000011920929)
          keyState = KeyState.Down;
        if ((double) Mathf.Abs(joystickInputValues2.ButtonDeltaBuffer) > 0.300000011920929)
          joystickInputValues2.ButtonDeltaBuffer = 0.0f;
        if (joystickInputValues2.ButtonState != keyState)
        {
          joystickInputValues2.ButtonState = keyState;
          joystickInputValues2.ButtonStateChanged = true;
        }
      }
      this.joystickInputValues[code] = joystickInputValues2;
    }
    else
      this.joystickInputValues.Add(code, new InputReceiverJoystick.JoystickInputValues()
      {
        Delta = magnitude,
        Magnitude = magnitude
      });
  }

  private void NotifyAboutJoystickButtonTriggered(JoystickButtonCode buttonCode, KeyState triggerState)
  {
    this.inputDispatcher.ResetInactiveTimer((ushort) buttonCode);
    using (List<JoystickBinding>.Enumerator enumerator = this.SelectToggledJoystickBindings(buttonCode).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JoystickBinding current = enumerator.Current;
        switch (triggerState)
        {
          case KeyState.Up:
            this.inputDispatcher.SendActionUp(KeyCode.JoystickButton0, current.Action);
            this.joystickActionsDown.Remove(current.Action);
            continue;
          case KeyState.Down:
            this.inputDispatcher.SendActionDown(KeyCode.JoystickButton0, current.Action);
            this.joystickActionsDown.Add(current.Action);
            continue;
          default:
            continue;
        }
      }
    }
  }

  private void NotifyAboutJoystickAxisInput(JoystickButtonCode joystickButtonCode, float magnitude, float delta)
  {
    this.inputDispatcher.ResetInactiveTimer((ushort) joystickButtonCode);
    List<IInputBinding> triggerBindingList = this.settingsDataProvider.InputBinder.TryGetTriggerBindingList(InputDevice.Joystick, (ushort) joystickButtonCode);
    JoystickBinding joystickBinding1 = (JoystickBinding) null;
    using (List<IInputBinding>.Enumerator enumerator = triggerBindingList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JoystickBinding joystickBinding2 = (JoystickBinding) enumerator.Current;
        if (joystickBinding2.ModifierCode == JoystickButtonCode.None || this.IsJoystickButtonPressed(joystickBinding2.ModifierCode))
        {
          joystickBinding1 = joystickBinding2;
          if (joystickBinding2.ModifierCode != JoystickButtonCode.None)
            break;
        }
      }
    }
    if (joystickBinding1 == null)
      return;
    Action action = Action.None;
    if (this.joystickAxesActions.ContainsKey(joystickButtonCode) && this.joystickAxesActions[joystickButtonCode] != joystickBinding1.Action)
    {
      action = this.joystickAxesActions[joystickButtonCode];
      this.joystickAxesActions.Remove(joystickButtonCode);
    }
    if (action != Action.None)
    {
      JoystickBinding joystickBinding2 = (JoystickBinding) this.settingsDataProvider.InputBinder.TryGetBinding(InputDevice.Joystick, action);
      if (joystickBinding2 == null)
      {
        Debug.LogError((object) ("Failed to reset action: " + (object) action));
      }
      else
      {
        JoystickButtonCode modifierCode = joystickBinding2.ModifierCode;
        this.inputDispatcher.SendJoystickAxesInput(joystickButtonCode, modifierCode, action, 0.0f, 0.0f, false);
      }
    }
    this.inputDispatcher.SendJoystickAxesInput(joystickButtonCode, joystickBinding1.ModifierCode, joystickBinding1.Action, magnitude, delta, joystickBinding1.IsAxisInverted);
    if (this.joystickAxesActions.ContainsKey(joystickButtonCode))
      return;
    this.joystickAxesActions.Add(joystickButtonCode, joystickBinding1.Action);
  }

  private List<JoystickBinding> SelectToggledJoystickBindings(JoystickButtonCode joystickButtonCode)
  {
    List<JoystickBinding> joystickBindingList = new List<JoystickBinding>();
    List<IInputBinding> triggerBindingList = this.settingsDataProvider.InputBinder.TryGetTriggerBindingList(InputDevice.Joystick, (ushort) joystickButtonCode);
    if (this.WasJoystickButtonReleased(joystickButtonCode))
    {
      using (List<IInputBinding>.Enumerator enumerator = triggerBindingList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JoystickBinding joystickBinding = (JoystickBinding) enumerator.Current;
          if (this.joystickActionsDown.Contains(joystickBinding.Action))
            joystickBindingList.Add(joystickBinding);
        }
      }
      return joystickBindingList;
    }
    JoystickBinding joystickBinding1 = (JoystickBinding) null;
    using (List<IInputBinding>.Enumerator enumerator = triggerBindingList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JoystickBinding joystickBinding2 = (JoystickBinding) enumerator.Current;
        if (joystickBinding2.ModifierCode == JoystickButtonCode.None || this.IsJoystickButtonPressed(joystickBinding2.ModifierCode))
        {
          joystickBinding1 = joystickBinding2;
          if (joystickBinding2.ModifierCode != JoystickButtonCode.None)
            break;
        }
      }
    }
    if (joystickBinding1 != null)
      joystickBindingList.Add(joystickBinding1);
    return joystickBindingList;
  }

  private bool IsJoystickButtonPressed(JoystickButtonCode joystickCode)
  {
    if (!this.joystickInputValues.ContainsKey(joystickCode))
      return false;
    return this.joystickInputValues[joystickCode].ButtonState == KeyState.Down;
  }

  private bool WasJoystickButtonReleased(JoystickButtonCode joystickCode)
  {
    if (!this.joystickInputValues.ContainsKey(joystickCode) || !this.joystickInputValues[joystickCode].ButtonStateChanged)
      return false;
    return this.joystickInputValues[joystickCode].ButtonState == KeyState.Up;
  }

  private bool WasJoystickButtonPressed(JoystickButtonCode joystickCode)
  {
    if (!this.joystickInputValues.ContainsKey(joystickCode) || !this.joystickInputValues[joystickCode].ButtonStateChanged)
      return false;
    return this.joystickInputValues[joystickCode].ButtonState == KeyState.Down;
  }

  private struct JoystickInputValues
  {
    public float Magnitude;
    public float Delta;
    public KeyState ButtonState;
    public bool ButtonStateChanged;
    public bool AxisChanged;
    public float ButtonDeltaBuffer;

    public override string ToString()
    {
      return string.Format("Magnitude: {0}, Delta: {1}, ButtonState: {2}, ButtonStateChanged: {3}, ButtonDeltaBuffer {4}", (object) this.Magnitude, (object) this.Delta, (object) this.ButtonState, (object) this.ButtonStateChanged, (object) this.ButtonDeltaBuffer);
    }
  }
}
