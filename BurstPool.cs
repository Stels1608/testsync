﻿// Decompiled with JetBrains decompiler
// Type: BurstPool
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class BurstPool
{
  private Dictionary<BurstKey, HashSet<Burst>> burstPool = new Dictionary<BurstKey, HashSet<Burst>>();
  private Dictionary<BurstKey, Mesh> burstMeshes = new Dictionary<BurstKey, Mesh>();
  public int BurstCount;
  public int PoolCount;
  public int MeshCount;

  public Burst GetBurst(GameObject bulletPrefab, int bulletCount)
  {
    Mesh sharedMesh = bulletPrefab.GetComponent<MeshFilter>().sharedMesh;
    Material sharedMaterial = bulletPrefab.GetComponent<Renderer>().sharedMaterial;
    BurstKey key = this.MakeKey(sharedMesh, bulletCount);
    HashSet<Burst> burstSet = (HashSet<Burst>) null;
    if (!this.burstPool.TryGetValue(key, out burstSet))
    {
      burstSet = new HashSet<Burst>();
      this.burstPool.Add(key, burstSet);
    }
    Burst burst = (Burst) null;
    if (burstSet.Count > 0)
    {
      using (HashSet<Burst>.Enumerator enumerator = burstSet.GetEnumerator())
      {
        if (enumerator.MoveNext())
        {
          burst = enumerator.Current;
          burst.Init();
        }
      }
      burstSet.Remove(burst);
      --this.PoolCount;
    }
    else
      burst = this.MakeBurst(sharedMesh, bulletCount);
    burst.GetComponent<SkinnedMeshRenderer>().sharedMaterial = sharedMaterial;
    return burst;
  }

  private Burst MakeBurst(Mesh bulletMesh, int bulletCount)
  {
    ++this.BurstCount;
    Transform[] bulletTransforms = new Transform[bulletCount];
    for (int index = 0; index < bulletCount; ++index)
      bulletTransforms[index] = new GameObject("bullet").transform;
    GameObject gameObject = new GameObject("burst");
    Burst burst = gameObject.AddComponent<Burst>();
    burst.BurstKey = this.MakeKey(bulletMesh, bulletCount);
    burst.SetBullets(bulletTransforms);
    burst.Destroyed = new Burst.DestroyedEventHandler(this.RestoreToPool);
    SkinnedMeshRenderer skinnedMeshRenderer = gameObject.AddComponent<SkinnedMeshRenderer>();
    skinnedMeshRenderer.sharedMesh = this.GetBurstMesh(bulletMesh, bulletCount);
    skinnedMeshRenderer.bones = bulletTransforms;
    skinnedMeshRenderer.updateWhenOffscreen = true;
    skinnedMeshRenderer.quality = SkinQuality.Bone1;
    skinnedMeshRenderer.receiveShadows = false;
    skinnedMeshRenderer.shadowCastingMode = ShadowCastingMode.Off;
    return burst;
  }

  private Mesh GetBurstMesh(Mesh bulletMesh, int bulletCount)
  {
    BurstKey key = this.MakeKey(bulletMesh, bulletCount);
    Mesh mesh = (Mesh) null;
    if (!this.burstMeshes.TryGetValue(key, out mesh))
    {
      mesh = this.MakeBurstMesh(bulletMesh, bulletCount);
      this.burstMeshes[key] = mesh;
    }
    return mesh;
  }

  private Mesh MakeBurstMesh(Mesh bulletMesh, int bulletCount)
  {
    ++this.MeshCount;
    CombineInstance[] combine = new CombineInstance[bulletCount];
    for (int index = 0; index < bulletCount; ++index)
      combine[index].mesh = bulletMesh;
    Mesh mesh = new Mesh();
    mesh.CombineMeshes(combine, true, false);
    BoneWeight[] boneWeightArray = new BoneWeight[mesh.vertexCount];
    for (int index1 = 0; index1 < bulletCount; ++index1)
    {
      for (int index2 = 0; index2 < bulletMesh.vertexCount; ++index2)
        boneWeightArray[index1 * bulletMesh.vertexCount + index2] = new BoneWeight()
        {
          weight0 = 1f,
          boneIndex0 = index1
        };
    }
    mesh.boneWeights = boneWeightArray;
    Matrix4x4[] matrix4x4Array = new Matrix4x4[bulletCount];
    for (int index = 0; index < bulletCount; ++index)
      matrix4x4Array[index] = Matrix4x4.identity;
    mesh.bindposes = matrix4x4Array;
    return mesh;
  }

  private void RestoreToPool(Burst burst)
  {
    HashSet<Burst> burstSet;
    if (!this.burstPool.TryGetValue(burst.BurstKey, out burstSet))
      return;
    ++this.PoolCount;
    burstSet.Add(burst);
  }

  private BurstKey MakeKey(Mesh bulletMesh, int bulletCount)
  {
    return new BurstKey() { meshID = bulletMesh.GetInstanceID(), count = bulletCount };
  }
}
