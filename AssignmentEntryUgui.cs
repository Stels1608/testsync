﻿// Decompiled with JetBrains decompiler
// Type: AssignmentEntryUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssignmentEntryUgui : MonoBehaviour
{
  private readonly List<AssignmentObjectiveEntryUgui> objectives = new List<AssignmentObjectiveEntryUgui>();
  private bool _isRendered = true;
  [SerializeField]
  private Toggle toggleButton;
  public AssignmentObjectiveEntryUgui primaryObjective;
  private AnonymousDelegate onClick;
  private float lastClickTime;

  public Mission Mission { get; private set; }

  public bool Selected
  {
    get
    {
      return this.toggleButton.isOn;
    }
    set
    {
      this.toggleButton.isOn = value;
    }
  }

  public bool IsRendered
  {
    get
    {
      return this._isRendered;
    }
    set
    {
      this._isRendered = value;
      this.gameObject.SetActive(this._isRendered);
    }
  }

  public void SetMission(Mission mission)
  {
    this.Mission = mission;
    this.UpdateObjectiveEntries(mission);
    this.primaryObjective.SetMission(mission);
    for (int index = 0; index < mission.countables.Count; ++index)
    {
      MissionCountable countable = mission.countables[index];
      AssignmentObjectiveEntryUgui objectiveEntryUgui = this.objectives[index];
      objectiveEntryUgui.EnableProgress(mission.countables.Count > 0);
      objectiveEntryUgui.SetObjective(countable, mission);
    }
  }

  private void UpdateObjectiveEntries(Mission mission)
  {
    if (!this.objectives.Contains(this.primaryObjective))
      this.objectives.Add(this.primaryObjective);
    while (this.objectives.Count < mission.countables.Count)
    {
      AssignmentObjectiveEntryUgui child = UguiTools.CreateChild<AssignmentObjectiveEntryUgui>(this.primaryObjective.gameObject, this.primaryObjective.transform.parent);
      child.gameObject.name = "(G) Secondary Objective";
      this.objectives.Add(child);
    }
    while (this.objectives.Count > mission.countables.Count)
    {
      AssignmentObjectiveEntryUgui objectiveEntryUgui = this.objectives[this.objectives.Count - 1];
      if ((Object) objectiveEntryUgui == (Object) this.primaryObjective)
        break;
      Object.Destroy((Object) objectiveEntryUgui.gameObject);
      this.objectives.Remove(objectiveEntryUgui);
    }
  }

  public void OnToggleChange()
  {
    if (!this.toggleButton.isOn)
      return;
    FacadeFactory.GetInstance().SendMessage(Message.TrackMission, (object) this.Mission);
  }

  public void OnClick()
  {
    FacadeFactory.GetInstance().SendMessage(Message.TrackMission, (object) this.Mission);
    if ((double) Time.realtimeSinceStartup - (double) this.lastClickTime < 0.400000005960464)
      this.OnDoubleClick();
    this.lastClickTime = Time.realtimeSinceStartup;
  }

  private void OnDoubleClick()
  {
    FacadeFactory.GetInstance().SendMessage(Message.ShowSelectedMission, (object) this.Mission.ServerID);
  }
}
