﻿// Decompiled with JetBrains decompiler
// Type: Spot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Spot
{
  private readonly ushort objectPointServerHash;
  private SpotType type;
  protected GameObject gameObject;
  protected Transform transform;
  private Vector3 localPosition;
  private Quaternion localRotation;

  public ushort ObjectPointServerHash
  {
    get
    {
      return this.objectPointServerHash;
    }
  }

  public SpotType Type
  {
    get
    {
      return this.type;
    }
  }

  public Transform Transform
  {
    get
    {
      return this.transform;
    }
  }

  public Vector3 Position
  {
    get
    {
      return this.transform.position;
    }
  }

  public Quaternion Rotation
  {
    get
    {
      return this.transform.rotation;
    }
  }

  public Vector3 LocalPosition
  {
    get
    {
      return this.localPosition;
    }
  }

  public Quaternion LocalRotation
  {
    get
    {
      return this.localRotation;
    }
  }

  protected Spot(SpotDesc spotDesc, GameObject gameObject)
  {
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      throw new ArgumentNullException("gameObject");
    this.objectPointServerHash = spotDesc.ObjectPointServerHash;
    this.type = spotDesc.Type;
    this.gameObject = gameObject;
    this.transform = gameObject.transform;
    this.localPosition = spotDesc.LocalPosition;
    this.localRotation = spotDesc.LocalRotation;
  }

  public static Spot Create(SpotDesc spotDesc, GameObject gameObject)
  {
    if (spotDesc.Type == SpotType.Sticker)
      return (Spot) new DecalSpot(spotDesc, gameObject);
    return (Spot) new FixingSpot(spotDesc, gameObject);
  }

  public static Spot[] FindSpots(SpotDesc[] spotDescs, GameObject root)
  {
    List<Spot> spotList = new List<Spot>();
    Transform[] componentsInChildren = root.GetComponentsInChildren<Transform>();
    foreach (SpotDesc spotDesc in spotDescs)
    {
      foreach (Transform transform in componentsInChildren)
      {
        if (transform.name.Equals(spotDesc.ObjectPointName))
        {
          Spot spot = Spot.Create(spotDesc, transform.gameObject);
          spotList.Add(spot);
          break;
        }
      }
    }
    return spotList.ToArray();
  }
}
