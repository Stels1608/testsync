﻿// Decompiled with JetBrains decompiler
// Type: InputMessageContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class InputMessageContent
{
  public object Data { get; set; }

  public InputMessage InputMessage { get; set; }

  public InputMessageContent(InputMessage inputMessage, object value)
  {
    this.InputMessage = inputMessage;
    this.Data = value;
  }
}
