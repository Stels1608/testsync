﻿// Decompiled with JetBrains decompiler
// Type: TurnToDirectionStrikes
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class TurnToDirectionStrikes : Maneuver
{
  private Euler3 direction;
  private float roll;
  private float slideX;
  private float slideY;

  public TurnToDirectionStrikes()
  {
  }

  public TurnToDirectionStrikes(Euler3 direction, float roll, float slideX, float slideY, MovementOptions movementOptions)
  {
    this.direction = direction;
    this.roll = roll;
    this.slideX = slideX;
    this.slideY = slideY;
    this.options = movementOptions;
  }

  public override string ToString()
  {
    return string.Format("TurnToDirectionStrikes: startTick = {0}, direction={1}, roll={2}, slideX={3}, slideY={4} ", (object) this.startTick, (object) this.direction, (object) this.roll, (object) this.slideX, (object) this.slideY);
  }

  public override MovementFrame NextFrame(Tick tick, MovementFrame prevFrame)
  {
    return Simulation.TurnToDirectionStrikes(prevFrame, this.direction, this.roll, this.slideX, this.slideY, this.options);
  }

  public override void Read(BgoProtocolReader pr)
  {
    base.Read(pr);
    this.startTick = pr.ReadTick();
    this.direction = pr.ReadEuler();
    this.roll = pr.ReadSingle();
    this.slideX = pr.ReadSingle();
    this.slideY = pr.ReadSingle();
    this.options.Read(pr);
  }
}
