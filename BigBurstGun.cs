﻿// Decompiled with JetBrains decompiler
// Type: BigBurstGun
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BigBurstGun : AbstractBurstGun
{
  public float Framerate = 30f;
  public float Volume = 1f;
  public float MinDistance = 1f;
  public Transform MuzzleSpot;
  public GameObject MuzzleEffect;
  public int[] ShotFrames;
  public AudioClip VolleyClip;
  private AudioSource gunAudio;
  private float fireTime;

  protected override void Awake()
  {
    base.Awake();
    if (!((Object) this.gunAudio == (Object) null))
      return;
    this.gunAudio = this.gameObject.AddComponent<AudioSource>();
    this.gunAudio.spatialBlend = 1f;
  }

  protected override void OnDisable()
  {
    base.OnDisable();
    if (!((Object) Game.Instance != (Object) null) || Game.IsLoadingLevel || Game.IsQuitting)
      return;
    if ((Object) this.burst != (Object) null)
      this.burst.DestroyBulletRest();
    this.burst = (Burst) null;
  }

  protected override void LateUpdate()
  {
    base.LateUpdate();
    if (this.madeShotCount < this.ShotCount && (double) Time.time >= (double) (this.fireTime + (float) this.ShotFrames[this.madeShotCount] / this.Framerate))
    {
      this.MakeShot();
      ++this.madeShotCount;
    }
    this.SaveLastTargetPosition();
  }

  public override void Fire(SpaceObject target)
  {
    base.Fire(target);
    if (this.HighQuality)
    {
      if ((Object) this.gunAudio == (Object) null)
      {
        this.gunAudio = this.gameObject.AddComponent<AudioSource>();
        this.gunAudio.spatialBlend = 1f;
      }
      this.gunAudio.volume = this.Volume;
      this.gunAudio.minDistance = this.MinDistance;
      this.gunAudio.maxDistance = this.gunAudio.minDistance * 256f;
      this.gunAudio.clip = this.VolleyClip;
      this.gunAudio.loop = false;
      this.gunAudio.Play();
    }
    this.fireTime = Time.time;
  }

  protected override Vector3 GetShotOrigin()
  {
    if ((Object) this.MuzzleSpot != (Object) null)
      return this.MuzzleSpot.transform.position;
    return this.transform.position;
  }

  protected override void MakeShot()
  {
    base.MakeShot();
    BulletScript bulletScript = !((Object) this.burst != (Object) null) ? (BulletScript) null : this.burst.CurrentBullet();
    if (!this.HighQuality || !(bool) ((Object) bulletScript))
      return;
    Vector3 position = bulletScript.transform.position;
    Quaternion rotation = bulletScript.transform.rotation;
    if (!((Object) this.MuzzleEffect != (Object) null))
      return;
    Object.Instantiate((Object) this.MuzzleEffect, position, rotation);
  }
}
