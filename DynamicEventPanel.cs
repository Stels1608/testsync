﻿// Decompiled with JetBrains decompiler
// Type: DynamicEventPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DynamicEventPanel : MonoBehaviour
{
  private readonly Dictionary<uint, EventFoldOutWidget> activeTrackers = new Dictionary<uint, EventFoldOutWidget>();

  public void RemoveEventTracker(uint eventObjectId)
  {
    this.activeTrackers.Remove(eventObjectId);
  }

  public void UpdateEventTasks(uint eventObjectId, List<SectorEventTask> tasks)
  {
    this.GetTracker(eventObjectId).UpdateTasks(tasks);
    this.RepositionTable();
  }

  public void UpdateEventState(SectorEventStateUpdate eventStateUpdate)
  {
    EventFoldOutWidget tracker = this.GetTracker(eventStateUpdate.EventObjectId);
    tracker.SectorEvent.Update(eventStateUpdate);
    tracker.Refresh();
  }

  private EventFoldOutWidget GetTracker(uint eventObjectId)
  {
    if (this.activeTrackers.ContainsKey(eventObjectId))
      return this.activeTrackers[eventObjectId];
    SectorEvent sectorEvent = (SectorEvent) SpaceLevel.GetLevel().GetObjectRegistry().Get(eventObjectId);
    GameObject child = (GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/ui_elements/event/DynamicEventTracker"));
    PositionUtils.CorrectTransform(this.gameObject, child);
    EventFoldOutWidget component = child.GetComponent<EventFoldOutWidget>();
    component.SectorEvent = sectorEvent;
    component.parentTable = this.GetComponent<UITable>();
    this.activeTrackers.Add(eventObjectId, component);
    return component;
  }

  public void ShowReward(SectorEventReward reward)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DynamicEventPanel.\u003CShowReward\u003Ec__AnonStoreyCB rewardCAnonStoreyCb = new DynamicEventPanel.\u003CShowReward\u003Ec__AnonStoreyCB();
    // ISSUE: reference to a compiler-generated field
    rewardCAnonStoreyCb.reward = reward;
    // ISSUE: reference to a compiler-generated field
    rewardCAnonStoreyCb.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    rewardCAnonStoreyCb.foldout = (EventFoldOutWidget) null;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    Game.Catalogue.FetchCard(rewardCAnonStoreyCb.reward.SectorEventGuid, CardView.SectorEvent).IsLoaded.AddHandler(new SignalHandler(rewardCAnonStoreyCb.\u003C\u003Em__1E4));
  }

  public void RepositionPanel(bool assignmentsCollapsed)
  {
    this.transform.localPosition = (Vector3) (!assignmentsCollapsed ? new Vector2(0.0f, 380f) : new Vector2(0.0f, 200f));
  }

  public void RepositionTable()
  {
    this.GetComponent<UITable>().Reposition();
  }

  public void Show(bool show)
  {
    this.gameObject.SetActive(show);
  }
}
