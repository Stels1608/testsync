﻿// Decompiled with JetBrains decompiler
// Type: ExplosionEffectState
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ExplosionEffectState : EffectState
{
  private float distInvisible = 1000f;
  private int qInvisible = 1;
  private Vector3 position;
  private Quaternion rotation;
  private ExplosionArgs explosionArgs;
  private GameObject explosion;
  private GameObject prefab;
  private bool executed;
  private float speed;

  public override EffectCategory Category
  {
    get
    {
      if (this.explosionArgs is BulletExplosionArgs)
        return EffectCategory.BulletExplosion;
      if (this.explosionArgs is MissileExplosionArgs)
        return EffectCategory.MissileExplosion;
      return this.explosionArgs is ShipExplosionArgs ? EffectCategory.ShipExplosion : EffectCategory.Nil;
    }
  }

  public ExplosionEffectState(Vector3 position, Quaternion rotation, ExplosionArgs explosionArgs, float speed = 0)
  {
    this.position = position;
    this.rotation = rotation;
    this.explosionArgs = explosionArgs;
    this.speed = speed;
  }

  protected override void OnLaunched()
  {
    GameObject[] gameObjectArray = (GameObject[]) null;
    if (this.explosionArgs is MissileExplosionArgs)
    {
      MissileExplosionArgs missileExplosionArgs = (MissileExplosionArgs) this.explosionArgs;
      switch (missileExplosionArgs.ExplosionView)
      {
        case MissileExplosionArgs.MissileExplosionView.Standard:
          switch (missileExplosionArgs.ExplosionTier)
          {
            case 1:
              gameObjectArray = SpaceExposer.GetInstance().SmallMissileExplosions;
              this.distInvisible = 500f;
              break;
            case 2:
              gameObjectArray = SpaceExposer.GetInstance().MediumMissileExplosions;
              this.distInvisible = 750f;
              break;
            case 3:
              gameObjectArray = SpaceExposer.GetInstance().LargeMissileExplosions;
              this.distInvisible = 1000f;
              break;
            case 4:
              gameObjectArray = SpaceExposer.GetInstance().LargeMissileExplosions;
              this.distInvisible = 8000f;
              break;
            default:
              Debug.LogWarning((object) ("Unknown missile explosion tier: " + (object) missileExplosionArgs.ExplosionTier));
              break;
          }
        case MissileExplosionArgs.MissileExplosionView.Nuclear:
          gameObjectArray = SpaceExposer.GetInstance().NukeExplosions;
          this.distInvisible = 15000f;
          break;
        case MissileExplosionArgs.MissileExplosionView.NuclearMini:
          gameObjectArray = SpaceExposer.GetInstance().MiniNukeExplosions;
          this.distInvisible = 15000f;
          break;
        case MissileExplosionArgs.MissileExplosionView.Torpedo:
          this.distInvisible = 20000f;
          if ((double) missileExplosionArgs.ExplosionRange - 200.0 < 1.0 / 1000.0)
          {
            gameObjectArray = SpaceExposer.GetInstance().Torpedo200Explosions;
            break;
          }
          if ((double) missileExplosionArgs.ExplosionRange - 400.0 < 1.0 / 1000.0)
          {
            gameObjectArray = SpaceExposer.GetInstance().Torpedo400Explosions;
            break;
          }
          if ((double) missileExplosionArgs.ExplosionRange - 600.0 < 1.0 / 1000.0)
          {
            gameObjectArray = SpaceExposer.GetInstance().Torpedo600Explosions;
            break;
          }
          break;
      }
    }
    else if (this.explosionArgs is ShipExplosionArgs)
    {
      ShipExplosionArgs shipExplosionArgs = (ShipExplosionArgs) this.explosionArgs;
      switch (shipExplosionArgs.Faction)
      {
        case Faction.Colonial:
          switch (shipExplosionArgs.Tier)
          {
            case 1:
              gameObjectArray = SpaceExposer.GetInstance().ColonialSmallShipExplosions;
              this.distInvisible = 1000f;
              break;
            case 2:
              gameObjectArray = SpaceExposer.GetInstance().ColonialMediumShipExplosions;
              this.distInvisible = 7500f;
              break;
            case 3:
            case 4:
              gameObjectArray = SpaceExposer.GetInstance().ColonialLargeShipExplosions;
              this.distInvisible = 10000f;
              break;
          }
        case Faction.Cylon:
        case Faction.Ancient:
          switch (shipExplosionArgs.Tier)
          {
            case 1:
              gameObjectArray = SpaceExposer.GetInstance().CylonSmallShipExplosions;
              this.distInvisible = 1000f;
              break;
            case 2:
              gameObjectArray = SpaceExposer.GetInstance().CylonMediumShipExplosions;
              this.distInvisible = 7500f;
              break;
            case 3:
            case 4:
              gameObjectArray = SpaceExposer.GetInstance().CylonLargeShipExplosions;
              this.distInvisible = 10000f;
              break;
          }
      }
    }
    if (gameObjectArray == null)
    {
      Debug.LogError((object) ("No explosion prefabs found for " + (object) this.explosionArgs));
    }
    else
    {
      this.prefab = gameObjectArray[Random.Range(0, gameObjectArray.Length)];
      this.qualityEvaluator = (IQualityEvaluator) new DistanceQualityEvaluator();
      (this.qualityEvaluator as DistanceQualityEvaluator).SourcePosition = this.position;
      (this.qualityEvaluator as DistanceQualityEvaluator).Target = Camera.main.transform;
      (this.qualityEvaluator as DistanceQualityEvaluator).MinQuality = this.qInvisible;
      (this.qualityEvaluator as DistanceQualityEvaluator).QualityDist = this.distInvisible;
    }
  }

  protected override void OnUpdate()
  {
    if (!this.executed)
    {
      this.executed = true;
      this.OnExecute();
    }
    if (!((Object) this.explosion == (Object) null))
      return;
    this.Terminate();
  }

  private void OnExecute()
  {
    if (this.Quality >= this.qInvisible)
      return;
    this.explosion = (GameObject) Object.Instantiate((Object) this.prefab, this.position, this.rotation);
    this.explosion.AddComponent<FloatingSpaceObject>().velocity = this.explosion.transform.forward * this.speed;
  }

  protected override void OnTerminate()
  {
    Object.Destroy((Object) this.explosion);
  }
}
