﻿// Decompiled with JetBrains decompiler
// Type: WoFMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System.Collections.Generic;
using UnityEngine;

public class WoFMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "WoFMediator";
  private WoFWindowWidget woFWindow;

  public WoFMediator()
    : base("WoFMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.ShowWoFWindow);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.ShowWofWindowWithDialogHandling);
    this.AddMessageInterest(Message.GetDradisGameDetails);
    this.AddMessageInterest(Message.ShowRewad);
    this.AddMessageInterest(Message.WofBonusMapPartsUpdate);
    this.AddMessageInterest(Message.WofVisibleMaps);
    this.AddMessageInterest(Message.PlayerLevel);
    this.AddMessageInterest(Message.PlayerCubitsHold);
    this.AddMessageInterest(Message.WofMapInformation);
    this.AddMessageInterest(Message.FtlMissionsOff);
    this.AddMessageInterest(Message.WofShowConfirmation);
    this.AddMessageInterest(Message.GuiContentWindowVisibility);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.WofVisibleMaps:
        this.woFWindow.SetVisibleBonusLevel((List<int>) message.Data);
        break;
      case Message.WofBonusMapPartsUpdate:
        this.woFWindow.UpdateBonusMapParts((Dictionary<int, List<int>>) message.Data);
        break;
      case Message.GetDradisGameDetails:
        WofInitReply wofInitReply = (WofInitReply) message.Data;
        this.woFWindow.SetDradisGameDetails(wofInitReply.jackPotCard, wofInitReply.isFreeGame, wofInitReply.costsPerStep);
        break;
      case Message.ShowRewad:
        this.woFWindow.ShowRewad((WofDrawReply) message.Data);
        break;
      case Message.WofMapInformation:
        this.woFWindow.UpdateMapInformation((Dictionary<int, GUICard>) message.Data);
        break;
      case Message.WofShowConfirmation:
        this.woFWindow.SetWofConfirmation((bool) message.Data);
        break;
      case Message.PlayerLevel:
        this.woFWindow.UpdateLevel((byte) message.Data);
        break;
      case Message.PlayerCubitsHold:
        this.woFWindow.UpdateCubits((uint) message.Data);
        break;
      case Message.FtlMissionsOff:
        this.woFWindow.DisableFtlMissions();
        break;
      default:
        if (id != Message.ShowWoFWindow)
        {
          if (id != Message.ShowWofWindowWithDialogHandling)
          {
            if (id != Message.UiCreated)
            {
              if (id != Message.LoadNewLevel)
              {
                if (id == Message.GuiContentWindowVisibility)
                {
                  Tuple<WindowTypes, bool> tuple = (Tuple<WindowTypes, bool>) message.Data;
                  if (tuple.First != WindowTypes.WoFWindow)
                    break;
                  if (this.woFWindow.IsOpen && !tuple.Second)
                    this.woFWindow.Close();
                  if (this.woFWindow.IsOpen || !tuple.Second)
                    break;
                  this.woFWindow.Open();
                  break;
                }
                Log.Warning((object) ("Unknown Message was catched but not handled by the HelpMediator => " + (object) message.Id));
                break;
              }
              this.HideWoFWindow();
              break;
            }
            this.CreateView();
            break;
          }
          this.ShowWoFWindow(true);
          break;
        }
        this.ShowWoFWindow(false);
        break;
    }
  }

  private void CreateView()
  {
    this.woFWindow = ((GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/ui_elements/WoF/WoFWindow"))).GetComponent<WoFWindowWidget>();
    if ((Object) this.woFWindow == (Object) null)
      Debug.LogError((object) "WoFWindowWidget was not registered correctly");
    GuiDataProvider guiDataProvider = this.OwnerFacade.FetchDataProvider("GuiDataProvider") as GuiDataProvider;
    if (guiDataProvider == null)
      return;
    guiDataProvider.AddWindow((WindowWidget) this.woFWindow);
  }

  private void ShowWoFWindow(bool dialogHandling)
  {
    if ((Object) null == (Object) this.woFWindow)
      return;
    if (this.woFWindow.IsOpen)
    {
      this.woFWindow.Close();
    }
    else
    {
      this.woFWindow.Open();
      if (!dialogHandling)
        return;
      this.woFWindow.closeDelegate = new AnonymousDelegate(this.DialogHandling);
    }
  }

  private void DialogHandling()
  {
    RoomStateNewDialog roomStateNewDialog = (RoomStateNewDialog) RoomLevel.GetLevel().RoomState;
    if (roomStateNewDialog != null)
      roomStateNewDialog.OnCloseHangarWindow();
    this.woFWindow.Close();
  }

  private void HideWoFWindow()
  {
    if (!((Object) this.woFWindow != (Object) null) || !this.woFWindow.IsOpen)
      return;
    this.woFWindow.Close();
  }
}
