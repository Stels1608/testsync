﻿// Decompiled with JetBrains decompiler
// Type: BgoProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class BgoProtocol
{
  public readonly Dictionary<Enum, uint> protocolCounts = new Dictionary<Enum, uint>();
  public readonly BgoProtocol.ProtocolID protocolID;
  private Connection connection;
  private bool enabled;
  private bool wasWarnedOnDisabledProtocol;

  public BgoProtocol(BgoProtocol.ProtocolID protocolID)
  {
    this.protocolID = protocolID;
    this.enabled = true;
  }

  protected void DebugAddMsg(Enum proto)
  {
    if (this.protocolCounts.ContainsKey(proto))
    {
      Dictionary<Enum, uint> dictionary;
      Enum index;
      (dictionary = this.protocolCounts)[index = proto] = dictionary[index] + 1U;
    }
    else
      this.protocolCounts[proto] = 1U;
  }

  public bool IsConnected()
  {
    return this.connection.GetState() == Connection.State.Connected;
  }

  public void SetConnection(Connection connection)
  {
    this.connection = connection;
  }

  public void ReceiveMessage(ushort msgType, BgoProtocolReader br)
  {
    if (this.enabled)
      this.ParseMessage(msgType, br);
    else
      Log.Warning((object) string.Format("Message {0} received for disabled protocol {1} {2}", (object) msgType, (object) this.protocolID, (object) this.GetType().ToString()));
  }

  public virtual void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
  }

  public virtual void UpdateMessage()
  {
  }

  protected BgoProtocolWriter NewMessage()
  {
    BgoProtocolWriter bgoProtocolWriter = new BgoProtocolWriter();
    bgoProtocolWriter.Write((byte) this.protocolID);
    return bgoProtocolWriter;
  }

  protected void SendMessage(BgoProtocolWriter bw)
  {
    if (bw.GetLength() <= 3)
      return;
    if (this.enabled)
    {
      this.connection.SendMessage(bw);
      this.wasWarnedOnDisabledProtocol = false;
    }
    else
    {
      if (this.wasWarnedOnDisabledProtocol)
        return;
      this.wasWarnedOnDisabledProtocol = true;
      Debug.LogError((object) string.Format("Trying to send message for disabled protocol {0}", (object) this.protocolID));
    }
  }

  public void SetEnabled(bool enabled)
  {
    this.enabled = enabled;
  }

  public bool IsEnabled()
  {
    return this.enabled;
  }

  public enum ProtocolID : byte
  {
    Login,
    Universe,
    Game,
    Sync,
    Player,
    Debug,
    Catalogue,
    Ranking,
    Story,
    Scene,
    Room,
    Community,
    Shop,
    Setting,
    Ship,
    Dialog,
    Market,
    Notification,
    Subscribe,
    Feedback,
    Tournament,
    Arena,
    Battlespace,
    Wof,
  }
}
