﻿// Decompiled with JetBrains decompiler
// Type: InputDataProvider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Model;
using UnityEngine;

public class InputDataProvider : DataProvider<Message>
{
  public InputDispatcher InputDispatcher
  {
    get
    {
      return Game.InputDispatcher;
    }
  }

  public SpaceCameraBase SpaceCamera
  {
    get
    {
      return Camera.main.transform.parent.GetComponent<SpaceCameraBase>();
    }
  }

  public InputDataProvider()
    : base("InputDataProvider")
  {
  }
}
