﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorDistanceStringCache
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class HudIndicatorDistanceStringCache
{
  private static readonly HudIndicatorDistanceStringCache instance = new HudIndicatorDistanceStringCache();
  private const int ENTRIES = 20000;
  private static string[] distanceStrings;
  private readonly UIAtlas uiAtlas;

  public static HudIndicatorDistanceStringCache Instance
  {
    get
    {
      return HudIndicatorDistanceStringCache.instance;
    }
  }

  private HudIndicatorDistanceStringCache()
  {
    HudIndicatorDistanceStringCache.distanceStrings = new string[20000];
    for (int index = 0; index < 20000; ++index)
      HudIndicatorDistanceStringCache.distanceStrings[index] = index.ToString();
  }

  public string GetDistanceString(int distance)
  {
    if (distance >= 20000)
      return distance.ToString();
    return HudIndicatorDistanceStringCache.distanceStrings[distance];
  }
}
