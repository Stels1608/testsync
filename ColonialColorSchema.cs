﻿// Decompiled with JetBrains decompiler
// Type: ColonialColorSchema
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ColonialColorSchema : WidgetColorScheme
{
  public ColonialColorSchema()
  {
    this.Set(WidgetColorType.WINDOW_STRIPE, ImageUtils.HexStringToColor("3fa1ca"));
    this.Set(WidgetColorType.FACTION_COLOR, ImageUtils.HexStringToColor("698ccd"));
    this.Set(WidgetColorType.FACTION_CLICK_COLOR, ImageUtils.HexStringToColor("cbdbf3"));
    this.Set(WidgetColorType.FACTION_DISABLED_COLOR, Color.grey);
    this.Set(WidgetColorType.UGUI_FACTION_COLOR_NORMAL, ImageUtils.HexStringToColor("395881"));
    this.Set(WidgetColorType.UGUI_FACTION_COLOR_CLICK, ImageUtils.HexStringToColor("bfdaff"));
    this.Set(WidgetColorType.UGUI_FACTION_COLOR_DISABLED, Color.grey);
    this.Set(WidgetColorType.DIALOG_NPC_NAME, this.Get(WidgetColorType.FACTION_COLOR));
    this.Set(WidgetColorType.DIALOG_ANSWER_COLOR_NORMAL, this.Get(WidgetColorType.FACTION_CLICK_COLOR));
  }
}
