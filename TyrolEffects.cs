﻿// Decompiled with JetBrains decompiler
// Type: TyrolEffects
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TyrolEffects : FlickeringLightEffect
{
  public ParticleSystem sparks;
  public ParticleSystem flame;
  public bool isInTalkMode;

  public override void Start()
  {
    base.Start();
    this.flame.enableEmission = true;
    this.sparks.enableEmission = true;
  }

  public override void FixedUpdate()
  {
    if (!this.isInTalkMode)
      this.flashingLight.enabled = (double) Random.Range(this.min, this.max) <= (double) this.isTrueValue;
    else
      this.flashingLight.enabled = false;
  }

  public void IsInTalk()
  {
    this.isInTalkMode = true;
    this.flame.enableEmission = false;
    this.sparks.enableEmission = false;
    this.CancelInvoke("IsNotInTalk");
  }

  public void IsNotInTalk()
  {
    this.isInTalkMode = false;
    this.flame.enableEmission = true;
    this.sparks.enableEmission = true;
  }

  public void DelayIsNotInTalk()
  {
    this.Invoke("IsNotInTalk", 2f);
  }
}
