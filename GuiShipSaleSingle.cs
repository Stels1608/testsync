﻿// Decompiled with JetBrains decompiler
// Type: GuiShipSaleSingle
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GuiShipSaleSingle : GuiShipSaleDialog
{
  private readonly GuiButton purchaseButton;

  public GuiShipSaleSingle(ShipSaleCard saleCard)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiShipSaleSingle.\u003CGuiShipSaleSingle\u003Ec__AnonStoreyB4 singleCAnonStoreyB4 = new GuiShipSaleSingle.\u003CGuiShipSaleSingle\u003Ec__AnonStoreyB4();
    // ISSUE: explicit constructor call
    base.\u002Ector(saleCard);
    this.m_backgroundImage.Texture = ResourceLoader.Load<Texture2D>("GUI/Gui2/background_small_dark");
    this.m_backgroundImage.NineSliceEdge = new GuiElementPadding(new Rect(8f, 8f, 8f, 8f));
    this.Size = new Vector2(812f, 392f);
    this.AddChild((GuiElementBase) new GuiLabel(saleCard.GUICard.Description, Gui.Options.FontBGM_BT, 20), Align.UpCenter, new Vector2(0.0f, 16f));
    RewardCard rewardCard = this.shipSaleCard.ShipSales[0];
    GuiLabel guiLabel = new GuiLabel(rewardCard.GUICard.Description, Gui.Options.FontBGM_BT, 15);
    guiLabel.SizeX = 780f;
    guiLabel.WordWrap = true;
    this.AddChild((GuiElementBase) guiLabel, Align.UpCenter, new Vector2(0.0f, 42f));
    this.AddChild((GuiElementBase) new GuiImage(rewardCard.GUICard.GUITexture), Align.UpLeft, new Vector2(0.0f, 94f));
    // ISSUE: reference to a compiler-generated field
    singleCAnonStoreyB4.pack = rewardCard.Package;
    // ISSUE: reference to a compiler-generated field
    singleCAnonStoreyB4.itemGroup = rewardCard.ItemGroup;
    this.purchaseButton = new GuiButton("%$bgo.shipsale.viewshippack%");
    // ISSUE: reference to a compiler-generated method
    this.purchaseButton.OnClick = new AnonymousDelegate(singleCAnonStoreyB4.\u003C\u003Em__188);
    this.purchaseButton.Font = Gui.Options.FontBGM_BT;
    this.purchaseButton.FontSize = 14;
    this.purchaseButton.SizeX = 240f;
    this.AddChild((GuiElementBase) this.purchaseButton, Align.DownCenter, new Vector2(0.0f, -20f));
    ShopProtocol.GetProtocol().RequestBoughtShipSaleOffer(rewardCard.CardGUID);
    this.OnClose = new AnonymousDelegate(GuiDialogPopupManager.CloseDialog);
  }

  public override void DisableOffer(uint guid)
  {
    GuiButton guiButton = this.purchaseButton;
    bool flag = false;
    this.purchaseButton.HandleMouseInput = flag;
    int num = flag ? 1 : 0;
    guiButton.IsActive = num != 0;
    GuiDialogPopupManager.CloseDialog();
  }
}
