﻿// Decompiled with JetBrains decompiler
// Type: AvatarSelector
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AvatarSelector
{
  public static string avatarDocName = "avatar";

  public static void SaveDescription(AvatarDescriptionInDB avatarDescription)
  {
    if (ContentDB.DocumentExists(AvatarSelector.avatarDocName))
      ContentDB.DeleteDocument(AvatarSelector.avatarDocName);
    ContentDB.CreateDocument<AvatarDescriptionInDB>(avatarDescription, AvatarSelector.avatarDocName);
    Debug.Log((object) (avatarDescription.ToString() + " saved (count = " + (object) avatarDescription.Avatars.Count + ")"));
  }

  public static List<string> GetAllTextures(string race, string sex)
  {
    List<string> stringList = new List<string>();
    using (Dictionary<string, List<string>>.Enumerator enumerator = StaticCards.Avatar.AvatarIndexes[AvatarSelector.GetAvatarIndex(race, sex)].textures.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, List<string>> current = enumerator.Current;
        stringList.AddRange((IEnumerable<string>) current.Value);
      }
    }
    return stringList;
  }

  public static List<string> GetAllMaterials(string race, string sex)
  {
    List<string> stringList = new List<string>();
    using (Dictionary<string, Dictionary<string, List<string>>>.Enumerator enumerator1 = StaticCards.Avatar.AvatarIndexes[AvatarSelector.GetAvatarIndex(race, sex)].materials.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (Dictionary<string, List<string>>.Enumerator enumerator2 = enumerator1.Current.Value.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            KeyValuePair<string, List<string>> current = enumerator2.Current;
            stringList.AddRange((IEnumerable<string>) current.Value);
          }
        }
      }
    }
    return stringList;
  }

  private static string GetRace(int index)
  {
    return StaticCards.Avatar.AvatarIndexes[index].race;
  }

  private static string GetSex(int index)
  {
    return StaticCards.Avatar.AvatarIndexes[index].race;
  }

  private static int GetItemIndex(int avatar, string item, string itemValue)
  {
    if (StaticCards.Avatar.AvatarIndexes[avatar].items[item].Count == 0)
    {
      Debug.Log((object) ("Not item found: " + item));
      return -1;
    }
    for (int index = 0; index < StaticCards.Avatar.AvatarIndexes[avatar].items[item].Count; ++index)
    {
      if (StaticCards.Avatar.AvatarIndexes[avatar].items[item][index] == itemValue)
        return index;
    }
    return -1;
  }

  private static int GetTextureIndex(int avatar, string texture, string textureValue)
  {
    if (StaticCards.Avatar.AvatarIndexes[avatar].textures[texture].Count == 0)
    {
      Debug.Log((object) ("Not texture found: " + texture));
      return -1;
    }
    for (int index = 0; index < StaticCards.Avatar.AvatarIndexes[avatar].textures[texture].Count; ++index)
    {
      if (StaticCards.Avatar.AvatarIndexes[avatar].textures[texture][index] == textureValue)
        return index;
    }
    return -1;
  }

  private static string GetItemNameByMaterial(int avatar, string material, string materialValue)
  {
    using (Dictionary<string, List<string>>.Enumerator enumerator1 = StaticCards.Avatar.AvatarIndexes[avatar].materials[material].GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<string, List<string>> current = enumerator1.Current;
        using (List<string>.Enumerator enumerator2 = current.Value.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            if (enumerator2.Current == materialValue)
              return current.Key;
          }
        }
      }
    }
    return string.Empty;
  }

  private static int GetMaterialIndex(int avatar, string material, string materialValue)
  {
    if (StaticCards.Avatar.AvatarIndexes[avatar].materials[material].Count == 0)
      return -1;
    using (Dictionary<string, List<string>>.Enumerator enumerator = StaticCards.Avatar.AvatarIndexes[avatar].materials[material].GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, List<string>> current = enumerator.Current;
        for (int index = 0; index < current.Value.Count; ++index)
        {
          if (current.Value[index] == materialValue)
            return index;
        }
      }
    }
    return -1;
  }

  public static int GetPropertyIndex(string race, string sex, string property, string propertyValue)
  {
    int avatarIndex = AvatarSelector.GetAvatarIndex(race, sex);
    if (AvatarInfo.IsItem(race, property))
      return AvatarSelector.GetItemIndex(avatarIndex, property, propertyValue);
    if (AvatarInfo.IsTexture(race, property))
      return AvatarSelector.GetTextureIndex(avatarIndex, property, propertyValue);
    if (AvatarInfo.IsMaterial(race, property))
      return AvatarSelector.GetMaterialIndex(avatarIndex, property, propertyValue);
    return -1;
  }

  private static int GetMaterialCount(string race, string sex, string material)
  {
    using (Dictionary<string, List<string>>.Enumerator enumerator = StaticCards.Avatar.AvatarIndexes[AvatarSelector.GetAvatarIndex(race, sex)].materials[material].GetEnumerator())
    {
      if (enumerator.MoveNext())
        return enumerator.Current.Value.Count;
    }
    return -1;
  }

  public static int GetPropertyCount(string race, string sex, string property)
  {
    int avatarIndex = AvatarSelector.GetAvatarIndex(race, sex);
    if (AvatarInfo.IsItem(race, property))
      return StaticCards.Avatar.AvatarIndexes[avatarIndex].items[property].Count;
    if (AvatarInfo.IsTexture(race, property))
      return StaticCards.Avatar.AvatarIndexes[avatarIndex].textures[property].Count;
    if (AvatarInfo.IsMaterial(race, property))
      return AvatarSelector.GetMaterialCount(race, sex, property);
    return -1;
  }

  public static string GetItem(int avatar, string item, int index)
  {
    if (StaticCards.Avatar.AvatarIndexes[avatar].items[item].Count == 0)
      return string.Empty;
    return StaticCards.Avatar.AvatarIndexes[avatar].items[item][index];
  }

  public static string GetTexture(int avatar, string texture, int index)
  {
    if (StaticCards.Avatar.AvatarIndexes[avatar].textures[texture].Count == 0)
      return string.Empty;
    return StaticCards.Avatar.AvatarIndexes[avatar].textures[texture][index];
  }

  public static string GetMaterial(int avatar, string material, string item, int index)
  {
    if (StaticCards.Avatar.AvatarIndexes[avatar].materials[material].Count == 0)
      return string.Empty;
    using (Dictionary<string, List<string>>.Enumerator enumerator = StaticCards.Avatar.AvatarIndexes[avatar].materials[material].GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, List<string>> current = enumerator.Current;
        if (current.Key == item && index > -1 && index < current.Value.Count)
          return current.Value[index];
      }
    }
    return string.Empty;
  }

  public static string GetProperty(string race, string sex, string property, string propParam, int index)
  {
    int avatarIndex = AvatarSelector.GetAvatarIndex(race, sex);
    if (AvatarInfo.IsItem(race, property))
      return AvatarSelector.GetItem(avatarIndex, property, index);
    if (AvatarInfo.IsTexture(race, property))
      return AvatarSelector.GetTexture(avatarIndex, property, index);
    if (AvatarInfo.IsMaterial(race, property))
      return AvatarSelector.GetMaterial(avatarIndex, property, propParam, index);
    return string.Empty;
  }

  public static int GetAvatarIndex(string race, string sex)
  {
    for (int index = 0; index < StaticCards.Avatar.AvatarIndexes.Count; ++index)
    {
      if (StaticCards.Avatar.AvatarIndexes[index].race == race && StaticCards.Avatar.AvatarIndexes[index].sex == sex)
        return index;
    }
    return -1;
  }

  private static bool PropIsRandom(string prop)
  {
    return new List<string>().Contains(prop);
  }

  private static int SetRandom(string race, string sex, string prop)
  {
    return Random.Range(0, AvatarSelector.GetPropertyCount(race, sex, prop) - 1);
  }

  private static int GetIndexOfItemEmpty(string race, string sex, string item)
  {
    int avatarIndex = AvatarSelector.GetAvatarIndex(race, sex);
    if (avatarIndex == -1)
      return 0;
    int propertyCount = AvatarSelector.GetPropertyCount(race, sex, item);
    for (int index = 0; index < propertyCount; ++index)
    {
      if (AvatarSelector.GetItem(avatarIndex, item, index).Contains("_empty"))
        return index;
    }
    return 0;
  }

  public static Avatar Create(string race, string sex, Vector3 position, Quaternion rotation)
  {
    Dictionary<string, string> properties = new Dictionary<string, string>();
    int avatarIndex = AvatarSelector.GetAvatarIndex(race, sex);
    if (avatarIndex == -1)
      return (Avatar) null;
    using (List<string>.Enumerator enumerator = AvatarInfo.GetItemsOfRace(race).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        int index = 0;
        if (AvatarSelector.PropIsRandom(current))
          index = AvatarSelector.SetRandom(race, sex, current);
        string str = AvatarSelector.GetItem(avatarIndex, current, index);
        if (str.Length > 0)
          properties[current] = str;
      }
    }
    if (race == "human")
    {
      using (List<string>.Enumerator enumerator = AvatarInfo.GetTexturesOfRace(race).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          int index = 0;
          if (AvatarSelector.PropIsRandom(current))
            index = AvatarSelector.SetRandom(race, sex, current);
          string texture = AvatarSelector.GetTexture(avatarIndex, current, index);
          if (texture.Length > 0)
            properties[current] = texture;
        }
      }
    }
    if (race != "cylon")
    {
      using (List<string>.Enumerator enumerator = AvatarInfo.GetMaterialsOfRace(race).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          int index1 = 0;
          if (AvatarSelector.PropIsRandom(current))
            index1 = AvatarSelector.SetRandom(race, sex, current);
          string itemFromMaterial = AvatarInfo.GetItemFromMaterial(current);
          int index2 = 0;
          string material = AvatarSelector.GetMaterial(avatarIndex, current, AvatarSelector.GetItem(avatarIndex, itemFromMaterial, index2), index1);
          if (material.Length > 0)
            properties[current] = material;
        }
      }
    }
    if (race == "human")
    {
      string str = string.Empty;
      int index1 = 2;
      string index2 = "hair";
      properties[index2] = AvatarSelector.GetItem(avatarIndex, index2, index1);
      string index3 = "beard";
      properties[index3] = AvatarSelector.GetItem(avatarIndex, index3, AvatarSelector.GetIndexOfItemEmpty(race, sex, index3));
      string index4 = "helmet";
      properties[index4] = AvatarSelector.GetItem(avatarIndex, index4, AvatarSelector.GetIndexOfItemEmpty(race, sex, index4));
      string index5 = "glasses";
      properties[index5] = AvatarSelector.GetItem(avatarIndex, index5, AvatarSelector.GetIndexOfItemEmpty(race, sex, index5));
      string index6 = "head";
      properties[index6] = AvatarSelector.GetItem(avatarIndex, index6, 6);
      int index7 = 3;
      string textureFromItem1 = AvatarInfo.GetTextureFromItem("faces");
      properties[textureFromItem1] = AvatarSelector.GetTexture(avatarIndex, textureFromItem1, index7);
      string textureFromItem2 = AvatarInfo.GetTextureFromItem("hands");
      properties[textureFromItem2] = AvatarSelector.GetTexture(avatarIndex, textureFromItem2, index7);
      int index8 = 4;
      string materialFromItem1 = AvatarInfo.GetMaterialFromItem("hair");
      properties[materialFromItem1] = AvatarSelector.GetMaterial(avatarIndex, materialFromItem1, AvatarSelector.GetItem(avatarIndex, AvatarInfo.GetItemFromMaterial(materialFromItem1), index1), index8);
      string materialFromItem2 = AvatarInfo.GetMaterialFromItem("beard");
      properties[materialFromItem2] = AvatarSelector.GetMaterial(avatarIndex, materialFromItem2, string.Empty, index8);
    }
    return AvatarSelector.Create(race, sex, properties, position, rotation);
  }

  public static Avatar Create(string race, string sex)
  {
    return AvatarSelector.Create(race, sex, Vector3.zero, Quaternion.identity);
  }

  public static Avatar Create(string race, string sex, Dictionary<string, string> properties, Vector3 position, Quaternion rotation)
  {
    Avatar avatar1 = (Avatar) null;
    List<AvatarProperty> properties1 = new List<AvatarProperty>();
    using (Dictionary<string, string>.Enumerator enumerator = properties.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        AvatarProperty avatarProperty = new AvatarProperty(current.Key, current.Value);
        properties1.Add(avatarProperty);
      }
    }
    string key = race;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AvatarSelector.\u003C\u003Ef__switch\u0024map4 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AvatarSelector.\u003C\u003Ef__switch\u0024map4 = new Dictionary<string, int>(2)
        {
          {
            "human",
            0
          },
          {
            "cylon",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AvatarSelector.\u003C\u003Ef__switch\u0024map4.TryGetValue(key, out num))
      {
        if (num != 0)
        {
          if (num == 1)
          {
            Avatar avatar2 = (Avatar) new AvatarCylon();
            avatar2.Init(race, sex, position, rotation);
            avatar2.Lock();
            avatar2.SetProperties(properties1);
            avatar2.Unlock();
            return avatar2;
          }
        }
        else
        {
          Avatar avatar2 = (Avatar) new AvatarHuman();
          avatar2.Init(race, sex, position, rotation);
          avatar2.Lock();
          avatar2.SetProperties(properties1);
          avatar2.Unlock();
          return avatar2;
        }
      }
    }
    return avatar1;
  }

  public static Avatar Create(string race, string sex, Dictionary<string, string> properties)
  {
    return AvatarSelector.Create(race, sex, properties, Vector3.zero, Quaternion.identity);
  }

  public static Avatar Create(AvatarItems avatarDesc, Vector3 position, Quaternion rotation)
  {
    Dictionary<string, string> properties = new Dictionary<string, string>();
    string str = avatarDesc.GetItem(AvatarItem.Race);
    if (str == "human")
    {
      properties.Add("helmet", avatarDesc.GetItem(AvatarItem.HumanHelmet));
      properties.Add("beard", avatarDesc.GetItem(AvatarItem.HumanBeard));
      properties.Add(AvatarInfo.GetTextureFromItem("faces"), avatarDesc.GetItem(AvatarItem.HumanFace));
      properties.Add(AvatarInfo.GetTextureFromItem("hands"), avatarDesc.GetItem(AvatarItem.HumanHands));
      properties.Add("glasses", avatarDesc.GetItem(AvatarItem.HumanGlasses));
      properties.Add(AvatarInfo.GetMaterialFromItem("hair"), avatarDesc.GetItem(AvatarItem.HumanHairColor));
      properties.Add("hair", avatarDesc.GetItem(AvatarItem.HumanHair));
      properties.Add("suit", avatarDesc.GetItem(AvatarItem.HumanSuit));
      properties.Add("head", avatarDesc.GetItem(AvatarItem.HumanHead));
      properties.Add(AvatarInfo.GetMaterialFromItem("beard"), avatarDesc.GetItem(AvatarItem.HumanBeardColor));
    }
    else if (str == "cylon")
    {
      properties.Add("head", avatarDesc.GetItem(AvatarItem.CylonHead));
      properties.Add(AvatarInfo.GetMaterialFromItem("head"), avatarDesc.GetItem(AvatarItem.CylonHeadSkin));
      properties.Add("body", avatarDesc.GetItem(AvatarItem.CylonBody));
      properties.Add(AvatarInfo.GetMaterialFromItem("body"), avatarDesc.GetItem(AvatarItem.CylonBodySkin));
      properties.Add("arms", avatarDesc.GetItem(AvatarItem.CylonArms));
      properties.Add(AvatarInfo.GetMaterialFromItem("arms"), avatarDesc.GetItem(AvatarItem.CylonArmsSkin));
      properties.Add("legs", avatarDesc.GetItem(AvatarItem.CylonLegs));
      properties.Add(AvatarInfo.GetMaterialFromItem("legs"), avatarDesc.GetItem(AvatarItem.CylonLegsSkin));
    }
    return AvatarSelector.Create(avatarDesc.GetItem(AvatarItem.Race), avatarDesc.GetItem(AvatarItem.Sex), properties, position, rotation);
  }

  public static AvatarDescription GetDefault(Faction faction)
  {
    string str1 = faction != Faction.Cylon ? "human" : "cylon";
    string str2 = faction != Faction.Cylon ? "male" : "centurion";
    AvatarDescription avatarDescription = new AvatarDescription();
    avatarDescription.SetItem(AvatarItem.Sex, str2);
    avatarDescription.SetItem(AvatarItem.Race, str1);
    if (faction == Faction.Colonial)
    {
      avatarDescription.SetItem(AvatarItem.HumanBeardColor, string.Empty);
      avatarDescription.SetItem(AvatarItem.HumanBeard, "volume_beard_empty");
      avatarDescription.SetItem(AvatarItem.HumanSuit, "male_suit_01");
      avatarDescription.SetItem(AvatarItem.HumanHairColor, "male_hair_03_5.mat");
      avatarDescription.SetItem(AvatarItem.HumanHair, "male_hair_03");
      avatarDescription.SetItem(AvatarItem.HumanHelmet, "helmet_empty");
      avatarDescription.SetItem(AvatarItem.HumanGlasses, "glasses_empty");
      avatarDescription.SetItem(AvatarItem.HumanHands, "male_hands4.png");
      avatarDescription.SetItem(AvatarItem.HumanFace, "male_face_4.tga");
      avatarDescription.SetItem(AvatarItem.HumanHead, "male_head_08");
    }
    else if (str1 == "cylon")
    {
      avatarDescription.SetItem(AvatarItem.CylonLegsSkin, string.Empty);
      avatarDescription.SetItem(AvatarItem.CylonLegs, "centurion_legs_v1");
      avatarDescription.SetItem(AvatarItem.CylonBodySkin, string.Empty);
      avatarDescription.SetItem(AvatarItem.CylonBody, "centurion_body_v1");
      avatarDescription.SetItem(AvatarItem.CylonArmsSkin, string.Empty);
      avatarDescription.SetItem(AvatarItem.CylonArms, "centurion_arms_v1");
      avatarDescription.SetItem(AvatarItem.CylonHeadSkin, string.Empty);
      avatarDescription.SetItem(AvatarItem.CylonHead, "centurion_head_v1");
    }
    return avatarDescription;
  }

  public static Avatar Create(AvatarItems avatarDesc)
  {
    return AvatarSelector.Create(avatarDesc, Vector3.zero, Quaternion.identity);
  }

  public static void ClearAvatar(Avatar avatar)
  {
    if (avatar == null || (Object) avatar.obj == (Object) null)
      return;
    NormalCharacterMotor component1 = avatar.obj.GetComponent<NormalCharacterMotor>();
    if ((bool) ((Object) component1))
      component1.enabled = false;
    CharacterController component2 = avatar.obj.GetComponent<CharacterController>();
    if ((bool) ((Object) component2))
      component2.isTrigger = true;
    avatar.obj.GetComponent<Animation>().Play("idle");
  }
}
