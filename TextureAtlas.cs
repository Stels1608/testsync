﻿// Decompiled with JetBrains decompiler
// Type: TextureAtlas
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class TextureAtlas
{
  protected Rect frameRect;
  protected uint width;
  protected uint height;
  protected Texture2D texture;

  public Texture2D Texture
  {
    get
    {
      return this.texture;
    }
  }

  public float2 ElementSize
  {
    get
    {
      return new float2((float) ((long) this.texture.width / (long) this.width), (float) ((long) this.texture.height / (long) this.height));
    }
  }

  public TextureAtlas(Texture2D texture, uint width, uint height)
  {
    if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
      throw new ArgumentNullException();
    if ((int) width == 0)
      throw new ArgumentOutOfRangeException();
    if ((int) height == 0)
      throw new ArgumentOutOfRangeException();
    this.texture = texture;
    this.width = width;
    this.height = height;
    this.frameRect = new Rect(0.0f, 0.0f, 1f / (float) width, 1f / (float) height);
  }

  public Rect GetFrameRectByIndex(uint index)
  {
    if (index > (uint) ((int) this.width * (int) this.height - 1))
      throw new ArgumentOutOfRangeException();
    Vector2 vector2 = new Vector2();
    vector2.y = (float) (index / this.width);
    vector2.x = (float) index - vector2.y * (float) this.width;
    this.frameRect.x = vector2.x / (float) this.width;
    this.frameRect.y = (float) (1.0 - ((double) vector2.y + 1.0) / (double) this.height);
    return this.frameRect;
  }
}
