﻿// Decompiled with JetBrains decompiler
// Type: Comet
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Comet : SpaceObject
{
  public MissileCard MissileCard;

  public override bool IsHostileCongener
  {
    get
    {
      return true;
    }
  }

  public Comet(uint objectID)
    : base(objectID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.MissileCard = (MissileCard) Game.Catalogue.FetchCard(this.objectGUID, CardView.Missile);
    this.AreCardsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.MissileCard
    });
    this.IsLoaded.AddHandler(new SignalHandler(this.StartEmission));
  }

  protected override IMovementController CreateMovementController()
  {
    return (IMovementController) new ManeuverController((SpaceObject) this, (MovementCard) Game.Catalogue.FetchCard(this.WorldCard.CardGUID, CardView.Movement));
  }

  private void StartEmission()
  {
    this.Model.GetComponent<ParticleSystem>().Play(true);
  }

  public override void Remove(RemovingCause removingCause)
  {
    if ((Object) this.Model == (Object) null || removingCause == RemovingCause.TTL)
    {
      base.Remove(removingCause);
    }
    else
    {
      this.UnSubscribe();
      if ((Object) SpaceLevel.GetLevel() != (Object) null)
        SpaceLevel.GetLevel().PaintedTargets.Remove((SpaceObject) this);
      this.dead = true;
      foreach (ParticleSystem componentsInChild in this.Model.GetComponentsInChildren<ParticleSystem>())
        componentsInChild.enableEmission = false;
      foreach (Component componentsInChild in this.Model.GetComponentsInChildren<MeshRenderer>())
        Object.Destroy((Object) componentsInChild.gameObject);
      this.Model.GetComponent<AudioSource>().enabled = false;
      this.Model.GetComponentInChildren<CometExplosion>().Explode();
      if (!((Object) this.Root != (Object) null))
        return;
      Object.Destroy((Object) this.Root.gameObject, 10f);
    }
  }
}
