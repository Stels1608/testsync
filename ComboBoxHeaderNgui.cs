﻿// Decompiled with JetBrains decompiler
// Type: ComboBoxHeaderNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ComboBoxHeaderNgui : ComboBoxHeaderUi
{
  [SerializeField]
  private UILabel headerLabel;
  [SerializeField]
  private UI2DSprite buttonCollapseImage;
  [SerializeField]
  private UnityEngine.Sprite collapsedImageSrc;
  [SerializeField]
  private UnityEngine.Sprite uncollapsedImageSrc;

  public override void SetHeaderText(string headerText)
  {
    this.headerLabel.text = headerText;
  }

  public override void SetButtonImageCollapsed()
  {
    this.buttonCollapseImage.sprite2D = this.collapsedImageSrc;
  }

  public override void SetButtonImageUncollapsed()
  {
    this.buttonCollapseImage.sprite2D = this.uncollapsedImageSrc;
  }
}
