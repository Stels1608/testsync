﻿// Decompiled with JetBrains decompiler
// Type: GuiCombatText
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GuiCombatText : GuiPanel
{
  private readonly Queue<AbstractCombatMessage> combatMessages = new Queue<AbstractCombatMessage>();

  public bool ShowCombatText { get; set; }

  public GuiCombatText()
  {
    Camera.main.gameObject.AddComponent<GUILayer>();
  }

  public void AddMessage(AbstractCombatMessage combatMessage)
  {
    if (!this.ShowCombatText)
      combatMessage.Dispose();
    else
      this.combatMessages.Enqueue(combatMessage);
  }

  public override void Draw()
  {
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    using (Queue<AbstractCombatMessage>.Enumerator enumerator = this.combatMessages.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Update(Time.deltaTime);
    }
    while (this.combatMessages.Count > 0 && this.combatMessages.Peek().Expired)
      this.combatMessages.Dequeue().Dispose();
  }
}
