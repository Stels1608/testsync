﻿// Decompiled with JetBrains decompiler
// Type: SpaceCameraMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SpaceCameraMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "SpaceCameraMediator";
  private CameraModeWindow cameraModeWindow;

  public CameraModeWindow CameraModeWindow
  {
    get
    {
      return this.cameraModeWindow;
    }
    set
    {
      this.cameraModeWindow = value;
    }
  }

  public SpaceCameraMediator()
    : base("SpaceCameraMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.UndockFromPlayerShipReply);
    this.AddMessageInterest(Message.DockAtPlayerShipReply);
    this.AddMessageInterest(Message.CameraBehaviorChanged);
    this.AddMessageInterest(Message.SettingChangedCameraZoom);
    this.AddMessageInterest(Message.SettingChangedCameraMode);
    this.AddMessageInterest(Message.LevelStarted);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.ToggleOldUi);
    this.AddMessageInterest(Message.SelectedTargetChanged);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.LoadNewLevel:
        this.CameraModeWindow.IsRendered = false;
        break;
      case Message.LevelStarted:
        GameLevel gameLevel = (GameLevel) message.Data;
        this.CameraModeWindow.gameObject.SetActive(gameLevel is SpaceLevel);
        if (!(gameLevel is SpaceLevel))
          break;
        this.CameraModeWindow.SetSupportedCameraModes(SpaceLevel.GetLevel().ShipRoleSpecificsFactory.GetSupportedCameraModes());
        this.SetCameraMode(((SettingsDataProvider) this.OwnerFacade.FetchDataProvider("SettingsDataProvider")).CurrentSettings.CameraMode);
        Game.InputDispatcher.AddListener((InputListener) this.CameraModeWindow);
        this.SendMessage(Message.AddToCombatGui, (object) this.cameraModeWindow);
        break;
      default:
        if (id != Message.SettingChangedCameraMode)
        {
          if (id != Message.SettingChangedCameraZoom)
          {
            if (id != Message.DockAtPlayerShipReply)
            {
              if (id != Message.UndockFromPlayerShipReply)
              {
                if (id != Message.UiCreated)
                {
                  if (id != Message.ToggleOldUi)
                  {
                    if (id != Message.SelectedTargetChanged)
                    {
                      if (id != Message.CameraBehaviorChanged)
                        break;
                      Game.InputDispatcher.RemoveAllListeners((Predicate<InputListener>) (listener => listener is ISpaceCameraBehavior));
                      Game.InputDispatcher.AddListener((InputListener) ((SpaceCameraBehaviorChangeMessage) message).NewBehavior);
                      break;
                    }
                    SpaceLevel.GetLevel().SpaceCamera.OnTargetChanged(((KeyValuePair<ISpaceEntity, ISpaceEntity>) message.Data).Value);
                    break;
                  }
                  if ((bool) message.Data || !((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null))
                    break;
                  SpaceLevel.GetLevel().SpaceCamera.OnMouseUp(float2.zero, KeyCode.Mouse0);
                  SpaceLevel.GetLevel().SpaceCamera.OnMouseUp(float2.zero, KeyCode.Mouse1);
                  break;
                }
                this.CreateCameraMenu();
                break;
              }
              SpaceLevel.GetLevel().SpaceCamera.SetCameraCard(Game.Me.ActualShip.CameraCard);
              break;
            }
            // ISSUE: object of a compiler-generated type is created
            // ISSUE: variable of a compiler-generated type
            SpaceCameraMediator.\u003CReceiveMessage\u003Ec__AnonStorey5C messageCAnonStorey5C = new SpaceCameraMediator.\u003CReceiveMessage\u003Ec__AnonStorey5C();
            // ISSUE: reference to a compiler-generated field
            messageCAnonStorey5C.anchorship = message.Data as PlayerShip;
            // ISSUE: reference to a compiler-generated field
            if (messageCAnonStorey5C.anchorship != null)
            {
              // ISSUE: reference to a compiler-generated field
              // ISSUE: reference to a compiler-generated method
              messageCAnonStorey5C.anchorship.CameraCard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStorey5C.\u003C\u003Em__25));
              break;
            }
            Debug.LogError((object) "Anchorship is null!");
            break;
          }
          SpaceCameraBase.ZoomPercentage = (float) message.Data;
          break;
        }
        this.SetCameraMode((SpaceCameraBase.CameraMode) message.Data);
        break;
    }
  }

  private void SetCameraMode(SpaceCameraBase.CameraMode cameraMode)
  {
    if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null && (UnityEngine.Object) SpaceLevel.GetLevel().SpaceCamera != (UnityEngine.Object) null)
      SpaceLevel.GetLevel().SpaceCamera.SetCameraMode(cameraMode);
    this.CameraModeWindow.OnCameraModeChanged(cameraMode);
  }

  private void CreateCameraMenu()
  {
    this.CameraModeWindow = UguiTools.CreateChild<CameraModeWindow>("CameraMenu", UguiTools.GetCanvas(BsgoCanvas.Hud).transform);
    this.CameraModeWindow.IsRendered = false;
  }
}
