﻿// Decompiled with JetBrains decompiler
// Type: BattleValidator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BattleValidator
{
  public bool IsReady()
  {
    return this.WaitForBasestars();
  }

  private bool WaitForBasestars()
  {
    if ((Object) SpaceLevel.GetLevel() != (Object) null)
    {
      foreach (Ship ship in SpaceLevel.GetLevel().GetObjectRegistry().GetLoaded<CruiserShip>())
      {
        if ((int) ship.ShipCardLight.Tier == 4 && ship.WorldCard.PrefabName.CompareTo("basestar") == 0)
          return true;
      }
    }
    return false;
  }
}
