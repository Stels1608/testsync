﻿// Decompiled with JetBrains decompiler
// Type: TutorialMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "TutorialMediator";
  public HelpContent help;

  public TutorialMediator()
    : base("TutorialMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.RegisterHelpContent);
    this.AddMessageInterest(Message.SettingChangedCompletedTutorials);
    this.AddMessageInterest(Message.SettingChangedShowTutorials);
    this.AddMessageInterest(Message.SettingChangedShowPopups);
    this.AddMessageInterest(Message.LevelLoaded);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.SettingChangedShowTutorials:
        if (!((Object) this.help != (Object) null))
          break;
        this.help.ShowTutorial = (bool) message.Data;
        break;
      case Message.SettingChangedCompletedTutorials:
        if (!((Object) this.help != (Object) null))
          break;
        this.help.CompletedTutorials = (List<HelpScreenType>) message.Data;
        break;
      case Message.SettingChangedShowPopups:
        PopupTutorialTipManager.Instance.ShowPopupTips = (bool) message.Data;
        break;
      case Message.LevelLoaded:
        this.ImportSettings((this.OwnerFacade.FetchDataProvider("SettingsDataProvider") as SettingsDataProvider).CurrentSettings);
        break;
      case Message.RegisterHelpContent:
        this.help = (HelpContent) message.Data;
        break;
    }
  }

  private void ImportSettings(UserSettings settings)
  {
    if ((Object) this.help == (Object) null)
      return;
    this.help.CompletedTutorials = settings.CompletedTutorials;
    this.help.ShowTutorial = settings.ShowTutorial;
    PopupTutorialTipManager.Instance.ShowPopupTips = settings.ShowPopups;
  }
}
