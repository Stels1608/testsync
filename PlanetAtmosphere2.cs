﻿// Decompiled with JetBrains decompiler
// Type: PlanetAtmosphere2
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class PlanetAtmosphere2 : MonoBehaviour
{
  public Color AtmosphereColor = new Color(0.1f, 0.2f, 0.4f, 0.0f);
  public Color AtmosphereEdgeColor = new Color(0.5f, 0.3f, 0.0f, 0.0f);
  public Color AtmosphereDawnColor = new Color(0.9f, 0.4f, 0.3f, 0.0f);
  private UpdateLimiter updateLimiter = new UpdateLimiter(25);
  public float SurfaceMeshRadius = 10f;
  public float AtmosphereMeshRadius = 10.25f;
  public float Intensity = 8f;
  public float Density = 1f;
  public float Illumination = 0.4f;
  public Transform SunPosition;
  public float AtmosphereColorBlend;
  public bool BackgroundPlanet;
  private float fCameraHeight;
  private float fCameraHeight2;
  private float fOuterRadius;
  private float fOuterRadius2;
  private float fInnerRadius;
  private float fInnerRadius2;
  private float fScaleDepth;
  private float baseScale;
  private float fScaledOuterRadius;

  private void LateUpdate()
  {
    if (!this.updateLimiter.CanUpdate() || (Object) SpaceLevel.GetLevel() == (Object) null || SpaceLevel.GetLevel().cameraSwitcher == null)
      return;
    Transform transform = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera().transform;
    if (this.BackgroundPlanet)
    {
      for (int index = 0; index < Camera.allCameras.Length; ++index)
      {
        if (Camera.allCameras[index].name == "BgCamera")
        {
          transform = Camera.allCameras[index].transform;
          break;
        }
      }
    }
    float num1 = 4f - Mathf.Min(4f, this.Density);
    float num2 = Mathf.Min(1f, Mathf.Abs(this.Illumination));
    this.transform.localScale = Vector3.one;
    this.baseScale = this.transform.lossyScale.x;
    this.fInnerRadius = this.SurfaceMeshRadius;
    this.fOuterRadius = this.AtmosphereMeshRadius;
    this.fScaledOuterRadius = this.fOuterRadius;
    Vector3 localScale = this.transform.localScale;
    this.fInnerRadius2 = this.fInnerRadius * this.fInnerRadius;
    this.fOuterRadius2 = this.fOuterRadius * this.fOuterRadius;
    this.fScaleDepth = this.fOuterRadius - this.fInnerRadius;
    this.fCameraHeight = (transform.position - this.transform.position).magnitude - this.fInnerRadius * this.baseScale;
    this.fCameraHeight2 = this.fCameraHeight * this.fCameraHeight;
    this.GetComponent<Renderer>().sharedMaterial.SetColor("_Color", this.AtmosphereColor);
    this.GetComponent<Renderer>().sharedMaterial.SetColor("_ColorEdge", this.AtmosphereEdgeColor);
    this.GetComponent<Renderer>().sharedMaterial.SetColor("_ColorDawn", this.AtmosphereDawnColor);
    Vector3 vector3 = Vector3.forward;
    if ((Object) this.SunPosition != (Object) null)
      vector3 = (this.transform.worldToLocalMatrix.MultiplyPoint(this.SunPosition.position) - this.transform.worldToLocalMatrix.MultiplyPoint(this.transform.position)).normalized;
    this.GetComponent<Renderer>().sharedMaterial.SetVector("v3CamPos", (Vector4) this.transform.worldToLocalMatrix.MultiplyPoint3x4(transform.position));
    this.GetComponent<Renderer>().sharedMaterial.SetVector("v3LightDir", (Vector4) vector3);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fIntensity", this.Intensity);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fDensity", num1);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fIllumination", num2);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fCameraHeight", this.fCameraHeight);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fCameraHeight2", this.fCameraHeight2);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fInnerRadius", this.fInnerRadius);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fInnerRadius2", this.fInnerRadius2);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fOuterRadius", this.fOuterRadius);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fOuterRadius2", this.fOuterRadius2);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fScaledOuterRadius", this.fScaledOuterRadius);
    this.GetComponent<Renderer>().sharedMaterial.SetFloat("fScaleDepth", this.fScaleDepth);
    this.transform.localScale = localScale;
  }
}
