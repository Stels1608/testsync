﻿// Decompiled with JetBrains decompiler
// Type: TargetSelector
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public class TargetSelector : InputListener
{
  private static readonly int ignoreRaycastLayer = LayerMask.NameToLayer("Ignore Raycast");
  private static readonly int filteredRaycastMask = ~(1 << TargetSelector.ignoreRaycastLayer);
  private ISpaceEntity targetOnMouseDown;
  private static int blockFrameCount;

  public static bool ShowShipNames { get; set; }

  public static bool ShowPlayerNames { get; set; }

  public bool HandleMouseInput
  {
    get
    {
      return true;
    }
  }

  public bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public bool HandleJoystickAxesInput
  {
    get
    {
      return true;
    }
  }

  private SpaceObject SelectAtMousePosition()
  {
    if (Game.Me.Ship == null)
      return (SpaceObject) null;
    if ((Object) Game.Me.Ship.Model == (Object) null)
      return (SpaceObject) null;
    if (GUIManager.MouseOverAnyGuiElement && HudIndicatorMediator.UseUguiBrackets)
      return (SpaceObject) null;
    int layer = Game.Me.Ship.Model.layer;
    Game.Me.Ship.Model.layer = TargetSelector.ignoreRaycastLayer;
    SpaceObject spaceObject = (SpaceObject) null;
    RaycastHit hitInfo;
    if (HudIndicatorMediator.UseUguiBrackets)
    {
      GameObject uiRaycastTarget = HudIndicatorManagerUgui.CurrentManager.GetUiRaycastTarget();
      if ((Object) uiRaycastTarget != (Object) null)
      {
        HudIndicatorBaseUgui componentInParent = uiRaycastTarget.GetComponentInParent<HudIndicatorBaseUgui>();
        if ((Object) componentInParent != (Object) null && componentInParent.Target is SpaceObject)
          spaceObject = this.SelectByHudIndicator((HudIndicatorBase) componentInParent);
      }
    }
    else if (UICamera.Raycast(Input.mousePosition))
    {
      hitInfo = UICamera.lastHit;
      HudIndicatorBracketBaseNgui component;
      if ((Object) (component = hitInfo.collider.gameObject.GetComponent<HudIndicatorBracketBaseNgui>()) != (Object) null)
        spaceObject = this.SelectByHudIndicator(component);
    }
    if (spaceObject == null && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 20000f, TargetSelector.filteredRaycastMask))
      spaceObject = this.SelectByModel(hitInfo.collider.gameObject);
    Game.Me.Ship.Model.layer = layer;
    if (spaceObject != null)
      spaceObject.OnClicked();
    return spaceObject;
  }

  private SpaceObject SelectByHudIndicator(HudIndicatorBase clickedIndicator)
  {
    return this.FilterTarget(clickedIndicator.Target as SpaceObject);
  }

  private SpaceObject SelectByHudIndicator(HudIndicatorBracketBaseNgui clickedIndicator)
  {
    return this.FilterTarget(clickedIndicator.Target as SpaceObject);
  }

  private SpaceObject FilterTarget(SpaceObject target)
  {
    if (target == null || target.IsCloaked && !target.IsInMapRange)
      return (SpaceObject) null;
    return target;
  }

  private SpaceObject SelectByModel(GameObject model)
  {
    SpaceObject spaceObject = (SpaceObject) null;
    RootScript component = model.transform.root.gameObject.GetComponent<RootScript>();
    if ((Object) component != (Object) null)
      spaceObject = component.SpaceObject;
    if (spaceObject != null && spaceObject != SpaceLevel.GetLevel().GetPlayerShip())
      return spaceObject;
    return (SpaceObject) null;
  }

  private void SelectAtScreenCenter()
  {
    ObjectRegistry objectRegistry = SpaceLevel.GetLevel().GetObjectRegistry();
    Vector3 from = Camera.main.gameObject.transform.rotation * Vector3.forward;
    Vector3 position = Camera.main.transform.position;
    float a = float.MaxValue;
    SpaceObject target = (SpaceObject) null;
    foreach (SpaceObject spaceObject in objectRegistry.GetAllLoaded())
    {
      Vector3 to = spaceObject.Position - position;
      float num = Vector3.Angle(from, to);
      if ((double) num < (double) Mathf.Min(a, 7.5f) && spaceObject.IsInMapRange)
      {
        a = num;
        target = spaceObject;
      }
    }
    if (target == null)
      return;
    TargetSelector.SelectTargetRequest(target, false);
    target.OnClicked();
  }

  private void SelectClosestEnemySpaceObject<T>() where T : SpaceObject
  {
    SpaceLevel level = SpaceLevel.GetLevel();
    if ((Object) level == (Object) null)
      return;
    T obj = (T) null;
    float num = 0.0f;
    Vector3 position = Game.Me.Ship.Position;
    List<T> objList = new List<T>();
    objList.AddRange((IEnumerable<T>) level.GetObjectRegistry().GetLoaded<T>());
    using (List<T>.Enumerator enumerator = objList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        T current = enumerator.Current;
        if (current.PlayerRelation != Relation.Friend)
        {
          float magnitude = (current.Position - position).magnitude;
          if ((object) obj == null)
          {
            obj = current;
            num = magnitude;
          }
          else if ((double) num > (double) magnitude)
          {
            obj = current;
            num = magnitude;
          }
        }
      }
    }
    if ((object) obj == null || !obj.IsInMapRange)
      return;
    TargetSelector.SelectTargetRequest((SpaceObject) obj, false);
  }

  public static void AutoTargetAfterAttack(SpaceObject target)
  {
    PlayerShip playerShip = target as PlayerShip;
    if (playerShip == null || target.SpaceEntityType == SpaceEntityType.Asteroid || (target.IsCloaked || !playerShip.IsVisible) || SpaceLevel.GetLevel().GetPlayerTarget() != null)
      return;
    TargetSelector.SelectTargetRequest(target, false);
  }

  public static void SelectTargetRequest(SpaceObject target, bool blockOtherRequestsInThisFrame = false)
  {
    if (TargetSelector.blockFrameCount == Time.frameCount)
      return;
    if (blockOtherRequestsInThisFrame)
      TargetSelector.blockFrameCount = Time.frameCount;
    FacadeFactory.GetInstance().SendMessage(Message.SelectTargetRequest, (object) target);
  }

  private void ToggleShowNames()
  {
    int num = TargetSelector.ShowShipNames ? 1 : (TargetSelector.ShowPlayerNames ? 1 : 0);
    bool flag1;
    bool flag2 = flag1 = num == 0;
    TargetSelector.ShowPlayerNames = flag2;
    TargetSelector.ShowShipNames = flag2;
    FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.HudIndicatorShowShipNames, (object) flag1, true));
    FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.HudIndicatorShowTargetNames, (object) flag1, true));
  }

  public bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey != KeyCode.Mouse0)
      return false;
    SpaceObject target = this.SelectAtMousePosition();
    if (target != null && target == this.targetOnMouseDown)
      TargetSelector.SelectTargetRequest(target, false);
    return false;
  }

  public bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey != KeyCode.Mouse0)
      return false;
    this.targetOnMouseDown = (ISpaceEntity) this.SelectAtMousePosition();
    return false;
  }

  public void OnMouseMove(float2 mousePosition)
  {
  }

  public bool OnMouseScrollDown()
  {
    return false;
  }

  public bool OnMouseScrollUp()
  {
    return false;
  }

  public bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    switch (action)
    {
      case Action.SelectNearestMissile:
        this.SelectClosestEnemySpaceObject<Missile>();
        break;
      case Action.ToggleShipName:
        this.ToggleShowNames();
        break;
      case Action.SelectNearestMine:
        this.SelectClosestEnemySpaceObject<Mine>();
        break;
      case Action.SelectScreenCenterObject:
        this.SelectAtScreenCenter();
        break;
    }
    return false;
  }

  public bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    return false;
  }

  public bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    return false;
  }
}
