﻿// Decompiled with JetBrains decompiler
// Type: IMotionAnalyzer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public abstract class IMotionAnalyzer
{
  public string motionGroup = "locomotion";
  [HideInInspector]
  public string name;
  public AnimationClip animation;
  public MotionType motionType;

  public abstract int samples { get; }

  public abstract LegCycleData[] cycles { get; }

  public abstract Vector3 cycleDirection { get; }

  public abstract float cycleDistance { get; }

  public abstract Vector3 cycleVector { get; }

  public abstract float cycleDuration { get; }

  public abstract float cycleSpeed { get; }

  public abstract Vector3 cycleVelocity { get; }

  public abstract float cycleOffset { get; set; }

  public abstract Vector3 GetFlightFootPosition(int leg, float flightTime, int phase);

  public abstract void Analyze(GameObject o);
}
