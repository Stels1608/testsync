﻿// Decompiled with JetBrains decompiler
// Type: ChangeSettingMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class ChangeSettingMessage : IMessage<Message>
{
  private readonly UserSetting setting;
  private readonly object value;
  private readonly bool save;

  public Message Id
  {
    get
    {
      return Message.ChangeSetting;
    }
  }

  public object Data
  {
    get
    {
      return this.value;
    }
    set
    {
    }
  }

  public UserSetting Setting
  {
    get
    {
      return this.setting;
    }
  }

  public bool Save
  {
    get
    {
      return this.save;
    }
  }

  public ChangeSettingMessage(UserSetting setting, object value, bool save = false)
  {
    this.setting = setting;
    this.value = value;
    this.save = save;
  }
}
