﻿// Decompiled with JetBrains decompiler
// Type: DebugCardInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class DebugCardInfo : DebugBehaviour<DebugCardInfo>
{
  private Vector2 scroll = new Vector2();
  private string info = "empty";
  private int oldCount;

  private void Start()
  {
    this.windowID = 5;
    this.windowRect = new Rect((float) ((Screen.width - 300) / 2), (float) ((Screen.height - 300) / 2), 300f, 300f);
  }

  protected override void WindowFunc()
  {
    TextAnchor alignment = GUI.skin.box.alignment;
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.Box(this.info);
    GUILayout.EndScrollView();
    GUI.skin.box.alignment = alignment;
  }

  private void Update()
  {
    Dictionary<long, Card> debugCache = Game.Catalogue.DebugCache;
    if (this.oldCount == debugCache.Count)
      return;
    int num = 0;
    StringBuilder stringBuilder = new StringBuilder();
    Dictionary<CardView, int> dictionary1 = new Dictionary<CardView, int>();
    using (Dictionary<long, Card>.Enumerator enumerator = debugCache.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<long, Card> current = enumerator.Current;
        CardView key = (CardView) (current.Key >> 32);
        if (!dictionary1.ContainsKey(key))
          dictionary1.Add(key, 0);
        Dictionary<CardView, int> dictionary2;
        CardView index;
        (dictionary2 = dictionary1)[index = key] = dictionary2[index] + 1;
        if ((bool) current.Value.IsLoaded)
          ++num;
      }
    }
    using (Dictionary<CardView, int>.Enumerator enumerator = dictionary1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<CardView, int> current = enumerator.Current;
        stringBuilder.AppendFormat("{0} = {1}\n", (object) current.Key.ToString(), (object) current.Value);
      }
    }
    stringBuilder.AppendFormat("Total: {0}/{1}", (object) num, (object) debugCache.Count);
    this.info = stringBuilder.ToString();
  }
}
