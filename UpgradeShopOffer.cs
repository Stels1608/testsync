﻿// Decompiled with JetBrains decompiler
// Type: UpgradeShopOffer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

internal class UpgradeShopOffer : AbstractShopOffer
{
  private List<CategoryLevels> categoryLevels;

  private UpgradeShopOffer(int percentage, int expiry, List<CategoryLevels> categoryLevels)
    : base(percentage, expiry)
  {
    if (categoryLevels.Count == 0)
      throw new ArgumentException("You must specify at least one caetory with levels to be discounted.", "categoryLevels");
    this.categoryLevels = categoryLevels;
  }

  public override string ToString()
  {
    return string.Format("CategoryLevels: {0}", (object) this.categoryLevels);
  }

  public static UpgradeShopOffer Decode(BgoProtocolReader input)
  {
    List<CategoryLevels> categoryLevels = new List<CategoryLevels>();
    int percentage = (int) input.ReadByte();
    int expiry = input.ReadInt32();
    for (int index1 = input.ReadLength(); index1 > 0; --index1)
    {
      ShopItemType category = (ShopItemType) input.ReadByte();
      HashSet<int> levels = new HashSet<int>();
      for (int index2 = input.ReadLength(); index2 > 0; --index2)
      {
        int num = input.ReadInt32();
        levels.Add(num);
      }
      categoryLevels.Add(new CategoryLevels(category, levels));
    }
    return new UpgradeShopOffer(percentage, expiry, categoryLevels);
  }

  public ShopDiscount FindDiscount(ShopItemType category, int level)
  {
    if (this.IsDiscounted(category, level))
      return this.Discount;
    return (ShopDiscount) null;
  }

  public bool IsDiscounted(ShopItemType category, int level)
  {
    if (this.Discount.HasExpired())
      return false;
    using (List<CategoryLevels>.Enumerator enumerator = this.categoryLevels.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CategoryLevels current = enumerator.Current;
        if (category == current.Category() && current.Levels().Contains(level))
          return true;
      }
    }
    return false;
  }
}
