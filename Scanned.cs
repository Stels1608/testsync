﻿// Decompiled with JetBrains decompiler
// Type: Scanned
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Scanned : SingleLODListener
{
  private const float scanningTime = 5f;
  public Scanned.ScanningCompletedCallback Complete;
  private GameObject scannedClone;
  private ScannerScript scannerScript;
  private Renderer _renderer;
  private Color? scanColor;
  private bool isBeingScanned;

  public void BeginScan(ItemCountable resource, SpaceObject scanningObject, Color scanColor)
  {
    if (this.isBeingScanned)
      return;
    if ((Object) this._renderer != (Object) null)
    {
      this.scannedClone = Object.Instantiate((Object) this._renderer.gameObject, this._renderer.transform.position, this._renderer.transform.rotation) as GameObject;
      if ((Object) null == (Object) this.scannedClone)
      {
        Debug.LogError((object) "Scanned clone is NULL");
        return;
      }
      this.scannedClone.transform.parent = this.transform;
      this.scannedClone.transform.localScale = Vector3.one * 1.01f;
      Material material = new Material(SpaceExposer.GetInstance().scanMaterial);
      if (this._renderer.transform.childCount > 0)
      {
        Renderer[] componentsInChildren = this.scannedClone.transform.GetComponentsInChildren<Renderer>();
        for (int index = 0; index <= componentsInChildren.Length - 1; ++index)
        {
          if (componentsInChildren[index].material.HasProperty("_BumpMap"))
            material.SetTexture("_BumpMap", componentsInChildren[index].material.GetTexture("_BumpMap"));
          componentsInChildren[index].material = material;
        }
      }
      else
      {
        if (this._renderer.material.HasProperty("_BumpMap"))
          material.SetTexture("_BumpMap", this._renderer.material.GetTexture("_BumpMap"));
        this.scannedClone.GetComponent<Renderer>().material = material;
      }
      this.scannerScript = new GameObject().AddComponent<ScannerScript>();
      this.scannerScript.ScannedObject = this.transform;
      this.scannerScript.ScanningObject = scanningObject;
      float radius = Mathf.Max(Mathf.Max(this.GetComponent<Collider>().bounds.size.x, this.GetComponent<Collider>().bounds.size.y), this.GetComponent<Collider>().bounds.size.z);
      this.scannerScript.Distance = radius * 2f;
      this.AddScanningEffect(this.scannedClone, scanColor, radius);
      if (this.scannedClone.transform.childCount > 0)
      {
        for (int index = 0; index <= this.scannedClone.transform.childCount - 1; ++index)
          this.AddScanningEffect(this.scannedClone.transform.GetChild(index).gameObject, scanColor, radius);
      }
    }
    this.scanColor = resource == null || resource.Count <= 0U ? new Color?(new Color(0.5f, 0.0f, 0.0f)) : new Color?(scanColor);
    this.isBeingScanned = true;
    this.Invoke("OnScanningCompleted", 5f);
  }

  private void AddScanningEffect(GameObject sClone, Color scanColor, float radius)
  {
    ScannerEffect scannerEffect = sClone.AddComponent<ScannerEffect>();
    scannerEffect.radius = radius;
    scannerEffect.scannerPosition = this.scannerScript.transform;
    scannerEffect.waveColor = scanColor;
  }

  private void OnDisable()
  {
    if ((Object) this.scannedClone != (Object) null)
      Object.Destroy((Object) this.scannedClone);
    if (!((Object) this.scannerScript != (Object) null))
      return;
    Object.Destroy((Object) this.scannerScript.gameObject);
  }

  private void OnScanningCompleted()
  {
    if ((Object) this.scannedClone != (Object) null)
      Object.Destroy((Object) this.scannedClone);
    if ((Object) this.scannerScript != (Object) null)
      Object.Destroy((Object) this.scannerScript.gameObject);
    this.SetScanColor();
    if (this.Complete != null)
      this.Complete();
    this.Complete = (Scanned.ScanningCompletedCallback) null;
    this.isBeingScanned = false;
  }

  private void OnRendererNulled()
  {
    this._renderer = (Renderer) null;
  }

  private void OnRendererChanged(Renderer[] newRenderers)
  {
    this._renderer = newRenderers[0];
    this.SetScanColor();
  }

  private void SetScanColor()
  {
    if (!this.scanColor.HasValue || (Object) null == (Object) this._renderer)
      return;
    if (this._renderer.transform.childCount == 0)
    {
      this._renderer.material.color = this.scanColor.Value;
    }
    else
    {
      Renderer[] componentsInChildren = this._renderer.transform.GetComponentsInChildren<Renderer>();
      if (componentsInChildren.Length == 0)
        return;
      for (int index = 0; index <= componentsInChildren.Length - 1; ++index)
        componentsInChildren[index].material.color = this.scanColor.Value;
    }
  }

  public delegate void ScanningCompletedCallback();
}
