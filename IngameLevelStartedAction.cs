﻿// Decompiled with JetBrains decompiler
// Type: IngameLevelStartedAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;

public class IngameLevelStartedAction : Action<Message>
{
  private IngameLevelStartedCommandsDataProvider commandsDataProvider;

  public override void Execute(IMessage<Message> message)
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.commandsDataProvider = (IngameLevelStartedCommandsDataProvider) this.OwnerFacade.FetchDataProvider("GameLevelLoadedActionsDataProvider");
    using (List<IngameLevelStartedCommand>.Enumerator enumerator = this.commandsDataProvider.GameLevelLoadedCommands.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Execute();
    }
    this.commandsDataProvider.GameLevelLoadedCommands.Clear();
  }
}
