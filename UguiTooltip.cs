﻿// Decompiled with JetBrains decompiler
// Type: UguiTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Tooltips;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UguiTooltip : MonoBehaviour
{
  private GuiAdvancedTooltipBase tooltip;
  private EventTrigger eventTrigger;

  private void Start()
  {
    this.eventTrigger = this.gameObject.GetComponent<EventTrigger>() ?? this.gameObject.AddComponent<EventTrigger>();
    UguiTools.AddEventTrigger(this.eventTrigger, new UnityAction<BaseEventData>(this.OnPointerEnter), EventTriggerType.PointerEnter);
    UguiTools.AddEventTrigger(this.eventTrigger, new UnityAction<BaseEventData>(this.OnPointerExit), EventTriggerType.PointerExit);
  }

  public void SetTooltip(GuiAdvancedTooltipBase tooltip)
  {
    this.tooltip = tooltip;
    this.tooltip.HideManually = true;
  }

  private void HandleTooltip(bool isOver)
  {
    if (this.tooltip == null)
      return;
    if (isOver)
    {
      this.tooltip.Position = new Vector2(MouseSetup.MousePositionGui.x, MouseSetup.MousePositionGui.y);
      Game.TooltipManager.ShowTooltip(this.tooltip);
    }
    else
      Game.TooltipManager.HideTooltip(this.tooltip);
  }

  public void SetTooltip(string text)
  {
    this.SetTooltip((GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip(text));
  }

  private void OnPointerEnter(BaseEventData eventData)
  {
    this.HandleTooltip(true);
  }

  private void OnPointerExit(BaseEventData eventData)
  {
    this.HandleTooltip(false);
  }

  private void OnDisable()
  {
    this.HandleTooltip(false);
  }
}
