﻿// Decompiled with JetBrains decompiler
// Type: GuiBonuses
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GuiBonuses : GuiDialog
{
  private readonly GuiLabel bonusInfoLabel = new GuiLabel(Gui.Options.FontBGM_BT, 16);
  private readonly GuiPanelVerticalScroll scroll = new GuiPanelVerticalScroll();

  public GuiBonuses()
  {
    this.Size = new Vector2(925f, 443f);
    this.Position = new Vector2(0.0f, 0.0f);
    this.scroll.Size = new Vector2(400f, 390f);
    this.scroll.AutoSizeChildren = true;
    this.scroll.RenderDelimiters = true;
    this.scroll.RenderScrollLines = false;
    this.scroll.Space = 10f;
    this.AddChild((GuiElementBase) this.scroll, new Vector2(450f, 26f));
    GuiBonuses.Item obj = new GuiBonuses.Item();
    obj.SetFactor("%$bgo.pilot_log.source%", "%$bgo.pilot_log.factor%", "%$bgo.pilot_log.value%", "%$bgo.pilot_log.time%");
    this.scroll.AddChild((GuiElementBase) obj);
    this.bonusInfoLabel.Text = "%$bgo.pilot_log.bonuses_info%";
    this.bonusInfoLabel.WordWrap = true;
    this.bonusInfoLabel.AutoSize = false;
    this.bonusInfoLabel.Alignment = TextAnchor.MiddleCenter;
    this.bonusInfoLabel.Size = new Vector2(400f, 390f);
    this.AddChild((GuiElementBase) this.bonusInfoLabel, new Vector2(26f, 26f));
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    while (this.scroll.Children.Count - 1 > Game.Me.Factors.Count)
      this.scroll.RemoveChild(this.scroll.Children[this.scroll.Children.Count - 1]);
    while (this.scroll.Children.Count - 1 < Game.Me.Factors.Count)
      this.scroll.AddChild((GuiElementBase) new GuiBonuses.Item());
    int index = 1;
    foreach (Factor factor in (IEnumerable<Factor>) Game.Me.Factors)
    {
      (this.scroll.Children[index] as GuiBonuses.Item).SetFactor("%$bgo.bonus_factors.source." + factor.Source.ToString().ToLower() + "%", "%$bgo.bonus_factors.type." + factor.Type.ToString().ToLower() + "%", (factor.Value * 100f).ToString() + "%", Tools.FormatTime((float) factor.EndTime - (float) Game.TimeSync.ServerTime));
      ++index;
    }
  }

  public class Item : GuiPanel
  {
    private readonly GuiLabel source = new GuiLabel(Gui.Options.FontBGM_BT, 14);
    private readonly GuiLabel type = new GuiLabel(Gui.Options.FontBGM_BT, 14);
    private readonly GuiLabel value = new GuiLabel(Gui.Options.FontBGM_BT, 14);
    private readonly GuiLabel time = new GuiLabel(Gui.Options.FontBGM_BT, 14);

    public Item()
    {
      this.AddChild((GuiElementBase) this.source);
      this.AddChild((GuiElementBase) this.type, new Vector2(90f, 0.0f));
      this.type.AutoSize = false;
      this.type.SizeX = 235f;
      this.AddChild((GuiElementBase) this.value, new Vector2(335f, 0.0f));
      this.AddChild((GuiElementBase) this.time, Align.UpRight, new Vector2(70f, 0.0f));
    }

    public void SetFactor(string s, string t, string v, string strTime)
    {
      this.source.Text = s;
      this.type.Text = t;
      this.value.Text = v;
      this.time.Text = strTime;
      this.SizeY = this.source.SizeY;
    }
  }
}
