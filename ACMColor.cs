﻿// Decompiled with JetBrains decompiler
// Type: ACMColor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

internal class ACMColor
{
  private List<string> materials = new List<string>();
  public const int notContains = -1;

  public void Parse(List<string> materials)
  {
    this.materials.AddRange((IEnumerable<string>) materials);
  }

  public string GetMaterial(int number)
  {
    if (number < 0)
      number = 0;
    if (number > this.materials.Count - 1)
      number = this.materials.Count - 1;
    return this.materials[number];
  }

  public int GetMaterialNumber(string material)
  {
    for (int index = 0; index < this.materials.Count; ++index)
    {
      if (this.materials[index] == material)
        return index;
    }
    return -1;
  }

  public int GetMaterialsCount()
  {
    return this.materials.Count;
  }
}
