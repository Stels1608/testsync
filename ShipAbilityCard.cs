﻿// Decompiled with JetBrains decompiler
// Type: ShipAbilityCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ShipAbilityCard : Card
{
  public GUICard GUICard;
  public byte Level;
  public ShipAbilityLaunch Launch;
  public ShipAbilityAffect Affect;
  public HashSet<ShipAbilityTarget> Targets;
  public HashSet<ShipAbilityTargetTier> TargetTiers;
  public ushort ConsumableType;
  public uint ConsumableTier;
  public ShipConsumableOption ConsumableOption;
  public ObjectStats ItemBuffAdd;
  public ObjectStats ItemBuffMultiply;
  public uint AbilityGroupId;
  public string GUIBuffAtlas;
  public ushort GUIBuffIndex;
  public ObjectStats RemoteBuffMultiply;
  public ObjectStats RemoteBuffAdd;
  public ObjectStats ToggleSystemMultiply;
  public ObjectStats ToggleSystemAdd;
  public AbilityActionType ActionType;
  public AbilityActionType OverwriteActionType;
  public bool OnByDefault;
  public ConsumableEffectType[] effectTypeBlacklist;
  public List<AbilityActionType> AffectedAbilityTypes;

  public bool Auto
  {
    get
    {
      return this.Launch == ShipAbilityLaunch.Auto;
    }
  }

  public bool Countable
  {
    get
    {
      return this.ConsumableOption != ShipConsumableOption.NotUsing;
    }
  }

  public AbilityActionType BuffActionType
  {
    get
    {
      if (this.OverwriteActionType != AbilityActionType.None)
        return this.OverwriteActionType;
      return this.ActionType;
    }
  }

  public ShipAbilityCard(uint cardGUID)
    : base(cardGUID)
  {
    this.GUICard = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.GUICard
    });
  }

  public override string ToString()
  {
    return string.Format("Level: {0}, Launch: {1}, Affect: {2}, ConsumableType: {3}, ConsumableTier: {4}, ConsumableOption: {5}, AbilityGroupId: {6}, GuiBuffAtlas: {7}, GuiBuffIndex: {8}, ActionType: {9}, OnByDefault: {10}, EffectTypeBlacklist: {11}", (object) this.Level, (object) this.Launch, (object) this.Affect, (object) this.ConsumableType, (object) this.ConsumableTier, (object) this.ConsumableOption, (object) this.AbilityGroupId, (object) this.GUIBuffAtlas, (object) this.GUIBuffIndex, (object) this.ActionType, (object) this.OnByDefault, (object) this.effectTypeBlacklist);
  }

  public static ShipAbilityTargetTier TierToEnum(int tier)
  {
    return (ShipAbilityTargetTier) (1 << tier - 1);
  }

  public override void Read(BgoProtocolReader r)
  {
    this.Level = r.ReadByte();
    this.Launch = (ShipAbilityLaunch) r.ReadByte();
    this.Affect = (ShipAbilityAffect) r.ReadByte();
    this.AbilityGroupId = r.ReadUInt32();
    this.TargetTiers = r.ReadSet<ShipAbilityTargetTier>();
    this.ConsumableType = r.ReadUInt16();
    this.ConsumableTier = r.ReadUInt32();
    this.ConsumableOption = (ShipConsumableOption) r.ReadByte();
    if (this.ConsumableOption == ShipConsumableOption.Undefined)
      Debug.LogWarning((object) ("Undefined consumable option for " + this.GUICard.Name));
    this.ActionType = (AbilityActionType) r.ReadByte();
    this.OverwriteActionType = (AbilityActionType) r.ReadByte();
    this.GUIBuffAtlas = r.ReadString();
    this.GUIBuffIndex = r.ReadUInt16();
    this.ItemBuffAdd = r.ReadDesc<ObjectStats>();
    this.ItemBuffMultiply = r.ReadDesc<ObjectStats>();
    this.RemoteBuffAdd = r.ReadDesc<ObjectStats>();
    this.RemoteBuffMultiply = r.ReadDesc<ObjectStats>();
    this.ToggleSystemAdd = r.ReadDesc<ObjectStats>();
    this.ToggleSystemMultiply = r.ReadDesc<ObjectStats>();
    this.OnByDefault = r.ReadBoolean();
    int length = r.ReadLength();
    this.effectTypeBlacklist = new ConsumableEffectType[length];
    for (int index = 0; index < length; ++index)
      this.effectTypeBlacklist[index] = (ConsumableEffectType) r.ReadByte();
    int num = r.ReadLength();
    this.AffectedAbilityTypes = new List<AbilityActionType>();
    for (int index = 0; index < num; ++index)
      this.AffectedAbilityTypes.Add((AbilityActionType) r.ReadByte());
  }
}
