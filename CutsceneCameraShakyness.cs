﻿// Decompiled with JetBrains decompiler
// Type: CutsceneCameraShakyness
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CutsceneCameraShakyness : MonoBehaviour
{
  public float bigRadius = 3f;
  public float bigSpeed = 0.5f;
  public float smallRadius = 0.5f;
  public float smallSpeed = 2f;
  private Transform myTransform;
  private Vector3 localOrigin;
  private Quaternion localRotation;
  private CutsceneCameraShakyness.Noise noiseBigX;
  private CutsceneCameraShakyness.Noise noiseBigY;
  private CutsceneCameraShakyness.Noise noiseSmallX;
  private CutsceneCameraShakyness.Noise noiseSmallY;

  private void Start()
  {
    this.myTransform = this.transform;
    this.localOrigin = Vector3.zero;
    this.localRotation = Quaternion.identity;
    this.noiseBigX = new CutsceneCameraShakyness.Noise(this.bigSpeed);
    this.noiseBigY = new CutsceneCameraShakyness.Noise(this.bigSpeed);
    this.noiseSmallX = new CutsceneCameraShakyness.Noise(this.smallSpeed);
    this.noiseSmallY = new CutsceneCameraShakyness.Noise(this.smallSpeed);
  }

  private void Update()
  {
    this.noiseBigX.Update(this.bigSpeed);
    this.noiseBigY.Update(this.bigSpeed);
    this.noiseSmallX.Update(this.smallSpeed);
    this.noiseSmallY.Update(this.smallSpeed);
    float x = (float) ((double) this.noiseBigX.GetOffset * (double) this.bigRadius + (double) this.noiseSmallX.GetOffset * (double) this.smallRadius);
    float y = (float) ((double) this.noiseBigY.GetOffset * (double) this.bigRadius + (double) this.noiseSmallY.GetOffset * (double) this.smallRadius);
    this.myTransform.localPosition = this.localOrigin + new Vector3(x, y, 0.0f);
    this.myTransform.localRotation = this.localRotation * Quaternion.Euler(x, y, 0.0f);
  }

  private class Noise
  {
    private Vector2 seed;
    private float speed;

    public float GetOffset
    {
      get
      {
        this.seed += Vector2.one * this.speed * Time.deltaTime;
        return (float) ((double) Mathf.PerlinNoise(this.seed.x, this.seed.y) * 2.0 - 1.0);
      }
    }

    public Noise(float speed)
    {
      this.seed = new Vector2(Random.value, Random.value);
      this.speed = speed;
    }

    public void Update(float speed)
    {
      this.speed = speed;
    }
  }
}
