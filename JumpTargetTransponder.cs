﻿// Decompiled with JetBrains decompiler
// Type: JumpTargetTransponder
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class JumpTargetTransponder : SpaceObject
{
  private ArmedMineIndicator armedMineIndicator;

  protected override sbyte IconIndex
  {
    get
    {
      if (this.Faction == Faction.Colonial)
        return this.WorldCard.FrameIndex;
      return (sbyte) ((int) this.WorldCard.FrameIndex + 1);
    }
  }

  public JumpTargetTransponder(uint objectID)
    : base(objectID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    JumpTargetTransponder.\u003CRead\u003Ec__AnonStorey11A readCAnonStorey11A = new JumpTargetTransponder.\u003CRead\u003Ec__AnonStorey11A();
    // ISSUE: reference to a compiler-generated field
    readCAnonStorey11A.\u003C\u003Ef__this = this;
    base.Read(r);
    int num = (int) r.ReadUInt32();
    // ISSUE: reference to a compiler-generated field
    readCAnonStorey11A.timeWhenActive = r.ReadDateTime();
    // ISSUE: reference to a compiler-generated field
    readCAnonStorey11A.timeWhenInactive = r.ReadDateTime();
    // ISSUE: reference to a compiler-generated method
    this.IsConstructed.AddHandler(new SignalHandler(readCAnonStorey11A.\u003C\u003Em__2AD));
  }

  protected override IMovementController CreateMovementController()
  {
    return (IMovementController) new ManeuverController((SpaceObject) this, (MovementCard) Game.Catalogue.FetchCard(this.WorldCard.CardGUID, CardView.Movement));
  }
}
