﻿// Decompiled with JetBrains decompiler
// Type: HelpWindowWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class HelpWindowWidget : ContentWindowWidget
{
  public HelpContent helpContent;
  public UILabel windowTitle;
  public UILabel menuTitle;
  public ButtonWidget barCloseButton;

  public HelpWindowWidget()
  {
    this.type = WindowTypes.HelpWindow;
  }

  public override void Start()
  {
    this.ReloadLanguageData();
    PositionUtils.CenterWindowHorizontally((WindowWidget) this);
    this.barCloseButton.handleClick = new AnonymousDelegate(this.CloseOnButtonClick);
  }

  public override void Open()
  {
    base.Open();
    UguiTools.EnableCanvas(BsgoCanvas.HudIndicators, false);
    FacadeFactory.GetInstance().SendMessage(Message.HelpWindowOpened);
  }

  public override void Close()
  {
    base.Close();
    UguiTools.EnableCanvas(BsgoCanvas.HudIndicators, true);
    FacadeFactory.GetInstance().SendMessage(Message.HelpWindowClosed);
  }

  private void CloseOnButtonClick()
  {
    this.Close();
  }

  public override void ReloadLanguageData()
  {
    this.windowTitle.text = Tools.ParseMessage("%$bgo.HelpScreens.gui_menu_layout.title_label%");
    this.menuTitle.text = Tools.ParseMessage("%$bgo.HelpScreens.gui_menu_layout.subtitle_label%");
    if (!((Object) this.CloseButton != (Object) null))
      return;
    this.barCloseButton.TextLabel.text = Tools.ParseMessage("%$bgo.common.close%");
  }
}
