﻿// Decompiled with JetBrains decompiler
// Type: MissileExplosionArgs
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class MissileExplosionArgs : ExplosionArgs
{
  public MissileExplosionArgs.MissileExplosionView ExplosionView;
  public int ExplosionTier;
  public float ExplosionRange;

  public override string ToString()
  {
    return string.Format("MissileExplosionArgs: ExplosionView: {0}, ExplosionTier: {1}, ExplosionRange: {2}", (object) this.ExplosionView, (object) this.ExplosionTier, (object) this.ExplosionRange);
  }

  public enum MissileExplosionView : byte
  {
    Standard = 0,
    Nuclear = 1,
    NuclearMini = 2,
    Torpedo = 4,
  }
}
