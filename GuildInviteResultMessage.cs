﻿// Decompiled with JetBrains decompiler
// Type: GuildInviteResultMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System;

public class GuildInviteResultMessage : IMessage<Message>
{
  public uint PlayerId { get; private set; }

  public GuildInviteResult Result { get; private set; }

  public object Data
  {
    get
    {
      throw new NotImplementedException();
    }
    set
    {
      throw new NotImplementedException();
    }
  }

  public Message Id
  {
    get
    {
      return Message.GuildInviteResult;
    }
  }

  public GuildInviteResultMessage(uint playerId, GuildInviteResult result)
  {
    this.PlayerId = playerId;
    this.Result = result;
  }
}
