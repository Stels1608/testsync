﻿// Decompiled with JetBrains decompiler
// Type: UIBasicSprite
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public abstract class UIBasicSprite : UIWidget
{
  protected static Vector2[] mTempPos = new Vector2[4];
  protected static Vector2[] mTempUVs = new Vector2[4];
  [SerializeField]
  [HideInInspector]
  protected UIBasicSprite.FillDirection mFillDirection = UIBasicSprite.FillDirection.Radial360;
  [Range(0.0f, 1f)]
  [HideInInspector]
  [SerializeField]
  protected float mFillAmount = 1f;
  [NonSerialized]
  private Rect mInnerUV = new Rect();
  [NonSerialized]
  private Rect mOuterUV = new Rect();
  public UIBasicSprite.AdvancedType centerType = UIBasicSprite.AdvancedType.Sliced;
  public UIBasicSprite.AdvancedType leftType = UIBasicSprite.AdvancedType.Sliced;
  public UIBasicSprite.AdvancedType rightType = UIBasicSprite.AdvancedType.Sliced;
  public UIBasicSprite.AdvancedType bottomType = UIBasicSprite.AdvancedType.Sliced;
  public UIBasicSprite.AdvancedType topType = UIBasicSprite.AdvancedType.Sliced;
  [SerializeField]
  [HideInInspector]
  protected UIBasicSprite.Type mType;
  [HideInInspector]
  [SerializeField]
  protected bool mInvert;
  [SerializeField]
  [HideInInspector]
  protected UIBasicSprite.Flip mFlip;

  public virtual UIBasicSprite.Type type
  {
    get
    {
      return this.mType;
    }
    set
    {
      if (this.mType == value)
        return;
      this.mType = value;
      this.MarkAsChanged();
    }
  }

  public UIBasicSprite.Flip flip
  {
    get
    {
      return this.mFlip;
    }
    set
    {
      if (this.mFlip == value)
        return;
      this.mFlip = value;
      this.MarkAsChanged();
    }
  }

  public UIBasicSprite.FillDirection fillDirection
  {
    get
    {
      return this.mFillDirection;
    }
    set
    {
      if (this.mFillDirection == value)
        return;
      this.mFillDirection = value;
      this.mChanged = true;
    }
  }

  public float fillAmount
  {
    get
    {
      return this.mFillAmount;
    }
    set
    {
      float num = Mathf.Clamp01(value);
      if ((double) this.mFillAmount == (double) num)
        return;
      this.mFillAmount = num;
      this.mChanged = true;
    }
  }

  public override int minWidth
  {
    get
    {
      if (this.type != UIBasicSprite.Type.Sliced && this.type != UIBasicSprite.Type.Advanced)
        return base.minWidth;
      Vector4 vector4 = this.border * this.pixelSize;
      int @int = Mathf.RoundToInt(vector4.x + vector4.z);
      return Mathf.Max(base.minWidth, (@int & 1) != 1 ? @int : @int + 1);
    }
  }

  public override int minHeight
  {
    get
    {
      if (this.type != UIBasicSprite.Type.Sliced && this.type != UIBasicSprite.Type.Advanced)
        return base.minHeight;
      Vector4 vector4 = this.border * this.pixelSize;
      int @int = Mathf.RoundToInt(vector4.y + vector4.w);
      return Mathf.Max(base.minHeight, (@int & 1) != 1 ? @int : @int + 1);
    }
  }

  public bool invert
  {
    get
    {
      return this.mInvert;
    }
    set
    {
      if (this.mInvert == value)
        return;
      this.mInvert = value;
      this.mChanged = true;
    }
  }

  public bool hasBorder
  {
    get
    {
      Vector4 border = this.border;
      if ((double) border.x == 0.0 && (double) border.y == 0.0 && (double) border.z == 0.0)
        return (double) border.w != 0.0;
      return true;
    }
  }

  public virtual bool premultipliedAlpha
  {
    get
    {
      return false;
    }
  }

  public virtual float pixelSize
  {
    get
    {
      return 1f;
    }
  }

  private Vector4 drawingUVs
  {
    get
    {
      switch (this.mFlip)
      {
        case UIBasicSprite.Flip.Horizontally:
          return new Vector4(this.mOuterUV.xMax, this.mOuterUV.yMin, this.mOuterUV.xMin, this.mOuterUV.yMax);
        case UIBasicSprite.Flip.Vertically:
          return new Vector4(this.mOuterUV.xMin, this.mOuterUV.yMax, this.mOuterUV.xMax, this.mOuterUV.yMin);
        case UIBasicSprite.Flip.Both:
          return new Vector4(this.mOuterUV.xMax, this.mOuterUV.yMax, this.mOuterUV.xMin, this.mOuterUV.yMin);
        default:
          return new Vector4(this.mOuterUV.xMin, this.mOuterUV.yMin, this.mOuterUV.xMax, this.mOuterUV.yMax);
      }
    }
  }

  private Color32 drawingColor
  {
    get
    {
      Color c = this.color;
      c.a = this.finalAlpha;
      if (this.premultipliedAlpha)
        c = NGUITools.ApplyPMA(c);
      if (QualitySettings.activeColorSpace == ColorSpace.Linear)
      {
        c.r = Mathf.Pow(c.r, 2.2f);
        c.g = Mathf.Pow(c.g, 2.2f);
        c.b = Mathf.Pow(c.b, 2.2f);
      }
      return (Color32) c;
    }
  }

  protected void Fill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols, Rect outer, Rect inner)
  {
    this.mOuterUV = outer;
    this.mInnerUV = inner;
    switch (this.type)
    {
      case UIBasicSprite.Type.Simple:
        this.SimpleFill(verts, uvs, cols);
        break;
      case UIBasicSprite.Type.Sliced:
        this.SlicedFill(verts, uvs, cols);
        break;
      case UIBasicSprite.Type.Tiled:
        this.TiledFill(verts, uvs, cols);
        break;
      case UIBasicSprite.Type.Filled:
        this.FilledFill(verts, uvs, cols);
        break;
      case UIBasicSprite.Type.Advanced:
        this.AdvancedFill(verts, uvs, cols);
        break;
    }
  }

  private void SimpleFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    Vector4 drawingDimensions = this.drawingDimensions;
    Vector4 drawingUvs = this.drawingUVs;
    Color32 drawingColor = this.drawingColor;
    verts.Add(new Vector3(drawingDimensions.x, drawingDimensions.y));
    verts.Add(new Vector3(drawingDimensions.x, drawingDimensions.w));
    verts.Add(new Vector3(drawingDimensions.z, drawingDimensions.w));
    verts.Add(new Vector3(drawingDimensions.z, drawingDimensions.y));
    uvs.Add(new Vector2(drawingUvs.x, drawingUvs.y));
    uvs.Add(new Vector2(drawingUvs.x, drawingUvs.w));
    uvs.Add(new Vector2(drawingUvs.z, drawingUvs.w));
    uvs.Add(new Vector2(drawingUvs.z, drawingUvs.y));
    cols.Add(drawingColor);
    cols.Add(drawingColor);
    cols.Add(drawingColor);
    cols.Add(drawingColor);
  }

  private void SlicedFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    Vector4 vector4 = this.border * this.pixelSize;
    if ((double) vector4.x == 0.0 && (double) vector4.y == 0.0 && ((double) vector4.z == 0.0 && (double) vector4.w == 0.0))
    {
      this.SimpleFill(verts, uvs, cols);
    }
    else
    {
      Color32 drawingColor = this.drawingColor;
      Vector4 drawingDimensions = this.drawingDimensions;
      UIBasicSprite.mTempPos[0].x = drawingDimensions.x;
      UIBasicSprite.mTempPos[0].y = drawingDimensions.y;
      UIBasicSprite.mTempPos[3].x = drawingDimensions.z;
      UIBasicSprite.mTempPos[3].y = drawingDimensions.w;
      if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
      {
        UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x + vector4.z;
        UIBasicSprite.mTempPos[2].x = UIBasicSprite.mTempPos[3].x - vector4.x;
        UIBasicSprite.mTempUVs[3].x = this.mOuterUV.xMin;
        UIBasicSprite.mTempUVs[2].x = this.mInnerUV.xMin;
        UIBasicSprite.mTempUVs[1].x = this.mInnerUV.xMax;
        UIBasicSprite.mTempUVs[0].x = this.mOuterUV.xMax;
      }
      else
      {
        UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x + vector4.x;
        UIBasicSprite.mTempPos[2].x = UIBasicSprite.mTempPos[3].x - vector4.z;
        UIBasicSprite.mTempUVs[0].x = this.mOuterUV.xMin;
        UIBasicSprite.mTempUVs[1].x = this.mInnerUV.xMin;
        UIBasicSprite.mTempUVs[2].x = this.mInnerUV.xMax;
        UIBasicSprite.mTempUVs[3].x = this.mOuterUV.xMax;
      }
      if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
      {
        UIBasicSprite.mTempPos[1].y = UIBasicSprite.mTempPos[0].y + vector4.w;
        UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[3].y - vector4.y;
        UIBasicSprite.mTempUVs[3].y = this.mOuterUV.yMin;
        UIBasicSprite.mTempUVs[2].y = this.mInnerUV.yMin;
        UIBasicSprite.mTempUVs[1].y = this.mInnerUV.yMax;
        UIBasicSprite.mTempUVs[0].y = this.mOuterUV.yMax;
      }
      else
      {
        UIBasicSprite.mTempPos[1].y = UIBasicSprite.mTempPos[0].y + vector4.y;
        UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[3].y - vector4.w;
        UIBasicSprite.mTempUVs[0].y = this.mOuterUV.yMin;
        UIBasicSprite.mTempUVs[1].y = this.mInnerUV.yMin;
        UIBasicSprite.mTempUVs[2].y = this.mInnerUV.yMax;
        UIBasicSprite.mTempUVs[3].y = this.mOuterUV.yMax;
      }
      for (int index1 = 0; index1 < 3; ++index1)
      {
        int index2 = index1 + 1;
        for (int index3 = 0; index3 < 3; ++index3)
        {
          if (this.centerType != UIBasicSprite.AdvancedType.Invisible || index1 != 1 || index3 != 1)
          {
            int index4 = index3 + 1;
            verts.Add(new Vector3(UIBasicSprite.mTempPos[index1].x, UIBasicSprite.mTempPos[index3].y));
            verts.Add(new Vector3(UIBasicSprite.mTempPos[index1].x, UIBasicSprite.mTempPos[index4].y));
            verts.Add(new Vector3(UIBasicSprite.mTempPos[index2].x, UIBasicSprite.mTempPos[index4].y));
            verts.Add(new Vector3(UIBasicSprite.mTempPos[index2].x, UIBasicSprite.mTempPos[index3].y));
            uvs.Add(new Vector2(UIBasicSprite.mTempUVs[index1].x, UIBasicSprite.mTempUVs[index3].y));
            uvs.Add(new Vector2(UIBasicSprite.mTempUVs[index1].x, UIBasicSprite.mTempUVs[index4].y));
            uvs.Add(new Vector2(UIBasicSprite.mTempUVs[index2].x, UIBasicSprite.mTempUVs[index4].y));
            uvs.Add(new Vector2(UIBasicSprite.mTempUVs[index2].x, UIBasicSprite.mTempUVs[index3].y));
            cols.Add(drawingColor);
            cols.Add(drawingColor);
            cols.Add(drawingColor);
            cols.Add(drawingColor);
          }
        }
      }
    }
  }

  private void TiledFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
      return;
    Vector2 vector2 = new Vector2(this.mInnerUV.width * (float) mainTexture.width, this.mInnerUV.height * (float) mainTexture.height) * this.pixelSize;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null || (double) vector2.x < 2.0 || (double) vector2.y < 2.0)
      return;
    Color32 drawingColor = this.drawingColor;
    Vector4 drawingDimensions = this.drawingDimensions;
    Vector4 vector4;
    if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
    {
      vector4.x = this.mInnerUV.xMax;
      vector4.z = this.mInnerUV.xMin;
    }
    else
    {
      vector4.x = this.mInnerUV.xMin;
      vector4.z = this.mInnerUV.xMax;
    }
    if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
    {
      vector4.y = this.mInnerUV.yMax;
      vector4.w = this.mInnerUV.yMin;
    }
    else
    {
      vector4.y = this.mInnerUV.yMin;
      vector4.w = this.mInnerUV.yMax;
    }
    float num = drawingDimensions.x;
    float y1 = drawingDimensions.y;
    float x1 = vector4.x;
    float y2 = vector4.y;
    while ((double) y1 < (double) drawingDimensions.w)
    {
      float x2 = drawingDimensions.x;
      float y3 = y1 + vector2.y;
      float y4 = vector4.w;
      if ((double) y3 > (double) drawingDimensions.w)
      {
        y4 = Mathf.Lerp(vector4.y, vector4.w, (drawingDimensions.w - y1) / vector2.y);
        y3 = drawingDimensions.w;
      }
      while ((double) x2 < (double) drawingDimensions.z)
      {
        float x3 = x2 + vector2.x;
        float x4 = vector4.z;
        if ((double) x3 > (double) drawingDimensions.z)
        {
          x4 = Mathf.Lerp(vector4.x, vector4.z, (drawingDimensions.z - x2) / vector2.x);
          x3 = drawingDimensions.z;
        }
        verts.Add(new Vector3(x2, y1));
        verts.Add(new Vector3(x2, y3));
        verts.Add(new Vector3(x3, y3));
        verts.Add(new Vector3(x3, y1));
        uvs.Add(new Vector2(x1, y2));
        uvs.Add(new Vector2(x1, y4));
        uvs.Add(new Vector2(x4, y4));
        uvs.Add(new Vector2(x4, y2));
        cols.Add(drawingColor);
        cols.Add(drawingColor);
        cols.Add(drawingColor);
        cols.Add(drawingColor);
        x2 += vector2.x;
      }
      y1 += vector2.y;
    }
  }

  private void FilledFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    if ((double) this.mFillAmount < 1.0 / 1000.0)
      return;
    Vector4 drawingDimensions = this.drawingDimensions;
    Vector4 drawingUvs = this.drawingUVs;
    Color32 drawingColor = this.drawingColor;
    if (this.mFillDirection == UIBasicSprite.FillDirection.Horizontal || this.mFillDirection == UIBasicSprite.FillDirection.Vertical)
    {
      if (this.mFillDirection == UIBasicSprite.FillDirection.Horizontal)
      {
        float num = (drawingUvs.z - drawingUvs.x) * this.mFillAmount;
        if (this.mInvert)
        {
          drawingDimensions.x = drawingDimensions.z - (drawingDimensions.z - drawingDimensions.x) * this.mFillAmount;
          drawingUvs.x = drawingUvs.z - num;
        }
        else
        {
          drawingDimensions.z = drawingDimensions.x + (drawingDimensions.z - drawingDimensions.x) * this.mFillAmount;
          drawingUvs.z = drawingUvs.x + num;
        }
      }
      else if (this.mFillDirection == UIBasicSprite.FillDirection.Vertical)
      {
        float num = (drawingUvs.w - drawingUvs.y) * this.mFillAmount;
        if (this.mInvert)
        {
          drawingDimensions.y = drawingDimensions.w - (drawingDimensions.w - drawingDimensions.y) * this.mFillAmount;
          drawingUvs.y = drawingUvs.w - num;
        }
        else
        {
          drawingDimensions.w = drawingDimensions.y + (drawingDimensions.w - drawingDimensions.y) * this.mFillAmount;
          drawingUvs.w = drawingUvs.y + num;
        }
      }
    }
    UIBasicSprite.mTempPos[0] = new Vector2(drawingDimensions.x, drawingDimensions.y);
    UIBasicSprite.mTempPos[1] = new Vector2(drawingDimensions.x, drawingDimensions.w);
    UIBasicSprite.mTempPos[2] = new Vector2(drawingDimensions.z, drawingDimensions.w);
    UIBasicSprite.mTempPos[3] = new Vector2(drawingDimensions.z, drawingDimensions.y);
    UIBasicSprite.mTempUVs[0] = new Vector2(drawingUvs.x, drawingUvs.y);
    UIBasicSprite.mTempUVs[1] = new Vector2(drawingUvs.x, drawingUvs.w);
    UIBasicSprite.mTempUVs[2] = new Vector2(drawingUvs.z, drawingUvs.w);
    UIBasicSprite.mTempUVs[3] = new Vector2(drawingUvs.z, drawingUvs.y);
    if ((double) this.mFillAmount < 1.0)
    {
      if (this.mFillDirection == UIBasicSprite.FillDirection.Radial90)
      {
        if (!UIBasicSprite.RadialCut(UIBasicSprite.mTempPos, UIBasicSprite.mTempUVs, this.mFillAmount, this.mInvert, 0))
          return;
        for (int index = 0; index < 4; ++index)
        {
          verts.Add((Vector3) UIBasicSprite.mTempPos[index]);
          uvs.Add(UIBasicSprite.mTempUVs[index]);
          cols.Add(drawingColor);
        }
        return;
      }
      if (this.mFillDirection == UIBasicSprite.FillDirection.Radial180)
      {
        for (int index1 = 0; index1 < 2; ++index1)
        {
          float t1 = 0.0f;
          float t2 = 1f;
          float t3;
          float t4;
          if (index1 == 0)
          {
            t3 = 0.0f;
            t4 = 0.5f;
          }
          else
          {
            t3 = 0.5f;
            t4 = 1f;
          }
          UIBasicSprite.mTempPos[0].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t3);
          UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x;
          UIBasicSprite.mTempPos[2].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t4);
          UIBasicSprite.mTempPos[3].x = UIBasicSprite.mTempPos[2].x;
          UIBasicSprite.mTempPos[0].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t1);
          UIBasicSprite.mTempPos[1].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t2);
          UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[1].y;
          UIBasicSprite.mTempPos[3].y = UIBasicSprite.mTempPos[0].y;
          UIBasicSprite.mTempUVs[0].x = Mathf.Lerp(drawingUvs.x, drawingUvs.z, t3);
          UIBasicSprite.mTempUVs[1].x = UIBasicSprite.mTempUVs[0].x;
          UIBasicSprite.mTempUVs[2].x = Mathf.Lerp(drawingUvs.x, drawingUvs.z, t4);
          UIBasicSprite.mTempUVs[3].x = UIBasicSprite.mTempUVs[2].x;
          UIBasicSprite.mTempUVs[0].y = Mathf.Lerp(drawingUvs.y, drawingUvs.w, t1);
          UIBasicSprite.mTempUVs[1].y = Mathf.Lerp(drawingUvs.y, drawingUvs.w, t2);
          UIBasicSprite.mTempUVs[2].y = UIBasicSprite.mTempUVs[1].y;
          UIBasicSprite.mTempUVs[3].y = UIBasicSprite.mTempUVs[0].y;
          float num = this.mInvert ? this.mFillAmount * 2f - (float) (1 - index1) : this.fillAmount * 2f - (float) index1;
          if (UIBasicSprite.RadialCut(UIBasicSprite.mTempPos, UIBasicSprite.mTempUVs, Mathf.Clamp01(num), !this.mInvert, NGUIMath.RepeatIndex(index1 + 3, 4)))
          {
            for (int index2 = 0; index2 < 4; ++index2)
            {
              verts.Add((Vector3) UIBasicSprite.mTempPos[index2]);
              uvs.Add(UIBasicSprite.mTempUVs[index2]);
              cols.Add(drawingColor);
            }
          }
        }
        return;
      }
      if (this.mFillDirection == UIBasicSprite.FillDirection.Radial360)
      {
        for (int index1 = 0; index1 < 4; ++index1)
        {
          float t1;
          float t2;
          if (index1 < 2)
          {
            t1 = 0.0f;
            t2 = 0.5f;
          }
          else
          {
            t1 = 0.5f;
            t2 = 1f;
          }
          float t3;
          float t4;
          if (index1 == 0 || index1 == 3)
          {
            t3 = 0.0f;
            t4 = 0.5f;
          }
          else
          {
            t3 = 0.5f;
            t4 = 1f;
          }
          UIBasicSprite.mTempPos[0].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t1);
          UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x;
          UIBasicSprite.mTempPos[2].x = Mathf.Lerp(drawingDimensions.x, drawingDimensions.z, t2);
          UIBasicSprite.mTempPos[3].x = UIBasicSprite.mTempPos[2].x;
          UIBasicSprite.mTempPos[0].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t3);
          UIBasicSprite.mTempPos[1].y = Mathf.Lerp(drawingDimensions.y, drawingDimensions.w, t4);
          UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[1].y;
          UIBasicSprite.mTempPos[3].y = UIBasicSprite.mTempPos[0].y;
          UIBasicSprite.mTempUVs[0].x = Mathf.Lerp(drawingUvs.x, drawingUvs.z, t1);
          UIBasicSprite.mTempUVs[1].x = UIBasicSprite.mTempUVs[0].x;
          UIBasicSprite.mTempUVs[2].x = Mathf.Lerp(drawingUvs.x, drawingUvs.z, t2);
          UIBasicSprite.mTempUVs[3].x = UIBasicSprite.mTempUVs[2].x;
          UIBasicSprite.mTempUVs[0].y = Mathf.Lerp(drawingUvs.y, drawingUvs.w, t3);
          UIBasicSprite.mTempUVs[1].y = Mathf.Lerp(drawingUvs.y, drawingUvs.w, t4);
          UIBasicSprite.mTempUVs[2].y = UIBasicSprite.mTempUVs[1].y;
          UIBasicSprite.mTempUVs[3].y = UIBasicSprite.mTempUVs[0].y;
          float num = !this.mInvert ? this.mFillAmount * 4f - (float) (3 - NGUIMath.RepeatIndex(index1 + 2, 4)) : this.mFillAmount * 4f - (float) NGUIMath.RepeatIndex(index1 + 2, 4);
          if (UIBasicSprite.RadialCut(UIBasicSprite.mTempPos, UIBasicSprite.mTempUVs, Mathf.Clamp01(num), this.mInvert, NGUIMath.RepeatIndex(index1 + 2, 4)))
          {
            for (int index2 = 0; index2 < 4; ++index2)
            {
              verts.Add((Vector3) UIBasicSprite.mTempPos[index2]);
              uvs.Add(UIBasicSprite.mTempUVs[index2]);
              cols.Add(drawingColor);
            }
          }
        }
        return;
      }
    }
    for (int index = 0; index < 4; ++index)
    {
      verts.Add((Vector3) UIBasicSprite.mTempPos[index]);
      uvs.Add(UIBasicSprite.mTempUVs[index]);
      cols.Add(drawingColor);
    }
  }

  private void AdvancedFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
      return;
    Vector4 vector4 = this.border * this.pixelSize;
    if ((double) vector4.x == 0.0 && (double) vector4.y == 0.0 && ((double) vector4.z == 0.0 && (double) vector4.w == 0.0))
    {
      this.SimpleFill(verts, uvs, cols);
    }
    else
    {
      Color32 drawingColor = this.drawingColor;
      Vector4 drawingDimensions = this.drawingDimensions;
      Vector2 vector2 = new Vector2(this.mInnerUV.width * (float) mainTexture.width, this.mInnerUV.height * (float) mainTexture.height);
      vector2 *= this.pixelSize;
      if ((double) vector2.x < 1.0)
        vector2.x = 1f;
      if ((double) vector2.y < 1.0)
        vector2.y = 1f;
      UIBasicSprite.mTempPos[0].x = drawingDimensions.x;
      UIBasicSprite.mTempPos[0].y = drawingDimensions.y;
      UIBasicSprite.mTempPos[3].x = drawingDimensions.z;
      UIBasicSprite.mTempPos[3].y = drawingDimensions.w;
      if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
      {
        UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x + vector4.z;
        UIBasicSprite.mTempPos[2].x = UIBasicSprite.mTempPos[3].x - vector4.x;
        UIBasicSprite.mTempUVs[3].x = this.mOuterUV.xMin;
        UIBasicSprite.mTempUVs[2].x = this.mInnerUV.xMin;
        UIBasicSprite.mTempUVs[1].x = this.mInnerUV.xMax;
        UIBasicSprite.mTempUVs[0].x = this.mOuterUV.xMax;
      }
      else
      {
        UIBasicSprite.mTempPos[1].x = UIBasicSprite.mTempPos[0].x + vector4.x;
        UIBasicSprite.mTempPos[2].x = UIBasicSprite.mTempPos[3].x - vector4.z;
        UIBasicSprite.mTempUVs[0].x = this.mOuterUV.xMin;
        UIBasicSprite.mTempUVs[1].x = this.mInnerUV.xMin;
        UIBasicSprite.mTempUVs[2].x = this.mInnerUV.xMax;
        UIBasicSprite.mTempUVs[3].x = this.mOuterUV.xMax;
      }
      if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
      {
        UIBasicSprite.mTempPos[1].y = UIBasicSprite.mTempPos[0].y + vector4.w;
        UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[3].y - vector4.y;
        UIBasicSprite.mTempUVs[3].y = this.mOuterUV.yMin;
        UIBasicSprite.mTempUVs[2].y = this.mInnerUV.yMin;
        UIBasicSprite.mTempUVs[1].y = this.mInnerUV.yMax;
        UIBasicSprite.mTempUVs[0].y = this.mOuterUV.yMax;
      }
      else
      {
        UIBasicSprite.mTempPos[1].y = UIBasicSprite.mTempPos[0].y + vector4.y;
        UIBasicSprite.mTempPos[2].y = UIBasicSprite.mTempPos[3].y - vector4.w;
        UIBasicSprite.mTempUVs[0].y = this.mOuterUV.yMin;
        UIBasicSprite.mTempUVs[1].y = this.mInnerUV.yMin;
        UIBasicSprite.mTempUVs[2].y = this.mInnerUV.yMax;
        UIBasicSprite.mTempUVs[3].y = this.mOuterUV.yMax;
      }
      for (int index1 = 0; index1 < 3; ++index1)
      {
        int index2 = index1 + 1;
        for (int index3 = 0; index3 < 3; ++index3)
        {
          if (this.centerType != UIBasicSprite.AdvancedType.Invisible || index1 != 1 || index3 != 1)
          {
            int index4 = index3 + 1;
            if (index1 == 1 && index3 == 1)
            {
              if (this.centerType == UIBasicSprite.AdvancedType.Tiled)
              {
                float num1 = UIBasicSprite.mTempPos[index1].x;
                float num2 = UIBasicSprite.mTempPos[index2].x;
                float num3 = UIBasicSprite.mTempPos[index3].y;
                float num4 = UIBasicSprite.mTempPos[index4].y;
                float num5 = UIBasicSprite.mTempUVs[index1].x;
                float num6 = UIBasicSprite.mTempUVs[index3].y;
                float v0y = num3;
                while ((double) v0y < (double) num4)
                {
                  float v0x = num1;
                  float num7 = UIBasicSprite.mTempUVs[index4].y;
                  float v1y = v0y + vector2.y;
                  if ((double) v1y > (double) num4)
                  {
                    num7 = Mathf.Lerp(num6, num7, (num4 - v0y) / vector2.y);
                    v1y = num4;
                  }
                  while ((double) v0x < (double) num2)
                  {
                    float v1x = v0x + vector2.x;
                    float num8 = UIBasicSprite.mTempUVs[index2].x;
                    if ((double) v1x > (double) num2)
                    {
                      num8 = Mathf.Lerp(num5, num8, (num2 - v0x) / vector2.x);
                      v1x = num2;
                    }
                    UIBasicSprite.Fill(verts, uvs, cols, v0x, v1x, v0y, v1y, num5, num8, num6, num7, (Color) drawingColor);
                    v0x += vector2.x;
                  }
                  v0y += vector2.y;
                }
              }
              else if (this.centerType == UIBasicSprite.AdvancedType.Sliced)
                UIBasicSprite.Fill(verts, uvs, cols, UIBasicSprite.mTempPos[index1].x, UIBasicSprite.mTempPos[index2].x, UIBasicSprite.mTempPos[index3].y, UIBasicSprite.mTempPos[index4].y, UIBasicSprite.mTempUVs[index1].x, UIBasicSprite.mTempUVs[index2].x, UIBasicSprite.mTempUVs[index3].y, UIBasicSprite.mTempUVs[index4].y, (Color) drawingColor);
            }
            else if (index1 == 1)
            {
              if (index3 == 0 && this.bottomType == UIBasicSprite.AdvancedType.Tiled || index3 == 2 && this.topType == UIBasicSprite.AdvancedType.Tiled)
              {
                float num1 = UIBasicSprite.mTempPos[index1].x;
                float num2 = UIBasicSprite.mTempPos[index2].x;
                float v0y = UIBasicSprite.mTempPos[index3].y;
                float v1y = UIBasicSprite.mTempPos[index4].y;
                float num3 = UIBasicSprite.mTempUVs[index1].x;
                float u0y = UIBasicSprite.mTempUVs[index3].y;
                float u1y = UIBasicSprite.mTempUVs[index4].y;
                float v0x = num1;
                while ((double) v0x < (double) num2)
                {
                  float v1x = v0x + vector2.x;
                  float num4 = UIBasicSprite.mTempUVs[index2].x;
                  if ((double) v1x > (double) num2)
                  {
                    num4 = Mathf.Lerp(num3, num4, (num2 - v0x) / vector2.x);
                    v1x = num2;
                  }
                  UIBasicSprite.Fill(verts, uvs, cols, v0x, v1x, v0y, v1y, num3, num4, u0y, u1y, (Color) drawingColor);
                  v0x += vector2.x;
                }
              }
              else if (index3 == 0 && this.bottomType == UIBasicSprite.AdvancedType.Sliced || index3 == 2 && this.topType == UIBasicSprite.AdvancedType.Sliced)
                UIBasicSprite.Fill(verts, uvs, cols, UIBasicSprite.mTempPos[index1].x, UIBasicSprite.mTempPos[index2].x, UIBasicSprite.mTempPos[index3].y, UIBasicSprite.mTempPos[index4].y, UIBasicSprite.mTempUVs[index1].x, UIBasicSprite.mTempUVs[index2].x, UIBasicSprite.mTempUVs[index3].y, UIBasicSprite.mTempUVs[index4].y, (Color) drawingColor);
            }
            else if (index3 == 1)
            {
              if (index1 == 0 && this.leftType == UIBasicSprite.AdvancedType.Tiled || index1 == 2 && this.rightType == UIBasicSprite.AdvancedType.Tiled)
              {
                float v0x = UIBasicSprite.mTempPos[index1].x;
                float v1x = UIBasicSprite.mTempPos[index2].x;
                float num1 = UIBasicSprite.mTempPos[index3].y;
                float num2 = UIBasicSprite.mTempPos[index4].y;
                float u0x = UIBasicSprite.mTempUVs[index1].x;
                float u1x = UIBasicSprite.mTempUVs[index2].x;
                float num3 = UIBasicSprite.mTempUVs[index3].y;
                float v0y = num1;
                while ((double) v0y < (double) num2)
                {
                  float num4 = UIBasicSprite.mTempUVs[index4].y;
                  float v1y = v0y + vector2.y;
                  if ((double) v1y > (double) num2)
                  {
                    num4 = Mathf.Lerp(num3, num4, (num2 - v0y) / vector2.y);
                    v1y = num2;
                  }
                  UIBasicSprite.Fill(verts, uvs, cols, v0x, v1x, v0y, v1y, u0x, u1x, num3, num4, (Color) drawingColor);
                  v0y += vector2.y;
                }
              }
              else if (index1 == 0 && this.leftType == UIBasicSprite.AdvancedType.Sliced || index1 == 2 && this.rightType == UIBasicSprite.AdvancedType.Sliced)
                UIBasicSprite.Fill(verts, uvs, cols, UIBasicSprite.mTempPos[index1].x, UIBasicSprite.mTempPos[index2].x, UIBasicSprite.mTempPos[index3].y, UIBasicSprite.mTempPos[index4].y, UIBasicSprite.mTempUVs[index1].x, UIBasicSprite.mTempUVs[index2].x, UIBasicSprite.mTempUVs[index3].y, UIBasicSprite.mTempUVs[index4].y, (Color) drawingColor);
            }
            else
              UIBasicSprite.Fill(verts, uvs, cols, UIBasicSprite.mTempPos[index1].x, UIBasicSprite.mTempPos[index2].x, UIBasicSprite.mTempPos[index3].y, UIBasicSprite.mTempPos[index4].y, UIBasicSprite.mTempUVs[index1].x, UIBasicSprite.mTempUVs[index2].x, UIBasicSprite.mTempUVs[index3].y, UIBasicSprite.mTempUVs[index4].y, (Color) drawingColor);
          }
        }
      }
    }
  }

  private static bool RadialCut(Vector2[] xy, Vector2[] uv, float fill, bool invert, int corner)
  {
    if ((double) fill < 1.0 / 1000.0)
      return false;
    if ((corner & 1) == 1)
      invert = !invert;
    if (!invert && (double) fill > 0.999000012874603)
      return true;
    float num = Mathf.Clamp01(fill);
    if (invert)
      num = 1f - num;
    float f = num * 1.570796f;
    float cos = Mathf.Cos(f);
    float sin = Mathf.Sin(f);
    UIBasicSprite.RadialCut(xy, cos, sin, invert, corner);
    UIBasicSprite.RadialCut(uv, cos, sin, invert, corner);
    return true;
  }

  private static void RadialCut(Vector2[] xy, float cos, float sin, bool invert, int corner)
  {
    int index1 = corner;
    int index2 = NGUIMath.RepeatIndex(corner + 1, 4);
    int index3 = NGUIMath.RepeatIndex(corner + 2, 4);
    int index4 = NGUIMath.RepeatIndex(corner + 3, 4);
    if ((corner & 1) == 1)
    {
      if ((double) sin > (double) cos)
      {
        cos /= sin;
        sin = 1f;
        if (invert)
        {
          xy[index2].x = Mathf.Lerp(xy[index1].x, xy[index3].x, cos);
          xy[index3].x = xy[index2].x;
        }
      }
      else if ((double) cos > (double) sin)
      {
        sin /= cos;
        cos = 1f;
        if (!invert)
        {
          xy[index3].y = Mathf.Lerp(xy[index1].y, xy[index3].y, sin);
          xy[index4].y = xy[index3].y;
        }
      }
      else
      {
        cos = 1f;
        sin = 1f;
      }
      if (!invert)
        xy[index4].x = Mathf.Lerp(xy[index1].x, xy[index3].x, cos);
      else
        xy[index2].y = Mathf.Lerp(xy[index1].y, xy[index3].y, sin);
    }
    else
    {
      if ((double) cos > (double) sin)
      {
        sin /= cos;
        cos = 1f;
        if (!invert)
        {
          xy[index2].y = Mathf.Lerp(xy[index1].y, xy[index3].y, sin);
          xy[index3].y = xy[index2].y;
        }
      }
      else if ((double) sin > (double) cos)
      {
        cos /= sin;
        sin = 1f;
        if (invert)
        {
          xy[index3].x = Mathf.Lerp(xy[index1].x, xy[index3].x, cos);
          xy[index4].x = xy[index3].x;
        }
      }
      else
      {
        cos = 1f;
        sin = 1f;
      }
      if (invert)
        xy[index4].y = Mathf.Lerp(xy[index1].y, xy[index3].y, sin);
      else
        xy[index2].x = Mathf.Lerp(xy[index1].x, xy[index3].x, cos);
    }
  }

  private static void Fill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols, float v0x, float v1x, float v0y, float v1y, float u0x, float u1x, float u0y, float u1y, Color col)
  {
    verts.Add(new Vector3(v0x, v0y));
    verts.Add(new Vector3(v0x, v1y));
    verts.Add(new Vector3(v1x, v1y));
    verts.Add(new Vector3(v1x, v0y));
    uvs.Add(new Vector2(u0x, u0y));
    uvs.Add(new Vector2(u0x, u1y));
    uvs.Add(new Vector2(u1x, u1y));
    uvs.Add(new Vector2(u1x, u0y));
    cols.Add((Color32) col);
    cols.Add((Color32) col);
    cols.Add((Color32) col);
    cols.Add((Color32) col);
  }

  public enum Type
  {
    Simple,
    Sliced,
    Tiled,
    Filled,
    Advanced,
  }

  public enum FillDirection
  {
    Horizontal,
    Vertical,
    Radial90,
    Radial180,
    Radial360,
  }

  public enum AdvancedType
  {
    Invisible,
    Sliced,
    Tiled,
  }

  public enum Flip
  {
    Nothing,
    Horizontally,
    Vertically,
    Both,
  }
}
