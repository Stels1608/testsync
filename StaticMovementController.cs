﻿// Decompiled with JetBrains decompiler
// Type: StaticMovementController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StaticMovementController : IMovementController
{
  private readonly SpaceObject spaceObject;

  public float CurrentSpeed
  {
    get
    {
      return 0.0f;
    }
  }

  public Vector3 CurrentStrafingSpeed
  {
    get
    {
      return Vector3.zero;
    }
  }

  public float MarchSpeed
  {
    get
    {
      return 0.0f;
    }
  }

  public Gear Gear
  {
    get
    {
      return Gear.Regular;
    }
  }

  public StaticMovementController(SpaceObject spaceObject)
  {
    this.spaceObject = spaceObject;
  }

  public MovementFrame GetTickFrame(Tick tick)
  {
    return new MovementFrame(this.spaceObject.Position, Euler3.Rotation(this.spaceObject.Rotation), Vector3.zero, Vector3.zero, Euler3.zero);
  }

  public bool Move(double time)
  {
    return true;
  }

  public void Advance(Tick tick)
  {
  }

  public void PostAdvance()
  {
  }

  public void AddManeuver(Maneuver newManeuver)
  {
  }

  public void AddSyncFrame(Tick tick, MovementFrame frame)
  {
  }
}
