﻿// Decompiled with JetBrains decompiler
// Type: JumpActionsHandler
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class JumpActionsHandler : GUIPanel
{
  public JumpActionsHandler()
  {
    this.handlers.Add(Action.Jump, new AnonymousDelegate(JumpActionsHandler.InitJumpSequence));
    this.handlers.Add(Action.CancelJump, (AnonymousDelegate) (() =>
    {
      if (Game.Me.Anchored)
        return;
      JumpActionsHandler.CancelJumpSequence();
    }));
  }

  public static void InitJumpSequence()
  {
    GalaxyMapMain galaxyMapMain = Game.GUIManager.Find<GalaxyMapMain>();
    if (galaxyMapMain == null || galaxyMapMain.isJumpActive || (galaxyMapMain.SelectedGalaxyMapSector == null || !galaxyMapMain.IsSelectedSectorWithinWarpLimit()))
      return;
    GameProtocol.GetProtocol().RequestFTLJump(galaxyMapMain.SelectedId);
  }

  public static void LeaderCancelJumpSequence()
  {
    Game.Me.Party.inGroupJump = false;
    Game.GUIManager.Find<GUICancelGroupJumpWindow>().Hide();
    GalaxyMapMain galaxyMapMain = Game.GUIManager.Find<GalaxyMapMain>();
    if (galaxyMapMain != null)
      galaxyMapMain.isJumpActive = false;
    JumpActionsHandler.HideUI();
  }

  public static void CancelJumpSequence()
  {
    if (Game.Me.Party.inGroupJump)
    {
      GameProtocol.GetProtocol().RequestStopGroupJump();
      if (!Game.Me.Party.IsLeader)
      {
        GUICancelGroupJumpWindow cancelGroupJumpWindow = Game.GUIManager.Find<GUICancelGroupJumpWindow>();
        if (cancelGroupJumpWindow != null)
          cancelGroupJumpWindow.Hide();
      }
    }
    else
      GameProtocol.GetProtocol().RequestStopJump();
    Game.JustJumped = false;
    JumpActionsHandler.JumpFailed();
  }

  public static void JumpFailed()
  {
    Game.Me.Party.inGroupJump = false;
    GalaxyMapMain galaxyMapMain = Game.GUIManager.Find<GalaxyMapMain>();
    if (galaxyMapMain != null)
      galaxyMapMain.isJumpActive = false;
    JumpActionsHandler.HideUI();
  }

  private static void HideUI()
  {
    JumpCountdown.Hide();
    GUISmallPaperdoll guiSmallPaperdoll = Game.GUIManager.Find<GUISmallPaperdoll>();
    if (guiSmallPaperdoll == null)
      return;
    guiSmallPaperdoll.EnableAll();
  }
}
