﻿// Decompiled with JetBrains decompiler
// Type: Planetoid
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class Planetoid : Asteroid
{
  public override string Name
  {
    get
    {
      return BsgoLocalization.Get("%$bgo.common.planetoid%");
    }
  }

  public Planetoid()
    : this(0U)
  {
    this.HideStats = true;
  }

  public Planetoid(string type)
    : this(234881024U | IdGenerator.GenerateId())
  {
    this.type = type;
  }

  public Planetoid(uint objectID)
    : base(objectID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    float radius = this.Radius;
    this.Root.gameObject.transform.localScale = new Vector3(radius, radius, radius);
  }
}
