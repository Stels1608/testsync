﻿// Decompiled with JetBrains decompiler
// Type: OptionsMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "OptionsMediator";
  private OptionsWindowWidget optionsWindow;
  private SettingsDataProvider _settingsDataProvider;

  private SettingsDataProvider SettingsDataProvider
  {
    get
    {
      return this._settingsDataProvider ?? (this._settingsDataProvider = (SettingsDataProvider) FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider"));
    }
  }

  public OptionsMediator()
    : base("OptionsMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.FirstLoginEv0r);
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.InitInputBindingData);
    this.AddMessageInterest(Message.ShowOptionsWindow);
    this.AddMessageInterest(Message.ChangeInputBinding);
    this.AddMessageInterest(Message.LoadBindingPreset);
    this.AddMessageInterest(Message.SaveInputBindings);
    this.AddMessageInterest(Message.RevertInputBindings);
    this.AddMessageInterest(Message.ResetInputBindings);
    this.AddMessageInterest(Message.SettingsLoaded);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.PlayershipAspectsUpdated);
    this.AddMessageInterest(Message.ToggleJoystickGamepadEnabled);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.ToggleJoystickGamepadEnabled:
        this.optionsWindow.ToggleJoystickGamepadEnabled((bool) message.Data);
        break;
      case Message.InitInputBindingData:
        this.UpdateBindingOptionsUi(this.SettingsDataProvider.InputBinder.InputBindings);
        break;
      case Message.LoadBindingPreset:
        this.LoadPreset((BindingPreset) message.Data);
        break;
      case Message.ChangeInputBinding:
        OptionsContent componentInChildren = this.optionsWindow.gameObject.GetComponentInChildren<OptionsContent>();
        if ((Object) componentInChildren != (Object) null)
          componentInChildren.changed = true;
        this.ChangeInputBinding((InputBindingChangeMessage) message);
        break;
      case Message.SaveInputBindings:
        this.SaveInputBindings();
        break;
      case Message.RevertInputBindings:
        this.RevertInputBinding();
        break;
      case Message.ResetInputBindings:
        this.ResetInputBindings();
        break;
      case Message.PlayershipAspectsUpdated:
        this.optionsWindow.SetDogFightStatus(((ShipAspects) message.Data).ContainsAspect(ShipAspect.Dogfight));
        break;
      case Message.LoadNewLevel:
        this.HideOptionsWindow();
        break;
      default:
        if (id != Message.SettingsLoaded)
        {
          if (id != Message.UiCreated)
          {
            if (id != Message.ShowOptionsWindow)
            {
              if (id == Message.FirstLoginEv0r)
              {
                this.OwnerFacade.SendMessage(Message.ApplySettings);
                FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.HudIndicatorColorScheme, (object) HudIndicatorColorSchemeMode.FriendOrFoeColors, true));
                break;
              }
              Debug.LogWarning((object) ("RECEIVE MESSAGE UNKNOWN in OptionsMediator (" + (object) message.Id + ")"));
              break;
            }
            this.ShowOptionsWindow();
            break;
          }
          this.CreateView();
          break;
        }
        this.SetCurrentSettings((UserSettings) message.Data);
        break;
    }
    switch (message.Id)
    {
      case Message.InitInputBindingData:
      case Message.SaveInputBindings:
        this.OwnerFacade.SendMessage(Message.InputBindingDataChanged, (object) this.SettingsDataProvider.InputBinder);
        break;
    }
  }

  private void CreateView()
  {
    this.optionsWindow = ((GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/ui_elements/options/OptionsWindow"))).GetComponent<OptionsWindowWidget>();
    if ((Object) this.optionsWindow == (Object) null)
      Debug.LogError((object) "OptionsWindowWidget was not registered correctly");
    (this.OwnerFacade.FetchDataProvider("GuiDataProvider") as GuiDataProvider).AddWindow((WindowWidget) this.optionsWindow);
    this.optionsWindow.InjectDelegates(new OnSettingsApply(this.ApplySettings), new OnSettingsUndo(this.UndoSettingsChanged));
  }

  private void ApplySettings(UserSettings settings)
  {
    FacadeFactory.GetInstance().SendMessage(Message.ApplySettings, (object) settings);
    this.SetCurrentSettings(settings);
  }

  private void UndoSettingsChanged()
  {
    this.SetCurrentSettings(this.SettingsDataProvider.CurrentSettings);
  }

  private void SetCurrentSettings(UserSettings settings)
  {
    this.optionsWindow.InjectUserSettings(settings);
  }

  private void HideOptionsWindow()
  {
    if (!((Object) this.optionsWindow != (Object) null) || !this.optionsWindow.IsOpen)
      return;
    this.optionsWindow.Close();
  }

  private void ShowOptionsWindow()
  {
    if (!((Object) null != (Object) this.optionsWindow))
      return;
    if (this.optionsWindow.IsOpen)
      this.optionsWindow.Close();
    else
      this.optionsWindow.Open();
  }

  private void UpdateBindingOptionsUi(List<IInputBinding> inputBindings)
  {
    this.optionsWindow.UpdateBindingButtons(inputBindings);
  }

  private void LoadPreset(BindingPreset preset)
  {
    this.SettingsDataProvider.InputBinderCache.LoadPreset(preset);
    this.UpdateBindingOptionsUi(this.SettingsDataProvider.InputBinderCache.InputBindings);
  }

  private void ChangeInputBinding(InputBindingChangeMessage changeMessage)
  {
    if (this.SettingsDataProvider.InputBinderCache.InputBindings.Count == 0)
      this.SettingsDataProvider.InputBinderCache.MergeWith(this.SettingsDataProvider.InputBinder.InputBindings);
    this.SettingsDataProvider.InputBinderCache.UnbindCollidingBindings(changeMessage.BindingNew);
    this.SettingsDataProvider.InputBinderCache.UpdateInputBinding(changeMessage.BindingNew);
    this.UpdateBindingOptionsUi(this.SettingsDataProvider.InputBinderCache.InputBindings);
  }

  private void RevertInputBinding()
  {
    this.SettingsDataProvider.InputBinderCache.InputBindings.Clear();
    this.UpdateBindingOptionsUi(this.SettingsDataProvider.InputBinder.InputBindings);
  }

  private void SaveInputBindings()
  {
    if (this.SettingsDataProvider.InputBinderCache.InputBindings.Count == 0)
    {
      Debug.LogWarning((object) "There are no changed Keys. Abort Saving");
    }
    else
    {
      this.SettingsDataProvider.InputBinder.MergeWith(this.SettingsDataProvider.InputBinderCache.InputBindings);
      SettingProtocol.GetProtocol().RequestSaveKeys(this.SettingsDataProvider);
      this.SettingsDataProvider.InputBinderCache.InputBindings.Clear();
      this.UpdateBindingOptionsUi(this.SettingsDataProvider.InputBinder.InputBindings);
    }
  }

  private void ResetInputBindings()
  {
    this.SettingsDataProvider.InputBinderCache.InputBindings.Clear();
    this.SettingsDataProvider.InputBinder.ResetToDefaults((byte) 0);
    SettingProtocol.GetProtocol().RequestSaveKeys(this.SettingsDataProvider);
    this.UpdateBindingOptionsUi(this.SettingsDataProvider.InputBinder.InputBindings);
  }
}
