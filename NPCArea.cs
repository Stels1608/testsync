﻿// Decompiled with JetBrains decompiler
// Type: NPCArea
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using HighlightingSystem;
using UnityEngine;

public class NPCArea : RoomObject
{
  protected GameObject iconObject;
  public RoomNPC npc;
  public RoomLevel level;
  public Transform npcRoot;
  public Highlighter highlightableObject;

  public void Start()
  {
  }

  public override void CaptureObject()
  {
    this.level.roomDetectionToolTip.IsRendered = true;
    if ((Object) this.highlightableObject != (Object) null)
    {
      this.highlightableObject.ConstantOn();
      this.highlightableObject.ConstantOn(ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR));
    }
    base.CaptureObject();
  }

  public override void Update()
  {
    if (this.isCaptured && (bool) this.npc.NPCGUICard.IsLoaded)
    {
      this.level.roomDetectionToolTip.Position = new Vector2(MouseSetup.MousePositionGui.x, MouseSetup.MousePositionGui.y);
      this.level.roomDetectionToolTip.SetText(this.npc.NPCGUICard.Name + " [" + this.npc.NPCGUICard.Description + "]\n%$bgo.etc.left_click_to_interact%");
    }
    base.Update();
  }

  public override void EndCaptureObject()
  {
    this.level.roomDetectionToolTip.IsRendered = false;
    if ((Object) this.highlightableObject != (Object) null)
      this.highlightableObject.ConstantOff();
    base.EndCaptureObject();
  }

  public override void RunBehaviour()
  {
    if (Game.GUIManager.IsDialogBlocked())
      return;
    this.level.RequestTalk(this.npc.NPC);
  }
}
