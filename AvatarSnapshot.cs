﻿// Decompiled with JetBrains decompiler
// Type: AvatarSnapshot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AvatarSnapshot
{
  private static readonly string avatarGoName = "AvatarSnapshotGo" + UnityEngine.Random.value.ToString();
  private static bool snapshotPending = false;
  private static AvatarSnapshotCallback handler = (AvatarSnapshotCallback) (snapshot => {});
  private static Texture2D portraitCached121x165;
  private static GameObject lightSetup;
  private static AvatarItems avatarDesc;
  private static Avatar avatar;
  private static GameObject avatarGo;
  private static Timer avatarConstructTimer;

  private static int Layer
  {
    get
    {
      return AvatarView.layer;
    }
  }

  public static void RequestCharacterPortrait(AvatarSnapshotCallback snapshotCallback)
  {
    AvatarSnapshot.AddSnapshotCallbackHandler(snapshotCallback);
    if (AvatarSnapshot.snapshotPending)
    {
      Debug.Log((object) "There's already a snapshot procedure running...");
    }
    else
    {
      AvatarSnapshot.snapshotPending = true;
      if ((UnityEngine.Object) AvatarSnapshot.portraitCached121x165 == (UnityEngine.Object) null)
      {
        AvatarSnapshot.SetupScene();
        AvatarSnapshot.TriggerSnapshot();
      }
      else
        AvatarSnapshot.DeploySnapshot();
    }
  }

  private static void AddSnapshotCallbackHandler(AvatarSnapshotCallback snapshotCallback)
  {
    AvatarSnapshot.handler += snapshotCallback;
  }

  private static void SetupScene()
  {
    AvatarSnapshot.avatarDesc = (AvatarItems) Game.Me.AvatarDesc;
    AvatarSnapshot.avatarGo = AvatarSnapshot.InitializeAvatarGo();
    AvatarSnapshot.avatar = (Avatar) null;
    Camera.main.cullingMask &= ~(1 << AvatarSnapshot.Layer);
    Vector3 zero = Vector3.zero;
    AvatarSnapshot.avatarGo.transform.position = zero;
    AvatarSnapshot.SetupLight(!(AvatarSnapshot.avatarDesc.GetItem(AvatarItem.Race) == "human") ? Faction.Cylon : Faction.Colonial);
  }

  private static void DeploySnapshot()
  {
    AvatarSnapshot.TriggerAndReleaseHandlers();
    AvatarSnapshot.snapshotPending = false;
  }

  private static void TriggerAndReleaseHandlers()
  {
    AvatarSnapshot.handler(AvatarSnapshot.portraitCached121x165);
    AvatarSnapshot.handler = (AvatarSnapshotCallback) (snapshot => {});
  }

  private static void SetupLight(Faction faction)
  {
    GameObject original;
    switch (faction)
    {
      case Faction.Colonial:
        original = Resources.Load<GameObject>("Fx/snapshotscene_human");
        break;
      case Faction.Cylon:
        original = Resources.Load<GameObject>("Fx/snapshotscene_cylon");
        break;
      default:
        original = Resources.Load<GameObject>("Fx/snapshotscene_human");
        Debug.Log((object) "Unknown faction");
        break;
    }
    AvatarSnapshot.lightSetup = UnityEngine.Object.Instantiate<GameObject>(original);
    AvatarSnapshot.lightSetup.transform.position = Vector3.zero;
    foreach (Light componentsInChild in AvatarSnapshot.lightSetup.GetComponentsInChildren<Light>())
      componentsInChild.cullingMask = AvatarView.GetCullingMask();
  }

  private static GameObject InitializeAvatarGo()
  {
    return new GameObject(AvatarSnapshot.avatarGoName);
  }

  private static void TriggerSnapshot()
  {
    if ((UnityEngine.Object) AvatarSnapshot.avatarGo == (UnityEngine.Object) null)
      Debug.LogWarning((object) "AvatarGo is null.");
    else if (AvatarSnapshot.avatarGo.transform.childCount > 0)
    {
      Debug.LogWarning((object) "Avatar already constructed.");
    }
    else
    {
      StaticCards.Avatar.IsLoaded.AddHandler(new SignalHandler(AvatarSnapshot.StartConstructAvatar));
      StaticCards.Avatar.IsLoaded.Set();
    }
  }

  private static void StartConstructAvatar()
  {
    AvatarSnapshot.avatarConstructTimer = Timer.CreateTimer("avatarConstructTimer", 0.05f, 0.05f, new Timer.TickHandler(AvatarSnapshot.TryConstructAvatar));
  }

  private static void TryConstructAvatar()
  {
    if (AvatarSnapshot.avatar == null)
    {
      try
      {
        AvatarSnapshot.avatar = AvatarSelector.Create(AvatarSnapshot.avatarDesc) ?? AvatarSelector.Create("human", "male");
      }
      catch
      {
        AvatarSnapshot.avatar = AvatarSelector.Create("human", "male");
      }
      AvatarSnapshot.avatar.loadedCallback = new System.Action(AvatarSnapshot.PrepareAvatar);
    }
    else
      AvatarSnapshot.avatar.Update();
  }

  private static void PrepareAvatar()
  {
    AvatarSelector.ClearAvatar(AvatarSnapshot.avatar);
    AvatarSnapshot.avatar.obj.layer = AvatarSnapshot.Layer;
    AvatarSnapshot.avatar.obj.transform.parent = AvatarSnapshot.avatarGo.transform;
    AvatarSnapshot.avatar.obj.transform.localPosition = Vector3.zero;
    AvatarSnapshot.SetLayerToAll(AvatarSnapshot.avatarGo, AvatarSnapshot.Layer);
    Animation componentInChildren = AvatarSnapshot.avatar.obj.GetComponentInChildren<Animation>();
    if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
      componentInChildren.enabled = false;
    AvatarSnapshot.avatarConstructTimer.StopTiming();
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) AvatarSnapshot.avatarConstructTimer);
    AvatarSnapshot.OnEverythingIsSetup();
  }

  private static void SetLayerToAll(GameObject obj, int layer)
  {
    obj.layer = layer;
    for (int index = 0; index < obj.transform.childCount; ++index)
      AvatarSnapshot.SetLayerToAll(obj.transform.GetChild(index).gameObject, layer);
  }

  private static void DeleteSetup()
  {
    if ((UnityEngine.Object) AvatarSnapshot.avatarGo != (UnityEngine.Object) null)
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) AvatarSnapshot.avatarGo);
      AvatarSnapshot.avatarGo = (GameObject) null;
    }
    UnityEngine.Object.Destroy((UnityEngine.Object) AvatarSnapshot.lightSetup);
  }

  private static void OnEverythingIsSetup()
  {
    AvatarSnapshot.portraitCached121x165 = AvatarSnapshot.MakePortraitSnapshot(121, 165);
    AvatarSnapshot.DeleteSetup();
    AvatarSnapshot.DeploySnapshot();
  }

  private static Texture2D MakePortraitSnapshot(int width, int height)
  {
    if ((UnityEngine.Object) AvatarSnapshot.avatarGo != (UnityEngine.Object) null)
      AvatarSnapshot.avatarGo.transform.position = Vector3.zero;
    string key = AvatarSnapshot.avatarDesc.GetItem(AvatarItem.Race);
    Vector3 vector3;
    Quaternion quaternion;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (AvatarSnapshot.\u003C\u003Ef__switch\u0024map11 == null)
      {
        // ISSUE: reference to a compiler-generated field
        AvatarSnapshot.\u003C\u003Ef__switch\u0024map11 = new Dictionary<string, int>(2)
        {
          {
            "human",
            0
          },
          {
            "cylon",
            1
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (AvatarSnapshot.\u003C\u003Ef__switch\u0024map11.TryGetValue(key, out num))
      {
        if (num != 0)
        {
          if (num == 1)
            ;
        }
        else if (AvatarSnapshot.avatarDesc.GetItem(AvatarItem.Sex) == "female")
        {
          vector3 = new Vector3(0.045f, 1.424f, 0.333f);
          quaternion = Quaternion.Euler(0.0f, -167.39f, 0.0f);
          goto label_12;
        }
        else
        {
          vector3 = new Vector3(0.072f, 1.54f, 0.4f);
          quaternion = Quaternion.Euler(0.0f, -170.86f, 0.0f);
          goto label_12;
        }
      }
    }
    vector3 = new Vector3(0.1f, 1.85f, 0.58f);
    quaternion = Quaternion.Euler(0.0f, -170.06f, 0.0f);
label_12:
    GameObject gameObject = new GameObject("SnapshotCam");
    gameObject.transform.rotation = quaternion;
    gameObject.transform.position = vector3;
    Camera camera1 = gameObject.AddComponent<Camera>();
    camera1.clearFlags = CameraClearFlags.Color;
    camera1.backgroundColor = new Color(1f, 1f, 1f) * 0.0f;
    camera1.farClipPlane = 50000f;
    camera1.depth = 3f;
    camera1.cullingMask = 1 << AvatarSnapshot.Layer;
    camera1.enabled = false;
    camera1.nearClipPlane = 0.01f;
    camera1.renderingPath = RenderingPath.Forward;
    Texture2D texture2D = new Texture2D(width, height, TextureFormat.RGB24, false);
    Camera camera2 = camera1;
    List<Light> list = ((IEnumerable<Light>) (UnityEngine.Object.FindObjectsOfType(typeof (Light)) as Light[])).Where<Light>((Func<Light, bool>) (light => light.enabled)).ToList<Light>();
    using (List<Light>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.enabled = false;
    }
    AvatarSnapshot.ToggleSnapshotLights(true);
    RenderTexture renderTexture = new RenderTexture(width, height, 24);
    renderTexture.antiAliasing = 8;
    camera2.targetTexture = renderTexture;
    camera2.Render();
    RenderTexture.active = renderTexture;
    AvatarSnapshot.ToggleSnapshotLights(false);
    Rect source = new Rect(0.0f, 0.0f, (float) width, (float) height);
    texture2D.ReadPixels(source, 0, 0);
    texture2D.Apply();
    camera2.targetTexture = (RenderTexture) null;
    RenderTexture.active = (RenderTexture) null;
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) gameObject);
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) renderTexture);
    using (List<Light>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.enabled = true;
    }
    return texture2D;
  }

  private static void ToggleSnapshotLights(bool enable)
  {
    foreach (Behaviour componentsInChild in AvatarSnapshot.lightSetup.GetComponentsInChildren<Light>())
      componentsInChild.enabled = enable;
  }
}
