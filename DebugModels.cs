﻿// Decompiled with JetBrains decompiler
// Type: DebugModels
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Unity5AssetHandling;
using UnityEngine;

public class DebugModels : DebugBehaviour<DebugModels>
{
  private Vector2 scroll = new Vector2();
  private ShipListCard colonialShips;
  private ShipListCard cylonShips;
  private AssetRequest request;

  private void Start()
  {
    this.windowID = 24;
    this.SetSize(300f, 400f);
    this.colonialShips = StaticCards.Instance.Colonial;
    this.cylonShips = StaticCards.Instance.Cylon;
  }

  private void Update()
  {
    if (this.request == null || !this.request.IsDone)
      return;
    GameObject gameObject = (GameObject) Object.Instantiate(this.request.Asset);
    gameObject.name = this.GetType().ToString() + "Ship";
    Vector4 column = Camera.main.transform.localToWorldMatrix.GetColumn(2);
    Vector3 vector3 = new Vector3(column.x, column.y, column.z);
    gameObject.transform.position = Game.Me.Ship.Position + vector3 * 500f;
    foreach (Renderer componentsInChild in gameObject.GetComponentsInChildren<Renderer>())
    {
      foreach (Material material in componentsInChild.materials)
      {
        if (material.HasProperty("_RustColor"))
          material.SetColor("_RustColor", Color.clear);
      }
    }
    AudioSource[] componentsInChildren = gameObject.GetComponentsInChildren<AudioSource>();
    for (int index = 0; index < componentsInChildren.Length; ++index)
    {
      componentsInChildren[index].Stop();
      componentsInChildren[index].volume = 0.0f;
    }
    if ((Object) gameObject.GetComponent<Collider>() != (Object) null)
      Object.Destroy((Object) gameObject.GetComponent<Collider>());
    if ((Object) gameObject.GetComponent<Rigidbody>() != (Object) null)
      Object.Destroy((Object) gameObject.GetComponent<Rigidbody>());
    foreach (MeshFilter componentsInChild in gameObject.GetComponentsInChildren<MeshFilter>())
    {
      if (!componentsInChild.name.Contains("sticker"))
      {
        if ((Object) componentsInChild.GetComponent<Rigidbody>() != (Object) null)
          componentsInChild.GetComponent<Rigidbody>().detectCollisions = false;
        if ((Object) componentsInChild.GetComponent<Collider>() != (Object) null)
          Object.Destroy((Object) componentsInChild.GetComponent<Collider>());
        if ((Object) componentsInChild.GetComponent<Rigidbody>() != (Object) null)
          Object.Destroy((Object) componentsInChild.GetComponent<Rigidbody>());
      }
    }
    this.request = (AssetRequest) null;
  }

  protected override void WindowFunc()
  {
    TextAnchor alignment = GUI.skin.box.alignment;
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.Label("Colonial Ships:\n");
    foreach (ShipCard shipCard in this.colonialShips.ShipCards)
    {
      if (GUILayout.Button(shipCard.ItemGUICard.Name))
        this.request = AssetCatalogue.Instance.Request(shipCard.WorldCard.PrefabName + "_shop.prefab", false);
    }
    GUILayout.Label("Cylon Ships:\n");
    foreach (ShipCard shipCard in this.cylonShips.ShipCards)
    {
      if (GUILayout.Button(shipCard.ItemGUICard.Name))
        this.request = AssetCatalogue.Instance.Request(shipCard.WorldCard.PrefabName + "_shop.prefab", false);
    }
    GUILayout.EndScrollView();
    GUI.skin.box.alignment = alignment;
  }

  private void SetLayerToAll(GameObject obj, int layer)
  {
    obj.layer = layer;
    for (int index = 0; index < obj.transform.childCount; ++index)
      this.SetLayerToAll(obj.transform.GetChild(index).gameObject, layer);
  }
}
