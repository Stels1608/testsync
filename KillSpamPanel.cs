﻿// Decompiled with JetBrains decompiler
// Type: KillSpamPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class KillSpamPanel : GuiPanel
{
  private readonly Queue<KillSpamMessage> messages = new Queue<KillSpamMessage>();
  private const int VISIBLE_LINES = 4;
  private const float UPDATE_FREQUENCY = 0.25f;

  public KillSpamPanel(Vector2 size)
  {
    this.Size = size;
    this.AddChild((GuiElementBase) new Gui.Timer(0.25f));
  }

  public void AddMessage(KillSpamMessage message)
  {
    while (this.messages.Count >= 4)
      this.RemoveChild((GuiElementBase) this.messages.Dequeue());
    this.messages.Enqueue(message);
    this.AddChild((GuiElementBase) message);
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    this.UpdateMessages();
    int num = 0;
    using (Queue<KillSpamMessage>.Enumerator enumerator = this.messages.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.PositionY = (float) (num++ * 16);
    }
  }

  private void UpdateMessages()
  {
    while (this.messages.Count > 0 && this.messages.Peek().Expired)
      this.RemoveChild((GuiElementBase) this.messages.Dequeue());
  }
}
