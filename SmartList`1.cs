﻿// Decompiled with JetBrains decompiler
// Type: SmartList`1
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

public class SmartList<T> : IEnumerable<T>, IEnumerable where T : class, IServerItem
{
  protected List<T> items;

  public int Count
  {
    get
    {
      return this.items.Count;
    }
  }

  public T this[int index]
  {
    get
    {
      return this.items[index];
    }
  }

  public List<ushort> IDs
  {
    get
    {
      return this.items.ConvertAll<ushort>((Converter<T, ushort>) (item => item.ServerID));
    }
  }

  public SmartList()
  {
    this.items = new List<T>();
  }

  public SmartList(List<T> items)
  {
    this.items = items;
  }

  IEnumerator<T> IEnumerable<T>.GetEnumerator()
  {
    return (IEnumerator<T>) this.items.GetEnumerator();
  }

  IEnumerator IEnumerable.GetEnumerator()
  {
    return (IEnumerator) this.items.GetEnumerator();
  }

  public T GetByID(ushort id)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.Find(new Predicate<T>(new SmartList<T>.\u003CGetByID\u003Ec__AnonStorey4E() { id = id }.\u003C\u003Em__8));
  }

  public T Find(Predicate<T> predicate)
  {
    return this.items.Find(predicate);
  }

  public bool Contains(T item)
  {
    return this.items.Contains(item);
  }

  protected List<T> FilterList(Predicate<T> match)
  {
    return this.items.FindAll(match);
  }

  protected Acc FoldList<Acc>(SmartList<T>.FoldFun<Acc> foldFun, Acc startAcc)
  {
    Acc oldAcc = startAcc;
    using (List<T>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        T current = enumerator.Current;
        oldAcc = foldFun(current, oldAcc);
      }
    }
    return oldAcc;
  }

  protected virtual void Changed()
  {
  }

  protected virtual void Updated(T newItem, T oldItem)
  {
  }

  protected virtual void Removed(T item)
  {
  }

  public void _AddItems(IEnumerable<T> newItems)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SmartList<T>.\u003C_AddItems\u003Ec__AnonStorey4F itemsCAnonStorey4F = new SmartList<T>.\u003C_AddItems\u003Ec__AnonStorey4F();
    foreach (T newItem in newItems)
    {
      // ISSUE: reference to a compiler-generated field
      itemsCAnonStorey4F.newItem = newItem;
      // ISSUE: reference to a compiler-generated method
      int index = this.items.FindIndex(new Predicate<T>(itemsCAnonStorey4F.\u003C\u003Em__9));
      if (index >= 0)
      {
        T oldItem = this.items[index];
        this.items.RemoveAt(index);
        // ISSUE: reference to a compiler-generated field
        this.items.Insert(index, itemsCAnonStorey4F.newItem);
        // ISSUE: reference to a compiler-generated field
        this.Updated(itemsCAnonStorey4F.newItem, oldItem);
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        this.items.Add(itemsCAnonStorey4F.newItem);
        // ISSUE: reference to a compiler-generated field
        this.Updated(itemsCAnonStorey4F.newItem, (T) null);
      }
    }
    this.Changed();
  }

  public void _AddItems(SmartList<T> items)
  {
    this._AddItems((IEnumerable<T>) items.items);
  }

  public void _RemoveItems(IEnumerable<ushort> itemIDs)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SmartList<T>.\u003C_RemoveItems\u003Ec__AnonStorey50 itemsCAnonStorey50 = new SmartList<T>.\u003C_RemoveItems\u003Ec__AnonStorey50();
    foreach (ushort itemId in itemIDs)
    {
      // ISSUE: reference to a compiler-generated field
      itemsCAnonStorey50.itemID = itemId;
      // ISSUE: reference to a compiler-generated method
      int index = this.items.FindIndex(new Predicate<T>(itemsCAnonStorey50.\u003C\u003Em__A));
      if (index >= 0)
      {
        this.Removed(this.items[index]);
        this.items.RemoveAt(index);
      }
    }
    this.Changed();
  }

  public virtual void _Clear()
  {
    this.items.Clear();
  }

  public delegate Acc FoldFun<Acc>(T item, Acc oldAcc);
}
