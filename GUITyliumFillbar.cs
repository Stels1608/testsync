﻿// Decompiled with JetBrains decompiler
// Type: GUITyliumFillbar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System;
using UnityEngine;

internal class GUITyliumFillbar : GUIPanel
{
  private const float MAX_TYLIUM_COUNT = 20000f;
  private const float UPDATE_DELAY = 0.1f;
  private GUILabelNew tyliumCounter;
  private GUIImageNew filler;
  private float2 initialSize;
  private GUIImageNew darkFiller;
  private float level;
  private float timeToUpdate;

  public float Value
  {
    get
    {
      return this.level;
    }
    set
    {
      this.level = Mathf.Clamp01(value);
      this.darkFiller.SmartRect.Height = (1f - this.level) * this.initialSize.y;
      float2 position = this.filler.Position;
      position.x -= this.initialSize.x / 2f;
      position.y -= this.initialSize.y / 2f;
      this.darkFiller.Position = new float2(position.x + this.initialSize.x / 2f, position.y + this.darkFiller.SmartRect.Height / 2f);
    }
  }

  public override bool IsUpdated
  {
    get
    {
      return this.IsRendered;
    }
    set
    {
      throw new Exception("method call is prohibited");
    }
  }

  public GUITyliumFillbar()
  {
    this.IsRendered = true;
    JWindowDescription jwindowDescription = Loaders.LoadResource("GUI/TyliumFillbar/layout");
    GUIImageNew guiImageNew = (jwindowDescription["background"] as JImage).CreateGUIImageNew();
    this.root.Width = guiImageNew.SmartRect.Width;
    this.root.Height = guiImageNew.SmartRect.Height;
    this.AddPanel((GUIPanel) guiImageNew);
    this.tyliumCounter = (jwindowDescription["counter"] as JLabel).CreateGUILabelNew();
    this.AddPanel((GUIPanel) this.tyliumCounter);
    this.filler = (jwindowDescription["filler"] as JImage).CreateGUIImageNew();
    this.AddPanel((GUIPanel) this.filler);
    this.darkFiller = (jwindowDescription["fillerFiller"] as JImage).CreateGUIImageNew();
    this.initialSize = new float2(this.darkFiller.SmartRect.Width, this.darkFiller.SmartRect.Height);
    this.AddPanel((GUIPanel) this.darkFiller);
  }

  public override void Update()
  {
    base.Update();
    this.timeToUpdate -= Time.deltaTime;
    if ((double) this.timeToUpdate > 0.0)
      return;
    float num = Mathf.Clamp((float) Game.Me.Hold.Tylium, 0.0f, 20000f);
    this.tyliumCounter.Text = ((double) (int) Mathf.Clamp((float) ((double) Game.Me.Hold.Tylium / 20000.0 * 100.0), 0.0f, 100f)).ToString() + "%";
    this.Value = num / 20000f;
    this.timeToUpdate = 0.1f;
  }

  public override bool OnShowTooltip(float2 mousePosition)
  {
    if (base.OnShowTooltip(mousePosition))
      return true;
    if (!this.Contains(mousePosition))
      return false;
    this.SetTooltip("%$bgo.common.tylium_c%%$bgo.common.sem% " + Game.Me.Hold.Tylium.ToString());
    Game.TooltipManager.ShowTooltip(this.advancedTooltip);
    return true;
  }
}
