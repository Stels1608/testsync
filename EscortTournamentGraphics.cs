﻿// Decompiled with JetBrains decompiler
// Type: EscortTournamentGraphics
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class EscortTournamentGraphics : ITournamentGraphics
{
  public UnityEngine.Sprite EnterIconNormal
  {
    get
    {
      return ResourceLoader.Load<UnityEngine.Sprite>("GUI/Options/button_tournament_escort_normal");
    }
  }

  public UnityEngine.Sprite EnterIconOver
  {
    get
    {
      return ResourceLoader.Load<UnityEngine.Sprite>("GUI/Options/button_tournament_escort_over");
    }
  }

  public string LoadingScreen
  {
    get
    {
      return "GUI/LoadingScene/tournament_escort";
    }
  }
}
