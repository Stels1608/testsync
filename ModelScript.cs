﻿// Decompiled with JetBrains decompiler
// Type: ModelScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ModelScript : MonoBehaviour
{
  private SpaceObject _spaceObject;

  public SpaceObject SpaceObject
  {
    get
    {
      return this._spaceObject;
    }
    set
    {
      this._spaceObject = value;
      this.OnSpaceObjectInjected();
    }
  }

  protected virtual void OnSpaceObjectInjected()
  {
  }

  protected virtual void Awake()
  {
  }

  protected virtual void OnDestroy()
  {
  }

  protected virtual void ModelSetOrSwitched()
  {
  }
}
