﻿// Decompiled with JetBrains decompiler
// Type: Asteroid
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Unity5AssetHandling;
using UnityEngine;

[JsonClassDesc("radius", JsonName = "radius")]
[JsonClassDesc("type", JsonName = "type")]
[JsonClassDesc("rotationSpeed", JsonName = "rotationSpeed")]
[JsonClassDesc("dockingName", JsonName = "dockingName")]
public class Asteroid : SpaceObject
{
  private float radius = 1f;
  private float rotationSpeed = 3f;
  public string type = string.Empty;
  public Price price = new Price();
  public int island = -1;
  private const float SCALING_FACTOR = 0.05f;
  public string dockingName;
  private GameObject destroyedModel;

  public bool IsMinable { get; private set; }

  public bool WasScanned { get; private set; }

  public ItemCountable Resource { get; private set; }

  public override string ObjectName
  {
    get
    {
      return this.Name;
    }
  }

  public override string Name
  {
    get
    {
      return BsgoLocalization.Get("%$bgo.common.asteroid%");
    }
  }

  public float Radius
  {
    get
    {
      return this.radius;
    }
    set
    {
      this.radius = value;
    }
  }

  public bool HasResource
  {
    get
    {
      if (this.Resource != null)
        return this.Resource.Count > 0U;
      return false;
    }
  }

  public Asteroid()
    : this(0U)
  {
  }

  public Asteroid(string type)
    : this(117440512U | IdGenerator.GenerateId())
  {
    this.type = type;
  }

  public Asteroid(uint objectID)
    : base(objectID)
  {
    if (this.SpaceEntityType != SpaceEntityType.Asteroid)
      return;
    AsteroidGroupsManager.Instance.AddToAnAsteroidGroup(this);
  }

  protected override void OnLocate()
  {
    base.OnLocate();
    if (this.SpaceEntityType != SpaceEntityType.Asteroid || SectorEditorHelper.IsEditorLevel())
      return;
    AsteroidGroupsManager.Instance.UpdateAsteroidGroup(this);
  }

  public float GetRotationSpeed()
  {
    return this.rotationSpeed;
  }

  public void SetRotationSpeed(float rotationSpeed)
  {
    this.rotationSpeed = rotationSpeed;
    Revolving modelScript = this.GetModelScript<Revolving>();
    if (!((Object) modelScript != (Object) null))
      return;
    modelScript.Velocity = rotationSpeed;
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Position = r.ReadVector3();
    this.Radius = r.ReadSingle();
    this.rotationSpeed = r.ReadSingle();
    if (this is Planetoid)
      return;
    this.Scale = this.Radius * 0.05f;
  }

  public Asteroid Clone()
  {
    Asteroid asteroid1 = new Asteroid(this.type);
    asteroid1.radius = this.radius;
    asteroid1.Position = this.Position;
    Asteroid asteroid2 = asteroid1;
    Vector3 vector3 = asteroid2.Position + new Vector3(10f, 0.0f, 0.0f);
    asteroid2.Position = vector3;
    asteroid1.Rotation = this.Rotation;
    asteroid1.Scale = this.Scale;
    asteroid1.rotationSpeed = this.rotationSpeed;
    asteroid1.SetDistance(this.GetDistance());
    return asteroid1;
  }

  private void SetDeathExplosionLOD()
  {
    int level = this.Root.GetComponentInChildren<Body>().Level;
    if (level >= 0)
    {
      this.destroyedModel.GetComponent<ParticleLODShuriken>().InheritedLODLevel = level;
    }
    else
    {
      int num = 1000;
      Debug.LogError((object) ("LOD level returned as null, setting to " + (object) num));
      this.destroyedModel.GetComponent<ParticleLODShuriken>().InheritedLODLevel = num;
    }
  }

  private AssetRequest GetEffect()
  {
    AsteroidExplosion componentInChildren = this.Root.GetComponentInChildren<AsteroidExplosion>();
    if (!((Object) componentInChildren == (Object) null) && componentInChildren.PrefabName != null)
      return AssetCatalogue.Instance.Request(componentInChildren.PrefabName, true);
    Log.DebugInfo((object) "Asteroid explosion effect not found");
    return (AssetRequest) null;
  }

  public override void Remove(RemovingCause removingCause)
  {
    if (this.SpaceEntityType == SpaceEntityType.Asteroid)
      AsteroidGroupsManager.Instance.RemoveFromAnAsteroidGroup(this);
    if (removingCause == RemovingCause.Death)
      this.InstantiateDestructionModel();
    base.Remove(removingCause);
  }

  private void InstantiateDestructionModel()
  {
    AssetRequest effect = this.GetEffect();
    if (effect == null || !effect.IsDone || effect.Asset == (Object) null)
      return;
    this.destroyedModel = Object.Instantiate(effect.Asset) as GameObject;
    if ((Object) this.destroyedModel == (Object) null)
      return;
    this.destroyedModel.transform.position = this.Position;
    this.destroyedModel.transform.rotation = this.Rotation;
    this.SetDeathExplosionLOD();
    foreach (Component componentsInChild1 in this.Root.GetComponentsInChildren<Renderer>())
    {
      Renderer component1 = componentsInChild1.GetComponent<Renderer>();
      if (component1.enabled)
      {
        foreach (Component componentsInChild2 in this.destroyedModel.GetComponentsInChildren<Renderer>())
        {
          Renderer component2 = componentsInChild2.GetComponent<Renderer>();
          if (!((Object) component2 == (Object) null))
            component2.material.SetColor("_Color", component1.sharedMaterial.color);
        }
        break;
      }
    }
    this.destroyedModel.transform.GetComponent<ParticleSystem>().Play();
    for (int index = 0; index < this.destroyedModel.transform.childCount; ++index)
    {
      Transform child = this.destroyedModel.transform.GetChild(index);
      if (child.name == "asteroidExplosion_rockChunksA" || child.name == "asteroidExplosion_rockChunksB")
      {
        ParticleSystem component = child.GetComponent<ParticleSystem>();
        float num1 = this.Root.transform.localScale.x * Mathf.Sin(component.startSize);
        if ((double) num1 < 0.75)
          num1 = 0.75f;
        else if ((double) num1 > 1.0)
          num1 = 1f;
        int count = (int) ((double) this.Root.transform.localScale.x * (double) num1);
        float num2 = this.Root.transform.localScale.x / (float) (count + 2);
        component.startSize = num2;
        component.Emit(count);
      }
    }
  }

  public override void InitModelScripts()
  {
    Revolving modelScript = this.GetModelScript<Revolving>();
    if ((Object) modelScript != (Object) null)
      modelScript.Velocity = this.rotationSpeed;
    this.GetEffect();
    base.InitModelScripts();
  }

  public void InformBeingScanned(ItemCountable resource, bool isMinable, Price miningPrice)
  {
    this.Resource = resource;
    this.IsMinable = isMinable;
    this.price = miningPrice;
    this.WasScanned = true;
    Scanned modelScript = this.GetModelScript<Scanned>();
    if ((Object) modelScript != (Object) null)
    {
      modelScript.BeginScan(this.Resource, (SpaceObject) SpaceLevel.GetLevel().GetPlayerShip(), this.GetScanningColor());
      modelScript.Complete = new Scanned.ScanningCompletedCallback(this.ScanningComplete);
    }
    else
      this.ScanningComplete();
  }

  public void ScanningComplete()
  {
    FacadeFactory.GetInstance().SendMessage(Message.AsteroidScanned, (object) this);
    FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new AsteroidMiningNotification(this.Resource));
    if (!this.IsMinable || !this.HasResource)
      return;
    BuyBoxUi buyBox = MessageBoxFactory.CreateBuyBox();
    if (this.price.IsEnoughInHangar())
      buyBox.SetContent((OnMessageBoxClose) (actionType =>
      {
        if (actionType != MessageBoxActionType.Ok)
          return;
        GameProtocol.GetProtocol().RequestMining(this.ObjectID);
      }), "%$bgo.mining_facility.message_box_title%", "%$bgo.mining_facility.have_money%", "%$bgo.mining_facility.order%", "%$bgo.mining_facility.cancel%", this.price, true, 0U);
    else
      buyBox.SetContent((OnMessageBoxClose) (actionType => {}), "%$bgo.mining_facility.message_box_title%", "%$bgo.mining_facility.dont_have_money%", "%$bgo.mining_facility.order%", "%$bgo.mining_facility.cancel%", this.price, false, 0U);
  }

  public Color GetScanningColor()
  {
    if (!this.HasResource)
      return new Color(0.76f, 0.0f, 0.0f);
    switch ((ResourceType) this.Resource.CardGUID)
    {
      case ResourceType.Water:
        return new Color(0.0f, 0.38f, 1f);
      case ResourceType.Titanium:
        return new Color(0.69f, 0.28f, 1f);
      case ResourceType.Tylium:
        return new Color(1f, 0.67f, 0.0f);
      case ResourceType.Cubits:
        return Tools.Color(141, 22, 154);
      default:
        Log.Add("Unknown resource type in asteroid " + (object) this.Resource.GetType());
        return new Color(0.76f, 0.0f, 0.0f);
    }
  }
}
