﻿// Decompiled with JetBrains decompiler
// Type: DebugItems
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugItems : DebugBehaviour<DebugItems>
{
  private string str = string.Empty;
  private Vector2 scroll = new Vector2();
  private int value = 10000;
  private string bonus = string.Empty;
  private string bonusValue = string.Empty;
  private string bonusLevelValue = string.Empty;
  private int bonusLevel = 1;
  private string customResource = string.Empty;

  private void Start()
  {
    this.windowID = 7;
    this.SetSize(430f, 200f);
  }

  protected override void WindowFunc()
  {
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.BeginHorizontal(GUILayout.Width(200f));
    GUILayout.Label("Value:");
    this.str = GUILayout.TextField(this.value.ToString());
    int.TryParse(this.str, out this.value);
    GUILayout.EndHorizontal();
    GUILayout.BeginVertical(GUILayout.Width(400f));
    GUILayout.Label("Press to add Resource:");
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Cubits"))
      this.AddResource("resource_cubits");
    if (GUILayout.Button("Tylium"))
      this.AddResource("resource_tylium");
    if (GUILayout.Button("Titanium"))
      this.AddResource("resource_titanium");
    if (GUILayout.Button("Water"))
      this.AddResource("resource_water");
    if (GUILayout.Button("Merit"))
      this.AddResource("resource_token");
    if (GUILayout.Button("Experience"))
      this.AddExperience();
    GUILayout.EndHorizontal();
    GUILayout.Label("Press to add Map Parts:");
    GUILayout.BeginHorizontal();
    if (GUILayout.Button("Alpha"))
      this.AddMapPart("alpha");
    if (GUILayout.Button("Beta"))
      this.AddMapPart("beta");
    if (GUILayout.Button("Gamma"))
      this.AddMapPart("gamma");
    if (GUILayout.Button("Delta"))
      this.AddMapPart("delta");
    GUILayout.EndHorizontal();
    GUILayout.Label("Custom resource:");
    GUILayout.BeginHorizontal();
    GUILayout.Label("Key:");
    this.customResource = GUILayout.TextField(this.customResource);
    if (GUILayout.Button("Add custom resource"))
      this.AddResource(this.customResource);
    GUILayout.EndHorizontal();
    GUILayout.Label("Hold:");
    foreach (ShipItem si in (IEnumerable<ShipItem>) Game.Me.Hold)
      this.DrawItem(si, false);
    GUILayout.Label("Locker:");
    foreach (ShipItem si in (IEnumerable<ShipItem>) Game.Me.Locker)
      this.DrawItem(si, true);
    GUILayout.Label("Ships:");
    foreach (HangarShip ship in Game.Me.Hangar.Ships)
    {
      if ((bool) ship.IsLoaded)
      {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("UpL", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
          ship.Upgrade();
        if (GUILayout.Button("+Dr", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
          ship.Repair((float) this.value, false);
        if (GUILayout.Button("-Dr", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
          ship.Repair((float) -this.value, false);
        GUILayout.Label(string.Format("S{0}-L{1}D{2}:{3}", (object) ship.ServerID, (object) ship.Card.Level, (object) (int) ship.Durability, (object) ship.GUICard.Key));
        GUILayout.EndHorizontal();
        foreach (ShipSlot slot in ship.Slots)
        {
          if (slot.System != null && (bool) slot.IsLoaded)
          {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("UpL", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
              slot.System.Upgrade((int) slot.System.Card.Level + 1);
            if (GUILayout.Button("+Dr", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
              PlayerProtocol.GetProtocol().RepairSystem(slot.System, (float) this.value, false);
            if (GUILayout.Button("-Dr", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
              PlayerProtocol.GetProtocol().RepairSystem(slot.System, (float) -this.value, false);
            GUILayout.Label(string.Format("{0}-L{1}D{2}:{3}", (object) slot.ServerID, (object) slot.System.Card.Level, (object) (int) slot.System.Durability, (object) slot.System.ItemGUICard.Key));
            GUILayout.EndHorizontal();
          }
        }
      }
    }
    GUILayout.Label("BONUS:");
    GUILayout.BeginHorizontal();
    GUILayout.Label("Key:");
    this.bonusValue = GUILayout.TextField(this.bonus);
    GUILayout.Label("Level:");
    this.bonusLevelValue = GUILayout.TextField(this.bonusLevel.ToString());
    this.bonus = this.bonusValue;
    int.TryParse(this.bonusLevelValue, out this.bonusLevel);
    if (GUILayout.Button("Apply Bonus"))
      this.DoCommand("bonus", this.bonus, this.bonusLevel.ToString());
    GUILayout.EndHorizontal();
    GUILayout.EndVertical();
    GUILayout.EndScrollView();
  }

  private void DrawItem(ShipItem si, bool IsLocker)
  {
    string str = !IsLocker ? "hold_remove" : "locker_remove";
    ShipSystem system = si as ShipSystem;
    ItemCountable itemCountable = si as ItemCountable;
    if (!(bool) si.IsLoaded)
      return;
    if (system != null)
    {
      GUILayout.BeginHorizontal();
      if (GUILayout.Button("UpL", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
        system.Upgrade((int) system.Card.Level + 1);
      if (GUILayout.Button("Dla", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
        this.DoCommand(str, si.ServerID.ToString());
      if (GUILayout.Button("+Dr", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
        PlayerProtocol.GetProtocol().RepairSystem(system, (float) this.value, false);
      if (GUILayout.Button("-Dr", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
        PlayerProtocol.GetProtocol().RepairSystem(system, (float) -this.value, false);
      GUILayout.Label(string.Format("{0}-L{1}D{2}:{3}", (object) si.ServerID, (object) system.Card.Level, (object) (int) system.Durability, (object) system.ItemGUICard.Key));
      GUILayout.EndHorizontal();
    }
    if (itemCountable == null)
      return;
    GUILayout.BeginHorizontal();
    if (itemCountable.Card.IsAugment)
    {
      if (GUILayout.Button("Use", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
        itemCountable.Use();
    }
    if (IsLocker)
    {
      if (GUILayout.Button("Add", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
        this.DoCommand("consumable", itemCountable.ItemGUICard.Key, this.value.ToString());
    }
    if (GUILayout.Button("Dla", new GUILayoutOption[1]{ GUILayout.Width(35f) }))
      this.DoCommand(str, si.ServerID.ToString());
    GUILayout.Label(string.Format("{0}-({1}){2}", (object) si.ServerID, (object) itemCountable.count, (object) itemCountable.ItemGUICard.Key));
    GUILayout.EndHorizontal();
  }

  private void DoCommand(params string[] p)
  {
    DebugProtocol.GetProtocol().CommandList(p);
  }

  private void AddResource(string resource)
  {
    this.DoCommand("resource", resource, this.value.ToString(), "false", "false");
  }

  private void AddMapPart(string mapType)
  {
    this.DoCommand("map_part", mapType, this.value.ToString());
  }

  private void AddExperience()
  {
    this.DoCommand("experience", this.value.ToString());
  }
}
