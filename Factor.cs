﻿// Decompiled with JetBrains decompiler
// Type: Factor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class Factor : IServerItem, IProtocolRead
{
  private ushort serverID;
  public FactorSource Source;
  public FactorType Type;
  public float Value;
  public uint EndTime;

  public ushort ServerID
  {
    get
    {
      return this.serverID;
    }
  }

  public void Read(BgoProtocolReader r)
  {
    this.serverID = r.ReadUInt16();
    this.Type = (FactorType) r.ReadByte();
    this.Source = (FactorSource) r.ReadByte();
    this.Value = r.ReadSingle();
    this.EndTime = r.ReadUInt32();
  }
}
