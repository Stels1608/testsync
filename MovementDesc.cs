﻿// Decompiled with JetBrains decompiler
// Type: MovementDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class MovementDesc : IProtocolWrite, IProtocolRead
{
  private List<MovementDesc.Message> messages = new List<MovementDesc.Message>();
  public const float nearDistance = 0.0f;
  public const float farDistance = 100f;
  public const float stoppedNearTime = 1f;
  public const float stoppedFarTime = 3f;
  public const float stopNearTime = 1f;
  public const float stopFarTime = 2f;
  public const float moveNearTime = 0.5f;
  public const float moveFarTime = 1f;
  private ushort messageType;

  public MovementDesc()
  {
  }

  public MovementDesc(ushort messageType)
  {
    this.messageType = messageType;
  }

  public void Write(BgoProtocolWriter pw)
  {
    pw.Write(this.messageType);
    pw.Write((ushort) this.messages.Count);
    using (List<MovementDesc.Message>.Enumerator enumerator = this.messages.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MovementDesc.Message current = enumerator.Current;
        pw.Write((IProtocolWrite) current);
      }
    }
    this.messages.Clear();
  }

  public void Read(BgoProtocolReader pr)
  {
    ushort num = pr.ReadUInt16();
    for (ushort index = 0; (int) index < (int) num; ++index)
      this.messages.Add(pr.ReadDesc<MovementDesc.Message>());
  }

  public bool HaveData()
  {
    return this.messages.Count > 0;
  }

  public void AddMesage(MovementDesc.Message m)
  {
    this.messages.Add(m);
  }

  public List<MovementDesc.Message> GetMessages()
  {
    return this.messages;
  }

  public enum MovementType : byte
  {
    stopped,
    stop,
    move,
    debugMove,
    jump,
  }

  public enum LodType : byte
  {
    none = 0,
    near = 1,
    far = 2,
    all = 255,
  }

  public class Message : IProtocolWrite, IProtocolRead
  {
    public uint objectID;
    public IProtocolWrite moveWriteDesc;
    public IProtocolRead moveReadDesc;
    public MovementDesc.LodType lod;

    public Message()
    {
    }

    public Message(uint objectID, IProtocolWrite moveDesc, bool near, bool far)
    {
      this.objectID = objectID;
      this.moveWriteDesc = moveDesc;
      this.lod = (MovementDesc.LodType) ((int) (byte) (0 | (!near ? 1 : 0)) | (!far ? 2 : 0));
    }

    public void Write(BgoProtocolWriter pw)
    {
      pw.Write(this.objectID);
      pw.Write((byte) this.lod);
      pw.Write(this.moveWriteDesc);
    }

    public void Read(BgoProtocolReader pr)
    {
      this.objectID = pr.ReadUInt32();
      this.lod = (MovementDesc.LodType) pr.ReadByte();
      this.moveReadDesc = this.GetDescByType(pr);
    }

    private IProtocolRead GetDescByType(BgoProtocolReader pr)
    {
      switch (pr.ReadByte())
      {
        case 0:
          return (IProtocolRead) pr.ReadDesc<MovementDesc.StoppedDesc>();
        case 1:
          return (IProtocolRead) pr.ReadDesc<MovementDesc.StopDesc>();
        case 2:
          return (IProtocolRead) pr.ReadDesc<MovementDesc.MoveDesc>();
        case 3:
          return (IProtocolRead) pr.ReadDesc<MovementDesc.DebugMoveDesc>();
        case 4:
          return (IProtocolRead) pr.ReadDesc<MovementDesc.JumpDesc>();
        default:
          throw new NullReferenceException("MovementDesc.Message.GetDescByType");
      }
    }
  }

  public class StoppedDesc : IProtocolWrite, IProtocolRead
  {
    public Vector3 point;
    public Vector3 direction;

    public void Write(BgoProtocolWriter pw)
    {
      pw.Write((byte) 0);
      pw.Write(this.point);
      pw.Write(this.direction);
    }

    public void Read(BgoProtocolReader pr)
    {
      this.point = pr.ReadVector3();
      this.direction = pr.ReadVector3();
    }
  }

  public class StopDesc : IProtocolWrite, IProtocolRead
  {
    public Vector3 point;
    public Vector3 direction;
    public int time;

    public void Write(BgoProtocolWriter pw)
    {
      pw.Write((byte) 1);
      pw.Write(this.point);
      pw.Write(this.direction);
      pw.Write(this.time);
    }

    public void Read(BgoProtocolReader pr)
    {
      this.point = pr.ReadVector3();
      this.direction = pr.ReadVector3();
      this.time = pr.ReadInt32();
    }
  }

  public class MoveDesc : IProtocolWrite, IProtocolRead
  {
    public Vector3 point;
    public Vector3 direction;
    public int time;
    public float speed;

    public void Write(BgoProtocolWriter pw)
    {
      pw.Write((byte) 2);
      pw.Write(this.point);
      pw.Write(this.direction);
      pw.Write(this.time);
      pw.Write(this.speed);
    }

    public void Read(BgoProtocolReader pr)
    {
      this.point = pr.ReadVector3();
      this.direction = pr.ReadVector3();
      this.time = pr.ReadInt32();
      this.speed = pr.ReadSingle();
    }
  }

  public class DebugMoveDesc : IProtocolWrite, IProtocolRead
  {
    public Vector3 point;

    public void Write(BgoProtocolWriter pw)
    {
      pw.Write((byte) 3);
      pw.Write(this.point);
    }

    public void Read(BgoProtocolReader pr)
    {
      this.point = pr.ReadVector3();
    }
  }

  public class JumpDesc : IProtocolWrite, IProtocolRead
  {
    public Vector3 point;

    public void Write(BgoProtocolWriter pw)
    {
      pw.Write((byte) 4);
      pw.Write(this.point);
    }

    public void Read(BgoProtocolReader pr)
    {
      this.point = pr.ReadVector3();
    }
  }
}
