﻿// Decompiled with JetBrains decompiler
// Type: Chat.ColorScheme
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Chat
{
  public static class ColorScheme
  {
    public static Color DefaultTextColor
    {
      get
      {
        return Color.white;
      }
    }

    public static Color DefaultNameColor
    {
      get
      {
        return Color.yellow;
      }
    }

    public static Color WhisperNameColor
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255(252U, 3U, 202U);
      }
    }

    public static Color WhisperTextColor
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255(254U, 188U, 241U);
      }
    }

    public static Color Danger
    {
      get
      {
        return Color.red;
      }
    }

    public static Color Ouch
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255(122U, 163U, 238U);
      }
    }

    public static Color Nice
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255(122U, 163U, 238U);
      }
    }

    public static Color Ok
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255(207U, 230U, 236U);
      }
    }

    public static Color BeWare
    {
      get
      {
        return Color.yellow;
      }
    }

    public static Color FleetGlobalNameColor
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255(166U, 76U, 5U);
      }
    }

    public static Color FleetGlobalTextColor
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255((uint) byte.MaxValue, 128U, 64U);
      }
    }

    public static Color SystemGlobalNameColor
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255(5U, 166U, 76U);
      }
    }

    public static Color SystemGlobalTextColor
    {
      get
      {
        return ColorScheme.DefaultTextColor;
      }
    }

    public static Color FleetLocalNameColor
    {
      get
      {
        return ColorScheme.FleetGlobalNameColor;
      }
    }

    public static Color FleetLocalTextColor
    {
      get
      {
        return ColorScheme.FleetGlobalTextColor;
      }
    }

    public static Color SystemLocalNameColor
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255((uint) byte.MaxValue, (uint) byte.MaxValue, 0U);
      }
    }

    public static Color SystemLocalTextColor
    {
      get
      {
        return ColorScheme.DefaultTextColor;
      }
    }

    public static Color OpenNameColorDefault
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255((uint) byte.MaxValue, 128U, 0U);
      }
    }

    public static Color OpenNameColorColonial
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255(120U, 180U, (uint) byte.MaxValue);
      }
    }

    public static Color OpenNameColorCylon
    {
      get
      {
        return ColorUtility.MakeColorFrom0To255(226U, 147U, 14U);
      }
    }

    public static Color WingNameColor
    {
      get
      {
        return Color.green;
      }
    }

    public static Color WingTextColor
    {
      get
      {
        return ColorScheme.DefaultTextColor;
      }
    }

    public static Color FleetName(bool global)
    {
      if (global)
        return ColorScheme.FleetGlobalNameColor;
      return ColorScheme.FleetLocalNameColor;
    }

    public static Color FleetText(bool global)
    {
      if (global)
        return ColorScheme.FleetGlobalTextColor;
      return ColorScheme.FleetLocalTextColor;
    }

    public static Color SystemName(bool global)
    {
      if (global)
        return ColorScheme.SystemGlobalNameColor;
      return ColorScheme.SystemLocalNameColor;
    }

    public static Color SystemText(bool global)
    {
      if (global)
        return ColorScheme.SystemGlobalTextColor;
      return ColorScheme.SystemLocalTextColor;
    }

    public static Color OpenName(Faction faction)
    {
      if (faction != Faction.Colonial && faction != Faction.Cylon)
        return ColorScheme.OpenNameColorDefault;
      if (faction == Faction.Colonial)
        return ColorScheme.OpenNameColorColonial;
      return ColorScheme.OpenNameColorCylon;
    }
  }
}
