﻿// Decompiled with JetBrains decompiler
// Type: Chat.ChannelName
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Chat
{
  public class ChannelName
  {
    public static readonly ChannelName FleetGlobal = new ChannelName(0, "%$bgo.chat.long_channel_name_fleet_global%");
    public static readonly ChannelName OpenGlobal = new ChannelName(1, "%$bgo.chat.long_channel_name_open_global%");
    public static readonly ChannelName SystemGlobal = new ChannelName(2, "%$bgo.chat.long_channel_name_system_global%");
    public static readonly ChannelName FleetLocal = new ChannelName(3, "%$bgo.chat.long_channel_name_fleet_local%");
    public static readonly ChannelName OpenLocal = new ChannelName(4, "%$bgo.chat.long_channel_name_open_local%");
    public static readonly ChannelName SystemLocal = new ChannelName(5, "%$bgo.chat.long_channel_name_system_local%");
    public static readonly ChannelName Squadron = new ChannelName(6, "%$bgo.chat.button_text_squadron%");
    public static readonly ChannelName Wing = new ChannelName(7, "%$bgo.chat.button_text_wing%");
    public static readonly ChannelName Officer = new ChannelName(8, "%$bgo.chat.button_text_officer%");
    private int id = -1;
    public string Name;

    public bool IsLocal
    {
      get
      {
        if (this.id >= ChannelName.FleetLocal.id)
          return this.id <= ChannelName.SystemLocal.id;
        return false;
      }
    }

    public bool IsGlobal
    {
      get
      {
        if (this.id >= ChannelName.FleetGlobal.id)
          return this.id <= ChannelName.SystemGlobal.id;
        return false;
      }
    }

    public ChannelName(int id, string name)
    {
      this.id = id;
      this.Name = name;
    }

    public override string ToString()
    {
      return this.Name;
    }
  }
}
