﻿// Decompiled with JetBrains decompiler
// Type: Chat.Commands
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Chat
{
  public static class Commands
  {
    public static string Help = "/h";
    public static string HelpExtended = "/help";
    public static string Syfy = "/syfy";
    public static string Scifi = "/scifi";
    public static string Whisper = "/w";
    public static string WhisperExtended = "/whisper";
    public static string Invite = "/i";
    public static string InviteExtended = "/invite";
    public static string Dismiss = "/d";
    public static string DismissExtended = "/dismiss";
    public static string Mute = "/m";
    public static string MuteExtended = "/mute";
    public static string IgnoreExtended = "/ignore";
    public static string MuteList = "/ml";
    public static string MuteListExtended = "/mutelist";
    public static string IgnoreListExtended = "/ignorelist";
    public static string Global = "/g";
    public static string GlobalExtended = "/global";
    public static string Local = "/l";
    public static string LocalExtended = "/local";
    public static string ShowPrefix = "/sp";
    public static string ShowPrefixExtended = "/showPrefix";
    public static string HidePrefix = "/hp";
    public static string HidePrefixExtended = "/hidePrefix";
    public static string WingOfficers = "/o";
    public static string WingOfficersExtended = "/officers";
    public static string FleetGlobal = "/fg";
    public static string FleetLocal = "/fl";
    public static string FleetGlobalExtended = "/fleetGlobal";
    public static string FleetLocalExtended = "/fleetLocal";
    public static string OpenGlobal = "/og";
    public static string OpenLocal = "/ol";
    public static string OpenGlobalExtended = "/openGlobal";
    public static string OpenLocalExtended = "/openLocal";
    public static string SystemGlobal = "/sg";
    public static string SystemLocal = "/sl";
    public static string SystemGlobalExtended = "/systemGlobal";
    public static string SystemLocalExtended = "/systemLocal";
    public static string DoNotDisturb = "/dnd";

    public static string Get(ChannelName channel)
    {
      if (channel == ChannelName.FleetGlobal)
        return Commands.FleetGlobal;
      if (channel == ChannelName.FleetLocal)
        return Commands.FleetLocal;
      if (channel == ChannelName.OpenGlobal)
        return Commands.OpenGlobal;
      if (channel == ChannelName.OpenLocal)
        return Commands.OpenLocal;
      if (channel == ChannelName.SystemGlobal)
        return Commands.SystemGlobal;
      if (channel == ChannelName.SystemLocal)
        return Commands.SystemLocal;
      if (channel == ChannelName.Officer)
        return Commands.WingOfficers;
      return string.Empty;
    }

    public static string GetEx(ChannelName channel)
    {
      if (channel == ChannelName.FleetGlobal)
        return Commands.FleetGlobalExtended;
      if (channel == ChannelName.FleetLocal)
        return Commands.FleetLocalExtended;
      if (channel == ChannelName.OpenGlobal)
        return Commands.OpenGlobalExtended;
      if (channel == ChannelName.OpenLocal)
        return Commands.OpenLocalExtended;
      if (channel == ChannelName.SystemGlobal)
        return Commands.SystemGlobalExtended;
      if (channel == ChannelName.Officer)
        return Commands.WingOfficersExtended;
      return string.Empty;
    }
  }
}
