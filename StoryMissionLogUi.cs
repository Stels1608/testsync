﻿// Decompiled with JetBrains decompiler
// Type: StoryMissionLogUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public abstract class StoryMissionLogUi : Window
{
  public System.Action OnDialogButtonPressed = (System.Action) (() => {});
  public System.Action OnSkipButtonPressed = (System.Action) (() => {});
  [SerializeField]
  private ComboBoxHeaderUi comboBoxHeader;
  [SerializeField]
  private GameObject content;

  public bool IsCollapsed
  {
    get
    {
      return this.comboBoxHeader.IsCollapsed;
    }
    set
    {
      this.comboBoxHeader.IsCollapsed = value;
    }
  }

  public override bool IsBigWindow
  {
    get
    {
      return false;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.comboBoxHeader.OnCollapseToggled += (System.Action<bool>) (isCollapsed => this.ShowContent(!isCollapsed));
    this.IsCollapsed = false;
  }

  private void ShowContent(bool show)
  {
    this.content.SetActive(show);
  }

  protected abstract void ColorizeMissionObjectiveText(ref string objectiveText);

  protected abstract void SetMissionObjectivesText(string objectiveText);

  protected abstract void SetDialogButtonText(string dialogButtonText);

  protected abstract void SetSkipButtonText(string skipButtonText);

  public void SetupDialogButton(string dialogButtonTextKey, System.Action onClick)
  {
    this.SetDialogButtonText(Tools.ParseMessage(dialogButtonTextKey));
    this.OnDialogButtonPressed += onClick;
  }

  public void SetupSkipButton(string skipButtonTextKey, System.Action onClick)
  {
    this.SetSkipButtonText(Tools.ParseMessage(skipButtonTextKey));
    this.OnSkipButtonPressed += onClick;
  }

  public void SetupMissionObjectiveText(string objectiveTextKey)
  {
    string message = Tools.ParseMessage(objectiveTextKey);
    this.ColorizeMissionObjectiveText(ref message);
    this.SetMissionObjectivesText(message);
  }

  public void DialogButtonClick()
  {
    this.OnDialogButtonPressed();
  }

  public void SkipButtonClick()
  {
    this.OnSkipButtonPressed();
  }
}
