﻿// Decompiled with JetBrains decompiler
// Type: StickerListCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class StickerListCard : Card
{
  public Sticker[] StickersCylon;
  public Sticker[] StickersColonial;

  public StickerListCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public Sticker GetSticker(int stickerID, Faction faction)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return Array.Find<Sticker>(this.GetStickers(faction), new Predicate<Sticker>(new StickerListCard.\u003CGetSticker\u003Ec__AnonStorey5D() { stickerID = stickerID }.\u003C\u003Em__29));
  }

  public Texture2D GetTexture(ushort stickerID, Faction faction)
  {
    Sticker sticker = this.GetSticker((int) stickerID, faction);
    if (sticker != null)
      return (Texture2D) ResourceLoader.Load(sticker.Texture);
    return (Texture2D) null;
  }

  public Sticker[] GetStickers(Faction faction)
  {
    switch (faction)
    {
      case Faction.Colonial:
        return this.StickersColonial;
      case Faction.Cylon:
        return this.StickersCylon;
      default:
        Debug.LogError((object) ("There is no Stickers for this faction: " + (object) faction));
        return (Sticker[]) null;
    }
  }

  public override void Read(BgoProtocolReader r)
  {
    this.StickersColonial = r.ReadDescArray<Sticker>();
    this.StickersCylon = r.ReadDescArray<Sticker>();
  }
}
