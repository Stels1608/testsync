﻿// Decompiled with JetBrains decompiler
// Type: SystemsStatsGenerator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public static class SystemsStatsGenerator
{
  private static readonly SystemsStatsGenerator.StatInfoDesc IGNORE = new SystemsStatsGenerator.StatInfoDesc("Unknown stat", "???", false);
  private static readonly Dictionary<ObjectStat, string> localizationTags = new Dictionary<ObjectStat, string>();
  private static readonly List<MetaStat> metaStats = new List<MetaStat>();

  static SystemsStatsGenerator()
  {
    SystemsStatsGenerator.metaStats.Add(new MetaStat(ObjectStat.MetaWeaponCooldown, new ObjectStat[8]
    {
      ObjectStat.HeavyMissileCooldown,
      ObjectStat.LightMissileCooldown,
      ObjectStat.MiningCooldown,
      ObjectStat.CannonCooldown,
      ObjectStat.MissileCooldown,
      ObjectStat.ShotgunCooldown,
      ObjectStat.KillCannonCooldown,
      ObjectStat.MachineGunCooldown
    }));
    SystemsStatsGenerator.metaStats.TrimExcess();
    SystemsStatsGenerator.localizationTags[ObjectStat.MinRange] = "minrange";
    SystemsStatsGenerator.localizationTags[ObjectStat.MaxRange] = "maxrange";
    SystemsStatsGenerator.localizationTags[ObjectStat.OptimalRange] = "optimalrange";
    SystemsStatsGenerator.localizationTags[ObjectStat.ArmorPiercing] = "armorpiercing";
    SystemsStatsGenerator.localizationTags[ObjectStat.Duration] = "duration";
    SystemsStatsGenerator.localizationTags[ObjectStat.Cooldown] = "cooldown";
    SystemsStatsGenerator.localizationTags[ObjectStat.PowerPointCost] = "powerpointcost";
    SystemsStatsGenerator.localizationTags[ObjectStat.DamageLow] = "dmglow";
    SystemsStatsGenerator.localizationTags[ObjectStat.DamageHigh] = "dmghigh";
    SystemsStatsGenerator.localizationTags[ObjectStat.DrainLow] = "drainlow";
    SystemsStatsGenerator.localizationTags[ObjectStat.DrainHigh] = "drainhigh";
    SystemsStatsGenerator.localizationTags[ObjectStat.Accuracy] = "accuracy";
    SystemsStatsGenerator.localizationTags[ObjectStat.CriticalOffense] = "criticaloffense";
    SystemsStatsGenerator.localizationTags[ObjectStat.Angle] = "angle";
    SystemsStatsGenerator.localizationTags[ObjectStat.DamageMining] = "damagemining";
    SystemsStatsGenerator.localizationTags[ObjectStat.FlareRange] = "flarerange";
    SystemsStatsGenerator.localizationTags[ObjectStat.HullPointRestore] = "hullpointrestore";
    SystemsStatsGenerator.localizationTags[ObjectStat.PowerPointRestore] = "powerpointrestore";
    SystemsStatsGenerator.localizationTags[ObjectStat.DetectionInnerRadius] = "dradis_base_range";
    SystemsStatsGenerator.localizationTags[ObjectStat.DetectionOuterRadius] = "dradis_max_range";
    SystemsStatsGenerator.localizationTags[ObjectStat.DetectionVisualRadius] = "dradis_visual_range";
    SystemsStatsGenerator.localizationTags[ObjectStat.Detection] = "dradis_detecton_strength";
    SystemsStatsGenerator.localizationTags[ObjectStat.Signature] = "dradis_signature_dispersion";
    SystemsStatsGenerator.localizationTags[ObjectStat.FirewallRating] = "firewallrating";
    SystemsStatsGenerator.localizationTags[ObjectStat.PenetrationStrength] = "penetrationstrength";
    SystemsStatsGenerator.localizationTags[ObjectStat.Speed] = "speed";
    SystemsStatsGenerator.localizationTags[ObjectStat.TurnAcceleration] = "turning_acceleration";
    SystemsStatsGenerator.localizationTags[ObjectStat.TurnSpeed] = "turning_speed";
    SystemsStatsGenerator.localizationTags[ObjectStat.InertiaCompensation] = "inertia_compensation";
    SystemsStatsGenerator.localizationTags[ObjectStat.BoostSpeed] = "boostspeed";
    SystemsStatsGenerator.localizationTags[ObjectStat.Acceleration] = "acceleration";
    SystemsStatsGenerator.localizationTags[ObjectStat.FtlRange] = "ftlrange";
    SystemsStatsGenerator.localizationTags[ObjectStat.FtlCharge] = "ftlcharge";
    SystemsStatsGenerator.localizationTags[ObjectStat.FtlCost] = "ftlcost";
    SystemsStatsGenerator.localizationTags[ObjectStat.Avoidance] = "avoidance";
    SystemsStatsGenerator.localizationTags[ObjectStat.MaxPowerPoints] = "maxpowerpoints";
    SystemsStatsGenerator.localizationTags[ObjectStat.MaxHullPoints] = "maxhullpoints";
    SystemsStatsGenerator.localizationTags[ObjectStat.HullRecovery] = "hullrecovery";
    SystemsStatsGenerator.localizationTags[ObjectStat.PowerRecovery] = "powerrecovery";
    SystemsStatsGenerator.localizationTags[ObjectStat.CriticalDefense] = "criticaldefense";
    SystemsStatsGenerator.localizationTags[ObjectStat.ArmorValue] = "armorvalue";
    SystemsStatsGenerator.localizationTags[ObjectStat.AccelerationMultiplierOnBoost] = "acceleration_multiplier_on_boost";
    SystemsStatsGenerator.localizationTags[ObjectStat.PitchAcceleration] = "pitch_acceleration";
    SystemsStatsGenerator.localizationTags[ObjectStat.YawAcceleration] = "yaw_acceleration";
    SystemsStatsGenerator.localizationTags[ObjectStat.RollAcceleration] = "roll_acceleration";
    SystemsStatsGenerator.localizationTags[ObjectStat.PitchMaxSpeed] = "pitch_max_speed";
    SystemsStatsGenerator.localizationTags[ObjectStat.YawMaxSpeed] = "yaw_max_speed";
    SystemsStatsGenerator.localizationTags[ObjectStat.RollMaxSpeed] = "roll_max_speed";
    SystemsStatsGenerator.localizationTags[ObjectStat.StrafeAcceleration] = "strafe_acceleration";
    SystemsStatsGenerator.localizationTags[ObjectStat.StrafeMaxSpeed] = "strafe_max_speed";
    SystemsStatsGenerator.localizationTags[ObjectStat.MiningAngle] = "mining_angle";
    SystemsStatsGenerator.localizationTags[ObjectStat.CannonAngle] = "cannon_angle";
    SystemsStatsGenerator.localizationTags[ObjectStat.MissileAngle] = "missile_angle";
    SystemsStatsGenerator.localizationTags[ObjectStat.AoeInnerRadius] = "aoe_radius";
    SystemsStatsGenerator.localizationTags[ObjectStat.AoeOuterRadius] = "aoe_radius";
    SystemsStatsGenerator.localizationTags[ObjectStat.PpCostPerSec] = "pp_per_sec";
    SystemsStatsGenerator.localizationTags[ObjectStat.MissileCooldown] = "missile_cooldown";
    SystemsStatsGenerator.localizationTags[ObjectStat.MissilePowerPointCost] = "missile_pp_cost";
    SystemsStatsGenerator.localizationTags[ObjectStat.DrainResistance] = "drain_resistance";
    SystemsStatsGenerator.localizationTags[ObjectStat.MetaWeaponPowerCost] = "meta_weapon_power_cost";
    SystemsStatsGenerator.localizationTags[ObjectStat.ToggleSystemCooldown] = "toggle_system_cooldown";
    SystemsStatsGenerator.localizationTags[ObjectStat.BoostCost] = "boost_cost";
    SystemsStatsGenerator.localizationTags[ObjectStat.MetaPropulsion] = "meta_propulsion";
    SystemsStatsGenerator.localizationTags[ObjectStat.MetaManeuverability] = "meta_maneuverability";
    SystemsStatsGenerator.localizationTags[ObjectStat.MaxHullPoints] = "max_hull_points";
    SystemsStatsGenerator.localizationTags[ObjectStat.LifeTime] = "lifetime";
    SystemsStatsGenerator.localizationTags[ObjectStat.MissileDamageLow] = "missile_damage_low";
    SystemsStatsGenerator.localizationTags[ObjectStat.MissileDamageHigh] = "missile_damage_high";
    SystemsStatsGenerator.localizationTags[ObjectStat.CannonDamageLow] = "cannon_damage_low";
    SystemsStatsGenerator.localizationTags[ObjectStat.CannonDamageHigh] = "cannon_damage_high";
    SystemsStatsGenerator.localizationTags[ObjectStat.DurabilityBonus] = "durability_bonus";
    SystemsStatsGenerator.localizationTags[ObjectStat.AvoidanceFading] = "avoidance_fading";
    SystemsStatsGenerator.localizationTags[ObjectStat.JumpTargetTransponderPowerPointCost] = "jtt_power_cost";
    SystemsStatsGenerator.localizationTags[ObjectStat.FtlCooldown] = "ftl_cooldown";
    SystemsStatsGenerator.localizationTags[ObjectStat.MetaWeaponCooldown] = "meta_weapon_cooldown";
  }

  public static string GetLocalizationTag(ObjectStat stat)
  {
    if (!SystemsStatsGenerator.localizationTags.ContainsKey(stat))
      return stat.ToString();
    return SystemsStatsGenerator.localizationTags[stat];
  }

  public static bool HasPrimaryStat(ShipSystem system)
  {
    return ShipSlot.IsWeaponSlot(system.Card.SlotType);
  }

  public static string GetPrimaryStatString(ShipSystem shipSystem, bool includeModifier = false)
  {
    string str = (string) null;
    ShipSystemCard shipSystemCard = shipSystem.Card;
    if (ShipSlot.IsWeaponSlot(shipSystem.Card.SlotType))
    {
      ObjectStats objectStats = shipSystemCard.AbilityCards[0].ItemBuffAdd;
      str = string.Format(BsgoLocalization.Get("%$bgo.statsdescription.dps.valueformat%"), (object) ((float) (((!includeModifier ? (double) objectStats.DamageLow : (double) SystemsStatsGenerator.GetSlotModifiedValue(shipSystem, SystemsStatsGenerator.StatListType.Ability, ObjectStat.DamageLow, objectStats.DamageLow)) + (!includeModifier ? (double) objectStats.DamageHigh : (double) SystemsStatsGenerator.GetSlotModifiedValue(shipSystem, SystemsStatsGenerator.StatListType.Ability, ObjectStat.DamageHigh, objectStats.DamageLow))) / 2.0) / (!includeModifier ? objectStats.Cooldown : SystemsStatsGenerator.GetSlotModifiedValue(shipSystem, SystemsStatsGenerator.StatListType.Ability, ObjectStat.Cooldown, objectStats.Cooldown)))) + " " + BsgoLocalization.Get("%$bgo.statsdescription.dps.name%");
    }
    return str;
  }

  public static string GenerateSimpleDescriptions(ShipSystem system)
  {
    string str1 = BsgoLocalization.Get("%$bgo.common.sem%");
    string str2 = string.Empty;
    using (List<SystemsStatsGenerator.StatInfoDesc>.Enumerator enumerator = SystemsStatsGenerator.GenerateDescriptions(system).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SystemsStatsGenerator.StatInfoDesc current = enumerator.Current;
        str2 = str2 + current.Name + str1 + " " + current.StatFormatedValue + "\n";
      }
    }
    return str2;
  }

  public static List<SystemsStatsGenerator.StatInfoDesc> GenerateDescriptions(ShipSystem system)
  {
    return SystemsStatsGenerator.GenerateDescriptions(system, (ShipSystemCard) null, false);
  }

  private static MetaStat GetMetaStat(List<ObjectStat> statList)
  {
    using (List<MetaStat>.Enumerator enumerator = SystemsStatsGenerator.metaStats.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MetaStat current = enumerator.Current;
        if (current.Encompasses(statList))
          return current;
      }
    }
    return (MetaStat) null;
  }

  public static List<SystemsStatsGenerator.StatInfoDesc> GenerateDescriptions(ShipSystem system, ShipSystemCard newSystemCard, bool includeSlotStats = false)
  {
    if (!(bool) system.IsLoaded)
      return new List<SystemsStatsGenerator.StatInfoDesc>();
    if (newSystemCard != null && !(bool) newSystemCard.IsLoaded)
      return new List<SystemsStatsGenerator.StatInfoDesc>();
    List<SystemsStatsGenerator.StatInfoDesc> statInfoDescList = new List<SystemsStatsGenerator.StatInfoDesc>();
    foreach (StatView view in system.Card.Views)
    {
      switch (view)
      {
        case StatView.MinRange:
        case StatView.Durability:
        case StatView.DMGLow:
        case StatView.DrainHigh:
          goto case StatView.MinRange;
        case StatView.MaxRange:
          statInfoDescList.Add(SystemsStatsGenerator.GetValueRangeDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.MinRange, ObjectStat.MaxRange, "range", includeSlotStats));
          goto case StatView.MinRange;
        case StatView.OptimalRange:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.OptimalRange, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.Duration:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.Duration, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.Cooldown:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.Cooldown, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.BuffCost:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.PowerPointCost, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.Target:
          statInfoDescList.Add(SystemsStatsGenerator.GetTargetDescription(system, newSystemCard));
          goto case StatView.MinRange;
        case StatView.DMGHigh:
          statInfoDescList.Add(SystemsStatsGenerator.GetValueRangeDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.DamageLow, ObjectStat.DamageHigh, "damage", includeSlotStats));
          goto case StatView.MinRange;
        case StatView.Accuracy:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.Accuracy, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.CriticalOffense:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.CriticalOffense, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.Angle:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.Angle, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.Mining:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.DamageMining, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.FlareRange:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.FlareRange, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.RestoreBuff:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.HullPointRestore, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.RemoteBuffMultiply:
          MetaStat metaStat = SystemsStatsGenerator.GetMetaStat(system.Card.AbilityCards[0].RemoteBuffMultiply.PropList);
          if (metaStat != null)
          {
            SystemsStatsGenerator.StatInfoDesc objStatDescription = SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.RemoteBuffMultiply, metaStat.Meta, includeSlotStats);
            statInfoDescList.Add(objStatDescription);
          }
          using (List<ObjectStat>.Enumerator enumerator = system.Card.AbilityCards[0].RemoteBuffMultiply.PropList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              ObjectStat current = enumerator.Current;
              if (metaStat == null || !metaStat.IsPartOfMetaStat(current))
              {
                SystemsStatsGenerator.StatInfoDesc objStatDescription = SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.RemoteBuffMultiply, current, includeSlotStats);
                float objStatValue = SystemsStatsGenerator.GetObjStatValue(system.Card, SystemsStatsGenerator.StatListType.RemoteBuffMultiply, current);
                objStatDescription.StatFormatedValue = ((double) objStatValue < 0.0 ? string.Empty : "+") + objStatDescription.StatFormatedValue;
                statInfoDescList.Add(objStatDescription);
              }
            }
            goto case StatView.MinRange;
          }
        case StatView.StaticBuff:
          using (List<ObjectStat>.Enumerator enumerator = system.Card.StaticBuffs.PropList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              ObjectStat current = enumerator.Current;
              SystemsStatsGenerator.StatInfoDesc objStatDescription = SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Static, current, includeSlotStats);
              if ((double) SystemsStatsGenerator.GetObjStatValue(system.Card, SystemsStatsGenerator.StatListType.Static, current) >= 0.0)
                objStatDescription.StatFormatedValue = "+" + objStatDescription.StatFormatedValue;
              statInfoDescList.Add(objStatDescription);
            }
            goto case StatView.MinRange;
          }
        case StatView.RestorePowerBuff:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.PowerPointRestore, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.DrainLow:
          if ((double) SystemsStatsGenerator.GetObjStatValue(system.Card, SystemsStatsGenerator.StatListType.Ability, ObjectStat.DrainHigh) > 0.0)
          {
            statInfoDescList.Add(SystemsStatsGenerator.GetValueRangeDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.DrainLow, ObjectStat.DrainHigh, "drain", includeSlotStats));
            goto case StatView.MinRange;
          }
          else
            goto case StatView.MinRange;
        case StatView.ArmorPiercing:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.ArmorPiercing, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.ArmorValue:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.ArmorValue, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.HPRecovery:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.HullRecovery, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.PPRecovery:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.PowerRecovery, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.Speed:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.Speed, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.TurnSpeed:
          statInfoDescList.Add(SystemsStatsGenerator.GetTurnSpeedStatInfoDesc(system, newSystemCard, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.TurnAcceleration:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.TurnAcceleration, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.InertiaCompensation:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.InertiaCompensation, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.MultiplyBuff:
          using (List<ObjectStat>.Enumerator enumerator = system.Card.MultiplyBuffs.PropList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              ObjectStat current = enumerator.Current;
              SystemsStatsGenerator.StatInfoDesc objStatDescription = SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Multiply, current, includeSlotStats);
              float objStatValue = SystemsStatsGenerator.GetObjStatValue(system.Card, SystemsStatsGenerator.StatListType.Multiply, current);
              objStatDescription.StatFormatedValue = ((double) objStatValue < 0.0 ? string.Empty : "+") + objStatDescription.StatFormatedValue;
              statInfoDescList.Add(objStatDescription);
            }
            goto case StatView.MinRange;
          }
        case StatView.RemoteBuffAdd:
          using (List<ObjectStat>.Enumerator enumerator = system.Card.AbilityCards[0].RemoteBuffAdd.PropList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              ObjectStat current = enumerator.Current;
              SystemsStatsGenerator.StatInfoDesc objStatDescription = SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.RemoteBuffAdd, current, includeSlotStats);
              if ((double) SystemsStatsGenerator.GetObjStatValue(system.Card, SystemsStatsGenerator.StatListType.RemoteBuffAdd, current) >= 0.0)
                objStatDescription.StatFormatedValue = "+" + objStatDescription.StatFormatedValue;
              statInfoDescList.Add(objStatDescription);
            }
            goto case StatView.MinRange;
          }
        case StatView.AoERadius:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.AoeInnerRadius, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.BuffCostPerSecond:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.PpCostPerSec, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.ToggleSystemAdd:
          using (List<ObjectStat>.Enumerator enumerator = system.Card.AbilityCards[0].ToggleSystemAdd.PropList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              ObjectStat current = enumerator.Current;
              SystemsStatsGenerator.StatInfoDesc objStatDescription = SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.ToggleSystemAdd, current, includeSlotStats);
              if ((double) SystemsStatsGenerator.GetObjStatValue(system.Card, SystemsStatsGenerator.StatListType.ToggleSystemAdd, current) >= 0.0)
                objStatDescription.StatFormatedValue = "+" + objStatDescription.StatFormatedValue;
              statInfoDescList.Add(objStatDescription);
            }
            goto case StatView.MinRange;
          }
        case StatView.ToggleSystemMultiply:
          using (List<ObjectStat>.Enumerator enumerator = system.Card.AbilityCards[0].ToggleSystemMultiply.PropList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              ObjectStat current = enumerator.Current;
              SystemsStatsGenerator.StatInfoDesc objStatDescription = SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.ToggleSystemMultiply, current, includeSlotStats);
              float objStatValue = SystemsStatsGenerator.GetObjStatValue(system.Card, SystemsStatsGenerator.StatListType.ToggleSystemMultiply, current);
              objStatDescription.StatFormatedValue = ((double) objStatValue < 0.0 ? string.Empty : "+") + objStatDescription.StatFormatedValue;
              statInfoDescList.Add(objStatDescription);
            }
            goto case StatView.MinRange;
          }
        case StatView.HP:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.MaxHullPoints, includeSlotStats));
          goto case StatView.MinRange;
        case StatView.LifeTime:
          statInfoDescList.Add(SystemsStatsGenerator.GetObjStatDescription(system, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.LifeTime, includeSlotStats));
          goto case StatView.MinRange;
        default:
          statInfoDescList.Add((SystemsStatsGenerator.StatInfoDesc) new SystemsStatsGenerator.StatInfoDescWithDiff(view.ToString(), "???", "???", false));
          goto case StatView.MinRange;
      }
    }
    statInfoDescList.RemoveAll((Predicate<SystemsStatsGenerator.StatInfoDesc>) (statDesc => statDesc == SystemsStatsGenerator.IGNORE));
    return statInfoDescList;
  }

  private static SystemsStatsGenerator.StatInfoDesc GetTurnSpeedStatInfoDesc(ShipSystem baseSystem, ShipSystemCard newSystemCard, bool showModifiedValues)
  {
    if ((double) SystemsStatsGenerator.GetObjStatValue(baseSystem.Card, SystemsStatsGenerator.StatListType.Ability, ObjectStat.TurnSpeed) > 0.0)
      return SystemsStatsGenerator.GetObjStatDescription(baseSystem, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.TurnSpeed, showModifiedValues);
    SystemsStatsGenerator.StatInfoDesc objStatDescription = SystemsStatsGenerator.GetObjStatDescription(baseSystem, newSystemCard, SystemsStatsGenerator.StatListType.Ability, ObjectStat.PitchMaxSpeed, showModifiedValues);
    objStatDescription.Name = SystemsStatsGenerator.GetObjStatName(ObjectStat.TurnSpeed);
    return objStatDescription;
  }

  private static SystemsStatsGenerator.StatInfoDesc GetObjStatDescription(ShipSystem baseSystem, ShipSystemCard newSystemCard, SystemsStatsGenerator.StatListType listType, ObjectStat statType, bool showModifiedValues)
  {
    string str = string.Empty;
    string diffStatFormatedValue = string.Empty;
    bool upgradable = false;
    string objStatName = SystemsStatsGenerator.GetObjStatName(statType);
    float objStatValue = SystemsStatsGenerator.GetObjStatValue(baseSystem.Card, listType, statType);
    if (newSystemCard != null)
    {
      float f = SystemsStatsGenerator.GetObjStatValue(newSystemCard, listType, statType) - objStatValue;
      upgradable = (double) Math.Abs(f) > 9.99999974737875E-05;
      diffStatFormatedValue = string.Format("{0}{1}", (object) TextUtility.GetValueFactor(f), (object) SystemsStatsGenerator.FormatFloat(Mathf.Abs(f)));
    }
    string baseStatFormatedValue = !showModifiedValues || listType == SystemsStatsGenerator.StatListType.RemoteBuffMultiply ? string.Format(SystemsStatsGenerator.GetObjStatValueFormat(listType, statType), (object) SystemsStatsGenerator.FormatFloat(objStatValue)) : SystemsStatsGenerator.AlignWithGameSlot(baseSystem, listType, statType, objStatValue);
    return (SystemsStatsGenerator.StatInfoDesc) new SystemsStatsGenerator.StatInfoDescWithDiff(objStatName, baseStatFormatedValue, diffStatFormatedValue, upgradable);
  }

  private static string AlignWithGameSlot(ShipSystem baseSystem, SystemsStatsGenerator.StatListType listType, ObjectStat statType, float baseValue)
  {
    float slotModifiedValue = SystemsStatsGenerator.GetSlotModifiedValue(baseSystem, listType, statType, baseValue);
    float num = slotModifiedValue - baseValue;
    if ((double) Math.Abs(slotModifiedValue) > 0.01 && (double) Math.Abs(num) > 0.01)
      return string.Format(SystemsStatsGenerator.GetObjStatValueFormat(listType, statType), (object) SystemsStatsGenerator.FormatFloat(slotModifiedValue));
    return string.Format(SystemsStatsGenerator.GetObjStatValueFormat(listType, statType), (object) SystemsStatsGenerator.FormatFloat(baseValue));
  }

  private static float GetSlotModifiedValue(ShipSystem baseSystem, SystemsStatsGenerator.StatListType listType, ObjectStat statType, float baseValue)
  {
    ShipSlot shipSlot = baseSystem.Container as ShipSlot;
    if (shipSlot == null)
      return baseValue;
    float num1 = SystemsStatsGenerator.GetSpecialStatValue(shipSlot.GameStats, statType, listType);
    if ((double) Math.Abs(num1) < 0.00999999977648258 && shipSlot.GameStats.Available && listType == SystemsStatsGenerator.StatListType.Ability)
    {
      switch (statType)
      {
        case ObjectStat.MaxRange:
          num1 = shipSlot.GameStats.MaxRange;
          break;
        case ObjectStat.MinRange:
          num1 = shipSlot.GameStats.MinRange;
          break;
        case ObjectStat.Angle:
          num1 = shipSlot.GameStats.Angle * 2f;
          break;
        case ObjectStat.PowerPointCost:
          num1 = shipSlot.GameStats.PowerPointCost;
          break;
        case ObjectStat.Cooldown:
          num1 = shipSlot.GameStats.Cooldown;
          break;
      }
    }
    float num2 = num1 - baseValue;
    if ((double) Math.Abs(num1) > 0.00999999977648258 && (double) Math.Abs(num2) > 0.00999999977648258)
      return num1;
    return baseValue;
  }

  public static string GetObjStatName(ObjectStat statType)
  {
    string @string = statType.ToString();
    if (!SystemsStatsGenerator.localizationTags.ContainsKey(statType))
      Debug.LogWarning((object) ("No loca tag for statType: " + (object) statType));
    if (SystemsStatsGenerator.localizationTags.ContainsKey(statType))
      return SystemsStatsGenerator.GetLocalization("bgo.statsdescription." + SystemsStatsGenerator.localizationTags[statType] + ".name", @string);
    return @string;
  }

  private static bool IsMultiplier(SystemsStatsGenerator.StatListType statListType)
  {
    SystemsStatsGenerator.StatListType statListType1 = statListType;
    switch (statListType1)
    {
      case SystemsStatsGenerator.StatListType.RemoteBuffMultiply:
      case SystemsStatsGenerator.StatListType.Multiply:
        return true;
      default:
        if (statListType1 != SystemsStatsGenerator.StatListType.ToggleSystemMultiply)
          return false;
        goto case SystemsStatsGenerator.StatListType.RemoteBuffMultiply;
    }
  }

  private static float GetObjStatValue(ShipSystemCard systemCard, SystemsStatsGenerator.StatListType listType, ObjectStat statType)
  {
    ObjectStats statList = (ObjectStats) null;
    switch (listType)
    {
      case SystemsStatsGenerator.StatListType.RemoteBuffMultiply:
        if (SystemsStatsGenerator.CheckForAbility(systemCard))
        {
          statList = systemCard.AbilityCards[0].RemoteBuffMultiply;
          break;
        }
        break;
      case SystemsStatsGenerator.StatListType.Static:
        statList = systemCard.StaticBuffs;
        break;
      case SystemsStatsGenerator.StatListType.Multiply:
        statList = systemCard.MultiplyBuffs;
        break;
      case SystemsStatsGenerator.StatListType.Ability:
        if (SystemsStatsGenerator.CheckForAbility(systemCard))
        {
          statList = systemCard.AbilityCards[0].ItemBuffAdd;
          break;
        }
        break;
      case SystemsStatsGenerator.StatListType.RemoteBuffAdd:
        if (SystemsStatsGenerator.CheckForAbility(systemCard))
        {
          statList = systemCard.AbilityCards[0].RemoteBuffAdd;
          break;
        }
        break;
      case SystemsStatsGenerator.StatListType.ToggleSystemAdd:
        if (SystemsStatsGenerator.CheckForAbility(systemCard))
        {
          statList = systemCard.AbilityCards[0].ToggleSystemAdd;
          break;
        }
        break;
      case SystemsStatsGenerator.StatListType.ToggleSystemMultiply:
        if (SystemsStatsGenerator.CheckForAbility(systemCard))
        {
          statList = systemCard.AbilityCards[0].ToggleSystemMultiply;
          break;
        }
        break;
    }
    float num = SystemsStatsGenerator.GetSpecialStatValue(statList, statType, listType);
    if (SystemsStatsGenerator.IsMultiplier(listType))
      num = (float) (((double) num - 1.0) * 100.0);
    return num;
  }

  private static string GetObjStatValueFormat(SystemsStatsGenerator.StatListType listType, ObjectStat statType)
  {
    if (SystemsStatsGenerator.IsMultiplier(listType))
      return "{0}%";
    if (!SystemsStatsGenerator.localizationTags.ContainsKey(statType))
      return statType.ToString();
    string str = string.Empty;
    if (BsgoLocalization.TryGet("bgo.statsdescription." + SystemsStatsGenerator.localizationTags[statType] + ".valueformat", out str))
      return str;
    Debug.LogWarning((object) ("Value format key for bgo.statsdescription." + SystemsStatsGenerator.localizationTags[statType] + " not found"));
    if (string.IsNullOrEmpty(str))
      return "{0}";
    return str;
  }

  private static SystemsStatsGenerator.StatInfoDesc GetValueRangeDescription(ShipSystem system, ShipSystemCard newSystemCard, SystemsStatsGenerator.StatListType statListType, ObjectStat fromStat, ObjectStat toStat, string identifier, bool showModifiedStatValues)
  {
    float objStatValue1 = SystemsStatsGenerator.GetObjStatValue(system.Card, statListType, fromStat);
    float objStatValue2 = SystemsStatsGenerator.GetObjStatValue(system.Card, statListType, toStat);
    float slotModifiedValue1 = SystemsStatsGenerator.GetSlotModifiedValue(system, statListType, fromStat, objStatValue1);
    float slotModifiedValue2 = SystemsStatsGenerator.GetSlotModifiedValue(system, statListType, toStat, objStatValue2);
    bool upgradable = false;
    string diffStatFormatedValue = string.Empty;
    if (newSystemCard != null)
    {
      float objStatValue3 = SystemsStatsGenerator.GetObjStatValue(newSystemCard, statListType, fromStat);
      float objStatValue4 = SystemsStatsGenerator.GetObjStatValue(newSystemCard, statListType, toStat);
      diffStatFormatedValue = BsgoLocalization.Get("bgo.statsdescription." + identifier + ".valueformat", (object) SystemsStatsGenerator.FormatFloat(objStatValue3), (object) SystemsStatsGenerator.FormatFloat(objStatValue4));
      upgradable = (double) objStatValue3 > (double) objStatValue1 || (double) objStatValue4 > (double) objStatValue2;
    }
    float f1 = slotModifiedValue1 - objStatValue1;
    float f2 = slotModifiedValue2 - objStatValue2;
    string name = BsgoLocalization.Get("bgo.statsdescription." + identifier + ".name");
    string str;
    if (showModifiedStatValues && ((double) Mathf.Abs(f1) > 1.0 / 1000.0 || (double) Mathf.Abs(f2) > 1.0 / 1000.0))
      str = BsgoLocalization.Get("bgo.statsdescription." + identifier + ".valueformat", (object) SystemsStatsGenerator.FormatFloat(slotModifiedValue1), (object) SystemsStatsGenerator.FormatFloat(slotModifiedValue2));
    else
      str = BsgoLocalization.Get("bgo.statsdescription." + identifier + ".valueformat", (object) SystemsStatsGenerator.FormatFloat(objStatValue1), (object) SystemsStatsGenerator.FormatFloat(objStatValue2));
    if (newSystemCard != null && upgradable)
      return (SystemsStatsGenerator.StatInfoDesc) new SystemsStatsGenerator.StatInfoDescWithDiff(name, str, diffStatFormatedValue, upgradable);
    return new SystemsStatsGenerator.StatInfoDesc(name, str, upgradable);
  }

  public static SystemsStatsGenerator.StatInfoDesc GetDurabilityDescription(ShipSystem system)
  {
    string name;
    if (!BsgoLocalization.TryGet("bgo.statsdescription.durability.name", out name))
      name = "Durability";
    string formatedValue = ((int) system.Durability).ToString() + "/" + (object) (int) system.Card.Durability;
    return new SystemsStatsGenerator.StatInfoDesc(name, formatedValue, false);
  }

  public static SystemsStatsGenerator.StatInfoDescWithDiff GetDurabilityDescription(ShipSystem system, ShipSystemCard newSystem)
  {
    string name;
    if (!BsgoLocalization.TryGet("bgo.statsdescription.durability.name", out name))
      name = "Durability";
    string baseStatFormatedValue = ((int) system.Card.Durability).ToString() + string.Empty;
    string diffStatFormatedValue = string.Empty;
    if (newSystem != null)
    {
      int num = (int) ((double) newSystem.Durability - (double) system.Card.Durability);
      diffStatFormatedValue = num <= 0 ? string.Empty + (object) num : "+" + (object) num;
    }
    return new SystemsStatsGenerator.StatInfoDescWithDiff(name, baseStatFormatedValue, diffStatFormatedValue, false);
  }

  private static SystemsStatsGenerator.StatInfoDesc GetTargetDescription(ShipSystem baseSystem, ShipSystemCard newSystemCard)
  {
    string name = string.Empty;
    if (!BsgoLocalization.TryGet("bgo.statsdescription.target.name", out name))
      name = "Target";
    if (!SystemsStatsGenerator.CheckForAbility(baseSystem.Card))
      return (SystemsStatsGenerator.StatInfoDesc) new SystemsStatsGenerator.StatInfoDescWithDiff(name, "???", "???", false);
    string string1 = Tools.EnumToString((Enum) baseSystem.Card.AbilityCards[0].Affect);
    if (newSystemCard == null)
      return new SystemsStatsGenerator.StatInfoDesc(name, string1, false);
    if (!SystemsStatsGenerator.CheckForAbility(newSystemCard))
      return (SystemsStatsGenerator.StatInfoDesc) new SystemsStatsGenerator.StatInfoDescWithDiff(name, "???", "???", false);
    string string2 = Tools.EnumToString((Enum) newSystemCard.AbilityCards[0].Affect);
    return (SystemsStatsGenerator.StatInfoDesc) new SystemsStatsGenerator.StatInfoDescWithDiff(name, string1, string2, false);
  }

  private static float GetSpecialStatValue(ObjectStats statList, ObjectStat statType, SystemsStatsGenerator.StatListType listType)
  {
    if (statList == null)
      return 0.0f;
    if (SystemsStatsGenerator.IsMultiplier(listType))
      return statList.GetObfuscatedFloat(statType);
    ObjectStat objectStat = statType;
    switch (objectStat)
    {
      case ObjectStat.FtlRange:
        return statList.FTLRange / 20f;
      case ObjectStat.FtlCost:
        return statList.FTLCost * 20f;
      default:
        if (objectStat == ObjectStat.Angle)
          return statList.Angle * 2f;
        if (objectStat == ObjectStat.FlareRange)
          return statList.FlareRange;
        if (objectStat == ObjectStat.AoeInnerRadius)
          return statList.GetObfuscatedFloat(ObjectStat.AoeInnerRadius) + statList.GetObfuscatedFloat(ObjectStat.AoeOuterRadius);
        return statList.GetObfuscatedFloat(statType);
    }
  }

  private static string FormatFloat(float v)
  {
    return v.ToString("#0.##");
  }

  private static bool CheckForAbility(ShipSystemCard systemCard)
  {
    if (systemCard.AbilityCards != null)
      return systemCard.AbilityCards.Length != 0;
    return false;
  }

  private static string GetLocalization(string tag, string defValue)
  {
    string str = string.Empty;
    BsgoLocalization.TryGet(tag, out str);
    return str;
  }

  private enum StatListType
  {
    RemoteBuffMultiply,
    Static,
    Multiply,
    Ability,
    RemoteBuffAdd,
    ToggleSystemAdd,
    ToggleSystemMultiply,
  }

  public class StatInfoDesc
  {
    public string Name;
    public string StatFormatedValue;
    public bool Upgradeable;

    public StatInfoDesc(string name, string formatedValue, bool upgradable = false)
    {
      this.Name = name;
      this.StatFormatedValue = formatedValue;
      this.Upgradeable = upgradable;
    }
  }

  public class StatInfoDescWithDiff : SystemsStatsGenerator.StatInfoDesc
  {
    public string DiffStatFormat;

    public StatInfoDescWithDiff(string name, string baseStatFormatedValue, string diffStatFormatedValue, bool upgradable = false)
      : base(name, baseStatFormatedValue, upgradable)
    {
      this.DiffStatFormat = diffStatFormatedValue;
    }
  }
}
