﻿// Decompiled with JetBrains decompiler
// Type: RenderModelToImage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using Unity5AssetHandling;
using UnityEngine;

public class RenderModelToImage
{
  private static readonly float ms_restoreViewTimerMax = 1.9f;
  public string LayerName = string.Empty;
  private string prefabName = string.Empty;
  public string msg = "waiting for names";
  public string skinName = string.Empty;
  public float OutRadiusFactor = 1.2f;
  public Rect ImgRect = new Rect(0.0f, 0.0f, 320f, 240f);
  public float autoRotationY = 35f;
  private Vector3 fModelCenter = new Vector3();
  private float fModelInitialRadiusFactor = 1.1f;
  private readonly Vector3 shipViewOffset = new Vector3(0.0f, 50000f, 0.0f);
  private bool showRust = true;
  public int layer;
  public GameObject obj;
  private ShipSkinSubstance shipSkinSubstance;
  private ShipSkin shipSkin;
  private GameObject cameraObj;
  public Camera camera;
  private bool stillLoading;
  private bool deleteOnLoaded;
  private float fModelRadius;
  private float fViewRestoreTimer;
  private float fVR_radiusRestoreStep;
  private float fVR_autoRotationRestoreStep;
  private Color rustColor;
  private AssetRequest assetRequest;

  public float RestoreTimer
  {
    get
    {
      return this.fViewRestoreTimer;
    }
  }

  public bool ShowRust
  {
    get
    {
      return this.showRust;
    }
    set
    {
      this.showRust = value;
      if (!((Object) this.obj != (Object) null))
        return;
      foreach (Renderer componentsInChild in this.obj.GetComponentsInChildren<Renderer>())
      {
        foreach (Material material in componentsInChild.materials)
        {
          if (material.HasProperty("_RustColor"))
          {
            Color color = !this.showRust ? Color.clear : this.rustColor;
            material.SetColor("_RustColor", color);
          }
        }
      }
    }
  }

  public bool StillLoading
  {
    get
    {
      return this.stillLoading;
    }
    private set
    {
      this.stillLoading = value;
      if (!((Object) this.camera != (Object) null))
        return;
      this.camera.cullingMask = !value ? 1 << this.layer : 0;
    }
  }

  public void RestoreView()
  {
    this.fViewRestoreTimer = RenderModelToImage.ms_restoreViewTimerMax;
    this.fVR_radiusRestoreStep = (this.fModelInitialRadiusFactor - this.OutRadiusFactor) / this.fViewRestoreTimer;
    this.fVR_autoRotationRestoreStep = 35f / this.fViewRestoreTimer;
    this.autoRotationY = 0.0f;
    this.SetupCamera();
  }

  public void Update()
  {
    if (0.0 < (double) this.fViewRestoreTimer)
    {
      this.obj.transform.rotation = Quaternion.Lerp(this.obj.transform.rotation, Quaternion.identity, (float) (1.0 - (double) this.fViewRestoreTimer / (double) RenderModelToImage.ms_restoreViewTimerMax));
      this.fViewRestoreTimer -= Time.deltaTime;
      this.autoRotationY += this.fVR_autoRotationRestoreStep * Time.deltaTime;
      this.OutRadiusFactor += Time.deltaTime * this.fVR_radiusRestoreStep;
      this.SetupCamera();
    }
    if (this.prefabName != string.Empty && this.LayerName != string.Empty && ((Object) this.obj == (Object) null && !this.StillLoading))
    {
      this.layer = LayerMask.NameToLayer(this.LayerName);
      this.assetRequest = AssetCatalogue.Instance.Request(this.prefabName + ".prefab", false);
    }
    if (this.assetRequest != null && this.assetRequest.IsDone)
    {
      Game.Instance.StartCoroutine(this.ModelRequestFulfilled(this.assetRequest));
      this.assetRequest = (AssetRequest) null;
    }
    if (!((Object) this.obj != (Object) null))
      return;
    this.obj.transform.RotateAround(this.shipViewOffset, new Vector3(0.0f, 1f, 0.0f), this.autoRotationY * Time.deltaTime);
  }

  public void UpdateRect()
  {
    if ((Object) this.camera == (Object) null)
      return;
    float left = this.ImgRect.xMin / (float) Screen.width;
    float num = this.ImgRect.yMin / (float) Screen.height;
    float width = this.ImgRect.width / (float) Screen.width;
    float height = this.ImgRect.height / (float) Screen.height;
    float top = 1f - num - height;
    this.camera.rect = new Rect(left, top, width, height);
  }

  public void OnRender()
  {
  }

  public void RotateModelInDirection(float dx, float dy, float DegreesPerSecond, bool NeedMousePressed)
  {
    if (!(bool) ((Object) this.obj) || NeedMousePressed && !Input.GetMouseButton(0))
      return;
    this.obj.transform.RotateAround(this.shipViewOffset, new Vector3(0.0f, 1f, 0.0f), -dx * DegreesPerSecond);
    this.obj.transform.RotateAround(this.shipViewOffset, new Vector3(1f, 0.0f, 0.0f), DegreesPerSecond * dy);
    this.autoRotationY = 0.0f;
  }

  public void RotateModelByMouse(float2 position, float2 previousPosition, float SpeedFactor, bool NeedMousePressed)
  {
    if (!(bool) ((Object) this.obj))
      return;
    Rect rect = new Rect(this.ImgRect);
    rect.x += 16f;
    rect.y += 16f;
    rect.width -= 32f;
    rect.height -= 32f;
    if (!rect.Contains(new Vector2(position.x, position.y)) || NeedMousePressed && !Input.GetMouseButton(0))
      return;
    float num1 = position.x - previousPosition.x;
    float num2 = position.y - previousPosition.y;
    if ((double) Mathf.Abs(num1 + num2) > 0.0)
      this.autoRotationY = 0.0f;
    if ((double) num1 != 0.0)
      this.obj.transform.RotateAround(this.shipViewOffset, new Vector3(0.0f, 1f, 0.0f), -num1 * SpeedFactor);
    if ((double) num2 == 0.0)
      return;
    this.obj.transform.RotateAround(this.shipViewOffset, new Vector3(1f, 0.0f, 0.0f), -num2 * SpeedFactor);
  }

  public void SetPrefab(string prefabName)
  {
    if (this.prefabName != prefabName)
      this.DeleteModel();
    this.prefabName = prefabName;
  }

  public void SetSkinnedModel(ShipSystemPaintCard paintCard, bool loadSkin)
  {
    this.SetPrefab(paintCard.PrefabName);
    this.SetSkin(paintCard.paintTexture, loadSkin);
  }

  public void SetSkin(string skinName, bool loadSkin)
  {
    this.StillLoading = true;
    this.skinName = skinName;
    if ((Object) this.shipSkin != (Object) null)
      this.shipSkin.SelectSkin(this.skinName);
    if ((Object) this.shipSkinSubstance != (Object) null)
      this.shipSkinSubstance.SelectSkin(this.skinName, this.obj.GetComponentsInChildren<Renderer>(), true, true);
    if (!loadSkin)
      return;
    Game.Instance.StartCoroutine(this.WaitForSkin());
  }

  [DebuggerHidden]
  private IEnumerator WaitForSkin()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RenderModelToImage.\u003CWaitForSkin\u003Ec__Iterator21() { \u003C\u003Ef__this = this };
  }

  public void ShowDefaultSkin(HangarShip hangarShip)
  {
    this.SetPrefab(hangarShip.Card.WorldCard.PrefabName);
    this.StillLoading = true;
    if ((Object) this.shipSkin != (Object) null)
      this.shipSkin.SwitchToDefaultSkin();
    if ((Object) this.shipSkinSubstance != (Object) null)
      this.shipSkinSubstance.SwitchToDefaultSkin();
    Game.Instance.StartCoroutine(this.WaitForSkin());
  }

  private void SetLayerToAll(GameObject obj, int layer)
  {
    obj.layer = layer;
    for (int index = 0; index < obj.transform.childCount; ++index)
      this.SetLayerToAll(obj.transform.GetChild(index).gameObject, layer);
  }

  [DebuggerHidden]
  private IEnumerator ModelRequestFulfilled(AssetRequest request)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RenderModelToImage.\u003CModelRequestFulfilled\u003Ec__Iterator22() { request = request, \u003C\u0024\u003Erequest = request, \u003C\u003Ef__this = this };
  }

  private void SetupCameraOnce()
  {
    Vector3 vector3 = new Vector3(0.0f, 0.0f, 0.0f);
    float num1 = 0.0f;
    MeshFilter[] componentsInChildren = this.obj.GetComponentsInChildren<MeshFilter>();
    this.msg = "meshes: ";
    this.obj.transform.position = this.shipViewOffset;
    if ((Object) this.obj.GetComponent<Collider>() != (Object) null)
      Object.Destroy((Object) this.obj.GetComponent<Collider>());
    if ((Object) this.obj.GetComponent<Rigidbody>() != (Object) null)
      Object.Destroy((Object) this.obj.GetComponent<Rigidbody>());
    foreach (MeshFilter meshFilter in componentsInChildren)
    {
      if (!meshFilter.name.Contains("sticker"))
      {
        if ((Object) meshFilter.GetComponent<Rigidbody>() != (Object) null)
          meshFilter.GetComponent<Rigidbody>().detectCollisions = false;
        if ((Object) meshFilter.GetComponent<Collider>() != (Object) null)
          Object.Destroy((Object) meshFilter.GetComponent<Collider>());
        if ((Object) meshFilter.GetComponent<Rigidbody>() != (Object) null)
          Object.Destroy((Object) meshFilter.GetComponent<Rigidbody>());
        if ((bool) ((Object) meshFilter.GetComponent<Renderer>()) && (double) num1 < (double) meshFilter.GetComponent<Renderer>().bounds.extents.magnitude)
          num1 = meshFilter.GetComponent<Renderer>().bounds.extents.magnitude;
      }
    }
    float num2 = num1 / 4f;
    if ((double) num1 < 0.1)
      num2 = 1f;
    float num3 = 1f / num2;
    this.obj.transform.localScale = new Vector3(num3, num3, num3);
    Vector3 min = new Vector3();
    Vector3 max = new Vector3();
    int num4 = 0;
    int num5 = 0;
    foreach (MeshFilter meshFilter in componentsInChildren)
    {
      if ((bool) ((Object) meshFilter.GetComponent<Renderer>()))
      {
        if (meshFilter.name.Contains("sticker"))
          ++num5;
        else if ((bool) ((Object) meshFilter.mesh) && meshFilter.mesh.name.Contains("engine"))
        {
          meshFilter.GetComponent<Renderer>().enabled = false;
        }
        else
        {
          if (num4 == 0)
          {
            min = meshFilter.GetComponent<Renderer>().bounds.min;
            max = meshFilter.GetComponent<Renderer>().bounds.max;
          }
          else
          {
            min.x = Mathf.Min(min.x, meshFilter.GetComponent<Renderer>().bounds.min.x);
            min.y = Mathf.Min(min.y, meshFilter.GetComponent<Renderer>().bounds.min.y);
            min.z = Mathf.Min(min.z, meshFilter.GetComponent<Renderer>().bounds.min.z);
            max.x = Mathf.Max(max.x, meshFilter.GetComponent<Renderer>().bounds.max.x);
            max.y = Mathf.Max(max.y, meshFilter.GetComponent<Renderer>().bounds.max.y);
            max.z = Mathf.Max(max.z, meshFilter.GetComponent<Renderer>().bounds.max.z);
          }
          ++num4;
          this.msg = this.msg + ", " + meshFilter.name;
        }
      }
    }
    Bounds bounds = new Bounds();
    bounds.SetMinMax(min, max);
    float magnitude = bounds.extents.magnitude;
    Vector3 center = bounds.center;
    this.obj.transform.position -= center;
    this.obj.transform.position += new Vector3(0.0f, center.y, 0.0f);
    this.fModelRadius = magnitude;
    this.fModelCenter = new Vector3(0.0f, 0.0f, 0.0f);
    this.fModelInitialRadiusFactor = this.OutRadiusFactor;
  }

  public void ZoomModel(float zFactor)
  {
    if ((double) this.OutRadiusFactor + (double) zFactor < (double) this.fModelInitialRadiusFactor * 0.75 || (double) this.OutRadiusFactor + (double) zFactor > 5.0 * (double) this.fModelInitialRadiusFactor)
      return;
    this.OutRadiusFactor += zFactor;
    this.SetupCamera();
  }

  private void SetupCamera()
  {
    if ((double) this.fModelRadius <= 0.0)
      return;
    Vector3 vector3 = this.fModelCenter;
    vector3.z -= this.fModelRadius * this.OutRadiusFactor;
    this.cameraObj.transform.position = this.shipViewOffset + vector3;
  }

  private void DeleteModel()
  {
    this.camera = (Camera) null;
    if ((Object) this.cameraObj != (Object) null)
      Object.Destroy((Object) this.cameraObj);
    this.cameraObj = (GameObject) null;
    if ((Object) this.obj != (Object) null)
      Object.Destroy((Object) this.obj);
    this.prefabName = string.Empty;
    this.obj = (GameObject) null;
    this.shipSkin = (ShipSkin) null;
    this.shipSkinSubstance = (ShipSkinSubstance) null;
  }

  public void Delete()
  {
    if (!this.StillLoading)
      this.DeleteModel();
    else
      this.deleteOnLoaded = true;
  }
}
