﻿// Decompiled with JetBrains decompiler
// Type: MovementCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class MovementCard : Card
{
  public float minYawSpeed = 3f;
  public float maxPitch = 55f;
  public float maxRoll = 50f;
  public float pitchFading = 0.3f;
  public float yawFading = 0.3f;
  public float rollFading = 0.6f;

  public MovementCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.minYawSpeed = r.ReadSingle();
    this.maxPitch = r.ReadSingle();
    this.maxRoll = r.ReadSingle();
    this.pitchFading = r.ReadSingle();
    this.yawFading = r.ReadSingle();
    this.rollFading = r.ReadSingle();
  }
}
