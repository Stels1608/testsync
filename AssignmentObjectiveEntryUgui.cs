﻿// Decompiled with JetBrains decompiler
// Type: AssignmentObjectiveEntryUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using TMPro;
using UnityEngine;

public class AssignmentObjectiveEntryUgui : MonoBehaviour
{
  private bool _isRendered = true;
  public TextMeshProUGUI nameLabel;
  public TextMeshProUGUI progressCurrentLabel;
  public TextMeshProUGUI progressSeparatorLabel;
  public TextMeshProUGUI progressMaxLabel;

  public bool IsRendered
  {
    get
    {
      return this._isRendered;
    }
    set
    {
      this._isRendered = value;
      this.gameObject.SetActive(this._isRendered);
    }
  }

  public void SetMission(Mission mission)
  {
    if (!string.IsNullOrEmpty(mission.missionCard.Action) && mission.missionCard.Action != "undefined")
    {
      this.EnableProgress(false);
      this.nameLabel.text = BsgoLocalization.Get("bgo.text_actions." + mission.missionCard.Action);
    }
    else if (mission.countables.Count == 0)
    {
      this.EnableProgress(false);
      this.nameLabel.text = mission.Name;
    }
    else
      this.EnableProgress(true);
    Color color1 = mission.Status == Mission.State.Completed || mission.Status == Mission.State.Submitting ? ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_POSITIVE) : Color.white;
    TextMeshProUGUI textMeshProUgui = this.nameLabel;
    Color color2 = color1;
    this.progressMaxLabel.color = color2;
    Color color3 = color2;
    this.progressSeparatorLabel.color = color3;
    Color color4 = color3;
    this.progressCurrentLabel.color = color4;
    Color color5 = color4;
    textMeshProUgui.color = color5;
  }

  public void SetObjective(MissionCountable countable, Mission mission)
  {
    string str = BsgoLocalization.Get("bgo.text_counters." + countable.Counter.Name);
    if (mission.sectorCard != null)
      str = str + ": " + mission.SectorName;
    this.nameLabel.text = str;
    int num1 = countable.Count;
    int num2 = countable.NeedCount;
    if (mission.Status == Mission.State.Completed || num1 > num2)
      num1 = num2;
    this.progressCurrentLabel.text = num1.ToString() + string.Empty;
    this.progressMaxLabel.text = num2.ToString() + string.Empty;
    Color color1 = num1 != num2 ? Color.white : ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_POSITIVE);
    TextMeshProUGUI textMeshProUgui = this.nameLabel;
    Color color2 = color1;
    this.progressMaxLabel.color = color2;
    Color color3 = color2;
    this.progressSeparatorLabel.color = color3;
    Color color4 = color3;
    this.progressCurrentLabel.color = color4;
    Color color5 = color4;
    textMeshProUgui.color = color5;
  }

  public void EnableProgress(bool enable)
  {
    TextMeshProUGUI textMeshProUgui = this.progressCurrentLabel;
    bool flag1 = enable;
    this.progressMaxLabel.enabled = flag1;
    bool flag2 = flag1;
    this.progressSeparatorLabel.enabled = flag2;
    int num = flag2 ? 1 : 0;
    textMeshProUgui.enabled = num != 0;
  }
}
