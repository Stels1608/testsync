﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorDistanceTextUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using TMPro;
using UnityEngine;

public class HudIndicatorDistanceTextUgui : HudIndicatorBaseComponentUgui, IReceivesSetting, IReceivesTarget, IRequiresLateUpdate
{
  private readonly HudIndicatorDistanceStringCache distanceStringCache = HudIndicatorDistanceStringCache.Instance;
  [SerializeField]
  protected TextMeshProUGUI distanceLabel;
  [NonSerialized]
  public int DisplayedDistance;
  [NonSerialized]
  public int UpdateFrame;
  private bool? wasInMiniMode;
  private ISpaceEntity target;

  protected UIAtlas Atlas
  {
    get
    {
      return HudIndicatorsAtlasLinker.Instance.Atlas;
    }
  }

  private bool IsVisible
  {
    get
    {
      if (HudIndicatorInfo.IsSmallProjectile(this.target))
      {
        if (!this.InScreen)
          return false;
        if (!this.IsActiveWaypoint && !this.IsSelected && !this.IsMultiselected)
          return this.IsDesignated;
        return true;
      }
      if (this.target.SpaceEntityType == SpaceEntityType.Trigger)
      {
        if (this.InScreen)
          return this.IsActiveWaypoint;
        return false;
      }
      if (!this.InScreen)
        return false;
      if (!this.IsActiveWaypoint && !this.IsSelected && !this.IsMultiselected)
        return this.InScannerRange;
      return true;
    }
  }

  protected override void ResetComponentStates()
  {
  }

  protected override void Awake()
  {
    base.Awake();
  }

  public void RemoteLateUpdate()
  {
    if (!this.IsVisible || this.target == null || Game.Me.Ship == null)
      return;
    if (this.IsSelected || this.IsMultiselected)
    {
      this.UpdateFrame = Time.frameCount;
      this.UpdateDistanceText();
    }
    else
    {
      if (this.UpdateFrame > Time.frameCount)
        return;
      this.UpdateDistanceText();
      this.UpdateFrame = HudIndicatorDistanceTicketMachine.RequestUpdateFrame(this);
    }
  }

  private void UpdateDistanceText()
  {
    int distance = (int) (this.target.Position - Game.Me.Ship.Position).magnitude;
    if (this.DisplayedDistance == distance)
      return;
    this.DisplayedDistance = distance;
    this.distanceLabel.textWithoutRelayouting = this.distanceStringCache.GetDistanceString(distance);
  }

  protected string GetDistanceUiLineAtlasSpriteName()
  {
    return "HudIndicator_Distance";
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.target = target;
    this.UpdateView();
    this.UpdateColorAndAlpha();
  }

  protected override void SetColoring()
  {
    this.distanceLabel.color = HudIndicatorColorScheme.Instance.TextColor(this.target);
  }

  protected override void UpdateView()
  {
    this.gameObject.SetActive(this.IsVisible);
    bool? nullable = this.wasInMiniMode;
    if ((nullable.GetValueOrDefault() != this.InMiniMode ? 1 : (!nullable.HasValue ? 1 : 0)) == 0)
      return;
    this.wasInMiniMode = new bool?(this.InMiniMode);
    this.SetAnchoring(!this.InMiniMode ? HudIndicatorDistanceTextUgui.AnchorMode.BottomLeftOfBracket : HudIndicatorDistanceTextUgui.AnchorMode.BelowBracket);
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  public void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    switch (userSetting)
    {
      case UserSetting.HudIndicatorTextSize:
        this.distanceLabel.fontSize = (float) data;
        break;
      case UserSetting.HudIndicatorColorScheme:
        this.SetColoring();
        break;
    }
  }

  private void SetAnchoring(HudIndicatorDistanceTextUgui.AnchorMode anchorMode)
  {
    switch (anchorMode)
    {
      case HudIndicatorDistanceTextUgui.AnchorMode.BelowBracket:
        RectTransform rectTransform1 = this.transform as RectTransform;
        rectTransform1.anchorMin = new Vector2(0.5f, 0.0f);
        rectTransform1.anchorMax = new Vector2(0.5f, 0.0f);
        rectTransform1.pivot = new Vector2(0.5f, 0.5f);
        rectTransform1.anchoredPosition = (Vector2) new Vector3(0.0f, -5f, 0.0f);
        this.distanceLabel.alignment = TextAlignmentOptions.Top;
        break;
      case HudIndicatorDistanceTextUgui.AnchorMode.BottomLeftOfBracket:
        RectTransform rectTransform2 = this.transform as RectTransform;
        rectTransform2.anchorMin = new Vector2(0.0f, 0.0f);
        rectTransform2.anchorMax = new Vector2(0.0f, 0.0f);
        rectTransform2.pivot = new Vector2(0.0f, 0.5f);
        rectTransform2.anchoredPosition = (Vector2) new Vector3(10f, 2f, 0.0f);
        this.distanceLabel.alignment = TextAlignmentOptions.TopLeft;
        break;
    }
  }

  private enum AnchorMode : byte
  {
    BelowBracket,
    BottomLeftOfBracket,
  }
}
