﻿// Decompiled with JetBrains decompiler
// Type: SectorEventStateUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SectorEventStateUpdate
{
  public uint EventObjectId { get; private set; }

  public Faction OwnerFaction { get; private set; }

  public SectorEventState State { get; private set; }

  public Vector3 Position { get; private set; }

  public float Radius { get; private set; }

  public SectorEventStateUpdate(uint eventObjectId, Faction ownerFaction, SectorEventState state, Vector3 position, float radius)
  {
    this.EventObjectId = eventObjectId;
    this.OwnerFaction = ownerFaction;
    this.State = state;
    this.Position = position;
    this.Radius = radius;
  }

  public override string ToString()
  {
    return string.Format("EventObjectId: {0}, OwnerFaction: {1}, State: {2}, Position: {3}, Radius: {4}", (object) this.EventObjectId, (object) this.OwnerFaction, (object) this.State, (object) this.Position, (object) this.Radius);
  }
}
