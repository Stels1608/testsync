﻿// Decompiled with JetBrains decompiler
// Type: AbstractBurstGun
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class AbstractBurstGun : AbstractGun
{
  protected int madeShotCount = int.MaxValue;
  public BurstPool BurstPool;
  protected Burst burst;
  public int ShotCount;

  protected virtual void Awake()
  {
    if ((Object) SpaceLevel.GetLevel() != (Object) null)
      this.BurstPool = SpaceLevel.GetLevel().BurstPool;
    else
      this.BurstPool = new BurstPool();
  }

  public override void Fire(SpaceObject target)
  {
    base.Fire(target);
    this.madeShotCount = 0;
  }

  protected override BulletScript GetNextBullet()
  {
    if ((Object) this.burst == (Object) null)
      this.burst = this.BurstPool.GetBurst(this.BulletPrefab, this.ShotCount);
    BulletScript bulletScript = this.burst.NextBullet();
    if (!this.burst.HasBullet())
      this.burst = (Burst) null;
    return bulletScript;
  }

  protected override void OnDisable()
  {
    base.OnDisable();
    if (Game.IsLoadingLevel || Game.IsQuitting)
      return;
    if ((Object) this.burst != (Object) null)
      this.burst.DestroyBulletRest();
    this.burst = (Burst) null;
  }
}
