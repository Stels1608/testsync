﻿// Decompiled with JetBrains decompiler
// Type: CannonadeSupply
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CannonadeSupply : MonoBehaviour
{
  public float DelayOnReady = 5f;
  private float lastSmallExplosionTime = float.NegativeInfinity;
  private float lastLargeExplosionTime = float.NegativeInfinity;
  private float lastNukeExplosionTime = float.NegativeInfinity;
  private BattleValidator validator = new BattleValidator();
  public Cannonade Cannonade;
  private float smallInterval;
  private float largeInterval;
  private float nukeInterval;
  private Transform _transform;
  private Transform _camera;
  private bool ready;
  private float readyTime;

  public bool Ready
  {
    get
    {
      if (this.ready)
        return (double) Time.time > (double) this.readyTime + (double) this.DelayOnReady;
      return false;
    }
    set
    {
      if (!this.ready && value)
        this.readyTime = Time.time;
      if (this.ready && !value)
        this.readyTime = float.PositiveInfinity;
      this.ready = value;
    }
  }

  private void Start()
  {
    this._transform = this.transform;
    this._camera = Camera.main.transform;
    this.smallInterval = Random.Range(this.Cannonade.SmallMinInterval, this.Cannonade.SmallMaxInterval);
    this.largeInterval = Random.Range(this.Cannonade.LargeMinInterval, this.Cannonade.LargeMaxInterval);
    this.nukeInterval = Random.Range(this.Cannonade.NukeMinInterval, this.Cannonade.NukeMaxInterval);
  }

  private void Update()
  {
    this.Ready = this.validator.IsReady();
    if (!this.Ready)
      return;
    float sqrMagnitude = (this._camera.position - this._transform.position).sqrMagnitude;
    if ((double) sqrMagnitude >= (double) this.Cannonade.MaxDistance * (double) this.Cannonade.MaxDistance)
      return;
    if ((double) Time.time - (double) this.lastSmallExplosionTime > (double) this.smallInterval && (double) sqrMagnitude > (double) this.Cannonade.SmallMinDistance * (double) this.Cannonade.SmallMinDistance)
    {
      this.lastSmallExplosionTime = Time.time;
      this.smallInterval = Random.Range(this.Cannonade.SmallMinInterval, this.Cannonade.SmallMaxInterval);
      this.Explode(this.Cannonade.SmallExplosion);
    }
    if ((double) Time.time - (double) this.lastLargeExplosionTime > (double) this.largeInterval && (double) sqrMagnitude > (double) this.Cannonade.LargeMinDistance * (double) this.Cannonade.LargeMinDistance)
    {
      this.lastLargeExplosionTime = Time.time;
      this.largeInterval = Random.Range(this.Cannonade.LargeMinInterval, this.Cannonade.LargeMaxInterval);
      this.Explode(this.Cannonade.LargeExplosion);
    }
    if ((double) Time.time - (double) this.lastNukeExplosionTime <= (double) this.nukeInterval || (double) sqrMagnitude <= (double) this.Cannonade.NukeMinDistance * (double) this.Cannonade.NukeMinDistance)
      return;
    this.lastNukeExplosionTime = Time.time;
    this.nukeInterval = Random.Range(this.Cannonade.NukeMinInterval, this.Cannonade.NukeMaxInterval);
    this.Explode(this.Cannonade.NukeExplosion);
  }

  private void Explode(GameObject prefab)
  {
    Vector2 vector2 = Random.insideUnitCircle * this.Cannonade.Radius;
    float y = Random.Range(-0.5f, 0.5f) * this.Cannonade.Height;
    Vector3 vector3 = new Vector3(vector2.x, y, vector2.y);
    Object.Instantiate((Object) prefab, this.transform.position + vector3, Quaternion.identity);
  }
}
