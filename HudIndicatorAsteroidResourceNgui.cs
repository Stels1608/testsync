﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorAsteroidResourceNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HudIndicatorAsteroidResourceNgui : HudIndicatorBaseComponentNgui, IReceivesTarget
{
  private UISprite frame;
  private UISprite resourceIcon;
  private bool wasScanned;
  private float smoothDistance;
  private float viewRange;
  private Asteroid asteroid;

  private UIAtlas Atlas
  {
    get
    {
      return HudIndicatorsAtlasLinker.Instance.Atlas;
    }
  }

  public override bool RequiresAnimationNgui
  {
    get
    {
      return false;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.frame = NGUIToolsExtension.AddSprite(this.gameObject, this.Atlas, "HudIndicator_Asteroid_Frame", true, string.Empty);
    this.frame.color = Color.white * 0.75f;
    this.resourceIcon = NGUIToolsExtension.AddSprite(this.gameObject, this.Atlas, "HudIndicator_Asteroid_Blank", true, string.Empty);
  }

  protected override void ResetComponentStates()
  {
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.asteroid = target as Asteroid;
    if (this.asteroid == null)
    {
      Debug.LogWarning((object) ("HudIndicatorAsteroidResourceUgui on non-asteroid type " + (object) target.SpaceEntityType + ". Sure??"));
    }
    else
    {
      if (this.asteroid.WasScanned)
        this.OnScanned(this.asteroid.Resource);
      this.UpdateColorAndAlpha();
    }
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    if (!this.InScreen || !this.wasScanned)
      this.ShowIcons(false);
    else
      this.ShowIcons(this.IsSelected && this.InScreen);
  }

  private void ShowIcons(bool show)
  {
    this.frame.enabled = show;
    this.resourceIcon.enabled = show;
  }

  public override void OnScanned(ItemCountable resource)
  {
    if (resource == null || (int) resource.Count == 0)
      return;
    this.wasScanned = true;
    ResourceType resourceType = (ResourceType) resource.CardGUID;
    switch (resourceType)
    {
      case ResourceType.Plutonium:
        NGUIToolsExtension.SetSprite(this.resourceIcon, this.Atlas, "HudIndicator_Asteroid_Plutonium");
        break;
      case ResourceType.Water:
        NGUIToolsExtension.SetSprite(this.resourceIcon, this.Atlas, "HudIndicator_Asteroid_Water");
        break;
      case ResourceType.Uranium:
        NGUIToolsExtension.SetSprite(this.resourceIcon, this.Atlas, "HudIndicator_Asteroid_Uranium");
        break;
      case ResourceType.Titanium:
        NGUIToolsExtension.SetSprite(this.resourceIcon, this.Atlas, "HudIndicator_Asteroid_Titanium");
        break;
      case ResourceType.Tylium:
        NGUIToolsExtension.SetSprite(this.resourceIcon, this.Atlas, "HudIndicator_Asteroid_Tylium");
        break;
      default:
        Debug.LogWarning((object) ("No icon for unknown resourceType: " + (object) resourceType));
        break;
    }
    this.UpdateView();
  }
}
