﻿// Decompiled with JetBrains decompiler
// Type: HudMenuButtonWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class HudMenuButtonWidget : NguiWidget
{
  private const string btnBackgroundNormal = "submenu_btn_normal";
  private const string btnBackgroundActive = "submenu_btn_active";
  private const string btnBackgroundHover = "submenu_btn_hover";
  [SerializeField]
  protected UISprite iconSprite;
  [SerializeField]
  protected UISprite bgSprite;
  public AnonymousDelegate handleClick;

  public UISprite BgSprite
  {
    get
    {
      return this.bgSprite;
    }
    set
    {
      this.bgSprite = value;
    }
  }

  public UISprite IconSprite
  {
    get
    {
      return this.iconSprite;
    }
    set
    {
      this.iconSprite = value;
    }
  }

  public override void OnClick()
  {
    this.OnHover(false);
    if (this.IsEnabled && this.handleClick != null && this.handleClick.GetInvocationList().Length > 0)
      this.handleClick();
    base.OnClick();
  }

  public override void OnHover(bool isOver)
  {
    if (isOver && this.bgSprite.spriteName != "submenu_btn_active")
    {
      this.bgSprite.spriteName = "submenu_btn_hover";
      this.iconSprite.color = Color.black;
    }
    if (!isOver && this.bgSprite.spriteName == "submenu_btn_hover")
    {
      this.bgSprite.spriteName = "submenu_btn_normal";
      this.iconSprite.color = Color.white;
    }
    base.OnHover(isOver);
  }
}
