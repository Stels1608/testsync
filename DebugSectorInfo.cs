﻿// Decompiled with JetBrains decompiler
// Type: DebugSectorInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class DebugSectorInfo : DebugBehaviour<DebugSectorInfo>
{
  private Vector2 scroll = new Vector2();
  private string info = "empty";

  private void Start()
  {
    this.windowID = 6;
    this.SetSize(300f, 300f);
  }

  protected override void WindowFunc()
  {
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.Box(this.info);
    GUILayout.EndScrollView();
  }

  private void Update()
  {
    if ((Object) SpaceLevel.GetLevel() != (Object) null)
    {
      PlayerShip playerShip = SpaceLevel.GetLevel().PlayerShip;
      SpaceLevel level = SpaceLevel.GetLevel();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.AppendFormat("SectorID = {0}\n", (object) level.ServerID);
      stringBuilder.AppendFormat("TimeOrigin = {0}\n", (object) level.TimeOrigin);
      stringBuilder.AppendFormat("IsInstance = {0}\n", (object) level.Location);
      stringBuilder.AppendFormat("Tick = {0}\n", (object) Tick.Current);
      stringBuilder.AppendFormat("GFX quality = {0}\n", (object) QualitySettings.GetQualityLevel());
      if (playerShip != null)
        stringBuilder.AppendFormat("Me = {0}:{1}\n", (object) (bool) playerShip.IsLoaded, (object) playerShip.ObjectID);
      ObjectRegistry objectRegistry = level.GetObjectRegistry();
      int num1 = 0;
      int num2 = 0;
      Dictionary<SpaceEntityType, int> dictionary1 = new Dictionary<SpaceEntityType, int>();
      foreach (SpaceObject spaceObject in objectRegistry.GetAll())
      {
        if (!dictionary1.ContainsKey(spaceObject.SpaceEntityType))
          dictionary1.Add(spaceObject.SpaceEntityType, 0);
        Dictionary<SpaceEntityType, int> dictionary2;
        SpaceEntityType spaceEntityType;
        (dictionary2 = dictionary1)[spaceEntityType = spaceObject.SpaceEntityType] = dictionary2[spaceEntityType] + 1;
        if ((bool) spaceObject.IsLoaded)
          ++num1;
        ++num2;
      }
      stringBuilder.AppendFormat("-----------\n");
      stringBuilder.AppendFormat("Objects:\n");
      using (Dictionary<SpaceEntityType, int>.Enumerator enumerator = dictionary1.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<SpaceEntityType, int> current = enumerator.Current;
          stringBuilder.AppendFormat("{0} = {1}\n", (object) current.Key.ToString(), (object) current.Value);
        }
      }
      stringBuilder.AppendFormat("Total: {0}/{1}\n", (object) num1, (object) num2);
      this.info = stringBuilder.ToString();
    }
    else
      this.info = "Need Space Level";
  }
}
