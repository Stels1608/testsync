﻿// Decompiled with JetBrains decompiler
// Type: ShipAbilityTarget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum ShipAbilityTarget
{
  Asteroid = 1,
  Ship = 2,
  Any = 4,
  Missile = 8,
  Planetoid = 16,
  Mine = 32,
  JumpTargetTransponder = 64,
  Comet = 128,
}
