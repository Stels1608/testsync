﻿// Decompiled with JetBrains decompiler
// Type: SectorOutpostPointsUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class SectorOutpostPointsUpdate : GalaxyMapUpdate
{
  public int OutpostPoints { get; private set; }

  public SectorOutpostPointsUpdate(Faction faction, uint sectorId, int outpostPoints)
    : base(GalaxyUpdateType.SectorOutpostPoints, faction, (int) sectorId)
  {
    this.OutpostPoints = outpostPoints;
  }

  public override string ToString()
  {
    return "[SectorOutpostPointsUpdate] (Faction: " + (object) this.Faction + ", SectorId: " + (object) this.SectorId + ", OutpostPoints:" + (object) this.OutpostPoints + ")";
  }
}
