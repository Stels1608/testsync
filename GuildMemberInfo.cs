﻿// Decompiled with JetBrains decompiler
// Type: GuildMemberInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class GuildMemberInfo
{
  public uint PlayerId { get; private set; }

  public string PlayerName { get; private set; }

  public byte PlayerLevel { get; private set; }

  public GuildRole PlayerRole { get; private set; }

  public DateTime LastLogout { get; private set; }

  public GuildMemberInfo(uint playerId, string playerName, byte playerLevel, GuildRole playerRole, DateTime lastLogout)
  {
    this.PlayerId = playerId;
    this.PlayerName = playerName;
    this.PlayerLevel = playerLevel;
    this.PlayerRole = playerRole;
    this.LastLogout = lastLogout;
  }
}
