﻿// Decompiled with JetBrains decompiler
// Type: ShopItemType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum ShopItemType
{
  None,
  Resource,
  Augment,
  Round,
  Flare,
  Mine,
  Missile,
  Power,
  Repair,
  PointDefense,
  Flak,
  JumpTargetTransponder,
  Junk,
  Radio,
  TechAnalysis,
  Weapon,
  Computer,
  Hull,
  Engine,
  ShipPaint,
  Avionics,
  StarterKit,
  Torpedo,
  Ship,
  MetalPlate,
  AntiCapitalMissile,
  Unknown,
}
