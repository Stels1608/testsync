﻿// Decompiled with JetBrains decompiler
// Type: BuyEventItemAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public class BuyEventItemAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    EventShop eventShop = ((ShopDataProvider) this.OwnerFacade.FetchDataProvider("ShopDataProvider")).EventShop;
    ItemAmountTupel itemAmountTupel = (ItemAmountTupel) message.Data;
    ShipItem shipItem = itemAmountTupel.Item;
    if (!shipItem.ShopItemCard.BuyPrice.IsEnoughInHangar(itemAmountTupel.Amount))
    {
      if (shipItem.ShopItemCard.BuyPrice.GetBuyForCubitsPrice() > Game.Me.Hold.Cubits)
        return;
      using (Dictionary<ShipConsumableCard, uint>.Enumerator enumerator = shipItem.ShopItemCard.BuyPrice.GetMissingResourcesAmount(1).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ShipConsumableCard, uint> current = enumerator.Current;
          ShipItem byGuid = eventShop.GetByGUID(current.Key.CardGUID);
          if (byGuid == null)
          {
            Debug.LogError((object) ("Required resource is not for sale in the EventShop: " + current.Key.GUICard.Name));
            return;
          }
          uint count = current.Value;
          byGuid.MoveTo((IContainer) Game.Me.Hold, count);
        }
      }
    }
    shipItem.MoveTo((IContainer) Game.Me.Hold, itemAmountTupel.Amount);
    eventShop.RequestItems();
  }
}
