﻿// Decompiled with JetBrains decompiler
// Type: HelpMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class HelpMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "HelpMediator";
  private HelpWindowWidget helpWindow;
  private HelpContent helpContent;

  public HelpMediator()
    : base("HelpMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.ShowHelpWindow);
    this.AddMessageInterest(Message.ShowHelpScreen);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.PlayerFactionReply);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.PlayerFactionReply:
        this.helpContent.SwitchAtlas((Faction) message.Data);
        break;
      case Message.LoadNewLevel:
        this.HideHelpWindow();
        break;
      default:
        if (id != Message.ShowHelpWindow)
        {
          if (id != Message.ShowHelpScreen)
          {
            if (id == Message.UiCreated)
            {
              this.CreateView();
              break;
            }
            Log.Warning((object) ("Unknown Message was catched but not handled by the HelpMediator => " + (object) message.Id));
            break;
          }
          this.ShowHelpScreen((HelpScreenType) message.Data);
          break;
        }
        this.ShowHelpWindow();
        break;
    }
  }

  private void CreateView()
  {
    this.helpWindow = ((GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/ui_elements/Help/HelpWindow"))).GetComponent<HelpWindowWidget>();
    if ((Object) this.helpWindow == (Object) null)
      Debug.LogError((object) "HelpWindowWidget was not registered correctly");
    (this.OwnerFacade.FetchDataProvider("GuiDataProvider") as GuiDataProvider).AddWindow((WindowWidget) this.helpWindow);
    this.helpContent = this.helpWindow.helpContent;
    if (!((Object) this.helpContent != (Object) null))
      return;
    this.OwnerFacade.SendMessage(Message.RegisterHelpContent, (object) this.helpWindow.helpContent);
  }

  private void ShowHelpWindow()
  {
    if (!((Object) null != (Object) this.helpWindow))
      return;
    if (this.helpWindow.IsOpen)
      this.helpWindow.Close();
    else
      this.helpWindow.Open();
  }

  private void ShowHelpScreen(HelpScreenType type)
  {
    if (!((Object) null != (Object) this.helpWindow))
      return;
    if (this.helpWindow.IsOpen)
    {
      if (this.helpContent.panel.activeTabButton.GetComponent<HelpButton>().helpType == type)
        this.helpWindow.Close();
      else
        this.SetActiveScreen(type);
    }
    else
    {
      this.SetActiveScreen(type);
      this.helpWindow.Open();
    }
  }

  private void SetActiveScreen(HelpScreenType type)
  {
    foreach (HelpButton componentsInChild in this.helpContent.helpListObject.GetComponentsInChildren<HelpButton>(true))
    {
      if (componentsInChild.helpType == type)
      {
        this.helpContent.panel.SetActiveButton(componentsInChild.tabPosition);
        break;
      }
    }
  }

  private void HideHelpWindow()
  {
    if (!((Object) this.helpWindow != (Object) null) || !this.helpWindow.IsOpen)
      return;
    this.helpWindow.Close();
  }
}
