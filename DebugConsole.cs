﻿// Decompiled with JetBrains decompiler
// Type: DebugConsole
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class DebugConsole : DebugBehaviour<DebugConsole>
{
  private Vector2 scroll = new Vector2();
  private string cmd = string.Empty;

  private void Start()
  {
    this.windowID = 14;
    this.SetSize(400f, 300f);
  }

  protected override void WindowFunc()
  {
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    GUILayout.Label("Only server commands, be careful :)");
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    using (List<string>.Enumerator enumerator = DebugUtility.Logs().GetEnumerator())
    {
      while (enumerator.MoveNext())
        GUILayout.Label(enumerator.Current);
    }
    GUILayout.EndScrollView();
    if (Event.current.isKey && Event.current.keyCode == KeyCode.Return)
    {
      Event.current.Use();
      try
      {
        DebugUtility.LogInfo("> " + this.cmd);
        this.scroll = new Vector2(0.0f, float.MaxValue);
        this.DoCommand(this.cmd);
      }
      catch (Exception ex)
      {
      }
      this.cmd = string.Empty;
    }
    this.cmd = GUILayout.TextField(this.cmd);
  }

  private void DoCommand(string line)
  {
    string str = this.CleanString(line);
    if (!(str != string.Empty))
      return;
    DebugProtocol.GetProtocol().CommandList(str.Split(' '));
  }

  private string CleanString(string toClean)
  {
    return this.CleanString(toClean.Trim(' ', '\n', '\t', '\r', '`'), '`', '\n', '\t', '\r');
  }

  private string CleanString(string toClean, params char[] toRemove)
  {
    string str = string.Empty;
    foreach (char ch1 in toClean)
    {
      bool flag = false;
      foreach (char ch2 in toRemove)
      {
        if ((int) ch1 == (int) ch2)
          flag = true;
      }
      if (!flag)
        str += (string) (object) ch1;
    }
    return str;
  }
}
