﻿// Decompiled with JetBrains decompiler
// Type: WarpManeuver
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WarpManeuver : Maneuver
{
  protected Vector3 position;
  protected Euler3 euler3;

  public override string ToString()
  {
    return string.Format("WarpManeuver: position={0}, euler={1}", (object) this.position, (object) this.euler3);
  }

  public override MovementFrame NextFrame(Tick tick, MovementFrame prevFrame)
  {
    return new MovementFrame(this.position, this.euler3, Vector3.zero, Vector3.zero, Euler3.zero);
  }

  public override void Read(BgoProtocolReader pr)
  {
    base.Read(pr);
    this.startTick = pr.ReadTick();
    this.position = pr.ReadVector3();
    this.euler3 = pr.ReadEuler();
    this.options.Read(pr);
  }
}
