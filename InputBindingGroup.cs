﻿// Decompiled with JetBrains decompiler
// Type: InputBindingGroup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class InputBindingGroup : MonoBehaviour
{
  private readonly List<InputBindingElement> inputBindingElements = new List<InputBindingElement>();
  public FoldOutWidget button;
  public GameObject keyListObject;
  private GameObject parent;

  public void Init(string groupName, List<Action> actions, InputDevice device, GameObject parent)
  {
    this.parent = parent;
    this.button.groupname.text = groupName;
    this.button.name = "inputBindingGroup_" + groupName;
    GameObject prefab = (GameObject) Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingElement");
    int num = 0;
    using (List<Action>.Enumerator enumerator = actions.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Action current = enumerator.Current;
        GameObject gameObject = NGUITools.AddChild(this.keyListObject, prefab);
        gameObject.name = num.ToString() + "_actions";
        InputBindingElement component = gameObject.GetComponent<InputBindingElement>();
        if ((Object) component != (Object) null)
          component.InjectData(current, device);
        else
          Debug.LogWarning((object) "BIND ELEMENT IS NULL");
        this.inputBindingElements.Add(component);
        ++num;
      }
    }
    this.button.foldOutContent.GetComponent<UITable>().Reposition();
    this.button.parentTable = parent.GetComponent<UITable>();
    this.button.parentTable.Reposition();
  }

  public void UpdateBindingButtons(List<IInputBinding> list)
  {
    using (List<InputBindingElement>.Enumerator enumerator = this.inputBindingElements.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateBindingButton((IEnumerable<IInputBinding>) list);
    }
  }

  public void ShowBindingButtons(bool isVisible)
  {
    this.button.gameObject.SetActive(isVisible);
    this.RepositionAll();
  }

  private void RepositionAll()
  {
    this.button.foldOutContent.GetComponent<UITable>().Reposition();
    this.button.parentTable = this.parent.GetComponent<UITable>();
    this.button.parentTable.Reposition();
  }

  public void RepaintBindingElements()
  {
    using (List<InputBindingElement>.Enumerator enumerator = this.inputBindingElements.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateUiElements();
    }
  }
}
