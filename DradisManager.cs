﻿// Decompiled with JetBrains decompiler
// Type: DradisManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class DradisManager : DradisHud
{
  public float timeBetweenUpdates = 0.1f;
  private const float SELECT_THROUGH_CLICK_DISTANCE = 5f;
  public float timeLastUpdate;
  public static bool AreHostilesInRange;

  public DradisManager(AtlasCache atlasCache)
    : base(atlasCache)
  {
    this.cycleHostile.Handler = (AnonymousDelegate) (() =>
    {
      this.Next(SpaceLevel.GetLevel().GetPlayerTarget() as Ship, DradisManager.GetShipsInRange(Relation.Enemy));
      if (!this.hostileBlinker.IsRendered)
        return;
      StoryProtocol.GetProtocol().TriggerControl(StoryProtocol.ControlType.TargetEnemy);
    });
    this.cycleFriendly.Handler = (AnonymousDelegate) (() =>
    {
      this.Next(SpaceLevel.GetLevel().GetPlayerTarget() as Ship, DradisManager.GetShipsInRange(Relation.Friend));
      if (!this.friendlyBlinker.IsRendered)
        return;
      StoryProtocol.GetProtocol().TriggerControl(StoryProtocol.ControlType.TargetAlly);
    });
    this.handlers.Add(Action.NearestFriendly, this.cycleFriendly.Handler);
    this.handlers.Add(Action.NearestEnemy, this.cycleHostile.Handler);
  }

  public override void Update()
  {
    base.Update();
    if ((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null)
      return;
    Ship ship = (Ship) SpaceLevel.GetLevel().GetPlayerShip();
    if (ship == null)
      return;
    Vector3 vector3 = this.AccountForAlignment(ship.Forward);
    float num = 57.29578f * Mathf.Atan2(vector3.x, vector3.z);
    this.marker.SetOrientation(Mathf.Clamp01(((double) num >= 0.0 ? num : 360f + num) / 360f));
    if ((double) Time.time - (double) this.timeLastUpdate <= (double) this.timeBetweenUpdates)
      return;
    if ((double) this.DradisRange > 0.0)
      this.ProcessObjects(this.GetObjects3DInRange(), ship.Position);
    this.timeLastUpdate = Time.time;
  }

  private List<Map.Object3D> GetObjects3DInRange()
  {
    List<Map.Object3D> mapObjects = new List<Map.Object3D>();
    if (this.GetObjectFilterValue(MapAlignmentFilter.Hostile))
      this.AddHostileObjectsInRange(mapObjects);
    DradisManager.AreHostilesInRange = mapObjects.Count > 0;
    if (this.GetObjectFilterValue(MapAlignmentFilter.Neutral))
      this.AddNeutralObjectsInRange(mapObjects);
    if (this.GetObjectFilterValue(MapAlignmentFilter.Friendly))
      this.AddFriendlyObjectsInRange(mapObjects);
    return mapObjects;
  }

  private void AddNeutralObjectsInRange(List<Map.Object3D> mapObjects)
  {
    foreach (SpaceObject spaceObject in SpaceLevel.GetLevel().GetObjectRegistry().GetAllLoaded())
    {
      if (spaceObject.IsInDradisRange)
      {
        Map.ObjectType type;
        switch (spaceObject.SpaceEntityType)
        {
          case SpaceEntityType.Debris:
            type = Map.ObjectType.Debris;
            break;
          case SpaceEntityType.Asteroid:
          case SpaceEntityType.AsteroidBot:
            type = Map.ObjectType.Asteroid;
            break;
          case SpaceEntityType.MiningShip:
            type = Map.ObjectType.MiningShip;
            break;
          case SpaceEntityType.Planetoid:
            type = Map.ObjectType.Planetoid;
            break;
          default:
            continue;
        }
        mapObjects.Add(new Map.Object3D(type, spaceObject.Position, spaceObject));
      }
    }
  }

  private void AddFriendlyObjectsInRange(List<Map.Object3D> mapObjects)
  {
    foreach (SpaceObject spaceObject in SpaceLevel.GetLevel().GetObjectRegistry().GetAllLoaded())
    {
      if (spaceObject.PlayerRelation == Relation.Friend && spaceObject.IsInDradisRange)
      {
        Map.ObjectType type;
        switch (spaceObject.SpaceEntityType)
        {
          case SpaceEntityType.Player:
          case SpaceEntityType.BotFighter:
            type = Map.ObjectType.FighterFriendly;
            break;
          case SpaceEntityType.WeaponPlatform:
          case SpaceEntityType.Cruiser:
          case SpaceEntityType.Outpost:
            type = Map.ObjectType.CruiserFriendly;
            break;
          case SpaceEntityType.MiningShip:
            type = Map.ObjectType.MiningShip;
            break;
          case SpaceEntityType.JumpBeacon:
            type = Map.ObjectType.JumpBeacon;
            break;
          default:
            continue;
        }
        mapObjects.Add(new Map.Object3D(type, spaceObject.Position, spaceObject));
      }
    }
  }

  private void AddHostileObjectsInRange(List<Map.Object3D> mapObjects)
  {
    foreach (SpaceObject spaceObject in SpaceLevel.GetLevel().GetObjectRegistry().GetAllLoaded())
    {
      if (spaceObject.PlayerRelation == Relation.Enemy && spaceObject.IsInDradisRange)
      {
        Map.ObjectType type;
        switch (spaceObject.SpaceEntityType)
        {
          case SpaceEntityType.Player:
          case SpaceEntityType.BotFighter:
            type = Map.ObjectType.FighterHostile;
            break;
          case SpaceEntityType.WeaponPlatform:
          case SpaceEntityType.Cruiser:
          case SpaceEntityType.Outpost:
            type = Map.ObjectType.CruiserHostile;
            break;
          case SpaceEntityType.MiningShip:
            type = Map.ObjectType.MiningShip;
            break;
          case SpaceEntityType.JumpBeacon:
            type = Map.ObjectType.JumpBeacon;
            break;
          default:
            continue;
        }
        mapObjects.Add(new Map.Object3D(type, spaceObject.Position, spaceObject));
      }
    }
  }

  private static List<Ship> GetShipsInRange(Relation relation)
  {
    List<Ship> objectList = new List<Ship>();
    foreach (SpaceObject spaceObject in SpaceLevel.GetLevel().GetObjectRegistry().GetAllLoaded())
    {
      if (spaceObject.PlayerRelation == relation && spaceObject.IsInMapRange)
      {
        switch (spaceObject.SpaceEntityType)
        {
          case SpaceEntityType.Player:
          case SpaceEntityType.Cruiser:
          case SpaceEntityType.BotFighter:
            objectList.Add(spaceObject as Ship);
            continue;
          default:
            continue;
        }
      }
    }
    DradisManager.SortByDistance(ref objectList);
    return objectList;
  }

  private static void SortByDistance(ref List<Ship> objectList)
  {
    objectList.Sort((Comparison<Ship>) ((x, y) =>
    {
      if ((double) x.GetSqrDistance() < (double) y.GetSqrDistance())
        return -1;
      return (double) x.GetSqrDistance() > (double) y.GetSqrDistance() ? 1 : 0;
    }));
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (!base.OnMouseUp(mousePosition, mouseKey))
      return false;
    if (mouseKey == KeyCode.Mouse0)
    {
      Vector2 v2 = mousePosition.ToV2();
      using (List<Map.MapObject>.Enumerator enumerator = this.ships.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Map.MapObject current = enumerator.Current;
          if ((double) (current.rect.AbsPosition - float2.FromV2(v2)).magnitude < 5.0)
          {
            TargetSelector.SelectTargetRequest(current.obj, false);
            return true;
          }
        }
      }
      using (List<Map.MapObject>.Enumerator enumerator = this.asteroids.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Map.MapObject current = enumerator.Current;
          if (current.type == Map.ObjectType.Debris && (double) (current.rect.AbsPosition - float2.FromV2(v2)).magnitude < 5.0)
          {
            TargetSelector.SelectTargetRequest(current.obj, false);
            return true;
          }
        }
      }
    }
    else if (mouseKey == KeyCode.Mouse1)
      Game.GUIManager.Find<GUISystemMap>().ToggleIsRendered();
    return true;
  }

  private void Next(Ship target, List<Ship> ships)
  {
    Ship ship = (Ship) null;
    if (ships.Count > 0)
    {
      int num = ships.IndexOf(target);
      ship = ships[(num + 1) % ships.Count];
    }
    TargetSelector.SelectTargetRequest((SpaceObject) ship, false);
  }
}
