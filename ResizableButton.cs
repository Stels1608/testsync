﻿// Decompiled with JetBrains decompiler
// Type: ResizableButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

internal class ResizableButton : ButtonWidget
{
  private BoxCollider boxCollider;
  [SerializeField]
  public UIWidget.Pivot pivot;
  public string text;
  public bool repositionNow;

  public string Text
  {
    get
    {
      return this.text;
    }
    set
    {
      this.text = value;
      this.ResizeButton();
      this.RepositionPivot();
    }
  }

  public override void Start()
  {
    if ((Object) this.bgSprite == (Object) null)
      this.bgSprite = this.GetComponentInChildren<UISprite>();
    if ((Object) this.textLabel == (Object) null)
      this.textLabel = this.GetComponentInChildren<UILabel>();
    this.boxCollider = this.GetComponent<BoxCollider>();
    this.ResizeButton();
    this.RepositionPivot();
    base.Start();
  }

  public void ResizeButton()
  {
    this.textLabel.text = this.text;
    this.bgSprite.transform.localScale = new Vector3((float) ((double) this.textLabel.width * (double) this.textLabel.transform.localScale.x + 28.0), this.bgSprite.transform.localScale.y, 1f);
    this.boxCollider.size = this.bgSprite.transform.localScale;
  }

  public override void OnClick()
  {
    this.ResizeButton();
    this.RepositionPivot();
    base.OnClick();
  }

  public void RepositionPivot()
  {
    this.bgSprite.pivot = this.pivot;
    float num1 = 0.0f;
    float num2 = 0.0f;
    Vector3 vector3 = new Vector3(this.bgSprite.transform.localPosition.x, this.bgSprite.transform.localPosition.y, this.bgSprite.transform.localPosition.z);
    if (this.pivot == UIWidget.Pivot.TopLeft || this.pivot == UIWidget.Pivot.Left || this.pivot == UIWidget.Pivot.BottomLeft)
      num1 = this.bgSprite.transform.localScale.x / 2f;
    else if (this.pivot == UIWidget.Pivot.TopRight || this.pivot == UIWidget.Pivot.Right || this.pivot == UIWidget.Pivot.BottomRight)
      num1 = (float) (-(double) this.bgSprite.transform.localScale.x / 2.0);
    if (this.pivot == UIWidget.Pivot.TopLeft || this.pivot == UIWidget.Pivot.Top || this.pivot == UIWidget.Pivot.TopRight)
      num2 = (float) (-(double) this.bgSprite.transform.localScale.y / 2.0);
    else if (this.pivot == UIWidget.Pivot.BottomLeft || this.pivot == UIWidget.Pivot.Bottom || this.pivot == UIWidget.Pivot.BottomRight)
      num2 = this.bgSprite.transform.localScale.y / 2f;
    vector3 = new Vector3(vector3.x + num1, vector3.y + num2, this.textLabel.transform.localPosition.z);
    this.textLabel.transform.localPosition = vector3;
    this.boxCollider.center = new Vector3(vector3.x, vector3.y, this.boxCollider.center.z);
  }

  private void LateUpdate()
  {
    if (!this.repositionNow)
      return;
    this.repositionNow = false;
    this.Reposition();
  }

  private void Reposition()
  {
    this.ResizeButton();
    this.RepositionPivot();
  }
}
