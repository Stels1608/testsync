﻿// Decompiled with JetBrains decompiler
// Type: CharacterCreationWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CharacterCreationWindow : AvatarWindow
{
  private string cachedName;
  public Text customizeLabel;
  public Text enterYourNameLabel;
  public Text genderLabel;
  public RectTransform mainPanel;
  public InputField nameField;
  public Button submitButton;
  public Text titleLabel;

  protected override void Start()
  {
    base.Start();
    this.nameField.placeholder.enabled = false;
    EventTrigger eventTrigger = this.nameField.gameObject.AddComponent<EventTrigger>();
    EventTrigger.Entry entry = new EventTrigger.Entry() { eventID = EventTriggerType.Select, callback = new EventTrigger.TriggerEvent() };
    entry.callback.AddListener(new UnityAction<BaseEventData>(this.OnNameFieldClicked));
    eventTrigger.triggers = new List<EventTrigger.Entry>();
    eventTrigger.triggers.Add(entry);
    this.submitButton.onClick.AddListener(new UnityAction(this.OnSubmit));
    this.nameField.onValidateInput += new InputField.OnValidateInput(this.OnValidateInput);
  }

  private char OnValidateInput(string text, int charIndex, char addedChar)
  {
    if (Tools.ValidNameChar(addedChar))
      return addedChar;
    return char.MinValue;
  }

  protected override void Localize()
  {
    this.titleLabel.text = BsgoLocalization.Get("bgo.gui_avatar_common.character_creation").ToUpperInvariant();
    this.enterYourNameLabel.text = BsgoLocalization.Get(this.faction != Faction.Colonial ? "bgo.gui_avatar_cylon.enter_your_name" : "bgo.gui_avatar_human.enter_your_name");
    this.genderLabel.text = BsgoLocalization.Get("bgo.gui_avatar_human.choose_gender");
    this.customizeLabel.text = BsgoLocalization.Get(this.faction != Faction.Colonial ? "bgo.gui_avatar_cylon.customize" : "bgo.gui_avatar_human.customize");
    this.submitButton.GetComponentInChildren<Text>().text = BsgoLocalization.Get(this.faction != Faction.Colonial ? "bgo.gui_avatar_cylon.finish" : "bgo.gui_avatar_human.finish");
  }

  [DebuggerHidden]
  protected override IEnumerator Resize()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CharacterCreationWindow.\u003CResize\u003Ec__Iterator1E() { \u003C\u003Ef__this = this };
  }

  private void OnNameFieldClicked(BaseEventData eventData)
  {
    if (this.cachedName == null)
      return;
    this.nameField.placeholder.enabled = false;
    this.nameField.text = this.cachedName;
    this.cachedName = (string) null;
  }

  public void OnNameAccepted()
  {
    PlayerProtocol.GetProtocol().ChooseName(this.nameField.text);
    PlayerProtocol.GetProtocol().Create(this.avatar.GetAvatarDescription(this.faction));
  }

  public void OnNameRejected()
  {
    this.cachedName = this.nameField.text;
    InputField inputField = this.nameField;
    string str1 = string.Empty;
    this.nameField.textComponent.text = str1;
    string str2 = str1;
    inputField.text = str2;
    this.nameField.placeholder.GetComponent<Text>().text = BsgoLocalization.Get("bgo.gui_avatar_common.name_rejected");
    this.nameField.placeholder.enabled = true;
    this.LockUi(false);
  }

  private void OnSubmit()
  {
    if (string.IsNullOrEmpty(this.nameField.text))
    {
      this.OnNameRejected();
    }
    else
    {
      PlayerProtocol.GetProtocol().CheckNameAvailability(this.nameField.text);
      this.LockUi(true);
    }
  }
}
