﻿// Decompiled with JetBrains decompiler
// Type: HeavyFightNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class HeavyFightNotification : OnScreenNotification
{
  private readonly GUICard sectorCard;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.HeavyFight;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Neutral;
    }
  }

  public override string TextMessage
  {
    get
    {
      return BsgoLocalization.Get("%$bgo.notif.heavy_fight%", (object) this.sectorCard.Name);
    }
  }

  public override bool Show
  {
    get
    {
      return OnScreenNotification.ShowHeavyFightingMessages;
    }
  }

  public HeavyFightNotification(GUICard sectorCard)
  {
    this.sectorCard = sectorCard;
    FacadeFactory.GetInstance().SendMessage(Message.CombatLogOk, (object) this.TextMessage);
  }
}
