﻿// Decompiled with JetBrains decompiler
// Type: DebugHappyHour
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class DebugHappyHour
{
  private static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
  private static bool enabled;

  public static void ToggleHappyHour()
  {
    DebugHappyHour.enabled = !DebugHappyHour.enabled;
    if (DebugHappyHour.enabled)
    {
      DebugHappyHour.EnableHappyHour();
      Game.SpecialOfferManager.Check();
    }
    else
      DebugHappyHour.DisableHappyHour();
  }

  private static void EnableHappyHour()
  {
    uint cardGUID = 66666;
    long num = DebugHappyHour.CurrentTimeMillis() + 7200000L;
    RewardCard rewardCard = new RewardCard(cardGUID);
    GUICard guiCard = new GUICard(cardGUID);
    guiCard.Key = "bonus_happyhour";
    rewardCard.GUICard = guiCard;
    Game.SpecialOfferManager.SpecialOfferCard = rewardCard;
    Game.SpecialOfferManager.SpecialOfferBonusEndTime = (double) num;
    Game.SpecialOfferManager.UpdateSpecialOffer(true);
    Game.Ticker.AddPendingCountdown(guiCard.ShortDescription, num / 1000L);
    Game.SpecialOfferManager.UpdateHappyHour(true);
  }

  private static void DisableHappyHour()
  {
    Game.SpecialOfferManager.UpdateSpecialOffer(false);
    Game.SpecialOfferManager.UpdateHappyHour(false);
  }

  public static long CurrentTimeMillis()
  {
    return (long) (DateTime.UtcNow - DebugHappyHour.Jan1st1970).TotalMilliseconds;
  }
}
