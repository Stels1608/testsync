﻿// Decompiled with JetBrains decompiler
// Type: DynamicEventTrigger
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DynamicEventTrigger : SpaceObject
{
  public float Radius
  {
    get
    {
      return this.SectorEventCard.Radius;
    }
  }

  public SectorEventCard SectorEventCard { get; private set; }

  public DynamicEventTrigger(uint objectId)
    : base(objectId)
  {
    this.AutoSubscribe = true;
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    uint cardGUID = r.ReadGUID();
    this.Position = Vector3.zero;
    this.SectorEventCard = (SectorEventCard) Game.Catalogue.FetchCard(cardGUID, CardView.SectorEvent);
    this.AreCardsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.SectorEventCard
    });
    this.OnLocate();
    this.IsConstructed.Set();
    this.Subscribe();
  }
}
