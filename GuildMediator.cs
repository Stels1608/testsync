﻿// Decompiled with JetBrains decompiler
// Type: GuildMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System.Collections.Generic;

public class GuildMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "GuildMediator";

  public GuildMediator()
    : base("GuildMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.LevelLoaded);
    this.AddMessageInterest(Message.GuildInfo);
    this.AddMessageInterest(Message.GuildQuit);
    this.AddMessageInterest(Message.GuildInvite);
    this.AddMessageInterest(Message.GuildInviteResult);
    this.AddMessageInterest(Message.GuildMemberUpdate);
    this.AddMessageInterest(Message.GuildMemberRemoved);
    this.AddMessageInterest(Message.GuildOperationResult);
    this.AddMessageInterest(Message.GuildMemberPromotion);
    this.AddMessageInterest(Message.GuildRankNameChanged);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.GuildQuit:
        Game.Me.Guild._Reset();
        break;
      case Message.GuildInfo:
        this.OnGuildInfo((GuildInfoMessage) message);
        break;
      case Message.GuildInvite:
        this.OnGuildInvite((GuildInviteMessage) message);
        break;
      case Message.GuildInviteResult:
        this.OnGuildInviteResult((GuildInviteResultMessage) message);
        break;
      case Message.GuildMemberUpdate:
        this.OnGuildMemberUpdate((GuildMemberInfo) message.Data);
        break;
      case Message.GuildMemberRemoved:
        this.OnRemoveGuildMember((GuildMemberRemovedMessage) message);
        break;
      case Message.GuildOperationResult:
        this.OnGuildOperationResult((GuildOperationResultMessage) message);
        break;
      case Message.GuildMemberPromotion:
        this.OnGuildPromotion((GuildMemberPromotionMessage) message);
        break;
      case Message.GuildRankNameChanged:
        this.OnGuildRankNameChanged((GuildRankNameChangedMessage) message);
        break;
    }
  }

  private void OnGuildInfo(GuildInfoMessage guildInfoMessage)
  {
    List<Player> players = new List<Player>();
    using (List<GuildMemberInfo>.Enumerator enumerator = guildInfoMessage.Members.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GuildMemberInfo current = enumerator.Current;
        Player player = Game.Players[current.PlayerId];
        player.lastLogout = current.LastLogout;
        player.guildId = guildInfoMessage.GuildId;
        player.GuildRole = current.PlayerRole;
        player.Level = current.PlayerLevel;
        player.Name = current.PlayerName;
        player.Faction = Game.Me.Faction;
        players.Add(player);
      }
    }
    Game.Me.Guild._SetInfo(guildInfoMessage.GuildId, guildInfoMessage.GuildName, players, guildInfoMessage.Ranks);
  }

  private void OnGuildInvite(GuildInviteMessage inviteMessage)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuildMediator.\u003COnGuildInvite\u003Ec__AnonStoreyD7 inviteCAnonStoreyD7 = new GuildMediator.\u003COnGuildInvite\u003Ec__AnonStoreyD7();
    // ISSUE: reference to a compiler-generated field
    inviteCAnonStoreyD7.guildId = inviteMessage.GuildId;
    // ISSUE: reference to a compiler-generated field
    inviteCAnonStoreyD7.inviterId = inviteMessage.InviterId;
    // ISSUE: reference to a compiler-generated field
    string str = Game.Players[inviteCAnonStoreyD7.inviterId].Name;
    string mainText = BsgoLocalization.Get("bgo.community.guild_invite", (object) inviteMessage.GuildName, (object) str);
    if (Game.Me.SocialState == PlayerSocialState.DoNotDisturb)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      CommunityProtocol.GetProtocol().RequestGuildInviteAccept(inviteCAnonStoreyD7.guildId, inviteCAnonStoreyD7.inviterId, false);
    }
    else
    {
      // ISSUE: reference to a compiler-generated method
      Game.GUIManager.Find<MessageBoxManager>().ShowDialogBox(mainText, "%$bgo.community.guild_invite_messagebox_title%", "%$bgo.common.yes%", "%$bgo.common.no%", new System.Action<MessageBoxActionType>(inviteCAnonStoreyD7.\u003C\u003Em__216), 15U);
    }
  }

  private void OnGuildInviteResult(GuildInviteResultMessage inviteResultMessage)
  {
    string playerName = Game.Players[inviteResultMessage.PlayerId].Name;
    Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox(inviteResultMessage.Result, playerName);
  }

  private void OnGuildMemberUpdate(GuildMemberInfo memberInfo)
  {
    Player player = Game.Players[memberInfo.PlayerId];
    player._SetWing(Game.Me.Guild.ServerID, Game.Me.Guild.Name, memberInfo.PlayerRole);
    player.Level = memberInfo.PlayerLevel;
    player.Name = memberInfo.PlayerName;
    player.Faction = Game.Me.Faction;
    Game.Me.Guild.AddOrUpdatePlayer(player);
  }

  private void OnRemoveGuildMember(GuildMemberRemovedMessage memberRemovedMessage)
  {
    Player player = Game.Players[memberRemovedMessage.PlayerId];
    player._SetWing(0U, (string) null, GuildRole.Recruit);
    Game.Me.Guild._RemovePlayer(player, memberRemovedMessage.Leave);
  }

  private void OnGuildOperationResult(GuildOperationResultMessage operationResultMessage)
  {
    string text = string.Empty;
    switch (operationResultMessage.Result)
    {
      case GuildOperationResult.InvalidPermissions:
        text = BsgoLocalization.Get("%$bgo.community.guild_low_rank_" + (object) operationResultMessage.Operation + "%");
        break;
      case GuildOperationResult.NotInGuild:
        text = BsgoLocalization.Get("%$bgo.community.guild_not_in_guild%");
        break;
      case GuildOperationResult.ErrorLeaderPermissions:
        text = BsgoLocalization.Get("%$bgo.community.guild_error_leader_permissions%");
        break;
    }
    if (!(text != string.Empty))
      return;
    Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox(text);
  }

  private void OnGuildPromotion(GuildMemberPromotionMessage memberPromotionMessage)
  {
    Player player = Game.Players[memberPromotionMessage.PlayerId];
    GuildRole newRole = memberPromotionMessage.NewRole;
    string text = string.Empty;
    if (player.IsMe)
    {
      if (player.GuildRole > newRole)
        text = BsgoLocalization.Get("%$bgo.chat.message_demotion_guild%", (object) Tools.FormatWingRole(newRole));
      else if (player.GuildRole < newRole)
        text = BsgoLocalization.Get("%$bgo.chat.message_promotion_guild%", (object) Tools.FormatWingRole(newRole));
    }
    else if (player.GuildRole > newRole)
      text = BsgoLocalization.Get("%$bgo.chat.message_demotion_guildmate%", (object) player.Name, (object) Tools.FormatWingRole(newRole));
    else if (player.GuildRole < newRole)
      text = BsgoLocalization.Get("%$bgo.chat.message_promotion_guildmate%", (object) player.Name, (object) Tools.FormatWingRole(newRole));
    if (text != string.Empty)
      Game.ChatStorage.SystemSays(text);
    player._SetWing(Game.Me.Guild.ServerID, Game.Me.Guild.Name, newRole);
  }

  private void OnGuildRankNameChanged(GuildRankNameChangedMessage rankNameChangedMessage)
  {
    Game.Me.Guild._UpdateRankName(rankNameChangedMessage.Rank, rankNameChangedMessage.RankName);
  }
}
