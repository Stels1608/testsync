﻿// Decompiled with JetBrains decompiler
// Type: GuiOutpostProgress
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System;
using System.Net;
using UnityEngine;

public class GuiOutpostProgress : QueueHorizontal
{
  private readonly GuiLabel sectorName = new GuiLabel("sectorName");
  private readonly GuiLabel outpostProgress = new GuiLabel("outpostProgress");
  private readonly GuiLabel serverTime = new GuiLabel("serverTime");
  private readonly GuiLabel serverName = new GuiLabel("serverName");
  private readonly GuiLabel fpsLabel = new GuiLabel("fps");
  private readonly GuiLabel pingLabel = new GuiLabel("ping");
  private const float UPDATE_RATE = 1f;
  private const int FRAME_RESET = 4;
  private int frameCount;
  private float fps;
  private float nextUpdate;
  private float time;
  private int updateCount;
  private int pingCount;
  private Ping ping;
  private string ip;

  public static bool ShowFps { get; set; }

  public GuiOutpostProgress()
  {
    this.PositionX = 10f;
    this.PositionY = 10f;
    this.Align = Align.UpLeft;
    this.ContainerAutoSize = true;
    GuiLabel guiLabel = this.outpostProgress;
    Color color1 = Game.Me.Faction != Faction.Colonial ? TargetColorsLegacyBrackets.cylonColor : TargetColorsLegacyBrackets.colonialColor;
    this.outpostProgress.OverColor = color1;
    Color color2 = color1;
    guiLabel.NormalColor = color2;
    this.AddChild((GuiElementBase) this.sectorName);
    this.AddChild((GuiElementBase) this.outpostProgress);
    this.AddChild((GuiElementBase) this.serverName);
    this.AddChild((GuiElementBase) this.serverTime);
    this.AddChild((GuiElementBase) this.pingLabel);
    this.AddChild((GuiElementBase) this.fpsLabel);
    this.fpsLabel.Font = Gui.Options.FontVerdana;
    this.fpsLabel.FontSize = 10;
    this.pingLabel.Font = Gui.Options.FontVerdana;
    this.pingLabel.FontSize = 10;
    this.serverName.Text = this.ServerName(Game.Me.ChatProjectID);
    this.serverName.AllColor = Color.gray;
    this.AddChild((GuiElementBase) new Gui.Timer(1f));
    this.AddChild((GuiElementBase) new Gui.Timer(5f, new AnonymousDelegate(this.Ping)));
    this.nextUpdate = Time.time + 1f;
  }

  public override void PeriodicUpdate()
  {
    this.sectorName.Text = SpaceLevel.GetLevel().Card.GUICard.Name.ToUpper();
    SectorDesc star = Game.Galaxy.GetStar(SpaceLevel.GetLevel().ServerID);
    if (!(bool) StaticCards.GalaxyMap.IsLoaded)
      return;
    if (star != null && star.CanOutpost)
    {
      if (star.IsOutpostBlocked)
        this.outpostProgress.Text = BsgoLocalization.Get("%$bgo.etc.outpost_repairing%", (object) Tools.FormatTime(star.OutpostBlockedEndtime - Time.time));
      else
        this.outpostProgress.Text = BsgoLocalization.Get("%$bgo.etc.outpost_progress%", (object) Mathf.Clamp((float) star.MyFactionOutpostPoints / 10f, 0.0f, 300f).ToString("F1"));
    }
    else
      this.outpostProgress.Text = "%$bgo.etc.outpost_na%";
    this.serverTime.Text = Game.TimeSync.ServerLocalDateTime.ToString();
    if (!GuiOutpostProgress.ShowFps)
      return;
    this.fpsLabel.Text = " FPS: " + (object) (int) this.fps;
    Color color1 = (double) this.fps <= 54.0 ? ((double) this.fps <= 39.0 ? ((double) this.fps <= 19.0 ? Color.red : new Color((float) byte.MaxValue, 165f, 0.0f)) : Color.yellow) : Color.green;
    GuiLabel guiLabel = this.fpsLabel;
    Color color2 = color1;
    this.fpsLabel.OverColor = color2;
    Color color3 = color2;
    guiLabel.NormalColor = color3;
    this.pingLabel.Text = " Ping: " + (this.ping == null || !this.ping.isDone ? "-" : this.ping.time.ToString());
  }

  public override void Draw()
  {
    base.Draw();
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    GuiLabel guiLabel = this.fpsLabel;
    bool showFps = GuiOutpostProgress.ShowFps;
    this.pingLabel.IsRendered = showFps;
    int num = showFps ? 1 : 0;
    guiLabel.IsRendered = num != 0;
    ++this.frameCount;
    this.time += Time.deltaTime;
    if ((double) Time.time <= (double) this.nextUpdate)
      return;
    this.nextUpdate = Time.time + 1f;
    this.fps = (float) this.frameCount / this.time;
    if (++this.updateCount <= 4)
      return;
    this.updateCount = 0;
    this.frameCount = 0;
    this.time = 0.0f;
  }

  private string ServerName(uint projectId)
  {
    string str = string.Empty;
    uint num = projectId;
    switch (num)
    {
      case 547:
        str = "Aerilon";
        break;
      case 548:
        str = "Aquaria";
        break;
      case 553:
        str = "Caprica";
        break;
      case 554:
        str = "Gemenon";
        break;
      default:
        if ((int) num != 474)
        {
          if ((int) num != 475)
          {
            if ((int) num != 561)
            {
              if ((int) num != 562)
              {
                if ((int) num != 756)
                {
                  if ((int) num != 757)
                  {
                    if ((int) num != 860)
                    {
                      if ((int) num != 861)
                      {
                        if ((int) num != 311)
                        {
                          if ((int) num != 455)
                          {
                            if ((int) num != 495)
                            {
                              if ((int) num != 704)
                              {
                                if ((int) num != 819)
                                {
                                  if ((int) num == 980)
                                  {
                                    str = "Troy";
                                    break;
                                  }
                                  break;
                                }
                                str = "Earth";
                                break;
                              }
                              str = "Franciscoton";
                              break;
                            }
                            str = "Scorpia";
                            break;
                          }
                          str = "Tauron";
                          break;
                        }
                        str = "Libran";
                        break;
                      }
                      str = "US";
                      break;
                    }
                    str = "EU";
                    break;
                  }
                  str = "Canceron";
                  break;
                }
                str = "Virgon";
                break;
              }
              str = "Sagittaron";
              break;
            }
            str = "Picon";
            break;
          }
          str = "Hamburgton";
          break;
        }
        str = "Kobol";
        break;
    }
    return str;
  }

  private void Ping()
  {
    if (this.ip == null)
    {
      try
      {
        this.ip = this.ResolveIp(Game.ProtocolManager.Host);
      }
      catch (Exception ex)
      {
        return;
      }
    }
    if (this.ping != null)
    {
      if (this.ping.isDone && this.pingCount % 100 == 0)
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new LogToEventStreamMessage("client.performance.report", new object[4]
        {
          (object) "ping",
          (object) this.ping.time,
          (object) "fps",
          (object) (int) this.fps
        }));
      this.ping.DestroyPing();
    }
    this.ping = new Ping(this.ip);
    ++this.pingCount;
  }

  private string ResolveIp(string host)
  {
    IPHostEntry hostEntry = Dns.GetHostEntry(host);
    if (hostEntry.AddressList.Length > 0)
      return hostEntry.AddressList[0].ToString();
    return host;
  }
}
