﻿// Decompiled with JetBrains decompiler
// Type: AvatarIconCreator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AvatarIconCreator : MonoBehaviour
{
  private static Texture2D sshot = (Texture2D) null;
  private static AvatarDescription avDesc = (AvatarDescription) null;
  private static Vector3 position = new Vector3();
  private static Quaternion rotation = Quaternion.identity;
  private Avatar avatar;
  private RenderTexture targetTex;

  public static Texture2D GenerateIcon(AvatarDescription desc, Vector3 pos, Quaternion rot)
  {
    if (AvatarIconCreator.avDesc == null)
      AvatarIconCreator.avDesc = desc;
    AvatarIconCreator.position = pos;
    AvatarIconCreator.rotation = rot;
    return AvatarIconCreator.sshot;
  }

  private void Start()
  {
    this.targetTex = new RenderTexture(64, 64, 32);
    this.targetTex.isPowerOfTwo = true;
    this.targetTex.depth = 32;
    this.targetTex.useMipMap = false;
    this.gameObject.GetComponent<Camera>().targetTexture = this.targetTex;
    this.gameObject.GetComponent<Camera>().aspect = 1f;
  }

  private void MoveToIconLayer(GameObject o)
  {
    if ((Object) o == (Object) null)
      return;
    o.layer = 19;
    for (int index = 0; index < o.transform.childCount; ++index)
      this.MoveToIconLayer(o.transform.GetChild(index).gameObject);
  }

  private void Update()
  {
    if (AvatarIconCreator.avDesc == null)
      return;
    if (this.avatar == null)
    {
      try
      {
        this.avatar = AvatarSelector.Create((AvatarItems) AvatarIconCreator.avDesc, AvatarIconCreator.position, AvatarIconCreator.rotation);
        AvatarIconCreator.avDesc = (AvatarDescription) null;
        if (this.avatar == null)
          this.avatar = AvatarSelector.Create("human", "male", AvatarIconCreator.position, AvatarIconCreator.rotation);
      }
      catch
      {
        this.avatar = AvatarSelector.Create("human", "male", AvatarIconCreator.position, AvatarIconCreator.rotation);
      }
    }
    if (this.avatar == null)
      return;
    this.avatar.Update();
    if (!this.avatar.IsReady)
      return;
    this.MoveToIconLayer(this.avatar.obj);
  }

  private void OnPostRender()
  {
    if (!((Object) AvatarIconCreator.sshot == (Object) null) || this.avatar == null || (!((Object) this.avatar.obj != (Object) null) || !this.avatar.IsReady))
      return;
    AvatarIconCreator.sshot = new Texture2D(64, 64, TextureFormat.ARGB32, false);
    AvatarIconCreator.sshot.ReadPixels(new Rect(0.0f, 0.0f, 64f, 64f), 0, 0);
    AvatarIconCreator.sshot.Apply();
    this.avatar.Destroy();
  }
}
