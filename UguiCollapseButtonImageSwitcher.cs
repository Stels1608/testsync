﻿// Decompiled with JetBrains decompiler
// Type: UguiCollapseButtonImageSwitcher
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class UguiCollapseButtonImageSwitcher : MonoBehaviour, ICollapseWindowListener
{
  [SerializeField]
  private Image ButtonImage;
  [SerializeField]
  private UnityEngine.Sprite SpriteCollapsed;
  [SerializeField]
  private UnityEngine.Sprite SpriteUncollapsed;

  public void OnCollapseEvent(bool isCollapsed)
  {
    this.ButtonImage.sprite = !isCollapsed ? this.SpriteUncollapsed : this.SpriteCollapsed;
  }
}
