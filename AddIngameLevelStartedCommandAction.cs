﻿// Decompiled with JetBrains decompiler
// Type: AddIngameLevelStartedCommandAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;

public class AddIngameLevelStartedCommandAction : Action<Message>
{
  private IngameLevelStartedCommandsDataProvider commandsDataProvider;

  public override void Execute(IMessage<Message> message)
  {
    IngameLevelStartedCommand levelStartedCommand = (IngameLevelStartedCommand) message.Data;
    if (GameLevel.Instance.IsIngameLevel && (bool) GameLevel.Instance.IsStarted)
    {
      levelStartedCommand.Execute();
    }
    else
    {
      this.commandsDataProvider = (IngameLevelStartedCommandsDataProvider) this.OwnerFacade.FetchDataProvider("GameLevelLoadedActionsDataProvider");
      this.commandsDataProvider.GameLevelLoadedCommands.Add(levelStartedCommand);
    }
  }
}
