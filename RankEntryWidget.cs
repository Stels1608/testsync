﻿// Decompiled with JetBrains decompiler
// Type: RankEntryWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RankEntryWidget : NguiWidget
{
  private static readonly Color highlightColor = new Color(0.1647059f, 0.4941176f, 0.3960784f);
  private static readonly Color darkFontColor = new Color(0.05882353f, 0.05882353f, 0.05882353f);
  private static readonly Color brightFontColor = new Color(0.9372549f, 0.9372549f, 0.9372549f);
  private const string CYLON_ICON = "faction_icon_cylon";
  private const string COLONIAL_ICON = "faction_icon_colonial";
  [SerializeField]
  private UIAtlas atlas;
  [SerializeField]
  private UISprite factionIcon;
  [SerializeField]
  private UILabel rankLabel;
  [SerializeField]
  private UILabel nameLabel;
  [SerializeField]
  private UILabel pointsLabel;
  [SerializeField]
  private UILabel killsLabel;

  public void SetData(RankEntry rankEntry)
  {
    this.rankLabel.text = rankEntry.Rank.ToString("D2");
    this.factionIcon.spriteName = rankEntry.Faction != Faction.Cylon ? "faction_icon_colonial" : "faction_icon_cylon";
    this.nameLabel.text = rankEntry.PlayerName;
    this.pointsLabel.text = rankEntry.Points.ToString("D");
    this.killsLabel.text = rankEntry.Kills.ToString("D") + " / " + rankEntry.Deaths.ToString("D");
  }

  public void Colorize(bool localPlayer)
  {
    foreach (UISprite componentsInChild in this.gameObject.GetComponentsInChildren<UISprite>(true))
    {
      if (!((Object) componentsInChild == (Object) this.factionIcon))
      {
        componentsInChild.spriteName = !localPlayer ? "table_cell_bg" : "ContainerBackground";
        componentsInChild.color = !localPlayer ? Color.white : RankEntryWidget.highlightColor;
      }
    }
    this.rankLabel.color = !localPlayer ? RankEntryWidget.highlightColor : RankEntryWidget.darkFontColor;
    this.nameLabel.color = !localPlayer ? RankEntryWidget.brightFontColor : RankEntryWidget.darkFontColor;
    this.pointsLabel.color = !localPlayer ? RankEntryWidget.highlightColor : RankEntryWidget.darkFontColor;
    this.killsLabel.color = !localPlayer ? RankEntryWidget.highlightColor : RankEntryWidget.darkFontColor;
  }
}
