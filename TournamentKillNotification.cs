﻿// Decompiled with JetBrains decompiler
// Type: TournamentKillNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class TournamentKillNotification : OnScreenNotification
{
  private const string TXT_KILL_SHOT = "%$bgo.tournament.personal.kill_shot%";
  private const string TXT_KILLING_SPREE = "%$bgo.tournament.personal.killing_spree%";
  private const string TXT_ACE_KILL = "%$bgo.tournament.personal.ace_kill%";
  private const string TXT_NEMESIS_KILL = "%$bgo.tournament.personal.nemesis_kill%";
  private const string TXT_TOP_GUN_KILL = "%$bgo.tournament.personal.top_gun_kill%";
  private readonly string message;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.TournamentKill;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Positive;
    }
  }

  public override string TextMessage
  {
    get
    {
      return this.message;
    }
  }

  public override bool Show
  {
    get
    {
      return true;
    }
  }

  public TournamentKillNotification(PersonalTournamentMessage messageType, uint points)
  {
    string key = (string) null;
    switch (messageType)
    {
      case PersonalTournamentMessage.Kill:
        key = "%$bgo.tournament.personal.kill_shot%";
        break;
      case PersonalTournamentMessage.KillingSpree:
        key = "%$bgo.tournament.personal.killing_spree%";
        break;
      case PersonalTournamentMessage.AceKill:
        key = "%$bgo.tournament.personal.ace_kill%";
        break;
      case PersonalTournamentMessage.NemesisKill:
        key = "%$bgo.tournament.personal.nemesis_kill%";
        break;
      case PersonalTournamentMessage.TopGunKill:
        key = "%$bgo.tournament.personal.top_gun_kill%";
        break;
    }
    this.message = BsgoLocalization.Get(key, (object) points);
  }
}
