﻿// Decompiled with JetBrains decompiler
// Type: JumpMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;

public class JumpMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "JumpMediator";
  private JumpQueueWindow view;

  public JumpMediator()
    : base("JumpMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.CreateLevelUi);
    this.AddMessageInterest(Message.FtlJumpQueued);
    this.AddMessageInterest(Message.FtlJumpCanceled);
    this.AddMessageInterest(Message.FtlJumpStarted);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.FtlJumpStarted:
      case Message.FtlJumpCanceled:
        if (this.view == null)
          break;
        JumpQueueWindow jumpQueueWindow1 = this.view;
        bool flag1 = false;
        this.view.Queued = flag1;
        int num1 = flag1 ? 1 : 0;
        jumpQueueWindow1.IsRendered = num1 != 0;
        break;
      case Message.FtlJumpQueued:
        JumpQueueWindow jumpQueueWindow2 = this.view;
        bool flag2 = true;
        this.view.Queued = flag2;
        int num2 = flag2 ? 1 : 0;
        jumpQueueWindow2.IsRendered = num2 != 0;
        break;
      case Message.CreateLevelUi:
        this.view = new JumpQueueWindow();
        Game.GUIManager.AddPanel((IGUIPanel) this.view);
        this.OwnerFacade.SendMessage(Message.AddToCombatGui, (object) this.view);
        break;
    }
  }
}
