﻿// Decompiled with JetBrains decompiler
// Type: ChatDynamicRoomCreated
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

internal class ChatDynamicRoomCreated : ChatProtocolParser
{
  public int roomID;
  public string roomName;

  public ChatDynamicRoomCreated()
  {
    this.type = ChatProtocolParserType.DynamicRoomCreateOk;
  }

  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split(new char[3]{ '%', '@', '#' });
    if (strArray.Length != 5 || strArray[0] != "fq" || !int.TryParse(strArray[1], out this.roomID))
      return false;
    this.roomName = strArray[2];
    return true;
  }
}
