﻿// Decompiled with JetBrains decompiler
// Type: NotificationManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;

public class NotificationManager : GuiPanel
{
  public NotificationManager()
  {
    this.MouseTransparent = true;
    this.Align = Align.MiddleCenter;
  }

  public static void Show(GuiElementBase dialog)
  {
    NotificationManager notificationManager = Game.GUIManager.Find<NotificationManager>();
    if (notificationManager == null)
    {
      notificationManager = new NotificationManager();
      Game.RegisterDialog((IGUIPanel) notificationManager, true);
    }
    for (int index = 0; index < notificationManager.Children.Count - 2; ++index)
      notificationManager.Children[index].RemoveThisFromParent();
    notificationManager.AddChild(dialog, Align.MiddleCenter);
    for (int index = 0; index < notificationManager.Children.Count; ++index)
      notificationManager.Children[index].PositionY = (float) ((double) index * (10.0 + (double) dialog.SizeY) - 200.0);
  }

  public static void ShowDailyLoginBonus(uint bonusLevel, List<uint> guids)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    Game.DelayedActions.Add(new DelayedActions.Predicate(new NotificationManager.\u003CShowDailyLoginBonus\u003Ec__AnonStoreyA8()
    {
      bonusLevel = bonusLevel,
      rewards = guids.ConvertAll<RewardCard>((Converter<uint, RewardCard>) (guid => (RewardCard) Game.Catalogue.FetchCard(guid, CardView.Reward)))
    }.\u003C\u003Em__16A));
  }

  public static void ShowDeathPaymentBonus(GUICard guiCard, RewardCard rewardCard, uint discountAmount)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    Game.DelayedActions.Add(new DelayedActions.Predicate(new NotificationManager.\u003CShowDeathPaymentBonus\u003Ec__AnonStoreyA9()
    {
      guiCard = guiCard,
      rewardCard = rewardCard,
      discountAmount = discountAmount
    }.\u003C\u003Em__16B));
  }

  public static void ShowSystemUpgradeResult(bool successful)
  {
    GuiUpgrading guiUpgrading = Game.GUIManager.Find<GuiUpgrading>();
    if (guiUpgrading == null)
      return;
    guiUpgrading.success = successful;
  }
}
