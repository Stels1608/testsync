﻿// Decompiled with JetBrains decompiler
// Type: IHudIndicatorColorScheme
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public interface IHudIndicatorColorScheme
{
  Color BracketColor(ISpaceEntity entity);

  Color TextColor(ISpaceEntity entity);

  Color HealthBarColor(ISpaceEntity entity);

  Color HealthBarOutlineColor(ISpaceEntity entity);

  Color SelectionColor(ISpaceEntity entity);

  Color IconColor(ISpaceEntity entity);
}
