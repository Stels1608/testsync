﻿// Decompiled with JetBrains decompiler
// Type: ACMObject
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

internal class ACMObject
{
  private static readonly List<string> availableVersions = new List<string>();
  private readonly Dictionary<string, ACMVersion> versions = new Dictionary<string, ACMVersion>();

  public static List<string> GetVersions()
  {
    List<string> stringList = new List<string>();
    using (List<string>.Enumerator enumerator = ACMObject.availableVersions.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        stringList.Add(current);
      }
    }
    return stringList;
  }

  public void Parse(List<string> materials)
  {
    ACMObject.availableVersions.Add("v1");
    ACMObject.availableVersions.Add("v2");
    using (List<string>.Enumerator enumerator = ACMObject.availableVersions.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        this.versions[current] = new ACMVersion();
        this.versions[current].Parse(this.FindMaterialsFromVersion(current, materials));
      }
    }
  }

  private List<string> FindMaterialsFromVersion(string ver, List<string> materials)
  {
    List<string> stringList = new List<string>();
    using (List<string>.Enumerator enumerator = materials.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        if (current.Contains(ver))
          stringList.Add(current);
      }
    }
    return stringList;
  }

  public string GetMaterial(string version, string color, int number)
  {
    if (!this.versions.ContainsKey(version))
      return string.Empty;
    return this.versions[version].GetMaterial(color, number);
  }

  public int GetMaterialNumber(string material)
  {
    using (Dictionary<string, ACMVersion>.Enumerator enumerator = this.versions.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int materialNumber = enumerator.Current.Value.GetMaterialNumber(material);
        if (materialNumber != -1)
          return materialNumber;
      }
    }
    return -1;
  }

  public int GetMaterialsCount(string version, string color)
  {
    ACMVersion acmVersion;
    if (this.versions.TryGetValue(version, out acmVersion))
      return acmVersion.GetMaterialsCount(color);
    return 0;
  }
}
