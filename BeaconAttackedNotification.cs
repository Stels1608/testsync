﻿// Decompiled with JetBrains decompiler
// Type: BeaconAttackedNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class BeaconAttackedNotification : OnScreenNotification
{
  private readonly string text;
  private readonly NotificationCategory category;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.BeaconAttacked;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return this.category;
    }
  }

  public override string TextMessage
  {
    get
    {
      return this.text;
    }
  }

  public override bool Show
  {
    get
    {
      if (OnScreenNotification.ShowOutpostMessages)
        return !string.IsNullOrEmpty(this.text);
      return false;
    }
  }

  public BeaconAttackedNotification(Faction faction, GUICard sectorCard, byte damageType)
  {
    this.category = faction == Game.Me.Faction ? NotificationCategory.Negative : NotificationCategory.Positive;
    switch (damageType)
    {
      case 2:
        this.text = BsgoLocalization.Get(faction != Faction.Cylon ? "%$bgo.notif.beacon_attacked_colonial%" : "%$bgo.notif.beacon_attacked_cylon%", (object) sectorCard.Name);
        break;
      case 3:
        this.text = BsgoLocalization.Get(faction != Faction.Cylon ? "%$bgo.notif.beacon_attacked_1_colonial%" : "%$bgo.notif.beacon_attacked_1_cylon%", (object) sectorCard.Name);
        break;
      case 4:
        this.text = BsgoLocalization.Get(faction != Faction.Cylon ? "%$bgo.notif.beacon_attacked_2_colonial%" : "%$bgo.notif.beacon_attacked_2_cylon%", (object) sectorCard.Name);
        break;
    }
  }
}
