﻿// Decompiled with JetBrains decompiler
// Type: ScannerScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ScannerScript : MonoBehaviour
{
  public Transform ScannedObject;
  public SpaceObject ScanningObject;
  public float Distance;
  private bool doneOnce;

  private void Update()
  {
    if ((Object) null != (Object) this.ScannedObject && this.ScanningObject != null)
    {
      this.transform.position = this.ScannedObject.position + (this.ScanningObject.Position - this.ScannedObject.position).normalized * this.Distance;
    }
    else
    {
      if (this.doneOnce)
        return;
      Debug.Log((object) ("ScannedObject = null : " + ((Object) null == (Object) this.ScannedObject).ToString() + " ScanningObject = null : " + ((Object) null == (Object) this.ScannedObject).ToString() + " this is a problem in ScanningScript:Update()"));
      this.doneOnce = true;
    }
  }
}
