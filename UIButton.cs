﻿// Decompiled with JetBrains decompiler
// Type: UIButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Button")]
public class UIButton : UIButtonColor
{
  public List<EventDelegate> onClick = new List<EventDelegate>();
  public static UIButton current;
  public bool dragHighlight;
  public string hoverSprite;
  public string pressedSprite;
  public string disabledSprite;
  public UnityEngine.Sprite hoverSprite2D;
  public UnityEngine.Sprite pressedSprite2D;
  public UnityEngine.Sprite disabledSprite2D;
  public bool pixelSnap;
  [NonSerialized]
  private UISprite mSprite;
  [NonSerialized]
  private UI2DSprite mSprite2D;
  [NonSerialized]
  private string mNormalSprite;
  [NonSerialized]
  private UnityEngine.Sprite mNormalSprite2D;

  public override bool isEnabled
  {
    get
    {
      if (!this.enabled)
        return false;
      Collider component1 = this.GetComponent<Collider>();
      if ((bool) ((UnityEngine.Object) component1) && component1.enabled)
        return true;
      Collider2D component2 = this.GetComponent<Collider2D>();
      if ((bool) ((UnityEngine.Object) component2))
        return component2.enabled;
      return false;
    }
    set
    {
      if (this.isEnabled == value)
        return;
      Collider component1 = this.GetComponent<Collider>();
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
      {
        component1.enabled = value;
        this.SetState(!value ? UIButtonColor.State.Disabled : UIButtonColor.State.Normal, false);
      }
      else
      {
        Collider2D component2 = this.GetComponent<Collider2D>();
        if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
        {
          component2.enabled = value;
          this.SetState(!value ? UIButtonColor.State.Disabled : UIButtonColor.State.Normal, false);
        }
        else
          this.enabled = value;
      }
    }
  }

  public string normalSprite
  {
    get
    {
      if (!this.mInitDone)
        this.OnInit();
      return this.mNormalSprite;
    }
    set
    {
      if ((UnityEngine.Object) this.mSprite != (UnityEngine.Object) null && !string.IsNullOrEmpty(this.mNormalSprite) && this.mNormalSprite == this.mSprite.spriteName)
      {
        this.mNormalSprite = value;
        this.SetSprite(value);
        NGUITools.SetDirty((UnityEngine.Object) this.mSprite);
      }
      else
      {
        this.mNormalSprite = value;
        if (this.mState != UIButtonColor.State.Normal)
          return;
        this.SetSprite(value);
      }
    }
  }

  public UnityEngine.Sprite normalSprite2D
  {
    get
    {
      if (!this.mInitDone)
        this.OnInit();
      return this.mNormalSprite2D;
    }
    set
    {
      if ((UnityEngine.Object) this.mSprite2D != (UnityEngine.Object) null && (UnityEngine.Object) this.mNormalSprite2D == (UnityEngine.Object) this.mSprite2D.sprite2D)
      {
        this.mNormalSprite2D = value;
        this.SetSprite(value);
        NGUITools.SetDirty((UnityEngine.Object) this.mSprite);
      }
      else
      {
        this.mNormalSprite2D = value;
        if (this.mState != UIButtonColor.State.Normal)
          return;
        this.SetSprite(value);
      }
    }
  }

  protected override void OnInit()
  {
    base.OnInit();
    this.mSprite = this.mWidget as UISprite;
    this.mSprite2D = this.mWidget as UI2DSprite;
    if ((UnityEngine.Object) this.mSprite != (UnityEngine.Object) null)
      this.mNormalSprite = this.mSprite.spriteName;
    if (!((UnityEngine.Object) this.mSprite2D != (UnityEngine.Object) null))
      return;
    this.mNormalSprite2D = this.mSprite2D.sprite2D;
  }

  protected override void OnEnable()
  {
    if (this.isEnabled)
    {
      if (!this.mInitDone)
        return;
      if (UICamera.currentScheme == UICamera.ControlScheme.Controller)
        this.OnHover((UnityEngine.Object) UICamera.selectedObject == (UnityEngine.Object) this.gameObject);
      else if (UICamera.currentScheme == UICamera.ControlScheme.Mouse)
        this.OnHover((UnityEngine.Object) UICamera.hoveredObject == (UnityEngine.Object) this.gameObject);
      else
        this.SetState(UIButtonColor.State.Normal, false);
    }
    else
      this.SetState(UIButtonColor.State.Disabled, true);
  }

  protected override void OnDragOver()
  {
    if (!this.isEnabled || !this.dragHighlight && !((UnityEngine.Object) UICamera.currentTouch.pressed == (UnityEngine.Object) this.gameObject))
      return;
    base.OnDragOver();
  }

  protected override void OnDragOut()
  {
    if (!this.isEnabled || !this.dragHighlight && !((UnityEngine.Object) UICamera.currentTouch.pressed == (UnityEngine.Object) this.gameObject))
      return;
    base.OnDragOut();
  }

  protected virtual void OnClick()
  {
    if (!((UnityEngine.Object) UIButton.current == (UnityEngine.Object) null) || !this.isEnabled)
      return;
    UIButton.current = this;
    EventDelegate.Execute(this.onClick);
    UIButton.current = (UIButton) null;
  }

  public override void SetState(UIButtonColor.State state, bool immediate)
  {
    base.SetState(state, immediate);
    if ((UnityEngine.Object) this.mSprite != (UnityEngine.Object) null)
    {
      switch (state)
      {
        case UIButtonColor.State.Normal:
          this.SetSprite(this.mNormalSprite);
          break;
        case UIButtonColor.State.Hover:
          this.SetSprite(this.hoverSprite);
          break;
        case UIButtonColor.State.Pressed:
          this.SetSprite(this.pressedSprite);
          break;
        case UIButtonColor.State.Disabled:
          this.SetSprite(this.disabledSprite);
          break;
      }
    }
    else
    {
      if (!((UnityEngine.Object) this.mSprite2D != (UnityEngine.Object) null))
        return;
      switch (state)
      {
        case UIButtonColor.State.Normal:
          this.SetSprite(this.mNormalSprite2D);
          break;
        case UIButtonColor.State.Hover:
          this.SetSprite(this.hoverSprite2D);
          break;
        case UIButtonColor.State.Pressed:
          this.SetSprite(this.pressedSprite2D);
          break;
        case UIButtonColor.State.Disabled:
          this.SetSprite(this.disabledSprite2D);
          break;
      }
    }
  }

  protected void SetSprite(string sp)
  {
    if (!((UnityEngine.Object) this.mSprite != (UnityEngine.Object) null) || string.IsNullOrEmpty(sp) || !(this.mSprite.spriteName != sp))
      return;
    this.mSprite.spriteName = sp;
    if (!this.pixelSnap)
      return;
    this.mSprite.MakePixelPerfect();
  }

  protected void SetSprite(UnityEngine.Sprite sp)
  {
    if (!((UnityEngine.Object) sp != (UnityEngine.Object) null) || !((UnityEngine.Object) this.mSprite2D != (UnityEngine.Object) null) || !((UnityEngine.Object) this.mSprite2D.sprite2D != (UnityEngine.Object) sp))
      return;
    this.mSprite2D.sprite2D = sp;
    if (!this.pixelSnap)
      return;
    this.mSprite2D.MakePixelPerfect();
  }
}
