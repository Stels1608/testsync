﻿// Decompiled with JetBrains decompiler
// Type: SceneProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;

public class SceneProtocol : BgoProtocol
{
  public SceneProtocol()
    : base(BgoProtocol.ProtocolID.Scene)
  {
  }

  public void NotifySceneLoaded()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    this.SendMessage(bw);
  }

  public static SceneProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Scene) as SceneProtocol;
  }

  public void RequestQuitLogin()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 4);
    this.SendMessage(bw);
  }

  public void RequestDisconnect()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 2);
    this.SendMessage(bw);
  }

  public void RequestStopDisconnect()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 3);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    SceneProtocol.Reply reply = (SceneProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case SceneProtocol.Reply.LoadNextScene:
        TransSceneType transType = (TransSceneType) br.ReadByte();
        GameLocation location = (GameLocation) br.ReadByte();
        Game.Me.FactionGroup = FactionGroup.Group0;
        LevelProfile nextLevelProfile;
        switch (location)
        {
          case GameLocation.Space:
          case GameLocation.Story:
          case GameLocation.BattleSpace:
          case GameLocation.Tournament:
          case GameLocation.Tutorial:
          case GameLocation.Teaser:
            nextLevelProfile = (LevelProfile) new SpaceLevelProfile(br.ReadUInt32(), br.ReadGUID(), location);
            break;
          case GameLocation.Room:
            uint cardGuid1 = br.ReadGUID();
            nextLevelProfile = (LevelProfile) new RoomLevelProfile(br.ReadUInt32(), cardGuid1);
            break;
          case GameLocation.Arena:
            uint serverId = br.ReadUInt32();
            uint cardGuid2 = br.ReadGUID();
            byte num1 = br.ReadByte();
            nextLevelProfile = (LevelProfile) new SpaceLevelProfile(serverId, cardGuid2, location);
            Game.Me.FactionGroup = (int) num1 != 0 ? FactionGroup.Group1 : FactionGroup.Group0;
            break;
          case GameLocation.Avatar:
            uint num2 = (uint) br.ReadUInt16();
            for (int index = 0; (long) index < (long) num2; ++index)
            {
              int num3 = (int) br.ReadGUID();
            }
            br.ReadBoolean();
            nextLevelProfile = (LevelProfile) new AvatarLevelProfile();
            break;
          case GameLocation.Starter:
            nextLevelProfile = (LevelProfile) new StarterLevelProfile(br.ReadUInt32(), br.ReadUInt32());
            Game.Funnel.ReportActivity(Activity.StartScene);
            FacadeFactory.GetInstance().SendMessage(Message.FirstLoginEv0r);
            break;
          default:
            throw new Exception("Unknown next scene index: " + (object) location);
        }
        if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null && SpaceLevel.GetLevel().Docking)
        {
          FacadeFactory.GetInstance().SendMessage(Message.DockAtNpcShipRequest, (object) SpaceLevel.GetLevel().DockTarget);
          SpaceLevel.GetLevel().Docking = false;
        }
        Log.Info((object) ("Next scene " + (object) nextLevelProfile.GetType()));
        Game.LoadNextScene(nextLevelProfile, transType);
        break;
      case SceneProtocol.Reply.DisconnectTimer:
        Game.GUIManager.SystemButtons.ShowLogoutTimer(br.ReadSingle());
        break;
      case SceneProtocol.Reply.Disconnect:
        Game.GetProtocolManager().Disconnect(BsgoLocalization.Get("%$bgo.etc.disconnect_successful%"));
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Universe protocol: " + (object) reply);
        break;
    }
  }

  public enum Request : ushort
  {
    SceneLoaded = 1,
    Disconnect = 2,
    StopDisconnect = 3,
    QuitLogin = 4,
  }

  public enum Reply : ushort
  {
    LoadNextScene = 1,
    DisconnectTimer = 2,
    Disconnect = 100,
  }
}
