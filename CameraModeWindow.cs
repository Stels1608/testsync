﻿// Decompiled with JetBrains decompiler
// Type: CameraModeWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraModeWindow : MonoBehaviour, IGUIRenderable, InputListener
{
  private readonly Dictionary<SpaceCameraBase.CameraMode, SystemButton> modeToButtonMap = new Dictionary<SpaceCameraBase.CameraMode, SystemButton>();
  public SystemButton chaseCamButton;
  public SystemButton noseCamButton;
  public SystemButton freeCamButton;
  public SystemButton targetCamButton;
  private RectTransform rectTransform;
  private RectTransform anchor;
  private List<SpaceCameraBase.CameraMode> supportedCameraModes;
  private int mode;

  public bool IsBlinking { get; set; }

  public bool HandleMouseInput
  {
    get
    {
      return false;
    }
  }

  public bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public bool HandleJoystickAxesInput
  {
    get
    {
      return false;
    }
  }

  public bool IsRendered
  {
    get
    {
      return this.gameObject.activeSelf;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  public bool IsBigWindow
  {
    get
    {
      return false;
    }
  }

  private void Awake()
  {
    this.InitButtonHandlers();
    this.modeToButtonMap.Add(SpaceCameraBase.CameraMode.Chase, this.chaseCamButton);
    this.modeToButtonMap.Add(SpaceCameraBase.CameraMode.Free, this.freeCamButton);
    this.modeToButtonMap.Add(SpaceCameraBase.CameraMode.Nose, this.noseCamButton);
    this.modeToButtonMap.Add(SpaceCameraBase.CameraMode.Target, this.targetCamButton);
    this.OnCameraModeChanged(SpaceCameraBase.CameraMode.Chase);
  }

  private void InitButtonHandlers()
  {
    this.noseCamButton.OnClicked = (UnityAction) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Nose, true)));
    this.chaseCamButton.OnClicked = (UnityAction) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Chase, true)));
    this.freeCamButton.OnClicked = (UnityAction) (() =>
    {
      FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Free, true));
      if (!this.IsBlinking)
        return;
      StoryProtocol.GetProtocol().TriggerControl(StoryProtocol.ControlType.Camera);
    });
    this.targetCamButton.OnClicked = (UnityAction) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Target, true)));
  }

  public void SetSupportedCameraModes(List<SpaceCameraBase.CameraMode> supportedCameraModes)
  {
    this.supportedCameraModes = supportedCameraModes;
    this.ToggleSupportedCameraModes();
    this.ReloadLanguageData();
  }

  private void ToggleSupportedCameraModes()
  {
    using (Dictionary<SpaceCameraBase.CameraMode, SystemButton>.ValueCollection.Enumerator enumerator = this.modeToButtonMap.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.SetVisible(false);
    }
    using (List<SpaceCameraBase.CameraMode>.Enumerator enumerator = this.supportedCameraModes.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.modeToButtonMap[enumerator.Current].SetVisible(true);
    }
  }

  public void OnCameraModeChanged(SpaceCameraBase.CameraMode cameraMode)
  {
    if (this.supportedCameraModes == null)
      return;
    for (int index = 0; index < this.supportedCameraModes.Count; ++index)
    {
      if (this.supportedCameraModes[index] == cameraMode)
      {
        this.mode = index;
        break;
      }
    }
    if (!this.supportedCameraModes.Contains(cameraMode))
      return;
    using (Dictionary<SpaceCameraBase.CameraMode, SystemButton>.Enumerator enumerator = this.modeToButtonMap.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<SpaceCameraBase.CameraMode, SystemButton> current = enumerator.Current;
        current.Value.Selected = cameraMode == current.Key;
      }
    }
  }

  private void Update()
  {
    if ((Object) this.rectTransform == (Object) null)
      this.rectTransform = this.gameObject.GetComponent<RectTransform>();
    if ((Object) this.anchor == (Object) null)
      this.anchor = Game.GUIManager.SystemButtons.GetComponent<RectTransform>();
    this.rectTransform.anchoredPosition = new Vector2((float) ((double) this.anchor.anchoredPosition.x - (double) this.anchor.sizeDelta.x - (double) this.rectTransform.sizeDelta.x - 60.0), this.rectTransform.anchoredPosition.y);
  }

  public void ReloadLanguageData()
  {
    this.noseCamButton.SetTooltip("%$bgo.option_buttons.nose_camera_mode% ", Action.NoseCamera);
    this.freeCamButton.SetTooltip("%$bgo.option_buttons.free_camera_mode% ", Action.FreeCamera);
    this.chaseCamButton.SetTooltip("%$bgo.option_buttons.chase_camera_mode% ", Action.ChaseCamera);
    this.targetCamButton.SetTooltip("%$bgo.option_buttons.look_at_target_camera_mode% ", Action.TargetCamera);
  }

  public bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public void OnMouseMove(float2 mousePosition)
  {
  }

  public bool OnMouseScrollDown()
  {
    return false;
  }

  public bool OnMouseScrollUp()
  {
    return false;
  }

  public bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (action != Action.ToggleCamera && action != Action.TargetCamera && (action != Action.NoseCamera && action != Action.FreeCamera))
      return action == Action.ChaseCamera;
    return true;
  }

  public bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    switch (action)
    {
      case Action.TargetCamera:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Target, true));
        break;
      case Action.ChaseCamera:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Chase, true));
        break;
      case Action.FreeCamera:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Free, true));
        break;
      case Action.NoseCamera:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Nose, true));
        break;
      case Action.ToggleCamera:
        if (++this.mode > this.supportedCameraModes.Count - 1)
          this.mode = 0;
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraMode, (object) this.supportedCameraModes[this.mode], true));
        break;
      default:
        return false;
    }
    return true;
  }

  public bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    return false;
  }
}
