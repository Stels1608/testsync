﻿// Decompiled with JetBrains decompiler
// Type: IMovementController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public interface IMovementController
{
  float CurrentSpeed { get; }

  Vector3 CurrentStrafingSpeed { get; }

  float MarchSpeed { get; }

  Gear Gear { get; }

  MovementFrame GetTickFrame(Tick tick);

  bool Move(double time);

  void Advance(Tick tick);

  void PostAdvance();

  void AddManeuver(Maneuver newManeuver);

  void AddSyncFrame(Tick tick, MovementFrame frame);
}
