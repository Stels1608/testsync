﻿// Decompiled with JetBrains decompiler
// Type: SystemButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Tooltips;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SystemButton : MonoBehaviour
{
  public UnityEngine.Sprite normal;
  public UnityEngine.Sprite mouseOver;
  public Image background;
  public Image icon;
  public Button button;
  private bool disabled;
  private bool selected;
  private GuiAdvancedTooltipBase tooltip;

  public bool Disabled
  {
    get
    {
      return this.disabled;
    }
    set
    {
      if (value)
        this.OnHover(false);
      this.disabled = value;
      this.button.interactable = !value;
      if (value)
      {
        Image image = this.background;
        Color grey = Color.grey;
        this.icon.color = grey;
        Color color = grey;
        image.color = color;
      }
      else
        this.OnHover(false);
    }
  }

  public bool Selected
  {
    get
    {
      return this.selected;
    }
    set
    {
      this.selected = value;
      if (value)
      {
        if (!((Object) this.background != (Object) null))
          return;
        this.background.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_CLICK_COLOR);
        this.icon.color = Gui.Options.MouseOverInvertColor;
      }
      else
        this.OnHover(false);
    }
  }

  public UnityAction OnClicked
  {
    set
    {
      this.button.onClick.RemoveAllListeners();
      this.button.onClick.AddListener(value);
    }
  }

  protected virtual void Start()
  {
    if ((Object) this.button == (Object) null)
    {
      Debug.LogError((object) ("Button is NULL: " + this.gameObject.name));
    }
    else
    {
      EventTrigger eventTrigger = this.button.gameObject.AddComponent<EventTrigger>();
      UguiTools.AddEventTrigger(eventTrigger, new UnityAction<BaseEventData>(this.OnPointerEnter), EventTriggerType.PointerEnter);
      UguiTools.AddEventTrigger(eventTrigger, new UnityAction<BaseEventData>(this.OnPointerExit), EventTriggerType.PointerExit);
      this.OnHover(false);
    }
  }

  private void OnPointerEnter(BaseEventData eventData)
  {
    this.OnHover(true);
  }

  private void OnPointerExit(BaseEventData eventData)
  {
    this.OnHover(false);
  }

  private void OnHover(bool isOver)
  {
    this.HandleTooltip(isOver);
    if (this.Disabled || this.Selected)
      return;
    if ((Object) this.icon == (Object) null)
      Debug.LogError((object) ("Icon is NULL for " + this.gameObject.name));
    if ((Object) this.background != (Object) null)
      this.icon.color = !isOver ? Color.white : Gui.Options.MouseOverInvertColor;
    else
      this.icon.color = !isOver ? Color.white : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_CLICK_COLOR);
    if (!((Object) this.background != (Object) null))
      return;
    this.background.sprite = !isOver ? this.normal : this.mouseOver;
    this.background.color = !isOver ? ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR) : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_CLICK_COLOR);
  }

  private void HandleTooltip(bool isOver)
  {
    if (this.tooltip == null)
      return;
    if (isOver)
    {
      this.tooltip.Position = new Vector2(MouseSetup.MousePositionGui.x, MouseSetup.MousePositionGui.y);
      Game.TooltipManager.ShowTooltip(this.tooltip);
    }
    else
      Game.TooltipManager.HideTooltip(this.tooltip);
  }

  public void SetTooltip(string text, Action action)
  {
    this.tooltip = (GuiAdvancedTooltipBase) new GuiAdvancedHotkeyTooltip(text, action);
    this.tooltip.HideManually = true;
  }

  public void SetTooltip(string text)
  {
    this.tooltip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip(text);
    this.tooltip.HideManually = true;
  }

  public void SetVisible(bool visible)
  {
    this.gameObject.SetActive(visible);
  }
}
