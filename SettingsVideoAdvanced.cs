﻿// Decompiled with JetBrains decompiler
// Type: SettingsVideoAdvanced
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class SettingsVideoAdvanced : SettingsVideoGroup
{
  [SerializeField]
  private UILabel optionsHeadlineLabel;
  [SerializeField]
  private GameObject content;
  [SerializeField]
  private GameObject content2;

  private void Awake()
  {
    this.CoveredSettings.Add(UserSetting.ShowStarDust);
    this.CoveredSettings.Add(UserSetting.ShowStarFog);
    this.CoveredSettings.Add(UserSetting.ShowGlowEffect);
    this.CoveredSettings.Add(UserSetting.HighResModels);
    this.CoveredSettings.Add(UserSetting.HighResTextures);
    this.CoveredSettings.Add(UserSetting.UseProceduralTextures);
    this.CoveredSettings.Add(UserSetting.ShowBulletImpactFx);
    this.CoveredSettings.Add(UserSetting.HighQualityParticles);
    this.CoveredSettings.Add(UserSetting.AnisotropicFiltering);
    this.CoveredSettings.Add(UserSetting.ShowShipSkins);
    this.CoveredSettings.Add(UserSetting.ShowWeaponModules);
    int num1 = 0;
    using (List<UserSetting>.Enumerator enumerator = this.CoveredSettings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        OptionButtonElement buttonElement = NguiWidgetFactory.Options.CreateButtonElement(this.content);
        buttonElement.gameObject.name = num1.ToString() + "_Button";
        ++num1;
        this.controls.Add(current, (OptionsElement) buttonElement);
      }
    }
    this.CoveredSettings.Add(UserSetting.Fullscreen);
    this.CoveredSettings.Add(UserSetting.FramerateCapping);
    this.CoveredSettings.Add(UserSetting.AntiAliasing);
    this.CoveredSettings.Add(UserSetting.ViewDistance);
    this.CoveredSettings.Add(UserSetting.FlakFieldDensity);
    OptionButtonElement buttonElement1 = NguiWidgetFactory.Options.CreateButtonElement(this.content2);
    GameObject gameObject1 = buttonElement1.gameObject;
    int num2 = num1;
    int num3 = 1;
    int num4 = num2 + num3;
    string str1 = num2.ToString() + "_Button";
    gameObject1.name = str1;
    this.controls.Add(UserSetting.Fullscreen, (OptionsElement) buttonElement1);
    OptionButtonElement buttonElement2 = NguiWidgetFactory.Options.CreateButtonElement(this.content2);
    GameObject gameObject2 = buttonElement2.gameObject;
    int num5 = num4;
    int num6 = 1;
    int num7 = num5 + num6;
    string str2 = num5.ToString() + "_Button";
    gameObject2.name = str2;
    this.controls.Add(UserSetting.FramerateCapping, (OptionsElement) buttonElement2);
    GameObject gameObject3 = NguiWidgetFactory.Options.CreateSliderElement(this.content2).gameObject;
    gameObject3.name = "0_slider";
    this.controls.Add(UserSetting.ViewDistance, (OptionsElement) gameObject3.GetComponent<OptionsSliderElement>());
    OptionsSliderElement sliderElement = NguiWidgetFactory.Options.CreateSliderElement(this.content2);
    sliderElement.ShowPercentage = true;
    sliderElement.SetLimits01(0.1f, 1f);
    sliderElement.name = "1_slider";
    this.controls.Add(UserSetting.FlakFieldDensity, (OptionsElement) sliderElement);
    GameObject gameObject4 = NGUITools.AddChild(this.content2, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/OptionsRadioBtnGroup"));
    gameObject4.name = "2_radio";
    this.controls.Add(UserSetting.AntiAliasing, (OptionsElement) gameObject4.GetComponent<OptionsRadioButtonGroup>());
    this.content.GetComponent<UITable>().Reposition();
    this.content2.GetComponent<UITable>().Reposition();
  }

  private void Start()
  {
    this.ReloadLanguageData();
  }

  public void ReloadLanguageData()
  {
    this.optionsHeadlineLabel.text = Tools.ParseMessage("%$bgo.ui.options.video.advanced%");
  }
}
