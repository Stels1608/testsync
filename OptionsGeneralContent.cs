﻿// Decompiled with JetBrains decompiler
// Type: OptionsGeneralContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class OptionsGeneralContent : OptionsContent
{
  private SettingsGeneralMessages generalMessages;
  private SettingsGeneralMisc generalMisc;
  private SettingsGeneralSound generalSound;
  private OneTwoOptionsLayout layout;
  private InputBindingFooterButtonBar footerButtonBar;

  public void Awake()
  {
    this.layout = new GameObject("layout")
    {
      transform = {
        parent = this.transform,
        localPosition = Vector3.zero,
        localScale = Vector3.one
      },
      layer = this.gameObject.layer
    }.AddComponent<OneTwoOptionsLayout>();
    this.InitContent();
  }

  public override void UpdateSettings(UserSettings settings)
  {
    base.UpdateSettings(settings);
    this.generalMessages.Init(settings);
    this.generalMisc.Init(settings);
    this.generalSound.Init(settings);
  }

  private void InitContent()
  {
    GameObject gameObject1 = NGUITools.AddChild(this.layout.ContentArea[0], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/general/SettingsGeneralMessages"));
    if ((UnityEngine.Object) gameObject1 == (UnityEngine.Object) null)
      throw new ArgumentNullException("Settings messages object failed to load");
    this.generalMessages = gameObject1.GetComponent<SettingsGeneralMessages>();
    if ((UnityEngine.Object) this.generalMessages == (UnityEngine.Object) null)
      Debug.LogError((object) "General Messages was not found");
    this.generalMessages.OnSettingChanged = new OnSettingChanged(((OptionsContent) this).ChangeSetting);
    GameObject gameObject2 = NGUITools.AddChild(this.layout.ContentArea[1], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/general/SettingsGeneralMisc"));
    if ((UnityEngine.Object) gameObject2 == (UnityEngine.Object) null)
      throw new ArgumentNullException("Settings misc object failed to load");
    this.generalMisc = gameObject2.GetComponent<SettingsGeneralMisc>();
    this.generalMisc.OnSettingChanged = new OnSettingChanged(((OptionsContent) this).ChangeSetting);
    GameObject gameObject3 = NGUITools.AddChild(this.layout.ContentArea[2], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/general/SettingsGeneralSound"));
    if ((UnityEngine.Object) gameObject3 == (UnityEngine.Object) null)
      throw new ArgumentNullException("Settings Sound object failed to load");
    this.generalSound = gameObject3.GetComponent<SettingsGeneralSound>();
    this.generalSound.OnSettingChanged = new OnSettingChanged(((OptionsContent) this).ChangeSetting);
    GameObject gameObject4 = NGUITools.AddChild(this.layout.ContentArea[3], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingFooterButtonBar"));
    if ((UnityEngine.Object) gameObject4 == (UnityEngine.Object) null)
      throw new ArgumentNullException("InputBindingFooterButtonBar");
    this.footerButtonBar = gameObject4.GetComponent<InputBindingFooterButtonBar>();
    this.footerButtonBar.SetButtonDelegates(new AnonymousDelegate(((OptionsContent) this).Undo), new AnonymousDelegate(((OptionsContent) this).Apply), new AnonymousDelegate(this.ResetDialog));
  }

  private void ResetDialog()
  {
    DialogBoxFactoryNgui.CreateResetSettingsDialogBox(new AnonymousDelegate(((OptionsContent) this).RestoreDefaultSettings));
  }

  public override void RestoreDefaultSettings()
  {
    Debug.Log((object) "Restoring default settings");
    this.generalMessages.RestoreDefaultSettings(this.settings);
    this.generalMisc.RestoreDefaultSettings(this.settings);
    this.generalSound.RestoreDefaultSettings(this.settings);
    this.UpdateSettings(this.settings);
    this.Apply();
  }
}
