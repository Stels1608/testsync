﻿// Decompiled with JetBrains decompiler
// Type: GUIPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Tooltips;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GUIPanel : IGUIPanel, IGUIRenderable, InputListener
{
  protected Dictionary<Action, AnonymousDelegate> handlers = new Dictionary<Action, AnonymousDelegate>();
  private bool handleMouseInput = true;
  private bool handleJoystickAxesInput = true;
  private string name = string.Empty;
  protected SmartRect root;
  protected List<GUIPanel> children;
  protected bool wasMouseDown;
  public bool MouseTransparent;
  protected GuiAdvancedTooltipBase advancedTooltip;
  private bool isDebug;
  private bool isRendered;
  protected bool isUpdated;

  public GuiAdvancedTooltipBase AdvancedTooltip
  {
    get
    {
      return this.advancedTooltip;
    }
    set
    {
      this.advancedTooltip = value;
    }
  }

  public bool IsDebug
  {
    get
    {
      return this.isDebug;
    }
    set
    {
      this.isDebug = value;
    }
  }

  public virtual bool IsRendered
  {
    get
    {
      return this.isRendered;
    }
    set
    {
      if (!(this.isRendered ^ value))
        return;
      this.isRendered = value;
      if (value)
        this.OnShow();
      else
        this.OnHide();
    }
  }

  public virtual bool IsUpdated
  {
    get
    {
      if (!this.IsRendered)
        return this.isUpdated;
      return true;
    }
    set
    {
      this.isUpdated = value;
    }
  }

  public string Name
  {
    get
    {
      return this.name;
    }
    set
    {
      this.name = value;
    }
  }

  public virtual bool IsBigWindow
  {
    get
    {
      return false;
    }
  }

  public virtual float2 Position
  {
    get
    {
      return this.root.Position;
    }
    set
    {
      this.root.Position = value;
    }
  }

  public virtual float PositionX
  {
    get
    {
      return this.root.Position.x;
    }
    set
    {
      this.Position = new float2(value, this.Position.y);
    }
  }

  public virtual float PositionY
  {
    get
    {
      return this.root.Position.y;
    }
    set
    {
      this.Position = new float2(this.Position.x, value);
    }
  }

  public Rect Rect
  {
    get
    {
      return this.root.Rect;
    }
    set
    {
      this.root.Rect = value;
    }
  }

  public SmartRect SmartRect
  {
    get
    {
      return this.root;
    }
  }

  public SmartRect Parent
  {
    get
    {
      return this.root.Parent;
    }
    set
    {
      this.root.Parent = value;
    }
  }

  public virtual float2 Size
  {
    get
    {
      return this.SmartRect.Size;
    }
    set
    {
      this.SmartRect.Size = value;
    }
  }

  public virtual float Width
  {
    get
    {
      return this.SmartRect.Width;
    }
    set
    {
      this.SmartRect.Width = value;
    }
  }

  public virtual float Height
  {
    get
    {
      return this.SmartRect.Height;
    }
    set
    {
      this.SmartRect.Height = value;
    }
  }

  public virtual List<GUIPanel> Children
  {
    get
    {
      return this.children;
    }
  }

  public virtual bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public virtual bool HandleJoystickAxesInput
  {
    get
    {
      if (this.IsRendered)
        return this.handleJoystickAxesInput;
      return false;
    }
    set
    {
      this.handleJoystickAxesInput = value;
    }
  }

  public virtual bool HandleMouseInput
  {
    get
    {
      if (this.IsRendered)
        return this.handleMouseInput;
      return false;
    }
    set
    {
      this.handleMouseInput = value;
    }
  }

  public GUIPanel()
    : this((SmartRect) null)
  {
  }

  public GUIPanel(SmartRect parent)
  {
    this.root = new SmartRect(parent);
    this.children = new List<GUIPanel>();
  }

  protected virtual void OnShow()
  {
    using (List<GUIPanel>.Enumerator enumerator = this.Children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIPanel current = enumerator.Current;
        if (current.IsRendered)
          current.OnShow();
      }
    }
  }

  protected virtual void OnHide()
  {
    using (List<GUIPanel>.Enumerator enumerator = this.Children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIPanel current = enumerator.Current;
        if (current.IsRendered)
          current.OnHide();
      }
    }
  }

  public virtual void ToggleIsRendered()
  {
    this.IsRendered = !this.IsRendered;
  }

  public virtual void Show()
  {
    this.IsRendered = true;
  }

  public virtual void Hide()
  {
    this.IsRendered = false;
  }

  public virtual void Update()
  {
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].IsUpdated)
        this.Children[index].Update();
    }
  }

  public virtual void Draw()
  {
    if (this.IsDebug)
      GUI.DrawTexture(this.SmartRect.AbsRect, (Texture) TextureCache.Get(new Color(1f, 0.0f, 0.0f, 0.2f)));
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].IsRendered)
        this.Children[index].Draw();
    }
  }

  public void PlaceLeftUpperCornerOf(GUIPanel panel)
  {
    this.Position = new float2((float) (-(double) panel.Width / 2.0 + (double) this.Width / 2.0), (float) (-(double) panel.Height / 2.0 + (double) this.Height / 2.0));
  }

  public void PlaceRightUpperCornerOf(GUIPanel panel)
  {
    this.Position = new float2((float) ((double) panel.Width / 2.0 + (double) this.Width / 2.0), (float) (-(double) panel.Height / 2.0 + (double) this.Height / 2.0));
  }

  public void PlaceRightDownCornerOf(GUIPanel panel, float offsetX)
  {
    this.Position = new float2((float) ((double) panel.Width / 2.0 + (double) this.Width / 2.0) + offsetX, (float) ((double) panel.Height / 2.0 + (double) this.Height / 2.0));
  }

  public void PlaceLeftOf(GUIPanel panel, float space)
  {
    this.Position = new float2((float) ((double) panel.Position.x - (double) panel.SmartRect.Width / 2.0 - (double) this.SmartRect.Width / 2.0) - space, this.Position.y);
  }

  public void PlaceRightOf(GUIPanel panel)
  {
    this.PlaceRightOf(panel, 0.0f);
  }

  public void PlaceRightOf(GUIPanel panel, float space)
  {
    this.Position = new float2((float) ((double) panel.PositionX + (double) panel.Width / 2.0 + (double) this.Width / 2.0) + space, panel.PositionY);
  }

  public float2 RightOf(GUIPanel panel)
  {
    return this.RightOf(panel, 0.0f);
  }

  public float2 RightOf(GUIPanel panel, float gap)
  {
    return new float2((float) ((double) panel.PositionX + (double) panel.Width / 2.0 + (double) this.Width / 2.0) + gap, panel.PositionY);
  }

  public void PlaceAboveOf(GUIPanel panel, float offsetY)
  {
    this.Position = new float2(this.Position.x, (float) ((double) panel.Position.y - (double) panel.SmartRect.Height / 2.0 - (double) this.SmartRect.Height / 2.0) - offsetY);
  }

  public void PlaceAboveOf(GUIPanel panel, float offsetX, float offsetY)
  {
    this.Position = new float2(this.Position.x + offsetX, (float) ((double) panel.Position.y - (double) panel.SmartRect.Height / 2.0 - (double) this.SmartRect.Height / 2.0) - offsetY);
  }

  public void PlaceBelowOf(GUIPanel panel)
  {
    this.PlaceBelowOf(panel, 0.0f);
  }

  public void PlaceBelowOf(GUIPanel panel, float space)
  {
    this.Position = new float2(panel.PositionX, (float) ((double) panel.PositionY + (double) panel.Height / 2.0 + (double) this.Height / 2.0) + space);
  }

  public float2 BelowOf(GUIPanel panel)
  {
    return this.BelowOf(panel, 0.0f);
  }

  public float2 BelowOf(GUIPanel panel, float gap)
  {
    return new float2(panel.Position.x, (float) ((double) panel.Position.y + (double) panel.Height / 2.0 + (double) this.Height / 2.0) + gap);
  }

  public virtual T Find<T>() where T : GUIPanel
  {
    GUIPanel guiPanel = this.children.Find((Predicate<GUIPanel>) (entry => entry is T));
    if (guiPanel != null)
      return guiPanel as T;
    return (T) null;
  }

  public virtual T Find<T>(string name) where T : GUIPanel
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    GUIPanel guiPanel = this.children.Find(new Predicate<GUIPanel>(new GUIPanel.\u003CFind\u003Ec__AnonStorey6D<T>() { name = name }.\u003C\u003Em__46));
    if (guiPanel != null)
      return guiPanel as T;
    return (T) null;
  }

  public virtual T Find<T>(Predicate<T> match) where T : GUIPanel
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    GUIPanel guiPanel = this.children.Find(new Predicate<GUIPanel>(new GUIPanel.\u003CFind\u003Ec__AnonStorey6E<T>() { match = match }.\u003C\u003Em__47));
    if (guiPanel != null)
      return guiPanel as T;
    return (T) null;
  }

  public virtual T FindRecoursively<T>() where T : GUIPanel
  {
    return this.FindRecoursively<T>((Predicate<T>) (child => true));
  }

  public virtual T FindRecoursively<T>(string name) where T : GUIPanel
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.FindRecoursively<T>(new Predicate<T>(new GUIPanel.\u003CFindRecoursively\u003Ec__AnonStorey6F<T>() { name = name }.\u003C\u003Em__49));
  }

  public virtual T FindRecoursively<T>(Predicate<T> match) where T : GUIPanel
  {
    using (List<GUIPanel>.Enumerator enumerator = this.Children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIPanel current = enumerator.Current;
        if (current is T && match(current as T))
          return current as T;
        T recoursively = current.FindRecoursively<T>(match);
        if ((object) recoursively != null)
          return recoursively;
      }
    }
    return (T) null;
  }

  public virtual List<T> FindAll<T>() where T : GUIPanel
  {
    return this.FindAll<T>((Predicate<T>) (child => true));
  }

  public virtual List<T> FindAll<T>(Predicate<T> match) where T : GUIPanel
  {
    List<T> objList = new List<T>();
    using (List<GUIPanel>.Enumerator enumerator = this.Children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIPanel current = enumerator.Current;
        if (current is T && match(current as T))
          objList.Add(current as T);
      }
    }
    return objList;
  }

  public virtual List<T> FindAllRecoursively<T>() where T : GUIPanel
  {
    return this.FindAllRecoursively<T>((Predicate<T>) (child => true));
  }

  public virtual List<T> FindAllRecoursively<T>(Predicate<T> match) where T : GUIPanel
  {
    List<T> objList = new List<T>();
    using (List<GUIPanel>.Enumerator enumerator = this.Children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIPanel current = enumerator.Current;
        if (current is T && match(current as T))
        {
          objList.Add(current as T);
          objList.AddRange((IEnumerable<T>) current.FindAllRecoursively<T>(match));
        }
      }
    }
    return objList;
  }

  public virtual void InsertPanel(GUIPanel panel)
  {
    this.children.Insert(0, panel);
    panel.Parent = this.root;
  }

  public virtual void AddPanel(GUIPanel panel)
  {
    this.children.Add(panel);
    panel.Parent = this.root;
  }

  public virtual void AddPanel(GUIPanel panel, float2 position)
  {
    panel.Position = position;
    this.AddPanel(panel);
  }

  public virtual void RemovePanel(GUIPanel panel)
  {
    this.children.Remove(panel);
  }

  public virtual void Clear()
  {
    this.children.Clear();
  }

  public virtual bool Contains(float2 point)
  {
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].HandleMouseInput && this.Children[index].Contains(point))
        return true;
    }
    if (this.MouseTransparent)
      return false;
    bool flag = this.root.AbsRect.Contains(point.ToV2());
    GUIManager.MouseOverAnyOldGuiElement |= flag;
    return flag;
  }

  public virtual void RecalculateAbsCoords()
  {
    this.root.RecalculateAbsCoords();
    using (List<GUIPanel>.Enumerator enumerator = this.children.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.RecalculateAbsCoords();
    }
  }

  public virtual bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    this.wasMouseDown = false;
    if (!this.HandleMouseInput)
      return false;
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].HandleMouseInput)
        this.Children[index].OnMouseDown(mousePosition, mouseKey);
    }
    if (!this.Contains(mousePosition))
      return false;
    this.wasMouseDown = true;
    return true;
  }

  public virtual void OnMouseMove(float2 mousePosition)
  {
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].HandleMouseInput)
        this.Children[index].OnMouseMove(mousePosition);
    }
  }

  public virtual bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (!this.wasMouseDown)
      return false;
    this.wasMouseDown = false;
    if (!this.HandleMouseInput)
      return false;
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].HandleMouseInput)
        this.Children[index].OnMouseUp(mousePosition, mouseKey);
    }
    return this.Contains(mousePosition);
  }

  public virtual bool OnMouseScrollDown()
  {
    if (!this.HandleMouseInput)
      return false;
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].HandleMouseInput && this.Children[index].OnMouseScrollDown())
        return true;
    }
    return false;
  }

  public virtual bool OnMouseScrollUp()
  {
    if (!this.HandleMouseInput)
      return false;
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].HandleMouseInput && this.Children[index].OnMouseScrollUp())
        return true;
    }
    return false;
  }

  public virtual bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (this.handlers.ContainsKey(action))
      return true;
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].HandleKeyboardInput && this.Children[index].OnKeyDown(keyboardKey, action))
        return true;
    }
    return false;
  }

  public virtual bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (this.handlers.ContainsKey(action))
    {
      this.handlers[action]();
      return true;
    }
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].HandleKeyboardInput && this.Children[index].OnKeyUp(keyboardKey, action))
        return true;
    }
    return false;
  }

  public virtual bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    return false;
  }

  public void SetTooltip(string text)
  {
    if (this.advancedTooltip == null || !(this.advancedTooltip is GuiAdvancedLabelTooltip))
      this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip(text);
    else
      (this.advancedTooltip as GuiAdvancedLabelTooltip).SetText(text);
  }

  public void SetTooltip(string label, Action hotKey)
  {
    if (this.advancedTooltip == null || !(this.advancedTooltip is GuiAdvancedHotkeyTooltip))
      this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedHotkeyTooltip(label, hotKey);
    else
      (this.advancedTooltip as GuiAdvancedHotkeyTooltip).SetText(label, hotKey);
  }

  public void SetTooltip(ShipItem item)
  {
    if (item == null)
      return;
    if (this.advancedTooltip == null || !(this.advancedTooltip is GuiAdvancedItemTooltip))
      this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedItemTooltip(item);
    else
      (this.advancedTooltip as GuiAdvancedItemTooltip).SetItem(item);
  }

  public void SetTooltip(GUICard card)
  {
    if (this.advancedTooltip == null || !(this.advancedTooltip is GuiAdvancedItemTooltip))
      this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedItemTooltip(card);
    else
      (this.advancedTooltip as GuiAdvancedItemTooltip).SetItem(card);
  }

  public void SetTooltip(ShipBuff buff)
  {
    if (this.advancedTooltip == null || !(this.advancedTooltip is GuiAdvancedBuffTooltip))
      this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedBuffTooltip(buff);
    else
      (this.advancedTooltip as GuiAdvancedBuffTooltip).SetBuff(buff);
  }

  public void SetStoreToolTip(ShipItem item, ShopInventoryContainer exchangeType = ShopInventoryContainer.Store, bool isSlotItem = false, bool condensedItemVersion = false)
  {
    if (item is StarterKit)
    {
      if (!(this.advancedTooltip is GuiAdvancedStarterKitTooltip))
        this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedStarterKitTooltip();
      (this.advancedTooltip as GuiAdvancedStarterKitTooltip).SetStarterKit(item as StarterKit);
    }
    else
    {
      if (!(this.advancedTooltip is GuiAdvancedShopItemTooltip))
        this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedShopItemTooltip();
      (this.advancedTooltip as GuiAdvancedShopItemTooltip).SetShopItem((GameItemCard) item, exchangeType, condensedItemVersion, false, isSlotItem);
    }
    if (exchangeType == ShopInventoryContainer.Store && !isSlotItem)
      return;
    ((GuiAdvancedCardTooltip) this.advancedTooltip).ShowUpgradeSaleInfo(item);
  }

  public void SetTooltip(ShipAbility ability, GUICard card)
  {
    if (this.advancedTooltip == null || !(this.advancedTooltip is GuiAdvancedAbilityTooltip))
      this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedAbilityTooltip(ability, card);
    else
      (this.advancedTooltip as GuiAdvancedAbilityTooltip).SetItem(ability, card);
  }

  public virtual bool OnShowTooltip(float2 mousePosition)
  {
    if (!this.isRendered || !this.HandleMouseInput && !this.Contains(mousePosition))
      return false;
    for (int index = 0; index < this.Children.Count; ++index)
    {
      if (this.Children[index].HandleMouseInput && this.Children[index].Contains(mousePosition) && this.Children[index].OnShowTooltip(mousePosition))
        return true;
    }
    if (!this.IsRendered || !this.Contains(mousePosition) || this.advancedTooltip == null)
      return false;
    Game.TooltipManager.ShowTooltip(this.advancedTooltip);
    return true;
  }

  protected class Timer : GUIPanel
  {
    private float timeLeft;
    private float interval;
    private bool once;
    private AnonymousDelegate Handler;

    public Timer(float interval, AnonymousDelegate handler)
    {
      this.interval = interval;
      this.Handler = handler;
      this.IsRendered = false;
      this.IsUpdated = true;
    }

    public Timer(float interval, AnonymousDelegate handler, bool once)
      : this(interval, handler)
    {
      this.once = once;
    }

    public void Reset()
    {
      this.timeLeft = this.interval;
      this.IsUpdated = false;
    }

    public void Awake()
    {
      this.IsUpdated = true;
    }

    public void SetInterval(float interval)
    {
      this.interval = interval;
    }

    public override void Update()
    {
      this.timeLeft -= Time.deltaTime;
      if ((double) this.timeLeft > 0.0)
        return;
      if (this.once)
        this.IsUpdated = false;
      this.Handler();
      this.timeLeft = this.interval;
    }
  }
}
