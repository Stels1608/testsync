﻿// Decompiled with JetBrains decompiler
// Type: StopAmbientSoundAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class StopAmbientSoundAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    AudioClip audioClip = (AudioClip) message.Data;
    AudioListener objectOfType = Object.FindObjectOfType<AudioListener>();
    if ((Object) objectOfType == (Object) null)
    {
      Debug.LogWarning((object) ("Cannot stop ambient sound " + audioClip.name + ", AudioListener is null"));
    }
    else
    {
      foreach (AudioSource componentsInChild in objectOfType.GetComponentsInChildren<AudioSource>())
      {
        if ((Object) componentsInChild.clip == (Object) audioClip)
        {
          Object.Destroy((Object) componentsInChild.gameObject);
          break;
        }
      }
    }
  }
}
