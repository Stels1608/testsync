﻿// Decompiled with JetBrains decompiler
// Type: TransScene
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;

public class TransScene : GameLevel
{
  private float delay;
  private int loadedBundles;
  private int totalBundles;
  private LoadingSceneWindow loadingSceneWindow;
  protected LevelProfile nextLevelProfile;
  protected TransSceneType type;
  public static bool roomPrefabLoaded;

  public override GameLevelType LevelType
  {
    get
    {
      return GameLevelType.TransScene;
    }
  }

  public override bool IsIngameLevel
  {
    get
    {
      return false;
    }
  }

  protected override void AddLoadingScreenDependencies()
  {
  }

  public override void Initialize(LevelProfile levelProfile)
  {
    base.Initialize(levelProfile);
    this.nextLevelProfile = (levelProfile as TransLevelProfile).NextLevelProfile;
    this.type = (levelProfile as TransLevelProfile).Type;
    this.delay = 2f;
  }

  protected override void Start()
  {
    this.loadingSceneWindow = new LoadingSceneWindow(this.type);
    this.loadingSceneWindow.IsRendered = true;
    Game.GUIManager.IsRendered = true;
    Game.GUIManager.AddPanel((IGUIPanel) this.loadingSceneWindow);
    Console console = Game.Console;
    Game.GUIManager.RemovePanel((IGUIPanel) console);
    Game.GUIManager.AddPanel((IGUIPanel) console);
    Game.Ticker.IsRendered = false;
    this.SceneLog("Transfering...");
    this.SceneLog("Loading scene");
    base.Start();
    this.StartCoroutine(this.Loop());
  }

  private float GetProgress()
  {
    if (this.totalBundles == 0)
      return 0.0f;
    return (float) this.loadedBundles / (float) this.totalBundles;
  }

  private void LoadNextScene()
  {
    Log.Add("Loading Next Scene : " + this.nextLevelProfile.SceneName);
    Game.LoadLevel(this.nextLevelProfile);
  }

  private void SceneLog(string message)
  {
    uint num = 0;
    if (Game.Me != null)
      num = Game.Me.ServerID;
    DebugUtility.LogInfo(message + " : " + (object) DateTime.UtcNow + "-UTC : " + num.ToString());
  }

  [DebuggerHidden]
  private IEnumerator ShowFakeCounter()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TransScene.\u003CShowFakeCounter\u003Ec__Iterator2E() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator Loop()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new TransScene.\u003CLoop\u003Ec__Iterator2F() { \u003C\u003Ef__this = this };
  }
}
