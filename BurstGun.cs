﻿// Decompiled with JetBrains decompiler
// Type: BurstGun
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BurstGun : AbstractBurstGun
{
  private float lastShotTime = float.NegativeInfinity;
  private bool burstEnded = true;
  public BurstGunType BurstGunType;
  public GunSoundPlayer GunSoundPlayer;
  public GameObject ShootingPrefab;
  public float BurstDuration;
  private GameObject shootingEffect;
  private float compensation;

  protected override void Awake()
  {
    base.Awake();
    this.BurstPool = !((Object) SpaceLevel.GetLevel() != (Object) null) ? new BurstPool() : SpaceLevel.GetLevel().BurstPool;
  }

  protected override void LateUpdate()
  {
    base.LateUpdate();
    if (this.madeShotCount < this.ShotCount)
    {
      float num1 = this.BurstDuration / (float) this.ShotCount;
      if ((double) Time.time - (double) this.lastShotTime > (double) num1)
      {
        int num2 = 1;
        this.compensation += Time.deltaTime - num1;
        while ((double) this.compensation > (double) num1)
        {
          ++num2;
          this.compensation -= num1;
        }
        this.lastShotTime = Time.time;
        for (int index = 0; index < num2 && this.madeShotCount < this.ShotCount; ++index)
        {
          this.MakeShot();
          ++this.madeShotCount;
        }
      }
    }
    else if (!this.burstEnded)
    {
      this.burstEnded = true;
      if ((Object) this.shootingEffect != (Object) null && this.shootingEffect.activeInHierarchy)
        this.shootingEffect.SetActive(false);
    }
    this.SaveLastTargetPosition();
  }

  public override void Fire(SpaceObject target)
  {
    base.Fire(target);
    this.compensation = 0.0f;
    if ((Object) this.GunSoundPlayer != (Object) null)
      this.GunSoundPlayer.Play(this.BurstDuration);
    if (this.HighQuality)
    {
      if ((Object) this.shootingEffect == (Object) null && (Object) this.ShootingPrefab != (Object) null)
      {
        this.shootingEffect = (GameObject) Object.Instantiate((Object) this.ShootingPrefab, this.transform.position, this.transform.rotation);
        this.shootingEffect.transform.parent = this.transform;
      }
      if ((Object) this.shootingEffect != (Object) null && !this.shootingEffect.activeInHierarchy)
        this.shootingEffect.SetActive(true);
    }
    this.burstEnded = false;
  }

  protected override Vector3 GetShotOrigin()
  {
    return this.transform.position;
  }
}
