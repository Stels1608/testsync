﻿// Decompiled with JetBrains decompiler
// Type: SectorEventRewardItemUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SectorEventRewardItemUgui : MonoBehaviour
{
  [SerializeField]
  private Image rewardIcon;
  [SerializeField]
  private TextMeshProUGUI amountText;
  private GUICard guiCard;

  public GUICard GuiCard
  {
    get
    {
      return this.guiCard;
    }
    set
    {
      this.guiCard = value;
      if (this.guiCard != null)
        this.guiCard.IsLoaded.AddHandler(new SignalHandler(this.SetupSprite));
      else
        this.SetupSprite();
    }
  }

  public void SetContent(GUICard guiCard, string amount)
  {
    this.rewardIcon.enabled = false;
    this.GuiCard = guiCard;
    this.amountText.text = amount;
  }

  private void SetupSprite()
  {
    this.rewardIcon.enabled = true;
    if (this.guiCard != null)
      this.rewardIcon.sprite = Tools.TexAtlasToSprite(ResourceLoader.Load<Texture2D>(this.guiCard.GUIAtlasTexturePath), new Vector2(40f, 35f), (uint) this.guiCard.FrameIndex);
    else
      this.rewardIcon.enabled = false;
  }
}
