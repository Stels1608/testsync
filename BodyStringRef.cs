﻿// Decompiled with JetBrains decompiler
// Type: BodyStringRef
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BodyStringRef : MultiBody
{
  public string[] rendererPath;

  protected override void Awake()
  {
    this.Renderers = new RendererArray[this.rendererPath.Length];
    for (int index1 = 0; index1 < this.rendererPath.Length; ++index1)
    {
      Transform child = this.transform.FindChild(this.rendererPath[index1]);
      if ((Object) child == (Object) null)
      {
        Debug.LogError((object) (this.name + " encountered NULL childTransform in BodyStringRef - Invalid RendererPath: " + this.rendererPath[index1]));
      }
      else
      {
        Renderer[] componentsInChildren = child.gameObject.GetComponentsInChildren<Renderer>();
        for (int index2 = 0; index2 < componentsInChildren.Length; ++index2)
          componentsInChildren[index2].enabled = false;
        this.Renderers[index1] = new RendererArray(componentsInChildren);
      }
    }
    base.Awake();
  }
}
