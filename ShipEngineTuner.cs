﻿// Decompiled with JetBrains decompiler
// Type: ShipEngineTuner
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class ShipEngineTuner : MonoBehaviour
{
  public float BoostVolume = 1f;
  public ShipEngineTuner.SizeElement SmallShip;
  public ShipEngineTuner.SizeElement MediumShip;
  public ShipEngineTuner.SizeElement LargeShip;
  public ShipEngineTuner.FactionSizeElement ColonialSmallShip;
  public ShipEngineTuner.FactionSizeElement ColonialMediumShip;
  public ShipEngineTuner.FactionSizeElement ColonialLargeShip;
  public ShipEngineTuner.FactionSizeElement CylonSmallShip;
  public ShipEngineTuner.FactionSizeElement CylonMediumShip;
  public ShipEngineTuner.FactionSizeElement CylonLargeShip;
  public AudioClip BoostStartClip;
  public AudioClip BoostLoopClip;
  public AudioClip BoostEndClip;

  public void Tune(Gearbox gearbox)
  {
    if (gearbox.SpaceObject is Ship)
    {
      switch ((gearbox.SpaceObject as Ship).ShipCardLight.Tier)
      {
        case 1:
          gearbox.Limit = this.SmallShip.Limit;
          break;
        case 2:
          gearbox.Limit = this.MediumShip.Limit;
          break;
        case 3:
        case 4:
          gearbox.Limit = this.LargeShip.Limit;
          break;
      }
    }
    ShipEngine shipEngine = (ShipEngine) null;
    using (List<ShipEngine>.Enumerator enumerator = gearbox.Engines.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipEngine current = enumerator.Current;
        if ((UnityEngine.Object) current == (UnityEngine.Object) null)
          Debug.LogError((object) "Encountered NULL engine in ShipEngineTuner - Invalid ShipEngine");
        else if (current.engineComponentType == ShipEngine.EngineComponentType.Flare)
        {
          shipEngine = current;
          break;
        }
      }
    }
    if (!((UnityEngine.Object) shipEngine != (UnityEngine.Object) null))
      return;
    if (gearbox.SpaceObject == SpaceLevel.GetLevel().GetPlayerShip())
      shipEngine.DopplerLevel = 0.0f;
    shipEngine.BoostStartClip = this.BoostStartClip;
    shipEngine.BoostLoopClip = this.BoostLoopClip;
    shipEngine.BoostEndClip = this.BoostEndClip;
    shipEngine.BoostVolume = this.BoostVolume;
    if (!(gearbox.SpaceObject is Ship))
      return;
    Ship ship = gearbox.SpaceObject as Ship;
    switch (ship.Faction)
    {
      case Faction.Neutral:
      case Faction.Colonial:
      case Faction.Ancient:
        switch (ship.ShipCardLight.Tier)
        {
          case 1:
            shipEngine.EngineClip = this.ColonialSmallShip.EngineClip;
            shipEngine.EngineVolume = this.ColonialSmallShip.EngineVolume;
            shipEngine.EngineMinDistance = this.ColonialSmallShip.MinDistance;
            return;
          case 2:
            shipEngine.EngineClip = this.ColonialMediumShip.EngineClip;
            shipEngine.EngineVolume = this.ColonialMediumShip.EngineVolume;
            shipEngine.EngineMinDistance = this.ColonialMediumShip.MinDistance;
            return;
          case 3:
          case 4:
            shipEngine.EngineClip = this.ColonialLargeShip.EngineClip;
            shipEngine.EngineVolume = this.ColonialLargeShip.EngineVolume;
            shipEngine.EngineMinDistance = this.ColonialLargeShip.MinDistance;
            return;
          default:
            return;
        }
      case Faction.Cylon:
        switch (ship.ShipCardLight.Tier)
        {
          case 1:
            shipEngine.EngineClip = this.CylonSmallShip.EngineClip;
            shipEngine.EngineVolume = this.CylonSmallShip.EngineVolume;
            shipEngine.EngineMinDistance = this.CylonSmallShip.MinDistance;
            return;
          case 2:
            shipEngine.EngineClip = this.CylonMediumShip.EngineClip;
            shipEngine.EngineVolume = this.CylonMediumShip.EngineVolume;
            shipEngine.EngineMinDistance = this.CylonMediumShip.MinDistance;
            return;
          case 3:
          case 4:
            shipEngine.EngineClip = this.CylonLargeShip.EngineClip;
            shipEngine.EngineVolume = this.CylonLargeShip.EngineVolume;
            shipEngine.EngineMinDistance = this.CylonLargeShip.MinDistance;
            return;
          default:
            return;
        }
    }
  }

  [Serializable]
  public class FactionSizeElement
  {
    public float EngineVolume = 1f;
    public float MinDistance = 1f;
    public AudioClip EngineClip;
  }

  [Serializable]
  public class SizeElement
  {
    public float Limit;
  }
}
