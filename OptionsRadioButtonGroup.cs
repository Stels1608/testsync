﻿// Decompiled with JetBrains decompiler
// Type: OptionsRadioButtonGroup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class OptionsRadioButtonGroup : OptionsElement
{
  private List<string> textList = new List<string>();
  private List<object> valueList = new List<object>();
  [SerializeField]
  private RadioButtonGroupWidget radioButtonGroupWidget;
  private UserSetting setting;
  private OnSettingChanged onSettingChanged;
  [SerializeField]
  private List<UILabel> lableList;

  public override OnSettingChanged OnSettingChanged
  {
    get
    {
      return this.onSettingChanged;
    }
    set
    {
      this.onSettingChanged = value;
      this.radioButtonGroupWidget.OnValueChanged = new AnonymousDelegate(this.SettingsChanged);
    }
  }

  public override void Init(UserSetting sett, object value)
  {
    this.setting = sett;
    OptionsEnumWrapper optionsEnumWrapper = value as OptionsEnumWrapper;
    if (optionsEnumWrapper != null)
    {
      object obj = optionsEnumWrapper.actualValue;
      this.textList = new List<string>((IEnumerable<string>) optionsEnumWrapper.valueText);
      this.valueList = new List<object>((IEnumerable<object>) optionsEnumWrapper.valueList);
      for (int index = 0; index < this.valueList.Count; ++index)
      {
        if (this.valueList[index].Equals(obj))
          this.radioButtonGroupWidget.SetActiveButtonSilent(index);
      }
    }
    this.ReloadLanguageData();
  }

  public void SettingsChanged()
  {
    if (this.onSettingChanged == null)
      return;
    this.OnSettingChanged(this.setting, this.valueList[this.radioButtonGroupWidget.GetIndexOfActiveButton()]);
  }

  public override void ReloadLanguageData()
  {
    this.optionDescLabel.text = Tools.GetLocalized(this.setting);
    for (int index = 0; index < this.lableList.Count; ++index)
      this.lableList[index].text = this.textList[index];
  }
}
