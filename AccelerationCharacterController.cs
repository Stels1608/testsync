﻿// Decompiled with JetBrains decompiler
// Type: AccelerationCharacterController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AccelerationCharacterController : MonoBehaviour
{
  private const float SENSITIVITY_X = 15f;
  private const float SENSITIVITY_Y = 15f;
  private const float ACCELERATION_SPEED = 1f;
  private CharacterMotor motor;
  private float rotationX;
  private float rotationY;
  private Quaternion originalRotation;

  private void Start()
  {
    this.motor = this.GetComponent(typeof (CharacterMotor)) as CharacterMotor;
    if ((Object) this.motor == (Object) null)
      Debug.Log((object) "Motor is null!!");
    this.originalRotation = this.transform.localRotation;
  }

  private void Update()
  {
    Vector3 vector3 = this.motor.desiredMovementDirection;
    vector3.z += (float) ((double) Input.GetAxis("Vertical") * (double) Time.deltaTime * 1.0);
    vector3.x += (float) ((double) Input.GetAxis("Horizontal") * (double) Time.deltaTime * 1.0);
    if ((double) vector3.magnitude > 1.0)
      vector3 = vector3.normalized;
    this.rotationX += Input.GetAxis("Mouse X") * 15f;
    this.rotationY += Input.GetAxis("Mouse Y") * 15f;
    this.rotationX = Mathf.Repeat(this.rotationX, 360f);
    this.rotationY = Mathf.Clamp(this.rotationY, -90f, 90f);
    this.motor.desiredFacingDirection = this.originalRotation * Quaternion.AngleAxis(this.rotationX, Vector3.up) * Quaternion.AngleAxis(this.rotationY, -Vector3.right) * Vector3.forward;
    this.motor.desiredMovementDirection = vector3;
  }
}
