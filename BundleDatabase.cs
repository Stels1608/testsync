﻿// Decompiled with JetBrains decompiler
// Type: BundleDatabase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using CouchDB;
using Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class BundleDatabase
{
  public List<BundleDatabase.BundleRecord> Bundles = new List<BundleDatabase.BundleRecord>();
  public const string Path = "Assets/BundleDatabase.asset";

  public string AssetAddedName { get; set; }

  public void Save()
  {
    JsonData jsonData = JsonSerializator.Serialize((object) this.Bundles);
    FileStream fileStream = File.Create("Assets\\BundleDatabase.json");
    byte[] bytes = new UTF8Encoding(true).GetBytes(new JsonFormatter(jsonData.ToJsonString(string.Empty)).Format());
    fileStream.Write(bytes, 0, bytes.Length);
    fileStream.Close();
  }

  public static BundleDatabase Load()
  {
    FileStream fileStream = File.OpenRead("Assets\\BundleDatabase.json");
    byte[] numArray = new byte[1024];
    UTF8Encoding utF8Encoding = new UTF8Encoding(true);
    string json1 = string.Empty;
    while (fileStream.Read(numArray, 0, numArray.Length) > 0)
      json1 += utF8Encoding.GetString(numArray);
    fileStream.Close();
    JsonData json2;
    try
    {
      json2 = JsonReader.Parse(json1);
    }
    catch (Exception ex)
    {
      Debug.LogError((object) "PARSE ERROR == in BundleDatabase");
      throw ex;
    }
    return new BundleDatabase() { AssetAddedName = string.Empty, Bundles = JsonSerializator.Deserialize<List<BundleDatabase.BundleRecord>>(json2) };
  }

  public bool ContainsBundle(string bundleName)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.Bundles.Exists(new Predicate<BundleDatabase.BundleRecord>(new BundleDatabase.\u003CContainsBundle\u003Ec__AnonStorey58() { bundleName = bundleName }.\u003C\u003Em__20));
  }

  public BundleDatabase.BundleRecord GetBundle(string bundleName)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.Bundles.FirstOrDefault<BundleDatabase.BundleRecord>(new Func<BundleDatabase.BundleRecord, bool>(new BundleDatabase.\u003CGetBundle\u003Ec__AnonStorey59() { bundleName = bundleName }.\u003C\u003Em__21));
  }

  public int AddBundle(string bundleName)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BundleDatabase.\u003CAddBundle\u003Ec__AnonStorey5A bundleCAnonStorey5A = new BundleDatabase.\u003CAddBundle\u003Ec__AnonStorey5A();
    // ISSUE: reference to a compiler-generated field
    bundleCAnonStorey5A.bundleName = bundleName;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if (string.IsNullOrEmpty(bundleCAnonStorey5A.bundleName) || this.ContainsBundle(bundleCAnonStorey5A.bundleName))
      return -1;
    // ISSUE: reference to a compiler-generated field
    this.Bundles.Add(new BundleDatabase.BundleRecord(bundleCAnonStorey5A.bundleName));
    this.Bundles.Sort((Comparison<BundleDatabase.BundleRecord>) ((x, y) => string.Compare(x.Name, y.Name, StringComparison.Ordinal)));
    // ISSUE: reference to a compiler-generated method
    return this.Bundles.FindIndex(new Predicate<BundleDatabase.BundleRecord>(bundleCAnonStorey5A.\u003C\u003Em__23));
  }

  public void RemoveBundle(int bundleIndex)
  {
    if (bundleIndex < 0 || bundleIndex >= this.Bundles.Count)
      return;
    this.Bundles.RemoveAt(bundleIndex);
  }

  public void RemoveBundle(string bundleName)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.Bundles.RemoveAll(new Predicate<BundleDatabase.BundleRecord>(new BundleDatabase.\u003CRemoveBundle\u003Ec__AnonStorey5B()
    {
      bundleName = bundleName
    }.\u003C\u003Em__24));
  }

  public void AddAssetsToBundle(int bundleIndex, List<string> assets)
  {
    if (bundleIndex < 0 || bundleIndex >= this.Bundles.Count)
      return;
    this.AssetAddedName = string.Empty;
    BundleDatabase.BundleRecord bundleRecord = this.Bundles[bundleIndex];
    for (int index = 0; index < assets.Count; ++index)
    {
      if (!bundleRecord.Assets.Contains(assets[index]))
      {
        bundleRecord.Assets.Add(assets[index]);
        BundleDatabase bundleDatabase = this;
        string str = bundleDatabase.AssetAddedName + assets[index] + ": Has Been Added to: " + bundleRecord.Name + "\n";
        bundleDatabase.AssetAddedName = str;
      }
      else
      {
        BundleDatabase bundleDatabase = this;
        string str = bundleDatabase.AssetAddedName + "Warning ADDED Failed: " + assets[index] + " is already exists in the bundle.\n";
        bundleDatabase.AssetAddedName = str;
      }
    }
    bundleRecord.Assets.Sort();
  }

  public void RemoveAssetsFromBundle(int bundleIndex, List<int> assetIndexes)
  {
    if (bundleIndex < 0 || bundleIndex >= this.Bundles.Count)
      return;
    BundleDatabase.BundleRecord bundleRecord = this.Bundles[bundleIndex];
    for (int index = 0; index < assetIndexes.Count; ++index)
      bundleRecord.Assets.RemoveAt(assetIndexes[index] - index);
  }

  public class BundleRecord
  {
    public List<string> Assets = new List<string>();
    public string Name;
    public int Priority;

    public BundleRecord()
    {
      this.Name = string.Empty;
    }

    public BundleRecord(string name)
    {
      this.Name = name;
    }
  }
}
