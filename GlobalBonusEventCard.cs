﻿// Decompiled with JetBrains decompiler
// Type: GlobalBonusEventCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class GlobalBonusEventCard : Card
{
  public const byte MIN_LEVEL = 2;

  public BannerCard BannerCard { get; private set; }

  public GlobalBonusEventCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    r.ReadBoolean();
    uint num = r.ReadGUID();
    this.BannerCard = (BannerCard) Game.Catalogue.FetchCard(Game.Me.Faction != Faction.Colonial ? r.ReadGUID() : num, CardView.Banner);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.BannerCard
    });
    this.IsLoaded.Set();
  }
}
