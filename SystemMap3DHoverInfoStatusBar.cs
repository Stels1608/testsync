﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DHoverInfoStatusBar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SystemMap3DHoverInfoStatusBar : MonoBehaviour
{
  [SerializeField]
  private TextMeshProUGUI textLabel;
  [SerializeField]
  private Slider slider;

  public void SetValue(float current, float max)
  {
    if ((double) max < 100000.0)
      this.textLabel.text = string.Format("{0:0} / {1:0}", (object) current, (object) max);
    else
      this.textLabel.text = string.Format("{0:0} / {1:0}K", (object) current, (object) (float) ((double) max / 1000.0));
    this.slider.value = (double) Math.Abs(max) >= 1.0 / 1000.0 ? current / max : 0.0f;
    this.slider.interactable = true;
  }

  public void Reset()
  {
    this.textLabel.text = string.Format("-- / --");
    this.slider.value = 1f;
    this.slider.interactable = false;
  }
}
