﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorAsteroidResourceUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class HudIndicatorAsteroidResourceUgui : HudIndicatorBaseComponentUgui, IReceivesTarget
{
  [SerializeField]
  private Image frame;
  [SerializeField]
  private Image resourceIcon;
  private bool wasScanned;
  private float smoothDistance;
  private float viewRange;
  private Asteroid asteroid;

  private HudIndicatorsSpriteLinker SpriteLinker
  {
    get
    {
      return HudIndicatorsSpriteLinker.Instance;
    }
  }

  protected override void Awake()
  {
    base.Awake();
  }

  protected override void ResetComponentStates()
  {
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.asteroid = target as Asteroid;
    if (this.asteroid == null)
    {
      Debug.LogWarning((object) ("HudIndicatorAsteroidResourceUgui on non-asteroid type " + (object) target.SpaceEntityType + ". Sure??"));
    }
    else
    {
      if (this.asteroid.WasScanned)
        this.OnScanned(this.asteroid.Resource);
      this.UpdateColorAndAlpha();
    }
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    if (!this.InScreen || !this.wasScanned)
      this.ShowIcons(false);
    else
      this.ShowIcons(this.IsSelected && this.InScreen);
  }

  private void ShowIcons(bool show)
  {
    this.gameObject.SetActive(show);
  }

  public override void OnScanned(ItemCountable resource)
  {
    if (resource == null || (int) resource.Count == 0)
      return;
    this.wasScanned = true;
    ResourceType resourceType = (ResourceType) resource.CardGUID;
    switch (resourceType)
    {
      case ResourceType.Plutonium:
        this.resourceIcon.sprite = this.SpriteLinker.AsteroidResourcePlutonium;
        break;
      case ResourceType.Water:
        this.resourceIcon.sprite = this.SpriteLinker.AsteroidResourceWater;
        break;
      case ResourceType.Uranium:
        this.resourceIcon.sprite = this.SpriteLinker.AsteroidResourceUranium;
        break;
      case ResourceType.Titanium:
        this.resourceIcon.sprite = this.SpriteLinker.AsteroidResourceTitanium;
        break;
      case ResourceType.Tylium:
        this.resourceIcon.sprite = this.SpriteLinker.AsteroidResourceTylium;
        break;
      default:
        Debug.LogWarning((object) ("No icon for unknown resourceType: " + (object) resourceType));
        break;
    }
    this.UpdateView();
  }
}
