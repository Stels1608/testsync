﻿// Decompiled with JetBrains decompiler
// Type: StarterKitCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class StarterKitCard : Card
{
  public List<ShipItem> Items = new List<ShipItem>();
  public GUICard GUICard;
  public ShipCard shipCard;
  public Texture2D icon;

  public StarterKitCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.shipCard = (ShipCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.Ship);
    this.Items = ItemFactory.ReadList<ShipItem>(r);
    this.GUICard = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
    this.icon = Resources.Load(this.GUICard.GUIIcon) as Texture2D;
    this.IsLoaded.Depend<ShipItem>((IEnumerable<ShipItem>) this.Items);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.GUICard
    });
  }
}
