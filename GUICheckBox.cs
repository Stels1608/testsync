﻿// Decompiled with JetBrains decompiler
// Type: GUICheckBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

public class GUICheckBox : GUIButtonNew
{
  public GUICheckBox()
  {
  }

  public GUICheckBox(string text)
    : base(text)
  {
  }

  public GUICheckBox(string normal, string over, string pressed)
    : base(normal, over, pressed)
  {
  }

  public GUICheckBox(string text, string normal, string over, string pressed)
    : base(text, normal, over, pressed)
  {
  }

  public GUICheckBox(JButton button)
    : base(button)
  {
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    this.wasMouseDown = false;
    if (mouseKey != KeyCode.Mouse0 || !this.Contains(mousePosition))
      return false;
    this.wasMouseDown = true;
    return true;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    this.MouseOver = this.Contains(mousePosition);
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse0 && this.wasMouseDown)
    {
      this.wasMouseDown = false;
      if (this.Contains(mousePosition))
      {
        this.isPressed = !this.isPressed;
        if ((Object) this.PressSound != (Object) null)
          GUISound.Instance.PlayOnce(this.PressSound);
        if (this.Handler != null)
          this.Handler();
        return true;
      }
    }
    return false;
  }
}
