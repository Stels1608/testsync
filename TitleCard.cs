﻿// Decompiled with JetBrains decompiler
// Type: TitleCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class TitleCard : Card
{
  public byte Level;
  public ObjectStats StaticBuff;
  public ObjectStats MultiplyBuff;
  public GUICard GUI;

  public string Name
  {
    get
    {
      return BsgoLocalization.Get("%$bgo." + this.GUI.Key + ".Title_" + (object) this.Level + "%");
    }
  }

  public TitleCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.Level = r.ReadByte();
    r.ReadString();
    this.StaticBuff = r.ReadDesc<ObjectStats>();
    this.MultiplyBuff = r.ReadDesc<ObjectStats>();
    this.GUI = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.GUI
    });
    this.IsLoaded.Set();
  }
}
