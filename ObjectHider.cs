﻿// Decompiled with JetBrains decompiler
// Type: ObjectHider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ObjectHider : MonoBehaviour
{
  private Vector3 scaleBackupped;

  private void Awake()
  {
    this.scaleBackupped = this.transform.localScale;
  }

  private void LateUpdate()
  {
    if (this.transform.localScale != Vector3.zero)
      this.scaleBackupped = this.transform.localScale;
    this.Hide();
  }

  private void OnEnable()
  {
    this.Hide();
  }

  private void OnDisable()
  {
    this.Unhide();
  }

  private void OnDestroy()
  {
    this.Unhide();
  }

  private void Hide()
  {
    this.transform.localScale = Vector3.zero;
  }

  private void Unhide()
  {
    this.transform.localScale = this.scaleBackupped;
  }
}
