﻿// Decompiled with JetBrains decompiler
// Type: StarterLevelProfile
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;

public class StarterLevelProfile : LevelProfile
{
  private readonly RewardCard colonialBonus;
  private readonly RewardCard cylonBonus;

  public string SceneName
  {
    get
    {
      return "DebugLevel";
    }
  }

  public bool Ready
  {
    get
    {
      if (this.colonialBonus != null && !(bool) this.colonialBonus.IsLoaded)
        return false;
      if (this.cylonBonus != null)
        return (bool) this.cylonBonus.IsLoaded;
      return true;
    }
  }

  public IEnumerable<string> RequiredAssets
  {
    get
    {
      return Enumerable.Empty<string>();
    }
  }

  public RewardCard ColonialBonus
  {
    get
    {
      return this.colonialBonus;
    }
  }

  public RewardCard CylonBonus
  {
    get
    {
      return this.cylonBonus;
    }
  }

  public StarterLevelProfile(uint colonialBonusGuid, uint cylonBonusGuid)
  {
    if ((int) colonialBonusGuid != 0)
      this.colonialBonus = (RewardCard) Game.Catalogue.FetchCard(colonialBonusGuid, CardView.Reward);
    if ((int) cylonBonusGuid == 0)
      return;
    this.cylonBonus = (RewardCard) Game.Catalogue.FetchCard(cylonBonusGuid, CardView.Reward);
  }
}
