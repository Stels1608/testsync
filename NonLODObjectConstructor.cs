﻿// Decompiled with JetBrains decompiler
// Type: NonLODObjectConstructor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class NonLODObjectConstructor : Constructor
{
  public NonLODObjectConstructor(GameObject root)
    : base(root)
  {
  }

  public override void Construct()
  {
    this.root.AddComponent<BuildSystem>().PrefabName = this.GetPrefabName();
  }

  protected abstract string GetPrefabName();
}
