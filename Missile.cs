﻿// Decompiled with JetBrains decompiler
// Type: Missile
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Text;
using UnityEngine;

public class Missile : SpaceObject
{
  private static readonly StringBuilder stringBuilder = new StringBuilder();
  public MissileCard MissileCard;
  public byte Tier;
  public float EffectRadius;
  private uint ownerId;

  public override bool IsHostileCongener
  {
    get
    {
      if (!this.IsMinion)
        return base.IsHostileCongener;
      return false;
    }
  }

  public override bool IsMinion
  {
    get
    {
      PlayerShip playerShip = SpaceLevel.GetLevel().PlayerShip;
      if (playerShip != null)
        return (int) this.ownerId == (int) playerShip.ObjectID;
      return false;
    }
  }

  public override string PrefabName
  {
    get
    {
      Missile.stringBuilder.Length = 0;
      switch (this.Faction)
      {
        case Faction.Colonial:
          Missile.stringBuilder.Append("colonial");
          break;
        case Faction.Cylon:
          Missile.stringBuilder.Append("cylon");
          break;
        case Faction.Ancient:
          Missile.stringBuilder.Append("ancient");
          break;
        default:
          return (string) null;
      }
      switch (this.Tier)
      {
        case 1:
          Missile.stringBuilder.Append("small");
          break;
        case 2:
          Missile.stringBuilder.Append("medium");
          break;
        case 3:
          Missile.stringBuilder.Append("large");
          break;
        case 4:
          Missile.stringBuilder.Append("xlarge");
          break;
        default:
          return (string) null;
      }
      switch (this.MissileCard.MissileType)
      {
        case MissileType.Normal:
          Missile.stringBuilder.Append("missile");
          break;
        case MissileType.Nuke:
          Missile.stringBuilder.Append("missile");
          break;
        case MissileType.Torpedo:
          Missile.stringBuilder.Append("torpedo");
          break;
        default:
          return (string) null;
      }
      return Missile.stringBuilder.ToString();
    }
  }

  public override UnityEngine.Sprite IconBrackets
  {
    get
    {
      return (UnityEngine.Sprite) null;
    }
  }

  public Missile(uint objectID)
    : base(objectID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.ownerId = r.ReadUInt32();
    uint objectID = r.ReadUInt32();
    this.Tier = r.ReadByte();
    ushort objectPointHash = r.ReadUInt16();
    this.EffectRadius = r.ReadSingle();
    Ship ship1 = SpaceLevel.GetLevel().GetObjectRegistry().Get(this.ownerId) as Ship;
    Ship ship2 = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID) as Ship;
    if (ship2 != null && ship2.IsMe)
      Game.ChatStorage.BeWareCombatLog("Detected missile launch, by " + (ship1 != null ? ship1.Name : "N/A"));
    if (ship1 != null)
    {
      Weaponry modelScript = ship1.GetModelScript<Weaponry>();
      if ((Object) modelScript != (Object) null)
        modelScript.FireMissile(objectPointHash);
    }
    this.MissileCard = (MissileCard) Game.Catalogue.FetchCard(this.objectGUID, CardView.Missile);
    this.AreCardsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.MissileCard
    });
    if (ship2 == null || !ship2.IsMe)
      return;
    this.MissileCard.IsLoaded.AddHandler((SignalHandler) (() => SpaceLevel.GetLevel().IncomingMissiles.Add(this)));
  }

  protected override IMovementController CreateMovementController()
  {
    return (IMovementController) new ManeuverController((SpaceObject) this, (MovementCard) Game.Catalogue.FetchCard(this.WorldCard.CardGUID, CardView.Movement));
  }
}
