﻿// Decompiled with JetBrains decompiler
// Type: OneButtonBar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class OneButtonBar : ButtonBar
{
  private string text = "%$bgo.common.close%";
  [SerializeField]
  private ButtonWidget button;

  protected override void Start()
  {
    this.button.TextLabel.text = Tools.ParseMessage(this.text);
    if (!((Object) this.button.BgSprite != (Object) null))
      return;
    this.button.BgSprite.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
  }

  public void InjectDifferentLoca(string newText)
  {
    this.text = newText;
  }

  public void SetButtonPositionX(float x)
  {
    this.button.transform.localPosition = new Vector3(x, this.button.transform.localPosition.y);
  }

  public void SetButtonDelegate(AnonymousDelegate buttonDelegate, Message message)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    OneButtonBar.\u003CSetButtonDelegate\u003Ec__AnonStoreyC9 delegateCAnonStoreyC9 = new OneButtonBar.\u003CSetButtonDelegate\u003Ec__AnonStoreyC9();
    // ISSUE: reference to a compiler-generated field
    delegateCAnonStoreyC9.message = message;
    this.button.handleClick = buttonDelegate;
    // ISSUE: reference to a compiler-generated field
    if (delegateCAnonStoreyC9.message == Message.None)
      return;
    // ISSUE: reference to a compiler-generated method
    this.button.handleClick += new AnonymousDelegate(delegateCAnonStoreyC9.\u003C\u003Em__1CF);
  }
}
