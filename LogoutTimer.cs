﻿// Decompiled with JetBrains decompiler
// Type: LogoutTimer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class LogoutTimer : GuiPanel
{
  private float delay = 10f;
  private const float UPDATE_INTERVAL = 0.1f;
  private GuiLabel text;
  private GuiLabel timeLeft;
  private float timeToClose;
  private float updateInterval;

  public float Delay
  {
    get
    {
      return this.delay;
    }
    set
    {
      this.delay = value;
    }
  }

  public LogoutTimer()
  {
    Loaders.Load((GuiPanel) this, "GUI/Options/logout_timer_layout");
    this.text = this.Find<GuiLabel>("text");
    this.timeLeft = this.Find<GuiLabel>("timeLeft");
    this.Find<GuiButton>("cancel").Pressed = (AnonymousDelegate) (() =>
    {
      SceneProtocol.GetProtocol().RequestStopDisconnect();
      this.Close();
    });
    this.Align = Align.UpCenter;
    this.Position = new Vector2(0.0f, 125f);
    this.IsRendered = false;
  }

  public void Show()
  {
    this.timeToClose = this.Delay;
    this.IsRendered = true;
    Game.RegisterDialog((IGUIPanel) this, true);
  }

  public override void Update()
  {
    this.timeToClose -= Time.deltaTime;
    this.updateInterval -= Time.deltaTime;
    if ((double) this.updateInterval <= 0.0)
    {
      if ((double) this.timeToClose <= 1.0 && this.timeLeft.Text != string.Empty)
      {
        this.text.Text = "%$bgo.option_buttons.confirmed_logout%";
        this.text.PositionX -= 20f;
        this.text.SizeX += 20f;
        this.timeLeft.Text = string.Empty;
      }
      else if ((double) this.timeToClose > 1.0)
        this.timeLeft.Text = ((int) this.timeToClose).ToString() + " %$bgo.option_buttons.seconds%";
      this.updateInterval = 0.1f;
    }
    if ((double) this.timeToClose >= 0.0)
      return;
    this.Close();
  }

  private void Close()
  {
    Game.UnregisterDialog((IGUIPanel) this);
    this.IsRendered = false;
  }
}
