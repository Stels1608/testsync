﻿// Decompiled with JetBrains decompiler
// Type: MagneticStorm
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class MagneticStorm : ClientBuffIcon
{
  public const int STEALTH_THREAT_THRESHOLD = 12;
  private static ShipBuff magneticStormBuff;

  private static ShipBuff MagneticStormBuff
  {
    get
    {
      if (MagneticStorm.magneticStormBuff == null)
      {
        MagneticStorm.magneticStormBuff = new ShipBuff();
        MagneticStorm.magneticStormBuff.ServerID = MagneticStorm.magneticStormBuff.AbilityGuid = 226742606U;
        MagneticStorm.magneticStormBuff.GuiCard = (GUICard) Game.Catalogue.FetchCard(226742606U, CardView.GUI);
        MagneticStorm.magneticStormBuff.MaxTime = float.MaxValue;
        MagneticStorm.magneticStormBuff.EndTime = float.MaxValue;
        MagneticStorm.magneticStormBuff.IsLoaded.Depend(new ILoadable[1]
        {
          (ILoadable) MagneticStorm.magneticStormBuff.GuiCard
        });
        MagneticStorm.magneticStormBuff.IsLoaded.Set();
      }
      return MagneticStorm.magneticStormBuff;
    }
  }

  public override ShipBuff ClientBuff
  {
    get
    {
      return MagneticStorm.MagneticStormBuff;
    }
  }

  public override bool Applies(SpaceObject spaceObject)
  {
    Ship ship = spaceObject as Ship;
    if (ship == null || !ship.ShipCardLight.HasRole(ShipRole.Stealth))
      return false;
    SectorDesc star = Game.Galaxy.GetStar(SpaceLevel.GetLevel().ServerID);
    if (star == null)
      return false;
    switch (ship.Faction)
    {
      case Faction.Colonial:
        return star.ColonialThreatLevel < 12;
      case Faction.Cylon:
        return star.CylonThreatLevel < 12;
      default:
        return false;
    }
  }
}
