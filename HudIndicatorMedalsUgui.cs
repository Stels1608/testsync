﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorMedalsUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class HudIndicatorMedalsUgui : HudIndicatorBaseComponentUgui, IHudIndicatorMedal, IReceivesSpaceObject
{
  private SpaceObject spaceObject;
  private HudIndicatorsSpriteLinker spriteLinker;

  protected override void ResetComponentStates()
  {
    this.spaceObject = (SpaceObject) null;
  }

  protected override void Awake()
  {
    base.Awake();
    this.spriteLinker = HudIndicatorsSpriteLinker.Instance;
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    this.gameObject.SetActive((this.InScannerRange || this.IsSelected) && this.InScreen);
  }

  public void OnSpaceObjectInjection(SpaceObject spaceObject)
  {
    this.spaceObject = spaceObject;
    this.UpdateMedals();
  }

  public void UpdateMedals()
  {
    PlayerShip playerShip = this.spaceObject as PlayerShip;
    if (playerShip == null)
      return;
    Player player = playerShip.Player;
    while (this.transform.childCount > 0)
      Object.DestroyImmediate((Object) this.transform.GetChild(0).gameObject);
    if (player.PvpMedal != PvpMedal.None && !Game.Me.TournamentParticipant)
      this.AddAward(this.spriteLinker.GetPvpMedal(player.PvpMedal, player.Faction));
    if (player.KillerMedal != KillerMedal.None)
      this.AddAward(this.spriteLinker.GetKillerMedal(player.KillerMedal));
    if (player.AssistMedal != AssistMedal.None)
      this.AddAward(this.spriteLinker.GetAssistMedal(player.AssistMedal));
    if (player.TournamentMedal != TournamentMedal.None)
      this.AddAward(this.spriteLinker.GetTournamentMedal(player.TournamentMedal));
    if (player.TournamentIndicator != TournamentIndicator.None)
      this.AddAward(this.spriteLinker.GetTournamentIndicator(player.TournamentIndicator));
    if (!player.Nemesis)
      return;
    this.AddAward(this.spriteLinker.GetTournamentNemesis);
  }

  private void AddAward(UnityEngine.Sprite medalSprite)
  {
    if ((Object) medalSprite == (Object) null)
      return;
    new GameObject()
    {
      transform = {
        parent = this.gameObject.transform
      }
    }.AddComponent<Image>().sprite = medalSprite;
  }
}
