﻿// Decompiled with JetBrains decompiler
// Type: Mine
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class Mine : SpaceObject
{
  public MissileCard MissileCard;
  public byte Tier;
  private uint ownerId;

  public override bool IsHostileCongener
  {
    get
    {
      if (!this.IsMinion)
        return base.IsHostileCongener;
      return false;
    }
  }

  public override bool IsMinion
  {
    get
    {
      PlayerShip playerShip = SpaceLevel.GetLevel().PlayerShip;
      if (playerShip != null)
        return (int) this.ownerId == (int) playerShip.ObjectID;
      return false;
    }
  }

  public override UnityEngine.Sprite IconBrackets
  {
    get
    {
      return (UnityEngine.Sprite) null;
    }
  }

  public Mine(uint objectID)
    : base(objectID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Mine.\u003CRead\u003Ec__AnonStorey11B readCAnonStorey11B = new Mine.\u003CRead\u003Ec__AnonStorey11B();
    // ISSUE: reference to a compiler-generated field
    readCAnonStorey11B.\u003C\u003Ef__this = this;
    base.Read(r);
    this.ownerId = r.ReadUInt32();
    this.Tier = r.ReadByte();
    // ISSUE: reference to a compiler-generated field
    readCAnonStorey11B.timeWhenArmed = r.ReadDateTime();
    // ISSUE: reference to a compiler-generated method
    this.IsConstructed.AddHandler(new SignalHandler(readCAnonStorey11B.\u003C\u003Em__2AE));
    this.MissileCard = (MissileCard) Game.Catalogue.FetchCard(this.objectGUID, CardView.Missile);
    this.AreCardsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.MissileCard
    });
  }

  public override void OnDestroyed()
  {
    Mine.MineExplosion(this);
  }

  protected override IMovementController CreateMovementController()
  {
    return (IMovementController) new ManeuverController((SpaceObject) this, (MovementCard) Game.Catalogue.FetchCard(this.WorldCard.CardGUID, CardView.Movement));
  }

  public static void MineExplosion(Mine mine)
  {
    FXManager.Instance.PutEffect((EffectState) new ExplosionEffectState(mine.Position, mine.Rotation, (ExplosionArgs) new MissileExplosionArgs()
    {
      ExplosionView = (mine.MissileCard == null ? MissileExplosionArgs.MissileExplosionView.Standard : mine.MissileCard.ExplosionView),
      ExplosionTier = (int) mine.Tier
    }, 0.0f));
  }
}
