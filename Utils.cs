﻿// Decompiled with JetBrains decompiler
// Type: Utils
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
  private const double BYTE_PARTION_CONSTANT = 0.0640000030398369;

  public static K GetKeyByValue<K, V>(this IDictionary<K, V> dictionary, object v)
  {
    foreach (K key in (IEnumerable<K>) dictionary.Keys)
    {
      object objB = (object) dictionary[key];
      if (object.Equals(v, objB))
        return key;
    }
    return default (K);
  }

  public static bool ArraysEqual<T>(T[] a1, T[] a2)
  {
    if (object.ReferenceEquals((object) a1, (object) a2))
      return true;
    if (a1 == null || a2 == null || a1.Length != a2.Length)
      return false;
    EqualityComparer<T> @default = EqualityComparer<T>.Default;
    for (int index = 0; index < a1.Length; ++index)
    {
      if (!@default.Equals(a1[index], a2[index]))
        return false;
    }
    return true;
  }

  public static string ConvertTimestampToDate(long date)
  {
    DateTime dateTime = new DateTime(1970, 1, 1);
    dateTime = dateTime.ToUniversalTime();
    dateTime = dateTime.AddMilliseconds((double) date);
    return dateTime.ToString("dd.MM.yy");
  }

  public static string GetDate()
  {
    return DateTime.Now.ToString("[dd/M/yyyy] ");
  }

  public static string GetTime()
  {
    return DateTime.Now.ToString("[HH:mm:ss] ");
  }

  public static Dictionary<TKey, TValue> CloneDictionaryCloningValues<TKey, TValue>(Dictionary<TKey, TValue> original) where TValue : ICloneable
  {
    Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>(original.Count, original.Comparer);
    using (Dictionary<TKey, TValue>.Enumerator enumerator = original.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<TKey, TValue> current = enumerator.Current;
        dictionary.Add(current.Key, (TValue) current.Value.Clone());
      }
    }
    return dictionary;
  }

  public static List<T> CloneListCloningValues<T>(List<T> original) where T : ICloneable
  {
    List<T> objList = new List<T>(original.Count);
    using (List<T>.Enumerator enumerator = original.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        T current = enumerator.Current;
        objList.Add((T) current.Clone());
      }
    }
    return objList;
  }

  public static bool IsNaN(Quaternion q)
  {
    if (!float.IsNaN(q.w) && !float.IsNaN(q.x) && !float.IsNaN(q.y))
      return float.IsNaN(q.z);
    return true;
  }

  public static bool IsNaN(Vector3 v)
  {
    if (!float.IsNaN(v.x) && !float.IsNaN(v.y))
      return float.IsNaN(v.z);
    return true;
  }

  public static byte[] GetPositionAsApproximatedBytes(Vector3 position)
  {
    byte[] numArray = new byte[2];
    int num1 = (int) Mathf.Round((float) (((double) position.x - 2000.0) * 0.0640000030398369));
    int num2 = (int) Mathf.Round((float) (((double) position.z - 2000.0) * 0.0640000030398369));
    numArray[0] = (byte) Mathf.Clamp((float) num1, 0.0f, (float) byte.MaxValue);
    numArray[1] = (byte) Mathf.Clamp((float) num2, 0.0f, (float) byte.MaxValue);
    return numArray;
  }

  public static Vector3 GetApproximatedBytesAsPosition(sbyte[] appPosition)
  {
    Vector3 zero = Vector3.zero;
    zero.x = Mathf.Round((float) ((double) appPosition[0] / 0.0640000030398369 + 2000.0));
    zero.z = Mathf.Round((float) ((double) appPosition[1] / 0.0640000030398369 + 2000.0));
    return zero;
  }

  public static T AddComponentIfNotExists<T>(this GameObject gameObject) where T : Component
  {
    T obj = gameObject.GetComponent<T>();
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      obj = gameObject.AddComponent<T>();
    return obj;
  }

  public static void SetVisible(this GameObject gameObject, bool visible)
  {
    foreach (Renderer componentsInChild in gameObject.GetComponentsInChildren<Renderer>())
      componentsInChild.enabled = visible;
  }

  public static float Distance2D(Vector3 pos1, Vector3 pos2)
  {
    Vector3 vector3 = pos2 - pos1;
    vector3.y = 0.0f;
    return vector3.magnitude;
  }

  public static int UnsignedShortToInt(ushort x, ushort y)
  {
    return (0 | (int) x & (int) ushort.MaxValue) << 16 | (int) y & (int) ushort.MaxValue;
  }

  public static string FormatTime(int time, bool show24h = false)
  {
    if (time < 60)
      return time.ToString() + "s";
    if (time < 3600)
      return (time / 60).ToString() + "m";
    if (show24h && time <= 86400)
      return (time / 3600).ToString() + "h";
    if (time < 86400)
      return (time / 3600).ToString() + "h";
    return (time / 86400).ToString() + "d";
  }

  public static string FormatTime2(int remainingTime)
  {
    if (remainingTime <= 0)
      return "0";
    TimeSpan timeSpan = TimeSpan.FromSeconds((double) remainingTime);
    if (timeSpan.Days > 0)
      return string.Format("{0:D2}d {1:D2}h {2:D2}m", (object) timeSpan.Days, (object) timeSpan.Hours, (object) timeSpan.Minutes);
    if (timeSpan.Hours > 0)
      return string.Format("{0:D2}h {1:D2}m", (object) timeSpan.Hours, (object) timeSpan.Minutes);
    if (timeSpan.Minutes > 0)
      return string.Format("{0:D2}m", (object) timeSpan.Minutes);
    return string.Format("{0:D2}s", (object) timeSpan.Seconds);
  }

  public static string FormatTime3(int remainingTime)
  {
    if (remainingTime <= 0)
      return "0";
    TimeSpan timeSpan = TimeSpan.FromSeconds((double) remainingTime);
    return string.Format("{0:D2}:{1:D2}:{2:D2}", (object) timeSpan.Hours, (object) timeSpan.Minutes, (object) timeSpan.Seconds);
  }

  public static Transform FindChildWithString(Transform parent, string name)
  {
    Queue<Transform> transformQueue = new Queue<Transform>();
    transformQueue.Enqueue(parent);
    while (transformQueue.Count > 0)
    {
      Transform transform = transformQueue.Dequeue();
      if (transform.name.Contains(name))
        return transform;
      for (int index = 0; index < transform.childCount; ++index)
        transformQueue.Enqueue(transform.GetChild(index));
    }
    return (Transform) null;
  }

  public static void SetLayerRecursively(this GameObject gameObject, int layer)
  {
    foreach (Component componentsInChild in gameObject.GetComponentsInChildren<Transform>(true))
      componentsInChild.gameObject.layer = layer;
  }

  public static bool ShiftPressed()
  {
    if (!Input.GetKey(KeyCode.LeftShift))
      return Input.GetKey(KeyCode.RightShift);
    return true;
  }

  public static void CalculateNormalizedVectorAndMagnitude(Vector3 inputVector, out float magnitude, out Vector3 normalizedVector)
  {
    magnitude = Vector3.Magnitude(inputVector);
    if ((double) magnitude > 9.99999974737875E-06)
      normalizedVector = inputVector / magnitude;
    else
      normalizedVector = Vector3.zero;
  }

  public static string GetFilenameFromPath(string path)
  {
    string[] strArray = path.Split('/');
    return strArray[strArray.Length - 1];
  }

  public static Vector3 ClampVector(Vector3 inputVector, float min, float max)
  {
    inputVector.x = Mathf.Clamp(inputVector.x, min, max);
    inputVector.y = Mathf.Clamp(inputVector.y, min, max);
    inputVector.z = Mathf.Clamp(inputVector.z, min, max);
    return inputVector;
  }
}
