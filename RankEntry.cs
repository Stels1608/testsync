﻿// Decompiled with JetBrains decompiler
// Type: RankEntry
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class RankEntry : IProtocolRead
{
  public uint PlayerId { get; private set; }

  public ushort Rank { get; private set; }

  public ushort Index { get; private set; }

  public string PlayerName { get; private set; }

  public Faction Faction { get; private set; }

  public uint Points { get; private set; }

  public uint Kills { get; private set; }

  public uint Deaths { get; private set; }

  public RankEntry()
  {
  }

  public RankEntry(ushort index, uint playerId, ushort rank, string name, Faction faction, uint points, uint kills, uint deaths)
  {
    this.Index = index;
    this.PlayerId = playerId;
    this.Rank = rank;
    this.PlayerName = name;
    this.Faction = faction;
    this.Points = points;
    this.Kills = kills;
    this.Deaths = deaths;
  }

  public void Read(BgoProtocolReader r)
  {
    this.Index = (ushort) r.ReadUInt32();
    this.PlayerId = r.ReadUInt32();
    this.Rank = (ushort) r.ReadUInt32();
    this.PlayerName = r.ReadString();
    int num1 = (int) r.ReadUInt32();
    this.Faction = (Faction) r.ReadByte();
    this.Points = r.ReadUInt32();
    this.Kills = r.ReadUInt32();
    this.Deaths = r.ReadUInt32();
    int num2 = (int) r.ReadUInt32();
  }

  public override string ToString()
  {
    return string.Format("PlayerId: {0}, Rank: {1}, Index: {2}, PlayerName: {3}, Faction: {4}, Points: {5}, Kills: {6}, Deaths: {7}", (object) this.PlayerId, (object) this.Rank, (object) this.Index, (object) this.PlayerName, (object) this.Faction, (object) this.Points, (object) this.Kills, (object) this.Deaths);
  }
}
