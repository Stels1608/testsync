﻿// Decompiled with JetBrains decompiler
// Type: CustomGUIElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class CustomGUIElement
{
  protected bool isRendered = true;
  protected SmartRect rect;
  protected bool isOver;

  public bool IsMouseOver
  {
    get
    {
      return this.isOver;
    }
    set
    {
      this.isOver = value;
    }
  }

  public bool IsRendered
  {
    get
    {
      return this.isRendered;
    }
    set
    {
      this.isRendered = value;
    }
  }

  public float2 GetPosition()
  {
    return this.rect.Position;
  }

  public virtual void SetPosition(float2 newValue)
  {
    this.rect.Position = newValue;
  }

  public void SetPosition(float x, float y)
  {
    this.SetPosition(new float2(x, y));
  }

  public Size GetSize()
  {
    return new Size((int) this.rect.Width, (int) this.rect.Height);
  }

  public void SetSize(Size size)
  {
    this.rect = new SmartRect(new Rect(0.0f, 0.0f, (float) size.width, (float) size.height), this.rect.Position, this.rect.Parent);
  }

  public SmartRect GetRect()
  {
    return this.rect;
  }

  public abstract void Draw();

  public virtual bool Contains(float2 point)
  {
    return this.rect.AbsRect.Contains(point.ToV2());
  }

  public virtual void SetIsMouseOver(bool newValue)
  {
    this.isOver = newValue;
  }

  public bool GetIsMouseOver()
  {
    return this.isOver;
  }

  public virtual void RecalculateAbsCoords()
  {
    this.rect.RecalculateAbsCoords();
  }
}
