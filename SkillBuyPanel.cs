﻿// Decompiled with JetBrains decompiler
// Type: SkillBuyPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Core;
using SkillBuyCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SkillBuyPanel : GuiPanel
{
  public static Color captionColor = ColorUtility.MakeColorFrom0To255((uint) byte.MaxValue, (uint) byte.MaxValue, (uint) byte.MaxValue);
  private AtlasCache atlasCache = new AtlasCache(new float2(47f, 40f));
  private GuiPanelVerticalScroll skillList;
  private bool isSelectSkill;
  private GuiButton reduceButton;
  private GuiButton instantButton;
  private GuiLabel caption;
  private GuiLabel costValue;
  private GuiLabel timeValue;
  private GuiLabel costLabel;
  private GuiLabel timeLabel;

  public bool DescGroupRendered
  {
    get
    {
      return this.isSelectSkill;
    }
    set
    {
      GuiLabel guiLabel = this.caption;
      bool flag1 = value;
      this.costLabel.IsRendered = flag1;
      bool flag2 = flag1;
      this.timeLabel.IsRendered = flag2;
      bool flag3 = flag2;
      this.timeValue.IsRendered = flag3;
      bool flag4 = flag3;
      this.costValue.IsRendered = flag4;
      bool flag5 = flag4;
      this.Find<GuiLabel>("description").IsRendered = flag5;
      bool flag6 = flag5;
      this.Find<GuiLabel>("level").IsRendered = flag6;
      int num = flag6 ? 1 : 0;
      guiLabel.IsRendered = num != 0;
      this.Find<GuiLabel>("noHave").IsRendered = !value;
      this.isSelectSkill = value;
    }
  }

  public SkillBuyPanel()
  {
    Loaders.Load((GuiPanel) this, "GUI/CharacterStatusWindow/skills/layout");
    this.Size = this.Find<GuiImage>("background").Size;
    GuiImage guiImage = this.Find<GuiImage>("ScrollLayout");
    this.skillList = new GuiPanelVerticalScroll();
    this.skillList.Position = guiImage.Position;
    this.skillList.Size = guiImage.Size;
    this.skillList.RenderDelimiters = true;
    this.costValue = this.Find<GuiLabel>("costValue");
    this.timeValue = this.Find<GuiLabel>("timeValue");
    this.costLabel = this.Find<GuiLabel>("costLabel");
    this.timeLabel = this.Find<GuiLabel>("timeLabel");
    this.caption = this.Find<GuiLabel>("caption");
    this.updateSkillList();
    this.AddChild((GuiElementBase) this.skillList);
    this.RemoveChild((GuiElementBase) guiImage);
    this.DescGroupRendered = this.isSelectSkill;
    this.reduceButton = this.Find<GuiButton>("reducetime");
    this.reduceButton.Text = Tools.ParseMessage("%$bgo.instant_skill.reducetime%", (object) 10);
    this.reduceButton.IsRendered = false;
    this.reduceButton.OnClick = (AnonymousDelegate) (() => this.CheckAvaibleCubits(new PlayerSkill()));
    this.instantButton = this.Find<GuiButton>("instant");
    this.instantButton.Text = "%$bgo.instant_skill.complete%";
    this.instantButton.IsRendered = false;
    this.instantButton.OnClick = (AnonymousDelegate) (() => this.CheckAvaibleCubits((PlayerSkill) null));
  }

  private void CheckAvaibleCubits(PlayerSkill skill)
  {
    float num = 10f;
    if (skill == null)
      num = 1f;
    if (Game.Me.Hold.Cubits < (uint) (Math.Ceiling(TimeSpan.FromSeconds((double) Game.Me.SkillBook.TrainingEndTime - (double) Time.time).TotalMinutes / (double) num) * 60.0))
      FacadeFactory.GetInstance().SendMessage(Message.ShopResourcesMissing, (object) new List<ResourceType>()
      {
        ResourceType.Cubits
      });
    else if (skill != null)
      GuiDialogPopupManager.ShowDialog((GuiPanel) new GuiSkillBuyDialog(skill), true);
    else
      GuiDialogPopupManager.ShowDialog((GuiPanel) new GuiSkillBuyDialog(), true);
  }

  private void updateDesc(PlayerSkill skill)
  {
    if (!this.DescGroupRendered)
    {
      GuiLabel guiLabel = this.costLabel;
      bool flag1 = false;
      this.costValue.IsRendered = flag1;
      bool flag2 = flag1;
      this.timeLabel.IsRendered = flag2;
      bool flag3 = flag2;
      this.timeValue.IsRendered = flag3;
      int num = flag3 ? 1 : 0;
      guiLabel.IsRendered = num != 0;
      this.Find<GuiLabel>("noHave").IsRendered = true;
    }
    if (skill == null)
      return;
    if (!this.DescGroupRendered)
      this.DescGroupRendered = true;
    GuiLabel guiLabel1 = this.Find<GuiLabel>("level");
    this.caption.Text = skill.Name;
    this.caption.SizeY = (float) TextUtility.CalcTextSize(this.caption.Text, this.caption.Font, this.caption.SizeX).height;
    guiLabel1.Text = "%$bgo.etc.skills_level%" + (skill.IsMaxLevel ? skill.Level.ToString() : skill.NextLevel.ToString());
    guiLabel1.PositionY = this.caption.PositionY + this.caption.SizeY;
    this.Find<GuiLabel>("description").Text = Tools.ParseMessage(skill.IsMaxLevel ? skill.Card.GUICard.Description : skill.Description);
    if (skill.IsAvailable)
    {
      GuiLabel guiLabel2 = this.costValue;
      bool flag1 = true;
      this.timeValue.IsRendered = flag1;
      bool flag2 = flag1;
      this.costLabel.IsRendered = flag2;
      bool flag3 = flag2;
      this.timeLabel.IsRendered = flag3;
      int num1 = flag3 ? 1 : 0;
      guiLabel2.IsRendered = num1 != 0;
      this.costValue.Text = skill.Cost.ToString() + " %$bgo.etc.skills_exp%";
      if (skill.TrainingTime > 60)
      {
        this.timeValue.Text = Log.GetFormatted((float) (skill.TrainingTime / 60)) + " %$bgo.etc.skills_hours%";
        int num2 = skill.TrainingTime - skill.TrainingTime / 60 * 60;
        if (num2 <= 0)
          return;
        GuiLabel guiLabel3 = this.timeValue;
        string str = guiLabel3.Text + " " + (object) num2 + "%$bgo.etc.skills_mins%";
        guiLabel3.Text = str;
      }
      else
        this.timeValue.Text = skill.TrainingTime.ToString() + " %$bgo.etc.skills_mins%";
    }
    else
    {
      GuiLabel guiLabel2 = this.costValue;
      bool flag1 = false;
      this.timeValue.IsRendered = flag1;
      bool flag2 = flag1;
      this.costLabel.IsRendered = flag2;
      bool flag3 = flag2;
      this.timeLabel.IsRendered = flag3;
      int num = flag3 ? 1 : 0;
      guiLabel2.IsRendered = num != 0;
    }
  }

  private void updateSkillList()
  {
    try
    {
      if (Game.Me.SkillBook.Count <= 0 || this.skillList == null)
        return;
      this.skillList.EmptyChildren();
      for (byte index = 4; (int) index > 0; --index)
      {
        SkillGroup skillGroup = (SkillGroup) index;
        this.skillList.AddChild((GuiElementBase) new GroupDelimeter(this.GetSkillGroup(skillGroup)));
        for (int skillIndex = 0; skillIndex < Game.Me.SkillBook.Count; ++skillIndex)
        {
          if (Game.Me.SkillBook[skillIndex].Card.Group == skillGroup)
            this.skillList.AddChild((GuiElementBase) new Container(skillIndex, new System.Action<PlayerSkill>(this.updateDesc), this.atlasCache));
        }
      }
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ("Exception inside the new Version of the Skill List " + (object) ex));
    }
  }

  private string GetSkillGroup(SkillGroup skillGroup)
  {
    switch (skillGroup)
    {
      case SkillGroup.Computer:
        return BsgoLocalization.Get("%$bgo.etc.skillgroup_Computer%");
      case SkillGroup.Engine:
        return BsgoLocalization.Get("%$bgo.etc.skillgroup_Engine%");
      case SkillGroup.Hull:
        return BsgoLocalization.Get("%$bgo.etc.skillgroup_Hull%");
      case SkillGroup.Weapon:
        return BsgoLocalization.Get("%$bgo.etc.skillgroup_Weapon%");
      default:
        return string.Empty;
    }
  }

  public override void OnShow()
  {
    base.OnShow();
    if (Game.Me.SkillBook.CurrentTrainingSkill == null)
      ;
    this.updateDesc(Game.Me.SkillBook.CurrentTrainingSkill);
    IEnumerable<Container> containers = this.skillList.Queue.Where<GuiElementBase>((Func<GuiElementBase, bool>) (p =>
    {
      if (p is Container)
        return (int) ((Container) p).Skill.ServerID == (int) Game.Me.SkillBook.CurrentTrainingSkill.ServerID;
      return false;
    })).Select<GuiElementBase, Container>((Func<GuiElementBase, Container>) (p => p as Container));
    if (Game.Me.SkillBook.CurrentTrainingSkill != null)
    {
      foreach (Container container in containers)
      {
        if (this.skillList.Selected != container)
          this.skillList.Selected = (ISelectable) container;
      }
    }
    this.SelectActualLeraningSkill();
  }

  private void SelectActualLeraningSkill()
  {
    if (Game.Me.SkillBook.CurrentTrainingSkill != null)
    {
      GuiButton guiButton = this.reduceButton;
      bool flag = this.caption.Text == Game.Me.SkillBook.CurrentTrainingSkill.Card.GUICard.Name;
      this.instantButton.IsRendered = flag;
      int num = flag ? 1 : 0;
      guiButton.IsRendered = num != 0;
    }
    else
    {
      GuiButton guiButton = this.reduceButton;
      bool flag = false;
      this.instantButton.IsRendered = flag;
      int num = flag ? 1 : 0;
      guiButton.IsRendered = num != 0;
    }
  }

  public override void Update()
  {
    base.Update();
    this.SelectActualLeraningSkill();
    if (!Game.Me.SkillBook.IsUpdated)
      return;
    this.updateSkillList();
  }
}
