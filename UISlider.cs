﻿// Decompiled with JetBrains decompiler
// Type: UISlider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/Interaction/NGUI Slider")]
public class UISlider : UIProgressBar
{
  [SerializeField]
  [HideInInspector]
  private float rawValue = 1f;
  [SerializeField]
  [HideInInspector]
  private UISlider.Direction direction = UISlider.Direction.Upgraded;
  [SerializeField]
  [HideInInspector]
  private Transform foreground;
  [HideInInspector]
  [SerializeField]
  protected bool mInverted;

  [Obsolete("Use 'value' instead")]
  public float sliderValue
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  [Obsolete("Use 'fillDirection' instead")]
  public bool inverted
  {
    get
    {
      return this.isInverted;
    }
    set
    {
    }
  }

  protected override void Upgrade()
  {
    if (this.direction == UISlider.Direction.Upgraded)
      return;
    this.mValue = this.rawValue;
    if ((UnityEngine.Object) this.foreground != (UnityEngine.Object) null)
      this.mFG = this.foreground.GetComponent<UIWidget>();
    if (this.direction == UISlider.Direction.Horizontal)
      this.mFill = !this.mInverted ? UIProgressBar.FillDirection.LeftToRight : UIProgressBar.FillDirection.RightToLeft;
    else
      this.mFill = !this.mInverted ? UIProgressBar.FillDirection.BottomToTop : UIProgressBar.FillDirection.TopToBottom;
    this.direction = UISlider.Direction.Upgraded;
  }

  protected override void OnStart()
  {
    UIEventListener uiEventListener1 = UIEventListener.Get(!((UnityEngine.Object) this.mBG != (UnityEngine.Object) null) || !((UnityEngine.Object) this.mBG.GetComponent<Collider>() != (UnityEngine.Object) null) && !((UnityEngine.Object) this.mBG.GetComponent<Collider2D>() != (UnityEngine.Object) null) ? this.gameObject : this.mBG.gameObject);
    uiEventListener1.onPress += new UIEventListener.BoolDelegate(this.OnPressBackground);
    uiEventListener1.onDrag += new UIEventListener.VectorDelegate(this.OnDragBackground);
    if (!((UnityEngine.Object) this.thumb != (UnityEngine.Object) null) || !((UnityEngine.Object) this.thumb.GetComponent<Collider>() != (UnityEngine.Object) null) && !((UnityEngine.Object) this.thumb.GetComponent<Collider2D>() != (UnityEngine.Object) null) || !((UnityEngine.Object) this.mFG == (UnityEngine.Object) null) && !((UnityEngine.Object) this.thumb != (UnityEngine.Object) this.mFG.cachedTransform))
      return;
    UIEventListener uiEventListener2 = UIEventListener.Get(this.thumb.gameObject);
    uiEventListener2.onPress += new UIEventListener.BoolDelegate(this.OnPressForeground);
    uiEventListener2.onDrag += new UIEventListener.VectorDelegate(this.OnDragForeground);
  }

  protected void OnPressBackground(GameObject go, bool isPressed)
  {
    if (UICamera.currentScheme == UICamera.ControlScheme.Controller)
      return;
    this.mCam = UICamera.currentCamera;
    this.value = this.ScreenToValue(UICamera.lastTouchPosition);
    if (isPressed || this.onDragFinished == null)
      return;
    this.onDragFinished();
  }

  protected void OnDragBackground(GameObject go, Vector2 delta)
  {
    if (UICamera.currentScheme == UICamera.ControlScheme.Controller)
      return;
    this.mCam = UICamera.currentCamera;
    this.value = this.ScreenToValue(UICamera.lastTouchPosition);
  }

  protected void OnPressForeground(GameObject go, bool isPressed)
  {
    if (UICamera.currentScheme == UICamera.ControlScheme.Controller)
      return;
    if (isPressed)
    {
      this.mOffset = !((UnityEngine.Object) this.mFG == (UnityEngine.Object) null) ? this.value - this.ScreenToValue(UICamera.lastTouchPosition) : 0.0f;
    }
    else
    {
      if (this.onDragFinished == null)
        return;
      this.onDragFinished();
    }
  }

  protected void OnDragForeground(GameObject go, Vector2 delta)
  {
    if (UICamera.currentScheme == UICamera.ControlScheme.Controller)
      return;
    this.mCam = UICamera.currentCamera;
    this.value = this.mOffset + this.ScreenToValue(UICamera.lastTouchPosition);
  }

  protected void OnKey(KeyCode key)
  {
    if (!this.enabled)
      return;
    float num = (double) this.numberOfSteps <= 1.0 ? 0.125f : 1f / (float) (this.numberOfSteps - 1);
    if (this.fillDirection == UIProgressBar.FillDirection.LeftToRight || this.fillDirection == UIProgressBar.FillDirection.RightToLeft)
    {
      if (key == KeyCode.LeftArrow)
      {
        this.value = this.mValue - num;
      }
      else
      {
        if (key != KeyCode.RightArrow)
          return;
        this.value = this.mValue + num;
      }
    }
    else if (key == KeyCode.DownArrow)
    {
      this.value = this.mValue - num;
    }
    else
    {
      if (key != KeyCode.UpArrow)
        return;
      this.value = this.mValue + num;
    }
  }

  private enum Direction
  {
    Horizontal,
    Vertical,
    Upgraded,
  }
}
