﻿// Decompiled with JetBrains decompiler
// Type: WindowCloseButtonWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WindowCloseButtonWidget : ButtonWidget
{
  [SerializeField]
  private WindowWidget parentWindow;
  [SerializeField]
  private WindowTitleBarWidget title;

  public override void Start()
  {
    base.Start();
    this.title = this.transform.parent.GetComponent<WindowTitleBarWidget>();
    if ((Object) null != (Object) this.title)
      this.parentWindow = this.title.transform.parent.GetComponent<WindowWidget>();
    if (!((Object) this.BgSprite != (Object) null))
      return;
    this.BgSprite.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
  }

  public override void OnClick()
  {
    if ((Object) null != (Object) this.parentWindow)
      this.parentWindow.Close();
    if (this.handleClick != null)
      this.handleClick();
    this.OnHover(false);
  }
}
