﻿// Decompiled with JetBrains decompiler
// Type: MissileTrail
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class MissileTrail : SingleLODListener
{
  public int MaxPointCount = 150;
  public float LifeTime = 2f;
  public Color[] Colors = new Color[2]{ new Color(1f, 1f, 1f, 0.8f), new Color(1f, 1f, 1f, 0.0f) };
  public float Width = 1f;
  public float MaxAngle = 2f;
  public float MinVertexDistance = 0.1f;
  public float MaxVertexDistance = 1f;
  public Material Material;
  private float sqrMinDist;
  private float sqrMaxDist;
  private MissileTrail.Point[] points;
  private Vector3[] vertices;
  private Vector2[] uv;
  private Vector2[] uv2;
  private int[] triangles;
  private int pointCnt;
  private int lastPointCnt;
  private float lifeTimeRatio;
  private Transform _transform;
  private Material trailMaterial;
  private GameObject trailObj;
  private Mesh trailMesh;
  private FadingOut trailFadingOut;

  private void Start()
  {
    this._transform = this.transform;
    this.lifeTimeRatio = 1f / this.LifeTime;
    this.sqrMinDist = this.MinVertexDistance * this.MinVertexDistance;
    this.sqrMaxDist = this.MaxVertexDistance * this.MaxVertexDistance;
  }

  private void OnEnable()
  {
    this.trailObj = new GameObject("MissileTrail");
    this.trailFadingOut = this.trailObj.AddComponent<FadingOut>();
    this.trailMesh = this.trailObj.AddComponent<MeshFilter>().mesh;
    this.trailMaterial = new Material(this.Material);
    this.trailObj.AddComponent<MeshRenderer>().material = this.trailMaterial;
    int length = this.MaxPointCount + 1;
    this.points = new MissileTrail.Point[length];
    this.vertices = new Vector3[length * 2];
    this.uv = new Vector2[length * 2];
    this.uv2 = new Vector2[length * 2];
    this.triangles = new int[(length - 1) * 6];
    this.pointCnt = 0;
    this.lastPointCnt = 0;
  }

  private void OnDisable()
  {
    if ((UnityEngine.Object) this.trailFadingOut != (UnityEngine.Object) null)
      this.trailFadingOut.Execute(this.LifeTime);
    this.points = (MissileTrail.Point[]) null;
    this.vertices = (Vector3[]) null;
    this.uv = (Vector2[]) null;
    this.uv2 = (Vector2[]) null;
    this.triangles = (int[]) null;
  }

  protected override void OnSwitched(bool value)
  {
    this.enabled = value;
  }

  private void LateUpdate()
  {
    for (int index = this.pointCnt - 1; index >= 0 && (double) this.points[index].TimeAlive > (double) this.LifeTime; --index)
    {
      this.points[index] = (MissileTrail.Point) null;
      --this.pointCnt;
    }
    if (this.pointCnt == 0)
    {
      this.points[this.pointCnt++] = new MissileTrail.Point(this._transform);
      this.points[this.pointCnt++] = new MissileTrail.Point(this._transform);
    }
    else if (this.pointCnt == 1)
      this.InsertPoint();
    float sqrMagnitude = (this.points[1].Position - this._transform.position).sqrMagnitude;
    if ((double) sqrMagnitude > (double) this.sqrMinDist && this.pointCnt < this.MaxPointCount && ((double) sqrMagnitude > (double) this.sqrMaxDist || (double) Quaternion.Angle(this._transform.rotation, this.points[1].Rotation) > (double) this.MaxAngle))
      this.InsertPoint();
    else
      this.points[0].Update(this._transform);
    int num1 = 0;
    if (this.pointCnt == 2)
      num1 = 1;
    for (int index = 0; index <= num1; ++index)
    {
      Matrix4x4 matrix4x4 = Matrix4x4.TRS(this.points[index].Position, this.points[index].Rotation, Vector3.one);
      this.vertices[index * 2] = matrix4x4.MultiplyPoint3x4(new Vector3(this.Width * 0.5f, 0.0f, 0.0f));
      this.vertices[index * 2 + 1] = matrix4x4.MultiplyPoint3x4(new Vector3((float) (-(double) this.Width * 0.5), 0.0f, 0.0f));
    }
    for (int index1 = this.lastPointCnt; index1 < this.pointCnt; ++index1)
    {
      if (index1 != 0)
      {
        int index2 = (index1 - 1) * 6;
        int num2 = index1 * 2;
        this.triangles[index2] = num2 - 2;
        this.triangles[index2 + 1] = num2 - 1;
        this.triangles[index2 + 2] = num2;
        this.triangles[index2 + 3] = num2 + 1;
        this.triangles[index2 + 4] = num2;
        this.triangles[index2 + 5] = num2 - 1;
      }
    }
    for (int index = 0; index < this.pointCnt; ++index)
    {
      MissileTrail.Point point = this.points[index];
      float x1 = (float) index / (float) this.pointCnt;
      this.uv[index * 2] = new Vector2(x1, 0.0f);
      this.uv[index * 2 + 1] = new Vector2(x1, 1f);
      float x2 = point.TimeAlive * this.lifeTimeRatio;
      this.uv2[index * 2] = new Vector2(x2, 0.0f);
      this.uv2[index * 2 + 1] = new Vector2(x2, 1f);
    }
    for (int index1 = this.pointCnt; index1 < this.lastPointCnt; ++index1)
    {
      if (index1 > 0)
      {
        int index2 = (index1 - 1) * 6;
        this.triangles[index2] = 0;
        this.triangles[index2 + 1] = 0;
        this.triangles[index2 + 2] = 0;
        this.triangles[index2 + 3] = 0;
        this.triangles[index2 + 4] = 0;
        this.triangles[index2 + 5] = 0;
      }
    }
    this.trailMesh.Clear();
    this.trailMesh.vertices = this.vertices;
    this.trailMesh.uv = this.uv;
    this.trailMesh.uv2 = this.uv2;
    this.trailMesh.triangles = this.triangles;
    this.trailMaterial.mainTextureScale = new Vector2((float) this.pointCnt, 1f);
    this.lastPointCnt = this.pointCnt;
  }

  private void InsertPoint()
  {
    Array.Copy((Array) this.points, 0, (Array) this.points, 1, this.pointCnt);
    Array.Copy((Array) this.vertices, 0, (Array) this.vertices, 2, this.pointCnt * 2);
    this.points[0] = new MissileTrail.Point(this._transform);
    ++this.pointCnt;
  }

  private class Point
  {
    public Vector3 Position = Vector3.zero;
    public Quaternion Rotation = Quaternion.identity;
    private float timeCreated;

    public float TimeAlive
    {
      get
      {
        return Time.time - this.timeCreated;
      }
    }

    public Point(Transform transform)
    {
      this.Position = transform.position;
      this.Rotation = transform.rotation;
      this.timeCreated = Time.time;
    }

    public void Update(Transform transform)
    {
      this.Position = transform.position;
      this.Rotation = transform.rotation;
      this.timeCreated = Time.time;
    }
  }
}
