﻿// Decompiled with JetBrains decompiler
// Type: LootResourceNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;

public class LootResourceNotification : OnScreenNotification
{
  private readonly ItemCountable countable;
  private readonly Dictionary<LootBonusType, float> bonusAmounts;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.Loot;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Neutral;
    }
  }

  public override string TextMessage
  {
    get
    {
      return BsgoLocalization.Get("bgo.loot_message.resource", (object) this.countable.Count, (object) this.countable.ItemGUICard.Name);
    }
  }

  private string CombatLogMessage
  {
    get
    {
      string textMessage = this.TextMessage;
      if (this.bonusAmounts == null)
        return textMessage;
      string str1 = textMessage + " (";
      string str2 = string.Empty;
      using (Dictionary<LootBonusType, float>.Enumerator enumerator = this.bonusAmounts.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<LootBonusType, float> current = enumerator.Current;
          string str3 = BsgoLocalization.Get("bgo.loot_bonus." + current.Key.ToString().ToLowerInvariant());
          str1 = str1 + str2 + str3 + ": +" + (object) current.Value;
          str2 = ", ";
        }
      }
      return str1 + ")";
    }
  }

  public override bool Show
  {
    get
    {
      return true;
    }
  }

  public LootResourceNotification(ItemCountable countable, Dictionary<LootBonusType, float> bonusAmounts)
  {
    this.countable = countable;
    this.bonusAmounts = bonusAmounts;
    FacadeFactory.GetInstance().SendMessage(Message.CombatLogOk, (object) this.CombatLogMessage);
  }
}
