﻿// Decompiled with JetBrains decompiler
// Type: ProjectionInvariantSize
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ProjectionInvariantSize : MonoBehaviour
{
  public float Scale = 0.1f;
  private float cachedScale = 0.1f;
  private float cachedFov = 1f;
  private float cachedSizeFactor;
  private Vector3 cachedParentScale;
  private Vector3 cachedParentScaleInv;
  private Camera cachedCamera;
  private Transform cachedCameraTransform;
  private Transform myTransform;

  private void Start()
  {
    this.myTransform = this.transform;
  }

  private void LateUpdate()
  {
    if ((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null || SpaceLevel.GetLevel().cameraSwitcher == null || (UnityEngine.Object) this.myTransform == (UnityEngine.Object) null)
      return;
    Camera activeCamera = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera();
    if ((double) Math.Abs(this.Scale - this.cachedScale) > 1.0000000116861E-07)
      this.UpdateVariables();
    if ((UnityEngine.Object) this.cachedCamera != (UnityEngine.Object) activeCamera)
      this.UpdateVariables();
    Vector3 vector3 = this.cachedSizeFactor * Vector3.Distance(this.cachedCameraTransform.position, this.myTransform.position) * this.cachedParentScaleInv;
    if (!(this.myTransform.localScale != vector3))
      return;
    this.myTransform.localScale = vector3;
  }

  private void UpdateVariables()
  {
    this.cachedScale = this.Scale;
    this.cachedCamera = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera();
    this.cachedCameraTransform = this.cachedCamera.transform;
    this.cachedFov = (float) (3.14159274101257 / (180.0 / (double) this.cachedCamera.fieldOfView));
    this.cachedSizeFactor = (float) (2.0 * (double) Mathf.Tan(this.cachedFov / 2f) * (768.0 / (double) Screen.height)) * this.Scale;
    this.cachedParentScale = this.myTransform.parent.lossyScale;
    this.cachedParentScaleInv = !(this.cachedParentScale == Vector3.zero) ? new Vector3(1f / this.cachedParentScale.x, 1f / this.cachedParentScale.y, 1f / this.cachedParentScale.z) : Vector3.zero;
  }
}
