﻿// Decompiled with JetBrains decompiler
// Type: PlayerRegistry
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class PlayerRegistry
{
  private readonly Dictionary<uint, Player> cache = new Dictionary<uint, Player>();

  public Player this[uint ID]
  {
    get
    {
      return this.GetPlayer(ID);
    }
  }

  public Player GetPlayer(uint ID)
  {
    Player player = (Player) null;
    if (!this.cache.TryGetValue(ID, out player))
    {
      player = new Player(ID);
      this.cache.Add(ID, player);
    }
    return player;
  }

  public Player[] GetPlayers(IEnumerable<uint> IDs)
  {
    List<Player> playerList = new List<Player>();
    foreach (uint id in IDs)
      playerList.Add(this.GetPlayer(id));
    return playerList.ToArray();
  }

  public Player GetPlayer(string name)
  {
    name = name.Trim();
    if (name.Length == 0)
      return (Player) null;
    using (Dictionary<uint, Player>.ValueCollection.Enumerator enumerator = this.cache.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Player current = enumerator.Current;
        if (current.Name.ToLower() == name.ToLower())
          return current;
      }
    }
    return (Player) null;
  }

  public void _Clear()
  {
    this.cache.Clear();
  }
}
