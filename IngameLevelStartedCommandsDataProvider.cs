﻿// Decompiled with JetBrains decompiler
// Type: IngameLevelStartedCommandsDataProvider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Model;
using System.Collections.Generic;

public class IngameLevelStartedCommandsDataProvider : DataProvider<Message>
{
  private readonly List<IngameLevelStartedCommand> gameLevelLoadedCommands;

  public List<IngameLevelStartedCommand> GameLevelLoadedCommands
  {
    get
    {
      return this.gameLevelLoadedCommands;
    }
  }

  public IngameLevelStartedCommandsDataProvider()
    : base("GameLevelLoadedActionsDataProvider")
  {
    this.gameLevelLoadedCommands = new List<IngameLevelStartedCommand>();
  }
}
