﻿// Decompiled with JetBrains decompiler
// Type: RoomStateLoad
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Unity5AssetHandling;

public class RoomStateLoad : RoomState
{
  public AssetRequest ScenePrefab;
  public RoomStateLoad.dHandler HandlerLoaded;

  public RoomStateLoad(RoomLevel level)
  {
    this.ScenePrefab = AssetCatalogue.Instance.Request(level.WorldCard.PrefabName + ".prefab", false);
  }

  public override void Update()
  {
    if (!this.ScenePrefab.IsDone)
      return;
    TransScene.roomPrefabLoaded = true;
    if (this.HandlerLoaded != null)
      this.HandlerLoaded();
    base.Update();
  }

  public delegate void dHandler();
}
