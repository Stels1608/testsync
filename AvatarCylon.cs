﻿// Decompiled with JetBrains decompiler
// Type: AvatarCylon
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class AvatarCylon : Avatar
{
  public override bool CheckBadObjectName(string name)
  {
    return name.Contains("_eye");
  }

  public override bool AdditionalObject(string name, AvatarProperty property)
  {
    if (property.name == "head")
      return name == property.value + "_eye";
    return false;
  }

  protected override void OnAvatarLoaded()
  {
    base.OnAvatarLoaded();
    AvatarCylonCustomizer.Avatar = (Avatar) this;
  }
}
