﻿// Decompiled with JetBrains decompiler
// Type: SpaceCameraBehaviorStrikesChase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class SpaceCameraBehaviorStrikesChase : SpaceCameraBehaviorBase
{
  private float projectionSphereRadius = 1000f;
  private float camLerpCurrentFactor = 5f;
  private Vector3 wantedPositionOld = Vector3.zero;
  private Quaternion focusError = Quaternion.identity;
  private const float PROJECTION_SPHERE_RADIUS_DEFAULT = 1000f;
  private float backOffsetTargetFactor;
  private float backOffsetCurrentFactor;
  private float focusToTargetStartupTime;
  private ISpaceEntity currentTarget;

  public override bool BlocksControls
  {
    get
    {
      return false;
    }
  }

  public override bool ShowStrikesHud
  {
    get
    {
      return true;
    }
  }

  public SpaceCameraBehaviorStrikesChase()
  {
    this.UpdateTargetFocus();
    try
    {
      this.wantedPositionOld = this.CamPosition;
    }
    catch
    {
      this.wantedPositionOld = Vector3.zero;
    }
  }

  public void OnTargetChanged(ISpaceEntity newTarget)
  {
    this.currentTarget = newTarget;
    this.UpdateTargetFocus();
  }

  private void UpdateTargetFocus()
  {
    this.focusToTargetStartupTime = Time.time;
    PlayerShip playerShip = SpaceLevel.GetLevel().GetPlayerShip();
    if (playerShip == null)
    {
      this.focusError = Quaternion.identity;
    }
    else
    {
      Vector3 camFocusPoint;
      this.CalculateLookAtPoint(out camFocusPoint);
      this.focusError = Quaternion.Inverse(Quaternion.LookRotation(camFocusPoint - this.CamPosition, this.DetermineCameraUpVector(playerShip, Camera.main.transform.rotation))) * this.CamRotation;
    }
  }

  public override SpaceCameraParameters CalculateCurrentCameraParameters()
  {
    PlayerShip playerShip = SpaceLevel.GetLevel().GetPlayerShip();
    float num = this.ExactMovementUpdateDeltaTime();
    float currentSpeed = playerShip.MovementController.CurrentSpeed;
    this.backOffsetTargetFactor = (float) ((double) currentSpeed / 165.0 * 25.0);
    this.backOffsetCurrentFactor = Mathf.Lerp(this.backOffsetCurrentFactor, this.backOffsetTargetFactor, num);
    float b = (float) ((double) currentSpeed / 165.0 * 20.0);
    this.camLerpCurrentFactor = Mathf.Lerp(this.camLerpCurrentFactor, !playerShip.IsStrafing ? Mathf.Max(3f, b) : Mathf.Max(12f, b), num);
    Vector3 toNewPos = playerShip.Position + playerShip.Rotation * (Vector3.back * (SpaceCameraBase.WantedZoomDistance + this.backOffsetCurrentFactor) + 4.4f * Vector3.up);
    Vector3 vector3 = this.SuperSmoothLerp(this.CamPosition, this.wantedPositionOld, toNewPos, num, this.camLerpCurrentFactor);
    Vector3 camFocusPoint;
    this.CalculateLookAtPoint(out camFocusPoint);
    Quaternion quaternion1 = Quaternion.LookRotation(camFocusPoint - vector3, this.DetermineCameraUpVector(playerShip, Camera.main.transform.rotation));
    this.focusError = Quaternion.Lerp(this.focusError, Quaternion.identity, Mathf.Min((float) (0.5 * ((double) Time.time - (double) this.focusToTargetStartupTime)), 1f));
    Quaternion quaternion2 = quaternion1 * this.focusError;
    if ((double) (vector3 - toNewPos).sqrMagnitude > 1000000.0)
    {
      vector3 = toNewPos;
      quaternion2 = quaternion1;
    }
    SpaceCameraParameters cameraParameters = new SpaceCameraParameters() { Fov = this.Fov, CamPosition = vector3, CamRotation = quaternion2 };
    this.wantedPositionOld = toNewPos;
    return cameraParameters;
  }

  private Vector3 DetermineCameraUpVector(PlayerShip playerShip, Quaternion camRotation)
  {
    return Vector3.Lerp(camRotation * Vector3.up, playerShip.Rotation * Vector3.up, 4.5f * this.ExactMovementUpdateDeltaTime());
  }

  private Vector3 SuperSmoothLerp(Vector3 fromOldPos, Vector3 toOldPos, Vector3 toNewPos, float deltaTime, float lerpRate)
  {
    if ((double) Math.Abs(deltaTime) < 9.99999974737875E-05)
      return fromOldPos;
    Vector3 vector3 = fromOldPos - toOldPos + (toNewPos - toOldPos) / (lerpRate * deltaTime);
    return toNewPos - (toNewPos - toOldPos) / (lerpRate * deltaTime) + vector3 * Mathf.Exp(-lerpRate * deltaTime);
  }

  public bool CalculateLookAtPoint(out Vector3 camFocusPoint)
  {
    Vector3 position = this.PlayerShip.Position;
    Quaternion rotation = this.PlayerShip.Rotation;
    this.projectionSphereRadius = Mathf.Lerp(this.projectionSphereRadius, ((this.currentTarget != null ? this.currentTarget.Position : position + rotation * Vector3.forward * 1000f) - this.CamPosition).magnitude, 5f * this.ExactMovementUpdateDeltaTime());
    if ((double) (position - this.CamPosition).sqrMagnitude > (double) this.projectionSphereRadius * (double) this.projectionSphereRadius)
      this.projectionSphereRadius = (position - this.CamPosition).magnitude + 0.01f;
    Vector3 intersectionPoint;
    if (BgoUtils.RaySphereIntersection(out intersectionPoint, position, rotation * Vector3.forward, this.CamPosition, this.projectionSphereRadius))
    {
      camFocusPoint = intersectionPoint;
      return true;
    }
    camFocusPoint = Vector3.zero;
    return false;
  }
}
