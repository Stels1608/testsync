﻿// Decompiled with JetBrains decompiler
// Type: SpecialOfferWindowUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public abstract class SpecialOfferWindowUi : MessageBoxBase
{
  private SpecialOfferCard specialOfferCard;
  private DateTime endTime;
  private Coroutine timerUpdater;
  private uint itemGroup;

  protected override void Awake()
  {
    base.Awake();
    this.gameObject.SetActive(false);
    this.ShowBackgroundUi(false);
  }

  public void SetDataAndShow(uint campaignGuid, DateTime endTime)
  {
    this.endTime = endTime;
    this.specialOfferCard = Game.Catalogue.FetchCard(campaignGuid, CardView.ConversionCampaign) as SpecialOfferCard;
    this.specialOfferCard.IsLoaded.AddHandler(new SignalHandler(this.ItsShowTime));
  }

  private void ShowBackgroundUi(bool show)
  {
    FacadeFactory.GetInstance().SendMessage(Message.ToggleOldUi, (object) show);
    FacadeFactory.GetInstance().SendMessage(Message.ToggleNgui, (object) show);
    FacadeFactory.GetInstance().SendMessage(Message.ToggleUGuiHubCanvas, (object) show);
    FacadeFactory.GetInstance().SendMessage(Message.ToggleUGuiHudCanvas, (object) show);
    FacadeFactory.GetInstance().SendMessage(Message.ToggleUGuiWindowsCanvas, (object) show);
    FacadeFactory.GetInstance().SendMessage(Message.ToggleUGuiHudIndicatorsCanvas, (object) show);
  }

  protected override void OnDestroy()
  {
    base.OnDestroy();
    this.ShowBackgroundUi(true);
  }

  protected override void OnEnable()
  {
    base.OnEnable();
    if (this.timerUpdater != null)
      this.StopCoroutine(this.timerUpdater);
    this.timerUpdater = this.StartCoroutine(this.UpdateTimer());
  }

  public void OnBuyButtonClick()
  {
    Game.Payment.OpenPaymentCampaignWindow(this.specialOfferCard.CardGUID);
  }

  private void ItsShowTime()
  {
    if (this.specialOfferCard == null)
    {
      UnityEngine.Debug.LogError((object) "SpecialOfferWindowUi: GUICard not loaded yet! Aborting display...");
    }
    else
    {
      this.itemGroup = this.specialOfferCard.ItemGroup;
      this.SetOfferIcon(Tools.TexToSprite((Texture2D) Resources.Load(this.specialOfferCard.OfferIconPath)));
      this.SetHeaderText(Tools.ParseMessage(this.specialOfferCard.Name.ToUpper()));
      this.SetMainText(this.specialOfferCard.Description.Replace("%br%", "\n"));
      this.SetOfferImage(Tools.TexToSprite((Texture2D) Resources.Load(Game.Me.Faction != Faction.Colonial ? this.specialOfferCard.OfferImagePathCylon : this.specialOfferCard.OfferImagePathColonial)));
      this.SetOkButtonText(Tools.ParseMessage("%$bgo.deathpopup.buy_now%"));
      this.gameObject.SetActive(true);
    }
  }

  [DebuggerHidden]
  private IEnumerator UpdateTimer()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpecialOfferWindowUi.\u003CUpdateTimer\u003Ec__Iterator44() { \u003C\u003Ef__this = this };
  }

  protected abstract void SetOfferIcon(UnityEngine.Sprite iconSprite);

  protected abstract void SetHeaderText(string text);

  protected abstract void SetTimerText(string text);

  protected abstract void SetOfferImage(UnityEngine.Sprite offerSprite);

  protected override void DestroyWindow()
  {
    if (this.timerUpdater != null)
      this.StopCoroutine(this.timerUpdater);
    base.DestroyWindow();
  }
}
