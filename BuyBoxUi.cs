﻿// Decompiled with JetBrains decompiler
// Type: BuyBoxUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;

public abstract class BuyBoxUi : MessageBoxBase
{
  public void SetContent(OnMessageBoxClose onClose, string titleText, string mainText, string okButtonText, string cancelButtonText, Price price, bool enableOkButton, uint timeToLive)
  {
    this.AddOnClosedAction(onClose);
    this.SetTitleText(Tools.ParseMessage(titleText).ToUpper());
    this.SetOkButtonText(Tools.ParseMessage(okButtonText));
    this.SetCancelButtonText(Tools.ParseMessage(cancelButtonText));
    this.EnableOkButton(enableOkButton);
    this.SetMainText(Tools.ParseMessage(mainText));
    this.SetTimeToLive(timeToLive);
    this.SetPrice(price);
    this.SetCostLabel(Tools.ParseMessage("%$bgo.mining_facility.cost_label%"));
  }

  protected abstract void EnableOkButton(bool enableOkButton);

  protected abstract void SetCancelButtonText(string text);

  protected abstract void SetTitleText(string text);

  protected abstract void ClearPrices();

  protected abstract void AddResourcePrice(string iconPath, float cost);

  protected abstract void SetCostLabel(string costText);

  public void OnCancelPressed()
  {
    this.CloseWindow(MessageBoxActionType.Cancel);
  }

  private void SetPrice(Price price)
  {
    this.ClearPrices();
    if (price == null)
      return;
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = price.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        float cost = current.Value;
        this.AddResourcePrice(current.Key.GUICard.GUIIcon, cost);
      }
    }
  }
}
