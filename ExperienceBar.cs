﻿// Decompiled with JetBrains decompiler
// Type: ExperienceBar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class ExperienceBar : GUIPanel
{
  private ExperienceBar.ChatTransform frameParent;
  private ExperienceBar.ChatTransform barParent;
  private ExperienceBar.ChatTransform myRoot;
  private ExperienceBar.Line frame;
  private ExperienceBar.Line expBar;
  private ExperienceBar.DivideLines divLines;

  public static bool ShowXpBar { get; set; }

  public override float Height
  {
    get
    {
      return this.myRoot.rect.height;
    }
  }

  public ExperienceBar()
    : base((SmartRect) null)
  {
    this.CreateGeometry();
    this.LoadTextures();
    this.SetupSizes();
    this.frame.SetLength((int) byte.MaxValue);
    this.expBar.SetLength((int) byte.MaxValue);
  }

  private void CreateGeometry()
  {
    this.myRoot = new ExperienceBar.ChatTransform();
    this.frameParent = new ExperienceBar.ChatTransform();
    this.frameParent.parent = this.myRoot;
    this.barParent = new ExperienceBar.ChatTransform();
    this.barParent.parent = this.myRoot;
    this.frame = new ExperienceBar.Line(this.frameParent);
    this.expBar = new ExperienceBar.Line(this.frameParent);
    this.divLines = new ExperienceBar.DivideLines(this.frameParent);
  }

  private void LoadTextures()
  {
    this.frame.LoadTextures("GUI/ExperienceBar/xpbar_frame");
    this.expBar.LoadTextures("GUI/ExperienceBar/xpbar");
    this.divLines.LoadTextures("GUI/ExperienceBar/");
  }

  private void SetupSizes()
  {
    this.frame.TakeSizesFromTexture();
    this.expBar.TakeSizesFromTexture();
    this.divLines.TakeSizesFromTextures();
    this.myRoot.rect.height = (float) this.frame.begin.texture.height;
  }

  private void SetExperienceLevel(int newValue)
  {
    this.expBar.SetLength(Mathf.Clamp(newValue, 0, 100));
  }

  public override void Update()
  {
    Me me = Game.Me;
    if (me == null)
      return;
    this.SetExperienceLevel((int) ((double) me.NormalExperience * 100.0));
  }

  public override void Draw()
  {
    if (!ExperienceBar.ShowXpBar)
      return;
    this.frame.Draw();
    this.expBar.Draw();
    this.divLines.Draw();
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.myRoot.rect.x = 10f;
    this.myRoot.rect.y = (float) Screen.height - this.myRoot.rect.height;
    GuiTicker ticker = Game.Ticker;
    if (ticker != null)
      this.myRoot.rect.y -= ticker.SizeY;
    this.myRoot.rect.width = (float) (Screen.width - 20);
    this.frameParent.rect = new Rect(0.0f, 0.0f, this.myRoot.rect.width, this.myRoot.rect.height);
    this.barParent.rect = new Rect(2f, 2f, this.myRoot.rect.width - 4f, this.myRoot.rect.height);
    this.divLines.Resize();
  }

  public override bool Contains(float2 point)
  {
    return this.myRoot.rect.Contains(point.ToV2());
  }

  public override bool OnShowTooltip(float2 mousePosition)
  {
    if (!this.Contains(mousePosition))
      return false;
    string text = BsgoLocalization.Get("%$bgo.etc.experience_bar_tooltip1%", (object) Game.Me.Experience);
    if ((int) Game.Me.Level < 20)
      text += BsgoLocalization.Get("%$bgo.etc.experience_bar_tooltip2%", (object) ((int) Game.Me.Level + 1), (object) Game.Me.NextLevelExperience);
    this.SetTooltip(text);
    Game.TooltipManager.ShowTooltip(this.advancedTooltip);
    return true;
  }

  private class ChatTransform
  {
    public ExperienceBar.ChatTransform parent;
    public Rect rect;

    public ChatTransform()
    {
      this.parent = (ExperienceBar.ChatTransform) null;
      this.rect = new Rect();
    }

    public ChatTransform(ExperienceBar.ChatTransform another)
    {
      this.rect = another.rect;
      this.parent = another.parent;
    }

    public static Rect CalculateRect(ExperienceBar.ChatTransform node)
    {
      if (node.parent == null)
        return node.rect;
      Rect rect1 = ExperienceBar.ChatTransform.CalculateRect(node.parent);
      Rect rect2 = node.rect;
      rect2.x += rect1.x;
      rect2.y += rect1.y;
      return rect2;
    }
  }

  private class Element
  {
    public Texture2D texture;
    public ExperienceBar.ChatTransform transform;

    public Element(ExperienceBar.ChatTransform parent)
    {
      this.transform = new ExperienceBar.ChatTransform();
      this.transform.parent = parent;
      this.texture = (Texture2D) null;
    }
  }

  private class Line
  {
    private ExperienceBar.ChatTransform parent;
    public ExperienceBar.Element begin;
    public ExperienceBar.Element end;
    public ExperienceBar.Element middle;
    private int lineLength;

    public Line(ExperienceBar.ChatTransform parent)
    {
      this.parent = parent;
      this.begin = new ExperienceBar.Element(parent);
      this.end = new ExperienceBar.Element(parent);
      this.middle = new ExperienceBar.Element(parent);
    }

    public void SetLength(int value)
    {
      this.lineLength = value;
    }

    public void Draw()
    {
      if (this.lineLength <= 0)
        return;
      Rect rect1 = ExperienceBar.ChatTransform.CalculateRect(this.begin.transform);
      GUI.DrawTexture(rect1, (Texture) this.begin.texture);
      float num = this.parent.rect.width - (float) this.begin.texture.width - (float) this.end.texture.width;
      Rect rect2 = ExperienceBar.ChatTransform.CalculateRect(this.middle.transform);
      rect2.width = (float) Mathf.Clamp(this.lineLength - 2, 0, 98) / 98f * num;
      GUI.DrawTexture(rect2, (Texture) this.middle.texture);
      Rect rect3 = ExperienceBar.ChatTransform.CalculateRect(this.end.transform);
      rect3.x = rect1.x + rect1.width + rect2.width;
      GUI.DrawTexture(rect3, (Texture) this.end.texture);
    }

    public void LoadTextures(string pathPrefix)
    {
      this.begin.texture = (Texture2D) ResourceLoader.Load(pathPrefix + "_left");
      this.end.texture = (Texture2D) ResourceLoader.Load(pathPrefix + "_right");
      this.middle.texture = (Texture2D) ResourceLoader.Load(pathPrefix + "_center");
    }

    public void TakeSizesFromTexture()
    {
      this.begin.transform.rect.width = (float) this.begin.texture.width;
      this.begin.transform.rect.height = (float) this.begin.texture.height;
      this.end.transform.rect.width = (float) this.end.texture.width;
      this.end.transform.rect.height = (float) this.end.texture.height;
      this.middle.transform.rect.width = (float) this.middle.texture.width;
      this.middle.transform.rect.height = (float) this.middle.texture.height;
      this.middle.transform.rect.x = (float) this.begin.texture.width;
    }
  }

  private class DivideLines
  {
    private ExperienceBar.ChatTransform parent;
    private Rect lineRect;
    private Texture2D divideLine;
    private Vector2[] points;

    public DivideLines(ExperienceBar.ChatTransform parent)
    {
      this.parent = parent;
      this.points = new Vector2[0];
    }

    public void Draw()
    {
      for (int index = 0; index < this.points.Length; ++index)
      {
        this.lineRect.x = this.points[index].x - this.lineRect.width / 2f;
        this.lineRect.y = this.points[index].y;
        GUI.DrawTexture(this.lineRect, (Texture) this.divideLine);
      }
    }

    public void LoadTextures(string pathPrefix)
    {
      this.divideLine = (Texture2D) ResourceLoader.Load(pathPrefix + "xpbar_divideline");
    }

    public void TakeSizesFromTextures()
    {
      this.lineRect = new Rect(0.0f, 0.0f, (float) this.divideLine.width, (float) this.divideLine.height);
    }

    public void Resize()
    {
      Rect rect = ExperienceBar.ChatTransform.CalculateRect(this.parent);
      List<Vector2> points = new List<Vector2>();
      this.CalcPointPosition((int) ((double) rect.width / 10.0), ref points, new Vector2(rect.x, rect.y), new Vector2(rect.x + rect.width, rect.y));
      this.points = points.ToArray();
    }

    private void CalcPointPosition(int threshold, ref List<Vector2> points, Vector2 left, Vector2 right)
    {
      if ((double) (left - right).magnitude / 2.0 < (double) threshold)
        return;
      Vector2 vector2 = new Vector2((float) (((double) left.x + (double) right.x) / 2.0), left.y);
      points.Add(vector2);
      this.CalcPointPosition(threshold, ref points, left, vector2);
      this.CalcPointPosition(threshold, ref points, vector2, right);
    }
  }
}
