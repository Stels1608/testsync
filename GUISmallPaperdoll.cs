﻿// Decompiled with JetBrains decompiler
// Type: GUISmallPaperdoll
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using Paperdolls.PaperdollLayoutSmall;
using System.Collections.Generic;
using UnityEngine;

public class GUISmallPaperdoll : GUIPanel
{
  private readonly JWindowDescription slotDesc = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/Paperdoll/slot_orientation_layout");
  private readonly JWindowDescription helpersDesc = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/Paperdoll/helpers_layout");
  private readonly Color toggledOnColor = ColorUtility.MakeColorFrom0To255(122U, 163U, 238U);
  private Dictionary<ushort, float> reloadTimeList = new Dictionary<ushort, float>();
  public Dictionary<GUISmallPaperdollShipSlots, bool> oldEnables = new Dictionary<GUISmallPaperdollShipSlots, bool>();
  private const float PERIODIC_UPDATE_INTERVAL = 1f;
  private const float CLICK_RADIUS = 21f;
  private const float BLINK_DURATION_PAPERDOLL = 0.3f;
  public static bool AutomaticAmmoChange;
  private AtlasCache atlasCache;
  public List<GUISmallPaperdollShipSlots> ShipSlots;
  private GUIImageNew background;
  private GUIButtonNew rightClicked;
  private SmartRect abilityRect;
  private GUIImageNew repair;
  private GUIImageNew unpowered;
  private GUIImageNew notAvailable;
  private GUIImageNew stateImage;
  private GUIImageNew amountImage;
  private GUILabelNew amountText;
  private GUILabelNew infinityAmountLabel;
  private GUICountdown countdown;
  public bool disabled;

  public InputBinder InputBindings { get; set; }

  public GUISmallPaperdoll(AtlasCache atlasCache)
  {
    this.atlasCache = atlasCache;
    if (Game.Me.ActiveShip == null)
    {
      Debug.LogError((object) "Ship null, can't construct Paperdoll!");
    }
    else
    {
      this.InputBindings = (FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider") as SettingsDataProvider).InputBinder;
      Game.Me.Hangar.OnActiveShipChanged = new AnonymousDelegate(this.CreatePaperdoll);
      Game.Me.ActiveShip.IsLoaded.AddHandler(new SignalHandler(this.CreatePaperdoll));
    }
  }

  private void CreatePaperdoll()
  {
    if ((Object) SpaceLevel.GetLevel() == (Object) null)
      return;
    this.Clear();
    HangarShip activeShip = Game.Me.ActiveShip;
    Paperdolls.PaperdollLayoutSmall.PaperdollLayoutSmall paperdollLayoutSmall = activeShip.Card.PaperdollLayoutSmall;
    this.background = new GUIImageNew(Resources.Load(Paperdolls.PaperdollLayoutSmall.PaperdollLayoutSmall.BlueprintTexturePath + BgoUtils.FactionUiFolder(activeShip.Card.Faction) + paperdollLayoutSmall.BlueprintTexture) as Texture2D);
    float2 float2 = new float2((float) this.background.Texture.width, (float) this.background.Texture.height);
    this.background.Size = float2;
    this.Size = float2;
    this.AddPanel((GUIPanel) this.background);
    this.ShipSlots = this.CreateShipSlots(paperdollLayoutSmall.GetSlotLayouts((int) activeShip.Card.Level));
    this.UpdateControlLabels();
    this.repair = (this.helpersDesc["repair"] as JImage).CreateGUIImageNew();
    this.unpowered = (this.helpersDesc["unpowered"] as JImage).CreateGUIImageNew();
    this.stateImage = (this.helpersDesc["stateImage"] as JImage).CreateGUIImageNew();
    this.notAvailable = (this.helpersDesc["disabled"] as JImage).CreateGUIImageNew();
    this.amountImage = (this.helpersDesc["amountImage"] as JImage).CreateGUIImageNew();
    this.amountText = (this.helpersDesc["amountText"] as JLabel).CreateGUILabelNew();
    this.infinityAmountLabel = new GUILabelNew("∞", this.amountImage.SmartRect, Gui.Options.FontDS_EF_M, Color.white, Color.white);
    this.infinityAmountLabel.FontSize = 14;
    this.infinityAmountLabel.Position = new float2(5f, 0.0f);
    this.abilityRect = new SmartRect(new Rect(0.0f, 0.0f, this.atlasCache.ElementSize.x, this.atlasCache.ElementSize.y), float2.zero, (SmartRect) null);
    this.abilityRect.Position = new float2(1f, 0.0f);
    this.countdown = new GUICountdown(ResourceLoader.Load<Texture2D>("GUI/AbilityToolbar/cooldown_atlas"), 8U, 8U);
    this.AddPanel((GUIPanel) new GUIPanel.Timer(0.3f, new AnonymousDelegate(this.UpdateControls)));
    this.IsRendered = true;
    this.Position = new float2(0.0f, (float) -((double) this.Height / 2.0 + 5.0 + (double) Game.GUIManager.Find<GUIAbilityToolbar>().background.Height / 2.0));
    this.reloadTimeList = new Dictionary<ushort, float>();
  }

  private List<GUISmallPaperdollShipSlots> CreateShipSlots(List<SlotLayout> slotLayouts)
  {
    List<GUISmallPaperdollShipSlots> paperdollShipSlotsList = new List<GUISmallPaperdollShipSlots>();
    using (List<SlotLayout>.Enumerator enumerator = slotLayouts.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SlotLayout current = enumerator.Current;
        ushort slotID = current.SlotId;
        ShipSlot slot = Game.Me.ActiveShip.GetSlot(slotID);
        if (slot != null)
        {
          GUISmallPaperdollShipSlots paperdollShipSlots1 = new GUISmallPaperdollShipSlots();
          string path = "GUI/Paperdoll/" + BgoUtils.FactionUiFolder(Game.Me.ActiveShip.Card.Faction) + "abilityslot_" + current.Arrowtype;
          Texture2D texture = Resources.Load(path) as Texture2D;
          if ((Object) texture == (Object) null)
          {
            Debug.LogError((object) ("Couldn't load arrowImage: " + path));
          }
          else
          {
            paperdollShipSlots1.image = new GUIImageNew(texture, this.root);
            paperdollShipSlots1.image.Position = current.Position;
            paperdollShipSlots1.label = !(current.Arrowtype == "none") ? (this.slotDesc[current.Arrowtype + "Label"] as JLabel).CreateGUILabelNew(paperdollShipSlots1.image.SmartRect) : (this.slotDesc["upLabel"] as JLabel).CreateGUILabelNew(paperdollShipSlots1.image.SmartRect);
            paperdollShipSlots1.button = (this.helpersDesc["button"] as JButton).CreateGUIButtonNew();
            paperdollShipSlots1.button.NormalTexture = (Texture2D) null;
            paperdollShipSlots1.button.Parent = paperdollShipSlots1.image.SmartRect;
            paperdollShipSlots1.slotID = slotID;
            paperdollShipSlots1.blinker = new GUIBlinker();
            paperdollShipSlots1.blinker.Width *= 1.5f;
            paperdollShipSlots1.blinker.Height *= 1.5f;
            paperdollShipSlots1.IsBlinking = false;
            paperdollShipSlots1.blinker.Parent = paperdollShipSlots1.image.SmartRect;
            paperdollShipSlots1.blinker.RecalculateAbsCoords();
            GUISmallPaperdollShipSlots paperdollShipSlots2 = paperdollShipSlots1;
            Action action = GUISmallPaperdoll.HotKeyIdToAction(current.Hotkey);
            slot.Action = action;
            int num = (int) action;
            paperdollShipSlots2.action = (Action) num;
            paperdollShipSlots1.label.Text = string.Empty;
            paperdollShipSlotsList.Add(paperdollShipSlots1);
          }
        }
      }
    }
    return paperdollShipSlotsList;
  }

  public static Action HotKeyIdToAction(int hotkeyId)
  {
    if (hotkeyId == -1)
      return Action.None;
    if (hotkeyId == 0)
      hotkeyId += 10;
    return (Action) (hotkeyId + 101 - 1);
  }

  public override void Draw()
  {
    base.Draw();
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        current.image.Draw();
        current.label.Draw();
        ShipAbility ability = current.Ability;
        if (ability != null)
        {
          this.abilityRect.Parent = current.image.SmartRect;
          AtlasEntry cachedEntryBy = this.atlasCache.GetCachedEntryBy(ability.guiCard.GUIAtlasTexturePath, (int) ability.guiCard.FrameIndex);
          if (Event.current.type == UnityEngine.EventType.Repaint)
            Graphics.DrawTexture(this.abilityRect.AbsRect, (Texture) cachedEntryBy.Texture, cachedEntryBy.FrameRect, 0, 0, 0, 0);
          this.stateImage.Parent = current.image.SmartRect;
          if (ability.IsBroken)
          {
            this.repair.Parent = current.image.SmartRect;
            this.repair.Draw();
          }
          else if (!ability.IsPowerPointsEnough)
          {
            this.unpowered.Parent = current.image.SmartRect;
            this.unpowered.Draw();
          }
          else if (ability.IsCooling)
          {
            this.countdown.Progress = 1f - ability.GetCoolingProgress();
            this.countdown.GetRect().Parent = current.image.SmartRect;
            this.countdown.RecalculateAbsCoords();
            this.countdown.Draw();
            if (!ability.card.Auto)
            {
              float cooldown = ability.GetCooldown();
              if ((double) cooldown < 0.300000011920929 && (double) cooldown > 0.0 && Event.current.type == UnityEngine.EventType.Repaint)
                Graphics.DrawTexture(this.stateImage.SmartRect.AbsRect, (Texture) this.stateImage.Texture, this.stateImage.InnerRect, 0, 0, 0, 0, new Color(1f, 1f, 1f, 1f - Mathf.Abs((float) (1.0 - (double) cooldown * 3.33333325386047 * 2.0))));
            }
          }
          else if (!current.IsValid || Game.Me.Anchored)
          {
            this.notAvailable.Parent = current.image.SmartRect;
            this.notAvailable.Draw();
          }
          if (ability.card.Auto && ability.On)
            Graphics.DrawTexture(this.stateImage.SmartRect.AbsRect, (Texture) this.stateImage.Texture, this.stateImage.InnerRect, 0, 0, 0, 0, this.toggledOnColor);
          if (ability.card.Countable)
          {
            this.amountText.Parent = current.image.SmartRect;
            this.amountImage.Parent = current.image.SmartRect;
            this.amountImage.Draw();
            if (!this.reloadTimeList.ContainsKey(ability.slot.ServerID))
              this.reloadTimeList.Add(ability.slot.ServerID, Time.time);
            this.AmmoSwitch(ability, current);
            if (current.count < 10000U)
            {
              this.amountText.TextNoParse = current.countString;
              this.amountText.Draw();
            }
            else
            {
              this.infinityAmountLabel.RecalculateAbsCoords();
              this.infinityAmountLabel.Draw();
            }
          }
          if (current.IsBlinking)
          {
            current.blinker.Parent = current.image.SmartRect;
            current.blinker.Draw();
          }
          current.button.Draw();
        }
      }
    }
  }

  private void AmmoSwitch(ShipAbility ability, GUISmallPaperdollShipSlots control)
  {
    if (!GUISmallPaperdoll.AutomaticAmmoChange || (int) control.count != 0 || (double) this.reloadTimeList[ability.slot.ServerID] >= (double) Time.time)
      return;
    string str = string.Empty;
    if (control.Ability == null || control.Ability.ConsumableCard == null || control.Ability.ConsumableCard.sortingAttributes == null)
      return;
    if (control.Ability.ConsumableCard.sortingAttributes.Length == 1)
      str = control.Ability.ConsumableCard.sortingAttributes[0].Attribute;
    this.reloadTimeList[ability.slot.ServerID] = 5f + Time.time;
    using (List<ItemCountable>.Enumerator enumerator = this.GetHoldList(Game.Me.ActiveShip.Card.Tier, (ShipAbstractAbility) ability).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ItemCountable current = enumerator.Current;
        foreach (ConsumableAttribute sortingAttribute in current.Card.sortingAttributes)
        {
          if (sortingAttribute.Attribute != str)
          {
            if ((int) control.count == 0 && sortingAttribute.Attribute == "standard" && current.Count > 0U)
            {
              control.Ability.slot.SelectConsumable(current);
              return;
            }
          }
          else
          {
            ObjectStat key = ObjectStat.None;
            if (str == "agility" || str == "nuclear_damage" || str == "total_damage")
              key = ObjectStat.DamageHigh;
            else if (str == "armor_piercing")
              key = ObjectStat.ArmorPiercing;
            else if (str == "speed")
              key = ObjectStat.Speed;
            else if (str == "average_damage")
              key = ObjectStat.DamageLow;
            else if (str == "accuracy")
              key = ObjectStat.Accuracy;
            else if (str == "duration")
              key = ObjectStat.Duration;
            if ((double) control.Ability.ConsumableCard.ItemBuffAdd.GetObfuscatedFloat(key) > (double) current.Card.ItemBuffAdd.GetObfuscatedFloat(key))
            {
              control.Ability.slot.SelectConsumable(current);
              return;
            }
          }
        }
      }
    }
  }

  private List<ItemCountable> GetHoldList(byte tier, ShipAbstractAbility m_ability)
  {
    List<ItemCountable> itemCountableList = new List<ItemCountable>();
    foreach (ShipItem shipItem in (IEnumerable<ShipItem>) Game.Me.Hold)
    {
      if (shipItem is ItemCountable)
      {
        ItemCountable itemCountable = shipItem as ItemCountable;
        if ((bool) m_ability.card.IsLoaded && (bool) itemCountable.Card.IsLoaded && ((int) m_ability.card.ConsumableType == (int) itemCountable.Card.ConsumableType && (int) m_ability.card.ConsumableTier == (int) itemCountable.Card.Tier) && ((int) itemCountable.Card.Tier == (int) tier || (int) itemCountable.Card.Tier == 0))
        {
          itemCountable.disableBuying = true;
          itemCountableList.Add(shipItem as ItemCountable);
        }
      }
    }
    return itemCountableList;
  }

  public override void Update()
  {
    base.Update();
    for (int index = 0; index < this.ShipSlots.Count; ++index)
    {
      if (this.ShipSlots[index].IsBlinking)
        this.ShipSlots[index].blinker.Update();
    }
  }

  private void UpdateControls()
  {
    for (int index = 0; index < this.ShipSlots.Count; ++index)
    {
      if (this.ShipSlots[index].Ability != null)
      {
        this.ShipSlots[index].count = this.ShipSlots[index].Ability.ConsumableCount;
        this.ShipSlots[index].countString = this.ShipSlots[index].count.ToString();
        this.ShipSlots[index].IsValid = this.ShipSlots[index].Ability.IsValid;
      }
    }
  }

  public void UpdateControlLabels()
  {
    for (int index = 0; index < this.ShipSlots.Count; ++index)
    {
      IInputBinding binding = this.InputBindings.TryGetBinding(InputDevice.KeyboardMouse, this.ShipSlots[index].action);
      this.ShipSlots[index].label.Text = binding == null ? string.Empty : binding.TriggerAlias;
      if (this.ShipSlots[index].label.Text.Length > 2)
        this.ShipSlots[index].label.Text = string.Empty;
    }
  }

  public override bool Contains(float2 point)
  {
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.button.Contains(point))
          return true;
      }
    }
    return false;
  }

  public void DisableAll()
  {
    this.oldEnables.Clear();
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        if (current.Ability != null)
        {
          this.oldEnables[current] = current.Ability.On;
          current.Ability.On = false;
        }
      }
    }
    this.disabled = true;
  }

  public void EnableAll()
  {
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        if (current.Ability != null && this.oldEnables.ContainsKey(current))
          current.Ability.On = this.oldEnables[current];
      }
    }
    this.oldEnables.Clear();
    this.disabled = false;
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    this.rightClicked = (GUIButtonNew) null;
    if (this.disabled)
      return false;
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        if (current.Ability != null && (double) (mousePosition - current.button.SmartRect.AbsPosition).magnitude < 21.0)
        {
          if (mouseKey == KeyCode.Mouse0)
          {
            current.button.IsPressed = true;
            return true;
          }
          if (mouseKey == KeyCode.Mouse1)
          {
            if (current.Ability.card.ConsumableOption != ShipConsumableOption.NotUsing)
              this.rightClicked = current.button;
            return true;
          }
        }
      }
    }
    return false;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    base.OnMouseMove(mousePosition);
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        if (current.Ability != null)
        {
          if ((double) (mousePosition - current.button.SmartRect.AbsPosition).magnitude < 21.0)
          {
            current.button.MouseOver = true;
            current.Ability.DrawFiringArc(true);
          }
          else
          {
            current.button.MouseOver = false;
            current.Ability.DrawFiringArc(false);
          }
        }
      }
    }
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (this.disabled)
      return false;
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        if (current.Ability != null && (double) (mousePosition - current.button.SmartRect.AbsPosition).magnitude < 21.0)
        {
          if (mouseKey == KeyCode.Mouse0 && current.button.IsPressed)
          {
            current.button.IsPressed = false;
            current.Ability.Touch();
            if (current.IsBlinking)
              StoryProtocol.GetProtocol().TriggerControl((StoryProtocol.ControlType) current.action);
            return true;
          }
          if (mouseKey == KeyCode.Mouse1 && this.rightClicked == current.button)
          {
            GuiConsumablePanel guiConsumablePanel = Game.GUIManager.Find<GuiConsumablePanel>();
            guiConsumablePanel.IsRendered = true;
            guiConsumablePanel.Ability = current.Ability;
            Game.GUIManager.RemovePanel((IGUIPanel) guiConsumablePanel);
            Game.GUIManager.AddPanel((IGUIPanel) guiConsumablePanel);
            guiConsumablePanel.MakeValidInScreenPosition(new Vector2()
            {
              x = this.rightClicked.SmartRect.AbsRect.x,
              y = this.rightClicked.SmartRect.AbsRect.y
            });
            this.rightClicked = (GUIButtonNew) null;
            return true;
          }
        }
      }
    }
    return false;
  }

  public override bool OnShowTooltip(float2 mousePosition)
  {
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        if (current.Ability != null && (double) (mousePosition - current.button.SmartRect.AbsPosition).magnitude < 21.0)
        {
          this.SetTooltip(current.Ability, current.Ability.guiCard);
          Game.TooltipManager.ShowTooltip(this.advancedTooltip);
          return true;
        }
      }
    }
    return false;
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (action == Action.None)
      return false;
    if (!this.disabled)
    {
      using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISmallPaperdollShipSlots current = enumerator.Current;
          if (current.action == action && current.Ability != null)
          {
            current.button.IsPressed = true;
            return true;
          }
        }
      }
    }
    if (action == Action.ToggleGunsOnHold)
      this.SetAllGunsAutofireState(true);
    if (action != Action.FireMissiles && action != Action.ToggleGuns && (action != Action.ToggleGunsOnHold && action != Action.ToggleFlak))
      return action == Action.TogglePointDefence;
    return true;
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (action == Action.None)
      return false;
    if (!this.disabled)
    {
      using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GUISmallPaperdollShipSlots current = enumerator.Current;
          if (current.action == action && current.Ability != null)
          {
            current.button.IsPressed = false;
            current.Ability.Touch();
            if (current.IsBlinking)
              StoryProtocol.GetProtocol().TriggerControl((StoryProtocol.ControlType) current.action);
            return true;
          }
        }
      }
      Action action1 = action;
      switch (action1)
      {
        case Action.ToggleGunsOnHold:
          this.SetAllGunsAutofireState(false);
          break;
        case Action.ToggleFlak:
          this.ToggleAllWeaponsByAction(Action.ToggleFlak, true);
          break;
        case Action.TogglePointDefence:
          this.ToggleAllWeaponsByAction(Action.TogglePointDefence, true);
          break;
        default:
          if (action1 != Action.FireMissiles)
          {
            if (action1 == Action.ToggleGuns)
            {
              this.ToggleAllWeaponsByAction(Action.ToggleGuns, false);
              break;
            }
            break;
          }
          this.ToggleAllWeaponsByAction(Action.FireMissiles, false);
          break;
      }
    }
    if (action != Action.FireMissiles && action != Action.ToggleGuns && action != Action.ToggleFlak)
      return action == Action.TogglePointDefence;
    return true;
  }

  private Action GetActionTypeFromWeaponAbility(ShipAbility ability)
  {
    AbilityActionType abilityActionType = ability.card.ActionType;
    switch (abilityActionType)
    {
      case AbilityActionType.FireMissle:
label_3:
        return Action.FireMissiles;
      case AbilityActionType.FireCannon:
      case AbilityActionType.FireMining:
label_2:
        return Action.ToggleGuns;
      case AbilityActionType.Flak:
        return Action.ToggleFlak;
      case AbilityActionType.PointDefence:
        return Action.TogglePointDefence;
      default:
        switch (abilityActionType - 19)
        {
          case AbilityActionType.None:
          case AbilityActionType.FireCannon:
          case AbilityActionType.DropFlare:
            goto label_3;
          case AbilityActionType.Buff:
          case AbilityActionType.RestoreBuff:
          case AbilityActionType.ResourceScan:
            goto label_2;
          default:
            return Action.None;
        }
    }
  }

  private void ToggleAllWeaponsByAction(Action actionType, bool onlyAutoAbilities = false)
  {
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        if (current.Ability != null && actionType == this.GetActionTypeFromWeaponAbility(current.Ability) && (!onlyAutoAbilities || current.Ability.card.Auto))
        {
          if (current.IsBlinking)
            StoryProtocol.GetProtocol().TriggerControl((StoryProtocol.ControlType) current.action);
          current.Ability.Touch();
        }
      }
    }
  }

  private void SetAllGunsAutofireState(bool bActive)
  {
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        if (current.Ability != null && this.GetActionTypeFromWeaponAbility(current.Ability) == Action.ToggleGuns)
          current.Ability.On = bActive;
      }
    }
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        current.image.RecalculateAbsCoords();
        current.button.RecalculateAbsCoords();
        current.label.RecalculateAbsCoords();
        current.blinker.RecalculateAbsCoords();
      }
    }
  }

  public void SetBlink(StoryProtocol.ControlType weaponSlot, bool value)
  {
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        if ((StoryProtocol.ControlType) current.action == weaponSlot)
          current.IsBlinking = value;
      }
    }
  }

  public void SetBlink(uint slotId, bool value)
  {
    using (List<GUISmallPaperdollShipSlots>.Enumerator enumerator = this.ShipSlots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUISmallPaperdollShipSlots current = enumerator.Current;
        if ((int) current.slotID == (int) slotId && current.IsBlinking != value)
          current.IsBlinking = value;
      }
    }
  }
}
