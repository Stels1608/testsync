﻿// Decompiled with JetBrains decompiler
// Type: Inspector2
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class Inspector2
{
  private static SRect m_selected;
  private static SRect m_topParent;

  public static SRect Selected
  {
    get
    {
      return Inspector2.m_selected;
    }
    set
    {
      Inspector2.m_selected = value;
      Inspector2.m_topParent = Inspector2.m_selected;
      while (Inspector2.m_topParent != null && Inspector2.m_topParent.Parent != null)
        Inspector2.m_topParent = Inspector2.m_topParent.Parent;
    }
  }

  public static SRect TopParentOfSelected
  {
    get
    {
      return Inspector2.m_topParent;
    }
    set
    {
      Inspector2.m_topParent = value;
    }
  }
}
