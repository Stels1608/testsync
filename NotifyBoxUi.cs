﻿// Decompiled with JetBrains decompiler
// Type: NotifyBoxUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public abstract class NotifyBoxUi : MessageBoxBase
{
  protected override void Awake()
  {
    base.Awake();
    this.ShowTimer(false);
  }

  public void SetContent(OnMessageBoxClose onPressed, string titleText, string mainText, uint timeToLive)
  {
    this.SetContent(onPressed, titleText, mainText, "%$bgo.common.ok%", timeToLive);
  }

  public void SetContent(OnMessageBoxClose onPressed, string titleText, string mainText, string okButtonText, uint timeToLive)
  {
    this.AddOnClosedAction(onPressed);
    this.SetTitleText(Tools.ParseMessage(titleText).ToUpper());
    this.SetOkButtonText(Tools.ParseMessage(okButtonText));
    this.SetMainText(Tools.ParseMessage(mainText));
    this.SetTimeToLive(timeToLive);
  }

  protected abstract void SetTitleText(string text);

  protected abstract void SetTimerText(string text);

  public abstract void ShowTimer(bool show);

  protected override void OnTimeLeftUpdated(string mainText)
  {
    this.SetTimerText(mainText);
  }
}
