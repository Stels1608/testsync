﻿// Decompiled with JetBrains decompiler
// Type: InputBindingDialogBoxContentKeyboard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class InputBindingDialogBoxContentKeyboard : InputBindingDialogBoxContentBase
{
  [SerializeField]
  private UILabel noneLabel;
  [SerializeField]
  private UILabel shiftLabel;
  [SerializeField]
  private UILabel ctrlLabel;
  [SerializeField]
  private RadioButtonGroupWidget radioButtonGroup;

  protected override void Start()
  {
    base.Start();
    this.noneLabel.text = Tools.ParseMessage("%$bgo.ui.options.popup.keybinding.key.none%");
    this.shiftLabel.text = "SHIFT";
    this.ctrlLabel.text = "CTRL";
    this.radioButtonGroup.OnValueChanged = new AnonymousDelegate(this.HandleRadioButtons);
  }

  public void HandleRadioButtons()
  {
    ((KeyBinding) this.pressedBinding).SetModifier((KeyModifier) this.radioButtonGroup.GetIndexOfActiveButton());
    this.UpdateUi();
  }

  protected override void HandleInput()
  {
    foreach (int unityKeyCode in this.unityKeyCodes)
    {
      KeyCode keyCode = (KeyCode) unityKeyCode;
      if (Input.GetKeyDown(keyCode) && (keyCode < KeyCode.Mouse0 || keyCode > KeyCode.Mouse1))
      {
        if (KeyBinding.KeyCodeIsJoystickButton(keyCode))
        {
          Debug.LogError((object) "KeyCode is Joystick Button");
        }
        else
        {
          ((KeyBinding) this.pressedBinding).SetKeyCode(keyCode);
          this.UpdateUi();
        }
      }
    }
  }

  public override void Setup(IInputBinding currentBinding)
  {
    base.Setup(currentBinding);
    this.radioButtonGroup.SetActiveButton((int) ((KeyBinding) currentBinding).KeyModifier);
  }

  protected override void UnmapAction()
  {
    base.UnmapAction();
    this.radioButtonGroup.SetActiveButton(0);
  }

  public override bool IsValidInput(IInputBinding inputBinding)
  {
    KeyCode keyCode = (KeyCode) inputBinding.DeviceTriggerCode;
    switch (keyCode)
    {
      case KeyCode.CapsLock:
      case KeyCode.Insert:
      case KeyCode.Delete:
      case KeyCode.Print:
      case KeyCode.ScrollLock:
      case KeyCode.Pause:
      case KeyCode.Home:
      case KeyCode.End:
        return false;
      default:
        return keyCode != KeyCode.Numlock;
    }
  }

  protected override string GetTextForValidInputState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.valid_key%";
  }

  protected override string GetTextForInitState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.init_key%";
  }

  protected override string GetTextForInvalidInputState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.invalid_key%";
  }

  protected override string GetTextForTriggerAlreadyMappedState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.already_mapped_key%";
  }

  protected override string GetTextForNotMappedState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.unmapped_key%";
  }
}
