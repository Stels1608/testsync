﻿// Decompiled with JetBrains decompiler
// Type: NotShowAgainDialogBoxContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using UnityEngine;

public class NotShowAgainDialogBoxContent : DialogBoxContent
{
  private UserSetting setting;
  [SerializeField]
  private CheckboxWidget checkboxWidget;

  public void SetSetting(UserSetting set)
  {
    this.setting = set;
    this.checkboxWidget.TextLabel.text = Tools.ParseMessage("%$bgo.etc.opt_show_popup_tips%");
  }

  public void SaveSetting()
  {
    FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(this.setting, (object) this.checkboxWidget.IsChecked, true));
  }

  public override void OnAccept()
  {
    this.SaveSetting();
  }

  public override void OnCancel()
  {
  }
}
