﻿// Decompiled with JetBrains decompiler
// Type: HappyHourLayer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HappyHourLayer : MonoBehaviour
{
  public Text headerLabel;
  public Text timeRemainingLabel;
  public Text descriptionLabel;
  public Button closeButton;
  public Button buyButton;

  private void Start()
  {
    this.closeButton.onClick.AddListener(new UnityAction(this.OnClose));
  }

  private void OnClose()
  {
    this.gameObject.SetActive(false);
  }

  public void Show(RewardCard card)
  {
    this.gameObject.SetActive(true);
    this.headerLabel.text = card != null ? card.GUICard.Name : "%$bgo.etc.special_offer_button%";
    this.descriptionLabel.text = card != null ? card.GUICard.Description : "%$bgo.etc.special_offer_text%";
    this.buyButton.GetComponentInChildren<Text>().text = BsgoLocalization.Get("bgo.etc.special_offer_get_button");
    this.CancelInvoke();
    if (Game.SpecialOfferManager.IsHappyHourActive())
    {
      this.timeRemainingLabel.enabled = true;
      this.InvokeRepeating("UpdateTime", 0.0f, 1f);
    }
    else
      this.timeRemainingLabel.enabled = false;
    this.buyButton.onClick.RemoveAllListeners();
    this.buyButton.onClick.AddListener(new UnityAction(Game.SpecialOfferManager.ProceedSpecialOffer));
  }

  private void UpdateTime()
  {
    this.timeRemainingLabel.text = BsgoLocalization.Get("bgo.etc.special_offer_remaining_time", (object) Tools.FormatTime((float) (Game.SpecialOfferManager.SpecialOfferBonusEndTime - Game.TimeSync.ServerTime)));
  }
}
