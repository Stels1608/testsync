﻿// Decompiled with JetBrains decompiler
// Type: Log
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Globalization;
using System.IO;
using UnityEngine;

public static class Log
{
  private static readonly NumberFormatInfo formatProv = new NumberFormatInfo();
  private const string fileLOG = "./log.txt";

  public static void Info(object msg)
  {
    if (!BgoDebug.enabled)
      return;
    Debug.Log((object) (msg.ToString() + " : " + (object) Time.realtimeSinceStartup));
  }

  public static void Add(string msg)
  {
    Log.Info((object) msg);
  }

  public static void Add(string msg, int number)
  {
    Log.Info((object) string.Format(msg + "{0}", (object) number));
  }

  public static void Add(string msg, float number)
  {
    Log.formatProv.NumberDecimalSeparator = ".";
    Log.Info((object) string.Format((IFormatProvider) Log.formatProv, msg + "{0}", new object[1]
    {
      (object) number
    }));
  }

  public static void Add(string msg, Vector2 value)
  {
    Log.formatProv.NumberDecimalSeparator = ".";
    Log.Info((object) string.Format((IFormatProvider) Log.formatProv, msg + "x={0};y={1}", new object[2]
    {
      (object) value.x,
      (object) value.y
    }));
  }

  public static void Add(string msg, float2 value)
  {
    Log.formatProv.NumberDecimalSeparator = ".";
    Log.Info((object) string.Format((IFormatProvider) Log.formatProv, msg + "x={0};y={1}", new object[2]
    {
      (object) value.x,
      (object) value.y
    }));
  }

  public static void Add(string msg, Vector3 value)
  {
    Log.formatProv.NumberDecimalSeparator = ".";
    Log.Info((object) string.Format((IFormatProvider) Log.formatProv, msg + "x={0};y={1};z={2}", (object) value.x, (object) value.y, (object) value.z));
  }

  public static void Add(string msg, bool value)
  {
    Log.Info((object) string.Format(msg + "{0}", (object) value.ToString()));
  }

  public static void Add(string format, params object[] par)
  {
    Log.Info((object) string.Format(format, par));
  }

  public static void AddToFile(string msg)
  {
    StreamWriter streamWriter = new StreamWriter("./log.txt", true);
    streamWriter.WriteLine(msg);
    streamWriter.Close();
    streamWriter.Dispose();
  }

  public static void DebugInfo(object msg)
  {
    if (!Application.isEditor)
      return;
    Debug.Log((object) (msg.ToString() + " : " + (object) Time.realtimeSinceStartup));
  }

  public static void Warning(object msg)
  {
    if (!BgoDebug.enabled)
      return;
    Debug.LogWarning((object) (msg.ToString() + " : " + (object) Time.realtimeSinceStartup));
  }

  public static string GetFormatted(float number)
  {
    Log.formatProv.NumberDecimalSeparator = ".";
    return string.Format((IFormatProvider) Log.formatProv, "{0}", new object[1]{ (object) number });
  }

  public static string DebugVectorPrint(Vector3 v)
  {
    return "(" + v.x.ToString("0.000") + ", " + v.y.ToString("0.000") + ", " + v.z.ToString("0.000") + ")";
  }
}
