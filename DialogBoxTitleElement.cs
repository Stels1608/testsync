﻿// Decompiled with JetBrains decompiler
// Type: DialogBoxTitleElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class DialogBoxTitleElement : MonoBehaviour
{
  [SerializeField]
  private UILabel titleLabel;

  public void Init(string text)
  {
    this.titleLabel.text = Tools.ParseMessage(text).ToUpperInvariant();
  }
}
