﻿// Decompiled with JetBrains decompiler
// Type: InviteTimeoutManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class InviteTimeoutManager
{
  private List<InviteTimeoutManager.InviteTimeout> timeout = new List<InviteTimeoutManager.InviteTimeout>();

  public void Update()
  {
    using (List<InviteTimeoutManager.InviteTimeout>.Enumerator enumerator = this.timeout.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Update();
    }
  }

  public void DeleteTimeout(Player player)
  {
    using (List<InviteTimeoutManager.InviteTimeout>.Enumerator enumerator = this.timeout.GetEnumerator())
    {
      if (!enumerator.MoveNext())
        return;
      InviteTimeoutManager.InviteTimeout current = enumerator.Current;
      if (!current.IsThisPlayer(player))
        return;
      this.timeout.Remove(current);
    }
  }

  public void AddTimeout(Player player)
  {
    this.timeout.Add(new InviteTimeoutManager.InviteTimeout(player));
  }

  public bool IsTimeout(Player player)
  {
    using (List<InviteTimeoutManager.InviteTimeout>.Enumerator enumerator = this.timeout.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        InviteTimeoutManager.InviteTimeout current = enumerator.Current;
        if (current.IsThisPlayer(player))
          return !current.IsEnabledInvite;
      }
    }
    return false;
  }

  private class InviteTimeout
  {
    private const float INVITE_TIMEMAX = 60f;
    private uint serverID;
    private float inviteTimeout;

    public bool IsEnabledInvite
    {
      get
      {
        return (double) this.inviteTimeout >= 60.0;
      }
    }

    public InviteTimeout(Player player)
    {
      this.serverID = player.ServerID;
    }

    public bool IsThisPlayer(Player player)
    {
      return (int) this.serverID == (int) player.ServerID;
    }

    public void Update()
    {
      if (this.IsEnabledInvite)
        return;
      this.inviteTimeout += Time.deltaTime;
    }
  }
}
