﻿// Decompiled with JetBrains decompiler
// Type: AvatarInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public static class AvatarInfo
{
  private static readonly List<string> races = new List<string>();
  private static readonly Dictionary<string, List<string>> sexesOfRaces = new Dictionary<string, List<string>>();
  private static readonly Dictionary<string, List<string>> itemsOfRaces = new Dictionary<string, List<string>>();
  private static readonly Dictionary<string, List<string>> texturesOfRaces = new Dictionary<string, List<string>>();
  private static readonly Dictionary<string, List<string>> materialsOfRaces = new Dictionary<string, List<string>>();
  private static bool init = false;

  private static void Init()
  {
    AvatarInfo.races.Add("human");
    AvatarInfo.races.Add("cylon");
    AvatarInfo.sexesOfRaces["human"] = new List<string>()
    {
      "female",
      "male"
    };
    AvatarInfo.sexesOfRaces["cylon"] = new List<string>()
    {
      "centurion"
    };
    AvatarInfo.itemsOfRaces["human"] = new List<string>()
    {
      "hair",
      "head",
      "suit",
      "beard",
      "glasses",
      "helmet"
    };
    AvatarInfo.itemsOfRaces["cylon"] = new List<string>()
    {
      "head",
      "arms",
      "body",
      "legs"
    };
    AvatarInfo.texturesOfRaces["human"] = new List<string>()
    {
      AvatarInfo.GetTextureFromItem("faces"),
      AvatarInfo.GetTextureFromItem("hands")
    };
    List<string> stringList = new List<string>();
    AvatarInfo.texturesOfRaces["cylon"] = stringList;
    AvatarInfo.materialsOfRaces["human"] = new List<string>()
    {
      AvatarInfo.GetMaterialFromItem("hair"),
      AvatarInfo.GetMaterialFromItem("beard")
    };
    AvatarInfo.materialsOfRaces["cylon"] = new List<string>()
    {
      AvatarInfo.GetMaterialFromItem("head"),
      AvatarInfo.GetMaterialFromItem("arms"),
      AvatarInfo.GetMaterialFromItem("body"),
      AvatarInfo.GetMaterialFromItem("legs")
    };
    AvatarInfo.init = true;
  }

  public static List<string> GetRaces()
  {
    if (!AvatarInfo.init)
      AvatarInfo.Init();
    return AvatarInfo.races;
  }

  public static List<string> GetSexesOfRace(string race)
  {
    if (!AvatarInfo.init)
      AvatarInfo.Init();
    return AvatarInfo.sexesOfRaces[race];
  }

  public static List<string> GetItemsOfRace(string race)
  {
    if (!AvatarInfo.init)
      AvatarInfo.Init();
    return AvatarInfo.itemsOfRaces[race];
  }

  public static List<string> GetTexturesOfRace(string race)
  {
    if (!AvatarInfo.init)
      AvatarInfo.Init();
    return AvatarInfo.texturesOfRaces[race];
  }

  public static List<string> GetMaterialsOfRace(string race)
  {
    if (!AvatarInfo.init)
      AvatarInfo.Init();
    return AvatarInfo.materialsOfRaces[race];
  }

  public static bool IsItem(string race, string item)
  {
    if (!AvatarInfo.init)
      AvatarInfo.Init();
    return AvatarInfo.GetItemsOfRace(race).Contains(item);
  }

  public static bool IsTexture(string race, string texture)
  {
    if (!AvatarInfo.init)
      AvatarInfo.Init();
    return AvatarInfo.GetTexturesOfRace(race).Contains(texture);
  }

  public static bool IsMaterial(string race, string material)
  {
    if (!AvatarInfo.init)
      AvatarInfo.Init();
    return AvatarInfo.GetMaterialsOfRace(race).Contains(material);
  }

  public static string GetItemFromMaterial(string material)
  {
    return material.Remove(material.Length - 1, 1);
  }

  public static string GetMaterialFromItem(string item)
  {
    return item + "_";
  }

  public static string GetItemFromTexture(string tex)
  {
    return tex.Remove(tex.Length - 4, 4);
  }

  public static string GetTextureFromItem(string item)
  {
    return item + "_tex";
  }
}
