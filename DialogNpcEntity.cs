﻿// Decompiled with JetBrains decompiler
// Type: DialogNpcEntity
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class DialogNpcEntity : MonoBehaviour
{
  public AnonymousDelegate handleClick;
  [SerializeField]
  private UILabel npcNameLabel;
  [SerializeField]
  private UILabel textLabel;
  public static string dialogText;
  private int drawIndex;
  private float lastUpdate;
  private bool drawFinished;

  public void Init(string npcName, string text)
  {
    this.npcNameLabel.text = npcName;
    DialogNpcEntity.dialogText = text;
    this.lastUpdate = Time.time;
  }

  public void OnClick()
  {
    if (this.textLabel.text.Length != DialogNpcEntity.dialogText.Length)
    {
      this.textLabel.text = DialogNpcEntity.dialogText;
    }
    else
    {
      if (this.handleClick == null)
        return;
      this.handleClick();
    }
  }

  public static GameObject Create(string npcNameString, string text, GameObject parent)
  {
    GameObject gameObject1 = new GameObject("npc_entity");
    PositionUtils.CorrectTransform(parent, gameObject1);
    DialogNpcEntity dialogNpcEntity = gameObject1.AddComponent<DialogNpcEntity>();
    GameObject gameObject2 = new GameObject("_text");
    PositionUtils.CorrectTransform(gameObject1, gameObject2);
    UILabel uiLabel1 = GuiWidgetsFactory.AddLabel(gameObject2, new Vector2(945f, 16f), ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_NPC_TEXT), 5, 16, UIWidget.Pivot.TopLeft, UILabel.Overflow.ResizeHeight);
    gameObject2.transform.localPosition = new Vector3(0.0f, -28f, 0.0f);
    uiLabel1.spacingY = 4;
    GameObject gameObject3 = new GameObject("_npcName");
    PositionUtils.CorrectTransform(gameObject1, gameObject3);
    UILabel uiLabel2 = GuiWidgetsFactory.AddLabel(gameObject3, new Vector2(600f, 16f), ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_NPC_NAME), 5, 16, UIWidget.Pivot.TopLeft, UILabel.Overflow.ResizeHeight);
    gameObject3.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    dialogNpcEntity.textLabel = uiLabel1;
    dialogNpcEntity.npcNameLabel = uiLabel2;
    NGUITools.AddWidgetCollider(gameObject1);
    BoxCollider component = gameObject1.GetComponent<BoxCollider>();
    component.center = new Vector3(500f, -250f);
    component.size = new Vector3(4000f, 500f, 0.0f);
    dialogNpcEntity.Init(npcNameString, text);
    return gameObject1;
  }

  public void LateUpdate()
  {
    if (this.drawFinished)
      return;
    float time = Time.time;
    int num1 = (int) ((double) (time - this.lastUpdate) * 75.0);
    int num2 = 0;
    while (DialogNpcEntity.dialogText != null && this.textLabel.text.Length != DialogNpcEntity.dialogText.Length && num2 <= num1)
    {
      ++num2;
      this.textLabel.text += (string) (object) DialogNpcEntity.dialogText[this.drawIndex];
      ++this.drawIndex;
    }
    if (num1 > 0)
      this.lastUpdate = time;
    if (this.textLabel.text.Length != DialogNpcEntity.dialogText.Length)
      return;
    this.drawFinished = true;
  }
}
