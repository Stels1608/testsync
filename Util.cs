﻿// Decompiled with JetBrains decompiler
// Type: Util
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Util
{
  public static bool IsSaneNumber(float f)
  {
    return !float.IsNaN(f) && (double) f != double.PositiveInfinity && ((double) f != double.NegativeInfinity && (double) f <= 999999995904.0) && (double) f >= -999999995904.0;
  }

  public static Vector3 Clamp(Vector3 v, float length)
  {
    float magnitude = v.magnitude;
    if ((double) magnitude > (double) length)
      return v / magnitude * length;
    return v;
  }

  public static float Mod(float x, float period)
  {
    float num = x % period;
    if ((double) num >= 0.0)
      return num;
    return num + period;
  }

  public static int Mod(int x, int period)
  {
    int num = x % period;
    if (num >= 0)
      return num;
    return num + period;
  }

  public static float Mod(float x)
  {
    return Util.Mod(x, 1f);
  }

  public static int Mod(int x)
  {
    return Util.Mod(x, 1);
  }

  public static float CyclicDiff(float high, float low, float period, bool skipWrap)
  {
    if (!skipWrap)
    {
      high = Util.Mod(high, period);
      low = Util.Mod(low, period);
    }
    if ((double) high >= (double) low)
      return high - low;
    return high + period - low;
  }

  public static int CyclicDiff(int high, int low, int period, bool skipWrap)
  {
    if (!skipWrap)
    {
      high = Util.Mod(high, period);
      low = Util.Mod(low, period);
    }
    if (high >= low)
      return high - low;
    return high + period - low;
  }

  public static float CyclicDiff(float high, float low, float period)
  {
    return Util.CyclicDiff(high, low, period, false);
  }

  public static int CyclicDiff(int high, int low, int period)
  {
    return Util.CyclicDiff(high, low, period, false);
  }

  public static float CyclicDiff(float high, float low)
  {
    return Util.CyclicDiff(high, low, 1f, false);
  }

  public static int CyclicDiff(int high, int low)
  {
    return Util.CyclicDiff(high, low, 1, false);
  }

  public static bool CyclicIsLower(float compared, float comparedTo, float reference, float period)
  {
    compared = Util.Mod(compared, period);
    comparedTo = Util.Mod(comparedTo, period);
    return (double) Util.CyclicDiff(compared, reference, period, true) < (double) Util.CyclicDiff(comparedTo, reference, period, true);
  }

  public static bool CyclicIsLower(int compared, int comparedTo, int reference, int period)
  {
    compared = Util.Mod(compared, period);
    comparedTo = Util.Mod(comparedTo, period);
    return Util.CyclicDiff(compared, reference, period, true) < Util.CyclicDiff(comparedTo, reference, period, true);
  }

  public static bool CyclicIsLower(float compared, float comparedTo, float reference)
  {
    return Util.CyclicIsLower(compared, comparedTo, reference, 1f);
  }

  public static bool CyclicIsLower(int compared, int comparedTo, int reference)
  {
    return Util.CyclicIsLower(compared, comparedTo, reference, 1);
  }

  public static float CyclicLerp(float a, float b, float t, float period)
  {
    if ((double) Mathf.Abs(b - a) <= (double) period / 2.0)
      return (float) ((double) a * (1.0 - (double) t) + (double) b * (double) t);
    if ((double) b < (double) a)
      a -= period;
    else
      b -= period;
    return Util.Mod((float) ((double) a * (1.0 - (double) t) + (double) b * (double) t));
  }

  public static Vector3 ProjectOntoPlane(Vector3 v, Vector3 normal)
  {
    return v - Vector3.Project(v, normal);
  }

  public static Vector3 SetHeight(Vector3 originalVector, Vector3 referenceHeightVector, Vector3 upVector)
  {
    return Util.ProjectOntoPlane(originalVector, upVector) + Vector3.Project(referenceHeightVector, upVector);
  }

  public static Vector3 GetHighest(Vector3 a, Vector3 b, Vector3 upVector)
  {
    if ((double) Vector3.Dot(a, upVector) >= (double) Vector3.Dot(b, upVector))
      return a;
    return b;
  }

  public static Vector3 GetLowest(Vector3 a, Vector3 b, Vector3 upVector)
  {
    if ((double) Vector3.Dot(a, upVector) <= (double) Vector3.Dot(b, upVector))
      return a;
    return b;
  }

  public static Matrix4x4 RelativeMatrix(Transform t, Transform relativeTo)
  {
    return relativeTo.worldToLocalMatrix * t.localToWorldMatrix;
  }

  public static Vector3 TransformVector(Matrix4x4 m, Vector3 v)
  {
    return m.MultiplyPoint(v) - m.MultiplyPoint(Vector3.zero);
  }

  public static Vector3 TransformVector(Transform t, Vector3 v)
  {
    return Util.TransformVector(t.localToWorldMatrix, v);
  }

  public static void TransformFromMatrix(Matrix4x4 matrix, Transform trans)
  {
    trans.rotation = Util.QuaternionFromMatrix(matrix);
    trans.position = (Vector3) matrix.GetColumn(3);
  }

  public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
  {
    Quaternion quaternion = new Quaternion();
    quaternion.w = Mathf.Sqrt(Mathf.Max(0.0f, 1f + m[0, 0] + m[1, 1] + m[2, 2])) / 2f;
    quaternion.x = Mathf.Sqrt(Mathf.Max(0.0f, 1f + m[0, 0] - m[1, 1] - m[2, 2])) / 2f;
    quaternion.y = Mathf.Sqrt(Mathf.Max(0.0f, 1f - m[0, 0] + m[1, 1] - m[2, 2])) / 2f;
    quaternion.z = Mathf.Sqrt(Mathf.Max(0.0f, 1f - m[0, 0] - m[1, 1] + m[2, 2])) / 2f;
    quaternion.x *= Mathf.Sign(quaternion.x * (m[2, 1] - m[1, 2]));
    quaternion.y *= Mathf.Sign(quaternion.y * (m[0, 2] - m[2, 0]));
    quaternion.z *= Mathf.Sign(quaternion.z * (m[1, 0] - m[0, 1]));
    return quaternion;
  }

  public static Matrix4x4 MatrixFromQuaternion(Quaternion q)
  {
    return Util.CreateMatrix(q * Vector3.right, q * Vector3.up, q * Vector3.forward, Vector3.zero);
  }

  public static Matrix4x4 MatrixFromQuaternionPosition(Quaternion q, Vector3 p)
  {
    Matrix4x4 matrix4x4 = Util.MatrixFromQuaternion(q);
    matrix4x4.SetColumn(3, (Vector4) p);
    matrix4x4[3, 3] = 1f;
    return matrix4x4;
  }

  public static Matrix4x4 MatrixSlerp(Matrix4x4 a, Matrix4x4 b, float t)
  {
    t = Mathf.Clamp01(t);
    Matrix4x4 matrix4x4 = Util.MatrixFromQuaternion(Quaternion.Slerp(Util.QuaternionFromMatrix(a), Util.QuaternionFromMatrix(b), t));
    matrix4x4.SetColumn(3, a.GetColumn(3) * (1f - t) + b.GetColumn(3) * t);
    matrix4x4[3, 3] = 1f;
    return matrix4x4;
  }

  public static Matrix4x4 CreateMatrix(Vector3 right, Vector3 up, Vector3 forward, Vector3 position)
  {
    Matrix4x4 identity = Matrix4x4.identity;
    identity.SetColumn(0, (Vector4) right);
    identity.SetColumn(1, (Vector4) up);
    identity.SetColumn(2, (Vector4) forward);
    identity.SetColumn(3, (Vector4) position);
    identity[3, 3] = 1f;
    return identity;
  }

  public static Matrix4x4 CreateMatrixPosition(Vector3 position)
  {
    Matrix4x4 identity = Matrix4x4.identity;
    identity.SetColumn(3, (Vector4) position);
    identity[3, 3] = 1f;
    return identity;
  }

  public static void TranslateMatrix(ref Matrix4x4 m, Vector3 position)
  {
    m.SetColumn(3, (Vector4) ((Vector3) m.GetColumn(3) + position));
    m[3, 3] = 1f;
  }

  public static Vector3 ConstantSlerp(Vector3 from, Vector3 to, float angle)
  {
    float t = Mathf.Min(1f, angle / Vector3.Angle(from, to));
    return Vector3.Slerp(from, to, t);
  }

  public static Quaternion ConstantSlerp(Quaternion from, Quaternion to, float angle)
  {
    float t = Mathf.Min(1f, angle / Quaternion.Angle(from, to));
    return Quaternion.Slerp(from, to, t);
  }

  public static Vector3 ConstantLerp(Vector3 from, Vector3 to, float length)
  {
    return from + Util.Clamp(to - from, length);
  }

  public static Vector3 Bezier(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float t)
  {
    Vector3 from = Vector3.Lerp(a, b, t);
    Vector3 vector3 = Vector3.Lerp(b, c, t);
    Vector3 to = Vector3.Lerp(c, d, t);
    return Vector3.Lerp(Vector3.Lerp(from, vector3, t), Vector3.Lerp(vector3, to, t), t);
  }

  public static GameObject Create3dText(Font font, string text, Vector3 position, float size, Color color)
  {
    GameObject gameObject = new GameObject("text_" + text);
    TextMesh textMesh = gameObject.AddComponent(typeof (TextMesh)) as TextMesh;
    gameObject.AddComponent(typeof (MeshRenderer));
    textMesh.font = font;
    gameObject.GetComponent<Renderer>().material = font.material;
    gameObject.GetComponent<Renderer>().material.color = color;
    textMesh.text = text;
    gameObject.transform.localScale = Vector3.one * size;
    gameObject.transform.Translate(position);
    return gameObject;
  }

  public static float[] GetLineSphereIntersections(Vector3 lineStart, Vector3 lineDir, Vector3 sphereCenter, float sphereRadius)
  {
    float sqrMagnitude = lineDir.sqrMagnitude;
    float num1 = (float) (2.0 * ((double) Vector3.Dot(lineStart, lineDir) - (double) Vector3.Dot(lineDir, sphereCenter)));
    float num2 = (float) ((double) lineStart.sqrMagnitude + (double) sphereCenter.sqrMagnitude - 2.0 * (double) Vector3.Dot(lineStart, sphereCenter) - (double) sphereRadius * (double) sphereRadius);
    float f = (float) ((double) num1 * (double) num1 - 4.0 * (double) sqrMagnitude * (double) num2);
    if ((double) f < 0.0)
      return (float[]) null;
    float num3 = (float) ((-(double) num1 - (double) Mathf.Sqrt(f)) / (2.0 * (double) sqrMagnitude));
    float num4 = (float) ((-(double) num1 + (double) Mathf.Sqrt(f)) / (2.0 * (double) sqrMagnitude));
    if ((double) num3 < (double) num4)
      return new float[2]{ num3, num4 };
    return new float[2]{ num4, num3 };
  }
}
