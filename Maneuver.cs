﻿// Decompiled with JetBrains decompiler
// Type: Maneuver
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public abstract class Maneuver : IComparable<Maneuver>, IProtocolRead
{
  protected MovementOptions options = new MovementOptions();
  protected byte isExclusive = 1;
  protected Tick startTick;

  public ManeuverType ManeuverType { get; set; }

  public MovementCard Card
  {
    set
    {
      this.options.ApplyCard(value);
    }
  }

  public byte IsExclusive
  {
    get
    {
      return this.isExclusive;
    }
  }

  public Maneuver()
  {
  }

  public Maneuver(Tick startTick)
  {
    this.startTick = startTick;
  }

  public Tick GetStartTick()
  {
    return this.startTick;
  }

  public Gear GetGear()
  {
    return this.options.gear;
  }

  public float GetMarchSpeed()
  {
    return this.options.speed;
  }

  public abstract MovementFrame NextFrame(Tick tick, MovementFrame prevFrame);

  public virtual void Read(BgoProtocolReader pr)
  {
    this.ManeuverType = (ManeuverType) pr.ReadByte();
  }

  public int CompareTo(Maneuver other)
  {
    int num = this.startTick.CompareTo(other.startTick);
    if (num == 0)
    {
      if (this.ManeuverType < other.ManeuverType)
        num = -1;
      else if (this.ManeuverType > other.ManeuverType)
        num = 1;
      else if (this.ManeuverType == other.ManeuverType)
        num = 0;
    }
    return num;
  }

  protected MovementFrame MoveToDirection(MovementFrame prevFrame, Euler3 direction)
  {
    if (!prevFrame.valid)
      return MovementFrame.Invalid;
    return Simulation.MoveToDirection(prevFrame, direction, this.options);
  }

  protected MovementFrame Drift(MovementFrame prevFrame)
  {
    if (!prevFrame.valid)
      return MovementFrame.Invalid;
    return Simulation.WASD(prevFrame, 0, 0, 0, this.options);
  }
}
