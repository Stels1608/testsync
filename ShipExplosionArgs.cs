﻿// Decompiled with JetBrains decompiler
// Type: ShipExplosionArgs
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class ShipExplosionArgs : ExplosionArgs
{
  public Faction Faction;
  public int Tier;

  public override string ToString()
  {
    return string.Format("ShipExplosionArgs: Faction: {0}, Tier: {1}", (object) this.Faction, (object) this.Tier);
  }
}
