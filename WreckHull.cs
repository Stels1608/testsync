﻿// Decompiled with JetBrains decompiler
// Type: WreckHull
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WreckHull : MonoBehaviour
{
  private void Start()
  {
    foreach (Renderer componentsInChild in this.gameObject.GetComponentsInChildren<Renderer>())
    {
      foreach (Material material in componentsInChild.materials)
      {
        if (material.HasProperty("_Damage"))
          material.SetFloat("_Damage", 1f);
        if (material.HasProperty("_GlowMap"))
          material.SetTexture("_GlowMap", (Texture) null);
      }
    }
  }
}
