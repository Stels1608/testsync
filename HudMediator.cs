﻿// Decompiled with JetBrains decompiler
// Type: HudMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public class HudMediator : Bigpoint.Core.Mvc.View.View<Message>, InputListener
{
  public new const string NAME = "HudMediator";
  private SettingsDataProvider settingsDataProvider;
  private List<IGUIRenderable> combatGui;

  public bool HandleJoystickAxesInput
  {
    get
    {
      return true;
    }
  }

  public bool HandleMouseInput
  {
    get
    {
      return false;
    }
  }

  public bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public HudMediator()
    : base("HudMediator")
  {
    this.combatGui = new List<IGUIRenderable>();
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.SettingChangedCameraMode);
    this.AddMessageInterest(Message.SettingChangedCombatGui);
    this.AddMessageInterest(Message.LoadingScreenDestroyed);
    this.AddMessageInterest(Message.SettingChangedShowFps);
    this.AddMessageInterest(Message.RegisterWindowManager);
    this.AddMessageInterest(Message.AddToCombatGui);
    this.AddMessageInterest(Message.RemoveFromCombatGui);
    this.AddMessageInterest(Message.ApplyCombatGui);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.settingsDataProvider = this.OwnerFacade.FetchDataProvider("SettingsDataProvider") as SettingsDataProvider;
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.LoadNewLevel:
        FacadeFactory.GetInstance().SendMessage(Message.EnableTargetting, (object) true);
        break;
      case Message.LoadingScreenDestroyed:
        Game.InputDispatcher.AddListener((InputListener) this);
        this.ToggleCombatGui(this.settingsDataProvider.CurrentSettings.CombatGui);
        GuiOutpostProgress.ShowFps = this.settingsDataProvider.CurrentSettings.ShowFps;
        this.OwnerFacade.SendMessage(Message.LevelUiInstalled);
        break;
      default:
        switch (id - 65)
        {
          case Message.None:
            this.AddToCombatGui((IGUIRenderable) message.Data);
            return;
          case Message.ChangeSetting:
            this.RemoveFromCombatGui((IGUIRenderable) message.Data);
            return;
          case Message.ApplySettings:
            this.ApplyCombatGui();
            return;
          default:
            if (id != Message.SettingChangedCombatGui)
            {
              if (id != Message.SettingChangedShowFps)
                return;
              GuiOutpostProgress.ShowFps = (bool) message.Data;
              return;
            }
            this.ToggleCombatGui((bool) message.Data);
            return;
        }
    }
  }

  private void AddToCombatGui(IGUIRenderable renderable)
  {
    if (this.combatGui.Contains(renderable))
      return;
    this.combatGui.Add(renderable);
    bool combatGui = this.settingsDataProvider.CurrentSettings.CombatGui;
    renderable.IsRendered = combatGui;
  }

  private void ApplyCombatGui()
  {
    this.ToggleCombatGui(this.settingsDataProvider.CurrentSettings.CombatGui);
  }

  private void RemoveFromCombatGui(IGUIRenderable renderable)
  {
    this.combatGui.Remove(renderable);
  }

  private void ToggleCombatGui(bool show)
  {
    if ((Object) SpaceLevel.GetLevel() == (Object) null)
      return;
    using (List<IGUIRenderable>.Enumerator enumerator = this.combatGui.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IGUIRenderable current = enumerator.Current;
        current.IsRendered = (!(current is GuiMailUnread) || Game.Me.MailBox.Count != 0) && show;
      }
    }
    if (Game.GUIManager.Find<GUISmallPaperdoll>() != null)
      Game.GUIManager.Find<GUISmallPaperdoll>().UpdateControlLabels();
    if (Game.GUIManager.Find<GuiThreat>() != null)
      Game.GUIManager.Find<GuiThreat>().IsCombatGuiShown = show;
    if (!((Object) SpaceLevel.GetLevel() != (Object) null))
      return;
    Game.GUIManager.Find<GUIChatNew>().IsRendered = !SpaceLevel.GetLevel().IsTutorialMission && show;
  }

  public bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public void OnMouseMove(float2 mousePosition)
  {
  }

  public bool OnMouseScrollDown()
  {
    return false;
  }

  public bool OnMouseScrollUp()
  {
    return false;
  }

  public bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (action != Action.ToggleCombatGUI && action != Action.ToggleFps && action != Action.ToggleTournamentRanking)
      return action == Action.ToggleAdvancedFlightControls;
    return true;
  }

  public bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (action == Action.ToggleCombatGUI)
    {
      this.OwnerFacade.SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CombatGui, (object) !this.settingsDataProvider.CurrentSettings.CombatGui, true));
      return true;
    }
    if (action == Action.ToggleFps)
    {
      this.OwnerFacade.SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.ShowFpsAndPing, (object) !GuiOutpostProgress.ShowFps, true));
      return true;
    }
    if (action == Action.ToggleTournamentRanking)
    {
      this.OwnerFacade.SendMessage(Message.ShowTournamentRanking);
      return true;
    }
    if (action != Action.ToggleAdvancedFlightControls)
      return false;
    this.OwnerFacade.SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.AdvancedFlightControls, (object) !this.settingsDataProvider.CurrentSettings.AdvancedFlightControls, true));
    return true;
  }

  public bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    return false;
  }
}
