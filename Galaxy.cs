﻿// Decompiled with JetBrains decompiler
// Type: Galaxy
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Galaxy
{
  private readonly Dictionary<Faction, float> RCP = new Dictionary<Faction, float>();
  public int pegasusSectorId = -1;
  public int basestarSectorId = -1;
  private readonly Dictionary<int, int> sectorScalingMultiplier = new Dictionary<int, int>();
  private Dictionary<uint, SectorDesc> galaxyStars;
  private UniverseProtocol protocol;
  private int capitalShipCost;

  public int BaseScalingMultiplier { get; set; }

  public DateTime BasestarExpire { get; private set; }

  public DateTime PegasusExpire { get; private set; }

  public float ColonialRCPoints
  {
    get
    {
      return this.RCP[Faction.Colonial];
    }
  }

  public float CylonRCPoints
  {
    get
    {
      return this.RCP[Faction.Cylon];
    }
  }

  public int CapitalShipCost
  {
    get
    {
      return this.capitalShipCost;
    }
  }

  public bool GalaxyLoaded
  {
    get
    {
      return this.galaxyStars.Count > 0;
    }
  }

  public Galaxy()
  {
    this.RCP[Faction.Colonial] = 0.0f;
    this.RCP[Faction.Cylon] = 0.0f;
    this.galaxyStars = new Dictionary<uint, SectorDesc>();
    this.protocol = new UniverseProtocol(this);
    Game.ProtocolManager.RegisterProtocol((BgoProtocol) this.protocol);
  }

  public void UnSetShipEvent(Faction faction)
  {
    switch (faction)
    {
      case Faction.Colonial:
        this.pegasusSectorId = -1;
        this.PegasusExpire = DateTime.MinValue;
        break;
      case Faction.Cylon:
        this.basestarSectorId = -1;
        this.BasestarExpire = DateTime.MinValue;
        break;
    }
  }

  public void AddSectorScalingMultiplier(int rcpDifference, int multiplier)
  {
    this.sectorScalingMultiplier[rcpDifference] = multiplier;
  }

  public float GetRemainingCapitalTime(Faction faction)
  {
    DateTime dateTime = DateTime.UtcNow;
    switch (faction)
    {
      case Faction.Colonial:
        dateTime = this.PegasusExpire;
        break;
      case Faction.Cylon:
        dateTime = this.BasestarExpire;
        break;
      default:
        Debug.LogError((object) ("[Galaxy] Invalid faction for GetRemainingCapitalTime: " + (object) faction));
        break;
    }
    return (float) (dateTime - Game.TimeSync.ServerDateTime).TotalSeconds;
  }

  public int GetScalingMultiplier()
  {
    float f = (Game.Me.Faction != Faction.Colonial ? this.RCP[Faction.Cylon] : this.RCP[Faction.Colonial]) - (Game.Me.Faction != Faction.Colonial ? this.RCP[Faction.Colonial] : this.RCP[Faction.Cylon]);
    float num1 = Mathf.Abs(f);
    List<int> list = this.sectorScalingMultiplier.Keys.ToList<int>();
    list.Sort();
    list.Reverse();
    int num2 = 0;
    using (List<int>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        if ((double) num1 >= (double) current)
        {
          num2 = this.sectorScalingMultiplier[current];
          break;
        }
      }
    }
    int num3 = num2 * this.BaseScalingMultiplier;
    if ((double) f > 0.0)
      num3 *= -1;
    return num3;
  }

  public void SetShipEvent(Faction faction, int sectorId, DateTime expireTime)
  {
    switch (faction)
    {
      case Faction.Colonial:
        this.pegasusSectorId = sectorId;
        this.PegasusExpire = expireTime;
        break;
      case Faction.Cylon:
        this.basestarSectorId = sectorId;
        this.BasestarExpire = expireTime;
        break;
    }
  }

  public void SetStars(SectorDesc[] starDescs)
  {
    foreach (SectorDesc starDesc in starDescs)
      this.galaxyStars[starDesc.Id] = starDesc;
  }

  public void SetRCP(Faction faction, float rcp)
  {
    this.RCP[faction] = rcp;
  }

  public void SetCapitalShipCost(int price)
  {
    this.capitalShipCost = price;
  }

  public void SetSectorOP(uint sectorID, Faction faction, int OP)
  {
    if (!this.galaxyStars.ContainsKey(sectorID))
      this.galaxyStars.Add(sectorID, new SectorDesc(sectorID));
    if (faction == Faction.Colonial)
      this.galaxyStars[sectorID].ColonialOutpostPoints = OP;
    else
      this.galaxyStars[sectorID].CylonOutpostPoints = OP;
  }

  public IEnumerable<SectorDesc> GetStars()
  {
    return (IEnumerable<SectorDesc>) this.galaxyStars.Values;
  }

  public SectorDesc GetStar(uint id)
  {
    if (this.galaxyStars.ContainsKey(id))
      return this.galaxyStars[id];
    return (SectorDesc) null;
  }

  public void SubscribeGalaxyMap()
  {
    if (Game.Me.Faction == Faction.Neutral)
      return;
    this.protocol.SubscribeGalaxyMap();
  }

  public void UnsubscribeGalaxyMap()
  {
    if (Game.Me.Faction == Faction.Neutral)
      return;
    this.protocol.UnsubscribeGalaxyMap();
  }
}
