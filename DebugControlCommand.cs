﻿// Decompiled with JetBrains decompiler
// Type: DebugControlCommand
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class DebugControlCommand
{
  public static bool ExecuteAbility(string[] commands)
  {
    ushort result = 0;
    if (!ushort.TryParse(commands[0], out result))
      return false;
    GameProtocol.GetProtocol().CastSlotAbility(result, new uint[0]);
    return true;
  }
}
