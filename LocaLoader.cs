﻿// Decompiled with JetBrains decompiler
// Type: LocaLoader
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class LocaLoader : MonoBehaviour
{
  public bool IsLocaLoaded;

  public static LocaLoader CreateAndStartLoading()
  {
    LocaLoader locaLoader = new GameObject().AddComponent<LocaLoader>();
    locaLoader.StartLoading();
    return locaLoader;
  }

  public void Destroy()
  {
    Object.Destroy((Object) this.gameObject);
  }

  public void StartLoading()
  {
    this.StartCoroutine(this.LoadLanguages());
  }

  [DebuggerHidden]
  public IEnumerator LoadLanguages()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LocaLoader.\u003CLoadLanguages\u003Ec__Iterator1D() { \u003C\u003Ef__this = this };
  }

  public static string[] GetLocaFiles()
  {
    return new string[7]{ "locale_0.xml", "locale_1.xml", "locale_2.xml", "locale_cdb_objects.xml", "locale_layouts.xml", "locale_dialogs.xml", "locale_sector_events.xml" };
  }
}
