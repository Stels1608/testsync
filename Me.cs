﻿// Decompiled with JetBrains decompiler
// Type: Me
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Paperdolls.PaperdollServerData;
using System;
using System.Collections.Generic;

public class Me
{
  public string ChatSessionId = "-1";
  public string ChatLanguage = "en";
  public string ChatserverUrl = string.Empty;
  public string Name = string.Empty;
  public byte Level = byte.MaxValue;
  public uint Experience = 16777215;
  public uint ServerID;
  public uint ChatProjectID;
  public bool ChatGlobal;
  public Faction Faction;
  public FactionGroup FactionGroup;
  public uint PrevLevelExperience;
  public uint NextLevelExperience;
  public uint SpentExperience;
  public AvatarDescription AvatarDesc;
  public Friends Friends;
  public Party Party;
  public Guild Guild;
  public Dictionary<uint, int> Counters;
  public Arena Arena;
  private bool tournamentParticipant;
  public bool Anchored;
  public uint AnchorTarget;
  public byte SectorId;
  public WaterExchangeValues WaterExchangeValues;
  private PlayerSocialState socialState;
  public Hold Hold;
  public Locker Locker;
  public PlayerHangar Hangar;
  public MyShipStats Stats;
  public MissionList MissionBook;
  public DutyList DutyBook;
  public SkillList SkillBook;
  public FactorList Factors;
  public MailList MailBox;
  public CapabilityList Caps;
  public Tuple<int, int> MeritCap;
  private readonly TargetSelectionDataProvider selectionDataProvider;

  public PlayerShip Ship
  {
    get
    {
      if ((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null)
        return (PlayerShip) null;
      return SpaceLevel.GetLevel().GetPlayerShip();
    }
  }

  public PlayerShip ActualShip
  {
    get
    {
      if ((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null)
        return (PlayerShip) null;
      return SpaceLevel.GetLevel().GetActualPlayerShip();
    }
  }

  public SpaceObject CurrentTarget
  {
    get
    {
      SpaceObject mainTarget = this.selectionDataProvider.GetMainTarget();
      if (mainTarget != null && mainTarget.Dead)
        return (SpaceObject) null;
      return mainTarget;
    }
  }

  public bool TournamentParticipant
  {
    get
    {
      return this.tournamentParticipant;
    }
    set
    {
      this.tournamentParticipant = value;
      if (!value)
        return;
      FacadeFactory.GetInstance().SendMessage(Message.TournamentJoined);
    }
  }

  public HangarShip ActiveShip
  {
    get
    {
      return this.Hangar.ActiveShip;
    }
  }

  public uint FreeExperience
  {
    get
    {
      return this.Experience - this.SpentExperience;
    }
  }

  public float NormalExperience
  {
    get
    {
      float num1 = (float) (this.Experience - this.PrevLevelExperience);
      float num2 = (float) (this.NextLevelExperience - this.PrevLevelExperience);
      if ((double) num2 > 0.0)
        return num1 / num2;
      return 0.0f;
    }
  }

  public string Title
  {
    get
    {
      Duty selected = this.DutyBook.Selected;
      if (selected != null && (bool) selected.Card.IsLoaded)
        return selected.Card.TitleCard.Name;
      return string.Empty;
    }
  }

  public float FTLTyliumRange
  {
    get
    {
      return (float) Game.Me.Hold.Tylium / this.Stats.FTLCost;
    }
  }

  public string Rank
  {
    get
    {
      if ((int) this.Level == (int) byte.MaxValue)
        return string.Empty;
      return BsgoLocalization.GetForLevel("bgo.common.rank." + (this.Faction != Faction.Colonial ? "cylon" : "colonial"), (int) this.Level);
    }
  }

  public string RankShort
  {
    get
    {
      if ((int) this.Level == (int) byte.MaxValue)
        return string.Empty;
      return BsgoLocalization.GetForLevel("bgo.common.rank_short." + (this.Faction != Faction.Colonial ? "cylon" : "colonial"), (int) this.Level);
    }
  }

  public PlayerSocialState SocialState
  {
    get
    {
      if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null && SpaceLevel.GetLevel().IsTournament)
        return PlayerSocialState.DoNotDisturb;
      return this.socialState;
    }
    set
    {
      this.socialState = value;
    }
  }

  public bool Dead
  {
    get
    {
      if (this.Ship != null)
        return this.Ship.Dead;
      return false;
    }
  }

  public Me()
  {
    this.Hangar = new PlayerHangar();
    this.Friends = new Friends();
    this.Party = new Party();
    this.Guild = new Guild();
    this.Hold = new Hold();
    this.Locker = new Locker();
    this.MissionBook = new MissionList();
    this.DutyBook = new DutyList();
    this.Stats = new MyShipStats(this.ServerID);
    this.SkillBook = new SkillList();
    this.Counters = new Dictionary<uint, int>();
    this.Factors = new FactorList();
    this.MailBox = new MailList();
    this.Caps = new CapabilityList();
    this.Arena = new Arena();
    this.WaterExchangeValues = new WaterExchangeValues();
    this.MeritCap = new Tuple<int, int>(0, 0);
    this.selectionDataProvider = (TargetSelectionDataProvider) FacadeFactory.GetInstance().FetchDataProvider("TargetSelectionProvider");
  }

  public void Follow()
  {
    ShipImmutableSlot followSlot = this.GetFollowSlot();
    if (followSlot.Ability == null)
      return;
    followSlot.Ability.Cast();
  }

  [Obsolete("Use Follow() with ability target check instead", false)]
  public void Follow(uint objectId)
  {
    GameProtocol.GetProtocol().CastImmutableSlotAbility(this.GetFollowSlot().SlotId, new uint[1]{ objectId });
  }

  private ShipImmutableSlot GetFollowSlot()
  {
    using (List<ShipImmutableSlot>.Enumerator enumerator = this.ActiveShip.Card.ImmutableSlots.GetEnumerator())
    {
      if (enumerator.MoveNext())
        return enumerator.Current;
    }
    return (ShipImmutableSlot) null;
  }

  public void AutoAllocateConsumables()
  {
    if (this.Hold.Consumables.Count == 0)
      return;
    foreach (ShipSlot slot in this.ActiveShip.Slots)
    {
      if (slot.Ability != null && (int) slot.Ability.ConsumableCount == 0 && slot.Ability.card.ConsumableOption != ShipConsumableOption.NotUsing)
      {
        ItemCountable itemCountable = (ItemCountable) null;
        for (int index = 0; index < this.Hold.Consumables.Count; ++index)
        {
          ItemCountable two = (ItemCountable) this.Hold.Consumables[index];
          if ((int) two.Card.ConsumableType == (int) slot.Ability.card.ConsumableType && (int) two.Card.Tier == (int) slot.Ability.card.ConsumableTier && (itemCountable == null || this.IsCheaper(itemCountable, two)))
            itemCountable = two;
        }
        if (itemCountable != null)
          slot.SelectConsumable(itemCountable);
      }
    }
  }

  private bool IsCheaper(ItemCountable one, ItemCountable two)
  {
    if (two.ShopItemCard.BuyPrice.items.Count > 0 && one.ShopItemCard.BuyPrice.items.Count > 0)
      return (double) two.ShopItemCard.BuyPrice.items[new List<ShipConsumableCard>((IEnumerable<ShipConsumableCard>) two.ShopItemCard.BuyPrice.items.Keys)[0]] < (double) one.ShopItemCard.BuyPrice.items[new List<ShipConsumableCard>((IEnumerable<ShipConsumableCard>) one.ShopItemCard.BuyPrice.items.Keys)[0]];
    return false;
  }

  public int Counter(CounterStore guid)
  {
    int num;
    if (this.Counters.TryGetValue((uint) guid, out num))
      return num;
    return 0;
  }

  public int GetSkillLevel(uint skillHash)
  {
    foreach (PlayerSkill playerSkill in (IEnumerable<PlayerSkill>) Game.Me.SkillBook)
    {
      if ((int) playerSkill.Card.Hash == (int) skillHash)
        return (int) playerSkill.Card.Level;
    }
    return -1;
  }

  public PlayerSkill GetSkillByHash(uint skillHash)
  {
    foreach (PlayerSkill playerSkill in (IEnumerable<PlayerSkill>) Game.Me.SkillBook)
    {
      if ((bool) playerSkill.IsLoaded && (int) playerSkill.Card.Hash == (int) skillHash)
        return playerSkill;
    }
    return (PlayerSkill) null;
  }

  public bool RequiresSkillsForSystem(ShipSystem system, int systemLevel, out int skillLevel, out List<PlayerSkill> needSkills)
  {
    skillLevel = systemLevel > 2 ? (systemLevel < 3 || systemLevel > 4 ? (systemLevel < 5 || systemLevel > 6 ? (systemLevel < 7 || systemLevel > 8 ? (systemLevel != 9 ? 5 : 4) : 3) : 2) : 1) : 0;
    needSkills = new List<PlayerSkill>();
    bool flag = true;
    foreach (uint skillHash in system.Card.SkillHashes)
    {
      PlayerSkill skillByHash = Game.Me.GetSkillByHash(skillHash);
      if (skillByHash != null && (bool) skillByHash.Card.IsLoaded)
      {
        if (skillByHash.Level < skillLevel)
          flag = false;
        needSkills.Add(skillByHash);
      }
    }
    return flag;
  }

  public bool AlreadyHaveThisItem(ShipItem item)
  {
    if (this.Hold.GetByGUID(item.CardGUID) != null || this.Locker.GetByGUID(item.CardGUID) != null)
      return true;
    if (item is ShipSystem)
    {
      foreach (HangarShip ship in this.Hangar.Ships)
      {
        foreach (ShipSlot slot in ship.Slots)
        {
          if (slot.System != null && (int) slot.System.CardGUID == (int) item.CardGUID)
            return true;
        }
      }
    }
    return false;
  }

  public int GetEquippedCount(ShipSystem shipSystem)
  {
    int num = 0;
    ShipSystemCard maxLevelCard1 = shipSystem.GetMaxLevelCard();
    foreach (ShipSlot slot in this.ActiveShip.Slots)
    {
      if (slot.System != null)
      {
        ShipSystemCard maxLevelCard2 = slot.System.GetMaxLevelCard();
        if ((int) maxLevelCard1.CardGUID == (int) maxLevelCard2.CardGUID)
          ++num;
      }
    }
    return num;
  }

  public void Reset()
  {
    this.Hangar.Clear();
    this.MissionBook._Clear();
    this.DutyBook._Clear();
    this.Hold._Clear();
    this.Locker._Clear();
    this.SkillBook._Clear();
    Game.Players._Clear();
    this.Counters.Clear();
    this.Factors._Clear();
    this.MailBox._Clear();
    this.Caps._Clear();
  }
}
