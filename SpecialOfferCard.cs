﻿// Decompiled with JetBrains decompiler
// Type: SpecialOfferCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class SpecialOfferCard : Card
{
  private string jsonKey = string.Empty;
  public string OfferIconPath;
  public string OfferImagePathColonial;
  public string OfferImagePathCylon;
  public uint ItemGroup;
  private string _name;
  private string _description;

  public string Name
  {
    get
    {
      return this._name ?? (this._name = this.Localize("%$bgo." + this.jsonKey + ".Name%"));
    }
  }

  public string Description
  {
    get
    {
      return this._description ?? (this._description = this.Localize("%$bgo." + this.jsonKey + ".Description%"));
    }
  }

  public SpecialOfferCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  private string Localize(string languageKey)
  {
    string str = (string) null;
    if (BsgoLocalization.TryGet(languageKey, out str))
      return str;
    return languageKey;
  }

  public override void Read(BgoProtocolReader r)
  {
    this.jsonKey = r.ReadString();
    this.OfferIconPath = r.ReadString();
    this.OfferImagePathColonial = r.ReadString();
    this.OfferImagePathCylon = r.ReadString();
    this.ItemGroup = r.ReadUInt32();
  }

  public override string ToString()
  {
    return string.Format("[SpecialOfferCard] Name: {0}, Description: {1}, OfferIconPath: {2}, OfferImagePathColonial: {3}, OfferImagePathCylon: {4}, ItemGroup: {5}", (object) this.Name, (object) this.Description, (object) this.OfferIconPath, (object) this.OfferImagePathColonial, (object) this.OfferImagePathCylon, (object) this.ItemGroup);
  }
}
