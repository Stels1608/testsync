﻿// Decompiled with JetBrains decompiler
// Type: StarterCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class StarterCard : Card
{
  public string ShipTexture;
  public ShipCard ShipCard;
  public Faction Faction;
  public GUICard GUI;
  public byte firePower;
  public byte toughNess;
  public byte speed;
  public byte electronicWarfare;

  public StarterCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    r.ReadString();
    r.ReadString();
    this.Faction = (Faction) r.ReadByte();
    this.ShipTexture = r.ReadString();
    uint cardGUID = r.ReadGUID();
    this.firePower = r.ReadByte();
    this.toughNess = r.ReadByte();
    this.speed = r.ReadByte();
    this.electronicWarfare = r.ReadByte();
    this.ShipCard = (ShipCard) Game.Catalogue.FetchCard(cardGUID, CardView.Ship);
    this.GUI = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
  }
}
