﻿// Decompiled with JetBrains decompiler
// Type: SettingsHudMisc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class SettingsHudMisc : SettingsGeneralGroup, ILocalizeable
{
  [SerializeField]
  private UILabel optionsHeadlineLabel;
  [SerializeField]
  private GameObject content;

  private void Awake()
  {
    using (List<UserSetting>.Enumerator enumerator = new List<UserSetting>() { UserSetting.ShowFpsAndPing, UserSetting.ShowDamageOverlay, UserSetting.StatsIndication, UserSetting.ShowPopups, UserSetting.ShowXpBar }.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        this.CoveredSettings.Add(current);
        OptionButtonElement buttonElement = NguiWidgetFactory.Options.CreateButtonElement(this.content);
        this.controls.Add(current, (OptionsElement) buttonElement);
      }
    }
    this.content.GetComponent<UITable>().Reposition();
  }

  private void Start()
  {
    this.ReloadLanguageData();
  }

  public void ReloadLanguageData()
  {
    this.optionsHeadlineLabel.text = Tools.ParseMessage("%$bgo.ui.options.hud.misc%");
  }
}
