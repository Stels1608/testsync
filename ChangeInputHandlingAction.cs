﻿// Decompiled with JetBrains decompiler
// Type: ChangeInputHandlingAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class ChangeInputHandlingAction : Action<Message>
{
  private InputDataProvider inputDataProvider;

  public override void Execute(IMessage<Message> message)
  {
    this.inputDataProvider = (InputDataProvider) this.OwnerFacade.FetchDataProvider("InputDataProvider");
    InputMessageContent inputMessageContent = (InputMessageContent) message.Data;
    InputMessage inputMessage = inputMessageContent.InputMessage;
    object data = inputMessageContent.Data;
    switch (inputMessage)
    {
      case InputMessage.ToggleMouseCameraControl:
        this.ToggleMouseCameraControl((bool) data);
        break;
      case InputMessage.ToggleHotKeyInput:
        this.ToggleHotKeyInput((bool) data);
        break;
    }
  }

  private void ToggleMouseCameraControl(bool isActiv)
  {
    if (isActiv)
    {
      this.inputDataProvider.InputDispatcher.AddListener((InputListener) this.inputDataProvider.SpaceCamera);
      Debug.Log((object) "ToggleMouseCameraControl on");
    }
    else
    {
      this.inputDataProvider.InputDispatcher.RemoveListener((InputListener) this.inputDataProvider.SpaceCamera);
      Debug.Log((object) "ToggleMouseCameraControl off");
    }
  }

  private void ToggleHotKeyInput(bool isActiv)
  {
    if (isActiv)
      Debug.Log((object) "ToggleMouseCameraControl on");
    else
      Debug.Log((object) "ToggleMouseCameraControl off");
  }
}
