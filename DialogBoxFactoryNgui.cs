﻿// Decompiled with JetBrains decompiler
// Type: DialogBoxFactoryNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class DialogBoxFactoryNgui
{
  private static string uiElementPath = "GUI/gui_2013/ui_elements";

  private static void PackAndFire(List<GameObject> content, int _yPadding = 0, bool _hideDialogBoxBackgrounds = false)
  {
    FacadeFactory.GetInstance().SendMessage(Message.ShowDialogBox, (object) new DialogBoxData()
    {
      contentData = content,
      yPadding = _yPadding,
      hideDialogBoxBackgrounds = _hideDialogBoxBackgrounds
    });
  }

  public static void CreateInputBindingDialogBox(IInputBinding currentBinding)
  {
    GameObject gameObject;
    string title;
    switch (currentBinding.Device)
    {
      case InputDevice.KeyboardMouse:
        gameObject = (GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingDialogBoxContentKeyboard"));
        title = "%$bgo.ui.options.popup.keybinding.title_key_remap%";
        break;
      case InputDevice.Joystick:
        gameObject = (GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingDialogBoxContentJoystick"));
        title = "%$bgo.ui.options.popup.keybinding.title_joystick_input%";
        break;
      default:
        Debug.LogError((object) ("No valid device given: " + (object) currentBinding.Device));
        return;
    }
    GameObject titleElement = DialogBoxFactoryNgui.CreateTitleElement(title);
    InputBindingDialogBoxContentBase component = gameObject.GetComponent<InputBindingDialogBoxContentBase>();
    if ((Object) component != (Object) null)
      component.Setup(currentBinding);
    SettingsDataProvider settingsDataProvider = FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider") as SettingsDataProvider;
    if (settingsDataProvider.InputBinderCache.InputBindings.Count == 0)
      settingsDataProvider.InputBinderCache.MergeWith(settingsDataProvider.InputBinder.InputBindings);
    GameObject buttonBarElement = DialogBoxFactoryNgui.CreateTwoButtonBarElement(new AnonymousDelegate(((DialogBoxContent) component).OnAccept), new AnonymousDelegate(((DialogBoxContent) component).OnCancel), (string) null, (string) null);
    DialogBoxFactoryNgui.PackAndFire(new List<GameObject>()
    {
      titleElement,
      gameObject,
      buttonBarElement
    }, 10, false);
  }

  public static void CreateSimpleDialogBox(string text, AnonymousDelegate acceptDelegate, AnonymousDelegate cancelDelegate, string titleText = "", string acceptText = null, string cancelText = null)
  {
    DialogBoxFactoryNgui.PackAndFire(new List<GameObject>()
    {
      DialogBoxFactoryNgui.CreateTitleElement(titleText),
      DialogBoxFactoryNgui.CreateSimpleTextElement(text),
      DialogBoxFactoryNgui.CreateTwoButtonBarElement(acceptDelegate, cancelDelegate, acceptText, cancelText)
    }, 10, false);
  }

  public static void CreateSimpleToggleSettingDialog(string text, AnonymousDelegate acceptDelegate, AnonymousDelegate cancelDelegate, UserSetting setting, string titleText = "", string acceptText = null, string cancelText = null)
  {
    GameObject titleElement = DialogBoxFactoryNgui.CreateTitleElement(titleText);
    GameObject simpleTextElement = DialogBoxFactoryNgui.CreateSimpleTextElement(text);
    GameObject gameObject = (GameObject) Object.Instantiate(Resources.Load(DialogBoxFactoryNgui.uiElementPath + "/general/NotShowAgainDialogBoxContent"));
    NotShowAgainDialogBoxContent component = gameObject.GetComponent<NotShowAgainDialogBoxContent>();
    if ((Object) component != (Object) null)
      component.SetSetting(setting);
    acceptDelegate += new AnonymousDelegate(component.OnAccept);
    cancelDelegate += new AnonymousDelegate(component.OnCancel);
    GameObject buttonBarElement = DialogBoxFactoryNgui.CreateTwoButtonBarElement(acceptDelegate, cancelDelegate, acceptText, cancelText);
    DialogBoxFactoryNgui.PackAndFire(new List<GameObject>()
    {
      titleElement,
      simpleTextElement,
      gameObject,
      buttonBarElement
    }, 10, false);
  }

  public static void CreateFtlResultDialog(FtlResultContainer ftlResult)
  {
    GameObject gameObject = (GameObject) Object.Instantiate(Resources.Load(DialogBoxFactoryNgui.uiElementPath + "/ftl/FtlResultDialogBoxContent"));
    FtlResultScreenDialogBoxContent component = gameObject.GetComponent<FtlResultScreenDialogBoxContent>();
    if ((Object) component != (Object) null)
      component.Init(ftlResult);
    component.ButtonBar.SetButtonDelegates((AnonymousDelegate) null);
    component.ButtonBar.InjectDifferentLoca(Tools.ParseMessage("%$bgo.common.ok%"));
    DialogBoxFactoryNgui.PackAndFire(new List<GameObject>()
    {
      gameObject
    }, 0, true);
  }

  public static ConstructDialogBoxContent CreateConstructDialog(string title, float timerDuration)
  {
    GameObject gameObject = (GameObject) Object.Instantiate(Resources.Load(DialogBoxFactoryNgui.uiElementPath + "/event_shop/ConstructDialog"));
    ConstructDialogBoxContent component = gameObject.GetComponent<ConstructDialogBoxContent>();
    component.GetComponentsInChildren<OneButtonBar>(true)[0].InjectDifferentLoca(Tools.ParseMessage("%$bgo.common.ok%"));
    gameObject.GetComponentInChildren<DialogBoxTitleElement>().Init(title);
    DialogBoxFactoryNgui.PackAndFire(new List<GameObject>()
    {
      gameObject
    }, 0, true);
    component.StartTimerBasedProgressBar(timerDuration);
    return component;
  }

  public static void CreateUnsavedChangesDialogBox(AnonymousDelegate acceptDelegate, AnonymousDelegate cancelDelegate)
  {
    DialogBoxFactoryNgui.CreateSimpleDialogBox("%$bgo.ui.options.popup.settings.unsafed%", acceptDelegate, cancelDelegate, "%$bgo.ui.options.popup.title%", "%$bgo.common.yes%", "%$bgo.common.no%");
  }

  public static void CreateResetSettingsDialogBox(AnonymousDelegate acceptDelegate)
  {
    DialogBoxFactoryNgui.CreateSimpleDialogBox("%$bgo.ui.options.popup.settings.reset%", acceptDelegate, (AnonymousDelegate) (() => {}), "%$bgo.ui.options.popup.title%", "%$bgo.common.yes%", "%$bgo.common.no%");
  }

  public static void CreateNotEnoughCubitsDialogBox(AnonymousDelegate cancelDelegate)
  {
    DialogBoxFactoryNgui.CreateSimpleDialogBox("%$bgo.wof.no_cubits%", (AnonymousDelegate) (() => ExternalApi.EvalJs("window.createPaymentSession();")), cancelDelegate, "%$bgo.common.cubits_c%", "%$bgo.common.yes%", "%$bgo.common.no%");
  }

  private static GameObject CreateTitleElement(string title)
  {
    GameObject gameObject = (GameObject) Object.Instantiate(Resources.Load(DialogBoxFactoryNgui.uiElementPath + "/general/DialogBoxTitleElement"));
    DialogBoxTitleElement component = gameObject.GetComponent<DialogBoxTitleElement>();
    if ((Object) component != (Object) null)
      component.Init(title);
    return gameObject;
  }

  private static GameObject CreateTwoButtonBarElement(AnonymousDelegate _acceptDelegate, AnonymousDelegate _cancelDelegate, string acceptText = null, string cancelText = null)
  {
    GameObject gameObject = (GameObject) Object.Instantiate(Resources.Load(DialogBoxFactoryNgui.uiElementPath + "/buttonbar/TwoButtonDialogBar"));
    TwoButtonBar component = gameObject.GetComponent<TwoButtonBar>();
    component.SetButtonDelegates(_cancelDelegate, _acceptDelegate);
    if (acceptText != null && cancelText != null)
      component.InjectDifferentLoca(acceptText, cancelText);
    return gameObject;
  }

  private static GameObject CreateSimpleTextElement(string text)
  {
    GameObject gameObject = (GameObject) Object.Instantiate(Resources.Load(DialogBoxFactoryNgui.uiElementPath + "/general/SimpleDialogBoxContent"));
    SimpleDialogBoxContent component = gameObject.GetComponent<SimpleDialogBoxContent>();
    if ((Object) component != (Object) null)
      component.Init(text);
    return gameObject;
  }
}
