﻿// Decompiled with JetBrains decompiler
// Type: GUIRadioGroup`1
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class GUIRadioGroup<T> : GUIPanel where T : GUIButtonNew
{
  public T Pressed
  {
    get
    {
      return this.FindRecoursively<T>((Predicate<T>) (button =>
      {
        if (button.IsRendered)
          return button.IsPressed;
        return false;
      }));
    }
  }

  public GUIRadioGroup()
  {
    this.IsRendered = true;
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (!this.HandleMouseInput)
      return false;
    if (mouseKey == KeyCode.Mouse0)
    {
      int num = -1;
      List<T> allRecoursively = this.FindAllRecoursively<T>((Predicate<T>) (button => button.IsRendered));
      for (int index = 0; index < allRecoursively.Count; ++index)
      {
        GUIButtonNew guiButtonNew = (GUIButtonNew) allRecoursively[index];
        if (guiButtonNew.Contains(mousePosition) && !guiButtonNew.IsPressed)
        {
          if (guiButtonNew.Handler != null)
            guiButtonNew.Handler();
          guiButtonNew.IsPressed = true;
          num = index;
        }
      }
      if (num != -1)
      {
        for (int index = 0; index < allRecoursively.Count; ++index)
        {
          if (index != num)
            allRecoursively[index].IsPressed = false;
        }
      }
      if (num != -1)
      {
        this.wasMouseDown = true;
        return true;
      }
    }
    if (!this.Contains(mousePosition))
      return false;
    this.wasMouseDown = true;
    return true;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (!this.HandleMouseInput || !this.wasMouseDown)
      return false;
    this.wasMouseDown = false;
    return this.Contains(mousePosition);
  }
}
