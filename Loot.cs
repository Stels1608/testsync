﻿// Decompiled with JetBrains decompiler
// Type: Loot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class Loot : SpaceObject
{
  private const float LOOT_DIST = 750f;

  public override string Name
  {
    get
    {
      return BsgoLocalization.Get("%$bgo.common.cargo_container%");
    }
  }

  public override UnityEngine.Sprite IconBrackets
  {
    get
    {
      return SystemMap3DMapIconLinker.Instance.MapObjectsNPCs[16];
    }
  }

  public Loot(uint objectID)
    : base(objectID)
  {
    this.HideStats = true;
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Position = r.ReadVector3();
  }

  public override void OnClicked()
  {
    if ((double) this.GetSqrDistance() >= 562500.0)
      return;
    GameProtocol.GetProtocol().RequestLoot(this.ObjectID);
  }
}
