﻿// Decompiled with JetBrains decompiler
// Type: LeaderboardEntryWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class LeaderboardEntryWidget : MonoBehaviour
{
  private const string FORMAT = "{0:0.###}";
  private const string PLUS_SIGN_FORMAT = "{0:0.###}";
  private static UnityEngine.Sprite colonialIcon;
  private static UnityEngine.Sprite cylonIcon;
  public Image crownIcon;
  public Text positionLabel;
  public Image factionIcon;
  public Text playerNameLabel;
  public Text score1Label;
  public Text score2Label;
  public Text score3Label;

  public static void LoadIcons()
  {
    LeaderboardEntryWidget.colonialIcon = Resources.Load<UnityEngine.Sprite>("GUI/Leaderboard/icon_colonial_mini");
    LeaderboardEntryWidget.cylonIcon = Resources.Load<UnityEngine.Sprite>("GUI/Leaderboard/icon_cylon_mini");
  }

  public void Display(RankDescription rank, bool showPlusSign)
  {
    this.crownIcon.gameObject.SetActive((int) rank.rank == 1);
    this.positionLabel.text = !rank.NotRankedYet ? ((int) rank.rank).ToString() + "." : "-";
    this.playerNameLabel.text = rank.name;
    this.score1Label.text = !rank.NotRankedYet ? string.Format("{0:0.###}", (object) rank.score1) : "-";
    this.score2Label.text = !rank.NotRankedYet ? string.Format("{0:0.###}", (object) rank.score2) : "-";
    this.score3Label.text = !rank.NotRankedYet ? string.Format("{0:0.###}", (object) rank.score3) : "-";
    if (showPlusSign && !rank.NotRankedYet)
    {
      this.score1Label.text = LeaderboardEntryWidget.AddPlusSign(rank.score1, this.score1Label.text);
      this.score2Label.text = LeaderboardEntryWidget.AddPlusSign(rank.score2, this.score2Label.text);
      this.score3Label.text = LeaderboardEntryWidget.AddPlusSign(rank.score3, this.score3Label.text);
    }
    if (rank.faction == Faction.Colonial)
      this.factionIcon.sprite = LeaderboardEntryWidget.colonialIcon;
    else if (rank.faction == Faction.Cylon)
      this.factionIcon.sprite = LeaderboardEntryWidget.cylonIcon;
    else
      Debug.LogWarning((object) ("Detected player in leaderboard which is neither Colonial nor Cylon: " + (object) rank.faction + " by the name of " + rank.name));
    if ((int) rank.playerId == (int) Game.Me.ServerID)
      this.SetTextColor(ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_SPECIAL));
    else
      this.SetTextColor(Color.white);
    Button component = this.GetComponent<Button>();
    if (!((Object) component != (Object) null))
      return;
    ColorBlock colors = component.colors;
    colors.colorMultiplier = 1f;
    component.colors = colors;
  }

  private static string AddPlusSign(double number, string numberString)
  {
    if (number > 0.0)
      return "+" + numberString;
    return numberString;
  }

  public void SetColumnEnabled(LeaderboardColumn column, bool enable)
  {
    Text text = (Text) null;
    switch (column)
    {
      case LeaderboardColumn.Column1:
        text = this.score1Label;
        break;
      case LeaderboardColumn.Column2:
        text = this.score2Label;
        break;
      case LeaderboardColumn.Column3:
        text = this.score3Label;
        break;
    }
    text.gameObject.SetActive(enable);
  }

  private void SetTextColor(Color color)
  {
    Text text = this.positionLabel;
    Color color1 = color;
    this.score3Label.color = color1;
    Color color2 = color1;
    this.score2Label.color = color2;
    Color color3 = color2;
    this.score1Label.color = color3;
    Color color4 = color3;
    this.playerNameLabel.color = color4;
    Color color5 = color4;
    text.color = color5;
  }
}
