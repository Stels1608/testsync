﻿// Decompiled with JetBrains decompiler
// Type: GameLevel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Tooltips;
using System;
using UnityEngine;

public abstract class GameLevel : MonoBehaviour, ILoadable
{
  private bool updateInputManager = true;
  protected const int MAX_BATTLESPACE_LEVEL = 10;
  private static GameLevel instance;
  protected Shop shop;
  protected GUIManager guiManager;
  [SerializeField]
  protected InputManager inputManager;
  protected GuiAdvancedTooltipManager tooltipManager;
  protected CutsceneManager cutsceneManager;
  public CameraSwitcher cameraSwitcher;
  private Flag isStarted;

  public Flag IsStarted
  {
    get
    {
      return this.isStarted;
    }
  }

  public Flag IsLoaded
  {
    get
    {
      return this.isStarted;
    }
  }

  public static GameLevel Instance
  {
    get
    {
      return GameLevel.instance;
    }
  }

  public abstract GameLevelType LevelType { get; }

  public abstract bool IsIngameLevel { get; }

  public Shop Shop
  {
    get
    {
      return this.shop;
    }
  }

  public GUIManager GUIManager
  {
    get
    {
      return this.guiManager;
    }
  }

  public InputManager InputManager
  {
    get
    {
      return this.inputManager;
    }
  }

  public GuiAdvancedTooltipManager TooltipManager
  {
    get
    {
      return this.tooltipManager;
    }
  }

  public CutsceneManager CutsceneManager
  {
    get
    {
      return this.cutsceneManager;
    }
  }

  public virtual float NearClipPlane
  {
    get
    {
      return 0.3f;
    }
  }

  public virtual float FarClipPlane
  {
    get
    {
      return -1f;
    }
  }

  public bool UpdateInputManager
  {
    get
    {
      return this.updateInputManager;
    }
    set
    {
      this.updateInputManager = value;
    }
  }

  public bool IsBattlespaceAvailable()
  {
    SpaceLevel spaceLevel = GameLevel.Instance as SpaceLevel;
    if (!((UnityEngine.Object) spaceLevel != (UnityEngine.Object) null) || !spaceLevel.IsBattlespace)
      return (int) Game.Me.Level < 10;
    return true;
  }

  public bool CanAccessTournament()
  {
    if (Game.Tournament.TournamentCard == null)
      return false;
    return (int) Game.Me.Level >= (int) Game.Tournament.TournamentCard.MinLevel;
  }

  private void Awake()
  {
    GameLevel.instance = this;
    this.isStarted = new Flag();
  }

  protected abstract void AddLoadingScreenDependencies();

  public virtual void Initialize(LevelProfile levelProfile)
  {
    GuiPanel.ClearHotkeys();
    this.guiManager = new GUIManager();
    this.inputManager = new InputManager();
    this.tooltipManager = new GuiAdvancedTooltipManager();
    Game.SpecialOfferManager.OnHappyHourChanged = (SpecialOfferManager.HappyHourDelegate) null;
    Console console = new Console();
    this.guiManager.AddPanel((IGUIPanel) console);
    this.inputManager.InputDispatcher.AddListener((InputListener) console);
    GUIChatNew guiChatNew = new GUIChatNew();
    this.GUIManager.AddPanel((IGUIPanel) guiChatNew);
    this.inputManager.InputDispatcher.AddListener((InputListener) guiChatNew);
    Game.DelayedActions.isSleeping = (bool) ((UnityEngine.Object) this.transform);
    this.cutsceneManager = CutsceneManager.Instance;
  }

  protected virtual void Start()
  {
    if ((UnityEngine.Object) Camera.main != (UnityEngine.Object) null)
    {
      Camera.main.nearClipPlane = this.NearClipPlane;
      if ((double) this.FarClipPlane > 0.0)
        Camera.main.farClipPlane = this.FarClipPlane;
    }
    Game.GUIManager.NotifyAboutResolutionChanged();
    this.AddLoadingScreenDependencies();
    this.isStarted.AddHandler((SignalHandler) (() => FacadeFactory.GetInstance().SendMessage(Message.LevelStarted, (object) this)));
    this.isStarted.Set();
  }

  protected virtual void Update()
  {
    if (this.UpdateInputManager)
      this.inputManager.UpdateInputs();
    this.guiManager.Update();
    this.tooltipManager.Update();
  }

  protected virtual void OnGUI()
  {
    if (this.guiManager.IsRendered)
      this.guiManager.Draw();
    this.inputManager.InputDispatcher.ApplyMacChatHack();
  }

  protected virtual void LateUpdate()
  {
    this.GUIManager.LateUpdate();
  }

  private void OnDisable()
  {
    GameLevel.instance = (GameLevel) null;
    this.OnQuit();
  }

  public virtual void OnQuit()
  {
    if (Game.IsQuitting || this.shop == null)
      return;
    this.shop.Close();
  }

  public virtual bool ConsoleCommand(string command)
  {
    return false;
  }

  public ItemList GetCountableListForShip(byte tier)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GameLevel.\u003CGetCountableListForShip\u003Ec__AnonStoreyD8 shipCAnonStoreyD8 = new GameLevel.\u003CGetCountableListForShip\u003Ec__AnonStoreyD8();
    // ISSUE: reference to a compiler-generated field
    shipCAnonStoreyD8.tier = tier;
    if (this.Shop == null)
      return (ItemList) null;
    // ISSUE: reference to a compiler-generated method
    return this.Shop.Filter(new Predicate<ShipItem>(shipCAnonStoreyD8.\u003C\u003Em__218));
  }

  public virtual void CheckSystemButtons(SystemButtonWindow systemButtons)
  {
    systemButtons.ShowSectorMap(this is RoomLevel);
    systemButtons.ShowBattlespace(this.IsBattlespaceAvailable());
    if ((int) Game.Me.ActiveShip.Card.Tier > 1)
    {
      systemButtons.EnableBattlespaceButton(false);
      systemButtons.SetBattlespaceTooltip("%$bgo.option_buttons.battlespace_disabled_strikes_only%");
    }
    else
      systemButtons.SetBattlespaceTooltip("%$bgo.option_buttons.battlespace%");
    systemButtons.ShowTournament(Game.Tournament.IsActive && this.CanAccessTournament());
    systemButtons.ShowTournamentLeaderboards(false);
    systemButtons.ShowGuild(true);
    systemButtons.ShowJournal(true);
    systemButtons.ShowHold(true);
    systemButtons.ShowBuyCubit(true);
    systemButtons.ShowLeaderboards(true);
    systemButtons.ShowCharacterMenu(this is RoomLevel);
  }
}
