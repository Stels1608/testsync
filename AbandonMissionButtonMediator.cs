﻿// Decompiled with JetBrains decompiler
// Type: AbandonMissionButtonMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class AbandonMissionButtonMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "AbandonMissionButtonMediator";
  private AbandonMissionButton abandonButton;

  public AbandonMissionButtonMediator()
    : base("AbandonMissionButtonMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.ShowAbandonMissionButton);
    this.AddMessageInterest(Message.LoadNewLevel);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.ShowAbandonMissionButton:
        this.ShowAbandonMissionButton((bool) message.Data);
        break;
      case Message.LoadNewLevel:
        if (!((Object) this.abandonButton != (Object) null))
          break;
        this.abandonButton.Remove();
        break;
    }
  }

  private void ShowAbandonMissionButton(bool inTutorial)
  {
    GuiHookPoint objectOfType = Object.FindObjectOfType<GuiHookPoint>();
    if ((Object) objectOfType == (Object) null)
    {
      Debug.LogError((object) "Could not locate GuiHookPoint for attaching the Abandon Mission Button");
    }
    else
    {
      this.abandonButton = NGUITools.AddChild(objectOfType.gameObject, Resources.Load<GameObject>("GUI/gui_2013/ui_elements/abandon_mission_button/AbandonMissionButtonNgui")).GetComponentInChildren<AbandonMissionButton>();
      this.abandonButton.SetupButton(inTutorial);
    }
  }
}
