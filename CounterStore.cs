﻿// Decompiled with JetBrains decompiler
// Type: CounterStore
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum CounterStore
{
  InviteSent = 127680069,
  AP1x1Tier1 = 224365282,
  AP1x1Tier2 = 224365283,
  AP1x1Tier3 = 224365284,
  AP3x3MixedTier1 = 227822786,
  AP3x3MixedTier2 = 227822787,
  AP3x3MixedTier3 = 227822788,
}
