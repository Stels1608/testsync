﻿// Decompiled with JetBrains decompiler
// Type: ConsumableAttribute
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class ConsumableAttribute
{
  private static List<string> ms_types = new List<string>();
  private const string mc_localHeader = "bgo.consumable_attribute.";
  private int m_value;
  private string m_attribute;

  public string Attribute
  {
    get
    {
      return this.m_attribute;
    }
    set
    {
      this.m_attribute = value;
      this.m_value = ConsumableAttribute.ms_types.Count;
      string str = this.m_attribute.ToLower().Replace(" ", "_");
      bool translation = Game.Localization.TryGetTranslation("%$bgo.consumable_attribute." + str + "%", out str);
      ConsumableAttribute.ms_types.Add(!translation ? this.m_attribute : str);
    }
  }

  public ConsumableAttribute()
  {
  }

  public ConsumableAttribute(string value)
  {
    this.Attribute = value;
  }

  public static implicit operator ConsumableAttribute(string value)
  {
    return new ConsumableAttribute(value);
  }

  public static implicit operator string(ConsumableAttribute value)
  {
    return value.ToString();
  }

  public override string ToString()
  {
    return ConsumableAttribute.ms_types[this.m_value];
  }
}
