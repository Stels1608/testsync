﻿// Decompiled with JetBrains decompiler
// Type: GalaxyMapInfoPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalaxyMapInfoPanel : GuiTooltipBase
{
  private readonly GuiButton jumpButton = new GuiButton("%$bgo.galaxy_map.jump%");
  private readonly GuiButton groupJumpButton = new GuiButton("%$bgo.group_jump.group_jump%");
  private readonly GuiButton outpostBeaconButton = new GuiButton("BEACON");
  private readonly GuiButton groupJumpToBeaconButton = new GuiButton("GROUP BEACON");
  private readonly List<GuiButton> jumpToButtons = new List<GuiButton>();
  private readonly List<GuiLabel> shipCapLabels = new List<GuiLabel>();
  private readonly Color hostileColor = Tools.Color(163, 0, 10);
  private readonly Color friendlyColor = Tools.Color(55, 113, 153);
  private readonly Color defaultColor = Tools.Color(32, 47, 51);
  private readonly List<GuiButton> availableButton = new List<GuiButton>();
  private const float SECTOR_JUMP_MULTIPLIER = 1f;
  private const float OUTPOST_JUMP_MULTIPLIER = 1.7f;
  private const float PLAYER_JUMP_MULTIPLIER = 2.2f;
  private SectorDesc desc;
  private readonly GuiImage bgrnd;
  private readonly GuiLabel name;
  private readonly GuiLabel distanceTitle;
  private readonly GuiLabel threatLevelTitle;
  private readonly GuiLabel outpostProgressTitle;
  private readonly GuiLabel sectorCapTitle;
  private readonly GuiLabel factionCapTitle;
  private readonly GuiLabel distance;
  private readonly GuiLabel outpostProgress;
  private readonly GuiLabel threatLevel;
  private readonly GuiLabel jumpTo;
  private readonly GuiLabel factionCap;
  private GalaxyMapSector actualGalaxyMapSector;
  private GalaxyMapSector destinationGalaxyMapSector;
  [Gui2Editor]
  private float xMin;
  private float lastButtonY;

  public GalaxyMapInfoPanel()
  {
    this.m_backgroundImage.IsRendered = false;
    this.bgrnd = new GuiImage("GUI/GalaxyMap/infobox");
    this.bgrnd.NineSliceEdge = (GuiElementPadding) new Rect(10f, 10f, 10f, 10f);
    this.AddChild((GuiElementBase) this.bgrnd);
    this.xMin = this.bgrnd.SizeX;
    this.name = new GuiLabel(string.Empty, this.defaultColor, this.defaultColor, Gui.Options.FontBGM_BT);
    this.name.FontSize = 16;
    this.AddChild((GuiElementBase) this.name, new Vector2(20f, 3f));
    Font fontBgmBt = Gui.Options.FontBGM_BT;
    this.distanceTitle = new GuiLabel("%$bgo.galaxy_map.distance%", this.defaultColor, this.defaultColor, fontBgmBt);
    this.distanceTitle.FontSize = 10;
    this.AddChild((GuiElementBase) this.distanceTitle, new Vector2(20f, 18f));
    this.distance = new GuiLabel(string.Empty, this.defaultColor, this.defaultColor, Gui.Options.FontBGM_BT);
    this.distance.FontSize = 10;
    this.AddChild((GuiElementBase) this.distance, Align.UpRight, new Vector2(-13f, 18f));
    this.threatLevelTitle = new GuiLabel("%$bgo.galaxy_map.threat_level%", this.defaultColor, this.defaultColor, fontBgmBt);
    this.threatLevelTitle.FontSize = 10;
    this.AddChild((GuiElementBase) this.threatLevelTitle, new Vector2(20f, 27f));
    this.threatLevel = new GuiLabel(string.Empty, this.defaultColor, this.defaultColor, fontBgmBt);
    this.threatLevel.FontSize = 10;
    this.AddChild((GuiElementBase) this.threatLevel, Align.UpRight, new Vector2(-13f, 27f));
    this.outpostProgressTitle = new GuiLabel("%$bgo.galaxy_map.outpost_progress%", this.defaultColor, this.defaultColor, fontBgmBt);
    this.outpostProgressTitle.FontSize = 10;
    this.AddChild((GuiElementBase) this.outpostProgressTitle, new Vector2(20f, 35f));
    this.outpostProgress = new GuiLabel(string.Empty, this.defaultColor, this.defaultColor, fontBgmBt);
    this.outpostProgress.FontSize = 10;
    this.AddChild((GuiElementBase) this.outpostProgress, Align.UpRight, new Vector2(-13f, 35f));
    this.sectorCapTitle = new GuiLabel("%$bgo.galaxy_map.sector_cap%", this.defaultColor, this.defaultColor, Gui.Options.FontBGM_BT);
    this.sectorCapTitle.FontSize = 11;
    this.AddChild((GuiElementBase) this.sectorCapTitle, new Vector2(20f, 55f));
    this.factionCapTitle = new GuiLabel("%$bgo.galaxy_map.faction_cap%", this.defaultColor, this.defaultColor, fontBgmBt);
    this.factionCapTitle.FontSize = 10;
    this.AddChild((GuiElementBase) this.factionCapTitle, new Vector2(20f, 68f));
    this.factionCap = new GuiLabel(string.Empty, this.defaultColor, this.defaultColor, fontBgmBt);
    this.factionCap.FontSize = 10;
    this.AddChild((GuiElementBase) this.factionCap, Align.UpRight, new Vector2(-13f, 68f));
    this.jumpTo = new GuiLabel("JUMP TO", Color.black, Color.black, Gui.Options.FontBGM_BT);
    this.jumpTo.FontSize = 13;
    this.AddChild((GuiElementBase) this.jumpTo, new Vector2(20f, 60f));
    this.jumpButton.Pressed = (AnonymousDelegate) (() =>
    {
      GameProtocol.GetProtocol().RequestFTLJump(this.desc.Id);
      this.FindParent<GalaxyMapMain>().ToggleIsRendered();
    });
    this.jumpButton.Font = Gui.Options.FontBGM_BT;
    this.jumpButton.FontSize = 11;
    this.jumpButton.Label.AllColor = Color.black;
    this.jumpButton.Size = new Vector2(this.bgrnd.SizeX - 25f, 13f);
    this.groupJumpButton.Pressed = (AnonymousDelegate) (() =>
    {
      GUIGroupJumpWindow guiGroupJumpWindow = new GUIGroupJumpWindow(GameProtocol.Request.GroupJump);
      Game.RegisterDialog((IGUIPanel) guiGroupJumpWindow, true);
      guiGroupJumpWindow.sectorId = this.desc.Id;
      this.FindParent<GalaxyMapMain>().ToggleIsRendered();
    });
    this.groupJumpButton.Label.AllColor = Color.black;
    this.groupJumpButton.Font = Gui.Options.FontBGM_BT;
    this.groupJumpButton.FontSize = 11;
    this.groupJumpButton.Size = new Vector2(this.jumpButton.Size.x, 13f);
    this.outpostBeaconButton.Pressed = (AnonymousDelegate) (() =>
    {
      this.FindParent<GalaxyMapMain>().ToggleIsRendered();
      GameProtocol.GetProtocol().RequestJumpToBeacon(this.desc.Id);
    });
    this.outpostBeaconButton.Font = Gui.Options.FontBGM_BT;
    this.outpostBeaconButton.FontSize = 11;
    this.outpostBeaconButton.Label.AllColor = Color.black;
    this.outpostBeaconButton.Size = new Vector2(this.jumpButton.Size.x, 13f);
    this.groupJumpToBeaconButton.Pressed = (AnonymousDelegate) (() =>
    {
      GUIGroupJumpWindow guiGroupJumpWindow = new GUIGroupJumpWindow(GameProtocol.Request.GroupJumpToBeacon);
      Game.RegisterDialog((IGUIPanel) guiGroupJumpWindow, true);
      guiGroupJumpWindow.sectorId = this.desc.Id;
      this.FindParent<GalaxyMapMain>().ToggleIsRendered();
    });
    this.groupJumpToBeaconButton.Label.AllColor = Color.black;
    this.groupJumpToBeaconButton.Font = Gui.Options.FontBGM_BT;
    this.groupJumpToBeaconButton.FontSize = 11;
    this.groupJumpToBeaconButton.Size = new Vector2(this.jumpButton.Size.x, 13f);
  }

  private GuiButton MakeJumpTargetButton(uint playerId, uint spaceId, uint sectorId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GalaxyMapInfoPanel.\u003CMakeJumpTargetButton\u003Ec__AnonStoreyA5 buttonCAnonStoreyA5 = new GalaxyMapInfoPanel.\u003CMakeJumpTargetButton\u003Ec__AnonStoreyA5();
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStoreyA5.sectorId = sectorId;
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStoreyA5.spaceId = spaceId;
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStoreyA5.\u003C\u003Ef__this = this;
    GuiButton guiButton = new GuiButton((int) playerId != (int) Game.Me.ServerID ? Game.Players[playerId].Name : Game.Me.Name);
    // ISSUE: reference to a compiler-generated method
    guiButton.Pressed = new AnonymousDelegate(buttonCAnonStoreyA5.\u003C\u003Em__161);
    guiButton.Font = Gui.Options.FontBGM_BT;
    guiButton.FontSize = 11;
    guiButton.Size = new Vector2(this.jumpButton.SizeX, 13f);
    guiButton.Label.AllColor = Color.black;
    guiButton.IsRendered = false;
    return guiButton;
  }

  private void AddJumpTarget(uint sectorId, uint playerId, uint spaceId)
  {
    GuiButton guiButton = this.MakeJumpTargetButton(playerId, spaceId, sectorId);
    Player player = Game.Players[playerId];
    if (!(bool) player.IsLoaded)
      player.Request(Player.InfoType.Name);
    string first = (int) playerId != (int) Game.Me.ServerID ? player.Name : Game.Me.Name;
    guiButton.Text = this.CreateJumpButtonText(first, 2.2f * this.actualGalaxyMapSector.CalculateDistance(this.destinationGalaxyMapSector));
    this.jumpToButtons.Add(guiButton);
    guiButton.IsRendered = !Game.Me.Anchored;
    if (!guiButton.IsRendered)
      return;
    this.availableButton.Add(guiButton);
  }

  private void CancelAllJumpTargets()
  {
    using (List<GuiButton>.Enumerator enumerator = this.jumpToButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.RemoveChild((GuiElementBase) enumerator.Current);
    }
    this.jumpToButtons.Clear();
  }

  private string CreateJumpButtonText(string first, float tyliumCost)
  {
    string str = " ";
    string message = Tools.ParseMessage(Mathf.Ceil(Game.Me.Stats.FTLCost * tyliumCost).ToString() + " %$bgo.common.tylium%");
    if (first.Length > 7)
      first = first.Substring(0, 7) + ".";
    string text = first + str + message;
    bool flag = true;
    while (flag)
    {
      if ((double) TextUtility.CalcTextSize(text, this.jumpButton.Font, this.jumpButton.FontSize).width < (double) this.jumpButton.Size.x - 12.0)
      {
        str += " ";
        text = first + str + message;
      }
      else
        flag = false;
    }
    return text;
  }

  private void ShowSectorCapInfo()
  {
    if (this.desc.SectorCaps == null)
    {
      this.sectorCapTitle.IsRendered = false;
      this.factionCapTitle.IsRendered = false;
      this.factionCap.IsRendered = false;
    }
    else
    {
      this.RemoveAll((IEnumerable) this.shipCapLabels);
      this.shipCapLabels.Clear();
      int slotsForMyFaction = this.desc.SectorCaps.GetFreeSlotsForMyFaction();
      if (slotsForMyFaction == 0)
      {
        this.sectorCapTitle.IsRendered = true;
        this.factionCapTitle.IsRendered = true;
        this.factionCapTitle.Text = this.desc.SectorCaps.GetMaxSlotsForMyFaction() != 0 ? "%$bgo.galaxy_map.sector_full%" : "%$bgo.galaxy_map.sector_restricted%";
        this.factionCapTitle.NormalColor = this.hostileColor;
        this.factionCap.IsRendered = false;
      }
      else
      {
        if (slotsForMyFaction <= 10)
        {
          this.sectorCapTitle.IsRendered = true;
          this.factionCapTitle.IsRendered = true;
          this.factionCapTitle.Text = "%$bgo.galaxy_map.faction_cap%";
          this.factionCapTitle.NormalColor = this.defaultColor;
          this.factionCap.IsRendered = true;
          this.factionCap.Text = slotsForMyFaction.ToString();
        }
        else
        {
          this.sectorCapTitle.IsRendered = false;
          this.factionCapTitle.IsRendered = false;
          this.factionCap.IsRendered = false;
        }
        using (Dictionary<GUICard, uint>.Enumerator enumerator = this.desc.SectorCaps.MaxSlotsForShip.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<GUICard, uint> current = enumerator.Current;
            this.sectorCapTitle.IsRendered = true;
            GuiLabel guiLabel = new GuiLabel(this.defaultColor, Gui.Options.FontBGM_BT, 10);
            string shipName = BsgoLocalization.GetShipName(current.Key.Key);
            guiLabel.Text = shipName + ": " + (object) this.desc.SectorCaps.GetFreeSlotsForShip(current.Key);
            guiLabel.PositionX = 20f;
            this.shipCapLabels.Add(guiLabel);
            this.AddChild((GuiElementBase) guiLabel);
          }
        }
      }
    }
  }

  public void SetInfo(GalaxyMapSector galaxyMapSector, GalaxyMapSector myGalaxyMapSector)
  {
    this.desc = galaxyMapSector.Desc;
    this.actualGalaxyMapSector = myGalaxyMapSector;
    this.destinationGalaxyMapSector = galaxyMapSector;
    this.name.Text = this.desc.Name;
    this.ShowSectorCapInfo();
    using (List<GuiButton>.Enumerator enumerator = this.availableButton.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GuiButton current = enumerator.Current;
        current.IsRendered = false;
        this.RemoveChild((GuiElementBase) current);
      }
    }
    this.availableButton.Clear();
    float distance = myGalaxyMapSector.CalculateDistance(galaxyMapSector);
    bool flag1 = myGalaxyMapSector.CanJump(galaxyMapSector) && galaxyMapSector != myGalaxyMapSector && (!GalaxyMapMain.JumpActive && (Object) SpaceLevel.GetLevel() != (Object) null) && (this.desc.SectorCaps.MaxTotalSlots > 0U && this.desc.SectorCaps.GetMaxSlotsForMyFaction() > 0);
    this.distance.Text = ((float) Mathf.CeilToInt((float) ((double) distance / 20.0 * 10.0)) / 10f).ToString("0.0") + " %$bgo.common.light_year%";
    this.distance.AllColor = !flag1 ? this.hostileColor : this.friendlyColor;
    this.threatLevel.Text = this.desc.MyFactionThreatLevel.ToString();
    this.threatLevel.AllColor = (int) Game.Me.Level >= this.desc.MyFactionThreatLevel ? this.friendlyColor : this.hostileColor;
    this.jumpButton.Text = this.CreateJumpButtonText("Sector", distance * 1f);
    if (this.desc.CanOutpost)
    {
      if (this.desc.IsOutpostBlocked)
      {
        this.outpostProgressTitle.Text = "%$bgo.galaxy_map.outpost_repairing%";
        this.outpostProgress.Text = Tools.FormatTime(this.desc.OutpostBlockedEndtime - Time.time);
      }
      else
      {
        this.outpostProgressTitle.Text = "%$bgo.galaxy_map.outpost_progress%";
        this.outpostProgress.Text = ((float) this.desc.MyFactionOutpostPoints / 10f).ToString("F1");
        this.outpostProgress.AllColor = this.friendlyColor;
      }
    }
    else
    {
      this.outpostProgressTitle.Text = "%$bgo.galaxy_map.outpost_progress%";
      this.outpostProgress.Text = "%$bgo.galaxy_map.na%";
    }
    float num = Mathf.Max(Mathf.Max(Mathf.Max(this.name.SizeX, this.distanceTitle.SizeX + this.distance.SizeX), this.threatLevelTitle.SizeX + this.threatLevel.SizeX), this.outpostProgressTitle.SizeX + this.outpostProgress.SizeX) + 33f;
    if ((double) num < (double) this.xMin)
      num = this.xMin;
    this.bgrnd.SizeX = num;
    this.Size = this.bgrnd.Size + new Vector2(0.0f, 20f);
    this.jumpButton.IsRendered = flag1;
    if (flag1)
      this.availableButton.Add(this.jumpButton);
    bool flag2 = Game.Me.Faction != Faction.Cylon ? galaxyMapSector.Desc.ColonialIsJumpBeacon : galaxyMapSector.Desc.CylonIsJumpBeacon;
    if ((flag1 || (int) galaxyMapSector.Desc.Id == (int) myGalaxyMapSector.Desc.Id) && flag2)
    {
      this.outpostBeaconButton.IsRendered = true;
      this.outpostBeaconButton.Text = this.CreateJumpButtonText(Tools.ParseMessage("%$bgo.galaxy_map.jump_to_beacon%"), distance * 1.7f);
      this.availableButton.Add(this.outpostBeaconButton);
    }
    this.groupJumpButton.Text = this.CreateJumpButtonText(Tools.ParseMessage("%$bgo.common.group%"), distance * 1f);
    this.groupJumpButton.IsRendered = flag1 && Game.Me.Party.IsLeader;
    if (flag1 && Game.Me.Party.IsLeader)
      this.availableButton.Add(this.groupJumpButton);
    if ((flag1 || (int) galaxyMapSector.Desc.Id == (int) myGalaxyMapSector.Desc.Id) && (flag2 && Game.Me.Party.IsLeader))
    {
      this.groupJumpToBeaconButton.IsRendered = true;
      this.groupJumpToBeaconButton.Text = this.CreateJumpButtonText(Tools.ParseMessage("%$bgo.galaxy_map.group_jump_to_beacon%"), distance * 1.7f);
      this.availableButton.Add(this.groupJumpToBeaconButton);
    }
    if (!GalaxyMapMain.JumpActive && (Object) SpaceLevel.GetLevel() != (Object) null)
      this.CreateJumpTargetTransponderButtons(this.desc);
    this.RepositionButtons();
    this.PeriodicUpdate();
  }

  private void CreateJumpTargetTransponderButtons(SectorDesc desc)
  {
    this.CancelAllJumpTargets();
    using (List<JumpTargetTransponderDesc>.Enumerator enumerator = desc.FilteredJumpTargetTransponders.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JumpTargetTransponderDesc current = enumerator.Current;
        this.AddJumpTarget(desc.Id, current.PlayerId, current.SpaceId);
      }
    }
  }

  private void RepositionButtons()
  {
    this.jumpTo.IsRendered = this.availableButton.Count > 0;
    this.lastButtonY = !this.jumpTo.IsRendered ? 30f : 74f;
    for (int index = 0; index < this.availableButton.Count; ++index)
    {
      this.lastButtonY += (float) (index * 15);
      this.AddChild((GuiElementBase) this.availableButton[index], Align.UpLeft, new Vector2(15f, this.lastButtonY));
    }
    this.lastButtonY += 22f;
    if (this.sectorCapTitle.IsRendered)
    {
      this.sectorCapTitle.PositionY = this.lastButtonY;
      this.lastButtonY += 12f;
    }
    if (this.factionCapTitle.IsRendered)
    {
      this.factionCapTitle.PositionY = this.lastButtonY;
      this.factionCap.PositionY = this.lastButtonY;
      this.lastButtonY += 20f;
    }
    if (this.shipCapLabels.Count > 0)
    {
      using (List<GuiLabel>.Enumerator enumerator = this.shipCapLabels.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          enumerator.Current.PositionY = this.lastButtonY;
          this.lastButtonY += 12f;
        }
      }
      this.lastButtonY += 5f;
    }
    this.bgrnd.Size = new Vector2(this.bgrnd.Size.x, this.lastButtonY);
  }

  public override bool Contains(float2 point)
  {
    if (this.jumpButton.Contains(point) || this.groupJumpButton.Contains(point) || (this.outpostBeaconButton.Contains(point) || this.groupJumpToBeaconButton.Contains(point)))
      return true;
    using (List<GuiButton>.Enumerator enumerator = this.jumpToButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Contains(point))
          return true;
      }
    }
    return false;
  }
}
