﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorDesignatorNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HudIndicatorDesignatorNgui : HudIndicatorBaseComponentNgui
{
  private const float mc_stillTriDistanceFromCenter = 18.5f;
  private const float mc_minTriDistanceFromCenter = 25f;
  private const float mc_maxTriDistanceFromCenter = 40f;
  private const float mc_aniSpeed = 0.5f;
  private Transform outerTriangleLeftTransform;
  private Transform outerTriangleRightTransform;
  private Transform outerTriangleBottomTransform;
  private bool m_playAnimation;

  protected UIAtlas Atlas
  {
    get
    {
      return HudIndicatorsAtlasLinker.Instance.Atlas;
    }
  }

  public override bool RequiresAnimationNgui
  {
    get
    {
      return this.IsVisible;
    }
  }

  private bool IsVisible
  {
    get
    {
      if (this.IsDesignated)
        return this.InScreen;
      return false;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    NGUIToolsExtension.AddSprite(this.gameObject, this.Atlas, "HudIndicator_DesignatedTarget_InnerTriangle", true, string.Empty);
    this.outerTriangleLeftTransform = NGUIToolsExtension.AddSprite(this.gameObject, this.Atlas, "HudIndicator_DesignatedTarget_OuterTriangle", true, string.Empty).transform;
    this.outerTriangleRightTransform = NGUIToolsExtension.AddSprite(this.gameObject, this.Atlas, "HudIndicator_DesignatedTarget_OuterTriangle", true, string.Empty).transform;
    this.outerTriangleBottomTransform = NGUIToolsExtension.AddSprite(this.gameObject, this.Atlas, "HudIndicator_DesignatedTarget_OuterTriangle", true, string.Empty).transform;
    this.ApplyStillPosition();
  }

  protected override void ResetComponentStates()
  {
    this.ApplyStillPosition();
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    this.gameObject.SetActive(this.IsVisible);
  }

  private void Update()
  {
    if (!this.gameObject.activeSelf)
      return;
    if (this.IsSelected)
    {
      if (this.m_playAnimation)
        this.m_playAnimation = false;
    }
    else if (!this.m_playAnimation)
      this.m_playAnimation = true;
    this.Animate();
  }

  private void ApplyStillPosition()
  {
    if ((Object) this.outerTriangleLeftTransform == (Object) null)
      return;
    this.outerTriangleLeftTransform.localPosition = (Vector3) (new Vector2(-1f, 1f) * 18.5f);
    this.outerTriangleRightTransform.localPosition = (Vector3) (new Vector2(1f, 1f) * 18.5f);
    this.outerTriangleBottomTransform.localPosition = (Vector3) (new Vector2(0.0f, -1f) * 18.5f);
  }

  private void Animate()
  {
    if (!this.m_playAnimation)
    {
      this.ApplyStillPosition();
    }
    else
    {
      float num = (float) (25.0 + ((double) Mathf.Sin(Time.time * 5f) + 1.0) / 2.0 * 15.0);
      this.outerTriangleLeftTransform.localPosition = new Vector3(1f, 1f) * num;
      this.outerTriangleRightTransform.localPosition = new Vector3(-1f, 1f) * num;
      this.outerTriangleBottomTransform.localPosition = new Vector3(0.0f, -1f) * num;
    }
  }
}
