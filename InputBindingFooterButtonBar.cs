﻿// Decompiled with JetBrains decompiler
// Type: InputBindingFooterButtonBar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class InputBindingFooterButtonBar : ButtonBar
{
  [SerializeField]
  private ButtonWidget revertButton;
  [SerializeField]
  private ButtonWidget applyButton;
  [SerializeField]
  private ButtonWidget resetButton;

  protected override void Start()
  {
    base.Start();
    this.revertButton.BgSprite.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
    this.applyButton.BgSprite.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
    this.resetButton.BgSprite.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
  }

  public void SetButtonDelegates(AnonymousDelegate revertDelegate, AnonymousDelegate applyDeletegate, AnonymousDelegate resetDelegate)
  {
    this.applyButton.handleClick = applyDeletegate;
    this.revertButton.handleClick = revertDelegate;
    this.resetButton.handleClick = resetDelegate;
  }

  public override void ReloadLanguageData()
  {
    this.revertButton.TextLabel.text = Tools.ParseMessage("%$bgo.ui.options.key.revert%");
    this.applyButton.TextLabel.text = Tools.ParseMessage("%$bgo.ui.options.key.apply%");
    this.resetButton.TextLabel.text = Tools.ParseMessage("%$bgo.ui.options.key.reset%");
  }
}
