﻿// Decompiled with JetBrains decompiler
// Type: AbstractGun
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class AbstractGun : Weapon
{
  private static readonly int impactLayer = LayerMask.NameToLayer("impactLayer");
  public float DebugFireInterval = 2f;
  private float lastDebugFireTime = float.NegativeInfinity;
  public float MaxDistanceToShowHitEffect = 2000f;
  public float ScatterAngle = 90f;
  public const float BulletSpeed = 400f;
  public SpaceObject Target;
  protected BulletHitSoundPlayer bulletHitSoundPlayer;
  public GameObject BulletPrefab;
  public float Dispersion;
  public float MissChance;
  public float BulletFreeFlightInterval;
  public bool DebugFire;
  protected Vector3 lastTargetPosition;
  public GameObject BulletHitEffectPrefab;

  public static bool ShowBulletImpactFx { get; set; }

  protected virtual void OnDisable()
  {
  }

  protected virtual void LateUpdate()
  {
    if (!this.DebugFire || (double) Time.time - (double) this.lastDebugFireTime <= (double) this.DebugFireInterval)
      return;
    this.lastDebugFireTime = Time.time;
    this.Fire(this.Target);
  }

  protected void SaveLastTargetPosition()
  {
    if (this.Target != null)
      this.lastTargetPosition = this.Target.Position;
    else
      this.lastTargetPosition = this.transform.position + this.transform.forward;
  }

  public override void Fire(SpaceObject target)
  {
    if (target == this.Target)
      return;
    this.Target = target;
    if (target == null)
      return;
    this.bulletHitSoundPlayer = target.GetModelScript<BulletHitSoundPlayer>();
  }

  protected abstract Vector3 GetShotOrigin();

  protected void CalculateBulletParameters(out Transform bulletTarget, out Vector3 direction, out Vector3 hullOffset)
  {
    bulletTarget = (Transform) null;
    hullOffset = Vector3.zero;
    if (this.Target != null && (Object) this.Target.Root != (Object) null && (Object) this.Target.Root.gameObject != (Object) null)
    {
      direction = this.Target.Position - this.transform.position;
      Collider componentInChildren = this.Target.Root.gameObject.GetComponentInChildren<Collider>();
      if (!((Object) componentInChildren != (Object) null))
        return;
      int layer = componentInChildren.gameObject.layer;
      componentInChildren.gameObject.layer = AbstractGun.impactLayer;
      RaycastHit hitInfo;
      if (Physics.Raycast(this.transform.position, direction, out hitInfo, 20000f, 1 << AbstractGun.impactLayer))
      {
        bulletTarget = hitInfo.transform;
        hullOffset = bulletTarget.transform.position - hitInfo.point;
      }
      else
        bulletTarget = this.Target.Root;
      componentInChildren.gameObject.layer = layer;
    }
    else
      direction = this.lastTargetPosition - this.transform.position;
  }

  protected abstract BulletScript GetNextBullet();

  protected virtual void MakeShot()
  {
    if ((Object) this.BulletPrefab == (Object) null)
      return;
    Transform bulletTarget;
    Vector3 direction;
    Vector3 hullOffset;
    this.CalculateBulletParameters(out bulletTarget, out direction, out hullOffset);
    float magnitude = direction.magnitude;
    float num = magnitude / 400f;
    float max = this.Dispersion * magnitude;
    Vector3 vector3 = new Vector3(Random.Range(-max, max), Random.Range(-max, max), Random.Range(-max, max)) - hullOffset;
    bool flag = (double) Random.Range(0.0f, 1f) <= (double) this.MissChance;
    BulletScript nextBullet = this.GetNextBullet();
    nextBullet.transform.position = this.GetShotOrigin();
    if (this.Target == null)
      direction = AbstractGun.GetRandomInsideCone(this.ScatterAngle) * this.transform.forward;
    nextBullet.transform.rotation = Quaternion.LookRotation(direction);
    nextBullet.Target = bulletTarget;
    nextBullet.Speed = 400f;
    nextBullet.PursuitTime = num;
    nextBullet.Miss = flag;
    nextBullet.Offset = vector3;
    nextBullet.FreeFlightInterval = this.BulletFreeFlightInterval;
    nextBullet.Hit = new BulletScript.HitEventHandler(this.BulletHit);
    nextBullet.Go();
  }

  private static Quaternion GetRandomInsideCone(float conicAngle)
  {
    float max = conicAngle * 0.5f;
    Quaternion quaternion = Quaternion.AngleAxis(Random.Range(-max, max), Vector3.up);
    return Quaternion.AngleAxis(Random.Range(-max, max), Vector3.forward) * quaternion;
  }

  protected virtual void BulletHit(BulletScript bullet)
  {
    if ((Object) this == (Object) null || (Object) bullet == (Object) null || (this.Target == null || (Object) this.Target.Model == (Object) null) || !this.Target.Model.activeInHierarchy)
      return;
    if ((Object) this.bulletHitSoundPlayer != (Object) null)
      this.bulletHitSoundPlayer.Play();
    if (!AbstractGun.ShowBulletImpactFx || !(bool) ((Object) this.BulletHitEffectPrefab) || (this.Target.Dead || (double) Vector3.SqrMagnitude(this.transform.position - bullet.transform.position) >= (double) Mathf.Pow(this.MaxDistanceToShowHitEffect, 2f)))
      return;
    Vector3 origin = bullet.transform.position + bullet.transform.rotation * Vector3.back * 30f;
    Collider componentInChildren = this.Target.Model.gameObject.GetComponentInChildren<Collider>();
    if (!((Object) componentInChildren != (Object) null))
      return;
    int layer = componentInChildren.gameObject.layer;
    componentInChildren.gameObject.layer = AbstractGun.impactLayer;
    RaycastHit hitInfo;
    bool flag = Physics.Raycast(origin, this.Target.Position - origin, out hitInfo, 20000f, 1 << AbstractGun.impactLayer);
    componentInChildren.gameObject.layer = layer;
    if (this.Target is Ship)
    {
      Ship ship = this.Target as Ship;
      flag = ((flag ? 1 : 0) & ((int) ship.ShipCardLight.Tier > 1 ? 1 : ((int) ship.ShipCardLight.Tier != 1 ? 0 : ((double) Random.value < 0.200000002980232 ? 1 : 0)))) != 0;
    }
    if (!flag)
      return;
    GameObject gameObject = Object.Instantiate((Object) this.BulletHitEffectPrefab, hitInfo.point, bullet.transform.rotation) as GameObject;
    gameObject.transform.parent = bullet.Target;
    gameObject.transform.LookAt(this.transform.position - bullet.GetDirection());
  }
}
