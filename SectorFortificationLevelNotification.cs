﻿// Decompiled with JetBrains decompiler
// Type: SectorFortificationLevelNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class SectorFortificationLevelNotification : OnScreenNotification
{
  private readonly string message;
  private readonly NotificationCategory category;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.SectorFortificationLevel;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return this.category;
    }
  }

  public override string TextMessage
  {
    get
    {
      return this.message;
    }
  }

  public override bool Show
  {
    get
    {
      return OnScreenNotification.ShowOutpostMessages;
    }
  }

  public SectorFortificationLevelNotification(Faction faction, GUICard sectorCard, byte level, SectorFortificationChangeType changeType)
  {
    bool flag1 = faction == Game.Me.Faction;
    bool flag2 = changeType == SectorFortificationChangeType.Increment;
    this.category = flag1 && flag2 || !flag1 && !flag2 ? NotificationCategory.Positive : NotificationCategory.Negative;
    this.message = BsgoLocalization.Get("%$bgo.notif.sector_fortification_" + (!flag2 ? "decreased" : "increased") + "_" + (!flag1 ? (faction != Faction.Cylon ? "colonial" : "cylon") : "ownfaction") + "%", (object) sectorCard.Name, (object) level);
    FacadeFactory.GetInstance().SendMessage(this.category != NotificationCategory.Positive ? Message.CombatLogOuch : Message.CombatLogNice, (object) this.message);
  }
}
