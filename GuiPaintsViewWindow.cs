﻿// Decompiled with JetBrains decompiler
// Type: GuiPaintsViewWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GuiPaintsViewWindow : GuiDialog
{
  private readonly GuiLabel title = new GuiLabel(Gui.Options.FontBGM_BT, 17);
  private readonly GuiAtlasImage icon = new GuiAtlasImage();
  private readonly ShipView shipView = new ShipView();
  public readonly GuiButton close;

  public GuiPaintsViewWindow(ShipSystem item)
  {
    GuiPaintsViewWindow paintsViewWindow = Game.GUIManager.Find<GuiPaintsViewWindow>();
    if (paintsViewWindow != null)
    {
      paintsViewWindow.OnClose();
      Game.UnregisterDialog((IGUIPanel) paintsViewWindow);
    }
    this.Align = Align.MiddleCenter;
    this.Position = new Vector2(0.0f, 0.0f);
    this.Size = new Vector2(460f, 540f);
    this.OnClose = (AnonymousDelegate) (() =>
    {
      this.shipView.Delete();
      Game.UnregisterDialog((IGUIPanel) this);
    });
    this.title.WordWrap = true;
    this.title.Text = item.ItemGUICard.Name;
    this.title.SizeX = 410f;
    this.title.Alignment = TextAnchor.UpperCenter;
    this.AddChild((GuiElementBase) this.title, Align.UpCenter, new Vector2(0.0f, 13f));
    this.AddChild((GuiElementBase) new GuiColoredBox(Gui.Options.PositiveColor, new Vector2(0.0f, 60f), new Vector2(300f, 1f)), Align.UpCenter);
    this.shipView.Size = new Vector2(400f, 350f);
    this.shipView.SetPrefab(item.PaintCard.PrefabName);
    this.shipView.ShowRust = false;
    this.shipView.SetSkinName(item.PaintCard.paintTexture);
    this.AddChild((GuiElementBase) this.shipView, Align.UpCenter, new Vector2(0.0f, 90f));
    this.shipView.Reposition();
    this.icon.GuiCard = item.ItemGUICard;
    this.AddChild((GuiElementBase) this.icon, Align.UpLeft, new Vector2(25f, 455f));
    this.close = new GuiButton("%$bgo.EquipBuyPanel.DetailsWindow.gui_window_layout.label_1%");
    this.close.OnClick = (AnonymousDelegate) (() => this.OnClose());
    this.AddChild((GuiElementBase) this.close, Align.DownCenter, new Vector2(130f, -20f));
    this.AddChild((GuiElementBase) new GuiButton(string.Empty, ResourceLoader.Load<Texture2D>("GUI/ShipCustomization/camerareset"), ResourceLoader.Load<Texture2D>("GUI/ShipCustomization/camerareset_click"), ResourceLoader.Load<Texture2D>("GUI/ShipCustomization/camerareset_over"))
    {
      Pressed = (AnonymousDelegate) (() =>
      {
        if (this.shipView == null)
          return;
        this.shipView.RestoreView();
      })
    }, new Vector2(420f, 455f));
    this.AddChild((GuiElementBase) new Gui.Timer(1f));
  }
}
