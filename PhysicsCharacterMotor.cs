﻿// Decompiled with JetBrains decompiler
// Type: PhysicsCharacterMotor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (CapsuleCollider))]
[RequireComponent(typeof (Rigidbody))]
public class PhysicsCharacterMotor : CharacterMotor
{
  public float maxRotationSpeed = 270f;
  public Vector3 gravityCenter = Vector3.zero;
  public bool useCentricGravity;
  public LayerMask groundLayers;

  private void Awake()
  {
    this.GetComponent<Rigidbody>().freezeRotation = true;
    this.GetComponent<Rigidbody>().useGravity = false;
  }

  private void AdjustToGravity()
  {
    int layer = this.gameObject.layer;
    this.gameObject.layer = 2;
    Vector3 up = this.transform.up;
    float num = Mathf.Clamp01(Time.deltaTime * 5f);
    Vector3 zero = Vector3.zero;
    for (int index = 0; index < 8; ++index)
    {
      RaycastHit hitInfo;
      if (Physics.Raycast(this.transform.position + this.transform.up + Quaternion.AngleAxis((float) (360 * index) / 8f, this.transform.up) * this.transform.right * 0.5f + this.desiredVelocity * 0.2f, this.transform.up * -2f, out hitInfo, 3f, this.groundLayers.value))
        zero += hitInfo.normal;
    }
    Vector3 normalized1 = (up + zero).normalized;
    Vector3 normalized2 = (up + normalized1 * num).normalized;
    float angle = Vector3.Angle(up, normalized2);
    if ((double) angle > 0.01)
    {
      Vector3 normalized3 = Vector3.Cross(up, normalized2).normalized;
      this.transform.rotation = Quaternion.AngleAxis(angle, normalized3) * this.transform.rotation;
    }
    this.gameObject.layer = layer;
  }

  private void UpdateFacingDirection()
  {
    float magnitude = this.desiredFacingDirection.magnitude;
    Vector3 to = this.alignCorrection * Util.ProjectOntoPlane(this.transform.rotation * this.desiredMovementDirection * (1f - magnitude) + this.desiredFacingDirection * magnitude, this.transform.up);
    if ((double) to.sqrMagnitude <= 0.100000001490116)
      return;
    Vector3 view = Util.ProjectOntoPlane(Util.ConstantSlerp(this.transform.forward, to, this.maxRotationSpeed * Time.deltaTime), this.transform.up);
    Quaternion quaternion = new Quaternion();
    quaternion.SetLookRotation(view, this.transform.up);
    this.transform.rotation = quaternion;
  }

  private void UpdateVelocity()
  {
    Vector3 v = this.GetComponent<Rigidbody>().velocity;
    if (this.grounded)
      v = Util.ProjectOntoPlane(v, this.transform.up);
    this.jumping = false;
    if (this.grounded)
    {
      Vector3 force = this.desiredVelocity - v;
      if ((double) force.magnitude > (double) this.maxVelocityChange)
        force = force.normalized * this.maxVelocityChange;
      this.GetComponent<Rigidbody>().AddForce(force, ForceMode.VelocityChange);
      if (this.canJump && Input.GetButton("Jump"))
      {
        this.GetComponent<Rigidbody>().velocity = v + this.transform.up * Mathf.Sqrt(2f * this.jumpHeight * this.gravity);
        this.jumping = true;
      }
    }
    this.GetComponent<Rigidbody>().AddForce(this.transform.up * -this.gravity * this.GetComponent<Rigidbody>().mass);
    this.grounded = false;
  }

  private void OnCollisionStay()
  {
    this.grounded = true;
  }

  private void FixedUpdate()
  {
    if (this.useCentricGravity)
      this.AdjustToGravity();
    this.UpdateFacingDirection();
    this.UpdateVelocity();
  }
}
