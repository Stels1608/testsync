﻿// Decompiled with JetBrains decompiler
// Type: GuiMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class GuiMediator : Bigpoint.Core.Mvc.View.View<Message>, InputListener
{
  public new const string NAME = "GuiMediator";
  private NguiWindowManager manager;
  private UIRoot nguiRoot;

  public NguiWindowManager Manager
  {
    get
    {
      return this.manager;
    }
    set
    {
      this.manager = value;
    }
  }

  public bool HandleJoystickAxesInput
  {
    get
    {
      return true;
    }
  }

  public bool HandleMouseInput
  {
    get
    {
      return true;
    }
  }

  public bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public UIRoot NguiRoot
  {
    get
    {
      return this.nguiRoot ?? (this.nguiRoot = Object.FindObjectOfType<UIRoot>());
    }
  }

  public GuiMediator()
    : base("GuiMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.LocalizationData);
    this.AddMessageInterest(Message.ToggleOldUi);
    this.AddMessageInterest(Message.ToggleNgui);
    this.AddMessageInterest(Message.ToggleUGuiHubCanvas);
    this.AddMessageInterest(Message.ToggleUGuiHudCanvas);
    this.AddMessageInterest(Message.ToggleUGuiWindowsCanvas);
    this.AddMessageInterest(Message.ToggleUGuiHudIndicatorsCanvas);
    this.AddMessageInterest(Message.ToggleInputActive);
    this.AddMessageInterest(Message.ShowDialogBox);
    this.AddMessageInterest(Message.HideDialogBox);
    this.AddMessageInterest(Message.RegisterWindowManager);
    this.AddMessageInterest(Message.PlayerFactionReply);
    this.AddMessageInterest(Message.GuiContentWindowVisibility);
    this.AddMessageInterest(Message.LoadNewLevel);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.ToggleOldUi:
        this.ToggleOldUI((bool) message.Data);
        break;
      case Message.ToggleNgui:
        this.ToggleNgui((bool) message.Data);
        break;
      case Message.ToggleUGuiHubCanvas:
        this.ToggleUguiCanvas(BsgoCanvas.Hub, (bool) message.Data);
        break;
      case Message.ToggleUGuiHudCanvas:
        this.ToggleUguiCanvas(BsgoCanvas.Hud, (bool) message.Data);
        break;
      case Message.ToggleUGuiWindowsCanvas:
        this.ToggleUguiCanvas(BsgoCanvas.Windows, (bool) message.Data);
        break;
      case Message.ToggleUGuiHudIndicatorsCanvas:
        this.ToggleUguiCanvas(BsgoCanvas.HudIndicators, (bool) message.Data);
        break;
      case Message.ToggleInputActive:
        this.ToggleInputActive((bool) message.Data);
        break;
      case Message.PlayerFactionReply:
        ColorManager.SwitchColorScheme((int) message.Data != 2 ? (WidgetColorScheme) new ColonialColorSchema() : (WidgetColorScheme) new CylonColorSchema());
        break;
      case Message.LoadNewLevel:
        MessageBoxManager messageBoxManager = Game.GUIManager.Find<MessageBoxManager>();
        if (messageBoxManager == null)
          break;
        messageBoxManager.DeleteAllBoxes();
        break;
      default:
        switch (id - 134)
        {
          case Message.None:
            this.manager = (NguiWindowManager) message.Data;
            return;
          case Message.ApplySettings:
            if ((Object) null != (Object) this.manager.DialogBox)
            {
              this.manager.ShowDialogBox((DialogBoxData) message.Data);
              return;
            }
            Debug.LogWarning((object) "GUI MEDIATOR == MESSAGEBOX IS NULL");
            return;
          case Message.SaveSettings:
            if (!((Object) null != (Object) this.manager.DialogBox))
              return;
            if (this.manager.GetContentWindowAmount() == 0)
              this.ToggleOldUI(true);
            this.manager.HideDialogBox();
            return;
          default:
            if (id == Message.GuiContentWindowVisibility)
            {
              this.ContentWindowVisibility(message);
              return;
            }
            Log.Warning((object) ("Unknown Message was catched but not handled by the GuiMediator => " + (object) message.Id));
            return;
        }
    }
  }

  private void ContentWindowVisibility(IMessage<Message> message)
  {
    Tuple<WindowTypes, bool> tuple = (Tuple<WindowTypes, bool>) message.Data;
    ContentWindowWidget contentWindowByType = this.manager.GetContentWindowByType(tuple.First);
    if (!((Object) contentWindowByType != (Object) null))
      return;
    if (tuple.Second)
      contentWindowByType.Open();
    else
      contentWindowByType.Close();
  }

  private void ToggleOldUI(bool show)
  {
    Game.GUIManager.IsRendered = show;
  }

  private void ToggleUguiCanvas(BsgoCanvas bsgoCanvas, bool show)
  {
    UguiTools.EnableCanvas(bsgoCanvas, show);
  }

  private void ToggleNgui(bool show)
  {
    this.NguiRoot.gameObject.SetActive(show);
  }

  private void ToggleInputActive(bool show)
  {
    GameLevel.Instance.UpdateInputManager = show;
  }

  public bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return this.manager.GetContentWindowAmount() > 0;
  }

  public bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return this.manager.GetContentWindowAmount() > 0;
  }

  public void OnMouseMove(float2 mousePosition)
  {
  }

  public bool OnMouseScrollDown()
  {
    return this.manager.GetContentWindowAmount() > 0;
  }

  public bool OnMouseScrollUp()
  {
    return this.manager.GetContentWindowAmount() > 0;
  }

  public bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    return false;
  }

  public bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (action != Action.ToggleWindowOptions && action != Action.ToggleWindowTutorial)
      return action == Action.ToggleFullscreen;
    return true;
  }

  public bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (action == Action.ToggleFullscreen)
    {
      SettingProtocol.GetProtocol().FullScreen();
      FacadeFactory.GetInstance().SendMessage(Message.SetFullscreen, (object) !Screen.fullScreen);
      return true;
    }
    if (!this.NguiRoot.gameObject.activeSelf)
      return true;
    if (action == Action.ToggleWindowOptions)
    {
      this.OwnerFacade.SendMessage(Message.ShowOptionsWindow);
      return true;
    }
    if (action != Action.ToggleWindowTutorial)
      return this.manager.GetContentWindowAmount() > 0;
    this.OwnerFacade.SendMessage(Message.ShowHelpWindow);
    return true;
  }
}
