﻿// Decompiled with JetBrains decompiler
// Type: MiningShipScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class MiningShipScript : ModelScript
{
  public Animation Animation;
  public ParticlesHelper ParticlesHelper;
  public CreatingCause CreatingCause;
  public bool IsLanding;

  private void Start()
  {
    if (this.IsLanding)
      return;
    if (this.SpaceObject != null)
      this.CreatingCause = this.SpaceObject.CreatingCause;
    if (this.CreatingCause == CreatingCause.JumpIn)
      this.BeginOrContinueLanding();
    else
      this.SkipLanding();
  }

  private void Update()
  {
    this.IsLanding = this.Animation.isPlaying;
  }

  public void BeginOrContinueLanding()
  {
    this.IsLanding = true;
    this.Animation.Play("land");
    if (!((Object) null != (Object) this.ParticlesHelper))
      return;
    this.ParticlesHelper.emitStartDelay = this.Animation["land"].length - this.Animation["land"].time;
  }

  private void SkipLanding()
  {
    this.Animation.Play("land");
    this.Animation["land"].normalizedTime = 1f;
    if (!((Object) null != (Object) this.ParticlesHelper))
      return;
    this.ParticlesHelper.emitStartDelay = 0.0f;
  }

  public float GetLandingAnimationTime()
  {
    return this.Animation["land"].normalizedTime;
  }

  public void SetLandingAnimationTime(float normalizedAnimTime)
  {
    this.Animation["land"].normalizedTime = normalizedAnimTime;
  }

  public void TakeOff()
  {
    this.StartCoroutine(this.TakeOffLoop());
  }

  [DebuggerHidden]
  private IEnumerator TakeOffLoop()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new MiningShipScript.\u003CTakeOffLoop\u003Ec__Iterator39() { \u003C\u003Ef__this = this };
  }
}
