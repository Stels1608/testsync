﻿// Decompiled with JetBrains decompiler
// Type: EmergencyNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class EmergencyNotification : OnScreenNotification
{
  private readonly float time;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.Emergency;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.System;
    }
  }

  public override string TextMessage
  {
    get
    {
      return BsgoLocalization.Get("%$bgo.notif.server_shutdown%", (object) (Mathf.Floor(this.time * 10f) / 10f));
    }
  }

  public override bool Show
  {
    get
    {
      return true;
    }
  }

  public EmergencyNotification(string message, float time)
  {
    this.time = time;
    FacadeFactory.GetInstance().SendMessage(Message.ChatSystemPositive, (object) this.TextMessage);
  }
}
