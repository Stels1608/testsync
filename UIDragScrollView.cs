﻿// Decompiled with JetBrains decompiler
// Type: UIDragScrollView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Drag Scroll View")]
public class UIDragScrollView : MonoBehaviour
{
  public UIScrollView scrollView;
  [HideInInspector]
  [SerializeField]
  private UIScrollView draggablePanel;
  private Transform mTrans;
  private UIScrollView mScroll;
  private bool mAutoFind;
  private bool mStarted;

  private void OnEnable()
  {
    this.mTrans = this.transform;
    if ((Object) this.scrollView == (Object) null && (Object) this.draggablePanel != (Object) null)
    {
      this.scrollView = this.draggablePanel;
      this.draggablePanel = (UIScrollView) null;
    }
    if (!this.mStarted || !this.mAutoFind && !((Object) this.mScroll == (Object) null))
      return;
    this.FindScrollView();
  }

  private void Start()
  {
    this.mStarted = true;
    this.FindScrollView();
  }

  private void FindScrollView()
  {
    UIScrollView inParents = NGUITools.FindInParents<UIScrollView>(this.mTrans);
    if ((Object) this.scrollView == (Object) null)
    {
      this.scrollView = inParents;
      this.mAutoFind = true;
    }
    else if ((Object) this.scrollView == (Object) inParents)
      this.mAutoFind = true;
    this.mScroll = this.scrollView;
  }

  private void OnPress(bool pressed)
  {
    if (this.mAutoFind && (Object) this.mScroll != (Object) this.scrollView)
    {
      this.mScroll = this.scrollView;
      this.mAutoFind = false;
    }
    if (!(bool) ((Object) this.scrollView) || !this.enabled || !NGUITools.GetActive(this.gameObject))
      return;
    this.scrollView.Press(pressed);
    if (pressed || !this.mAutoFind)
      return;
    this.scrollView = NGUITools.FindInParents<UIScrollView>(this.mTrans);
    this.mScroll = this.scrollView;
  }

  private void OnDrag(Vector2 delta)
  {
    if (!(bool) ((Object) this.scrollView) || !NGUITools.GetActive((Behaviour) this))
      return;
    this.scrollView.Drag();
  }

  private void OnScroll(float delta)
  {
    if (!(bool) ((Object) this.scrollView) || !NGUITools.GetActive((Behaviour) this))
      return;
    this.scrollView.Scroll(delta);
  }
}
