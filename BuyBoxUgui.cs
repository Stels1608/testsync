﻿// Decompiled with JetBrains decompiler
// Type: BuyBoxUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuyBoxUgui : BuyBoxUi
{
  [SerializeField]
  private TextMeshProUGUI headerLabel;
  [SerializeField]
  private Button okButton;
  [SerializeField]
  private TextMeshProUGUI okButtonLabel;
  [SerializeField]
  private TextMeshProUGUI cancelButtonLabel;
  [SerializeField]
  private TextMeshProUGUI messageLabel;
  [SerializeField]
  private TextMeshProUGUI costLabel;
  [SerializeField]
  private PriceTagUgui priceTagPrototype;
  private List<PriceTagUgui> priceList;

  protected override void Awake()
  {
    this.priceTagPrototype.gameObject.SetActive(false);
    this.priceList = new List<PriceTagUgui>();
  }

  protected override void SetTitleText(string text)
  {
    this.headerLabel.text = text;
  }

  protected override void SetOkButtonText(string text)
  {
    this.okButtonLabel.text = text;
  }

  protected override void EnableOkButton(bool enableOkButton)
  {
    this.okButton.interactable = enableOkButton;
  }

  protected override void SetCancelButtonText(string text)
  {
    this.cancelButtonLabel.text = text;
  }

  protected override void SetMainText(string mainText)
  {
    this.messageLabel.text = mainText;
  }

  protected override void SetCostLabel(string costText)
  {
    this.costLabel.text = costText;
  }

  protected override void DestroyWindow()
  {
    Object.Destroy((Object) this.gameObject);
  }

  protected override void ClearPrices()
  {
    using (List<PriceTagUgui>.Enumerator enumerator = this.priceList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current.gameObject);
    }
  }

  protected override void AddResourcePrice(string iconPath, float cost)
  {
    GameObject child = UguiTools.CreateChild(this.priceTagPrototype.gameObject, this.priceTagPrototype.transform.parent);
    child.SetActive(true);
    PriceTagUgui component = child.GetComponent<PriceTagUgui>();
    component.SetCost(cost);
    component.SetResourceIcon(iconPath);
  }
}
