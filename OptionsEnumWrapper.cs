﻿// Decompiled with JetBrains decompiler
// Type: OptionsEnumWrapper
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class OptionsEnumWrapper
{
  public object actualValue = (object) -1;
  public List<string> valueText = new List<string>();
  public List<object> valueList = new List<object>();

  public OptionsEnumWrapper()
  {
  }

  public OptionsEnumWrapper(OptionsEnumWrapper copy)
  {
    this.valueList = new List<object>((IEnumerable<object>) copy.valueList);
    this.valueText = new List<string>((IEnumerable<string>) copy.valueText);
    this.actualValue = copy.actualValue;
  }
}
