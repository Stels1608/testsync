﻿// Decompiled with JetBrains decompiler
// Type: CutsceneFtlTrapEvents
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CutsceneFtlTrapEvents : MonoBehaviour
{
  public void FtlTrapOpen()
  {
    FtlTrapBehaviour ftlTrapBehaviour = this.GetFtlTrapBehaviour();
    if ((Object) ftlTrapBehaviour == (Object) null)
      return;
    ftlTrapBehaviour.PlayOpenAnim();
  }

  public void FtlTrapCloseSlowly()
  {
    FtlTrapBehaviour ftlTrapBehaviour = this.GetFtlTrapBehaviour();
    if ((Object) ftlTrapBehaviour == (Object) null)
      return;
    ftlTrapBehaviour.PlayCloseAnimSlowly();
  }

  public void FtlTrapStartPulse()
  {
    FtlTrapBehaviour ftlTrapBehaviour = this.GetFtlTrapBehaviour();
    if ((Object) ftlTrapBehaviour == (Object) null)
      return;
    ftlTrapBehaviour.StartPulseEffect();
  }

  public void FtlTrapEnablePulsingLoadingLight()
  {
    FtlTrapBehaviour ftlTrapBehaviour = this.GetFtlTrapBehaviour();
    if ((Object) ftlTrapBehaviour == (Object) null)
      return;
    ftlTrapBehaviour.EnablePulsingLoadingLight();
  }

  public void FtlTrapEnableFullLoadingLight()
  {
    FtlTrapBehaviour ftlTrapBehaviour = this.GetFtlTrapBehaviour();
    if ((Object) ftlTrapBehaviour == (Object) null)
      return;
    ftlTrapBehaviour.EnableFullLoadingLight();
  }

  public void FtlTrapDisableLoadingLight()
  {
    FtlTrapBehaviour ftlTrapBehaviour = this.GetFtlTrapBehaviour();
    if ((Object) ftlTrapBehaviour == (Object) null)
      return;
    ftlTrapBehaviour.DisableLoadingLight();
  }

  private FtlTrapBehaviour GetFtlTrapBehaviour()
  {
    GameObject actorById = CutsceneActor.FindActorById("ftl_trap");
    if ((Object) actorById == (Object) null)
    {
      Debug.LogError((object) "GetFtlTrapBehaviour(): Didn't find ftl_trap.");
      return (FtlTrapBehaviour) null;
    }
    FtlTrapBehaviour component = actorById.GetComponent<FtlTrapBehaviour>();
    if (!((Object) component == (Object) null))
      return component;
    Debug.LogError((object) "GetFtlTrapBehaviour(): ftl_trap didn't have an FtlTrapBehaviour.");
    return (FtlTrapBehaviour) null;
  }
}
