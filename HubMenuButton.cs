﻿// Decompiled with JetBrains decompiler
// Type: HubMenuButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class HubMenuButton : MonoBehaviour
{
  public UnityEngine.Sprite normal;
  public UnityEngine.Sprite mouseOver;
  public UnityEngine.Sprite cylonIcon;
  public UILabel label;
  public UI2DSprite background;
  public UI2DSprite icon;
  private bool selected;
  public AnonymousDelegate OnClicked;

  public bool Selected
  {
    get
    {
      return this.selected;
    }
    set
    {
      this.selected = value;
      this.OnHover(this.Selected);
    }
  }

  private void Start()
  {
    if (Game.Me.Faction == Faction.Cylon && (Object) this.cylonIcon != (Object) null)
      this.icon.sprite2D = this.cylonIcon;
    this.OnHover(false);
  }

  public void OnHover(bool isOver)
  {
    if (this.Selected)
      return;
    this.label.color = !isOver ? Color.white : Gui.Options.MouseOverInvertColor;
    this.icon.color = !isOver ? Color.white : Gui.Options.MouseOverInvertColor;
    this.background.sprite2D = !isOver ? this.normal : this.mouseOver;
    this.background.color = !isOver ? ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR) : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_CLICK_COLOR);
  }

  public virtual void OnClick()
  {
    this.OnClicked();
  }

  public void SetText(string text)
  {
    this.label.text = text.ToUpper();
  }
}
