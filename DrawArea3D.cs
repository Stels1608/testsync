﻿// Decompiled with JetBrains decompiler
// Type: DrawArea3D
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DrawArea3D : DrawArea
{
  public Matrix4x4 matrix;

  public DrawArea3D(Vector3 min, Vector3 max, Matrix4x4 matrix)
    : base(min, max)
  {
    this.matrix = matrix;
  }

  public override Vector3 Point(Vector3 p)
  {
    return this.matrix.MultiplyPoint3x4(Vector3.Scale(new Vector3((float) (((double) p.x - (double) this.canvasMin.x) / ((double) this.canvasMax.x - (double) this.canvasMin.x)), (float) (((double) p.y - (double) this.canvasMin.y) / ((double) this.canvasMax.y - (double) this.canvasMin.y)), p.z), this.max - this.min) + this.min);
  }
}
