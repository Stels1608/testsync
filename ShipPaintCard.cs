﻿// Decompiled with JetBrains decompiler
// Type: ShipPaintCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class ShipPaintCard : Card
{
  public GUICard GUI;

  public ShipPaintCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.GUI = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
  }
}
