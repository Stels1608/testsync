﻿// Decompiled with JetBrains decompiler
// Type: GuiMail
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiMail : GuiDialog
{
  private GuiPanelVerticalScroll scroll = new GuiPanelVerticalScroll();
  private readonly GuiLabel title = new GuiLabel(Gui.Options.FontBGM_BT, 20);
  private readonly GuiLabel from = new GuiLabel(Gui.Options.FontBGM_BT, 13);
  private readonly GuiLabel expires = new GuiLabel(Gui.Options.FontBGM_BT, 13);
  private readonly GuiPanel body = new GuiPanel(new Vector2(100f, 100f), Vector2.zero);
  private readonly GuiLabel attachments = new GuiLabel("%$bgo.mail.attachments%", Gui.Options.FontBGM_BT);
  private readonly QueueHorizontal reward = new QueueHorizontal();
  private readonly GuiButton takeAll = new GuiButton("%$bgo.mail.take_all%");
  private readonly GuiColoredBox line1 = new GuiColoredBox(Gui.Options.PositiveColor, new Vector2(480f, 1f));
  private readonly GuiColoredBox line2 = new GuiColoredBox(Gui.Options.PositiveColor, new Vector2(480f, 1f));
  private readonly GuiColoredBox line3 = new GuiColoredBox(Gui.Options.PositiveColor, new Vector2(480f, 1f));
  private GuiMail.Item selectedMail;

  public GuiMail()
  {
    this.Size = new Vector2(925f, 443f);
    this.Position = new Vector2(0.0f, 0.0f);
    this.scroll.Size = new Vector2(400f, 437f);
    this.scroll.AutoSizeChildren = true;
    this.scroll.RenderDelimiters = true;
    this.AddChild((GuiElementBase) this.scroll, new Vector2(2f, 2f));
    this.scroll.OnSelectionChanged = (System.Action<ISelectable>) (el =>
    {
      this.selectedMail = el as GuiMail.Item;
      this.SelectionChanged(this.selectedMail);
    });
    this.title.WordWrap = true;
    this.title.AutoSize = false;
    this.title.Size = new Vector2(450f, 40f);
    this.title.Alignment = TextAnchor.MiddleCenter;
    this.AddChild((GuiElementBase) this.title, Align.UpCenter, new Vector2(212f, 5f));
    this.AddChild((GuiElementBase) this.line1, Align.UpCenter, new Vector2(212f, 50f));
    this.AddChild((GuiElementBase) this.from, Align.UpLeft, new Vector2(445f, 60f));
    this.AddChild((GuiElementBase) this.expires, Align.UpRight, new Vector2(-20f, 60f));
    this.AddChild((GuiElementBase) this.line2, Align.UpCenter, new Vector2(212f, 86f));
    this.body.Size = new Vector2(480f, 225f);
    this.AddChild((GuiElementBase) this.body, Align.UpCenter, new Vector2(212f, 95f));
    this.AddChild((GuiElementBase) this.line3, Align.UpCenter, new Vector2(212f, 345f));
    this.AddChild((GuiElementBase) this.attachments, Align.UpLeft, new Vector2(445f, 325f));
    this.reward.ContainerAutoSize = true;
    this.AddChild((GuiElementBase) this.reward, Align.UpCenter, new Vector2(212f, 355f));
    this.AddChild((GuiElementBase) this.takeAll, Align.UpCenter, new Vector2(212f, 400f));
    this.takeAll.Pressed = (AnonymousDelegate) (() =>
    {
      if (this.selectedMail != null)
      {
        if (Game.Me.Hold.Count + this.selectedMail.mail.Items.Count >= 70)
        {
          if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null)
          {
            new InfoBox(Tools.ParseMessage("%$bgo.EquipBuyPanel.gui_filledhold_layout.text_label%")).Show();
            return;
          }
          new InfoBox(Tools.ParseMessage("%$bgo.mail.gui_filledhold_layout.text_label%")).Show();
          this.selectedMail.mail.Items.MoveAll((IContainer) Game.Me.Locker);
        }
        else
          this.selectedMail.mail.Items.MoveAll((IContainer) Game.Me.Hold);
      }
      this.selectedMail.DeleteButton.IsRendered = this.selectedMail.mail.Items != null && this.selectedMail.mail.Items.Count == 0;
    });
    this.SelectionChanged((GuiMail.Item) null);
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    int count = Game.Me.MailBox.Count;
    while (this.scroll.Children.Count > count)
      this.scroll.RemoveChild(this.scroll.Children[this.scroll.Children.Count - 1]);
    while (this.scroll.Children.Count < count)
      this.scroll.AddChild((GuiElementBase) new GuiMail.Item());
    int i = 0;
    foreach (Mail mail in (IEnumerable<Mail>) Game.Me.MailBox)
    {
      (this.scroll.Children[i] as GuiMail.Item).SetMail(mail, i, count);
      ++i;
    }
    this.SelectionChanged(this.selectedMail);
  }

  private void SelectionChanged(GuiMail.Item item)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiMail.\u003CSelectionChanged\u003Ec__AnonStorey7E changedCAnonStorey7E = new GuiMail.\u003CSelectionChanged\u003Ec__AnonStorey7E();
    // ISSUE: reference to a compiler-generated field
    changedCAnonStorey7E.item = item;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    changedCAnonStorey7E.mail = changedCAnonStorey7E.item != null ? changedCAnonStorey7E.item.mail : (Mail) null;
    // ISSUE: reference to a compiler-generated field
    bool flag = changedCAnonStorey7E.mail != null;
    this.title.IsRendered = flag;
    this.from.IsRendered = flag;
    this.expires.IsRendered = flag;
    this.body.IsRendered = flag;
    this.line1.IsRendered = flag;
    this.line2.IsRendered = flag;
    // ISSUE: reference to a compiler-generated field
    this.reward.IsRendered = flag && changedCAnonStorey7E.mail.Items.Count > 0;
    // ISSUE: reference to a compiler-generated field
    this.takeAll.IsRendered = flag && changedCAnonStorey7E.mail.Items.Count > 0;
    // ISSUE: reference to a compiler-generated field
    this.attachments.IsRendered = flag && changedCAnonStorey7E.mail.Items.Count > 0;
    // ISSUE: reference to a compiler-generated field
    this.line3.IsRendered = flag && changedCAnonStorey7E.mail.Items.Count > 0;
    if (!flag)
      return;
    // ISSUE: reference to a compiler-generated field
    if (changedCAnonStorey7E.mail.Status == Mail.MailStatus.Unread)
    {
      // ISSUE: reference to a compiler-generated field
      PlayerProtocol.GetProtocol().ReadMail(changedCAnonStorey7E.mail.ServerID);
    }
    // ISSUE: reference to a compiler-generated field
    this.title.Text = changedCAnonStorey7E.mail.Card.GUI.Name;
    // ISSUE: reference to a compiler-generated field
    this.from.Text = BsgoLocalization.Get("%$bgo.mail.from%", (object) changedCAnonStorey7E.mail.Card.SenderCard.Name);
    // ISSUE: reference to a compiler-generated field
    this.expires.Text = BsgoLocalization.Get("%$bgo.mail.expires%", (object) changedCAnonStorey7E.mail.Expires);
    this.body.RemoveAll();
    // ISSUE: reference to a compiler-generated field
    Gui2Loader.LoadRaw((GuiElementBase) this.body, changedCAnonStorey7E.mail.Card.Body);
    List<GuiLabel> guiLabelList = this.body.ChildrenOfType<GuiLabel>();
    if (guiLabelList != null && guiLabelList.Count > 0)
    {
      string str1 = Game.Me.Faction != Faction.Colonial ? ".BodyCylon_Text%" : ".Body_Text%";
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      string str2 = BsgoLocalization.Get("%$bgo." + changedCAnonStorey7E.mail.Card.GUI.Key + str1, (object[]) changedCAnonStorey7E.mail.Parameters);
      if (Game.Me.Faction == Faction.Cylon && str2.Contains(".BodyCylon_Text"))
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        str2 = BsgoLocalization.Get("%$bgo." + changedCAnonStorey7E.mail.Card.GUI.Key + ".Body_Text%", (object[]) changedCAnonStorey7E.mail.Parameters);
      }
      guiLabelList[0].Text = str2;
      guiLabelList[0].WordWrap = true;
      guiLabelList[0].Size = new Vector2(470f, 225f);
    }
    this.body.Size = new Vector2(480f, 225f);
    this.body.Align = Align.UpCenter;
    this.body.Position = new Vector2(212f, 95f);
    using (List<GuiButton>.Enumerator enumerator = this.body.ChildrenOfType<GuiButton>().GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.PressedAction = (System.Action<GuiButton>) (bb => Log.Add("Action " + bb.ActionString));
    }
    this.reward.RemoveAll((IEnumerable) new List<GuiElementBase>((IEnumerable<GuiElementBase>) this.reward.Children));
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiMail.\u003CSelectionChanged\u003Ec__AnonStorey7D changedCAnonStorey7D = new GuiMail.\u003CSelectionChanged\u003Ec__AnonStorey7D();
    // ISSUE: reference to a compiler-generated field
    changedCAnonStorey7D.\u003C\u003Ef__ref\u0024126 = changedCAnonStorey7E;
    // ISSUE: reference to a compiler-generated field
    foreach (ShipItem shipItem in (IEnumerable<ShipItem>) changedCAnonStorey7E.mail.Items)
    {
      // ISSUE: reference to a compiler-generated field
      changedCAnonStorey7D.si = shipItem;
      GuiAtlasImage guiAtlasImage = new GuiAtlasImage();
      // ISSUE: reference to a compiler-generated field
      guiAtlasImage.GuiCard = changedCAnonStorey7D.si.ItemGUICard;
      this.reward.AddChild((GuiElementBase) guiAtlasImage);
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      string str = !string.IsNullOrEmpty(changedCAnonStorey7D.si.ItemGUICard.Description) ? changedCAnonStorey7D.si.ItemGUICard.Description : changedCAnonStorey7D.si.ItemGUICard.Name;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      guiAtlasImage.SetTooltip(!(changedCAnonStorey7D.si is ItemCountable) ? str : string.Format("{0} {1}", (object) (changedCAnonStorey7D.si as ItemCountable).Count, (object) changedCAnonStorey7D.si.ItemGUICard.Name));
      // ISSUE: reference to a compiler-generated field
      guiAtlasImage.SetEnvironment((object) changedCAnonStorey7D.si);
      // ISSUE: reference to a compiler-generated method
      guiAtlasImage.OnClickAction = new System.Action<GuiElementBase>(changedCAnonStorey7D.\u003C\u003Em__91);
    }
  }

  public class Item : GuiPanel, ISelectable
  {
    private GuiImage bgrnd = new GuiImage();
    public Mail mail;
    private GuiLabel title;
    private GuiLabel expires;
    private GuiButton deleteButton;

    public GuiButton DeleteButton
    {
      get
      {
        return this.deleteButton;
      }
    }

    public Item()
    {
      this.AddChild((GuiElementBase) this.bgrnd);
      Gui2Loader.Load((GuiElementBase) this, "GUI/CharacterStatusWindow/mail/item");
      this.title = this.Find<GuiLabel>("title");
      this.expires = this.Find<GuiLabel>("expires");
      this.deleteButton = this.Find<GuiButton>("delete");
      this.deleteButton.Pressed = new AnonymousDelegate(this.DeleteMail);
      this.OnClick = (AnonymousDelegate) (() => this.FindParent<ISelectableContainer>().Selected = (ISelectable) this);
    }

    private void DeleteMail()
    {
      PlayerProtocol.GetProtocol().RemoveMail(this.mail.ServerID);
    }

    public void SetMail(Mail mail, int i, int total)
    {
      this.mail = mail;
      this.deleteButton.IsRendered = mail != null && mail.Items != null && mail.Items.Count == 0;
      this.title.Text = mail.Card.GUI.Name;
      this.expires.Text = BsgoLocalization.Get("%$bgo.mail.expires%", (object) mail.Expires);
      this.bgrnd.IsRendered = mail.Status == Mail.MailStatus.Unread;
      if (!this.bgrnd.IsRendered)
        return;
      this.bgrnd.Texture = i != 0 ? (i != total - 1 ? Gui.Options.QueueSelectTextureNormal : Gui.Options.QueueSelectTextureDown) : Gui.Options.QueueSelectTextureUp;
      this.bgrnd.Size = this.Size;
    }

    public void OnSelected()
    {
    }

    public void OnDeselected()
    {
    }
  }
}
