﻿// Decompiled with JetBrains decompiler
// Type: DirectionalWithoutRollManeuver
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class DirectionalWithoutRollManeuver : Maneuver
{
  protected Euler3 direction;
  protected bool slide;

  public override string ToString()
  {
    return string.Format("DirectionalWithoutRollManeuver: direction={0}", (object) this.direction);
  }

  public override MovementFrame NextFrame(Tick tick, MovementFrame prevFrame)
  {
    return Simulation.MoveToDirectionWithoutRoll(prevFrame, this.direction, this.options);
  }

  public override void Read(BgoProtocolReader pr)
  {
    base.Read(pr);
    this.startTick = pr.ReadTick();
    this.direction = pr.ReadEuler();
    this.options.Read(pr);
  }
}
