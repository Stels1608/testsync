﻿// Decompiled with JetBrains decompiler
// Type: MovingNebula
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MovingNebula : MonoBehaviour
{
  private float magicNumber = 0.2f;
  private float nebulaSphereRadius = 2.3f;
  private Vector3 initialPosition = Vector3.zero;
  private const float expRange = 5f;
  private float maxDistanceShipCanFly;
  private bool editorLevelLoaded;

  private void Start()
  {
    this.initialPosition = this.transform.position;
    this.editorLevelLoaded = SectorEditorHelper.IsEditorLevel();
    if ((Object) SpaceLevel.GetLevel() == (Object) null)
    {
      this.enabled = false;
    }
    else
    {
      Vector2 vector2 = new Vector2();
      SpaceLevel.GetLevel().GetSectorSize(out vector2.x, out vector2.y);
      vector2.x *= 1.333333f;
      vector2.y *= 1.333333f;
      this.maxDistanceShipCanFly = vector2.magnitude;
      this.nebulaSphereRadius = this.transform.localScale.x / 2f - Camera.main.nearClipPlane - this.magicNumber;
    }
  }

  private void Update()
  {
    if (this.editorLevelLoaded)
      return;
    Vector3 playerPosition = SpaceLevel.GetLevel().GetPlayerPosition();
    playerPosition.y = 0.0f;
    playerPosition.x /= this.maxDistanceShipCanFly;
    playerPosition.z /= this.maxDistanceShipCanFly;
    float num = (1f - Mathf.Clamp01(Mathf.Exp((float) (-(double) playerPosition.magnitude * 5.0)))) * this.nebulaSphereRadius;
    playerPosition.Normalize();
    Vector3 position = this.transform.position;
    this.transform.position = this.initialPosition - playerPosition * num;
  }
}
