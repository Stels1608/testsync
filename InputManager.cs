﻿// Decompiled with JetBrains decompiler
// Type: InputManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class InputManager
{
  private readonly InputDispatcher inputDispatcher;
  private readonly InputReceiverJoystick inputReceiverJoystick;
  private readonly InputReceiverKeyboard inputReceiverKeyboard;
  private readonly JoystickSetup joystickSetup;
  private readonly MouseSetup mouseSetup;

  public InputDispatcher InputDispatcher
  {
    get
    {
      return this.inputDispatcher;
    }
  }

  public JoystickSetup JoystickSetup
  {
    get
    {
      return this.joystickSetup;
    }
  }

  public MouseSetup MouseSetup
  {
    get
    {
      return this.mouseSetup;
    }
  }

  public InputManager()
  {
    this.inputDispatcher = new InputDispatcher();
    this.inputReceiverKeyboard = new InputReceiverKeyboard(this.inputDispatcher);
    this.inputReceiverJoystick = new InputReceiverJoystick(this.inputDispatcher);
    this.joystickSetup = new JoystickSetup();
    this.mouseSetup = new MouseSetup();
    JoystickSetup.PrintJoystickNames();
  }

  public void UpdateInputs()
  {
    this.inputReceiverKeyboard.Update();
    this.inputReceiverJoystick.Update();
    this.inputDispatcher.UpdateInactiveNotification();
  }
}
