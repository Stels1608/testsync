﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorColorSchemeFriendOrFoe
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HudIndicatorColorSchemeFriendOrFoe : IHudIndicatorColorScheme
{
  private float alpha
  {
    get
    {
      return 0.6f;
    }
  }

  private Color AddAlpha(Color color)
  {
    color.a = this.alpha;
    return color;
  }

  private Color EntityColor(ISpaceEntity entity)
  {
    Color color = TargetColors.neutralColor;
    if (entity == null)
      return color;
    if (entity.SpaceEntityType == SpaceEntityType.SectorEvent)
      return TargetColors.sectorEventColor;
    if (entity.PlayerRelation == Relation.Neutral)
      color = TargetColors.neutralColor;
    if (entity.PlayerRelation == Relation.Enemy)
      color = TargetColors.enemyColorFoF;
    if (entity.PlayerRelation == Relation.Friend)
      color = TargetColors.friendlyColor;
    if (entity is PlayerShip && Game.Me.Party.Members.Contains((entity as PlayerShip).Player))
      color = Game.Me.Faction != Faction.Colonial ? TargetColors.cylonPartyColor : TargetColors.colonialPartyColor;
    return color;
  }

  public Color TextColor(ISpaceEntity entity)
  {
    return this.EntityColor(entity);
  }

  public Color BracketColor(ISpaceEntity entity)
  {
    return this.AddAlpha(this.EntityColor(entity));
  }

  public Color HealthBarColor(ISpaceEntity entity)
  {
    return HudIndicatorColorsShared.HealthBarColor;
  }

  public Color HealthBarOutlineColor(ISpaceEntity entity)
  {
    return this.AddAlpha(this.EntityColor(entity));
  }

  public Color SelectionColor(ISpaceEntity entity)
  {
    return this.AddAlpha(this.EntityColor(entity));
  }

  public Color IconColor(ISpaceEntity entity)
  {
    return this.AddAlpha(TargetColors.GetFactionColor(entity.Faction));
  }
}
