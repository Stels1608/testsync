﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DLegendUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;
using UnityEngine.UI;

public class SystemMap3DLegendUgui : Window
{
  [SerializeField]
  private Text headerLabel;
  [SerializeField]
  private GameObject keysRoot;
  private SystemMap3DManager systemMap3DManager;

  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  protected override bool IsInCombatGui
  {
    get
    {
      return false;
    }
  }

  private bool IsVisible
  {
    get
    {
      return this.gameObject.activeSelf;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.SetLocalizedTexts();
  }

  protected override void Start()
  {
    base.Start();
    this.CloseWindow();
  }

  private void SetHeaderText(string text)
  {
    this.headerLabel.text = text;
  }

  private void CloseWindow()
  {
    this.IsVisible = false;
  }

  public void ToggleVisibility()
  {
    this.IsVisible = !this.IsVisible;
  }

  private void SetLocalizedTexts()
  {
    this.SetHeaderText(Tools.ParseMessage("%$bgo.sector_map.map_legend.title%"));
    foreach (Text componentsInChild in this.keysRoot.GetComponentsInChildren<Text>())
      componentsInChild.text = Tools.ParseMessage(componentsInChild.text);
  }
}
