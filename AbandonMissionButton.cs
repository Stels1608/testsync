﻿// Decompiled with JetBrains decompiler
// Type: AbandonMissionButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class AbandonMissionButton : MonoBehaviour
{
  private static DialogBoxUi openedBox = (DialogBoxUi) null;
  public static System.Action SkipDialogAction = (System.Action) (() =>
  {
    if ((UnityEngine.Object) AbandonMissionButton.openedBox != (UnityEngine.Object) null)
      return;
    DialogBoxUi dialogBox = MessageBoxFactory.CreateDialogBox(BsgoCanvas.Windows);
    dialogBox.SetContent((OnMessageBoxClose) (actionType =>
    {
      if (actionType != MessageBoxActionType.Ok)
        return;
      StoryProtocol.GetProtocol().Skip();
    }), "%$bgo.etc.abort_mission1%", "%$bgo.etc.abort_mission_confirm1%", "%$bgo.etc.abort_mission_confirm_ok1%", "%$bgo.etc.abort_mission_confirm_cancel1%", 0U);
    AbandonMissionButton.openedBox = dialogBox;
  });
  public static System.Action AbandonDialogAction = (System.Action) (() =>
  {
    if ((UnityEngine.Object) AbandonMissionButton.openedBox != (UnityEngine.Object) null)
      return;
    DialogBoxUi dialogBox = MessageBoxFactory.CreateDialogBox(BsgoCanvas.Windows);
    dialogBox.SetContent((OnMessageBoxClose) (actionType =>
    {
      if (actionType != MessageBoxActionType.Ok)
        return;
      StoryProtocol.GetProtocol().Abandon();
    }), "%$bgo.etc.abandon_mission1%", "%$bgo.etc.abandon_mission_confirm1%", "%$bgo.etc.abort_mission_confirm_ok1%", "%$bgo.etc.abort_mission_confirm_cancel1%", 0U);
    AbandonMissionButton.openedBox = dialogBox;
  });
  private System.Action buttonAction = (System.Action) (() => {});
  public UILabel ButtonText;
  public UIWidget Anchor;
  private bool inTutorial;

  public void SetupButton(bool inTutorial)
  {
    if (inTutorial)
    {
      this.ButtonText.text = BsgoLocalization.Get("bgo.etc.skip_tutorial_button");
      this.buttonAction = AbandonMissionButton.SkipDialogAction;
      this.Anchor.leftAnchor.absolute = -333;
      this.Anchor.rightAnchor.absolute = -127;
      this.Anchor.bottomAnchor.absolute = -38;
      this.Anchor.topAnchor.absolute = -64;
    }
    else
    {
      this.ButtonText.text = BsgoLocalization.Get("bgo.etc.abandon_mission_button");
      this.buttonAction = AbandonMissionButton.AbandonDialogAction;
      this.Anchor.leftAnchor.absolute = -450;
      this.Anchor.rightAnchor.absolute = -244;
      this.Anchor.bottomAnchor.absolute = -38;
      this.Anchor.topAnchor.absolute = -64;
    }
  }

  public void OnPressed()
  {
    this.buttonAction();
  }

  public void Remove()
  {
    if ((UnityEngine.Object) AbandonMissionButton.openedBox != (UnityEngine.Object) null)
      AbandonMissionButton.openedBox.CloseWindow(MessageBoxActionType.Cancel);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }
}
