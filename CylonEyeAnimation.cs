﻿// Decompiled with JetBrains decompiler
// Type: CylonEyeAnimation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CylonEyeAnimation : MonoBehaviour
{
  private float speedAnimation = 0.8f;
  private float maxOffset = 0.4f;
  private float currentOffset;
  private int direction;

  private void Update()
  {
    if (this.direction == 0)
    {
      this.currentOffset += this.speedAnimation * Time.deltaTime;
      if ((double) this.currentOffset > (double) this.maxOffset)
        this.direction = 1;
    }
    else
    {
      this.currentOffset -= this.speedAnimation * Time.deltaTime;
      if ((double) this.currentOffset < -(double) this.maxOffset)
        this.direction = 0;
    }
    this.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(this.currentOffset, 0.0f);
  }
}
