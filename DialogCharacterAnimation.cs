﻿// Decompiled with JetBrains decompiler
// Type: DialogCharacterAnimation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class DialogCharacterAnimation : MonoBehaviour
{
  private Animation anim;

  private void Start()
  {
    Transform transform = this.transform;
    while ((UnityEngine.Object) transform.GetComponent<Animation>() == (UnityEngine.Object) null && (UnityEngine.Object) transform.parent != (UnityEngine.Object) null)
      transform = transform.parent;
    if ((UnityEngine.Object) transform.GetComponent<Animation>() == (UnityEngine.Object) null)
      throw new Exception("Character have no animation [" + this.name + "]");
    this.anim = transform.GetComponent<Animation>();
    if ((UnityEngine.Object) this.anim.GetClip("idle") != (UnityEngine.Object) null)
      this.anim["idle"].wrapMode = WrapMode.Loop;
    if ((UnityEngine.Object) this.anim.GetClip("idle_to_talk") != (UnityEngine.Object) null)
      this.anim["idle_to_talk"].wrapMode = WrapMode.Default;
    if ((UnityEngine.Object) this.anim.GetClip("talk") != (UnityEngine.Object) null)
      this.anim["talk"].wrapMode = WrapMode.Loop;
    if (!((UnityEngine.Object) this.anim.GetClip("talk_to_idle") != (UnityEngine.Object) null))
      return;
    this.anim["talk_to_idle"].wrapMode = WrapMode.Default;
  }

  public void ToTalkMode()
  {
    this.SafeCrossFade("idle_to_talk");
    this.SafeCrossFadeQueued("talk");
  }

  public void ToIdleMode()
  {
    this.SafeCrossFade("talk_to_idle");
    this.SafeCrossFadeQueued("idle");
  }

  protected void SafeCrossFade(string animName)
  {
    if (!((UnityEngine.Object) this.anim.GetClip(animName) != (UnityEngine.Object) null))
      return;
    this.anim.CrossFade(animName);
  }

  protected void SafeCrossFadeQueued(string animName)
  {
    if (!((UnityEngine.Object) this.anim.GetClip(animName) != (UnityEngine.Object) null))
      return;
    this.anim.CrossFadeQueued(animName);
  }
}
