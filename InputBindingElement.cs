﻿// Decompiled with JetBrains decompiler
// Type: InputBindingElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class InputBindingElement : MonoBehaviour
{
  private IInputBinding inputBinding;
  private Action controlAction;
  public InputBinder InputBinder;
  private InputDevice inputDevice;
  [SerializeField]
  private LabelWidget actionLabel;
  [SerializeField]
  private ButtonWidget bindingButton;
  [SerializeField]
  private InputBindingXboxIconArea xboxIconArea;

  public void Start()
  {
  }

  public void InjectData(Action action, InputDevice device)
  {
    this.inputDevice = device;
    this.controlAction = action;
    this.inputBinding = InputBindingFactory.Unmapped(this.controlAction, this.inputDevice, (byte) 0);
    this.UpdateUiElements();
  }

  public void UpdateUiElements()
  {
    if ((Object) this.actionLabel != (Object) null)
      this.actionLabel.Label.text = Tools.GetLocalized(this.controlAction);
    if ((Object) this.bindingButton != (Object) null)
    {
      this.bindingButton.TextLabel.text = string.Empty;
      ((BoxCollider) this.bindingButton.GetComponent<Collider>()).center += new Vector3(0.0f, 0.0f, -5f);
    }
    this.actionLabel.name = "label_" + (object) this.controlAction;
    if (JoystickSetup.ShowXbox360Buttons && this.inputDevice == InputDevice.Joystick)
    {
      this.bindingButton.TextLabel.enabled = false;
      this.xboxIconArea.gameObject.SetActive(true);
      this.xboxIconArea.SetInputBinding(this.inputBinding);
    }
    else
    {
      this.xboxIconArea.gameObject.SetActive(false);
      this.bindingButton.TextLabel.enabled = true;
      this.bindingButton.TextLabel.text = this.inputBinding.BindingAlias;
    }
  }

  public void UpdateBindingButton(IEnumerable<IInputBinding> list)
  {
    this.inputBinding.Unmap();
    foreach (IInputBinding inputBinding in list)
    {
      if (inputBinding.Action == this.controlAction && inputBinding.Device == this.inputDevice)
      {
        this.inputBinding = inputBinding.Copy();
        break;
      }
    }
    this.bindingButton.handleClick = new AnonymousDelegate(this.ButtonFunction);
    this.UpdateUiElements();
  }

  private void ButtonFunction()
  {
    DialogBoxFactoryNgui.CreateInputBindingDialogBox(this.inputBinding);
  }
}
