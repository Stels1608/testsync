﻿// Decompiled with JetBrains decompiler
// Type: PrefabLightmapData
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class PrefabLightmapData : MonoBehaviour
{
  [SerializeField]
  private PrefabLightmapData.RendererInfo[] m_RendererInfo;
  [SerializeField]
  private Texture2D[] m_Lightmaps;

  private void Awake()
  {
    if (this.m_RendererInfo == null || this.m_RendererInfo.Length == 0)
      return;
    LightmapData[] lightmaps = LightmapSettings.lightmaps;
    LightmapData[] lightmapDataArray = new LightmapData[lightmaps.Length + this.m_Lightmaps.Length];
    lightmaps.CopyTo((Array) lightmapDataArray, 0);
    for (int index = 0; index < this.m_Lightmaps.Length; ++index)
    {
      lightmapDataArray[index + lightmaps.Length] = new LightmapData();
      lightmapDataArray[index + lightmaps.Length].lightmapFar = this.m_Lightmaps[index];
    }
    PrefabLightmapData.ApplyRendererInfo(this.m_RendererInfo, lightmaps.Length);
    LightmapSettings.lightmaps = lightmapDataArray;
  }

  private static void ApplyRendererInfo(PrefabLightmapData.RendererInfo[] infos, int lightmapOffsetIndex)
  {
    for (int index = 0; index < infos.Length; ++index)
    {
      PrefabLightmapData.RendererInfo rendererInfo = infos[index];
      rendererInfo.renderer.lightmapIndex = rendererInfo.lightmapIndex + lightmapOffsetIndex;
      rendererInfo.renderer.lightmapScaleOffset = rendererInfo.lightmapOffsetScale;
      rendererInfo.renderer.sharedMaterial = rendererInfo.renderer.sharedMaterial;
    }
  }

  [Serializable]
  private struct RendererInfo
  {
    public Renderer renderer;
    public int lightmapIndex;
    public Vector4 lightmapOffsetScale;

    public override string ToString()
    {
      return string.Format("Renderer: {0}, LightmapIndex: {1}, LightmapOffsetScale: {2}", !((UnityEngine.Object) this.renderer == (UnityEngine.Object) null) ? (object) this.renderer.name : (object) "NULL", (object) this.lightmapIndex, (object) this.lightmapOffsetScale);
    }
  }
}
