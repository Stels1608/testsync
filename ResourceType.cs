﻿// Decompiled with JetBrains decompiler
// Type: ResourceType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum ResourceType : uint
{
  CommAccess = 28157328,
  Plutonium = 63148366,
  Water = 130762195,
  FragmentedFTLCoordinates = 130797813,
  Token = 130920111,
  Uranium = 172582782,
  TechnicalAnalysisKit = 187088612,
  Titanium = 207047790,
  Tylium = 215278030,
  TuningKit = 254909109,
  Cubits = 264733124,
}
