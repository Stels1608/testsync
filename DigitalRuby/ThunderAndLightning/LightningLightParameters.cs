﻿// Decompiled with JetBrains decompiler
// Type: DigitalRuby.ThunderAndLightning.LightningLightParameters
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace DigitalRuby.ThunderAndLightning
{
  [Serializable]
  public class LightningLightParameters
  {
    [Tooltip("Color of the light")]
    public Color LightColor = Color.white;
    [Range(0.0f, 1f)]
    [Tooltip("What percent of segments should have a light? For performance you may want to keep this small.")]
    public float LightPercent = 1E-06f;
    [Tooltip("Light intensity")]
    [Range(0.0f, 8f)]
    public float LightIntensity = 0.5f;
    [Range(0.0f, 1f)]
    [Tooltip("Shadow strength, 0 means all light, 1 means all shadow")]
    public float ShadowStrength = 1f;
    [Range(0.0f, 2f)]
    [Tooltip("Shadow bias, 0 - 2")]
    public float ShadowBias = 0.05f;
    [Tooltip("Shadow normal bias, 0 - 3")]
    [Range(0.0f, 3f)]
    public float ShadowNormalBias = 0.4f;
    [Tooltip("Only light objects that match this layer mask")]
    public LayerMask CullingMask = (LayerMask) -1;
    [HideInInspector]
    [Tooltip("Light render mode - leave as auto unless you have special use cases")]
    public LightRenderMode RenderMode;
    [Tooltip("What percent of lights created should cast shadows?")]
    [Range(0.0f, 1f)]
    public float LightShadowPercent;
    [Range(0.0f, 8f)]
    [Tooltip("Bounce intensity")]
    public float BounceIntensity;
    [Tooltip("The range of each light created")]
    public float LightRange;

    public bool HasLight
    {
      get
      {
        if ((double) this.LightColor.a > 0.0 && (double) this.LightIntensity >= 0.00999999977648258 && (double) this.LightPercent >= 1.0000000116861E-07)
          return (double) this.LightRange > 0.00999999977648258;
        return false;
      }
    }
  }
}
