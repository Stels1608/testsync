﻿// Decompiled with JetBrains decompiler
// Type: DigitalRuby.ThunderAndLightning.LightningBoltScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace DigitalRuby.ThunderAndLightning
{
  public class LightningBoltScript : MonoBehaviour
  {
    [Tooltip("True if you are using world space coordinates for the lightning bolt, false if you are using coordinates relative to the parent game object.")]
    public bool UseWorldSpace = true;
    [Tooltip("Determines how the lightning is rendererd - this needs to be setup before the script starts.")]
    public LightningBoltRenderMode RenderMode = LightningBoltRenderMode.MeshRendererLineGlow;
    [Tooltip("Tint color for the lightning")]
    public Color LightningTintColor = Color.white;
    [Tooltip("Tint color for the lightning glow")]
    public Color GlowTintColor = new Color(0.1f, 0.2f, 1f, 1f);
    [Tooltip("Global turbulence velocity for this script")]
    public Vector3 TurbulenceVelocity = Vector3.zero;
    [Tooltip("The render queue for the lightning. -1 for default.")]
    public int RenderQueue = -1;
    private LightningBoltRenderMode lastRenderMode = (LightningBoltRenderMode) 2147483647;
    private readonly List<LightningBolt> bolts = new List<LightningBolt>();
    private readonly LightningBoltParameters[] oneParameterArray = new LightningBoltParameters[1];
    private const LightningBoltRenderMode defaultRenderMode = LightningBoltRenderMode.MeshRendererLineGlow;
    private LightningBoltMeshRenderer lightningBoltRenderer;
    [Tooltip("The camera the lightning should be shown in. Defaults to the current camera, or the main camera if current camera is null. If you are using a different camera, you may want to put the lightning in it's own layer and cull that layer out of any other cameras.")]
    public Camera Camera;
    [Tooltip("Lightning quality setting. This allows setting limits on generations, lights and shadow casting lights based on the global quality setting.")]
    public LightningBoltQualitySetting QualitySetting;
    [Tooltip("Lightning material for mesh renderer")]
    public Material LightningMaterialMesh;
    [Tooltip("Lightning material for mesh renderer, without glow")]
    public Material LightningMaterialMeshNoGlow;
    [Tooltip("The texture to use for the lightning bolts, or null for the material default texture.")]
    public Texture2D LightningTexture;
    [Tooltip("Particle system to play at the point of emission (start). 'Emission rate' particles will be emitted all at once.")]
    public ParticleSystem LightningOriginParticleSystem;
    [Tooltip("Particle system to play at the point of impact (end). 'Emission rate' particles will be emitted all at once.")]
    public ParticleSystem LightningDestinationParticleSystem;
    [Tooltip("Jitter multiplier to randomize lightning size. Jitter depends on trunk width and will make the lightning move rapidly and jaggedly, giving a more lively and sometimes cartoony feel. Jitter may be shared with other bolts depending on materials. If you need different jitters for the same material, create a second script object.")]
    public float JitterMultiplier;
    [Tooltip("Built in turbulance based on the direction of each segment. Small values usually work better, like 0.2.")]
    public float Turbulence;
    private Material lightningMaterialMeshInternal;
    private Material lightningMaterialMeshNoGlowInternal;
    private Texture2D lastLightningTexture;

    private void UpdateMaterialsForLastTexture()
    {
      if (!Application.isPlaying)
        return;
      this.lightningMaterialMeshInternal = new Material(this.LightningMaterialMesh);
      this.lightningMaterialMeshNoGlowInternal = new Material(this.LightningMaterialMeshNoGlow);
      if ((Object) this.LightningTexture != (Object) null)
      {
        this.lightningMaterialMeshInternal.SetTexture("_MainTex", (Texture) this.LightningTexture);
        this.lightningMaterialMeshNoGlowInternal.SetTexture("_MainTex", (Texture) this.LightningTexture);
      }
      this.lastRenderMode = (LightningBoltRenderMode) 2147483647;
      this.CreateRenderer();
    }

    private void CreateRenderer()
    {
      if (this.RenderMode == this.lastRenderMode)
        return;
      this.lastRenderMode = this.RenderMode;
      this.lightningBoltRenderer = new LightningBoltMeshRenderer();
      this.lightningBoltRenderer.MaterialNoGlow = this.lightningMaterialMeshNoGlowInternal;
      if (this.RenderMode == LightningBoltRenderMode.MeshRendererSquareBillboardGlow)
        this.lightningMaterialMeshInternal.DisableKeyword("USE_LINE_GLOW");
      else
        this.lightningMaterialMeshInternal.EnableKeyword("USE_LINE_GLOW");
      this.lightningBoltRenderer.Script = this;
      this.lightningBoltRenderer.Material = this.lightningMaterialMeshInternal;
    }

    private void UpdateTexture()
    {
      if ((Object) this.LightningTexture != (Object) null && (Object) this.LightningTexture != (Object) this.lastLightningTexture)
      {
        this.lastLightningTexture = this.LightningTexture;
        this.UpdateMaterialsForLastTexture();
      }
      else
        this.CreateRenderer();
    }

    private void UpdateShaderParameters()
    {
      this.lightningMaterialMeshInternal.SetColor("_TintColor", this.LightningTintColor);
      this.lightningMaterialMeshInternal.SetColor("_GlowTintColor", this.GlowTintColor);
      this.lightningMaterialMeshInternal.SetFloat("_JitterMultiplier", this.JitterMultiplier);
      this.lightningMaterialMeshInternal.SetFloat("_Turbulence", this.Turbulence);
      this.lightningMaterialMeshInternal.SetVector("_TurbulenceVelocity", (Vector4) this.TurbulenceVelocity);
      this.lightningMaterialMeshInternal.renderQueue = this.RenderQueue;
      this.lightningMaterialMeshNoGlowInternal.SetColor("_TintColor", this.LightningTintColor);
      this.lightningMaterialMeshNoGlowInternal.SetFloat("_JitterMultiplier", this.JitterMultiplier);
      this.lightningMaterialMeshNoGlowInternal.SetFloat("_Turbulence", this.Turbulence);
      this.lightningMaterialMeshNoGlowInternal.SetVector("_TurbulenceVelocity", (Vector4) this.TurbulenceVelocity);
      this.lightningMaterialMeshNoGlowInternal.renderQueue = this.RenderQueue;
    }

    private void OnDestroy()
    {
      using (List<LightningBolt>.Enumerator enumerator = this.bolts.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Cleanup();
      }
    }

    protected virtual void Start()
    {
      if ((Object) this.LightningMaterialMesh == (Object) null || (Object) this.LightningMaterialMeshNoGlow == (Object) null)
        Debug.LogError((object) "Must assign all lightning materials");
      this.UpdateMaterialsForLastTexture();
      if (!((Object) this.Camera == (Object) null))
        return;
      this.Camera = Camera.current;
      if (!((Object) this.Camera == (Object) null))
        return;
      this.Camera = Camera.main;
    }

    protected virtual void Update()
    {
      if (this.bolts.Count == 0)
        return;
      this.UpdateShaderParameters();
      for (int index = this.bolts.Count - 1; index >= 0; --index)
      {
        if (!this.bolts[index].Update())
          this.bolts.RemoveAt(index);
      }
    }

    public virtual void CreateLightningBolt(LightningBoltParameters p)
    {
      if (p == null)
        return;
      this.UpdateTexture();
      this.oneParameterArray[0] = p;
      LightningBolt lightningBolt = LightningBolt.GetOrCreateLightningBolt();
      lightningBolt.Initialize(this.Camera, this.UseWorldSpace, this.QualitySetting, this.lightningBoltRenderer, this.gameObject, this, this.LightningOriginParticleSystem, this.LightningDestinationParticleSystem, (ICollection<LightningBoltParameters>) this.oneParameterArray);
      this.bolts.Add(lightningBolt);
    }

    public void CreateLightningBolts(ICollection<LightningBoltParameters> parameters)
    {
      if (parameters == null)
        return;
      this.UpdateTexture();
      LightningBolt lightningBolt = LightningBolt.GetOrCreateLightningBolt();
      lightningBolt.Initialize(this.Camera, this.UseWorldSpace, this.QualitySetting, this.lightningBoltRenderer, this.gameObject, this, this.LightningOriginParticleSystem, this.LightningDestinationParticleSystem, parameters);
      this.bolts.Add(lightningBolt);
    }
  }
}
