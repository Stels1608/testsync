﻿// Decompiled with JetBrains decompiler
// Type: DigitalRuby.ThunderAndLightning.LightningBoltShapeSphereScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace DigitalRuby.ThunderAndLightning
{
  public class LightningBoltShapeSphereScript : LightningBoltPrefabScriptBase
  {
    [Tooltip("Radius inside the sphere where lightning can emit from")]
    public float InnerRadius = 0.1f;
    [Tooltip("Radius of the sphere")]
    public float Radius = 4f;

    public override void CreateLightningBolt(LightningBoltParameters parameters)
    {
      Vector3 vector3_1 = Random.insideUnitSphere * this.InnerRadius;
      Vector3 vector3_2 = Random.onUnitSphere * this.Radius;
      parameters.Start = vector3_1;
      parameters.End = vector3_2;
      base.CreateLightningBolt(parameters);
    }
  }
}
