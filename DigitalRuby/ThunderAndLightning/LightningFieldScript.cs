﻿// Decompiled with JetBrains decompiler
// Type: DigitalRuby.ThunderAndLightning.LightningFieldScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace DigitalRuby.ThunderAndLightning
{
  public class LightningFieldScript : LightningBoltPrefabScriptBase
  {
    [Tooltip("The bounds to put the field in.")]
    public Bounds FieldBounds;
    [Tooltip("Optional light for the lightning field to emit")]
    public Light Light;

    private Vector3 RandomPointInBounds()
    {
      return new Vector3(Random.Range(this.FieldBounds.min.x, this.FieldBounds.max.x), Random.Range(this.FieldBounds.min.y, this.FieldBounds.max.y), Random.Range(this.FieldBounds.min.z, this.FieldBounds.max.z));
    }

    protected override void Start()
    {
      base.Start();
      if (!((Object) this.Light != (Object) null))
        return;
      this.Light.enabled = false;
    }

    protected override void Update()
    {
      base.Update();
      if (!((Object) this.Light != (Object) null))
        return;
      this.Light.transform.position = this.FieldBounds.center;
      this.Light.intensity = Random.Range(2.8f, 3.2f);
    }

    public override void CreateLightningBolt(LightningBoltParameters parameters)
    {
      parameters.Start = this.RandomPointInBounds();
      parameters.End = this.RandomPointInBounds();
      if ((Object) this.Light != (Object) null)
        this.Light.enabled = true;
      base.CreateLightningBolt(parameters);
    }
  }
}
