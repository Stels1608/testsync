﻿// Decompiled with JetBrains decompiler
// Type: DigitalRuby.ThunderAndLightning.LightningBoltPrefabScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace DigitalRuby.ThunderAndLightning
{
  public class LightningBoltPrefabScript : LightningBoltPrefabScriptBase
  {
    [Tooltip("The source game object")]
    public GameObject Source;
    [Tooltip("The destination game object")]
    public GameObject Destination;

    public override void CreateLightningBolt(LightningBoltParameters parameters)
    {
      if ((Object) this.Source == (Object) null || (Object) this.Destination == (Object) null)
        return;
      parameters.Start = this.Source.transform.position;
      parameters.End = this.Destination.transform.position;
      base.CreateLightningBolt(parameters);
    }
  }
}
