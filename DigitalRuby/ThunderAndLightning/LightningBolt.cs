﻿// Decompiled with JetBrains decompiler
// Type: DigitalRuby.ThunderAndLightning.LightningBolt
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace DigitalRuby.ThunderAndLightning
{
  public class LightningBolt
  {
    public static int GenerationWhereForksStopSubtractor = 5;
    public static int MaximumLightCount = 128;
    public static int MaximumLightsPerBatch = 8;
    public static readonly Dictionary<int, LightningQualityMaximum> QualityMaximums = new Dictionary<int, LightningQualityMaximum>();
    private static readonly List<LightningBoltSegmentGroup> groupCache = new List<LightningBoltSegmentGroup>();
    private static readonly List<Light> lightCache = new List<Light>();
    private static readonly List<LightningBolt> lightningBoltCache = new List<LightningBolt>();
    private readonly List<LightningBoltSegmentGroup> segmentGroups = new List<LightningBoltSegmentGroup>();
    private readonly List<LightningBoltSegmentGroup> segmentGroupsWithLight = new List<LightningBoltSegmentGroup>();
    private float elapsedTime;
    private float lifeTime;
    private int generationWhereForksStop;
    private LightningBoltMeshRenderer lightningBoltRenderer;
    private LightningBoltScript script;
    private bool hasLight;
    private static int lightCount;

    public GameObject Parent { get; private set; }

    public float MinimumDelay { get; private set; }

    public bool HasGlow { get; private set; }

    public bool IsActive
    {
      get
      {
        return (double) this.elapsedTime < (double) this.lifeTime;
      }
    }

    public Camera Camera { get; private set; }

    public bool UseWorldSpace { get; set; }

    static LightningBolt()
    {
      string[] names = QualitySettings.names;
      for (int index = 0; index < names.Length; ++index)
      {
        switch (index)
        {
          case 0:
            LightningBolt.QualityMaximums[index] = new LightningQualityMaximum()
            {
              MaximumGenerations = 3,
              MaximumLightPercent = 0.0f,
              MaximumShadowPercent = 0.0f
            };
            break;
          case 1:
            LightningBolt.QualityMaximums[index] = new LightningQualityMaximum()
            {
              MaximumGenerations = 4,
              MaximumLightPercent = 0.0f,
              MaximumShadowPercent = 0.0f
            };
            break;
          case 2:
            LightningBolt.QualityMaximums[index] = new LightningQualityMaximum()
            {
              MaximumGenerations = 5,
              MaximumLightPercent = 0.1f,
              MaximumShadowPercent = 0.0f
            };
            break;
          case 3:
            LightningBolt.QualityMaximums[index] = new LightningQualityMaximum()
            {
              MaximumGenerations = 5,
              MaximumLightPercent = 0.1f,
              MaximumShadowPercent = 0.0f
            };
            break;
          case 4:
            LightningBolt.QualityMaximums[index] = new LightningQualityMaximum()
            {
              MaximumGenerations = 6,
              MaximumLightPercent = 0.05f,
              MaximumShadowPercent = 0.1f
            };
            break;
          case 5:
            LightningBolt.QualityMaximums[index] = new LightningQualityMaximum()
            {
              MaximumGenerations = 7,
              MaximumLightPercent = 0.025f,
              MaximumShadowPercent = 0.05f
            };
            break;
          default:
            LightningBolt.QualityMaximums[index] = new LightningQualityMaximum()
            {
              MaximumGenerations = 8,
              MaximumLightPercent = 0.025f,
              MaximumShadowPercent = 0.05f
            };
            break;
        }
      }
    }

    public static LightningBolt GetOrCreateLightningBolt()
    {
      if (LightningBolt.lightningBoltCache.Count == 0)
        return new LightningBolt();
      LightningBolt lightningBolt = LightningBolt.lightningBoltCache[LightningBolt.lightningBoltCache.Count - 1];
      LightningBolt.lightningBoltCache.RemoveAt(LightningBolt.lightningBoltCache.Count - 1);
      return lightningBolt;
    }

    public void Initialize(Camera camera, bool useWorldSpace, LightningBoltQualitySetting quality, LightningBoltMeshRenderer lightningBoltRenderer, GameObject parent, LightningBoltScript script, ParticleSystem originParticleSystem, ParticleSystem destParticleSystem, ICollection<LightningBoltParameters> parameters)
    {
      if (parameters == null || lightningBoltRenderer == null || (parameters.Count == 0 || (UnityEngine.Object) script == (UnityEngine.Object) null))
        return;
      this.UseWorldSpace = useWorldSpace;
      this.lightningBoltRenderer = lightningBoltRenderer;
      this.Parent = parent;
      this.script = script;
      this.CheckForGlow((IEnumerable<LightningBoltParameters>) parameters);
      lightningBoltRenderer.Begin(this);
      this.MinimumDelay = float.MaxValue;
      int maxLights = LightningBolt.MaximumLightsPerBatch / parameters.Count;
      foreach (LightningBoltParameters parameter in (IEnumerable<LightningBoltParameters>) parameters)
        this.ProcessParameters(parameter, quality, originParticleSystem, destParticleSystem, maxLights);
      lightningBoltRenderer.End(this);
    }

    public bool Update()
    {
      this.elapsedTime += Time.deltaTime;
      if ((double) this.elapsedTime > (double) this.lifeTime)
      {
        this.Cleanup();
        return false;
      }
      if (this.hasLight)
        this.UpdateLightsForGroups();
      return true;
    }

    public void Cleanup()
    {
      using (List<LightningBoltSegmentGroup>.Enumerator enumerator1 = this.segmentGroups.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          LightningBoltSegmentGroup current1 = enumerator1.Current;
          using (List<Light>.Enumerator enumerator2 = current1.Lights.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              Light current2 = enumerator2.Current;
              LightningBolt.lightCache.Add(current2);
              current2.gameObject.SetActive(false);
              --LightningBolt.lightCount;
            }
          }
          current1.LightParameters = (LightningLightParameters) null;
          current1.Segments.Clear();
          current1.Lights.Clear();
          current1.StartIndex = 0;
          LightningBolt.groupCache.Add(current1);
        }
      }
      this.segmentGroups.Clear();
      this.segmentGroupsWithLight.Clear();
      if (this.lightningBoltRenderer != null)
      {
        this.lightningBoltRenderer.Cleanup(this);
        this.lightningBoltRenderer = (LightningBoltMeshRenderer) null;
      }
      this.hasLight = false;
      this.elapsedTime = 0.0f;
      this.lifeTime = 0.0f;
      LightningBolt.lightningBoltCache.Add(this);
    }

    private void ProcessParameters(LightningBoltParameters p, LightningBoltQualitySetting quality, ParticleSystem sourceParticleSystem, ParticleSystem destinationParticleSystem, int maxLights)
    {
      this.MinimumDelay = Mathf.Min(p.Delay, this.MinimumDelay);
      this.generationWhereForksStop = p.Generations - LightningBolt.GenerationWhereForksStopSubtractor;
      p.GlowIntensity = Mathf.Clamp(p.GlowIntensity, 0.0f, 1f);
      p.Random = p.Random ?? new System.Random(Environment.TickCount);
      p.GrowthMultiplier = Mathf.Clamp(p.GrowthMultiplier, 0.0f, 0.999f);
      this.lifeTime = Mathf.Max(p.LifeTime + p.Delay, this.lifeTime);
      LightningLightParameters lp = p.LightParameters;
      if (lp != null)
      {
        if (this.hasLight |= lp.HasLight)
        {
          lp.LightPercent = Mathf.Clamp(lp.LightPercent, 1E-07f, 1f);
          lp.LightShadowPercent = Mathf.Clamp(lp.LightShadowPercent, 0.0f, 1f);
        }
        else
          lp = (LightningLightParameters) null;
      }
      if (p.Generations < 1)
      {
        p.TrunkWidth = 0.0f;
        p.Generations = 1;
      }
      else if (p.Generations > 8)
        p.Generations = 8;
      int forkedness = (int) ((double) p.Forkedness * (double) p.Generations);
      int count = this.segmentGroups.Count;
      int num;
      if (quality == LightningBoltQualitySetting.UseScript)
      {
        num = p.Generations;
      }
      else
      {
        int qualityLevel = QualitySettings.GetQualityLevel();
        LightningQualityMaximum lightningQualityMaximum;
        if (LightningBolt.QualityMaximums.TryGetValue(qualityLevel, out lightningQualityMaximum))
        {
          num = Mathf.Min(lightningQualityMaximum.MaximumGenerations, p.Generations);
        }
        else
        {
          num = p.Generations;
          UnityEngine.Debug.LogError((object) ("Unable to read lightning quality settings from level " + qualityLevel.ToString()));
        }
      }
      this.GenerateLightningBolt(p.Start, p.End, num, num, 0.0f, 0.0f, forkedness, p);
      this.RenderLightningBolt(quality, num, p.Start, p.End, count, sourceParticleSystem, destinationParticleSystem, p, lp, maxLights);
    }

    private void RenderLightningBolt(LightningBoltQualitySetting quality, int generations, Vector3 start, Vector3 end, int groupIndex, ParticleSystem originParticleSystem, ParticleSystem destParticleSystem, LightningBoltParameters parameters, LightningLightParameters lp, int maxLights)
    {
      if (this.segmentGroups.Count == 0 || groupIndex >= this.segmentGroups.Count)
        return;
      float num1 = parameters.LifeTime / (float) this.segmentGroups.Count;
      float num2 = num1 * 0.9f;
      float num3 = num1 * 1.1f - num2;
      parameters.FadePercent = Mathf.Clamp(parameters.FadePercent, 0.0f, 0.5f);
      if ((UnityEngine.Object) originParticleSystem != (UnityEngine.Object) null)
        this.script.StartCoroutine(this.GenerateParticle(originParticleSystem, start, parameters.Delay));
      if ((UnityEngine.Object) destParticleSystem != (UnityEngine.Object) null)
        this.script.StartCoroutine(this.GenerateParticle(destParticleSystem, end, parameters.Delay * 1.1f));
      if (this.HasGlow)
      {
        this.lightningBoltRenderer.GlowIntensityMultiplier = parameters.GlowIntensity;
        this.lightningBoltRenderer.GlowWidthMultiplier = parameters.GlowWidthMultiplier;
      }
      float num4 = 0.0f;
      for (int index = groupIndex; index < this.segmentGroups.Count; ++index)
      {
        LightningBoltSegmentGroup group = this.segmentGroups[index];
        group.Delay = num4 + parameters.Delay;
        group.LifeTime = parameters.LifeTime - num4;
        group.PeakStart = group.LifeTime * parameters.FadePercent;
        group.PeakEnd = group.LifeTime - group.PeakStart;
        group.LightParameters = lp;
        this.lightningBoltRenderer.AddGroup(this, group, parameters.GrowthMultiplier);
        num4 += (float) parameters.Random.NextDouble() * num2 + num3;
        if (lp != null && group.Generation == generations)
          this.CreateLightsForGroup(group, lp, quality, maxLights, groupIndex);
      }
    }

    private void CreateLightsForGroup(LightningBoltSegmentGroup group, LightningLightParameters lp, LightningBoltQualitySetting quality, int maxLights, int groupIndex)
    {
      if (LightningBolt.lightCount == LightningBolt.MaximumLightCount || maxLights <= 0)
        return;
      this.segmentGroupsWithLight.Add(group);
      int segmentCount = group.SegmentCount;
      float num1;
      float num2;
      if (quality == LightningBoltQualitySetting.LimitToQualitySetting)
      {
        int qualityLevel = QualitySettings.GetQualityLevel();
        LightningQualityMaximum lightningQualityMaximum;
        if (LightningBolt.QualityMaximums.TryGetValue(qualityLevel, out lightningQualityMaximum))
        {
          num1 = Mathf.Min(lp.LightPercent, lightningQualityMaximum.MaximumLightPercent);
          num2 = Mathf.Min(lp.LightShadowPercent, lightningQualityMaximum.MaximumShadowPercent);
        }
        else
        {
          UnityEngine.Debug.LogError((object) ("Unable to read lightning quality for level " + qualityLevel.ToString()));
          num1 = lp.LightPercent;
          num2 = lp.LightShadowPercent;
        }
      }
      else
      {
        num1 = lp.LightPercent;
        num2 = lp.LightShadowPercent;
      }
      maxLights = Mathf.Max(1, Mathf.Min(maxLights, (int) ((double) segmentCount * (double) num1)));
      int nthLight = Mathf.Max(1, segmentCount / maxLights);
      int nthShadows = maxLights - (int) ((double) maxLights * (double) num2);
      int nthShadowCounter = nthShadows;
      int segmentIndex = group.StartIndex + (int) ((double) nthLight * 0.5);
      while (segmentIndex < group.Segments.Count && !this.AddLightToGroup(group, lp, segmentIndex, nthLight, nthShadows, ref maxLights, ref nthShadowCounter))
        segmentIndex += nthLight;
    }

    private bool AddLightToGroup(LightningBoltSegmentGroup group, LightningLightParameters lp, int segmentIndex, int nthLight, int nthShadows, ref int maxLights, ref int nthShadowCounter)
    {
      Light light = this.CreateLight(group, lp);
      light.gameObject.transform.position = (group.Segments[segmentIndex].Start + group.Segments[segmentIndex].End) * 0.5f;
      if ((double) lp.LightShadowPercent == 0.0 || (nthShadowCounter = nthShadowCounter + 1) < nthShadows)
      {
        light.shadows = LightShadows.None;
      }
      else
      {
        light.shadows = LightShadows.Soft;
        nthShadowCounter = 0;
      }
      if (++LightningBolt.lightCount != LightningBolt.MaximumLightCount)
        return (maxLights = maxLights - 1) == 0;
      return true;
    }

    private Light CreateLight(LightningBoltSegmentGroup group, LightningLightParameters lp)
    {
      Light light;
      while (LightningBolt.lightCache.Count != 0)
      {
        light = LightningBolt.lightCache[LightningBolt.lightCache.Count - 1];
        LightningBolt.lightCache.RemoveAt(LightningBolt.lightCache.Count - 1);
        if (!((UnityEngine.Object) light == (UnityEngine.Object) null))
          goto label_3;
      }
      GameObject gameObject = new GameObject();
      gameObject.hideFlags = HideFlags.HideAndDontSave;
      gameObject.name = "LightningBoltLight";
      light = gameObject.AddComponent<Light>();
      light.type = LightType.Point;
label_3:
      light.color = lp.LightColor;
      light.renderMode = lp.RenderMode;
      light.range = lp.LightRange;
      light.bounceIntensity = lp.BounceIntensity;
      light.shadowStrength = lp.ShadowStrength;
      light.shadowBias = lp.ShadowBias;
      light.shadowNormalBias = lp.ShadowNormalBias;
      light.intensity = 0.0f;
      light.gameObject.transform.parent = this.Parent.transform;
      light.gameObject.SetActive(true);
      group.Lights.Add(light);
      return light;
    }

    private void UpdateLightsForGroups()
    {
      using (List<LightningBoltSegmentGroup>.Enumerator enumerator1 = this.segmentGroupsWithLight.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          LightningBoltSegmentGroup current = enumerator1.Current;
          if ((double) this.elapsedTime >= (double) current.Delay)
          {
            float num = this.elapsedTime - current.Delay;
            if ((double) num >= (double) current.PeakStart)
            {
              if ((double) num <= (double) current.PeakEnd)
              {
                using (List<Light>.Enumerator enumerator2 = current.Lights.GetEnumerator())
                {
                  while (enumerator2.MoveNext())
                    enumerator2.Current.intensity = current.LightParameters.LightIntensity;
                }
              }
              else
              {
                float t = (float) (((double) num - (double) current.PeakEnd) / ((double) current.LifeTime - (double) current.PeakEnd));
                using (List<Light>.Enumerator enumerator2 = current.Lights.GetEnumerator())
                {
                  while (enumerator2.MoveNext())
                    enumerator2.Current.intensity = Mathf.Lerp(current.LightParameters.LightIntensity, 0.0f, t);
                }
              }
            }
            else
            {
              float t = num / current.PeakStart;
              using (List<Light>.Enumerator enumerator2 = current.Lights.GetEnumerator())
              {
                while (enumerator2.MoveNext())
                  enumerator2.Current.intensity = Mathf.Lerp(0.0f, current.LightParameters.LightIntensity, t);
              }
            }
          }
        }
      }
    }

    private LightningBoltSegmentGroup CreateGroup()
    {
      LightningBoltSegmentGroup boltSegmentGroup;
      if (LightningBolt.groupCache.Count == 0)
      {
        boltSegmentGroup = new LightningBoltSegmentGroup();
      }
      else
      {
        int index = LightningBolt.groupCache.Count - 1;
        boltSegmentGroup = LightningBolt.groupCache[index];
        LightningBolt.groupCache.RemoveAt(index);
      }
      this.segmentGroups.Add(boltSegmentGroup);
      return boltSegmentGroup;
    }

    [DebuggerHidden]
    private IEnumerator GenerateParticle(ParticleSystem p, Vector3 pos, float delay)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new LightningBolt.\u003CGenerateParticle\u003Ec__Iterator5() { delay = delay, pos = pos, p = p, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003Epos = pos, \u003C\u0024\u003Ep = p, \u003C\u003Ef__this = this };
    }

    private void GenerateLightningBolt(Vector3 start, Vector3 end, int generation, int totalGenerations, float offsetAmount, float lineWidth, int forkedness, LightningBoltParameters parameters)
    {
      if (generation < 1)
        return;
      LightningBoltSegmentGroup group = this.CreateGroup();
      group.EndWidthMultiplier = parameters.EndWidthMultiplier;
      group.Segments.Add(new LightningBoltSegment()
      {
        Start = start,
        End = end
      });
      float num1 = (float) generation / (float) totalGenerations;
      float num2 = num1 * num1;
      if ((double) offsetAmount <= 0.0)
        offsetAmount = (end - start).magnitude * parameters.ChaosFactor;
      group.LineWidth = (double) lineWidth > 0.0 ? lineWidth * num2 : parameters.TrunkWidth;
      group.LineWidth *= num2;
      group.Generation = generation;
      while (generation-- > 0)
      {
        int num3 = group.StartIndex;
        group.StartIndex = group.Segments.Count;
        for (int index = num3; index < group.StartIndex; ++index)
        {
          start = group.Segments[index].Start;
          end = group.Segments[index].End;
          Vector3 vector3_1 = (start + end) * 0.5f;
          Vector3 result;
          this.RandomVector(ref start, ref end, offsetAmount, parameters.Random, out result);
          Vector3 start1 = vector3_1 + result;
          group.Segments.Add(new LightningBoltSegment()
          {
            Start = start,
            End = start1
          });
          group.Segments.Add(new LightningBoltSegment()
          {
            Start = start1,
            End = end
          });
          if (generation > this.generationWhereForksStop && generation >= totalGenerations - forkedness && parameters.Random.Next(0, generation) < forkedness)
          {
            float num4 = (float) (parameters.Random.NextDouble() * 0.200000002980232 + 0.600000023841858);
            Vector3 vector3_2 = (start1 - start) * num4;
            Vector3 end1 = start1 + vector3_2;
            this.GenerateLightningBolt(start1, end1, generation, totalGenerations, 0.0f, lineWidth, forkedness, parameters);
          }
        }
        offsetAmount *= 0.5f;
      }
    }

    private void RandomVector(ref Vector3 start, ref Vector3 end, float offsetAmount, System.Random random, out Vector3 result)
    {
      Vector3 normalized1 = (end - start).normalized;
      if ((UnityEngine.Object) this.Camera == (UnityEngine.Object) null || !this.Camera.orthographic)
      {
        Vector3 normalized2 = Vector3.Cross(start, end).normalized;
        float num = ((float) random.NextDouble() + 0.1f) * offsetAmount;
        float angle = (float) random.NextDouble() * 360f;
        result = Quaternion.AngleAxis(angle, normalized1) * normalized2 * num;
      }
      else
      {
        Vector3 vector3 = new Vector3(-normalized1.y, normalized1.x, normalized1.z);
        float num = (float) (random.NextDouble() * (double) offsetAmount * 2.0) - offsetAmount;
        result = vector3 * num;
      }
    }

    private void CheckForGlow(IEnumerable<LightningBoltParameters> parameters)
    {
      foreach (LightningBoltParameters parameter in parameters)
      {
        this.HasGlow = (double) parameter.GlowIntensity > 9.99999974737875E-05 && (double) parameter.GlowWidthMultiplier >= 9.99999974737875E-05;
        if (this.HasGlow)
          break;
      }
    }
  }
}
