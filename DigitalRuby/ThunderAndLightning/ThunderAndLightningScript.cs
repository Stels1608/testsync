﻿// Decompiled with JetBrains decompiler
// Type: DigitalRuby.ThunderAndLightning.ThunderAndLightningScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace DigitalRuby.ThunderAndLightning
{
  public class ThunderAndLightningScript : MonoBehaviour
  {
    [Tooltip("Random duration that the scene will light up - intense lightning extends this a little.")]
    public Vector2 LightningShowTimeRange = new Vector2(0.2f, 0.5f);
    [Tooltip("Random interval between strikes.")]
    public Vector2 LightningIntervalTimeRange = new Vector2(10f, 25f);
    [Tooltip("Probability (0-1) of an intense lightning bolt that hits really close. Intense lightning has increased brightness and louder thunder compared to normal lightning, and the thunder sounds plays a lot sooner.")]
    [Range(0.0f, 1f)]
    public float LightningIntenseProbability = 0.2f;
    [Tooltip("Whether lightning strikes should always try to be in the camera view")]
    public bool LightningAlwaysVisible = true;
    [Range(0.0f, 1f)]
    [Tooltip("The chance lightning will simply be in the clouds with no visible bolt")]
    public float CloudLightningChance = 0.5f;
    [Tooltip("How much the lightning should glow, 0 for none 1 for full glow")]
    [Range(0.0f, 1f)]
    public float GlowIntensity = 0.1f;
    [Range(0.0f, 64f)]
    [Tooltip("How the glow width should be multiplied, 0 for none, 64 is max")]
    public float GlowWidthMultiplier = 4f;
    [Tooltip("How forked the lightning should be. 0 for none, 1 for LOTS of forks.")]
    [Range(0.0f, 1f)]
    public float Forkedness = 0.5f;
    [Tooltip("How chaotic is the lightning? Higher numbers make more chaotic lightning.")]
    [Range(0.0f, 1f)]
    public float ChaosFactor = 0.2f;
    [Range(4f, 8f)]
    [Tooltip("Number of generations. The higher the number, the more detailed the lightning is, but more expensive to create.")]
    public int Generations = 6;
    [Tooltip("Lightning bolt script - optional, leave null if you don't want lightning bolts")]
    public LightningBoltScript LightningBoltScript;
    [Tooltip("Camera where the lightning should be centered over. Defaults to main camera.")]
    public Camera Camera;
    [Tooltip("Sounds to play for normal thunder. One will be chosen at random for each lightning strike. Depending on intensity, some normal lightning may not play a thunder sound.")]
    public AudioClip[] ThunderSoundsNormal;
    [Tooltip("Sounds to play for intense thunder. One will be chosen at random for each lightning strike.")]
    public AudioClip[] ThunderSoundsIntense;
    [Tooltip("Whether to modify the skybox exposure when lightning is created")]
    public bool ModifySkyboxExposure;
    private float skyboxExposureOriginal;
    private float skyboxExposureStorm;
    private float nextLightningTime;
    private float lightningDuration;
    private bool lightningInProgress;
    private AudioSource audioSourceThunder;
    private ThunderAndLightningScript.LightningBoltHandler lightningBoltHandler;
    private Material skyboxMaterial;
    private AudioClip lastThunderSound;

    public float SkyboxExposureOriginal
    {
      get
      {
        return this.skyboxExposureOriginal;
      }
    }

    private void Start()
    {
      if ((UnityEngine.Object) this.Camera == (UnityEngine.Object) null)
        this.Camera = Camera.main;
      if ((double) this.Camera.farClipPlane < 10000.0)
        UnityEngine.Debug.LogWarning((object) "Far clip plane should be 10000+ for best lightning effects");
      if ((UnityEngine.Object) RenderSettings.skybox != (UnityEngine.Object) null)
      {
        Material material = new Material(RenderSettings.skybox);
        RenderSettings.skybox = material;
        this.skyboxMaterial = material;
      }
      this.skyboxExposureOriginal = this.skyboxExposureStorm = !((UnityEngine.Object) this.skyboxMaterial == (UnityEngine.Object) null) ? this.skyboxMaterial.GetFloat("_Exposure") : 1f;
      this.audioSourceThunder = this.gameObject.AddComponent<AudioSource>();
      this.lightningBoltHandler = new ThunderAndLightningScript.LightningBoltHandler(this);
    }

    private void Update()
    {
      if (this.lightningBoltHandler == null)
        return;
      this.lightningBoltHandler.Update();
    }

    public void CallNormalLightning()
    {
      this.StartCoroutine(this.lightningBoltHandler.ProcessLightning(false, true));
    }

    public void CallIntenseLightning()
    {
      this.StartCoroutine(this.lightningBoltHandler.ProcessLightning(true, true));
    }

    private class LightningBoltHandler
    {
      private ThunderAndLightningScript script;

      public LightningBoltHandler(ThunderAndLightningScript script)
      {
        this.script = script;
        this.CalculateNextLightningTime();
      }

      private void UpdateLighting()
      {
        if (this.script.lightningInProgress)
          return;
        if (this.script.ModifySkyboxExposure)
        {
          this.script.skyboxExposureStorm = 0.35f;
          if ((UnityEngine.Object) this.script.skyboxMaterial != (UnityEngine.Object) null)
            this.script.skyboxMaterial.SetFloat("_Exposure", this.script.skyboxExposureStorm);
        }
        this.CheckForLightning();
      }

      private void CalculateNextLightningTime()
      {
        this.script.nextLightningTime = Time.time + UnityEngine.Random.Range(this.script.LightningIntervalTimeRange.x, this.script.LightningIntervalTimeRange.y);
        this.script.lightningInProgress = false;
        if (!this.script.ModifySkyboxExposure)
          return;
        this.script.skyboxMaterial.SetFloat("_Exposure", this.script.skyboxExposureStorm);
      }

      [DebuggerHidden]
      private IEnumerator CalculateNextLightningLater(float duration)
      {
        // ISSUE: object of a compiler-generated type is created
        return (IEnumerator) new ThunderAndLightningScript.LightningBoltHandler.\u003CCalculateNextLightningLater\u003Ec__Iterator6() { duration = duration, \u003C\u0024\u003Eduration = duration, \u003C\u003Ef__this = this };
      }

      [DebuggerHidden]
      public IEnumerator ProcessLightning(bool intense, bool visible)
      {
        // ISSUE: object of a compiler-generated type is created
        return (IEnumerator) new ThunderAndLightningScript.LightningBoltHandler.\u003CProcessLightning\u003Ec__Iterator7() { intense = intense, visible = visible, \u003C\u0024\u003Eintense = intense, \u003C\u0024\u003Evisible = visible, \u003C\u003Ef__this = this };
      }

      private void Strike(bool intense, int generations, float duration, float intensity, float chaosFactor, float glowIntensity, float glowWidth, float forkedness, int count, Camera camera, Camera visibleInCamera)
      {
        if (count < 1)
          return;
        System.Random random = new System.Random();
        float min1 = !intense ? -5000f : -1000f;
        float max = !intense ? 5000f : 1000f;
        float min2 = !intense ? 2500f : 500f;
        float num1 = UnityEngine.Random.Range(0, 2) != 0 ? UnityEngine.Random.Range(min2, max) : UnityEngine.Random.Range(min1, -min2);
        float num2 = 620f;
        float num3 = UnityEngine.Random.Range(0, 2) != 0 ? UnityEngine.Random.Range(min2, max) : UnityEngine.Random.Range(min1, -min2);
        float num4 = 0.0f;
        Vector3 vector3_1 = this.script.Camera.transform.position;
        vector3_1.x += num1;
        vector3_1.y = num2;
        vector3_1.z += num3;
        if ((UnityEngine.Object) visibleInCamera != (UnityEngine.Object) null)
        {
          Quaternion rotation = visibleInCamera.transform.rotation;
          visibleInCamera.transform.rotation = Quaternion.Euler(0.0f, rotation.eulerAngles.y, 0.0f);
          float x = UnityEngine.Random.Range((float) visibleInCamera.pixelWidth * 0.1f, (float) visibleInCamera.pixelWidth * 0.9f);
          float z = UnityEngine.Random.Range(visibleInCamera.nearClipPlane + min2 + min2, max);
          vector3_1 = visibleInCamera.ScreenToWorldPoint(new Vector3(x, 0.0f, z));
          vector3_1.y = num2;
          visibleInCamera.transform.rotation = rotation;
        }
        while (count-- > 0)
        {
          Vector3 vector3_2 = vector3_1;
          float num5 = UnityEngine.Random.Range(-100f, 100f);
          float num6 = UnityEngine.Random.Range(0, 4) != 0 ? -1f : UnityEngine.Random.Range(-1f, 600f);
          num3 += UnityEngine.Random.Range(-100f, 100f);
          vector3_2.x += num5;
          vector3_2.y = num6;
          vector3_2.z += num3;
          vector3_2.x += min2 * camera.transform.forward.x;
          vector3_2.z += min2 * camera.transform.forward.z;
          while ((double) (vector3_1 - vector3_2).magnitude < 500.0)
          {
            vector3_2.x += min2 * camera.transform.forward.x;
            vector3_2.z += min2 * camera.transform.forward.z;
          }
          if ((UnityEngine.Object) this.script.LightningBoltScript != (UnityEngine.Object) null)
          {
            if ((double) UnityEngine.Random.value < (double) this.script.CloudLightningChance)
              generations = 0;
            this.script.LightningBoltScript.CreateLightningBolt(new LightningBoltParameters()
            {
              Start = vector3_1,
              End = vector3_2,
              Generations = generations,
              LifeTime = duration,
              Delay = num4,
              ChaosFactor = chaosFactor,
              TrunkWidth = 8f,
              EndWidthMultiplier = 0.25f,
              GlowIntensity = glowIntensity,
              GlowWidthMultiplier = glowWidth,
              Forkedness = forkedness,
              Random = random,
              LightParameters = new LightningLightParameters()
              {
                LightIntensity = intensity,
                LightRange = 5000f,
                LightShadowPercent = 1f
              }
            });
            num4 += duration / (float) count * UnityEngine.Random.Range(0.2f, 0.5f);
          }
        }
      }

      private void CheckForLightning()
      {
        if ((double) Time.time < (double) this.script.nextLightningTime)
          return;
        this.script.StartCoroutine(this.ProcessLightning((double) UnityEngine.Random.value < (double) this.script.LightningIntenseProbability, this.script.LightningAlwaysVisible));
      }

      public void Update()
      {
        this.UpdateLighting();
      }
    }
  }
}
