﻿// Decompiled with JetBrains decompiler
// Type: DigitalRuby.ThunderAndLightning.LightningBoltPrefabScriptBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace DigitalRuby.ThunderAndLightning
{
  public abstract class LightningBoltPrefabScriptBase : LightningBoltScript
  {
    private static readonly System.Random random = new System.Random();
    [SingleLineClamp("How long to wait before creating another round of lightning bolts in seconds", 0.001, 1.79769313486232E+308)]
    public RangeOfFloats IntervalRange = new RangeOfFloats() { Minimum = 0.05f, Maximum = 0.1f };
    [SingleLineClamp("How many lightning bolts to emit for each interval", 0.0, 100.0)]
    public RangeOfIntegers CountRange = new RangeOfIntegers() { Minimum = 1, Maximum = 1 };
    [SingleLineClamp("Delay in seconds (range) before each lightning bolt in count range is emitted", 0.0, 30.0)]
    public RangeOfFloats DelayRange = new RangeOfFloats();
    [SingleLineClamp("For each bolt emitted, how long should it stay in seconds", 0.01, 10.0)]
    public RangeOfFloats DurationRange = new RangeOfFloats() { Minimum = 0.06f, Maximum = 0.12f };
    [SingleLineClamp("The trunk width range in unity units (x = min, y = max)", 0.0001, 100.0)]
    public RangeOfFloats TrunkWidthRange = new RangeOfFloats() { Minimum = 0.1f, Maximum = 0.2f };
    [Tooltip("Generations (1 - 8, higher makes more detailed but more expensive lightning)")]
    [Range(1f, 8f)]
    public int Generations = 6;
    [Range(0.0f, 1f)]
    [Tooltip("The chaos factor determines how far the lightning can spread out, higher numbers spread out more. 0 - 1.")]
    public float ChaosFactor = 0.075f;
    [Range(0.0f, 1f)]
    [Tooltip("The intensity of the glow, 0 - 1")]
    public float GlowIntensity = 0.1f;
    [Range(0.0f, 64f)]
    [Tooltip("The width multiplier for the glow, 0 - 64")]
    public float GlowWidthMultiplier = 4f;
    [Tooltip("How forked should the lightning be? (0 - 1, 0 for none, 1 for lots of forks)")]
    [Range(0.0f, 1f)]
    public float Forkedness = 0.25f;
    [Tooltip("What percent of time the lightning should fade in and out. For example, 0.15 fades in 15% of the time and fades out 15% of the time, with full visibility 70% of the time.")]
    [Range(0.0f, 0.5f)]
    public float FadePercent = 0.15f;
    [Tooltip("How much smaller the lightning should get as it goes towards the end of the bolt. For example, 0.5 will make the end 50% the width of the start.")]
    [Range(0.0f, 10f)]
    public float EndWidthMultiplier = 0.5f;
    [Range(0.0f, 64f)]
    [Tooltip("Maximum number of lights that can be created per batch of lightning")]
    public int MaximumLightsPerBatch = 8;
    private readonly List<LightningBoltParameters> batchParameters = new List<LightningBoltParameters>();
    private const float duration = 0.3f;
    private const float overlapMultiplier = 0.35f;
    [Tooltip("How long (in seconds) this game object should live before destroying itself. Leave as 0 for infinite.")]
    [Range(0.0f, 1000f)]
    public float LifeTime;
    [Tooltip("0 - 1, how slowly the lightning should grow. 0 for instant, 1 for slow.")]
    [Range(0.0f, 1f)]
    public float GrowthMultiplier;
    [Tooltip("Light parameters")]
    public LightningLightParameters LightParameters;
    private float nextArc;
    private float lifeTimeRemaining;

    private void CreateInterval(float offset)
    {
      this.nextArc = offset + (float) (LightningBoltPrefabScriptBase.random.NextDouble() * ((double) this.IntervalRange.Maximum - (double) this.IntervalRange.Minimum)) + this.IntervalRange.Minimum;
    }

    private void CallLightning()
    {
      int num1 = LightningBoltPrefabScriptBase.random.Next(this.CountRange.Minimum, this.CountRange.Maximum + 1);
      while (num1-- > 0)
      {
        float num2 = (float) LightningBoltPrefabScriptBase.random.NextDouble() * (this.DurationRange.Maximum - this.DurationRange.Minimum) + this.DurationRange.Maximum;
        float num3 = (float) LightningBoltPrefabScriptBase.random.NextDouble() * (this.TrunkWidthRange.Maximum - this.TrunkWidthRange.Minimum) + this.TrunkWidthRange.Maximum;
        this.CreateLightningBolt(new LightningBoltParameters()
        {
          Generations = this.Generations,
          LifeTime = num2,
          ChaosFactor = this.ChaosFactor,
          TrunkWidth = num3,
          GlowIntensity = this.GlowIntensity,
          GlowWidthMultiplier = this.GlowWidthMultiplier,
          Forkedness = this.Forkedness,
          FadePercent = this.FadePercent,
          GrowthMultiplier = this.GrowthMultiplier,
          EndWidthMultiplier = this.EndWidthMultiplier,
          Random = LightningBoltPrefabScriptBase.random,
          Delay = UnityEngine.Random.Range(this.DelayRange.Minimum, this.DelayRange.Maximum),
          LightParameters = this.LightParameters
        });
      }
      int num4 = LightningBolt.MaximumLightsPerBatch;
      LightningBolt.MaximumLightsPerBatch = this.MaximumLightsPerBatch;
      this.CreateLightningBolts((ICollection<LightningBoltParameters>) this.batchParameters);
      LightningBolt.MaximumLightsPerBatch = num4;
      this.batchParameters.Clear();
    }

    protected override void Start()
    {
      base.Start();
      this.CreateInterval(0.0f);
      this.lifeTimeRemaining = (double) this.LifeTime > 0.0 ? this.LifeTime : float.MaxValue;
    }

    protected override void Update()
    {
      base.Update();
      if ((double) (this.lifeTimeRemaining -= Time.deltaTime) < 0.0)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
      }
      else
      {
        if ((double) (this.nextArc -= Time.deltaTime) >= 0.0)
          return;
        this.CreateInterval(this.nextArc);
        this.CallLightning();
      }
    }

    public override void CreateLightningBolt(LightningBoltParameters p)
    {
      this.batchParameters.Add(p);
    }
  }
}
