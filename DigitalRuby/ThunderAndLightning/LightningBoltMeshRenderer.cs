﻿// Decompiled with JetBrains decompiler
// Type: DigitalRuby.ThunderAndLightning.LightningBoltMeshRenderer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Rendering;

namespace DigitalRuby.ThunderAndLightning
{
  public class LightningBoltMeshRenderer
  {
    private readonly Dictionary<LightningBolt, List<LightningBoltMeshRenderer.LineRendererMesh>> renderers = new Dictionary<LightningBolt, List<LightningBoltMeshRenderer.LineRendererMesh>>();
    private readonly List<LightningBoltMeshRenderer.LineRendererMesh> rendererCache = new List<LightningBoltMeshRenderer.LineRendererMesh>();
    private LightningBoltMeshRenderer.LineRendererMesh currentLineRenderer;

    public LightningBoltScript Script { get; set; }

    public Material Material { get; set; }

    public Material MaterialNoGlow { get; set; }

    public float GlowWidthMultiplier { get; set; }

    public float GlowIntensityMultiplier { get; set; }

    [DebuggerHidden]
    private IEnumerator EnableRenderer(LightningBoltMeshRenderer.LineRendererMesh renderer, LightningBolt lightningBolt)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new LightningBoltMeshRenderer.\u003CEnableRenderer\u003Ec__Iterator4() { lightningBolt = lightningBolt, renderer = renderer, \u003C\u0024\u003ElightningBolt = lightningBolt, \u003C\u0024\u003Erenderer = renderer };
    }

    private LightningBoltMeshRenderer.LineRendererMesh CreateLineRenderer(LightningBolt lightningBolt, List<LightningBoltMeshRenderer.LineRendererMesh> lineRenderers)
    {
      LightningBoltMeshRenderer.LineRendererMesh lineRendererMesh;
      if (this.rendererCache.Count == 0)
      {
        GameObject gameObject = new GameObject();
        gameObject.name = "LightningBoltMeshRenderer";
        gameObject.hideFlags = HideFlags.HideAndDontSave;
        lineRendererMesh = gameObject.AddComponent<LightningBoltMeshRenderer.LineRendererMesh>();
      }
      else
      {
        lineRendererMesh = this.rendererCache[this.rendererCache.Count - 1];
        this.rendererCache.RemoveAt(this.rendererCache.Count - 1);
      }
      lineRendererMesh.gameObject.transform.parent = lightningBolt.Parent.transform;
      if (lightningBolt.UseWorldSpace)
        lineRendererMesh.gameObject.transform.position = Vector3.zero;
      else
        lineRendererMesh.gameObject.transform.localPosition = Vector3.zero;
      if (lightningBolt.UseWorldSpace)
        lineRendererMesh.gameObject.transform.rotation = Quaternion.identity;
      else
        lineRendererMesh.gameObject.transform.localRotation = Quaternion.identity;
      lineRendererMesh.gameObject.transform.localScale = Vector3.one;
      lineRendererMesh.Material = !lightningBolt.HasGlow ? this.MaterialNoGlow : this.Material;
      this.currentLineRenderer = lineRendererMesh;
      lineRenderers.Add(lineRendererMesh);
      return lineRendererMesh;
    }

    public void Begin(LightningBolt lightningBolt)
    {
      List<LightningBoltMeshRenderer.LineRendererMesh> lineRendererMeshList;
      if (this.renderers.TryGetValue(lightningBolt, out lineRendererMeshList))
        return;
      List<LightningBoltMeshRenderer.LineRendererMesh> lineRenderers = new List<LightningBoltMeshRenderer.LineRendererMesh>();
      this.renderers[lightningBolt] = lineRenderers;
      this.CreateLineRenderer(lightningBolt, lineRenderers);
    }

    public void End(LightningBolt lightningBolt)
    {
      if (!((Object) this.currentLineRenderer != (Object) null))
        return;
      this.Script.StartCoroutine(this.EnableRenderer(this.currentLineRenderer, lightningBolt));
      this.currentLineRenderer = (LightningBoltMeshRenderer.LineRendererMesh) null;
    }

    public void AddGroup(LightningBolt lightningBolt, LightningBoltSegmentGroup group, float growthMultiplier)
    {
      List<LightningBoltMeshRenderer.LineRendererMesh> lineRenderers = this.renderers[lightningBolt];
      LightningBoltMeshRenderer.LineRendererMesh renderer = lineRenderers[lineRenderers.Count - 1];
      float r = Time.timeSinceLevelLoad + group.Delay;
      Color c = new Color(r, r + group.PeakStart, r + group.PeakEnd, r + group.LifeTime);
      float radius = group.LineWidth * 0.5f;
      int lineCount = group.Segments.Count - group.StartIndex;
      float num1 = (radius - radius * group.EndWidthMultiplier) / (float) lineCount;
      float num2;
      float num3;
      if ((double) growthMultiplier > 0.0)
      {
        num2 = group.LifeTime / (float) lineCount * growthMultiplier;
        num3 = 0.0f;
      }
      else
      {
        num2 = 0.0f;
        num3 = 0.0f;
      }
      if (!renderer.PrepareForLines(lineCount))
      {
        this.Script.StartCoroutine(this.EnableRenderer(renderer, lightningBolt));
        renderer = this.CreateLineRenderer(lightningBolt, lineRenderers);
      }
      renderer.BeginLine(group.Segments[group.StartIndex].Start, group.Segments[group.StartIndex].End, radius, c, this.GlowWidthMultiplier, this.GlowIntensityMultiplier);
      for (int index = group.StartIndex + 1; index < group.Segments.Count; ++index)
      {
        radius -= num1;
        if ((double) growthMultiplier < 1.0)
        {
          num3 += num2;
          c = new Color(r + num3, r + group.PeakStart + num3, r + group.PeakEnd, r + group.LifeTime);
        }
        renderer.AppendLine(group.Segments[index].Start, group.Segments[index].End, radius, c, this.GlowWidthMultiplier, this.GlowIntensityMultiplier);
      }
    }

    public void Cleanup(LightningBolt lightningBolt)
    {
      List<LightningBoltMeshRenderer.LineRendererMesh> lineRendererMeshList;
      if (!this.renderers.TryGetValue(lightningBolt, out lineRendererMeshList))
        return;
      this.renderers.Remove(lightningBolt);
      using (List<LightningBoltMeshRenderer.LineRendererMesh>.Enumerator enumerator = lineRendererMeshList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          LightningBoltMeshRenderer.LineRendererMesh current = enumerator.Current;
          this.rendererCache.Add(current);
          current.Reset();
        }
      }
    }

    [RequireComponent(typeof (MeshFilter), typeof (MeshRenderer))]
    public class LineRendererMesh : MonoBehaviour
    {
      private static readonly Vector2 uv1 = new Vector2(0.0f, 0.0f);
      private static readonly Vector2 uv2 = new Vector2(1f, 0.0f);
      private static readonly Vector2 uv3 = new Vector2(0.0f, 1f);
      private static readonly Vector2 uv4 = new Vector2(1f, 1f);
      private readonly List<int> indices = new List<int>();
      private readonly List<Vector2> texCoords = new List<Vector2>();
      private readonly List<Vector3> vertices = new List<Vector3>();
      private readonly List<Vector4> lineDirs = new List<Vector4>();
      private readonly List<Color> colors = new List<Color>();
      private readonly List<Vector2> glowModifiers = new List<Vector2>();
      private readonly List<Vector3> ends = new List<Vector3>();
      private Mesh mesh;
      private MeshRenderer meshRenderer;

      public Material Material
      {
        get
        {
          return this.GetComponent<MeshRenderer>().sharedMaterial;
        }
        set
        {
          this.GetComponent<MeshRenderer>().sharedMaterial = value;
        }
      }

      private void Awake()
      {
        this.mesh = new Mesh();
        this.GetComponent<MeshFilter>().sharedMesh = this.mesh;
        this.meshRenderer = this.GetComponent<MeshRenderer>();
        this.meshRenderer.shadowCastingMode = ShadowCastingMode.Off;
        this.meshRenderer.useLightProbes = false;
        this.meshRenderer.receiveShadows = false;
        this.meshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
        this.meshRenderer.enabled = false;
      }

      private void AddIndices()
      {
        int count = this.vertices.Count;
        List<int> intList1 = this.indices;
        int num1 = count;
        int num2 = 1;
        int num3 = num1 + num2;
        intList1.Add(num1);
        List<int> intList2 = this.indices;
        int num4 = num3;
        int num5 = 1;
        int num6 = num4 + num5;
        intList2.Add(num4);
        this.indices.Add(num6);
        List<int> intList3 = this.indices;
        int num7 = num6;
        int num8 = 1;
        int num9 = num7 - num8;
        intList3.Add(num7);
        this.indices.Add(num9);
        int num10;
        this.indices.Add(num10 = num9 + 2);
      }

      public void Begin()
      {
        this.meshRenderer.enabled = true;
        this.mesh.vertices = this.vertices.ToArray();
        this.mesh.tangents = this.lineDirs.ToArray();
        this.mesh.colors = this.colors.ToArray();
        this.mesh.uv = this.texCoords.ToArray();
        this.mesh.uv2 = this.glowModifiers.ToArray();
        this.mesh.normals = this.ends.ToArray();
        this.mesh.triangles = this.indices.ToArray();
      }

      public bool PrepareForLines(int lineCount)
      {
        return this.vertices.Count + lineCount * 4 <= 64999;
      }

      public void BeginLine(Vector3 start, Vector3 end, float radius, Color c, float glowWidthModifier, float glowIntensity)
      {
        this.AddIndices();
        Vector2 vector2 = new Vector2(glowWidthModifier, glowIntensity);
        Vector4 vector4 = (Vector4) (end - start);
        vector4.w = radius;
        this.vertices.Add(start);
        this.texCoords.Add(LightningBoltMeshRenderer.LineRendererMesh.uv1);
        this.lineDirs.Add(vector4);
        this.colors.Add(c);
        this.glowModifiers.Add(vector2);
        this.ends.Add((Vector3) vector4);
        this.vertices.Add(end);
        this.texCoords.Add(LightningBoltMeshRenderer.LineRendererMesh.uv2);
        this.lineDirs.Add(vector4);
        this.colors.Add(c);
        this.glowModifiers.Add(vector2);
        this.ends.Add((Vector3) vector4);
        vector4.w = -radius;
        this.vertices.Add(start);
        this.texCoords.Add(LightningBoltMeshRenderer.LineRendererMesh.uv3);
        this.lineDirs.Add(vector4);
        this.colors.Add(c);
        this.glowModifiers.Add(vector2);
        this.ends.Add((Vector3) vector4);
        this.vertices.Add(end);
        this.texCoords.Add(LightningBoltMeshRenderer.LineRendererMesh.uv4);
        this.lineDirs.Add(vector4);
        this.colors.Add(c);
        this.glowModifiers.Add(vector2);
        this.ends.Add((Vector3) vector4);
      }

      public void AppendLine(Vector3 start, Vector3 end, float radius, Color c, float glowWidthModifier, float glowIntensity)
      {
        this.AddIndices();
        Vector2 vector2 = new Vector2(glowWidthModifier, glowIntensity);
        Vector4 vector4 = (Vector4) (end - start);
        vector4.w = radius;
        this.vertices.Add(start);
        this.texCoords.Add(LightningBoltMeshRenderer.LineRendererMesh.uv1);
        this.lineDirs.Add(this.lineDirs[this.lineDirs.Count - 3]);
        this.colors.Add(c);
        this.glowModifiers.Add(vector2);
        this.ends.Add((Vector3) vector4);
        this.vertices.Add(end);
        this.texCoords.Add(LightningBoltMeshRenderer.LineRendererMesh.uv2);
        this.lineDirs.Add(vector4);
        this.colors.Add(c);
        this.glowModifiers.Add(vector2);
        this.ends.Add((Vector3) vector4);
        vector4.w = -radius;
        this.vertices.Add(start);
        this.texCoords.Add(LightningBoltMeshRenderer.LineRendererMesh.uv3);
        this.lineDirs.Add(this.lineDirs[this.lineDirs.Count - 3]);
        this.colors.Add(c);
        this.glowModifiers.Add(vector2);
        this.ends.Add((Vector3) vector4);
        this.vertices.Add(end);
        this.texCoords.Add(LightningBoltMeshRenderer.LineRendererMesh.uv4);
        this.lineDirs.Add(vector4);
        this.colors.Add(c);
        this.glowModifiers.Add(vector2);
        this.ends.Add((Vector3) vector4);
      }

      public void Reset()
      {
        this.meshRenderer.enabled = false;
        this.mesh.triangles = (int[]) null;
        this.indices.Clear();
        this.vertices.Clear();
        this.colors.Clear();
        this.lineDirs.Clear();
        this.texCoords.Clear();
        this.glowModifiers.Clear();
        this.ends.Clear();
      }
    }
  }
}
