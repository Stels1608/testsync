﻿// Decompiled with JetBrains decompiler
// Type: SettingRadioButtonsUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SettingRadioButtonsUgui : MonoBehaviour, ISettingsReceiver
{
  private bool notifyListeners = true;
  [SerializeField]
  private Toggle[] toggles;
  [SerializeField]
  private Text settingName;
  private UserSetting setting;
  private Array options;

  public UserSetting Setting
  {
    get
    {
      return this.setting;
    }
  }

  public void Initialize<T>(UserSetting setting) where T : struct, IConvertible
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SettingRadioButtonsUgui.\u003CInitialize\u003Ec__AnonStorey129<T> initializeCAnonStorey129 = new SettingRadioButtonsUgui.\u003CInitialize\u003Ec__AnonStorey129<T>();
    // ISSUE: reference to a compiler-generated field
    initializeCAnonStorey129.setting = setting;
    // ISSUE: reference to a compiler-generated field
    initializeCAnonStorey129.\u003C\u003Ef__this = this;
    if (!typeof (T).IsEnum)
      Debug.LogError((object) "T must be an enumerated type");
    this.options = Enum.GetValues(typeof (T));
    // ISSUE: reference to a compiler-generated field
    this.setting = initializeCAnonStorey129.setting;
    for (int index = 0; index < this.options.Length; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      SettingRadioButtonsUgui.\u003CInitialize\u003Ec__AnonStorey128<T> initializeCAnonStorey128 = new SettingRadioButtonsUgui.\u003CInitialize\u003Ec__AnonStorey128<T>();
      // ISSUE: reference to a compiler-generated field
      initializeCAnonStorey128.\u003C\u003Ef__ref\u0024297 = initializeCAnonStorey129;
      // ISSUE: reference to a compiler-generated field
      initializeCAnonStorey128.\u003C\u003Ef__this = this;
      if (index >= this.toggles.Length)
      {
        Debug.LogError((object) ("SettingRadioButtonsUi.Initialize(): Amount of options and UI toggle buttons doesn't match! Toggle Buttons: " + (object) this.toggles.Length + " Options: " + (object) this.options.Length));
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        initializeCAnonStorey128.enumValue = (T) this.options.GetValue(index);
        // ISSUE: reference to a compiler-generated field
        initializeCAnonStorey128.toggle = this.toggles[index];
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        initializeCAnonStorey128.toggle.onValueChanged.AddListener(new UnityAction<bool>(initializeCAnonStorey128.\u003C\u003Em__2C5));
      }
    }
    this.LocalizeSettingName();
    this.LocalizeOptions<T>();
    FacadeFactory.GetInstance().SendMessage(Message.RegisterSettingsReceiver, (object) this);
  }

  private void OnDestroy()
  {
    FacadeFactory.GetInstance().SendMessage(Message.UnregisterSettingsReceiver, (object) this);
  }

  private void LocalizeSettingName()
  {
    this.settingName.text = Tools.GetLocalized(this.setting);
  }

  private void LocalizeOptions<T>() where T : struct, IConvertible
  {
    for (int index = 0; index < this.options.Length; ++index)
    {
      if (index < this.toggles.Length)
      {
        Enum enumValue = (Enum) this.options.GetValue(index);
        this.toggles[index].GetComponentsInChildren<Text>(true)[0].text = Tools.GetEnumLocalization<T>(enumValue);
      }
    }
  }

  private void ApplyOption(object optionToSelect)
  {
    this.notifyListeners = false;
    byte num = (byte) optionToSelect;
    for (int index = 0; index < this.options.Length; ++index)
      this.toggles[index].isOn = (int) (byte) this.options.GetValue(index) == (int) num;
    this.notifyListeners = true;
  }

  public void ReceiveSettings(UserSettings userSettings)
  {
    this.ApplyOption(userSettings.Get(this.setting));
  }

  public void ReceiveSetting(UserSetting userSetting, object data)
  {
    if (userSetting != this.setting)
      return;
    this.ApplyOption(data);
  }
}
