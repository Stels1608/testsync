﻿// Decompiled with JetBrains decompiler
// Type: GUIAbilityToolbar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class GUIAbilityToolbar : GUIPanel
{
  private int rightClickedIndex = -1;
  private List<GUIAbilityToolbar.Control> controls = new List<GUIAbilityToolbar.Control>();
  private const float CLICK_RADIUS = 20f;
  private const float BLINK_DURATION_TOOLBAR = 0.6f;
  public GUIImageNew background;
  private List<GUIButtonNew> buttons;
  private SmartRect imageRect;
  private GUIImageNew iItemAmount;
  private GUILabelNew lItemAmount;
  private GUILabelNew infinityAmount;
  private Texture2D availableTexture;
  private Texture2D notAvailableTexture;
  private Texture2D unpoweredTexture;
  private Texture2D repairTexture;
  private Texture2D toggledOnTexture;
  private Texture2D tStateImage;
  private GUIColoredImage stateImage;
  private GUICountdown cooldown;
  private List<Action> actions;
  private AtlasCache atlasCache;

  private ReadOnlyCollection<ShipAbility> Abilities
  {
    get
    {
      return Game.Me.ActiveShip.AbilityToolbar;
    }
  }

  public GUIAbilityToolbar(AtlasCache atlasCache)
    : base((SmartRect) null)
  {
    this.background = new GUIImageNew((Texture2D) ResourceLoader.Load("GUI/AbilityToolbar/background_9_slots"), this.root);
    this.actions = new List<Action>();
    for (int index = 114; index < 126; ++index)
      this.actions.Add((Action) index);
    this.MakeButtons();
    this.notAvailableTexture = ResourceLoader.Load<Texture2D>("GUI/AbilityToolbar/hex_slot_disabled");
    this.repairTexture = ResourceLoader.Load<Texture2D>("GUI/AbilityToolbar/hex_slot_repair");
    this.unpoweredTexture = ResourceLoader.Load<Texture2D>("GUI/AbilityToolbar/hex_slot_nopower");
    this.tStateImage = (Texture2D) ResourceLoader.Load("GUI/Common/state_image");
    this.stateImage = new GUIColoredImage(this.tStateImage, (SmartRect) null);
    this.toggledOnTexture = (Texture2D) ResourceLoader.Load("GUI/AbilityToolbar/hex_slot_active");
    this.iItemAmount = new GUIImageNew((Texture2D) ResourceLoader.Load("GUI/AbilityToolbar/ability_hexagon_slot_itemamountbox"), (SmartRect) null);
    this.iItemAmount.Position = new float2(10f, 5f);
    this.lItemAmount = new GUILabelNew(string.Empty, this.iItemAmount.SmartRect, Gui.Options.FontDS_EF_M, Color.white, Color.white);
    this.lItemAmount.FontSize = 8;
    this.lItemAmount.Position = new float2(0.0f, -1f);
    this.lItemAmount.AutoSize = true;
    this.lItemAmount.Alignment = TextAnchor.MiddleCenter;
    this.infinityAmount = new GUILabelNew("∞", this.iItemAmount.SmartRect, Gui.Options.FontDS_EF_M, Color.white, Color.white);
    this.infinityAmount.FontSize = 14;
    this.infinityAmount.Alignment = TextAnchor.MiddleCenter;
    this.cooldown = new GUICountdown((Texture2D) ResourceLoader.Load("GUI/AbilityToolbar/cooldown_atlas"), 8U, 8U);
    this.atlasCache = atlasCache;
    this.imageRect = new SmartRect(new Rect(0.0f, 0.0f, atlasCache.ElementSize.x, atlasCache.ElementSize.y), float2.zero, (SmartRect) null);
    this.AddPanel((GUIPanel) new GUIPanel.Timer(0.3f, new AnonymousDelegate(this.UpdateControls)));
  }

  private void UpdateControls()
  {
    for (int index = 0; index < Mathf.Min(this.Abilities.Count, 10); ++index)
    {
      ShipAbility shipAbility = this.Abilities[index];
      if (shipAbility != null)
      {
        this.controls[index].count = shipAbility.ConsumableCount;
        this.controls[index].countString = this.controls[index].count.ToString();
        this.controls[index].IsValid = shipAbility.IsValid;
      }
    }
  }

  private void MakeButtons()
  {
    float2 float2_1 = new float2(this.background.Width, this.background.Height);
    float2 float2_2 = new float2((float) (int) ((double) this.background.PositionX - (double) float2_1.x / 2.0 + 1.0), (float) (int) ((double) this.background.PositionY - (double) float2_1.y / 2.0));
    float2 float2_3 = new float2(25f, 21f);
    float2 float2_4 = new float2(61f, 43f);
    this.availableTexture = ResourceLoader.Load<Texture2D>("GUI/AbilityToolbar/hex_slot_active");
    Texture2D texture2D = ResourceLoader.Load<Texture2D>("GUI/AbilityToolbar/ability_hexagon_slot_itemover");
    Texture2D pressed = ResourceLoader.Load<Texture2D>("GUI/AbilityToolbar/ability_hexagon_slot_itemclick");
    this.buttons = new List<GUIButtonNew>();
    for (int index = 0; index < 10; ++index)
    {
      GUIButtonNew guiButtonNew = new GUIButtonNew(texture2D, texture2D, pressed);
      guiButtonNew.NormalTexture = (Texture2D) null;
      guiButtonNew.Parent = this.background.SmartRect;
      GUIBlinker guiBlinker = new GUIBlinker();
      guiBlinker.Width *= 1.5f;
      guiBlinker.Height *= 1.5f;
      guiButtonNew.AddPanel((GUIPanel) guiBlinker);
      this.buttons.Add(guiButtonNew);
    }
    this.buttons[0].Position = float2_2 + float2_3;
    this.buttons[1].Position = float2_2 + float2_4;
    float positionY1 = this.buttons[0].PositionY;
    float positionY2 = this.buttons[1].PositionY;
    for (int index = 2; index < 10; ++index)
    {
      float _y = index % 2 != 0 ? positionY2 : positionY1;
      this.buttons[index].Position = new float2(this.buttons[index - 2].PositionX + 72f, _y);
      this.buttons[index].Position = this.buttons[index].Position - this.background.Position;
    }
    for (int index = 0; index < this.buttons.Count; ++index)
      this.controls.Add(new GUIAbilityToolbar.Control());
  }

  public override void Draw()
  {
    this.background.Draw();
    for (int index = 0; index < Mathf.Min(this.Abilities.Count, 10); ++index)
    {
      ShipAbility shipAbility = this.Abilities[index];
      if (shipAbility != null)
      {
        SmartRect smartRect = this.buttons[index].SmartRect;
        this.imageRect.Parent = smartRect;
        this.imageRect.RecalculateAbsCoords();
        AtlasEntry atlasEntry = this.atlasCache.UnknownItem;
        if (shipAbility.guiCard != null)
          atlasEntry = this.atlasCache.GetCachedEntryBy(shipAbility.guiCard.GUIAtlasTexturePath, (int) shipAbility.guiCard.FrameIndex);
        if (Event.current.type == UnityEngine.EventType.Repaint)
          Graphics.DrawTexture(this.imageRect.AbsRect, (Texture) atlasEntry.Texture, atlasEntry.FrameRect, 0, 0, 0, 0);
        if (shipAbility.card != null)
        {
          this.stateImage.GetRect().Parent = smartRect;
          if (shipAbility.IsBroken)
            GUI.DrawTexture(smartRect.AbsRect, (Texture) this.repairTexture);
          else if (!shipAbility.IsPowerPointsEnough || shipAbility.ShortCircuited)
            GUI.DrawTexture(smartRect.AbsRect, (Texture) this.unpoweredTexture);
          else if (shipAbility.IsCooling)
          {
            this.cooldown.Progress = 1f - shipAbility.GetCoolingProgress();
            this.cooldown.GetRect().Parent = smartRect;
            this.cooldown.RecalculateAbsCoords();
            this.cooldown.Draw();
            if (!shipAbility.card.Auto)
            {
              float cooldown = shipAbility.GetCooldown();
              if ((double) cooldown < 0.600000023841858 && (double) cooldown > 0.0 && Event.current.type == UnityEngine.EventType.Repaint)
                Graphics.DrawTexture(smartRect.AbsRect, (Texture) this.availableTexture, new Rect(0.0f, 0.0f, 1f, 1f), 0, 0, 0, 0, new Color(1f, 1f, 1f, 1f - Mathf.Abs((float) (1.0 - (double) cooldown * 1.66666662693024 * 2.0))));
            }
          }
          else if (!this.controls[index].IsValid || Game.Me.Anchored)
            GUI.DrawTexture(smartRect.AbsRect, (Texture) this.notAvailableTexture);
          if (shipAbility.card.Countable)
          {
            this.iItemAmount.SmartRect.Parent = smartRect;
            this.iItemAmount.RecalculateAbsCoords();
            this.lItemAmount.RecalculateAbsCoords();
            this.infinityAmount.RecalculateAbsCoords();
            this.iItemAmount.Draw();
            if (this.controls[index].count < 10000U)
            {
              this.lItemAmount.Text = this.controls[index].countString;
              this.lItemAmount.Draw();
            }
            else
              this.infinityAmount.Draw();
          }
          if (shipAbility.ToggleAbilityActivated || shipAbility.card.Auto && shipAbility.On)
            GUI.DrawTexture(smartRect.AbsRect, (Texture) this.toggledOnTexture);
        }
      }
    }
    using (List<GUIButtonNew>.Enumerator enumerator = this.buttons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Draw();
    }
  }

  public override void Update()
  {
    base.Update();
    using (List<GUIButtonNew>.Enumerator enumerator = this.buttons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Update();
    }
  }

  public override void RecalculateAbsCoords()
  {
    float num = 20f;
    GuiTicker ticker = Game.Ticker;
    ExperienceBar experienceBar = Game.GUIManager.Find<ExperienceBar>();
    if (ticker != null && experienceBar != null)
      num = ticker.SizeY + experienceBar.Height;
    this.Position = new float2((float) ((double) Screen.width - (double) this.background.Width / 2.0 - 10.0), (float) Screen.height - this.background.Height / 2f - num);
    base.RecalculateAbsCoords();
    this.background.RecalculateAbsCoords();
    using (List<GUIButtonNew>.Enumerator enumerator = this.buttons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.RecalculateAbsCoords();
    }
  }

  public override bool Contains(float2 point)
  {
    return this.background.Contains(point);
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (Game.Me.Anchored || !this.Contains(mousePosition))
      return false;
    this.wasMouseDown = true;
    if (mouseKey == KeyCode.Mouse0)
    {
      float2 float2 = mousePosition - this.Position;
      for (int index = 0; index < Mathf.Min(this.Abilities.Count, 10); ++index)
      {
        if (this.Abilities[index] != null && (double) (float2 - this.buttons[index].Position).sqrMagnitude < 400.0)
        {
          this.buttons[index].IsPressed = true;
          break;
        }
      }
    }
    else if (mouseKey == KeyCode.Mouse1)
    {
      for (int index = 0; index < Mathf.Min(this.Abilities.Count, 10); ++index)
      {
        if (this.Abilities[index] != null && this.buttons[index].Contains(mousePosition))
        {
          if (this.Abilities[index].card.ConsumableOption != ShipConsumableOption.NotUsing)
          {
            this.rightClickedIndex = index;
            break;
          }
          break;
        }
      }
    }
    return true;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (Game.Me.Anchored || !this.wasMouseDown)
      return false;
    this.wasMouseDown = false;
    if (!this.Contains(mousePosition))
      return false;
    if (mouseKey == KeyCode.Mouse0)
    {
      float2 float2 = mousePosition - this.Position;
      for (int index = 0; index < Mathf.Min(this.Abilities.Count, 10); ++index)
      {
        ShipAbility shipAbility = this.Abilities[index];
        if (shipAbility != null && this.buttons[index].IsPressed)
        {
          this.buttons[index].IsPressed = false;
          if ((double) (float2 - this.buttons[index].Position).sqrMagnitude < 400.0)
          {
            shipAbility.Touch();
            if (this.buttons[index].Find<GUIBlinker>().IsRendered)
              StoryProtocol.GetProtocol().TriggerControl((StoryProtocol.ControlType) this.actions[index]);
            return true;
          }
        }
      }
    }
    else if (mouseKey == KeyCode.Mouse1 && this.rightClickedIndex >= 0 && this.buttons[this.rightClickedIndex].Contains(mousePosition))
    {
      GuiConsumablePanel guiConsumablePanel = Game.GUIManager.Find<GuiConsumablePanel>();
      guiConsumablePanel.IsRendered = true;
      guiConsumablePanel.Ability = this.Abilities[this.rightClickedIndex];
      guiConsumablePanel.MakeValidInScreenPosition(new Vector2()
      {
        x = this.buttons[this.rightClickedIndex].SmartRect.AbsRect.x,
        y = this.buttons[this.rightClickedIndex].SmartRect.AbsRect.y
      });
      this.rightClickedIndex = -1;
    }
    return true;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    if (Game.Me.Anchored)
      return;
    mousePosition -= this.Position;
    for (int index = 0; index < Mathf.Min(this.Abilities.Count, 10); ++index)
    {
      if (this.Abilities[index] != null)
      {
        bool flag = (double) (mousePosition - this.buttons[index].Position).sqrMagnitude < 400.0;
        this.buttons[index].MouseOver = flag;
      }
    }
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (Game.Me.Anchored)
      return false;
    for (int index = 0; index < this.actions.Count && index < this.buttons.Count; ++index)
    {
      if (action == this.actions[index] && this.Abilities.Count > index && this.Abilities[index] != null)
      {
        if (this.buttons[index] != null)
          this.buttons[index].IsPressed = true;
        return true;
      }
    }
    return false;
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (Game.Me.Anchored)
      return false;
    for (int index = 0; index < this.actions.Count && index < this.buttons.Count; ++index)
    {
      if (action == this.actions[index])
      {
        if (this.buttons[index] != null)
        {
          this.buttons[index].IsPressed = false;
          if (this.Abilities.Count > index && this.Abilities[index] != null)
            this.Abilities[index].Touch();
          if (this.buttons[index].Find<GUIBlinker>().IsRendered)
            StoryProtocol.GetProtocol().TriggerControl((StoryProtocol.ControlType) this.actions[index]);
        }
        return true;
      }
    }
    return false;
  }

  public override bool OnShowTooltip(float2 mousePosition)
  {
    float2 float2 = mousePosition - this.Position;
    for (int index = 0; index < Mathf.Min(this.Abilities.Count, 10); ++index)
    {
      ShipAbility ability = this.Abilities[index];
      if (ability != null && (double) (float2 - this.buttons[index].Position).sqrMagnitude < 400.0)
      {
        this.SetTooltip(ability, ability.guiCard);
        this.advancedTooltip.Position = new Vector2(float2.x, float2.y);
        Game.TooltipManager.ShowTooltip(this.advancedTooltip);
        return true;
      }
    }
    return false;
  }

  public void SetBlink(StoryProtocol.ControlType control, bool highlighted)
  {
    for (int index = 0; index < this.buttons.Count; ++index)
    {
      if ((StoryProtocol.ControlType) this.actions[index] == control)
        this.buttons[index].Find<GUIBlinker>().IsRendered = highlighted;
    }
  }

  private class Control
  {
    public string countString = string.Empty;
    public bool IsValid = true;
    public uint count;
  }
}
