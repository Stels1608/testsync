﻿// Decompiled with JetBrains decompiler
// Type: WofBonusMapLayoutDelta
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class WofBonusMapLayoutDelta : WofBonusMapLayout
{
  public WofBonusMapLayoutDelta()
  {
    this.bonusMapId = 4;
    this.maxParts = 15;
    this.mapPositions = new Dictionary<ushort, Vector2>();
    this.mapPositions.Add((ushort) 1, new Vector2(13f, -2f));
    this.mapPositions.Add((ushort) 2, new Vector2(7f, -3f));
    this.mapPositions.Add((ushort) 3, new Vector2(11f, -4f));
    this.mapPositions.Add((ushort) 4, new Vector2(0.0f, -6f));
    this.mapPositions.Add((ushort) 5, new Vector2(12f, -2f));
    this.mapPositions.Add((ushort) 6, new Vector2(3f, -2f));
    this.mapPositions.Add((ushort) 7, new Vector2(4f, -3f));
    this.mapPositions.Add((ushort) 8, new Vector2(3f, -5f));
    this.mapPositions.Add((ushort) 9, new Vector2(12f, -5f));
    this.mapPositions.Add((ushort) 10, new Vector2(0.0f, -5f));
    this.mapPositions.Add((ushort) 11, new Vector2(15f, -5f));
    this.mapPositions.Add((ushort) 12, new Vector2(2f, -1f));
    this.mapPositions.Add((ushort) 13, new Vector2(5f, -4f));
    this.mapPositions.Add((ushort) 14, new Vector2(8f, -2f));
    this.mapPositions.Add((ushort) 15, new Vector2(12f, -6f));
    this.difficulty = WofBonusMapDifficulty.Beginner;
    this.requiredLevel = 1;
    this.mapName = Tools.ParseMessage("%$bgo.wof_bonusmap." + (object) this.bonusMapId + "%");
  }
}
