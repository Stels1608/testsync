﻿// Decompiled with JetBrains decompiler
// Type: FtlResultScreenDialogBoxContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

internal class FtlResultScreenDialogBoxContent : MonoBehaviour, ILocalizeable
{
  [SerializeField]
  private UILabel enemysKilledNumber;
  [SerializeField]
  private UILabel enemysKilledText;
  [SerializeField]
  private UILabel missionTimeNumber;
  [SerializeField]
  private UILabel missionTimeText;
  [SerializeField]
  private UILabel wavesCompleteText;
  [SerializeField]
  private UILabel wavesCompleteNumber;
  [SerializeField]
  private UISprite medalIcon;
  [SerializeField]
  private UILabel medaltext;
  [SerializeField]
  private UISprite medalBackground;
  [SerializeField]
  private OneButtonDialogBar buttonBar;
  [SerializeField]
  private UILabel missionSummaryText;
  [SerializeField]
  private GameObject rewardList;
  [SerializeField]
  private WindowCloseButtonWidget closeButton;
  private FtlResultContainer result;

  public OneButtonDialogBar ButtonBar
  {
    get
    {
      return this.buttonBar;
    }
  }

  private void Start()
  {
    this.ReloadLanguageData();
    this.Invoke("DelayRewardItems", 0.2f);
    foreach (UIWidget componentsInChild in this.GetComponentInChildren<ScrollBarWidget>().GetComponentsInChildren<UISprite>())
      componentsInChild.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
  }

  public void Init(FtlResultContainer info)
  {
    this.result = info;
    this.wavesCompleteNumber.text = ((int) info.WavesCompleted).ToString() + " / " + (object) info.MaxWaves;
    this.enemysKilledNumber.text = info.EnemysKilled.ToString();
    this.missionTimeNumber.text = Utils.FormatTime3((int) info.MissionTime);
    this.medaltext.text = Tools.ParseMessage("%$bgo.wof.result_rank_" + info.Rank.ToString().ToLower() + "%");
    this.medalIcon.spriteName = "icon_medal_" + info.Rank.ToString().ToLower();
    this.SetMedalBgColor(info.Rank);
    this.closeButton.handleClick = (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage(Message.HideDialogBox));
  }

  private void SetMedalBgColor(FtlRanks rank)
  {
    Color white = Color.white;
    switch (rank)
    {
      case FtlRanks.Platinum:
        white = ColorManager.currentColorScheme.Get(WidgetColorType.MEDAL_PLATINUM);
        break;
      case FtlRanks.Gold:
        white = ColorManager.currentColorScheme.Get(WidgetColorType.MEDAL_GOLD);
        break;
      case FtlRanks.Silver:
        white = ColorManager.currentColorScheme.Get(WidgetColorType.MEDAL_SILVER);
        break;
      case FtlRanks.Bronze:
        white = ColorManager.currentColorScheme.Get(WidgetColorType.MEDAL_BRONZE);
        break;
    }
    this.medalBackground.color = white;
  }

  public void ReloadLanguageData()
  {
    this.enemysKilledText.text = Tools.ParseMessage("%$bgo.wof.result_enemys_killed%");
    this.missionTimeText.text = Tools.ParseMessage("%$bgo.wof.result_mission_time%");
    this.wavesCompleteText.text = Tools.ParseMessage("%$bgo.wof.result_waves_complete%");
    this.missionSummaryText.text = Tools.ParseMessage("%$bgo.wof.mission_summary%");
  }

  public void DelayRewardItems()
  {
    if (this.result == null || this.result.RewardList == null)
      return;
    using (List<ShipItem>.Enumerator enumerator = this.result.RewardList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (!(bool) enumerator.Current.IsLoaded)
        {
          this.Invoke("DelayRewardItems", 0.1f);
          return;
        }
      }
    }
    this.rewardList.SetActive(true);
    using (List<ShipItem>.Enumerator enumerator = this.result.RewardList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipItem current = enumerator.Current;
        ItemListElement component = NGUITools.AddChild(this.rewardList, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/general/ItemListElement")).GetComponent<ItemListElement>();
        if ((Object) component != (Object) null)
        {
          uint amount = 1;
          if (current is ItemCountable)
            amount = (current as ItemCountable).Count;
          component.SetItem(current.ItemGUICard, amount, 505);
        }
      }
    }
    this.rewardList.GetComponent<UIGrid>().Reposition();
    this.rewardList.GetComponent<UIScrollView>().ResetPosition();
  }
}
