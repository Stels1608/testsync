﻿// Decompiled with JetBrains decompiler
// Type: GUIDoubleCountdown
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIDoubleCountdown : CustomGUIElement
{
  private const float grayPiePercent = 0.3f;
  private const int numChunks = 12;
  private TextureAtlas inner;
  private TextureAtlas outer;
  private SmartRect srOuter;
  private SmartRect srInner;
  private int iOuter;
  private int iInner;

  public GUIDoubleCountdown()
  {
    Texture2D texture1 = (Texture2D) ResourceLoader.Load("GUI/Common/outer");
    Texture2D texture2 = (Texture2D) ResourceLoader.Load("GUI/Common/inner");
    this.outer = new TextureAtlas(texture1, 6U, 4U);
    this.inner = new TextureAtlas(texture2, 6U, 4U);
    this.rect = new SmartRect(new Rect(0.0f, 0.0f, this.outer.ElementSize.x, this.outer.ElementSize.y), float2.zero, (SmartRect) null);
    this.srInner = new SmartRect(new Rect(0.0f, 0.0f, this.inner.ElementSize.x, this.inner.ElementSize.y), float2.zero, this.rect);
    this.srOuter = new SmartRect(new Rect(0.0f, 0.0f, this.outer.ElementSize.x, this.outer.ElementSize.y), float2.zero, this.rect);
  }

  public override void Draw()
  {
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    Graphics.DrawTexture(this.srOuter.AbsRect, (Texture) this.outer.Texture, this.outer.GetFrameRectByIndex((uint) this.iOuter), 0, 0, 0, 0);
    Graphics.DrawTexture(this.srInner.AbsRect, (Texture) this.inner.Texture, this.inner.GetFrameRectByIndex((uint) this.iInner), 0, 0, 0, 0);
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.srOuter.RecalculateAbsCoords();
    this.srInner.RecalculateAbsCoords();
  }

  public void SetProgress(float progress)
  {
    int num1 = (int) ((double) progress * 12.0);
    if (num1 == 12)
      --num1;
    float num2 = Mathf.Clamp01(progress * 12f - (float) num1);
    int num3 = num1 * 2;
    this.iInner = num3 + 1;
    if ((double) num2 < 0.300000011920929)
      this.iInner = num3;
    this.iOuter = (int) ((double) num2 * 24.0);
    if (this.iOuter != 24)
      return;
    --this.iOuter;
  }
}
