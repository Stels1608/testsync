﻿// Decompiled with JetBrains decompiler
// Type: Cache`2
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class Cache<U, T> where T : ICacheValue<U>, new()
{
  private Dictionary<U, T> cache = new Dictionary<U, T>();

  public T Get(U key)
  {
    T obj;
    if (!this.cache.TryGetValue(key, out obj))
    {
      obj = new T();
      obj.SetKey(key);
      this.cache.Add(key, obj);
    }
    return obj;
  }

  public V Get<V>(U key) where V : T, new()
  {
    T obj;
    if (!this.cache.TryGetValue(key, out obj))
    {
      obj = (T) new V();
      obj.SetKey(key);
      this.cache.Add(key, obj);
    }
    return (V) (object) obj;
  }
}
