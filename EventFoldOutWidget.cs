﻿// Decompiled with JetBrains decompiler
// Type: EventFoldOutWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class EventFoldOutWidget : FoldOutWidget
{
  public AnonymousDelegate handleClick;
  public UITexture eventIcon;
  private SectorEvent sectorEvent;
  private bool fadeout;
  public DynamicEventTracker tracker;

  public SectorEvent SectorEvent
  {
    get
    {
      return this.sectorEvent;
    }
    set
    {
      this.sectorEvent = value;
      this.eventIcon.mainTexture = (Texture) this.sectorEvent.SectorEventCard.Icon;
      this.groupname.text = this.sectorEvent.SectorEventCard.Name;
      this.Refresh();
    }
  }

  public new void Start()
  {
    this.handleClick = new AnonymousDelegate(((FoldOutWidget) this).OnClick);
    this.tracker = this.foldOutContent.GetComponent<DynamicEventTracker>();
  }

  public void ActivateCloseButton()
  {
    this.handleClick = new AnonymousDelegate(this.Close);
  }

  public override void OnClick()
  {
    if (this.handleClick == null)
      return;
    this.handleClick();
  }

  public void Close()
  {
    if (this.SectorEvent == null)
      return;
    FacadeFactory.GetInstance().SendMessage(Message.SectorEventTerminated, (object) this.SectorEvent.ObjectID);
  }

  public void Refresh()
  {
    this.Flash();
    this.SectorEvent.SectorEventCard.IsLoaded.AddHandler((SignalHandler) (() => this.groupname.text = BsgoLocalization.Get(this.SectorEvent.SectorEventCard.Name).ToUpperInvariant()));
    if (this.SectorEvent.State != SectorEventState.Failed && this.SectorEvent.State != SectorEventState.Success)
      return;
    this.FadeOut();
  }

  private void Flash()
  {
  }

  public void FadeComponent(GameObject component, float d, float a)
  {
    if ((Object) component == (Object) null)
      return;
    TweenAlpha.Begin(component, d, a);
  }

  public void UpdateTasks(List<SectorEventTask> tasks)
  {
    if (this.fadeout || tasks.Count <= 0)
      return;
    SectorEventTask sectorEventTask = tasks[0];
    if (sectorEventTask.TaskType != SectorEventTaskType.Protect)
      return;
    this.tracker.ShowProtectTask((SectorEventProtectTask) sectorEventTask, this.SectorEvent.OwnerFaction);
  }

  private void FadeOut()
  {
    this.fadeout = true;
    this.FadeComponent(this.gameObject, 2f, 0.0f);
  }
}
