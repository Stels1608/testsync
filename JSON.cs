﻿// Decompiled with JetBrains decompiler
// Type: JSON
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Globalization;
using System.Text;

public class JSON
{
  public const int TOKEN_NONE = 0;
  public const int TOKEN_CURLY_OPEN = 1;
  public const int TOKEN_CURLY_CLOSE = 2;
  public const int TOKEN_SQUARED_OPEN = 3;
  public const int TOKEN_SQUARED_CLOSE = 4;
  public const int TOKEN_COLON = 5;
  public const int TOKEN_COMMA = 6;
  public const int TOKEN_STRING = 7;
  public const int TOKEN_NUMBER = 8;
  public const int TOKEN_TRUE = 9;
  public const int TOKEN_FALSE = 10;
  public const int TOKEN_NULL = 11;
  private const int BUILDER_CAPACITY = 2000;

  public static object JsonDecode(string json)
  {
    bool success = true;
    return JSON.JsonDecode(json, ref success);
  }

  public static object JsonDecode(string json, ref bool success)
  {
    success = true;
    if (json == null)
      return (object) null;
    char[] charArray = json.ToCharArray();
    int index = 0;
    return JSON.ParseValue(charArray, ref index, ref success);
  }

  public static string JsonEncode(object json)
  {
    StringBuilder builder = new StringBuilder(2000);
    if (JSON.SerializeValue(json, builder))
      return builder.ToString();
    return (string) null;
  }

  protected static Hashtable ParseObject(char[] json, ref int index, ref bool success)
  {
    Hashtable hashtable = new Hashtable();
    JSON.NextToken(json, ref index);
    bool flag = false;
    while (!flag)
    {
      switch (JSON.LookAhead(json, index))
      {
        case 0:
          success = false;
          return (Hashtable) null;
        case 6:
          JSON.NextToken(json, ref index);
          continue;
        case 2:
          JSON.NextToken(json, ref index);
          return hashtable;
        default:
          string @string = JSON.ParseString(json, ref index, ref success);
          if (!success)
          {
            success = false;
            return (Hashtable) null;
          }
          if (JSON.NextToken(json, ref index) != 5)
          {
            success = false;
            return (Hashtable) null;
          }
          object obj = JSON.ParseValue(json, ref index, ref success);
          if (!success)
          {
            success = false;
            return (Hashtable) null;
          }
          hashtable[(object) @string] = obj;
          continue;
      }
    }
    return hashtable;
  }

  protected static ArrayList ParseArray(char[] json, ref int index, ref bool success)
  {
    ArrayList arrayList = new ArrayList();
    JSON.NextToken(json, ref index);
    bool flag = false;
    while (!flag)
    {
      switch (JSON.LookAhead(json, index))
      {
        case 0:
          success = false;
          return (ArrayList) null;
        case 6:
          JSON.NextToken(json, ref index);
          continue;
        case 4:
          JSON.NextToken(json, ref index);
          goto label_9;
        default:
          object obj = JSON.ParseValue(json, ref index, ref success);
          if (!success)
            return (ArrayList) null;
          arrayList.Add(obj);
          continue;
      }
    }
label_9:
    return arrayList;
  }

  protected static object ParseValue(char[] json, ref int index, ref bool success)
  {
    switch (JSON.LookAhead(json, index))
    {
      case 1:
        return (object) JSON.ParseObject(json, ref index, ref success);
      case 3:
        return (object) JSON.ParseArray(json, ref index, ref success);
      case 7:
        return (object) JSON.ParseString(json, ref index, ref success);
      case 8:
        return (object) JSON.ParseNumber(json, ref index, ref success);
      case 9:
        JSON.NextToken(json, ref index);
        return (object) true;
      case 10:
        JSON.NextToken(json, ref index);
        return (object) false;
      case 11:
        JSON.NextToken(json, ref index);
        return (object) null;
      default:
        success = false;
        return (object) null;
    }
  }

  protected static string ParseString(char[] json, ref int index, ref bool success)
  {
    StringBuilder stringBuilder = new StringBuilder(2000);
    JSON.EatWhitespace(json, ref index);
    char[] chArray1 = json;
    int num1;
    index = (num1 = index) + 1;
    int index1 = num1;
    char ch1 = chArray1[index1];
    bool flag = false;
    while (!flag && index != json.Length)
    {
      char[] chArray2 = json;
      int num2;
      index = (num2 = index) + 1;
      int index2 = num2;
      char ch2 = chArray2[index2];
      switch (ch2)
      {
        case '"':
          flag = true;
          goto label_19;
        case '\\':
          if (index != json.Length)
          {
            char[] chArray3 = json;
            int num3;
            index = (num3 = index) + 1;
            int index3 = num3;
            switch (chArray3[index3])
            {
              case '"':
                stringBuilder.Append('"');
                continue;
              case '\\':
                stringBuilder.Append('\\');
                continue;
              case '/':
                stringBuilder.Append('/');
                continue;
              case 'b':
                stringBuilder.Append('\b');
                continue;
              case 'f':
                stringBuilder.Append('\f');
                continue;
              case 'n':
                stringBuilder.Append('\n');
                continue;
              case 'r':
                stringBuilder.Append('\r');
                continue;
              case 't':
                stringBuilder.Append('\t');
                continue;
              case 'u':
                if (json.Length - index >= 4)
                {
                  uint result;
                  // ISSUE: explicit reference operation
                  // ISSUE: cast to a reference type
                  // ISSUE: explicit reference operation
                  if ((int) (^(sbyte&) @success = (sbyte) uint.TryParse(new string(json, index, 4), NumberStyles.HexNumber, (IFormatProvider) CultureInfo.InvariantCulture, out result)) == 0)
                    return string.Empty;
                  stringBuilder.Append(char.ConvertFromUtf32((int) result));
                  index = index + 4;
                  continue;
                }
                goto label_19;
              default:
                continue;
            }
          }
          else
            goto label_19;
        default:
          stringBuilder.Append(ch2);
          continue;
      }
    }
label_19:
    if (flag)
      return stringBuilder.ToString();
    success = false;
    return (string) null;
  }

  protected static double ParseNumber(char[] json, ref int index, ref bool success)
  {
    JSON.EatWhitespace(json, ref index);
    int lastIndexOfNumber = JSON.GetLastIndexOfNumber(json, index);
    int length = lastIndexOfNumber - index + 1;
    double result;
    success = double.TryParse(new string(json, index, length), NumberStyles.Any, (IFormatProvider) CultureInfo.InvariantCulture, out result);
    index = lastIndexOfNumber + 1;
    return result;
  }

  protected static int GetLastIndexOfNumber(char[] json, int index)
  {
    int index1 = index;
    while (index1 < json.Length && "0123456789+-.eE".IndexOf(json[index1]) != -1)
      ++index1;
    return index1 - 1;
  }

  protected static void EatWhitespace(char[] json, ref int index)
  {
    while (index < json.Length && " \t\n\r".IndexOf(json[index]) != -1)
      index = index + 1;
  }

  protected static int LookAhead(char[] json, int index)
  {
    int index1 = index;
    return JSON.NextToken(json, ref index1);
  }

  protected static int NextToken(char[] json, ref int index)
  {
    JSON.EatWhitespace(json, ref index);
    if (index == json.Length)
      return 0;
    char ch1 = json[index];
    index = index + 1;
    char ch2 = ch1;
    switch (ch2)
    {
      case '"':
        return 7;
      case ',':
        return 6;
      case '-':
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        return 8;
      case ':':
        return 5;
      default:
        switch (ch2)
        {
          case '[':
            return 3;
          case ']':
            return 4;
          default:
            switch (ch2)
            {
              case '{':
                return 1;
              case '}':
                return 2;
              default:
                index = index - 1;
                int num = json.Length - index;
                if (num >= 5 && (int) json[index] == 102 && ((int) json[index + 1] == 97 && (int) json[index + 2] == 108) && ((int) json[index + 3] == 115 && (int) json[index + 4] == 101))
                {
                  index = index + 5;
                  return 10;
                }
                if (num >= 4 && (int) json[index] == 116 && ((int) json[index + 1] == 114 && (int) json[index + 2] == 117) && (int) json[index + 3] == 101)
                {
                  index = index + 4;
                  return 9;
                }
                if (num < 4 || (int) json[index] != 110 || ((int) json[index + 1] != 117 || (int) json[index + 2] != 108) || (int) json[index + 3] != 108)
                  return 0;
                index = index + 4;
                return 11;
            }
        }
    }
  }

  protected static bool SerializeValue(object value, StringBuilder builder)
  {
    bool flag = true;
    if (value is string)
      flag = JSON.SerializeString((string) value, builder);
    else if (value is Hashtable)
      flag = JSON.SerializeObject((Hashtable) value, builder);
    else if (value is ArrayList)
      flag = JSON.SerializeArray((ArrayList) value, builder);
    else if (value is bool && (bool) value)
      builder.Append("true");
    else if (value is bool && !(bool) value)
      builder.Append("false");
    else if (value is ValueType)
      flag = JSON.SerializeNumber(Convert.ToDouble(value), builder);
    else if (value == null)
      builder.Append("null");
    else
      flag = false;
    return flag;
  }

  protected static bool SerializeObject(Hashtable anObject, StringBuilder builder)
  {
    builder.Append("{");
    IDictionaryEnumerator enumerator = anObject.GetEnumerator();
    bool flag = true;
    while (enumerator.MoveNext())
    {
      string @string = enumerator.Key.ToString();
      object obj = enumerator.Value;
      if (!flag)
        builder.Append(", ");
      JSON.SerializeString(@string, builder);
      builder.Append(":");
      if (!JSON.SerializeValue(obj, builder))
        return false;
      flag = false;
    }
    builder.Append("}");
    return true;
  }

  protected static bool SerializeArray(ArrayList anArray, StringBuilder builder)
  {
    builder.Append("[");
    bool flag = true;
    for (int index = 0; index < anArray.Count; ++index)
    {
      object obj = anArray[index];
      if (!flag)
        builder.Append(", ");
      if (!JSON.SerializeValue(obj, builder))
        return false;
      flag = false;
    }
    builder.Append("]");
    return true;
  }

  protected static bool SerializeString(string aString, StringBuilder builder)
  {
    builder.Append("\"");
    foreach (char @char in aString.ToCharArray())
    {
      switch (@char)
      {
        case '"':
          builder.Append("\\\"");
          break;
        case '\\':
          builder.Append("\\\\");
          break;
        case '\b':
          builder.Append("\\b");
          break;
        case '\f':
          builder.Append("\\f");
          break;
        case '\n':
          builder.Append("\\n");
          break;
        case '\r':
          builder.Append("\\r");
          break;
        case '\t':
          builder.Append("\\t");
          break;
        default:
          int int32 = Convert.ToInt32(@char);
          if (int32 >= 32 && int32 <= 126)
          {
            builder.Append(@char);
            break;
          }
          builder.Append("\\u" + Convert.ToString(int32, 16).PadLeft(4, '0'));
          break;
      }
    }
    builder.Append("\"");
    return true;
  }

  protected static bool SerializeNumber(double number, StringBuilder builder)
  {
    builder.Append(Convert.ToString(number, (IFormatProvider) CultureInfo.InvariantCulture));
    return true;
  }
}
