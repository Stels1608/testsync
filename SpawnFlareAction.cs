﻿// Decompiled with JetBrains decompiler
// Type: SpawnFlareAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class SpawnFlareAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    this.SpawnFlareAtBackOf((SpaceObject) message.Data);
  }

  private void SpawnFlareAtBackOf(SpaceObject shooter)
  {
    if (!(shooter is Ship))
    {
      Debug.LogError((object) "FlareReleased by Non-Ship. Error.");
    }
    else
    {
      Ship ship = (Ship) shooter;
      GameObject model = shooter.Model;
      if ((Object) model == (Object) null)
      {
        Debug.LogError((object) "SpawnFlareAction(): Spawning SpaceObject's Model is null");
      }
      else
      {
        Vector3 vector3 = model.transform.position + model.transform.rotation * shooter.MeshBoundsRaw.center + model.transform.rotation * shooter.MeshBoundsRaw.extents.z * Vector3.back;
        GameObject gameObject = Object.Instantiate<GameObject>((int) ship.ShipCardLight.Tier == 2 ? Resources.Load("Fx/flareT2") as GameObject : Resources.Load("Fx/flareT1") as GameObject);
        gameObject.transform.position = vector3;
        gameObject.transform.rotation = model.transform.rotation * Quaternion.Euler(0.0f, 180f, 0.0f);
      }
    }
  }
}
