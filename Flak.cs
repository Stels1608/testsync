﻿// Decompiled with JetBrains decompiler
// Type: Flak
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Flak : Weapon
{
  public static float ExplosionEmissionRateMultiplier = 1f;
  public bool m_createFlakExplosion = true;
  public float AudioInnerSphereRadius = 200f;
  public float AudioOuterSphereRadius = 300f;
  public float AudioDistance = 50f;
  public float AudioDelay = 0.5f;
  public float VolleyVolume = 1f;
  public float VolleyMinDistance = 1f;
  public float ExplosionsVolume = 1f;
  public float ExplosionMinDistance = 1f;
  private float lastDebugFireTime = float.NegativeInfinity;
  public GameObject Explosion;
  [HideInInspector]
  public GameObject ExplosionInstance;
  private ParticleSystem explosionParticleSystem;
  private float explosionEmissionRate;
  public Transform MuzzleSpot;
  public GameObject MuzzleEffect;
  public AudioClip VolleyClip;
  public AudioClip ExplosionsLoop;
  public AudioClip ExplosionsEnd;
  public bool DebugFire;
  public float DebugFireInterval;
  private GameObject muzzle;
  private Transform _transform;
  private AudioSource gunAudio;

  private void Awake()
  {
    this._transform = this.transform;
    this.gunAudio = this.gameObject.AddComponent<AudioSource>();
    this.gunAudio.spatialBlend = 1f;
    this.gunAudio.volume = this.VolleyVolume;
    this.gunAudio.minDistance = this.VolleyMinDistance;
    this.gunAudio.maxDistance = this.gunAudio.minDistance * 256f;
    this.gunAudio.clip = this.VolleyClip;
    this.gunAudio.playOnAwake = false;
    this.gunAudio.loop = false;
    if (!((Object) this.Explosion != (Object) null) || !((Object) this.ExplosionInstance == (Object) null))
      return;
    this.ExplosionInstance = Object.Instantiate((Object) this.Explosion, this.transform.position, this.transform.rotation) as GameObject;
    this.ExplosionInstance.transform.parent = this._transform;
    this.explosionParticleSystem = this.ExplosionInstance.GetComponentInChildren<ParticleSystem>();
    this.explosionEmissionRate = this.explosionParticleSystem.emissionRate;
  }

  private void Update()
  {
    if (!this.DebugFire || (double) Time.time - (double) this.lastDebugFireTime <= (double) this.DebugFireInterval)
      return;
    this.lastDebugFireTime = Time.time;
    this.Fire((SpaceObject) null);
  }

  private bool IsPrimaryWeapon()
  {
    return this.WeaponLocationId == 0;
  }

  public override void Fire(SpaceObject target)
  {
    if (!this.HighQuality)
      return;
    if (!this.IsPrimaryWeapon())
    {
      if (!((Object) this.ExplosionInstance != (Object) null))
        return;
      Object.Destroy((Object) this.ExplosionInstance);
    }
    else
    {
      if ((Object) this._transform != (Object) null && (Object) this.ExplosionInstance != (Object) null)
      {
        if (!this.explosionParticleSystem.isPlaying)
          this.explosionParticleSystem.Play();
        this.explosionParticleSystem.emissionRate = this.explosionEmissionRate * Flak.ExplosionEmissionRateMultiplier;
        this.ExplosionInstance.transform.position = this._transform.position;
        this.ExplosionInstance.transform.rotation = this._transform.rotation;
        FlakExplosionTimer componentInChildren = this.ExplosionInstance.GetComponentInChildren<FlakExplosionTimer>();
        if (this.explosionParticleSystem.isPlaying && (double) componentInChildren.m_lifeTime < (double) this.explosionParticleSystem.duration / 2.0)
          componentInChildren.m_lifeTime = this.explosionParticleSystem.duration;
      }
      if ((Object) this.MuzzleEffect != (Object) null)
        Object.Instantiate((Object) this.MuzzleEffect, this.MuzzleSpot.position, this.MuzzleSpot.rotation);
      this.gunAudio.Play();
    }
  }
}
