﻿// Decompiled with JetBrains decompiler
// Type: InjectSettingAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;

public class InjectSettingAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    SettingsDataProvider settingsDataProvider = (SettingsDataProvider) this.OwnerFacade.FetchDataProvider("SettingsDataProvider");
    Tuple<ISettingsReceiver, UserSetting> tuple = message.Data as Tuple<ISettingsReceiver, UserSetting>;
    ISettingsReceiver first = tuple.First;
    UserSetting second = tuple.Second;
    first.ReceiveSetting(second, settingsDataProvider.CurrentSettings.Get(second));
  }
}
