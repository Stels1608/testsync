﻿// Decompiled with JetBrains decompiler
// Type: DebugSpline
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugSpline : MonoBehaviour
{
  private List<Vector3> list = new List<Vector3>();
  private Vector3 p0 = new Vector3(0.0f, 0.0f, 0.0f);
  private Vector3 p1 = new Vector3(0.0f, 10f, 0.0f);
  private Vector3 p2 = new Vector3(0.0f, 10f, 0.0f);
  private Vector3 p3 = new Vector3(10f, 0.0f, 0.0f);

  public void Start()
  {
    float t = 0.0f;
    while ((double) t < 1.0)
    {
      this.list.Add(Algorithm3D.CubicBezierCurves(this.p0, this.p1, this.p2, this.p3, t));
      t += 0.02f;
    }
    for (int index = 1; index < this.list.Count; ++index)
      Log.DebugInfo((object) (this.list[index] - this.list[index - 1]).magnitude);
  }

  public void Update()
  {
    BgoDebug.DrawPathWithNormal(this.list.ToArray(), Vector3.back, 5f, Color.white);
    Debug.DrawLine(this.p0, this.p1, Color.blue);
    Debug.DrawLine(this.p1, this.p2, Color.blue);
    Debug.DrawLine(this.p2, this.p3, Color.blue);
  }
}
