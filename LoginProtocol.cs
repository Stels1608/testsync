﻿// Decompiled with JetBrains decompiler
// Type: LoginProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class LoginProtocol : BgoProtocol
{
  public string PlayerName = string.Empty;
  public ConnectType ConnectType;
  public uint PlayerId;
  public string SessionCode;

  public LoginProtocol()
    : base(BgoProtocol.ProtocolID.Login)
  {
  }

  public static LoginProtocol GetProtocol()
  {
    return Game.ProtocolManager.GetProtocol(BgoProtocol.ProtocolID.Login) as LoginProtocol;
  }

  public void SendInit()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    this.SendMessage(bw);
    Game.Funnel.ReportActivity(Activity.GameclientRequestsLogin);
  }

  public void SendPlayer()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 2);
    bw.Write((byte) this.ConnectType);
    bw.Write(this.PlayerId);
    bw.Write(this.PlayerName);
    bw.Write(this.SessionCode);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    LoginProtocol.Reply reply = (LoginProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case LoginProtocol.Reply.Hello:
        this.SendInit();
        break;
      case LoginProtocol.Reply.Init:
        Game.ServerRevision = br.ReadUInt32();
        if (LoginLevel.GetLevel().CheckServer())
        {
          LoginLevel.GetLevel().OnInit();
          break;
        }
        Game.GetProtocolManager().Disconnect("Wrong protocol rev." + (object) Game.ServerRevision + ", support only rev." + (object) 4578U + "\nTry to update client.");
        break;
      case LoginProtocol.Reply.Error:
        Game.GetProtocolManager().Disconnect("Login Error\nType: " + (object) (LoginError) br.ReadByte() + "\n");
        Game.Funnel.ReportActivity(Activity.LoginFailed);
        break;
      case LoginProtocol.Reply.Player:
        int years = br.ReadInt32();
        int months = br.ReadInt32();
        int days = br.ReadInt32();
        int hours = br.ReadInt32();
        int minutes = br.ReadInt32();
        int seconds = br.ReadInt32();
        Game.TimeSync.Init(br.ReadInt64(), years, months, days, hours, minutes, seconds);
        DebugUtility.Create(br.ReadUInt32());
        LoginLevel.GetLevel().OnLogin();
        Game.Funnel.ReportActivity(Activity.LoginSuccessful);
        break;
      case LoginProtocol.Reply.Wait:
        LoginLevel.GetLevel().OnWait(br.ReadUInt32());
        Game.Funnel.ReportActivity(Activity.LoginQueue);
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Login protocol: " + (object) reply);
        break;
    }
  }

  public enum Request : ushort
  {
    Init = 1,
    Player = 2,
    Echo = 5,
  }

  public enum Reply : ushort
  {
    Hello,
    Init,
    Error,
    Player,
    Wait,
    Echo,
  }
}
