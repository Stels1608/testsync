﻿// Decompiled with JetBrains decompiler
// Type: MovementOptions
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MovementOptions : IProtocolRead
{
  public float maxRegularLinSpeed = 60f;
  public float acceleration = 10f;
  public float inertiaCompensation = 10f;
  public float strafeAcceleration = 20f;
  public float strafeMaxSpeed = 20f;
  public Gear gear;
  public float speed;
  public float pitchAcceleration;
  public float pitchMaxSpeed;
  public float yawAcceleration;
  public float yawMaxSpeed;
  public float rollAcceleration;
  public float rollMaxSpeed;
  public float minYawSpeed;
  public float maxPitch;
  public float maxRoll;
  public float pitchFading;
  public float yawFading;
  public float rollFading;

  public Euler3 maxTurnAcceleration
  {
    get
    {
      return new Euler3(this.pitchAcceleration, this.yawAcceleration, this.rollAcceleration);
    }
  }

  public Euler3 maxTurnSpeed
  {
    get
    {
      return new Euler3(this.pitchMaxSpeed, this.yawMaxSpeed, this.rollMaxSpeed);
    }
  }

  public Euler3 MinEulerSpeed(Euler3 euler3)
  {
    return new Euler3(this.MinPitchSpeed(euler3.pitch), this.MinYawSpeed(euler3.roll, euler3.pitch), this.MinRollSpeed(euler3.roll));
  }

  public Euler3 MaxEulerSpeed(Euler3 euler3)
  {
    return new Euler3(this.MaxPitchSpeed(euler3.pitch), this.MaxYawSpeed(euler3.roll, euler3.pitch), this.MaxRollSpeed(euler3.roll));
  }

  public float MinYawSpeed(float roll, float pitch)
  {
    return Mathf.Lerp(-this.yawMaxSpeed, -this.minYawSpeed, (float) (((double) -Mathf.Clamp(Algorithm3D.NormalizeAngle(roll) / this.maxRoll, -1f, 1f) + 1.0) / 2.0)) * (float) (1.0 + (double) Mathf.Abs(Algorithm3D.NormalizeAngle(pitch)) * 1.0 / 90.0);
  }

  public float MaxYawSpeed(float roll, float pitch)
  {
    return Mathf.Lerp(this.minYawSpeed, this.yawMaxSpeed, (float) (((double) -Mathf.Clamp(Algorithm3D.NormalizeAngle(roll) / this.maxRoll, -1f, 1f) + 1.0) / 2.0)) * (float) (1.0 + (double) Mathf.Abs(Algorithm3D.NormalizeAngle(pitch)) * 1.0 / 90.0);
  }

  public float MinPitchSpeed(float pitch)
  {
    float num = (float) (-(double) this.maxPitch * 0.699999988079071);
    if ((double) pitch < (double) num)
      return Mathf.Lerp(-this.pitchMaxSpeed, 0.0f, Mathf.Clamp01((float) (((double) num - (double) pitch) / ((double) this.maxPitch * 0.300000011920929))));
    return -this.pitchMaxSpeed;
  }

  public float MaxPitchSpeed(float pitch)
  {
    float num = this.maxPitch * 0.7f;
    if ((double) pitch > (double) num)
      return Mathf.Lerp(this.pitchMaxSpeed, 0.0f, Mathf.Clamp01((float) ((-(double) num + (double) pitch) / ((double) this.maxPitch * 0.300000011920929))));
    return this.pitchMaxSpeed;
  }

  public float MinRollSpeed(float roll)
  {
    float num = (float) (-(double) this.maxRoll * 0.5);
    return (double) roll >= (double) num ? -this.rollMaxSpeed : Mathf.Lerp(-this.rollMaxSpeed, 0.0f, Mathf.Clamp01((float) (((double) num - (double) roll) / ((double) this.maxRoll * 0.5))));
  }

  public float MaxRollSpeed(float roll)
  {
    float num = this.maxRoll * 0.5f;
    return (double) roll <= (double) num ? this.rollMaxSpeed : Mathf.Lerp(this.rollMaxSpeed, 0.0f, Mathf.Clamp01((float) ((-(double) num + (double) roll) / ((double) this.maxRoll * 0.5))));
  }

  public void Read(BgoProtocolReader pr)
  {
    this.gear = (Gear) pr.ReadByte();
    this.speed = pr.ReadSingle();
    this.acceleration = pr.ReadSingle();
    this.inertiaCompensation = pr.ReadSingle();
    this.pitchAcceleration = pr.ReadSingle();
    this.pitchMaxSpeed = pr.ReadSingle();
    this.yawAcceleration = pr.ReadSingle();
    this.yawMaxSpeed = pr.ReadSingle();
    this.rollAcceleration = pr.ReadSingle();
    this.rollMaxSpeed = pr.ReadSingle();
    this.strafeAcceleration = pr.ReadSingle();
    this.strafeMaxSpeed = pr.ReadSingle();
  }

  public void ApplyCard(MovementCard card)
  {
    this.minYawSpeed = card.minYawSpeed * this.yawMaxSpeed;
    this.maxPitch = card.maxPitch;
    this.maxRoll = card.maxRoll;
    this.pitchFading = card.pitchFading;
    this.yawFading = card.yawFading;
    this.rollFading = card.rollFading;
  }

  public override string ToString()
  {
    return string.Format("Gear: {0}, Speed: {1}, MaxRegularLinSpeed: {2}, Acceleration: {3}, InteriaCompensation: {4}, PitchAcceleration: {5}, PitchMaxSpeed: {6}, YawAcceleration: {7}, YawMaxSpeed: {8}, RollAcceleration: {9}, RollMaxSpeed: {10}, StrafeAcceleration: {11}, StrafeMaxSpeed: {12}, MinYawSpeed: {13}, MaxPitch: {14}, MaxRoll: {15}, PitchFading: {16}, YawFading: {17}, RollFading: {18}", (object) this.gear, (object) this.speed, (object) this.maxRegularLinSpeed, (object) this.acceleration, (object) this.inertiaCompensation, (object) this.pitchAcceleration, (object) this.pitchMaxSpeed, (object) this.yawAcceleration, (object) this.yawMaxSpeed, (object) this.rollAcceleration, (object) this.rollMaxSpeed, (object) this.strafeAcceleration, (object) this.strafeMaxSpeed, (object) this.minYawSpeed, (object) this.maxPitch, (object) this.maxRoll, (object) this.pitchFading, (object) this.yawFading, (object) this.rollFading);
  }
}
