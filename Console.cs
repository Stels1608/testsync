﻿// Decompiled with JetBrains decompiler
// Type: Console
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public sealed class Console : GUIPanel
{
  private bool isFocused;
  private List<string> log;
  private List<string> history;
  private int historyIndex;
  private Vector2 hintScrollPosition;
  private Dictionary<string, Console.Parser> parsers;
  private Dictionary<string, string> help;
  private GUIStyle labelStyle;
  private int cWidth;
  private int cHeight;
  private Rect scrollRect;
  private Vector2 scrollPosition;
  private string[] commandNames;
  private Rect bgRect;
  private Texture2D background;
  private string text;
  private string textInt;
  private string textAreaName;
  private Rect textAreaRect;

  public override bool IsRendered
  {
    get
    {
      return this.isFocused;
    }
    set
    {
      this.isFocused = value;
    }
  }

  public Console()
    : base((SmartRect) null)
  {
    this.Name = "Console";
    this.isFocused = false;
    this.log = new List<string>();
    this.history = new List<string>();
    this.hintScrollPosition = new Vector2();
    this.parsers = new Dictionary<string, Console.Parser>();
    this.help = new Dictionary<string, string>();
    this.RegisterCommand("die", new Console.Parser(TestCommands.Die), "Type this to die instantly");
    this.RegisterCommand("debug.traffic", new Console.Parser(DebugBehaviour<DebugTraffic>.Command), "Show traffic info");
    this.RegisterCommand("debug.stats", new Console.Parser(DebugBehaviour<DebugStatsView>.Command), "Show stats info");
    this.RegisterCommand("debug.fxmanager", new Console.Parser(DebugBehaviour<DebugFXManager>.Command), "Show SFX info");
    this.RegisterCommand("debug.buttons", new Console.Parser(DebugBehaviour<DebugButtons>.Command), "Show buttons debug");
    this.RegisterCommand("server", new Console.Parser(DebugUtility.Command), "Server commands, restart/reset_mobs");
    this.RegisterCommand("execute.ability", new Console.Parser(DebugControlCommand.ExecuteAbility), "Execute Ability");
    this.RegisterCommand("l", new Console.Parser(Game.LevelConsoleCommand), "Level specific command");
    this.RegisterCommand("give.system", new Console.Parser(DebugUtility.GiveSystem), "Gives system item to player\nUsage: give.system PlayerName ItemId ItemLevel");
    this.RegisterCommand("give.consumable", new Console.Parser(DebugUtility.GiveConsumable), "Gives consumable item to player\nUsage: give.consumable PlayerName ItemId ItemCount");
    this.RegisterCommand("give.resource", new Console.Parser(DebugUtility.GiveResource), "Gives resource item to player\nUsage: give.resource PlayerName ItemId ItemAmount");
    this.RegisterCommand("give.experience", new Console.Parser(DebugUtility.GiveExperience), "Gives experience to player\nUsage: give.experience PlayerName ExpAmount");
    this.RegisterCommand("help", new Console.Parser(this.Help), "Show help");
    this.labelStyle = new GUIStyle();
    this.labelStyle.normal.textColor = new Color((float) byte.MaxValue, (float) byte.MaxValue, (float) byte.MaxValue);
    this.labelStyle.margin.left = 3;
    this.scrollPosition = new Vector2();
    this.background = (Texture2D) ResourceLoader.Load("GUI/Console/console_background");
    if ((UnityEngine.Object) this.background == (UnityEngine.Object) null)
      throw new ArgumentException("texture not loaded");
    this.text = string.Empty;
    this.textInt = string.Empty;
    this.textAreaName = "consoleTextArea";
  }

  public override void Draw()
  {
    this.cWidth = Camera.main.pixelWidth;
    this.cHeight = Camera.main.pixelHeight / 2;
    this.scrollRect = new Rect(0.0f, 0.0f, (float) this.cWidth, (float) (this.cHeight - 25));
    this.bgRect = new Rect(0.0f, 0.0f, (float) this.cWidth, (float) this.cHeight);
    this.textAreaRect = new Rect(0.0f, (float) (this.cHeight - 20), (float) (this.cWidth - 30), 20f);
    GUI.color = new Color(1f, 1f, 1f, 0.7f);
    GUI.DrawTexture(this.bgRect, (Texture) this.background);
    GUI.FocusControl(this.textAreaName);
    GUI.SetNextControlName(this.textAreaName);
    string str = GUI.TextArea(this.textAreaRect, this.text);
    if (str != this.text)
    {
      this.text = str;
      this.textInt = str;
    }
    this.OnGUIHintProcessing();
    this.OnGUIRenderScrollView();
  }

  private void OnGUIHintProcessing()
  {
    if (this.textInt.Length <= 0)
      return;
    this.commandNames = this.GetAllParsersStartsWith(this.textInt);
    if (this.commandNames.Length > 0)
    {
      float num = 0.0f;
      foreach (string commandName in this.commandNames)
      {
        Size size = TextUtility.CalcTextSize(commandName, GUI.skin.font, GUI.skin.font.fontSize);
        num = (double) size.width <= (double) num ? num : (float) size.width;
      }
      Rect rect = new Rect(30f, (float) this.cHeight, num + 30f, (float) (this.commandNames.Length * 25));
      GUI.DrawTexture(rect, (Texture) this.background);
      GUILayout.BeginArea(rect);
      this.hintScrollPosition = GUILayout.BeginScrollView(this.hintScrollPosition);
      foreach (string commandName in this.commandNames)
        GUILayout.Label(commandName);
      GUILayout.EndScrollView();
      GUILayout.EndArea();
    }
    else
      this.hintScrollPosition = new Vector2();
  }

  private void OnGUIRenderScrollView()
  {
    GUILayout.BeginArea(this.scrollRect);
    this.scrollPosition = GUILayout.BeginScrollView(this.scrollPosition);
    using (List<string>.Enumerator enumerator = this.log.GetEnumerator())
    {
      while (enumerator.MoveNext())
        GUILayout.Label(enumerator.Current, this.labelStyle, new GUILayoutOption[0]);
    }
    GUILayout.EndScrollView();
    GUILayout.EndArea();
  }

  public void Parse(string text)
  {
    text = Console.CleanString(Console.CleanString(text, '`', '~', '\n', '\t', '\r'));
    this.Add("> " + text);
    this.history.Add(text);
    this.historyIndex = this.history.Count;
    List<string> stringList = new List<string>();
    string str1 = string.Empty;
    for (int index = 0; index < text.Length; ++index)
    {
      if ((int) text[index] == 32)
      {
        if (str1.Length > 0 && (int) str1[0] == 34)
        {
          str1 += (string) (object) text[index];
        }
        else
        {
          if (str1.Length > 0)
            stringList.Add(str1);
          str1 = string.Empty;
        }
      }
      else if ((int) text[index] == 34 && str1.Length > 0 && (int) str1[0] == 34 && (text.Length == index + 1 || (int) text[index + 1] == 32))
      {
        string str2 = str1 + (object) text[index];
        stringList.Add(str2);
        str1 = string.Empty;
      }
      else
      {
        str1 += (string) (object) text[index];
        if (text.Length == index + 1)
          stringList.Add(str1);
      }
    }
    string key = stringList[0];
    stringList.RemoveAt(0);
    string[] array = stringList.ToArray();
    for (int index = 0; index < array.Length; ++index)
      array[index] = array[index].TrimStart('"').TrimEnd('"');
    Console.Parser parser;
    if (this.parsers.TryGetValue(key, out parser))
    {
      if (parser(array))
        return;
      this.Add("Bad argument(s): " + string.Join(" ", array));
    }
    else
      this.Add("Unknown command: " + key);
  }

  private string[] GetAllParsersStartsWith(string text)
  {
    List<string> stringList = new List<string>();
    using (Dictionary<string, Console.Parser>.Enumerator enumerator = this.parsers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, Console.Parser> current = enumerator.Current;
        if (current.Key.StartsWith(text))
          stringList.Add(current.Key);
      }
    }
    return stringList.ToArray();
  }

  public static string CleanString(string str)
  {
    char[] chArray = new char[6]{ ' ', '`', '~', '\n', '\t', '\r' };
    str = str.TrimStart(chArray);
    str = str.TrimEnd(chArray);
    return str;
  }

  public static string CleanString(string toClean, params char[] toRemove)
  {
    string str = string.Empty;
    foreach (char ch1 in toClean)
    {
      bool flag = false;
      foreach (char ch2 in toRemove)
      {
        if ((int) ch1 == (int) ch2)
          flag = true;
      }
      if (!flag)
        str += (string) (object) ch1;
    }
    return str;
  }

  public static bool ParseBool(string command, out bool res)
  {
    res = false;
    if (command == "true" || command == "1" || command == "on")
    {
      res = true;
      return true;
    }
    if (!(command == "false") && !(command == "0") && !(command == "off"))
      return false;
    res = false;
    return true;
  }

  public void Add(string message)
  {
    this.log.Add(message);
  }

  public void ExecuteCommand(string command)
  {
    this.Parse(command);
  }

  public void RegisterCommand(string command, Console.Parser handler)
  {
    this.RegisterCommand(command, handler, string.Empty);
  }

  public void RegisterCommand(string command, Console.Parser handler, string helpText)
  {
    if (handler == null || this.parsers.ContainsKey(command))
      return;
    this.parsers.Add(command, handler);
    this.help.Add(command, helpText);
  }

  public override bool Contains(float2 point)
  {
    if (!this.isFocused)
      return false;
    return this.bgRect.Contains(point.ToV2());
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (!DebugUtility.HasAccess(BgoAdminRoles.Console))
      return false;
    this.text = Console.CleanString(this.text, '`', '~', '\n', '\t', '\r');
    if (Game.GUIManager.Find<GUIChatNew>() != null && Game.GUIManager.Find<GUIChatNew>().IsFocused)
      return false;
    if (keyboardKey == KeyCode.F10)
    {
      this.ToggleIsRendered();
      Game.InputDispatcher.Focused = !this.IsRendered ? (InputListener) null : (InputListener) this;
      this.textInt = this.text;
      return true;
    }
    if (!this.IsRendered)
      return false;
    if (keyboardKey == KeyCode.Return)
    {
      if (this.text != string.Empty)
      {
        this.ExecuteCommand(this.text);
        this.text = string.Empty;
        this.scrollPosition = new Vector2(0.0f, float.PositiveInfinity);
      }
      this.textInt = this.text;
      return true;
    }
    if (keyboardKey == KeyCode.Tab)
    {
      this.commandNames = this.GetAllParsersStartsWith(this.text);
      if (this.commandNames.Length == 1)
        this.text = this.commandNames[0] + " ";
      else if (this.commandNames.Length > 1)
        this.text = this.FindMaximumLeadingSubstring(this.commandNames);
      this.textInt = this.text;
      return true;
    }
    if (keyboardKey == KeyCode.UpArrow || keyboardKey == KeyCode.DownArrow)
    {
      if (this.textInt.Length == 0)
      {
        if (this.history.Count > 0)
        {
          if (keyboardKey == KeyCode.UpArrow)
            --this.historyIndex;
          else
            ++this.historyIndex;
          if (this.historyIndex < 0)
            this.historyIndex = this.history.Count - 1;
          if (this.historyIndex >= this.history.Count)
            this.historyIndex = 0;
          this.text = this.history[this.historyIndex];
        }
      }
      else
      {
        this.commandNames = this.GetAllParsersStartsWith(this.textInt);
        if (this.commandNames.Length > 0)
        {
          int num = Array.IndexOf<string>(this.commandNames, this.text.Trim());
          int index = keyboardKey != KeyCode.UpArrow ? num + 1 : num - 1;
          if (index < 0)
            index = this.commandNames.Length - 1;
          if (index >= this.commandNames.Length)
            index = 0;
          this.text = this.commandNames[index] + " ";
        }
      }
      return true;
    }
    if (keyboardKey >= KeyCode.A && keyboardKey <= KeyCode.Z || keyboardKey >= KeyCode.Alpha0 && keyboardKey <= KeyCode.Alpha9)
    {
      this.textInt = this.text;
      return true;
    }
    this.textInt = this.text;
    return false;
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    return DebugUtility.HasAccess(BgoAdminRoles.Console) && (Game.GUIManager.Find<GUIChatNew>() == null || !Game.GUIManager.Find<GUIChatNew>().IsFocused) && this.IsRendered && (keyboardKey == KeyCode.Return || keyboardKey == KeyCode.Tab || (keyboardKey == KeyCode.UpArrow || keyboardKey == KeyCode.DownArrow) || (keyboardKey >= KeyCode.A && keyboardKey <= KeyCode.Z || keyboardKey >= KeyCode.Alpha0 && keyboardKey <= KeyCode.Alpha9));
  }

  private string FindMaximumLeadingSubstring(string[] entries)
  {
    int index1 = 0;
    for (int index2 = 0; index2 < entries.Length; ++index2)
    {
      if (entries[index2].Length < entries[index1].Length)
        index1 = index2;
    }
    for (int length = 0; length < entries[index1].Length; ++length)
    {
      foreach (string entry in entries)
      {
        if ((int) entry[length] != (int) entries[index1][length])
          return entries[index1].Substring(0, length);
      }
    }
    return entries[0];
  }

  private bool Help(string[] messages)
  {
    if (messages.Length == 0)
    {
      this.Add("All available commands:");
      using (Dictionary<string, Console.Parser>.Enumerator enumerator = this.parsers.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.Add(enumerator.Current.Key);
      }
      this.Add("To get help on command, type help commandName");
      return true;
    }
    if (!this.help.ContainsKey(messages[0]))
      return false;
    this.Add(this.help[messages[0]]);
    return true;
  }

  public delegate bool Parser(string[] message);
}
