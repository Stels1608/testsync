﻿// Decompiled with JetBrains decompiler
// Type: RoomLevelProfile
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RoomLevelProfile : LevelProfile
{
  private static readonly List<string> requiredRoomAssets = new List<string>();
  private readonly uint sectorId;
  private readonly uint cardGuid;

  public uint CardGuid
  {
    get
    {
      return this.cardGuid;
    }
  }

  public string SceneName
  {
    get
    {
      return "HangarLevel";
    }
  }

  public RoomCard Card
  {
    get
    {
      return (RoomCard) Game.Catalogue.FetchCard(this.cardGuid, CardView.Room);
    }
  }

  public WorldCard WorldCard
  {
    get
    {
      return (WorldCard) Game.Catalogue.FetchCard(this.cardGuid, CardView.World);
    }
  }

  public bool Ready
  {
    get
    {
      if ((bool) this.Card.IsLoaded)
        return (bool) this.WorldCard.IsLoaded;
      return false;
    }
  }

  public IEnumerable<string> RequiredAssets
  {
    get
    {
      return (IEnumerable<string>) RoomLevelProfile.requiredRoomAssets;
    }
  }

  public uint SectorId
  {
    get
    {
      return this.sectorId;
    }
  }

  public RoomLevelProfile(uint sectorId, uint cardGuid)
  {
    this.sectorId = sectorId;
    this.cardGuid = cardGuid;
  }
}
