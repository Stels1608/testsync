﻿// Decompiled with JetBrains decompiler
// Type: NGUISetAlpha
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class NGUISetAlpha : MonoBehaviour
{
  public float alpha = 0.8f;
  private bool mCached;
  private UIRect[] mRects;
  private List<Material> mMats;

  public float value
  {
    set
    {
      if (!this.mCached)
        this.Cache();
      if (this.mRects.Length != 0)
      {
        foreach (UIRect mRect in this.mRects)
          mRect.alpha = value;
      }
      else
      {
        using (List<Material>.Enumerator enumerator = this.mMats.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Material current = enumerator.Current;
            Color color = current.color;
            color.a = value;
            current.color = color;
          }
        }
      }
    }
  }

  private void Awake()
  {
    this.value = this.alpha;
  }

  private void Cache()
  {
    this.mCached = true;
    this.mRects = this.GetComponentsInChildren<UIRect>();
    if (this.mRects.Length != 0)
      return;
    Renderer[] componentsInChildren = this.GetComponentsInChildren<Renderer>();
    if (componentsInChildren != null)
    {
      foreach (Renderer renderer in componentsInChildren)
        this.mMats.Add(renderer.material);
    }
    if (this.mMats.Count != 0)
      return;
    this.mRects = this.GetComponentsInChildren<UIRect>();
  }
}
