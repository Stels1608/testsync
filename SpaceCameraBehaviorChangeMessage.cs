﻿// Decompiled with JetBrains decompiler
// Type: SpaceCameraBehaviorChangeMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class SpaceCameraBehaviorChangeMessage : IMessage<Message>
{
  private readonly ISpaceCameraBehavior newBehavior;
  private readonly ISpaceCameraBehavior oldBehavior;

  public Message Id
  {
    get
    {
      return Message.CameraBehaviorChanged;
    }
  }

  public object Data
  {
    get
    {
      return (object) this.newBehavior;
    }
    set
    {
    }
  }

  public ISpaceCameraBehavior NewBehavior
  {
    get
    {
      return this.newBehavior;
    }
  }

  public ISpaceCameraBehavior OldBehavior
  {
    get
    {
      return this.oldBehavior;
    }
  }

  public SpaceCameraBehaviorChangeMessage(ISpaceCameraBehavior oldBehavior, ISpaceCameraBehavior newBehavior)
  {
    this.newBehavior = newBehavior;
    this.oldBehavior = oldBehavior;
  }
}
