﻿// Decompiled with JetBrains decompiler
// Type: ShipBuilderSpot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUI;
using System;
using UnityEngine;

[Serializable]
public class ShipBuilderSpot
{
  public int level = -1;
  public int dummyShipSelected = -1;
  public int place_id = -1;
  public string spot_id = string.Empty;
  public Vector2 pos = Vector2.zero;
  private Vector2 dragStart = Vector2.zero;
  public SlotGui image;
  public ShipSlotType slotType;
  private bool dragging;
  public ShipSlotType slotTypeSelected;
  public bool isSelected;

  public void OnGUI(Rect paperDollSize)
  {
    Vector2 vector2 = this.pos + new Vector2(paperDollSize.x, paperDollSize.y) + new Vector2(paperDollSize.width / 2f, paperDollSize.height / 2f);
    GUIStyle style = new GUIStyle(UnityEngine.GUI.skin.label);
    style.alignment = TextAnchor.MiddleCenter;
    style.normal.textColor = !this.isSelected ? Color.white : Color.green;
    UnityEngine.GUI.Label(new Rect(vector2.x - 50f, vector2.y - 23f, 100f, 47f), this.place_id.ToString(), style);
    UnityEngine.GUI.Label(new Rect(vector2.x - 100f, vector2.y, 200f, 47f), this.slotType.ToString(), style);
  }

  public bool Drag(Rect draggingRect)
  {
    if (Event.current.type == UnityEngine.EventType.MouseUp)
      this.dragging = false;
    else if (Event.current.type == UnityEngine.EventType.MouseDown && draggingRect.Contains(Event.current.mousePosition))
    {
      this.dragging = true;
      this.dragStart = Event.current.mousePosition - this.pos;
      Event.current.Use();
      return true;
    }
    if (this.dragging && (double) (Event.current.mousePosition - this.pos - this.dragStart).magnitude > 5.0)
      this.pos = Event.current.mousePosition - this.dragStart;
    return false;
  }

  public void Load(Vector2 pos, Rect paperDollSize)
  {
    this.pos = pos + new Vector2(paperDollSize.width / 2f, paperDollSize.height / 2f);
  }

  public Vector2 Save(Rect paperDollSize)
  {
    return this.pos - new Vector2(paperDollSize.width / 2f, paperDollSize.height / 2f);
  }
}
