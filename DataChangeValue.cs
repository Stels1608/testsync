﻿// Decompiled with JetBrains decompiler
// Type: DataChangeValue
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public struct DataChangeValue
{
  private readonly float valueOld;
  private readonly float valueNew;

  public float ValueOld
  {
    get
    {
      return this.valueOld;
    }
  }

  public float ValueNew
  {
    get
    {
      return this.valueNew;
    }
  }

  public DataChangeValue(float valueOld, float valueNew)
  {
    this.valueOld = valueOld;
    this.valueNew = valueNew;
  }
}
