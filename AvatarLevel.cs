﻿// Decompiled with JetBrains decompiler
// Type: AvatarLevel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.View;
using System.Collections;
using System.Diagnostics;
using Unity5AssetHandling;
using UnityEngine;

public class AvatarLevel : GameLevel
{
  public override GameLevelType LevelType
  {
    get
    {
      return GameLevelType.AvatarLevel;
    }
  }

  public override bool IsIngameLevel
  {
    get
    {
      return false;
    }
  }

  protected override void AddLoadingScreenDependencies()
  {
  }

  public override void Initialize(LevelProfile levelProfile)
  {
    base.Initialize(levelProfile);
    SceneProtocol.GetProtocol().NotifySceneLoaded();
  }

  protected override void Start()
  {
    FacadeFactory.GetInstance().AttachView((IView<Message>) new CharacterCreationMediator());
    AssetRequest request = (AssetRequest) null;
    if (Game.Me.Faction == Faction.Colonial)
      request = AssetCatalogue.Instance.Request("character_creation_human.prefab", false);
    else if (Game.Me.Faction == Faction.Cylon)
      request = AssetCatalogue.Instance.Request("character_creation_cylon.prefab", false);
    else
      UnityEngine.Debug.LogError((object) ("Unknown faction: " + (object) Game.Me.Faction));
    this.StartCoroutine(this.WaitForPrefab(request));
    MessageBoxManager messageBoxManager = new MessageBoxManager();
    messageBoxManager.Position = new Vector2(50f, 0.0f);
    Game.InputDispatcher.AddListener((InputListener) messageBoxManager);
    Game.GUIManager.AddPanel((IGUIPanel) messageBoxManager);
    base.Start();
    FacadeFactory.GetInstance().SendMessage(Message.CharacterCreationStarted);
  }

  [DebuggerHidden]
  private IEnumerator WaitForPrefab(AssetRequest request)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AvatarLevel.\u003CWaitForPrefab\u003Ec__Iterator28() { request = request, \u003C\u0024\u003Erequest = request };
  }
}
