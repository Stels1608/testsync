﻿// Decompiled with JetBrains decompiler
// Type: HelpContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class HelpContent : MonoBehaviour
{
  private const string KEY_START = "%$bgo.HelpScreens.Screens.";
  private const string KEY_TITLE = ".title_label%";
  private const string KEY_MAIN = ".main_label%";
  private const string KEY_SUBMAIN = ".submain_label%";
  private const string KEY_S1 = ".step1_label%";
  private const string KEY_S2 = ".step2_label%";
  private const string KEY_S3 = ".step3_label%";
  public UILabel title_L;
  public UILabel main_L;
  public UILabel subMain_L;
  public UILabel step1_L;
  public UILabel step2_L;
  public UILabel step3_L;
  public UISprite main_S;
  public UISprite step1_S;
  public UISprite step2_S;
  public UISprite step3_S;
  [SerializeField]
  public UIAtlas colonialAtlas;
  [SerializeField]
  public UIAtlas cylonAtlas;
  [SerializeField]
  public List<HelpScreen> screens;
  [SerializeField]
  public GameObject helpListObject;
  [SerializeField]
  public TabPanelWidget panel;

  public List<HelpScreenType> CompletedTutorials { get; set; }

  public bool ShowTutorial { get; set; }

  public void Awake()
  {
    this.InitContent();
    this.AddButtons();
  }

  public void Start()
  {
    this.Invoke("RepositionButtons", 0.1f);
  }

  public void RepositionButtons()
  {
    this.panel.GetComponent<UITable>().Reposition();
  }

  public void SwitchAtlas(Faction faction)
  {
    if (faction == Faction.Cylon)
    {
      this.main_S.atlas = this.cylonAtlas;
      this.step1_S.atlas = this.cylonAtlas;
      this.step2_S.atlas = this.cylonAtlas;
      this.step3_S.atlas = this.cylonAtlas;
    }
    else
    {
      this.main_S.atlas = this.colonialAtlas;
      this.step1_S.atlas = this.colonialAtlas;
      this.step2_S.atlas = this.colonialAtlas;
      this.step3_S.atlas = this.colonialAtlas;
    }
  }

  public void InitContent()
  {
    this.screens = new List<HelpScreen>()
    {
      new HelpScreen()
      {
        screen = "screen_shipcontrols",
        tutname = "%$bgo.etc.tut1%",
        picturename = "shipcontrols"
      },
      new HelpScreen()
      {
        screen = "screen_advancedcontrol",
        tutname = "%$bgo.etc.tut2%",
        picturename = "advancedcontrol",
        type = HelpScreenType.AdvancedControls
      },
      new HelpScreen()
      {
        screen = "screen_npcinteraction",
        tutname = "%$bgo.etc.tut3%",
        picturename = "npcinteraction",
        type = HelpScreenType.NPCInteraction
      },
      new HelpScreen()
      {
        screen = "screen_dailyassignment",
        tutname = "%$bgo.etc.tut4%",
        picturename = "dailyassignment",
        type = HelpScreenType.DailyAssignments
      },
      new HelpScreen()
      {
        screen = "screen_ftl",
        tutname = "%$bgo.etc.tut5%",
        picturename = "ftl",
        type = HelpScreenType.FTLJump
      },
      new HelpScreen()
      {
        screen = "screen_mining",
        tutname = "%$bgo.etc.tut6%",
        picturename = "mining",
        type = HelpScreenType.Mining
      },
      new HelpScreen()
      {
        screen = "screen_storymissions",
        tutname = "%$bgo.etc.tut7%",
        picturename = "storymissions",
        type = HelpScreenType.StoryMissions
      },
      new HelpScreen()
      {
        screen = "screen_industrialmining",
        tutname = "%$bgo.etc.tut8%",
        picturename = "stationarymining",
        type = HelpScreenType.IndustrialMining
      },
      new HelpScreen()
      {
        screen = "screen_duties",
        tutname = "%$bgo.etc.tut9%",
        picturename = "duties",
        type = HelpScreenType.Duties
      },
      new HelpScreen()
      {
        screen = "screen_buynewship",
        tutname = "%$bgo.etc.tut10%",
        picturename = "buynewship",
        type = HelpScreenType.BuyingNewShip
      },
      new HelpScreen()
      {
        screen = "screen_attack",
        tutname = "%$bgo.etc.tut11%",
        picturename = "attack",
        type = HelpScreenType.Attacking
      },
      new HelpScreen()
      {
        screen = "screen_shop",
        tutname = "%$bgo.etc.tut12%",
        picturename = "shop",
        type = HelpScreenType.Shop
      },
      new HelpScreen()
      {
        screen = "screen_repair",
        tutname = "%$bgo.etc.tut13%",
        picturename = "repair",
        type = HelpScreenType.Repair
      },
      new HelpScreen()
      {
        screen = "screen_upgradingsystems",
        tutname = "%$bgo.etc.tut14%",
        picturename = "upgradesystem",
        type = HelpScreenType.UpgradeSystems
      },
      new HelpScreen()
      {
        screen = "screen_buyingcubits",
        tutname = "%$bgo.etc.tut15%",
        picturename = "buycubits",
        type = HelpScreenType.BuyingCubits
      },
      new HelpScreen()
      {
        screen = "screen_dradiscontact",
        tutname = "%$bgo.etc.tut16%",
        picturename = "dradiscontact",
        type = HelpScreenType.DradisContact
      },
      new HelpScreen()
      {
        screen = "screen_ftlmission",
        tutname = "%$bgo.etc.tut17%",
        picturename = "ftlmission",
        type = HelpScreenType.FTLMission
      }
    };
  }

  public void AddButtons()
  {
    GameObject prefab = (GameObject) Resources.Load("GUI/gui_2013/ui_elements/Help/HelpButton");
    this.panel = this.helpListObject.GetComponent<TabPanelWidget>();
    this.panel.sortByName = false;
    if (!((Object) null != (Object) prefab))
      return;
    int num = 0;
    using (List<HelpScreen>.Enumerator enumerator = this.screens.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HelpScreen current = enumerator.Current;
        prefab.GetComponent<HelpButton>().tabPanelWidget = this.panel;
        HelpButton component = NGUITools.AddChild(this.helpListObject, prefab).GetComponent<HelpButton>();
        component.titleLabel = this.title_L;
        component.mainLabel = this.main_L;
        component.subMainLabel = this.subMain_L;
        component.step1Label = this.step1_L;
        component.step2Label = this.step2_L;
        component.step3Label = this.step3_L;
        component.mainSprite = this.main_S;
        component.step1Sprite = this.step1_S;
        component.step2Sprite = this.step2_S;
        component.step3Sprite = this.step3_S;
        component.parent = this;
        component.title = "%$bgo.HelpScreens.Screens." + current.screen + ".title_label%";
        component.main = "%$bgo.HelpScreens.Screens." + current.screen + ".main_label%";
        component.subMain = "%$bgo.HelpScreens.Screens." + current.screen + ".submain_label%";
        component.step1 = "%$bgo.HelpScreens.Screens." + current.screen + ".step1_label%";
        component.step2 = "%$bgo.HelpScreens.Screens." + current.screen + ".step2_label%";
        component.step3 = "%$bgo.HelpScreens.Screens." + current.screen + ".step3_label%";
        component.spriteMainName = current.picturename + "big";
        component.sprite1Name = current.picturename + "1";
        component.sprite2Name = current.picturename + "2";
        component.sprite3Name = current.picturename + "3";
        component.tutname = current.tutname;
        component.name = num.ToString() + "_";
        component.helpType = current.type;
        component.tabPosition = num;
        ++num;
        this.panel.AddTabButton((TabButtonWidget) component);
      }
    }
  }
}
