﻿// Decompiled with JetBrains decompiler
// Type: TextUtility
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class TextUtility
{
  private static float MAGIC_CONST;
  private static GUIStyle style;
  private static GUIContent content;

  public static Size CalcTextSize(string text, Font font, int fontSize)
  {
    if (TextUtility.content == null || TextUtility.style == null)
      TextUtility.Initialize();
    TextUtility.content.text = text;
    TextUtility.style.font = font;
    TextUtility.style.fontSize = fontSize;
    float minWidth;
    float maxWidth;
    TextUtility.style.CalcMinMaxWidth(TextUtility.content, out minWidth, out maxWidth);
    float width = maxWidth + TextUtility.MAGIC_CONST;
    return new Size((int) width, (int) TextUtility.style.CalcHeight(TextUtility.content, width));
  }

  public static Size CalcTextSize(string text, Font font, float fixedWidth)
  {
    if (TextUtility.content == null || TextUtility.style == null)
      TextUtility.Initialize();
    Size size = new Size();
    TextUtility.content.text = text;
    TextUtility.style.font = font;
    TextUtility.style.wordWrap = true;
    size.width = (int) fixedWidth;
    size.height = (int) TextUtility.style.CalcHeight(TextUtility.content, fixedWidth);
    return size;
  }

  public static float CalcWidth(string text, GUIStyle guiStyle)
  {
    if (TextUtility.content == null || TextUtility.style == null)
      TextUtility.Initialize();
    TextUtility.content.text = text;
    return guiStyle.CalcSize(TextUtility.content).x;
  }

  public static string GetValueFactor(float value)
  {
    return (double) value < 0.0 ? "-" : "+";
  }

  private static void Initialize()
  {
    TextUtility.MAGIC_CONST = 5f;
    TextUtility.style = new GUIStyle();
    TextUtility.content = new GUIContent();
  }
}
