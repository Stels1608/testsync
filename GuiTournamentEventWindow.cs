﻿// Decompiled with JetBrains decompiler
// Type: GuiTournamentEventWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class GuiTournamentEventWindow : GuiPanel
{
  private readonly GuiLabel timeLeftLabel = new GuiLabel(string.Empty, Gui.Options.FontEurostileT, 14);
  private readonly GuiLabel statsLabel = new GuiLabel("Kills: 0", Gui.Options.FontEurostileT, 14);
  private readonly KillSpamPanel killSpamPanel;
  private uint score;
  private uint kills;
  private uint deaths;

  private DateTime EndTime { get; set; }

  public uint Score
  {
    private get
    {
      return this.score;
    }
    set
    {
      this.score = value;
      this.UpdateStatsString();
    }
  }

  public uint Kills
  {
    private get
    {
      return this.kills;
    }
    set
    {
      this.kills = value;
      this.UpdateStatsString();
    }
  }

  public uint Deaths
  {
    private get
    {
      return this.deaths;
    }
    set
    {
      this.deaths = value;
      this.UpdateStatsString();
    }
  }

  public GuiTournamentEventWindow(DateTime endTime)
  {
    this.MouseTransparent = true;
    this.Size = new Vector2(420f, 120f);
    this.killSpamPanel = new KillSpamPanel(new Vector2(this.Size.x, this.Size.y - 20f));
    this.AddChild((GuiElementBase) this.statsLabel, Align.UpLeft);
    this.AddChild((GuiElementBase) this.timeLeftLabel, Align.UpRight);
    this.AddChild((GuiElementBase) new Gui.Timer(1f));
    this.AddChild((GuiElementBase) this.killSpamPanel, Align.DownCenter);
    GuiLabel guiLabel = new GuiLabel("_____________________________________________________________________");
    guiLabel.PositionY = 6f;
    this.AddChild((GuiElementBase) guiLabel);
    this.EndTime = endTime;
  }

  private void UpdateStatsString()
  {
    this.statsLabel.Text = BsgoLocalization.Get("%$bgo.tournament.score%", (object) this.Score);
    GuiLabel guiLabel = this.statsLabel;
    string str = guiLabel.Text + "    " + BsgoLocalization.Get("%$bgo.tournament.kills_deaths%", (object) this.Kills, (object) this.Deaths);
    guiLabel.Text = str;
  }

  public void AddKillSpamMessage(KillSpamMessage message)
  {
    this.killSpamPanel.AddMessage(message);
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    this.Position = new Vector2((float) (Screen.width - 430), 72f);
    if (this.EndTime < Game.TimeSync.ServerDateTime)
      this.timeLeftLabel.Text = BsgoLocalization.Get("%$bgo.tournament.time_left%", (object) Tools.FormatTime(TimeSpan.FromTicks(0L)));
    else
      this.timeLeftLabel.Text = BsgoLocalization.Get("%$bgo.tournament.time_left%", (object) Tools.FormatTime(this.EndTime - Game.TimeSync.ServerDateTime));
  }
}
