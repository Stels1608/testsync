﻿// Decompiled with JetBrains decompiler
// Type: JoystickBinding
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class JoystickBinding : IInputBinding
{
  private readonly Action action;
  private JoystickButtonCode triggerCode;
  private JoystickButtonCode modifierCode;
  private JoystickFlags flags;
  private readonly InputDevice device;
  private byte profileNo;

  public bool IsDualAxis
  {
    get
    {
      if (JoystickSetup.GetJoystickInputType(this.triggerCode) == JoystickInputType.Axis)
        return (this.flags & JoystickFlags.InvertedAxis) == JoystickFlags.IsDualAxis;
      return false;
    }
    set
    {
      if (value && JoystickSetup.GetJoystickInputType(this.triggerCode) == JoystickInputType.Axis)
        this.flags |= JoystickFlags.IsDualAxis;
      else
        this.flags &= ~JoystickFlags.IsDualAxis;
    }
  }

  public bool IsAxisInverted
  {
    get
    {
      if (JoystickSetup.GetJoystickInputType(this.triggerCode) == JoystickInputType.Axis)
        return (this.flags & JoystickFlags.InvertedAxis) == JoystickFlags.InvertedAxis;
      return false;
    }
    set
    {
      if (value && JoystickSetup.GetJoystickInputType(this.triggerCode) == JoystickInputType.Axis)
        this.flags |= JoystickFlags.InvertedAxis;
      else
        this.flags &= ~JoystickFlags.InvertedAxis;
    }
  }

  public bool IsUnbound
  {
    get
    {
      return this.triggerCode == JoystickButtonCode.None;
    }
  }

  public byte ProfileNo
  {
    get
    {
      return this.profileNo;
    }
  }

  public Action Action
  {
    get
    {
      return this.action;
    }
  }

  public InputDevice Device
  {
    get
    {
      return this.device;
    }
  }

  public byte Flags
  {
    get
    {
      return (byte) this.flags;
    }
  }

  public JoystickButtonCode ModifierCode
  {
    get
    {
      return this.modifierCode;
    }
  }

  public JoystickButtonCode TriggerCode
  {
    get
    {
      return this.triggerCode;
    }
  }

  public ushort DeviceTriggerCode
  {
    get
    {
      return (ushort) this.TriggerCode;
    }
  }

  public ushort DeviceModifierCode
  {
    get
    {
      return (ushort) this.ModifierCode;
    }
  }

  public string TriggerAlias
  {
    get
    {
      return this.GetCodeAlias(this.TriggerCode);
    }
  }

  public string ModifierAlias
  {
    get
    {
      return this.GetCodeAlias(this.ModifierCode);
    }
  }

  public string BindingAlias
  {
    get
    {
      return (this.ModifierString + this.TriggerAlias + this.IsInvertedString).Replace("_Positive", "+").Replace("_Negative", "-").Replace("Axis", Tools.ParseMessage("%$bgo.ui.options.popup.keybinding.joystick.axis%"));
    }
  }

  private string ModifierString
  {
    get
    {
      if (this.ModifierCode != JoystickButtonCode.None)
        return this.ModifierCode.ToString() + "  +  ";
      return string.Empty;
    }
  }

  private string IsInvertedString
  {
    get
    {
      if (this.IsAxisInverted)
        return " [" + BsgoLocalization.Get("%$bgo.keybindings.joystick_axis_inverted_abbreviation%") + "]";
      return string.Empty;
    }
  }

  private bool IsBoundSingleTrigger
  {
    get
    {
      if (this.TriggerCode != JoystickButtonCode.None)
        return this.ModifierCode == JoystickButtonCode.None;
      return false;
    }
  }

  public JoystickBinding(JoystickBinding source)
    : this(source.action, source.profileNo, source.modifierCode, source.triggerCode, source.flags)
  {
  }

  public JoystickBinding(Action action, byte profileNo, JoystickButtonCode modifierCode, JoystickButtonCode triggerCode)
    : this(action, profileNo, modifierCode, triggerCode, JoystickFlags.None)
  {
  }

  public JoystickBinding(Action action, byte profileNo, JoystickButtonCode modifierCode, JoystickButtonCode triggerCode, JoystickFlags flags)
  {
    this.triggerCode = triggerCode;
    this.action = action;
    this.modifierCode = modifierCode;
    this.device = InputDevice.Joystick;
    this.flags = flags;
    this.profileNo = profileNo;
  }

  public void Unmap()
  {
    this.triggerCode = JoystickButtonCode.None;
    this.modifierCode = JoystickButtonCode.None;
    this.IsAxisInverted = false;
  }

  public bool CombinationCollidesWith(IInputBinding otherInputBinding)
  {
    if (this.Device != otherInputBinding.Device)
      return false;
    JoystickBinding joystickBinding = (JoystickBinding) otherInputBinding;
    if (joystickBinding.IsUnbound)
      return false;
    if (this.IsBoundByCombinationOf(otherInputBinding) || this.IsBoundSingleTrigger && this.TriggerCode == joystickBinding.ModifierCode || joystickBinding.IsBoundSingleTrigger && this.ModifierCode == joystickBinding.TriggerCode)
      return true;
    if (!JoystickSetup.IsDualAxisAction(otherInputBinding.Action))
      return false;
    if (joystickBinding.TriggerCode != this.TriggerCode)
      return JoystickSetup.GetPartnerAxis(joystickBinding.TriggerCode) == this.TriggerCode;
    return true;
  }

  public bool IsBoundByTrigger(InputDevice device, ushort deviceTriggerCode)
  {
    if ((int) this.DeviceTriggerCode == (int) deviceTriggerCode)
      return this.device == device;
    return false;
  }

  public bool IsBoundByCombinationOf(IInputBinding otherInputBinding)
  {
    return this.IsBoundByCombination(otherInputBinding.Device, otherInputBinding.DeviceTriggerCode, otherInputBinding.DeviceModifierCode);
  }

  public bool IsBoundByCombination(InputDevice device, ushort deviceTriggerCode, ushort deviceModifierCode)
  {
    if (this.IsUnbound)
      return false;
    JoystickButtonCode otherJoystickButton = (JoystickButtonCode) deviceTriggerCode;
    if (otherJoystickButton != JoystickButtonCode.None && (this.triggerCode == otherJoystickButton || this.DetectDualAxisCollisionWith(otherJoystickButton)) && (int) this.DeviceModifierCode == (int) deviceModifierCode)
      return this.device == device;
    return false;
  }

  private bool DetectDualAxisCollisionWith(JoystickButtonCode otherJoystickButton)
  {
    if (JoystickSetup.GetJoystickInputType(otherJoystickButton) != JoystickInputType.Axis || !JoystickSetup.IsDualAxisAction(this.action))
      return false;
    return otherJoystickButton == JoystickSetup.GetPartnerAxis(this.triggerCode);
  }

  public bool IsBoundByModifier(InputDevice device, ushort deviceModifierCode)
  {
    if ((int) this.DeviceModifierCode == (int) deviceModifierCode)
      return this.device == device;
    return false;
  }

  public IInputBinding Copy()
  {
    return (IInputBinding) new JoystickBinding(this);
  }

  public void SetTriggerCode(JoystickButtonCode newTriggerCode)
  {
    this.triggerCode = newTriggerCode;
  }

  public void SetModifierCode(JoystickButtonCode newModifierCode)
  {
    this.modifierCode = newModifierCode;
  }

  private string GetCodeAlias(JoystickButtonCode code)
  {
    if (code == JoystickButtonCode.None)
      return BsgoLocalization.Get("%$bgo.ui.options.popup.keybinding.key.unmapped%");
    return code.ToString();
  }

  public override string ToString()
  {
    return string.Format("Action: {0}, TriggerCode: {1}, ModifierCode: {2}, Flags: {3}, Device: {4}, ProfileNo: {5}", (object) this.action, (object) this.triggerCode, (object) this.modifierCode, (object) this.flags, (object) this.device, (object) this.profileNo);
  }
}
