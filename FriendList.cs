﻿// Decompiled with JetBrains decompiler
// Type: FriendList
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class FriendList : GuiPanelVerticalScroll
{
  public FriendList()
  {
    this.AutoSizeChildren = true;
    this.RenderScrollLines = false;
  }

  public void UpdateList(Friends friends)
  {
    for (int index = 0; index < friends.Players.Count; ++index)
    {
      Player player = friends.Players[index];
      FriendListItem itemWithPlayer = this.FindItemWithPlayer(player);
      if (itemWithPlayer != null)
        itemWithPlayer.player = player;
      else
        this.AddChild((GuiElementBase) new FriendListItem(player));
    }
    foreach (GuiElementBase child in this.Children)
    {
      if (!this.CurrentFriend(friends, (child as FriendListItem).player))
        this.RemoveChildLater(child);
    }
  }

  private FriendListItem FindItemWithPlayer(Player player)
  {
    foreach (GuiElementBase child in this.Children)
    {
      if ((int) (child as FriendListItem).player.ServerID == (int) player.ServerID)
        return child as FriendListItem;
    }
    return (FriendListItem) null;
  }

  private bool CurrentFriend(Friends friends, Player player)
  {
    for (int index = 0; index < friends.Players.Count; ++index)
    {
      if ((int) friends.Players[index].ServerID == (int) player.ServerID)
        return true;
    }
    return false;
  }
}
