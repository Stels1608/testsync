﻿// Decompiled with JetBrains decompiler
// Type: WanderingAICharacterController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WanderingAICharacterController : MonoBehaviour
{
  public float idleThreshold = 0.1f;
  public bool onlyWalkForward;
  private CharacterMotor motor;
  private float moveDirection;
  private float faceDirection;
  private float acceleration;
  private float moveSpeed;
  private float turnSpeed;
  private float faceSpeed;

  private void Start()
  {
    this.motor = this.GetComponent(typeof (CharacterMotor)) as CharacterMotor;
  }

  private void Update()
  {
    if ((Object) this.motor == (Object) null)
      return;
    this.turnSpeed += (float) (((double) Random.value - 0.5) * (double) Time.deltaTime * 5.0);
    this.faceSpeed += (float) (((double) Random.value - 0.5) * (double) Time.deltaTime * 5.0);
    this.turnSpeed = Mathf.Clamp(this.turnSpeed, -1f, 1f) * Mathf.Pow(0.5f, Time.deltaTime);
    this.faceSpeed = Mathf.Clamp(this.faceSpeed, -1f, 1f) * Mathf.Pow(0.5f, Time.deltaTime);
    this.moveDirection += this.turnSpeed * Time.deltaTime;
    this.faceDirection += this.faceSpeed * Time.deltaTime;
    this.moveDirection = Util.Mod(this.moveDirection);
    this.faceDirection = Util.Mod(this.faceDirection);
    this.acceleration += (float) (((double) Random.value - 0.5) * (double) Time.deltaTime / 10.0);
    this.acceleration = Mathf.Clamp(this.acceleration, -1f, 1f);
    this.moveSpeed += this.acceleration;
    this.moveSpeed = Mathf.Clamp(this.moveSpeed, 0.0f, 1f);
    if ((double) this.acceleration < 0.0 && (double) this.moveSpeed == 0.0)
      this.acceleration = 0.0f;
    if ((double) this.acceleration > 0.0 && (double) this.moveSpeed == 1.0)
      this.acceleration = 0.0f;
    if ((double) Time.time < 5.0)
    {
      this.moveDirection = 0.0f;
      this.moveSpeed = 1f;
    }
    Vector3 vector3 = Quaternion.AngleAxis(this.moveDirection * 360f, Vector3.up) * Vector3.forward * this.moveSpeed;
    Vector3 normalized = (Quaternion.AngleAxis(this.faceDirection * 360f, Vector3.up) * Vector3.forward + vector3 * 0.5f).normalized;
    if (this.onlyWalkForward)
      normalized = vector3.normalized;
    float magnitude = vector3.magnitude;
    this.motor.desiredFacingDirection = normalized;
    if ((double) magnitude < (double) this.idleThreshold)
    {
      this.motor.desiredMovementDirection = Vector3.zero;
      if (!this.onlyWalkForward)
        return;
      this.motor.desiredFacingDirection = Vector3.zero;
    }
    else
      this.motor.desiredMovementDirection = Quaternion.Inverse(this.transform.rotation) * vector3 / magnitude * (float) (((double) magnitude - (double) this.idleThreshold) / (1.0 - (double) this.idleThreshold));
  }
}
