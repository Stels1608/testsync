﻿// Decompiled with JetBrains decompiler
// Type: LeaderboardToggle
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof (Image), typeof (Button))]
public abstract class LeaderboardToggle : MonoBehaviour
{
  private bool selected;
  protected Image image;
  private Button button;
  protected Text label;

  public UnityEngine.Sprite SelectedGfx { get; protected set; }

  public UnityEngine.Sprite NotSelectedGfx { get; protected set; }

  public List<LeaderboardToggle> ToggleGroup { get; set; }

  public bool Selected
  {
    get
    {
      return this.selected;
    }
    set
    {
      this.selected = value;
      this.OnSelectedChanged(value);
      if (this.ToggleGroup == null)
        return;
      using (List<LeaderboardToggle>.Enumerator enumerator = this.ToggleGroup.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          LeaderboardToggle current = enumerator.Current;
          if ((Object) current != (Object) this)
          {
            current.selected = false;
            current.OnSelectedChanged(false);
          }
        }
      }
    }
  }

  private void Awake()
  {
    this.image = this.GetComponent<Image>();
    this.button = this.GetComponent<Button>();
    this.label = this.GetComponentInChildren<Text>();
    this.button.onClick.AddListener(new UnityAction(this.OnClicked));
    this.OnAwake();
  }

  protected abstract void OnAwake();

  protected virtual void OnSelectedChanged(bool newState)
  {
    this.image.sprite = !newState ? this.NotSelectedGfx : this.SelectedGfx;
  }

  private void OnClicked()
  {
    if (this.Selected)
      return;
    this.Selected = true;
  }

  public void Trigger()
  {
    ExecuteEvents.Execute<ISubmitHandler>(this.button.gameObject, (BaseEventData) new PointerEventData(EventSystem.current), ExecuteEvents.submitHandler);
  }
}
