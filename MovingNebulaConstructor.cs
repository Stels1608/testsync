﻿// Decompiled with JetBrains decompiler
// Type: MovingNebulaConstructor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MovingNebulaConstructor : NonLODObjectConstructor
{
  public static readonly string goName = "movingNebula";
  protected MovingNebulaDesc desc;

  public MovingNebulaConstructor(MovingNebulaDesc desc)
    : base(new GameObject(MovingNebulaConstructor.goName))
  {
    if (desc.modelName.Contains("_"))
      desc.modelName = desc.modelName.Substring(0, desc.modelName.LastIndexOf("_"));
    this.desc = desc;
  }

  public override void Construct()
  {
    base.Construct();
    this.root.transform.rotation = this.desc.rotation;
    this.root.transform.position = this.desc.position;
    this.root.AddComponent<MovingNebula>();
    this.root.AddComponent<CustomSupply>().Action = (System.Action<GameObject>) (obj =>
    {
      foreach (MeshRenderer componentsInChild in obj.GetComponentsInChildren<MeshRenderer>())
      {
        componentsInChild.gameObject.layer = 8;
        componentsInChild.material.SetColor("_Color", this.desc.color);
        componentsInChild.material.SetTextureOffset("_MainTex", this.desc.textureOffset);
        componentsInChild.material.SetTextureScale("_MainTex", this.desc.textureScale);
      }
    });
  }

  protected override string GetPrefabName()
  {
    return this.desc.modelName + "_" + this.desc.matSuffix + ".prefab";
  }
}
