﻿// Decompiled with JetBrains decompiler
// Type: IndentWriter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Text;

public class IndentWriter
{
  private StringBuilder _sb = new StringBuilder();
  private int _indent;

  public void Indent()
  {
    ++this._indent;
  }

  public void UnIndent()
  {
    if (this._indent <= 0)
      return;
    --this._indent;
  }

  public void WriteLine(string line)
  {
    this._sb.AppendLine(this.CreateIndent() + line);
  }

  private string CreateIndent()
  {
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < this._indent; ++index)
      stringBuilder.Append("    ");
    return stringBuilder.ToString();
  }

  public override string ToString()
  {
    return this._sb.ToString();
  }
}
