﻿// Decompiled with JetBrains decompiler
// Type: ChatLoggedIn
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

internal class ChatLoggedIn : ChatProtocolParser
{
  public int userId;

  public ChatLoggedIn()
  {
    this.type = ChatProtocolParserType.LoginOk;
  }

  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split(new char[2]{ '%', '#' });
    return strArray.Length == 3 && !(strArray[0] != "bv") && int.TryParse(strArray[1], out this.userId);
  }
}
