﻿// Decompiled with JetBrains decompiler
// Type: HelperUtil
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class HelperUtil
{
  public static readonly char[] s_ExcelSplitters = new char[3]{ '$', '/', '\\' };
  public static readonly Regex StripSpaces = new Regex("\\s+");
  private static bool ms_isRunningUnity34 = false;
  private static bool ms_hasCheckedRunningUnity34 = false;
  private const float EPSILON = 0.01f;

  public static bool IsRunningUnity34()
  {
    if (!HelperUtil.ms_hasCheckedRunningUnity34)
    {
      HelperUtil.ms_isRunningUnity34 = Application.unityVersion.StartsWith("3.4");
      HelperUtil.ms_hasCheckedRunningUnity34 = true;
    }
    return HelperUtil.ms_isRunningUnity34;
  }

  public static float GetAngleBetweenVectors(Vector3 fromVector, Vector3 toVector)
  {
    if ((double) fromVector.sqrMagnitude < 0.00999999977648258 || (double) toVector.sqrMagnitude < 0.00999999977648258)
      return 0.0f;
    float num = Vector3.Angle(fromVector, toVector);
    if ((double) Vector3.Cross(fromVector, toVector).y > 0.0)
      num *= -1f;
    return num;
  }

  public static float GetAngleAlongVector(Vector3 vec)
  {
    if ((double) vec.sqrMagnitude < 0.01)
      return 0.0f;
    return 57.29578f * Mathf.Atan2(vec.x, vec.z);
  }

  public static bool AlmostEquals(Vector3 v1, Vector3 v2, float e)
  {
    return (double) Mathf.Abs(v1.x - v2.x) <= (double) e && (double) Mathf.Abs(v1.y - v2.y) <= (double) e && (double) Mathf.Abs(v1.z - v2.z) <= (double) e;
  }

  public static float GetLookAtAngle(Vector3 eye, Vector3 center)
  {
    return Mathf.Atan2(center.x - eye.x, center.z - eye.z) * 57.29578f;
  }

  public static Vector3 ProjectOntoPlane(Vector3 v, Vector3 up)
  {
    return v - Vector3.Project(v, up);
  }

  public static Vector3 GetForceFromCollision(Collider receiver, Collider other, float defaultForce)
  {
    bool flag = true;
    Vector3 vector3_1 = new Vector3(0.0f, 0.0f, 0.0f);
    Rigidbody component1 = receiver.GetComponent<Rigidbody>();
    CharacterController component2 = receiver.GetComponent<CharacterController>();
    if ((bool) ((UnityEngine.Object) component1))
    {
      vector3_1 = component1.velocity * component1.mass;
      flag = false;
    }
    if ((bool) ((UnityEngine.Object) component2))
    {
      vector3_1 = component2.velocity;
      flag = false;
    }
    Vector3 vector3_2 = new Vector3(0.0f, 0.0f, 0.0f);
    Rigidbody component3 = other.GetComponent<Rigidbody>();
    CharacterController component4 = other.GetComponent<CharacterController>();
    if ((bool) ((UnityEngine.Object) component3))
    {
      vector3_2 = component3.velocity * component3.mass;
      flag = false;
    }
    if ((bool) ((UnityEngine.Object) component4))
    {
      vector3_2 = component4.velocity;
      flag = false;
    }
    return !flag ? (vector3_2 - vector3_1) * defaultForce : (receiver.transform.position - other.transform.position).normalized * defaultForce;
  }

  public static Vector3 GetRandomVector()
  {
    return new Vector3((float) ((double) UnityEngine.Random.value * 2.0 - 1.0), (float) ((double) UnityEngine.Random.value * 2.0 - 1.0), (float) ((double) UnityEngine.Random.value * 2.0 - 1.0));
  }

  public static GameObject NewGameObjectAtOrigin(string objName, Transform parentTransform)
  {
    GameObject gameObject = new GameObject(objName);
    if ((UnityEngine.Object) parentTransform == (UnityEngine.Object) null)
    {
      gameObject.transform.position = Vector3.zero;
      gameObject.transform.rotation = Quaternion.identity;
      gameObject.transform.localScale = Vector3.one;
    }
    else
      HelperUtil.AttachGameObjectToTransform(gameObject, parentTransform);
    return gameObject;
  }

  public static void AttachGameObjectToTransform(GameObject obj, Transform parentTransform)
  {
    obj.transform.parent = parentTransform;
    obj.transform.localPosition = Vector3.zero;
    obj.transform.localRotation = Quaternion.identity;
    obj.transform.localScale = Vector3.one;
  }

  public static T GetInterface<T>(GameObject obj) where T : class
  {
    if ((UnityEngine.Object) obj != (UnityEngine.Object) null)
    {
      Component[] components = obj.GetComponents<Component>();
      for (int index = 0; index < components.Length; ++index)
      {
        if (components[index] is T)
          return (object) components[index] as T;
      }
    }
    return (T) null;
  }

  public static T GetInterfaceInChildren<T>(GameObject obj) where T : class
  {
    if ((UnityEngine.Object) obj != (UnityEngine.Object) null)
    {
      Stack<GameObject> gameObjectStack = new Stack<GameObject>();
      gameObjectStack.Push(obj);
      while (gameObjectStack.Count > 0)
      {
        GameObject gameObject = gameObjectStack.Pop();
        T @interface = HelperUtil.GetInterface<T>(gameObject);
        if ((object) @interface != null)
          return @interface;
        Transform transform = gameObject.transform;
        for (int index = 0; index < transform.childCount; ++index)
          gameObjectStack.Push(transform.GetChild(index).gameObject);
      }
    }
    return (T) null;
  }

  public static T[] GetInterfacesInChildren<T>(GameObject obj) where T : class
  {
    List<T> objList = new List<T>();
    if ((UnityEngine.Object) obj != (UnityEngine.Object) null)
    {
      Stack<GameObject> gameObjectStack = new Stack<GameObject>();
      gameObjectStack.Push(obj);
      while (gameObjectStack.Count > 0)
      {
        GameObject gameObject = gameObjectStack.Pop();
        T[] interfaces = HelperUtil.GetInterfaces<T>(gameObject);
        if (interfaces != null)
          objList.AddRange((IEnumerable<T>) interfaces);
        Transform transform = gameObject.transform;
        for (int index = 0; index < transform.childCount; ++index)
          gameObjectStack.Push(transform.GetChild(index).gameObject);
      }
    }
    return objList.ToArray();
  }

  public static T[] GetInterfaces<T>(GameObject obj) where T : class
  {
    Component[] components = obj.GetComponents<Component>();
    List<T> objList = new List<T>();
    foreach (Component component in components)
    {
      if (component is T)
        objList.Add((object) component as T);
    }
    return objList.ToArray();
  }

  public static List<T> FindComponentsInChildrenRecursively<T>(GameObject gameObj) where T : class
  {
    List<T> collectedComponentList = new List<T>();
    HelperUtil.FindComponentsInChildrenRecursivelyHelper<T>(gameObj, collectedComponentList);
    return collectedComponentList;
  }

  private static void FindComponentsInChildrenRecursivelyHelper<T>(GameObject gameObj, List<T> collectedComponentList) where T : class
  {
    foreach (Component componentsInChild in gameObj.GetComponentsInChildren(typeof (T)))
    {
      T obj = (object) componentsInChild as T;
      if (!collectedComponentList.Contains(obj))
      {
        collectedComponentList.Add(obj);
        HelperUtil.FindComponentsInChildrenRecursivelyHelper<T>(componentsInChild.gameObject, collectedComponentList);
      }
    }
  }

  public static GameObject FindGameObjectInChildren(GameObject obj, string name)
  {
    return HelperUtil.FindGameObjectInChildren(obj, name, false);
  }

  public static GameObject FindGameObjectInChildren(GameObject obj, string name, bool prefix)
  {
    GameObject gameObject = (GameObject) null;
    int childCount = obj.transform.childCount;
    for (int index = 0; index < childCount; ++index)
    {
      Transform child = obj.transform.GetChild(index);
      bool flag = false;
      if (child.name == name)
        flag = true;
      else if (prefix && HelperUtil.FindSubstring(child.name, name))
        flag = true;
      if (flag)
      {
        gameObject = child.gameObject;
        break;
      }
      if (child.childCount > 0)
      {
        gameObject = HelperUtil.FindGameObjectInChildren(child.gameObject, name, prefix);
        if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
          break;
      }
    }
    return gameObject;
  }

  public static Transform FindTransformInChildren(Transform t, string name)
  {
    return HelperUtil.FindTransformInChildren(t, name, false);
  }

  public static Transform FindTransformInChildren(Transform t, string name, bool fuzzy)
  {
    GameObject objectInChildren = HelperUtil.FindGameObjectInChildren(t.gameObject, name, fuzzy);
    if ((UnityEngine.Object) objectInChildren != (UnityEngine.Object) null)
      return objectInChildren.transform;
    return (Transform) null;
  }

  public static bool FindSubstring(string string1, string string2)
  {
    int length1 = string1.Length;
    int length2 = string2.Length;
    if (length1 < length2)
      return false;
    if (length1 == length2)
      return string1 == string2;
    bool flag = false;
    for (int index1 = 0; index1 < length1 - length2 + 1; ++index1)
    {
      flag = true;
      for (int index2 = 0; index2 < length2; ++index2)
      {
        if ((int) string1[index1 + index2] != (int) string2[index2])
        {
          flag = false;
          break;
        }
      }
      if (flag)
        break;
    }
    return flag;
  }

  public static void SwapAndRemoveAtEnd<T>(List<T> theList, int indexToRemove)
  {
    if (indexToRemove < 0 || indexToRemove >= theList.Count)
      return;
    if (theList.Count - 1 != indexToRemove)
      theList[indexToRemove] = theList[theList.Count - 1];
    theList.RemoveAt(theList.Count - 1);
  }

  public static void RemoveNull<T>(List<T> theList) where T : class
  {
    for (int indexToRemove = theList.Count - 1; indexToRemove >= 0; --indexToRemove)
    {
      if ((object) theList[indexToRemove] == null || theList[indexToRemove].Equals((object) null))
        HelperUtil.SwapAndRemoveAtEnd<T>(theList, indexToRemove);
    }
  }

  public static void RemoveRepeats<T>(List<T> theList)
  {
    for (int index = 0; index < theList.Count; ++index)
    {
      for (int indexToRemove = index + 1; indexToRemove < theList.Count; ++indexToRemove)
      {
        if (EqualityComparer<T>.Default.Equals(theList[index], theList[indexToRemove]))
        {
          HelperUtil.SwapAndRemoveAtEnd<T>(theList, indexToRemove);
          --indexToRemove;
        }
      }
    }
  }

  public static void RemoveNullAndRepeats<T>(List<T> theList) where T : class
  {
    HelperUtil.RemoveNull<T>(theList);
    HelperUtil.RemoveRepeats<T>(theList);
  }

  public static void MultiJitter(ref Vector2[] samples)
  {
    int num1 = (int) Mathf.Sqrt((float) samples.Length);
    float num2 = 1f / (float) samples.Length;
    for (int index1 = 0; index1 < num1; ++index1)
    {
      for (int index2 = 0; index2 < num1; ++index2)
      {
        samples[index1 * num1 + index2].x = (float) ((double) (index1 * num1) * (double) num2 + (double) index2 * (double) num2 + (double) UnityEngine.Random.value * (double) num2);
        samples[index1 * num1 + index2].y = (float) ((double) (index2 * num1) * (double) num2 + (double) index1 * (double) num2 + (double) UnityEngine.Random.value * (double) num2);
      }
    }
    for (int index1 = 0; index1 < num1; ++index1)
    {
      for (int index2 = 0; index2 < num1; ++index2)
      {
        int num3 = index2 + (int) ((double) UnityEngine.Random.value * (double) (num1 - index2 - 1));
        float num4 = samples[index1 * num1 + index2].x;
        samples[index1 * num1 + index2].x = samples[index1 * num1 + num3].x;
        samples[index1 * num1 + num3].x = num4;
        int num5 = index2 + (int) ((double) UnityEngine.Random.value * (double) (num1 - index2 - 1));
        float num6 = samples[index2 * num1 + index1].y;
        samples[index2 * num1 + index1].y = samples[num5 * num1 + index1].y;
        samples[num5 * num1 + index1].y = num6;
      }
    }
  }

  public static void DrawAnimInfo(float x, float y, Animation animation, string clipName)
  {
    float num = (float) ((int) ((double) animation[clipName].time * 100.0) % (int) ((double) animation[clipName].clip.length * 100.0));
    GUI.Label(new Rect(x, y, 100f, 50f), clipName + ": ");
    GUI.Label(new Rect(x + 100f, y, 100f, 50f), "  playing: " + (object) animation.IsPlaying(clipName));
    GUI.Label(new Rect(x + 200f, y, 120f, 50f), "  spd: " + (object) animation[clipName].speed);
    GUI.Label(new Rect(x + 320f, y, 120f, 50f), "  wt: " + (object) animation[clipName].weight);
    GUI.Label(new Rect(x + 440f, y, 150f, 50f), "  t: " + (object) animation[clipName].time + "/" + (object) animation[clipName].length);
    GUI.Label(new Rect(x + 590f, y, 150f, 50f), "  pct: " + (object) num);
  }

  public static List<T> GetListAllocateIfNeeded<S, T>(Dictionary<S, List<T>> map, S key)
  {
    if (map.ContainsKey(key))
      return map[key];
    List<T> objList = new List<T>();
    map[key] = objList;
    return objList;
  }

  public static HashSet<T> GetHashSetAllocateIfNeeded<S, T>(Dictionary<S, HashSet<T>> map, S key)
  {
    if (map.ContainsKey(key))
      return map[key];
    HashSet<T> objSet = new HashSet<T>();
    map[key] = objSet;
    return objSet;
  }

  public static bool IsBadNumber(float t)
  {
    if (!float.IsInfinity(t) && !float.IsNaN(t) && !float.IsNegativeInfinity(t))
      return float.IsPositiveInfinity(t);
    return true;
  }

  public static void SetLayerRecursive(GameObject obj, string name)
  {
    HelperUtil.SetLayerRecursive(obj, LayerMask.NameToLayer(name));
  }

  public static void SetLayerRecursive(GameObject obj, int layer)
  {
    obj.layer = layer;
    Transform transform = obj.transform;
    int childCount = transform.childCount;
    for (int index = 0; index < childCount; ++index)
      HelperUtil.SetLayerRecursive(transform.GetChild(index).gameObject, layer);
  }

  private static string RecurseUpPath(GameObject gameObj)
  {
    if ((UnityEngine.Object) gameObj.transform.parent == (UnityEngine.Object) null)
      return gameObj.name;
    return HelperUtil.RecurseUpPath(gameObj.transform.parent.gameObject) + "/" + gameObj.name;
  }

  public static string GetGameObjectPath(GameObject gameObj)
  {
    return HelperUtil.RecurseUpPath(gameObj);
  }

  public static GameObject ConvertObjectToGameObject(object obj)
  {
    if (!(obj is UnityEngine.Object))
      return (GameObject) null;
    if (obj is GameObject)
      return obj as GameObject;
    if (obj is Component)
      return (obj as Component).gameObject;
    return (GameObject) null;
  }

  public static Vector3 GetGroundPosition(CharacterController cc, Vector3 pos, float skin_width)
  {
    Vector3 ground_pos;
    if (!HelperUtil.TryGetGroundPosition(cc, pos, skin_width, out ground_pos))
      BPDebug.LogError((object) ("Unable to find ground position below " + (object) pos + " for " + (object) cc.gameObject));
    return ground_pos;
  }

  public static bool TryGetGroundPosition(CharacterController cc, Vector3 pos, float skin_width, out Vector3 ground_pos)
  {
    if ((UnityEngine.Object) cc != (UnityEngine.Object) null)
    {
      float radius = cc.radius;
      Vector3 origin = pos;
      origin.y += radius;
      RaycastHit hitInfo;
      if (Physics.SphereCast(origin, radius, Vector3.down, out hitInfo))
      {
        pos.y += -hitInfo.distance + skin_width;
        ground_pos = pos;
        return true;
      }
    }
    ground_pos = pos;
    return false;
  }

  public static string[] ExtractExcelNames(string to_parse)
  {
    if (to_parse == null)
      return (string[]) null;
    to_parse = HelperUtil.StripSpaces.Replace(to_parse, string.Empty);
    return to_parse.Split(HelperUtil.s_ExcelSplitters, StringSplitOptions.RemoveEmptyEntries);
  }

  public static void TurnEmmittersOn(GameObject fx_obj, bool emit)
  {
    if (!((UnityEngine.Object) fx_obj != (UnityEngine.Object) null))
      return;
    foreach (ParticleEmitter componentsInChild in fx_obj.GetComponentsInChildren<ParticleEmitter>())
      componentsInChild.emit = emit;
    foreach (ParticleAnimator componentsInChild in fx_obj.GetComponentsInChildren<ParticleAnimator>())
      componentsInChild.autodestruct = false;
  }

  public static string MeasureTimeFormat(float secs)
  {
    int num1 = (int) ((double) secs / 3600.0);
    int num2 = (int) (((double) secs - (double) (num1 * 3600)) / 60.0);
    int num3 = (int) ((double) secs - (double) (num2 * 60) - (double) (num1 * 3600));
    string str = string.Empty;
    if (num1 > 0)
      str = str + (object) num1 + "H";
    if (num2 > 0)
      str = str + (object) num2 + "'";
    if (num3 > 0 || str.Length == 0)
      str = str + (object) num3 + "\"";
    return str;
  }

  public class EvenDistribution
  {
    private Vector2[] m_distribution;
    private int m_index;

    public Vector2 NextValue
    {
      get
      {
        if (this.m_distribution.Length <= 0)
          return Vector2.zero;
        if (this.m_index >= this.m_distribution.Length)
        {
          this.m_index = 0;
          HelperUtil.MultiJitter(ref this.m_distribution);
          float num = (float) this.m_distribution.Length - 1f;
          for (int index1 = 0; index1 < this.m_distribution.Length; ++index1)
          {
            int index2 = (int) ((double) UnityEngine.Random.value * (double) num);
            Vector2 vector2 = this.m_distribution[index1];
            this.m_distribution[index1] = this.m_distribution[index2];
            this.m_distribution[index2] = vector2;
          }
        }
        ++this.m_index;
        return this.m_distribution[this.m_index - 1];
      }
    }

    public EvenDistribution(int suggested_size)
    {
      if (suggested_size < 9)
        suggested_size = 9;
      int @int = Mathf.CeilToInt(Mathf.Sqrt((float) suggested_size));
      this.m_distribution = new Vector2[@int * @int];
      this.m_index = this.m_distribution.Length;
    }
  }

  [Serializable]
  public class ExcelSettings
  {
    public string tab = string.Empty;
    public string section = string.Empty;
    public string row = string.Empty;

    public ExcelSettings(string t, string s, string r)
    {
      this.tab = t;
      this.section = s;
      this.row = r;
    }

    public override string ToString()
    {
      return "$" + this.tab + "/" + this.section + "/" + this.row;
    }

    public static HelperUtil.ExcelSettings Create(string oneLineReference)
    {
      string[] excelNames = HelperUtil.ExtractExcelNames(oneLineReference);
      if (excelNames.Length == 3)
        return new HelperUtil.ExcelSettings(excelNames[0], excelNames[1], excelNames[2]);
      return (HelperUtil.ExcelSettings) null;
    }
  }
}
