﻿// Decompiled with JetBrains decompiler
// Type: RadioMessageBoxNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class RadioMessageBoxNgui : RadioMessageBoxUi
{
  private string mainText = string.Empty;
  private string hintText = string.Empty;
  [SerializeField]
  private UILabel okButtonLabel;
  [SerializeField]
  private UILabel messageLabel;
  [SerializeField]
  private UI2DSprite hintImage;
  [SerializeField]
  private UI2DSprite npcPortrait;
  [SerializeField]
  private UILabel npcNameLabel;
  [SerializeField]
  private UIButton navButtonLeft;
  [SerializeField]
  private UIButton navButtonRight;
  [SerializeField]
  private GameObject pageIndexGroup;
  [SerializeField]
  private UILabel pageIndexLabel;

  protected override void SetOkButtonText(string text)
  {
    this.okButtonLabel.text = text;
  }

  protected override void SetMainText(string mainText)
  {
    this.mainText = mainText;
  }

  protected override void SetHintText(string hintText)
  {
    this.hintText = hintText;
  }

  protected override void SetHintImage2D(Texture2D hintImageTex2D)
  {
    this.hintImage.sprite2D = UnityEngine.Sprite.Create(hintImageTex2D, new Rect(0.0f, 0.0f, (float) hintImageTex2D.width, (float) hintImageTex2D.height), new Vector2(0.5f, 0.5f));
  }

  protected override void ShowHintImage(bool isVisible)
  {
    this.hintImage.enabled = isVisible;
  }

  protected override void ShowOkButton(bool isVisible)
  {
    throw new NotImplementedException();
  }

  protected override void EnableLeftNavigationButton(bool isEnabled)
  {
    this.navButtonLeft.isEnabled = isEnabled;
    this.UpdateNavigationElements();
  }

  protected override void EnableRightNavigationButton(bool isEnabled)
  {
    this.navButtonRight.isEnabled = isEnabled;
    this.UpdateNavigationElements();
  }

  private void UpdateNavigationElements()
  {
    this.pageIndexGroup.gameObject.SetActive(this.navButtonLeft.isEnabled || this.navButtonRight.isEnabled);
  }

  protected override void SetNavigationLabel(string navPageIndex)
  {
    this.pageIndexLabel.text = navPageIndex;
  }

  protected override void SetPilotName(string pilotName)
  {
    this.npcNameLabel.text = pilotName;
  }

  protected override void SetPilotPortrait(Texture2D portrait)
  {
    this.npcPortrait.sprite2D = UnityEngine.Sprite.Create(portrait, new Rect(0.0f, 0.0f, (float) portrait.width, (float) portrait.height), new Vector2(0.5f, 0.5f));
  }

  protected override void DisplayMessageText()
  {
    this.messageLabel.text = this.mainText + "\n\n[" + Tools.ColorToHexRGB((Color32) ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_SPECIAL)) + "]" + this.hintText + "[-]";
  }
}
