﻿// Decompiled with JetBrains decompiler
// Type: FlipManeuver
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class FlipManeuver : Maneuver
{
  public override string ToString()
  {
    return string.Format("FlipManeuver");
  }

  public override MovementFrame NextFrame(Tick tick, MovementFrame prevFrame)
  {
    return MovementFrame.Invalid;
  }

  public override void Read(BgoProtocolReader pr)
  {
    base.Read(pr);
    this.startTick = pr.ReadTick();
    pr.ReadVector3();
    this.options.Read(pr);
  }
}
