﻿// Decompiled with JetBrains decompiler
// Type: FtlTrapBehaviour
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FtlTrapBehaviour : MonoBehaviour
{
  public int TriggerDistance = 300;
  public Animation FtlAni;
  public GameObject Pulse;
  private Transform transformCached;
  private int triggerDistanceSqr;
  private bool closed;
  public GameObject FlickeringLight;
  public Animation LoadingLightAnimation;
  public AudioSource OpenSound;
  public AudioSource CloseSound;

  private void Awake()
  {
    this.transformCached = this.transform;
    this.triggerDistanceSqr = this.TriggerDistance * this.TriggerDistance;
    this.DisableLoadingLight();
  }

  private void Update()
  {
    if (SectorEditorHelper.IsEditorLevel() || Game.Me == null || this.closed || (double) (Game.Me.Ship.Position - this.transformCached.position).sqrMagnitude >= (double) this.triggerDistanceSqr)
      return;
    this.PlayCloseAnimSlowly();
    this.EnablePulsingLoadingLight();
    this.closed = true;
  }

  public void PlayCloseAnimSlowly()
  {
    this.PlayAnim(false, 0.6f);
    this.CloseSound.Play();
  }

  public void PlayOpenAnim()
  {
    this.PlayAnim(true, 1f);
    this.OpenSound.Play();
  }

  public void StartPulseEffect()
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this.Pulse);
    if ((Object) gameObject == (Object) null)
    {
      Debug.LogError((object) "FtlTrapBehaviour:StartPulseEffect(): Pulse is null.");
    }
    else
    {
      gameObject.transform.parent = this.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localRotation = Quaternion.identity;
    }
  }

  private void PlayAnim(bool reverseAnim, float speed = 1)
  {
    if ((Object) this.FtlAni == (Object) null)
    {
      Debug.LogError((object) ("Can't Play Ftl Open Anim. Linking Error in FtlTrap.prefab. FtlAni: " + (object) this.FtlAni));
    }
    else
    {
      AnimationState animationState = this.FtlAni.GetComponent<Animation>()["close"];
      animationState.normalizedTime = !reverseAnim ? 0.0f : 1f;
      animationState.speed = !reverseAnim ? speed : -speed;
      this.FtlAni.Play("close");
    }
  }

  public void EnablePulsingLoadingLight()
  {
    this.LoadingLightAnimation.Play("ftl_trap_pulsing_loading_light");
  }

  public void EnableFullLoadingLight()
  {
    this.LoadingLightAnimation.Play("ftl_trap_enable_full_loading_light");
  }

  public void DisableLoadingLight()
  {
    this.LoadingLightAnimation.Play("ftl_trap_disable_loading_light");
  }
}
