﻿// Decompiled with JetBrains decompiler
// Type: CombatTextMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class CombatTextMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "CombatTextMediator";
  private GuiCombatText view;

  public CombatTextMediator()
    : base("CombatTextMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.LevelLoaded);
    this.AddMessageInterest(Message.ShowCombatText);
    this.AddMessageInterest(Message.SettingChangedCombatText);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.SettingChangedCombatText:
        this.ShowCombatText((bool) message.Data);
        break;
      case Message.LevelLoaded:
        this.CreateView((GameLevel) message.Data);
        this.ShowCombatText(((SettingsDataProvider) this.OwnerFacade.FetchDataProvider("SettingsDataProvider")).CurrentSettings.CombatText);
        break;
      case Message.ShowCombatText:
        this.AddCombatMessage((AbstractCombatMessage) message.Data);
        break;
    }
  }

  private void CreateView(GameLevel level)
  {
    this.view = (GuiCombatText) null;
    if (level.LevelType != GameLevelType.SpaceLevel)
      return;
    this.view = new GuiCombatText();
    Game.GUIManager.AddPanel((IGUIPanel) this.view);
  }

  private void AddCombatMessage(AbstractCombatMessage combatMessage)
  {
    if (this.view == null)
      return;
    this.view.AddMessage(combatMessage);
  }

  private void ShowCombatText(bool show)
  {
    if (this.view == null)
      return;
    this.view.ShowCombatText = show;
  }
}
