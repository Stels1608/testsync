﻿// Decompiled with JetBrains decompiler
// Type: ShipControlsMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class ShipControlsMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  private bool gearIsEnabled = true;
  public new const string NAME = "ShipControlsMediator";

  public ShipControlsMediator()
    : base("ShipControlsMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.DockAtNpcShipRequest);
    this.AddMessageInterest(Message.DockAtPlayerShipReply);
    this.AddMessageInterest(Message.DockAtPlayerShipRequest);
    this.AddMessageInterest(Message.CameraBehaviorChanged);
    this.AddMessageInterest(Message.Death);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.ShipSpeedChangeCall);
    this.AddMessageInterest(Message.ShipSpeedChangeDone);
    this.AddMessageInterest(Message.ShipGearChangeCall);
    this.AddMessageInterest(Message.ShipGearToggleBoostCall);
    this.AddMessageInterest(Message.ShipGearToggleBoostDone);
    this.AddMessageInterest(Message.EnableGear);
    this.AddMessageInterest(Message.ToggleJoystickGamepadEnabled);
    this.AddMessageInterest(Message.ToggleOldUi);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.Death:
        SpaceLevel.GetLevel().ShipControls.ResetKeyStates();
        break;
      case Message.CameraBehaviorChanged:
        if (!((SpaceCameraBehaviorChangeMessage) message).NewBehavior.BlocksControls || Game.Me.Dead || !Game.Me.Ship.SpawnedInSector)
          break;
        SpaceLevel.GetLevel().ShipControls.StopTurning();
        break;
      case Message.ShipSpeedChangeCall:
        this.HandleSpeedChangeCall(message);
        break;
      case Message.ShipSpeedChangeDone:
        this.HandleSpeedChangeResponse(message);
        break;
      case Message.ShipGearToggleBoostCall:
        this.HandleGearToggleBoostCall();
        break;
      case Message.ShipGearToggleBoostDone:
        this.HandleGearToggleBoostDone(message);
        break;
      case Message.ShipGearChangeCall:
        this.HandleGearChangeCall(message);
        break;
      case Message.DockAtNpcShipRequest:
        GameProtocol.GetProtocol().TurnByPitchYawStrikes(Vector3.zero, Vector2.zero, 0.0f);
        break;
      case Message.DockAtPlayerShipRequest:
        SpaceLevel.GetLevel().ShipControls.ChangeCurrentSpeed(ShipControlsBase.SpeedMode.Abs, 0.0f);
        break;
      default:
        if (id != Message.EnableGear)
        {
          if (id != Message.ToggleOldUi)
          {
            if (id != Message.LoadNewLevel)
            {
              if (id != Message.ToggleJoystickGamepadEnabled)
              {
                if (id != Message.DockAtPlayerShipReply)
                  break;
                goto case Message.DockAtPlayerShipRequest;
              }
              else
              {
                JoystickSetup.Enabled = (bool) message.Data;
                break;
              }
            }
            else
            {
              this.gearIsEnabled = true;
              break;
            }
          }
          else
          {
            if ((bool) message.Data || !((Object) SpaceLevel.GetLevel() != (Object) null))
              break;
            SpaceLevel.GetLevel().ShipControls.ResetKeyStates();
            SpaceLevel.GetLevel().ShipControls.ResetMouseStates();
            SpaceLevel.GetLevel().ShipControls.StopTurning();
            break;
          }
        }
        else
        {
          this.gearIsEnabled = (bool) message.Data;
          break;
        }
    }
  }

  private void HandleSpeedChangeCall(IMessage<Message> message)
  {
    if (!this.gearIsEnabled || Game.Me.Anchored)
      return;
    DataChangeSpeed dataChangeSpeed = (DataChangeSpeed) message.Data;
    ShipControlsBase shipControlsBase = SpaceLevel.GetLevel().ShipControls;
    if (shipControlsBase == null)
      ShipControlsBase.cachedData = dataChangeSpeed;
    else
      shipControlsBase.ChangeCurrentSpeed(dataChangeSpeed.SpeedMode, dataChangeSpeed.SpeedChange);
  }

  private void HandleSpeedChangeResponse(IMessage<Message> message)
  {
    SpaceLevel.GetLevel().GUISpeedControl.SetSpeedAbs((float) message.Data);
  }

  private void HandleGearChangeCall(IMessage<Message> message)
  {
    if (!this.gearIsEnabled)
      return;
    SpaceLevel.GetLevel().ShipControls.SetActiveGear((Gear) message.Data);
  }

  private void HandleGearToggleBoostCall()
  {
    if (!this.gearIsEnabled)
      return;
    SpaceLevel.GetLevel().ShipControls.ToggleBoost();
  }

  private void HandleGearToggleBoostDone(IMessage<Message> message)
  {
    SpaceLevel.GetLevel().GUISpeedControl.ToggleBoost((bool) message.Data);
  }
}
