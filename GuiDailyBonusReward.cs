﻿// Decompiled with JetBrains decompiler
// Type: GuiDailyBonusReward
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GuiDailyBonusReward : GuiDialog
{
  public GuiDailyBonusReward(List<RewardCard> rewards)
  {
    this.Align = Align.MiddleCenter;
    Texture2D texture2D = ResourceLoader.Load<Texture2D>("GUI/DailyBonus/background_giveaway");
    this.m_backgroundImage.Texture = texture2D;
    this.Size = new Vector2((float) texture2D.width, (float) texture2D.height);
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.daily_bonus.2_title%", Gui.Options.FontBGM_BT, 20), Align.UpCenter, new Vector2(0.0f, 25f));
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.daily_bonus.2_strike%", Gui.Options.FontBGM_BT, 20), Align.UpCenter, new Vector2(-270f, 95f));
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.daily_bonus.2_escort%", Gui.Options.FontBGM_BT, 20), Align.UpCenter, new Vector2(0.0f, 95f));
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.daily_bonus.2_line%", Gui.Options.FontBGM_BT, 20), Align.UpCenter, new Vector2(267f, 95f));
    int num = 0;
    using (List<RewardCard>.Enumerator enumerator = rewards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        RewardCard current = enumerator.Current;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GuiDailyBonusReward.\u003CGuiDailyBonusReward\u003Ec__AnonStorey83 rewardCAnonStorey83 = new GuiDailyBonusReward.\u003CGuiDailyBonusReward\u003Ec__AnonStorey83();
        // ISSUE: reference to a compiler-generated field
        rewardCAnonStorey83.\u003C\u003Ef__this = this;
        GuiLabel guiLabel = new GuiLabel(string.Join("%br%", current.Items.ConvertAll<string>((Converter<ShipItem, string>) (si => (!(si is ItemCountable) ? string.Empty : ((int) (si as ItemCountable).Count).ToString() + " ") + si.ItemGUICard.Name)).ToArray()), Gui.Options.FontBGM_BT, 14);
        guiLabel.WordWrap = true;
        guiLabel.SizeX = 210f;
        this.AddChild((GuiElementBase) guiLabel, Align.MiddleLeft, new Vector2((float) (30 + num * 265), 0.0f));
        // ISSUE: reference to a compiler-generated field
        rewardCAnonStorey83.button = new GuiButton("%$bgo.daily_bonus.2_button%");
        // ISSUE: reference to a compiler-generated field
        rewardCAnonStorey83.button.SizeX *= 1.5f;
        // ISSUE: reference to a compiler-generated field
        rewardCAnonStorey83.button.SizeY = 30f;
        // ISSUE: reference to a compiler-generated field
        rewardCAnonStorey83.button.Label.Font = Gui.Options.FontBGM_BT;
        // ISSUE: reference to a compiler-generated field
        rewardCAnonStorey83.button.Label.FontSize = 14;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        rewardCAnonStorey83.button.Pressed = new AnonymousDelegate(rewardCAnonStorey83.\u003C\u003Em__B8);
        // ISSUE: reference to a compiler-generated field
        this.AddChild((GuiElementBase) rewardCAnonStorey83.button, Align.DownCenter, new Vector2((float) (num * 270 - 270), -15f));
        // ISSUE: reference to a compiler-generated field
        rewardCAnonStorey83.button.SetEnvironment((object) current);
        this.OnClose = (AnonymousDelegate) (() => GuiDialogPopupManager.CloseDialog());
        ++num;
      }
    }
  }
}
