﻿// Decompiled with JetBrains decompiler
// Type: ComboBoxHeaderUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class ComboBoxHeaderUi : MonoBehaviour
{
  public System.Action<bool> OnCollapseToggled = (System.Action<bool>) (param0 => {});
  private bool isCollapsed = true;

  public bool IsCollapsed
  {
    get
    {
      return this.isCollapsed;
    }
    set
    {
      this.isCollapsed = value;
      this.OnCollapseToggled(this.isCollapsed);
      this.ToggleCollapseButtonImage(this.isCollapsed);
    }
  }

  public abstract void SetHeaderText(string headerText);

  public abstract void SetButtonImageCollapsed();

  public abstract void SetButtonImageUncollapsed();

  private void ToggleCollapseButtonImage(bool isCollapsed)
  {
    if (isCollapsed)
      this.SetButtonImageCollapsed();
    else
      this.SetButtonImageUncollapsed();
  }

  public void CollapseButtonClick()
  {
    this.IsCollapsed = !this.isCollapsed;
  }
}
