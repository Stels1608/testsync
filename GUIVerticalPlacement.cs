﻿// Decompiled with JetBrains decompiler
// Type: GUIVerticalPlacement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIVerticalPlacement : GUIVerticalLayout
{
  public GUIVerticalPlacement()
    : base((SmartRect) null)
  {
  }

  public GUIVerticalPlacement(SmartRect parent)
    : base(parent)
  {
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }
}
