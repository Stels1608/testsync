﻿// Decompiled with JetBrains decompiler
// Type: ShipCustomizationWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.AvatarScreen;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ShipCustomizationWindow : GuiPanel
{
  private StickerBinding stickerBinding = new StickerBinding();
  private readonly GuiPanel panel = new GuiPanel();
  private readonly GuiPanelVerticalScroll scroll = new GuiPanelVerticalScroll();
  private readonly GuiLabel labelLoading = new GuiLabel("%$bgo.etc.ship_shop_loading%", Gui.Options.FontBGM_BT, 20);
  private Faction faction = Faction.Colonial;
  private StickerManager stickerManager;
  private ShipSlot paintSlot;
  private ShipSystem selectedPaint;
  private readonly GuiNameBox namePanel;
  private HangarShip theShip;
  private bool isShipLoading;
  private bool ready;
  private RenderModelToImage ShipView;
  private bool haveButtons;
  private Rect ImgRect;
  private GuiButton resetShipRotation;
  private GuiTabsSimple customizationTab;
  private ShipCustomizationWindow.CustomizationPart customizationPart;
  public ShipCustomizationWindow.dHandlerClose HandlerClose;

  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  public override bool IsRendered
  {
    get
    {
      return base.IsRendered;
    }
    set
    {
      if (this.IsRendered == value)
        return;
      if (value)
      {
        this.ShipView = new RenderModelToImage();
        this.ready = false;
        this.panel.IsRendered = true;
        this.scroll.IsRendered = true;
        this.isShipLoading = false;
        this.theShip = Game.Me.ActiveShip;
        this.faction = Game.Me.Faction;
        this.namePanel.textBox.Text = this.theShip.Name;
        this.paintSlot = (ShipSlot) null;
        this.selectedPaint = (ShipSystem) null;
        foreach (ShipSlot slot in this.theShip.Slots)
        {
          if (slot.Card.SystemType == ShipSlotType.ship_paint)
          {
            this.paintSlot = slot;
            break;
          }
        }
        if (this.paintSlot != null && this.paintSlot.System != null)
        {
          this.selectedPaint = this.paintSlot.System;
          if (this.selectedPaint.PaintCard != null)
            this.ShipView.SetSkinnedModel(this.selectedPaint.PaintCard, true);
        }
      }
      else if (this.ShipView != null)
      {
        this.ShipView.Delete();
        this.ShipView = (RenderModelToImage) null;
      }
      base.IsRendered = value;
    }
  }

  public ShipCustomizationWindow()
  {
    this.enabled = false;
    this.IsRendered = false;
    Loaders.Load(this.panel, "GUI/ShipCustomization/layout");
    this.panel.IsRendered = false;
    this.Size = this.panel.Size;
    this.AddChild((GuiElementBase) this.panel);
    this.panel.AddChild((GuiElementBase) this.labelLoading, Align.MiddleCenter, Vector2.right * 200f);
    this.scroll.Position = new Vector2(3f, 3f);
    this.scroll.Size = new Vector2(388f, 440f);
    this.scroll.IsRendered = false;
    this.scroll.RenderScrollLines = false;
    this.AddChild((GuiElementBase) this.scroll);
    this.panel.Find<GuiButton>("close").Pressed = new AnonymousDelegate(this.Close);
    GuiButton guiButton = this.panel.Find<GuiButton>("btnCancel");
    this.resetShipRotation = new GuiButton("<>");
    this.resetShipRotation.Font = Gui.Options.FontBGM_BT;
    this.resetShipRotation.FontSize = 16;
    this.resetShipRotation.SizeY += 10f;
    this.resetShipRotation.SizeX = this.resetShipRotation.SizeY;
    this.resetShipRotation.PositionX = guiButton.PositionX + guiButton.SizeX + this.resetShipRotation.SizeX;
    this.resetShipRotation.PositionY = (float) ((double) guiButton.PositionY - (double) this.resetShipRotation.SizeY - 25.0);
    this.resetShipRotation.NormalTexture = ResourceLoader.Load<Texture2D>("GUI/ShipCustomization/camerareset");
    this.resetShipRotation.PressedTexture = ResourceLoader.Load<Texture2D>("GUI/ShipCustomization/camerareset_click");
    this.resetShipRotation.OverTexture = ResourceLoader.Load<Texture2D>("GUI/ShipCustomization/camerareset_over");
    this.resetShipRotation.Text = string.Empty;
    this.resetShipRotation.Pressed = (AnonymousDelegate) (() =>
    {
      if (this.ShipView == null)
        return;
      this.ShipView.RestoreView();
    });
    this.AddChild((GuiElementBase) this.resetShipRotation);
    this.panel.Find<GuiImage>("sticker").IsRendered = false;
    this.panel.Find<GuiButton>("btnCancel").Pressed = (AnonymousDelegate) (() => this.Close());
    this.panel.Find<GuiImage>("imgShip").IsRendered = false;
    this.panel.Find<GuiImage>("sticker").IsRendered = false;
    this.namePanel = new GuiNameBox(new Vector2(180f, 30f));
    this.namePanel.Position = this.panel.Find<GuiImage>("name_panel").Position;
    this.namePanel.PositionX += 5f;
    this.namePanel.PositionY += 5f;
    this.namePanel.textBox.MaxSize = 21;
    this.panel.AddChild((GuiElementBase) this.namePanel);
    this.customizationTab = new GuiTabsSimple();
    this.customizationTab.AddTab("%$bgo.ShipCustomization.layout.decals_tab_label%", (AnonymousDelegate) (() =>
    {
      this.customizationPart = ShipCustomizationWindow.CustomizationPart.Decal;
      this.UpdateScroll();
    }));
    this.customizationTab.AddTab("%$bgo.ShipCustomization.layout.paints_tab_label%", (AnonymousDelegate) (() =>
    {
      this.customizationPart = ShipCustomizationWindow.CustomizationPart.ShipPaint;
      this.UpdateScroll();
    }));
    this.customizationTab.SelectIndex(0);
    this.AddChild((GuiElementBase) this.customizationTab, new Vector2(100f, this.panel.SizeY + 10f));
    this.MouseTransparent = true;
    this.RecalculateAbsCoords();
  }

  private void Close()
  {
    if (this.ShipView != null)
    {
      this.ShipView.Delete();
      this.ShipView = (RenderModelToImage) null;
    }
    this.panel.IsRendered = false;
    this.scroll.IsRendered = false;
    this.IsRendered = false;
    this.scroll.EmptyChildren();
    if (this.HandlerClose == null)
      return;
    this.HandlerClose();
  }

  public override void OnShow()
  {
    base.OnShow();
    FeedbackProtocol.GetProtocol().ReportUiElementShown(FeedbackProtocol.UiElementId.ShipCustomizationWindow);
  }

  public override void OnHide()
  {
    base.OnHide();
    FeedbackProtocol.GetProtocol().ReportUiElementHidden(FeedbackProtocol.UiElementId.ShipCustomizationWindow);
  }

  public override bool ScrollDown(float2 position)
  {
    if (0.0 < (double) this.ShipView.RestoreTimer)
      return false;
    if (this.ShipView != null && this.ImgRect.Contains(new Vector2(position.x, position.y)))
      this.ShipView.ZoomModel(0.2f);
    return base.ScrollDown(position);
  }

  public override bool ScrollUp(float2 position)
  {
    if (0.0 < (double) this.ShipView.RestoreTimer)
      return false;
    if (this.ShipView != null && this.ImgRect.Contains(new Vector2(position.x, position.y)))
      this.ShipView.ZoomModel(-0.2f);
    return base.ScrollUp(position);
  }

  public override bool MouseMove(float2 position, float2 previousPosition)
  {
    this.ShipView.RotateModelByMouse(position, previousPosition, 0.3f, true);
    return base.MouseMove(position, previousPosition);
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    base.OnKeyDown(keyboardKey, action);
    return this.IsRendered;
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    base.OnKeyUp(keyboardKey, action);
    return this.IsRendered;
  }

  public override void Draw()
  {
    base.Draw();
    if (this.ShipView == null || !((UnityEngine.Object) this.ShipView.camera != (UnityEngine.Object) null))
      return;
    if (Event.current.type == UnityEngine.EventType.Repaint)
      this.ShipView.camera.Render();
    for (int index = 0; index < this.panel.Children.Count; ++index)
    {
      if (this.panel.Children[index].Name.Contains("btn"))
        this.panel.Children[index].Draw();
    }
  }

  private void UpdateScroll()
  {
    if (!(bool) StaticCards.Instance.Stickers.IsLoaded || !this.isShipLoading || !((UnityEngine.Object) this.ShipView.obj != (UnityEngine.Object) null))
      return;
    switch (this.customizationPart)
    {
      case ShipCustomizationWindow.CustomizationPart.Decal:
        this.scroll.EmptyChildren();
        foreach (Sticker sticker in StaticCards.Instance.Stickers.GetStickers(this.faction))
        {
          ShipCustomizationSlot customizationSlot = (ShipCustomizationSlot) new StickerSlot(sticker, new System.Action<Sticker>(this.updateSticker));
          customizationSlot.PositionX = 0.0f;
          this.scroll.AddChild((GuiElementBase) customizationSlot);
        }
        break;
      case ShipCustomizationWindow.CustomizationPart.ShipPaint:
        this.GetPlayerOwnedSkin();
        break;
    }
  }

  private void GetPlayerOwnedSkin()
  {
    this.scroll.EmptyChildren();
    bool isUpgraded = this.theShip.IsUpgraded;
    ShipPaintSlot shipPaintSlot1 = new ShipPaintSlot((ShipSystem) null, new System.Action<ShipSystem>(this.updateShipPaint));
    shipPaintSlot1.PositionX = 0.0f;
    shipPaintSlot1.SetInstallMessage(true);
    this.scroll.AddChild((GuiElementBase) shipPaintSlot1);
    foreach (ShipSlot slot in this.theShip.Slots)
    {
      if (slot.Card.SystemType == ShipSlotType.ship_paint && slot.System != null)
      {
        this.selectedPaint = slot.System;
        ShipPaintSlot shipPaintSlot2 = new ShipPaintSlot(slot.System, new System.Action<ShipSystem>(this.updateShipPaint));
        shipPaintSlot2.PositionX = 0.0f;
        shipPaintSlot2.SetInstallMessage(isUpgraded);
        this.scroll.AddChild((GuiElementBase) shipPaintSlot2);
        break;
      }
    }
    ItemList itemList1 = Game.Me.Hold.FilterBySlotType(ShipSlotType.ship_paint).Filter((Predicate<ShipItem>) (item => this.IsShipPaintForActiveShip(item as ShipSystem)));
    for (int index = 0; index < itemList1.Count; ++index)
    {
      ShipPaintSlot shipPaintSlot2 = new ShipPaintSlot(itemList1[index] as ShipSystem, new System.Action<ShipSystem>(this.updateShipPaint));
      shipPaintSlot2.PositionX = 0.0f;
      shipPaintSlot2.SetInstallMessage(isUpgraded);
      this.scroll.AddChild((GuiElementBase) shipPaintSlot2);
    }
    ItemList itemList2 = Game.Me.Locker.FilterBySlotType(ShipSlotType.ship_paint).Filter((Predicate<ShipItem>) (item => this.IsShipPaintForActiveShip(item as ShipSystem)));
    for (int index = 0; index < itemList2.Count; ++index)
    {
      ShipPaintSlot shipPaintSlot2 = new ShipPaintSlot(itemList2[index] as ShipSystem, new System.Action<ShipSystem>(this.updateShipPaint));
      shipPaintSlot2.PositionX = 0.0f;
      shipPaintSlot2.SetInstallMessage(isUpgraded);
      this.scroll.AddChild((GuiElementBase) shipPaintSlot2);
    }
  }

  public override void Update()
  {
    if (this.panel.IsUpdated)
      this.panel.Update();
    if (this.ShipView == null)
      return;
    this.labelLoading.IsRendered = this.ShipView.StillLoading;
    if (!this.ready && (bool) StaticCards.Instance.Stickers.IsLoaded && (this.isShipLoading && (UnityEngine.Object) this.ShipView.obj != (UnityEngine.Object) null))
    {
      this.UpdateScroll();
      this.panel.Find<GuiButton>("btnDone").Pressed = (AnonymousDelegate) (() =>
      {
        if ((int) this.stickerBinding.StickerID != 0)
        {
          foreach (SpotDesc spot in this.theShip.Card.WorldCard.Spots)
          {
            if (spot.Type == SpotType.Sticker)
            {
              this.stickerBinding.ObjectPointHash = spot.ObjectPointServerHash;
              this.theShip.BindSticker(this.stickerBinding);
            }
          }
        }
        if (this.paintSlot != null && this.paintSlot.System != this.selectedPaint)
        {
          if (this.paintSlot.System != null)
            this.paintSlot.System.MoveTo((IContainer) Game.Me.Locker);
          if (this.selectedPaint != null)
            this.selectedPaint.MoveTo((IContainer) this.paintSlot);
        }
        this.theShip.SetName(this.namePanel.textBox.Text);
        this.Close();
      });
      List<DecalSpot> decalSpotList = new List<DecalSpot>();
      foreach (Spot spot in Spot.FindSpots(this.theShip.Card.WorldCard.Spots, this.ShipView.obj))
      {
        if (spot.Type == SpotType.Sticker)
          decalSpotList.Add((DecalSpot) spot);
      }
      this.stickerManager = new StickerManager((IEnumerable<DecalSpot>) decalSpotList, this.faction);
      if (this.theShip.Stickers.Count > 0)
        this.stickerBinding = this.theShip.Stickers[0];
      else
        this.stickerBinding.StickerID = StaticCards.Instance.Stickers.GetStickers(this.faction)[0].ID;
      this.ApplyIcon(StaticCards.Instance.Stickers.GetTexture(this.stickerBinding.StickerID, this.faction));
      this.stickerManager.ApplyBindingAll(this.stickerBinding);
      this.ready = true;
    }
    if ((bool) ((UnityEngine.Object) this.ShipView.obj) && this.haveButtons)
    {
      float DegreesPerSecond = Time.deltaTime * 35f;
      if (this.panel.Find<GuiButton>("btnR").IsMouseOver && Input.GetMouseButton(0))
        this.ShipView.RotateModelInDirection(1f, 0.0f, DegreesPerSecond, true);
      if (this.panel.Find<GuiButton>("btnL").IsMouseOver && Input.GetMouseButton(0))
        this.ShipView.RotateModelInDirection(-1f, 0.0f, DegreesPerSecond, true);
      if (this.panel.Find<GuiButton>("btnU").IsMouseOver && Input.GetMouseButton(0))
        this.ShipView.RotateModelInDirection(0.0f, 1f, DegreesPerSecond, true);
      if (this.panel.Find<GuiButton>("btnD").IsMouseOver && Input.GetMouseButton(0))
        this.ShipView.RotateModelInDirection(0.0f, -1f, DegreesPerSecond, true);
    }
    if (!this.isShipLoading && this.theShip != null && (bool) this.theShip.Card.IsLoaded)
    {
      if (this.panel.Find<GuiButton>("btnR") != null && this.panel.Find<GuiButton>("btnL") != null && (this.panel.Find<GuiButton>("btnU") != null && this.panel.Find<GuiButton>("btnD") != null))
        this.haveButtons = true;
      this.ShipView.LayerName = "AvatarInfoWindow";
      this.ShipView.SetPrefab(this.GetPrefabName());
      this.ShipView.ShowRust = !this.theShip.IsUpgraded || this.paintSlot != null && this.paintSlot.System == null;
      this.isShipLoading = true;
      this.RecalculateAbsCoords();
    }
    this.ShipView.Update();
  }

  private string GetPrefabName()
  {
    if (this.paintSlot != null && this.paintSlot.System != null && !this.paintSlot.System.PaintCard.UsesDefaultModel)
      return this.paintSlot.System.PaintCard.PrefabName.ToLower();
    return this.theShip.Card.WorldCard.PrefabName.ToLower();
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.Position = new Vector2((float) (Screen.width / 2 - 450), (float) (Screen.height / 2 - 200));
    if (this.ShipView == null)
      return;
    GuiImage guiImage = this.panel.Find<GuiImage>("imgShip");
    guiImage.ForceReposition();
    this.ImgRect = guiImage.Rect;
    this.ShipView.ImgRect = this.ImgRect;
    this.ShipView.UpdateRect();
  }

  private void Delete()
  {
    this.ShipView.Delete();
  }

  public void ApplyIcon(Texture2D text)
  {
    GuiImage guiImage = this.panel.Find<GuiImage>("sticker");
    guiImage.IsRendered = true;
    guiImage.Texture = text;
    guiImage.Size = new Vector2(64f, 64f);
  }

  private void updateSticker(Sticker sticker)
  {
    if (sticker != null && this.theShip != null)
      this.stickerBinding.StickerID = sticker.ID;
    this.ApplyIcon(StaticCards.Instance.Stickers.GetTexture(this.stickerBinding.StickerID, this.faction));
    this.stickerManager.ApplyBindingAll(this.stickerBinding);
  }

  private void updateShipPaint(ShipSystem shipPaint)
  {
    if (this.theShip == null)
      return;
    if (this.theShip.IsUpgraded)
      this.selectedPaint = shipPaint;
    if (shipPaint == null)
    {
      this.ShipView.ShowRust = true;
      this.ShipView.ShowDefaultSkin(this.theShip);
    }
    else
    {
      if (shipPaint.PaintCard == null || !this.IsShipPaintForActiveShip(shipPaint))
        return;
      this.ShipView.ShowRust = false;
      this.ShipView.SetSkinnedModel(shipPaint.PaintCard, true);
    }
  }

  private bool IsShipPaintForActiveShip(ShipSystem shipPaint)
  {
    if (shipPaint == null || !(bool) shipPaint.PaintCard.IsLoaded)
      return false;
    if ((int) Game.Me.ActiveShip.Card.CardGUID == (int) shipPaint.PaintCard.shipCard.CardGUID)
      return true;
    if (shipPaint.PaintCard.shipCard.NextCard != null)
      return (int) Game.Me.ActiveShip.Card.CardGUID == (int) shipPaint.PaintCard.shipCard.NextCard.CardGUID;
    return false;
  }

  private enum CustomizationPart : byte
  {
    Decal,
    ShipPaint,
  }

  public delegate void dHandlerClose();
}
