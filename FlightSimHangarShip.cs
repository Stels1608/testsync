﻿// Decompiled with JetBrains decompiler
// Type: FlightSimHangarShip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class FlightSimHangarShip : HangarShip
{
  public FlightSimHangarShip()
  {
    this.slots = new Dictionary<ushort, ShipSlot>();
    this.Card = new ShipCard(0U);
    ShipSlot shipSlot = new ShipSlot((HangarShip) this, (ushort) 0);
    shipSlot.System = new ShipSystem();
    shipSlot.System.Card = new ShipSystemCard(0U);
    this.slots.Add((ushort) 0, shipSlot);
  }
}
