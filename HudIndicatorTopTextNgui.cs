﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorTopTextNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class HudIndicatorTopTextNgui : HudIndicatorBaseComponentUgui, IReceivesSetting, IReceivesTarget, IHudIndicatorTopText
{
  private bool showPlayerName = true;
  private bool showTargetName = true;
  private bool showWing = true;
  private bool showTitle = true;
  [SerializeField]
  protected UILabel textLabel;
  private ISpaceEntity target;

  public UILabel TextLabel
  {
    get
    {
      return this.textLabel;
    }
  }

  protected override void ResetComponentStates()
  {
    this.target = (ISpaceEntity) null;
    if (!((Object) this.textLabel != (Object) null))
      return;
    this.textLabel.text = string.Empty;
  }

  protected override void Awake()
  {
    base.Awake();
    this.textLabel = NGUIToolsExtension.AddLabel(this.gameObject, Gui.Options.FontBGM_BT, 12, UIWidget.Pivot.Bottom, string.Empty);
    this.textLabel.overflowMethod = UILabel.Overflow.ResizeFreely;
    this.textLabel.width = 300;
    this.textLabel.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.target = target;
    this.UpdateLabelText();
    this.UpdateColorAndAlpha();
  }

  public void UpdateLabelText()
  {
    this.TextLabel.text = !this.showPlayerName ? string.Empty : this.target.Name;
    if (Game.Me.TournamentParticipant)
      return;
    PlayerShip playerShip = this.target as PlayerShip;
    if (playerShip == null)
      return;
    if (this.showTargetName)
    {
      string str1 = playerShip.Player.ShipName;
      if (!string.IsNullOrEmpty(str1))
      {
        UILabel textLabel = this.TextLabel;
        string str2 = textLabel.text + ", " + str1;
        textLabel.text = str2;
      }
      else
      {
        UILabel textLabel = this.TextLabel;
        string str2 = textLabel.text + ", " + playerShip.GUICard.Name;
        textLabel.text = str2;
      }
    }
    if (this.showTitle && !string.IsNullOrEmpty(playerShip.Player.Title))
    {
      UILabel textLabel = this.TextLabel;
      string str = textLabel.text + "\n[ " + playerShip.Player.Title + " ]";
      textLabel.text = str;
    }
    if (!this.showWing || string.IsNullOrEmpty(playerShip.Player.guildName))
      return;
    UILabel textLabel1 = this.TextLabel;
    string str3 = textLabel1.text + "\n<" + Tools.SubStr(playerShip.Player.guildName, 30) + ">";
    textLabel1.text = str3;
  }

  protected override void SetColoring()
  {
    this.TextLabel.color = HudIndicatorColorInfo.GetTargetColor4LegacyBracket(this.target);
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    this.textLabel.enabled = (this.InScannerRange || this.IsSelected || this.IsMultiselected) && this.InScreen;
  }

  public void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    switch (userSetting)
    {
      case UserSetting.HudIndicatorShowShipNames:
        this.showTargetName = (bool) data;
        break;
      case UserSetting.HudIndicatorShowWingNames:
        this.showWing = (bool) data;
        break;
      case UserSetting.HudIndicatorShowTitles:
        this.showTitle = (bool) data;
        break;
      case UserSetting.HudIndicatorShowTargetNames:
        this.showPlayerName = (bool) data;
        break;
    }
    this.UpdateLabelText();
  }
}
