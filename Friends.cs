﻿// Decompiled with JetBrains decompiler
// Type: Friends
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class Friends
{
  private readonly List<Player> players = new List<Player>();
  private readonly List<Player> ignored = new List<Player>();
  private readonly List<Player> recruits = new List<Player>();
  private readonly InviteTimeoutManager inviteTimeout = new InviteTimeoutManager();
  private int requiredRecruitLevel = 10;
  private readonly CommunityProtocol communityProtocol;
  private bool ignoreListInit;

  public ReadOnlyCollection<Player> Ignore
  {
    get
    {
      return this.ignored.AsReadOnly();
    }
  }

  public ReadOnlyCollection<Player> Players
  {
    get
    {
      return this.players.AsReadOnly();
    }
  }

  public ReadOnlyCollection<Player> Recruits
  {
    get
    {
      return this.recruits.AsReadOnly();
    }
  }

  public int RequiredRecruitLevel
  {
    get
    {
      return this.requiredRecruitLevel;
    }
    set
    {
      this.requiredRecruitLevel = value;
    }
  }

  public Friends()
  {
    this.communityProtocol = CommunityProtocol.GetProtocol();
  }

  public void Update()
  {
    this.inviteTimeout.Update();
  }

  public void AddToIgnored(string playerName)
  {
    this.communityProtocol.RequestAddToIgnore(playerName);
  }

  public void ToggleIsIgnored(string playerName)
  {
    if (this.IsIgnored(playerName))
      this.RemoveFromIgnored(playerName);
    else
      this.AddToIgnored(playerName);
  }

  public bool IsOnIgnoreList(Player player)
  {
    return this.ignored.Contains(player);
  }

  public bool IsIgnored(string playerName)
  {
    using (List<Player>.Enumerator enumerator = this.ignored.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Player current = enumerator.Current;
        if (!current.IsModerator && current.Name == playerName)
          return true;
      }
    }
    return false;
  }

  public void ClearIgnore()
  {
    CommunityProtocol.GetProtocol().RequestClearIgnore();
  }

  public void RemoveFromIgnored(string playerName)
  {
    this.communityProtocol.RequestRemoveFromIgnore(playerName);
  }

  public void AddFriend(Player player)
  {
    if (this.inviteTimeout.IsTimeout(player))
    {
      FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.friends_invites.cant_invite_often%", NotificationCategory.Neutral));
    }
    else
    {
      this.inviteTimeout.DeleteTimeout(player);
      this.inviteTimeout.AddTimeout(player);
      this.communityProtocol.RequestAddFriend(player.ServerID);
    }
  }

  public void RemoveFriend(Player player)
  {
    this.communityProtocol.RequestRemoveFriend(player.ServerID);
  }

  public bool IsFriend(Player player)
  {
    return this.players.Contains(player);
  }

  public void SetFriends(List<FriendDesc> friends)
  {
    for (int index = 0; index < this.players.Count; ++index)
      this.UnsubscribeFriend(this.players[index]);
    this.players.Clear();
    for (int index = 0; index < friends.Count; ++index)
    {
      FriendDesc friendDesc = friends[index];
      Player player = Game.Players[friendDesc.id];
      player.Name = friendDesc.name;
      player.Subscribe(Player.InfoType.FriendWindowInfo);
      this.players.Add(player);
    }
  }

  public void RemoveFriend(uint friendID)
  {
    Player player = Game.Players.GetPlayer(friendID);
    this.UnsubscribeFriend(player);
    this.players.Remove(player);
  }

  public void SetRecruits(List<RecruitDesc> friendRecruits)
  {
    for (int index = 0; index < this.recruits.Count; ++index)
      this.UnsubscribeFriend(this.players[index]);
    this.recruits.Clear();
    if (friendRecruits.Count > 0)
      Game.DelayedActions.Add((DelayedActions.Predicate) (() =>
      {
        CommunityProtocol.GetProtocol().RequestRecruitInvited();
        return true;
      }));
    for (int index = 0; index < friendRecruits.Count; ++index)
    {
      RecruitDesc recruitDesc = friendRecruits[index];
      Player player = Game.Players[recruitDesc.id];
      if (player != null)
      {
        player.Level = recruitDesc.level;
        player.Name = recruitDesc.name;
        player.Subscribe(Player.InfoType.FriendWindowInfo);
        this.recruits.Add(player);
      }
    }
  }

  public void SetIgnore(IEnumerable<uint> ignoreIDs, string[] ignoreNames)
  {
    Player[] players = Game.Players.GetPlayers(ignoreIDs);
    this.ignored.AddRange((IEnumerable<Player>) players);
    int num = 0;
    foreach (Player player in players)
    {
      player.Subscribe(Player.InfoType.Name);
      if (num >= ignoreNames.Length)
      {
        Debug.LogWarning((object) "List of ignored playerIds is longer than supplied player names");
      }
      else
      {
        player.Name = ignoreNames[num++];
        if (this.ignoreListInit)
          Game.ChatStorage.SystemSays(BsgoLocalization.Get("bgo.chat.ignore_add", (object) player.Name));
      }
    }
    this.ignoreListInit = true;
  }

  public void RemoveIgnore(uint ignoreID)
  {
    if ((int) ignoreID == 0)
    {
      Game.ChatStorage.SystemSays("This user does not exist");
    }
    else
    {
      Player player = Game.Players.GetPlayer(ignoreID);
      this.ignored.Remove(player);
      Game.ChatStorage.SystemSays(BsgoLocalization.Get("bgo.chat.ignore_remove", (object) player.Name));
    }
  }

  private void UnsubscribeFriend(Player player)
  {
    if (Game.Me.Party.IsMember(player))
      return;
    player.Unsubscribe(Player.InfoType.FriendWindowInfo);
  }
}
