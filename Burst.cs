﻿// Decompiled with JetBrains decompiler
// Type: Burst
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Burst : MonoBehaviour
{
  private Vector3 farAway = new Vector3(50000f, 50000f, 50000f);
  public BurstKey BurstKey;
  public Burst.DestroyedEventHandler Destroyed;
  private int current;
  private BulletScript[] bullets;
  private int destroyedBulletCount;

  public void Init()
  {
    this.destroyedBulletCount = 0;
    this.current = 0;
  }

  public void SetBullets(Transform[] bulletTransforms)
  {
    this.bullets = new BulletScript[bulletTransforms.Length];
    for (int index = 0; index < this.bullets.Length; ++index)
    {
      bulletTransforms[index].position = this.farAway;
      this.bullets[index] = bulletTransforms[index].gameObject.AddComponent<BulletScript>();
      this.bullets[index].Destroyed = new BulletScript.DestroyedEventHandler(this.DestroyBullet);
    }
  }

  public bool HasBullet()
  {
    return this.current < this.bullets.Length;
  }

  public BulletScript CurrentBullet()
  {
    if (this.current == 0)
      return (BulletScript) null;
    return this.bullets[this.current - 1];
  }

  public BulletScript NextBullet()
  {
    BulletScript bulletScript = this.bullets[this.current];
    ++this.current;
    return bulletScript;
  }

  public void Clear()
  {
    foreach (BulletScript bullet in this.bullets)
    {
      if ((Object) bullet != (Object) null)
        Object.Destroy((Object) bullet.gameObject);
    }
  }

  public void DestroyBulletRest()
  {
    while (this.HasBullet())
      this.DestroyBullet(this.NextBullet());
  }

  public void DestroyBullet(BulletScript bullet)
  {
    for (int index = 0; index < this.bullets.Length; ++index)
    {
      if ((Object) this.bullets[index] == (Object) bullet)
      {
        if ((Object) this.bullets[index] != (Object) null && (Object) this.bullets[index].transform != (Object) null)
          this.bullets[index].transform.position = this.farAway;
        ++this.destroyedBulletCount;
        break;
      }
    }
    if (this.destroyedBulletCount != this.bullets.Length)
      return;
    this.OnDestroyed();
  }

  protected virtual void OnDestroyed()
  {
    if (this.Destroyed == null)
      return;
    this.Destroyed(this);
  }

  public delegate void DestroyedEventHandler(Burst burst);
}
