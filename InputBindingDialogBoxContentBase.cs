﻿// Decompiled with JetBrains decompiler
// Type: InputBindingDialogBoxContentBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System;
using UnityEngine;

public abstract class InputBindingDialogBoxContentBase : DialogBoxContent
{
  protected readonly Array unityKeyCodes = Enum.GetValues(typeof (KeyCode));
  protected readonly SettingsDataProvider settingsData = (SettingsDataProvider) FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider");
  [SerializeField]
  protected UILabel actionLabel;
  [SerializeField]
  protected UILabel informationLabel;
  [SerializeField]
  protected UILabel keyCombination;
  [SerializeField]
  protected ButtonWidget unmapButton;
  protected IInputBinding initBinding;
  protected InputDevice inputDevice;
  protected IInputBinding pressedBinding;
  protected Action actionToBind;

  protected virtual void Start()
  {
    this.unmapButton.handleClick = new AnonymousDelegate(this.UnmapAction);
  }

  public virtual void Update()
  {
    if (!Input.anyKeyDown)
      return;
    this.HandleInput();
  }

  protected abstract void HandleInput();

  protected virtual InputBindingState DetermineInputBindingState()
  {
    Action action = this.settingsData.InputBinderCache.TryGetAction(this.pressedBinding.Device, this.pressedBinding.DeviceTriggerCode, this.pressedBinding.DeviceModifierCode);
    if (!this.IsValidInput(this.pressedBinding))
      return InputBindingState.NOT_VALID;
    if (this.pressedBinding.IsBoundByCombination(this.initBinding.Device, this.initBinding.DeviceTriggerCode, this.initBinding.DeviceModifierCode))
      return InputBindingState.INITIALIZED;
    if ((int) this.pressedBinding.DeviceModifierCode != 0 && this.settingsData.InputBinderCache.TryGetAction(this.pressedBinding.Device, this.pressedBinding.DeviceModifierCode, (ushort) 0) != Action.None)
      return InputBindingState.MODIFIER_ALREADY_MAPPED_AS_SINGLE_TRIGGER;
    if (this.settingsData.InputBinderCache.TryGetActionUsingModifier(this.pressedBinding.Device, this.pressedBinding.DeviceTriggerCode) != Action.None)
      return InputBindingState.TRIGGER_ALREADY_MAPPED_AS_MODIFIER;
    if ((int) this.pressedBinding.DeviceTriggerCode == 0)
      return InputBindingState.NOT_MAPPED;
    return action == Action.None ? InputBindingState.VALID : InputBindingState.TRIGGER_ALREADY_MAPPED;
  }

  public virtual void Setup(IInputBinding currentBinding)
  {
    this.initBinding = currentBinding.Copy();
    this.pressedBinding = currentBinding.Copy();
    this.inputDevice = currentBinding.Device;
    this.actionToBind = currentBinding.Action;
    this.actionLabel.text = Tools.GetLocalized(this.actionToBind);
    this.UpdateUi();
  }

  protected virtual void UnmapAction()
  {
    this.pressedBinding.Unmap();
    this.UpdateUi();
  }

  protected virtual void UpdateUi()
  {
    InputBindingState inputBindingState = this.DetermineInputBindingState();
    WidgetColorScheme widgetColorScheme = ColorManager.currentColorScheme;
    this.keyCombination.text = this.pressedBinding.TriggerAlias;
    switch (inputBindingState)
    {
      case InputBindingState.INITIALIZED:
        this.informationLabel.text = Tools.ParseMessage(this.GetTextForInitState());
        this.informationLabel.color = widgetColorScheme.Get(WidgetColorType.TEXT_RUNNING);
        break;
      case InputBindingState.VALID:
        this.informationLabel.text = Tools.ParseMessage(this.GetTextForValidInputState());
        this.informationLabel.color = widgetColorScheme.Get(WidgetColorType.TEXT_GREEN);
        break;
      case InputBindingState.TRIGGER_ALREADY_MAPPED:
        this.informationLabel.text = Tools.ParseMessage(this.GetTextForTriggerAlreadyMappedState(), (object) this.pressedBinding.TriggerAlias, (object) Tools.GetLocalized(this.settingsData.InputBinderCache.TryGetAction(this.pressedBinding.Device, this.pressedBinding.DeviceTriggerCode, this.pressedBinding.DeviceModifierCode)));
        this.informationLabel.color = widgetColorScheme.Get(WidgetColorType.TEXT_RED);
        break;
      case InputBindingState.NOT_MAPPED:
        this.keyCombination.text = Tools.ParseMessage("%$bgo.ui.options.popup.keybinding.key.unmapped%");
        this.informationLabel.text = Tools.ParseMessage(this.GetTextForNotMappedState());
        this.informationLabel.color = widgetColorScheme.Get(WidgetColorType.TEXT_RED);
        break;
      case InputBindingState.NOT_VALID:
        this.informationLabel.text = Tools.ParseMessage(this.GetTextForInvalidInputState(), (object) this.keyCombination.text);
        this.informationLabel.color = widgetColorScheme.Get(WidgetColorType.TEXT_RED);
        break;
    }
  }

  public abstract bool IsValidInput(IInputBinding inputBinding);

  public override void OnAccept()
  {
    Log.Add("ON ACCEPT INPUTBINDING BOX");
    if (this.DetermineInputBindingState() == InputBindingState.NOT_VALID)
      return;
    FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new InputBindingChangeMessage(this.inputDevice, this.initBinding, this.pressedBinding.Copy()));
  }

  public override void OnCancel()
  {
  }

  protected abstract string GetTextForValidInputState();

  protected abstract string GetTextForInitState();

  protected abstract string GetTextForInvalidInputState();

  protected abstract string GetTextForTriggerAlreadyMappedState();

  protected abstract string GetTextForNotMappedState();
}
