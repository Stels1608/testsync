﻿// Decompiled with JetBrains decompiler
// Type: GUIChatExtendedTextMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

internal class GUIChatExtendedTextMessage : GUIPanel
{
  private float fixedWidth;
  private ChatDataStorage.ExtendedTextMessage message;
  private GUILabelNew prefix;
  private GUILabelNew name;
  private GUILabelNew suffix;
  private GUILabelNew text;

  public bool ShowCaption
  {
    get
    {
      return this.name.IsRendered;
    }
    set
    {
      GUILabelNew guiLabelNew = this.prefix;
      bool flag1 = value;
      this.name.IsRendered = flag1;
      bool flag2 = flag1;
      this.suffix.IsRendered = flag2;
      int num = flag2 ? 1 : 0;
      guiLabelNew.IsRendered = num != 0;
    }
  }

  public bool ShowPrefix
  {
    get
    {
      return this.prefix.IsRendered;
    }
    set
    {
      this.prefix.IsRendered = value;
      this.FixLayout();
    }
  }

  public bool ShowSuffix
  {
    get
    {
      return this.suffix.IsRendered;
    }
    set
    {
      this.suffix.IsRendered = value;
    }
  }

  public float AlphaLevel
  {
    set
    {
      Color captionColor = this.CaptionColor;
      Color textColor = this.TextColor;
      captionColor.a = value;
      textColor.a = value;
      this.CaptionColor = captionColor;
      this.TextColor = textColor;
    }
  }

  public Color CaptionColor
  {
    get
    {
      return this.name.NormalColor;
    }
    set
    {
      GUILabelNew guiLabelNew = this.name;
      Color color1 = value;
      this.suffix.AllColor = color1;
      Color color2 = color1;
      this.prefix.AllColor = color2;
      Color color3 = color2;
      guiLabelNew.AllColor = color3;
    }
  }

  public Color TextColor
  {
    get
    {
      return this.text.NormalColor;
    }
    set
    {
      this.text.AllColor = value;
    }
  }

  public GUIChatExtendedTextMessage(ChatDataStorage.Message message, float fixedWidth)
    : this(new ChatDataStorage.ExtendedTextMessage(string.Empty, message.text, string.Empty, string.Empty, Color.white, message.textColor), fixedWidth)
  {
    this.ShowCaption = false;
    this.FixLayout();
  }

  public GUIChatExtendedTextMessage(ChatDataStorage.TextMessage message, float fixedWidth)
    : this(new ChatDataStorage.ExtendedTextMessage(message.name, message.text, string.Empty, string.Empty, message.nameColor, message.textColor), fixedWidth, false, false)
  {
  }

  public GUIChatExtendedTextMessage(ChatDataStorage.ExtendedTextMessage message, float fixedWidth)
    : this(message, fixedWidth, true, true)
  {
  }

  public GUIChatExtendedTextMessage(ChatDataStorage.ExtendedTextMessage message, float fixedWidth, bool showPrefix, bool showSuffix)
  {
    this.Name = "GUIChatExtendedTextMessage" + message.text;
    this.message = message;
    this.prefix = new GUILabelNew(message.prefix);
    this.name = new GUILabelNew("|" + message.name + "|");
    this.suffix = new GUILabelNew(message.suffix + " :");
    this.text = new GUILabelNew(message.text);
    this.prefix.Width = 50f;
    this.name.AutoSize = true;
    this.suffix.AutoSize = true;
    this.prefix.Alignment = TextAnchor.MiddleLeft;
    this.name.Alignment = TextAnchor.MiddleLeft;
    this.suffix.Alignment = TextAnchor.MiddleLeft;
    this.text.Alignment = TextAnchor.UpperLeft;
    this.text.WordWrap = true;
    this.AddPanel((GUIPanel) this.prefix);
    this.AddPanel((GUIPanel) this.name);
    this.AddPanel((GUIPanel) this.suffix);
    this.AddPanel((GUIPanel) this.text);
    this.CaptionColor = message.nameColor;
    this.TextColor = message.textColor;
    this.fixedWidth = fixedWidth;
    this.ShowPrefix = showPrefix;
    this.ShowSuffix = showSuffix;
    this.FixLayout();
    this.IsRendered = true;
  }

  private void FixLayout()
  {
    this.text.Width = this.fixedWidth;
    if (this.ShowCaption)
      this.text.Width = this.fixedWidth - (!this.ShowPrefix ? 0.0f : this.prefix.Width);
    this.text.Height = (float) TextUtility.CalcTextSize(this.text.Text, this.text.Font, this.text.Width).height;
    this.Size = new float2(this.fixedWidth, (!this.ShowCaption ? 0.0f : this.name.Height) + this.text.Height);
    this.prefix.PlaceLeftUpperCornerOf((GUIPanel) this);
    if (this.ShowPrefix)
      this.name.PlaceRightOf((GUIPanel) this.prefix);
    else
      this.name.PlaceLeftUpperCornerOf((GUIPanel) this);
    if (this.ShowSuffix)
    {
      this.suffix.PlaceRightOf((GUIPanel) this.name, 5f);
      this.suffix.RecalculateAbsCoords();
      if ((double) (this.suffix.PositionX + this.suffix.Width / 2f) > (double) this.Width / 2.0)
        this.suffix.IsRendered = false;
    }
    this.text.Position = new float2(this.text.RightOf((GUIPanel) this.name).x - this.name.Width, this.text.BelowOf((GUIPanel) this.name).y);
    if (this.ShowCaption)
      return;
    this.text.PlaceLeftUpperCornerOf((GUIPanel) this);
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (this.Contains(mousePosition))
    {
      if (this.name.Contains(mousePosition) && mouseKey == KeyCode.Mouse0)
      {
        if (this.message.name != Game.Me.Name)
          Game.GUIManager.Find<GUIChatNew>().StartWhisper(this.message.name);
        return true;
      }
      if (this.name.Contains(mousePosition) && mouseKey == KeyCode.Mouse1)
      {
        if (this.message.name == Game.Me.Name)
          return true;
        Player player = Game.Players.GetPlayer(this.message.name);
        if (player == null)
        {
          Log.Warning((object) ("player with name " + this.message.name + " not found!!"));
          return false;
        }
        GUISlotPopup guiSlotPopup = Game.GUIManager.Find<GUIChatNew>().Popup;
        guiSlotPopup.Clear();
        guiSlotPopup.Player = player;
        guiSlotPopup.AppendPartyMenu();
        guiSlotPopup.Parent = this.name.SmartRect;
        guiSlotPopup.Position = float2.zero;
        guiSlotPopup.PlaceAboveOf((GUIPanel) this.name, (float) ((double) guiSlotPopup.Width / 2.0 - 10.0), 0.0f);
        guiSlotPopup.IsRendered = true;
        if (Game.Me.ActiveShip.IsCapitalShip)
          guiSlotPopup.duel.IsRendered = false;
        return true;
      }
    }
    return false;
  }
}
