﻿// Decompiled with JetBrains decompiler
// Type: DialogMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System.Collections.Generic;
using UnityEngine;

public class DialogMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "DialogMediator";
  private DialogManager dialogManager;
  private DialogProtocol dialogProtocol;
  private IDialog dialog;

  public DialogMediator()
    : base("DialogMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.DialogRegister);
    this.AddMessageInterest(Message.DialogReplyAction);
    this.AddMessageInterest(Message.DialogReplyStopped);
    this.AddMessageInterest(Message.DialogReplyPcRemarks);
    this.AddMessageInterest(Message.DialogReplyNpcRemark);
    this.AddMessageInterest(Message.DialogClickedIndex);
    this.AddMessageInterest(Message.DialogAdvance);
    this.AddMessageInterest(Message.DialogToggleVisibility);
    this.AddMessageInterest(Message.DialogStop);
    this.AddMessageInterest(Message.LoadNewLevel);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.DialogReplyNpcRemark:
        this.dialogManager.ShowNpcMark((Remark) message.Data);
        break;
      case Message.DialogReplyPcRemarks:
        this.dialogManager.ShowPcMark((List<Remark>) message.Data);
        break;
      case Message.DialogReplyStopped:
        NGUITools.SetActive(this.dialogManager.gameObject, false);
        Game.ProtocolManager.UnregisterProtocol((BgoProtocol) this.dialogProtocol);
        this.dialogProtocol = (DialogProtocol) null;
        ((RoomStateNewDialog) RoomLevel.GetLevel().RoomState).OnStopped();
        this.dialogManager.OnStopDialog();
        this.OwnerFacade.SendMessage(Message.ToggleInputActive, (object) true);
        break;
      case Message.DialogReplyAction:
        this.dialog.ReceiveAction((DialogAction) message.Data);
        break;
      case Message.DialogClickedIndex:
        this.GetDialogProtocoll().Say((int) message.Data);
        break;
      case Message.DialogAdvance:
        this.GetDialogProtocoll().Advance();
        break;
      case Message.DialogRegister:
        this.dialogProtocol = this.GetDialogProtocoll();
        this.dialog = (IDialog) message.Data;
        NGUITools.SetActive(this.dialogManager.gameObject, true);
        this.dialogManager.NpcName = this.dialog.NpcName();
        this.OwnerFacade.SendMessage(Message.ToggleInputActive, (object) false);
        break;
      case Message.DialogToggleVisibility:
        NGUITools.SetActive(this.dialogManager.gameObject, !this.dialogManager.gameObject.activeInHierarchy);
        this.OwnerFacade.SendMessage(Message.ToggleInputActive, (object) true);
        break;
      case Message.DialogStop:
        this.GetDialogProtocoll().Stop();
        break;
      case Message.UiCreated:
        this.CreateView();
        break;
      case Message.LoadNewLevel:
        NGUITools.SetActive(this.dialogManager.gameObject, false);
        break;
      default:
        Debug.LogError((object) ("Unexpected Message was caught in Dialog Mediator => " + (object) message.Id));
        break;
    }
  }

  private DialogProtocol GetDialogProtocoll()
  {
    this.dialogProtocol = Game.ProtocolManager.GetProtocol(BgoProtocol.ProtocolID.Dialog) as DialogProtocol;
    if (this.dialogProtocol == null)
    {
      this.dialogProtocol = new DialogProtocol();
      Game.ProtocolManager.RegisterProtocol((BgoProtocol) this.dialogProtocol);
    }
    return this.dialogProtocol;
  }

  private void CreateView()
  {
    GameObject gameObject = Resources.Load("GUI/gui_2013/gui_2013", typeof (GameObject)) as GameObject;
    if ((Object) gameObject == (Object) null)
      Debug.LogError((object) "Atlas not found: GUI/gui_2013/ gui_2013");
    UIAtlas component = gameObject.GetComponent<UIAtlas>();
    GuiDataProvider guiDataProvider = this.OwnerFacade.FetchDataProvider("GuiDataProvider") as GuiDataProvider;
    GameObject correctedGameObject1 = PositionUtils.CreateCorrectedGameObject(guiDataProvider.uiHook, "DialogManager");
    GameObject correctedGameObject2 = PositionUtils.CreateCorrectedGameObject(correctedGameObject1, "DialogHolder");
    UITable uiTable = correctedGameObject2.AddComponent<UITable>();
    uiTable.columns = 1;
    uiTable.padding.y = 2f;
    UIAnchor uiAnchor1 = correctedGameObject2.AddComponent<UIAnchor>();
    uiAnchor1.side = UIAnchor.Side.Bottom;
    uiAnchor1.pixelOffset.x = -490f;
    uiAnchor1.pixelOffset.y = 155f;
    uiAnchor1.container = guiDataProvider.uiHook;
    GameObject correctedGameObject3 = PositionUtils.CreateCorrectedGameObject(correctedGameObject1, "HigherBackground");
    UISprite uiSprite1 = correctedGameObject3.AddComponent<UISprite>();
    uiSprite1.atlas = component;
    uiSprite1.type = UIBasicSprite.Type.Sliced;
    uiSprite1.spriteName = "ContainerBackground";
    uiSprite1.color = new Color(0.05882353f, 0.05882353f, 0.05882353f, 0.9607843f);
    uiSprite1.width = 4000;
    uiSprite1.height = 500;
    uiSprite1.pivot = UIWidget.Pivot.Bottom;
    UIAnchor uiAnchor2 = correctedGameObject3.AddComponent<UIAnchor>();
    uiAnchor2.side = UIAnchor.Side.Top;
    uiAnchor2.pixelOffset.y = -100f;
    uiAnchor2.runOnlyOnce = false;
    uiAnchor2.container = guiDataProvider.uiHook;
    GameObject correctedGameObject4 = PositionUtils.CreateCorrectedGameObject(correctedGameObject1, "LowerBackground");
    UISprite uiSprite2 = correctedGameObject4.AddComponent<UISprite>();
    uiSprite2.atlas = component;
    uiSprite2.type = UIBasicSprite.Type.Sliced;
    uiSprite2.spriteName = "ContainerBackground";
    uiSprite2.color = new Color(0.05882353f, 0.05882353f, 0.05882353f, 0.9607843f);
    uiSprite2.width = 4000;
    uiSprite2.height = 500;
    uiSprite2.pivot = UIWidget.Pivot.Top;
    UIAnchor uiAnchor3 = correctedGameObject4.AddComponent<UIAnchor>();
    uiAnchor3.side = UIAnchor.Side.Bottom;
    uiAnchor3.pixelOffset.y = 165f;
    uiAnchor3.runOnlyOnce = false;
    uiAnchor3.container = guiDataProvider.uiHook;
    GameObject correctedGameObject5 = PositionUtils.CreateCorrectedGameObject(correctedGameObject1, "SpaceInfoHolder");
    UISprite uiSprite3 = correctedGameObject5.AddComponent<UISprite>();
    uiSprite3.atlas = component;
    uiSprite3.spriteName = "icon_mouse_click";
    uiSprite3.type = UIBasicSprite.Type.Simple;
    uiSprite3.width = 28;
    uiSprite3.height = 28;
    uiSprite3.depth = 5;
    UIAnchor uiAnchor4 = correctedGameObject5.AddComponent<UIAnchor>();
    uiAnchor4.side = UIAnchor.Side.Bottom;
    uiAnchor4.pixelOffset.y = 25f;
    uiAnchor4.runOnlyOnce = false;
    uiAnchor4.container = guiDataProvider.uiHook;
    TweenAlpha tweenAlpha = correctedGameObject5.AddComponent<TweenAlpha>();
    tweenAlpha.from = 0.0f;
    tweenAlpha.to = 1f;
    tweenAlpha.method = UITweener.Method.Linear;
    tweenAlpha.style = UITweener.Style.PingPong;
    GameObject correctedGameObject6 = PositionUtils.CreateCorrectedGameObject(correctedGameObject1, "ExitBtn");
    DialogExitBtn dialogExitBtn = correctedGameObject6.AddComponent<DialogExitBtn>();
    dialogExitBtn.handleClick = (AnonymousDelegate) (() => this.GetDialogProtocoll().Stop());
    UISprite uiSprite4 = correctedGameObject6.AddComponent<UISprite>();
    uiSprite4.atlas = component;
    uiSprite4.spriteName = "btn_dialog_exit";
    uiSprite4.type = UIBasicSprite.Type.Simple;
    uiSprite4.pivot = UIWidget.Pivot.BottomRight;
    uiSprite4.width = 44;
    uiSprite4.height = 44;
    uiSprite4.depth = 8;
    dialogExitBtn.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 90f);
    UIAnchor uiAnchor5 = correctedGameObject6.AddComponent<UIAnchor>();
    uiAnchor5.side = UIAnchor.Side.BottomRight;
    uiAnchor5.runOnlyOnce = false;
    uiAnchor5.container = guiDataProvider.uiHook;
    uiAnchor5.pixelOffset.y = 165f;
    NGUITools.AddWidgetCollider(correctedGameObject6);
    this.dialogManager = correctedGameObject1.AddComponent<DialogManager>();
    this.dialogManager.dialogHolder = correctedGameObject2;
    this.dialogManager.spacetextHolder = correctedGameObject5;
    correctedGameObject1.SetActive(false);
  }
}
