﻿// Decompiled with JetBrains decompiler
// Type: FlakExplosionTimer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FlakExplosionTimer : MonoBehaviour
{
  [HideInInspector]
  public float m_lifeTime;
  private ParticleSystem m_explosion;

  private void Start()
  {
    this.m_explosion = this.gameObject.GetComponent<ParticleSystem>();
    this.m_lifeTime = this.m_explosion.duration;
  }

  private void FixedUpdate()
  {
    if ((double) this.m_lifeTime > 0.0)
      this.m_lifeTime -= Time.smoothDeltaTime;
    else
      this.m_explosion.Stop();
  }
}
