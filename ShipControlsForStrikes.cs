﻿// Decompiled with JetBrains decompiler
// Type: ShipControlsForStrikes
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ShipControlsForStrikes : ShipControlsBase
{
  private ShipControlsForStrikes.MouseControlMode mouseControlMode;
  private Vector3 pitchYawRollFactors;

  protected override void FlushInputs()
  {
    base.FlushInputs();
  }

  protected override bool TrySendNewInputs()
  {
    bool flag1 = false;
    bool flag2 = this.MouseInsideDeadZone();
    if (((false | this.mouseControlMode == ShipControlsForStrikes.MouseControlMode.ShipMovement ? 1 : 0) | (this.mouseControlMode != ShipControlsForStrikes.MouseControlMode.Ui ? 0 : (this.MouseBtnRightHeld ? 1 : 0))) != 0 & !flag2)
    {
      Vector3 pitchYawRollFactors = this.CalculatePitchYawRollFactors((Vector2) Input.mousePosition);
      if ((double) (this.pitchYawRollFactors - pitchYawRollFactors).sqrMagnitude > 9.99999974737875E-05)
      {
        this.pitchYawRollFactors = pitchYawRollFactors;
        flag1 = true;
      }
    }
    else
    {
      this.pitchYawRollFactors = Vector3.zero;
      if (flag2 || this.MouseBtnRightUp || this.MouseBtnMiddleUp)
        flag1 = true;
    }
    bool flag3 = flag1 & !SpaceLevel.GetLevel().SpaceCamera.CurrentBehavior.BlocksControls | this.qweasd.InputChanged;
    this.pitchYawRollFactors.z = (float) this.qweasd.Roll;
    Vector3 inputsConditioned = this.joystickSteeringInputs.JoystickXyzInputsConditioned;
    if (this.joystickAxesMovementInputHappened)
    {
      this.pitchYawRollFactors += inputsConditioned;
      this.pitchYawRollFactors = Utils.ClampVector(this.pitchYawRollFactors, -1f, 1f);
      flag3 = true;
    }
    if (flag3)
    {
      this.SendControl();
      this.FlushInputs();
    }
    return flag3;
  }

  private Vector3 CalculatePitchYawRollFactors(Vector2 mousePosition)
  {
    Quaternion rotation = Camera.main.transform.rotation;
    Vector3 vector3 = (Vector3) mousePosition;
    vector3.x -= (float) Screen.width / 2f;
    vector3.y -= (float) Screen.height / 2f;
    vector3 = Mathf.Min(vector3.magnitude, (float) Screen.height / 2f) * vector3.normalized;
    Vector3 zero = Vector3.zero;
    zero.x = (float) (-(double) vector3.y / ((double) Screen.height / 2.0));
    zero.y = vector3.x / ((float) Screen.height / 2f);
    return Quaternion.Inverse(Game.Me.Ship.Rotation) * rotation * zero;
  }

  private bool MouseInsideDeadZone()
  {
    return (double) new Vector2((float) (((double) Input.mousePosition.x - (double) Screen.width / 2.0) * 2.0) / (float) Screen.width, (float) (-((double) Input.mousePosition.y - (double) Screen.height / 2.0) * 2.0) / (float) Screen.height).sqrMagnitude < (double) Mathf.Pow(ShipControlsBase.DeadZoneMouseRadiusFactor / 8f, 2f);
  }

  public override void Move(Vector3 direction)
  {
    Vector3 eulerAngles = Quaternion.FromToRotation(Vector3.forward, direction).eulerAngles;
    GameProtocol.GetProtocol().TurnToDirectionStrikes(new Euler3(eulerAngles.x, eulerAngles.y, eulerAngles.z), 0.0f, new Vector2(0.0f, 0.0f));
  }

  public override void StopTurning()
  {
    float magnitude;
    Vector3 normalizedVector;
    Utils.CalculateNormalizedVectorAndMagnitude((Vector3) this.qweasd.DeriveDirectionVectorFromPressedKeys(), out magnitude, out normalizedVector);
    GameProtocol.GetProtocol().TurnByPitchYawStrikes(Vector3.zero, (Vector2) normalizedVector, magnitude);
  }

  private Vector2 GetStrafingMagnitudes()
  {
    return (Vector2) Utils.ClampVector((Vector3) (this.qweasd.DeriveDirectionVectorFromPressedKeys() + this.joystickSteeringInputs.GetStrafeInputs()), -1f, 1f);
  }

  protected override void SendControl()
  {
    if (this.SectorMapControlsEnabled && !this.joystickAxesMovementInputHappened)
    {
      this.gameProtocol.QWEASD(this.qweasd);
    }
    else
    {
      float magnitude;
      Vector3 normalizedVector;
      Utils.CalculateNormalizedVectorAndMagnitude((Vector3) this.GetStrafingMagnitudes(), out magnitude, out normalizedVector);
      this.gameProtocol.TurnByPitchYawStrikes(this.pitchYawRollFactors, (Vector2) normalizedVector, magnitude);
      if (!this.HighlightControl)
        return;
      if ((double) magnitude > 0.0)
        this.SendStoryProtocol(StoryProtocol.ControlType.Strafe);
      if ((double) (Vector2) this.pitchYawRollFactors.sqrMagnitude > 0.0)
        this.SendStoryProtocol(StoryProtocol.ControlType.Turn);
      if ((double) Mathf.Abs(this.pitchYawRollFactors.z) <= 0.0)
        return;
      this.SendStoryProtocol(StoryProtocol.ControlType.Roll);
    }
  }

  private void ToggleMouseControlMode()
  {
    this.mouseControlMode = (ShipControlsForStrikes.MouseControlMode) ((int) (this.mouseControlMode + 1) % 2);
    FacadeFactory.GetInstance().SendMessage(Message.ShipInputMouseAutoTurnToggle, (object) (this.mouseControlMode == ShipControlsForStrikes.MouseControlMode.ShipMovement));
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse1)
      FacadeFactory.GetInstance().SendMessage(Message.ShipInputMouseManualTurnToggle, (object) true);
    return base.OnMouseDown(mousePosition, mouseKey);
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse1)
      FacadeFactory.GetInstance().SendMessage(Message.ShipInputMouseManualTurnToggle, (object) false);
    return base.OnMouseUp(mousePosition, mouseKey);
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (action == Action.ToggleMovementMode)
      this.ToggleMouseControlMode();
    return base.OnKeyDown(keyboardKey, action);
  }

  private enum MouseControlMode
  {
    Ui,
    ShipMovement,
  }
}
