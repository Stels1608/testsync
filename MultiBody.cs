﻿// Decompiled with JetBrains decompiler
// Type: MultiBody
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class MultiBody : MultiLODListener
{
  public RendererArray[] Renderers;
  protected Renderer[] activeRenderers;

  public Renderer[] ActiveRenderers
  {
    get
    {
      return this.activeRenderers;
    }
  }

  protected override void Awake()
  {
    foreach (RendererArray renderer in this.Renderers)
    {
      foreach (Renderer element in renderer.Elements)
      {
        if ((Object) element == (Object) null)
          Debug.LogError((object) ("Renderer is null on GO " + (object) this.gameObject + " - " + (object) this));
        else
          element.enabled = false;
      }
    }
    base.Awake();
  }

  public void ReloadSkin()
  {
    this.OnLevelChanged();
  }

  protected override void OnLevelChanged()
  {
    if (this.activeRenderers != null)
    {
      foreach (Renderer activeRenderer in this.activeRenderers)
        activeRenderer.enabled = false;
      this.activeRenderers = (Renderer[]) null;
      this.SendMessage("OnRendererNulled", SendMessageOptions.DontRequireReceiver);
    }
    if (this.Level >= this.Renderers.Length)
      return;
    this.activeRenderers = this.Renderers[this.Level].Elements;
    foreach (Renderer activeRenderer in this.activeRenderers)
      activeRenderer.enabled = true;
    this.SendMessage("OnRendererChanged", (object) this.activeRenderers, SendMessageOptions.DontRequireReceiver);
  }

  protected override void Reset()
  {
    this.Renderers = this.FindLODs();
    if (this.Renderers.Length == 0)
      this.Renderers = new RendererArray[1];
    this.LevelLimits = new float[this.Renderers.Length];
    for (int index = 0; index < this.LevelLimits.Length; ++index)
      this.LevelLimits[index] = 1000f * (float) (index + 1);
  }

  protected RendererArray[] FindLODs()
  {
    List<RendererArray> rendererArrayList = new List<RendererArray>();
    for (int index = 0; index < this.transform.childCount; ++index)
    {
      GameObject gameObject = this.transform.GetChild(index).gameObject;
      if (gameObject.name.ToLower().Contains("_lod"))
      {
        Renderer[] componentsInChildren = gameObject.GetComponentsInChildren<Renderer>();
        rendererArrayList.Add(new RendererArray(componentsInChildren));
      }
    }
    return rendererArrayList.ToArray();
  }
}
