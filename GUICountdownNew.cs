﻿// Decompiled with JetBrains decompiler
// Type: GUICountdownNew
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUICountdownNew : GUIPanel
{
  private AtlasAnimation animation;
  private float progress;

  public float Progress
  {
    get
    {
      return this.animation.PlaybackPosition;
    }
    set
    {
      this.progress = Mathf.Clamp01(value);
      this.animation.PlaybackPosition = this.progress;
    }
  }

  public GUICountdownNew(Texture2D texture, uint width, uint height)
  {
    this.animation = new AtlasAnimation(texture, width, height);
    this.root.Width = (float) texture.width / (float) width;
    this.root.Height = (float) texture.height / (float) height;
    this.Progress = 0.0f;
    this.Name = "countdown";
    this.IsRendered = true;
  }

  public override void Draw()
  {
    base.Draw();
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    Graphics.DrawTexture(this.root.AbsRect, (Texture) this.animation.Texture, this.animation.FrameRect, 0, 0, 0, 0);
  }
}
