﻿// Decompiled with JetBrains decompiler
// Type: GUIScreenResolution
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public struct GUIScreenResolution
{
  public const int defaultWidth = 1024;
  public const int defaultHeight = 768;
  public int width;
  public int height;

  public GUIScreenResolution(int width, int height)
  {
    this.width = width;
    this.height = height;
  }

  public static bool operator ==(GUIScreenResolution lhs, GUIScreenResolution rhs)
  {
    return lhs.width == rhs.width && lhs.height == rhs.height;
  }

  public static bool operator !=(GUIScreenResolution lhs, GUIScreenResolution rhs)
  {
    return lhs.width != rhs.width && lhs.height != rhs.height;
  }

  public static bool IsDefault()
  {
    if (Screen.width == 1024)
      return Screen.height == 768;
    return false;
  }

  public override string ToString()
  {
    return string.Format("width:{0}, height:{1}", (object) this.width, (object) this.height);
  }

  public override int GetHashCode()
  {
    return base.GetHashCode();
  }

  public override bool Equals(object o)
  {
    return this == (GUIScreenResolution) o;
  }
}
