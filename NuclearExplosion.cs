﻿// Decompiled with JetBrains decompiler
// Type: NuclearExplosion
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class NuclearExplosion : MonoBehaviour
{
  public float effectSpeed = 100f;
  public float effectSize = 100f;
  public float effectRotationSpeed = 1f;
  private float size = 0.01f;
  public GameObject targetObject;
  public GameObject targetObject2;
  public NuclearExplosion.ComponentType componentType;
  private NuclearExplosion.TComponentUpdate ComponentUpdate;
  private float opacity;
  private float opacityTimer;
  private float rotation;
  private Color color;

  private void Start()
  {
    switch (this.componentType)
    {
      case NuclearExplosion.ComponentType.Undefined:
        this.ComponentUpdate = new NuclearExplosion.TComponentUpdate(this.Update_Undefined);
        break;
      case NuclearExplosion.ComponentType.Flash:
      case NuclearExplosion.ComponentType.FlashSlow:
        this.targetObject.transform.localScale = new Vector3(this.size, this.size, 1f);
        this.ComponentUpdate = new NuclearExplosion.TComponentUpdate(this.Update_Flash);
        break;
      case NuclearExplosion.ComponentType.Shockwave:
        this.targetObject.transform.localScale = new Vector3(this.size, this.size, 1f);
        this.targetObject2.transform.localScale = new Vector3(this.size, this.size, 1f);
        this.ComponentUpdate = new NuclearExplosion.TComponentUpdate(this.Update_Shockwave);
        break;
    }
    this.color = this.targetObject.GetComponent<Renderer>().sharedMaterial.GetColor("_TintColor");
  }

  private void Update()
  {
    this.ComponentUpdate();
  }

  private void Update_Undefined()
  {
  }

  private void Update_Flash()
  {
    this.size += this.effectSpeed * Time.deltaTime;
    this.opacityTimer = this.size / this.effectSize;
    this.opacity = this.componentType != NuclearExplosion.ComponentType.FlashSlow ? Mathf.Lerp(1f, 0.0f, this.opacityTimer) : Mathf.Lerp(1f, 0.0f, this.opacityTimer);
    this.color.a = this.opacity;
    this.targetObject.GetComponent<Renderer>().sharedMaterial.SetColor("_TintColor", this.color);
    this.targetObject.transform.localScale = new Vector3(this.size, 1f, this.size);
    this.targetObject.transform.LookAt(Camera.main.transform.position);
    this.targetObject.transform.Rotate(Vector3.left, 90f);
    if ((double) this.size < (double) this.effectSize)
      return;
    Object.Destroy((Object) this.targetObject);
    Object.Destroy((Object) this);
  }

  private void Update_Shockwave()
  {
    this.size += this.effectSpeed * Time.deltaTime;
    this.rotation += this.effectRotationSpeed * this.effectSpeed * Time.deltaTime;
    this.opacityTimer = this.size / this.effectSize;
    this.opacity = Mathf.Lerp(0.5f, 0.0f, this.opacityTimer);
    this.color.a = this.opacity;
    this.targetObject.GetComponent<Renderer>().sharedMaterial.SetColor("_TintColor", this.color);
    this.targetObject.transform.localScale = new Vector3(this.size, this.size, 1f);
    this.targetObject.transform.Rotate(Vector3.forward, this.rotation);
    this.targetObject2.GetComponent<Renderer>().sharedMaterial.SetColor("_TintColor", this.color);
    this.targetObject2.transform.localScale = new Vector3(this.size, this.size, 1f);
    this.targetObject2.transform.Rotate(Vector3.forward, -this.rotation);
    if ((double) this.size < (double) this.effectSize)
      return;
    Object.Destroy((Object) this.targetObject);
    Object.Destroy((Object) this.targetObject2);
    Object.Destroy((Object) this);
  }

  public enum ComponentType
  {
    Undefined,
    Flash,
    FlashSlow,
    Shockwave,
  }

  private delegate void TComponentUpdate();
}
