﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DGridView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using SystemMap3D;
using UnityEngine;
using UnityEngine.UI;

public class SystemMap3DGridView : MonoBehaviour, ISystemMap3DView
{
  [SerializeField]
  private SystemMap3DGrid grid;
  [SerializeField]
  private GameObject gridMarker;
  private SystemMap3DManager systemMap3DManager;
  private int gridLayer;

  public bool IsRendered
  {
    get
    {
      return this.gameObject.activeSelf;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  public static SystemMap3DGridView Create(SystemMap3DManager systemMap3DManager)
  {
    SystemMap3DGridView child = UguiTools.CreateChild<SystemMap3DGridView>("SystemMap3D/SystemMap3DGridView", UguiTools.GetCanvas(BsgoCanvas.SystemMapCanvas3D).transform);
    child.Initialize(systemMap3DManager);
    return child;
  }

  public void Initialize(SystemMap3DManager systemMap3DManager)
  {
    this.systemMap3DManager = systemMap3DManager;
    this.gridLayer = LayerMask.NameToLayer("Ignore Raycast");
    this.grid.transform.position = Vector3.zero;
    this.gridMarker.transform.SetParent(this.grid.gameObject.transform, false);
  }

  private void LateUpdate()
  {
    if ((UnityEngine.Object) this.systemMap3DManager == (UnityEngine.Object) null)
      return;
    Vector3? mouseOnGridPosition = this.GetMouseOnGridPosition();
    this.ShowMousePositionOnGrid(mouseOnGridPosition);
    this.ProcessMouseInput(mouseOnGridPosition);
  }

  public void SetDiameter(float gridDiameter)
  {
    this.grid.transform.localScale = new Vector3(gridDiameter, gridDiameter, 1f);
  }

  private Vector3? GetMouseOnGridPosition()
  {
    if (this.systemMap3DManager.IconView.MouseIsHoveringEntity)
      return new Vector3?();
    Ray ray = this.systemMap3DManager.CameraView.MapCamera.ScreenPointToRay(Input.mousePosition);
    Debug.DrawLine(ray.origin, ray.direction * 20000f, Color.red);
    foreach (RaycastHit raycastHit in Physics.RaycastAll(ray, float.MaxValue, 1 << this.gridLayer))
    {
      if ((UnityEngine.Object) raycastHit.collider.gameObject.GetComponent<SystemMap3DGrid>() != (UnityEngine.Object) null)
      {
        this.gridMarker.transform.position = raycastHit.point;
        return new Vector3?(raycastHit.point);
      }
    }
    return new Vector3?();
  }

  private void ShowMousePositionOnGrid(Vector3? mouseOnGridPosition)
  {
    this.gridMarker.SetActive(mouseOnGridPosition.HasValue);
    if (!mouseOnGridPosition.HasValue)
      return;
    this.gridMarker.transform.position = mouseOnGridPosition.Value;
  }

  private void ProcessMouseInput(Vector3? mouseOnGridPosition)
  {
    if (!Input.GetMouseButtonDown(1) || !mouseOnGridPosition.HasValue)
      return;
    Vector3 vector3 = mouseOnGridPosition.Value;
    this.gridMarker.GetComponent<Animation>().Stop();
    this.gridMarker.GetComponent<Animation>().Play("GridMarkerAnim");
    SpaceLevel.GetLevel().ShipControls.Move(vector3 - Game.Me.Ship.Position);
  }

  public void TransitionToMap(TransitionMode transitionMode)
  {
    Color from = new Color(1f, 1f, 1f, 0.0f);
    Color to = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
    this.IsRendered = true;
    LeanTween.value(this.grid.gameObject, from, to, 1f).setOnUpdate((System.Action<Color>) (val =>
    {
      foreach (Graphic componentsInChild in this.grid.gameObject.GetComponentsInChildren<Image>())
        componentsInChild.color = val;
      foreach (Graphic componentsInChild in this.grid.gameObject.GetComponentsInChildren<Text>())
        componentsInChild.color = val;
    }));
  }

  public void TransitionToGame(TransitionMode transitionMode)
  {
    this.IsRendered = false;
  }

  public void OnTransitionToGameFinished()
  {
  }

  public void ApplySetting(UserSetting setting, object data)
  {
  }

  public void Destroy()
  {
    this.DestroyGrid();
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }

  private void DestroyGrid()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.grid);
  }
}
