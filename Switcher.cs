﻿// Decompiled with JetBrains decompiler
// Type: Switcher
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class Switcher : SingleLODListener
{
  public Component[] Components;

  protected override void OnSwitched(bool value)
  {
    foreach (Component component in this.Components)
    {
      if (component is Behaviour)
        (component as Behaviour).enabled = value;
      else if (component is Renderer)
        (component as Renderer).enabled = value;
    }
  }

  private void Reset()
  {
    Component[] components = this.GetComponents<Component>();
    List<Component> componentList = new List<Component>();
    foreach (Component component in components)
    {
      if ((Object) component != (Object) this && (component is Behaviour || component is Renderer))
        componentList.Add(component);
    }
    this.Components = componentList.ToArray();
  }
}
