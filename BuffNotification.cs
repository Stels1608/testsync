﻿// Decompiled with JetBrains decompiler
// Type: BuffNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class BuffNotification : OnScreenNotification
{
  private readonly string message;
  private readonly NotificationCategory category;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.Buff;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return this.category;
    }
  }

  public override string TextMessage
  {
    get
    {
      return BsgoLocalization.Get(this.message);
    }
  }

  public override bool Show
  {
    get
    {
      if (OnScreenNotification.ShowAugmentMessages)
        return !string.IsNullOrEmpty(this.message);
      return false;
    }
  }

  public BuffNotification(ShipBuff buff)
  {
    AbilityActionType buffActionType = buff.Ability.BuffActionType;
    switch (buffActionType)
    {
      case AbilityActionType.Buff:
        this.message = "%$bgo.game_protocol.add_buf%";
        this.category = NotificationCategory.Positive;
        break;
      case AbilityActionType.Debuff:
        this.message = "%$bgo.game_protocol.remove_buf%";
        this.category = NotificationCategory.Negative;
        break;
      default:
        if (buffActionType != AbilityActionType.DevBuff)
          break;
        this.message = "Developer-only buff active";
        this.category = NotificationCategory.Positive;
        break;
    }
  }
}
