﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorDesignatorUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class HudIndicatorDesignatorUgui : HudIndicatorBaseComponentUgui, IRequiresLateUpdate
{
  private const float mc_stillTriDistanceFromCenter = 12f;
  private const float mc_minTriDistanceFromCenter = 15.5f;
  private const float mc_maxTriDistanceFromCenter = 25f;
  [SerializeField]
  private Transform outerTriangleLeftTransform;
  [SerializeField]
  private Transform outerTriangleRightTransform;
  [SerializeField]
  private Transform outerTriangleTopTransform;
  private bool m_playAnimation;

  private bool IsVisible
  {
    get
    {
      if (this.IsDesignated)
        return this.InScreen;
      return false;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.ApplyStillPosition();
    foreach (Graphic componentsInChild in this.GetComponentsInChildren<Image>())
      componentsInChild.color = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_SPECIAL);
  }

  protected override void ResetComponentStates()
  {
    this.ApplyStillPosition();
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    this.transform.localPosition = !this.InMiniMode ? Vector3.zero : new Vector3(0.0f, 3f, 0.0f);
    this.gameObject.SetActive(this.IsVisible);
  }

  public void RemoteLateUpdate()
  {
    if (!this.gameObject.activeSelf)
      return;
    if (this.IsSelected)
    {
      if (this.m_playAnimation)
        this.m_playAnimation = false;
    }
    else if (!this.m_playAnimation)
      this.m_playAnimation = true;
    this.Animate();
  }

  private void ApplyStillPosition()
  {
    this.outerTriangleLeftTransform.localPosition = (Vector3) (new Vector2(-1f, -1f) * 12f + new Vector2(-1f, 0.5f));
    this.outerTriangleRightTransform.localPosition = (Vector3) (new Vector2(1f, -1f) * 12f + new Vector2(1.5f, 0.5f));
    this.outerTriangleTopTransform.localPosition = (Vector3) (new Vector2(0.0f, 1f) * 12f + new Vector2(0.0f, -1f));
  }

  private void Animate()
  {
    if (!this.m_playAnimation)
    {
      this.ApplyStillPosition();
    }
    else
    {
      float num = (float) (15.5 + ((double) Mathf.Sin(Time.time * 5f) + 1.0) / 2.0 * 9.5);
      this.outerTriangleLeftTransform.localPosition = new Vector3(1f, -1f) * num + new Vector3(2.5f, 2f);
      this.outerTriangleRightTransform.localPosition = new Vector3(-1f, -1f) * num + new Vector3(-1.5f, 2f);
      this.outerTriangleTopTransform.localPosition = new Vector3(0.0f, 1f) * num;
    }
  }
}
