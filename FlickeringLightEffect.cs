﻿// Decompiled with JetBrains decompiler
// Type: FlickeringLightEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FlickeringLightEffect : MonoBehaviour
{
  public float max = 1f;
  public float isTrueValue = 0.7f;
  public Light flashingLight;
  public float min;

  public virtual void Start()
  {
    this.flashingLight.enabled = false;
  }

  public virtual void FixedUpdate()
  {
    this.flashingLight.enabled = (double) Random.Range(this.min, this.max) <= (double) this.isTrueValue;
  }
}
