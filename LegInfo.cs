﻿// Decompiled with JetBrains decompiler
// Type: LegInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class LegInfo
{
  public Transform hip;
  public Transform ankle;
  public Transform toe;
  public float footWidth;
  public float footLength;
  public Vector2 footOffset;
  [HideInInspector]
  public Transform[] legChain;
  [HideInInspector]
  public Transform[] footChain;
  [HideInInspector]
  public float legLength;
  [HideInInspector]
  public Vector3 ankleHeelVector;
  [HideInInspector]
  public Vector3 toeToetipVector;
  [HideInInspector]
  public Color debugColor;
}
