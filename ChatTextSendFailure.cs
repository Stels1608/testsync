﻿// Decompiled with JetBrains decompiler
// Type: ChatTextSendFailure
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

internal class ChatTextSendFailure : ChatProtocolParser
{
  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split('#');
    if (strArray.Length != 2)
      return false;
    string key = strArray[0];
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ChatTextSendFailure.\u003C\u003Ef__switch\u0024map8 == null)
      {
        // ISSUE: reference to a compiler-generated field
        ChatTextSendFailure.\u003C\u003Ef__switch\u0024map8 = new Dictionary<string, int>(5)
        {
          {
            "cd",
            0
          },
          {
            "ce",
            1
          },
          {
            "cu",
            2
          },
          {
            "da",
            3
          },
          {
            "ct",
            4
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (ChatTextSendFailure.\u003C\u003Ef__switch\u0024map8.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            this.type = ChatProtocolParserType.TextFailureBan;
            break;
          case 1:
            this.type = ChatProtocolParserType.TextFailureKick;
            break;
          case 2:
            this.type = ChatProtocolParserType.TextFailureCannotWhisperSelf;
            break;
          case 3:
            this.type = ChatProtocolParserType.TextFailureFlood;
            break;
          case 4:
            this.type = ChatProtocolParserType.TextFailureUserNotExists;
            break;
          default:
            goto label_12;
        }
        return true;
      }
    }
label_12:
    return false;
  }
}
