﻿// Decompiled with JetBrains decompiler
// Type: GuiDockUndock
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class GuiDockUndock : GuiPanel
{
  private static readonly Color BUTTON_INACTIVE_COLOR = Tools.Color((int) byte.MaxValue, (int) byte.MaxValue, (int) byte.MaxValue, 64);
  private static readonly Color ANIM_INACTIVE_COLOR = Tools.Color((int) byte.MaxValue, (int) byte.MaxValue, (int) byte.MaxValue, 32);
  private static int undockOffsetY = 50;
  private readonly GuiLabel countdown = new GuiLabel(Gui.Options.FontBGM_BT, 20);
  private readonly GuiButton button = new GuiButton("%$bgo.etc.dock%");
  private readonly GuiAtlasImageBase animationAtlas = new GuiAtlasImageBase(ResourceLoader.Load<Texture2D>("GUI/Slots/docklight"), new Vector2(50f, 44f));
  private readonly Gui.Timer animTimer = new Gui.Timer(0.05f);
  private const float UPDATE_RATE = 0.3f;
  private const string dockText = "%$bgo.etc.dock%";
  private const string undockText = "%$bgo.RoomGUI.gui_undock_layout.label%";
  private const string launchStrikesText = "%$bgo.carrier.launch_all_strikes%";
  private static GuiDockUndock instance;
  private static bool dockState;
  private static bool undockState;
  private static bool launchStrikesState;
  private bool dockingRequested;
  private GUISlotManager slotManager;
  private float dockDelay;

  public static GuiDockUndock Instance
  {
    get
    {
      return GuiDockUndock.instance ?? (GuiDockUndock.instance = new GuiDockUndock());
    }
  }

  public GuiDockUndock()
  {
    this.Align = Align.UpCenter;
    this.Position = new Vector2(0.0f, (float) GuiDockUndock.undockOffsetY);
    this.MouseTransparent = true;
    this.button.SizeY = 30f;
    this.animTimer.Event = (AnonymousDelegate) (() => ++this.animationAtlas.TextureIndex);
    this.countdown.Alignment = TextAnchor.UpperCenter;
    this.AddChild((GuiElementBase) this.animationAtlas, Align.UpCenter);
    this.AddChild((GuiElementBase) this.button, Align.UpCenter, new Vector2(0.0f, this.animationAtlas.SizeY + 5f));
    this.AddChild((GuiElementBase) this.countdown, Align.UpCenter);
    this.AddChild((GuiElementBase) this.animTimer);
    this.AddChild((GuiElementBase) new Gui.Timer(0.3f));
    Game.GUIManager.AddPanel((IGUIPanel) this);
    this.IsUpdated = true;
  }

  private bool CancelDocking()
  {
    if ((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null || !SpaceLevel.GetLevel().Docking)
      return false;
    GameProtocol.GetProtocol().CancelDocking();
    SpaceLevel.GetLevel().Docking = false;
    this.dockDelay = 0.0f;
    return true;
  }

  public override bool KeyUp(KeyCode key, Action action)
  {
    if (action == Action.CancelJump && this.CancelDocking())
      return true;
    return this.KeyDown(key, action);
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    SpaceObject spaceObject = (SpaceObject) null;
    GuiDockUndock.dockState = false;
    if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null)
    {
      spaceObject = SpaceLevel.GetLevel().GetPlayerTarget();
      if (spaceObject != null)
        GuiDockUndock.dockState = spaceObject.CanDock(true) && !this.dockingRequested;
      GuiDockUndock.dockState |= (double) this.dockDelay > 0.0;
    }
    if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null && this.slotManager == null)
      this.slotManager = Game.GUIManager.Find<GUISlotManager>();
    this.IsRendered = GuiDockUndock.dockState || GuiDockUndock.undockState || GuiDockUndock.launchStrikesState;
    this.Position = new Vector2(0.0f, (float) GuiDockUndock.undockOffsetY);
    this.Align = Align.UpCenter;
    if (this.slotManager != null)
    {
      GUISlot guiSlot = (GUISlot) this.slotManager.target;
      if (this.IsRendered && guiSlot != null && guiSlot.IsRendered)
      {
        float2 position = guiSlot.Position;
        this.Position = new Vector2(position.x + this.slotManager.target.SmartRect.Width, position.y);
        this.Align = Align.UpLeft;
      }
    }
    bool flag = false;
    TimeSpan time = TimeSpan.FromSeconds(0.0);
    if (GuiDockUndock.dockState)
    {
      flag = (double) this.dockDelay > 0.0;
      time = TimeSpan.FromSeconds((double) this.dockDelay);
      this.animTimer.IsRendered = true;
      this.countdown.IsRendered = flag;
      this.animationAtlas.OverlayColor = new Color?();
      this.button.OverlayColor = new Color?();
      this.button.Label.Text = "%$bgo.etc.dock%";
      this.button.HandleMouseInput = (double) this.dockDelay <= 0.0;
      if (spaceObject is PlayerShip && GalaxyMapMain.JumpActive)
      {
        this.button.HandleMouseInput = false;
        this.button.OverlayColor = new Color?(GuiDockUndock.BUTTON_INACTIVE_COLOR);
        this.animationAtlas.OverlayColor = new Color?(GuiDockUndock.ANIM_INACTIVE_COLOR);
      }
      else
      {
        GuiButton guiButton = this.button;
        GuiAtlasImageBase guiAtlasImageBase = this.animationAtlas;
        Color? nullable1 = new Color?();
        Color? nullable2;
        Color? nullable3 = nullable2 = nullable1;
        guiAtlasImageBase.OverlayColor = nullable2;
        Color? nullable4 = nullable3;
        guiButton.OverlayColor = nullable4;
      }
      this.button.Pressed = (AnonymousDelegate) (() =>
      {
        if (Game.Me.ActiveShip.IsCapitalShip)
          MessageBoxFactory.CreateDialogBox(BsgoCanvas.Windows).SetContent((OnMessageBoxClose) (actionType =>
          {
            if (actionType != MessageBoxActionType.Ok)
              return;
            SpaceLevel.GetLevel().Dock();
          }), "%$bgo.etc.undock_capital_ship_confirmation_title%", "%$bgo.etc.undock_capital_ship_confirmation%", 0U);
        else
          SpaceLevel.GetLevel().Dock();
      });
      this.dockDelay -= 0.3f;
    }
    else if (GuiDockUndock.undockState || GuiDockUndock.launchStrikesState)
    {
      flag = Game.Me.Caps.IsRestricted(Capability.Undock);
      this.animTimer.IsRendered = !flag;
      this.countdown.IsRendered = flag;
      this.button.Label.Text = !GuiDockUndock.launchStrikesState ? "%$bgo.RoomGUI.gui_undock_layout.label%" : "%$bgo.carrier.launch_all_strikes%";
      if (!flag)
      {
        this.animationAtlas.OverlayColor = new Color?();
        this.button.OverlayColor = new Color?();
        this.button.Pressed = (AnonymousDelegate) (() =>
        {
          if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null && Game.Me.Anchored)
            GameProtocol.GetProtocol().RequestUnanchor();
          else if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null && SpaceLevel.GetLevel().GetActualPlayerShip() != null && SpaceLevel.GetLevel().GetActualPlayerShip().ShipCardLight.ShipRoleDeprecated == ShipRoleDeprecated.Carrier)
            GameProtocol.GetProtocol().RequestLaunchStrikes();
          else
            RoomProtocol.GetProtocol().Quit();
        });
      }
      else
        time = Game.Me.Caps.Timeout(Capability.Undock);
    }
    if (!flag)
      return;
    this.countdown.Text = !(time > TimeSpan.Zero) ? Tools.FormatTime(TimeSpan.Zero) : Tools.FormatTime(time);
    this.button.OverlayColor = new Color?(GuiDockUndock.BUTTON_INACTIVE_COLOR);
    this.animationAtlas.OverlayColor = new Color?(GuiDockUndock.ANIM_INACTIVE_COLOR);
    this.button.Pressed = (AnonymousDelegate) (() => {});
  }

  public void DockingRequested(float dockDelay)
  {
    this.dockDelay = dockDelay;
  }

  public static void ShowUndock()
  {
    GuiDockUndock.undockState = true;
    GuiDockUndock.Instance.PeriodicUpdate();
  }

  public static void HideUndock()
  {
    GuiDockUndock.undockState = false;
    GuiDockUndock.Instance.PeriodicUpdate();
  }

  public static void ShowLaunchStrikes()
  {
    GuiDockUndock.launchStrikesState = true;
    GuiDockUndock.Instance.PeriodicUpdate();
  }

  public static void HideLaunchStrikes()
  {
    GuiDockUndock.launchStrikesState = false;
    GuiDockUndock.Instance.PeriodicUpdate();
  }

  public static void Reinitialize()
  {
    if ((UnityEngine.Object) RoomLevel.GetLevel() != (UnityEngine.Object) null)
    {
      GuiDockUndock.undockState = true;
      GuiDockUndock.launchStrikesState = false;
    }
    else if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null)
    {
      if (!Game.Me.Anchored)
        GuiDockUndock.undockState = false;
      if (Game.Me.Party.Anchored.ContainsKey(Game.Me.ServerID))
        GuiDockUndock.launchStrikesState = Game.Me.Party.Anchored[Game.Me.ServerID].Count > 0;
    }
    GuiDockUndock.instance = new GuiDockUndock();
  }
}
