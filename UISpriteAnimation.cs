﻿// Decompiled with JetBrains decompiler
// Type: UISpriteAnimation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (UISprite))]
[ExecuteInEditMode]
[AddComponentMenu("NGUI/UI/Sprite Animation")]
public class UISpriteAnimation : MonoBehaviour
{
  [HideInInspector]
  [SerializeField]
  protected int mFPS = 30;
  [HideInInspector]
  [SerializeField]
  protected string mPrefix = string.Empty;
  [SerializeField]
  [HideInInspector]
  protected bool mLoop = true;
  [HideInInspector]
  [SerializeField]
  protected bool mSnap = true;
  protected bool mActive = true;
  protected List<string> mSpriteNames = new List<string>();
  protected UISprite mSprite;
  protected float mDelta;
  protected int mIndex;

  public int frames
  {
    get
    {
      return this.mSpriteNames.Count;
    }
  }

  public int framesPerSecond
  {
    get
    {
      return this.mFPS;
    }
    set
    {
      this.mFPS = value;
    }
  }

  public string namePrefix
  {
    get
    {
      return this.mPrefix;
    }
    set
    {
      if (!(this.mPrefix != value))
        return;
      this.mPrefix = value;
      this.RebuildSpriteList();
    }
  }

  public bool loop
  {
    get
    {
      return this.mLoop;
    }
    set
    {
      this.mLoop = value;
    }
  }

  public bool isPlaying
  {
    get
    {
      return this.mActive;
    }
  }

  protected virtual void Start()
  {
    this.RebuildSpriteList();
  }

  protected virtual void Update()
  {
    if (!this.mActive || this.mSpriteNames.Count <= 1 || (!Application.isPlaying || this.mFPS <= 0))
      return;
    this.mDelta += RealTime.deltaTime;
    float num = 1f / (float) this.mFPS;
    if ((double) num >= (double) this.mDelta)
      return;
    this.mDelta = (double) num <= 0.0 ? 0.0f : this.mDelta - num;
    if (++this.mIndex >= this.mSpriteNames.Count)
    {
      this.mIndex = 0;
      this.mActive = this.mLoop;
    }
    if (!this.mActive)
      return;
    this.mSprite.spriteName = this.mSpriteNames[this.mIndex];
    if (!this.mSnap)
      return;
    this.mSprite.MakePixelPerfect();
  }

  public void RebuildSpriteList()
  {
    if ((Object) this.mSprite == (Object) null)
      this.mSprite = this.GetComponent<UISprite>();
    this.mSpriteNames.Clear();
    if (!((Object) this.mSprite != (Object) null) || !((Object) this.mSprite.atlas != (Object) null))
      return;
    List<UISpriteData> spriteList = this.mSprite.atlas.spriteList;
    int index = 0;
    for (int count = spriteList.Count; index < count; ++index)
    {
      UISpriteData uiSpriteData = spriteList[index];
      if (string.IsNullOrEmpty(this.mPrefix) || uiSpriteData.name.StartsWith(this.mPrefix))
        this.mSpriteNames.Add(uiSpriteData.name);
    }
    this.mSpriteNames.Sort();
  }

  public void Play()
  {
    this.mActive = true;
  }

  public void Pause()
  {
    this.mActive = false;
  }

  public void ResetToBeginning()
  {
    this.mActive = true;
    this.mIndex = 0;
    if (!((Object) this.mSprite != (Object) null) || this.mSpriteNames.Count <= 0)
      return;
    this.mSprite.spriteName = this.mSpriteNames[this.mIndex];
    if (!this.mSnap)
      return;
    this.mSprite.MakePixelPerfect();
  }
}
