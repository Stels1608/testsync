﻿// Decompiled with JetBrains decompiler
// Type: StickerSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

internal class StickerSlot : ShipCustomizationSlot
{
  private Sticker sticker;
  private System.Action<Sticker> updateSticker;

  public StickerSlot(Sticker sticker, System.Action<Sticker> updateSticker)
  {
    this.updateSticker = updateSticker;
    Texture2D texture = (Texture2D) ResourceLoader.Load(sticker.Texture);
    this.sticker = sticker;
    GuiImage guiImage = new GuiImage(texture);
    guiImage.PositionX += 30f;
    guiImage.Size = new Vector2(40f, 40f);
    this.AddChild((GuiElementBase) guiImage);
  }

  public override void OnSelected()
  {
    if (this.updateSticker == null)
      return;
    this.updateSticker(this.sticker);
  }
}
