﻿// Decompiled with JetBrains decompiler
// Type: ShipView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Common;
using System.Collections;
using System.Diagnostics;
using Unity5AssetHandling;
using UnityEngine;

public class ShipView : GuiPanel
{
  public Vector3 shipViewOffset = new Vector3(0.0f, 50000f, 0.0f);
  public bool ShowRust = true;
  public float OutRadiusFactor = 1.3f;
  private string prefabName = string.Empty;
  private string skinName = string.Empty;
  private float autoRotationY = 35f;
  private float fModelInitialRadiusFactor = 1.1f;
  private readonly GuiLabel labelLoading = new GuiLabel("%$bgo.etc.ship_shop_loading%", Gui.Options.FontBGM_BT, 20);
  private readonly GuiImage shipBanner = new GuiImage();
  private const float MS_RESTORE_VIEW_TIMER_MAX = 1.9f;
  private GameObject obj;
  private ShipSkin shipSkin;
  private ShipSkinSubstance shipSkinSubstance;
  private GameObject cameraObj;
  private Camera camera;
  private float fModelRadius;
  private float fViewRestoreTimer;
  private float fVR_radiusRestoreStep;
  private float fVR_autoRotationRestoreStep;
  private AssetRequest request;

  public ShipView()
  {
    this.InitCamera();
    this.AddChild((GuiElementBase) this.labelLoading, Align.MiddleCenter);
    this.AddChild((GuiElementBase) this.shipBanner, Align.DownLeft);
  }

  private void InitCamera()
  {
    if ((Object) this.cameraObj != (Object) null)
      return;
    this.cameraObj = this.InitializeCameraGo();
    this.camera = this.cameraObj.GetComponent<Camera>();
    Camera.main.cullingMask &= ~(1 << AvatarView.layer);
    this.Reposition();
  }

  private GameObject InitializeCameraGo()
  {
    GameObject gameObject = new GameObject(this.GetType().ToString() + "Camera");
    gameObject.transform.position = this.shipViewOffset;
    gameObject.transform.rotation = new Quaternion(0.0f, 0.0f, 0.0f, 1f);
    Camera camera = gameObject.AddComponent<Camera>();
    camera.farClipPlane = 50000f;
    camera.depth = 3f;
    camera.fieldOfView = 60f;
    camera.cullingMask = AvatarView.GetCullingMask();
    camera.clearFlags = CameraClearFlags.Depth;
    camera.enabled = false;
    camera.renderingPath = RenderingPath.Forward;
    Light light = gameObject.AddComponent<Light>();
    light.type = LightType.Directional;
    light.intensity = 1f;
    light.cullingMask = AvatarView.GetCullingMask();
    return gameObject;
  }

  public void SetPrefab(string prefabName)
  {
    this.SetPrefab(prefabName, false);
  }

  public void SetSkinName(string skinName)
  {
    this.skinName = skinName;
  }

  public void SetSkin(string skinName, bool isAdvanced)
  {
    if ((Object) this.shipSkin != (Object) null)
    {
      this.skinName = !string.IsNullOrEmpty(skinName) ? skinName : "default";
      this.shipSkin.SelectSkin(this.skinName);
    }
    if (!((Object) this.shipSkinSubstance != (Object) null))
      return;
    this.skinName = !string.IsNullOrEmpty(skinName) ? skinName : "advanced";
    this.shipSkinSubstance.SelectSkin(this.skinName, this.obj.GetComponentsInChildren<Renderer>(), isAdvanced, true);
  }

  public void SetPrefab(string prefabName, bool reload)
  {
    if (this.prefabName == prefabName && !reload)
      return;
    this.InitCamera();
    this.DeleteModel();
    this.prefabName = prefabName;
    this.labelLoading.IsRendered = true;
    this.OutRadiusFactor = 1.3f;
    this.request = AssetCatalogue.Instance.Request(prefabName + ".prefab", false);
  }

  public void RestoreView()
  {
    this.fViewRestoreTimer = 1.9f;
    this.fVR_radiusRestoreStep = (this.fModelInitialRadiusFactor - this.OutRadiusFactor) / this.fViewRestoreTimer;
    this.fVR_autoRotationRestoreStep = 35f / this.fViewRestoreTimer;
    this.autoRotationY = 0.0f;
    this.SetupCamera();
  }

  public override void Update()
  {
    base.Update();
    if (this.request != null && this.request.IsDone)
    {
      Game.Instance.StartCoroutine(this.ModelRequestFulfilled(this.request));
      this.request = (AssetRequest) null;
    }
    if ((Object) this.obj == (Object) null)
      return;
    if (0.0 < (double) this.fViewRestoreTimer)
    {
      float t = (float) (1.0 - (double) this.fViewRestoreTimer / 1.89999997615814);
      this.obj.transform.rotation = Quaternion.Lerp(this.obj.transform.rotation, Quaternion.identity, t);
      this.obj.transform.position = Vector3.Lerp(this.obj.transform.position, this.shipViewOffset, t);
      this.fViewRestoreTimer -= Time.deltaTime;
      this.autoRotationY += this.fVR_autoRotationRestoreStep * Time.deltaTime;
      this.OutRadiusFactor += Time.deltaTime * this.fVR_radiusRestoreStep;
      this.SetupCamera();
    }
    if ((double) this.fViewRestoreTimer >= 0.00999999977648258)
      return;
    this.obj.transform.RotateAround(this.shipViewOffset, new Vector3(0.0f, 1f, 0.0f), this.autoRotationY * Time.deltaTime);
  }

  public override void Reposition()
  {
    base.Reposition();
    if (!((Object) this.camera != (Object) null))
      return;
    float left = (this.Rect.xMin - 1f) / (float) Screen.width;
    float num = (this.Rect.yMin - 1f) / (float) Screen.height;
    float width = (this.Rect.width - 2f) / (float) Screen.width;
    float height = (this.Rect.height - 2f) / (float) Screen.height;
    float top = 1f - num - height;
    this.camera.rect = new Rect(left, top, width, height);
  }

  public override void Draw()
  {
    if ((Object) this.camera != (Object) null && Event.current.type == UnityEngine.EventType.Repaint)
      this.camera.Render();
    base.Draw();
  }

  public void SetShipBanner(int hangarId)
  {
    if (hangarId == 16)
      this.shipBanner.Texture = Resources.Load("GUI/BuyShip/ShipBanners/BloodAndChrome") as Texture2D;
    else
      this.shipBanner.Texture = new Texture2D(0, 0);
  }

  public void RotateModelInDirection(float dx, float dy, float DegreesPerSecond, bool NeedMousePressed)
  {
    if (!(bool) ((Object) this.obj) || NeedMousePressed && !Input.GetMouseButton(0))
      return;
    this.obj.transform.RotateAround(this.shipViewOffset, new Vector3(0.0f, 1f, 0.0f), -dx * DegreesPerSecond);
    this.obj.transform.RotateAround(this.shipViewOffset, new Vector3(1f, 0.0f, 0.0f), DegreesPerSecond * dy);
    this.autoRotationY = 0.0f;
  }

  public void RotateModelByMouse(float2 position, float2 previousPosition, float SpeedFactor, bool NeedMousePressed)
  {
    if (!(bool) ((Object) this.obj) || NeedMousePressed && !Input.GetMouseButton(0))
      return;
    float num1 = position.x - previousPosition.x;
    float num2 = position.y - previousPosition.y;
    if ((double) Mathf.Abs(num1 + num2) > 0.0)
      this.autoRotationY = 0.0f;
    if ((double) num1 != 0.0)
      this.obj.transform.RotateAround(this.shipViewOffset, new Vector3(0.0f, 1f, 0.0f), -num1 * SpeedFactor);
    if ((double) num2 == 0.0)
      return;
    this.obj.transform.RotateAround(this.shipViewOffset, new Vector3(1f, 0.0f, 0.0f), -num2 * SpeedFactor);
  }

  private void SetLayerToAll(GameObject obj, int layer)
  {
    obj.layer = layer;
    for (int index = 0; index < obj.transform.childCount; ++index)
      this.SetLayerToAll(obj.transform.GetChild(index).gameObject, layer);
  }

  [DebuggerHidden]
  private IEnumerator ModelRequestFulfilled(AssetRequest request)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ShipView.\u003CModelRequestFulfilled\u003Ec__Iterator26() { request = request, \u003C\u0024\u003Erequest = request, \u003C\u003Ef__this = this };
  }

  private void SetupCameraOnce()
  {
    float num1 = 0.0f;
    MeshFilter[] componentsInChildren = this.obj.GetComponentsInChildren<MeshFilter>();
    this.obj.transform.position = this.shipViewOffset;
    if ((Object) this.obj.GetComponent<Collider>() != (Object) null)
      Object.Destroy((Object) this.obj.GetComponent<Collider>());
    if ((Object) this.obj.GetComponent<Rigidbody>() != (Object) null)
      Object.Destroy((Object) this.obj.GetComponent<Rigidbody>());
    foreach (MeshFilter meshFilter in componentsInChildren)
    {
      if (!meshFilter.name.Contains("sticker"))
      {
        if ((Object) meshFilter.GetComponent<Rigidbody>() != (Object) null)
          meshFilter.GetComponent<Rigidbody>().detectCollisions = false;
        if ((Object) meshFilter.GetComponent<Collider>() != (Object) null)
          Object.Destroy((Object) meshFilter.GetComponent<Collider>());
        if ((Object) meshFilter.GetComponent<Rigidbody>() != (Object) null)
          Object.Destroy((Object) meshFilter.GetComponent<Rigidbody>());
        if ((bool) ((Object) meshFilter.GetComponent<Renderer>()) && (double) num1 < (double) meshFilter.GetComponent<Renderer>().bounds.extents.magnitude)
          num1 = meshFilter.GetComponent<Renderer>().bounds.extents.magnitude;
      }
    }
    float num2 = num1 / 4f;
    if ((double) num1 < 0.1)
      num2 = 1f;
    float num3 = 1f / num2;
    this.obj.transform.localScale = new Vector3(num3, num3, num3);
    Vector3 min = new Vector3();
    Vector3 max = new Vector3();
    int num4 = 0;
    foreach (MeshFilter meshFilter in componentsInChildren)
    {
      if ((bool) ((Object) meshFilter.GetComponent<Renderer>()) && !meshFilter.name.Contains("sticker"))
      {
        if ((bool) ((Object) meshFilter.mesh) && meshFilter.mesh.name.Contains("engine"))
        {
          meshFilter.GetComponent<Renderer>().enabled = false;
        }
        else
        {
          if (num4 == 0)
          {
            min = meshFilter.GetComponent<Renderer>().bounds.min;
            max = meshFilter.GetComponent<Renderer>().bounds.max;
          }
          else
          {
            min.x = Mathf.Min(min.x, meshFilter.GetComponent<Renderer>().bounds.min.x);
            min.y = Mathf.Min(min.y, meshFilter.GetComponent<Renderer>().bounds.min.y);
            min.z = Mathf.Min(min.z, meshFilter.GetComponent<Renderer>().bounds.min.z);
            max.x = Mathf.Max(max.x, meshFilter.GetComponent<Renderer>().bounds.max.x);
            max.y = Mathf.Max(max.y, meshFilter.GetComponent<Renderer>().bounds.max.y);
            max.z = Mathf.Max(max.z, meshFilter.GetComponent<Renderer>().bounds.max.z);
          }
          ++num4;
        }
      }
    }
    Bounds bounds = new Bounds();
    bounds.SetMinMax(min, max);
    float magnitude = bounds.extents.magnitude;
    Vector3 center = bounds.center;
    this.obj.transform.position -= center;
    this.obj.transform.position += new Vector3(0.0f, center.y, 0.0f);
    this.obj.transform.rotation = Quaternion.Euler(0.0f, -90f, 0.0f);
    this.fModelRadius = magnitude;
    this.fModelInitialRadiusFactor = this.OutRadiusFactor;
  }

  public void ZoomModel(float zFactor)
  {
    if ((double) this.OutRadiusFactor + (double) zFactor < (double) this.fModelInitialRadiusFactor * 0.75 || (double) this.OutRadiusFactor + (double) zFactor > 5.0 * (double) this.fModelInitialRadiusFactor)
      return;
    this.OutRadiusFactor += zFactor;
    this.SetupCamera();
  }

  private void SetupCamera()
  {
    if ((double) this.fModelRadius <= 0.0)
      return;
    Vector3 vector3 = new Vector3(0.0f, 0.0f, 0.0f);
    vector3.z -= this.fModelRadius * this.OutRadiusFactor;
    this.cameraObj.transform.position = vector3 + this.shipViewOffset;
  }

  public override void OnHide()
  {
    base.OnHide();
    this.Delete();
  }

  public void DeleteModel()
  {
    if ((Object) this.obj != (Object) null)
      Object.DestroyObject((Object) this.obj);
    Game.Instance.StopCoroutine(this.ModelRequestFulfilled(this.request));
    this.obj = (GameObject) null;
    this.shipSkin = (ShipSkin) null;
    this.shipSkinSubstance = (ShipSkinSubstance) null;
    this.prefabName = (string) null;
  }

  public void Delete()
  {
    this.DeleteModel();
    if ((Object) this.cameraObj != (Object) null)
    {
      Object.DestroyObject((Object) this.cameraObj);
      this.cameraObj = (GameObject) null;
    }
    this.camera = (Camera) null;
  }

  public override bool ScrollDown(float2 position)
  {
    if (0.0 < (double) this.fViewRestoreTimer)
      return false;
    this.ZoomModel(0.2f);
    base.ScrollDown(position);
    return true;
  }

  public override bool ScrollUp(float2 position)
  {
    if (0.0 < (double) this.fViewRestoreTimer)
      return false;
    this.ZoomModel(-0.2f);
    base.ScrollUp(position);
    return true;
  }

  public override bool MouseMove(float2 position, float2 previousPosition)
  {
    this.RotateModelByMouse(position, previousPosition, 0.3f, true);
    return base.MouseMove(position, previousPosition);
  }
}
