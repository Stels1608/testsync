﻿// Decompiled with JetBrains decompiler
// Type: AutoSubscribeAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;

public class AutoSubscribeAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    foreach (SpaceObject spaceObject in (IEnumerable<SpaceObject>) message.Data)
    {
      if (spaceObject.AutoSubscribe)
        spaceObject.Subscribe();
    }
  }
}
