﻿// Decompiled with JetBrains decompiler
// Type: ChatProtocolParserType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

internal enum ChatProtocolParserType
{
  LoginOk,
  LoginFailureAuthenticationFail,
  LoginFailureBanLogin,
  LoginFailureBanIp,
  LoginFailureWrongChatVersion,
  FleetChannelUpdate,
  Text,
  TextFromModerator,
  TextClanTagged,
  TextYouWhisper,
  TextWhisperToYou,
  TextFailureCannotWhisperSelf,
  TextFailureBan,
  TextFailureKick,
  TextFailureFlood,
  TextFailureUserNotExists,
  SystemOpenChannelsUpdate,
  DynamicRoomCreateOk,
  DynamicRoomCreateFailureTooMuch,
  DynamicRoomCreateFailureNoName,
  DynamicRoomCreateFailureNameTooShort,
  DynamicRoomCreateFailureNameBanned,
  DynamicRoomCreateFailureNameKicked,
  DynamicRoomCreateFailureNameExists,
  DynamicRoomClosed,
  KickUserWithReason,
  BannUserWithReason,
}
