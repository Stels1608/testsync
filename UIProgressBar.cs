﻿// Decompiled with JetBrains decompiler
// Type: UIProgressBar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/Interaction/NGUI Progress Bar")]
public class UIProgressBar : UIWidgetContainer
{
  [SerializeField]
  [HideInInspector]
  protected float mValue = 1f;
  public List<EventDelegate> onChange = new List<EventDelegate>();
  public static UIProgressBar current;
  public UIProgressBar.OnDragFinished onDragFinished;
  public Transform thumb;
  [SerializeField]
  [HideInInspector]
  protected UIWidget mBG;
  [HideInInspector]
  [SerializeField]
  protected UIWidget mFG;
  [HideInInspector]
  [SerializeField]
  protected UIProgressBar.FillDirection mFill;
  protected Transform mTrans;
  protected bool mIsDirty;
  protected Camera mCam;
  protected float mOffset;
  public int numberOfSteps;

  public Transform cachedTransform
  {
    get
    {
      if ((Object) this.mTrans == (Object) null)
        this.mTrans = this.transform;
      return this.mTrans;
    }
  }

  public Camera cachedCamera
  {
    get
    {
      if ((Object) this.mCam == (Object) null)
        this.mCam = NGUITools.FindCameraForLayer(this.gameObject.layer);
      return this.mCam;
    }
  }

  public UIWidget foregroundWidget
  {
    get
    {
      return this.mFG;
    }
    set
    {
      if (!((Object) this.mFG != (Object) value))
        return;
      this.mFG = value;
      this.mIsDirty = true;
    }
  }

  public UIWidget backgroundWidget
  {
    get
    {
      return this.mBG;
    }
    set
    {
      if (!((Object) this.mBG != (Object) value))
        return;
      this.mBG = value;
      this.mIsDirty = true;
    }
  }

  public UIProgressBar.FillDirection fillDirection
  {
    get
    {
      return this.mFill;
    }
    set
    {
      if (this.mFill == value)
        return;
      this.mFill = value;
      this.ForceUpdate();
    }
  }

  public float value
  {
    get
    {
      if (this.numberOfSteps > 1)
        return Mathf.Round(this.mValue * (float) (this.numberOfSteps - 1)) / (float) (this.numberOfSteps - 1);
      return this.mValue;
    }
    set
    {
      float num1 = Mathf.Clamp01(value);
      if ((double) this.mValue == (double) num1)
        return;
      float num2 = this.value;
      this.mValue = num1;
      if ((double) num2 == (double) this.value)
        return;
      this.ForceUpdate();
      if (!((Object) UIProgressBar.current == (Object) null) || !NGUITools.GetActive((Behaviour) this) || !EventDelegate.IsValid(this.onChange))
        return;
      UIProgressBar.current = this;
      EventDelegate.Execute(this.onChange);
      UIProgressBar.current = (UIProgressBar) null;
    }
  }

  public float alpha
  {
    get
    {
      if ((Object) this.mFG != (Object) null)
        return this.mFG.alpha;
      if ((Object) this.mBG != (Object) null)
        return this.mBG.alpha;
      return 1f;
    }
    set
    {
      if ((Object) this.mFG != (Object) null)
      {
        this.mFG.alpha = value;
        if ((Object) this.mFG.GetComponent<Collider>() != (Object) null)
          this.mFG.GetComponent<Collider>().enabled = (double) this.mFG.alpha > 1.0 / 1000.0;
        else if ((Object) this.mFG.GetComponent<Collider2D>() != (Object) null)
          this.mFG.GetComponent<Collider2D>().enabled = (double) this.mFG.alpha > 1.0 / 1000.0;
      }
      if ((Object) this.mBG != (Object) null)
      {
        this.mBG.alpha = value;
        if ((Object) this.mBG.GetComponent<Collider>() != (Object) null)
          this.mBG.GetComponent<Collider>().enabled = (double) this.mBG.alpha > 1.0 / 1000.0;
        else if ((Object) this.mBG.GetComponent<Collider2D>() != (Object) null)
          this.mBG.GetComponent<Collider2D>().enabled = (double) this.mBG.alpha > 1.0 / 1000.0;
      }
      if (!((Object) this.thumb != (Object) null))
        return;
      UIWidget component = this.thumb.GetComponent<UIWidget>();
      if (!((Object) component != (Object) null))
        return;
      component.alpha = value;
      if ((Object) component.GetComponent<Collider>() != (Object) null)
      {
        component.GetComponent<Collider>().enabled = (double) component.alpha > 1.0 / 1000.0;
      }
      else
      {
        if (!((Object) component.GetComponent<Collider2D>() != (Object) null))
          return;
        component.GetComponent<Collider2D>().enabled = (double) component.alpha > 1.0 / 1000.0;
      }
    }
  }

  protected bool isHorizontal
  {
    get
    {
      if (this.mFill != UIProgressBar.FillDirection.LeftToRight)
        return this.mFill == UIProgressBar.FillDirection.RightToLeft;
      return true;
    }
  }

  protected bool isInverted
  {
    get
    {
      if (this.mFill != UIProgressBar.FillDirection.RightToLeft)
        return this.mFill == UIProgressBar.FillDirection.TopToBottom;
      return true;
    }
  }

  protected void Start()
  {
    this.Upgrade();
    if (Application.isPlaying)
    {
      if ((Object) this.mBG != (Object) null)
        this.mBG.autoResizeBoxCollider = true;
      this.OnStart();
      if ((Object) UIProgressBar.current == (Object) null && this.onChange != null)
      {
        UIProgressBar.current = this;
        EventDelegate.Execute(this.onChange);
        UIProgressBar.current = (UIProgressBar) null;
      }
    }
    this.ForceUpdate();
  }

  protected virtual void Upgrade()
  {
  }

  protected virtual void OnStart()
  {
  }

  protected void Update()
  {
    if (!this.mIsDirty)
      return;
    this.ForceUpdate();
  }

  protected void OnValidate()
  {
    if (NGUITools.GetActive((Behaviour) this))
    {
      this.Upgrade();
      this.mIsDirty = true;
      float num = Mathf.Clamp01(this.mValue);
      if ((double) this.mValue != (double) num)
        this.mValue = num;
      if (this.numberOfSteps < 0)
        this.numberOfSteps = 0;
      else if (this.numberOfSteps > 20)
        this.numberOfSteps = 20;
      this.ForceUpdate();
    }
    else
    {
      float num = Mathf.Clamp01(this.mValue);
      if ((double) this.mValue != (double) num)
        this.mValue = num;
      if (this.numberOfSteps < 0)
      {
        this.numberOfSteps = 0;
      }
      else
      {
        if (this.numberOfSteps <= 20)
          return;
        this.numberOfSteps = 20;
      }
    }
  }

  protected float ScreenToValue(Vector2 screenPos)
  {
    Transform cachedTransform = this.cachedTransform;
    Plane plane = new Plane(cachedTransform.rotation * Vector3.back, cachedTransform.position);
    Ray ray = this.cachedCamera.ScreenPointToRay((Vector3) screenPos);
    float enter;
    if (!plane.Raycast(ray, out enter))
      return this.value;
    return this.LocalToValue((Vector2) cachedTransform.InverseTransformPoint(ray.GetPoint(enter)));
  }

  protected virtual float LocalToValue(Vector2 localPos)
  {
    if (!((Object) this.mFG != (Object) null))
      return this.value;
    Vector3[] localCorners = this.mFG.localCorners;
    Vector3 vector3 = localCorners[2] - localCorners[0];
    if (this.isHorizontal)
    {
      float num = (localPos.x - localCorners[0].x) / vector3.x;
      if (this.isInverted)
        return 1f - num;
      return num;
    }
    float num1 = (localPos.y - localCorners[0].y) / vector3.y;
    if (this.isInverted)
      return 1f - num1;
    return num1;
  }

  public virtual void ForceUpdate()
  {
    this.mIsDirty = false;
    if ((Object) this.mFG != (Object) null)
    {
      UIBasicSprite uiBasicSprite = this.mFG as UIBasicSprite;
      if (this.isHorizontal)
      {
        if ((Object) uiBasicSprite != (Object) null && uiBasicSprite.type == UIBasicSprite.Type.Filled)
        {
          if (uiBasicSprite.fillDirection == UIBasicSprite.FillDirection.Horizontal || uiBasicSprite.fillDirection == UIBasicSprite.FillDirection.Vertical)
          {
            uiBasicSprite.fillDirection = UIBasicSprite.FillDirection.Horizontal;
            uiBasicSprite.invert = this.isInverted;
          }
          uiBasicSprite.fillAmount = this.value;
        }
        else
        {
          this.mFG.drawRegion = !this.isInverted ? new Vector4(0.0f, 0.0f, this.value, 1f) : new Vector4(1f - this.value, 0.0f, 1f, 1f);
          this.mFG.enabled = (double) this.value > 1.0 / 1000.0;
        }
      }
      else if ((Object) uiBasicSprite != (Object) null && uiBasicSprite.type == UIBasicSprite.Type.Filled)
      {
        if (uiBasicSprite.fillDirection == UIBasicSprite.FillDirection.Horizontal || uiBasicSprite.fillDirection == UIBasicSprite.FillDirection.Vertical)
        {
          uiBasicSprite.fillDirection = UIBasicSprite.FillDirection.Vertical;
          uiBasicSprite.invert = this.isInverted;
        }
        uiBasicSprite.fillAmount = this.value;
      }
      else
      {
        this.mFG.drawRegion = !this.isInverted ? new Vector4(0.0f, 0.0f, 1f, this.value) : new Vector4(0.0f, 1f - this.value, 1f, 1f);
        this.mFG.enabled = (double) this.value > 1.0 / 1000.0;
      }
    }
    if (!((Object) this.thumb != (Object) null) || !((Object) this.mFG != (Object) null) && !((Object) this.mBG != (Object) null))
      return;
    Vector3[] vector3Array = !((Object) this.mFG != (Object) null) ? this.mBG.localCorners : this.mFG.localCorners;
    Vector4 vector4 = !((Object) this.mFG != (Object) null) ? this.mBG.border : this.mFG.border;
    vector3Array[0].x += vector4.x;
    vector3Array[1].x += vector4.x;
    vector3Array[2].x -= vector4.z;
    vector3Array[3].x -= vector4.z;
    vector3Array[0].y += vector4.y;
    vector3Array[1].y -= vector4.w;
    vector3Array[2].y -= vector4.w;
    vector3Array[3].y += vector4.y;
    Transform transform = !((Object) this.mFG != (Object) null) ? this.mBG.cachedTransform : this.mFG.cachedTransform;
    for (int index = 0; index < 4; ++index)
      vector3Array[index] = transform.TransformPoint(vector3Array[index]);
    if (this.isHorizontal)
      this.SetThumbPosition(Vector3.Lerp(Vector3.Lerp(vector3Array[0], vector3Array[1], 0.5f), Vector3.Lerp(vector3Array[2], vector3Array[3], 0.5f), !this.isInverted ? this.value : 1f - this.value));
    else
      this.SetThumbPosition(Vector3.Lerp(Vector3.Lerp(vector3Array[0], vector3Array[3], 0.5f), Vector3.Lerp(vector3Array[1], vector3Array[2], 0.5f), !this.isInverted ? this.value : 1f - this.value));
  }

  protected void SetThumbPosition(Vector3 worldPos)
  {
    Transform parent = this.thumb.parent;
    if ((Object) parent != (Object) null)
    {
      worldPos = parent.InverseTransformPoint(worldPos);
      worldPos.x = Mathf.Round(worldPos.x);
      worldPos.y = Mathf.Round(worldPos.y);
      worldPos.z = 0.0f;
      if ((double) Vector3.Distance(this.thumb.localPosition, worldPos) <= 1.0 / 1000.0)
        return;
      this.thumb.localPosition = worldPos;
    }
    else
    {
      if ((double) Vector3.Distance(this.thumb.position, worldPos) <= 9.99999974737875E-06)
        return;
      this.thumb.position = worldPos;
    }
  }

  public enum FillDirection
  {
    LeftToRight,
    RightToLeft,
    BottomToTop,
    TopToBottom,
  }

  public delegate void OnDragFinished();
}
