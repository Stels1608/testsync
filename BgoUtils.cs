﻿// Decompiled with JetBrains decompiler
// Type: BgoUtils
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public static class BgoUtils
{
  private static readonly string[] colonialShips = new string[12]{ "ship_viper", "ship_raptor", "ship_rhino", "ship_berserker", "ship_avenger", "ship_dominator", "ship_halberd", "ship_gunstar", "ship_cruiser", "ship_dreadnought", "ship_gungnir", "ship_brimir" };
  private static readonly string[] cylonShips = new string[13]{ "ship_raider", "ship_heavy_raider", "ship_war_raider", "ship_scout", "ship_wrath", "ship_spectre", "ship_banshee", "ship_liche", "ship_nova", "ship_phantom", "ship_sentinel", "ship_nidhogg", "ship_surtur" };
  private static readonly HashSet<string> _loggedMessages = new HashSet<string>();
  private static BgoUtils.LoaderBehaviour _loaderBehaviour;

  private static BgoUtils.LoaderBehaviour loaderBehaviour
  {
    get
    {
      if ((UnityEngine.Object) BgoUtils._loaderBehaviour == (UnityEngine.Object) null)
        BgoUtils._loaderBehaviour = new GameObject().AddComponent<BgoUtils.LoaderBehaviour>();
      return BgoUtils._loaderBehaviour;
    }
  }

  public static string FactionUiFolder(Faction faction)
  {
    if (faction == Faction.Cylon)
      return "_cylon/";
    return string.Empty;
  }

  public static UIAtlas GetNguiAtlas(GUICard guiCard)
  {
    string str = BgoUtils.FactionUiFolder(Game.Me.Faction) + Utils.GetFilenameFromPath(guiCard.GUIAtlasTexturePath);
    GameObject gameObject = Resources.Load("GUI/gui_2013/atlas/" + str, typeof (GameObject)) as GameObject;
    if (!((UnityEngine.Object) gameObject == (UnityEngine.Object) null))
      return gameObject.GetComponent<UIAtlas>();
    UnityEngine.Debug.LogError((object) ("Unknown atlas: " + str));
    return (UIAtlas) null;
  }

  public static void ApplyDefaultSkin(GameObject shipGameObject, bool isAdvanced)
  {
    ShipSkin componentInChildren1 = shipGameObject.GetComponentInChildren<ShipSkin>();
    ShipSkinSubstance componentInChildren2 = shipGameObject.GetComponentInChildren<ShipSkinSubstance>();
    if ((UnityEngine.Object) componentInChildren1 != (UnityEngine.Object) null)
    {
      componentInChildren1.Loaded = false;
      componentInChildren1.SelectSkin("default");
    }
    if (!((UnityEngine.Object) componentInChildren2 != (UnityEngine.Object) null))
      return;
    componentInChildren2.Loaded = false;
    componentInChildren2.SelectSkin("advanced", shipGameObject.GetComponentsInChildren<Renderer>(), isAdvanced, false);
  }

  private static void TurnShipIntoPuppet(GameObject ship, string[] childrenWhitelist, List<System.Type> additionalComponentWhitelist = null)
  {
    List<System.Type> typeList = new List<System.Type>() { typeof (ShipSkin), typeof (Transform), typeof (ShipSkinSubstance), typeof (Gearbox) };
    if (additionalComponentWhitelist != null)
      typeList.AddRange((IEnumerable<System.Type>) additionalComponentWhitelist);
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BgoUtils.\u003CTurnShipIntoPuppet\u003Ec__AnonStorey113 puppetCAnonStorey113 = new BgoUtils.\u003CTurnShipIntoPuppet\u003Ec__AnonStorey113();
    foreach (Transform transform in ship.transform)
    {
      // ISSUE: reference to a compiler-generated field
      puppetCAnonStorey113.transform = transform;
      // ISSUE: reference to a compiler-generated method
      if (!((IEnumerable<string>) childrenWhitelist).Any<string>(new Func<string, bool>(puppetCAnonStorey113.\u003C\u003Em__299)))
      {
        // ISSUE: reference to a compiler-generated field
        puppetCAnonStorey113.transform.gameObject.SetActive(false);
      }
      else
      {
        // ISSUE: reference to a compiler-generated field
        foreach (Renderer componentsInChild in puppetCAnonStorey113.transform.GetComponentsInChildren<Renderer>())
          componentsInChild.enabled = true;
      }
    }
    Component[] components = ship.GetComponents<Component>();
    for (int index = 0; index < components.Length; ++index)
    {
      System.Type type = components[index].GetType();
      if (!typeList.Contains(type))
        UnityEngine.Object.Destroy((UnityEngine.Object) components[index]);
    }
  }

  public static void CleanupModelForShopView(GameObject ship)
  {
    string[] childrenWhitelist = new string[2]{ "lod1", "sticker" };
    BgoUtils.TurnShipIntoPuppet(ship, childrenWhitelist, (List<System.Type>) null);
  }

  public static void CleanupModelForCutscene(GameObject ship)
  {
    string[] childrenWhitelist = new string[5]{ !ship.transform.Cast<Transform>().Any<Transform>((Func<Transform, bool>) (t => t.name.ToLower().Contains("lod1"))) ? "lod4" : "lod1", "sticker", "engine", "gearbox", "thruster" };
    List<System.Type> additionalComponentWhitelist = new List<System.Type>() { typeof (Gearbox), typeof (Thrusters), typeof (JumpEffectNew), typeof (ShipSkinSubstance) };
    BgoUtils.TurnShipIntoPuppet(ship, childrenWhitelist, additionalComponentWhitelist);
  }

  public static GameObject CreateShipCopyForCutscene(GameObject originalShipModel)
  {
    if ((UnityEngine.Object) originalShipModel == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "CreateShipCopyForCutscene(): parsed ship to copy was null");
      return (GameObject) null;
    }
    GameObject gameObject = new GameObject("Cutscene_ShipCopy_" + originalShipModel.name);
    GameObject ship = UnityEngine.Object.Instantiate<GameObject>(originalShipModel);
    ship.transform.localScale = Vector3.one;
    ship.transform.parent = gameObject.transform;
    ship.transform.localPosition = Vector3.zero;
    ship.transform.localRotation = Quaternion.identity;
    BgoUtils.CleanupModelForCutscene(ship);
    ShipEngine[] componentsInChildren1 = originalShipModel.GetComponentsInChildren<ShipEngine>();
    ShipEngine[] componentsInChildren2 = ship.GetComponentsInChildren<ShipEngine>();
    for (int index = 0; index < ((IEnumerable<ShipEngine>) componentsInChildren1).Count<ShipEngine>(); ++index)
      componentsInChildren2[index].FlareOriginalScale = componentsInChildren1[index].FlareOriginalScale;
    return gameObject;
  }

  public static Vector3 SetVectorLength(Vector3 vector, float size)
  {
    return Vector3.Normalize(vector) * size;
  }

  public static bool RaySphereIntersection(out Vector3 intersectionPoint, Vector3 rayStart, Vector3 rayDirection, Vector3 sphereCenter, float sphereRadius)
  {
    Vector3 vector3 = rayStart - sphereCenter;
    float num1 = 2f * Vector3.Dot(rayDirection, vector3);
    float num2 = Vector3.Dot(vector3, vector3) - sphereRadius * sphereRadius;
    float f = (float) ((double) num1 * (double) num1 - 4.0 * (double) num2);
    if ((double) f > 0.0)
    {
      float num3 = (float) ((-(double) num1 - (double) Mathf.Sqrt(f)) / 2.0);
      float num4 = (float) ((-(double) num1 + (double) Mathf.Sqrt(f)) / 2.0);
      if ((double) num3 >= 0.0 && (double) num4 >= 0.0)
      {
        intersectionPoint = rayStart + rayDirection * num3;
        return true;
      }
      if ((double) num3 < 0.0 && (double) num4 >= 0.0)
      {
        intersectionPoint = rayStart + rayDirection * num4;
        return true;
      }
    }
    intersectionPoint = Vector3.zero;
    return false;
  }

  public static Faction GetFactionForShip(string shipname)
  {
    if (((IEnumerable<string>) BgoUtils.colonialShips).Contains<string>(shipname))
      return Faction.Colonial;
    return ((IEnumerable<string>) BgoUtils.cylonShips).Contains<string>(shipname) ? Faction.Cylon : Faction.Neutral;
  }

  public static bool Vector3FromString(string inputString, out Vector3 outputVector)
  {
    string[] strArray = inputString.Replace(" ", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Split(',');
    float result1;
    float result2;
    float result3;
    bool flag = true & float.TryParse(strArray[0], out result1) & float.TryParse(strArray[1], out result2) & float.TryParse(strArray[2], out result3);
    outputVector.x = result1;
    outputVector.y = result2;
    outputVector.z = result3;
    return flag;
  }

  public static bool Vector3FromString(string xS, string yS, string zS, out Vector3 outputVector)
  {
    float result1;
    float result2;
    float result3;
    bool flag = true & float.TryParse(xS, out result1) & float.TryParse(yS, out result2) & float.TryParse(zS, out result3);
    outputVector.x = result1;
    outputVector.y = result2;
    outputVector.z = result3;
    return flag;
  }

  public static Bounds MeasureRenderingBounds(GameObject model)
  {
    MeshRenderer[] componentsInChildren = model.GetComponentsInChildren<MeshRenderer>();
    if (componentsInChildren.Length == 0)
      return new Bounds(Vector3.zero, Vector3.zero);
    MeshRenderer meshRenderer1 = componentsInChildren[0];
    foreach (MeshRenderer meshRenderer2 in componentsInChildren)
    {
      if ((double) meshRenderer2.bounds.size.z > (double) meshRenderer1.bounds.size.z)
        meshRenderer1 = meshRenderer2;
    }
    return meshRenderer1.bounds;
  }

  public static void LogOnce(string errorMsg)
  {
    if (BgoUtils._loggedMessages.Contains(errorMsg))
      return;
    BgoUtils._loggedMessages.Add(errorMsg);
    UnityEngine.Debug.LogError((object) errorMsg);
  }

  public static void LoadAssetFromBundle<T>(string filename, BgoUtils.AssetLoadedCallback<T> assetLoadedCallback) where T : class
  {
    BgoUtils.loaderBehaviour.StartCoroutine(BgoUtils.LoadAssetCoroutine<T>(filename, assetLoadedCallback));
  }

  [DebuggerHidden]
  private static IEnumerator LoadAssetCoroutine<T>(string fileName, BgoUtils.AssetLoadedCallback<T> assetLoadedCallback) where T : class
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new BgoUtils.\u003CLoadAssetCoroutine\u003Ec__Iterator30<T>() { fileName = fileName, assetLoadedCallback = assetLoadedCallback, \u003C\u0024\u003EfileName = fileName, \u003C\u0024\u003EassetLoadedCallback = assetLoadedCallback };
  }

  private class LoaderBehaviour : MonoBehaviour
  {
  }

  public delegate void AssetLoadedCallback<T>(T loadedAsset);
}
