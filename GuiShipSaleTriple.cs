﻿// Decompiled with JetBrains decompiler
// Type: GuiShipSaleTriple
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GuiShipSaleTriple : GuiShipSaleDialog
{
  private readonly Dictionary<RewardCard, GuiShipSaleTriple.OfferPanel> offerPanels = new Dictionary<RewardCard, GuiShipSaleTriple.OfferPanel>();
  private readonly RewardCard[] offers;

  public GuiShipSaleTriple(ShipSaleCard saleCard)
    : base(saleCard)
  {
    Texture2D texture2D = ResourceLoader.Load<Texture2D>("GUI/DailyBonus/background_giveaway");
    this.m_backgroundImage.Texture = texture2D;
    this.Size = new Vector2((float) texture2D.width, (float) texture2D.height);
    this.AddChild((GuiElementBase) new GuiLabel(saleCard.GUICard.Description, Gui.Options.FontBGM_BT, 20), Align.UpCenter, new Vector2(0.0f, 25f));
    this.offers = this.shipSaleCard.ShipSales;
    for (int index = 0; index < this.offers.Length; ++index)
    {
      GuiShipSaleTriple.OfferPanel offerPanel = new GuiShipSaleTriple.OfferPanel(this.offers[index]);
      this.AddChild((GuiElementBase) offerPanel, Align.UpLeft, new Vector2((float) (21 + index * 262 + (index != 2 ? 0 : 2)), 80f));
      this.offerPanels.Add(this.offers[index], offerPanel);
      ShopProtocol.GetProtocol().RequestBoughtShipSaleOffer(this.offers[index].CardGUID);
    }
    this.OnClose = new AnonymousDelegate(GuiDialogPopupManager.CloseDialog);
  }

  public override void DisableOffer(uint guid)
  {
    for (int index1 = 0; index1 < this.offers.Length; ++index1)
    {
      RewardCard index2 = this.offers[index1];
      if ((int) index2.CardGUID == (int) guid)
      {
        GuiShipSaleTriple.OfferPanel offerPanel = this.offerPanels[index2];
        offerPanel.Image.OverlayColor = new Color?(Color.gray);
        offerPanel.DescriptionLabel.OverlayColor = new Color?(Color.gray);
        GuiButton buyButton = offerPanel.BuyButton;
        bool flag = false;
        offerPanel.BuyButton.HandleMouseInput = flag;
        int num = flag ? 1 : 0;
        buyButton.IsActive = num != 0;
        break;
      }
    }
  }

  private class OfferPanel : GuiPanel
  {
    private readonly GuiImage image;
    private readonly GuiLabel descriptionLabel;
    private readonly GuiButton buyButton;

    public GuiImage Image
    {
      get
      {
        return this.image;
      }
    }

    public GuiLabel DescriptionLabel
    {
      get
      {
        return this.descriptionLabel;
      }
    }

    public GuiButton BuyButton
    {
      get
      {
        return this.buyButton;
      }
    }

    public OfferPanel(RewardCard card)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      GuiShipSaleTriple.OfferPanel.\u003COfferPanel\u003Ec__AnonStoreyB5 panelCAnonStoreyB5 = new GuiShipSaleTriple.OfferPanel.\u003COfferPanel\u003Ec__AnonStoreyB5();
      // ISSUE: explicit constructor call
      base.\u002Ector(new Vector2(240f, 280f), new Vector2(0.0f, 0.0f));
      this.image = new GuiImage(card.GUICard.GUITexture);
      this.AddChild((GuiElementBase) this.Image, Align.UpLeft, new Vector2(0.0f, 40f));
      this.AddChild((GuiElementBase) new GuiImage(ResourceLoader.Load<Texture2D>("GUI/ShipSale/upgrade_pack")), Align.UpLeft, new Vector2(94f, 104f));
      this.AddChild((GuiElementBase) new GuiImage(ResourceLoader.Load<Texture2D>("GUI/ShipSale/icon_plus")), Align.UpLeft, new Vector2(106f, 116f));
      this.descriptionLabel = new GuiLabel(card.GUICard.Description, Gui.Options.FontBGM_BT, 17);
      this.DescriptionLabel.Style.fixedWidth = 230f;
      this.DescriptionLabel.WordWrap = true;
      this.AddChild((GuiElementBase) this.DescriptionLabel, Align.UpLeft, new Vector2(4f, 12f));
      this.buyButton = new GuiButton("%$bgo.shipsale.viewshippack%");
      // ISSUE: reference to a compiler-generated field
      panelCAnonStoreyB5.pack = card.Package;
      // ISSUE: reference to a compiler-generated field
      panelCAnonStoreyB5.itemGroup = card.ItemGroup;
      // ISSUE: reference to a compiler-generated method
      this.BuyButton.OnClick = new AnonymousDelegate(panelCAnonStoreyB5.\u003C\u003Em__189);
      this.BuyButton.Font = Gui.Options.FontBGM_BT;
      this.BuyButton.FontSize = 14;
      this.BuyButton.SizeX = 240f;
      this.AddChild((GuiElementBase) this.BuyButton, Align.UpLeft, new Vector2(0.0f, 251f));
    }
  }
}
