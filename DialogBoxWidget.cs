﻿// Decompiled with JetBrains decompiler
// Type: DialogBoxWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DialogBoxWidget : WindowWidget
{
  private const int buttonbarHeight = 90;
  [SerializeField]
  private GameObject uiOverlay;
  [SerializeField]
  private UISprite uiOverlaySprite;
  [SerializeField]
  public UISprite contentBG;
  private BoxCollider uiOverlayCollider;
  public GameObject content;
  private int yPadding;

  public int YPadding
  {
    private get
    {
      return this.yPadding;
    }
    set
    {
      this.yPadding = value;
    }
  }

  public override void Start()
  {
    base.Start();
    this.WindowManager.RegisterDialogBox(this);
    this.IsOpen = false;
  }

  public void RedrawOverlay()
  {
    this.uiOverlay.SetActive(true);
    if ((Object) null == (Object) this.uiOverlayCollider)
      this.uiOverlayCollider = (BoxCollider) this.uiOverlay.transform.GetComponent<Collider>();
    this.uiOverlaySprite.width = 3000;
    this.uiOverlaySprite.height = 3000;
    this.uiOverlayCollider.size = new Vector3(3000f, 3000f);
    this.uiOverlayCollider.center = new Vector3(1500f, -1500f, -1f);
    PositionUtils.SetUIPosition(this.uiOverlay, false, UIWidget.Pivot.Center);
  }

  public void RedrawMessageBoxPosition()
  {
    this.uiOverlay.SetActive(false);
    Bounds bounds = new Bounds();
    if ((Object) this.content != (Object) null)
      bounds = NGUIMath.CalculateRelativeWidgetBounds(this.content.transform, false);
    if (this.BackgroundSprite.gameObject.activeSelf)
      this.BackgroundSprite.height = (int) bounds.size.y;
    if ((Object) this.contentBG != (Object) null && this.contentBG.gameObject.activeSelf)
      this.contentBG.height = (int) bounds.size.y - 90;
    this.WindowSize = new Vector2(bounds.size.x, bounds.size.y);
    PositionUtils.SetUIPosition(this.gameObject, true, UIWidget.Pivot.Center);
    this.uiOverlay.SetActive(true);
    this.RedrawOverlay();
  }

  public void AddContent(List<GameObject> contentObjects, bool hiddenBackground = false)
  {
    this.gameObject.SetActive(true);
    this.BackgroundSprite.gameObject.SetActive(!hiddenBackground);
    this.contentBG.gameObject.SetActive(!hiddenBackground);
    foreach (Component component in this.content.transform)
      Object.Destroy((Object) component.gameObject);
    int num = 0;
    using (List<GameObject>.Enumerator enumerator = contentObjects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        PositionUtils.CorrectTransform(this.content, current);
        current.name = num.ToString() + "_" + current.name;
        ++num;
      }
    }
    this.Invoke("DelayedReposition", 1f / 1000f);
  }

  private void DelayedReposition()
  {
    UITable component = this.content.GetComponent<UITable>();
    if ((Object) component != (Object) null)
    {
      component.padding.y = (float) this.yPadding;
      component.Reposition();
      component.gameObject.transform.localPosition = new Vector3(0.0f, (float) this.yPadding);
    }
    this.RedrawMessageBoxPosition();
  }

  public override void OnWindowOpen()
  {
    base.OnWindowOpen();
    this.gameObject.SetActive(true);
  }

  public override void Close()
  {
    base.Close();
    this.gameObject.SetActive(false);
  }
}
