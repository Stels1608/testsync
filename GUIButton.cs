﻿// Decompiled with JetBrains decompiler
// Type: GUIButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIButton : CustomGUIElement
{
  public AudioClip MouseOverSound = GUISound.Instance.MouseOver;
  public AudioClip PressSound = GUISound.Instance.Select;
  protected bool isPressed;
  protected Texture2D normalTexture;
  protected Texture2D pressedTexture;
  protected Texture2D overTexture;
  public GUILabel textLabel;

  public bool IsPressed
  {
    get
    {
      return this.isPressed;
    }
    set
    {
      this.isPressed = value;
    }
  }

  public GUIButton(Texture2D normal, Texture2D pressed, bool isPressed, SmartRect parent)
    : this(normal, pressed, (Texture2D) null, isPressed, parent)
  {
  }

  public GUIButton(Texture2D normal, Texture2D pressed, Texture2D over, bool isPressed, SmartRect parent)
    : this(normal, pressed, over, isPressed, parent, string.Empty)
  {
  }

  public GUIButton(Texture2D normal, Texture2D pressed, Texture2D over, bool isPressed, SmartRect parent, string text)
  {
    this.rect = new SmartRect(new Rect(0.0f, 0.0f, (float) normal.width, (float) normal.height), float2.zero, parent);
    this.normalTexture = normal;
    this.pressedTexture = pressed;
    this.overTexture = over;
    this.isPressed = isPressed;
    if (text == null || !(text != string.Empty))
      return;
    this.textLabel = new GUILabel(text, Color.gray, Color.white, (Font) null, this.GetRect());
  }

  public bool GetIsPressed()
  {
    return this.isPressed;
  }

  public void SetIsPressed(bool newValue)
  {
    this.isPressed = newValue;
  }

  public void SetLabel(GUILabel newValue)
  {
    this.textLabel = newValue;
    if (this.textLabel == null)
      return;
    this.textLabel.SetIsMouseOver(this.isOver);
  }

  public GUILabel GetLabel()
  {
    return this.textLabel;
  }

  public override void Draw()
  {
    Texture2D texture2D = !this.isPressed ? this.normalTexture : this.pressedTexture;
    Rect absRect = this.rect.AbsRect;
    if (Event.current.type == UnityEngine.EventType.Repaint && (Object) texture2D != (Object) null)
    {
      Graphics.DrawTexture(absRect, (Texture) texture2D, 5, 5, 5, 5);
      if (this.isOver && (Object) this.overTexture != (Object) null)
        Graphics.DrawTexture(absRect, (Texture) texture2D, 5, 5, 5, 5);
    }
    if (this.textLabel == null)
      return;
    this.textLabel.Draw();
  }

  public override bool Contains(float2 point)
  {
    bool flag = false;
    if (this.textLabel != null)
      flag = this.textLabel.Contains(point);
    if (!base.Contains(point))
      return flag;
    return true;
  }

  public void Toggle()
  {
    this.SetIsPressed(!this.GetIsPressed());
  }

  public bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey != KeyCode.Mouse0 || !this.Contains(mousePosition))
      return false;
    this.SetIsPressed(true);
    return true;
  }

  public bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey != KeyCode.Mouse0)
      return false;
    bool flag = false;
    if (this.IsPressed && this.Contains(mousePosition))
      flag = true;
    this.IsPressed = false;
    if (flag && (Object) this.PressSound != (Object) null)
      GUISound.Instance.PlayOnce(this.PressSound);
    return flag;
  }

  public void OnMouseMove(float2 mousePosition)
  {
    this.IsMouseOver = this.Contains(mousePosition);
  }

  public override void SetIsMouseOver(bool newValue)
  {
    if (this.isRendered && !this.isOver && newValue)
      this.OnMouseOver();
    base.SetIsMouseOver(newValue);
    if (this.textLabel == null)
      return;
    this.textLabel.SetIsMouseOver(newValue);
  }

  protected virtual void OnMouseOver()
  {
    if (!((Object) this.MouseOverSound != (Object) null))
      return;
    GUISound.Instance.PlayOnce(this.MouseOverSound);
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    if (this.textLabel == null)
      return;
    this.textLabel.RecalculateAbsCoords();
  }
}
