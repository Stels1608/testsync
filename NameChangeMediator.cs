﻿// Decompiled with JetBrains decompiler
// Type: NameChangeMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class NameChangeMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "NameChangeMediator";
  private NameChangeDialog view;

  public NameChangeMediator(CharacterService nameChangeService)
    : base("NameChangeMediator")
  {
    this.view = this.CreateView();
    this.view.SetCharacterService(nameChangeService);
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.NameRejected);
    this.AddMessageInterest(Message.NameChangeSuccessful);
    this.AddMessageInterest(Message.CancelNameChange);
    this.AddMessageInterest(Message.PlayerCubitsHold);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.CancelNameChange:
        this.OwnerFacade.DetachView("NameChangeMediator");
        break;
      case Message.NameChangeSuccessful:
        Game.QuitUpdate();
        break;
      case Message.NameRejected:
        this.view.InvalidName();
        break;
      case Message.PlayerCubitsHold:
        if (!((Object) this.view != (Object) null))
          break;
        this.view.OnCubitsBalanceChanged();
        break;
    }
  }

  public override void OnBeforeDetach()
  {
    if (!((Object) this.view != (Object) null))
      return;
    Object.Destroy((Object) this.view.gameObject);
  }

  private NameChangeDialog CreateView()
  {
    return UguiTools.CreateChild<NameChangeDialog>("CharacterMenu/NameChangeDialog", UguiTools.GetCanvas(BsgoCanvas.CharacterMenu).transform);
  }
}
