﻿// Decompiled with JetBrains decompiler
// Type: InfoJournal
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Common;
using UnityEngine;

public class InfoJournal : GuiDialog
{
  private readonly GuiImage medalIcon = new GuiImage();
  private readonly GuiImage imageTokens = new GuiImage();
  private readonly GuiLabel tokens = new GuiLabel();
  private readonly Me me;
  private readonly FriendList fl;
  private readonly ShipsArea shipsArea;
  private readonly AvatarView avatarView;

  public InfoJournal()
  {
    this.me = Game.Me;
    this.m_backgroundImage.IsRendered = false;
    Loaders.Load((GuiPanel) this, "GUI/InfoJournal/infoJournalPanel");
    this.BringToFront((GuiElementBase) this.CloseButton);
    GuiImage guiImage1 = this.Find<GuiImage>("ShipOwnedRect");
    this.shipsArea = new ShipsArea();
    this.shipsArea.isMe = true;
    this.shipsArea.Position = guiImage1.Position;
    this.RemoveChild((GuiElementBase) guiImage1);
    this.AddChild((GuiElementBase) this.shipsArea);
    GuiImage guiImage2 = this.Find<GuiImage>("friendsListBackground");
    this.fl = new FriendList();
    this.fl.Position = guiImage2.Position + new Vector2(2f, 2f);
    this.fl.Size = guiImage2.Size - new Vector2(4f, 4f);
    this.AddChild((GuiElementBase) this.fl);
    this.fl.UpdateList(this.me.Friends);
    GuiHealthbar guiHealthbar1 = new GuiHealthbar();
    GuiImage guiImage3 = this.Find<GuiImage>("xpProgressBar");
    guiHealthbar1.Size = guiImage3.Size;
    guiHealthbar1.Position = guiImage3.Position;
    guiHealthbar1.Name = "xpProgressBar";
    this.RemoveChild((GuiElementBase) guiImage3);
    this.AddChild((GuiElementBase) guiHealthbar1);
    GuiHealthbar guiHealthbar2 = new GuiHealthbar();
    GuiImage guiImage4 = this.Find<GuiImage>("skillProgressBar");
    guiHealthbar2.Size = guiImage4.Size;
    guiHealthbar2.Position = guiImage4.Position;
    guiHealthbar2.Name = "skillProgressBar";
    this.RemoveChild((GuiElementBase) guiImage4);
    this.AddChild((GuiElementBase) guiHealthbar2);
    GuiImage guiImage5 = this.Find<GuiImage>("avatar");
    this.avatarView = new AvatarView((AvatarItems) Game.Me.AvatarDesc);
    guiImage5.IsRendered = true;
    this.avatarView.Size = guiImage5.Size;
    this.avatarView.Position = guiImage5.Position;
    this.AddChild((GuiElementBase) this.avatarView);
    this.Find<GuiElementBase>("cubitsImage").SetTooltip("%$bgo.resource_cubits.Description%");
    this.Find<GuiElementBase>("cubitsCountLabel").SetTooltip("%$bgo.resource_cubits.Description%");
    this.Find<GuiElementBase>("tiliumImage").SetTooltip("%$bgo.resource_tylium.Description%");
    this.Find<GuiElementBase>("tiliumCountLabel").SetTooltip("%$bgo.resource_tylium.Description%");
    this.Find<GuiElementBase>("waterImage").SetTooltip("%$bgo.resource_water.Description%");
    this.Find<GuiElementBase>("waterCountLabel").SetTooltip("%$bgo.resource_water.Description%");
    this.Find<GuiElementBase>("titaniumImage").SetTooltip("%$bgo.resource_titanium.Description%");
    this.Find<GuiElementBase>("titaniumCountLabel").SetTooltip("%$bgo.resource_titanium.Description%");
    this.Find<GuiLabel>("titlePrefixLabel").Text = "%$bgo.InfoJournal.infoJournalPanel.titleLabel%";
    this.Find<GuiLabel>("rankPrefixLabel").Text = "%$bgo.InfoJournal.infoJournalPanel.rankLabel%";
    this.Find<GuiLabel>("levelPrefixLabel").Text = "%$bgo.InfoJournal.infoJournalPanel.levelLabel%";
    this.Find<GuiLabel>("wingPrefixLabel").Text = "%$bgo.InfoJournal.infoJournalPanel.wingLabel%";
    this.Find<GuiLabel>("wingPositionPrefixLabel").Text = "%$bgo.InfoJournal.infoJournalPanel.wingPositionLabel%";
    this.Find<GuiLabel>("shipTypePrefixLabel").Text = "%$bgo.InfoJournal.infoJournalPanel.shipLabel%";
    this.Find<GuiLabel>("shipNamePrefixLabel").Text = "%$bgo.InfoJournal.infoJournalPanel.shipNameLabel%";
    this.Find<GuiLabel>("meritCapPrefixLabel").Text = "%$bgo.InfoJournal.infoJournalPanel.meritCapLabel%";
    this.AddChild((GuiElementBase) this.imageTokens, new Vector2(455f, 373f));
    this.AddChild((GuiElementBase) this.tokens, new Vector2(486f, 375f));
    Game.Catalogue.FetchCard(130920111U, CardView.GUI).IsLoaded.AddHandler((SignalHandler) (() =>
    {
      GUICard guiCard = Game.Catalogue.FetchCard(130920111U, CardView.GUI) as GUICard;
      this.imageTokens.TexturePath = guiCard.GUIIcon;
      this.imageTokens.SetTooltip(guiCard.Description);
      this.tokens.SetTooltip(guiCard.Description);
    }));
    this.AddChild((GuiElementBase) this.medalIcon, new Vector2(this.Find<GuiLabel>("nameLabel").PositionX, 10f));
  }

  public override void PeriodicUpdate()
  {
    this.fl.UpdateList(this.me.Friends);
    Player player = Game.Players.GetPlayer(Game.Me.ServerID);
    if ((bool) player.IsLoaded)
    {
      if (player.PvpMedal > PvpMedal.None)
      {
        switch (player.PvpMedal)
        {
          case PvpMedal.PvpArena1st:
            this.medalIcon.TexturePath = "GUI/PlayerStars/pvparena_1st";
            break;
          case PvpMedal.PvpArena2nd:
            this.medalIcon.TexturePath = "GUI/PlayerStars/pvparena_2nd";
            break;
          case PvpMedal.PvpArena3rd:
            this.medalIcon.TexturePath = "GUI/PlayerStars/pvparena_3rd";
            break;
          case PvpMedal.PvpArena4thTo20th:
            this.medalIcon.TexturePath = "GUI/PlayerStars/pvparena_4th-20th";
            break;
          case PvpMedal.PvpArena21stTo100th:
            this.medalIcon.TexturePath = "GUI/PlayerStars/pvparena_21st-100th";
            break;
        }
      }
      else
        this.medalIcon.Texture = (Texture2D) null;
    }
    GuiLabel guiLabel = this.Find<GuiLabel>("nameLabel");
    guiLabel.Text = this.me.Name;
    guiLabel.PositionX = this.medalIcon.PositionX;
    guiLabel.PositionX += (float) (!((Object) this.medalIcon.Texture != (Object) null) ? 0.0 : 37.5);
    guiLabel.PositionY = this.medalIcon.PositionY + 3f;
    this.Find<GuiLabel>("titleLabel").Text = this.me.Title;
    this.Find<GuiLabel>("rankLabel").Text = this.me.Rank;
    this.Find<GuiLabel>("levelLabel").Text = this.me.Level.ToString();
    bool flag1 = this.me.Guild.Has && this.me.Guild.GuildRole != GuildRole.Recruit;
    this.Find<GuiLabel>("wingField").Text = !flag1 ? "%$bgo.pilot_log.none%" : this.me.Guild.Name;
    this.Find<GuiLabel>("wingPositionField").Text = !flag1 ? "%$bgo.pilot_log.none%" : Tools.FormatWingRole(this.me.Guild.GuildRole);
    this.Find<GuiLabel>("shipTypeField").Text = this.me.ActiveShip.GUICard.Name;
    this.Find<GuiLabel>("shipNameField").Text = this.me.ActiveShip.Name;
    this.Find<GuiLabel>("meritCapLabel").Text = this.me.MeritCap.First.ToString() + "/" + (object) this.me.MeritCap.Second;
    float num = this.me.SkillBook.TrainingEndTime - Time.time;
    bool flag2 = this.me.SkillBook.IsTrainingSkill;
    if (flag2 && (double) num <= 0.0)
    {
      flag2 = false;
      this.me.SkillBook.EndTraining();
    }
    this.Find<GuiLabel>("skillNameLabel").Text = "%$bgo.pilot_log.skill_training%   <<" + (!flag2 ? "%$bgo.pilot_log.none%" : this.me.SkillBook.CurrentTrainingSkill.Name + "-" + (object) this.me.SkillBook.CurrentTrainingSkill.Level) + ">>";
    this.Find<GuiLabel>("trainingCompleteLabel").IsRendered = flag2;
    if (flag2)
      this.Find<GuiLabel>("trainingCompleteLabel").Text = "%$bgo.pilot_log.skill_training_time%   <<" + Tools.FormatTime(this.me.SkillBook.TrainingEndTime - Time.time) + ">>";
    this.Find<GuiHealthbar>("skillProgressBar").IsRendered = flag2;
    if (flag2)
      this.Find<GuiHealthbar>("skillProgressBar").Progress = (float) (1.0 - ((double) this.me.SkillBook.TrainingEndTime - (double) Time.time) / (double) this.me.SkillBook.CurrentTrainingSkill.Card.TrainingTime);
    this.Find<GuiLabel>("xpLabel").Text = "%$bgo.pilot_log.xp_earned%   " + this.me.Experience.ToString();
    this.Find<GuiLabel>("freeXpLabel").Text = "%$bgo.pilot_log.free_xp%   " + this.me.FreeExperience.ToString();
    this.Find<GuiLabel>("promotionLabel").Text = "%$bgo.InfoJournal.infoJournalPanel.promotionLabel%   " + (this.me.NextLevelExperience - this.me.PrevLevelExperience).ToString();
    this.Find<GuiHealthbar>("xpProgressBar").Progress = Game.Me.NormalExperience;
    this.Find<GuiLabel>("cubitsCountLabel").Text = this.me.Hold.Cubits.ToString();
    this.Find<GuiLabel>("waterCountLabel").Text = this.me.Hold.Water.ToString();
    this.Find<GuiLabel>("tiliumCountLabel").Text = this.me.Hold.Tylium.ToString();
    this.Find<GuiLabel>("titaniumCountLabel").Text = this.me.Hold.Titanium.ToString();
    this.tokens.Text = this.me.Hold.Token.ToString();
    base.PeriodicUpdate();
  }

  public override void OnShow()
  {
    base.OnShow();
    PlayerProtocol.GetProtocol().RequestMeritCap();
  }

  public void UpdateAvatar(AvatarDescription desc)
  {
    this.avatarView.UpdateAvatar(desc);
  }

  public void UpdateDisplay()
  {
    this.shipsArea.UpdateDisplay();
  }
}
