﻿// Decompiled with JetBrains decompiler
// Type: SettingsDataProvider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Model;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SettingsDataProvider : DataProvider<Message>
{
  private readonly InputBinder inputBinder;
  private readonly List<ISettingsReceiver> settingsReceivers;

  public UserSettings CurrentSettings { get; set; }

  public InputBinder InputBinderCache { get; set; }

  public InputBinder InputBinder
  {
    get
    {
      return this.inputBinder;
    }
  }

  public List<ISettingsReceiver> SettingsReceivers
  {
    get
    {
      return this.settingsReceivers;
    }
  }

  public SettingsDataProvider()
    : base("SettingsDataProvider")
  {
    this.CurrentSettings = UserSettings.DefaultSettings;
    this.inputBinder = new InputBinder();
    this.InputBinderCache = new InputBinder();
    this.settingsReceivers = new List<ISettingsReceiver>();
  }

  public void RegisterSettingsReceiver(ISettingsReceiver settingsReceiver)
  {
    this.settingsReceivers.Add(settingsReceiver);
  }

  public void UnregisterSettingsReceiver(ISettingsReceiver settingsReceiver)
  {
    this.settingsReceivers.Remove(settingsReceiver);
  }

  public void Save()
  {
    SettingProtocol.GetProtocol().RequestSaveSettings(this);
    this.SaveLocal(this.CurrentSettings);
  }

  public void Load(BgoProtocolReader protocolReader)
  {
    this.ReadServerSavedSettings(protocolReader);
    this.LoadLocal(this.CurrentSettings);
  }

  private void LoadLocal(UserSettings settings)
  {
    UserSettings localSettings = settings.LocalSettings;
    UserSettings defaultSettings = UserSettings.DefaultSettings;
    using (List<UserSetting>.Enumerator enumerator = localSettings.DefinedSettings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        UserSettingValueType valueType = UserSettings.GetValueType(current);
        object obj = (object) null;
        if (PlayerPrefs.HasKey(current.ToString()))
        {
          switch (valueType)
          {
            case UserSettingValueType.Float:
              obj = (object) PlayerPrefs.GetFloat(current.ToString());
              break;
            case UserSettingValueType.Boolean:
              obj = (object) (PlayerPrefs.GetInt(current.ToString()) == 1);
              break;
            case UserSettingValueType.Integer:
              obj = (object) PlayerPrefs.GetInt(current.ToString());
              break;
          }
        }
        else
          obj = defaultSettings.Get(current);
        settings.Set(current, obj);
      }
    }
  }

  private void SaveLocal(UserSettings settings)
  {
    using (List<UserSetting>.Enumerator enumerator = settings.LocalSettings.DefinedSettings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        switch (UserSettings.GetValueType(current))
        {
          case UserSettingValueType.Float:
            float num1 = (float) settings.Get(current);
            PlayerPrefs.SetFloat(current.ToString(), num1);
            continue;
          case UserSettingValueType.Boolean:
            bool flag = (bool) settings.Get(current);
            PlayerPrefs.SetInt(current.ToString(), !flag ? 0 : 1);
            continue;
          case UserSettingValueType.Integer:
            int num2 = (int) settings.Get(current);
            PlayerPrefs.SetInt(current.ToString(), num2);
            continue;
          default:
            continue;
        }
      }
    }
  }

  public void WriteServerSavedSettings(BgoProtocolWriter w)
  {
    List<UserSetting> definedSettings1 = this.CurrentSettings.LocalSettings.DefinedSettings;
    List<UserSetting> definedSettings2 = this.CurrentSettings.DefinedSettings;
    int num = definedSettings2.Count - definedSettings1.Count;
    w.Write((ushort) num);
    using (List<UserSetting>.Enumerator enumerator = definedSettings2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        if (!definedSettings1.Contains(current))
          this.WriteProperty(w, this.CurrentSettings, current);
      }
    }
  }

  private void WriteProperty(BgoProtocolWriter w, UserSettings settings, UserSetting property)
  {
    object obj = settings.Get(property);
    if (obj == null)
    {
      Debug.LogError((object) ("NO VALID VALUE FOR == " + (object) property));
    }
    else
    {
      w.Write((byte) property);
      UserSettingValueType valueType = UserSettings.GetValueType(property);
      w.Write((byte) valueType);
      switch (valueType)
      {
        case UserSettingValueType.Float:
          w.Write((float) obj);
          break;
        case UserSettingValueType.Boolean:
          w.Write((bool) obj);
          break;
        case UserSettingValueType.Integer:
          w.Write((int) obj);
          break;
        case UserSettingValueType.Float2:
          float2 float2 = (float2) obj;
          w.Write(float2.x);
          w.Write(float2.y);
          break;
        case UserSettingValueType.HelpScreenType:
          List<HelpScreenType> helpScreenTypeList = (List<HelpScreenType>) obj;
          w.Write((ushort) helpScreenTypeList.Count);
          using (List<HelpScreenType>.Enumerator enumerator = helpScreenTypeList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              HelpScreenType current = enumerator.Current;
              w.Write((ushort) current);
            }
            break;
          }
        case UserSettingValueType.Byte:
          w.Write((byte) obj);
          break;
        default:
          w.Write((byte) 0);
          break;
      }
    }
  }

  private void ReadServerSavedSettings(BgoProtocolReader r)
  {
    List<UserSetting> definedSettings = this.CurrentSettings.LocalSettings.DefinedSettings;
    int num1 = r.ReadLength();
    for (int index1 = 0; index1 < num1; ++index1)
    {
      UserSetting userSetting = (UserSetting) r.ReadByte();
      object obj = (object) null;
      UserSettingValueType valueType = UserSettings.GetValueType(userSetting);
      int num2 = (int) r.ReadByte();
      switch (valueType)
      {
        case UserSettingValueType.Float:
          obj = (object) r.ReadSingle();
          break;
        case UserSettingValueType.Boolean:
          obj = (object) r.ReadBoolean();
          break;
        case UserSettingValueType.Integer:
          obj = (object) r.ReadInt32();
          break;
        case UserSettingValueType.Float2:
          obj = (object) new float2(r.ReadSingle(), r.ReadSingle());
          break;
        case UserSettingValueType.HelpScreenType:
          List<HelpScreenType> helpScreenTypeList = new List<HelpScreenType>();
          int num3 = r.ReadLength();
          for (int index2 = 0; index2 < num3; ++index2)
            helpScreenTypeList.Add((HelpScreenType) r.ReadUInt16());
          obj = (object) helpScreenTypeList;
          break;
        case UserSettingValueType.Byte:
          obj = (object) r.ReadByte();
          break;
        default:
          Debug.LogWarning((object) ("Invalid setting value type: " + (object) valueType));
          break;
      }
      if (Enum.IsDefined(typeof (UserSetting), (object) userSetting) && !definedSettings.Contains(userSetting))
        this.CurrentSettings.Set(userSetting, obj);
    }
  }

  public void LoadControls(BgoProtocolReader r)
  {
    List<IInputBinding> newBindingList = new List<IInputBinding>();
    int num = r.ReadLength();
    for (int index = 0; index < num; ++index)
    {
      ushort deviceTriggerCode = r.ReadUInt16();
      Action action = (Action) r.ReadUInt16();
      byte modifierCode = r.ReadByte();
      InputDevice device = (InputDevice) r.ReadByte();
      byte flags = r.ReadByte();
      byte profile = r.ReadByte();
      IInputBinding newBinding = InputBindingFactory.CreateNewBinding(action, device, profile, deviceTriggerCode, modifierCode, flags);
      if (newBinding != null)
        newBindingList.Add(newBinding);
      else
        Debug.LogError((object) "Uh oh! Problem reading input bindings. Undefined device?");
    }
    this.InputBinder.ResetToDefaults((byte) 0);
    this.InputBinder.MergeWith(newBindingList);
  }

  public void SaveControls(BgoProtocolWriter w)
  {
    List<IInputBinding> inputBindings = this.InputBinder.InputBindings;
    w.Write((ushort) inputBindings.Count);
    using (List<IInputBinding>.Enumerator enumerator = inputBindings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IInputBinding current = enumerator.Current;
        w.Write(current.DeviceTriggerCode);
        w.Write((ushort) current.Action);
        w.Write((byte) current.DeviceModifierCode);
        w.Write((byte) current.Device);
        w.Write(current.Flags);
        w.Write(current.ProfileNo);
      }
    }
  }
}
