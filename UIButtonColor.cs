﻿// Decompiled with JetBrains decompiler
// Type: UIButtonColor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Button Color")]
[ExecuteInEditMode]
public class UIButtonColor : UIWidgetContainer
{
  public Color hover = new Color(0.8823529f, 0.7843137f, 0.5882353f, 1f);
  public Color pressed = new Color(0.7176471f, 0.6392157f, 0.4823529f, 1f);
  public Color disabledColor = Color.grey;
  public float duration = 0.2f;
  public GameObject tweenTarget;
  [NonSerialized]
  protected Color mStartingColor;
  [NonSerialized]
  protected Color mDefaultColor;
  [NonSerialized]
  protected bool mInitDone;
  [NonSerialized]
  protected UIWidget mWidget;
  [NonSerialized]
  protected UIButtonColor.State mState;

  public UIButtonColor.State state
  {
    get
    {
      return this.mState;
    }
    set
    {
      this.SetState(value, false);
    }
  }

  public Color defaultColor
  {
    get
    {
      if (!this.mInitDone)
        this.OnInit();
      return this.mDefaultColor;
    }
    set
    {
      if (!this.mInitDone)
        this.OnInit();
      this.mDefaultColor = value;
      UIButtonColor.State state = this.mState;
      this.mState = UIButtonColor.State.Disabled;
      this.SetState(state, false);
    }
  }

  public virtual bool isEnabled
  {
    get
    {
      return this.enabled;
    }
    set
    {
      this.enabled = value;
    }
  }

  public void ResetDefaultColor()
  {
    this.defaultColor = this.mStartingColor;
  }

  private void Awake()
  {
    if (this.mInitDone)
      return;
    this.OnInit();
  }

  private void Start()
  {
    if (this.isEnabled)
      return;
    this.SetState(UIButtonColor.State.Disabled, true);
  }

  protected virtual void OnInit()
  {
    this.mInitDone = true;
    if ((UnityEngine.Object) this.tweenTarget == (UnityEngine.Object) null)
      this.tweenTarget = this.gameObject;
    this.mWidget = this.tweenTarget.GetComponent<UIWidget>();
    if ((UnityEngine.Object) this.mWidget != (UnityEngine.Object) null)
    {
      this.mDefaultColor = this.mWidget.color;
      this.mStartingColor = this.mDefaultColor;
    }
    else
    {
      Renderer component1 = this.tweenTarget.GetComponent<Renderer>();
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
      {
        this.mDefaultColor = !Application.isPlaying ? component1.sharedMaterial.color : component1.material.color;
        this.mStartingColor = this.mDefaultColor;
      }
      else
      {
        Light component2 = this.tweenTarget.GetComponent<Light>();
        if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
        {
          this.mDefaultColor = component2.color;
          this.mStartingColor = this.mDefaultColor;
        }
        else
        {
          this.tweenTarget = (GameObject) null;
          this.mInitDone = false;
        }
      }
    }
  }

  protected virtual void OnEnable()
  {
    if (this.mInitDone)
      this.OnHover(UICamera.IsHighlighted(this.gameObject));
    if (UICamera.currentTouch == null)
      return;
    if ((UnityEngine.Object) UICamera.currentTouch.pressed == (UnityEngine.Object) this.gameObject)
    {
      this.OnPress(true);
    }
    else
    {
      if (!((UnityEngine.Object) UICamera.currentTouch.current == (UnityEngine.Object) this.gameObject))
        return;
      this.OnHover(true);
    }
  }

  protected virtual void OnDisable()
  {
    if (!this.mInitDone || !((UnityEngine.Object) this.tweenTarget != (UnityEngine.Object) null))
      return;
    this.SetState(UIButtonColor.State.Normal, true);
    TweenColor component = this.tweenTarget.GetComponent<TweenColor>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.value = this.mDefaultColor;
    component.enabled = false;
  }

  protected virtual void OnHover(bool isOver)
  {
    if (!this.isEnabled)
      return;
    if (!this.mInitDone)
      this.OnInit();
    if (!((UnityEngine.Object) this.tweenTarget != (UnityEngine.Object) null))
      return;
    this.SetState(!isOver ? UIButtonColor.State.Normal : UIButtonColor.State.Hover, false);
  }

  protected virtual void OnPress(bool isPressed)
  {
    if (!this.isEnabled || UICamera.currentTouch == null)
      return;
    if (!this.mInitDone)
      this.OnInit();
    if (!((UnityEngine.Object) this.tweenTarget != (UnityEngine.Object) null))
      return;
    if (isPressed)
      this.SetState(UIButtonColor.State.Pressed, false);
    else if ((UnityEngine.Object) UICamera.currentTouch.current == (UnityEngine.Object) this.gameObject)
    {
      if (UICamera.currentScheme == UICamera.ControlScheme.Controller)
        this.SetState(UIButtonColor.State.Hover, false);
      else if (UICamera.currentScheme == UICamera.ControlScheme.Mouse && (UnityEngine.Object) UICamera.hoveredObject == (UnityEngine.Object) this.gameObject)
        this.SetState(UIButtonColor.State.Hover, false);
      else
        this.SetState(UIButtonColor.State.Normal, false);
    }
    else
      this.SetState(UIButtonColor.State.Normal, false);
  }

  protected virtual void OnDragOver()
  {
    if (!this.isEnabled)
      return;
    if (!this.mInitDone)
      this.OnInit();
    if (!((UnityEngine.Object) this.tweenTarget != (UnityEngine.Object) null))
      return;
    this.SetState(UIButtonColor.State.Pressed, false);
  }

  protected virtual void OnDragOut()
  {
    if (!this.isEnabled)
      return;
    if (!this.mInitDone)
      this.OnInit();
    if (!((UnityEngine.Object) this.tweenTarget != (UnityEngine.Object) null))
      return;
    this.SetState(UIButtonColor.State.Normal, false);
  }

  protected virtual void OnSelect(bool isSelected)
  {
    if (!this.isEnabled || !((UnityEngine.Object) this.tweenTarget != (UnityEngine.Object) null))
      return;
    if (UICamera.currentScheme == UICamera.ControlScheme.Controller)
    {
      this.OnHover(isSelected);
    }
    else
    {
      if (isSelected || UICamera.touchCount >= 2)
        return;
      this.OnHover(isSelected);
    }
  }

  public virtual void SetState(UIButtonColor.State state, bool instant)
  {
    if (!this.mInitDone)
    {
      this.mInitDone = true;
      this.OnInit();
    }
    if (this.mState == state)
      return;
    this.mState = state;
    this.UpdateColor(instant);
  }

  public void UpdateColor(bool instant)
  {
    if ((UnityEngine.Object) this.tweenTarget == (UnityEngine.Object) null)
      return;
    TweenColor tweenColor;
    switch (this.mState)
    {
      case UIButtonColor.State.Hover:
        tweenColor = TweenColor.Begin(this.tweenTarget, this.duration, this.hover);
        break;
      case UIButtonColor.State.Pressed:
        tweenColor = TweenColor.Begin(this.tweenTarget, this.duration, this.pressed);
        break;
      case UIButtonColor.State.Disabled:
        tweenColor = TweenColor.Begin(this.tweenTarget, this.duration, this.disabledColor);
        break;
      default:
        tweenColor = TweenColor.Begin(this.tweenTarget, this.duration, this.mDefaultColor);
        break;
    }
    if (!instant || !((UnityEngine.Object) tweenColor != (UnityEngine.Object) null))
      return;
    tweenColor.value = tweenColor.to;
    tweenColor.enabled = false;
  }

  public enum State
  {
    Normal,
    Hover,
    Pressed,
    Disabled,
  }
}
