﻿// Decompiled with JetBrains decompiler
// Type: UserSettings
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Language;
using System;
using System.Collections.Generic;
using System.Linq;
using SystemMap3D;
using UnityEngine;

public class UserSettings
{
  private static readonly UserSetting[] localSettings = new UserSetting[16]{ UserSetting.GraphicsQuality, UserSetting.ViewDistance, UserSetting.AntiAliasing, UserSetting.ShowGlowEffect, UserSetting.ShowStarFog, UserSetting.ShowStarDust, UserSetting.Fullscreen, UserSetting.HighResModels, UserSetting.HighResTextures, UserSetting.UseProceduralTextures, UserSetting.FlakFieldDensity, UserSetting.JoystickGamepadEnabled, UserSetting.HighQualityParticles, UserSetting.AnisotropicFiltering, UserSetting.ShowShipSkins, UserSetting.ShowWeaponModules };
  private static readonly UserSettings lowPreset = new UserSettings();
  private readonly IDictionary<UserSetting, object> settings = (IDictionary<UserSetting, object>) new Dictionary<UserSetting, object>();
  private static readonly UserSettings mediumPreset;
  private static readonly UserSettings highPreset;
  private static readonly UserSettings ultraPreset;
  private static readonly UserSettings defaultSettings;

  public static UserSettings DefaultSettings
  {
    get
    {
      return new UserSettings(UserSettings.defaultSettings);
    }
  }

  public UserSettings LocalSettings
  {
    get
    {
      UserSettings userSettings = new UserSettings();
      foreach (UserSetting localSetting in UserSettings.localSettings)
        userSettings.Set(localSetting, this.Get(localSetting));
      return userSettings;
    }
  }

  public bool CombatGui
  {
    get
    {
      return this.Get<bool>(UserSetting.CombatGui);
    }
  }

  public bool CombatText
  {
    get
    {
      return this.Get<bool>(UserSetting.CombatText);
    }
  }

  public bool ShowFps
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowFpsAndPing);
    }
  }

  public bool ShowTutorial
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowTutorial);
    }
  }

  public SpaceCameraBase.CameraMode CameraMode
  {
    get
    {
      return this.Get<SpaceCameraBase.CameraMode>(UserSetting.CameraMode);
    }
  }

  public float CameraZoom
  {
    get
    {
      return this.Get<float>(UserSetting.CameraZoom);
    }
  }

  public bool AdvancedFlightControls
  {
    get
    {
      return this.Get<bool>(UserSetting.AdvancedFlightControls);
    }
  }

  public float DeadZoneMouse
  {
    get
    {
      return this.Get<float>(UserSetting.DeadZoneMouse);
    }
  }

  public float DeadZoneJoystick
  {
    get
    {
      return this.Get<float>(UserSetting.DeadZoneJoystick);
    }
  }

  public float SensitivityJoystick
  {
    get
    {
      return this.Get<float>(UserSetting.SensitivityJoystick);
    }
  }

  public bool JoystickGamepadEnabled
  {
    get
    {
      return this.Get<bool>(UserSetting.JoystickGamepadEnabled);
    }
  }

  public bool ShowXbox360Buttons
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowXbox360Buttons);
    }
  }

  public float SoundVolume
  {
    get
    {
      return this.Get<float>(UserSetting.SoundVolume);
    }
  }

  public float MusicVolume
  {
    get
    {
      return this.Get<float>(UserSetting.MusicVolume);
    }
  }

  public float ViewDistance
  {
    get
    {
      return this.Get<float>(UserSetting.ViewDistance);
    }
  }

  public bool Fullscreen
  {
    get
    {
      return this.Get<bool>(UserSetting.Fullscreen);
    }
  }

  public bool Fullframe
  {
    get
    {
      return this.Get<bool>(UserSetting.Fullframe);
    }
  }

  public int AntiAliasing
  {
    get
    {
      return this.Get<int>(UserSetting.AntiAliasing);
    }
  }

  public bool InvertedVertical
  {
    get
    {
      return this.Get<bool>(UserSetting.InvertedVertical);
    }
  }

  public bool StatsIndication
  {
    get
    {
      return this.Get<bool>(UserSetting.StatsIndication);
    }
  }

  public float2 LootPanelPosition
  {
    get
    {
      return this.Get<float2>(UserSetting.LootPanelPosition);
    }
  }

  public float2 InventoryPanelPosition
  {
    get
    {
      return this.Get<float2>(UserSetting.InventoryPanelPosition);
    }
  }

  public bool ShowStarFog
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowStarFog);
    }
  }

  public bool ShowStarDust
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowStarDust);
    }
  }

  public bool ShowGlowEffect
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowGlowEffect);
    }
  }

  public bool ChatShowPrefix
  {
    get
    {
      return this.Get<bool>(UserSetting.ChatShowPrefix);
    }
  }

  public bool ChatViewGlobal
  {
    get
    {
      return this.Get<bool>(UserSetting.ChatViewGlobal);
    }
  }

  public bool ChatViewLocal
  {
    get
    {
      return this.Get<bool>(UserSetting.ChatViewLocal);
    }
  }

  public bool AutoLoot
  {
    get
    {
      return this.Get<bool>(UserSetting.AutoLoot);
    }
  }

  public bool ShowCutscenes
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowCutscenes);
    }
  }

  public bool ShowMissionArrow
  {
    get
    {
      return this.Get<bool>(UserSetting.HudIndicatorShowMissionArrow);
    }
  }

  public bool ShowHeavyFightingMessages
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowHeavyFightingMessages);
    }
  }

  public bool ShowMiningShipMessages
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowMiningShipMessages);
    }
  }

  public bool ShowOutpostMessages
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowOutpostMessages);
    }
  }

  public bool ShowExperienceMessages
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowExperienceMessages);
    }
  }

  public bool ShowPopups
  {
    get
    {
      return this.Get<bool>(UserSetting.ShowPopups);
    }
  }

  public MouseWheelBinding MouseWheel
  {
    get
    {
      return this.Get<MouseWheelBinding>(UserSetting.MouseWheelBinding);
    }
  }

  public HudIndicatorColorSchemeMode HudIndicatorColorScheme
  {
    get
    {
      return this.Get<HudIndicatorColorSchemeMode>(UserSetting.HudIndicatorColorScheme);
    }
  }

  public List<HelpScreenType> CompletedTutorials
  {
    get
    {
      return this.Get<List<HelpScreenType>>(UserSetting.CompletedTutorials);
    }
  }

  public bool ShowHUBFog
  {
    get
    {
      return this.Get<bool>(UserSetting.HighQualityParticles);
    }
  }

  public List<UserSetting> DefinedSettings
  {
    get
    {
      return new List<UserSetting>((IEnumerable<UserSetting>) this.settings.Keys);
    }
  }

  static UserSettings()
  {
    UserSettings.lowPreset.Set(UserSetting.GraphicsQuality, (object) GraphicsQuality.Low);
    UserSettings.lowPreset.Set(UserSetting.FramerateCapping, (object) true);
    UserSettings.lowPreset.Set(UserSetting.ViewDistance, (object) 0.25f);
    UserSettings.lowPreset.Set(UserSetting.AntiAliasing, (object) 0);
    UserSettings.lowPreset.Set(UserSetting.ShowGlowEffect, (object) false);
    UserSettings.lowPreset.Set(UserSetting.ShowStarDust, (object) false);
    UserSettings.lowPreset.Set(UserSetting.ShowStarFog, (object) false);
    UserSettings.lowPreset.Set(UserSetting.HighResModels, (object) false);
    UserSettings.lowPreset.Set(UserSetting.HighResTextures, (object) false);
    UserSettings.lowPreset.Set(UserSetting.UseProceduralTextures, (object) false);
    UserSettings.lowPreset.Set(UserSetting.ShowBulletImpactFx, (object) false);
    UserSettings.lowPreset.Set(UserSetting.FlakFieldDensity, (object) 0.1f);
    UserSettings.lowPreset.Set(UserSetting.HighQualityParticles, (object) false);
    UserSettings.lowPreset.Set(UserSetting.AnisotropicFiltering, (object) false);
    UserSettings.lowPreset.Set(UserSetting.ShowShipSkins, (object) false);
    UserSettings.lowPreset.Set(UserSetting.ShowWeaponModules, (object) false);
    UserSettings.mediumPreset = new UserSettings();
    UserSettings.mediumPreset.Set(UserSetting.GraphicsQuality, (object) GraphicsQuality.Medium);
    UserSettings.mediumPreset.Set(UserSetting.FramerateCapping, (object) false);
    UserSettings.mediumPreset.Set(UserSetting.ViewDistance, (object) 0.75f);
    UserSettings.mediumPreset.Set(UserSetting.AntiAliasing, (object) 0);
    UserSettings.mediumPreset.Set(UserSetting.ShowGlowEffect, (object) true);
    UserSettings.mediumPreset.Set(UserSetting.ShowStarDust, (object) true);
    UserSettings.mediumPreset.Set(UserSetting.ShowStarFog, (object) true);
    UserSettings.mediumPreset.Set(UserSetting.HighResModels, (object) true);
    UserSettings.mediumPreset.Set(UserSetting.HighResTextures, (object) true);
    UserSettings.mediumPreset.Set(UserSetting.UseProceduralTextures, (object) true);
    UserSettings.mediumPreset.Set(UserSetting.ShowBulletImpactFx, (object) false);
    UserSettings.mediumPreset.Set(UserSetting.FlakFieldDensity, (object) 0.3f);
    UserSettings.mediumPreset.Set(UserSetting.HighQualityParticles, (object) false);
    UserSettings.mediumPreset.Set(UserSetting.AnisotropicFiltering, (object) false);
    UserSettings.mediumPreset.Set(UserSetting.ShowShipSkins, (object) true);
    UserSettings.mediumPreset.Set(UserSetting.ShowWeaponModules, (object) false);
    UserSettings.highPreset = new UserSettings();
    UserSettings.highPreset.Set(UserSetting.GraphicsQuality, (object) GraphicsQuality.High);
    UserSettings.highPreset.Set(UserSetting.FramerateCapping, (object) false);
    UserSettings.highPreset.Set(UserSetting.ViewDistance, (object) 1f);
    UserSettings.highPreset.Set(UserSetting.AntiAliasing, (object) 2);
    UserSettings.highPreset.Set(UserSetting.ShowGlowEffect, (object) true);
    UserSettings.highPreset.Set(UserSetting.ShowStarDust, (object) true);
    UserSettings.highPreset.Set(UserSetting.ShowStarFog, (object) true);
    UserSettings.highPreset.Set(UserSetting.HighResModels, (object) true);
    UserSettings.highPreset.Set(UserSetting.HighResTextures, (object) true);
    UserSettings.highPreset.Set(UserSetting.UseProceduralTextures, (object) true);
    UserSettings.highPreset.Set(UserSetting.ShowBulletImpactFx, (object) true);
    UserSettings.highPreset.Set(UserSetting.FlakFieldDensity, (object) 0.7f);
    UserSettings.highPreset.Set(UserSetting.HighQualityParticles, (object) true);
    UserSettings.highPreset.Set(UserSetting.AnisotropicFiltering, (object) true);
    UserSettings.highPreset.Set(UserSetting.ShowShipSkins, (object) true);
    UserSettings.highPreset.Set(UserSetting.ShowWeaponModules, (object) true);
    UserSettings.ultraPreset = new UserSettings();
    UserSettings.ultraPreset.Set(UserSetting.GraphicsQuality, (object) GraphicsQuality.Ultra);
    UserSettings.ultraPreset.Set(UserSetting.FramerateCapping, (object) false);
    UserSettings.ultraPreset.Set(UserSetting.ViewDistance, (object) 1f);
    UserSettings.ultraPreset.Set(UserSetting.AntiAliasing, (object) 8);
    UserSettings.ultraPreset.Set(UserSetting.ShowGlowEffect, (object) true);
    UserSettings.ultraPreset.Set(UserSetting.ShowStarDust, (object) true);
    UserSettings.ultraPreset.Set(UserSetting.ShowStarFog, (object) true);
    UserSettings.ultraPreset.Set(UserSetting.HighResModels, (object) true);
    UserSettings.ultraPreset.Set(UserSetting.HighResTextures, (object) true);
    UserSettings.ultraPreset.Set(UserSetting.UseProceduralTextures, (object) true);
    UserSettings.ultraPreset.Set(UserSetting.ShowBulletImpactFx, (object) true);
    UserSettings.ultraPreset.Set(UserSetting.FlakFieldDensity, (object) 1f);
    UserSettings.ultraPreset.Set(UserSetting.HighQualityParticles, (object) true);
    UserSettings.ultraPreset.Set(UserSetting.AnisotropicFiltering, (object) true);
    UserSettings.ultraPreset.Set(UserSetting.ShowShipSkins, (object) true);
    UserSettings.ultraPreset.Set(UserSetting.ShowWeaponModules, (object) true);
    UserSettings.defaultSettings = new UserSettings();
    UserSettings.defaultSettings.Set(UserSetting.CombatGui, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.CombatText, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.ShowDamageOverlay, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ShowTutorial, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.CameraMode, (object) SpaceCameraBase.CameraMode.Chase);
    UserSettings.defaultSettings.Set(UserSetting.AdvancedFlightControls, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ShowChangeFaction, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.InvertedVertical, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.DeadZoneMouse, (object) 0.0f);
    UserSettings.defaultSettings.Set(UserSetting.DeadZoneJoystick, (object) 0.3f);
    UserSettings.defaultSettings.Set(UserSetting.SensitivityJoystick, (object) 1f);
    UserSettings.defaultSettings.Set(UserSetting.JoystickGamepadEnabled, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.ShowXbox360Buttons, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.MusicVolume, (object) 0.7f);
    UserSettings.defaultSettings.Set(UserSetting.SoundVolume, (object) 0.7f);
    UserSettings.defaultSettings.Set(UserSetting.StatsIndication, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.LootPanelPosition, (object) new float2(0.0f, 0.0f));
    UserSettings.defaultSettings.Set(UserSetting.InventoryPanelPosition, (object) new float2((float) (Screen.width - 300), 400f));
    UserSettings.defaultSettings.Set(UserSetting.CompletedTutorials, (object) new List<HelpScreenType>());
    UserSettings.defaultSettings.Set(UserSetting.GraphicsQuality, (object) GraphicsQuality.Medium);
    UserSettings.defaultSettings.Set(UserSetting.AssignmentsCollapsed, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.AutomaticAmmoReload, (object) false);
    UserSettings.defaultSettings.ApplyGraphicPreset(GraphicsQuality.Medium);
    UserSettings.defaultSettings.Set(UserSetting.VSync, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.Fullscreen, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.FramerateCapping, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.Fullframe, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.ShowBulletImpactFx, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.FlakFieldDensity, (object) 1f);
    UserSettings.defaultSettings.Set(UserSetting.ChatShowPrefix, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ChatViewGlobal, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ChatViewLocal, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.MouseWheelBinding, (object) MouseWheelBinding.Thrust);
    UserSettings.defaultSettings.Set(UserSetting.AutoLoot, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ShowCutscenes, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorShowMissionArrow, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorTextSize, (object) 10f);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorMinimizeDistance, (object) 0.5f);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorDescriptionDisplayDistance, (object) 0.5f);
    UserSettings.defaultSettings.Set(UserSetting.ShowPopups, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ShowXpBar, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorShowWingNames, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorShowTitles, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorShowTargetNames, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorShowShipNames, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorShowShipTierIcon, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorBracketResizing, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorSelectionCrosshair, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorHealthBar, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HudIndicatorColorScheme, (object) HudIndicatorColorSchemeMode.FactionColors);
    UserSettings.defaultSettings.Set(UserSetting.ShowOutpostMessages, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ShowHeavyFightingMessages, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.ShowAugmentMessages, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ShowMiningShipMessages, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ShowExperienceMessages, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ShowAssignmentMessages, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.Layout, (object) Layout.English);
    UserSettings.defaultSettings.Set(UserSetting.ShowFpsAndPing, (object) false);
    UserSettings.defaultSettings.Set(UserSetting.ShowEnemyIndication, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ShowFriendIndication, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.ShowWofConfirmation, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.HighQualityParticles, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.SystemMap3DTransitionMode, (object) TransitionMode.Smooth);
    UserSettings.defaultSettings.Set(UserSetting.SystemMap3DCameraView, (object) CameraView.Top);
    UserSettings.defaultSettings.Set(UserSetting.SystemMap3DShowAsteroids, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.SystemMap3DShowDynamicMissions, (object) true);
    UserSettings.defaultSettings.Set(UserSetting.SystemMap3DFormAsteroidGroups, (object) false);
  }

  public UserSettings(UserSettings userSettings)
  {
    foreach (UserSetting key in (IEnumerable<UserSetting>) userSettings.settings.Keys)
      this.Set(key, this.CopyValue(userSettings.settings[key]));
  }

  public UserSettings()
  {
  }

  public static UserSettingValueType GetValueType(UserSetting setting)
  {
    if (setting == UserSetting.CompletedTutorials)
      return UserSettingValueType.HelpScreenType;
    switch (setting - 1)
    {
      case (UserSetting) 0:
      case UserSetting.CombatGui:
      case (UserSetting) 4:
      case UserSetting.SoundVolume:
      case UserSetting.CombatGui | UserSetting.Fullscreen:
      case UserSetting.StatsIndication:
      case UserSetting.Layout:
      case UserSetting.AssignmentsCollapsed:
      case UserSetting.CameraMode | UserSetting.Layout:
      case UserSetting.ViewDistance:
      case UserSetting.ShowGlowEffect:
      case UserSetting.ShowChangeFaction:
      case UserSetting.MouseWheelBinding:
      case UserSetting.ChatViewLocal:
      case UserSetting.ChatViewGlobal:
      case UserSetting.AutoLoot:
      case UserSetting.GraphicsQuality | UserSetting.Layout:
      case UserSetting.ShowPopups:
      case UserSetting.ShowOutpostMessages:
      case UserSetting.ShowHeavyFightingMessages:
      case UserSetting.ShowAugmentMessages:
      case UserSetting.ShowMiningShipMessages:
      case UserSetting.ShowExperienceMessages:
      case UserSetting.HudIndicatorShowWingNames:
      case UserSetting.AntiAliasing:
      case UserSetting.StatsIndication | UserSetting.ShowPopups:
      case UserSetting.UseProceduralTextures:
      case UserSetting.ShowFpsAndPing:
      case UserSetting.ShowBulletImpactFx:
      case UserSetting.Layout | UserSetting.ShowPopups:
      case UserSetting.ShowEnemyIndication:
      case UserSetting.DeadZoneMouse:
      case UserSetting.HudIndicatorShowMissionArrow:
      case UserSetting.AutomaticAmmoReload:
      case UserSetting.CameraZoom:
      case UserSetting.JoystickGamepadEnabled:
      case UserSetting.ShowXbox360Buttons:
      case UserSetting.HudIndicatorShowTitles:
      case UserSetting.HighResModels:
      case UserSetting.HighResTextures:
      case UserSetting.HighQualityParticles:
      case UserSetting.AnisotropicFiltering:
      case UserSetting.ShowCutscenes:
      case UserSetting.MuteSound:
      case UserSetting.ShowDamageOverlay:
      case UserSetting.ShowShipSkins:
      case UserSetting.SystemMap3DCameraView:
      case UserSetting.SystemMap3DShowAsteroids:
      case UserSetting.SystemMap3DShowDynamicMissions:
      case UserSetting.HudIndicatorMinimizeDistance:
      case UserSetting.HudIndicatorColorScheme:
      case UserSetting.HudIndicatorShowShipTierIcon:
      case UserSetting.HudIndicatorDescriptionDisplayDistance:
      case UserSetting.HudIndicatorSelectionCrosshair:
      case UserSetting.ShowBulletImpactFx | UserSetting.AnisotropicFiltering:
      case UserSetting.ShowAssignmentMessages:
      case UserSetting.DeadZoneJoystick | UserSetting.AnisotropicFiltering:
        return UserSettingValueType.Boolean;
      case UserSetting.ShowTutorial:
      case UserSetting.CompletedTutorials:
      case UserSetting.GraphicsQuality:
      case UserSetting.AdvancedFlightControls:
        return UserSettingValueType.Integer;
      case UserSetting.InvertedVertical:
      case UserSetting.MusicVolume:
      case UserSetting.InvertedVertical | UserSetting.Layout:
      case UserSetting.CombatText:
      case UserSetting.ShowFriendIndication:
      case UserSetting.ShowWofConfirmation:
      case UserSetting.DeadZoneJoystick:
      case UserSetting.SensitivityJoystick:
      case UserSetting.ChatShowPrefix | UserSetting.AnisotropicFiltering:
      case UserSetting.HudIndicatorShowTargetNames:
      case UserSetting.HudIndicatorBracketResizing:
        return UserSettingValueType.Float;
      case UserSetting.HudIndicatorShowShipNames:
      case UserSetting.LootPanelPosition:
        return UserSettingValueType.Float2;
      case UserSetting.ChatShowPrefix:
      case UserSetting.ShowWeaponModules:
      case UserSetting.SystemMap3DTransitionMode:
      case UserSetting.HudIndicatorTextSize:
        return UserSettingValueType.Byte;
      default:
        Debug.LogWarning((object) ("No value Type found for: " + (object) setting + " (assuming bool)"));
        return UserSettingValueType.Boolean;
    }
  }

  public UserSettings Diff(UserSettings newSettings)
  {
    UserSettings userSettings = new UserSettings();
    foreach (UserSetting key in (IEnumerable<UserSetting>) newSettings.settings.Keys)
    {
      object obj1 = !this.settings.ContainsKey(key) ? (object) null : this.settings[key];
      object obj2 = newSettings.settings[key];
      if (!this.CompareValue(obj1, obj2))
        userSettings.Set(key, obj2);
    }
    return userSettings;
  }

  public void Set(UserSetting key, object value)
  {
    this.settings[key] = value;
  }

  public object Get(UserSetting key)
  {
    return this.Get<object>(key);
  }

  private T Get<T>(UserSetting key)
  {
    return (T) (!this.settings.ContainsKey(key) ? (!UserSettings.defaultSettings.settings.ContainsKey(key) ? (object) null : UserSettings.defaultSettings.settings[key]) : this.settings[key]);
  }

  private object CopyValue(object value)
  {
    if (value is List<HelpScreenType>)
      return (object) new List<HelpScreenType>((IEnumerable<HelpScreenType>) value);
    return value;
  }

  private bool CompareValue(object value1, object value2)
  {
    if (!(value1 is List<HelpScreenType>) || !(value2 is List<HelpScreenType>))
      return value1 == value2;
    List<HelpScreenType> source = (List<HelpScreenType>) value1;
    List<HelpScreenType> helpScreenTypeList = (List<HelpScreenType>) value2;
    return source.OrderBy<HelpScreenType, HelpScreenType>((Func<HelpScreenType, HelpScreenType>) (h => h)).SequenceEqual<HelpScreenType>((IEnumerable<HelpScreenType>) helpScreenTypeList);
  }

  public void ApplyGraphicPreset(GraphicsQuality graphicsQuality)
  {
    UserSettings userSettings;
    switch (graphicsQuality)
    {
      case GraphicsQuality.Low:
        userSettings = UserSettings.lowPreset;
        break;
      case GraphicsQuality.Medium:
        userSettings = UserSettings.mediumPreset;
        break;
      case GraphicsQuality.High:
        userSettings = UserSettings.highPreset;
        break;
      case GraphicsQuality.Ultra:
        userSettings = UserSettings.ultraPreset;
        break;
      default:
        return;
    }
    using (List<UserSetting>.Enumerator enumerator = userSettings.DefinedSettings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        this.Set(current, userSettings.Get(current));
      }
    }
  }
}
