﻿// Decompiled with JetBrains decompiler
// Type: HangarShip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class HangarShip
{
  public string Name = string.Empty;
  public List<StickerBinding> Stickers = new List<StickerBinding>();
  protected Dictionary<ushort, ShipSlot> slots = new Dictionary<ushort, ShipSlot>();
  private List<ShipAbility> abilityToolbar = new List<ShipAbility>();
  private HangarShip.AbilitySlotComparer abilitySort = new HangarShip.AbilitySlotComparer();
  public const int MAX_ABILITY_COUNT = 10;
  private ushort serverID;
  private uint guid;
  private float durability;
  public ShipCard Card;
  public GUICard GUICard;
  public ShopItemCard shopItemCard;
  public CameraCard CameraCard;
  public Flag IsLoaded;
  private ShipAspects shipAspects;
  public Flag SlotsCreated;

  public ushort ServerID
  {
    get
    {
      return this.serverID;
    }
  }

  public uint GUID
  {
    get
    {
      return this.guid;
    }
  }

  public bool IsUpgraded
  {
    get
    {
      return this.Card.IsUpgraded;
    }
  }

  public bool IsVariant
  {
    get
    {
      return this.Card.HasParent;
    }
  }

  public float Durability
  {
    get
    {
      if ((double) this.durability < 0.0)
        return this.Card.Durability;
      return this.durability;
    }
  }

  public float Quality
  {
    get
    {
      return this.Durability / this.Card.Durability;
    }
  }

  public bool NeedsRepair
  {
    get
    {
      return (double) this.Durability < (double) this.Card.Durability;
    }
  }

  public ReadOnlyCollection<ShipSlot> Slots
  {
    get
    {
      return new List<ShipSlot>((IEnumerable<ShipSlot>) this.slots.Values).AsReadOnly();
    }
  }

  public ReadOnlyCollection<ShipAbility> AbilityToolbar
  {
    get
    {
      return this.abilityToolbar.AsReadOnly();
    }
  }

  public bool IsCapitalShip
  {
    get
    {
      return (int) this.ServerID == 12;
    }
  }

  public ShipAspects ShipAspects
  {
    get
    {
      return this.shipAspects;
    }
  }

  public bool AnySystemNeedsRepair
  {
    get
    {
      foreach (ShipSlot slot in this.Slots)
      {
        if (slot.System != null && (double) slot.System.Quality < 0.990000009536743)
          return true;
      }
      return false;
    }
  }

  public HangarShip()
  {
  }

  public HangarShip(ushort serverID, uint guid)
  {
    this.serverID = serverID;
    this.guid = guid;
    this.GUICard = (GUICard) Game.Catalogue.FetchCard(guid, CardView.GUI);
    this.shopItemCard = (ShopItemCard) Game.Catalogue.FetchCard(guid, CardView.Price);
    this.Card = (ShipCard) Game.Catalogue.FetchCard(guid, CardView.Ship);
    this.CameraCard = (CameraCard) Game.Catalogue.FetchCard(guid, CardView.Camera);
    this.shipAspects = new ShipAspects();
    this.IsLoaded = new Flag();
    this.SlotsCreated = new Flag();
    this.IsLoaded.Depend((ILoadable) this.GUICard, (ILoadable) this.shopItemCard, (ILoadable) this.Card, (ILoadable) this.CameraCard);
    this.IsLoaded.Set();
  }

  public ShipSlot GetSlot(ushort slotID)
  {
    ShipSlot shipSlot;
    this.slots.TryGetValue(slotID, out shipSlot);
    return shipSlot;
  }

  public void SwitchAmmoAllSlots(ItemCountable consumable)
  {
    using (Dictionary<ushort, ShipSlot>.Enumerator enumerator = this.slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ushort, ShipSlot> current = enumerator.Current;
        if (current.Value.Ability != null && current.Value.Ability.card.ConsumableOption != ShipConsumableOption.NotUsing && ((int) current.Value.Ability.card.ConsumableType == (int) consumable.Card.ConsumableType && (int) current.Value.Ability.card.ConsumableTier == (int) consumable.Card.Tier))
          current.Value.Ability.slot.SelectConsumable(consumable);
      }
    }
  }

  public void DisableSlotAbilitiesByTarget(ISpaceEntity target)
  {
    using (Dictionary<ushort, ShipSlot>.Enumerator enumerator = this.slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ushort, ShipSlot> current = enumerator.Current;
        if (current.Value.Ability != null && current.Value.Ability.validation.Target == target)
          current.Value.Ability.On = false;
      }
    }
  }

  public void DisableSlotAbility(ushort slotId)
  {
    using (Dictionary<ushort, ShipSlot>.Enumerator enumerator = this.slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ushort, ShipSlot> current = enumerator.Current;
        if ((int) current.Key == (int) slotId && current.Value.Ability != null)
          current.Value.Ability.On = false;
      }
    }
  }

  public void DisableAllSlotAbilities()
  {
    using (Dictionary<ushort, ShipSlot>.Enumerator enumerator = this.slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ushort, ShipSlot> current = enumerator.Current;
        if (current.Value.Ability != null)
          current.Value.Ability.On = false;
      }
    }
  }

  public bool HasAspect(ShipAspect aspect)
  {
    if (this.shipAspects == null)
      return false;
    return this.shipAspects.ContainsAspect(aspect);
  }

  public void BindSticker(StickerBinding binding)
  {
    PlayerProtocol.GetProtocol().BindSticker(this.ServerID, binding);
  }

  public void UnbindSticker(SpotDesc spot)
  {
    PlayerProtocol.GetProtocol().UnbindSticker(this.ServerID, spot.ObjectPointServerHash);
  }

  public void Repair(float repairValue, bool useCubits)
  {
    PlayerProtocol.GetProtocol().RepairShip(this.ServerID, repairValue, useCubits);
  }

  public void SetName(string name)
  {
    PlayerProtocol.GetProtocol().SetShipName(this.ServerID, name);
  }

  public void SetShipAspects(ShipAspects shipAspects)
  {
    this.shipAspects = shipAspects;
  }

  public void Upgrade()
  {
    PlayerProtocol.GetProtocol().UpgradeShip(this.ServerID);
  }

  public void _SetDurability(float durability)
  {
    this.durability = durability;
  }

  public void _AddStickers(IEnumerable<StickerBinding> newStickers)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    HangarShip.\u003C_AddStickers\u003Ec__AnonStorey107 stickersCAnonStorey107 = new HangarShip.\u003C_AddStickers\u003Ec__AnonStorey107();
    foreach (StickerBinding newSticker in newStickers)
    {
      // ISSUE: reference to a compiler-generated field
      stickersCAnonStorey107.newSticker = newSticker;
      // ISSUE: reference to a compiler-generated method
      this.Stickers.Remove(this.Stickers.Find(new Predicate<StickerBinding>(stickersCAnonStorey107.\u003C\u003Em__25C)));
    }
    this.Stickers.AddRange(newStickers);
  }

  public void _RemoveStickers(IEnumerable<ushort> objectPointHashes)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    HangarShip.\u003C_RemoveStickers\u003Ec__AnonStorey108 stickersCAnonStorey108 = new HangarShip.\u003C_RemoveStickers\u003Ec__AnonStorey108();
    foreach (ushort objectPointHash in objectPointHashes)
    {
      // ISSUE: reference to a compiler-generated field
      stickersCAnonStorey108.objectPointHash = objectPointHash;
      // ISSUE: reference to a compiler-generated method
      this.Stickers.Remove(this.Stickers.Find(new Predicate<StickerBinding>(stickersCAnonStorey108.\u003C\u003Em__25D)));
    }
  }

  public void _AddSlot(ushort serverId, ShipSlot newSlot)
  {
    if (this.slots.ContainsKey(serverId))
      Debug.LogWarning((object) ("HangarShip: Trying to add already existing slot: " + (object) newSlot));
    this.slots.Add(serverId, newSlot);
  }

  public void AllSlotsCreated()
  {
    this.SlotsCreated.Set();
  }

  public void UpdateSlotListeners()
  {
    foreach (ShipSlot slot in this.Slots)
    {
      if (slot.System != null && (double) slot.System.Durability <= 0.0 && !slot.System.Card.Indestructible)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: reference to a compiler-generated method
        slot.System.IsLoaded.AddHandler(new SignalHandler(new HangarShip.\u003CUpdateSlotListeners\u003Ec__AnonStorey109()
        {
          slot = slot
        }.\u003C\u003Em__25E));
      }
    }
    this._UpdateToolbar();
  }

  public void _UpdateValidation(List<AbilityValidationDesc> validationDescs)
  {
    using (List<AbilityValidationDesc>.Enumerator enumerator = validationDescs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AbilityValidationDesc current = enumerator.Current;
        ShipSlot shipSlot = (ShipSlot) null;
        if (current.SlotCategory == GameProtocol.SlotCategory.Slot && this.slots.TryGetValue(current.SlotID, out shipSlot))
          shipSlot.UpdatedStats = true;
      }
    }
  }

  public void _ActivateOnByDefaultSlots()
  {
    using (Dictionary<ushort, ShipSlot>.ValueCollection.Enumerator enumerator = this.slots.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipSlot current = enumerator.Current;
        if ((bool) current.IsLoaded && current.Ability != null && ((bool) current.Ability.card.IsLoaded && current.Ability.card.OnByDefault))
          current.Ability.On = true;
      }
    }
  }

  public void _UpdateToolbar()
  {
    this.abilityToolbar.Clear();
    using (Dictionary<ushort, ShipSlot>.ValueCollection.Enumerator enumerator = this.slots.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipSlot current = enumerator.Current;
        if (current != null && (bool) current.IsLoaded && (current.Ability != null && !current.IsWeaponTypeSlot) && current.Ability != null)
          this.abilityToolbar.Add(current.Ability);
      }
    }
    this.abilityToolbar.Sort((IComparer<ShipAbility>) this.abilitySort);
  }

  public void OnTargetLost(SpaceObject obj)
  {
    using (Dictionary<ushort, ShipSlot>.ValueCollection.Enumerator enumerator = this.slots.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipSlot current = enumerator.Current;
        if ((bool) current.IsLoaded && current.Ability != null && (current.Ability.IsMultiTargetAbility && current.Ability.Target == obj))
          current.Ability.ClearTarget();
      }
    }
  }

  public void ResetSlots()
  {
    using (Dictionary<ushort, ShipSlot>.ValueCollection.Enumerator enumerator = this.slots.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipSlot current = enumerator.Current;
        if ((bool) current.IsLoaded && current.Ability != null)
          current.Ability.ClearTarget();
      }
    }
  }

  private static int ParseSlotTypeforSort(ShipSlotType slotType)
  {
    switch (slotType)
    {
      case ShipSlotType.undefined:
        return 6;
      case ShipSlotType.computer:
        return 1;
      case ShipSlotType.engine:
        return 3;
      case ShipSlotType.hull:
        return 2;
      case ShipSlotType.weapon:
        return 0;
      case ShipSlotType.ship_paint:
        return 4;
      case ShipSlotType.avionics:
        return 5;
      default:
        return 6;
    }
  }

  public void DebugPrintSlots()
  {
    int num = 0;
    using (Dictionary<ushort, ShipSlot>.Enumerator enumerator = this.slots.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ushort, ShipSlot> current = enumerator.Current;
        ++num;
        Debug.Log((object) (num.ToString() + ": Id: " + (object) current.Key + " Slot: " + (object) current.Value));
      }
    }
  }

  private class AbilitySlotComparer : IComparer<ShipAbility>
  {
    public int Compare(ShipAbility x, ShipAbility y)
    {
      if (x.slot.System.Card.SlotType == y.slot.System.Card.SlotType)
        return x.slot.ServerID.CompareTo(y.slot.ServerID);
      return HangarShip.ParseSlotTypeforSort(x.slot.System.Card.SlotType).CompareTo(HangarShip.ParseSlotTypeforSort(y.slot.System.Card.SlotType));
    }
  }
}
