﻿// Decompiled with JetBrains decompiler
// Type: AvatarLevelProfile
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AvatarLevelProfile : LevelProfile
{
  public string SceneName
  {
    get
    {
      return "AvatarLevel";
    }
  }

  public bool Ready
  {
    get
    {
      return true;
    }
  }

  public IEnumerable<string> RequiredAssets
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return (IEnumerable<string>) new List<string>() { "character_creation_human.prefab", "avatar_human_male.prefab", "avatar_human_female.prefab" };
      return (IEnumerable<string>) new List<string>() { "character_creation_cylon.prefab", "avatar_cylon_centurion.prefab" };
    }
  }
}
