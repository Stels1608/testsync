﻿// Decompiled with JetBrains decompiler
// Type: SectorSlotData
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class SectorSlotData
{
  public Dictionary<GUICard, uint> CurrentSlotsForShip { get; private set; }

  public Dictionary<GUICard, uint> MaxSlotsForShip { get; private set; }

  public Dictionary<Faction, uint> CurrentSlotsForFaction { get; private set; }

  public Dictionary<Faction, uint> MaxSlotsForFaction { get; private set; }

  public uint CurrentUsedSlots { get; set; }

  public uint MaxTotalSlots { get; set; }

  public SectorSlotData()
  {
    this.CurrentSlotsForShip = new Dictionary<GUICard, uint>();
    this.MaxSlotsForShip = new Dictionary<GUICard, uint>();
    this.CurrentSlotsForFaction = new Dictionary<Faction, uint>();
    this.MaxSlotsForFaction = new Dictionary<Faction, uint>();
    this.MaxTotalSlots = uint.MaxValue;
  }

  public void AddShipSectorCap(uint shipGuid, uint current, uint max)
  {
    GUICard key = (GUICard) Game.Catalogue.FetchCard(shipGuid, CardView.GUI);
    this.CurrentSlotsForShip.Add(key, current);
    this.MaxSlotsForShip.Add(key, max);
  }

  public void SetFactionSlots(Faction faction, uint current, uint max)
  {
    this.CurrentSlotsForFaction.Add(faction, current);
    this.MaxSlotsForFaction.Add(faction, max);
  }

  public int GetMaxSlotsForMyFaction()
  {
    Faction key = Game.Me.Faction;
    if (this.MaxSlotsForFaction.ContainsKey(key))
      return (int) this.MaxSlotsForFaction[key];
    return int.MaxValue;
  }

  public int GetFreeSlotsForMyFaction()
  {
    Faction key = Game.Me.Faction;
    int num1 = !this.MaxSlotsForFaction.ContainsKey(key) ? int.MaxValue : (int) this.MaxSlotsForFaction[key];
    if (num1 == 0 || this.CurrentUsedSlots >= this.MaxTotalSlots)
      return 0;
    uint num2 = !this.CurrentSlotsForFaction.ContainsKey(key) ? 0U : this.CurrentSlotsForFaction[key];
    return Mathf.Min(Mathf.Clamp(num1 - (int) num2, 0, int.MaxValue), Mathf.Clamp((int) this.MaxTotalSlots - (int) this.CurrentUsedSlots, 0, int.MaxValue));
  }

  public int GetFreeSlotsForShip(GUICard guiCard)
  {
    int num1 = !this.MaxSlotsForShip.ContainsKey(guiCard) ? int.MaxValue : (int) this.MaxSlotsForShip[guiCard];
    if (num1 == 0)
      return 0;
    uint num2 = !this.CurrentSlotsForShip.ContainsKey(guiCard) ? 0U : this.CurrentSlotsForShip[guiCard];
    return Mathf.Clamp(num1 - (int) num2, 0, int.MaxValue);
  }
}
