﻿// Decompiled with JetBrains decompiler
// Type: PlanetScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlanetScript : ModelScript
{
  public Renderer Renderer;
  public PlanetAtmosphere2 Athmospere;
  public bool RenderInBackground;

  private void Start()
  {
    Planet planet = this.SpaceObject as Planet;
    if (planet == null)
    {
      Debug.LogError((object) "PlanetScript have to be attached to Planet");
    }
    else
    {
      this.Renderer.material.SetColor("_Color", planet.Color);
      this.Renderer.material.SetColor("_SpecColor", planet.SpecularColor);
      this.Renderer.material.SetFloat("_Shininess", planet.Shininess);
      if (!this.RenderInBackground)
      {
        if (!((Object) this.Athmospere != (Object) null))
          return;
        GameObject gameObjectWithTag = GameObject.FindGameObjectWithTag("Sun");
        if (!((Object) gameObjectWithTag != (Object) null))
          return;
        this.Athmospere.SunPosition = gameObjectWithTag.transform;
      }
      else
        this.SetLightDirectionPos(planet);
    }
  }

  private void SetLightDirectionPos(Planet planet)
  {
    if (planet == null)
      return;
    Transform transformInChildren = HelperUtil.FindTransformInChildren(planet.Root.gameObject.transform, Planet.LIGHTDIRNAME);
    if (!((Object) null != (Object) transformInChildren))
      return;
    transformInChildren.localPosition = planet.lightDirection;
  }

  private void Reset()
  {
    this.Renderer = (Renderer) this.GetComponentInChildren<MeshRenderer>();
    this.Athmospere = this.GetComponentInChildren<PlanetAtmosphere2>();
  }
}
