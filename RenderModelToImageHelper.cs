﻿// Decompiled with JetBrains decompiler
// Type: RenderModelToImageHelper
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RenderModelToImageHelper
{
  public List<RenderModelToImage> WhatToRender = new List<RenderModelToImage>();

  public void OnGUI()
  {
    for (int index = 0; index < this.WhatToRender.Count; ++index)
    {
      RenderModelToImage renderModelToImage = this.WhatToRender[index];
      if (renderModelToImage != null)
        renderModelToImage.OnRender();
    }
  }
}
