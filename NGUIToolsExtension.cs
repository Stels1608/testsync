﻿// Decompiled with JetBrains decompiler
// Type: NGUIToolsExtension
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public static class NGUIToolsExtension
{
  private static UIRoot _uiRoot;
  private static UICamera _uiCamera;
  private static GuiHookPoint _guiHookPoint;

  public static UIRoot GetUiRoot
  {
    get
    {
      return NGUIToolsExtension._uiRoot ?? (NGUIToolsExtension._uiRoot = Object.FindObjectOfType<UIRoot>());
    }
  }

  public static UICamera GetUiCamera
  {
    get
    {
      return NGUIToolsExtension._uiCamera ?? (NGUIToolsExtension._uiCamera = Object.FindObjectOfType<UICamera>());
    }
  }

  public static GuiHookPoint GetGuiHookPoint
  {
    get
    {
      return NGUIToolsExtension._guiHookPoint ?? (NGUIToolsExtension._guiHookPoint = Object.FindObjectOfType<GuiHookPoint>());
    }
  }

  public static UISprite AddSprite(GameObject go, UIAtlas atlas, string spriteName, bool keepDimensions, string instanceName = "")
  {
    if (string.IsNullOrEmpty(instanceName))
      instanceName = spriteName;
    UISprite uiSprite = NGUITools.AddSprite(go, atlas, spriteName);
    uiSprite.name = instanceName;
    if (keepDimensions)
    {
      uiSprite.width = atlas.GetSprite(spriteName).width;
      uiSprite.height = atlas.GetSprite(spriteName).height;
    }
    return uiSprite;
  }

  public static void SetSprite(UISprite uiSprite, UIAtlas atlas, string spriteName)
  {
    uiSprite.atlas = atlas;
    uiSprite.spriteName = spriteName;
    uiSprite.width = atlas.GetSprite(spriteName).width;
    uiSprite.height = atlas.GetSprite(spriteName).height;
  }

  public static UILabel AddLabel(GameObject go, Font font, int fontSize, UIWidget.Pivot pivot = UIWidget.Pivot.Center, string text = "")
  {
    UILabel uiLabel = NGUITools.AddChild<UILabel>(go);
    uiLabel.trueTypeFont = font;
    uiLabel.fontSize = fontSize;
    uiLabel.pivot = pivot;
    uiLabel.text = text;
    return uiLabel;
  }

  public static void AnchorTo(UIRect myRect, UIRect targetRect, UIAnchor.Side side, int offsetX = 0, int offsetY = 0)
  {
    float num1 = 0.0f;
    float num2 = 0.0f;
    float num3 = 0.0f;
    float num4 = 0.0f;
    float num5 = 0.0f;
    float num6 = 0.0f;
    float num7 = 0.0f;
    float num8 = 0.0f;
    float num9 = Mathf.Abs(myRect.localCorners[0].x - myRect.localCorners[2].x);
    float num10 = Mathf.Abs(myRect.localCorners[0].y - myRect.localCorners[2].y);
    switch (side)
    {
      case UIAnchor.Side.BottomLeft:
        num1 = num2 = 0.0f;
        num3 = num4 = 0.0f;
        num5 = -num9;
        num6 = 0.0f;
        num7 = -num10;
        num8 = 0.0f;
        break;
      case UIAnchor.Side.Left:
        num1 = num2 = 0.0f;
        num3 = num4 = 0.5f;
        num5 = -num9;
        num6 = 0.0f;
        num7 = (float) (-(double) num10 / 2.0);
        num8 = num10 / 2f;
        break;
      case UIAnchor.Side.TopLeft:
        num1 = num2 = 0.0f;
        num3 = num4 = 1f;
        num5 = -num9;
        num6 = 0.0f;
        num7 = 0.0f;
        num8 = num10;
        break;
      case UIAnchor.Side.Top:
        num1 = num2 = 0.5f;
        num3 = num4 = 1f;
        num5 = (float) (-(double) num9 / 2.0);
        num6 = num9 / 2f;
        num7 = 0.0f;
        num8 = num10;
        break;
      case UIAnchor.Side.TopRight:
        num1 = num2 = 1f;
        num3 = num4 = 1f;
        num5 = 0.0f;
        num6 = num9;
        num7 = 0.0f;
        num8 = num10;
        break;
      case UIAnchor.Side.Right:
        num3 = 0.5f;
        num4 = 0.5f;
        num5 = 0.0f;
        num6 = num9;
        num7 = (float) (-(double) num10 / 2.0);
        num8 = num10 / 2f;
        num1 = num2 = 1f;
        break;
      case UIAnchor.Side.BottomRight:
        num1 = num2 = 1f;
        num3 = num4 = 0.0f;
        num5 = 0.0f;
        num6 = num10;
        num7 = -num9;
        num8 = 0.0f;
        break;
      case UIAnchor.Side.Bottom:
        num1 = num2 = 0.5f;
        num3 = num4 = 0.0f;
        num5 = (float) (-(double) num9 / 2.0);
        num6 = num9 / 2f;
        num7 = -num10;
        num8 = 0.0f;
        break;
      case UIAnchor.Side.Center:
        double num11;
        num4 = (float) (num11 = 0.5);
        num3 = (float) num11;
        num2 = (float) num11;
        num1 = (float) num11;
        num5 = (float) (-(double) num9 / 2.0);
        num6 = num9 / 2f;
        num7 = (float) (-(double) num10 / 2.0);
        num8 = num10 / 2f;
        break;
    }
    float f1 = num5 + (float) offsetX;
    float f2 = num6 + (float) offsetX;
    float f3 = num8 + (float) offsetY;
    float f4 = num7 + (float) offsetY;
    myRect.leftAnchor.target = targetRect.transform;
    myRect.rightAnchor.target = targetRect.transform;
    myRect.topAnchor.target = targetRect.transform;
    myRect.bottomAnchor.target = targetRect.transform;
    myRect.leftAnchor.relative = num1;
    myRect.rightAnchor.relative = num2;
    myRect.topAnchor.relative = num4;
    myRect.bottomAnchor.relative = num3;
    myRect.leftAnchor.absolute = Mathf.RoundToInt(f1);
    myRect.rightAnchor.absolute = Mathf.RoundToInt(f2);
    myRect.topAnchor.absolute = Mathf.RoundToInt(f3);
    myRect.bottomAnchor.absolute = Mathf.RoundToInt(f4);
  }

  public static void SetTextClamped(UILabel label, string text)
  {
    label.text = text;
    if (!(label.processedText != text))
      return;
    label.text = label.processedText.Substring(0, label.processedText.Length - 3) + "...";
  }

  public static void ApplyHighlightColorTag(ref string inputString)
  {
    string hexRgb = Tools.ColorToHexRGB((Color32) ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_SPECIAL));
    inputString = inputString.Replace("<highlight>", "[" + hexRgb + "]").Replace("</highlight>", "[-]");
  }
}
