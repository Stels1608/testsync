﻿// Decompiled with JetBrains decompiler
// Type: JumpEffectNew
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class JumpEffectNew : ModelScript
{
  private bool playJumpSound = true;
  [SerializeField]
  private JumpEffectNew.JumpEffectSize jumpEffectSize;
  [SerializeField]
  public GameObject DictateModelToWarp;
  [SerializeField]
  public GameObject DictateParticleParent;
  private GameObject jumpInEffectPt;
  private GameObject jumpOutEffectPt;
  private RootScriptBase rootScript;
  public Flag IsJumpEffectSetUp;
  private JumpEffectController runningJumpEffect;

  protected override void Awake()
  {
    this.IsJumpEffectSetUp = new Flag();
    if (this.jumpEffectSize == JumpEffectNew.JumpEffectSize.Undefined || !((UnityEngine.Object) this.DictateModelToWarp != (UnityEngine.Object) null) || !((UnityEngine.Object) this.DictateParticleParent != (UnityEngine.Object) null))
      return;
    this.IsJumpEffectSetUp.Set();
  }

  public void SetupJumpEffect(CutsceneRootScript rootScript, JumpEffectNew.JumpEffectSize jumpEffectSize, bool playJumpSound)
  {
    if ((UnityEngine.Object) rootScript == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "SetupJumpEffect() failed. rootScript is null");
    }
    else
    {
      this.rootScript = (RootScriptBase) rootScript;
      if (!playJumpSound)
        this.MuteSound();
      if (this.jumpEffectSize == JumpEffectNew.JumpEffectSize.Undefined)
        this.jumpEffectSize = jumpEffectSize;
      SignalHandler newHandler = (SignalHandler) (() => this.IsJumpEffectSetUp.Set());
      rootScript.IsAnyModelLoaded.AddHandler(newHandler);
    }
  }

  public void SetupJumpEffect(SpaceObject spaceObject)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    JumpEffectNew.\u003CSetupJumpEffect\u003Ec__AnonStorey116 effectCAnonStorey116 = new JumpEffectNew.\u003CSetupJumpEffect\u003Ec__AnonStorey116();
    // ISSUE: reference to a compiler-generated field
    effectCAnonStorey116.spaceObject = spaceObject;
    // ISSUE: reference to a compiler-generated field
    effectCAnonStorey116.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    this.SpaceObject = effectCAnonStorey116.spaceObject;
    // ISSUE: reference to a compiler-generated field
    this.rootScript = (RootScriptBase) effectCAnonStorey116.spaceObject.RootScript;
    // ISSUE: reference to a compiler-generated method
    SignalHandler newHandler = new SignalHandler(effectCAnonStorey116.\u003C\u003Em__2A1);
    // ISSUE: reference to a compiler-generated field
    effectCAnonStorey116.spaceObject.IsAnyResLoaded.AddHandler(newHandler);
  }

  private void LoadJumpEffect()
  {
    switch (this.jumpEffectSize)
    {
      case JumpEffectNew.JumpEffectSize.Ship6Meter:
        this.jumpInEffectPt = Resources.Load<GameObject>("FX/FTL/JumpInEffect (6m)");
        this.jumpOutEffectPt = Resources.Load<GameObject>("FX/FTL/JumpOutEffect (6m)");
        break;
      case JumpEffectNew.JumpEffectSize.Ship100Meter:
        this.jumpInEffectPt = Resources.Load<GameObject>("FX/FTL/JumpInEffect (100m)");
        this.jumpOutEffectPt = Resources.Load<GameObject>("FX/FTL/JumpOutEffect (100m)");
        break;
      case JumpEffectNew.JumpEffectSize.Ship400Meter:
        this.jumpInEffectPt = Resources.Load<GameObject>("FX/FTL/JumpInEffect (400m)");
        this.jumpOutEffectPt = Resources.Load<GameObject>("FX/FTL/JumpOutEffect (400m)");
        break;
      case JumpEffectNew.JumpEffectSize.Ship750Meter:
        this.jumpInEffectPt = Resources.Load<GameObject>("FX/FTL/JumpInEffect (750m)");
        this.jumpOutEffectPt = Resources.Load<GameObject>("FX/FTL/JumpOutEffect (750m)");
        break;
      case JumpEffectNew.JumpEffectSize.Ship1200Meter:
        this.jumpInEffectPt = Resources.Load<GameObject>("FX/FTL/JumpInEffect (1200m)");
        this.jumpOutEffectPt = Resources.Load<GameObject>("FX/FTL/JumpOutEffect (1200m)");
        break;
      case JumpEffectNew.JumpEffectSize.Ship1400Meter:
        this.jumpInEffectPt = Resources.Load<GameObject>("FX/FTL/JumpInEffect (1400m)");
        this.jumpOutEffectPt = Resources.Load<GameObject>("FX/FTL/JumpOutEffect (1400m)");
        break;
      default:
        Debug.LogError((object) ("No Jump Effect was linked for Jump Effect Size: " + (object) this.jumpEffectSize));
        break;
    }
    if (!((UnityEngine.Object) this.jumpInEffectPt == (UnityEngine.Object) null) && !((UnityEngine.Object) this.jumpOutEffectPt == (UnityEngine.Object) null))
      return;
    Debug.LogError((object) ("JumpEffect is null. Size: " + (object) this.jumpEffectSize + "JumpInEffect null? " + (object) ((UnityEngine.Object) this.jumpInEffectPt == (UnityEngine.Object) null) + " JumpOutEffect null? " + (object) ((UnityEngine.Object) this.jumpOutEffectPt == (UnityEngine.Object) null)));
  }

  private JumpEffectNew.JumpEffectSize DetermineJumpEffectSizeForShipLength(float shipLength)
  {
    JumpEffectNew.JumpEffectSize[] jumpEffectSizeArray = (JumpEffectNew.JumpEffectSize[]) Enum.GetValues(typeof (JumpEffectNew.JumpEffectSize));
    float num1 = float.MaxValue;
    JumpEffectNew.JumpEffectSize jumpEffectSize1 = JumpEffectNew.JumpEffectSize.Undefined;
    foreach (JumpEffectNew.JumpEffectSize jumpEffectSize2 in jumpEffectSizeArray)
    {
      if (jumpEffectSize2 != JumpEffectNew.JumpEffectSize.Undefined)
      {
        float num2 = Mathf.Abs(shipLength - (float) jumpEffectSize2);
        if ((double) num2 < (double) num1)
        {
          num1 = num2;
          jumpEffectSize1 = jumpEffectSize2;
        }
      }
    }
    return jumpEffectSize1;
  }

  public void JumpIn()
  {
    this.InstantiateJumpEffect(JumpType.JumpIn);
  }

  public void JumpOut()
  {
    this.InstantiateJumpEffect(JumpType.JumpOut);
  }

  public bool TryGetRunningJumpEffect(out JumpEffectController jumpEffectController)
  {
    jumpEffectController = this.runningJumpEffect;
    return (UnityEngine.Object) jumpEffectController != (UnityEngine.Object) null;
  }

  private void InstantiateJumpEffect(JumpType jumpType)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.IsJumpEffectSetUp.AddHandler(new SignalHandler(new JumpEffectNew.\u003CInstantiateJumpEffect\u003Ec__AnonStorey117()
    {
      jumpType = jumpType,
      \u003C\u003Ef__this = this
    }.\u003C\u003Em__2A2));
  }

  public void MuteSound()
  {
    this.playJumpSound = false;
  }

  public enum JumpEffectSize
  {
    Undefined = 0,
    Ship6Meter = 6,
    Ship100Meter = 100,
    Ship400Meter = 400,
    Ship750Meter = 750,
    Ship1200Meter = 1200,
    Ship1400Meter = 1400,
  }
}
