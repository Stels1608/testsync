﻿// Decompiled with JetBrains decompiler
// Type: TiledTextureAnimator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TiledTextureAnimator : MonoBehaviour
{
  public int uvAnimationTilesX = 1;
  public int uvAnimationTilesY = 1;
  public int numberOfFrames = -1;
  public float framesPerSecond = 10f;
  public bool oneFrameMapping = true;
  public bool PlayOnAwake = true;
  public bool PlayOnce;
  private bool playing;
  private float playingTime;
  public bool DebugSelfStarter;
  private Renderer _renderer;

  private void Start()
  {
    if (this.numberOfFrames <= 1)
      this.numberOfFrames = this.uvAnimationTilesX * this.uvAnimationTilesY;
    if (this.numberOfFrames > this.uvAnimationTilesX * this.uvAnimationTilesY)
      this.numberOfFrames = this.uvAnimationTilesX * this.uvAnimationTilesY;
    this._renderer = this.GetComponent<Renderer>();
    this.enabled = false;
    if (!this.PlayOnAwake)
      return;
    this.Play();
  }

  private void OnEnable()
  {
    if (!((Object) this._renderer != (Object) null))
      return;
    this._renderer.enabled = true;
  }

  private void OnDisable()
  {
    if (!((Object) this._renderer != (Object) null))
      return;
    this._renderer.enabled = false;
  }

  public void Play()
  {
    this.enabled = true;
    this.playingTime = 0.0f;
    this.playing = true;
  }

  private void Update()
  {
    if (this.DebugSelfStarter)
    {
      this.DebugSelfStarter = false;
      this.Play();
    }
    if (this.playing)
    {
      int num1 = (int) ((double) this.playingTime * (double) this.framesPerSecond);
      this.playingTime += Time.deltaTime;
      if (this.PlayOnce && num1 >= this.numberOfFrames)
        this.playing = false;
      int num2 = num1 % this.numberOfFrames;
      Vector2 scale = new Vector2(1f / (float) this.uvAnimationTilesX, 1f / (float) this.uvAnimationTilesY);
      int num3 = num2 % this.uvAnimationTilesX;
      int num4 = num2 / this.uvAnimationTilesX;
      Vector2 offset = new Vector2((float) num3 * scale.x, (float) (1.0 - (double) num4 * (double) scale.y));
      if (this.oneFrameMapping)
        scale = new Vector2(scale.x * (float) this.uvAnimationTilesX, scale.y * (float) this.uvAnimationTilesY);
      this.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", offset);
      this.GetComponent<Renderer>().material.SetTextureScale("_MainTex", scale);
    }
    else
      this.enabled = false;
  }

  private void Reset()
  {
    this.numberOfFrames = this.uvAnimationTilesX * this.uvAnimationTilesY;
  }
}
