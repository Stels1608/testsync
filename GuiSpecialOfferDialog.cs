﻿// Decompiled with JetBrains decompiler
// Type: GuiSpecialOfferDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GuiSpecialOfferDialog : GuiDialog
{
  private readonly GuiLabel timeRemaining;

  public GuiSpecialOfferDialog()
  {
    GuiDialogPopupManager.ShowDialog((GuiPanel) this, true);
    RewardCard specialOfferCard = Game.SpecialOfferManager.SpecialOfferCard;
    this.m_backgroundImage.Texture = ResourceLoader.Load<Texture2D>("GUI/SpecialOffer/background_specialoffer");
    this.Size = this.m_backgroundImage.Size;
    GuiLabel guiLabel = new GuiLabel(Gui.Options.FontBGM_BT, 15);
    guiLabel.Text = specialOfferCard != null ? specialOfferCard.GUICard.Description : "%$bgo.etc.special_offer_text%";
    guiLabel.WordWrap = true;
    guiLabel.SizeX = 340f;
    guiLabel.Position = new Vector2(60f, 60f);
    this.AddChild((GuiElementBase) guiLabel);
    this.timeRemaining = new GuiLabel(Gui.Options.FontBGM_BT, 40);
    this.timeRemaining.Text = Tools.FormatTime((float) (Game.SpecialOfferManager.SpecialOfferBonusEndTime - Game.TimeSync.ServerTime));
    this.timeRemaining.WordWrap = true;
    this.timeRemaining.SizeX = 340f;
    this.timeRemaining.Position = new Vector2(220f, 320f);
    this.AddChild((GuiElementBase) this.timeRemaining);
    this.Align = Align.MiddleCenter;
    this.CloseButton.Position = new Vector2(-21f, 29f);
    this.OnClose = new AnonymousDelegate(GuiDialogPopupManager.CloseDialog);
    GuiButton guiButton = new GuiButton("%$bgo.etc.special_offer_get_button%");
    guiButton.Size = new Vector2(300f, 30f);
    guiButton.Pressed = new AnonymousDelegate(Game.SpecialOfferManager.ProceedSpecialOffer);
    this.AddChild((GuiElementBase) guiButton, Align.DownCenter, new Vector2(0.0f, -50f));
  }

  [Gui2Editor]
  public static void Show()
  {
    GuiSpecialOfferDialog specialOfferDialog = new GuiSpecialOfferDialog();
  }

  public override void Draw()
  {
    this.timeRemaining.IsRendered = Game.SpecialOfferManager.IsHappyHourActive();
    this.timeRemaining.Text = Tools.FormatTime((float) (Game.SpecialOfferManager.SpecialOfferBonusEndTime - Game.TimeSync.ServerTime));
    base.Draw();
  }
}
