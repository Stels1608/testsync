﻿// Decompiled with JetBrains decompiler
// Type: ObjectStats
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ObjectStats : Properties, IProtocolRead
{
  public float Accuracy
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.Accuracy);
    }
  }

  public float MaxHullPoints
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.MaxHullPoints);
    }
  }

  public float MaxPowerPoints
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.MaxPowerPoints);
    }
  }

  public float HullRecovery
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.HullRecovery);
    }
  }

  public float PowerRecovery
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.PowerRecovery);
    }
  }

  public float Avoidance
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.Avoidance);
    }
  }

  public float FirewallRating
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.FirewallRating);
    }
  }

  public float ArmorValue
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.ArmorValue);
    }
  }

  public float CriticalDefense
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.CriticalDefense);
    }
  }

  public float DamageHigh
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.DamageHigh);
    }
  }

  public float DamageLow
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.DamageLow);
    }
  }

  public float DrainHigh
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.DrainHigh);
    }
  }

  public float DrainLow
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.DrainLow);
    }
  }

  public float DPS
  {
    get
    {
      return (float) (((double) this.DamageHigh + (double) this.DamageLow) / 2.0) / this.Cooldown;
    }
  }

  public float PenetrationStrength
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.PenetrationStrength);
    }
  }

  public float ArmorPiercing
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.ArmorPiercing);
    }
  }

  public float CriticalOffense
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.CriticalOffense);
    }
  }

  public float Acceleration
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.Acceleration);
    }
  }

  public float MaxSpeed
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.Speed);
    }
  }

  public float BoostSpeed
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.BoostSpeed);
    }
  }

  public float BoostCost
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.BoostCost);
    }
  }

  public float TurnAcceleration
  {
    get
    {
      return (float) (((double) this.GetObfuscatedFloat(ObjectStat.PitchAcceleration) + (double) this.GetObfuscatedFloat(ObjectStat.YawAcceleration)) / 2.0);
    }
  }

  public float TurnSpeed
  {
    get
    {
      return (float) (((double) this.GetObfuscatedFloat(ObjectStat.PitchMaxSpeed) + (double) this.GetObfuscatedFloat(ObjectStat.YawMaxSpeed)) / 2.0);
    }
  }

  public float PitchMaxSpeed
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.PitchMaxSpeed);
    }
  }

  public float YawMaxSpeed
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.YawMaxSpeed);
    }
  }

  public float RollMaxSpeed
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.RollMaxSpeed);
    }
  }

  public float StrafeMaxSpeed
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.StrafeMaxSpeed);
    }
  }

  public float PitchAcceleration
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.PitchAcceleration);
    }
  }

  public float YawAcceleration
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.YawAcceleration);
    }
  }

  public float RollAcceleration
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.RollAcceleration);
    }
  }

  public float StrafeAcceleration
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.StrafeAcceleration);
    }
  }

  public float InertiaCompensation
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.InertiaCompensation);
    }
  }

  public float LifeTime
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.LifeTime);
    }
  }

  public float FTLRange
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.FtlRange);
    }
  }

  public float FTLCharge
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.FtlCharge);
    }
  }

  public float FTLCooldown
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.FtlCooldown);
    }
  }

  public float FTLCost
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.FtlCost);
    }
  }

  public float OptimalRange
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.OptimalRange);
    }
  }

  public float MaxRange
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.MaxRange);
    }
  }

  public float MinRange
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.MinRange);
    }
  }

  public float Angle
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.Angle);
    }
  }

  public float Cooldown
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.Cooldown);
    }
  }

  public float DamageMining
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.DamageMining);
    }
  }

  public float PowerPointCost
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.PowerPointCost);
    }
  }

  public float PowerPointCostPerSecond
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.PpCostPerSec);
    }
  }

  public float FlareRange
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.FlareRange);
    }
  }

  public float HullPointRestore
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.HullPointRestore);
    }
  }

  public float PowerPointRestore
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.PowerPointRestore);
    }
  }

  public float DetectionInnerRadius
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.DetectionInnerRadius);
    }
  }

  public float DetectionOuterRadius
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.DetectionOuterRadius);
    }
  }

  public float DetectionVisualRadius
  {
    get
    {
      return this.GetObfuscatedFloat(ObjectStat.DetectionVisualRadius);
    }
  }

  public List<ObjectStat> PropList
  {
    get
    {
      return new List<ObjectStat>((IEnumerable<ObjectStat>) this.props.Keys);
    }
  }

  void IProtocolRead.Read(BgoProtocolReader r)
  {
    int num = r.ReadLength();
    for (int index = 0; index < num; ++index)
      this.SetObfuscatedFloat((ObjectStat) r.ReadUInt16(), r.ReadSingle());
  }

  public List<Tuple<string, float>> ToPrintableList()
  {
    List<Tuple<string, float>> tupleList = new List<Tuple<string, float>>();
    using (Dictionary<ObjectStat, float>.Enumerator enumerator = this.props.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ObjectStat, float> current = enumerator.Current;
        if (current.Key != ObjectStat.DradisRange && current.Key != ObjectStat.MapRange && current.Key != ObjectStat.DetectionDropoffIndex)
        {
          string objStatName = SystemsStatsGenerator.GetObjStatName(current.Key);
          if (objStatName != null)
            tupleList.Add(new Tuple<string, float>(objStatName, this.GetObfuscatedFloat(current.Key)));
          else
            Debug.LogWarning((object) ("Name is NULL for stat " + (object) current.Key));
        }
      }
    }
    return tupleList;
  }
}
