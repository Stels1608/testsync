﻿// Decompiled with JetBrains decompiler
// Type: EffectTarget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class EffectTarget
{
  public string eventType;
  public string effectName;
  public GameObject target;

  public EffectTarget()
  {
    this.eventType = string.Empty;
    this.effectName = string.Empty;
    this.target = (GameObject) null;
  }
}
