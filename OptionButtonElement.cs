﻿// Decompiled with JetBrains decompiler
// Type: OptionButtonElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class OptionButtonElement : OptionsElement
{
  [SerializeField]
  public CheckboxWidget toggleButton;

  public override OnSettingChanged OnSettingChanged
  {
    get
    {
      return this.toggleButton.OnSettingChanged;
    }
    set
    {
      this.toggleButton.OnSettingChanged = value;
    }
  }

  public override bool IsEnabled
  {
    get
    {
      return base.IsEnabled;
    }
    set
    {
      this.toggleButton.IsEnabled = value;
      this.toggleButton.GetComponent<Collider>().enabled = value;
      base.IsEnabled = value;
    }
  }

  public override void Init(UserSetting setting, object value)
  {
    this.optionDescLabel.text = Tools.GetLocalized(setting);
    this.toggleButton.Init(setting, (bool) value);
  }
}
