﻿// Decompiled with JetBrains decompiler
// Type: BridgeGui2ToGUI
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class BridgeGui2ToGUI : GUIPanel
{
  private GuiPanel el;

  public override bool HandleKeyboardInput
  {
    get
    {
      return this.el.HandleKeyboardInput;
    }
  }

  public override bool HandleMouseInput
  {
    get
    {
      return this.el.HandleMouseInput;
    }
  }

  public BridgeGui2ToGUI(GuiPanel el)
  {
    this.el = el;
    this.IsRendered = true;
    this.Rect = el.Rect;
  }

  public T Element<T>() where T : GuiPanel
  {
    return this.el as T;
  }

  public override void Draw()
  {
    this.el.Draw();
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return this.el.OnMouseDown(mousePosition, mouseKey);
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return this.el.OnMouseDown(mousePosition, mouseKey);
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    this.el.OnMouseMove(mousePosition);
  }

  public override bool OnMouseScrollDown()
  {
    return this.el.OnMouseScrollDown();
  }

  public override bool OnMouseScrollUp()
  {
    return this.el.OnMouseScrollUp();
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    return this.el.OnKeyDown(keyboardKey, action);
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    return this.el.OnKeyUp(keyboardKey, action);
  }

  public override void RecalculateAbsCoords()
  {
    this.el.RecalculateAbsCoords();
  }

  public override void Update()
  {
    base.Update();
    this.el.Update();
  }
}
