﻿// Decompiled with JetBrains decompiler
// Type: DelayedEnable
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DelayedEnable : MonoBehaviour
{
  [SerializeField]
  private float delay;
  [SerializeField]
  private GameObject[] objects;

  private void Awake()
  {
    foreach (GameObject @object in this.objects)
      @object.SetActive(false);
  }

  private void Start()
  {
    this.StartCoroutine(this.enableObjects());
  }

  [DebuggerHidden]
  private IEnumerator enableObjects()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DelayedEnable.\u003CenableObjects\u003Ec__Iterator36() { \u003C\u003Ef__this = this };
  }
}
