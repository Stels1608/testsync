﻿// Decompiled with JetBrains decompiler
// Type: ShipControlsForTacticals
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ShipControlsForTacticals : ShipControlsBase
{
  private ShipControlsForTacticals.InputMethod inputMethod;

  protected override bool TrySendNewInputs()
  {
    bool flag1 = false;
    bool flag2 = this.qweasd.IsAnyKeyPressed();
    bool mouseBtnRightHeld = this.MouseBtnRightHeld;
    if (this.MouseBtnRightHeld)
      this.wantedDirection = Camera.main.ScreenPointToRay(Input.mousePosition).direction;
    ShipControlsForTacticals.InputMethod inputMethod = this.inputMethod;
    if (flag2)
      inputMethod = ShipControlsForTacticals.InputMethod.Keyboard;
    else if (mouseBtnRightHeld)
      inputMethod = ShipControlsForTacticals.InputMethod.Mouse;
    else if (this.joystickAxesMovementInputHappened)
      inputMethod = ShipControlsForTacticals.InputMethod.Joystick;
    bool flag3 = this.inputMethod != inputMethod;
    this.inputMethod = inputMethod;
    switch (inputMethod)
    {
      case ShipControlsForTacticals.InputMethod.Keyboard:
        flag1 = this.qweasd.InputChanged;
        break;
      case ShipControlsForTacticals.InputMethod.Mouse:
        flag1 = (double) Vector3.Angle(this.wantedDirection, this.directionInput) > 3.0;
        if (flag1)
        {
          this.directionInput = this.wantedDirection;
          break;
        }
        break;
      case ShipControlsForTacticals.InputMethod.Joystick:
        flag1 = this.joystickAxesMovementInputHappened;
        break;
    }
    bool flag4 = flag3 || flag1;
    if (flag4)
    {
      this.SendControl();
      this.FlushInputs();
    }
    return flag4;
  }

  protected override void SendControl()
  {
    switch (this.inputMethod)
    {
      case ShipControlsForTacticals.InputMethod.Keyboard:
        if (ShipControlsBase.AdvancedFlightControls)
          this.gameProtocol.QWEASD(this.qweasd);
        else
          this.gameProtocol.WASD(this.qweasd);
        this.SendStoryProtocol(StoryProtocol.ControlType.Turn);
        break;
      case ShipControlsForTacticals.InputMethod.Mouse:
        this.Move(this.directionInput);
        break;
      case ShipControlsForTacticals.InputMethod.Joystick:
        this.gameProtocol.TurnByPitchYawStrikes(this.joystickSteeringInputs.JoystickXyzInputsConditioned, Vector2.zero, 0.0f);
        break;
    }
  }

  public override void Move(Vector3 direction)
  {
    if (ShipControlsBase.AdvancedFlightControls)
      this.gameProtocol.MoveToDirectionWithoutRoll(Euler3.Direction(direction));
    else
      this.gameProtocol.MoveToDirection(Euler3.Direction(direction));
  }

  private enum InputMethod
  {
    Keyboard,
    Mouse,
    Joystick,
  }
}
