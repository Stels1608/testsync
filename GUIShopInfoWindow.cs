﻿// Decompiled with JetBrains decompiler
// Type: GUIShopInfoWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;

public class GUIShopInfoWindow : GUIShopTooltipBox
{
  private GUIImageNew itemImage;
  private GUILabelNew titleLabel;
  private GUILabelNew subTitleLabel;
  private GUILabelNew descLabel;
  private GUILabelNew abilLabel;
  private GUILabelNew statsLabel;
  private GUIImageNew separatorImage;
  private AtlasCache atlasCashe;
  private ShipItem item;

  public GUIShopInfoWindow(SmartRect parent)
    : base(parent)
  {
    this.IsUpdated = false;
    JWindowDescription jwindowDescription = Loaders.LoadResource("GUI/EquipBuyPanel/gui_info_layout");
    GUIImageNew guiImageNew = (jwindowDescription["background_image"] as JImage).CreateGUIImageNew(this.root);
    this.root.Width = guiImageNew.SmartRect.Width;
    this.root.Height = guiImageNew.SmartRect.Height;
    this.AddPanel((GUIPanel) guiImageNew);
    this.titleLabel = (jwindowDescription["title_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.titleLabel);
    this.subTitleLabel = (jwindowDescription["subtitle_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.subTitleLabel);
    this.descLabel = (jwindowDescription["desc_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.descLabel);
    this.separatorImage = (jwindowDescription["separator_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.separatorImage);
    this.itemImage = (jwindowDescription["item_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.itemImage);
    this.abilLabel = (jwindowDescription["abil_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.abilLabel);
    this.statsLabel = (jwindowDescription["stats_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.statsLabel);
    this.atlasCashe = new AtlasCache(new float2(40f, 35f));
  }

  public void TryToShow(float2 absPos, ShipItem item)
  {
    this.item = item;
    this.Appear(absPos);
  }

  public override void Hide()
  {
    this.item = (ShipItem) null;
    this.Disappear();
  }

  public override void Update()
  {
    if (this.item != null && (bool) this.item.IsLoaded)
    {
      this.titleLabel.Text = this.item.ItemGUICard.Name;
      this.descLabel.Text = this.item.ItemGUICard.Description;
      ShipSystem shipSystem = this.item as ShipSystem;
      this.subTitleLabel.Text = shipSystem == null ? (!(this.item is ItemCountable) ? "???" : "%$bgo.inflight_shop.count%: " + (object) (this.item as ItemCountable).Count) : "%$bgo.inflight_shop.level% " + (object) shipSystem.Card.Level;
      AtlasEntry cachedEntryBy = this.atlasCashe.GetCachedEntryBy(this.item.ItemGUICard.GUIAtlasTexturePath, (int) this.item.ItemGUICard.FrameIndex);
      this.itemImage.Texture = cachedEntryBy.Texture;
      this.itemImage.InnerRect = cachedEntryBy.FrameRect;
      this.itemImage.IsRendered = true;
      this.statsLabel.Text = this.GetTextStats(this.item);
      this.abilLabel.Text = this.GetTextAbilities(this.item);
    }
    else
    {
      this.titleLabel.Text = "???";
      this.descLabel.Text = "???";
      this.subTitleLabel.Text = "???";
      this.statsLabel.Text = "???";
      this.abilLabel.Text = "???";
      this.itemImage.IsRendered = false;
    }
    this.abilLabel.SmartRect.Height = (float) TextUtility.CalcTextSize(this.abilLabel.Text, this.abilLabel.Font, this.abilLabel.SmartRect.Width).height;
    this.abilLabel.Position = new float2(this.abilLabel.Position.x, (float) ((double) this.itemImage.Position.y + (double) this.itemImage.SmartRect.Height / 2.0 - (double) this.abilLabel.SmartRect.Height / 2.0));
    if ((double) this.abilLabel.SmartRect.Height > (double) this.itemImage.SmartRect.Height)
      this.separatorImage.PlaceAboveOf((GUIPanel) this.abilLabel, 2f);
    else
      this.separatorImage.PlaceAboveOf((GUIPanel) this.itemImage, 2f);
    this.statsLabel.SmartRect.Height = (float) TextUtility.CalcTextSize(this.statsLabel.Text, this.statsLabel.Font, this.statsLabel.SmartRect.Width).height;
    this.statsLabel.PlaceAboveOf((GUIPanel) this.separatorImage, 2f);
    this.titleLabel.SmartRect.Height = (float) TextUtility.CalcTextSize(this.titleLabel.Text, this.titleLabel.Font, this.titleLabel.SmartRect.Width).height;
    this.titleLabel.Position = new float2(this.titleLabel.Position.x, (float) (-(double) this.root.Height / 2.0 + 30.0 + (double) this.titleLabel.SmartRect.Height / 2.0));
    this.subTitleLabel.PlaceBelowOf((GUIPanel) this.titleLabel, 2f);
    this.descLabel.SmartRect.Height = (float) TextUtility.CalcTextSize(this.descLabel.Text, this.descLabel.Font, this.descLabel.SmartRect.Width).height;
    this.descLabel.PlaceBelowOf((GUIPanel) this.subTitleLabel, 2f);
    this.RecalculateAbsCoords();
    base.Update();
  }

  protected string GetTextStats(ShipItem item)
  {
    return string.Empty;
  }

  protected string GetTextAbilities(ShipItem item)
  {
    ShipSystem system = item as ShipSystem;
    if (system != null && (bool) system.IsLoaded)
      return SystemsStatsGenerator.GenerateSimpleDescriptions(system);
    return string.Empty;
  }
}
