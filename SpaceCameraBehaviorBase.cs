﻿// Decompiled with JetBrains decompiler
// Type: SpaceCameraBehaviorBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class SpaceCameraBehaviorBase : ISpaceCameraBehavior, InputListener
{
  protected float Fov = 60f;

  public Vector3 CamPosition
  {
    get
    {
      return Camera.main.transform.position;
    }
  }

  public Quaternion CamRotation
  {
    get
    {
      return Camera.main.transform.rotation;
    }
  }

  public PlayerShip PlayerShip
  {
    get
    {
      return SpaceLevel.GetLevel().GetPlayerShip();
    }
  }

  public abstract bool BlocksControls { get; }

  public abstract bool ShowStrikesHud { get; }

  public virtual KeyCode SelectionMouseButton
  {
    get
    {
      return KeyCode.Mouse0;
    }
  }

  public virtual bool HandleMouseInput
  {
    get
    {
      return true;
    }
  }

  public virtual bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public virtual bool HandleJoystickAxesInput
  {
    get
    {
      return true;
    }
  }

  public virtual void LateUpdate()
  {
  }

  public abstract SpaceCameraParameters CalculateCurrentCameraParameters();

  protected float ExactMovementUpdateDeltaTime()
  {
    return (float) ((IPlayerMovementController) this.PlayerShip.MovementController).MovementUpdateDeltaTime;
  }

  public virtual bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public virtual bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public virtual void OnMouseMove(float2 mousePosition)
  {
  }

  public virtual bool OnMouseScrollDown()
  {
    return false;
  }

  public virtual bool OnMouseScrollUp()
  {
    return false;
  }

  public virtual bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    return false;
  }

  public virtual bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    return false;
  }

  public virtual bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    return false;
  }
}
