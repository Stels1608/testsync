﻿// Decompiled with JetBrains decompiler
// Type: GuiHudStrikesMouseMovementArrows
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class GuiHudStrikesMouseMovementArrows
{
  private const int MAX_AMOUNT_OF_MOVEMENT_INDICATORS = 10;
  private readonly Texture2D indicatorTex;
  private bool active;

  private Vector3 ScreenCenter
  {
    get
    {
      return new Vector3((float) Screen.width / 2f, (float) Screen.height / 2f);
    }
  }

  public GuiHudStrikesMouseMovementArrows()
  {
    this.indicatorTex = (Texture2D) ResourceLoader.Load("GUI/StrikeControlHudElements/MouseMovementIndicator");
    this.indicatorTex = GuiUtils.TintTexture(this.indicatorTex, Gui.Options.StrikeHudElementsColor);
  }

  public void Activate()
  {
    this.active = true;
  }

  public void Deactivate()
  {
    this.active = false;
  }

  public void Draw()
  {
    if (!this.active)
      return;
    this.DrawMovementIndicators();
  }

  private void DrawMovementIndicators()
  {
    Vector2 vector2 = new Vector2(-1f, 0.0f);
    Vector2 a = (Vector2) (Input.mousePosition - this.ScreenCenter);
    bool flag = this.IsVectorLeftOfOther(a, vector2);
    float num1 = Vector2.Angle(vector2, a.normalized);
    float angle = !flag ? num1 : 360f - num1;
    float magnitude = a.magnitude;
    float num2 = (float) (30.0 * (double) Screen.height / 768.0);
    float num3 = (float) (1.0 * (double) Screen.height / 768.0);
    int num4 = Math.Min(10, (int) Mathf.Floor(magnitude / num2));
    GUIUtility.RotateAroundPivot(angle, (Vector2) this.ScreenCenter);
    for (int index = 1; index <= num4; ++index)
    {
      Rect position = new Rect((float) -this.indicatorTex.width / 2f, (float) -this.indicatorTex.height / 2f, (float) this.indicatorTex.width, (float) this.indicatorTex.height);
      float num5 = num3 * 0.05f * (float) index;
      position.x *= num5;
      position.y *= num5;
      position.width *= num5;
      position.height *= num5;
      float num6 = num2 * (float) index;
      position.x += this.ScreenCenter.x - num6;
      position.y += this.ScreenCenter.y;
      GUI.DrawTexture(position, (Texture) this.indicatorTex, ScaleMode.ScaleToFit);
    }
    GUIUtility.RotateAroundPivot(-angle, (Vector2) this.ScreenCenter);
  }

  private bool IsVectorLeftOfOther(Vector2 a, Vector2 b)
  {
    return (double) a.x * -(double) b.y + (double) a.y * (double) b.x > 0.0;
  }
}
