﻿// Decompiled with JetBrains decompiler
// Type: ManeuverRecorder
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public abstract class ManeuverRecorder : MonoBehaviour
{
  public int frameRate = 10;
  protected List<ManeuverRecorder.Frame> frames = new List<ManeuverRecorder.Frame>();
  public int loopShift;
  protected int currentFrame;
  protected int frameCount;
  protected bool loop;

  private void Start()
  {
    Animation component = this.transform.root.GetComponent<Animation>();
    AnimationState animationState = component[component.clip.name];
    Time.captureFramerate = this.frameRate;
    this.frameCount = Mathf.RoundToInt((float) ((double) animationState.length * (double) this.frameRate + 1.0) + (float) this.loopShift);
    this.loop = animationState.wrapMode == WrapMode.Loop;
  }

  private void Update()
  {
    if (this.currentFrame > this.frameCount)
    {
      this.NormalizeRotation();
      this.NormalizeTime();
      this.OnFinished();
      this.enabled = false;
    }
    else
    {
      if (this.currentFrame > this.loopShift)
        this.frames.Add(new ManeuverRecorder.Frame(this.currentFrame, this.transform, Time.time));
      ++this.currentFrame;
    }
  }

  protected abstract void OnFinished();

  protected void NormalizeRotation()
  {
    Quaternion quaternion = Quaternion.Inverse(this.frames[0].rotation);
    for (int index = 0; index < this.frameCount; ++index)
      this.frames[index].rotation = quaternion * this.frames[index].rotation;
  }

  protected void NormalizeTime()
  {
    float num1 = this.frames[this.frameCount - 1].time - this.frames[0].time;
    float num2 = this.frames[0].time;
    using (List<ManeuverRecorder.Frame>.Enumerator enumerator = this.frames.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ManeuverRecorder.Frame current = enumerator.Current;
        current.time = (current.time - num2) / num1;
      }
    }
  }

  public class Frame
  {
    public float time;
    public int index;
    public Quaternion rotation;
    public Vector3 position;

    public Frame(int index, Transform transform, float time)
    {
      this.index = index;
      this.position = transform.position;
      this.rotation = transform.rotation;
      this.time = time;
    }
  }
}
