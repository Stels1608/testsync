﻿// Decompiled with JetBrains decompiler
// Type: OneButtonDialogBar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class OneButtonDialogBar : ButtonBar
{
  private string acceptText = "%$bgo.ui.options.key.accept%";
  [SerializeField]
  protected UISprite background;
  [SerializeField]
  protected GameObject button;

  public override void Init(int width)
  {
    this.background.width = width;
    this.button.transform.localPosition = new Vector3((float) (width / 2), this.button.transform.localPosition.y, this.button.transform.localPosition.z);
  }

  public void InjectDifferentLoca(string accept)
  {
    this.acceptText = accept;
  }

  public void SetButtonDelegates(AnonymousDelegate acceptDelegate)
  {
    ButtonWidget component = this.button.GetComponent<ButtonWidget>();
    if ((Object) component == (Object) null)
      return;
    component.handleClick = acceptDelegate;
    component.handleClick += (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage(Message.HideDialogBox));
    this.button.GetComponent<ButtonWidget>().BgSprite.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
  }

  public override void ReloadLanguageData()
  {
    this.button.GetComponent<ButtonWidget>().TextLabel.text = Tools.ParseMessage(this.acceptText);
  }
}
