﻿// Decompiled with JetBrains decompiler
// Type: iTweenPath
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("iTween/iTweenPath")]
public class iTweenPath : MonoBehaviour
{
  public static Dictionary<string, iTweenPath> paths = new Dictionary<string, iTweenPath>();
  public string pathName = string.Empty;
  public Color pathColor = Color.cyan;
  public List<Vector3> nodes = new List<Vector3>()
  {
    Vector3.zero,
    Vector3.zero
  };
  public string initialName = string.Empty;
  public bool pathVisible = true;
  public int nodeCount;
  public bool initialized;

  private void OnEnable()
  {
    if (iTweenPath.paths.ContainsKey(this.pathName))
      return;
    iTweenPath.paths.Add(this.pathName.ToLower(), this);
  }

  private void OnDisable()
  {
    iTweenPath.paths.Remove(this.pathName.ToLower());
  }

  private void OnDrawGizmosSelected()
  {
    if (!this.pathVisible || this.nodes.Count <= 0)
      return;
    iTween.DrawPath(this.nodes.ToArray(), this.pathColor);
  }

  public static Vector3[] GetPath(string requestedName)
  {
    requestedName = requestedName.ToLower();
    if (iTweenPath.paths.ContainsKey(requestedName))
      return iTweenPath.paths[requestedName].nodes.ToArray();
    Debug.Log((object) ("No path with that name (" + requestedName + ") exists! Are you sure you wrote it correctly?"));
    return (Vector3[]) null;
  }

  public static Vector3[] GetPathReversed(string requestedName)
  {
    requestedName = requestedName.ToLower();
    if (iTweenPath.paths.ContainsKey(requestedName))
    {
      List<Vector3> range = iTweenPath.paths[requestedName].nodes.GetRange(0, iTweenPath.paths[requestedName].nodes.Count);
      range.Reverse();
      return range.ToArray();
    }
    Debug.Log((object) ("No path with that name (" + requestedName + ") exists! Are you sure you wrote it correctly?"));
    return (Vector3[]) null;
  }
}
