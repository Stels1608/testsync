﻿// Decompiled with JetBrains decompiler
// Type: Revolving
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Revolving : SingleLODListener
{
  public bool RandomVelocity = true;
  public bool RandomAxis = true;
  public float Velocity;
  public float MinVelocity;
  public float MaxVelocity;
  public Vector3 Axis;
  private Renderer _renderer;

  protected override void Awake()
  {
    base.Awake();
    if (this.RandomAxis)
      this.Axis = Random.onUnitSphere;
    if (this.RandomVelocity)
      this.Velocity = Random.Range(this.MinVelocity, this.MaxVelocity);
    this._renderer = this.GetComponent<Renderer>();
  }

  protected virtual void Update()
  {
    if (!((Object) this._renderer != (Object) null) || !this._renderer.isVisible)
      return;
    this.Rotate();
  }

  protected void Rotate()
  {
    this.transform.Rotate(this.Axis, this.Velocity * Time.deltaTime, Space.Self);
  }

  protected override void OnSwitched(bool value)
  {
    this.enabled = value;
  }

  private void OnRendererNulled()
  {
    this._renderer = (Renderer) null;
  }

  private void OnRendererChanged(Renderer[] newRenderers)
  {
    this._renderer = newRenderers[0];
  }
}
