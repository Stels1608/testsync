﻿// Decompiled with JetBrains decompiler
// Type: AccordeonSidebarRightUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AccordeonSidebarRightUgui : MonoBehaviour, IGUIRenderable
{
  private static AccordeonSidebarRightUgui _instance;
  [SerializeField]
  public GameObject SpecialOfferButton;
  [SerializeField]
  public GameObject AssignmentListRoot;
  [SerializeField]
  public GameObject SectorEventsRoot;

  public static AccordeonSidebarRightUgui Instance
  {
    get
    {
      if ((Object) AccordeonSidebarRightUgui._instance == (Object) null)
        AccordeonSidebarRightUgui._instance = UguiTools.CreateChild<AccordeonSidebarRightUgui>("AccordeonSidebarRight/AccordeonSidebarRightUgui", UguiTools.GetCanvas(BsgoCanvas.Hud).transform);
      return AccordeonSidebarRightUgui._instance;
    }
  }

  public bool IsRendered
  {
    get
    {
      return this.gameObject.activeInHierarchy;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  public bool IsBigWindow
  {
    get
    {
      return false;
    }
  }
}
