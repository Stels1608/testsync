﻿// Decompiled with JetBrains decompiler
// Type: JumpEffectData
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class JumpEffectData
{
  public JumpType JumpType { get; private set; }

  public AnonymousDelegate OnJumpCompleted { get; private set; }

  public JumpEffectData(JumpType jumpType, AnonymousDelegate onJumpCompleted = null)
  {
    this.JumpType = jumpType;
    this.OnJumpCompleted = onJumpCompleted;
  }
}
