﻿// Decompiled with JetBrains decompiler
// Type: AvatarCatalogueCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AvatarCatalogueCard : Card
{
  public List<AvatarIndex> AvatarIndexes;

  public AvatarCatalogueCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.AvatarIndexes = new List<AvatarIndex>();
    ushort num = r.ReadUInt16();
    for (int index = 0; index < (int) num; ++index)
      this.AvatarIndexes.Add(r.ReadDesc<AvatarIndex>());
  }
}
