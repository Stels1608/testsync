﻿// Decompiled with JetBrains decompiler
// Type: SectorFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class SectorFactory
{
  public static SpaceObject CreateSpaceObject(uint serverID)
  {
    SpaceEntityType spaceEntityType = (SpaceEntityType) ((int) serverID & 520093696);
    switch (spaceEntityType)
    {
      case SpaceEntityType.Player:
        return (SpaceObject) new PlayerShip(serverID);
      case SpaceEntityType.Missile:
        return (SpaceObject) new Missile(serverID);
      case SpaceEntityType.WeaponPlatform:
        return (SpaceObject) new WeaponPlatform(serverID);
      case SpaceEntityType.Cruiser:
        return (SpaceObject) new CruiserShip(serverID);
      case SpaceEntityType.BotFighter:
        return (SpaceObject) new FighterShip(serverID);
      case SpaceEntityType.Debris:
        return (SpaceObject) new DebrisPile(serverID);
      case SpaceEntityType.Asteroid:
        return (SpaceObject) new Asteroid(serverID);
      case SpaceEntityType.Container:
        return (SpaceObject) new Loot(serverID);
      case SpaceEntityType.MiningShip:
        return (SpaceObject) new MiningShip(serverID);
      case SpaceEntityType.Outpost:
        return (SpaceObject) new OutpostShip(serverID);
      case SpaceEntityType.AsteroidBot:
        return (SpaceObject) new AsteroidShip(serverID);
      case SpaceEntityType.Trigger:
        return (SpaceObject) new BsgoTrigger(serverID);
      case SpaceEntityType.Planet:
        return (SpaceObject) new Planet(serverID);
      case SpaceEntityType.Planetoid:
        return (SpaceObject) new Planetoid(serverID);
      case SpaceEntityType.Mine:
      case SpaceEntityType.MineField:
      case SpaceEntityType.SmartMine:
        return (SpaceObject) new Mine(serverID);
      case SpaceEntityType.DotArea:
        return (SpaceObject) new DotAreaObject(serverID);
      case SpaceEntityType.JumpBeacon:
        return (SpaceObject) new JumpBeacon(serverID);
      case SpaceEntityType.SectorEvent:
        return (SpaceObject) new SectorEvent(serverID);
      case SpaceEntityType.JumpTargetTransponder:
        return (SpaceObject) new JumpTargetTransponder(serverID);
      case SpaceEntityType.Comet:
        return (SpaceObject) new Comet(serverID);
      default:
        throw new Exception("Unknown Sector Entity Type: " + (object) spaceEntityType);
    }
  }
}
