﻿// Decompiled with JetBrains decompiler
// Type: Map
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public abstract class Map : GUIPanel
{
  protected MapAlignmentFilter filter = MapAlignmentFilter.All;
  protected List<Map.MapObject> ships = new List<Map.MapObject>();
  protected List<Map.MapObject> asteroids = new List<Map.MapObject>();
  private Color colonialColor = ColorUtility.MakeColorFrom0To255(122U, 163U, 238U);
  private Color cylonColor = ColorUtility.MakeColorFrom0To255(201U, 46U, 71U);
  protected Texture2D backgroundTexture;
  private Color partyColor;
  private Texture2D targetSelection;
  private Texture2D partySelection;
  protected Rect mapObjectSizeRect;
  protected AtlasCache atlasCache;
  protected Texture2D markerTexture;
  protected Map.Marker marker;

  public Map(AtlasCache atlasCache)
  {
    this.LoadTextures();
    this.targetSelection = ResourceLoader.Load<Texture2D>("GUI/Map/selection_target");
    this.partySelection = ResourceLoader.Load<Texture2D>("GUI/Map/selection_party");
    this.colonialColor.a = 1f;
    this.cylonColor.a = 1f;
    this.partyColor = Game.Me.Faction != Faction.Colonial ? this.cylonColor : this.colonialColor;
    this.root.Width = (float) this.backgroundTexture.width;
    this.root.Height = (float) this.backgroundTexture.height;
    this.mapObjectSizeRect = new Rect(0.0f, 0.0f, 25f, 25f);
    this.atlasCache = atlasCache;
    this.marker = new Map.Marker(this.root, this.markerTexture);
  }

  public void SetObjectFilter(MapAlignmentFilter newValue)
  {
    this.filter = newValue;
  }

  public MapAlignmentFilter GetObjectFilter()
  {
    return this.filter;
  }

  public void SetObjectFilterValue(MapAlignmentFilter entry, bool newValue)
  {
    if (newValue)
      this.filter = this.filter | entry;
    else
      this.filter = this.filter & ~entry;
  }

  public bool GetObjectFilterValue(MapAlignmentFilter entry)
  {
    return (this.filter & entry) > MapAlignmentFilter.None;
  }

  protected float CalcAngle0to360CW(Vector3 vector1, Vector3 vector2)
  {
    float num = Vector3.Angle(vector1, vector2);
    if ((double) Vector3.Cross(vector1, vector2).y > 0.0)
      return num;
    return 360f - num;
  }

  public override void Draw()
  {
    base.Draw();
    GUI.DrawTexture(this.root.AbsRect, (Texture) this.backgroundTexture);
    if (Event.current.type == UnityEngine.EventType.Repaint)
    {
      using (List<Map.MapObject>.Enumerator enumerator = this.asteroids.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Map.MapObject current = enumerator.Current;
          Graphics.DrawTexture(current.rect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
          if (current.isTarget)
            GUI.DrawTexture(current.rect.AbsRect, (Texture) this.targetSelection);
        }
      }
      using (List<Map.MapObject>.Enumerator enumerator = this.ships.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Map.MapObject current = enumerator.Current;
          Graphics.DrawTexture(current.rect.AbsRect, (Texture) current.atlasEntry.Texture, current.atlasEntry.FrameRect, 0, 0, 0, 0);
          if (current.isPartyMember && current.obj.PlayerRelation == Relation.Friend)
          {
            Color color = GUI.color;
            GUI.color = this.partyColor;
            GUI.DrawTexture(current.rect.AbsRect, (Texture) this.partySelection);
            GUI.color = color;
          }
          if (current.isTarget)
          {
            Color color1 = current.obj.Faction != Faction.Colonial ? (!current.obj.IsHostileCongener ? this.cylonColor : this.colonialColor) : (!current.obj.IsHostileCongener ? this.colonialColor : this.cylonColor);
            Color color2 = GUI.color;
            GUI.color = color1;
            GUI.DrawTexture(current.rect.AbsRect, (Texture) this.targetSelection);
            GUI.color = color2;
          }
        }
      }
    }
    this.marker.Draw();
  }

  protected abstract void LoadTextures();

  protected abstract float2 CalcScreenPosition(float screenWidth, float screenHeight);

  public override void RecalculateAbsCoords()
  {
    this.root.Position = this.CalcScreenPosition((float) Screen.width, (float) Screen.height);
    base.RecalculateAbsCoords();
    using (List<Map.MapObject>.Enumerator enumerator = this.ships.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.rect.RecalculateAbsCoords();
    }
    using (List<Map.MapObject>.Enumerator enumerator = this.asteroids.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.rect.RecalculateAbsCoords();
    }
    this.marker.rect.RecalculateAbsCoords();
  }

  public override bool Contains(float2 point)
  {
    return this.root.AbsRect.Contains(point.ToV2());
  }

  public void RemoveOldObjects()
  {
    this.ships.Clear();
    this.asteroids.Clear();
  }

  public enum ObjectType : uint
  {
    FighterFriendly = 0,
    FighterHostile = 1,
    FighterNeutral = 2,
    FrigateFriendly = 3,
    FrigateHostile = 4,
    FrigateNeutral = 5,
    CruiserFriendly = 6,
    CruiserHostile = 7,
    CruiserNeutral = 8,
    Asteroid = 9,
    Debris = 10,
    Trigger = 11,
    MiningShip = 12,
    Planetoid = 17,
    JumpBeacon = 23,
  }

  public struct Object3D
  {
    public Map.ObjectType type;
    public Vector3 position;
    public SpaceObject obj;

    public Object3D(Map.ObjectType type, Vector3 position, SpaceObject obj)
    {
      this.type = type;
      this.position = position;
      this.obj = obj;
    }
  }

  protected class MapObject
  {
    public SmartRect rect;
    public Map.ObjectType type;
    public AtlasEntry atlasEntry;
    public SpaceObject obj;
    public bool isTarget;
    public bool isPartyMember;

    public MapObject(Rect initialRect, float2 centerPosition, SmartRect parent, AtlasEntry atlasEntry, SpaceObject obj, bool isTarget, bool isPartyMember)
    {
      this.rect = new SmartRect(initialRect, centerPosition, parent);
      this.atlasEntry = atlasEntry;
      this.obj = obj;
      this.isTarget = isTarget;
      this.isPartyMember = isPartyMember;
    }
  }

  protected class Marker
  {
    public SmartRect rect;
    private Texture2D marker;
    private float orientation;

    public Marker(SmartRect parent, Texture2D texture)
    {
      this.rect = new SmartRect(new Rect(), float2.zero, parent);
      this.marker = texture;
      this.rect = new SmartRect(new Rect(0.0f, 0.0f, (float) this.marker.width, (float) this.marker.height), float2.zero, parent);
    }

    public void Draw()
    {
      GUIUtility.RotateAroundPivot(this.orientation * 360f, this.rect.AbsPosition.ToV2());
      GUI.DrawTexture(this.rect.AbsRect, (Texture) this.marker);
      GUI.matrix = Matrix4x4.identity;
    }

    public void SetOrientation(float orientation)
    {
      this.orientation = orientation;
    }

    public void RecalculateAbsCoords()
    {
      this.rect.RecalculateAbsCoords();
    }
  }
}
