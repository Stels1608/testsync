﻿// Decompiled with JetBrains decompiler
// Type: RoomStateNewDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Arena;
using System;
using UnityEngine;

public class RoomStateNewDialog : RoomState
{
  private RoomLevel roomLevel;
  private DialogCharacterInfo charaterInfo;
  public RoomStateNewDialog.dHandler HandlerEndDialog;

  public RoomStateNewDialog(RoomLevel level, GameObject npcObject)
  {
    this.roomLevel = level;
    CameraDetector component = Camera.main.GetComponent<CameraDetector>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.IsActive = false;
    this.charaterInfo = npcObject.GetComponent<DialogCharacterInfo>();
    if ((UnityEngine.Object) this.charaterInfo == (UnityEngine.Object) null)
      throw new Exception("Character have no DialogCharacterInfo");
    this.charaterInfo.ChangeCameraBox();
    Game.GUIManager.Find<GUIChatNew>().IsRendered = false;
    Game.Ticker.IsRendered = false;
    FacadeFactory.GetInstance().SendMessage(Message.GuiToggleHubMenuVisibility, (object) false);
    this.charaterInfo.GetAnimantion().ToTalkMode();
    if (this.charaterInfo.characterName.Equals("Tyrol"))
      npcObject.GetComponent<TyrolEffects>().IsInTalk();
    this.roomLevel.roomBlackFading.StartFading(new RoomBlackFading.dHandler(this.OnFinishStartFading));
  }

  private void OnFinishStartFading()
  {
  }

  protected void OnFinishEndFading()
  {
    Game.GUIManager.Find<GUIChatNew>().IsRendered = true;
    Game.Ticker.IsRendered = true;
    FacadeFactory.GetInstance().SendMessage(Message.GuiToggleHubMenuVisibility, (object) true);
    if (this.HandlerEndDialog == null)
      return;
    this.HandlerEndDialog();
  }

  private void CloseArenaMatch()
  {
    if (Game.Me.Arena.State == ArenaState.None)
      return;
    if (ArenaProtocol.GetProtocol().CloseArenaInvite != null)
    {
      ArenaProtocol.GetProtocol().CloseArenaInvite();
    }
    else
    {
      ArenaProtocol.GetProtocol().RequestArenaInviteCancel();
      Game.Me.Arena.State = ArenaState.None;
      ArenaManager.Changed();
    }
    ArenaCancelWindow.Show();
  }

  public void OnAction(DialogAction action)
  {
    Debug.Log((object) ("ROOM STATE DIALOG " + (object) action));
    this.CloseArenaMatch();
    FacadeFactory.GetInstance().SendMessage(Message.DialogToggleVisibility, (object) false);
    switch (action)
    {
      case DialogAction.OpenShop:
        this.roomLevel.shopWindow.IsRendered = true;
        this.roomLevel.shopWindow.HandlerClose = new ShopWindow.Handlr(this.OnCloseShop);
        break;
      case DialogAction.OpenRepair:
        this.roomLevel.repairGui.IsRendered = true;
        this.roomLevel.repairGui.HandlerClose = new AnonymousDelegate(this.OnCloseRepairWindow);
        break;
      case DialogAction.BuyShip:
        this.roomLevel.shipShop.IsRendered = true;
        this.roomLevel.shipShop.OnCloseOuter = new AnonymousDelegate(this.OnCloseShipShop);
        break;
      case DialogAction.CustomizeShip:
        this.roomLevel.shipCustomizationWindow.IsRendered = true;
        Game.InputDispatcher.Focused = (InputListener) this.roomLevel.shipCustomizationWindow;
        this.roomLevel.shipCustomizationWindow.HandlerClose = new ShipCustomizationWindow.dHandlerClose(this.OnCloseShipCustomization);
        break;
      case DialogAction.SelectShip:
        this.roomLevel.hangarWindow.IsRendered = true;
        this.roomLevel.hangarWindow.DeselectAllShipVariants();
        this.roomLevel.hangarWindow.HandlerClose = new AnonymousDelegate(this.OnCloseHangarWindow);
        break;
      case DialogAction.PlayWoF:
        FacadeFactory.GetInstance().SendMessage(Message.ShowWofWindowWithDialogHandling);
        break;
      case DialogAction.OpenFriendInvite:
        this.roomLevel.characterStatusWindow.OpenFriendInvite();
        this.roomLevel.characterStatusWindow.HandlerClose = new System.Action(this.ContinueDialog);
        this.roomLevel.characterStatusWindow.HandlerClose = new System.Action(this.OnCloseCharacterStatus);
        break;
    }
  }

  protected void ContinueDialog()
  {
    FacadeFactory.GetInstance().SendMessage(Message.DialogToggleVisibility, (object) true);
    FacadeFactory.GetInstance().SendMessage(Message.DialogAdvance);
  }

  public void OnStopped()
  {
    CameraDetector component = Camera.main.GetComponent<CameraDetector>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.IsActive = true;
    this.charaterInfo.ChangeCameraBoxToInit();
    this.charaterInfo.GetAnimantion().ToIdleMode();
    if (this.charaterInfo.characterName.Equals("Tyrol"))
      this.charaterInfo.gameObject.GetComponent<TyrolEffects>().DelayIsNotInTalk();
    this.roomLevel.roomBlackFading.StartFading(new RoomBlackFading.dHandler(this.OnFinishEndFading));
    this.OnFinishEndFading();
  }

  protected void OnCloseShop()
  {
    this.roomLevel.shopWindow.IsRendered = false;
    this.roomLevel.shopWindow.HandlerClose = (ShopWindow.Handlr) null;
    this.ContinueDialog();
  }

  public void OnCloseHangarWindow()
  {
    this.roomLevel.hangarWindow.IsRendered = false;
    this.roomLevel.hangarWindow.HandlerClose = (AnonymousDelegate) null;
    this.roomLevel.hangarWindow.CloseVariantWindow();
    this.ContinueDialog();
  }

  protected void OnCloseRepairWindow()
  {
    this.roomLevel.repairGui.IsRendered = false;
    this.roomLevel.repairGui.HandlerClose = (AnonymousDelegate) (() => this.roomLevel.repairGui.IsRendered = false);
    this.ContinueDialog();
  }

  protected void OnCloseShipShop()
  {
    this.roomLevel.shipShop.IsRendered = false;
    this.roomLevel.shipShop.OnCloseOuter = (AnonymousDelegate) null;
    this.ContinueDialog();
  }

  protected void OnCloseShipCustomization()
  {
    this.roomLevel.shipCustomizationWindow.IsRendered = false;
    this.roomLevel.shipCustomizationWindow.HandlerClose = (ShipCustomizationWindow.dHandlerClose) null;
    this.ContinueDialog();
  }

  protected void OnCloseCharacterStatus()
  {
    this.roomLevel.characterStatusWindow.IsRendered = false;
    this.roomLevel.characterStatusWindow.HandlerClose = (System.Action) null;
    this.ContinueDialog();
  }

  public delegate void dHandler();
}
