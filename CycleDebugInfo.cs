﻿// Decompiled with JetBrains decompiler
// Type: CycleDebugInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

[Serializable]
public class CycleDebugInfo
{
  public float toeLiftTime;
  public float toeLandTime;
  public float ankleLiftTime;
  public float ankleLandTime;
  public float footLiftTime;
  public float footLandTime;
}
