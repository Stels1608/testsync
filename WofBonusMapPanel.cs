﻿// Decompiled with JetBrains decompiler
// Type: WofBonusMapPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class WofBonusMapPanel : MonoBehaviour, ILocalizeable
{
  private List<WofBonusMapLayout> bonusMapList = new List<WofBonusMapLayout>();
  private Dictionary<int, List<int>> mapParts = new Dictionary<int, List<int>>();
  [SerializeField]
  private ButtonWidget prevBtn;
  [SerializeField]
  private ButtonWidget nextBtn;
  [SerializeField]
  private ButtonWidget startBonusMapBtn;
  [SerializeField]
  private ButtonWidget helpButton;
  [SerializeField]
  private UILabel mapNameLabel;
  [SerializeField]
  private UILabel partTextLabel;
  [SerializeField]
  private UILabel partNumberLabel;
  [SerializeField]
  private GameObject LevelReqObject;
  [SerializeField]
  private UILabel reqLevelLabel;
  [SerializeField]
  private UILabel reqLevelText;
  [SerializeField]
  private UILabel reqLevelNumber;
  [SerializeField]
  private UILabel serverShutdownLabel;
  [SerializeField]
  private UISprite reqLevelNumberBG;
  [SerializeField]
  private UILabel ftlMissionHeadline;
  [SerializeField]
  private WofBonusMapPlateManager plateManager;
  public byte playerLevel;
  private int actualMapIndex;
  private bool ftlMissionBlocked;
  private bool activeGame;

  private void Awake()
  {
    this.prevBtn.handleClick = new AnonymousDelegate(this.PrevMap);
    this.nextBtn.handleClick = new AnonymousDelegate(this.NextMap);
    this.helpButton.handleClick = new AnonymousDelegate(this.ShowHelp);
    this.startBonusMapBtn.handleClick = (AnonymousDelegate) (() => DialogBoxFactoryNgui.CreateSimpleDialogBox(Tools.ParseMessage("%$bgo.wof.start_text%", (object) this.bonusMapList[this.actualMapIndex].MapName), new AnonymousDelegate(this.RequestStartMap), (AnonymousDelegate) null, this.bonusMapList[this.actualMapIndex].MapName, (string) null, (string) null));
  }

  public void Start()
  {
    this.ReloadLanguageData();
  }

  public void BlockedByActiveGame(bool blocked)
  {
    if (blocked == this.activeGame)
      return;
    this.activeGame = blocked;
    this.ShowMap();
  }

  private void ShowMap()
  {
    if (this.bonusMapList.Count == 0)
      return;
    WofBonusMapLayout lay = this.bonusMapList[this.actualMapIndex];
    string message = Tools.ParseMessage("%$bgo.wof.difficulty_" + lay.Difficulty.ToString().ToLower() + "%");
    this.mapNameLabel.text = lay.MapName + " (" + message + ")";
    this.LevelReqObject.SetActive(lay.RequiredLevel > (int) this.playerLevel || this.ftlMissionBlocked);
    if (this.LevelReqObject.activeSelf)
    {
      if (lay.RequiredLevel > (int) this.playerLevel)
      {
        this.reqLevelLabel.gameObject.SetActive(true);
        this.reqLevelText.gameObject.SetActive(true);
        this.reqLevelNumber.gameObject.SetActive(true);
        this.reqLevelNumber.text = lay.RequiredLevel.ToString();
        this.reqLevelNumberBG.gameObject.SetActive(true);
        this.serverShutdownLabel.gameObject.SetActive(false);
      }
      if (this.ftlMissionBlocked)
      {
        this.reqLevelLabel.gameObject.SetActive(false);
        this.reqLevelText.gameObject.SetActive(false);
        this.reqLevelNumber.gameObject.SetActive(false);
        this.serverShutdownLabel.gameObject.SetActive(true);
        this.reqLevelNumberBG.gameObject.SetActive(false);
      }
    }
    this.plateManager.SetMapLayout(lay);
    int bonusMapId = lay.BonusMapId;
    int num = 0;
    if (this.mapParts.ContainsKey(bonusMapId))
    {
      this.plateManager.UpdateVisibility(bonusMapId, this.mapParts[bonusMapId]);
      num = this.mapParts[bonusMapId].Count;
    }
    this.startBonusMapBtn.IsEnabled = num == lay.MaxParts && (int) Game.Me.Level >= lay.RequiredLevel && !this.ftlMissionBlocked && !this.activeGame;
    this.partNumberLabel.text = num.ToString() + "/" + (object) lay.MaxParts;
    this.ReloadLanguageData();
  }

  public void SetFtlState(bool blocked)
  {
    this.ftlMissionBlocked = blocked;
  }

  public void UpdateLevel(byte level)
  {
    this.playerLevel = level;
    if (!this.gameObject.activeSelf)
      return;
    this.ShowMap();
  }

  private void RequestStartMap()
  {
    WofProtocol.GetProtocol().RequestMapStart(this.bonusMapList[this.actualMapIndex].BonusMapId);
  }

  private void ShowHelp()
  {
    FacadeFactory.GetInstance().SendMessage(Message.ShowHelpScreen, (object) HelpScreenType.FTLMission);
  }

  private void NextMap()
  {
    if (this.actualMapIndex + 1 == this.bonusMapList.Count)
      this.actualMapIndex = 0;
    else
      ++this.actualMapIndex;
    this.ShowMap();
  }

  private void PrevMap()
  {
    if (this.actualMapIndex - 1 < 0)
      this.actualMapIndex = this.bonusMapList.Count - 1;
    else
      --this.actualMapIndex;
    this.ShowMap();
  }

  public void ReloadLanguageData()
  {
    this.startBonusMapBtn.TextLabel.text = Tools.ParseMessage("%$bgo.wof.play_map%");
    this.partTextLabel.text = Tools.ParseMessage("%$bgo.wof.bonusmap_parts%").ToUpper();
    this.ftlMissionHeadline.text = Tools.ParseMessage("%$bgo.wof.ftl_mission%").ToUpper();
    this.reqLevelLabel.text = Tools.ParseMessage("%$bgo.wof.required_level%");
    this.reqLevelText.text = Tools.ParseMessage("%$bgo.wof.level%");
    this.serverShutdownLabel.text = Tools.ParseMessage("%$bgo.wof.server_blocked%");
  }

  public void UpdateBonusMapParts(Dictionary<int, List<int>> dict)
  {
    this.mapParts = dict;
    this.ShowMap();
  }

  public void SetVisibleBonusLevel(List<int> visibleLevel)
  {
    if (this.bonusMapList.Count > 0)
      return;
    if (visibleLevel.Contains(1))
      this.bonusMapList.Add((WofBonusMapLayout) new WofBonusMapLayoutAlpha());
    if (visibleLevel.Contains(2))
      this.bonusMapList.Add((WofBonusMapLayout) new WofBonusMapLayoutBeta());
    if (visibleLevel.Contains(3))
      this.bonusMapList.Add((WofBonusMapLayout) new WofBonusMapLayoutGamma());
    if (visibleLevel.Contains(4))
      this.bonusMapList.Add((WofBonusMapLayout) new WofBonusMapLayoutDelta());
    this.ShowMap();
  }

  public void DisableFtlMissions()
  {
    this.ftlMissionBlocked = true;
    this.ShowMap();
  }
}
