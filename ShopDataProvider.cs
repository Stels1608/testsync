﻿// Decompiled with JetBrains decompiler
// Type: ShopDataProvider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Model;

public class ShopDataProvider : DataProvider<Message>
{
  public Shop Shop { get; private set; }

  public EventShop EventShop { get; private set; }

  public ShopDataProvider()
    : base("ShopDataProvider")
  {
    this.Shop = new Shop();
    this.EventShop = new EventShop();
  }
}
