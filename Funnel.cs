﻿// Decompiled with JetBrains decompiler
// Type: Funnel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class Funnel
{
  public void ReportActivity(Activity action)
  {
    ExternalApi.EvalJs("window.funnelTrack(\"" + this.ActivityToString(action) + "\");");
  }

  private string ActivityToString(Activity act)
  {
    switch (act)
    {
      case Activity.GameclientStarted:
        return "51500";
      case Activity.GameclientConnectsToServer:
        return "51520";
      case Activity.GameclientConnectedToServer:
        return "51540";
      case Activity.GameclientConnectionFailed:
        return "51560";
      case Activity.GameclientRequestsLocalization:
        return "51580";
      case Activity.GameclientDownloadedLocalization:
        return "51620";
      case Activity.GameclientRequestsLogin:
        return "51640";
      case Activity.LoginSuccessful:
        return "51660";
      case Activity.LoginFailed:
        return "51680";
      case Activity.LoginQueue:
        return "51720";
      case Activity.StartScene:
        return "1101000";
      case Activity.FactionSelected:
        return "1102000";
      case Activity.Logout:
        return "59500";
      case Activity.CharacterCreationStarted:
        return "52180";
      case Activity.CharacterCreationCompleted:
        return "52220";
      default:
        return "undefined";
    }
  }
}
