﻿// Decompiled with JetBrains decompiler
// Type: FactionSelectionNGui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FactionSelectionNGui : FactionSelectionUi
{
  [SerializeField]
  private UILabel windowHeader;
  [SerializeField]
  private UILabel factionTextColonial;
  [SerializeField]
  private UILabel factionTextCylon;
  [SerializeField]
  private UILabel[] factionBonusHeaders;
  [SerializeField]
  private UILabel bonusTextColonial;
  [SerializeField]
  private UILabel bonusTextCylon;
  [SerializeField]
  private UILabel pleaseWaitLabel;

  public override void SetWindowHeaderText(string text)
  {
    this.windowHeader.text = text;
  }

  public override void SetFactionTextColonial(string text)
  {
    this.factionTextColonial.text = text;
  }

  public override void SetFactionTextCylon(string text)
  {
    this.factionTextCylon.text = text;
  }

  public override void SetFactionBonusHeaders(string text)
  {
    foreach (UILabel factionBonusHeader in this.factionBonusHeaders)
      factionBonusHeader.text = text;
  }

  public override void SetBonusTextColonial(string text)
  {
    this.bonusTextColonial.text = text;
  }

  public override void SetBonusTextCylon(string text)
  {
    this.bonusTextCylon.text = text;
  }

  public override void SetPleaseWaitText(string text)
  {
    this.pleaseWaitLabel.text = text;
  }

  public override void ShowPleaseWaitLabel()
  {
    this.pleaseWaitLabel.enabled = true;
  }
}
