﻿// Decompiled with JetBrains decompiler
// Type: TargetPassedMapRangeAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;

public class TargetPassedMapRangeAction : Action<Message>
{
  private const float DESELECT_DELAY = 2.2f;
  private TargetSelectionDataProvider selectionDataProvider;

  public override void Execute(IMessage<Message> message)
  {
    this.selectionDataProvider = (TargetSelectionDataProvider) this.OwnerFacade.FetchDataProvider("TargetSelectionProvider");
    ISpaceEntity target = (ISpaceEntity) message.Data;
    if (target.TargetType != TargetType.SpaceObject)
      return;
    SpaceObject spaceObject = target as SpaceObject;
    if (!spaceObject.IsCloaked || spaceObject.IsInMapRange)
      return;
    this.TryMainTargetDeselection(target);
    this.TryAbilityTargetDeselection(target);
  }

  private void TryAbilityTargetDeselection(ISpaceEntity target)
  {
    Game.Me.ActiveShip.DisableSlotAbilitiesByTarget(target);
  }

  private void TryMainTargetDeselection(ISpaceEntity target)
  {
    if (!this.selectionDataProvider.IsMainTarget(target))
      return;
    TargetSelector.SelectTargetRequest((SpaceObject) null, false);
  }
}
