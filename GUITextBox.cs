﻿// Decompiled with JetBrains decompiler
// Type: GUITextBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUITextBox : GUIPanel
{
  public int maxSymbols = (int) byte.MaxValue;
  public GUILabelNew Hint = new GUILabelNew("<click here to write a message...");
  private static int counter;
  private string name;
  private string text;
  private GUIStyle style;
  private bool isFocused;
  private bool digitsOnly;
  private GUITextBox.Hndler handler;
  public bool RenderHint;

  public GUITextBox.Hndler Handler
  {
    get
    {
      return this.handler;
    }
    set
    {
      this.handler = value;
    }
  }

  public override float Width
  {
    set
    {
      base.Width = value;
      this.AdjustHint();
    }
  }

  public override float2 Size
  {
    set
    {
      base.Size = value;
      this.AdjustHint();
    }
  }

  public bool IsFocused
  {
    get
    {
      return this.isFocused;
    }
    set
    {
      this.isFocused = value;
      this.AdjustHint();
      if (this.IsFocused)
      {
        Game.InputDispatcher.Focused = (InputListener) this;
      }
      else
      {
        if (Game.InputDispatcher.Focused != this)
          return;
        Game.InputDispatcher.Focused = (InputListener) null;
      }
    }
  }

  public string Text
  {
    get
    {
      return this.text;
    }
    set
    {
      this.text = value;
      this.AdjustHint();
    }
  }

  public bool DigitsOnly
  {
    get
    {
      return this.digitsOnly;
    }
    set
    {
      this.digitsOnly = value;
      if (!this.DigitsOnly)
        return;
      this.RemoveNonDigits();
    }
  }

  public int FontSize
  {
    get
    {
      return this.style.fontSize;
    }
    set
    {
      this.style.fontSize = value;
    }
  }

  public override bool HandleKeyboardInput
  {
    get
    {
      return this.IsFocused;
    }
  }

  public GUITextBox()
    : this((SmartRect) null, string.Empty, Gui.Options.FontVerdana, Color.white, (GUIStyle) null)
  {
  }

  public GUITextBox(SmartRect parent, string text, Font font, Color textColor, GUIStyle newStyle = null)
    : base(parent)
  {
    ++GUITextBox.counter;
    this.name = GUITextBox.counter.ToString();
    this.text = text;
    this.style = newStyle ?? new GUIStyle();
    this.style.font = font;
    this.style.normal.textColor = textColor;
    this.Hint.IsRendered = false;
    this.Hint.AutoSize = true;
    this.AddPanel((GUIPanel) this.Hint);
    global::Size size = TextUtility.CalcTextSize(text, font, this.style.fontSize);
    this.Size = new float2((float) size.width, (float) size.height);
    this.IsRendered = true;
  }

  public override void Draw()
  {
    base.Draw();
    if (this.IsFocused)
    {
      GUI.SetNextControlName(this.name);
      GUI.FocusControl(this.name);
      this.Text = GUI.TextArea(this.root.AbsRect, this.Text, this.maxSymbols, this.style);
    }
    else
      GUI.Label(this.root.AbsRect, this.Text, this.style);
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    this.wasMouseDown = false;
    if (!this.HandleMouseInput)
      return false;
    this.IsFocused = false;
    if (mouseKey != KeyCode.Mouse0 || !this.Contains(mousePosition))
      return false;
    this.wasMouseDown = true;
    this.IsFocused = true;
    return true;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (!this.HandleMouseInput)
      return false;
    if (this.wasMouseDown && mouseKey == KeyCode.Mouse0 && this.Contains(mousePosition))
    {
      this.wasMouseDown = false;
      return true;
    }
    this.IsFocused = false;
    return false;
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    bool isFocused = this.IsFocused;
    if (this.Handler != null)
      this.Handler(keyboardKey, action);
    if (this.DigitsOnly)
      this.RemoveNonDigits();
    return isFocused;
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    return this.IsFocused;
  }

  private void RemoveNonDigits()
  {
    string str = string.Empty;
    foreach (char c in this.text)
    {
      if (char.IsDigit(c))
        str += (string) (object) c;
    }
    this.text = str;
  }

  public void AdjustHint()
  {
    this.Hint.PositionX = (float) (-(double) this.Width / 2.0 + (double) this.Hint.Width / 2.0);
    this.Hint.IsRendered = !this.IsFocused && this.Text == string.Empty && this.RenderHint;
  }

  public delegate void Hndler(KeyCode keyCode, Action action);
}
