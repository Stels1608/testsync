﻿// Decompiled with JetBrains decompiler
// Type: ShipCardLight
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;

public class ShipCardLight : Card
{
  public uint ShipObjectKey;
  public byte Tier;
  public ShipRole[] ShipRoles;
  public ShipRoleDeprecated ShipRoleDeprecated;

  public ShipCardLight(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.ShipObjectKey = r.ReadGUID();
    this.Tier = r.ReadByte();
    int length = r.ReadLength();
    this.ShipRoles = new ShipRole[length];
    for (int index = 0; index < length; ++index)
      this.ShipRoles[index] = (ShipRole) r.ReadByte();
    this.ShipRoleDeprecated = (ShipRoleDeprecated) r.ReadByte();
  }

  public bool HasRole(ShipRole role)
  {
    if (this.ShipRoles == null)
      return false;
    return ((IEnumerable<ShipRole>) this.ShipRoles).Contains<ShipRole>(role);
  }
}
