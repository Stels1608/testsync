﻿// Decompiled with JetBrains decompiler
// Type: SoundPlayer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Unity5AssetHandling;
using UnityEngine;

public class SoundPlayer : SingleLODListener
{
  private static readonly Dictionary<string, AssetRequest> requestCache = new Dictionary<string, AssetRequest>();
  public int AudioPoolCapacity = 1;
  private AudioPool audioPool;
  public bool Shuffle;
  public SoundSet SoundSet;
  public string SoundSetResource;

  private void Start()
  {
    this.audioPool = new AudioPool(this.gameObject, this.AudioPoolCapacity);
    if (!((Object) this.SoundSet == (Object) null) || string.IsNullOrEmpty(this.SoundSetResource))
      return;
    GameObject gameObject = Resources.Load(this.SoundSetResource) as GameObject;
    if ((Object) gameObject != (Object) null)
    {
      this.SoundSet = gameObject.GetComponent<SoundSet>();
    }
    else
    {
      string lower = Path.GetFileNameWithoutExtension(this.SoundSetResource).ToLower();
      AssetRequest assetRequest;
      if (SoundPlayer.requestCache.ContainsKey(lower))
      {
        assetRequest = SoundPlayer.requestCache[lower];
      }
      else
      {
        assetRequest = AssetCatalogue.Instance.Request(lower + ".prefab", false);
        SoundPlayer.requestCache.Add(lower, assetRequest);
      }
      this.StartCoroutine(this.WaitForAsset(assetRequest));
    }
  }

  protected override void OnSwitched(bool value)
  {
    this.enabled = value;
  }

  public static void ClearCache()
  {
    SoundPlayer.requestCache.Clear();
  }

  public void Play()
  {
    if (!this.enabled)
      return;
    AudioSource audioSource = (AudioSource) null;
    if (this.audioPool != null)
      audioSource = this.audioPool.GetAudio();
    if (!((Object) audioSource != (Object) null) || !((Object) this.SoundSet != (Object) null))
      return;
    audioSource.volume = this.SoundSet.Volume;
    audioSource.minDistance = this.SoundSet.MinDistance;
    audioSource.maxDistance = audioSource.minDistance * 256f;
    audioSource.loop = false;
    audioSource.clip = this.SoundSet.GetNextClip(this.Shuffle);
    audioSource.Play();
  }

  [DebuggerHidden]
  private IEnumerator WaitForAsset(AssetRequest assetRequest)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SoundPlayer.\u003CWaitForAsset\u003Ec__Iterator33()
    {
      assetRequest = assetRequest,
      \u003C\u0024\u003EassetRequest = assetRequest,
      \u003C\u003Ef__this = this
    };
  }
}
