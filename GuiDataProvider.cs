﻿// Decompiled with JetBrains decompiler
// Type: GuiDataProvider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Model;
using UnityEngine;

public class GuiDataProvider : DataProvider<Message>
{
  private NguiMvcRoot mvcRoot;
  public GameObject uiHook;
  public GameObject guiRootGameObject;

  public NguiMvcRoot MvcRoot
  {
    get
    {
      return this.mvcRoot;
    }
    set
    {
      this.mvcRoot = value;
    }
  }

  public GuiDataProvider()
    : base("GuiDataProvider")
  {
  }

  public void AddWindow(WindowWidget window)
  {
    window.MvcRoot = this.MvcRoot;
    window.transform.parent = this.uiHook.transform;
    window.transform.localPosition = Vector3.zero;
    window.transform.localScale = Vector3.one;
    window.gameObject.SetActive(false);
  }
}
