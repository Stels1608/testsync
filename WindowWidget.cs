﻿// Decompiled with JetBrains decompiler
// Type: WindowWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WindowWidget : NguiWidget, IGUIRenderable, ILocalizeable
{
  [SerializeField]
  private Vector2 windowSize = new Vector2(400f, 350f);
  [SerializeField]
  private bool resizable = true;
  [SerializeField]
  private bool openOnStart;
  [SerializeField]
  private UISprite backgroundSprite;

  public Vector2 WindowSize
  {
    get
    {
      return this.windowSize;
    }
    set
    {
      this.windowSize = value;
    }
  }

  public Vector2 PivotOffset { get; set; }

  public bool IsResizable
  {
    get
    {
      return this.resizable;
    }
    set
    {
      this.resizable = value;
    }
  }

  public bool OpenOnStart
  {
    get
    {
      return this.openOnStart;
    }
    set
    {
      this.openOnStart = value;
    }
  }

  public UISprite BackgroundSprite
  {
    get
    {
      return this.backgroundSprite;
    }
    set
    {
      this.backgroundSprite = value;
    }
  }

  public bool IsOpen { get; protected set; }

  protected NguiWindowManager WindowManager
  {
    get
    {
      if ((Object) null != (Object) this.MvcRoot)
        return this.MvcRoot.WindowManager;
      return (NguiWindowManager) null;
    }
  }

  public virtual bool IsRendered
  {
    get
    {
      return this.IsOpen;
    }
    set
    {
      this.gameObject.SetActive(value);
      if (value)
        this.Open();
      else
        this.Close();
    }
  }

  public virtual bool IsBigWindow
  {
    get
    {
      return false;
    }
  }

  public WindowWidget()
  {
    this.IsOpen = false;
  }

  public override void Start()
  {
    base.Start();
    this.ReloadLanguageData();
    if (!this.OpenOnStart)
      return;
    this.Open();
  }

  public virtual void Open()
  {
    if (this.IsOpen)
      return;
    this.IsOpen = true;
    this.OnWindowOpen();
  }

  public virtual void Close()
  {
    if (!this.IsOpen)
      return;
    this.IsOpen = false;
    this.OnWindowClose();
  }

  public bool HasWindowSizeChanged()
  {
    return (double) this.windowSize.x != (double) this.backgroundSprite.transform.localScale.x || (double) this.windowSize.y != (double) this.backgroundSprite.transform.localScale.y;
  }

  public virtual void SetSize(Vector2 dimensions)
  {
    if (!this.IsResizable)
      return;
    this.SetSize(dimensions.x, dimensions.y);
  }

  public virtual void SetSize(float width, float height)
  {
    if (!this.IsResizable)
      return;
    this.windowSize.x = width;
    this.windowSize.y = height;
  }

  public virtual void OnWindowOpen()
  {
  }

  public virtual void OnWindowClose()
  {
  }

  public virtual void ReloadLanguageData()
  {
  }
}
