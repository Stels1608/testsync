﻿// Decompiled with JetBrains decompiler
// Type: BackgroundDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BackgroundDesc : Desc, IProtocolRead
{
  [JsonField(JsonName = "prefabName")]
  public string prefabName = string.Empty;
  public Color color = Color.white;
  public const int VERSIONS_COUNT = 4;
  public const int BG_LAYER = 8;

  public BackgroundDesc()
    : base("Background")
  {
  }

  public BackgroundDesc(string modelName)
    : this()
  {
    this.prefabName = modelName;
  }

  public void Read(BgoProtocolReader pr)
  {
    this.prefabName = pr.ReadString();
    this.rotation = pr.ReadQuaternion();
    this.color = pr.ReadColor();
    this.position = pr.ReadVector3();
  }
}
