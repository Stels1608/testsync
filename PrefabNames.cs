﻿// Decompiled with JetBrains decompiler
// Type: PrefabNames
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public static class PrefabNames
{
  public static string GetShipPrefabName(ShipType shipType)
  {
    if (shipType != ShipType.Undefined)
      return EnumHelper.GetDescription((Enum) shipType);
    Debug.LogError((object) "GetShipPrefabName(): Undefined ship type passed.");
    return string.Empty;
  }

  public static string GetDecorationPrefabName(DecorationType decoType)
  {
    if (decoType != DecorationType.Undefined)
      return EnumHelper.GetDescription((Enum) decoType);
    Debug.LogError((object) "GetDecorationPrefabName(): Undefined ship type passed.");
    return string.Empty;
  }
}
