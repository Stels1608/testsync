﻿// Decompiled with JetBrains decompiler
// Type: CameraFacingBillboard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CameraFacingBillboard : MonoBehaviour
{
  private Camera cachedCamera;
  private Transform cameraTransform;
  private Transform myTransform;

  private void Awake()
  {
    this.myTransform = this.transform;
  }

  private void LateUpdate()
  {
    if ((Object) SpaceLevel.GetLevel() == (Object) null || SpaceLevel.GetLevel().cameraSwitcher == null)
      return;
    Camera activeCamera = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera();
    if ((Object) this.cachedCamera != (Object) activeCamera)
    {
      this.cachedCamera = activeCamera;
      this.cameraTransform = activeCamera.transform;
    }
    this.myTransform.rotation = this.cameraTransform.rotation;
  }
}
