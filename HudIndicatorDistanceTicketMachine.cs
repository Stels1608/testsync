﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorDistanceTicketMachine
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public static class HudIndicatorDistanceTicketMachine
{
  private static readonly SortedList<HudIndicatorDistanceTextUgui, bool> updatingIndicators = new SortedList<HudIndicatorDistanceTextUgui, bool>((IComparer<HudIndicatorDistanceTextUgui>) new HudIndicatorDistanceTicketMachine.DistanceTextComparer());
  private static readonly List<HudIndicatorDistanceTextUgui> tempCleanupList = new List<HudIndicatorDistanceTextUgui>();
  private static int lastCleanupFrame;

  public static int RequestUpdateFrame(HudIndicatorDistanceTextUgui textField)
  {
    if (HudIndicatorDistanceTicketMachine.lastCleanupFrame != Time.frameCount)
    {
      HudIndicatorDistanceTicketMachine.lastCleanupFrame = Time.frameCount;
      HudIndicatorDistanceTicketMachine.RemoveExpiredIndicators();
    }
    if (HudIndicatorDistanceTicketMachine.updatingIndicators.ContainsKey(textField))
      return textField.UpdateFrame;
    HudIndicatorDistanceTicketMachine.updatingIndicators.Add(textField, true);
    int num = Mathf.Max(HudIndicatorDistanceTicketMachine.updatingIndicators.Count / 10, 5);
    return Time.frameCount + HudIndicatorDistanceTicketMachine.updatingIndicators.IndexOfKey(textField) / num;
  }

  private static void RemoveExpiredIndicators()
  {
    HudIndicatorDistanceTicketMachine.tempCleanupList.Clear();
    foreach (KeyValuePair<HudIndicatorDistanceTextUgui, bool> updatingIndicator in HudIndicatorDistanceTicketMachine.updatingIndicators)
    {
      if (updatingIndicator.Key.UpdateFrame <= Time.frameCount)
        HudIndicatorDistanceTicketMachine.tempCleanupList.Add(updatingIndicator.Key);
    }
    using (List<HudIndicatorDistanceTextUgui>.Enumerator enumerator = HudIndicatorDistanceTicketMachine.tempCleanupList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HudIndicatorDistanceTextUgui current = enumerator.Current;
        HudIndicatorDistanceTicketMachine.updatingIndicators.Remove(current);
      }
    }
  }

  private class DistanceTextComparer : IComparer<HudIndicatorDistanceTextUgui>
  {
    public int Compare(HudIndicatorDistanceTextUgui x, HudIndicatorDistanceTextUgui y)
    {
      return x.DisplayedDistance.CompareTo(y.DisplayedDistance);
    }
  }
}
