﻿// Decompiled with JetBrains decompiler
// Type: CometLOD
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CometLOD : MonoBehaviour
{
  public static bool HighQualityParticles;
  public ParticleSystem comaClouds;
  public ParticleSystem surfaceJets;
  public ParticleSystem debrisBig;
  public ParticleSystem hydrogenCloud;
  public ParticleSystem comaTrail;

  private void Start()
  {
    this.ToggleHighQualityParticles(CometLOD.HighQualityParticles);
  }

  public void ToggleHighQualityParticles(bool enable)
  {
    this.comaClouds.gameObject.SetActive(enable);
    this.surfaceJets.gameObject.SetActive(enable);
    this.debrisBig.gameObject.SetActive(enable);
    this.hydrogenCloud.startLifetime = !enable ? 7f : 10f;
    this.hydrogenCloud.startSize = !enable ? 620f : 750f;
    this.hydrogenCloud.emissionRate = !enable ? 6f : 8f;
    this.comaTrail.emissionRate = !enable ? 10f : 15f;
  }
}
