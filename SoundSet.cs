﻿// Decompiled with JetBrains decompiler
// Type: SoundSet
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SoundSet : MonoBehaviour
{
  public float Volume = 1f;
  public float MinDistance = 1f;
  private int index = -1;
  public AudioClip[] Clips;

  public AudioClip GetNextClip(bool shuffle)
  {
    if (this.Clips.Length == 0)
      return (AudioClip) null;
    this.index = !shuffle ? (this.index + 1) % this.Clips.Length : Random.Range(0, this.Clips.Length);
    return this.Clips[this.index];
  }
}
