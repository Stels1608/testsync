﻿// Decompiled with JetBrains decompiler
// Type: SpecialOfferButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Tooltips;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SpecialOfferButton : MonoBehaviour, IGUIRenderable, ILocalizeable
{
  public UnityEngine.Sprite normal;
  public UnityEngine.Sprite mouseOver;
  public UnityEngine.Sprite glowNormal;
  public UnityEngine.Sprite glowAmplified;
  public Image buttonBackground;
  public Image icon;
  public Image glowSprite;
  private GuiAdvancedLabelTooltip tooltip;
  private LTDescr tween;

  public bool IsRendered
  {
    get
    {
      return this.gameObject.activeSelf;
    }
    set
    {
      this.gameObject.SetActive(value && this.IsEnabledInLevel);
    }
  }

  public bool IsEnabledInLevel { get; set; }

  public bool IsBigWindow
  {
    get
    {
      return false;
    }
  }

  private void Start()
  {
    this.OnHover(false);
    EventTrigger eventTrigger = this.gameObject.AddComponent<EventTrigger>();
    UguiTools.AddEventTrigger(eventTrigger, new UnityAction<BaseEventData>(this.OnPointerEnter), EventTriggerType.PointerEnter);
    UguiTools.AddEventTrigger(eventTrigger, new UnityAction<BaseEventData>(this.OnPointerExit), EventTriggerType.PointerExit);
    UguiTools.AddEventTrigger(eventTrigger, new UnityAction<BaseEventData>(this.OnClick), EventTriggerType.PointerClick);
    this.ReloadLanguageData();
  }

  private void OnPointerEnter(BaseEventData eventData)
  {
    this.OnHover(true);
  }

  private void OnPointerExit(BaseEventData eventData)
  {
    this.OnHover(false);
  }

  private void OnHover(bool isOver)
  {
    this.buttonBackground.color = !isOver ? ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR) : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_CLICK_COLOR);
    this.buttonBackground.sprite = !isOver ? this.normal : this.mouseOver;
    this.icon.color = !isOver ? Color.white : Gui.Options.MouseOverInvertColor;
    if (isOver)
      Game.TooltipManager.ShowTooltip((GuiAdvancedTooltipBase) this.tooltip);
    else
      Game.TooltipManager.HideTooltip((GuiAdvancedTooltipBase) this.tooltip);
  }

  private void OnClick(BaseEventData eventData)
  {
    FacadeFactory.GetInstance().SendMessage(Message.ShowSpecialOffer);
  }

  public void Glow(bool glow)
  {
    this.glowSprite.sprite = !glow ? this.glowNormal : this.glowAmplified;
    if (this.tween != null)
      LeanTween.cancel(this.glowSprite.gameObject);
    if (!glow)
      return;
    this.tween = LeanTween.alpha(this.glowSprite.rectTransform, 1f, 1f).setFrom(0.5f).setLoopPingPong().setIgnoreTimeScale(true);
  }

  public void ReloadLanguageData()
  {
    this.tooltip = new GuiAdvancedLabelTooltip();
    this.tooltip.HideManually = true;
    this.tooltip.SetText("%$bgo.etc.special_offer_tooltip%");
  }

  public void RegisterCallbacks()
  {
    Game.SpecialOfferManager.OnHappyHourChanged += new SpecialOfferManager.HappyHourDelegate(this.OnHappyHourChanged);
    Game.SpecialOfferManager.OnSpecialOfferChanged += new SpecialOfferManager.SpecialOfferDelegate(this.OnSpecialOfferChanged);
    this.UpdateSpecialOffers();
  }

  public void UpdateSpecialOffers()
  {
    this.OnSpecialOfferChanged(Game.SpecialOfferManager.IsSpecialOffer());
    this.OnHappyHourChanged(Game.SpecialOfferManager.IsHappyHourActive());
  }

  private void OnHappyHourChanged(bool activeFlag)
  {
    this.Glow(activeFlag);
  }

  private void OnSpecialOfferChanged(bool activeFlag)
  {
    this.IsRendered = activeFlag;
  }
}
