﻿// Decompiled with JetBrains decompiler
// Type: RecruitDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class RecruitDesc : IProtocolRead
{
  public uint id;
  public string name;
  public byte level;

  public void Read(BgoProtocolReader r)
  {
    this.id = r.ReadUInt32();
    this.name = r.ReadString();
    this.level = r.ReadByte();
  }
}
