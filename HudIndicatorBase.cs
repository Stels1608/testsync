﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class HudIndicatorBase : MonoBehaviour
{
  protected List<IHudIndicatorComponent> hudIndicatorComponents = new List<IHudIndicatorComponent>();
  protected Vector2 borderOffset = new Vector2(0.0f, 0.0f);
  protected readonly CameraHelper cameraHelper = SpaceCameraBase.CameraHelper;
  protected Transform cachedIndicatorTransform;
  private ISpaceEntity target;
  protected bool? isVisibleOnScreen;
  protected bool isTargetInFrontOfCamera;
  protected bool? isBeyondMiniModeRange;
  protected bool isSelected;
  protected bool isMultiSelected;
  protected bool isActiveWaypoint;
  private bool needsScreenBorderClamping;

  public ISpaceEntity Target
  {
    get
    {
      return this.target;
    }
    set
    {
      this.target = value;
      this.InjectTargetToComponents();
      this.OnTargetInjection();
    }
  }

  protected abstract Vector3 WorldPosition { get; }

  public virtual void ResetStates()
  {
    this.isVisibleOnScreen = new bool?();
    this.target = (ISpaceEntity) null;
    this.isTargetInFrontOfCamera = false;
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.ResetStates();
    }
  }

  protected virtual void Awake()
  {
    this.cachedIndicatorTransform = this.transform;
    this.ResetStates();
  }

  public virtual void RemoteLateUpdate()
  {
    this.PositionUpdate();
    this.TriggerRemoteLateUpdates();
  }

  private void TriggerRemoteLateUpdates()
  {
    for (int index = 0; index < this.hudIndicatorComponents.Count; ++index)
    {
      IRequiresLateUpdate requiresLateUpdate = this.hudIndicatorComponents[index] as IRequiresLateUpdate;
      if (requiresLateUpdate != null)
        requiresLateUpdate.RemoteLateUpdate();
    }
  }

  public void PositionUpdate()
  {
    if ((UnityEngine.Object) this.cameraHelper.CachedMainCam == (UnityEngine.Object) null)
      this.cameraHelper.Update();
    Vector3 screenPoint = this.cameraHelper.CachedMainCam.WorldToScreenPoint(this.WorldPosition);
    this.isTargetInFrontOfCamera = (double) Vector3.Dot(this.cameraHelper.CachedMainCamForwardVector, this.WorldPosition - this.cameraHelper.CachedMainCameraPosition) > 0.0;
    bool? nullable1 = this.isVisibleOnScreen;
    int num1 = nullable1.GetValueOrDefault() ? 1 : 0;
    bool? nullable2 = this.isVisibleOnScreen = new bool?(this.isTargetInFrontOfCamera && this.IsInScreenArea(screenPoint));
    int num2 = nullable2.GetValueOrDefault() ? 1 : 0;
    bool flag = num1 != num2 || nullable1.HasValue ^ nullable2.HasValue;
    if (flag)
    {
      if (this.isVisibleOnScreen.Value)
        this.OnScreenEntered();
      else
        this.OnScreenLeft();
    }
    if (flag || this.isSelected || (this.isMultiSelected || this.isActiveWaypoint))
      this.needsScreenBorderClamping = this.hudIndicatorComponents.Exists((Predicate<IHudIndicatorComponent>) (comp => comp.RequiresScreenBorderClamping()));
    if (!this.isVisibleOnScreen.Value && !this.needsScreenBorderClamping)
      return;
    this.UpdateIndicatorPositionAndOrientation(screenPoint);
  }

  protected virtual void InjectTargetToComponents()
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IReceivesTarget receivesTarget = enumerator.Current as IReceivesTarget;
        if (receivesTarget != null)
          receivesTarget.OnTargetInjection(this.Target);
      }
    }
  }

  protected abstract void UpdateIndicatorPositionAndOrientation(Vector3 unityScreenCoordinates);

  protected float GetIndicatorOrientation(Vector3 unityScreenCoordinates)
  {
    Vector2 normalized = (new Vector2(unityScreenCoordinates.x, (float) Screen.height - unityScreenCoordinates.y) - new Vector2((float) Screen.width / 2f, (float) Screen.height / 2f)).normalized;
    float num1 = float2.up.Angle(float2.FromV2(normalized)) * Mathf.Sign(float2.up.Cross(float2.FromV2(normalized)));
    if (!this.isTargetInFrontOfCamera)
      num1 += 180f;
    float num2 = num1 / 360f;
    if ((double) num2 > 1.0)
      num2 -= (float) (int) num2;
    else if ((double) num2 < -1.0)
      num2 += (float) (int) num2;
    if ((double) num2 < 0.0)
      ++num2;
    return num2;
  }

  public Vector3 GetBorderClampedPosition(float orientation)
  {
    float num1 = (float) Screen.width;
    float num2 = (float) Screen.height;
    Vector3 zero = Vector3.zero;
    if ((double) orientation < 0.125 || (double) orientation >= 0.875)
    {
      float num3 = (float) ((double) orientation / 0.125 * 0.5 + 0.5);
      if ((double) orientation >= 0.875)
        num3 = (float) (((double) orientation - 0.875) / 0.125 * 0.5);
      zero.x = this.borderOffset.y + num3 * (num1 - 2f * this.borderOffset.y);
      zero.y = this.borderOffset.y;
    }
    else if ((double) orientation >= 0.125 && (double) orientation < 0.375)
    {
      float num3 = (float) (((double) orientation - 0.125) / 0.25);
      zero.x = num1 - this.borderOffset.x;
      zero.y = this.borderOffset.x + num3 * (num2 - 2f * this.borderOffset.x);
    }
    else if ((double) orientation >= 0.375 && (double) orientation < 0.625)
    {
      float num3 = (float) (1.0 - ((double) orientation - 0.375) / 0.25);
      zero.x = this.borderOffset.y + num3 * (num1 - 2f * this.borderOffset.y);
      zero.y = num2 - this.borderOffset.y;
    }
    else
    {
      float num3 = (float) (1.0 - ((double) orientation - 0.625) / 0.25);
      zero.x = this.borderOffset.x;
      zero.y = this.borderOffset.x + num3 * (num2 - 2f * this.borderOffset.x);
    }
    zero.y = num2 - zero.y;
    return zero;
  }

  public bool TryGetHudIndicatorComponent<T>(out T hudIndicatorComponent)
  {
    IHudIndicatorComponent indicatorComponent = this.hudIndicatorComponents.Find((Predicate<IHudIndicatorComponent>) (compo => compo is T));
    if (indicatorComponent != null)
    {
      hudIndicatorComponent = (T) indicatorComponent;
      return true;
    }
    hudIndicatorComponent = default (T);
    return false;
  }

  public bool HasHudIndicatorComponent<T>()
  {
    return null != this.hudIndicatorComponents.Find((Predicate<IHudIndicatorComponent>) (compo => compo is T));
  }

  protected void UpdateArrowOrientation(float orientation)
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IOutsideScreenArrow outsideScreenArrow = enumerator.Current as IOutsideScreenArrow;
        if (outsideScreenArrow != null)
          outsideScreenArrow.OnOrientationChanged(orientation);
      }
    }
  }

  protected abstract bool IsInScreenArea(Vector3 targetScreenPos);

  public abstract T AddHudIndicatorComponent<T>() where T : Component, IHudIndicatorComponent;

  protected T RegisterHudIndicatorComponent<T>(T hudIndicatorComponent) where T : Component, IHudIndicatorComponent
  {
    hudIndicatorComponent.name = typeof (T).ToString();
    this.hudIndicatorComponents.Add((IHudIndicatorComponent) hudIndicatorComponent);
    return hudIndicatorComponent;
  }

  public void OnScannerVisibilityChange()
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnScannerVisibilityChange(this.Target.IsInMapRange);
    }
  }

  private void OnScreenEntered()
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnScreenEnter();
    }
  }

  private void OnScreenLeft()
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnScreenLeave();
    }
  }

  public void UpdateColoring()
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateColorAndAlpha();
    }
  }

  public abstract void SetOpacity(float opacity);

  public virtual void SetSelected(bool targetIsSelected)
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnSelectionChange(targetIsSelected);
    }
  }

  public virtual void SetMultiSelected(bool isMultiSelected, ShipAbility ability)
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnMultiTargetSelectionChange(isMultiSelected, ability);
    }
  }

  public void SetDesignated(bool isDesignated)
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnDesignationChange(isDesignated);
    }
  }

  public void SetScanned(ItemCountable resource)
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnScanned(resource);
    }
  }

  public void SetWaypointStatus(bool isWaypoint)
  {
    this.isActiveWaypoint = isWaypoint;
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnWaypointStatusChange(isWaypoint);
    }
  }

  private void OnTargetInjection()
  {
    this.UpdateVisibility();
  }

  public abstract void UpdateVisibility();

  protected abstract void EnableCombatGui(bool enable);

  public virtual void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IHudIndicatorComponent current = enumerator.Current;
        if (current is IReceivesSetting)
          ((IReceivesSetting) current).ApplyOptionsSetting(userSetting, data);
      }
    }
    if (userSetting != UserSetting.CombatGui)
      return;
    this.EnableCombatGui((bool) data);
  }

  public virtual void ApplyInputBindings(InputBinder inputBinder)
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IHudIndicatorComponent current = enumerator.Current;
        if (current is IReceivesInputBindings)
          ((IReceivesInputBindings) current).OnInputBinderInjection(inputBinder);
      }
    }
  }
}
