﻿// Decompiled with JetBrains decompiler
// Type: ScannerEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ScannerEffect : MonoBehaviour
{
  public int uvAnimationTilesX = 4;
  public int uvAnimationTilesY = 4;
  public int numberOfFrames = 16;
  public float waveSpeed = 0.035f;
  public Color waveColor = new Color(1f, 0.0f, 0.0f);
  public Transform scannerPosition;
  public bool loopWave;
  public float scanDistance;
  private float scanAngle;
  public float radius;
  private Renderer _renderer;

  private void Start()
  {
    if (this.numberOfFrames <= 1)
      this.numberOfFrames = this.uvAnimationTilesX * this.uvAnimationTilesY;
    if (this.numberOfFrames > this.uvAnimationTilesX * this.uvAnimationTilesY)
      this.numberOfFrames = this.uvAnimationTilesX * this.uvAnimationTilesY;
    this._renderer = this.GetComponent<Renderer>();
  }

  private void Update()
  {
    this._renderer.material.color = this.waveColor;
    this._renderer.material.SetVector("v3ScannerPosition", (Vector4) this.scannerPosition.position);
    this._renderer.material.SetVector("v3ScannerDirection", (Vector4) (this.scannerPosition.position - this.transform.position).normalized);
    this.scanAngle += Time.deltaTime * 45f;
    if ((double) this.scanAngle > 360.0)
      this.scanAngle -= 360f;
    this.scanDistance += Time.deltaTime * 4f * this.waveSpeed;
    if ((double) this.scanDistance > 1.0)
      this.scanDistance = !this.loopWave ? 1f : 0.0f;
    float magnitude = (this.transform.position - this.scannerPosition.position).magnitude;
    this._renderer.material.SetFloat("fScannerDistance", Mathf.Lerp(magnitude - this.radius, magnitude + this.radius * 2f, this.scanDistance));
    this._renderer.material.SetFloat("fObjectRadius", this.radius);
  }
}
