﻿// Decompiled with JetBrains decompiler
// Type: ShipSystemCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class ShipSystemCard : Card
{
  public float Durability = float.MaxValue;
  public byte Level;
  public byte MaxLevel;
  public ShipSystemCard NextCard;
  public ShipSlotType SlotType;
  public byte Tier;
  public List<uint> ShipObjectKeyRestrictions;
  public ShipRole[] ShipRoleRestrictions;
  public uint[] SkillHashes;
  public ShipAbilityCard[] AbilityCards;
  public ObjectStats StaticBuffs;
  public ObjectStats MultiplyBuffs;
  public ShipSystemClass Class;
  public ShopItemCard shopItemCard;
  public StatView[] Views;
  public bool Unique;
  public bool ReplaceableOnly;
  public bool UserUpgradeable;
  public bool Trashable;
  public bool Indestructible;
  public byte MaxCountPerShip;

  public List<ShipCard> ShipRestrictions
  {
    get
    {
      List<ShipCard> shipCardList = new List<ShipCard>();
      using (List<uint>.Enumerator enumerator = this.ShipObjectKeyRestrictions.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ShipCard shipCard = (ShipCard) Game.Catalogue.FetchCard(enumerator.Current, CardView.Ship);
          shipCardList.Add(shipCard);
        }
      }
      return shipCardList;
    }
  }

  public ShipSystemCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public ShipSystemCard GetNextCardByLevel(int level)
  {
    if (level < (int) this.Level)
      return this;
    return this.FindCardOfLevel(this, level);
  }

  protected ShipSystemCard FindCardOfLevel(ShipSystemCard systemCard, int level)
  {
    if ((int) systemCard.Level == level || systemCard.NextCard == null)
      return systemCard;
    return this.FindCardOfLevel(systemCard.NextCard, level);
  }

  public override void Read(BgoProtocolReader r)
  {
    this.Level = r.ReadByte();
    this.MaxLevel = r.ReadByte();
    uint cardGUID1 = r.ReadGUID();
    this.SlotType = (ShipSlotType) r.ReadByte();
    this.Tier = r.ReadByte();
    int num = r.ReadLength();
    this.ShipObjectKeyRestrictions = new List<uint>();
    for (int index = 0; index < num; ++index)
    {
      uint cardGUID2 = r.ReadGUID();
      ShipCard shipCard = (ShipCard) Game.Catalogue.FetchCard(cardGUID2, CardView.Ship);
      this.ShipObjectKeyRestrictions.Add(cardGUID2);
      this.IsLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) shipCard
      });
    }
    int length1 = r.ReadLength();
    this.ShipRoleRestrictions = new ShipRole[length1];
    for (int index = 0; index < length1; ++index)
      this.ShipRoleRestrictions[index] = (ShipRole) r.ReadByte();
    int length2 = r.ReadLength();
    this.SkillHashes = new uint[length2];
    for (int index = 0; index < length2; ++index)
      this.SkillHashes[index] = r.ReadUInt32();
    int length3 = r.ReadLength();
    this.AbilityCards = new ShipAbilityCard[length3];
    for (int index = 0; index < length3; ++index)
    {
      uint cardGUID2 = r.ReadGUID();
      this.AbilityCards[index] = (ShipAbilityCard) Game.Catalogue.FetchCard(cardGUID2, CardView.ShipAbility);
      this.IsLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) this.AbilityCards[index]
      });
    }
    this.StaticBuffs = r.ReadDesc<ObjectStats>();
    this.MultiplyBuffs = r.ReadDesc<ObjectStats>();
    this.Durability = r.ReadSingle();
    this.Class = (ShipSystemClass) r.ReadByte();
    int length4 = r.ReadLength();
    this.Views = new StatView[length4];
    for (int index = 0; index < length4; ++index)
      this.Views[index] = (StatView) r.ReadByte();
    this.shopItemCard = (ShopItemCard) Game.Catalogue.FetchCard(this.CardGUID, CardView.Price);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.shopItemCard
    });
    if ((int) cardGUID1 != 0)
    {
      this.NextCard = (ShipSystemCard) Game.Catalogue.FetchCard(cardGUID1, CardView.ShipSystem);
      this.IsLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) this.NextCard
      });
    }
    this.Unique = r.ReadBoolean();
    this.ReplaceableOnly = r.ReadBoolean();
    this.UserUpgradeable = r.ReadBoolean();
    this.Trashable = r.ReadBoolean();
    this.Indestructible = r.ReadBoolean();
    this.MaxCountPerShip = r.ReadByte();
  }

  public override string ToString()
  {
    return string.Format("Level: {0}, MaxLevel: {1}, NextCard: {2}, SlotType: {3}, Tier: {4}, ShipObjectKeyRestrictions: {5}, ShipRoleRestrictions: {6}, SkillHashes: {7}, AbilityCards: {8}, StaticBuffs: {9}, MultiplyBuffs: {10}, Durability: {11}, Class: {12}, PriceCard: {13}, Views: {14}, Unique: {15}, ReplaceableOnly: {16}, UserUpgradeable: {17}, Trashable: {18}, Indestructible: {19}", (object) this.Level, (object) this.MaxLevel, (object) this.NextCard, (object) this.SlotType, (object) this.Tier, (object) this.ShipObjectKeyRestrictions, (object) this.ShipRoleRestrictions, (object) this.SkillHashes, (object) this.AbilityCards, (object) this.StaticBuffs, (object) this.MultiplyBuffs, (object) this.Durability, (object) this.Class, (object) this.shopItemCard, (object) this.Views, (object) this.Unique, (object) this.ReplaceableOnly, (object) this.UserUpgradeable, (object) this.Trashable, (object) this.Indestructible);
  }
}
