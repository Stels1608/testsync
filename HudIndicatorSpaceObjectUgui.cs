﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorSpaceObjectUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class HudIndicatorSpaceObjectUgui : HudIndicatorBaseUgui
{
  private bool showRelationSetting = true;

  protected SpaceObject SpaceObject
  {
    get
    {
      return (SpaceObject) this.Target;
    }
  }

  protected override Vector3 WorldPosition
  {
    get
    {
      return this.SpaceObject.Position;
    }
  }

  protected override void InjectTargetToComponents()
  {
    if (this.Target == null)
      Debug.LogError((object) "Target is nuuuuull");
    base.InjectTargetToComponents();
    this.InjectSpaceObjectToComponents();
  }

  protected void InjectSpaceObjectToComponents()
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IReceivesSpaceObject receivesSpaceObject = enumerator.Current as IReceivesSpaceObject;
        if (receivesSpaceObject != null)
          receivesSpaceObject.OnSpaceObjectInjection(this.SpaceObject);
      }
    }
  }

  public override void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    base.ApplyOptionsSetting(userSetting, data);
    switch (userSetting)
    {
      case UserSetting.ShowEnemyIndication:
        if (this.SpaceObject.PlayerRelation != Relation.Enemy)
          break;
        this.showRelationSetting = (bool) data;
        this.UpdateVisibility();
        break;
      case UserSetting.ShowFriendIndication:
        if (this.SpaceObject.PlayerRelation != Relation.Friend)
          break;
        this.showRelationSetting = (bool) data;
        this.UpdateVisibility();
        break;
    }
  }

  public override void UpdateVisibility()
  {
    bool flag;
    if (this.Target.SpaceEntityType == SpaceEntityType.Player)
    {
      PlayerShip playerShip = this.Target as PlayerShip;
      flag = playerShip != null && (bool) playerShip.HasSpawnedInSector && playerShip.IsVisible;
      if (playerShip == null)
      {
        Debug.LogError((object) "Playership is null???");
        flag = true;
      }
    }
    else
      flag = true;
    this.gameObject.SetActive(flag && this.showRelationSetting);
  }
}
