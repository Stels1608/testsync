﻿// Decompiled with JetBrains decompiler
// Type: UISprite
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/UI/NGUI Sprite")]
public class UISprite : UIBasicSprite
{
  [HideInInspector]
  [SerializeField]
  private bool mFillCenter = true;
  [SerializeField]
  [HideInInspector]
  private UIAtlas mAtlas;
  [HideInInspector]
  [SerializeField]
  private string mSpriteName;
  [NonSerialized]
  protected UISpriteData mSprite;
  [NonSerialized]
  private bool mSpriteSet;

  public override Material material
  {
    get
    {
      if ((UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null)
        return this.mAtlas.spriteMaterial;
      return (Material) null;
    }
  }

  public UIAtlas atlas
  {
    get
    {
      return this.mAtlas;
    }
    set
    {
      if (!((UnityEngine.Object) this.mAtlas != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mAtlas = value;
      this.mSpriteSet = false;
      this.mSprite = (UISpriteData) null;
      if (string.IsNullOrEmpty(this.mSpriteName) && (UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null && this.mAtlas.spriteList.Count > 0)
      {
        this.SetAtlasSprite(this.mAtlas.spriteList[0]);
        this.mSpriteName = this.mSprite.name;
      }
      if (string.IsNullOrEmpty(this.mSpriteName))
        return;
      string str = this.mSpriteName;
      this.mSpriteName = string.Empty;
      this.spriteName = str;
      this.MarkAsChanged();
    }
  }

  public string spriteName
  {
    get
    {
      return this.mSpriteName;
    }
    set
    {
      if (string.IsNullOrEmpty(value))
      {
        if (string.IsNullOrEmpty(this.mSpriteName))
          return;
        this.mSpriteName = string.Empty;
        this.mSprite = (UISpriteData) null;
        this.mChanged = true;
        this.mSpriteSet = false;
      }
      else
      {
        if (!(this.mSpriteName != value))
          return;
        this.mSpriteName = value;
        this.mSprite = (UISpriteData) null;
        this.mChanged = true;
        this.mSpriteSet = false;
      }
    }
  }

  public bool isValid
  {
    get
    {
      return this.GetAtlasSprite() != null;
    }
  }

  [Obsolete("Use 'centerType' instead")]
  public bool fillCenter
  {
    get
    {
      return this.centerType != UIBasicSprite.AdvancedType.Invisible;
    }
    set
    {
      if (value == (this.centerType != UIBasicSprite.AdvancedType.Invisible))
        return;
      this.centerType = !value ? UIBasicSprite.AdvancedType.Invisible : UIBasicSprite.AdvancedType.Sliced;
      this.MarkAsChanged();
    }
  }

  public override Vector4 border
  {
    get
    {
      UISpriteData atlasSprite = this.GetAtlasSprite();
      if (atlasSprite == null)
        return base.border;
      return new Vector4((float) atlasSprite.borderLeft, (float) atlasSprite.borderBottom, (float) atlasSprite.borderRight, (float) atlasSprite.borderTop);
    }
  }

  public override float pixelSize
  {
    get
    {
      if ((UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null)
        return this.mAtlas.pixelSize;
      return 1f;
    }
  }

  public override int minWidth
  {
    get
    {
      if (this.type != UIBasicSprite.Type.Sliced && this.type != UIBasicSprite.Type.Advanced)
        return base.minWidth;
      Vector4 vector4 = this.border * this.pixelSize;
      int @int = Mathf.RoundToInt(vector4.x + vector4.z);
      UISpriteData atlasSprite = this.GetAtlasSprite();
      if (atlasSprite != null)
        @int += atlasSprite.paddingLeft + atlasSprite.paddingRight;
      return Mathf.Max(base.minWidth, (@int & 1) != 1 ? @int : @int + 1);
    }
  }

  public override int minHeight
  {
    get
    {
      if (this.type != UIBasicSprite.Type.Sliced && this.type != UIBasicSprite.Type.Advanced)
        return base.minHeight;
      Vector4 vector4 = this.border * this.pixelSize;
      int @int = Mathf.RoundToInt(vector4.y + vector4.w);
      UISpriteData atlasSprite = this.GetAtlasSprite();
      if (atlasSprite != null)
        @int += atlasSprite.paddingTop + atlasSprite.paddingBottom;
      return Mathf.Max(base.minHeight, (@int & 1) != 1 ? @int : @int + 1);
    }
  }

  public override Vector4 drawingDimensions
  {
    get
    {
      Vector2 pivotOffset = this.pivotOffset;
      float from1 = -pivotOffset.x * (float) this.mWidth;
      float from2 = -pivotOffset.y * (float) this.mHeight;
      float to1 = from1 + (float) this.mWidth;
      float to2 = from2 + (float) this.mHeight;
      if (this.GetAtlasSprite() != null && this.mType != UIBasicSprite.Type.Tiled)
      {
        int num1 = this.mSprite.paddingLeft;
        int num2 = this.mSprite.paddingBottom;
        int num3 = this.mSprite.paddingRight;
        int num4 = this.mSprite.paddingTop;
        int num5 = this.mSprite.width + num1 + num3;
        int num6 = this.mSprite.height + num2 + num4;
        float num7 = 1f;
        float num8 = 1f;
        if (num5 > 0 && num6 > 0 && (this.mType == UIBasicSprite.Type.Simple || this.mType == UIBasicSprite.Type.Filled))
        {
          if ((num5 & 1) != 0)
            ++num3;
          if ((num6 & 1) != 0)
            ++num4;
          num7 = 1f / (float) num5 * (float) this.mWidth;
          num8 = 1f / (float) num6 * (float) this.mHeight;
        }
        if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
        {
          from1 += (float) num3 * num7;
          to1 -= (float) num1 * num7;
        }
        else
        {
          from1 += (float) num1 * num7;
          to1 -= (float) num3 * num7;
        }
        if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
        {
          from2 += (float) num4 * num8;
          to2 -= (float) num2 * num8;
        }
        else
        {
          from2 += (float) num2 * num8;
          to2 -= (float) num4 * num8;
        }
      }
      Vector4 vector4 = !((UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null) ? Vector4.zero : this.border * this.pixelSize;
      float num9 = vector4.x + vector4.z;
      float num10 = vector4.y + vector4.w;
      return new Vector4(Mathf.Lerp(from1, to1 - num9, this.mDrawRegion.x), Mathf.Lerp(from2, to2 - num10, this.mDrawRegion.y), Mathf.Lerp(from1 + num9, to1, this.mDrawRegion.z), Mathf.Lerp(from2 + num10, to2, this.mDrawRegion.w));
    }
  }

  public override bool premultipliedAlpha
  {
    get
    {
      if ((UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null)
        return this.mAtlas.premultipliedAlpha;
      return false;
    }
  }

  public UISpriteData GetAtlasSprite()
  {
    if (!this.mSpriteSet)
      this.mSprite = (UISpriteData) null;
    if (this.mSprite == null && (UnityEngine.Object) this.mAtlas != (UnityEngine.Object) null)
    {
      if (!string.IsNullOrEmpty(this.mSpriteName))
      {
        UISpriteData sprite = this.mAtlas.GetSprite(this.mSpriteName);
        if (sprite == null)
          return (UISpriteData) null;
        this.SetAtlasSprite(sprite);
      }
      if (this.mSprite == null && this.mAtlas.spriteList.Count > 0)
      {
        UISpriteData sp = this.mAtlas.spriteList[0];
        if (sp == null)
          return (UISpriteData) null;
        this.SetAtlasSprite(sp);
        if (this.mSprite == null)
        {
          Debug.LogError((object) (this.mAtlas.name + " seems to have a null sprite!"));
          return (UISpriteData) null;
        }
        this.mSpriteName = this.mSprite.name;
      }
    }
    return this.mSprite;
  }

  protected void SetAtlasSprite(UISpriteData sp)
  {
    this.mChanged = true;
    this.mSpriteSet = true;
    if (sp != null)
    {
      this.mSprite = sp;
      this.mSpriteName = this.mSprite.name;
    }
    else
    {
      this.mSpriteName = this.mSprite == null ? string.Empty : this.mSprite.name;
      this.mSprite = sp;
    }
  }

  public override void MakePixelPerfect()
  {
    if (!this.isValid)
      return;
    base.MakePixelPerfect();
    if (this.mType == UIBasicSprite.Type.Tiled)
      return;
    UISpriteData atlasSprite = this.GetAtlasSprite();
    if (atlasSprite == null)
      return;
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null || this.mType != UIBasicSprite.Type.Simple && this.mType != UIBasicSprite.Type.Filled && atlasSprite.hasBorder || !((UnityEngine.Object) mainTexture != (UnityEngine.Object) null))
      return;
    int int1 = Mathf.RoundToInt(this.pixelSize * (float) (atlasSprite.width + atlasSprite.paddingLeft + atlasSprite.paddingRight));
    int int2 = Mathf.RoundToInt(this.pixelSize * (float) (atlasSprite.height + atlasSprite.paddingTop + atlasSprite.paddingBottom));
    if ((int1 & 1) == 1)
      ++int1;
    if ((int2 & 1) == 1)
      ++int2;
    this.width = int1;
    this.height = int2;
  }

  protected override void OnInit()
  {
    if (!this.mFillCenter)
    {
      this.mFillCenter = true;
      this.centerType = UIBasicSprite.AdvancedType.Invisible;
    }
    base.OnInit();
  }

  protected override void OnUpdate()
  {
    base.OnUpdate();
    if (!this.mChanged && this.mSpriteSet)
      return;
    this.mSpriteSet = true;
    this.mSprite = (UISpriteData) null;
    this.mChanged = true;
  }

  public override void OnFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
      return;
    if (this.mSprite == null)
      this.mSprite = this.atlas.GetSprite(this.spriteName);
    if (this.mSprite == null)
      return;
    Rect rect1 = new Rect((float) this.mSprite.x, (float) this.mSprite.y, (float) this.mSprite.width, (float) this.mSprite.height);
    Rect rect2 = new Rect((float) (this.mSprite.x + this.mSprite.borderLeft), (float) (this.mSprite.y + this.mSprite.borderTop), (float) (this.mSprite.width - this.mSprite.borderLeft - this.mSprite.borderRight), (float) (this.mSprite.height - this.mSprite.borderBottom - this.mSprite.borderTop));
    Rect texCoords1 = NGUIMath.ConvertToTexCoords(rect1, mainTexture.width, mainTexture.height);
    Rect texCoords2 = NGUIMath.ConvertToTexCoords(rect2, mainTexture.width, mainTexture.height);
    int bufferOffset = verts.size;
    this.Fill(verts, uvs, cols, texCoords1, texCoords2);
    if (this.onPostFill == null)
      return;
    this.onPostFill((UIWidget) this, bufferOffset, verts, uvs, cols);
  }
}
