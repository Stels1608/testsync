﻿// Decompiled with JetBrains decompiler
// Type: Cannonade
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Cannonade : MonoBehaviour
{
  public float SmallMinDistance;
  public float LargeMinDistance;
  public float NukeMinDistance;
  public float MaxDistance;
  public GameObject SmallExplosion;
  public GameObject LargeExplosion;
  public GameObject NukeExplosion;
  public float Radius;
  public float Height;
  public float SmallMinInterval;
  public float SmallMaxInterval;
  public float LargeMinInterval;
  public float LargeMaxInterval;
  public float NukeMinInterval;
  public float NukeMaxInterval;
  public bool Ready;

  private void Start()
  {
    CannonadeSupply cannonadeSupply = this.transform.parent.GetComponent<CannonadeSupply>();
    if ((Object) cannonadeSupply == (Object) null)
      cannonadeSupply = this.transform.parent.gameObject.AddComponent<CannonadeSupply>();
    cannonadeSupply.Cannonade = this;
  }
}
