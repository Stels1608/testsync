﻿// Decompiled with JetBrains decompiler
// Type: Shrapnel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Shrapnel : Weapon
{
  public GameObject muzzle;
  public AudioClip[] fireSounds;
  private AudioSource gunAudio;

  private void Awake()
  {
    this.gunAudio = this.gameObject.AddComponent<AudioSource>();
    this.gunAudio.rolloffMode = AudioRolloffMode.Linear;
    this.gunAudio.minDistance = 10f;
    this.gunAudio.maxDistance = 2000f;
    this.gunAudio.playOnAwake = false;
    this.gunAudio.loop = false;
    this.gunAudio.spatialBlend = 1f;
  }

  public override void Fire(SpaceObject target)
  {
    if ((Object) this.muzzle == (Object) null)
      return;
    GameObject gameObject = Object.Instantiate<GameObject>(this.muzzle);
    gameObject.transform.position = this.transform.position;
    gameObject.transform.rotation = this.transform.rotation;
    this.PlayFireSound();
  }

  private void PlayFireSound()
  {
    if (this.fireSounds == null || this.fireSounds.Length == 0)
      return;
    this.gunAudio.clip = this.fireSounds[Random.Range(0, this.fireSounds.Length)];
    this.gunAudio.Play();
  }
}
