﻿// Decompiled with JetBrains decompiler
// Type: GuildOperation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum GuildOperation : ushort
{
  ChangePermissions = 1,
  ChangeRankNames = 2,
  Invite = 3,
  PromoteDemote = 4,
  KickMember = 5,
  OfficerChat = 6,
}
