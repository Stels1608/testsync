﻿// Decompiled with JetBrains decompiler
// Type: CardFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class CardFactory
{
  public static Card CreateCard(uint cardGUID, CardView cardView)
  {
    switch (cardView)
    {
      case CardView.GUI:
        return (Card) new GUICard(cardGUID);
      case CardView.ShipSystem:
        return (Card) new ShipSystemCard(cardGUID);
      case CardView.ShipConsumable:
        return (Card) new ShipConsumableCard(cardGUID);
      case CardView.World:
        return (Card) new WorldCard(cardGUID);
      case CardView.Global:
        return (Card) new GlobalCard(cardGUID);
      case CardView.ShipAbility:
        return (Card) new ShipAbilityCard(cardGUID);
      case CardView.Counter:
        return (Card) new CounterCard(cardGUID);
      case CardView.Skill:
        return (Card) new SkillCard(cardGUID);
      case CardView.Ship:
        return (Card) new ShipCard(cardGUID);
      case CardView.Sector:
        return (Card) new SectorCard(cardGUID);
      case CardView.Starter:
        return (Card) new StarterCard(cardGUID);
      case CardView.Room:
        return (Card) new RoomCard(cardGUID);
      case CardView.Mission:
        return (Card) new MissionCard(cardGUID);
      case CardView.Reward:
        return (Card) new RewardCard(cardGUID);
      case CardView.Title:
        return (Card) new TitleCard(cardGUID);
      case CardView.Duty:
        return (Card) new DutyCard(cardGUID);
      case CardView.AvatarCatalogue:
        return (Card) new AvatarCatalogueCard(cardGUID);
      case CardView.Module:
        return (Card) new ModuleCard(cardGUID);
      case CardView.Price:
        return (Card) new ShopItemCard(cardGUID);
      case CardView.Missile:
        return (Card) new MissileCard(cardGUID);
      case CardView.ShipList:
        return (Card) new ShipListCard(cardGUID);
      case CardView.StickerList:
        return (Card) new StickerListCard(cardGUID);
      case CardView.Movement:
        return (Card) new MovementCard(cardGUID);
      case CardView.Owner:
        return (Card) new OwnerCard(cardGUID);
      case CardView.GalaxyMap:
        return (Card) new GalaxyMapCard(cardGUID);
      case CardView.Camera:
        return (Card) new CameraCard(cardGUID);
      case CardView.MailTemplate:
        return (Card) new MailTemplateCard(cardGUID);
      case CardView.StarterKit:
        return (Card) new StarterKitCard(cardGUID);
      case CardView.ShipPaint:
        return (Card) new ShipSystemPaintCard(cardGUID);
      case CardView.Regulation:
        return (Card) new RegulationCard(cardGUID);
      case CardView.ShipSale:
        return (Card) new ShipSaleCard(cardGUID);
      case CardView.SectorEvent:
        return (Card) new SectorEventCard(cardGUID);
      case CardView.Tournament:
        return (Card) new TournamentCard(cardGUID);
      case CardView.ShipLight:
        return (Card) new ShipCardLight(cardGUID);
      case CardView.EventShop:
        return (Card) new EventShopCard(cardGUID);
      case CardView.GlobalBonusEvent:
        return (Card) new GlobalBonusEventCard(cardGUID);
      case CardView.Banner:
        return (Card) new BannerCard(cardGUID);
      case CardView.ConversionCampaign:
        return (Card) new SpecialOfferCard(cardGUID);
      default:
        throw new Exception("Unknown Card View: " + (object) cardView);
    }
  }
}
