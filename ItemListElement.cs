﻿// Decompiled with JetBrains decompiler
// Type: ItemListElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ItemListElement : MonoBehaviour
{
  [SerializeField]
  private UILabel amountLabel;
  public UISprite background;
  [SerializeField]
  private UILabel itemName;
  [SerializeField]
  private ItemIcon itemIcon;

  public UILabel ItemName
  {
    get
    {
      return this.itemName;
    }
  }

  public void SetItem(GUICard itemCard, uint amount, int width = 450)
  {
    this.background.width = width;
    this.amountLabel.text = amount <= 1U ? string.Empty : "x" + amount.ToString();
    this.itemIcon.SetGUICard(itemCard);
    this.itemName.text = itemCard.Name;
  }
}
