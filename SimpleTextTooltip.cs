﻿// Decompiled with JetBrains decompiler
// Type: SimpleTextTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class SimpleTextTooltip : TooltipContent
{
  private string text = string.Empty;

  public SimpleTextTooltip(string givenText)
  {
    this.tooltipType = TooltipType.SimpleTextTooltip;
    this.text = Tools.ParseMessage(givenText);
  }

  public override GameObject Generate()
  {
    GameObject gameObject = (GameObject) Object.Instantiate(Resources.Load(this.tooltipResourcesDir + (object) this.tooltipType));
    gameObject.GetComponent<SimpleTextTooltipGameObject>().Init(this.text);
    return gameObject;
  }
}
