﻿// Decompiled with JetBrains decompiler
// Type: AdaptiveZFudgeOffsetter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AdaptiveZFudgeOffsetter : MonoBehaviour
{
  [SerializeField]
  private ParticleSystem[] particleSystems;
  private Material[] particleMaterials;

  private void Awake()
  {
    this.particleMaterials = new Material[this.particleSystems.Length];
    for (int index = 0; index < this.particleSystems.Length; ++index)
      this.particleMaterials[index] = this.particleSystems[index].GetComponent<Renderer>().material;
  }

  private void Update()
  {
    Camera camera;
    if ((Object) GameLevel.Instance == (Object) null)
    {
      camera = Camera.main;
    }
    else
    {
      if (GameLevel.Instance.cameraSwitcher == null)
        return;
      camera = GameLevel.Instance.cameraSwitcher.GetActiveCamera();
    }
    if ((Object) camera == (Object) null)
      return;
    float zfudge = this.CalculateZFudge((this.transform.position - camera.transform.position).magnitude);
    foreach (Material particleMaterial in this.particleMaterials)
      particleMaterial.SetFloat("_ZFudge", -zfudge);
  }

  private float CalculateZFudge(float camDist)
  {
    return Mathf.Min(20000f, 1150325f * Mathf.Pow(camDist, -1.033698f));
  }
}
