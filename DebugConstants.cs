﻿// Decompiled with JetBrains decompiler
// Type: DebugConstants
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class DebugConstants
{
  public const int DebugTraffic = 0;
  public const int DebugStatsView = 1;
  public const int DebugFXManager = 2;
  public const int DebugMessage = 3;
  public const int DebugScene = 4;
  public const int DebugCardInfo = 5;
  public const int DebugSectorInfo = 6;
  public const int DebugItems = 7;
  public const int DebugCounters = 8;
  public const int DebugWindows = 9;
  public const int DebugButtons = 10;
  public const int DebugPlayerInfo = 11;
  public const int DebugSceneStory = 12;
  public const int DebugProtocolView = 13;
  public const int DebugConsole = 14;
  public const int DebugServer = 15;
  public const int DebugSkills = 16;
  public const int DebugProcessStateView = 17;
  public const int DebugAbility = 18;
  public const int DebugSceneSector = 19;
  public const int DebugSceneRoom = 20;
  public const int DebugPositions = 21;
  public const int DebugGodMode = 22;
  public const int DebugSlots = 23;
  public const int DebugModels = 24;
  public const int DebugSubstances = 25;
}
