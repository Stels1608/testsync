﻿// Decompiled with JetBrains decompiler
// Type: SmokeTrail
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SmokeTrail : MonoBehaviour
{
  public bool emit = true;
  public float lifeTime = 2f;
  public int pointMaxCount = 100;
  public float vertexDistance = 0.1f;
  public Color color = new Color(1f, 1f, 1f, 1f);
  private const int w_count = 4;
  private const float u_step = 0.25f;
  private const int h_count = 8;
  private const float v_step = 0.125f;
  private const int frameCount = 32;
  public Material material;
  private Material instanceMaterial;
  private bool emittingDone;
  private float leaveDistance;
  private Vector3 lastPosition;
  private GameObject trailObj;
  private Mesh mesh;
  private SmokeTrail.Point[] points;
  private int endPoint;
  private int pointCnt;

  private void Start()
  {
    this.material = GameObject.Find("material_MissileTrail").GetComponent<MaterialExposer>().material;
    this.trailObj = new GameObject("MissileTrail");
    this.trailObj.transform.parent = (Transform) null;
    this.trailObj.transform.position = Vector3.zero;
    this.trailObj.transform.rotation = Quaternion.identity;
    this.trailObj.transform.localScale = Vector3.one;
    this.mesh = this.trailObj.AddComponent<MeshFilter>().mesh;
    this.trailObj.AddComponent<MeshRenderer>();
    this.instanceMaterial = new Material(this.material);
    this.trailObj.GetComponent<Renderer>().material = this.instanceMaterial;
    this.lastPosition = this.transform.position;
    this.leaveDistance = 0.0f;
    this.points = new SmokeTrail.Point[this.pointMaxCount];
  }

  public void RemoveTrail()
  {
    Object.DestroyImmediate((Object) this.trailObj);
  }

  private void BuildMesh()
  {
    Vector3[] vector3Array = new Vector3[this.pointCnt * 4];
    Color[] colorArray = new Color[this.pointCnt * 4];
    Vector2[] vector2Array = new Vector2[this.pointCnt * 4];
    int[] numArray = new int[this.pointCnt * 6];
    for (int index1 = 0; index1 < this.points.Length; ++index1)
    {
      SmokeTrail.Point point = this.points[index1];
      if (point != null)
      {
        int index2 = index1 * 4;
        this.trailObj.transform.rotation = Camera.main.transform.rotation;
        this.trailObj.transform.rotation *= Quaternion.AngleAxis(point.rotation, Vector3.forward);
        this.trailObj.transform.position = point.position;
        float num1 = point.width;
        vector3Array[index2] = this.trailObj.transform.TransformPoint((float) (-(double) num1 * 0.5), num1 * 0.5f, 0.0f);
        vector3Array[index2 + 1] = this.trailObj.transform.TransformPoint(num1 * 0.5f, num1 * 0.5f, 0.0f);
        vector3Array[index2 + 2] = this.trailObj.transform.TransformPoint(num1 * 0.5f, (float) (-(double) num1 * 0.5), 0.0f);
        vector3Array[index2 + 3] = this.trailObj.transform.TransformPoint((float) (-(double) num1 * 0.5), (float) (-(double) num1 * 0.5), 0.0f);
        int num2 = point.frame / 4;
        float x = (float) (point.frame % 8) * 0.25f;
        float num3 = (float) num2 * 0.125f;
        vector2Array[index2] = new Vector2(x, 1f - num3);
        vector2Array[index2 + 1] = new Vector2(x + 0.25f, 1f - num3);
        vector2Array[index2 + 2] = new Vector2(x + 0.25f, (float) (1.0 - ((double) num3 + 0.125)));
        vector2Array[index2 + 3] = new Vector2(x, (float) (1.0 - ((double) num3 + 0.125)));
        Color color = new Color(this.color.r, this.color.g, this.color.b, this.color.a * point.alpha);
        colorArray[index2] = color;
        colorArray[index2 + 1] = color;
        colorArray[index2 + 2] = color;
        colorArray[index2 + 3] = color;
        int index3 = index1 * 6;
        numArray[index3] = index2;
        numArray[index3 + 1] = index2 + 1;
        numArray[index3 + 2] = index2 + 2;
        numArray[index3 + 3] = index2 + 2;
        numArray[index3 + 4] = index2 + 3;
        numArray[index3 + 5] = index2;
      }
    }
    this.trailObj.transform.position = Vector3.zero;
    this.trailObj.transform.rotation = Quaternion.identity;
    this.mesh.Clear();
    this.mesh.vertices = vector3Array;
    this.mesh.colors = colorArray;
    this.mesh.uv = vector2Array;
    this.mesh.triangles = numArray;
  }

  private void Update()
  {
    this.leaveDistance += (this.transform.position - this.lastPosition).magnitude;
    this.lastPosition = this.transform.position;
    if (!this.emit)
      this.emittingDone = true;
    if (this.emittingDone)
      this.emit = false;
    if (this.emit)
    {
      while ((double) this.leaveDistance > (double) this.vertexDistance)
      {
        this.InsertPoint(this.transform.position - this.transform.forward * this.leaveDistance);
        this.leaveDistance -= this.vertexDistance;
      }
    }
    for (int index = 0; index < this.points.Length; ++index)
    {
      SmokeTrail.Point point = this.points[index];
      if (point != null)
        point.Update();
    }
    this.trailObj.GetComponent<Renderer>().enabled = true;
    this.BuildMesh();
  }

  private void InsertPoint(Vector3 position)
  {
    this.points[this.endPoint] = new SmokeTrail.Point(this, position);
    ++this.endPoint;
    ++this.pointCnt;
    if (this.endPoint >= this.points.Length)
      this.endPoint = 0;
    if (this.pointCnt >= this.points.Length)
      return;
    ++this.pointCnt;
  }

  private class Point
  {
    private SmokeTrail parent;
    public float timeCreated;
    public Vector3 position;
    public float rotation;
    public float width;
    public int frame;
    public float alpha;
    private float beginRotation;
    private float endRotation;
    private float beginWidth;
    private float endWidth;

    public Point(SmokeTrail parent, Vector3 position)
    {
      this.parent = parent;
      this.frame = 0;
      this.alpha = 1f;
      this.timeCreated = Time.time + Random.Range(-0.1f, 0.1f);
      this.position = position + new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f));
      this.beginRotation = (float) Random.Range(-45, 45);
      this.endRotation = (float) Random.Range(-45, 45);
      this.beginWidth = (float) Random.Range(2, 3);
      this.endWidth = Random.Range(3f, 5f);
    }

    public void Update()
    {
      float t = Mathf.Clamp01((Time.time - this.timeCreated) / this.parent.lifeTime);
      this.frame = Mathf.Clamp((int) ((double) t * 32.0), 0, 31);
      this.alpha = Mathf.Lerp(1f, 0.0f, t);
      this.width = Mathf.Lerp(this.beginWidth, this.endWidth, t);
      this.rotation = Mathf.Lerp(this.beginRotation, this.endRotation, t);
    }
  }
}
