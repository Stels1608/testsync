﻿// Decompiled with JetBrains decompiler
// Type: WidgetColorScheme
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public abstract class WidgetColorScheme
{
  private readonly Dictionary<WidgetColorType, Color> colorScheme = new Dictionary<WidgetColorType, Color>();

  public WidgetColorScheme()
  {
    this.colorScheme.Add(WidgetColorType.DEFAULT, Color.magenta);
    this.colorScheme.Add(WidgetColorType.CONTAINER_STRIPE, ImageUtils.HexStringToColor("5e5e5e"));
    this.colorScheme.Add(WidgetColorType.CONTAINER_BACKGROUND, ImageUtils.HexStringToColor("0e0e0e"));
    this.colorScheme.Add(WidgetColorType.TEXT_RUNNING, ImageUtils.HexStringToColor("efefef"));
    this.colorScheme.Add(WidgetColorType.TEXT_ACTIVE, ImageUtils.HexStringToColor("0f0f0f"));
    this.colorScheme.Add(WidgetColorType.TEXT_INACTIVE, ImageUtils.HexStringToColor("3b4047"));
    this.colorScheme.Add(WidgetColorType.TEXT_HIGHLIGHT, ImageUtils.HexStringToColor("3dbf99"));
    this.colorScheme.Add(WidgetColorType.TEXT_RED, ImageUtils.HexStringToColor("b22d2d"));
    this.colorScheme.Add(WidgetColorType.TEXT_ORANGE, ImageUtils.HexStringToColor("b27436"));
    this.colorScheme.Add(WidgetColorType.TEXT_YELLOW, ImageUtils.HexStringToColor("b2b236"));
    this.colorScheme.Add(WidgetColorType.TEXT_GREEN, ImageUtils.HexStringToColor("4ab236"));
    this.colorScheme.Add(WidgetColorType.MEDAL_PLATINUM, ImageUtils.HexStringToColor("ede3cc"));
    this.colorScheme.Add(WidgetColorType.MEDAL_GOLD, ImageUtils.HexStringToColor("c7c2a2"));
    this.colorScheme.Add(WidgetColorType.MEDAL_SILVER, ImageUtils.HexStringToColor("c7c7c7"));
    this.colorScheme.Add(WidgetColorType.MEDAL_BRONZE, ImageUtils.HexStringToColor("c7aea2"));
    this.colorScheme.Add(WidgetColorType.DARK_GREEN, ImageUtils.HexStringToColor("0d2627"));
    this.colorScheme.Add(WidgetColorType.GREY, ImageUtils.HexStringToColor("231f20"));
    this.colorScheme.Add(WidgetColorType.NPC_OUTLINE, ImageUtils.HexStringToColor("3dbf99"));
    this.colorScheme.Add(WidgetColorType.DIALOG_NPC_NAME, ImageUtils.HexStringToColor("3dbf99"));
    this.colorScheme.Add(WidgetColorType.DIALOG_NPC_TEXT, ImageUtils.HexStringToColor("efefef"));
    this.colorScheme.Add(WidgetColorType.DIALOG_ANSWER_COLOR_NORMAL, ImageUtils.HexStringToColor("94ffff"));
    this.colorScheme.Add(WidgetColorType.DIALOG_ANSWER_COLOR_HOVER, ImageUtils.HexStringToColor("f3c960"));
    this.colorScheme.Add(WidgetColorType.TEXT_EVENT, ImageUtils.HexStringToColor("ffffff"));
    this.colorScheme.Add(WidgetColorType.TEXT_POSITIVE, ImageUtils.HexStringToColor("6bbd4e"));
    this.colorScheme.Add(WidgetColorType.TEXT_NEGATIVE, ImageUtils.HexStringToColor("d63636"));
    this.colorScheme.Add(WidgetColorType.TEXT_SPECIAL, ImageUtils.HexStringToColor("f5cb81"));
    this.colorScheme.Add(WidgetColorType.TEXT_UPGRADABLE, ImageUtils.HexStringToColor("a1e2ed"));
  }

  public Color Get(WidgetColorType colorType)
  {
    if (this.colorScheme.ContainsKey(colorType))
      return this.colorScheme[colorType];
    return Color.magenta;
  }

  public void Set(WidgetColorType colorType, Color color)
  {
    this.colorScheme[colorType] = color;
  }
}
