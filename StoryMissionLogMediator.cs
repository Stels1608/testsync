﻿// Decompiled with JetBrains decompiler
// Type: StoryMissionLogMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;

public class StoryMissionLogMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "StoryMissionLogMediator";
  private StoryMissionLogUi _storyMissionLog;

  private StoryMissionLogUi StoryMissionLog
  {
    get
    {
      return this._storyMissionLog ?? (this._storyMissionLog = this.CreateStoryMissionLog());
    }
  }

  private bool IsTutorialMission
  {
    get
    {
      return (GameLevel.Instance as SpaceLevel).IsTutorialMission;
    }
  }

  public StoryMissionLogMediator()
    : base("StoryMissionLogMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.StoryMissionLogAddObjective);
    this.AddMessageInterest(Message.StoryMissionLogDestroyWindow);
    this.AddMessageInterest(Message.LoadNewLevel);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.StoryMissionLogAddObjective:
        this.StoryMissionLog.SetupMissionObjectiveText((string) message.Data);
        break;
      case Message.StoryMissionLogDestroyWindow:
      case Message.LoadNewLevel:
        this.CloseStoryMissionWindow();
        break;
    }
  }

  private void CloseStoryMissionWindow()
  {
    if ((UnityEngine.Object) this._storyMissionLog == (UnityEngine.Object) null)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this._storyMissionLog.gameObject);
    this._storyMissionLog = (StoryMissionLogUi) null;
  }

  private StoryMissionLogUi CreateStoryMissionLog()
  {
    StoryMissionLogUi child = UguiTools.CreateChild<StoryMissionLogUi>("StoryMissionLogUgui", UguiTools.GetCanvas(BsgoCanvas.Hud).transform);
    child.SetupDialogButton(Tools.ParseMessage("%$bgo.story_mission_log.button_dialog_log%"), (System.Action) (() => Game.GUIManager.Find<MessageBoxManager>().ShowPictureNotifyHistory()));
    if (this.IsTutorialMission)
    {
      string message = Tools.ParseMessage("%$bgo.story_mission_log.button_skip%");
      System.Action onClick = AbandonMissionButton.SkipDialogAction;
      child.SetupSkipButton(message, onClick);
    }
    else
    {
      string message = Tools.ParseMessage("%$bgo.story_mission_log.button_abandon%");
      System.Action onClick = AbandonMissionButton.AbandonDialogAction;
      child.SetupSkipButton(message, onClick);
    }
    return child;
  }
}
