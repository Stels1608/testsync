﻿// Decompiled with JetBrains decompiler
// Type: FakeCounter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class FakeCounter
{
  private DateTime nextUpdateDue = DateTime.MinValue;
  private const int REFRESH_INTERVAL_MILLISECONDS = 100;
  private const int CHUNK_COUNT = 12;
  private int counter;
  private PreloaderState state;

  public FakeCounter(PreloaderState state)
  {
    this.state = state;
  }

  public void Update()
  {
    DateTime utcNow = DateTime.UtcNow;
    if (this.nextUpdateDue >= utcNow)
      return;
    this.nextUpdateDue = utcNow.AddMilliseconds(100.0);
    this.state.Progress = (float) (this.counter++ % 12) / 12f;
  }
}
