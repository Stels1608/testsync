﻿// Decompiled with JetBrains decompiler
// Type: LODListener
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LODListener : ModelScript
{
  private int level = int.MaxValue;
  private float sqrDistance = float.PositiveInfinity;
  private float rootScale = 1f;
  private float initialLossyScale = 1f;
  protected float[] levelLimits;
  private float[] sqrDistances;

  public static float ViewDistance { get; set; }

  public float SqrDistance
  {
    get
    {
      return this.sqrDistance;
    }
    set
    {
      if ((double) value == (double) this.sqrDistance)
        return;
      this.sqrDistance = value;
      this.OnDistanceChanged();
    }
  }

  public float Scale
  {
    set
    {
      if ((double) value == (double) this.rootScale)
        return;
      this.rootScale = value;
      this.OnScaleChanged();
    }
  }

  public int Level
  {
    get
    {
      return this.level;
    }
    private set
    {
      if (value == this.level)
        return;
      this.level = value;
      this.OnLevelChanged();
    }
  }

  protected override void Awake()
  {
    Vector3 lossyScale = this.transform.lossyScale;
    this.initialLossyScale = Mathf.Max(lossyScale.x, lossyScale.y);
    this.initialLossyScale = Mathf.Max(this.initialLossyScale, lossyScale.z);
    this.CalculateSqrDistances();
    base.Awake();
  }

  protected virtual void OnDistanceChanged()
  {
    if (this.sqrDistances == null)
      Debug.Log((object) ("Null on " + (object) this));
    int num = this.sqrDistances.Length;
    for (int index = 0; index < this.sqrDistances.Length; ++index)
    {
      if ((double) this.sqrDistance < (double) this.sqrDistances[index] * (double) Mathf.Max(0.25f, LODListener.ViewDistance))
      {
        num = index;
        break;
      }
    }
    this.Level = num;
  }

  protected virtual void OnScaleChanged()
  {
    this.CalculateSqrDistances();
  }

  private void CalculateSqrDistances()
  {
    this.sqrDistances = new float[this.levelLimits.Length];
    for (int index = 0; index < this.sqrDistances.Length; ++index)
    {
      float num = this.levelLimits[index];
      if ((double) num == 0.0)
        num = 2000f;
      this.sqrDistances[index] = num * num * this.initialLossyScale * this.initialLossyScale * this.rootScale * this.rootScale;
    }
  }

  protected virtual void OnLevelChanged()
  {
  }
}
