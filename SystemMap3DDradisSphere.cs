﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DDradisSphere
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SystemMap3DDradisSphere : MonoBehaviour
{
  private static readonly Color SPHERE_COLOR_COLONIAL = new Color(0.3372549f, 0.4078431f, 0.5254902f, 0.2196078f);
  private static readonly Color SPHERE_COLOR_CYLON = new Color(0.6117647f, 0.1176471f, 0.09411765f, 0.2196078f);
  private const float MAX_ALPHA_COLORED_PARTS = 0.51f;
  private const float MAX_ALPHA_WHITE_PART = 0.205f;
  [SerializeField]
  private Renderer[] factionColoredRenderers;
  [SerializeField]
  private Renderer whitePartRenderer;

  private Color FactionColor
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return SystemMap3DDradisSphere.SPHERE_COLOR_COLONIAL;
      return SystemMap3DDradisSphere.SPHERE_COLOR_CYLON;
    }
  }

  private void Start()
  {
    this.SetFactionColor(this.FactionColor);
  }

  private void OnEnable()
  {
    this.SetColoredPartsAlpha(0.0f);
    this.SetWhitePartAlpha(0.0f);
  }

  private void SetColoredPartsAlpha(float alpha)
  {
    Color factionColor = this.FactionColor;
    factionColor.a = alpha;
    this.SetFactionColor(factionColor);
  }

  private void SetWhitePartAlpha(float alpha)
  {
    Color white = Color.white;
    white.a = alpha;
    this.whitePartRenderer.material.SetColor("_TintColor", white);
  }

  private void SetFactionColor(Color color)
  {
    foreach (Renderer factionColoredRenderer in this.factionColoredRenderers)
      factionColoredRenderer.material.SetColor("_TintColor", color);
  }

  private void Update()
  {
    this.UpdateAlpha();
    this.ResetRotation();
  }

  private void UpdateAlpha()
  {
    if ((Object) SpaceLevel.GetLevel() == (Object) null || SpaceLevel.GetLevel().cameraSwitcher == null)
      return;
    Camera activeCamera = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera();
    float num1 = this.transform.localScale.x / (2f * Mathf.Tan((float) (3.14159274101257 / (180.0 / (double) activeCamera.fieldOfView)) / 2f));
    float max = 2f * num1;
    float min = 0.9f * num1;
    float num2 = (float) (((double) Mathf.Clamp((activeCamera.transform.position - this.transform.position).magnitude, min, max) - (double) min) / ((double) max - (double) min));
    this.SetWhitePartAlpha(Mathf.Max(0.0f, 0.205f * num2));
    this.SetColoredPartsAlpha(Mathf.Max(0.008f, 0.51f * num2));
  }

  private void ResetRotation()
  {
    this.transform.rotation = Quaternion.identity;
  }
}
