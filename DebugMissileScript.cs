﻿// Decompiled with JetBrains decompiler
// Type: DebugMissileScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugMissileScript : MonoBehaviour
{
  public SpaceObject Target;
  public Vector3 Offset;
  public float Speed;
  public GameObject ExplosionPrefab;

  private void Update()
  {
    this.transform.position += (this.Target.Position + this.Offset - this.transform.position).normalized * this.Speed * Time.deltaTime;
    if ((double) Vector3.Distance(this.Target.Position + this.Offset, this.transform.position) >= (double) this.Speed * (double) Time.deltaTime / 2.0)
      return;
    this.Explode();
  }

  private void Explode()
  {
    Object.Destroy((Object) this.gameObject);
    Object.Instantiate((Object) this.ExplosionPrefab, this.transform.position, this.transform.rotation);
  }
}
