﻿// Decompiled with JetBrains decompiler
// Type: TooltipMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class TooltipMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "TooltipMediator";
  public TooltipManager tManager;

  public TooltipMediator()
    : base("TooltipMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.ShowTooltip);
    this.AddMessageInterest(Message.HideTooltip);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.ShowTooltip:
        this.ShowTooltip((TooltipContent) message.Data);
        break;
      case Message.HideTooltip:
        this.tManager.HideTooltip();
        break;
      case Message.UiCreated:
        this.CreateTooltipHook();
        break;
      default:
        Log.Warning((object) ("Unknown Message was catched but not handled by the TooltipMediator => " + (object) message.Id));
        break;
    }
  }

  private void ShowTooltip(TooltipContent message)
  {
    if (message == null)
    {
      Debug.LogError((object) "Show Tooltip was not called with a valid Tooltip Content Object");
    }
    else
    {
      this.tManager.gameObject.SetActive(true);
      this.tManager.ShowTooltip(message.TooltipType, message.Generate());
    }
  }

  private void CreateTooltipHook()
  {
    GuiHookPoint componentInChildren = GameObject.Find("UIRoot(Clone)").GetComponentInChildren<GuiHookPoint>();
    if ((Object) componentInChildren.gameObject == (Object) null)
      Debug.Log((object) "GUI HOOK POINT is null");
    if ((Object) (Resources.Load("GUI/gui_2013/ui_elements/tooltip/TooltipContentHolder") as GameObject) == (Object) null)
      Debug.LogError((object) "CONTENT HOLDER == NULL");
    else
      this.tManager = NGUITools.AddChild(componentInChildren.gameObject, Resources.Load("GUI/gui_2013/ui_elements/tooltip/TooltipContentHolder") as GameObject).GetComponentInChildren<TooltipManager>();
  }
}
