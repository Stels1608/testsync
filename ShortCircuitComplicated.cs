﻿// Decompiled with JetBrains decompiler
// Type: ShortCircuitComplicated
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using DigitalRuby.ThunderAndLightning;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class ShortCircuitComplicated : ModelScript
{
  private const string PREFAB = "Fx/ShortCircuitComplex";
  private const string GLOW_MATERIAL = "Fx/lightning_glow";
  private float size;
  private Material glowMaterial;
  private GameObject clusterObject;
  private bool burstNotFoundErrorLogged;

  private void Start()
  {
    HullPathPoints componentInChildren = this.SpaceObject.Root.GetComponentInChildren<HullPathPoints>();
    if ((Object) componentInChildren == (Object) null || componentInChildren.paths == null)
    {
      UnityEngine.Debug.LogWarning((object) ("No HullPathPoints found, cannot display ShortCircuit effect for " + this.SpaceObject.Root.name));
      this.enabled = false;
      Object.Destroy((Object) this);
    }
    else
    {
      GameObject activeRendererObject = this.GetActiveRendererObject(this.SpaceObject);
      if ((Object) activeRendererObject == (Object) null)
      {
        UnityEngine.Debug.LogWarning((object) ("No active renderer found, cannot display ShortCircuit effect for " + this.SpaceObject.Root.name));
        this.enabled = false;
        Object.Destroy((Object) this);
      }
      else
      {
        this.clusterObject = new GameObject("ShortCircuitEffect");
        this.clusterObject.transform.parent = this.SpaceObject.Model.transform;
        this.clusterObject.transform.localPosition = Vector3.zero;
        this.clusterObject.transform.localRotation = Quaternion.identity;
        this.size = (float) (((double) this.SpaceObject.MeshBoundsRaw.size.x + (double) this.SpaceObject.MeshBoundsRaw.size.z) / 2.0);
        for (int index = 0; index < componentInChildren.paths.Length; ++index)
          this.CreateLightningPath(componentInChildren.paths[index]);
        GameObject gameObject = Object.Instantiate<GameObject>(activeRendererObject);
        gameObject.name = "Shell";
        gameObject.transform.parent = this.clusterObject.transform;
        gameObject.transform.localPosition = activeRendererObject.transform.localPosition;
        gameObject.transform.localRotation = activeRendererObject.transform.localRotation;
        gameObject.transform.localScale *= 1.01f;
        Renderer[] componentsInChildren = gameObject.GetComponentsInChildren<Renderer>();
        this.glowMaterial = Object.Instantiate<Material>(Resources.Load<Material>("Fx/lightning_glow"));
        foreach (Renderer renderer in componentsInChildren)
        {
          Material[] materials = renderer.materials;
          for (int index = 0; index < materials.Length; ++index)
            materials[index] = this.glowMaterial;
          renderer.materials = materials;
          renderer.enabled = true;
        }
        this.StartCoroutine(this.Flicker());
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator Flicker()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ShortCircuitComplicated.\u003CFlicker\u003Ec__Iterator3D()
    {
      \u003C\u003Ef__this = this
    };
  }

  private void UpdateGlowColor(Color color)
  {
    this.glowMaterial.color = color;
  }

  private void CreateLightningPath(HullPathPoints.HullPath hullPath)
  {
    GameObject gameObject1 = Object.Instantiate<GameObject>(Resources.Load<GameObject>("Fx/ShortCircuitComplex"));
    gameObject1.transform.parent = this.clusterObject.transform;
    gameObject1.transform.localPosition = Vector3.zero;
    gameObject1.transform.localRotation = Quaternion.identity;
    LightningBoltPathScript component = gameObject1.GetComponent<LightningBoltPathScript>();
    component.Camera = Camera.main;
    component.TrunkWidthRange.Minimum *= this.size;
    component.TrunkWidthRange.Maximum *= this.size;
    ReorderableList_GameObject reorderableListGameObject = new ReorderableList_GameObject();
    reorderableListGameObject.List = new List<GameObject>(hullPath.points.Length);
    for (int index = 0; index < hullPath.points.Length; ++index)
      reorderableListGameObject.List.Add(new GameObject("lightning_point_" + (object) index)
      {
        transform = {
          parent = gameObject1.transform,
          localPosition = hullPath.points[index]
        }
      });
    component.LightningPath = reorderableListGameObject;
    component.UseWorldSpace = false;
    Ship ship = this.SpaceObject as Ship;
    if (ship == null || ship.ShipCardLight == null || !(bool) ship.ShipCardLight.IsLoaded)
      return;
    string path = "Fx/BurstGlowT" + (object) ship.ShipCardLight.Tier;
    GameObject original = Resources.Load<GameObject>(path);
    if ((Object) original != (Object) null)
    {
      GameObject gameObject2 = Object.Instantiate<GameObject>(original);
      gameObject2.transform.parent = gameObject1.transform;
      component.LightningOriginParticleSystem = gameObject2.GetComponent<ParticleSystem>();
      component.LightningDestinationParticleSystem = gameObject2.GetComponent<ParticleSystem>();
    }
    else
    {
      if (this.burstNotFoundErrorLogged)
        return;
      UnityEngine.Debug.LogError((object) ("ShortCircuitComplicated(): Didn't find burstPrefab: " + path));
      this.burstNotFoundErrorLogged = true;
    }
  }

  private Renderer[] GetRenderers(SpaceObject spaceObject)
  {
    Body modelScript1 = spaceObject.GetModelScript<Body>();
    if ((Object) modelScript1 != (Object) null)
      return modelScript1.Renderers;
    MultiBody modelScript2 = spaceObject.GetModelScript<MultiBody>();
    if (!((Object) modelScript2 != (Object) null))
      return (Renderer[]) null;
    List<Renderer> rendererList = new List<Renderer>();
    foreach (RendererArray renderer in modelScript2.Renderers)
      rendererList.AddRange((IEnumerable<Renderer>) renderer.Elements);
    return rendererList.ToArray();
  }

  private GameObject GetActiveRendererObject(SpaceObject spaceObject)
  {
    Body modelScript1 = spaceObject.GetModelScript<Body>();
    GameObject gameObject1 = (GameObject) null;
    if ((Object) modelScript1 != (Object) null)
    {
      Renderer activeRenderer = modelScript1.ActiveRenderer;
      if ((Object) activeRenderer != (Object) null)
        gameObject1 = activeRenderer.gameObject;
    }
    MultiBody modelScript2 = spaceObject.GetModelScript<MultiBody>();
    if ((Object) modelScript2 != (Object) null)
    {
      Renderer[] activeRenderers = modelScript2.ActiveRenderers;
      if (activeRenderers != null)
        gameObject1 = activeRenderers[0].gameObject;
    }
    if ((Object) gameObject1 == (Object) null)
      return (GameObject) null;
    Transform transform = spaceObject.Model.transform;
    for (int index = 0; index < transform.childCount; ++index)
    {
      GameObject gameObject2 = transform.GetChild(index).gameObject;
      if (gameObject2.name.ToLower().Contains("_lod") && gameObject1.transform.IsChildOf(gameObject2.transform))
        return gameObject2;
    }
    return (GameObject) null;
  }

  private Mesh[] GetEmittingMeshes(SpaceObject spaceObject)
  {
    Body modelScript1 = spaceObject.GetModelScript<Body>();
    if ((Object) modelScript1 != (Object) null)
    {
      MeshFilter component = (modelScript1.Renderers == null || modelScript1.Renderers.Length <= 2 ? (Component) modelScript1.ActiveRenderer : (Component) modelScript1.Renderers[2]).GetComponent<MeshFilter>();
      if ((Object) component != (Object) null)
        return new Mesh[1]{ component.mesh };
    }
    MultiBody modelScript2 = spaceObject.GetModelScript<MultiBody>();
    if (!((Object) modelScript2 != (Object) null))
      return (Mesh[]) null;
    Renderer[] rendererArray = modelScript2.Renderers == null || modelScript2.Renderers.Length <= 2 ? modelScript2.ActiveRenderers : modelScript2.Renderers[2].Elements;
    List<Mesh> meshList = new List<Mesh>();
    foreach (Component component1 in rendererArray)
    {
      MeshFilter component2 = component1.GetComponent<MeshFilter>();
      if ((Object) component2 != (Object) null)
        meshList.Add(component2.mesh);
    }
    return meshList.ToArray();
  }

  protected override void OnDestroy()
  {
    this.StopAllCoroutines();
    LeanTween.cancel(this.gameObject);
    if ((Object) this.clusterObject == (Object) null)
      return;
    Object.Destroy((Object) this.clusterObject);
  }
}
