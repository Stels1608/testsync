﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorCombatInfoNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class HudIndicatorCombatInfoNgui : HudIndicatorBaseComponentNgui, ICombatInfo, IReceivesSetting, IReceivesSpaceObject
{
  private readonly Queue<HudIndicatorCombatInfoFaderNgui> displayedCombatInfos = new Queue<HudIndicatorCombatInfoFaderNgui>();
  private float lastDisplayUpdate;
  private int damage;
  private float dpsTime;
  private float timeWithoutDamage;
  private int oldDamage;
  private SpaceObject spaceObject;
  private bool isEnabledInSettings;

  public override bool RequiresAnimationNgui
  {
    get
    {
      if (this.isEnabledInSettings)
        return this.displayedCombatInfos.Count > 0;
      return false;
    }
  }

  protected override void ResetComponentStates()
  {
    this.spaceObject = (SpaceObject) null;
    this.RemoveRemainingCombatText();
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    bool flag = this.isEnabledInSettings && (this.InScannerRange || this.IsSelected) && this.InScreen;
    if ((Object) this.gameObject == (Object) null)
      Debug.LogError((object) ("Tried to access destroyed CombatInfo for SpaceObject: " + this.spaceObject.Name));
    this.gameObject.SetActive(flag);
  }

  public void OnSpaceObjectInjection(SpaceObject spaceObject)
  {
    this.spaceObject = spaceObject;
    this.RemoveRemainingCombatText();
  }

  public void Update()
  {
    this.UpdateCombatText();
  }

  public void OnCombatInfo(AbstractCombatInfo combatInfo)
  {
    if (!(combatInfo is DamageCombatInfo) || !this.InScannerRange)
      return;
    DamageCombatInfo damageCombatInfo = (DamageCombatInfo) combatInfo;
    this.damage += damageCombatInfo.Damage;
    if ((double) Time.time - (double) this.lastDisplayUpdate <= 0.449999988079071)
      return;
    this.displayedCombatInfos.Enqueue(new HudIndicatorCombatInfoFaderNgui(this.damage.ToString("n0"), this.CreateCombatInfoLabel(damageCombatInfo.CriticalHit)));
    this.damage = 0;
    this.lastDisplayUpdate = Time.time;
  }

  private UILabel CreateCombatInfoLabel(bool criticalHit)
  {
    UILabel uiLabel = NGUIToolsExtension.AddLabel(this.gameObject, Gui.Options.FontDS_EF_M, 9, UIWidget.Pivot.TopRight, string.Empty);
    uiLabel.overflowMethod = UILabel.Overflow.ResizeFreely;
    uiLabel.width = 100;
    uiLabel.color = HudIndicatorColorInfo.GetTargetColor4LegacyBracket((ISpaceEntity) this.spaceObject);
    uiLabel.cachedTransform.localPosition = (Vector3) new Vector2(-40f, -8f);
    return uiLabel;
  }

  private void ResetDps()
  {
    this.dpsTime = (float) (this.damage = this.oldDamage = 0);
    this.timeWithoutDamage = 0.0f;
  }

  private void RemoveRemainingCombatText()
  {
    this.damage = 0;
    while (this.displayedCombatInfos.Count > 0)
      NGUITools.Destroy((Object) this.displayedCombatInfos.Dequeue().ParentLabel.gameObject);
  }

  private void UpdateCombatText()
  {
    using (Queue<HudIndicatorCombatInfoFaderNgui>.Enumerator enumerator = this.displayedCombatInfos.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Update();
    }
    while (this.displayedCombatInfos.Count > 0 && this.displayedCombatInfos.Peek().Expired)
      NGUITools.Destroy((Object) this.displayedCombatInfos.Dequeue().ParentLabel.gameObject);
  }

  private void CalcuateDps()
  {
    this.dpsTime += Time.deltaTime;
    if (this.damage == this.oldDamage)
      this.timeWithoutDamage += Time.deltaTime;
    else
      this.timeWithoutDamage = 0.0f;
    if ((double) this.timeWithoutDamage > 4.0)
      this.ResetDps();
    this.oldDamage = this.damage;
  }

  public void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    if (userSetting == UserSetting.CombatText)
      this.isEnabledInSettings = (bool) data;
    this.UpdateView();
  }
}
