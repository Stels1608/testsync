﻿// Decompiled with JetBrains decompiler
// Type: SpaceLevelPreloaderScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class SpaceLevelPreloaderScript : MonoBehaviour, ILoadable
{
  public PreloadCoroutine Preload;
  public TransSceneType TransSceneType;
  private Flag isLoaded;

  public Flag IsLoaded
  {
    get
    {
      return this.isLoaded;
    }
  }

  private void Awake()
  {
    this.isLoaded = new Flag();
  }

  [DebuggerHidden]
  private IEnumerator Start()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpaceLevelPreloaderScript.\u003CStart\u003Ec__Iterator2B() { \u003C\u003Ef__this = this };
  }

  public static SpaceLevelPreloaderScript Run(PreloadCoroutine preload)
  {
    SpaceLevelPreloaderScript levelPreloaderScript = new GameObject("Preloader").AddComponent<SpaceLevelPreloaderScript>();
    levelPreloaderScript.Preload = preload;
    return levelPreloaderScript;
  }
}
