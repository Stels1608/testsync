﻿// Decompiled with JetBrains decompiler
// Type: GalaxyRcpUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class GalaxyRcpUpdate : GalaxyMapUpdate
{
  public float Rcp { get; private set; }

  public GalaxyRcpUpdate(Faction faction, float rcp)
    : base(GalaxyUpdateType.Rcp, faction, -1)
  {
    this.Rcp = rcp;
  }

  public override string ToString()
  {
    return "[GalaxyRcpUpdate] (Faction: " + (object) this.Faction + ", SectorId: " + (object) this.SectorId + ", Rcp:" + (object) this.Rcp + ")";
  }
}
