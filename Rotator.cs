﻿// Decompiled with JetBrains decompiler
// Type: Rotator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Rotator : MonoBehaviour
{
  public float MinAngularVelocity = 1f;
  public float MaxAngularVelocity = 7f;
  private float angularVelocitry;
  private Vector3 rotationAxis;

  private void Start()
  {
    this.angularVelocitry = Random.Range(this.MinAngularVelocity, this.MaxAngularVelocity);
    this.rotationAxis = new Vector3(Random.value, Random.value, Random.value);
  }

  private void Update()
  {
    this.transform.Rotate(this.rotationAxis, this.angularVelocitry * Time.deltaTime, Space.World);
  }
}
