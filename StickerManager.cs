﻿// Decompiled with JetBrains decompiler
// Type: StickerManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class StickerManager
{
  private List<DecalSpot> stickerSpots;
  private Faction faction;

  public StickerManager(IEnumerable<DecalSpot> stickerSpots, Faction faction)
  {
    this.faction = faction;
    this.stickerSpots = new List<DecalSpot>(stickerSpots);
  }

  public void ApplyTextureAll(Texture2D texture)
  {
    using (List<DecalSpot>.Enumerator enumerator = this.stickerSpots.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Texture = (Texture) texture;
    }
  }

  public void ApplyBindingAll(StickerBinding binding)
  {
    this.ApplyTextureAll(StaticCards.Instance.Stickers.GetTexture(binding.StickerID, this.faction));
  }
}
