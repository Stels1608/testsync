﻿// Decompiled with JetBrains decompiler
// Type: SettingsHudIndicators
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class SettingsHudIndicators : SettingsGeneralGroup, ILocalizeable
{
  [SerializeField]
  private UILabel optionsHeadlineLabel;
  [SerializeField]
  private GameObject content1;
  [SerializeField]
  private GameObject content2;

  private void Awake()
  {
    this.CoveredSettings.Add(UserSetting.HudIndicatorColorScheme);
    GameObject gameObject = NGUITools.AddChild(this.content1, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/hud/IndicatorColorSchemeRadioBtnGroup"));
    gameObject.name = "color_scheme_radio_group";
    this.controls.Add(UserSetting.HudIndicatorColorScheme, (OptionsElement) gameObject.GetComponent<OptionsRadioButtonGroup>());
    using (List<UserSetting>.Enumerator enumerator = new List<UserSetting>() { UserSetting.HudIndicatorBracketResizing, UserSetting.CombatText, UserSetting.HudIndicatorShowShipTierIcon, UserSetting.HudIndicatorShowTargetNames, UserSetting.HudIndicatorShowShipNames, UserSetting.HudIndicatorShowWingNames, UserSetting.HudIndicatorShowTitles, UserSetting.HudIndicatorShowMissionArrow }.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        this.CoveredSettings.Add(current);
        OptionButtonElement buttonElement = NguiWidgetFactory.Options.CreateButtonElement(this.content1);
        this.controls.Add(current, (OptionsElement) buttonElement);
      }
    }
    this.CoveredSettings.Add(UserSetting.HudIndicatorMinimizeDistance);
    OptionsSliderElement sliderElement1 = NguiWidgetFactory.Options.CreateSliderElement(this.content1);
    sliderElement1.ShowPercentage = true;
    sliderElement1.gameObject.transform.localPosition = new Vector3(0.0f, 0.0f);
    this.controls.Add(UserSetting.HudIndicatorMinimizeDistance, (OptionsElement) sliderElement1);
    this.CoveredSettings.Add(UserSetting.HudIndicatorDescriptionDisplayDistance);
    OptionsSliderElement sliderElement2 = NguiWidgetFactory.Options.CreateSliderElement(this.content1);
    sliderElement2.ShowPercentage = true;
    sliderElement2.gameObject.transform.localPosition = new Vector3(0.0f, 0.0f);
    this.controls.Add(UserSetting.HudIndicatorDescriptionDisplayDistance, (OptionsElement) sliderElement2);
    this.CoveredSettings.Add(UserSetting.HudIndicatorTextSize);
    OptionsSliderElement sliderElement3 = NguiWidgetFactory.Options.CreateSliderElement(this.content1);
    sliderElement3.ShowPercentage = false;
    sliderElement3.SetLimits01(8f, 13f);
    sliderElement3.Steps = 6;
    sliderElement3.Multiplier = 1;
    sliderElement3.gameObject.transform.localPosition = new Vector3(0.0f, 0.0f);
    this.controls.Add(UserSetting.HudIndicatorTextSize, (OptionsElement) sliderElement3);
    this.content1.GetComponent<UITable>().Reposition();
    using (List<UserSetting>.Enumerator enumerator = new List<UserSetting>() { UserSetting.HudIndicatorSelectionCrosshair, UserSetting.HudIndicatorHealthBar, UserSetting.ShowEnemyIndication, UserSetting.ShowFriendIndication }.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        this.CoveredSettings.Add(current);
        OptionButtonElement buttonElement = NguiWidgetFactory.Options.CreateButtonElement(this.content2);
        this.controls.Add(current, (OptionsElement) buttonElement);
      }
    }
    this.content2.GetComponent<UITable>().Reposition();
  }

  private void Start()
  {
    this.ReloadLanguageData();
  }

  public void ReloadLanguageData()
  {
    this.optionsHeadlineLabel.text = Tools.ParseMessage("%$bgo.ui.options.hud.indicators%");
  }
}
