﻿// Decompiled with JetBrains decompiler
// Type: GUIImage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIImage : CustomGUIElement
{
  protected int padding;
  public Texture2D texture;

  public int Padding
  {
    get
    {
      return this.padding;
    }
    set
    {
      this.padding = value;
    }
  }

  public GUIImage(Texture2D texture, SmartRect parent)
  {
    this.rect = new SmartRect(new Rect(0.0f, 0.0f, (float) texture.width, (float) texture.height), float2.zero, parent);
    this.texture = texture;
  }

  public override void Draw()
  {
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    Graphics.DrawTexture(this.rect.AbsRect, (Texture) this.texture, this.padding, this.padding, this.padding, this.padding);
  }
}
