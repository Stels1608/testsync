﻿// Decompiled with JetBrains decompiler
// Type: GUIShopSubtypeFilter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class GUIShopSubtypeFilter : GUIPanel
{
  private float2 nextElementPosition = new float2(0.0f, 22f);
  protected readonly GUIShopTypeFilter filterMenu;
  private readonly GUIImageNew nineSlice;

  public GUILabelNew HeaderLabel { get; private set; }

  public GUIShopSubtypeFilter(string categoryName, GUIShopTypeFilter filterMenu, bool singleEntry = false)
  {
    this.filterMenu = filterMenu;
    this.Size = new float2(200f, 40f);
    this.nineSlice = new GUIImageNew(ResourceLoader.Load<Texture2D>("GUI/Gui2/background_small_dark"));
    this.nineSlice.Size = this.Size;
    this.nineSlice.IsRendered = true;
    this.nineSlice.Padding = 8;
    this.MouseTransparent = false;
    this.HandleMouseInput = true;
    this.AddPanel((GUIPanel) this.nineSlice);
    this.HeaderLabel = new GUILabelNew(categoryName);
    this.HeaderLabel.Font = Gui.Options.FontBGM_BT;
    this.HeaderLabel.FontSize = 15;
    this.HeaderLabel.Alignment = TextAnchor.MiddleCenter;
    this.HeaderLabel.PositionY = -8f;
    this.AddPanel((GUIPanel) this.HeaderLabel);
    if (!singleEntry)
    {
      GUIImageNew guiImageNew = new GUIImageNew(ResourceLoader.Load<Texture2D>("GUI/Inventory/line"));
      guiImageNew.Width = this.Width + 4f;
      guiImageNew.PositionX = 2f;
      guiImageNew.PositionY = 10f;
      this.AddPanel((GUIPanel) guiImageNew);
    }
    else
    {
      GUIImageNew guiImageNew = this.nineSlice;
      float num1 = 32f;
      this.Height = num1;
      double num2 = (double) num1;
      guiImageNew.Height = (float) num2;
      this.HeaderLabel.PositionY = -4f;
    }
  }

  public void AddButton(string buttonText, ShopCategory category, ShopItemType itemType, params string[] sortingKeys)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    AnonymousDelegate buttonHandler = new AnonymousDelegate(new GUIShopSubtypeFilter.\u003CAddButton\u003Ec__AnonStoreyC1() { category = category, itemType = itemType, sortingKeys = sortingKeys, \u003C\u003Ef__this = this }.\u003C\u003Em__1B5);
    this.AddButton(buttonText, buttonHandler);
  }

  public void AddButton(string buttonText, ShopCategory shopCategory, Predicate<ShipItem> filter)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.AddButton(buttonText, new AnonymousDelegate(new GUIShopSubtypeFilter.\u003CAddButton\u003Ec__AnonStoreyC2()
    {
      shopCategory = shopCategory,
      filter = filter,
      \u003C\u003Ef__this = this
    }.\u003C\u003Em__1B6));
  }

  private void AddButton(string buttonText, AnonymousDelegate buttonHandler)
  {
    GUIButtonNew guiButtonNew = new GUIButtonNew();
    guiButtonNew.Text = buttonText;
    guiButtonNew.NormalTexture = (Texture2D) null;
    guiButtonNew.OverTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/StarterPack/FilterOverlay");
    guiButtonNew.PressedTexture = guiButtonNew.OverTexture;
    guiButtonNew.TextLabel.Font = Gui.Options.FontBGM_BT;
    guiButtonNew.TextLabel.FontSize = 14;
    guiButtonNew.Size = new float2(200f, 20f);
    guiButtonNew.TextLabel.AutoSize = true;
    guiButtonNew.Position = this.nextElementPosition;
    guiButtonNew.Handler = buttonHandler;
    this.PrepareNextPosition(guiButtonNew.Height);
    this.AddPanel((GUIPanel) guiButtonNew);
  }

  private void ApplyFilter(ShopCategory category, Predicate<ShipItem> filter)
  {
    this.filterMenu.SelectCategory(category, ShopItemType.None);
    this.filterMenu.SelectedFilter = filter;
    this.IsRendered = false;
  }

  private void ApplyFilter(ShopCategory category, ShopItemType itemType, params string[] sortingKeys)
  {
    this.filterMenu.SelectCategory(category, itemType);
    this.filterMenu.SelectedFilter = this.GetFilter(category, itemType, sortingKeys);
    this.IsRendered = false;
  }

  private Predicate<ShipItem> GetFilter(ShopCategory category, ShopItemType itemType, params string[] sortingKeys)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return new Predicate<ShipItem>(new GUIShopSubtypeFilter.\u003CGetFilter\u003Ec__AnonStoreyC3() { category = category, itemType = itemType, sortingKeys = sortingKeys }.\u003C\u003Em__1B7);
  }

  private void PrepareNextPosition(float height)
  {
    this.nextElementPosition.y += height;
    this.Height += (float) (double) height;
    this.nineSlice.PositionY += (float) ((double) height / 2.0);
    this.nineSlice.Height += (float) (double) height;
  }
}
