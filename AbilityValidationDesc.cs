﻿// Decompiled with JetBrains decompiler
// Type: AbilityValidationDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class AbilityValidationDesc : IProtocolRead
{
  public GameProtocol.SlotCategory SlotCategory;
  public ushort SlotID;
  public float Cooldown;
  public float MaxRange;
  public float MinRange;
  public float Angle;
  public float PPCost;

  void IProtocolRead.Read(BgoProtocolReader r)
  {
    this.SlotCategory = (GameProtocol.SlotCategory) r.ReadByte();
    this.SlotID = r.ReadUInt16();
    this.Cooldown = r.ReadSingle();
    this.MaxRange = r.ReadSingle();
    this.MinRange = r.ReadSingle();
    this.Angle = r.ReadSingle();
    this.PPCost = r.ReadSingle();
  }
}
