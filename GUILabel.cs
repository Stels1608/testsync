﻿// Decompiled with JetBrains decompiler
// Type: GUILabel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUILabel : CustomGUIElement
{
  private string text = string.Empty;
  public GUIStyle style;
  private Color normalColor;
  private Color overColor;

  public GUILabel(string text, SmartRect parent)
    : this(text, Color.white, Color.white, (Font) null, parent)
  {
  }

  public GUILabel(string text, Color normalColor, Color overColor, Font font, SmartRect parent)
  {
    this.style = new GUIStyle();
    this.style.font = font;
    this.style.normal.textColor = normalColor;
    this.rect = new SmartRect(new Rect(), float2.zero, parent);
    this.SetText(text);
    this.normalColor = normalColor;
    this.overColor = overColor;
  }

  public void SetWordWrap(bool newValue)
  {
    this.style.wordWrap = newValue;
  }

  public void SetText(string newValue)
  {
    newValue = Tools.ParseMessage(newValue);
    if (!this.style.wordWrap)
    {
      Size size = TextUtility.CalcTextSize(newValue, this.style.font, this.style.fontSize);
      this.rect.Width = (float) size.width;
      this.rect.Height = (float) size.height;
    }
    this.rect.RecalculateAbsCoords();
    this.text = newValue;
  }

  public void SetTextNoParse(string newValue)
  {
    if (!this.style.wordWrap)
    {
      Size size = TextUtility.CalcTextSize(newValue, this.style.font, this.style.fontSize);
      this.rect.Width = (float) size.width;
      this.rect.Height = (float) size.height;
    }
    this.rect.RecalculateAbsCoords();
    this.text = newValue;
  }

  public string GetText()
  {
    return this.text;
  }

  public void SetAligment(TextAnchor value)
  {
    this.style.alignment = value;
  }

  public override void Draw()
  {
    GUI.Label(this.rect.AbsRect, this.text, this.style);
  }

  public override void SetIsMouseOver(bool newValue)
  {
    base.SetIsMouseOver(newValue);
    this.style.normal.textColor = !this.isOver ? this.normalColor : this.overColor;
  }

  public void SetNormalColor(Color newValue)
  {
    this.normalColor = newValue;
    this.style.normal.textColor = !this.isOver ? this.normalColor : this.overColor;
  }

  public void SetOverColor(Color newValue)
  {
    this.overColor = newValue;
    this.style.normal.textColor = !this.isOver ? this.normalColor : this.overColor;
  }

  public Color GetNormalColor()
  {
    return this.normalColor;
  }

  public Color GetOverColor()
  {
    return this.overColor;
  }
}
