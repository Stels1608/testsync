﻿// Decompiled with JetBrains decompiler
// Type: Party
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

public class Party
{
  private readonly List<Player> members = new List<Player>();
  private InviteTimeoutManager inviteTimeout = new InviteTimeoutManager();
  public readonly Dictionary<uint, List<uint>> Anchored = new Dictionary<uint, List<uint>>();
  public readonly List<uint> NonAnchored = new List<uint>();
  public const uint NO_PARTY = 0;
  private const float INVITE_RANGE_LIMIT = 1000f;
  public bool inGroupJump;
  public Player Leader;
  public uint PartyId;

  public bool HasParty
  {
    get
    {
      return this.members.Count > 0;
    }
  }

  public bool IsLeader
  {
    get
    {
      if (this.Leader == null)
        return false;
      return (int) Game.Me.ServerID == (int) this.Leader.ServerID;
    }
  }

  public bool ArenaSuitable
  {
    get
    {
      if (!this.HasParty || !this.IsLeader)
        return false;
      List<byte> byteList = this.members.ConvertAll<byte>((Converter<Player, byte>) (p =>
      {
        if (p.ShipCards == null)
          return 0;
        return p.ShipCards[0].Tier;
      }));
      byteList.Add(Game.Me.ActiveShip.Card.Tier);
      byteList.Sort();
      return byteList.Count == 3 && (int) byteList[0] == 1 && ((int) byteList[1] == 2 && (int) byteList[2] == 3);
    }
  }

  public ReadOnlyCollection<Player> Members
  {
    get
    {
      return this.members.AsReadOnly();
    }
  }

  public bool IsMember(Player player)
  {
    return this.members.Contains(player);
  }

  public bool IsIdMemberOrMe(uint playerId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Party.\u003CIsIdMemberOrMe\u003Ec__AnonStorey10A meCAnonStorey10A = new Party.\u003CIsIdMemberOrMe\u003Ec__AnonStorey10A();
    // ISSUE: reference to a compiler-generated field
    meCAnonStorey10A.playerId = playerId;
    // ISSUE: reference to a compiler-generated field
    if ((int) meCAnonStorey10A.playerId != (int) Game.Me.ServerID)
    {
      // ISSUE: reference to a compiler-generated method
      return this.members.FirstOrDefault<Player>(new Func<Player, bool>(meCAnonStorey10A.\u003C\u003Em__261)) != null;
    }
    return true;
  }

  public List<Player> GetGroupJumpParty()
  {
    if (!this.IsLeader)
      return (List<Player>) null;
    List<Player> playerList = new List<Player>();
    foreach (Player member in this.Members)
    {
      if (member.Online && member.Location == GameLocation.Space && member.IsSameSector)
        playerList.Add(member);
    }
    return playerList;
  }

  public void Update()
  {
    this.inviteTimeout.Update();
  }

  public void Invite(Player player)
  {
    if (player == null)
      return;
    this.inviteTimeout.DeleteTimeout(player);
    this.inviteTimeout.AddTimeout(player);
    CommunityProtocol.GetProtocol().RequestInviteToParty(player.ServerID);
  }

  public void Dismiss(Player player)
  {
    CommunityProtocol.GetProtocol().RequestDismissFromParty(player.ServerID);
  }

  public void Leave()
  {
    if (Game.Me.Party.inGroupJump)
      JumpActionsHandler.CancelJumpSequence();
    CommunityProtocol.GetProtocol().RequestLeaveParty();
  }

  public void AppointLeader(Player newLeader)
  {
    CommunityProtocol.GetProtocol().PartyAppointLeader(newLeader.ServerID);
  }

  public void SetParty(uint leaderId, uint[] partyIDs, HashSet<uint> carrierIDs)
  {
    this.ClearParty();
    SpaceLevel level = SpaceLevel.GetLevel();
    if ((UnityEngine.Object) level != (UnityEngine.Object) null && level.GetActualPlayerShip() != null && level.GetActualPlayerShip().ShipCardLight.ShipRoleDeprecated == ShipRoleDeprecated.Carrier)
      GuiDockUndock.HideLaunchStrikes();
    if ((int) leaderId == 0)
      return;
    this.Leader = Game.Players.GetPlayer(leaderId);
    this.members.Add(this.Leader);
    this.members.AddRange((IEnumerable<Player>) Game.Players.GetPlayers((IEnumerable<uint>) partyIDs));
    this.members.Remove(Game.Players.GetPlayer(Game.Me.ServerID));
    if (carrierIDs.Contains(Game.Me.ServerID))
    {
      if (!this.Anchored.ContainsKey(Game.Me.ServerID))
        this.Anchored.Add(Game.Me.ServerID, new List<uint>());
    }
    else if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null && SpaceLevel.GetLevel().GetActualPlayerShip() != null && (SpaceLevel.GetLevel().GetActualPlayerShip().ShipCardLight.ShipRoleDeprecated == ShipRoleDeprecated.Carrier && !this.Anchored.ContainsKey(Game.Me.ServerID)))
      this.Anchored.Add(Game.Me.ServerID, new List<uint>());
    using (List<Player>.Enumerator enumerator = this.members.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Player current = enumerator.Current;
        FacadeFactory.GetInstance().SendMessage(Message.PlayerChangedPartyStatus, (object) current);
        current.Request(Player.InfoType.PartyMemberRequest);
        current.Subscribe(Player.InfoType.PartyMemberSubscription);
        SubscribeProtocol.GetProtocol().SubscribeStats(current.ServerID);
        if (carrierIDs.Contains(current.ServerID))
        {
          if (!this.Anchored.ContainsKey(current.ServerID))
            this.Anchored.Add(current.ServerID, new List<uint>());
        }
        else if (!this.NonAnchored.Contains(current.ServerID))
          this.NonAnchored.Add(current.ServerID);
      }
    }
  }

  private void ClearParty()
  {
    this.Leader = (Player) null;
    using (List<Player>.Enumerator enumerator = this.members.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Player current = enumerator.Current;
        if (current.IsSameSector)
        {
          Player.InfoType types = this.GetUnsubscriptionFlags(current) & ~Player.InfoType.SpaceSubscription;
          current.Unsubscribe(types);
        }
        else
        {
          current.Unsubscribe(this.GetUnsubscriptionFlags(current));
          SubscribeProtocol.GetProtocol().UnsubscribeStats(current.ServerID);
        }
      }
    }
    while (this.members.Count > 0)
    {
      Player player = this.members[0];
      this.members.RemoveAt(0);
      FacadeFactory.GetInstance().SendMessage(Message.PlayerChangedPartyStatus, (object) player);
    }
    this.Anchored.Clear();
    this.NonAnchored.Clear();
  }

  private Player.InfoType GetUnsubscriptionFlags(Player p)
  {
    return Game.Me.Friends.IsFriend(p) ? Player.InfoType.Ships : Player.InfoType.PartyMemberSubscription;
  }
}
