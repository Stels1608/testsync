﻿// Decompiled with JetBrains decompiler
// Type: ComponentAce.Compression.Libs.zlib.zlibConst
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace ComponentAce.Compression.Libs.zlib
{
  public sealed class zlibConst
  {
    private const string version_Renamed_Field = "1.0.2";
    public const int Z_NO_COMPRESSION = 0;
    public const int Z_BEST_SPEED = 1;
    public const int Z_BEST_COMPRESSION = 9;
    public const int Z_DEFAULT_COMPRESSION = -1;
    public const int Z_FILTERED = 1;
    public const int Z_HUFFMAN_ONLY = 2;
    public const int Z_DEFAULT_STRATEGY = 0;
    public const int Z_NO_FLUSH = 0;
    public const int Z_PARTIAL_FLUSH = 1;
    public const int Z_SYNC_FLUSH = 2;
    public const int Z_FULL_FLUSH = 3;
    public const int Z_FINISH = 4;
    public const int Z_OK = 0;
    public const int Z_STREAM_END = 1;
    public const int Z_NEED_DICT = 2;
    public const int Z_ERRNO = -1;
    public const int Z_STREAM_ERROR = -2;
    public const int Z_DATA_ERROR = -3;
    public const int Z_MEM_ERROR = -4;
    public const int Z_BUF_ERROR = -5;
    public const int Z_VERSION_ERROR = -6;

    public static string version()
    {
      return "1.0.2";
    }
  }
}
