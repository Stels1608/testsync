﻿// Decompiled with JetBrains decompiler
// Type: ComponentAce.Compression.Libs.zlib.Inflate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace ComponentAce.Compression.Libs.zlib
{
  internal sealed class Inflate
  {
    private static byte[] mark = new byte[4]{ (byte) 0, (byte) 0, (byte) SupportClass.Identity((long) byte.MaxValue), (byte) SupportClass.Identity((long) byte.MaxValue) };
    internal long[] was = new long[1];
    private const int MAX_WBITS = 15;
    private const int PRESET_DICT = 32;
    internal const int Z_NO_FLUSH = 0;
    internal const int Z_PARTIAL_FLUSH = 1;
    internal const int Z_SYNC_FLUSH = 2;
    internal const int Z_FULL_FLUSH = 3;
    internal const int Z_FINISH = 4;
    private const int Z_DEFLATED = 8;
    private const int Z_OK = 0;
    private const int Z_STREAM_END = 1;
    private const int Z_NEED_DICT = 2;
    private const int Z_ERRNO = -1;
    private const int Z_STREAM_ERROR = -2;
    private const int Z_DATA_ERROR = -3;
    private const int Z_MEM_ERROR = -4;
    private const int Z_BUF_ERROR = -5;
    private const int Z_VERSION_ERROR = -6;
    private const int METHOD = 0;
    private const int FLAG = 1;
    private const int DICT4 = 2;
    private const int DICT3 = 3;
    private const int DICT2 = 4;
    private const int DICT1 = 5;
    private const int DICT0 = 6;
    private const int BLOCKS = 7;
    private const int CHECK4 = 8;
    private const int CHECK3 = 9;
    private const int CHECK2 = 10;
    private const int CHECK1 = 11;
    private const int DONE = 12;
    private const int BAD = 13;
    internal int mode;
    internal int method;
    internal long need;
    internal int marker;
    internal int nowrap;
    internal int wbits;
    internal InfBlocks blocks;

    internal int inflateReset(ZStream z)
    {
      if (z == null || z.istate == null)
        return -2;
      z.total_in = z.total_out = 0L;
      z.msg = (string) null;
      z.istate.mode = z.istate.nowrap == 0 ? 0 : 7;
      z.istate.blocks.reset(z, (long[]) null);
      return 0;
    }

    internal int inflateEnd(ZStream z)
    {
      if (this.blocks != null)
        this.blocks.free(z);
      this.blocks = (InfBlocks) null;
      return 0;
    }

    internal int inflateInit(ZStream z, int w)
    {
      z.msg = (string) null;
      this.blocks = (InfBlocks) null;
      this.nowrap = 0;
      if (w < 0)
      {
        w = -w;
        this.nowrap = 1;
      }
      if (w < 8 || w > 15)
      {
        this.inflateEnd(z);
        return -2;
      }
      this.wbits = w;
      z.istate.blocks = new InfBlocks(z, z.istate.nowrap == 0 ? (object) this : (object) (Inflate) null, 1 << w);
      this.inflateReset(z);
      return 0;
    }

    internal int inflate(ZStream z, int f)
    {
      if (z == null || z.istate == null || z.next_in == null)
        return -2;
      f = f != 4 ? 0 : -5;
      int r = -5;
      while (true)
      {
        switch (z.istate.mode)
        {
          case 0:
            if (z.avail_in != 0)
            {
              r = f;
              --z.avail_in;
              ++z.total_in;
              Inflate inflate = z.istate;
              byte[] numArray = z.next_in;
              int index = z.next_in_index++;
              int num1;
              int num2 = num1 = (int) numArray[index];
              inflate.method = num1;
              if ((num2 & 15) != 8)
              {
                z.istate.mode = 13;
                z.msg = "unknown compression method";
                z.istate.marker = 5;
                continue;
              }
              if ((z.istate.method >> 4) + 8 > z.istate.wbits)
              {
                z.istate.mode = 13;
                z.msg = "invalid window size";
                z.istate.marker = 5;
                continue;
              }
              z.istate.mode = 1;
              goto case 1;
            }
            else
              goto label_5;
          case 1:
            if (z.avail_in != 0)
            {
              r = f;
              --z.avail_in;
              ++z.total_in;
              int num = (int) z.next_in[z.next_in_index++] & (int) byte.MaxValue;
              if (((z.istate.method << 8) + num) % 31 != 0)
              {
                z.istate.mode = 13;
                z.msg = "incorrect header check";
                z.istate.marker = 5;
                continue;
              }
              if ((num & 32) == 0)
              {
                z.istate.mode = 7;
                continue;
              }
              goto label_17;
            }
            else
              goto label_12;
          case 2:
            goto label_18;
          case 3:
            goto label_21;
          case 4:
            goto label_24;
          case 5:
            goto label_27;
          case 6:
            goto label_30;
          case 7:
            r = z.istate.blocks.proc(z, r);
            if (r == -3)
            {
              z.istate.mode = 13;
              z.istate.marker = 0;
              continue;
            }
            if (r == 0)
              r = f;
            if (r == 1)
            {
              r = f;
              z.istate.blocks.reset(z, z.istate.was);
              if (z.istate.nowrap != 0)
              {
                z.istate.mode = 12;
                continue;
              }
              z.istate.mode = 8;
              goto case 8;
            }
            else
              goto label_36;
          case 8:
            if (z.avail_in != 0)
            {
              r = f;
              --z.avail_in;
              ++z.total_in;
              z.istate.need = (long) (((int) z.next_in[z.next_in_index++] & (int) byte.MaxValue) << 24 & -16777216);
              z.istate.mode = 9;
              goto case 9;
            }
            else
              goto label_41;
          case 9:
            if (z.avail_in != 0)
            {
              r = f;
              --z.avail_in;
              ++z.total_in;
              z.istate.need += (long) (((int) z.next_in[z.next_in_index++] & (int) byte.MaxValue) << 16) & 16711680L;
              z.istate.mode = 10;
              goto case 10;
            }
            else
              goto label_44;
          case 10:
            if (z.avail_in != 0)
            {
              r = f;
              --z.avail_in;
              ++z.total_in;
              z.istate.need += (long) (((int) z.next_in[z.next_in_index++] & (int) byte.MaxValue) << 8) & 65280L;
              z.istate.mode = 11;
              goto case 11;
            }
            else
              goto label_47;
          case 11:
            if (z.avail_in != 0)
            {
              r = f;
              --z.avail_in;
              ++z.total_in;
              z.istate.need += (long) z.next_in[z.next_in_index++] & (long) byte.MaxValue;
              if ((int) z.istate.was[0] != (int) z.istate.need)
              {
                z.istate.mode = 13;
                z.msg = "incorrect data check";
                z.istate.marker = 5;
                continue;
              }
              goto label_53;
            }
            else
              goto label_50;
          case 12:
            goto label_54;
          case 13:
            goto label_55;
          default:
            goto label_56;
        }
      }
label_5:
      return r;
label_12:
      return r;
label_17:
      z.istate.mode = 2;
label_18:
      if (z.avail_in == 0)
        return r;
      r = f;
      --z.avail_in;
      ++z.total_in;
      z.istate.need = (long) (((int) z.next_in[z.next_in_index++] & (int) byte.MaxValue) << 24 & -16777216);
      z.istate.mode = 3;
label_21:
      if (z.avail_in == 0)
        return r;
      r = f;
      --z.avail_in;
      ++z.total_in;
      z.istate.need += (long) (((int) z.next_in[z.next_in_index++] & (int) byte.MaxValue) << 16) & 16711680L;
      z.istate.mode = 4;
label_24:
      if (z.avail_in == 0)
        return r;
      r = f;
      --z.avail_in;
      ++z.total_in;
      z.istate.need += (long) (((int) z.next_in[z.next_in_index++] & (int) byte.MaxValue) << 8) & 65280L;
      z.istate.mode = 5;
label_27:
      if (z.avail_in == 0)
        return r;
      --z.avail_in;
      ++z.total_in;
      z.istate.need += (long) z.next_in[z.next_in_index++] & (long) byte.MaxValue;
      z.adler = z.istate.need;
      z.istate.mode = 6;
      return 2;
label_30:
      z.istate.mode = 13;
      z.msg = "need dictionary";
      z.istate.marker = 0;
      return -2;
label_36:
      return r;
label_41:
      return r;
label_44:
      return r;
label_47:
      return r;
label_50:
      return r;
label_53:
      z.istate.mode = 12;
label_54:
      return 1;
label_55:
      return -3;
label_56:
      return -2;
    }

    internal int inflateSetDictionary(ZStream z, byte[] dictionary, int dictLength)
    {
      int start = 0;
      int n = dictLength;
      if (z == null || z.istate == null || z.istate.mode != 6)
        return -2;
      if (z._adler.adler32(1L, dictionary, 0, dictLength) != z.adler)
        return -3;
      z.adler = z._adler.adler32(0L, (byte[]) null, 0, 0);
      if (n >= 1 << z.istate.wbits)
      {
        n = (1 << z.istate.wbits) - 1;
        start = dictLength - n;
      }
      z.istate.blocks.set_dictionary(dictionary, start, n);
      z.istate.mode = 7;
      return 0;
    }

    internal int inflateSync(ZStream z)
    {
      if (z == null || z.istate == null)
        return -2;
      if (z.istate.mode != 13)
      {
        z.istate.mode = 13;
        z.istate.marker = 0;
      }
      int num1;
      if ((num1 = z.avail_in) == 0)
        return -5;
      int index1 = z.next_in_index;
      int index2;
      for (index2 = z.istate.marker; num1 != 0 && index2 < 4; --num1)
      {
        if ((int) z.next_in[index1] == (int) Inflate.mark[index2])
          ++index2;
        else
          index2 = (int) z.next_in[index1] == 0 ? 4 - index2 : 0;
        ++index1;
      }
      z.total_in += (long) (index1 - z.next_in_index);
      z.next_in_index = index1;
      z.avail_in = num1;
      z.istate.marker = index2;
      if (index2 != 4)
        return -3;
      long num2 = z.total_in;
      long num3 = z.total_out;
      this.inflateReset(z);
      z.total_in = num2;
      z.total_out = num3;
      z.istate.mode = 7;
      return 0;
    }

    internal int inflateSyncPoint(ZStream z)
    {
      if (z == null || z.istate == null || z.istate.blocks == null)
        return -2;
      return z.istate.blocks.sync_point();
    }
  }
}
