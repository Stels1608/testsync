﻿// Decompiled with JetBrains decompiler
// Type: ComponentAce.Compression.Libs.zlib.SupportClass
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Text;

namespace ComponentAce.Compression.Libs.zlib
{
  public class SupportClass
  {
    public static long Identity(long literal)
    {
      return literal;
    }

    public static ulong Identity(ulong literal)
    {
      return literal;
    }

    public static float Identity(float literal)
    {
      return literal;
    }

    public static double Identity(double literal)
    {
      return literal;
    }

    public static int URShift(int number, int bits)
    {
      if (number >= 0)
        return number >> bits;
      return (number >> bits) + (2 << ~bits);
    }

    public static int URShift(int number, long bits)
    {
      return SupportClass.URShift(number, (int) bits);
    }

    public static long URShift(long number, int bits)
    {
      if (number >= 0L)
        return number >> bits;
      return (number >> bits) + (2L << ~bits);
    }

    public static long URShift(long number, long bits)
    {
      return SupportClass.URShift(number, (int) bits);
    }

    public static int ReadInput(Stream sourceStream, byte[] target, int start, int count)
    {
      if (target.Length == 0)
        return 0;
      byte[] buffer = new byte[target.Length];
      int num = sourceStream.Read(buffer, start, count);
      if (num == 0)
        return -1;
      for (int index = start; index < start + num; ++index)
        target[index] = buffer[index];
      return num;
    }

    public static int ReadInput(TextReader sourceTextReader, byte[] target, int start, int count)
    {
      if (target.Length == 0)
        return 0;
      char[] buffer = new char[target.Length];
      int num = sourceTextReader.Read(buffer, start, count);
      if (num == 0)
        return -1;
      for (int index = start; index < start + num; ++index)
        target[index] = (byte) buffer[index];
      return num;
    }

    public static byte[] ToByteArray(string sourceString)
    {
      return Encoding.UTF8.GetBytes(sourceString);
    }

    public static char[] ToCharArray(byte[] byteArray)
    {
      return Encoding.UTF8.GetChars(byteArray);
    }
  }
}
