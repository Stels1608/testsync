﻿// Decompiled with JetBrains decompiler
// Type: ComponentAce.Compression.Libs.zlib.InfCodes
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

namespace ComponentAce.Compression.Libs.zlib
{
  internal sealed class InfCodes
  {
    private static readonly int[] inflate_mask = new int[17]{ 0, 1, 3, 7, 15, 31, 63, (int) sbyte.MaxValue, (int) byte.MaxValue, 511, 1023, 2047, 4095, 8191, 16383, (int) short.MaxValue, (int) ushort.MaxValue };
    private const int Z_OK = 0;
    private const int Z_STREAM_END = 1;
    private const int Z_NEED_DICT = 2;
    private const int Z_ERRNO = -1;
    private const int Z_STREAM_ERROR = -2;
    private const int Z_DATA_ERROR = -3;
    private const int Z_MEM_ERROR = -4;
    private const int Z_BUF_ERROR = -5;
    private const int Z_VERSION_ERROR = -6;
    private const int START = 0;
    private const int LEN = 1;
    private const int LENEXT = 2;
    private const int DIST = 3;
    private const int DISTEXT = 4;
    private const int COPY = 5;
    private const int LIT = 6;
    private const int WASH = 7;
    private const int END = 8;
    private const int BADCODE = 9;
    internal int mode;
    internal int len;
    internal int[] tree;
    internal int tree_index;
    internal int need;
    internal int lit;
    internal int get_Renamed;
    internal int dist;
    internal byte lbits;
    internal byte dbits;
    internal int[] ltree;
    internal int ltree_index;
    internal int[] dtree;
    internal int dtree_index;

    internal InfCodes(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index, ZStream z)
    {
      this.mode = 0;
      this.lbits = (byte) bl;
      this.dbits = (byte) bd;
      this.ltree = tl;
      this.ltree_index = tl_index;
      this.dtree = td;
      this.dtree_index = td_index;
    }

    internal InfCodes(int bl, int bd, int[] tl, int[] td, ZStream z)
    {
      this.mode = 0;
      this.lbits = (byte) bl;
      this.dbits = (byte) bd;
      this.ltree = tl;
      this.ltree_index = 0;
      this.dtree = td;
      this.dtree_index = 0;
    }

    internal int proc(InfBlocks s, ZStream z, int r)
    {
      int num1 = z.next_in_index;
      int num2 = z.avail_in;
      int number = s.bitb;
      int num3 = s.bitk;
      int num4 = s.write;
      int num5 = num4 >= s.read ? s.end - num4 : s.read - num4 - 1;
      while (true)
      {
        switch (this.mode)
        {
          case 0:
            if (num5 >= 258 && num2 >= 10)
            {
              s.bitb = number;
              s.bitk = num3;
              z.avail_in = num2;
              z.total_in += (long) (num1 - z.next_in_index);
              z.next_in_index = num1;
              s.write = num4;
              r = this.inflate_fast((int) this.lbits, (int) this.dbits, this.ltree, this.ltree_index, this.dtree, this.dtree_index, s, z);
              num1 = z.next_in_index;
              num2 = z.avail_in;
              number = s.bitb;
              num3 = s.bitk;
              num4 = s.write;
              num5 = num4 >= s.read ? s.end - num4 : s.read - num4 - 1;
              if (r != 0)
              {
                this.mode = r != 1 ? 9 : 7;
                continue;
              }
            }
            this.need = (int) this.lbits;
            this.tree = this.ltree;
            this.tree_index = this.ltree_index;
            this.mode = 1;
            goto case 1;
          case 1:
            int index1 = this.need;
            while (num3 < index1)
            {
              if (num2 != 0)
              {
                r = 0;
                --num2;
                number |= ((int) z.next_in[num1++] & (int) byte.MaxValue) << num3;
                num3 += 8;
              }
              else
              {
                s.bitb = number;
                s.bitk = num3;
                z.avail_in = num2;
                z.total_in += (long) (num1 - z.next_in_index);
                z.next_in_index = num1;
                s.write = num4;
                return s.inflate_flush(z, r);
              }
            }
            int index2 = (this.tree_index + (number & InfCodes.inflate_mask[index1])) * 3;
            number = SupportClass.URShift(number, this.tree[index2 + 1]);
            num3 -= this.tree[index2 + 1];
            int num6 = this.tree[index2];
            if (num6 == 0)
            {
              this.lit = this.tree[index2 + 2];
              this.mode = 6;
              continue;
            }
            if ((num6 & 16) != 0)
            {
              this.get_Renamed = num6 & 15;
              this.len = this.tree[index2 + 2];
              this.mode = 2;
              continue;
            }
            if ((num6 & 64) == 0)
            {
              this.need = num6;
              this.tree_index = index2 / 3 + this.tree[index2 + 2];
              continue;
            }
            if ((num6 & 32) != 0)
            {
              this.mode = 7;
              continue;
            }
            goto label_19;
          case 2:
            int index3 = this.get_Renamed;
            while (num3 < index3)
            {
              if (num2 != 0)
              {
                r = 0;
                --num2;
                number |= ((int) z.next_in[num1++] & (int) byte.MaxValue) << num3;
                num3 += 8;
              }
              else
              {
                s.bitb = number;
                s.bitk = num3;
                z.avail_in = num2;
                z.total_in += (long) (num1 - z.next_in_index);
                z.next_in_index = num1;
                s.write = num4;
                return s.inflate_flush(z, r);
              }
            }
            this.len += number & InfCodes.inflate_mask[index3];
            number >>= index3;
            num3 -= index3;
            this.need = (int) this.dbits;
            this.tree = this.dtree;
            this.tree_index = this.dtree_index;
            this.mode = 3;
            goto case 3;
          case 3:
            int index4 = this.need;
            while (num3 < index4)
            {
              if (num2 != 0)
              {
                r = 0;
                --num2;
                number |= ((int) z.next_in[num1++] & (int) byte.MaxValue) << num3;
                num3 += 8;
              }
              else
              {
                s.bitb = number;
                s.bitk = num3;
                z.avail_in = num2;
                z.total_in += (long) (num1 - z.next_in_index);
                z.next_in_index = num1;
                s.write = num4;
                return s.inflate_flush(z, r);
              }
            }
            int index5 = (this.tree_index + (number & InfCodes.inflate_mask[index4])) * 3;
            number >>= this.tree[index5 + 1];
            num3 -= this.tree[index5 + 1];
            int num7 = this.tree[index5];
            if ((num7 & 16) != 0)
            {
              this.get_Renamed = num7 & 15;
              this.dist = this.tree[index5 + 2];
              this.mode = 4;
              continue;
            }
            if ((num7 & 64) == 0)
            {
              this.need = num7;
              this.tree_index = index5 / 3 + this.tree[index5 + 2];
              continue;
            }
            goto label_35;
          case 4:
            int index6 = this.get_Renamed;
            while (num3 < index6)
            {
              if (num2 != 0)
              {
                r = 0;
                --num2;
                number |= ((int) z.next_in[num1++] & (int) byte.MaxValue) << num3;
                num3 += 8;
              }
              else
              {
                s.bitb = number;
                s.bitk = num3;
                z.avail_in = num2;
                z.total_in += (long) (num1 - z.next_in_index);
                z.next_in_index = num1;
                s.write = num4;
                return s.inflate_flush(z, r);
              }
            }
            this.dist += number & InfCodes.inflate_mask[index6];
            number >>= index6;
            num3 -= index6;
            this.mode = 5;
            goto case 5;
          case 5:
            int num8 = num4 - this.dist;
            while (num8 < 0)
              num8 += s.end;
            for (; this.len != 0; --this.len)
            {
              if (num5 == 0)
              {
                if (num4 == s.end && s.read != 0)
                {
                  num4 = 0;
                  num5 = num4 >= s.read ? s.end - num4 : s.read - num4 - 1;
                }
                if (num5 == 0)
                {
                  s.write = num4;
                  r = s.inflate_flush(z, r);
                  num4 = s.write;
                  num5 = num4 >= s.read ? s.end - num4 : s.read - num4 - 1;
                  if (num4 == s.end && s.read != 0)
                  {
                    num4 = 0;
                    num5 = num4 >= s.read ? s.end - num4 : s.read - num4 - 1;
                  }
                  if (num5 == 0)
                  {
                    s.bitb = number;
                    s.bitk = num3;
                    z.avail_in = num2;
                    z.total_in += (long) (num1 - z.next_in_index);
                    z.next_in_index = num1;
                    s.write = num4;
                    return s.inflate_flush(z, r);
                  }
                }
              }
              s.window[num4++] = s.window[num8++];
              --num5;
              if (num8 == s.end)
                num8 = 0;
            }
            this.mode = 0;
            continue;
          case 6:
            if (num5 == 0)
            {
              if (num4 == s.end && s.read != 0)
              {
                num4 = 0;
                num5 = num4 >= s.read ? s.end - num4 : s.read - num4 - 1;
              }
              if (num5 == 0)
              {
                s.write = num4;
                r = s.inflate_flush(z, r);
                num4 = s.write;
                num5 = num4 >= s.read ? s.end - num4 : s.read - num4 - 1;
                if (num4 == s.end && s.read != 0)
                {
                  num4 = 0;
                  num5 = num4 >= s.read ? s.end - num4 : s.read - num4 - 1;
                }
                if (num5 == 0)
                  goto label_65;
              }
            }
            r = 0;
            s.window[num4++] = (byte) this.lit;
            --num5;
            this.mode = 0;
            continue;
          case 7:
            goto label_67;
          case 8:
            goto label_72;
          case 9:
            goto label_73;
          default:
            goto label_74;
        }
      }
label_19:
      this.mode = 9;
      z.msg = "invalid literal/length code";
      r = -3;
      s.bitb = number;
      s.bitk = num3;
      z.avail_in = num2;
      z.total_in += (long) (num1 - z.next_in_index);
      z.next_in_index = num1;
      s.write = num4;
      return s.inflate_flush(z, r);
label_35:
      this.mode = 9;
      z.msg = "invalid distance code";
      r = -3;
      s.bitb = number;
      s.bitk = num3;
      z.avail_in = num2;
      z.total_in += (long) (num1 - z.next_in_index);
      z.next_in_index = num1;
      s.write = num4;
      return s.inflate_flush(z, r);
label_65:
      s.bitb = number;
      s.bitk = num3;
      z.avail_in = num2;
      z.total_in += (long) (num1 - z.next_in_index);
      z.next_in_index = num1;
      s.write = num4;
      return s.inflate_flush(z, r);
label_67:
      if (num3 > 7)
      {
        num3 -= 8;
        ++num2;
        --num1;
      }
      s.write = num4;
      r = s.inflate_flush(z, r);
      num4 = s.write;
      int num9 = num4 >= s.read ? s.end - num4 : s.read - num4 - 1;
      if (s.read != s.write)
      {
        s.bitb = number;
        s.bitk = num3;
        z.avail_in = num2;
        z.total_in += (long) (num1 - z.next_in_index);
        z.next_in_index = num1;
        s.write = num4;
        return s.inflate_flush(z, r);
      }
      this.mode = 8;
label_72:
      r = 1;
      s.bitb = number;
      s.bitk = num3;
      z.avail_in = num2;
      z.total_in += (long) (num1 - z.next_in_index);
      z.next_in_index = num1;
      s.write = num4;
      return s.inflate_flush(z, r);
label_73:
      r = -3;
      s.bitb = number;
      s.bitk = num3;
      z.avail_in = num2;
      z.total_in += (long) (num1 - z.next_in_index);
      z.next_in_index = num1;
      s.write = num4;
      return s.inflate_flush(z, r);
label_74:
      r = -2;
      s.bitb = number;
      s.bitk = num3;
      z.avail_in = num2;
      z.total_in += (long) (num1 - z.next_in_index);
      z.next_in_index = num1;
      s.write = num4;
      return s.inflate_flush(z, r);
    }

    internal void free(ZStream z)
    {
    }

    internal int inflate_fast(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index, InfBlocks s, ZStream z)
    {
      int num1 = z.next_in_index;
      int num2 = z.avail_in;
      int num3 = s.bitb;
      int num4 = s.bitk;
      int destinationIndex = s.write;
      int num5 = destinationIndex >= s.read ? s.end - destinationIndex : s.read - destinationIndex - 1;
      int num6 = InfCodes.inflate_mask[bl];
      int num7 = InfCodes.inflate_mask[bd];
      do
      {
        while (num4 < 20)
        {
          --num2;
          num3 |= ((int) z.next_in[num1++] & (int) byte.MaxValue) << num4;
          num4 += 8;
        }
        int num8 = num3 & num6;
        int[] numArray1 = tl;
        int num9 = tl_index;
        int index1;
        if ((index1 = numArray1[(num9 + num8) * 3]) == 0)
        {
          num3 >>= numArray1[(num9 + num8) * 3 + 1];
          num4 -= numArray1[(num9 + num8) * 3 + 1];
          s.window[destinationIndex++] = (byte) numArray1[(num9 + num8) * 3 + 2];
          --num5;
        }
        else
        {
          do
          {
            num3 >>= numArray1[(num9 + num8) * 3 + 1];
            num4 -= numArray1[(num9 + num8) * 3 + 1];
            if ((index1 & 16) != 0)
            {
              int index2 = index1 & 15;
              int length1 = numArray1[(num9 + num8) * 3 + 2] + (num3 & InfCodes.inflate_mask[index2]);
              int num10 = num3 >> index2;
              int num11 = num4 - index2;
              while (num11 < 15)
              {
                --num2;
                num10 |= ((int) z.next_in[num1++] & (int) byte.MaxValue) << num11;
                num11 += 8;
              }
              int num12 = num10 & num7;
              int[] numArray2 = td;
              int num13 = td_index;
              int index3 = numArray2[(num13 + num12) * 3];
              while (true)
              {
                num10 >>= numArray2[(num13 + num12) * 3 + 1];
                num11 -= numArray2[(num13 + num12) * 3 + 1];
                if ((index3 & 16) == 0)
                {
                  if ((index3 & 64) == 0)
                  {
                    num12 = num12 + numArray2[(num13 + num12) * 3 + 2] + (num10 & InfCodes.inflate_mask[index3]);
                    index3 = numArray2[(num13 + num12) * 3];
                  }
                  else
                    goto label_30;
                }
                else
                  break;
              }
              int index4 = index3 & 15;
              while (num11 < index4)
              {
                --num2;
                num10 |= ((int) z.next_in[num1++] & (int) byte.MaxValue) << num11;
                num11 += 8;
              }
              int num14 = numArray2[(num13 + num12) * 3 + 2] + (num10 & InfCodes.inflate_mask[index4]);
              num3 = num10 >> index4;
              num4 = num11 - index4;
              num5 -= length1;
              int sourceIndex1;
              int num15;
              if (destinationIndex >= num14)
              {
                int sourceIndex2 = destinationIndex - num14;
                if (destinationIndex - sourceIndex2 > 0 && 2 > destinationIndex - sourceIndex2)
                {
                  byte[] numArray3 = s.window;
                  int index5 = destinationIndex;
                  int num16 = 1;
                  int num17 = index5 + num16;
                  byte[] numArray4 = s.window;
                  int index6 = sourceIndex2;
                  int num18 = 1;
                  int num19 = index6 + num18;
                  int num20 = (int) numArray4[index6];
                  numArray3[index5] = (byte) num20;
                  int num21 = length1 - 1;
                  byte[] numArray5 = s.window;
                  int index7 = num17;
                  int num22 = 1;
                  destinationIndex = index7 + num22;
                  byte[] numArray6 = s.window;
                  int index8 = num19;
                  int num23 = 1;
                  sourceIndex1 = index8 + num23;
                  int num24 = (int) numArray6[index8];
                  numArray5[index7] = (byte) num24;
                  length1 = num21 - 1;
                }
                else
                {
                  Array.Copy((Array) s.window, sourceIndex2, (Array) s.window, destinationIndex, 2);
                  destinationIndex += 2;
                  sourceIndex1 = sourceIndex2 + 2;
                  length1 -= 2;
                }
              }
              else
              {
                sourceIndex1 = destinationIndex - num14;
                do
                {
                  sourceIndex1 += s.end;
                }
                while (sourceIndex1 < 0);
                int length2 = s.end - sourceIndex1;
                if (length1 > length2)
                {
                  length1 -= length2;
                  if (destinationIndex - sourceIndex1 > 0 && length2 > destinationIndex - sourceIndex1)
                  {
                    do
                    {
                      s.window[destinationIndex++] = s.window[sourceIndex1++];
                    }
                    while (--length2 != 0);
                  }
                  else
                  {
                    Array.Copy((Array) s.window, sourceIndex1, (Array) s.window, destinationIndex, length2);
                    destinationIndex += length2;
                    num15 = sourceIndex1 + length2;
                  }
                  sourceIndex1 = 0;
                }
              }
              if (destinationIndex - sourceIndex1 > 0 && length1 > destinationIndex - sourceIndex1)
              {
                do
                {
                  s.window[destinationIndex++] = s.window[sourceIndex1++];
                }
                while (--length1 != 0);
                goto label_37;
              }
              else
              {
                Array.Copy((Array) s.window, sourceIndex1, (Array) s.window, destinationIndex, length1);
                destinationIndex += length1;
                num15 = sourceIndex1 + length1;
                goto label_37;
              }
label_30:
              z.msg = "invalid distance code";
              int num25 = z.avail_in - num2;
              int num26 = num11 >> 3 >= num25 ? num25 : num11 >> 3;
              int num27 = num2 + num26;
              int num28 = num1 - num26;
              int num29 = num11 - (num26 << 3);
              s.bitb = num10;
              s.bitk = num29;
              z.avail_in = num27;
              z.total_in += (long) (num28 - z.next_in_index);
              z.next_in_index = num28;
              s.write = destinationIndex;
              return -3;
            }
            if ((index1 & 64) == 0)
              num8 = num8 + numArray1[(num9 + num8) * 3 + 2] + (num3 & InfCodes.inflate_mask[index1]);
            else
              goto label_34;
          }
          while ((index1 = numArray1[(num9 + num8) * 3]) != 0);
          num3 >>= numArray1[(num9 + num8) * 3 + 1];
          num4 -= numArray1[(num9 + num8) * 3 + 1];
          s.window[destinationIndex++] = (byte) numArray1[(num9 + num8) * 3 + 2];
          --num5;
          goto label_37;
label_34:
          if ((index1 & 32) != 0)
          {
            int num10 = z.avail_in - num2;
            int num11 = num4 >> 3 >= num10 ? num10 : num4 >> 3;
            int num12 = num2 + num11;
            int num13 = num1 - num11;
            int num14 = num4 - (num11 << 3);
            s.bitb = num3;
            s.bitk = num14;
            z.avail_in = num12;
            z.total_in += (long) (num13 - z.next_in_index);
            z.next_in_index = num13;
            s.write = destinationIndex;
            return 1;
          }
          z.msg = "invalid literal/length code";
          int num30 = z.avail_in - num2;
          int num31 = num4 >> 3 >= num30 ? num30 : num4 >> 3;
          int num32 = num2 + num31;
          int num33 = num1 - num31;
          int num34 = num4 - (num31 << 3);
          s.bitb = num3;
          s.bitk = num34;
          z.avail_in = num32;
          z.total_in += (long) (num33 - z.next_in_index);
          z.next_in_index = num33;
          s.write = destinationIndex;
          return -3;
        }
label_37:;
      }
      while (num5 >= 258 && num2 >= 10);
      int num35 = z.avail_in - num2;
      int num36 = num4 >> 3 >= num35 ? num35 : num4 >> 3;
      int num37 = num2 + num36;
      int num38 = num1 - num36;
      int num39 = num4 - (num36 << 3);
      s.bitb = num3;
      s.bitk = num39;
      z.avail_in = num37;
      z.total_in += (long) (num38 - z.next_in_index);
      z.next_in_index = num38;
      s.write = destinationIndex;
      return 0;
    }
  }
}
