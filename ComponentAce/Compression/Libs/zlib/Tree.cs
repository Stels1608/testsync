﻿// Decompiled with JetBrains decompiler
// Type: ComponentAce.Compression.Libs.zlib.Tree
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

namespace ComponentAce.Compression.Libs.zlib
{
  internal sealed class Tree
  {
    private static readonly int L_CODES = 286;
    private static readonly int HEAP_SIZE = 2 * Tree.L_CODES + 1;
    internal static readonly int[] extra_lbits = new int[29]{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0 };
    internal static readonly int[] extra_dbits = new int[30]{ 0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13 };
    private const int MAX_BITS = 15;
    private const int BL_CODES = 19;
    private const int D_CODES = 30;
    private const int LITERALS = 256;
    private const int LENGTH_CODES = 29;
    internal const int MAX_BL_BITS = 7;
    internal const int END_BLOCK = 256;
    internal const int REP_3_6 = 16;
    internal const int REPZ_3_10 = 17;
    internal const int REPZ_11_138 = 18;
    internal const int Buf_size = 16;
    internal const int DIST_CODE_LEN = 512;
    internal static readonly int[] extra_blbits;
    internal static readonly byte[] bl_order;
    internal static readonly byte[] _dist_code;
    internal static readonly byte[] _length_code;
    internal static readonly int[] base_length;
    internal static readonly int[] base_dist;
    internal short[] dyn_tree;
    internal int max_code;
    internal StaticTree stat_desc;

    static Tree()
    {
      int[] numArray = new int[19];
      numArray[16] = 2;
      numArray[17] = 3;
      numArray[18] = 7;
      Tree.extra_blbits = numArray;
      Tree.bl_order = new byte[19]
      {
        (byte) 16,
        (byte) 17,
        (byte) 18,
        (byte) 0,
        (byte) 8,
        (byte) 7,
        (byte) 9,
        (byte) 6,
        (byte) 10,
        (byte) 5,
        (byte) 11,
        (byte) 4,
        (byte) 12,
        (byte) 3,
        (byte) 13,
        (byte) 2,
        (byte) 14,
        (byte) 1,
        (byte) 15
      };
      Tree._dist_code = new byte[512]
      {
        (byte) 0,
        (byte) 1,
        (byte) 2,
        (byte) 3,
        (byte) 4,
        (byte) 4,
        (byte) 5,
        (byte) 5,
        (byte) 6,
        (byte) 6,
        (byte) 6,
        (byte) 6,
        (byte) 7,
        (byte) 7,
        (byte) 7,
        (byte) 7,
        (byte) 8,
        (byte) 8,
        (byte) 8,
        (byte) 8,
        (byte) 8,
        (byte) 8,
        (byte) 8,
        (byte) 8,
        (byte) 9,
        (byte) 9,
        (byte) 9,
        (byte) 9,
        (byte) 9,
        (byte) 9,
        (byte) 9,
        (byte) 9,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 10,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 11,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 0,
        (byte) 0,
        (byte) 16,
        (byte) 17,
        (byte) 18,
        (byte) 18,
        (byte) 19,
        (byte) 19,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 28,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29,
        (byte) 29
      };
      Tree._length_code = new byte[256]
      {
        (byte) 0,
        (byte) 1,
        (byte) 2,
        (byte) 3,
        (byte) 4,
        (byte) 5,
        (byte) 6,
        (byte) 7,
        (byte) 8,
        (byte) 8,
        (byte) 9,
        (byte) 9,
        (byte) 10,
        (byte) 10,
        (byte) 11,
        (byte) 11,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 12,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 13,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 14,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 15,
        (byte) 16,
        (byte) 16,
        (byte) 16,
        (byte) 16,
        (byte) 16,
        (byte) 16,
        (byte) 16,
        (byte) 16,
        (byte) 17,
        (byte) 17,
        (byte) 17,
        (byte) 17,
        (byte) 17,
        (byte) 17,
        (byte) 17,
        (byte) 17,
        (byte) 18,
        (byte) 18,
        (byte) 18,
        (byte) 18,
        (byte) 18,
        (byte) 18,
        (byte) 18,
        (byte) 18,
        (byte) 19,
        (byte) 19,
        (byte) 19,
        (byte) 19,
        (byte) 19,
        (byte) 19,
        (byte) 19,
        (byte) 19,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 20,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 21,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 22,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 23,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 24,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 25,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 26,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 27,
        (byte) 28
      };
      Tree.base_length = new int[29]
      {
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        10,
        12,
        14,
        16,
        20,
        24,
        28,
        32,
        40,
        48,
        56,
        64,
        80,
        96,
        112,
        128,
        160,
        192,
        224,
        0
      };
      Tree.base_dist = new int[30]
      {
        0,
        1,
        2,
        3,
        4,
        6,
        8,
        12,
        16,
        24,
        32,
        48,
        64,
        96,
        128,
        192,
        256,
        384,
        512,
        768,
        1024,
        1536,
        2048,
        3072,
        4096,
        6144,
        8192,
        12288,
        16384,
        24576
      };
    }

    internal static int d_code(int dist)
    {
      if (dist < 256)
        return (int) Tree._dist_code[dist];
      return (int) Tree._dist_code[256 + SupportClass.URShift(dist, 7)];
    }

    internal void gen_bitlen(Deflate s)
    {
      short[] numArray1 = this.dyn_tree;
      short[] numArray2 = this.stat_desc.static_tree;
      int[] numArray3 = this.stat_desc.extra_bits;
      int num1 = this.stat_desc.extra_base;
      int index1 = this.stat_desc.max_length;
      int num2 = 0;
      for (int index2 = 0; index2 <= 15; ++index2)
        s.bl_count[index2] = (short) 0;
      numArray1[s.heap[s.heap_max] * 2 + 1] = (short) 0;
      int index3;
      for (index3 = s.heap_max + 1; index3 < Tree.HEAP_SIZE; ++index3)
      {
        int num3 = s.heap[index3];
        int index2 = (int) numArray1[(int) numArray1[num3 * 2 + 1] * 2 + 1] + 1;
        if (index2 > index1)
        {
          index2 = index1;
          ++num2;
        }
        numArray1[num3 * 2 + 1] = (short) index2;
        if (num3 <= this.max_code)
        {
          ++s.bl_count[index2];
          int num4 = 0;
          if (num3 >= num1)
            num4 = numArray3[num3 - num1];
          short num5 = numArray1[num3 * 2];
          s.opt_len += (int) num5 * (index2 + num4);
          if (numArray2 != null)
            s.static_len += (int) num5 * ((int) numArray2[num3 * 2 + 1] + num4);
        }
      }
      if (num2 == 0)
        return;
      do
      {
        int index2 = index1 - 1;
        while ((int) s.bl_count[index2] == 0)
          --index2;
        --s.bl_count[index2];
        s.bl_count[index2 + 1] = (short) ((int) s.bl_count[index2 + 1] + 2);
        --s.bl_count[index1];
        num2 -= 2;
      }
      while (num2 > 0);
      for (int index2 = index1; index2 != 0; --index2)
      {
        int num3 = (int) s.bl_count[index2];
        while (num3 != 0)
        {
          int num4 = s.heap[--index3];
          if (num4 <= this.max_code)
          {
            if ((int) numArray1[num4 * 2 + 1] != index2)
            {
              s.opt_len = (int) ((long) s.opt_len + ((long) index2 - (long) numArray1[num4 * 2 + 1]) * (long) numArray1[num4 * 2]);
              numArray1[num4 * 2 + 1] = (short) index2;
            }
            --num3;
          }
        }
      }
    }

    internal void build_tree(Deflate s)
    {
      short[] tree = this.dyn_tree;
      short[] numArray = this.stat_desc.static_tree;
      int num = this.stat_desc.elems;
      int max_code = -1;
      s.heap_len = 0;
      s.heap_max = Tree.HEAP_SIZE;
      for (int index = 0; index < num; ++index)
      {
        if ((int) tree[index * 2] != 0)
        {
          s.heap[++s.heap_len] = max_code = index;
          s.depth[index] = (byte) 0;
        }
        else
          tree[index * 2 + 1] = (short) 0;
      }
      while (s.heap_len < 2)
      {
        int index = s.heap[++s.heap_len] = max_code >= 2 ? 0 : ++max_code;
        tree[index * 2] = (short) 1;
        s.depth[index] = (byte) 0;
        --s.opt_len;
        if (numArray != null)
          s.static_len -= (int) numArray[index * 2 + 1];
      }
      this.max_code = max_code;
      for (int k = s.heap_len / 2; k >= 1; --k)
        s.pqdownheap(tree, k);
      int index1 = num;
      do
      {
        int index2 = s.heap[1];
        s.heap[1] = s.heap[s.heap_len--];
        s.pqdownheap(tree, 1);
        int index3 = s.heap[1];
        s.heap[--s.heap_max] = index2;
        s.heap[--s.heap_max] = index3;
        tree[index1 * 2] = (short) ((int) tree[index2 * 2] + (int) tree[index3 * 2]);
        s.depth[index1] = (byte) ((uint) Math.Max(s.depth[index2], s.depth[index3]) + 1U);
        tree[index2 * 2 + 1] = tree[index3 * 2 + 1] = (short) index1;
        s.heap[1] = index1++;
        s.pqdownheap(tree, 1);
      }
      while (s.heap_len >= 2);
      s.heap[--s.heap_max] = s.heap[1];
      this.gen_bitlen(s);
      Tree.gen_codes(tree, max_code, s.bl_count);
    }

    internal static void gen_codes(short[] tree, int max_code, short[] bl_count)
    {
      short[] numArray = new short[16];
      short num = 0;
      for (int index = 1; index <= 15; ++index)
        numArray[index] = num = (short) ((int) num + (int) bl_count[index - 1] << 1);
      for (int index = 0; index <= max_code; ++index)
      {
        int len = (int) tree[index * 2 + 1];
        if (len != 0)
          tree[index * 2] = (short) Tree.bi_reverse((int) numArray[len]++, len);
      }
    }

    internal static int bi_reverse(int code, int len)
    {
      int number = 0;
      do
      {
        int num = number | code & 1;
        code = SupportClass.URShift(code, 1);
        number = num << 1;
      }
      while (--len > 0);
      return SupportClass.URShift(number, 1);
    }
  }
}
