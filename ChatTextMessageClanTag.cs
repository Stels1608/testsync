﻿// Decompiled with JetBrains decompiler
// Type: ChatTextMessageClanTag
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

internal class ChatTextMessageClanTag : ChatProtocolParser
{
  public string text;
  public string userName;
  public int roomId;
  public string clanTag;

  public ChatTextMessageClanTag()
  {
    this.type = ChatProtocolParserType.TextClanTagged;
  }

  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split(new char[3]{ '%', '@', '#' });
    if (strArray.Length != 6 || strArray[0] != "a" || !int.TryParse(strArray[1], out this.roomId))
      return false;
    this.userName = strArray[2];
    this.text = strArray[3];
    this.clanTag = strArray[4];
    return true;
  }
}
