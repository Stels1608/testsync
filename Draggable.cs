﻿// Decompiled with JetBrains decompiler
// Type: Draggable
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IDragHandler, IEventSystemHandler
{
  public RectTransform target;
  private Vector3 startPosition;

  private void Start()
  {
    this.startPosition = this.target.localPosition;
  }

  public void OnDrag(PointerEventData eventData)
  {
    RectTransform rectTransform = this.target;
    Vector3 vector3 = rectTransform.position + new Vector3(eventData.delta.x, eventData.delta.y);
    rectTransform.position = vector3;
  }

  private void OnEnable()
  {
    this.target.localPosition = this.startPosition;
  }
}
