﻿// Decompiled with JetBrains decompiler
// Type: GuildRankNameChangedMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System;

public class GuildRankNameChangedMessage : IMessage<Message>
{
  public GuildRole Rank { get; set; }

  public string RankName { get; set; }

  public object Data
  {
    get
    {
      throw new NotImplementedException();
    }
    set
    {
      throw new NotImplementedException();
    }
  }

  public Message Id
  {
    get
    {
      return Message.GuildRankNameChanged;
    }
  }

  public GuildRankNameChangedMessage(GuildRole rank, string rankName)
  {
    this.Rank = rank;
    this.RankName = rankName;
  }
}
