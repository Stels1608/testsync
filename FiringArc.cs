﻿// Decompiled with JetBrains decompiler
// Type: FiringArc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FiringArc : ModelScript
{
  private ShipAbility highlightedAbility;
  private GameObject firingArc;

  public void ChangeFiringArc(ShipAbility value)
  {
    if (this.highlightedAbility == value)
      return;
    if ((Object) this.firingArc != (Object) null)
      Object.Destroy((Object) this.firingArc);
    this.highlightedAbility = value;
    if (this.highlightedAbility == null)
      return;
    FixingSpot fixingSpot = Game.Me.Ship.GetObjectPoint(this.highlightedAbility.slot.Card.ObjectPointServerHash) as FixingSpot;
    if (fixingSpot == null)
      return;
    this.firingArc = Object.Instantiate<GameObject>(this.GetFiringArcPrefab(this.highlightedAbility));
    this.firingArc.transform.localPosition = fixingSpot.Position;
    this.firingArc.transform.localRotation = fixingSpot.Rotation;
    this.firingArc.transform.localScale = Vector3.one;
    this.firingArc.transform.parent = Game.Me.Ship.Model.transform;
    float num = 1f;
    switch (Game.Me.ActiveShip.Card.Tier)
    {
      case 2:
        num = 5f;
        break;
      case 3:
        num = 10f;
        break;
      case 4:
        num = 100f;
        break;
    }
    this.firingArc.transform.localScale *= num;
  }

  private GameObject GetFiringArcPrefab(ShipAbility ability)
  {
    switch (Game.Me.Faction)
    {
      case Faction.Colonial:
        if ((double) ability.Angle < 10.0)
          return SpaceExposer.GetInstance().ColonialFiringArc20;
        if ((double) ability.Angle < 15.0)
          return SpaceExposer.GetInstance().ColonialFiringArc30;
        if ((double) ability.Angle < 20.0)
          return SpaceExposer.GetInstance().ColonialFiringArc40;
        if ((double) ability.Angle < 25.0)
          return SpaceExposer.GetInstance().ColonialFiringArc50;
        if ((double) ability.Angle < 30.0)
          return SpaceExposer.GetInstance().ColonialFiringArc60;
        if ((double) ability.Angle < 38.0)
          return SpaceExposer.GetInstance().ColonialFiringArc75;
        if ((double) ability.Angle < 46.0)
          return SpaceExposer.GetInstance().ColonialFiringArc90;
        if ((double) ability.Angle < 61.0)
          return SpaceExposer.GetInstance().ColonialFiringArc120;
        if ((double) ability.Angle < 91.0)
          return SpaceExposer.GetInstance().ColonialFiringArc180;
        return SpaceExposer.GetInstance().ColonialFiringArc360;
      case Faction.Cylon:
        if ((double) ability.Angle < 10.0)
          return SpaceExposer.GetInstance().CylonFiringArc20;
        if ((double) ability.Angle < 15.0)
          return SpaceExposer.GetInstance().CylonFiringArc30;
        if ((double) ability.Angle < 20.0)
          return SpaceExposer.GetInstance().CylonFiringArc40;
        if ((double) ability.Angle < 25.0)
          return SpaceExposer.GetInstance().CylonFiringArc50;
        if ((double) ability.Angle < 30.0)
          return SpaceExposer.GetInstance().CylonFiringArc60;
        if ((double) ability.Angle < 38.0)
          return SpaceExposer.GetInstance().CylonFiringArc75;
        if ((double) ability.Angle < 46.0)
          return SpaceExposer.GetInstance().CylonFiringArc90;
        if ((double) ability.Angle < 61.0)
          return SpaceExposer.GetInstance().CylonFiringArc120;
        if ((double) ability.Angle < 91.0)
          return SpaceExposer.GetInstance().CylonFiringArc180;
        return SpaceExposer.GetInstance().CylonFiringArc360;
      default:
        return (GameObject) null;
    }
  }
}
