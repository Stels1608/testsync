﻿// Decompiled with JetBrains decompiler
// Type: PropertyReference
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Diagnostics;
using System.Reflection;
using UnityEngine;

[Serializable]
public class PropertyReference
{
  private static int s_Hash = "PropertyBinding".GetHashCode();
  [SerializeField]
  private Component mTarget;
  [SerializeField]
  private string mName;
  private FieldInfo mField;
  private PropertyInfo mProperty;

  public Component target
  {
    get
    {
      return this.mTarget;
    }
    set
    {
      this.mTarget = value;
      this.mProperty = (PropertyInfo) null;
      this.mField = (FieldInfo) null;
    }
  }

  public string name
  {
    get
    {
      return this.mName;
    }
    set
    {
      this.mName = value;
      this.mProperty = (PropertyInfo) null;
      this.mField = (FieldInfo) null;
    }
  }

  public bool isValid
  {
    get
    {
      if ((UnityEngine.Object) this.mTarget != (UnityEngine.Object) null)
        return !string.IsNullOrEmpty(this.mName);
      return false;
    }
  }

  public bool isEnabled
  {
    get
    {
      if ((UnityEngine.Object) this.mTarget == (UnityEngine.Object) null)
        return false;
      MonoBehaviour monoBehaviour = this.mTarget as MonoBehaviour;
      if (!((UnityEngine.Object) monoBehaviour == (UnityEngine.Object) null))
        return monoBehaviour.enabled;
      return true;
    }
  }

  public PropertyReference()
  {
  }

  public PropertyReference(Component target, string fieldName)
  {
    this.mTarget = target;
    this.mName = fieldName;
  }

  public System.Type GetPropertyType()
  {
    if (this.mProperty == null && this.mField == null && this.isValid)
      this.Cache();
    if (this.mProperty != null)
      return this.mProperty.PropertyType;
    if (this.mField != null)
      return this.mField.FieldType;
    return typeof (void);
  }

  public override bool Equals(object obj)
  {
    if (obj == null)
      return !this.isValid;
    if (!(obj is PropertyReference))
      return false;
    PropertyReference propertyReference = obj as PropertyReference;
    if ((UnityEngine.Object) this.mTarget == (UnityEngine.Object) propertyReference.mTarget)
      return string.Equals(this.mName, propertyReference.mName);
    return false;
  }

  public override int GetHashCode()
  {
    return PropertyReference.s_Hash;
  }

  public void Set(Component target, string methodName)
  {
    this.mTarget = target;
    this.mName = methodName;
  }

  public void Clear()
  {
    this.mTarget = (Component) null;
    this.mName = (string) null;
  }

  public void Reset()
  {
    this.mField = (FieldInfo) null;
    this.mProperty = (PropertyInfo) null;
  }

  public override string ToString()
  {
    return PropertyReference.ToString(this.mTarget, this.name);
  }

  public static string ToString(Component comp, string property)
  {
    if (!((UnityEngine.Object) comp != (UnityEngine.Object) null))
      return (string) null;
    string str = comp.GetType().ToString();
    int num = str.LastIndexOf('.');
    if (num > 0)
      str = str.Substring(num + 1);
    if (!string.IsNullOrEmpty(property))
      return str + "." + property;
    return str + ".[property]";
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  public object Get()
  {
    if (this.mProperty == null && this.mField == null && this.isValid)
      this.Cache();
    if (this.mProperty != null)
    {
      if (this.mProperty.CanRead)
        return this.mProperty.GetValue((object) this.mTarget, (object[]) null);
    }
    else if (this.mField != null)
      return this.mField.GetValue((object) this.mTarget);
    return (object) null;
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  public bool Set(object value)
  {
    if (this.mProperty == null && this.mField == null && this.isValid)
      this.Cache();
    if (this.mProperty == null && this.mField == null)
      return false;
    if (value == null)
    {
      try
      {
        if (this.mProperty != null)
        {
          if (this.mProperty.CanWrite)
          {
            this.mProperty.SetValue((object) this.mTarget, (object) null, (object[]) null);
            return true;
          }
        }
        else
        {
          this.mField.SetValue((object) this.mTarget, (object) null);
          return true;
        }
      }
      catch (Exception ex)
      {
        return false;
      }
    }
    if (!this.Convert(ref value))
    {
      if (Application.isPlaying)
        UnityEngine.Debug.LogError((object) ("Unable to convert " + (object) value.GetType() + " to " + (object) this.GetPropertyType()));
    }
    else
    {
      if (this.mField != null)
      {
        this.mField.SetValue((object) this.mTarget, value);
        return true;
      }
      if (this.mProperty.CanWrite)
      {
        this.mProperty.SetValue((object) this.mTarget, value, (object[]) null);
        return true;
      }
    }
    return false;
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  private bool Cache()
  {
    if ((UnityEngine.Object) this.mTarget != (UnityEngine.Object) null && !string.IsNullOrEmpty(this.mName))
    {
      System.Type type = this.mTarget.GetType();
      this.mField = type.GetField(this.mName);
      this.mProperty = type.GetProperty(this.mName);
    }
    else
    {
      this.mField = (FieldInfo) null;
      this.mProperty = (PropertyInfo) null;
    }
    if (this.mField == null)
      return this.mProperty != null;
    return true;
  }

  private bool Convert(ref object value)
  {
    if ((UnityEngine.Object) this.mTarget == (UnityEngine.Object) null)
      return false;
    System.Type propertyType = this.GetPropertyType();
    System.Type from;
    if (value == null)
    {
      if (!propertyType.IsClass)
        return false;
      from = propertyType;
    }
    else
      from = value.GetType();
    return PropertyReference.Convert(ref value, from, propertyType);
  }

  public static bool Convert(System.Type from, System.Type to)
  {
    object obj = (object) null;
    return PropertyReference.Convert(ref obj, from, to);
  }

  public static bool Convert(object value, System.Type to)
  {
    if (value != null)
      return PropertyReference.Convert(ref value, value.GetType(), to);
    value = (object) null;
    return PropertyReference.Convert(ref value, to, to);
  }

  public static bool Convert(ref object value, System.Type from, System.Type to)
  {
    if (to.IsAssignableFrom(from))
      return true;
    if (to == typeof (string))
    {
      value = value == null ? (object) "null" : (object) value.ToString();
      return true;
    }
    if (value == null)
      return false;
    if (to == typeof (int))
    {
      if (from == typeof (string))
      {
        int result;
        if (int.TryParse((string) value, out result))
        {
          value = (object) result;
          return true;
        }
      }
      else if (from == typeof (float))
      {
        value = (object) Mathf.RoundToInt((float) value);
        return true;
      }
    }
    else
    {
      float result;
      if (to == typeof (float) && from == typeof (string) && float.TryParse((string) value, out result))
      {
        value = (object) result;
        return true;
      }
    }
    return false;
  }
}
