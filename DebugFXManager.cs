﻿// Decompiled with JetBrains decompiler
// Type: DebugFXManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugFXManager : DebugBehaviour<DebugFXManager>
{
  private float updateInterval = 0.5f;
  private float lastUpdateTime = float.NegativeInfinity;
  private int gunshotCount;
  private int bulletCount;
  private int bulletExplCount;
  private int missileExplCount;

  private void Start()
  {
    this.windowID = 2;
    this.SetSize(200f, 400f);
  }

  protected override void WindowFunc()
  {
    GUILayout.BeginArea(new Rect(0.0f, 30f, this.windowRect.width, this.windowRect.height));
    GUILayout.BeginVertical();
    GUILayout.BeginHorizontal();
    GUILayout.Label("Gunshot count: " + this.gunshotCount.ToString());
    this.SwitchButton(EffectCategory.Weapon);
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Label("Bullet count: " + this.bulletCount.ToString());
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Label("Bullet explosion count: " + this.bulletExplCount.ToString());
    this.SwitchButton(EffectCategory.BulletExplosion);
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Label("Missile explosion count: " + this.missileExplCount.ToString());
    this.SwitchButton(EffectCategory.MissileExplosion);
    GUILayout.EndHorizontal();
    GUILayout.BeginVertical();
    GUILayout.EndArea();
  }

  private void Update()
  {
    if ((double) Time.time - (double) this.lastUpdateTime < (double) this.updateInterval)
      return;
    FXManager instance = FXManager.Instance;
    this.lastUpdateTime = Time.time;
    List<EffectState> effectStateList;
    if (instance.CategorizedEffects.TryGetValue(EffectCategory.Weapon, out effectStateList))
    {
      this.gunshotCount = effectStateList.Count;
      this.bulletCount = 0;
    }
    if (instance.CategorizedEffects.TryGetValue(EffectCategory.BulletExplosion, out effectStateList))
      this.bulletExplCount = effectStateList.Count;
    if (!instance.CategorizedEffects.TryGetValue(EffectCategory.MissileExplosion, out effectStateList))
      return;
    this.missileExplCount = effectStateList.Count;
  }

  private void SwitchButton(EffectCategory category)
  {
    FXManager instance = FXManager.Instance;
    bool flag;
    string text;
    if (instance.HaveCategory(category))
    {
      flag = false;
      text = "o";
    }
    else
    {
      flag = true;
      text = "|";
    }
    if (!GUILayout.Button(text))
      return;
    if (flag)
      instance.AddCategory(category);
    else
      instance.RemoveCategory(category);
  }
}
