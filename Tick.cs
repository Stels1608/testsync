﻿// Decompiled with JetBrains decompiler
// Type: Tick
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public struct Tick : IComparable<Tick>, IProtocolWrite, IProtocolRead
{
  private const double tickTime = 0.1;
  private const double ticksInSec = 10.0;
  public const float Delta = 0.1f;
  private static bool isLastValid;
  private static Tick last;
  private static Tick current;
  private static Tick next;
  private static bool isNewTick;
  private int value;

  public double Time
  {
    get
    {
      return (double) this.value * 0.1;
    }
  }

  public static Tick Current
  {
    get
    {
      return Tick.current;
    }
  }

  public static Tick Next
  {
    get
    {
      return Tick.next;
    }
  }

  public static Tick Last
  {
    get
    {
      return Tick.last;
    }
  }

  public Tick(int value)
  {
    this.value = value;
  }

  public static bool operator ==(Tick a, Tick b)
  {
    return a.value == b.value;
  }

  public static bool operator !=(Tick a, Tick b)
  {
    return a.value != b.value;
  }

  public static bool operator <(Tick a, Tick b)
  {
    return a.value < b.value;
  }

  public static bool operator <=(Tick a, Tick b)
  {
    return a.value <= b.value;
  }

  public static bool operator >=(Tick a, Tick b)
  {
    return a.value >= b.value;
  }

  public static bool operator >(Tick a, Tick b)
  {
    return a.value > b.value;
  }

  public static int operator -(Tick a, Tick b)
  {
    return a.value - b.value;
  }

  public static Tick operator +(Tick a, int b)
  {
    return new Tick(a.value + b);
  }

  public static Tick operator -(Tick a, int b)
  {
    return new Tick(a.value - b);
  }

  public static Tick operator ++(Tick a)
  {
    ++a.value;
    return a;
  }

  public static Tick operator -(Tick a)
  {
    --a.value;
    return a;
  }

  public override string ToString()
  {
    return string.Format("tick{0}", (object) this.value);
  }

  public static void Nearest(double time, out Tick previousTick, out Tick nextTick)
  {
    double d = time * 10.0;
    previousTick.value = (int) Math.Floor(d);
    nextTick.value = previousTick.value + 1;
  }

  public static Tick Previous(double time)
  {
    return new Tick((int) Math.Floor(time * 10.0));
  }

  public static bool IsNewTick()
  {
    return Tick.isNewTick;
  }

  public static void Init()
  {
  }

  public static void Update(double currentSectorTime)
  {
    if (Tick.isLastValid)
    {
      Tick.last = Tick.current;
      Tick.current = Tick.Previous(currentSectorTime);
      if (Tick.current < Tick.last)
        Tick.current = Tick.last;
      Tick.isNewTick = Tick.current != Tick.last;
      Tick.next = Tick.current + 1;
    }
    else
    {
      Tick.current = Tick.Previous(currentSectorTime);
      Tick.last = Tick.current - 1;
      Tick.next = Tick.current + 1;
      Tick.isNewTick = true;
      Tick.isLastValid = true;
      Log.Add("WARNING: ticks reset " + (object) Tick.Last + " current ");
    }
  }

  public static void Reset(double currentSectorTime)
  {
    Tick.isLastValid = false;
    Tick.Update(currentSectorTime);
  }

  public void Write(BgoProtocolWriter pw)
  {
    pw.Write(this.value);
  }

  public void Read(BgoProtocolReader pr)
  {
    this.value = pr.ReadInt32();
  }

  public override int GetHashCode()
  {
    return this.value;
  }

  public override bool Equals(object obj)
  {
    if (obj is Tick)
      return this.value == ((Tick) obj).value;
    return false;
  }

  public int CompareTo(Tick other)
  {
    if (this.value < other.value)
      return -1;
    return this.value > other.value ? 1 : 0;
  }
}
