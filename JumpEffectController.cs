﻿// Decompiled with JetBrains decompiler
// Type: JumpEffectController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class JumpEffectController : MonoBehaviour
{
  [SerializeField]
  private float duration = 2.5f;
  private bool destroyOnFinish = true;
  [SerializeField]
  private FtlModelWarpEffect warpEffect;
  [SerializeField]
  private DelayedAudioPlay delayedAudioPlayer;
  [SerializeField]
  private bool debugPlayOnEnable;
  private float startTime;
  private bool finished;
  public AnonymousDelegate OnFinished;
  private RootScriptBase rootScript;
  private GameObject currentModel;
  private JumpEffectNew jumpEffect;

  private GameObject ModelToWarp
  {
    get
    {
      if ((Object) this.jumpEffect.DictateModelToWarp == (Object) null)
        return this.rootScript.Model;
      return this.jumpEffect.DictateModelToWarp;
    }
  }

  private GameObject ParticleParent
  {
    get
    {
      if ((Object) this.jumpEffect.DictateParticleParent == (Object) null)
        return this.rootScript.gameObject;
      return this.jumpEffect.DictateParticleParent;
    }
  }

  public DelayedAudioPlay DelayedAudioPlayer
  {
    get
    {
      return this.delayedAudioPlayer;
    }
  }

  private void Awake()
  {
    this.startTime = Time.realtimeSinceStartup;
    this.OnFinished = (AnonymousDelegate) (() => {});
    this.destroyOnFinish = (Object) GameLevel.Instance != (Object) null;
  }

  private void LateUpdate()
  {
    if (this.finished)
      return;
    if ((Object) this.rootScript != (Object) null && (Object) this.rootScript.Model != (Object) null && (Object) this.rootScript.Model != (Object) this.currentModel)
    {
      this.currentModel = this.rootScript.Model;
      this.OnModelChange();
    }
    this.AlignParticles();
    FtlModelWarpEffect ftlModelWarpEffect = this.warpEffect;
    bool flag = this.debugPlayOnEnable;
    this.delayedAudioPlayer.DebugPlayOnEnable = flag;
    int num = flag ? 1 : 0;
    ftlModelWarpEffect.DebugPlayOnEnable = num != 0;
    if ((double) Time.realtimeSinceStartup - (double) this.startTime <= (double) this.duration)
      return;
    this.finished = true;
    this.OnFinished();
    if (!this.destroyOnFinish)
      return;
    Object.Destroy((Object) this.gameObject);
  }

  public void Play(RootScriptBase rootScript)
  {
    this.rootScript = rootScript;
    this.OnModelChange();
    this.AlignParticles();
    this.warpEffect.Play(this.ModelToWarp.transform);
  }

  private void AlignParticles()
  {
    if (!((Object) this.jumpEffect != (Object) null))
      return;
    this.transform.position = this.ParticleParent.transform.position;
    this.transform.rotation = this.ParticleParent.transform.rotation;
  }

  private void OnModelChange()
  {
    this.currentModel = this.rootScript.Model;
    this.jumpEffect = this.rootScript.Model.GetComponentInChildren<JumpEffectNew>();
    if ((Object) this.jumpEffect == (Object) null)
    {
      Debug.LogError((object) ("jump Effect was null. Model: " + (object) this.rootScript.Model));
    }
    else
    {
      this.warpEffect.SetModelToWarp(this.ModelToWarp.transform);
      this.AlignParticles();
    }
  }
}
