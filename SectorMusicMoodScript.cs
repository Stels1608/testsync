﻿// Decompiled with JetBrains decompiler
// Type: SectorMusicMoodScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (MusicBox))]
public class SectorMusicMoodScript : MonoBehaviour
{
  public float MoodTimeout = 10f;
  public SectorMusicMoodScript.OperationMode operationMode = SectorMusicMoodScript.OperationMode.Normal;
  private const float updateTensionInterval = 1f;
  private MusicBox musicBox;
  private float moodStartTime;
  private float lastTensionUpdateTime;

  private void Awake()
  {
    this.musicBox = this.GetComponent<MusicBox>();
    this.musicBox.Mood = MusicBoxMood.Calm;
  }

  private void Update()
  {
    PlayerShip playerShip = SpaceLevel.GetLevel().GetPlayerShip();
    if (playerShip == null)
      return;
    if (this.operationMode == SectorMusicMoodScript.OperationMode.Normal)
    {
      if (playerShip.Props.InCombat)
      {
        this.ChangeMood(MusicBoxMood.Battle);
      }
      else
      {
        if ((double) Time.time <= (double) this.lastTensionUpdateTime + 1.0)
          return;
        this.lastTensionUpdateTime = Time.time;
        if (DradisManager.AreHostilesInRange)
          this.ChangeMood(MusicBoxMood.Tension);
        else
          this.ChangeMood(MusicBoxMood.Calm);
      }
    }
    else
      this.ChangeMood((MusicBoxMood) this.operationMode);
  }

  private void ChangeMood(MusicBoxMood value)
  {
    if (this.musicBox.Mood == value || (double) Time.time <= (double) this.moodStartTime + (double) this.MoodTimeout)
      return;
    this.moodStartTime = Time.time;
    this.musicBox.Mood = value;
  }

  public enum OperationMode
  {
    TestCalm,
    TestTension,
    TestBattle,
    Normal,
  }
}
