﻿// Decompiled with JetBrains decompiler
// Type: StoryMissileTutorial
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StoryMissileTutorial
{
  private static Dictionary<uint, StoryMissileTutorial> runningSlotTutorials = new Dictionary<uint, StoryMissileTutorial>();
  private const float messageInterval = 3f;
  private Timer runningUpdater;
  private readonly ShipSlot missileSlot;
  private readonly uint slotId;
  private float lastMessageTime;
  private readonly string pressToFireText;
  private readonly string missileOnCooldownText;

  private StoryMissileTutorial(uint slotId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    StoryMissileTutorial.\u003CStoryMissileTutorial\u003Ec__AnonStorey10D tutorialCAnonStorey10D = new StoryMissileTutorial.\u003CStoryMissileTutorial\u003Ec__AnonStorey10D();
    // ISSUE: reference to a compiler-generated field
    tutorialCAnonStorey10D.slotId = slotId;
    // ISSUE: explicit constructor call
    base.\u002Ector();
    // ISSUE: reference to a compiler-generated field
    this.slotId = tutorialCAnonStorey10D.slotId;
    // ISSUE: reference to a compiler-generated method
    this.missileSlot = Game.Me.Hangar.ActiveShip.Slots.First<ShipSlot>(new Func<ShipSlot, bool>(tutorialCAnonStorey10D.\u003C\u003Em__27C));
    if (this.missileSlot == null)
    {
      // ISSUE: reference to a compiler-generated field
      Debug.LogError((object) ("StoryMissileTutorial: Couldn't find a shipSlot with id " + (object) tutorialCAnonStorey10D.slotId));
    }
    else
    {
      string hotKeyFor = Tools.GetHotKeyFor(this.missileSlot.Action);
      this.pressToFireText = BsgoLocalization.Get("%$bgo.tutorial.notification.press_to_fire_missile%", (object) hotKeyFor);
      this.missileOnCooldownText = BsgoLocalization.Get("%$bgo.tutorial.notification.missile_launcher_still_reloading%", (object) hotKeyFor);
    }
  }

  public static void EnableTutorialForSlot(uint slotId)
  {
    if (StoryMissileTutorial.runningSlotTutorials.ContainsKey(slotId))
    {
      Debug.LogError((object) ("Couldn't enable a tutorial for slot " + (object) slotId + ". Another tutorial is already running on this slot."));
    }
    else
    {
      StoryMissileTutorial storyMissileTutorial = new StoryMissileTutorial(slotId);
      storyMissileTutorial.Start();
      StoryMissileTutorial.runningSlotTutorials.Add(slotId, storyMissileTutorial);
    }
  }

  public static void DisableTutorialForSlot(uint slotId)
  {
    if (!StoryMissileTutorial.runningSlotTutorials.ContainsKey(slotId))
      return;
    StoryMissileTutorial.runningSlotTutorials[slotId].Stop();
    StoryMissileTutorial.runningSlotTutorials.Remove(slotId);
  }

  private void Start()
  {
    if ((UnityEngine.Object) this.runningUpdater != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.runningUpdater);
    this.missileSlot.Ability.OnTouch += this.TouchedWhileCooldownNotification();
    this.runningUpdater = Timer.CreateTimer("StoryMissileTutorial: SlotTutorialTimer " + (object) this.slotId, 0.1f, 0.1f, (Timer.TickHandler) (() => this.Updater(this.missileSlot)));
  }

  private void Stop()
  {
    if ((UnityEngine.Object) this.runningUpdater != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.runningUpdater);
    this.missileSlot.Ability.OnTouch -= this.TouchedWhileCooldownNotification();
    Game.GUIManager.Find<GUISmallPaperdoll>().SetBlink(this.slotId, false);
  }

  private void Updater(ShipSlot slot)
  {
    bool canCast = slot.Ability.CanCast;
    Game.GUIManager.Find<GUISmallPaperdoll>().SetBlink(this.slotId, canCast);
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    if (canCast && (double) realtimeSinceStartup - (double) this.lastMessageTime > 3.0)
    {
      this.lastMessageTime = realtimeSinceStartup;
      FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification(this.pressToFireText, NotificationCategory.Neutral));
    }
    if (canCast)
      return;
    this.lastMessageTime = 0.0f;
  }

  private AnonymousDelegate TouchedWhileCooldownNotification()
  {
    return (AnonymousDelegate) (() =>
    {
      if (!this.missileSlot.Ability.IsCooling)
        return;
      FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification(this.missileOnCooldownText, NotificationCategory.Neutral));
    });
  }
}
