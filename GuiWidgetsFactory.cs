﻿// Decompiled with JetBrains decompiler
// Type: GuiWidgetsFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GuiWidgetsFactory
{
  public static UILabel AddLabel(GameObject parent, Vector2 dimensions, Color color, int depth = 0, int fontSize = 16, UIWidget.Pivot pivot = UIWidget.Pivot.TopLeft, UILabel.Overflow overflowStrategy = UILabel.Overflow.ResizeHeight)
  {
    UILabel uiLabel = parent.AddComponent<UILabel>();
    uiLabel.pivot = pivot;
    uiLabel.depth = depth;
    uiLabel.trueTypeFont = Gui.Options.fontEurostileTRegCon;
    uiLabel.fontSize = fontSize;
    uiLabel.overflowMethod = UILabel.Overflow.ResizeHeight;
    uiLabel.keepCrispWhenShrunk = UILabel.Crispness.Always;
    uiLabel.color = color;
    uiLabel.width = (int) dimensions.x;
    uiLabel.height = (int) dimensions.y;
    return uiLabel;
  }
}
