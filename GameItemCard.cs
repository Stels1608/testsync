﻿// Decompiled with JetBrains decompiler
// Type: GameItemCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GameItemCard : Card
{
  private GUICard m_GUICard;
  private ShopItemCard mCard;

  public GUICard ItemGUICard
  {
    get
    {
      return this.m_GUICard;
    }
    set
    {
      this.m_GUICard = value;
    }
  }

  public ShopItemCard ShopItemCard
  {
    get
    {
      return this.mCard;
    }
    set
    {
      this.mCard = value;
    }
  }

  public GameItemCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.m_GUICard = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
    this.mCard = (ShopItemCard) Game.Catalogue.FetchCard(this.CardGUID, CardView.Price);
    this.IsLoaded.Depend((ILoadable) this.m_GUICard, (ILoadable) this.mCard);
    this.IsLoaded.Set();
  }

  public virtual bool SetItemIcon(ref GuiImage image, Vector2 elementSize)
  {
    return this.m_GUICard.SetGuiImage(ref image, elementSize);
  }
}
