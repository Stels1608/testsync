﻿// Decompiled with JetBrains decompiler
// Type: ParticleLODShuriken
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

[AddComponentMenu("+LOD/ParticleShuriken")]
public class ParticleLODShuriken : MonoBehaviour
{
  private static bool m_globalEnable = true;
  public bool m_oneShot = true;
  private bool m_particlesEnabled = true;
  public Transform m_LodListener;
  public ParticleSystem[] m_emittersMinimal;
  public ParticleSystem[] m_emittersLow;
  public ParticleSystem[] m_emittersHigh;
  private bool m_HasCollider;
  private bool m_IfSelfDest;
  private bool m_childCount;
  private bool m_rendererOn;

  public static bool GlobalEnable
  {
    get
    {
      return ParticleLODShuriken.m_globalEnable;
    }
    set
    {
      ParticleLODShuriken.m_globalEnable = value;
    }
  }

  public int InheritedLODLevel { get; set; }

  public void Expire()
  {
    foreach (ParticleSystem emitter in this.GetEmitters())
    {
      if (!((Object) emitter == (Object) null))
      {
        emitter.transform.parent = (Transform) null;
        emitter.enableEmission = false;
        emitter.GetComponent<Renderer>().enabled = false;
        Object.Destroy((Object) emitter.gameObject, 5f);
      }
    }
    Object.Destroy((Object) this);
  }

  private void HideEmitters(ParticleSystem[] emitters)
  {
    for (int index = 0; index <= emitters.Length - 1; ++index)
    {
      if ((Object) emitters[index] != (Object) null)
      {
        emitters[index].GetComponent<Renderer>().shadowCastingMode = ShadowCastingMode.Off;
        emitters[index].GetComponent<Renderer>().receiveShadows = false;
        emitters[index].enableEmission = false;
        emitters[index].GetComponent<Renderer>().enabled = false;
        emitters[index].playOnAwake = true;
      }
    }
  }

  private void ShowEmitters(ParticleSystem[] emitters)
  {
    for (int index = 0; index <= emitters.Length - 1; ++index)
    {
      if ((Object) emitters[index] != (Object) null)
      {
        emitters[index].GetComponent<Renderer>().shadowCastingMode = ShadowCastingMode.Off;
        emitters[index].GetComponent<Renderer>().receiveShadows = false;
        emitters[index].enableEmission = true;
        emitters[index].GetComponent<Renderer>().enabled = true;
        emitters[index].playOnAwake = false;
      }
    }
  }

  public void DoLOD()
  {
    if (Game.GraphicsQuality >= GraphicsQuality.High && this.InheritedLODLevel <= 1)
    {
      this.HideEmitters(this.m_emittersMinimal);
      this.HideEmitters(this.m_emittersLow);
      this.ShowEmitters(this.m_emittersHigh);
    }
    else if (Game.GraphicsQuality >= GraphicsQuality.Medium && this.InheritedLODLevel <= 2)
    {
      this.HideEmitters(this.m_emittersHigh);
      this.HideEmitters(this.m_emittersMinimal);
      this.ShowEmitters(this.m_emittersLow);
    }
    else
    {
      this.HideEmitters(this.m_emittersHigh);
      this.HideEmitters(this.m_emittersLow);
      this.ShowEmitters(this.m_emittersMinimal);
    }
  }

  public void Start()
  {
    this.DoLOD();
  }

  public void Awake()
  {
    if ((Object) this.m_LodListener == (Object) null)
      this.m_LodListener = this.gameObject.transform;
    ParticleSystem component = this.gameObject.GetComponent<ParticleSystem>();
    if ((Object) component != (Object) null && component.GetComponent<Renderer>().enabled)
      this.m_rendererOn = true;
    if (!this.m_oneShot)
      this.enabled = false;
    if ((bool) ((Object) this.gameObject.GetComponent<Collider>()))
      this.m_HasCollider = true;
    if ((bool) ((Object) this.gameObject.GetComponent<SelfDestruct>()))
      this.m_IfSelfDest = true;
    if (this.gameObject.transform.childCount > 0)
      this.m_childCount = true;
    if (this.m_childCount && (Object) component != (Object) null)
    {
      Object.Destroy((Object) component);
      if ((this.m_rendererOn || !this.m_IfSelfDest) && (!this.m_HasCollider && (Object) this.gameObject.GetComponent<MeshRenderer>() == (Object) null))
      {
        MeshRenderer meshRenderer = this.gameObject.AddComponent<MeshRenderer>();
        meshRenderer.shadowCastingMode = ShadowCastingMode.Off;
        meshRenderer.receiveShadows = false;
      }
    }
    if (!this.m_HasCollider)
      return;
    this.SetChildrenActive(false);
  }

  private void LODSwitcher()
  {
    if ((Object) this.m_LodListener.GetComponent<Body>() != (Object) null)
    {
      this.InheritedLODLevel = this.m_LodListener.GetComponent<Body>().Level;
      this.DoLOD();
    }
    else if ((Object) this.m_LodListener.GetComponent<BodyStringRef>() != (Object) null)
    {
      this.InheritedLODLevel = this.m_LodListener.GetComponent<BodyStringRef>().Level;
      this.DoLOD();
    }
    else
    {
      if (!((Object) this.m_LodListener.GetComponent<MultiBody>() != (Object) null))
        return;
      this.InheritedLODLevel = this.m_LodListener.GetComponent<MultiBody>().Level;
      this.DoLOD();
    }
  }

  public void Update()
  {
    this.LODSwitcher();
    if (this.m_particlesEnabled == ParticleLODShuriken.m_globalEnable)
      return;
    this.m_particlesEnabled = ParticleLODShuriken.m_globalEnable;
  }

  private void OnTriggerEnter(Collider other)
  {
    if (!this.m_particlesEnabled || !(other.gameObject.tag == "MainCamera"))
      return;
    this.SetChildrenActive(true);
  }

  private void OnTriggerExit(Collider other)
  {
    if (!(other.gameObject.tag == "MainCamera"))
      return;
    this.SetChildrenActive(false);
  }

  public ParticleSystem[] GetEmitters()
  {
    return ((IEnumerable<ParticleSystem>) this.m_emittersHigh).Concat<ParticleSystem>((IEnumerable<ParticleSystem>) this.m_emittersLow).Concat<ParticleSystem>((IEnumerable<ParticleSystem>) this.m_emittersMinimal).ToArray<ParticleSystem>();
  }

  public void OnBecameVisible()
  {
    if (this.m_IfSelfDest || !this.m_particlesEnabled || (!this.m_childCount || this.m_HasCollider))
      return;
    this.SetChildrenActive(true);
  }

  public void OnBecameInvisible()
  {
    if (this.m_IfSelfDest || !this.gameObject.activeInHierarchy || (!this.m_childCount || this.m_HasCollider))
      return;
    this.SetChildrenActive(false);
  }

  private void SetChildrenActive(bool active)
  {
    foreach (Component component in this.transform)
      component.gameObject.SetActive(active);
  }
}
