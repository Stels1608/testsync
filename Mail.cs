﻿// Decompiled with JetBrains decompiler
// Type: Mail
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class Mail : IServerItem, ILoadable, IComparable<Mail>, IProtocolRead
{
  private ushort serverID;
  public MailTemplateCard Card;
  private Flag isLoaded;
  public DateTime Received;
  public Mail.MailStatus Status;
  public Mail.MailContainer Items;
  public string[] Parameters;

  public ushort ServerID
  {
    get
    {
      return this.serverID;
    }
  }

  public Flag IsLoaded
  {
    get
    {
      return this.isLoaded;
    }
  }

  public int Expires
  {
    get
    {
      return (int) this.Card.Expire - Mathf.FloorToInt((float) (Game.TimeSync.ServerDateTime - this.Received).TotalDays);
    }
  }

  public Mail()
  {
    this.isLoaded = new Flag();
  }

  public void Read(BgoProtocolReader r)
  {
    this.serverID = r.ReadUInt16();
    uint cardGUID = r.ReadGUID();
    this.Status = (Mail.MailStatus) r.ReadByte();
    this.Received = r.ReadDateTime();
    this.Items = new Mail.MailContainer(this.serverID);
    this.Items._AddItems((SmartList<ShipItem>) ItemFactory.ReadItemList(r));
    this.Parameters = r.ReadStringArray();
    this.Card = (MailTemplateCard) Game.Catalogue.FetchCard(cardGUID, CardView.MailTemplate);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.Card
    });
    this.IsLoaded.Set();
  }

  public int CompareTo(Mail other)
  {
    int num = this.Received.CompareTo(other.Received);
    if (num == 0)
      num = this.serverID.CompareTo(other.serverID);
    return num;
  }

  public class MailContainer : ItemList
  {
    public MailContainer(ushort id)
      : base((IContainerID) new Mail.MailContainer.MailContainerID(id))
    {
    }

    public class MailContainerID : IContainerID, IProtocolWrite
    {
      private ushort id;

      public MailContainerID(ushort id)
      {
        this.id = id;
      }

      void IProtocolWrite.Write(BgoProtocolWriter w)
      {
        w.Write((byte) 7);
        w.Write(this.id);
      }

      public ContainerType GetContainerType()
      {
        return ContainerType.Mail;
      }
    }
  }

  public enum MailStatus
  {
    Normal,
    Unread,
  }
}
