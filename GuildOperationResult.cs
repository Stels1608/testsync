﻿// Decompiled with JetBrains decompiler
// Type: GuildOperationResult
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum GuildOperationResult : byte
{
  Ok = 1,
  InvalidPermissions = 2,
  NotInGuild = 3,
  ErrorLeaderPermissions = 4,
}
