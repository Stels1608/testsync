﻿// Decompiled with JetBrains decompiler
// Type: Gui.SRect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Gui
{
  public class SRect
  {
    private string name = string.Empty;
    private string action = string.Empty;
    [HideInInspector]
    private bool needRecachingRect = true;
    [HideInInspector]
    private bool needRepositioning = true;
    [HideInInspector]
    public AnonymousDelegate WhenElementChanges = (AnonymousDelegate) (() => {});
    [HideInInspector]
    private bool getInheritedFields = true;
    [HideInInspector]
    private SRect parent;
    [HideInInspector]
    private bool requireBackRepositioning;
    private Vector2 position;
    private Vector2 size;
    [SerializeField]
    private Align align;
    [HideInInspector]
    private Rect absRect;
    [HideInInspector]
    private bool repositioning;
    [HideInInspector]
    private List<MemberInfo> fieldMembers;

    public SRect Parent
    {
      get
      {
        return this.parent;
      }
      set
      {
        this.parent = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public bool RequireBackRepositioning
    {
      get
      {
        return this.requireBackRepositioning;
      }
      set
      {
        this.requireBackRepositioning = value;
      }
    }

    [Gui2Editor]
    public Align Align
    {
      get
      {
        return this.align;
      }
      set
      {
        if (this.align == value)
          return;
        this.align = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public virtual Vector2 Position
    {
      get
      {
        if (this.needRepositioning)
          this.RepositionBase();
        return this.position;
      }
      set
      {
        if (this.position.Equals((object) value))
          return;
        this.position = value;
        this.ForceReposition();
      }
    }

    public float PositionX
    {
      get
      {
        if (this.needRepositioning)
          this.RepositionBase();
        return this.position.x;
      }
      set
      {
        if ((double) this.position.x == (double) value)
          return;
        this.position.x = value;
        this.ForceReposition();
      }
    }

    public float PositionY
    {
      get
      {
        if (this.needRepositioning)
          this.RepositionBase();
        return this.position.y;
      }
      set
      {
        if ((double) this.position.y == (double) value)
          return;
        this.position.y = value;
        this.ForceReposition();
      }
    }

    public Vector2 PositionCenter
    {
      get
      {
        return this.Position + this.Size / 2f;
      }
      set
      {
        this.Position = value - this.Size / 2f;
      }
    }

    [Gui2Editor]
    public Vector2 Size
    {
      get
      {
        if (this.needRepositioning)
          this.RepositionBase();
        return this.size;
      }
      set
      {
        if (this.size.Equals((object) value))
          return;
        this.size = value;
        this.ForceReposition();
      }
    }

    public float SizeX
    {
      get
      {
        if (this.needRepositioning)
          this.RepositionBase();
        return this.size.x;
      }
      set
      {
        if ((double) this.size.x == (double) value)
          return;
        this.size.x = value;
        this.ForceReposition();
      }
    }

    public float SizeY
    {
      get
      {
        if (this.needRepositioning)
          this.RepositionBase();
        return this.size.y;
      }
      set
      {
        if ((double) this.size.y == (double) value)
          return;
        this.size.y = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    [Gui2Editor]
    public string ActionString
    {
      get
      {
        return this.action;
      }
      set
      {
        this.action = value;
      }
    }

    [Gui2Editor(write = false)]
    public Rect Rect
    {
      get
      {
        this.EnsurePositioned();
        return this.absRect;
      }
    }

    public Vector2 PositionNormal
    {
      get
      {
        Vector2 parentSize = this.GetParentSize();
        switch (this.align)
        {
          case Align.UpLeft:
            return this.position;
          case Align.UpCenter:
            return new Vector2((float) ((double) parentSize.x / 2.0 - (double) this.Size.x / 2.0) + this.Position.x, this.Position.y);
          case Align.UpRight:
            return new Vector2(parentSize.x - this.Size.x + this.Position.x, this.Position.y);
          case Align.MiddleLeft:
            return new Vector2(this.Position.x, (float) ((double) parentSize.y / 2.0 - (double) this.Size.y / 2.0) + this.Position.y);
          case Align.MiddleCenter:
            return new Vector2((float) ((double) parentSize.x / 2.0 - (double) this.Size.x / 2.0) + this.Position.x, (float) ((double) parentSize.y / 2.0 - (double) this.Size.y / 2.0) + this.Position.y);
          case Align.MiddleRight:
            return new Vector2(parentSize.x - this.Size.x + this.Position.x, (float) ((double) parentSize.y / 2.0 - (double) this.Size.y / 2.0) + this.Position.y);
          case Align.DownLeft:
            return new Vector2(this.Position.x, parentSize.y - this.Size.y + this.Position.y);
          case Align.DownCenter:
            return new Vector2((float) ((double) parentSize.x / 2.0 - (double) this.Size.x / 2.0) + this.Position.x, parentSize.y - this.Size.y + this.Position.y);
          case Align.DownRight:
            return new Vector2(parentSize.x - this.Size.x + this.Position.x, parentSize.y - this.Size.y + this.Position.y);
          default:
            throw new Exception(this.GetType().ToString() + ": strange align.");
        }
      }
    }

    public float PositionNormalX
    {
      get
      {
        return this.PositionNormal.x;
      }
    }

    public float PositionNormalY
    {
      get
      {
        return this.PositionNormal.y;
      }
    }

    public SRect(GuiElementBase parent)
      : this(Vector2.zero, Vector2.zero, parent)
    {
    }

    public SRect(Vector2 position, Vector2 size)
      : this(position, size, (GuiElementBase) null)
    {
    }

    public SRect(Rect rect)
      : this(new Vector2(rect.x, rect.y), new Vector2(rect.width, rect.height), (GuiElementBase) null)
    {
    }

    public SRect(SRect copy)
    {
      this.parent = copy.parent;
      this.requireBackRepositioning = copy.requireBackRepositioning;
      this.name = copy.name;
      this.action = copy.action;
      this.align = copy.align;
      this.WhenElementChanges = copy.WhenElementChanges;
      this.position = copy.position;
      this.size = copy.size;
      this.needRecachingRect = true;
      this.needRepositioning = true;
    }

    public SRect(JElementBase json)
    {
      this.parent = (SRect) null;
      this.Position = json.position.ToV2();
      this.Size = json.size.ToV2();
      this.Name = json.name;
      this.action = json.action;
      this.align = json.alignment;
    }

    public SRect(Vector2 position, Vector2 size, GuiElementBase parent)
    {
      this.parent = (SRect) parent;
      this.position = position;
      this.size = size;
    }

    public List<MemberInfo> GetFieldMembers(bool inherited)
    {
      if (this.fieldMembers == null || inherited != this.getInheritedFields)
      {
        this.fieldMembers = new List<MemberInfo>();
        this.FillFieldMemberList(inherited);
        this.getInheritedFields = inherited;
      }
      return this.fieldMembers;
    }

    private void FillFieldMemberList(bool inherited)
    {
      BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
      System.Type type = this.GetType();
      while (type != typeof (SRect).BaseType)
      {
        MemberInfo[] members = type.FindMembers(MemberTypes.Field, bindingAttr, (MemberFilter) null, (object) null);
        for (int index1 = members.Length - 1; index1 >= 0; --index1)
        {
          if (members[index1] is FieldInfo)
          {
            Attribute[] customAttributes = Attribute.GetCustomAttributes(members[index1], true);
            bool flag = true;
            for (int index2 = 0; index2 < customAttributes.Length; ++index2)
            {
              if (customAttributes[index2] is HideInInspector)
                flag = false;
            }
            if (flag)
              this.fieldMembers.Add(members[index1]);
          }
        }
        type = type.BaseType;
        if (!inherited)
          break;
      }
      this.fieldMembers.Reverse();
    }

    public virtual void LocalizeElement()
    {
    }

    protected virtual void EnsurePositioned()
    {
      if (this.needRepositioning)
        this.RepositionBase();
      if (!this.needRecachingRect)
        return;
      this.DoCaching();
      if (this.WhenElementChanges == null)
        return;
      this.WhenElementChanges();
    }

    protected void RepositionBase()
    {
      if (this.repositioning)
        return;
      if (!GUIManager.DebugProfile)
        ;
      this.repositioning = true;
      this.Reposition();
      this.repositioning = false;
      if (!GUIManager.DebugProfile)
        ;
      this.needRepositioning = false;
      this.needRecachingRect = true;
    }

    private Vector2 GetParentSize()
    {
      if (this.parent != null)
        return this.parent.Size;
      if ((UnityEngine.Object) GameLevel.Instance != (UnityEngine.Object) null && Game.GUIManager != null)
        return new Vector2((float) Game.GUIManager.Resolution.width, (float) Game.GUIManager.Resolution.height);
      return new Vector2((float) Screen.width, (float) Screen.height);
    }

    public virtual void Reposition()
    {
    }

    public virtual void DoCaching()
    {
      Vector2 positionNormal = this.PositionNormal;
      if (this.parent == null)
      {
        Rect rect = new Rect(positionNormal.x, positionNormal.y, this.size.x, this.size.y);
        if (rect != this.absRect)
          this.absRect = rect;
      }
      else
      {
        Rect rect = this.parent.Rect;
        rect = new Rect(rect.x + positionNormal.x, rect.y + positionNormal.y, this.size.x, this.size.y);
        if (rect != this.absRect)
          this.absRect = rect;
      }
      this.needRecachingRect = false;
    }

    public override string ToString()
    {
      return this.GetType().ToString() + (!string.IsNullOrEmpty(this.name) ? " '" + this.name + "'" : string.Empty);
    }

    [Gui2Editor]
    public virtual void ForceReposition()
    {
      this.ForceRepositionInt();
    }

    private void ForceRepositionInt()
    {
      if (this.repositioning)
        return;
      this.needRepositioning = true;
      this.needRecachingRect = true;
      if (this.parent == null || !this.parent.RequireBackRepositioning)
        return;
      this.parent.ForceRepositionInt();
    }
  }
}
