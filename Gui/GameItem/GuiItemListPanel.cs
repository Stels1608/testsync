﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiItemListPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System.Collections.Generic;
using UnityEngine;

namespace Gui.GameItem
{
  [UseInGuiEditor("GUI/_NewGui/layout/GameItemLayouts/itemListPanelLayout")]
  public class GuiItemListPanel : GuiPanel
  {
    [HideInInspector]
    private List<GuiItemPanelBase> m_itemList = new List<GuiItemPanelBase>();
    [HideInInspector]
    private GuiScrollPanel m_scrollPanel;
    [HideInInspector]
    private GuiItemPanelBase m_itemPanel;
    [HideInInspector]
    private ShopInventoryContainer m_currentContainer;

    public GuiItemListPanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiItemListPanel>())
    {
      this.Initialize();
    }

    public GuiItemListPanel(GuiItemListPanel copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiItemListPanel(JItemListPanel json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiItemListPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JItemListPanel((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      this.m_itemPanel.IsRendered = true;
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
    }

    private void Initialize()
    {
      this.m_scrollPanel = this.Find<GuiScrollPanel>();
      this.m_itemPanel = this.m_scrollPanel.FindScrollChild<GuiItemPanelBase>();
      this.m_itemPanel.IsRendered = false;
    }

    public void SetItemList(List<GameItemCard> itemList, ShopInventoryContainer container)
    {
      if (itemList == null)
        return;
      int count1 = itemList.Count;
      int count2 = this.m_itemList.Count;
      for (int index = count1; index < count2; ++index)
      {
        this.m_scrollPanel.RemoveScrollChild((GuiElementBase) this.m_itemList[index]);
        this.m_itemList.RemoveAt(index);
      }
      float num = 0.0f;
      for (int index = 0; index < itemList.Count; ++index)
      {
        if (this.m_itemList.Count <= index)
        {
          this.m_itemList.Add((GuiItemPanelBase) this.m_itemPanel.CloneElement());
          this.m_scrollPanel.AddScrollChild((GuiElementBase) this.m_itemList[index]);
          this.m_itemList[index].IsRendered = true;
          this.m_itemList[index].Position = this.m_itemList[index].ElementPadding.TopLeft;
          this.m_itemList[index].PositionY += (float) (double) num;
          num = this.m_itemList[index].BottomRightCornerWithPadding.y;
        }
        if (itemList[index] != this.m_itemList[index].ItemCard || this.m_currentContainer != container)
        {
          this.m_itemList[index].CurrentContainer = container;
          this.m_itemList[index].SlotNumber = index;
          this.m_itemList[index].SetProperties(itemList[index], true);
        }
      }
      this.m_currentContainer = container;
    }
  }
}
