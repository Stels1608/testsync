﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiShopItemTitlePanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.GameItem
{
  public class GuiShopItemTitlePanel : GuiItemPanelBase
  {
    private const string mc_panelLayout = "GUI/Tooltips/ShopPanels/AdvancedShopTitlePanelLayout";
    private const string mc_itemImageName = "itemLogo";
    private const string mc_itemTitleName = "itemTitle";
    private const string mc_levelLabelName = "levelLabel";
    private GuiImage m_itemImage;
    private GuiLabel m_itemTitle;
    private GuiLabel m_levelLabel;

    public GuiShopItemTitlePanel()
      : base("GUI/Tooltips/ShopPanels/AdvancedShopTitlePanelLayout")
    {
      this.m_itemImage = this.Find<GuiImage>("itemLogo");
      this.m_itemTitle = this.Find<GuiLabel>("itemTitle");
      this.m_levelLabel = this.Find<GuiLabel>("levelLabel");
      this.ElementPadding = (GuiElementPadding) new Rect(10f, 1f, 10f, 1f);
    }

    public GuiShopItemTitlePanel(GuiShopItemTitlePanel copy)
      : base((GuiItemPanelBase) copy)
    {
      this.m_itemImage = this.Find<GuiImage>("itemLogo");
      this.m_itemTitle = this.Find<GuiLabel>("itemTitle");
      this.m_levelLabel = this.Find<GuiLabel>("levelLabel");
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiShopItemTitlePanel(this);
    }

    protected override void ResizePanel()
    {
      this.m_itemImage.Position = this.m_itemImage.ElementPadding.TopLeft;
      this.m_itemTitle.PositionX = this.m_itemImage.PositionX + this.m_itemImage.SizeX + this.m_itemImage.ElementPadding.Right + this.m_itemTitle.ElementPadding.Left;
      this.m_itemTitle.PositionY = this.m_itemTitle.ElementPadding.Top;
      if (this.m_levelLabel.IsRendered)
      {
        this.m_levelLabel.PositionX = this.m_itemImage.PositionY + this.m_itemImage.SizeX + this.m_itemImage.ElementPadding.Right + this.m_levelLabel.ElementPadding.Left;
        this.m_levelLabel.PositionY = this.m_itemTitle.SizeY + this.m_itemTitle.PositionY + this.m_itemTitle.ElementPadding.Bottom + this.m_levelLabel.ElementPadding.Top;
      }
      base.ResizePanel();
    }

    private void SetLevel(int level)
    {
      this.m_levelLabel.Text = "%$bgo.etc.dw_level% " + level.ToString();
      this.m_levelLabel.IsRendered = level > 0;
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      this.m_itemImage.IsRendered = !condensedVersion;
      this.m_levelLabel.IsRendered = !(item is ShipCard) && !condensedVersion;
      bool flag = this.SetProperties(item.ItemGUICard, GuiItemPanelBase.ms_atlasSourceRectSmall);
      this.IsRendered = flag;
      return flag;
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      if (guiCard != null)
      {
        this.m_itemTitle.Text = guiCard.Name;
        this.m_itemImage.IsRendered = guiCard.SetGuiImage(ref this.m_itemImage, elementSize);
        this.SetLevel((int) guiCard.Level);
        this.ResizePanel();
      }
      return guiCard != null;
    }
  }
}
