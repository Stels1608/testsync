﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiShopStatsPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui.GameItem
{
  public class GuiShopStatsPanel : GuiItemPanelBase
  {
    private const string mc_panelLayout = "GUI/Tooltips/ShopPanels/AdvancedShopStatsPanelLayout";
    private const string mc_statLabelName = "statLabels";
    private const string mc_statDataName = "statData";
    private GuiLabel m_statLabels;
    private GuiLabel m_statData;

    public GuiShopStatsPanel()
      : base("GUI/Tooltips/ShopPanels/AdvancedShopStatsPanelLayout")
    {
      this.m_statLabels = this.Find<GuiLabel>("statLabels");
      this.m_statData = this.Find<GuiLabel>("statData");
      this.ElementPadding = (GuiElementPadding) new Rect(10f, 1f, 10f, 1f);
    }

    public GuiShopStatsPanel(GuiShopStatsPanel copy)
      : base((GuiItemPanelBase) copy)
    {
      this.m_statLabels = this.Find<GuiLabel>("statLabels");
      this.m_statData = this.Find<GuiLabel>("statData");
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiShopStatsPanel(this);
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      this.IsRendered = false;
      this.m_statLabels.Text = string.Empty;
      this.m_statData.Text = string.Empty;
      this.m_statLabels.WordWrap = false;
      this.m_statLabels.AutoSize = true;
      if (item is ShipSystem)
      {
        ShipSystem system = item as ShipSystem;
        if (system.Card.SlotType == ShipSlotType.ship_paint)
          return this.IsRendered;
        List<SystemsStatsGenerator.StatInfoDesc> descriptions = SystemsStatsGenerator.GenerateDescriptions(system);
        for (int index = 0; index < descriptions.Count; ++index)
        {
          this.m_statLabels.Text += descriptions[index].Name;
          if (descriptions[index].StatFormatedValue != "+0.0")
            this.m_statData.Text += descriptions[index].StatFormatedValue;
          if (index < descriptions.Count - 1)
          {
            this.m_statData.Text += "\n";
            this.m_statLabels.Text += "\n";
          }
        }
        this.IsRendered = true;
      }
      else if (item is ShipCard && !condensedVersion)
      {
        ShipCard shipCard = item as ShipCard;
        string str1 = string.Empty;
        using (List<Tuple<string, float>>.Enumerator enumerator = shipCard.Stats.ToPrintableList().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Tuple<string, float> current = enumerator.Current;
            GuiLabel guiLabel1 = this.m_statLabels;
            string str2 = guiLabel1.Text + str1 + current.First;
            guiLabel1.Text = str2;
            GuiLabel guiLabel2 = this.m_statData;
            string str3 = guiLabel2.Text + str1 + (object) current.Second;
            guiLabel2.Text = str3;
            str1 = "\n";
          }
        }
        this.IsRendered = this.m_statLabels.Text != string.Empty;
      }
      if (this.IsRendered)
      {
        this.m_statData.PositionX = this.m_statLabels.BottomRightCornerWithPadding.x + this.m_statData.ElementPadding.Left;
        this.ResizePanel();
      }
      return this.IsRendered;
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      return false;
    }
  }
}
