﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiShopIconAndLabelPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui.GameItem
{
  [UseInGuiEditor("GUI/Tooltips/ShopPanels/AdvancedShopIconAndLabelPanelLayout")]
  public class GuiShopIconAndLabelPanel : GuiItemPanelBase
  {
    private const string mc_iconName = "icon";
    private const string mc_labelName = "label";
    [HideInInspector]
    private GuiImage m_icon;
    [HideInInspector]
    private GuiLabel m_label;

    public Color TextColor
    {
      get
      {
        return this.m_label.NormalColor;
      }
      set
      {
        this.m_label.NormalColor = value;
      }
    }

    public GuiShopIconAndLabelPanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiShopIconAndLabelPanel>())
    {
      this.Initialize();
    }

    public GuiShopIconAndLabelPanel(GuiShopIconAndLabelPanel copy)
      : base((GuiItemPanelBase) copy)
    {
      this.Initialize();
    }

    public GuiShopIconAndLabelPanel(JIconAndLabelPanel json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiShopIconAndLabelPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JIconAndLabelPanel((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      this.ResizePanel();
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
    }

    private void Initialize()
    {
      this.m_icon = this.Find<GuiImage>("icon");
      this.m_label = this.Find<GuiLabel>("label");
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      this.SetProperties(item.ItemGUICard, this.m_icon.Size);
      if (item is ItemCountable)
        this.m_label.Text = "[" + (object) (item as ItemCountable).Count + "] " + item.ItemGUICard.Name;
      return true;
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      this.m_label.Text = guiCard.Name;
      guiCard.SetGuiImage(ref this.m_icon, elementSize);
      this.ResizePanel();
      return true;
    }

    public void SetProperties(GUICard card, Vector2 sourceRect, string label, bool useTextureSize = false)
    {
      this.SetProperties(new AtlasCache(float2.FromV2(sourceRect)), card.GUIAtlasTexturePath, (int) card.FrameIndex, label, useTextureSize);
    }

    public void SetProperties(AtlasCache atlasCache, string texturePath, int frameIndex, string label, bool useTextureSize = true)
    {
      Vector2 size = this.m_icon.Size;
      AtlasEntry cachedEntryBy = atlasCache.GetCachedEntryBy(texturePath, frameIndex);
      this.m_label.Text = Tools.ParseMessage(label);
      this.m_icon.Texture = cachedEntryBy.Texture;
      this.m_icon.SourceRect = cachedEntryBy.FrameRect;
      if (useTextureSize)
        this.m_icon.Size = float2.ToV2(atlasCache.ElementSize);
      else
        this.m_icon.Size = size;
      this.m_label.PositionX = this.m_icon.PositionX + this.m_icon.SizeX + this.m_icon.ElementPadding.Right + this.m_label.ElementPadding.Left;
      this.ResizePanel();
      this.IsRendered = true;
    }

    public void SetProperties(string label)
    {
      this.m_icon.SizeX = 0.0f;
      this.m_icon.SizeY = 0.0f;
      this.m_label.Text = label;
      this.m_label.Position = this.m_label.ElementPadding.TopLeft;
      this.ResizePanel();
      this.IsRendered = true;
    }

    private void CenterElementsVertically()
    {
      float num = Mathf.Max(this.m_icon.ElementWithPaddingSizeY, this.m_label.ElementWithPaddingSizeY);
      this.m_icon.PositionY = (float) (((double) num - (double) this.m_icon.SizeY) / 2.0);
      this.m_label.PositionY = (float) (((double) num - (double) this.m_label.SizeY) / 2.0);
    }

    private void RepositionElements()
    {
      this.CenterElementsVertically();
      this.m_icon.PositionX = this.m_icon.ElementPadding.Left;
      this.m_label.PositionX = !this.m_icon.IsRendered ? 0.0f : this.m_icon.BottomRightCornerWithPadding.x;
      this.m_label.PositionX += (float) (double) this.m_label.ElementPadding.Left;
      this.m_label.SizeX = this.SizeX - (this.m_label.PositionX + this.m_label.ElementPadding.Right);
    }

    protected override void ResizePanel()
    {
      this.RepositionElements();
      base.ResizePanel();
    }
  }
}
