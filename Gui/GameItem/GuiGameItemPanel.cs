﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiGameItemPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui.GameItem
{
  [UseInGuiEditor("GUI/_NewGui/layout/GameItemLayouts/gameItemPanelLayout.txt")]
  public class GuiGameItemPanel : GuiItemPanelBase
  {
    [HideInInspector]
    private GuiImage m_itemIcon;
    [HideInInspector]
    private GuiLabel m_itemName;
    [HideInInspector]
    private GuiLabel m_itemDescription;
    [HideInInspector]
    private GuiLabel m_extraContent;
    [HideInInspector]
    private GuiCostPanel m_costPanel;
    [HideInInspector]
    private GuiLabel m_itemNumber;
    [HideInInspector]
    private GuiButton m_backgroundButton;

    public GuiGameItemPanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiGameItemPanel>())
    {
      this.Initialize();
    }

    public GuiGameItemPanel(GuiGameItemPanel copy)
      : base((GuiItemPanelBase) copy)
    {
      this.Initialize();
    }

    public GuiGameItemPanel(JGameItemPanel json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiGameItemPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JGameItemPanel((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
    }

    public void Initialize()
    {
      this.m_itemIcon = this.Find<GuiImage>("itemIcon");
      this.m_itemName = this.Find<GuiLabel>("itemName");
      this.m_itemDescription = this.Find<GuiLabel>("itemDescription");
      this.m_extraContent = this.Find<GuiLabel>("extraContent");
      this.m_costPanel = this.Find<GuiCostPanel>("CostPanel");
      this.m_itemNumber = this.Find<GuiLabel>("itemNumber");
      this.m_backgroundButton = this.Find<GuiButton>("backgroundButton");
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      this.ItemCard = item;
      if (this.ItemCard == null)
      {
        Vector2 size = this.m_itemIcon.Size;
        this.m_itemIcon.Texture = ResourceLoader.Load<Texture2D>("GUI/Inventory/items_atlas");
        this.m_itemIcon.SourceRect = new Rect()
        {
          x = ((float) this.m_itemIcon.Texture.width - GuiItemPanelBase.ms_atlasSourceRectSmall.x) / (float) this.m_itemIcon.Texture.width,
          y = 0.0f,
          width = GuiItemPanelBase.ms_atlasSourceRectSmall.x / (float) this.m_itemIcon.Texture.width,
          height = GuiItemPanelBase.ms_atlasSourceRectSmall.y / (float) this.m_itemIcon.Texture.height
        };
        this.m_itemIcon.Size = size;
        this.m_itemNumber.IsRendered = true;
        this.m_itemNumber.Text = (this.SlotNumber + 1).ToString();
        this.m_itemNumber.PositionCenter = this.m_itemIcon.PositionCenter;
        this.m_itemName.IsRendered = false;
        this.m_itemDescription.IsRendered = false;
        this.m_extraContent.IsRendered = false;
        this.m_costPanel.IsRendered = false;
      }
      else
      {
        this.SetProperties(this.ItemCard.ItemGUICard, GuiItemPanelBase.ms_atlasSourceRectSmall);
        bool flag = this.ItemCard is ShipSystem && (this.ItemCard as ShipSystem).Card.SlotType == ShipSlotType.ship_paint;
        if (this.ItemCard is ItemCountable)
        {
          this.m_extraContent.Text = "%$bgo.shop.count%" + (object) (this.ItemCard as ItemCountable).Count;
          this.m_extraContent.IsRendered = true;
        }
        else if (this.ItemCard is ShipSystem && !flag)
        {
          this.m_extraContent.Text = "%$bgo.shop.level%" + (object) (this.ItemCard as ShipSystem).Card.Level + "   %$bgo.shop.max_level%" + (object) (item as ShipSystem).Card.MaxLevel;
          this.m_extraContent.IsRendered = true;
        }
        else
        {
          this.m_extraContent.Text = string.Empty;
          this.m_extraContent.IsRendered = false;
        }
        this.m_costPanel.SetProperties(this.ItemCard, true);
        if (this.ItemCard is ShipCard)
          this.ItemCard.IsLoaded.AddHandler((SignalHandler) (() => this.m_backgroundButton.SetShipToolTip(this.ItemCard as ShipCard, this.CurrentContainer)));
        else
          this.m_backgroundButton.SetStoreToolTip(this.ItemCard, this.CurrentContainer, false);
        this.m_backgroundButton.ToolTip.UseMousePosition = false;
        this.m_itemNumber.IsRendered = false;
      }
      bool flag1 = true;
      this.IsRendered = flag1;
      return flag1;
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      if (this.ItemCard != null)
        this.ItemCard.ItemGUICard = guiCard;
      if (guiCard != null)
      {
        Vector2 size = this.m_itemIcon.Size;
        guiCard.SetGuiImage(ref this.m_itemIcon, elementSize);
        this.m_itemIcon.Size = size;
        this.m_itemName.Text = guiCard.Name;
        this.m_itemName.IsRendered = true;
        this.m_itemDescription.Text = guiCard.ShortDescription;
        this.m_itemDescription.IsRendered = true;
        bool flag = true;
        this.IsRendered = flag;
        return flag;
      }
      bool flag1 = false;
      this.IsRendered = flag1;
      return flag1;
    }
  }
}
