﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiShopSalePanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui.GameItem
{
  [UseInGuiEditor("GUI/Tooltips/ShopPanels/AdvancedShopSalePanelLayout")]
  public class GuiShopSalePanel : GuiItemPanelBase
  {
    private const string mc_amountOffName = "amountOff";
    private const string mc_saleTimeName = "saleTime";
    [HideInInspector]
    private GuiLabel m_amountOffLabel;
    [HideInInspector]
    private GuiLabel m_saleTimeLabel;
    [HideInInspector]
    private ShopDiscount m_itemDiscount;
    private ShipItem shipItem;

    public GuiShopSalePanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiShopSalePanel>())
    {
      this.Initialize();
    }

    public GuiShopSalePanel(GuiShopSalePanel copy)
      : base((GuiItemPanelBase) copy)
    {
      this.Initialize();
    }

    public GuiShopSalePanel(JSalesPanel json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiShopSalePanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JSalesPanel((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      this.SetSaleTimeLeft();
      this.ResizePanel();
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
    }

    private void Initialize()
    {
      this.m_amountOffLabel = this.Find<GuiLabel>("amountOff");
      this.m_saleTimeLabel = this.Find<GuiLabel>("saleTime");
      this.m_amountOffLabel.Text = BsgoLocalization.Get("%$bgo.shop_tooltip.percent_off%", (object) "25%\n");
    }

    public void ShowUpgradeSales()
    {
      if (!(this.shipItem is ShipSystem))
        return;
      this.m_itemDiscount = (this.shipItem as ShipSystem).FindDiscountInUpgradeLevels();
      if (this.m_itemDiscount != null)
      {
        this.m_amountOffLabel.Text = BsgoLocalization.Get("%$bgo.shop_tooltip.percent_off%", (object) (this.m_itemDiscount.Percentage.ToString() + "%\n"));
        this.SetSaleTimeLeft();
        this.IsRendered = true;
      }
      else
        this.IsRendered = false;
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      this.IsRendered = false;
      if (!condensedVersion && item != null)
      {
        this.IsRendered = this.SetProperties(item.CardGUID);
        this.shipItem = item as ShipItem;
      }
      return this.IsRendered;
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      return false;
    }

    private bool SetProperties(uint GUID)
    {
      this.m_itemDiscount = Shop.FindItemDiscount(GUID);
      bool flag = false;
      foreach (HangarShip ship in Game.Me.Hangar.Ships)
      {
        if ((int) ship.GUID == (int) GUID)
        {
          flag = true;
          break;
        }
      }
      if (!flag && this.m_itemDiscount != null)
      {
        this.m_amountOffLabel.Text = BsgoLocalization.Get("%$bgo.shop_tooltip.percent_off%", (object) (this.m_itemDiscount.Percentage.ToString() + "%\n"));
        this.SetSaleTimeLeft();
        this.SizeX = 0.0f;
        return true;
      }
      this.m_saleTimeLabel.Text = "%$bgo.shop_tooltip.sale_expired%";
      return false;
    }

    private void SetSaleTimeLeft()
    {
      int num1 = this.m_itemDiscount == null ? 123456789 : this.m_itemDiscount.Duration;
      int num2 = num1 / 60;
      int num3 = num1 - num2 * 60;
      int num4 = num2 / 60;
      int num5 = num2 - num4 * 60;
      int num6 = num4 / 24;
      int num7 = num4 - num6 * 24;
      this.m_saleTimeLabel.Text = "%$bgo.shop_tooltip.time_remaining% " + num6.ToString("D2") + ":" + num7.ToString("D2") + ":" + num5.ToString("D2") + ":" + num3.ToString("D2");
    }

    protected override void ResizePanel()
    {
      if (this.Parent != null)
      {
        this.PositionX = this.ElementPadding.Left;
        this.SizeX = this.Parent.SizeX - this.PositionX - this.ElementPadding.Right;
      }
      this.m_amountOffLabel.Position = this.m_amountOffLabel.ElementPadding.TopLeft;
      float num = this.m_amountOffLabel.PositionY + this.m_amountOffLabel.SizeY * 0.5f;
      this.m_saleTimeLabel.PositionX = this.m_amountOffLabel.BottomRightCornerWithPadding.x + this.m_saleTimeLabel.ElementPadding.Left;
      this.m_saleTimeLabel.PositionY = num - this.m_saleTimeLabel.SizeY * 0.5f;
      this.SizeY = this.m_amountOffLabel.BottomRightCornerWithPadding.y;
    }

    public override void Update()
    {
      base.Update();
      if (this.m_itemDiscount == null)
        return;
      this.SetSaleTimeLeft();
      this.ResizePanel();
    }
  }
}
