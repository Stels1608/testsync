﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiItemPanelBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui.GameItem
{
  public abstract class GuiItemPanelBase : GuiPanel
  {
    public static Vector2 ms_atlasSourceRectSmall = new Vector2(40f, 35f);
    public static Vector2 ms_atlasSourceRectLarge = new Vector2(47f, 40f);
    private GameItemCard m_itemCard;
    private ShopInventoryContainer m_currentContainer;
    private int m_slotNumber;

    public GameItemCard ItemCard
    {
      get
      {
        return this.m_itemCard;
      }
      set
      {
        this.m_itemCard = value;
      }
    }

    public ShopInventoryContainer CurrentContainer
    {
      get
      {
        return this.m_currentContainer;
      }
      set
      {
        this.m_currentContainer = value;
      }
    }

    public int SlotNumber
    {
      get
      {
        return this.m_slotNumber;
      }
      set
      {
        this.m_slotNumber = value;
      }
    }

    public GuiItemPanelBase(string panelLayoutName)
      : base(panelLayoutName)
    {
    }

    public GuiItemPanelBase(GuiItemPanelBase copy)
      : base((GuiPanel) copy)
    {
    }

    public GuiItemPanelBase(JPanel json)
      : base(json)
    {
    }

    public abstract bool SetProperties(GameItemCard item, bool condensedVersion);

    public abstract bool SetProperties(GUICard guiCard, Vector2 elementSize);

    protected virtual void ResizePanel()
    {
      Vector2 lhs = Vector2.zero;
      for (int index = 0; index < this.Children.Count; ++index)
      {
        if (this.Children[index].IsRendered && this.Children[index] != this.m_backgroundImage)
          lhs = Vector2.Max(lhs, this.Children[index].BottomRightCornerWithPadding);
      }
      this.Size = lhs;
    }
  }
}
