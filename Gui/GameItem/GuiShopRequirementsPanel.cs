﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiShopRequirementsPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Gui.GameItem
{
  public class GuiShopRequirementsPanel : GuiItemPanelBase
  {
    private string m_shipTypeTexturePath = "GUI/AbilityToolbar/abilities_atlas_B";
    private const string mc_panelLayout = "GUI/Tooltips/ShopPanels/AdvancedShopRequirementsPanelLayout";
    private const string mc_requirementsLabelName = "requirementsLabel";
    private const string mc_skillLabelName = "skillLabel";
    private const string mc_ammoLabelName = "ammoLabel";
    private const int mc_cylonIndexOffset = 10;
    private GuiShopRequirementsPanel.LabelAndList m_requirementsList;
    private GuiShopRequirementsPanel.LabelAndList m_skillList;
    private GuiShopRequirementsPanel.LabelAndList m_ammoList;
    private GuiShopIconAndLabelPanel m_iconAndLabelTemplate;
    private AtlasCache m_shipTextureAtlas;

    public GuiShopRequirementsPanel()
      : base("GUI/Tooltips/ShopPanels/AdvancedShopRequirementsPanelLayout")
    {
      this.m_shipTextureAtlas = new AtlasCache(float2.FromV2(GuiItemPanelBase.ms_atlasSourceRectSmall));
      GuiLabel label = this.Find<GuiLabel>("requirementsLabel");
      this.RemoveChild((GuiElementBase) label);
      this.m_requirementsList = new GuiShopRequirementsPanel.LabelAndList(label);
      this.AddChild((GuiElementBase) this.m_requirementsList);
      this.m_skillList = new GuiShopRequirementsPanel.LabelAndList(this.m_requirementsList);
      this.m_skillList.Name = "skillLabel";
      this.m_skillList.Label.Text = "%$bgo.shop_tooltip.skill%";
      this.AddChild((GuiElementBase) this.m_skillList);
      this.m_ammoList = new GuiShopRequirementsPanel.LabelAndList(this.m_requirementsList);
      this.m_ammoList.Name = "ammoLabel";
      this.m_ammoList.Label.Text = "%$bgo.shop_tooltip.ammunition_types%";
      this.AddChild((GuiElementBase) this.m_ammoList);
      this.m_iconAndLabelTemplate = this.Find<GuiShopIconAndLabelPanel>("iconAndLabel");
      this.m_iconAndLabelTemplate.IsRendered = false;
      this.ElementPadding = (GuiElementPadding) new Rect(10f, 1f, 10f, 1f);
    }

    public GuiShopRequirementsPanel(GuiShopRequirementsPanel copy)
      : base((GuiItemPanelBase) copy)
    {
      this.m_shipTextureAtlas = new AtlasCache(float2.FromV2(GuiItemPanelBase.ms_atlasSourceRectSmall));
      this.m_requirementsList = this.Find<GuiShopRequirementsPanel.LabelAndList>("requirementsLabel");
      this.m_skillList = this.Find<GuiShopRequirementsPanel.LabelAndList>("skillLabel");
      this.m_ammoList = this.Find<GuiShopRequirementsPanel.LabelAndList>("ammoLabel");
      this.m_iconAndLabelTemplate = this.Find<GuiShopIconAndLabelPanel>("iconAndLabel");
      this.m_iconAndLabelTemplate.IsRendered = false;
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiShopRequirementsPanel(this);
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      if (condensedVersion)
      {
        this.IsRendered = false;
        return false;
      }
      this.ResetPanel();
      if (item is ShipSystem)
      {
        ShipSystem system = item as ShipSystem;
        this.SetShipProperties(system.Card.Tier);
        this.AddRequiredSkills(system);
      }
      else if (item is ShipCard)
      {
        ShipCard shipCard = item as ShipCard;
        string label = BsgoLocalization.Get("%$bgo.shop_tooltip.requiredLevel%", (object) shipCard.RequiredPlayerLevel());
        GuiShopIconAndLabelPanel iconAndLabelPanel = this.AddRequirementPanel(this.m_requirementsList);
        iconAndLabelPanel.SetProperties(label);
        iconAndLabelPanel.TextColor = (int) Game.Me.Level < shipCard.RequiredPlayerLevel() ? Color.red : Color.white;
      }
      else if (item is ItemCountable)
        this.SetShipProperties(((ItemCountable) item).Card.Tier);
      this.IsRendered = this.PositionChildren();
      this.ResizePanel();
      return this.IsRendered;
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      return false;
    }

    private void ResetPanel()
    {
      this.m_requirementsList.ClearList();
      this.m_skillList.ClearList();
      this.m_ammoList.ClearList();
    }

    private GuiShopIconAndLabelPanel AddRequirementPanel(GuiShopRequirementsPanel.LabelAndList labelAndList)
    {
      if (this.m_iconAndLabelTemplate != null)
        return labelAndList.AddToList(new GuiShopIconAndLabelPanel(this.m_iconAndLabelTemplate));
      throw new NullReferenceException("The requirements panel was not setup correctly");
    }

    private void SetShipProperties(byte shipTier)
    {
      int frameIndex;
      switch (shipTier)
      {
        case 1:
          frameIndex = 9;
          break;
        case 2:
          frameIndex = 8;
          break;
        case 3:
          frameIndex = 7;
          break;
        case 4:
          frameIndex = 6;
          break;
        default:
          return;
      }
      if (Game.Me.Faction == Faction.Cylon)
        frameIndex += 10;
      string label = BsgoLocalization.Get("%$bgo.shop_tooltip.ship_class%", (object) BsgoLocalization.Get("%$bgo.common.ship_class_tier_" + (object) shipTier + "%"));
      GuiShopIconAndLabelPanel iconAndLabelPanel = this.AddRequirementPanel(this.m_requirementsList);
      iconAndLabelPanel.SetProperties(this.m_shipTextureAtlas, this.m_shipTypeTexturePath, frameIndex, label, false);
      bool flag = (int) Game.Me.Hangar.ActiveShip.Card.Tier == (int) shipTier;
      iconAndLabelPanel.TextColor = !flag ? Color.red : Color.white;
    }

    private void AddRequiredSkills(ShipSystem system)
    {
      int skillLevel = 0;
      List<PlayerSkill> needSkills = (List<PlayerSkill>) null;
      Game.Me.RequiresSkillsForSystem(system, (int) system.Card.Level, out skillLevel, out needSkills);
      if (needSkills.Count == 0)
        return;
      for (int index = 0; index < needSkills.Count; ++index)
      {
        GuiShopIconAndLabelPanel iconAndLabelPanel = this.AddRequirementPanel(this.m_skillList);
        iconAndLabelPanel.SetProperties(needSkills[index].Card.GUICard, GuiItemPanelBase.ms_atlasSourceRectLarge, needSkills[index].Name + " " + (object) skillLevel, false);
        bool flag = Game.Me.GetSkillLevel(needSkills[index].Card.Hash) >= skillLevel;
        iconAndLabelPanel.TextColor = !flag ? Color.red : Color.white;
      }
    }

    private void AddAmmunitionDependencies(ShipSystem system)
    {
      ItemList consumables = Game.Me.Hold.Consumables;
      ItemList countableListForShip = GameLevel.Instance.GetCountableListForShip(Game.Me.ActiveShip.Card.Tier);
      if (countableListForShip == null || system.Card.AbilityCards.Length <= 0)
        return;
      foreach (ShipItem shipItem in (IEnumerable<ShipItem>) countableListForShip)
      {
        if (shipItem is ItemCountable)
        {
          ShipConsumableCard shipConsumableCard = (shipItem as ItemCountable).Card;
          if ((int) shipConsumableCard.ConsumableType == (int) system.Card.AbilityCards[0].ConsumableType && (int) shipConsumableCard.Tier == (int) system.Card.AbilityCards[0].ConsumableTier && (system.Card.AbilityCards[0].ConsumableOption == ShipConsumableOption.Using && (int) shipConsumableCard.Tier == (int) system.Card.Tier))
          {
            GuiShopIconAndLabelPanel iconAndLabelPanel = this.AddRequirementPanel(this.m_ammoList);
            iconAndLabelPanel.SetProperties((GameItemCard) shipItem, false);
            bool flag = false;
            foreach (ItemCountable itemCountable in (IEnumerable<ShipItem>) consumables)
            {
              flag = (int) itemCountable.Card.CardGUID == (int) shipConsumableCard.CardGUID;
              if (flag)
                break;
            }
            iconAndLabelPanel.TextColor = !flag ? Color.red : Color.white;
          }
        }
      }
    }

    private bool PositionChildren()
    {
      Vector2 zero = Vector2.zero;
      this.PositionLabelAndList(this.m_requirementsList, ref zero);
      this.PositionLabelAndList(this.m_skillList, ref zero);
      this.PositionLabelAndList(this.m_ammoList, ref zero);
      if (!this.m_requirementsList.IsRendered && !this.m_skillList.IsRendered)
        return this.m_ammoList.IsRendered;
      return true;
    }

    private void PositionLabelAndList(GuiShopRequirementsPanel.LabelAndList labelAndList, ref Vector2 position)
    {
      labelAndList.Position = position + labelAndList.ElementPadding.TopLeft;
      labelAndList.ResizePanel();
      position.y = labelAndList.PositionY + labelAndList.SizeY + labelAndList.ElementPadding.Bottom;
      labelAndList.IsRendered = labelAndList.List.Count > 0;
    }

    private class LabelAndList : GuiPanel
    {
      private GuiLabel m_label;
      private List<GuiShopIconAndLabelPanel> m_list;

      public GuiLabel Label
      {
        get
        {
          return this.m_label;
        }
        set
        {
          this.m_label = value;
        }
      }

      public ReadOnlyCollection<GuiShopIconAndLabelPanel> List
      {
        get
        {
          return this.m_list.AsReadOnly();
        }
      }

      public LabelAndList()
        : this(new GuiLabel())
      {
      }

      public LabelAndList(GuiLabel label)
      {
        this.m_label = label;
        this.AddChild((GuiElementBase) this.m_label);
        this.m_list = new List<GuiShopIconAndLabelPanel>();
        this.ElementPadding = (GuiElementPadding) new Rect(0.0f, 0.0f, 0.0f, 0.0f);
      }

      public LabelAndList(GuiShopRequirementsPanel.LabelAndList copy)
        : base((GuiPanel) copy)
      {
        this.m_label = this.Find<GuiLabel>();
        this.m_list = new List<GuiShopIconAndLabelPanel>();
        for (int index = 0; index < copy.List.Count; ++index)
          this.m_list.Add(copy.m_list[index].CloneElement() as GuiShopIconAndLabelPanel);
      }

      public GuiShopIconAndLabelPanel AddToList(GuiShopIconAndLabelPanel panel)
      {
        this.m_list.Add(panel);
        this.AddChild((GuiElementBase) panel, true);
        return this.m_list[this.m_list.Count - 1];
      }

      public void ClearList()
      {
        this.m_list.Clear();
        this.EmptyChildren<GuiShopIconAndLabelPanel>();
      }

      public void ResizePanel()
      {
        Vector2 zero = Vector2.zero;
        this.m_label.Position = zero + this.m_label.ElementPadding.TopLeft;
        zero.x = this.m_label.PositionX;
        zero.y = this.m_label.PositionY + this.m_label.SizeY + this.m_label.ElementPadding.Bottom;
        for (int index = 0; index < this.m_list.Count; ++index)
        {
          this.m_list[index].Position = zero + this.m_list[index].ElementPadding.TopLeft;
          zero.y = this.m_list[index].PositionY + this.m_list[index].SizeY + this.m_list[index].ElementPadding.Bottom;
          this.SizeX = Mathf.Max(this.SizeX, this.m_list[index].PositionX + this.m_list[index].SizeX + this.m_list[index].ElementPadding.Right);
        }
        this.SizeY = zero.y;
      }
    }
  }
}
