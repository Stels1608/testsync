﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiShopSlotInfoPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.GameItem
{
  public class GuiShopSlotInfoPanel : GuiItemPanelBase
  {
    private Texture2D[] m_firingArcTextures = new Texture2D[2];
    private string m_slotTypeTexturePath = "GUI/AbilityToolbar/abilities_atlas_B";
    private const string mc_panelLayout = "GUI/Tooltips/ShopPanels/AdvancedShopSlotInfoPanelLayout";
    private const string mc_slotInfoName = "slotInfoLabel";
    private const string mc_firingArcImageName = "firingArcImage";
    private const string mc_firingArcLabelName = "firingArcLabel";
    private GuiLabel m_slotInfoLabel;
    private GuiImage m_firingArcImage;
    private GuiShopIconAndLabelPanel m_slotPanel;
    private AtlasCache m_slotTextureAtlas;

    public GuiShopSlotInfoPanel()
      : base("GUI/Tooltips/ShopPanels/AdvancedShopSlotInfoPanelLayout")
    {
      this.m_slotTextureAtlas = new AtlasCache(float2.FromV2(GuiItemPanelBase.ms_atlasSourceRectSmall));
      this.m_slotInfoLabel = this.Find<GuiLabel>("slotInfoLabel");
      this.m_firingArcImage = this.Find<GuiImage>("firingArcImage");
      this.m_slotPanel = this.Find<GuiShopIconAndLabelPanel>("iconAndLabel");
      this.m_firingArcTextures[0] = (Texture2D) ResourceLoader.Load("GUI/Gui2/firing_arc_75");
      this.m_firingArcTextures[1] = (Texture2D) ResourceLoader.Load("GUI/Gui2/firing_arc_180");
      this.ElementPadding = (GuiElementPadding) new Rect(10f, 1f, 10f, 1f);
    }

    public GuiShopSlotInfoPanel(GuiShopSlotInfoPanel copy)
      : base((GuiItemPanelBase) copy)
    {
      this.m_slotTextureAtlas = new AtlasCache(float2.FromV2(GuiItemPanelBase.ms_atlasSourceRectSmall));
      this.m_slotInfoLabel = this.Find<GuiLabel>("slotInfoLabel");
      this.m_firingArcImage = this.Find<GuiImage>("firingArcImage");
      this.m_slotPanel = new GuiShopIconAndLabelPanel(copy.m_slotPanel);
      this.m_firingArcTextures[0] = (Texture2D) ResourceLoader.Load("GUI/Gui2/firing_arc_75");
      this.m_firingArcTextures[1] = (Texture2D) ResourceLoader.Load("GUI/Gui2/firing_arc_180");
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiShopSlotInfoPanel(this);
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      this.IsRendered = false;
      if (item is ShipSystem)
      {
        ShipSystem shipSystem = item as ShipSystem;
        this.m_slotInfoLabel.Text = shipSystem.Card.AbilityCards.Length <= 0 ? "%$bgo.shop_tooltip.passive%" : (!shipSystem.Card.AbilityCards[0].OnByDefault ? "%$bgo.shop_tooltip.active%" : "%$bgo.shop_tooltip.toggle%");
        this.m_slotPanel.PositionY = this.m_slotInfoLabel.PositionY + this.m_slotInfoLabel.SizeY + this.m_slotInfoLabel.ElementPadding.Bottom + this.m_slotPanel.ElementPadding.Top;
        int frameIndex = 0;
        switch (shipSystem.Card.SlotType)
        {
          case ShipSlotType.computer:
            frameIndex = 3;
            break;
          case ShipSlotType.engine:
            frameIndex = 4;
            break;
          case ShipSlotType.hull:
            frameIndex = 1;
            break;
          case ShipSlotType.weapon:
            frameIndex = 2;
            break;
          case ShipSlotType.ship_paint:
            this.m_slotPanel.SetProperties(this.m_slotTextureAtlas, "GUI/AbilityToolbar/abilities_atlas", 129, Tools.ParseMessage("%$bgo.stats." + (object) shipSystem.Card.SlotType + "%"), false);
            break;
          case ShipSlotType.avionics:
            frameIndex = 21;
            break;
          default:
            this.m_slotPanel.SetProperties(shipSystem.Card.SlotType.ToString());
            break;
        }
        if (frameIndex > 0)
          this.m_slotPanel.SetProperties(this.m_slotTextureAtlas, this.m_slotTypeTexturePath, frameIndex, "%$bgo.stats." + (object) shipSystem.Card.SlotType + "%", false);
        this.m_firingArcImage.IsRendered = false;
        this.Find<GuiLabel>("firingArcLabel").IsRendered = false;
        if (condensedVersion)
        {
          this.m_firingArcImage.IsRendered = false;
          this.m_slotPanel.IsRendered = false;
        }
        this.ResizePanel();
        this.IsRendered = true;
      }
      return this.IsRendered;
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      return false;
    }
  }
}
