﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiCostPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System.Collections.Generic;
using UnityEngine;

namespace Gui.GameItem
{
  [UseInGuiEditor("GUI/Tooltips/CostPanelLayout.txt")]
  public class GuiCostPanel : GuiItemPanelBase
  {
    [HideInInspector]
    private bool m_updatePrice = true;
    [HideInInspector]
    private bool m_arrangeHorizontally = true;
    private GuiCostPanel.ExchangeType m_currentExchange;
    [HideInInspector]
    private GuiLabel m_costTypeLabel;
    [HideInInspector]
    private GuiCostPanel.GuiCostElement m_costElementTemplate;
    [HideInInspector]
    private ShopItemCard mShopItemCard;

    public GuiCostPanel.ExchangeType CurrentExchange
    {
      get
      {
        return this.m_currentExchange;
      }
      set
      {
        if (this.m_currentExchange == value)
          return;
        this.m_currentExchange = value;
        this.m_updatePrice = true;
      }
    }

    public GuiCostPanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiCostPanel>())
    {
      this.Initialize();
    }

    public GuiCostPanel(GuiCostPanel copy)
      : base((GuiItemPanelBase) copy)
    {
      this.m_currentExchange = copy.m_currentExchange;
      this.Initialize();
    }

    public GuiCostPanel(JCostPanel json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiCostPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JCostPanel((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      this.m_costElementTemplate.Reposition();
      this.ResizePanelToFitChildren();
    }

    private void Initialize()
    {
      this.m_costTypeLabel = this.Find<GuiLabel>("costTypeLabel");
      this.m_costElementTemplate = this.Find<GuiCostPanel.GuiCostElement>("costElement");
      if (this.m_costElementTemplate == null)
      {
        GuiPanel panel = this.Find<GuiPanel>("costElement");
        this.m_costElementTemplate = new GuiCostPanel.GuiCostElement(panel);
        this.RemoveChild((GuiElementBase) panel);
        this.AddChild((GuiElementBase) this.m_costElementTemplate);
      }
      this.m_updatePrice = true;
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      if (item == null || item is ShipCard && !this.SetShipExchangeType(item as ShipCard))
      {
        this.IsRendered = false;
        return false;
      }
      this.SetCostAmount(item.ShopItemCard, condensedVersion);
      return this.IsRendered;
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      bool flag = false;
      this.IsRendered = flag;
      return flag;
    }

    private bool SetShipExchangeType(ShipCard ship)
    {
      if (this.m_currentExchange != GuiCostPanel.ExchangeType.SELL)
      {
        if (ship.HaveShip)
        {
          if (ship.IsUpgraded || ship.HasParent)
            return false;
          this.m_currentExchange = GuiCostPanel.ExchangeType.UPGRADE;
        }
        else
          this.m_currentExchange = GuiCostPanel.ExchangeType.BUY;
      }
      return true;
    }

    private void SetCostAmount(ShopItemCard shopItemCard, bool condensedVersion)
    {
      if (this.m_updatePrice || shopItemCard != this.mShopItemCard)
      {
        this.m_arrangeHorizontally = condensedVersion;
        this.mShopItemCard = shopItemCard;
        if (this.mShopItemCard != null)
          this.UpdateCostElements(this.UpdateExchangeType());
        else
          this.IsRendered = false;
      }
      this.m_updatePrice = false;
    }

    private Price UpdateExchangeType()
    {
      switch (this.m_currentExchange)
      {
        case GuiCostPanel.ExchangeType.BUY:
          this.m_costTypeLabel.Text = "%$bgo.cost_panel.buy%";
          return this.mShopItemCard.BuyPrice;
        case GuiCostPanel.ExchangeType.SELL:
          this.m_costTypeLabel.Text = "%$bgo.cost_panel.sell%";
          return this.mShopItemCard.SellPrice;
        case GuiCostPanel.ExchangeType.UPGRADE:
          this.m_costTypeLabel.Text = "%$bgo.cost_panel.upgrade%";
          return this.mShopItemCard.UpgradePrice;
        default:
          return this.mShopItemCard.BuyPrice;
      }
    }

    private void UpdateCostElements(Price price)
    {
      if (price.items.Count == 0)
      {
        this.IsRendered = false;
      }
      else
      {
        this.IsRendered = true;
        List<GuiCostPanel.GuiCostElement> all = this.FindAll<GuiCostPanel.GuiCostElement>();
        int num = 0;
        Vector2 cornerWithPadding = this.m_costTypeLabel.BottomRightCornerWithPadding;
        cornerWithPadding.x = this.m_costElementTemplate.ElementPadding.Left;
        using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = price.items.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
            if ((double) current.Value > 0.0)
            {
              string salePrice = string.Empty;
              float result;
              if (this.m_currentExchange == GuiCostPanel.ExchangeType.BUY && Shop.ApplyDiscount(out result, this.mShopItemCard.CardGUID, current.Value))
              {
                salePrice = result.ToString("#0.0");
              }
              else
              {
                ShopDiscount upgradeDiscount;
                if (this.m_currentExchange == GuiCostPanel.ExchangeType.UPGRADE && this.mShopItemCard.Category == ShopCategory.Ship && (upgradeDiscount = Shop.FindUpgradeDiscount(ShopItemType.Ship, 1)) != null)
                {
                  result = upgradeDiscount.ApplyDiscount(current.Value);
                  salePrice = result.ToString("#0.0");
                }
              }
              GuiCostPanel.GuiCostElement guiCostElement;
              if (all.Count > num)
              {
                guiCostElement = all[num++];
              }
              else
              {
                guiCostElement = new GuiCostPanel.GuiCostElement(this.m_costElementTemplate);
                this.AddChild((GuiElementBase) guiCostElement);
              }
              guiCostElement.SetInfo(ResourceLoader.Load<Texture2D>(current.Key.GUICard.GUIIcon), current.Value.ToString("#0.0"), salePrice, this.m_arrangeHorizontally);
              guiCostElement.Position = cornerWithPadding + guiCostElement.ElementPadding.TopLeft;
              if (this.m_arrangeHorizontally)
                cornerWithPadding.x = guiCostElement.BottomRightCornerWithPadding.x;
              else
                cornerWithPadding.y = guiCostElement.BottomRightCornerWithPadding.y;
            }
          }
        }
        for (int index = num; index < all.Count; ++index)
          this.RemoveChild((GuiElementBase) all[index]);
      }
      this.ResizePanelToFitChildren();
    }

    private class GuiCostElement : GuiPanel
    {
      private const string mc_currencyImageName = "currencyIcon";
      private const string mc_costLabelName = "costLabel";
      private const string mc_saleLabelName = "saleLabel";
      private const string mc_strikeLabelName = "strikeLabel";
      [HideInInspector]
      private GuiImage m_currency;
      [HideInInspector]
      private GuiLabel m_cost;
      [HideInInspector]
      private GuiLabel m_salePrice;
      [HideInInspector]
      private GuiLabel m_strike;

      public GuiCostElement(GuiPanel panel)
        : base(panel)
      {
        this.Initialize();
      }

      public GuiCostElement(GuiCostPanel.GuiCostElement copy)
        : base((GuiPanel) copy)
      {
        this.Initialize();
      }

      private void Initialize()
      {
        this.m_currency = this.Find<GuiImage>("currencyIcon");
        this.m_cost = this.Find<GuiLabel>("costLabel");
        this.m_salePrice = this.Find<GuiLabel>("saleLabel");
        this.m_strike = this.Find<GuiLabel>("strikeLabel");
      }

      public void SetInfo(Texture2D iconTexture, string cost, string salePrice, bool showSinglePrice)
      {
        this.m_currency.Texture = iconTexture;
        this.m_cost.Text = cost;
        this.m_cost.NormalColor = !(salePrice != string.Empty) ? Color.white : Color.red;
        this.m_salePrice.Text = salePrice;
        this.m_salePrice.IsRendered = salePrice != string.Empty;
        this.m_strike.Text = new string('_', salePrice.Length + 3);
        this.m_cost.IsRendered = (!showSinglePrice ? 0 : (this.m_salePrice.IsRendered ? 1 : 0)) == 0;
        this.m_strike.IsRendered = this.m_cost.IsRendered && this.m_salePrice.IsRendered;
        this.Reposition();
      }

      public override void Reposition()
      {
        float a = this.m_currency.ElementWithPaddingSizeY;
        if (this.m_cost.IsRendered)
          a = Mathf.Max(a, this.m_cost.ElementWithPaddingSizeY);
        if (this.m_salePrice.IsRendered)
          a = Mathf.Max(a, this.m_salePrice.ElementWithPaddingSizeY);
        float num1 = a / 2f;
        this.m_currency.PositionX = this.m_currency.ElementPadding.Left;
        this.m_currency.PositionY = num1 - this.m_currency.SizeY / 2f;
        float num2 = this.m_currency.BottomRightCornerWithPadding.x;
        if (this.m_cost.IsRendered)
        {
          this.m_cost.PositionX = num2 + this.m_cost.ElementPadding.Left;
          this.m_cost.PositionY = num1 - this.m_cost.SizeY / 2f;
          this.m_strike.PositionX = this.m_cost.PositionX;
          this.m_strike.PositionY = this.m_cost.PositionY - this.m_cost.SizeY / 2f;
          num2 = this.m_cost.BottomRightCornerWithPadding.x;
        }
        if (this.m_salePrice.IsRendered)
        {
          this.m_salePrice.PositionX = num2 + this.m_salePrice.ElementPadding.Left;
          this.m_salePrice.PositionY = num1 - this.m_salePrice.SizeY / 2f;
          num2 = this.m_salePrice.BottomRightCornerWithPadding.x;
        }
        this.SizeX = num2;
        this.SizeY = a;
      }
    }

    public enum ExchangeType
    {
      BUY,
      SELL,
      UPGRADE,
    }
  }
}
