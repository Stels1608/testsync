﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiShopStarterKitPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui.GameItem
{
  [UseInGuiEditor("GUI/Tooltips/ShopPanels/AdvancedShopStarterKitPanelLayout")]
  public class GuiShopStarterKitPanel : GuiItemPanelBase
  {
    private float m_childIndent = 10f;
    private Dictionary<int, List<GuiShopIconAndLabelPanel>> m_itemList = new Dictionary<int, List<GuiShopIconAndLabelPanel>>();
    private const string mc_bundleLabelName = "bundleTitle";
    private const string mc_equipmentPanelName = "equipmentheader";
    private const string mc_ammunitionPanelName = "ammunitionheader";
    private GuiShopIconAndLabelPanel m_iconAndLabelToTemplate;
    private GuiLabel m_bundleTitle;
    private GuiPanel m_equipmentTitlePanel;
    private GuiPanel m_ammunitionTitlePanel;

    public GuiShopStarterKitPanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiShopStarterKitPanel>())
    {
      this.m_iconAndLabelToTemplate = this.Find<GuiShopIconAndLabelPanel>("iconAndLabel");
      this.m_bundleTitle = this.Find<GuiLabel>("bundleTitle");
      this.m_equipmentTitlePanel = this.Find<GuiPanel>("equipmentheader");
      this.m_ammunitionTitlePanel = this.Find<GuiPanel>("ammunitionheader");
      this.ResetItemsAndPanel();
      this.ElementPadding = (GuiElementPadding) new Rect(10f, 1f, 10f, 1f);
    }

    public GuiShopStarterKitPanel(GuiShopStarterKitPanel copy)
      : base((GuiItemPanelBase) copy)
    {
      this.m_iconAndLabelToTemplate = new GuiShopIconAndLabelPanel(copy.m_iconAndLabelToTemplate);
      this.m_bundleTitle = this.Find<GuiLabel>("bundleTitle");
      this.m_equipmentTitlePanel = this.Find<GuiPanel>("equipmentheader");
      this.m_ammunitionTitlePanel = this.Find<GuiPanel>("ammunitionheader");
      this.ResetItemsAndPanel();
    }

    private List<GuiShopIconAndLabelPanel> ItemList(GuiShopStarterKitPanel.ItemType itemType)
    {
      return this.m_itemList[(int) itemType];
    }

    private void Initialize()
    {
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiShopStarterKitPanel(this);
    }

    private GuiShopIconAndLabelPanel AddIconAndLabelPanel(GuiShopStarterKitPanel.ItemType itemType)
    {
      if (this.m_iconAndLabelToTemplate != null)
      {
        if (!this.m_itemList.ContainsKey((int) itemType))
        {
          Debug.LogError((object) ("The item type (" + itemType.ToString() + ") is not found in the dictionary"));
          return (GuiShopIconAndLabelPanel) null;
        }
        GuiShopIconAndLabelPanel iconAndLabelPanel = new GuiShopIconAndLabelPanel(this.m_iconAndLabelToTemplate);
        this.ItemList(itemType).Add(iconAndLabelPanel);
        return this.ItemList(itemType)[this.ItemList(itemType).Count - 1];
      }
      Debug.LogError((object) "The ship kit panel was not setup correctly");
      return (GuiShopIconAndLabelPanel) null;
    }

    private void ResetItemsAndPanel()
    {
      this.RemoveAll();
      this.m_itemList.Clear();
      this.AddChild((GuiElementBase) this.m_bundleTitle);
      this.AddChild((GuiElementBase) this.m_equipmentTitlePanel);
      this.AddChild((GuiElementBase) this.m_ammunitionTitlePanel);
      foreach (int key in Enum.GetValues(typeof (GuiShopStarterKitPanel.ItemType)))
        this.m_itemList.Add(key, new List<GuiShopIconAndLabelPanel>());
    }

    private void SetUpItemPositions()
    {
      this.m_equipmentTitlePanel.IsRendered = false;
      this.m_ammunitionTitlePanel.IsRendered = false;
      float num = this.m_bundleTitle.PositionY + this.m_bundleTitle.SizeY;
      using (Dictionary<int, List<GuiShopIconAndLabelPanel>>.KeyCollection.Enumerator enumerator = this.m_itemList.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GuiShopStarterKitPanel.ItemType itemType = (GuiShopStarterKitPanel.ItemType) enumerator.Current;
          List<GuiShopIconAndLabelPanel> iconAndLabelPanelList = this.ItemList(itemType);
          if (iconAndLabelPanelList.Count > 0)
          {
            if (itemType == GuiShopStarterKitPanel.ItemType.EQUIPMENT)
            {
              this.m_equipmentTitlePanel.IsRendered = true;
              this.m_equipmentTitlePanel.PositionY = num + this.m_equipmentTitlePanel.ElementPadding.Top;
              num = this.m_equipmentTitlePanel.BottomRightCornerWithPadding.y;
            }
            else if (itemType == GuiShopStarterKitPanel.ItemType.AMMUNITION)
            {
              this.m_ammunitionTitlePanel.IsRendered = true;
              this.m_ammunitionTitlePanel.PositionY = num + this.m_ammunitionTitlePanel.ElementPadding.Top;
              num = this.m_ammunitionTitlePanel.BottomRightCornerWithPadding.y;
            }
            this.m_childIndent = this.m_iconAndLabelToTemplate.PositionX;
            for (int index = 0; index < this.ItemList(itemType).Count; ++index)
            {
              iconAndLabelPanelList[index].PositionX = this.m_childIndent;
              iconAndLabelPanelList[index].PositionY = num + iconAndLabelPanelList[index].ElementPadding.Top;
              num = this.ItemList(itemType)[index].BottomRightCornerWithPadding.y;
            }
          }
        }
      }
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      this.IsRendered = false;
      if (item is StarterKit)
        this.IsRendered = this.SetProperties((item as StarterKit).Card.Items);
      return this.IsRendered;
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      return false;
    }

    public bool SetProperties(List<ShipItem> itemList)
    {
      if (itemList != null)
      {
        this.ResetItemsAndPanel();
        for (int index = 0; index < itemList.Count; ++index)
        {
          GuiShopIconAndLabelPanel iconAndLabelPanel = (GuiShopIconAndLabelPanel) null;
          if (itemList[index] is ShipSystem)
          {
            iconAndLabelPanel = this.AddIconAndLabelPanel(GuiShopStarterKitPanel.ItemType.EQUIPMENT);
            iconAndLabelPanel.SetProperties(itemList[index].ItemGUICard, GuiItemPanelBase.ms_atlasSourceRectSmall);
          }
          else if (itemList[index] is ItemCountable)
          {
            iconAndLabelPanel = this.AddIconAndLabelPanel(GuiShopStarterKitPanel.ItemType.AMMUNITION);
            iconAndLabelPanel.SetProperties(itemList[index].ItemGUICard, GuiItemPanelBase.ms_atlasSourceRectSmall);
          }
          if (iconAndLabelPanel != null)
            this.AddChild((GuiElementBase) iconAndLabelPanel);
        }
        this.SetUpItemPositions();
        this.ResizePanel();
      }
      this.IsRendered = itemList != null;
      return this.IsRendered;
    }

    private enum ItemType
    {
      EQUIPMENT,
      AMMUNITION,
    }
  }
}
