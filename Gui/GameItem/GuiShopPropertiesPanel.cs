﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiShopPropertiesPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.GameItem
{
  public class GuiShopPropertiesPanel : GuiItemPanelBase
  {
    private const string mc_panelLayout = "GUI/Tooltips/ShopPanels/AdvancedShopPropertiesPanelLayout";

    public GuiShopPropertiesPanel()
      : base("GUI/Tooltips/ShopPanels/AdvancedShopPropertiesPanelLayout")
    {
      this.ElementPadding = (GuiElementPadding) new Rect(10f, 1f, 10f, 1f);
    }

    public GuiShopPropertiesPanel(GuiShopPropertiesPanel copy)
      : base((GuiItemPanelBase) copy)
    {
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiShopPropertiesPanel(this);
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      return this.SetProperties(item.ItemGUICard, Vector2.zero);
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      this.IsRendered = false;
      return this.IsRendered;
    }
  }
}
