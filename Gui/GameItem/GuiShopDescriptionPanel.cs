﻿// Decompiled with JetBrains decompiler
// Type: Gui.GameItem.GuiShopDescriptionPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.GameItem
{
  public class GuiShopDescriptionPanel : GuiItemPanelBase
  {
    private float m_maxLength = 250f;
    private const string mc_panelLayout = "GUI/Tooltips/ShopPanels/AdvancedShopDescriptionPanelLayout";
    private const string mc_descriptionName = "descriptionLabel";
    private GuiLabel m_descriptionLabel;
    private float m_originalWidth;

    public GuiShopDescriptionPanel()
      : base("GUI/Tooltips/ShopPanels/AdvancedShopDescriptionPanelLayout")
    {
      this.m_descriptionLabel = this.Find<GuiLabel>("descriptionLabel");
      this.m_originalWidth = this.m_descriptionLabel.SizeX;
      this.ElementPadding = (GuiElementPadding) new Rect(10f, 1f, 10f, 1f);
    }

    public GuiShopDescriptionPanel(GuiShopDescriptionPanel copy)
      : base((GuiItemPanelBase) copy)
    {
      this.m_descriptionLabel = this.Find<GuiLabel>("descriptionLabel");
      this.m_maxLength = copy.m_maxLength;
      this.m_originalWidth = copy.m_originalWidth;
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiShopDescriptionPanel(this);
    }

    public void CalculatePanelWidth()
    {
      this.m_descriptionLabel.SizeX = this.m_originalWidth;
      while ((double) this.m_descriptionLabel.SizeY > (double) this.m_maxLength)
        this.m_descriptionLabel.SizeX += 5f;
    }

    public override bool SetProperties(GameItemCard item, bool condensedVersion)
    {
      if (item == null || item.ItemGUICard == null || item is StarterKit)
      {
        this.IsRendered = false;
        return this.IsRendered;
      }
      this.m_descriptionLabel.Text = !condensedVersion ? item.ItemGUICard.Description : item.ItemGUICard.ShortDescription;
      return this.SetProperties(item.ItemGUICard, Vector2.zero);
    }

    public override bool SetProperties(GUICard guiCard, Vector2 elementSize)
    {
      if (guiCard != null)
      {
        this.m_descriptionLabel.Text = guiCard.ShortDescription;
        this.CalculatePanelWidth();
        this.ResizePanel();
      }
      else
        this.m_descriptionLabel.Text = string.Empty;
      this.IsRendered = guiCard.Description != string.Empty;
      return this.IsRendered;
    }
  }
}
