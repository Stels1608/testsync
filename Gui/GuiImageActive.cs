﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiImageActive
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiImageActive : GuiImage
  {
    private GuiImageActive.State state;
    private Color? colorNormal;
    private Color? colorOver;
    private Color? colorPressed;

    [Gui2Editor]
    public Color? ColorNormal
    {
      get
      {
        return this.colorNormal;
      }
      set
      {
        this.colorNormal = value;
        if (this.state != GuiImageActive.State.Normal)
          return;
        this.OverlayColor = value;
      }
    }

    [Gui2Editor]
    public Color? ColorOver
    {
      get
      {
        return this.colorOver;
      }
      set
      {
        this.colorOver = value;
        if (this.state != GuiImageActive.State.Over)
          return;
        this.OverlayColor = value;
      }
    }

    [Gui2Editor]
    public Color? ColorPressed
    {
      get
      {
        return this.colorPressed;
      }
      set
      {
        this.colorPressed = value;
        if (this.state != GuiImageActive.State.Pressed)
          return;
        this.OverlayColor = value;
      }
    }

    public GuiImageActive()
    {
    }

    public GuiImageActive(Color? colorNormal, Color? colorOver, Color? colorPressed)
    {
      this.colorNormal = colorNormal;
      this.colorOver = colorOver;
      this.colorPressed = colorPressed;
    }

    public GuiImageActive(string texture)
      : base(texture)
    {
    }

    public GuiImageActive(Texture2D texture)
      : base(texture)
    {
    }

    public override void MouseEnter(float2 position, float2 previousPosition)
    {
      base.MouseEnter(position, previousPosition);
      this.OverlayColor = this.colorOver;
      this.state = GuiImageActive.State.Over;
    }

    public override void MouseLeave(float2 position, float2 previousPosition)
    {
      base.MouseLeave(position, previousPosition);
      this.OverlayColor = this.colorNormal;
      this.state = GuiImageActive.State.Normal;
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      this.OverlayColor = this.colorPressed;
      this.state = GuiImageActive.State.Pressed;
      return base.MouseDown(position, key);
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      this.OverlayColor = !this.IsMouseOver ? this.colorNormal : this.colorOver;
      this.state = !this.IsMouseOver ? GuiImageActive.State.Normal : GuiImageActive.State.Over;
      return base.MouseUp(position, key);
    }

    private enum State
    {
      Normal,
      Over,
      Pressed,
    }
  }
}
