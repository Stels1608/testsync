﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tools
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text;
using SystemMap3D;
using UnityEngine;

namespace Gui
{
  public class Tools
  {
    private static readonly Dictionary<Action, string> actionToString = new Dictionary<Action, string>();
    private static readonly Dictionary<UserSetting, string> settingToString = new Dictionary<UserSetting, string>();
    private static readonly Dictionary<string, GetterDelegate<string>> placeholders = new Dictionary<string, GetterDelegate<string>>();
    private static readonly Dictionary<Guid, Dictionary<Enum, string>> enumLocaDict = new Dictionary<Guid, Dictionary<Enum, string>>();

    static Tools()
    {
      for (int index = 101; index < 113; ++index)
      {
        int num = index - 101 + 1;
        Tools.actionToString.Add((Action) index, "%$bgo.keybindings.weapon_slot%" + num.ToString());
      }
      for (int index = 114; index < 126; ++index)
      {
        int num = index - 113;
        Tools.actionToString.Add((Action) index, "%$bgo.keybindings.ability_slot%" + num.ToString());
      }
      Tools.actionToString.Add(Action.ToggleWindowPilotLog, "%$bgo.keybindings.character_info%");
      Tools.actionToString.Add(Action.ToggleWindowGalaxyMap, "%$bgo.keybindings.sector_navigation%");
      Tools.actionToString.Add(Action.ToggleSystemMap2D, "%$bgo.keybindings.system_map_2D%");
      Tools.actionToString.Add(Action.ToggleSystemMap3D, "%$bgo.keybindings.system_map_3D.toggle%");
      Tools.actionToString.Add(Action.Map3DBackToOverview, "%$bgo.keybindings.system_map_3D.back_to_overview%");
      Tools.actionToString.Add(Action.Map3DFocusYourShip, "%$bgo.keybindings.system_map_3D.focus_your_ship%");
      Tools.actionToString.Add(Action.ToggleWindowLeaderboard, "%$bgo.keybindings.leaderboards%");
      Tools.actionToString.Add(Action.ToggleWindowWingRoster, "%$bgo.keybindings.wing_roster%");
      Tools.actionToString.Add(Action.ToggleWindowInventory, "%$bgo.keybindings.hold_inventory%");
      Tools.actionToString.Add(Action.TakeAllLoot, "%$bgo.keybindings.take_all_loot%");
      Tools.actionToString.Add(Action.ToggleWindowOptions, "%$bgo.keybindings.options_menu%");
      Tools.actionToString.Add(Action.ToggleCamera, "%$bgo.keybindings.toggle_camera_mode%");
      Tools.actionToString.Add(Action.ToggleShipName, "%$bgo.keybindings.toggle_names%");
      Tools.actionToString.Add(Action.ToggleCombatGUI, "%$bgo.keybindings.toggle_combat_gui%");
      Tools.actionToString.Add(Action.ToggleGuns, "%$bgo.keybindings.toggle_guns%");
      Tools.actionToString.Add(Action.ToggleFlak, "%$bgo.keybindings.toggle_flak%");
      Tools.actionToString.Add(Action.TogglePointDefence, "%$bgo.keybindings.toggle_point_defence%");
      Tools.actionToString.Add(Action.ToggleGunsOnHold, "%$bgo.keybindings.toggle_guns_on_hold%");
      Tools.actionToString.Add(Action.FireMissiles, "%$bgo.keybindings.fire_missiles%");
      Tools.actionToString.Add(Action.ToggleWindowTutorial, "%$bgo.keybindings.show_help%");
      Tools.actionToString.Add(Action.TargetCamera, "%$bgo.keybindings.camera_target_mode%");
      Tools.actionToString.Add(Action.ChaseCamera, "%$bgo.keybindings.camera_chase_mode%");
      Tools.actionToString.Add(Action.FreeCamera, "%$bgo.keybindings.camera_free_mode%");
      Tools.actionToString.Add(Action.NoseCamera, "%$bgo.keybindings.camera_nose_mode%");
      Tools.actionToString.Add(Action.ToggleWindowDuties, "%$bgo.keybindings.duties%");
      Tools.actionToString.Add(Action.ToggleWindowStatusAssignments, "%$bgo.keybindings.assignments%");
      Tools.actionToString.Add(Action.ToggleWindowSkills, "%$bgo.keybindings.skills%");
      Tools.actionToString.Add(Action.ToggleWindowShipStatus, "%$bgo.keybindings.ship_status%");
      Tools.actionToString.Add(Action.ToggleWindowInFlightSupply, "%$bgo.keybindings.in_flight_shop%");
      Tools.actionToString.Add(Action.ToggleTournamentRanking, "%$bgo.keybindings.tournament_ranking%");
      Tools.actionToString.Add(Action.ToggleFps, "%$bgo.keybindings.fps%");
      Tools.actionToString.Add(Action.ToggleFullscreen, "%$bgo.common.get_full_screen_full%");
      Tools.actionToString.Add(Action.ToggleSquadWindow, "%$bgo.keybindings.squad_window%");
      Tools.actionToString.Add(Action.TurnOrSlideLeft, "%$bgo.keybindings.turn_left%");
      Tools.actionToString.Add(Action.TurnOrSlideRight, "%$bgo.keybindings.turn_right%");
      Tools.actionToString.Add(Action.SlopeForwardOrSlideUp, "%$bgo.keybindings.forward%");
      Tools.actionToString.Add(Action.SlopeBackwardOrSlideDown, "%$bgo.keybindings.backward%");
      Tools.actionToString.Add(Action.RollLeft, "%$bgo.keybindings.roll_left%");
      Tools.actionToString.Add(Action.RollRight, "%$bgo.keybindings.roll_right%");
      Tools.actionToString.Add(Action.ToggleMovementMode, "%$bgo.keybindings.toggle_mouse_movement%");
      Tools.actionToString.Add(Action.AlignToHorizon, "%$bgo.keybindings.align_to_horizon%");
      Tools.actionToString.Add(Action.ToggleAdvancedFlightControls, "%$bgo.keybindings.toggle_advanced_flight_controls%");
      Tools.actionToString.Add(Action.JoystickTurnLeft, "%$bgo.keybindings.joystick_turn_left%");
      Tools.actionToString.Add(Action.JoystickTurnRight, "%$bgo.keybindings.joystick_turn_right%");
      Tools.actionToString.Add(Action.JoystickTurnUp, "%$bgo.keybindings.joystick_turn_up%");
      Tools.actionToString.Add(Action.JoystickTurnDown, "%$bgo.keybindings.joystick_turn_down%");
      Tools.actionToString.Add(Action.JoystickRollLeft, "%$bgo.keybindings.joystick_roll_left%");
      Tools.actionToString.Add(Action.JoystickRollRight, "%$bgo.keybindings.joystick_roll_right%");
      Tools.actionToString.Add(Action.JoystickSpeedController, "%$bgo.keybindings.joystick_speed_controller%");
      Tools.actionToString.Add(Action.JoystickLookLeft, "%$bgo.keybindings.joystick_look_left%");
      Tools.actionToString.Add(Action.JoystickLookRight, "%$bgo.keybindings.joystick_look_right%");
      Tools.actionToString.Add(Action.JoystickLookUp, "%$bgo.keybindings.joystick_look_up%");
      Tools.actionToString.Add(Action.JoystickLookDown, "%$bgo.keybindings.joystick_look_down%");
      Tools.actionToString.Add(Action.JoystickStrafeLeft, "%$bgo.keybindings.joystick_strafe_left%");
      Tools.actionToString.Add(Action.JoystickStrafeRight, "%$bgo.keybindings.joystick_strafe_right%");
      Tools.actionToString.Add(Action.JoystickStrafeUp, "%$bgo.keybindings.joystick_strafe_up%");
      Tools.actionToString.Add(Action.JoystickStrafeDown, "%$bgo.keybindings.joystick_strafe_down%");
      Tools.actionToString.Add(Action.Boost, "%$bgo.keybindings.boosters%");
      Tools.actionToString.Add(Action.ToggleBoost, "%$bgo.keybindings.toggle_boosters%");
      Tools.actionToString.Add(Action.FullSpeed, "%$bgo.keybindings.move%");
      Tools.actionToString.Add(Action.Stop, "%$bgo.keybindings.stop%");
      Tools.actionToString.Add(Action.SpeedUp, "%$bgo.keybindings.speed_up%");
      Tools.actionToString.Add(Action.SlowDown, "%$bgo.keybindings.slow_down%");
      Tools.actionToString.Add(Action.ZoomIn, "%$bgo.keybindings.zoom_in%");
      Tools.actionToString.Add(Action.ZoomOut, "%$bgo.keybindings.zoom_out%");
      Tools.actionToString.Add(Action.MatchSpeed, "%$bgo.keybindings.match_speed%");
      Tools.actionToString.Add(Action.Follow, "%$bgo.keybindings.follow%");
      Tools.actionToString.Add(Action.NearestFriendly, "%$bgo.keybindings.nearest_friendly%");
      Tools.actionToString.Add(Action.NearestEnemy, "%$bgo.keybindings.nearest_enemy%");
      Tools.actionToString.Add(Action.SelectScreenCenterObject, "%$bgo.keybindings.select_screen_center_object%");
      Tools.actionToString.Add(Action.CancelTarget, "%$bgo.keybindings.cancel_target%");
      Tools.actionToString.Add(Action.SelectNearestMissile, "%$bgo.keybindings.nearest_missile%");
      Tools.actionToString.Add(Action.SelectNearestMine, "%$bgo.keybindings.nearest_mine%");
      Tools.actionToString.Add(Action.Jump, "%$bgo.keybindings.jump%");
      Tools.actionToString.Add(Action.CancelJump, "%$bgo.keybindings.stop_jump%");
      Tools.actionToString.Add(Action.FocusChat, "%$bgo.keybindings.focus_chat%");
      Tools.actionToString.Add(Action.UnfocusChat, "%$bgo.keybindings.unfocus_chat%");
      Tools.actionToString.Add(Action.Reply, "%$bgo.keybindings.reply%");
      Tools.placeholders.Add("TimeTillMidnight".ToLower(), (GetterDelegate<string>) (() => Tools.FormatTime(Game.TimeSync.TimeTillMidnight, true)));
      Tools.placeholders.Add("Rank".ToLower(), (GetterDelegate<string>) (() => Game.Me.Name));
      Tools.placeholders.Add("RankFull".ToLower(), (GetterDelegate<string>) (() => Game.Me.Name));
      Tools.placeholders.Add("Level".ToLower(), (GetterDelegate<string>) (() => Game.Me.Level.ToString()));
      Tools.placeholders.Add("Experience".ToLower(), (GetterDelegate<string>) (() => Game.Me.Experience.ToString()));
      Tools.placeholders.Add("NextLevelExperience".ToLower(), (GetterDelegate<string>) (() => Game.Me.NextLevelExperience.ToString()));
      Tools.placeholders.Add("Name".ToLower(), (GetterDelegate<string>) (() => Game.Me.Name));
      Tools.placeholders.Add("CapShipCost".ToLower(), (GetterDelegate<string>) (() => Game.Galaxy.CapitalShipCost.ToString()));
      Tools.placeholders.Add("MaxWaterAmountExchange".ToLower(), (GetterDelegate<string>) (() => Game.Me.WaterExchangeValues.MaxWaterAmountExchange.ToString()));
      Tools.placeholders.Add("WaterInHold".ToLower(), (GetterDelegate<string>) (() => Game.Me.WaterExchangeValues.WaterInHold.ToString()));
      Tools.placeholders.Add("WaterAmountExchange".ToLower(), (GetterDelegate<string>) (() => Game.Me.WaterExchangeValues.WaterAmountExchange.ToString()));
      Tools.placeholders.Add("CubitAmountExchange".ToLower(), (GetterDelegate<string>) (() => Game.Me.WaterExchangeValues.CubitAmountExchange.ToString()));
      Tools.placeholders.Add("WaterCubitExchangeRate".ToLower(), (GetterDelegate<string>) (() => Game.Me.WaterExchangeValues.WaterCubitExchangeRate.ToString("n1")));
      Tools.placeholders.Add("br".ToLower(), (GetterDelegate<string>) (() => "\n"));
      Tools.placeholders.Add("t".ToLower(), (GetterDelegate<string>) (() => "\t"));
      Tools.placeholders.Add(string.Empty.ToLower(), (GetterDelegate<string>) (() => "%"));
      Tools.settingToString.Add(UserSetting.ShowFpsAndPing, "%$bgo.etc.opt_show_fps_and_ping%");
      Tools.settingToString.Add(UserSetting.CombatText, "%$bgo.etc.opt_combat_text%");
      Tools.settingToString.Add(UserSetting.ShowDamageOverlay, "%$bgo.etc.opt_damage_overlay%");
      Tools.settingToString.Add(UserSetting.StatsIndication, "%$bgo.etc.opt_stats_indication%");
      Tools.settingToString.Add(UserSetting.ShowPopups, "%$bgo.etc.opt_show_popup_tips%");
      Tools.settingToString.Add(UserSetting.ShowXpBar, "%$bgo.etc.opt_show_xp_bar%");
      Tools.settingToString.Add(UserSetting.HudIndicatorShowWingNames, "%$bgo.etc.opt_show_wing_names%");
      Tools.settingToString.Add(UserSetting.HudIndicatorShowTargetNames, "%$bgo.etc.opt_show_target_names%");
      Tools.settingToString.Add(UserSetting.HudIndicatorShowShipNames, "%$bgo.etc.opt_show_ship_names%");
      Tools.settingToString.Add(UserSetting.HudIndicatorShowTitles, "%$bgo.etc.opt_show_player_titles%");
      Tools.settingToString.Add(UserSetting.HudIndicatorShowMissionArrow, "%$bgo.etc.opt_show_mission_arrow%");
      Tools.settingToString.Add(UserSetting.HudIndicatorTextSize, "%$bgo.etc.opt_hud_indicator.text_size%");
      Tools.settingToString.Add(UserSetting.HudIndicatorShowShipTierIcon, "%$bgo.etc.opt_hud_indicator.ship_tier_icon%");
      Tools.settingToString.Add(UserSetting.HudIndicatorBracketResizing, "%$bgo.etc.opt_hud_indicator.bracket_resizing%");
      Tools.settingToString.Add(UserSetting.ShowOutpostMessages, "%$bgo.etc.opt_outpost_message%");
      Tools.settingToString.Add(UserSetting.ShowHeavyFightingMessages, "%$bgo.etc.opt_heavy_fighting_message%");
      Tools.settingToString.Add(UserSetting.ShowAugmentMessages, "%$bgo.etc.opt_augment_message%");
      Tools.settingToString.Add(UserSetting.ShowMiningShipMessages, "%$bgo.etc.opt_mining_ship_message%");
      Tools.settingToString.Add(UserSetting.ShowExperienceMessages, "%$bgo.etc.opt_experience_message%");
      Tools.settingToString.Add(UserSetting.ShowAssignmentMessages, "%$bgo.etc.opt_assignment_message%");
      Tools.settingToString.Add(UserSetting.MusicVolume, "%$bgo.etc.opt_music_volume%");
      Tools.settingToString.Add(UserSetting.SoundVolume, "%$bgo.etc.opt_sfx_volume%");
      Tools.settingToString.Add(UserSetting.AutoLoot, "%$bgo.etc.opt_auto_loot%");
      Tools.settingToString.Add(UserSetting.ShowCutscenes, "%$bgo.etc.opt_show_cutscenes%");
      Tools.settingToString.Add(UserSetting.ShowStarDust, "%$bgo.etc.opt_show_dust%");
      Tools.settingToString.Add(UserSetting.ShowStarFog, "%$bgo.etc.opt_show_fog%");
      Tools.settingToString.Add(UserSetting.ShowGlowEffect, "%$bgo.etc.opt_glow_effect%");
      Tools.settingToString.Add(UserSetting.HighResModels, "%$bgo.etc.opt_high_res_models%");
      Tools.settingToString.Add(UserSetting.HighResTextures, "%$bgo.etc.opt_high_res_textures%");
      Tools.settingToString.Add(UserSetting.UseProceduralTextures, "%$bgo.etc.opt_procedual_textures%");
      Tools.settingToString.Add(UserSetting.ViewDistance, "%$bgo.etc.opt_view_distance%");
      Tools.settingToString.Add(UserSetting.AntiAliasing, "%$bgo.etc.opt_anti_aliasing%");
      Tools.settingToString.Add(UserSetting.ShowBulletImpactFx, "%$bgo.etc.opt_show_bullet_impacts%");
      Tools.settingToString.Add(UserSetting.FlakFieldDensity, "%$bgo.etc.opt_flak_field_density%");
      Tools.settingToString.Add(UserSetting.HighQualityParticles, "%$bgo.etc.opt_high_quality_particles%");
      Tools.settingToString.Add(UserSetting.AnisotropicFiltering, "%$bgo.etc.opt_anisotropic_filtering%");
      Tools.settingToString.Add(UserSetting.ShowShipSkins, "%$bgo.etc.opt_show_skins%");
      Tools.settingToString.Add(UserSetting.ShowWeaponModules, "%$bgo.etc.opt_show_modules%");
      Tools.settingToString.Add(UserSetting.FramerateCapping, "%$bgo.etc.opt_framerate_cap%");
      Tools.settingToString.Add(UserSetting.Fullscreen, "%$bgo.etc.opt_fullscreen%");
      Tools.settingToString.Add(UserSetting.AdvancedFlightControls, "%$bgo.etc.opt_advanced_flight_controls%");
      Tools.settingToString.Add(UserSetting.InvertedVertical, "%$bgo.etc.opt_inverted_flight_controls%");
      Tools.settingToString.Add(UserSetting.MouseWheelBinding, "%$bgo.etc.opt_mouse_wheel%");
      Tools.settingToString.Add(UserSetting.DeadZoneMouse, "%$bgo.etc.opt_deadzone%");
      Tools.settingToString.Add(UserSetting.DeadZoneJoystick, "%$bgo.etc.opt_deadzone_joystick%");
      Tools.settingToString.Add(UserSetting.SensitivityJoystick, "%$bgo.etc.opt_sensitivity_joystick%");
      Tools.settingToString.Add(UserSetting.JoystickGamepadEnabled, "%$bgo.etc.opt_joystick_gamepad_enabled%");
      Tools.settingToString.Add(UserSetting.ShowXbox360Buttons, "%$bgo.etc.opt_show_xbox360_buttons%");
      Tools.settingToString.Add(UserSetting.ShowFriendIndication, "%$bgo.etc.opt_friendly_ships_indication%");
      Tools.settingToString.Add(UserSetting.ShowEnemyIndication, "%$bgo.etc.opt_ships_indication%");
      Tools.settingToString.Add(UserSetting.AutomaticAmmoReload, "%$bgo.etc.opt_auto_auto_reload%");
      Tools.settingToString.Add(UserSetting.SystemMap3DTransitionMode, "%$bgo.etc.opt_sector_map.transition%");
      Tools.settingToString.Add(UserSetting.SystemMap3DCameraView, "%$bgo.etc.opt_sector_map.camera_view%");
      Tools.settingToString.Add(UserSetting.SystemMap3DShowAsteroids, "%$bgo.etc.opt_sector_map.icon_filter.asteroids%");
      Tools.settingToString.Add(UserSetting.SystemMap3DShowDynamicMissions, "%$bgo.etc.opt_sector_map.icon_filter.dynamic_missions%");
      Tools.settingToString.Add(UserSetting.SystemMap3DFormAsteroidGroups, "%$bgo.etc.opt_sector_map.form_asteroid_groups%");
      Tools.settingToString.Add(UserSetting.HudIndicatorMinimizeDistance, "%$bgo.etc.opt_hud_indicator_minimize_distance%");
      Tools.settingToString.Add(UserSetting.HudIndicatorDescriptionDisplayDistance, "%$bgo.etc.opt_hud_description_display_distance%");
      Tools.settingToString.Add(UserSetting.HudIndicatorColorScheme, "%$bgo.etc.opt_hud_indicator_color_scheme%");
      Tools.settingToString.Add(UserSetting.HudIndicatorSelectionCrosshair, "%$bgo.etc.opt_hud_indicator_selection_crosshair%");
      Tools.settingToString.Add(UserSetting.HudIndicatorHealthBar, "%$bgo.etc.opt_hud_indicator.health_bar%");
      Tools.AddEnumLocalization<TransitionMode>((Enum) TransitionMode.Instant, "%$bgo.etc.opt_sector_map.transition.instant%");
      Tools.AddEnumLocalization<TransitionMode>((Enum) TransitionMode.Smooth, "%$bgo.etc.opt_sector_map.transition.smooth%");
      Tools.AddEnumLocalization<CameraView>((Enum) CameraView.Top, "%$bgo.etc.opt_sector_map.camera_view.top%");
      Tools.AddEnumLocalization<CameraView>((Enum) CameraView.Iso, "%$bgo.etc.opt_sector_map.camera_view.iso%");
    }

    public static string ParseMessage(string message)
    {
      if (message == null)
        return (string) null;
      message = Tools.Localize(message);
      message = Tools.ReplaceLocalizationPatterns(message);
      return message;
    }

    public static string ParseMessage(string message, params object[] args)
    {
      return string.Format(Tools.ParseMessage(message), args);
    }

    public static string ParseMessage(string message, object arg0)
    {
      return Tools.ParseMessage(Tools.ParseMessage(message), new object[1]{ arg0 });
    }

    public static string ParseMessage(string message, object arg0, object arg1)
    {
      return Tools.ParseMessage(Tools.ParseMessage(message), new object[2]{ arg0, arg1 });
    }

    public static string ParseMessage(string message, object arg0, object arg1, object arg2)
    {
      return Tools.ParseMessage(Tools.ParseMessage(message), new object[3]{ arg0, arg1, arg2 });
    }

    private static string Localize(string message)
    {
      for (string localizationPattern = Tools.GetLocalizationPattern(message); localizationPattern != null; localizationPattern = Tools.GetLocalizationPattern(message))
        message = message.Replace("%$" + localizationPattern + "%", BsgoLocalization.Get(localizationPattern));
      return message;
    }

    private static string GetLocalizationPattern(string str)
    {
      int num1 = str.IndexOf("%$");
      if (num1 == -1)
        return (string) null;
      int startIndex = num1 + 2;
      int num2 = str.IndexOf("%", startIndex);
      if (num2 != -1)
        return str.Substring(startIndex, num2 - startIndex);
      Debug.Log((object) ("Strange localization pattern: " + str));
      return (string) null;
    }

    private static string ReplaceLocalizationPatterns(string original)
    {
      if (original.IndexOf('%') == -1)
        return original;
      StringBuilder stringBuilder = new StringBuilder(original.Length * 6 / 5);
      int index = 0;
      int length = original.Length;
      bool flag = false;
      int startIndex = 0;
      for (; index < length; ++index)
      {
        if ((int) original[index] == 37)
        {
          if (!flag)
          {
            startIndex = index;
            flag = true;
          }
          else
          {
            string lower = original.Substring(startIndex + 1, index - startIndex - 1).ToLower();
            GetterDelegate<string> getterDelegate = (GetterDelegate<string>) null;
            if (!Tools.placeholders.TryGetValue(lower, out getterDelegate))
            {
              index = startIndex;
              stringBuilder.Append('%');
            }
            else
              stringBuilder.Append(getterDelegate());
            flag = false;
          }
        }
        else if (!flag)
          stringBuilder.Append(original[index]);
      }
      if (flag)
        stringBuilder.Append(original.Substring(startIndex));
      return stringBuilder.ToString();
    }

    public static string FormatWingRole(GuildRole role)
    {
      string str = Game.Me.Guild._RankName(role);
      if (!(str == string.Empty))
        return str;
      string key = string.Empty;
      switch (role)
      {
        case GuildRole.Recruit:
          key = "%$bgo.common.guild_role.recruit%";
          break;
        case GuildRole.Pilot:
          key = "%$bgo.common.guild_role.pilot%";
          break;
        case GuildRole.SeniorPilot:
          key = "%$bgo.common.guild_role.seniorpilot%";
          break;
        case GuildRole.FlightLeader:
          key = "%$bgo.common.guild_role.flightleader%";
          break;
        case GuildRole.GroupLeader:
          key = "%$bgo.common.guild_role.groupleader%";
          break;
        case GuildRole.Leader:
          key = "%$bgo.common.guild_role.leader%";
          break;
      }
      if (key != string.Empty && (UnityEngine.Object) Game.Instance != (UnityEngine.Object) null && !Game.Localization.TryGetTranslation(key, out str))
        str = key;
      return str;
    }

    public static string Escape(string str)
    {
      string str1 = "\\\n\t\r'\"/|.,:;`~=@}{#&%$^!?<>* ";
      string str2 = string.Empty;
      foreach (char ch in str)
      {
        if (!str1.Contains(ch.ToString()))
          str2 += (string) (object) ch;
      }
      return str2;
    }

    public static string SubStr(string str, int length)
    {
      if (str.Length > length + 3)
        str = str.Substring(0, length) + "...";
      return str;
    }

    public static string MakeFirstLetterCapital(string str)
    {
      string str1 = string.Empty;
      for (int index = 0; index < str.Length; ++index)
        str1 = index == 0 || (int) str[index - 1] == 32 ? str1 + (object) char.ToUpper(str[index]) : str1 + (object) char.ToLower(str[index]);
      return str1;
    }

    public static string RemoveNoLatins(string str)
    {
      string str1 = string.Empty;
      for (int index = 0; index < str.Length; ++index)
      {
        if ((int) str[index] == 32)
          str1 += (string) (object) str[index];
        else if ((int) str[index] >= 33 && (int) str[index] <= 126)
          str1 += (string) (object) str[index];
      }
      return str1;
    }

    public static string RemoveNoLatinsButSpaces(string str)
    {
      string str1 = string.Empty;
      for (int index = 0; index < str.Length; ++index)
      {
        if ((int) str[index] == 32)
          str1 += (string) (object) str[index];
        else if ((int) str[index] >= 33 && (int) str[index] <= 126)
          str1 += (string) (object) str[index];
      }
      return str1;
    }

    public static bool ValidNameChar(char c)
    {
      return ((char.IsLetterOrDigit(c) ? (true ? 1 : 0) : ((int) c == 95 ? 1 : 0)) & ((int) c < 33 ? 0 : ((int) c <= 126 ? 1 : 0))) != 0;
    }

    public static string FormatTime(float time)
    {
      return Tools.FormatTime(TimeSpan.FromSeconds((double) time));
    }

    public static string FormatTime(TimeSpan time)
    {
      return Tools.FormatTime(time, false);
    }

    public static string FormatTime(TimeSpan time, bool omitSeconds)
    {
      string str1 = string.Empty;
      if (time.TotalSeconds < 0.0 && Mathf.CeilToInt((float) time.TotalSeconds) < 0)
      {
        str1 += "-";
        time = time.Negate();
      }
      string str2 = BsgoLocalization.Get("%$bgo.common.sem%");
      bool flag1 = true;
      if (time.Days != 0 || time.Hours != 0 || !flag1)
      {
        str1 = str1 + (!flag1 ? str2 : string.Empty) + (time.Hours + 24 * time.Days).ToString("D2");
        flag1 = false;
      }
      string str3 = str1 + (!flag1 ? str2 : string.Empty) + time.Minutes.ToString("D2");
      bool flag2 = false;
      if (!omitSeconds)
        str3 = str3 + (!flag2 ? str2 : string.Empty) + time.Seconds.ToString("D2");
      return str3;
    }

    public static string GetHotKeyFor(Action action)
    {
      KeyBinding keyBinding = (FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider") as SettingsDataProvider).InputBinder.TryGetBinding(InputDevice.KeyboardMouse, action) as KeyBinding;
      if (keyBinding != null)
        return keyBinding.BindingAlias;
      return string.Empty;
    }

    public static string GetLocalized(Action action)
    {
      if (Tools.actionToString.ContainsKey(action))
        return Tools.ParseMessage(Tools.actionToString[action]);
      Log.Add(string.Format("Action {0}, not present in dictionary", (object) action));
      return action.ToString();
    }

    public static string GetLocalized(UserSetting setting)
    {
      if (Tools.settingToString.ContainsKey(setting))
        return Tools.ParseMessage(Tools.settingToString[setting]);
      Log.Add(string.Format("Action {0}, not present in dictionary", (object) setting));
      return setting.ToString();
    }

    private static void AddEnumLocalization<T>(Enum enumValue, string locaKey) where T : struct, IConvertible
    {
      Guid guid = typeof (T).GUID;
      if (!Tools.enumLocaDict.ContainsKey(guid))
        Tools.enumLocaDict.Add(guid, new Dictionary<Enum, string>());
      Tools.enumLocaDict[guid].Add(enumValue, locaKey);
    }

    public static string GetEnumLocalization<T>(Enum enumValue) where T : struct, IConvertible
    {
      Guid guid = typeof (T).GUID;
      if (!Tools.enumLocaDict.ContainsKey(guid))
        Debug.LogError((object) ("No loca entries for enum type: " + (object) typeof (T)));
      return Tools.ParseMessage(Tools.enumLocaDict[guid][enumValue]);
    }

    public static string EnumToString(Enum obj)
    {
      return Tools.EnumToString(obj, (string) null);
    }

    public static string EnumToString(Enum obj, string prefix)
    {
      string key = !string.IsNullOrEmpty(prefix) ? prefix + "." + obj.ToString() : "bgo." + obj.GetType().FullName + "." + obj.ToString();
      string str = (string) null;
      if (BsgoLocalization.TryGet(key, out str))
        return str;
      return obj.ToString();
    }

    public static string RandomString(int size)
    {
      StringBuilder stringBuilder = new StringBuilder();
      for (int index = 0; index < size; ++index)
      {
        char @char = Convert.ToChar(Convert.ToInt32(Mathf.Floor((float) (26.0 * (double) UnityEngine.Random.value + 65.0))));
        stringBuilder.Append(@char);
      }
      return stringBuilder.ToString();
    }

    public static Color Color(int r, int g, int b)
    {
      return Tools.Color(r, g, b, (int) byte.MaxValue);
    }

    public static Color Color(int r, int g, int b, int a)
    {
      return new Color((float) r / (float) byte.MaxValue, (float) g / (float) byte.MaxValue, (float) b / (float) byte.MaxValue, (float) a / (float) byte.MaxValue);
    }

    public static Color Color(Color c, int a)
    {
      return new Color(c.r, c.g, c.b, (float) a / (float) byte.MaxValue);
    }

    public static Dictionary<ushort, Vector2> GenerateMapPositions(int maxParts)
    {
      Dictionary<ushort, Vector2> dictionary = new Dictionary<ushort, Vector2>();
      dictionary.Clear();
      int max = 16;
      int min = -6;
      for (ushort key = 1; (int) key < maxParts + 1; ++key)
      {
        Vector2 vector2;
        bool flag;
        do
        {
          vector2 = new Vector2((float) UnityEngine.Random.Range(0, max), (float) UnityEngine.Random.Range(min, 0));
          flag = true;
          using (Dictionary<ushort, Vector2>.Enumerator enumerator = dictionary.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              if (enumerator.Current.Value == vector2)
                flag = false;
            }
          }
        }
        while (!flag);
        dictionary.Add(key, vector2);
        Debug.Log((object) ("ADD == " + (object) vector2));
      }
      string str = string.Empty;
      using (Dictionary<ushort, Vector2>.Enumerator enumerator = dictionary.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ushort, Vector2> current = enumerator.Current;
          str = str + "mapPositions.Add(" + (object) current.Key + ",new Vector2(" + (object) current.Value.x + "f," + (object) current.Value.y + "f));\n";
        }
      }
      Debug.Log((object) str);
      return dictionary;
    }

    public static string SecondsToTimeString(uint seconds)
    {
      TimeSpan timeSpan = TimeSpan.FromSeconds((double) seconds);
      string upper1 = BsgoLocalization.Get("%$bgo.common.second%").ToUpper();
      string upper2 = BsgoLocalization.Get("%$bgo.common.minute%").ToUpper();
      string upper3 = BsgoLocalization.Get("%$bgo.common.hours%").ToUpper();
      string upper4 = BsgoLocalization.Get("%$bgo.common.days%").ToUpper();
      string str = string.Format("{0:D2} " + upper1, (object) timeSpan.Seconds);
      if (seconds > 60U)
        str = string.Format("{0:D2} " + upper2, (object) timeSpan.Minutes) + " " + str;
      if (seconds > 3600U)
        str = string.Format("{0:D2} " + upper3, (object) timeSpan.Hours) + " " + str;
      if (seconds > 86400U)
        str = string.Format("{0:D2} " + upper4, (object) timeSpan.Days) + " " + str;
      return str;
    }

    public static string IEnumToString(Dictionary<string, int> dict)
    {
      StringBuilder stringBuilder = new StringBuilder();
      if (dict != null)
      {
        using (Dictionary<string, int>.Enumerator enumerator = dict.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, int> current = enumerator.Current;
            stringBuilder.Append("[" + current.Key + " | " + (object) current.Value + " ]");
          }
        }
      }
      return stringBuilder.ToString();
    }

    public static string IEnumToString(Dictionary<string, float> dict)
    {
      StringBuilder stringBuilder = new StringBuilder();
      if (dict != null)
      {
        using (Dictionary<string, float>.Enumerator enumerator = dict.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, float> current = enumerator.Current;
            stringBuilder.Append("[" + current.Key + " | " + current.Value.ToString() + " ]");
          }
        }
      }
      return stringBuilder.ToString();
    }

    public static string IEnumToString(Dictionary<string, string> dict)
    {
      StringBuilder stringBuilder = new StringBuilder();
      if (dict != null)
      {
        using (Dictionary<string, string>.Enumerator enumerator = dict.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, string> current = enumerator.Current;
            stringBuilder.Append("[" + current.Key + " | " + current.Value + " ]");
          }
        }
      }
      return stringBuilder.ToString();
    }

    public static string IEnumToString(Dictionary<string, Dictionary<string, string>> dict)
    {
      StringBuilder stringBuilder = new StringBuilder();
      if (dict != null)
      {
        using (Dictionary<string, Dictionary<string, string>>.Enumerator enumerator = dict.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, Dictionary<string, string>> current = enumerator.Current;
            stringBuilder.AppendLine(current.Key + "\n");
            stringBuilder.Append(Tools.IEnumToString(current.Value));
          }
        }
      }
      return stringBuilder.ToString();
    }

    public static string IEnumToString(Dictionary<string, List<string>> dict)
    {
      StringBuilder stringBuilder = new StringBuilder();
      if (dict != null)
      {
        using (Dictionary<string, List<string>>.Enumerator enumerator = dict.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, List<string>> current = enumerator.Current;
            stringBuilder.AppendLine(current.Key + "\n");
            stringBuilder.Append(Tools.IEnumToString((IEnumerable<string>) current.Value));
          }
        }
      }
      return stringBuilder.ToString();
    }

    public static string IEnumToString(Dictionary<string, List<Dictionary<string, string>>> dict)
    {
      StringBuilder stringBuilder = new StringBuilder();
      if (dict != null)
      {
        using (Dictionary<string, List<Dictionary<string, string>>>.Enumerator enumerator1 = dict.GetEnumerator())
        {
          while (enumerator1.MoveNext())
          {
            KeyValuePair<string, List<Dictionary<string, string>>> current1 = enumerator1.Current;
            stringBuilder.AppendLine(current1.Key + "\n");
            using (List<Dictionary<string, string>>.Enumerator enumerator2 = current1.Value.GetEnumerator())
            {
              while (enumerator2.MoveNext())
              {
                Dictionary<string, string> current2 = enumerator2.Current;
                stringBuilder.Append(Tools.IEnumToString(current2));
              }
            }
          }
        }
      }
      return stringBuilder.ToString();
    }

    public static string IEnumToString(IEnumerable<string> enumerable)
    {
      StringBuilder stringBuilder = new StringBuilder();
      if (enumerable != null)
      {
        foreach (string str in enumerable)
        {
          stringBuilder.Append(str);
          stringBuilder.Append(",");
        }
      }
      return stringBuilder.ToString();
    }

    public static string ColorToHexRGB(Color32 color)
    {
      return color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
    }

    public static UnityEngine.Sprite TexToSprite(Texture2D texture)
    {
      Rect rect = new Rect(0.0f, 0.0f, (float) texture.width, (float) texture.height);
      return UnityEngine.Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100f);
    }

    public static UnityEngine.Sprite TexAtlasToSprite(Texture2D texture, Vector2 itemSize, uint index)
    {
      if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
        return (UnityEngine.Sprite) null;
      uint num1 = (uint) ((double) texture.width / (double) itemSize.x);
      uint num2 = (uint) ((double) texture.height / (double) itemSize.y);
      uint num3 = index / num1;
      Rect rect = new Rect((float) (index - num3 * num1) / (float) num1, (float) (1.0 - (double) (num3 + 1U) / (double) num2), itemSize.x / (float) texture.width, itemSize.y / (float) texture.height);
      rect = new Rect(rect.x * (float) texture.width, rect.y * (float) texture.height, itemSize.x, itemSize.y);
      return UnityEngine.Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f), 100f);
    }
  }
}
