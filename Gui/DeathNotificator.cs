﻿// Decompiled with JetBrains decompiler
// Type: Gui.DeathNotificator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class DeathNotificator : GuiPanel
  {
    private readonly GuiLabel label1 = new GuiLabel(Options.FontBGM_BT, 20);
    private readonly GuiLabel label2 = new GuiLabel(Options.FontBGM_BT, 20);
    private readonly float interval = 0.02f;
    private readonly float intervalRandom = 0.2f;
    private readonly char[] arr1;
    private int arrPosition1;
    private readonly char[] arr2;
    private int arrPosition2;

    public DeathNotificator(DeathDesc deathDesc, string message1, string message2)
    {
      this.MouseTransparent = true;
      message1 = message1.Length <= 0 ? string.Empty : BsgoLocalization.Get(message1);
      message2 = message2.Length <= 0 ? string.Empty : BsgoLocalization.Get(message2);
      this.arr1 = message1.ToCharArray();
      this.arrPosition1 = 0;
      this.SetLabel(this.label1, message1);
      this.arr2 = message2.ToCharArray();
      this.arrPosition2 = 0;
      this.SetLabel(this.label2, message2);
      this.label2.PositionY = 20f;
      this.AddChild((GuiElementBase) new Timer(this.interval, new AnonymousDelegate(this.OnTimer)));
    }

    public static void Show(DeathDesc deathDesc)
    {
      string message1;
      if (deathDesc.Killer == null)
        message1 = "%$bgo.death_notificator.message1%";
      else
        message1 = BsgoLocalization.Get("%$bgo.death_notificator.message1_full%", (object) deathDesc.Killer.Name);
      string message2 = string.Empty;
      if (deathDesc.RespawnLocations.Count > 0)
        message2 = Game.Me.Faction != Faction.Colonial ? BsgoLocalization.Get("%$bgo.death_notificator.message3_cylon%") : BsgoLocalization.Get("%$bgo.death_notificator.message3%");
      NotificationManager.Show((GuiElementBase) new DeathNotificator(deathDesc, message1, message2));
    }

    [Gui2Editor]
    public static void Show(string message1, string message2)
    {
      NotificationManager.Show((GuiElementBase) new DeathNotificator((DeathDesc) null, message1, message2));
    }

    private void SetLabel(GuiLabel label, string text)
    {
      label.Text = text;
      label.PositionX = (float) (-(double) label.SizeX / 2.0);
      label.Text = string.Empty;
      label.NormalColor = new Color(0.9f, 0.9f, 0.9f);
      label.OverColor = label.NormalColor;
      label.Alignment = TextAnchor.UpperCenter;
      this.AddChild((GuiElementBase) label, Align.UpLeft);
    }

    private void OnTimer()
    {
      if ((double) Random.value < (double) this.intervalRandom)
        return;
      if (this.arrPosition1 < this.arr1.Length)
      {
        this.label1.Text += (string) (object) this.arr1[this.arrPosition1];
        ++this.arrPosition1;
      }
      else
      {
        if (this.arr2.Length > 0)
        {
          this.label2.Text += (string) (object) this.arr2[this.arrPosition2];
          ++this.arrPosition2;
        }
        if (this.arrPosition2 != this.arr2.Length)
          return;
        this.RemoveChild((GuiElementBase) this.Find<Timer>());
        this.AddChild((GuiElementBase) new TimerSimple(2f, new AnonymousDelegate(this.Close)));
      }
    }

    private void Close()
    {
      this.RemoveThisFromParentLater();
    }

    [Gui2Editor]
    private static SpaceLevel Level()
    {
      return SpaceLevel.GetLevel();
    }

    [Gui2Editor]
    private static bool IsInstance()
    {
      return SpaceLevel.GetLevel().IsStory;
    }
  }
}
