﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiShopCantBuyWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiShopCantBuyWindow : GuiPanel
  {
    private string m_meritsText = "%$bgo.inflight_shop.notEnoughMoneyMerits%";
    private string m_repeatText = "%$bgo.inflight_shop.no_repeat_item%";
    private GuiLabel m_textLabel;
    private GuiButton m_yesButton;
    private GuiButton m_cancelButton;
    private bool m_forMerits;
    private bool m_forRepeat;
    private string m_normalText;

    public AnonymousDelegate HandlerYes
    {
      set
      {
        this.m_yesButton.Pressed = value;
      }
    }

    public GuiShopCantBuyWindow()
      : base("GUI/EquipBuyPanel/gui_cantbuy_layout")
    {
      this.Size = this.Find<GuiImage>("background_image").Size;
      this.m_textLabel = this.Find<GuiLabel>("text_label");
      this.m_normalText = this.m_textLabel.Text;
      this.m_yesButton = this.Find<GuiButton>("yes_button");
      this.m_cancelButton = this.Find<GuiButton>("cancel_button");
      this.m_cancelButton.Pressed = new AnonymousDelegate(this.OnCancelButton);
      this.m_yesButton.Pressed = (AnonymousDelegate) (() =>
      {
        ExternalApi.EvalJs("window.createPaymentSession();");
        this.OnHide();
      });
      this.IsMouseInputGrabber = true;
    }

    protected void OnCancelButton()
    {
      this.OnHide();
    }

    public override void OnShow()
    {
      this.m_yesButton.IsRendered = (this.m_forMerits ? 1 : (this.m_forRepeat ? 1 : 0)) == 0;
      this.m_textLabel.Text = !this.m_forMerits ? (!this.m_forRepeat ? this.m_normalText : this.m_repeatText) : this.m_meritsText;
      this.m_cancelButton.Position = new Vector2(this.m_forMerits || this.m_forRepeat ? 0.0f : 50f, this.m_cancelButton.Position.y);
      this.m_cancelButton.Label.Text = this.m_forMerits || this.m_forRepeat ? "%$bgo.common.ok%" : "%$bgo.EquipBuyPanel.gui_cantbuy_layout.label_0%";
      this.IsRendered = true;
      Game.InputDispatcher.Focused = (InputListener) this;
      base.OnShow();
    }

    public override void OnHide()
    {
      this.IsRendered = false;
      Game.InputDispatcher.Focused = (InputListener) null;
      base.OnHide();
    }
  }
}
