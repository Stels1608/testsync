﻿// Decompiled with JetBrains decompiler
// Type: Gui.Common.MoneyCostVertical
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui.Common
{
  public class MoneyCostVertical : QueueVertical
  {
    private int ratio = 1;
    public ShopItemCard shopItemCard;
    private Price price;

    public int Ratio
    {
      get
      {
        return this.ratio;
      }
      set
      {
        this.ratio = value;
        this.UpdateValues();
      }
    }

    public Price Price
    {
      get
      {
        return this.price;
      }
      set
      {
        this.price = value;
        this.UpdateValues();
      }
    }

    public MoneyCostVertical(string title)
      : this()
    {
      this.AddChild((GuiElementBase) new GuiLabel(title));
    }

    public MoneyCostVertical()
    {
      this.ContainerAutoSize = true;
      this.Space = 10f;
    }

    private void UpdateValues()
    {
      if (this.price == null)
        return;
      while (this.ChildrenOfType<MoneyCostVertical.Item>().Count > this.price.items.Count)
        this.RemoveChild((GuiElementBase) this.ChildrenOfType<MoneyCostVertical.Item>()[0]);
      while (this.ChildrenOfType<MoneyCostVertical.Item>().Count < this.price.items.Count)
        this.AddChild((GuiElementBase) new MoneyCostVertical.Item());
      List<MoneyCostVertical.Item> objList = this.ChildrenOfType<MoneyCostVertical.Item>();
      int index = 0;
      using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = this.price.items.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
          float result = (float) this.ratio * current.Value;
          if (this.shopItemCard != null && this.price == this.shopItemCard.BuyPrice)
            Shop.ApplyDiscount(out result, this.shopItemCard.CardGUID, result);
          objList[index].Set(current.Key.GUICard.GUIIcon, result.ToString("#0.0"), current.Key.GUICard.Description);
          ++index;
        }
      }
    }

    private class Item : GuiPanel
    {
      public GuiImage image = new GuiImage();
      public GuiLabel label = new GuiLabel();

      public Item()
      {
        this.AddChild((GuiElementBase) this.image);
        this.AddChild((GuiElementBase) this.label, Align.MiddleRight);
      }

      public void Set(string icon, string count, string tooltip)
      {
        this.image.TexturePath = icon;
        this.label.Text = count.ToString();
        this.SetTooltip(tooltip);
        this.SizeX = (float) ((double) this.image.SizeX + (double) this.label.SizeX + 5.0);
        this.SizeY = Mathf.Max(this.image.SizeY, this.label.SizeY);
      }
    }
  }
}
