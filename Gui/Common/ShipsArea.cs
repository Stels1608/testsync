﻿// Decompiled with JetBrains decompiler
// Type: Gui.Common.ShipsArea
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Hangar;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Gui.Common
{
  public class ShipsArea : GuiTabsSmall
  {
    private Dictionary<int, List<ShipIcon>> shipLayoutArray = new Dictionary<int, List<ShipIcon>>();
    private bool alreadyLoaded;
    public bool isMe;
    private List<GuiPanel> shipTierPanel;

    public ShipsArea()
    {
      this.Size = new Vector2(281f, 216f);
      this.InitPanels();
    }

    private void InitPanels()
    {
      GuiImage guiImage = new GuiImage(ResourceLoader.Load<Texture2D>("GUI/InfoJournal/friendsbackground"));
      guiImage.NineSliceEdge = new GuiElementPadding(new Rect(5f, 5f, 5f, 5f));
      guiImage.Size = new Vector2(292f, 136f);
      guiImage.Position = new Vector2(-2f, 30f);
      this.AddChild((GuiElementBase) guiImage);
      this.Size = new Vector2(292f, 216f);
      this.shipTierPanel = new List<GuiPanel>();
      for (int index = 1; index < 5; ++index)
      {
        GuiPanel guiPanel = new GuiPanel();
        guiPanel.Size = new Vector2(280f, 130f);
        guiPanel.Position = new Vector2(0.0f, 30f);
        this.shipTierPanel.Add(guiPanel);
        this.AddTab(Tools.ParseMessage("%$bgo.slottooltip.tier%") + " " + (object) index, (GuiElementBase) guiPanel);
      }
      this.SelectIndex(0);
    }

    protected virtual Faction GetFaction()
    {
      return Game.Me.Faction;
    }

    protected virtual ShipCard[] GetOwnedCards()
    {
      return Array.ConvertAll<HangarShip, ShipCard>(new List<HangarShip>(Game.Me.Hangar.Ships).ToArray(), (Converter<HangarShip, ShipCard>) (h => h.Card));
    }

    protected virtual ShipCard GetActiveShip()
    {
      return Game.Me.Hangar.ActiveShip.Card;
    }

    private bool IsOwned(ShipCard card)
    {
      foreach (ShipCard ownedCard in this.GetOwnedCards())
      {
        if (ownedCard.Equals(card))
          return true;
      }
      return false;
    }

    public void UpdateDisplay()
    {
      if (!this.alreadyLoaded)
        return;
      ShipListCard shipList = StaticCards.Instance.GetShipList(this.GetFaction());
      using (Dictionary<int, List<ShipIcon>>.Enumerator enumerator1 = this.shipLayoutArray.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          using (List<ShipIcon>.Enumerator enumerator2 = enumerator1.Current.Value.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              ShipIcon current = enumerator2.Current;
              if (current != null)
              {
                current.PeriodicUpdate();
                for (uint index = 0; (long) index < (long) current.variantIcons.Length; ++index)
                {
                  GuiButton guiButton = current.variantIcons[(IntPtr) index];
                  ShipCard variantShipCard = shipList.GetVariantShipCard(current.Card, index);
                  guiButton.NormalTexture = variantShipCard == null || !this.IsOwned(variantShipCard) ? ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Button_dark") : (!this.GetActiveShip().Equals(variantShipCard) ? ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Button_light") : ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Buttons_fill"));
                  guiButton.OverTexture = guiButton.NormalTexture;
                  guiButton.PressedTexture = guiButton.NormalTexture;
                }
              }
            }
          }
        }
      }
    }

    public override void PeriodicUpdate()
    {
      this.UpdateDisplay();
      if (this.alreadyLoaded)
        return;
      ShipListCard shipList = StaticCards.Instance.GetShipList(this.GetFaction());
      if (!(bool) shipList.IsLoaded)
        return;
      Faction faction = this.GetFaction();
      for (int index = 0; index < 4; ++index)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: reference to a compiler-generated method
        IOrderedEnumerable<ShipCard> orderedEnumerable = ((IEnumerable<ShipCard>) shipList.ShipCards).Where<ShipCard>(new Func<ShipCard, bool>(new ShipsArea.\u003CPeriodicUpdate\u003Ec__AnonStorey8C() { innerTier = index }.\u003C\u003Em__F9)).OrderBy<ShipCard, byte>((Func<ShipCard, byte>) (sh => sh.HangarID));
        this.shipLayoutArray[index] = new List<ShipIcon>();
        int num1 = 0;
        int num2 = 0;
        foreach (ShipCard card in (IEnumerable<ShipCard>) orderedEnumerable)
        {
          if (num1 == 3)
          {
            ++num2;
            num1 = 0;
          }
          ShipIcon icon = new ShipIcon(faction, card, this.IsOwned(card), card.GetHangarShip != null && card.GetHangarShip.IsUpgraded, Game.Me.ActiveShip.Card, this.isMe);
          this.shipLayoutArray[index].Add(icon);
          icon.Size = icon.Size;
          icon.Position = new Vector2((float) (num1 * 90), (float) (num2 * 55));
          icon.OnSelected = (System.Action<ShipIcon>) null;
          icon.ShowText(false);
          icon.ShowOverlay(false);
          if (icon.Card.HaveShip && this.isMe)
            icon.SetTooltip(!(icon.GetHangarShip().Name == string.Empty) ? icon.GetHangarShip().Name + "\n(" + icon.GetHangarShip().GUICard.Name + ")" : icon.GetHangarShip().GUICard.Name);
          else
            icon.SetTooltip(icon.Card.ItemGUICard.Name);
          this.shipTierPanel[index].AddChild((GuiElementBase) icon);
          this.AddVariantButtons(icon, this.shipTierPanel[index]);
          ++num1;
        }
      }
      this.alreadyLoaded = true;
    }

    private void AddVariantButtons(ShipIcon icon, GuiPanel panel)
    {
      GuiPanel guiPanel = new GuiPanel();
      guiPanel.Size = new Vector2(40f, 20f);
      for (int index = 0; index < icon.variantIcons.Length; ++index)
      {
        GuiButton guiButton = icon.variantIcons[index];
        guiPanel.AddChild((GuiElementBase) guiButton);
        guiButton.PositionCenter = new Vector2((float) (5 + index * 15), 10f);
      }
      guiPanel.Position = new Vector2(icon.PositionX + 30f, icon.PositionY + 55f);
      panel.AddChild((GuiElementBase) guiPanel);
    }
  }
}
