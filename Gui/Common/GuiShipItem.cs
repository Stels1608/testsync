﻿// Decompiled with JetBrains decompiler
// Type: Gui.Common.GuiShipItem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Tooltips;
using UnityEngine;

namespace Gui.Common
{
  public class GuiShipItem : GuiPanel
  {
    private readonly GuiAtlasImage image = new GuiAtlasImage();
    private readonly GuiLabel label = new GuiLabel();
    private ShipItem item;

    public ShipItem ShipItem
    {
      get
      {
        return this.item;
      }
      set
      {
        this.item = value;
        if (this.item == null)
        {
          this.Image.GuiCard = (GUICard) null;
          this.label.Text = string.Empty;
          this.ToolTip = (GuiAdvancedTooltipBase) null;
        }
        else
        {
          this.Image.GuiCard = this.item.ItemGUICard;
          this.label.Text = (!(this.item is ItemCountable) ? string.Empty : (this.item as ItemCountable).Count.ToString() + " ") + this.item.ItemGUICard.Name;
          this.SetTooltip(this.item.ItemGUICard.Description);
        }
        this.SizeX = (float) ((double) this.label.PositionX + (double) this.label.SizeX + 10.0);
        this.SizeY = Mathf.Max(this.Image.SizeY, this.label.SizeY);
      }
    }

    public GuiAtlasImage Image
    {
      get
      {
        return this.image;
      }
    }

    public GuiShipItem(ShipItem item)
    {
      this.AddChild((GuiElementBase) this.Image, Align.MiddleLeft);
      this.AddChild((GuiElementBase) this.label, Align.MiddleLeft, new Vector2(50f, 0.0f));
      this.ShipItem = item;
    }
  }
}
