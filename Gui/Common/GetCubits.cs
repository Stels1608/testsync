﻿// Decompiled with JetBrains decompiler
// Type: Gui.Common.GetCubits
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Common
{
  internal class GetCubits : GuiButton
  {
    public GetCubits()
      : base(string.Empty, "GUI/Common/Buttons/getcubits", "GUI/Common/Buttons/getcubits_over", "GUI/Common/Buttons/getcubits_click")
    {
      this.Pressed = new AnonymousDelegate(ShopWindow.OnGetCubits);
      this.label.Text = "%$bgo.common.get_cubits%";
      this.label.Font = Gui.Options.FontBGM_BT;
      this.label.FontSize = 16;
      this.label.AutoSize = false;
      this.label.Alignment = TextAnchor.MiddleCenter;
      this.label.Size = this.Size;
      this.SetTooltip("%$bgo.common.get_cubits_tooltip%");
    }
  }
}
