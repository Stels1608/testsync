﻿// Decompiled with JetBrains decompiler
// Type: Gui.Common.AvatarView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Common
{
  public class AvatarView : GuiElementBase
  {
    public static readonly int layer = LayerMask.NameToLayer("AvatarInfoWindow");
    private readonly string cameraGoName = "AvatarGoCamera" + (object) UnityEngine.Random.value;
    private readonly string avatarGoName = "AvatarGo" + (object) UnityEngine.Random.value;
    private const string avatarLightGoName = "AvatarLight";
    private static Light avatarLight;
    private AvatarItems avatarDesc;
    private Avatar avatar;
    private GameObject avatarGo;
    private GameObject cameraGo;
    private Camera camera;

    public AvatarView(AvatarItems avatarDesc)
    {
      this.Size = new Vector2(200f, 400f);
      this.avatarDesc = avatarDesc;
      this.cameraGo = this.InitializeCameraGo();
      this.avatarGo = this.InitializeAvatarGo();
      this.camera = this.cameraGo.GetComponent<Camera>();
      this.avatar = (Avatar) null;
      Camera.main.cullingMask &= ~(1 << AvatarView.layer);
      this.avatarGo.transform.position = Vector3.zero;
      AvatarView.CheckAvatar();
    }

    public static int GetCullingMask()
    {
      return 1 << AvatarView.layer;
    }

    public static void CheckAvatar()
    {
      if ((UnityEngine.Object) GameObject.Find("AvatarLight") != (UnityEngine.Object) null)
        return;
      AvatarView.avatarLight = new GameObject("AvatarLight")
      {
        transform = {
          rotation = new Quaternion(0.0f, 180f, 0.0f, 1f)
        }
      }.AddComponent<Light>();
      AvatarView.avatarLight.intensity = 1f;
      AvatarView.avatarLight.type = LightType.Directional;
      AvatarView.avatarLight.cullingMask = AvatarView.GetCullingMask();
      AvatarView.avatarLight.renderMode = LightRenderMode.ForceVertex;
      AvatarView.avatarLight.enabled = false;
    }

    private void RemoveResources()
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.cameraGo);
      this.cameraGo = (GameObject) null;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.avatarGo);
      this.avatarGo = (GameObject) null;
      this.camera = (Camera) null;
      this.avatar = (Avatar) null;
    }

    private GameObject InitializeCameraGo()
    {
      GameObject gameObject = new GameObject(this.cameraGoName);
      gameObject.transform.rotation = new Quaternion(0.0f, 180f, 0.0f, 1f);
      Camera camera = gameObject.AddComponent<Camera>();
      camera.clearFlags = CameraClearFlags.Depth;
      camera.farClipPlane = 50000f;
      camera.depth = 3f;
      camera.cullingMask = 1 << AvatarView.layer;
      camera.enabled = false;
      camera.renderingPath = RenderingPath.Forward;
      return gameObject;
    }

    private GameObject InitializeAvatarGo()
    {
      return new GameObject(this.avatarGoName);
    }

    public override void Reposition()
    {
      base.Reposition();
      if (!((UnityEngine.Object) this.camera != (UnityEngine.Object) null))
        return;
      float left = (this.Rect.xMin - 1f) / (float) Screen.width;
      float num = (this.Rect.yMin - 1f) / (float) Screen.height;
      float width = (this.Rect.width - 2f) / (float) Screen.width;
      float height = (this.Rect.height - 2f) / (float) Screen.height;
      float top = 1f - num - height;
      this.camera.rect = new Rect(left, top, width, height);
    }

    public override void Update()
    {
      base.Update();
      this.ConstructAvatar();
    }

    private void ConstructAvatar()
    {
      if ((UnityEngine.Object) this.avatarGo == (UnityEngine.Object) null)
        return;
      if (this.avatar != null)
      {
        this.avatar.Update();
      }
      else
      {
        if (!(bool) StaticCards.Avatar.IsLoaded)
          return;
        try
        {
          this.avatar = AvatarSelector.Create(this.avatarDesc) ?? AvatarSelector.Create("human", "male");
        }
        catch
        {
          this.avatar = AvatarSelector.Create("human", "male");
        }
        this.avatar.loadedCallback = new System.Action(this.PrepareAvatar);
      }
    }

    private void PrepareAvatar()
    {
      AvatarSelector.ClearAvatar(this.avatar);
      this.avatar.obj.layer = AvatarView.layer;
      this.avatar.obj.transform.parent = this.avatarGo.transform;
      this.avatar.obj.transform.localPosition = Vector3.zero;
      this.SetLayerToAll(this.avatarGo, AvatarView.layer);
      string str = this.avatarDesc.GetItem(AvatarItem.Race);
      this.cameraGo.transform.position = Vector3.zero;
      this.cameraGo.transform.position += !(str == "human") ? new Vector3(0.0f, 1.1f, 2f) : new Vector3(0.0f, 0.9f, 1.6f);
    }

    private void SetLayerToAll(GameObject obj, int layer)
    {
      obj.layer = layer;
      for (int index = 0; index < obj.transform.childCount; ++index)
        this.SetLayerToAll(obj.transform.GetChild(index).gameObject, layer);
    }

    public override void Draw()
    {
      base.Draw();
      if (!((UnityEngine.Object) this.camera != (UnityEngine.Object) null) || Event.current.type != UnityEngine.EventType.Repaint)
        return;
      if ((UnityEngine.Object) AvatarView.avatarLight != (UnityEngine.Object) null)
        AvatarView.avatarLight.enabled = true;
      this.camera.Render();
      if (!((UnityEngine.Object) AvatarView.avatarLight != (UnityEngine.Object) null))
        return;
      AvatarView.avatarLight.enabled = false;
    }

    public void UpdateAvatar(AvatarDescription desc)
    {
      this.avatarDesc = (AvatarItems) desc;
      if (this.avatar == null)
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.avatar.obj);
      this.avatar = (Avatar) null;
    }

    public void DeleteModel()
    {
      this.camera = (Camera) null;
      if ((UnityEngine.Object) this.cameraGo != (UnityEngine.Object) null)
      {
        UnityEngine.Object.Destroy((UnityEngine.Object) this.cameraGo);
        this.cameraGo = (GameObject) null;
      }
      if (!((UnityEngine.Object) this.avatarGo != (UnityEngine.Object) null))
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.avatarGo);
      this.avatarGo = (GameObject) null;
    }
  }
}
