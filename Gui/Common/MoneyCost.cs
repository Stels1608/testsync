﻿// Decompiled with JetBrains decompiler
// Type: Gui.Common.MoneyCost
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gui.Common
{
  public class MoneyCost : QueueHorizontal
  {
    private GuiLabel title;
    public ShopItemCard shopItemCard;
    private List<MoneyCost.Item> itemList;

    public Price Price
    {
      set
      {
        Price price = value;
        if (this.title != null)
          this.title.IsRendered = price != null;
        if (price == null)
        {
          this.RemoveAll((IEnumerable) this.ChildrenOfType<MoneyCost.Item>());
        }
        else
        {
          while (this.ChildrenOfType<MoneyCost.Item>().Count > price.items.Count)
            this.RemoveChild((GuiElementBase) this.ChildrenOfType<MoneyCost.Item>()[0]);
          while (this.ChildrenOfType<MoneyCost.Item>().Count < price.items.Count)
            this.AddChild((GuiElementBase) new MoneyCost.Item());
          this.itemList = this.ChildrenOfType<MoneyCost.Item>();
          int index = 0;
          using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = price.items.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
              float result = current.Value;
              if (this.shopItemCard != null && price == this.shopItemCard.BuyPrice)
                Shop.ApplyDiscount(out result, this.shopItemCard.CardGUID, result);
              this.itemList[index].Set(current.Key.GUICard.GUIIcon, result.ToString("#0.0"), current.Key.GUICard.Description);
              ++index;
            }
          }
        }
      }
    }

    public MoneyCost(string title)
      : this()
    {
      this.title = new GuiLabel(title);
      this.AddChild((GuiElementBase) this.title);
    }

    public MoneyCost()
    {
      this.ContainerAutoSize = true;
      this.Space = 10f;
    }

    public void ShowIcons(bool show)
    {
      using (List<MoneyCost.Item>.Enumerator enumerator = this.itemList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.image.IsRendered = show;
      }
    }

    public void ShowStrikeoutLine(bool showLine)
    {
      using (List<MoneyCost.Item>.Enumerator enumerator = this.itemList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.strikeLine.IsRendered = showLine;
      }
    }

    private class Item : GuiPanel
    {
      public GuiImage image = new GuiImage();
      public GuiLabel label = new GuiLabel();
      public GuiColoredBox strikeLine = new GuiColoredBox(Gui.Options.PositiveColor);

      public Item()
      {
        this.AddChild((GuiElementBase) this.image);
        this.AddChild((GuiElementBase) this.label, Align.UpRight);
        this.AddChild((GuiElementBase) this.strikeLine, Align.UpRight);
        this.strikeLine.IsRendered = false;
      }

      public void Set(string icon, string count, string tooltip)
      {
        this.image.TexturePath = icon;
        this.label.Text = count.ToString();
        this.SizeX = (float) ((double) this.image.SizeX + (double) this.label.SizeX + 5.0);
        this.SizeY = Mathf.Max(this.image.SizeY, this.label.SizeY);
        this.strikeLine.Position = new Vector2(-2f, (float) ((double) this.label.SizeY / 2.0 - 1.0));
        this.strikeLine.Size = new Vector2(this.label.SizeX, 2f);
      }
    }
  }
}
