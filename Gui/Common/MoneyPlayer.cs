﻿// Decompiled with JetBrains decompiler
// Type: Gui.Common.MoneyPlayer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Gui.Common
{
  public class MoneyPlayer : QueueHorizontal
  {
    private GuiImage imageCubits = new GuiImage();
    private GuiLabel cubits = new GuiLabel();
    private GuiImage imageThylium = new GuiImage();
    private GuiLabel tylium = new GuiLabel();
    private GuiImage imageTitanium = new GuiImage();
    private GuiLabel titanium = new GuiLabel();
    private GuiImage imageWater = new GuiImage();
    private GuiLabel water = new GuiLabel();
    private GuiImage imageTokens = new GuiImage();
    private GuiLabel tokens = new GuiLabel();
    private bool imagesLoaded;

    public MoneyPlayer(bool showOnlyCubits)
    {
      this.ContainerAutoSize = true;
      this.AddChild((GuiElementBase) this.imageCubits, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(5f));
      this.AddChild((GuiElementBase) this.cubits, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(10f));
    }

    public MoneyPlayer()
    {
      this.ContainerAutoSize = true;
      this.AddChild((GuiElementBase) this.imageTokens, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(5f));
      this.AddChild((GuiElementBase) this.tokens, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(10f));
      this.AddChild((GuiElementBase) this.imageCubits, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(5f));
      this.AddChild((GuiElementBase) this.cubits, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(10f));
      this.AddChild((GuiElementBase) this.imageThylium, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(5f));
      this.AddChild((GuiElementBase) this.tylium, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(10f));
      this.AddChild((GuiElementBase) this.imageWater, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(5f));
      this.AddChild((GuiElementBase) this.water, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(10f));
      this.AddChild((GuiElementBase) this.imageTitanium, Align.MiddleLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(5f));
      this.AddChild((GuiElementBase) this.titanium, Align.MiddleLeft);
    }

    public override void PeriodicUpdate()
    {
      if (!this.imagesLoaded)
      {
        GUICard guiCard1 = Game.Catalogue.FetchCard(130920111U, CardView.GUI) as GUICard;
        if (!(bool) guiCard1.IsLoaded)
          return;
        this.imageTokens.TexturePath = guiCard1.GUIIcon;
        this.imageTokens.SetTooltip(guiCard1.Description);
        this.tokens.SetTooltip(guiCard1.Description);
        GUICard guiCard2 = Game.Catalogue.FetchCard(264733124U, CardView.GUI) as GUICard;
        if (!(bool) guiCard2.IsLoaded)
          return;
        this.imageCubits.TexturePath = guiCard2.GUIIcon;
        this.imageCubits.SetTooltip(guiCard2.Description);
        this.cubits.SetTooltip(guiCard2.Description);
        GUICard guiCard3 = Game.Catalogue.FetchCard(215278030U, CardView.GUI) as GUICard;
        if (!(bool) guiCard3.IsLoaded)
          return;
        this.imageThylium.TexturePath = guiCard3.GUIIcon;
        this.imageThylium.SetTooltip(guiCard3.Description);
        this.tylium.SetTooltip(guiCard3.Description);
        GUICard guiCard4 = Game.Catalogue.FetchCard(207047790U, CardView.GUI) as GUICard;
        if (!(bool) guiCard4.IsLoaded)
          return;
        this.imageTitanium.TexturePath = guiCard4.GUIIcon;
        this.imageTitanium.SetTooltip(guiCard4.Description);
        this.titanium.SetTooltip(guiCard4.Description);
        GUICard guiCard5 = Game.Catalogue.FetchCard(130762195U, CardView.GUI) as GUICard;
        if (!(bool) guiCard5.IsLoaded)
          return;
        this.imageWater.TexturePath = guiCard5.GUIIcon;
        this.imageWater.SetTooltip(guiCard5.Description);
        this.water.SetTooltip(guiCard5.Description);
        this.imagesLoaded = true;
      }
      Hold hold = Game.Me.Hold;
      this.tokens.Text = hold.Token.ToString();
      this.cubits.Text = hold.Cubits.ToString();
      this.tylium.Text = hold.Tylium.ToString();
      this.titanium.Text = hold.Titanium.ToString();
      this.water.Text = hold.Water.ToString();
      base.PeriodicUpdate();
    }

    public void SetDifferentValuesCubits(uint cubitsValue = 0)
    {
      this.cubits.Text = cubitsValue.ToString();
    }
  }
}
