﻿// Decompiled with JetBrains decompiler
// Type: Gui.Common.ShipStats
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Paperdolls.PaperdollServerData;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui.Common
{
  public class ShipStats : GuiPanelVerticalScrollNonDynamic
  {
    private readonly GuiLabel desc = new GuiLabel();

    public ShipStats(Vector2 size)
    {
      this.Size = size;
      this.AutoSizeChildren = true;
      this.RenderDelimiters = false;
      this.desc.WordWrap = true;
      this.AddChild((GuiElementBase) this.desc);
      this.AddChild((GuiElementBase) new GuiSpaceY(5f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.ship_class%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.role%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(15f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.systems%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.weapon%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.gun%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.launcher%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.defensive_weapon%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.special_weapon%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.hull1%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.engine%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.computer%", string.Empty, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(15f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.hull_points%", "%$bgo.stats.hull_points_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.hull_recovery%", "%$bgo.stats.hull_recovery_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.durability%", "%$bgo.stats.durability_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.armor%", "%$bgo.stats.armor_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.critical_defense%", "%$bgo.stats.critical_defense_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(15f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.avoidance%", "%$bgo.stats.avoidance_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.turning_speed%", "%$bgo.stats.turning_speed_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.turning_acceleration%", "%$bgo.stats.turning_acceleration_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.inertia_compensation%", "%$bgo.stats.inertia_compensation_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.acceleration%", "%$bgo.stats.acceleration_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.speed%", "%$bgo.stats.speed_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.boost_speed%", "%$bgo.stats.boost_speed_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.boost_cost%", "%$bgo.stats.boost_cost_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(15f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.ftl_range%", "%$bgo.stats.ftl_range_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.ftl_charge%", "%$bgo.stats.ftl_charge_tooltip%", true, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.ftl_cost%", "%$bgo.stats.ftl_cost_tooltip%", true, size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(15f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.power%", "%$bgo.stats.power_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.power_recharge%", "%$bgo.stats.power_recharge_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.firewall_rating%", "%$bgo.stats.firewall_rating_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.penetration_strength%", "%$bgo.stats.penetration_strength_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.dradis_range%", "%$bgo.stats.dradis_range_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
      this.AddChild((GuiElementBase) new ShipStats.Item("%$bgo.stats.dradis_visual_range%", "%$bgo.stats.dradis_visual_range_tooltip%", size.x));
      this.AddChild((GuiElementBase) new GuiSpaceY(10f));
    }

    public void ShowMyStatistics()
    {
      ShipCard shipCard = Game.Me.Hangar.ActiveShip.Card;
      this.TakeStatisticsCard(shipCard, shipCard);
      this.TakeStatisticsStats(shipCard.Stats, (ObjectStats) Game.Me.Stats);
      this.Mark("%$bgo.stats.durability%", shipCard.Durability, Game.Me.Hangar.ActiveShip.Durability, (string) null);
    }

    public void ShowDifference(ShipCard card1, ShipCard card2)
    {
      this.TakeStatisticsCard(card1, card2);
      this.TakeStatisticsStats(card1.Stats, card2.Stats);
    }

    private void TakeStatisticsCard(ShipCard c1, ShipCard c2)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      ShipStats.\u003CTakeStatisticsCard\u003Ec__AnonStorey8B cardCAnonStorey8B = new ShipStats.\u003CTakeStatisticsCard\u003Ec__AnonStorey8B();
      // ISSUE: reference to a compiler-generated field
      cardCAnonStorey8B.c1 = c1;
      // ISSUE: reference to a compiler-generated field
      cardCAnonStorey8B.c2 = c2;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      List<ShipSlotCard> all1 = cardCAnonStorey8B.c1.Slots.FindAll(new Predicate<ShipSlotCard>(cardCAnonStorey8B.\u003C\u003Em__E6));
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      List<ShipSlotCard> all2 = cardCAnonStorey8B.c2.Slots.FindAll(new Predicate<ShipSlotCard>(cardCAnonStorey8B.\u003C\u003Em__E7));
      // ISSUE: reference to a compiler-generated field
      this.desc.Text = cardCAnonStorey8B.c1.ItemGUICard.Description;
      // ISSUE: reference to a compiler-generated field
      this.Mark("%$bgo.stats.ship_class%", "%$bgo.common.ship_class_tier_" + (object) cardCAnonStorey8B.c2.Tier + "%");
      // ISSUE: reference to a compiler-generated field
      this.Mark("%$bgo.stats.role%", "%$bgo.common.role_" + (object) cardCAnonStorey8B.c2.ShipRoleDeprecated + "%");
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      this.Mark("%$bgo.stats.durability%", cardCAnonStorey8B.c1.Durability, cardCAnonStorey8B.c2.Durability, (string) null);
      string[] strArray = new string[8]{ "%$bgo.stats.weapon%", "%$bgo.stats.gun%", "%$bgo.stats.launcher%", "%$bgo.stats.defensive_weapon%", "%$bgo.stats.hull1%", "%$bgo.stats.engine%", "%$bgo.stats.computer%", "%$bgo.stats.special_weapon%" };
      int[] numArray1 = new int[8]{ all1.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.weapon)).Count, all1.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.gun)).Count, all1.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.launcher)).Count, all1.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.defensive_weapon)).Count, all1.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.hull)).Count, all1.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.engine)).Count, all1.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.computer)).Count, all1.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.special_weapon)).Count };
      int[] numArray2 = new int[8]{ all2.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.weapon)).Count, all2.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.gun)).Count, all2.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.launcher)).Count, all2.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.defensive_weapon)).Count, all2.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.hull)).Count, all2.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.engine)).Count, all2.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.computer)).Count, all2.FindAll((Predicate<ShipSlotCard>) (slotData => slotData.SystemType == ShipSlotType.special_weapon)).Count };
      // ISSUE: reference to a compiler-generated field
      string str1 = "%$bgo.common.size_tier_" + (object) cardCAnonStorey8B.c2.Tier + "%";
      for (int index = 0; index < strArray.Length; ++index)
      {
        this.Mark(strArray[index], (float) numArray1[index], (float) numArray2[index], "#0");
        ShipStats.Item obj = this.Take(strArray[index]);
        string str2 = obj.Value + " " + str1;
        obj.Value = str2;
      }
    }

    private void TakeStatisticsStats(ObjectStats stats1, ObjectStats stats2)
    {
      this.Mark("%$bgo.stats.critical_defense%", stats1.CriticalDefense, stats2.CriticalDefense, (string) null);
      this.Mark("%$bgo.stats.armor%", stats1.ArmorValue, stats2.ArmorValue, (string) null);
      this.Mark("%$bgo.stats.avoidance%", stats1.Avoidance, stats2.Avoidance, (string) null);
      this.Mark("%$bgo.stats.turning_speed%", stats1.TurnSpeed, stats2.TurnSpeed, "%$bgo.statsdescription.turning_speed.valueformat%");
      this.Mark("%$bgo.stats.turning_acceleration%", stats1.TurnAcceleration, stats2.TurnAcceleration, "%$bgo.statsdescription.turning_acceleration.valueformat%");
      this.Mark("%$bgo.stats.inertia_compensation%", stats1.InertiaCompensation, stats2.InertiaCompensation, "%$bgo.statsdescription.etc.meters_per_second.valueformat%");
      this.Mark("%$bgo.stats.acceleration%", stats1.Acceleration, stats2.Acceleration, "%$bgo.statsdescription.etc.acceleration.valueformat%");
      this.Mark("%$bgo.stats.speed%", stats1.MaxSpeed, stats2.MaxSpeed, "%$bgo.statsdescription.etc.meters_per_second.valueformat%");
      this.Mark("%$bgo.stats.boost_speed%", stats1.BoostSpeed, stats2.BoostSpeed, "%$bgo.statsdescription.etc.meters_per_second.valueformat%");
      this.Mark("%$bgo.stats.boost_cost%", stats1.BoostCost, stats2.BoostCost, "%$bgo.statsdescription.etc.tylium_per_second.valueformat%");
      this.Mark("%$bgo.stats.ftl_range%", stats1.FTLRange / 20f, stats2.FTLRange / 20f, "%$bgo.statsdescription.etc.lightyears.valueformat%");
      this.Mark("%$bgo.stats.ftl_charge%", stats1.FTLCharge, stats2.FTLCharge, "%$bgo.statsdescription.etc.seconds.valueformat%");
      this.Mark("%$bgo.stats.ftl_cost%", stats1.FTLCost * 20f, stats2.FTLCost * 20f, "%$bgo.statsdescription.etc.tylium.valueformat%");
      this.Mark("%$bgo.stats.power_recharge%", stats1.PowerRecovery, stats2.PowerRecovery, "%$bgo.statsdescription.etc.per_second.valueformat%");
      this.Mark("%$bgo.stats.firewall_rating%", stats1.FirewallRating, stats2.FirewallRating, (string) null);
      this.Mark("%$bgo.stats.penetration_strength%", stats1.PenetrationStrength, stats2.PenetrationStrength, (string) null);
      this.Mark("%$bgo.stats.hull_points%", stats1.MaxHullPoints, stats2.MaxHullPoints, (string) null);
      this.Mark("%$bgo.stats.hull_recovery%", stats1.HullRecovery, stats2.HullRecovery, "%$bgo.statsdescription.etc.per_second.valueformat%");
      this.Mark("%$bgo.stats.power%", stats1.MaxPowerPoints, stats2.MaxPowerPoints, (string) null);
      this.Mark("%$bgo.stats.dradis_range%", stats1.DetectionOuterRadius, stats2.DetectionOuterRadius, "%$bgo.statsdescription.etc.meters.valueformat%");
      this.Mark("%$bgo.stats.dradis_visual_range%", stats1.DetectionVisualRadius, stats2.DetectionVisualRadius, "%$bgo.statsdescription.etc.meters.valueformat%");
    }

    private string GetLocalizedFormat(string locaKey)
    {
      string str;
      BsgoLocalization.TryGet(locaKey, out str);
      if (string.IsNullOrEmpty(str))
        return "{0}";
      return str;
    }

    public bool IsUpgradable(ShipCard shipCard)
    {
      return shipCard.NextCard != null;
    }

    private ShipStats.Item Take(string name)
    {
      using (List<ShipStats.Item>.Enumerator enumerator = this.ChildrenOfType<ShipStats.Item>().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ShipStats.Item current = enumerator.Current;
          if (current.Name == name)
            return current;
        }
      }
      throw new ArgumentException("Item named:" + name + ", not found");
    }

    private void Mark(string entryName, string currentValue)
    {
      ShipStats.Item obj = this.Take(entryName);
      obj.Value = currentValue;
      obj.Color = Gui.Options.NormalColor;
    }

    private void Mark(string entryName, float currentValue, float nextValue, string formatKey = null)
    {
      string format = formatKey != null ? this.GetLocalizedFormat(formatKey) : "{0}";
      ShipStats.Item obj = this.Take(entryName);
      float num = nextValue - currentValue;
      if ((double) num > 0.0)
      {
        obj.Value = string.Format("{0}(+{1})", (object) this.FormatFloat(nextValue), (object) this.FormatFloat(num));
        obj.Value = string.Format(format, (object) obj.Value);
        obj.MarkDifferenceColor(num);
      }
      else if ((double) num < 0.0)
      {
        obj.Value = string.Format("{0}(-{1})", (object) this.FormatFloat(nextValue), (object) this.FormatFloat(Mathf.Abs(num)));
        obj.Value = string.Format(format, (object) obj.Value);
        obj.MarkDifferenceColor(num);
      }
      else
      {
        obj.Value = string.Format(format, (object) this.FormatFloat(currentValue));
        obj.Color = Gui.Options.NormalColor;
      }
    }

    private string FormatFloat(float f)
    {
      string str = f.ToString("F2");
      while (str.EndsWith("0") && str.Contains(".") || str.EndsWith("."))
        str = str.Substring(0, str.Length - 1);
      return str;
    }

    private class Item : GuiPanel
    {
      private readonly GuiLabel name = new GuiLabel(Gui.Options.FontBGM_BT, 12);
      private readonly GuiLabel value = new GuiLabel(Gui.Options.FontBGM_BT, 12);
      private readonly bool positiveInverted;

      public string Value
      {
        get
        {
          return this.value.Text;
        }
        set
        {
          this.value.Text = value;
        }
      }

      public Color Color
      {
        set
        {
          this.value.AllColor = value;
        }
      }

      public Item(string name)
        : this(name, name)
      {
      }

      private Item(string name, string tooltip)
        : this(name, tooltip, 0.0f)
      {
      }

      public Item(string name, string tooltip, float width)
      {
        this.Name = name;
        this.name.Text = name;
        Size size = TextUtility.CalcTextSize(name, Gui.Options.FontBGM_BT, 12);
        if ((double) width > 0.0)
        {
          GuiImage guiImage = new GuiImage((Texture2D) ResourceLoader.Load("GUI/BuyShip/bg_white"));
          guiImage.SizeY = (float) (size.height * 2);
          guiImage.SizeX = width - 25f;
          this.AddChild((GuiElementBase) guiImage);
        }
        this.AddChild((GuiElementBase) this.name);
        this.AddChild((GuiElementBase) this.value, Align.UpRight, new Vector2(-10f, (float) size.height));
        this.SizeY = this.value.SizeY + 5f;
        if (string.IsNullOrEmpty(tooltip))
          return;
        this.SetTooltip(tooltip);
      }

      public Item(string name, string tooltip, bool positiveInverted, float width)
        : this(name, tooltip, width)
      {
        this.positiveInverted = positiveInverted;
      }

      public void MarkDifferenceColor(float difference)
      {
        this.Color = ((double) difference <= 0.0 || this.positiveInverted) && ((double) difference >= 0.0 || !this.positiveInverted) ? Gui.Options.NegativeColor : Gui.Options.PositiveColor;
      }
    }
  }
}
