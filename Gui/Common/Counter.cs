﻿// Decompiled with JetBrains decompiler
// Type: Gui.Common.Counter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui.Common
{
  public class Counter : QueueHorizontal
  {
    private GuiButton veryLeft = new GuiButton(string.Empty, "GUI/EquipBuyPanel/doublearrow_left_normal", "GUI/EquipBuyPanel/doublearrow_left_over", "GUI/EquipBuyPanel/doublearrow_left_pressed");
    private GuiButton left = new GuiButton(string.Empty, "GUI/EquipBuyPanel/arrow_left_normal", "GUI/EquipBuyPanel/arrow_left_over", "GUI/EquipBuyPanel/arrow_left_pressed");
    private GuiButton right = new GuiButton(string.Empty, "GUI/EquipBuyPanel/arrow_right_normal", "GUI/EquipBuyPanel/arrow_right_over", "GUI/EquipBuyPanel/arrow_right_pressed");
    private GuiButton veryRight = new GuiButton(string.Empty, "GUI/EquipBuyPanel/doublearrow_right_normal", "GUI/EquipBuyPanel/doublearrow_right_over", "GUI/EquipBuyPanel/doublearrow_right_pressed");
    private GuiTextBox textBox = new GuiTextBox(60f);
    private int count = 1;
    private int minValue = 1;
    private int maxValue = 1000000;
    public int Step = 1;
    public Action<int> OnValueChanged;

    [Gui2Editor]
    public float TextboxWidth
    {
      get
      {
        return this.textBox.SizeX;
      }
      set
      {
        this.textBox.SizeX = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public float SpaceCounter
    {
      get
      {
        return this.ChildrenOfType<GuiSpaceX>()[0].SizeX;
      }
      set
      {
        using (List<GuiSpaceX>.Enumerator enumerator = this.ChildrenOfType<GuiSpaceX>().GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.SizeX = value;
        }
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public int Value
    {
      get
      {
        return this.count;
      }
      set
      {
        this.count = Mathf.Clamp(value, this.minValue, this.maxValue);
        this.textBox.Text = this.count.ToString();
        if (this.OnValueChanged == null)
          return;
        this.OnValueChanged(this.count);
      }
    }

    [Gui2Editor]
    public int MinValue
    {
      get
      {
        return this.minValue;
      }
      set
      {
        this.minValue = value;
        if (this.Value >= this.minValue)
          return;
        this.Value = this.minValue;
      }
    }

    [Gui2Editor]
    public int MaxValue
    {
      get
      {
        return this.maxValue;
      }
      set
      {
        this.maxValue = value;
        if (this.Value <= this.maxValue)
          return;
        this.Value = this.maxValue;
      }
    }

    public Counter()
    {
      this.ContainerAutoSize = true;
      this.AddChild((GuiElementBase) this.veryLeft);
      this.AddChild((GuiElementBase) new GuiSpaceX(5f));
      this.AddChild((GuiElementBase) this.left);
      this.AddChild((GuiElementBase) new GuiSpaceX(5f));
      this.AddChild((GuiElementBase) this.textBox);
      this.AddChild((GuiElementBase) new GuiSpaceX(5f));
      this.AddChild((GuiElementBase) this.right);
      this.AddChild((GuiElementBase) new GuiSpaceX(5f));
      this.AddChild((GuiElementBase) this.veryRight);
      this.textBox.Alignment = TextAnchor.MiddleCenter;
      this.textBox.OnTextChanged = (Action<string>) (text =>
      {
        int result = 0;
        if (!int.TryParse(text, out result))
          return;
        this.Value = result;
      });
      this.veryLeft.OnClick = new AnonymousDelegate(this.VeryDown);
      this.left.OnClick = new AnonymousDelegate(this.Down);
      this.right.OnClick = new AnonymousDelegate(this.Up);
      this.veryRight.OnClick = new AnonymousDelegate(this.VeryUp);
      this.Value = 0;
    }

    [Gui2Editor]
    public void Up()
    {
      this.Value += this.Step;
    }

    [Gui2Editor]
    public void VeryUp()
    {
      this.Value += 10 * this.Step;
    }

    [Gui2Editor]
    public void Down()
    {
      this.Value -= this.Step;
    }

    [Gui2Editor]
    public void VeryDown()
    {
      this.Value -= 10 * this.Step;
    }
  }
}
