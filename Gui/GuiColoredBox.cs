﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiColoredBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("")]
  public class GuiColoredBox : GuiElementBase
  {
    private Color color;
    private Texture2D texture;

    [Gui2Editor]
    public Color Color
    {
      get
      {
        return this.color;
      }
      set
      {
        this.color = value;
        this.texture = TextureCache.Get(this.color);
      }
    }

    public GuiColoredBox()
      : this(Game.Me == null ? Color.white : Options.BackgroundColor)
    {
    }

    public GuiColoredBox(Color color)
      : this(color, new Vector2(300f, 1f))
    {
    }

    public GuiColoredBox(Color color, Vector2 size)
      : this(color, Vector2.zero, size)
    {
    }

    public GuiColoredBox(Color color, Vector2 position, Vector2 size)
      : base(position, size)
    {
      this.Color = color;
    }

    public GuiColoredBox(GuiColoredBox copy)
      : base((GuiElementBase) copy)
    {
      this.color = copy.color;
    }

    public GuiColoredBox(JElementBase json)
      : base(json)
    {
      this.color = (json as JColoredBox).m_boxColor;
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiColoredBox(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JColoredBox((GuiElementBase) this);
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
      if ((Object) this.texture == (Object) null)
        this.texture = TextureCache.Get(this.color);
      Graphics.DrawTexture(this.Rect, (Texture) this.texture);
    }

    public override void Draw()
    {
      base.Draw();
      if (Event.current.type != UnityEngine.EventType.Repaint)
        return;
      if ((Object) this.texture == (Object) null)
        this.texture = TextureCache.Get(this.color);
      Graphics.DrawTexture(this.Rect, (Texture) this.texture);
    }
  }
}
