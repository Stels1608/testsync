﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiWingsCreationPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/_NewGui/layout/wings/wingsCreationPanel")]
  public class GuiWingsCreationPanel : GuiPanel
  {
    private int m_minWingNameLength = 3;
    private const string mc_newWingPanelName = "newWingPanel";
    private const string mc_nameTakenLabelName = "nameTakenLabel";
    [HideInInspector]
    private GuiPanel m_newWingPanel;
    [HideInInspector]
    private GuiInputTextBox m_newWingName;
    [HideInInspector]
    private GuiButton m_createWingButton;
    [HideInInspector]
    private GuiLabel m_nameTakenLabel;

    public int MinWingNameLength
    {
      get
      {
        return this.m_minWingNameLength;
      }
    }

    public GuiWingsCreationPanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiWingsCreationPanel>())
    {
      this.Initialize();
    }

    public GuiWingsCreationPanel(GuiWingsCreationPanel copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiWingsCreationPanel(JWingsCreationPanel json)
      : base((JPanel) json)
    {
      this.m_minWingNameLength = json.m_minWingNameLength;
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiWingsCreationPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JWingsCreationPanel((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      this.m_nameTakenLabel.IsRendered = true;
    }

    private void Initialize()
    {
      this.m_newWingPanel = this.Find<GuiPanel>("newWingPanel");
      this.m_newWingName = this.m_newWingPanel.Find<GuiInputTextBox>();
      this.m_newWingName.OnHitEnter = new System.Action(this.TryToCreateNewWing);
      this.m_nameTakenLabel = this.Find<GuiLabel>("nameTakenLabel");
      this.m_nameTakenLabel.IsRendered = false;
      this.m_createWingButton = this.m_newWingPanel.Find<GuiButton>();
      this.m_createWingButton.Pressed = new AnonymousDelegate(this.TryToCreateNewWing);
    }

    private void TryToCreateNewWing()
    {
      if (this.m_newWingName.Text != null && this.m_newWingName.Text.Length >= this.m_minWingNameLength)
      {
        Game.Guilds.StartGuild(this.m_newWingName.Text);
      }
      else
      {
        this.m_nameTakenLabel.IsRendered = true;
        this.m_nameTakenLabel.Text = BsgoLocalization.Get("%$bgo.Wings.create_wing.invalid_name_length%", (object) this.m_minWingNameLength);
        this.m_nameTakenLabel.PositionX = (float) (((double) this.m_nameTakenLabel.Parent.SizeX - (double) this.m_nameTakenLabel.SizeX) * 0.5);
      }
    }

    public override void OnShow()
    {
      base.OnShow();
      this.m_newWingName.Text = string.Empty;
      this.m_nameTakenLabel.IsRendered = false;
      Game.InputDispatcher.Focused = (InputListener) this;
    }

    public override void OnHide()
    {
      base.OnHide();
      Game.InputDispatcher.Focused = (InputListener) null;
    }

    public void NameIsAlreadyTaken(string nameTried)
    {
      this.m_nameTakenLabel.Text = BsgoLocalization.Get("%$bgo.guild.wing_exists%", (object) nameTried);
      this.m_nameTakenLabel.IsRendered = true;
    }

    public bool HasFocus()
    {
      return this.m_newWingName.IsFocused;
    }
  }
}
