﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiTextBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiTextBox : GuiElementBase
  {
    [Gui2Editor]
    public int MaxSize = 128;
    private string text = string.Empty;
    private GUIStyle style = new GUIStyle();
    public System.Action<string> OnTextChanged;

    [Gui2Editor]
    public string Text
    {
      get
      {
        return this.text;
      }
      set
      {
        this.text = value;
      }
    }

    [Gui2Editor]
    public Font Font
    {
      get
      {
        return this.style.font;
      }
      set
      {
        this.style.font = value;
      }
    }

    [Gui2Editor]
    public TextAnchor Alignment
    {
      get
      {
        return this.style.alignment;
      }
      set
      {
        this.style.alignment = value;
      }
    }

    public GuiTextBox()
      : this(Options.FontBGM_BT)
    {
    }

    public GuiTextBox(float size)
      : this(Options.FontBGM_BT, size)
    {
    }

    public GuiTextBox(Font font)
      : this(font, 100f)
    {
    }

    public GuiTextBox(Font font, float size)
      : this(font, new Vector2(size, 15f))
    {
    }

    public GuiTextBox(Font font, Vector2 size)
    {
      this.style.font = font;
      this.style.fontSize = 11;
      this.style.normal.textColor = Options.NormalColor;
      this.style.wordWrap = false;
      this.Size = size;
    }

    public override void Draw()
    {
      base.Draw();
      string str = GUI.TextArea(this.Rect, this.text, this.MaxSize, this.style);
      bool flag = str != this.text;
      this.text = str;
      if (!flag || this.OnTextChanged == null)
        return;
      this.OnTextChanged(this.text);
    }

    public override bool KeyUp(KeyCode key, Action action)
    {
      base.KeyUp(key, action);
      return true;
    }

    public override bool KeyDown(KeyCode key, Action action)
    {
      base.KeyDown(key, action);
      return true;
    }
  }
}
