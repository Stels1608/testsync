﻿// Decompiled with JetBrains decompiler
// Type: Gui.JumpQueueWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class JumpQueueWindow : GuiPanel
  {
    public bool Queued { get; set; }

    public override bool IsRendered
    {
      get
      {
        if (base.IsRendered)
          return this.Queued;
        return false;
      }
    }

    public JumpQueueWindow()
    {
      GuiImage guiImage = new GuiImage("GUI/JumpCountdown/ftlbox");
      this.Size = guiImage.Size;
      GuiLabel guiLabel = new GuiLabel("%$bgo.jump_countdown.queued%", Options.FontBGM_BT, 12);
      guiLabel.Alignment = TextAnchor.MiddleCenter;
      this.AddChild((GuiElementBase) guiImage);
      this.AddChild((GuiElementBase) guiLabel, Align.UpCenter, new Vector2(0.0f, 11f));
      this.Align = Align.DownCenter;
      this.PositionY = -200f;
      this.IsRendered = true;
    }
  }
}
