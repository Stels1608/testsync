﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiHealthbar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiHealthbar : GuiElementBase
  {
    private float borderWidth = 1f;
    private float freeSpaceWidth = 1f;
    private GuiColoredBox healthbarBorder;
    private GuiColoredBox healthbarBorder2;
    private GuiColoredBox healthbar;
    private float progress;

    [Gui2Editor]
    public float Progress
    {
      get
      {
        return this.progress;
      }
      set
      {
        this.progress = value;
        this.progress = Mathf.Clamp(this.progress, 0.0f, 1f);
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public Color Color
    {
      get
      {
        return this.healthbar.Color;
      }
      set
      {
        this.healthbar.Color = value;
      }
    }

    [Gui2Editor]
    public Color ColorBorder
    {
      get
      {
        return this.healthbarBorder.Color;
      }
      set
      {
        this.healthbarBorder.Color = value;
      }
    }

    [Gui2Editor]
    public Color ColorInner
    {
      get
      {
        return this.healthbarBorder2.Color;
      }
      set
      {
        this.healthbarBorder2.Color = value;
      }
    }

    [Gui2Editor]
    public float BorderWidth
    {
      get
      {
        return this.borderWidth;
      }
      set
      {
        this.borderWidth = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public float FreeSpaceWidth
    {
      get
      {
        return this.freeSpaceWidth;
      }
      set
      {
        this.freeSpaceWidth = value;
        this.ForceReposition();
      }
    }

    public GuiHealthbar()
      : base(Vector2.zero, new Vector2(30f, 7f))
    {
      this.healthbarBorder = new GuiColoredBox();
      this.healthbarBorder.Parent = (SRect) this;
      this.healthbarBorder2 = new GuiColoredBox(Color.black);
      this.healthbarBorder2.Parent = (SRect) this;
      this.healthbar = new GuiColoredBox(Options.ProgressBarColor);
      this.healthbar.Parent = (SRect) this;
    }

    public override void Draw()
    {
      base.Draw();
      this.healthbarBorder.Draw();
      this.healthbarBorder2.Draw();
      this.healthbar.Draw();
    }

    public override void ForceReposition()
    {
      base.ForceReposition();
      this.healthbarBorder.ForceReposition();
      this.healthbarBorder2.ForceReposition();
      this.healthbar.ForceReposition();
    }

    public override void Reposition()
    {
      base.Reposition();
      this.healthbarBorder.Size = this.Size;
      float num1 = this.Size.x;
      float num2 = this.Size.y;
      this.healthbarBorder2.Size = new Vector2(num1 - this.borderWidth * 2f, num2 - this.borderWidth * 2f);
      this.healthbarBorder2.Position = new Vector2(this.borderWidth, this.borderWidth);
      this.healthbar.Size = new Vector2(this.progress * (float) ((double) num1 - (double) this.borderWidth * 2.0 - (double) this.freeSpaceWidth * 2.0), (float) ((double) num2 - (double) this.borderWidth * 2.0 - (double) this.freeSpaceWidth * 2.0));
      this.healthbar.Position = new Vector2(this.borderWidth + this.freeSpaceWidth, this.borderWidth + this.freeSpaceWidth);
    }
  }
}
