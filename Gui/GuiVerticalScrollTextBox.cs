﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiVerticalScrollTextBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiVerticalScrollTextBox : GuiPanel
  {
    private GuiButton m_upButton = new GuiButton(string.Empty, "GUI/Common/up_normal", "GUI/Common/up_over", "GUI/Common/up_pressed");
    private GuiButton m_downButton = new GuiButton(string.Empty, "GUI/Common/down_normal", "GUI/Common/down_over", "GUI/Common/down_pressed");
    private GuiImage m_slider = new GuiImage("GUI/Common/scroller");
    private GuiColoredBox m_leftLine = new GuiColoredBox();
    private GuiColoredBox m_rightLine = new GuiColoredBox();
    private float m_buttonScrollSpeed = 1f;
    private float m_mouseWheelScrollSpeed = 1f;
    private float2 m_lastMousePosition = new float2();
    private GUIStyle m_defaultThumbStyle = new GUIStyle();
    private GUIStyle m_defaultBarStyle = new GUIStyle();
    private GuiLabel m_textBox = new GuiLabel();
    private Vector2 m_scrollOffset = new Vector2(0.0f, 0.0f);
    private const float SPACE = 2f;
    private const float LINE_WIDTH = 1f;
    private float m_scrollableHeight;
    private float m_viewHeight;
    private float m_sliderMinPosition;
    private float m_sliderMaxPosition;
    private bool m_sliderActive;

    private GuiLabel TextBox
    {
      get
      {
        return this.m_textBox;
      }
      set
      {
        this.m_textBox = value;
      }
    }

    public float ScrollOffset
    {
      get
      {
        return this.m_scrollOffset.y;
      }
      set
      {
        this.m_scrollOffset.y = value;
        if ((double) this.m_scrollOffset.y < 0.0)
        {
          this.m_scrollOffset.y = 0.0f;
        }
        else
        {
          if ((double) this.m_scrollOffset.y <= (double) this.m_scrollableHeight - (double) this.m_viewHeight)
            return;
          this.m_scrollOffset.y = this.m_scrollableHeight - this.m_viewHeight;
        }
      }
    }

    public bool IsScrollingActive
    {
      get
      {
        return (double) this.SizeY < (double) this.TextBox.SizeY;
      }
    }

    public GuiVerticalScrollTextBox(GuiLabel label, float buttonSpeed = 30f, float wheelSpeed = 30f, bool wrap = true)
    {
      this.ResetTextBox(label, buttonSpeed, wheelSpeed, true);
    }

    public void ResetTextBox(GuiLabel newLabel, float buttonSpeed, float wheelSpeed, bool wrap = true)
    {
      this.m_buttonScrollSpeed = buttonSpeed;
      this.m_mouseWheelScrollSpeed = wheelSpeed;
      this.TextBox.WordWrap = wrap;
      if (this.TextBox == newLabel)
        return;
      this.TextBox = newLabel;
      this.SetUpTextBoxAndViewRect();
      this.SetupTheScrollBar();
    }

    private void SetupTheScrollBar()
    {
      if (!this.IsScrollingActive)
        return;
      this.m_scrollableHeight = this.TextBox.SizeY;
      this.m_viewHeight = this.SizeY;
      this.m_upButton.Position = new Vector2(0.0f, 0.0f);
      float y1 = this.m_viewHeight - this.m_upButton.Image.SizeY - this.m_downButton.Image.SizeY;
      float y2 = this.m_viewHeight / this.m_scrollableHeight * y1;
      this.m_slider.Size = new Vector2((float) ((double) this.m_upButton.Image.SizeX - 4.0 - 2.0), y2);
      this.m_sliderMinPosition = this.m_upButton.Image.SizeY;
      this.m_sliderMaxPosition = y1 - y2 + this.m_sliderMinPosition;
      this.m_slider.Position = new Vector2(-3f, this.m_sliderMinPosition);
      this.m_downButton.Position = new Vector2(0.0f, this.m_viewHeight - this.m_downButton.Image.SizeY);
      this.m_leftLine.Position = new Vector2(0.0f, this.m_upButton.Image.SizeY);
      this.m_leftLine.Size = new Vector2(1f, y1);
      this.m_rightLine.Position = new Vector2((float) (-(double) this.m_upButton.Image.SizeX + 1.0), this.m_upButton.Image.SizeY);
      this.m_rightLine.Size = this.m_leftLine.Size;
      this.m_upButton.Pressed = new AnonymousDelegate(this.ButtonScrollUp);
      this.m_downButton.Pressed = new AnonymousDelegate(this.ButtonScrollDown);
      this.AddChild((GuiElementBase) this.m_upButton, Align.UpRight);
      this.AddChild((GuiElementBase) this.m_downButton, Align.UpRight);
      this.AddChild((GuiElementBase) this.m_slider, Align.UpRight);
      this.AddChild((GuiElementBase) this.m_leftLine, Align.UpRight);
      this.AddChild((GuiElementBase) this.m_rightLine, Align.UpRight);
    }

    private void SetUpTextBoxAndViewRect()
    {
      this.Position = this.TextBox.Position;
      this.Size = new Vector2(this.TextBox.SizeX, this.TextBox.SizeY);
      this.TextBox.Position = Vector2.zero;
      Size size;
      if (this.TextBox.WordWrap)
      {
        size = TextUtility.CalcTextSize(this.TextBox.TextParsed, this.TextBox.Style.font, this.TextBox.SizeX - this.m_upButton.SizeX);
      }
      else
      {
        size = TextUtility.CalcTextSize(this.TextBox.TextParsed, this.TextBox.Style.font, this.TextBox.FontSize);
        this.SizeX = (float) size.width - this.m_upButton.SizeX;
      }
      this.TextBox.Size = new Vector2((float) size.width, (float) size.height);
      if (this.Children.Contains((GuiElementBase) this.TextBox))
      {
        if (!this.IsScrollingActive)
          return;
        this.RemoveChild((GuiElementBase) this.TextBox);
      }
      else
      {
        if (this.IsScrollingActive)
          return;
        this.AddChild((GuiElementBase) this.TextBox);
      }
    }

    private void MoveSlider(float amount)
    {
      Vector2 position = this.m_slider.Position;
      position.y += amount;
      if ((double) position.y < (double) this.m_sliderMinPosition)
        position.y = this.m_sliderMinPosition;
      else if ((double) position.y > (double) this.m_sliderMaxPosition)
        position.y = this.m_sliderMaxPosition;
      this.m_slider.Position = position;
      this.ScrollOffset = (float) (((double) position.y - (double) this.m_sliderMinPosition) / ((double) this.m_sliderMaxPosition - (double) this.m_sliderMinPosition)) * (this.m_scrollableHeight - this.m_viewHeight);
    }

    private void PageUp()
    {
      this.MoveSlider(-(float) ((double) (this.m_viewHeight - this.m_textBox.Style.lineHeight) / (double) this.m_scrollableHeight * ((double) this.m_sliderMaxPosition - (double) this.m_sliderMinPosition)));
    }

    private void PageDown()
    {
      this.MoveSlider((float) ((double) (this.m_viewHeight - this.m_textBox.Style.lineHeight) / (double) this.m_scrollableHeight * ((double) this.m_sliderMaxPosition - (double) this.m_sliderMinPosition)));
    }

    private void ScrollToStart()
    {
      this.m_slider.Position = new Vector2(this.m_slider.Position.x, this.m_sliderMinPosition);
      this.ScrollOffset = 0.0f;
    }

    private void ScrollToEnd()
    {
      this.m_slider.Position = new Vector2(this.m_slider.Position.x, this.m_sliderMaxPosition);
      this.ScrollOffset = this.m_scrollableHeight - this.m_viewHeight;
    }

    public void ButtonScrollUp()
    {
      this.MoveSlider(-this.m_buttonScrollSpeed);
    }

    public void ButtonScrollDown()
    {
      this.MoveSlider(this.m_buttonScrollSpeed);
    }

    public override bool ScrollUp(float2 position)
    {
      if (!this.IsScrollingActive)
        return false;
      this.MoveSlider(-this.m_mouseWheelScrollSpeed);
      return true;
    }

    public override bool ScrollDown(float2 position)
    {
      if (!this.IsScrollingActive)
        return false;
      this.MoveSlider(this.m_mouseWheelScrollSpeed);
      return true;
    }

    public override bool MouseDown(float2 mousePosition, KeyCode mouseKey)
    {
      if (!this.IsScrollingActive)
        return false;
      if (this.m_upButton.Contains(mousePosition))
      {
        this.m_upButton.Pressed();
        return true;
      }
      if (this.m_downButton.Contains(mousePosition))
      {
        this.m_downButton.Pressed();
        return true;
      }
      this.m_lastMousePosition = mousePosition;
      if (!this.m_slider.Contains(mousePosition))
        return base.MouseDown(mousePosition, mouseKey);
      this.m_sliderActive = true;
      return true;
    }

    public override bool MouseUp(float2 mousePosition, KeyCode mouseKey)
    {
      if (!this.IsScrollingActive)
        return false;
      if (this.m_sliderActive)
        this.m_sliderActive = false;
      return base.MouseUp(mousePosition, mouseKey);
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      if (!this.IsScrollingActive)
        return false;
      if (this.m_sliderActive)
      {
        this.MoveSlider(position.y - this.m_lastMousePosition.y);
        this.m_lastMousePosition = position;
      }
      if (!base.MouseMove(position, previousPosition))
        return this.m_sliderActive;
      return true;
    }

    public override bool KeyDown(KeyCode keyboardKey, Action action)
    {
      if (!this.IsScrollingActive)
        return false;
      bool flag = false;
      if (keyboardKey == KeyCode.DownArrow)
      {
        this.MoveSlider(this.m_buttonScrollSpeed);
        flag = true;
        this.m_sliderActive = false;
      }
      if (keyboardKey == KeyCode.UpArrow)
      {
        this.MoveSlider(-this.m_buttonScrollSpeed);
        flag = true;
        this.m_sliderActive = false;
      }
      if (keyboardKey == KeyCode.End)
      {
        this.ScrollToEnd();
        flag = true;
        this.m_sliderActive = false;
      }
      if (keyboardKey == KeyCode.Home)
      {
        this.ScrollToStart();
        flag = true;
        this.m_sliderActive = false;
      }
      if (keyboardKey == KeyCode.PageDown)
      {
        this.PageDown();
        flag = true;
        this.m_sliderActive = false;
      }
      if (keyboardKey == KeyCode.PageUp)
      {
        this.PageUp();
        flag = true;
        this.m_sliderActive = false;
      }
      if (!base.KeyDown(keyboardKey, action))
        return flag;
      return true;
    }

    public override bool KeyUp(KeyCode keyboardKey, Action action)
    {
      if (!this.IsScrollingActive)
        return false;
      return base.KeyUp(keyboardKey, action);
    }

    public override void Draw()
    {
      if (!this.IsScrollingActive)
      {
        this.TextBox.Draw();
      }
      else
      {
        GUIStyle verticalScrollbarThumb = GUI.skin.verticalScrollbarThumb;
        GUIStyle verticalScrollbar = GUI.skin.verticalScrollbar;
        GUI.skin.verticalScrollbarThumb = this.m_defaultThumbStyle;
        GUI.skin.verticalScrollbar = this.m_defaultBarStyle;
        GUI.BeginScrollView(this.Rect, this.m_scrollOffset, this.TextBox.Rect);
        this.TextBox.Draw();
        GUI.EndScrollView();
        GUI.skin.verticalScrollbarThumb = verticalScrollbarThumb;
        GUI.skin.verticalScrollbar = verticalScrollbar;
        base.Draw();
      }
    }
  }
}
