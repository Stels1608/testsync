﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiDropdownMenu
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/_NewGui/layout/dropDownMenu")]
  public class GuiDropdownMenu : GuiPanel
  {
    private const string mc_headerButtonName = "headerButton";
    private const string mc_dropDownPanelName = "dropPanel";
    private const string mc_displayPanelName = "displayPanel";
    [HideInInspector]
    private GuiButton m_headerButton;
    [HideInInspector]
    private GuiPanel m_dropDownPanel;
    [HideInInspector]
    private GuiPanel m_displayPanel;
    public AnonymousDelegate ToggleDropDownDisplay;

    public GuiPanel DropDownPanel
    {
      get
      {
        return this.m_displayPanel;
      }
    }

    public GuiDropdownMenu()
      : base(UseInGuiEditor.GetDefaultLayout<GuiDropdownMenu>())
    {
      this.Initialize();
    }

    public GuiDropdownMenu(GuiDropdownMenu copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiDropdownMenu(JDropDownMenu json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiDropdownMenu(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JDropDownMenu((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      if (this.m_headerButton.Position != this.m_headerButton.ElementPadding.TopLeft)
      {
        GuiDropdownMenu guiDropdownMenu = this;
        Vector2 vector2 = guiDropdownMenu.Position + this.m_headerButton.Position - this.m_headerButton.ElementPadding.TopLeft;
        guiDropdownMenu.Position = vector2;
      }
      this.CheckChildPositioning();
    }

    public override void EditorDraw()
    {
      GuiElementBase.DrawElement.BeginStencil(this.Rect, false, false);
      base.EditorDraw();
      GuiElementBase.DrawElement.EndStencil();
    }

    private void Initialize()
    {
      this.m_headerButton = this.Find<GuiButton>("headerButton");
      this.m_dropDownPanel = this.Find<GuiPanel>("dropPanel");
      this.m_displayPanel = this.m_dropDownPanel.Find<GuiPanel>("displayPanel");
      this.m_dropDownPanel.IsRendered = false;
      this.ToggleDropDownDisplay = (AnonymousDelegate) (() =>
      {
        this.m_dropDownPanel.IsRendered = !this.m_dropDownPanel.IsRendered;
        this.CheckChildPositioning();
      });
      this.m_headerButton.Pressed = this.ToggleDropDownDisplay;
    }

    public void ShowDropDown()
    {
      Game.InputDispatcher.Focused = (InputListener) this;
      this.m_dropDownPanel.IsRendered = true;
      this.CheckChildPositioning();
    }

    public void HideDropDown()
    {
      if (Game.InputDispatcher.Focused == this)
        Game.InputDispatcher.Focused = (InputListener) null;
      this.m_dropDownPanel.IsRendered = false;
    }

    private void CheckChildPositioning()
    {
      this.m_headerButton.Position = this.m_headerButton.ElementPadding.TopLeft;
      if (this.m_dropDownPanel.IsRendered)
      {
        this.m_dropDownPanel.PositionX = this.m_dropDownPanel.ElementPadding.Left;
        this.m_dropDownPanel.PositionY = this.m_headerButton.BottomRightCornerWithPadding.y + this.m_dropDownPanel.ElementPadding.Top;
        this.m_displayPanel.Position = this.m_displayPanel.ElementPadding.TopLeft;
        this.m_displayPanel.ResizePanelToFitChildren();
        Vector2 vector2 = this.m_headerButton.BottomRightCornerWithPadding * 0.5f;
        this.m_displayPanel.SizeX = Mathf.Max(this.m_displayPanel.SizeX, vector2.x);
        this.m_displayPanel.SizeY = Mathf.Max(this.m_displayPanel.SizeY, vector2.y);
        this.m_dropDownPanel.Size = this.m_displayPanel.BottomRightCornerWithPadding;
        this.Size = this.m_dropDownPanel.BottomRightCornerWithPadding;
      }
      else
        this.Size = this.m_headerButton.BottomRightCornerWithPadding;
    }

    public void SetHeaderButtonText(string text)
    {
      this.m_headerButton.Text = text;
    }

    public void AddDropDownElements(GuiElementBase element)
    {
      this.m_displayPanel.AddChild(element);
    }

    public override void Update()
    {
      base.Update();
      if (!(this.Parent is GuiElementBase) || (this.Parent as GuiElementBase).IsRendered)
        return;
      this.IsRendered = false;
    }

    public override void Draw()
    {
      GuiElementBase.DrawElement.BeginStencil(this.Rect, false, false);
      base.Draw();
      GuiElementBase.DrawElement.EndStencil();
    }

    public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
    {
      if (this.Contains(mousePosition))
        return base.OnMouseDown(mousePosition, mouseKey);
      this.IsRendered = false;
      return false;
    }

    public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
    {
      if (this.Contains(mousePosition))
        return base.OnMouseUp(mousePosition, mouseKey);
      this.IsRendered = false;
      return false;
    }
  }
}
