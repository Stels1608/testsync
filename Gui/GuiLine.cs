﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiLine
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiLine : GuiElementBase
  {
    private float thickness = 1f;
    private Rect textureRext = new Rect();
    private Texture2D texture;

    [Gui2Editor]
    public Color Color
    {
      get
      {
        return this.texture.GetPixel(0, 0);
      }
      set
      {
        this.texture = TextureCache.Get(value);
      }
    }

    [Gui2Editor]
    public float Thickness
    {
      get
      {
        return this.thickness;
      }
      set
      {
        this.thickness = value;
        this.ForceReposition();
      }
    }

    public GuiLine()
      : this(Options.BackgroundColor)
    {
    }

    public GuiLine(Color color)
    {
      this.Color = color;
    }

    public override void Draw()
    {
      base.Draw();
      Matrix4x4 matrix = GUI.matrix;
      GUIUtility.RotateAroundPivot(this.CalcAngle0to360CW(), new Vector2(this.Rect.x, this.Rect.y));
      GUI.DrawTexture(this.textureRext, (Texture) this.texture);
      GUI.matrix = matrix;
    }

    public override void DoCaching()
    {
      base.DoCaching();
      this.textureRext = new Rect(this.Rect.x, this.Rect.y, this.Size.magnitude, this.thickness);
    }

    private float CalcAngle0to360CW()
    {
      float sizeX = this.SizeX;
      float sizeY = this.SizeY;
      if ((double) sizeX == 0.0)
        return 0.0f;
      float num = Mathf.Atan(Mathf.Abs(sizeY) / Mathf.Abs(sizeX)) * 57.29578f;
      if ((double) sizeX >= 0.0)
      {
        if ((double) sizeY >= 0.0)
          return num;
        return 360f - num;
      }
      if ((double) sizeY >= 0.0)
        return 180f - num;
      return 180f + num;
    }
  }
}
