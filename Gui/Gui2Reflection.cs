﻿// Decompiled with JetBrains decompiler
// Type: Gui.Gui2Reflection
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Gui
{
  public class Gui2Reflection
  {
    public static Dictionary<MemberInfo, T> GetMembers<T>(Type type, bool inherit, bool staticOnly) where T : Attribute
    {
      List<MemberInfo> memberInfoList = new List<MemberInfo>();
      MemberTypes memberType = MemberTypes.Field | MemberTypes.Method | MemberTypes.Property;
      BindingFlags bindingAttr1 = BindingFlags.Static | BindingFlags.Public;
      if (!inherit)
        bindingAttr1 |= BindingFlags.DeclaredOnly;
      memberInfoList.AddRange((IEnumerable<MemberInfo>) type.FindMembers(memberType, bindingAttr1, (MemberFilter) null, (object) null));
      BindingFlags bindingAttr2 = BindingFlags.Static | BindingFlags.NonPublic;
      if (!inherit)
        bindingAttr2 |= BindingFlags.DeclaredOnly;
      memberInfoList.AddRange((IEnumerable<MemberInfo>) type.FindMembers(memberType, bindingAttr2, (MemberFilter) null, (object) null));
      if (!staticOnly)
      {
        BindingFlags bindingAttr3 = BindingFlags.Instance | BindingFlags.Public;
        if (!inherit)
          bindingAttr3 |= BindingFlags.DeclaredOnly;
        memberInfoList.AddRange((IEnumerable<MemberInfo>) type.FindMembers(memberType, bindingAttr3, (MemberFilter) null, (object) null));
        BindingFlags bindingAttr4 = BindingFlags.Instance | BindingFlags.NonPublic;
        if (!inherit)
          bindingAttr4 |= BindingFlags.DeclaredOnly;
        memberInfoList.AddRange((IEnumerable<MemberInfo>) type.FindMembers(memberType, bindingAttr4, (MemberFilter) null, (object) null));
      }
      Dictionary<MemberInfo, T> dictionary = new Dictionary<MemberInfo, T>();
      using (List<MemberInfo>.Enumerator enumerator = memberInfoList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          MemberInfo current = enumerator.Current;
          object[] customAttributes = current.GetCustomAttributes(typeof (T), false);
          if (customAttributes.Length != 0)
          {
            T obj = customAttributes[0] as T;
            dictionary.Add(current, obj);
          }
        }
      }
      return dictionary;
    }

    public static Type MGetType(MemberInfo info)
    {
      if (info is FieldInfo)
        return (info as FieldInfo).FieldType;
      if (info is PropertyInfo)
        return (info as PropertyInfo).PropertyType;
      throw new Exception("MGetType: wrong MemberInfo: " + info.Name);
    }

    public static object MGetValue(object obj, MemberInfo info)
    {
      if (info is FieldInfo)
        return (info as FieldInfo).GetValue(obj);
      if (info is PropertyInfo)
        return (info as PropertyInfo).GetValue(obj, (object[]) null);
      throw new Exception("MGetValue: wrong MemberInfo: " + info.Name);
    }

    public static void MSetValue(object obj, MemberInfo info, object value)
    {
      if (info is FieldInfo)
      {
        (info as FieldInfo).SetValue(obj, value);
      }
      else
      {
        if (!(info is PropertyInfo))
          throw new Exception("MSetValue: wrong MemberInfo: " + info.Name + " " + info.GetType().ToString());
        (info as PropertyInfo).SetValue(obj, value, (object[]) null);
      }
    }

    public static string ToStringAdvanced(object obj)
    {
      return Gui2Reflection.ToStringAdvanced(obj, string.Empty);
    }

    private static string ToStringAdvanced(object obj, string prefix)
    {
      if (obj == null)
        return "Null";
      Type type = obj.GetType();
      if (obj is string || obj is int || (obj is uint || obj is float) || (obj is double || obj is bool || type.IsEnum))
        return obj.ToString();
      if (type.GetInterface("System.Collections.IDictionary", true) != null)
      {
        IDictionary dictionary = (IDictionary) obj;
        string str = "Dictionary (" + (object) dictionary.Count + " elements)\n";
        foreach (object key in (IEnumerable) dictionary.Keys)
          str = str + prefix + Gui2Reflection.ToStringAdvanced(key, prefix + "   ") + " -> " + Gui2Reflection.ToStringAdvanced(dictionary[key], prefix + "   ") + "\n";
        return str.Substring(0, str.Length - 1);
      }
      if (type.IsArray || type.GetInterface("System.Collections.IList", true) != null)
      {
        string str = "Enumeration\n";
        foreach (object obj1 in obj as IEnumerable)
          str = str + Gui2Reflection.ToStringAdvanced(obj1, prefix + "   ") + "\n";
        return str.Substring(0, str.Length - 1);
      }
      if (!type.IsClass && !type.IsValueType)
        throw new Exception("The type " + type.FullName + " is not supported");
      List<MemberInfo> memberInfoList = new List<MemberInfo>();
      MemberTypes memberType = MemberTypes.Field | MemberTypes.Property;
      BindingFlags bindingAttr1 = BindingFlags.Static | BindingFlags.Public;
      memberInfoList.AddRange((IEnumerable<MemberInfo>) type.FindMembers(memberType, bindingAttr1, (MemberFilter) null, (object) null));
      BindingFlags bindingAttr2 = BindingFlags.Static | BindingFlags.NonPublic;
      memberInfoList.AddRange((IEnumerable<MemberInfo>) type.FindMembers(memberType, bindingAttr2, (MemberFilter) null, (object) null));
      BindingFlags bindingAttr3 = BindingFlags.Instance | BindingFlags.Public;
      memberInfoList.AddRange((IEnumerable<MemberInfo>) type.FindMembers(memberType, bindingAttr3, (MemberFilter) null, (object) null));
      BindingFlags bindingAttr4 = BindingFlags.Instance | BindingFlags.NonPublic;
      memberInfoList.AddRange((IEnumerable<MemberInfo>) type.FindMembers(memberType, bindingAttr4, (MemberFilter) null, (object) null));
      string str1 = prefix + "object\n";
      using (List<MemberInfo>.Enumerator enumerator = memberInfoList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          MemberInfo current = enumerator.Current;
          object obj1 = Gui2Reflection.MGetValue(obj, current);
          str1 = str1 + prefix + current.Name + ": " + Gui2Reflection.ToStringAdvanced(obj1, prefix + "   ") + "\n";
        }
      }
      return str1;
    }
  }
}
