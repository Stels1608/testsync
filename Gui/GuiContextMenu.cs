﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiContextMenu
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/_NewGui/layout/contextMenu")]
  public class GuiContextMenu : GuiPanel
  {
    private const string mc_elementPanelName = "elementPanel";
    private GuiPanel m_elementPanel;

    public GuiPanel ElementsPanel
    {
      get
      {
        return this.m_elementPanel;
      }
    }

    public GuiContextMenu()
      : base(UseInGuiEditor.GetDefaultLayout<GuiContextMenu>())
    {
      this.Initialize();
    }

    public GuiContextMenu(GuiContextMenu copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiContextMenu(JContextMenu json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiContextMenu(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JContextMenu((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      this.CheckChildPositioning();
    }

    public override void EditorDraw()
    {
      GuiElementBase.DrawElement.BeginStencil(this.Rect, false, false);
      base.EditorDraw();
      GuiElementBase.DrawElement.EndStencil();
    }

    private void Initialize()
    {
      this.m_elementPanel = this.Find<GuiPanel>("elementPanel");
    }

    private void CheckChildPositioning()
    {
      this.m_elementPanel.ResizePanelToFitChildren();
      this.m_elementPanel.SizeX = Mathf.Max(this.m_elementPanel.SizeX, 10f);
      this.m_elementPanel.SizeY = Mathf.Max(this.m_elementPanel.SizeY, 10f);
      this.m_elementPanel.Position = this.m_elementPanel.ElementPadding.TopLeft;
      this.Size = this.m_elementPanel.BottomRightCornerWithPadding;
    }

    public override void OnShow()
    {
      base.OnShow();
      if ((Object) Game.Instance != (Object) null)
        Game.InputDispatcher.Focused = (InputListener) this;
      this.CheckChildPositioning();
    }

    public override void OnHide()
    {
      base.OnHide();
      if (!((Object) Game.Instance != (Object) null) || Game.InputDispatcher.Focused != this)
        return;
      Game.InputDispatcher.Focused = (InputListener) null;
    }

    public void ShowMenu(Vector2 showPosition)
    {
      this.IsRendered = true;
      this.ResizePanelToFitChildren();
      this.Position = showPosition;
      if (this.Parent == null)
        return;
      if ((double) this.BottomRightCornerWithPadding.x > (double) this.Parent.SizeX)
        this.PositionX -= (float) (double) this.SizeX;
      if ((double) this.BottomRightCornerWithPadding.y <= (double) this.Parent.SizeY)
        return;
      this.PositionY -= (float) (double) this.SizeY;
    }

    public void HideMenu()
    {
      this.IsRendered = false;
    }

    public override void Draw()
    {
      GuiElementBase.DrawElement.BeginStencil(this.Rect, false, false);
      base.Draw();
      GuiElementBase.DrawElement.EndStencil();
    }

    public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
    {
      if (this.Contains(mousePosition))
        return base.OnMouseDown(mousePosition, mouseKey);
      this.IsRendered = false;
      return false;
    }

    public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
    {
      if (this.Contains(mousePosition))
        return base.OnMouseUp(mousePosition, mouseKey);
      return false;
    }
  }
}
