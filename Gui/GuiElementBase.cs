﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiElementBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui.Tooltips;
using System;
using UnityEngine;

namespace Gui
{
  public class GuiElementBase : SRect
  {
    [HideInInspector]
    public bool SaveToLayout = true;
    [HideInInspector]
    protected bool enabled = true;
    private GuiElementPadding elementPadding = new GuiElementPadding();
    [HideInInspector]
    public bool handleMouseInput = true;
    [HideInInspector]
    private static DrawGuiElement drawElement;
    [HideInInspector]
    private bool updatedManually;
    [HideInInspector]
    private bool debug;
    [HideInInspector]
    private object environment;
    [HideInInspector]
    private bool isMouseOver;
    private bool fullPanelElement;
    private bool matchParentWidth;
    private bool matchParentHeight;
    [HideInInspector]
    private GuiAdvancedTooltipBase advancedTooltip;
    [HideInInspector]
    public AnonymousDelegate OnClick;
    [HideInInspector]
    public System.Action<GuiElementBase> OnClickAction;

    public static DrawGuiElement DrawElement
    {
      get
      {
        if (GuiElementBase.drawElement == null)
          GuiElementBase.drawElement = new DrawGuiElement();
        return GuiElementBase.drawElement;
      }
    }

    public bool IgnoreOnLayout { get; set; }

    public virtual bool Debug
    {
      get
      {
        return this.debug;
      }
      set
      {
        this.debug = value;
      }
    }

    public bool IsFullPanel
    {
      get
      {
        return this.fullPanelElement;
      }
      set
      {
        this.fullPanelElement = value;
      }
    }

    public bool MatchParentWidth
    {
      get
      {
        return this.matchParentWidth;
      }
      set
      {
        this.matchParentWidth = value;
      }
    }

    public bool MatchParentHeight
    {
      get
      {
        return this.matchParentHeight;
      }
      set
      {
        this.matchParentHeight = value;
      }
    }

    public Rect ElementPaddingRect
    {
      get
      {
        return (Rect) this.elementPadding;
      }
      set
      {
        this.elementPadding = (GuiElementPadding) value;
      }
    }

    public GuiElementPadding ElementPadding
    {
      get
      {
        return this.elementPadding;
      }
      set
      {
        this.elementPadding = value;
      }
    }

    public float ElementWithPaddingSizeX
    {
      get
      {
        return this.elementPadding.Left + this.SizeX + this.elementPadding.Right;
      }
    }

    public float ElementWithPaddingSizeY
    {
      get
      {
        return this.elementPadding.Top + this.SizeY + this.elementPadding.Bottom;
      }
    }

    public Vector2 TopLeftCornerWithPadding
    {
      get
      {
        return this.Position - this.elementPadding.TopLeft;
      }
    }

    public Vector2 BottomRightCornerWithPadding
    {
      get
      {
        return this.Position + this.Size + this.elementPadding.BottomRight;
      }
    }

    [Gui2Editor]
    public virtual bool IsRendered
    {
      get
      {
        return this.enabled;
      }
      set
      {
        if (this.enabled == value)
          return;
        this.enabled = value;
        if (value)
        {
          this.OnShow();
          this.ForceReposition();
        }
        else
          this.OnHide();
      }
    }

    [Gui2Editor]
    public virtual bool IsUpdated
    {
      get
      {
        if (!this.updatedManually)
          return this.enabled;
        return true;
      }
      set
      {
        this.updatedManually = value;
      }
    }

    public bool IsMouseOver
    {
      get
      {
        return this.isMouseOver;
      }
    }

    public virtual bool HandleMouseInput
    {
      get
      {
        return this.handleMouseInput;
      }
      set
      {
        this.handleMouseInput = value;
      }
    }

    public GuiAdvancedTooltipBase ToolTip
    {
      get
      {
        return this.advancedTooltip;
      }
      set
      {
        this.advancedTooltip = value;
      }
    }

    public GuiElementBase()
      : base(Vector2.zero, Vector2.zero)
    {
    }

    public GuiElementBase(GuiElementBase copy)
      : base((SRect) copy)
    {
      this.SaveToLayout = copy.SaveToLayout;
      this.enabled = copy.enabled;
      this.updatedManually = copy.updatedManually;
      this.debug = copy.debug;
      this.environment = copy.environment;
      this.isMouseOver = copy.isMouseOver;
      this.OnClick = copy.OnClick;
      this.OnClickAction = copy.OnClickAction;
      this.elementPadding = copy.elementPadding;
      this.fullPanelElement = copy.fullPanelElement;
      this.matchParentWidth = copy.matchParentWidth;
      this.matchParentHeight = copy.matchParentHeight;
      if (copy.advancedTooltip == null)
        return;
      this.advancedTooltip = new GuiAdvancedTooltipBase(copy.advancedTooltip);
    }

    public GuiElementBase(JElementBase json)
      : base(json)
    {
      this.elementPadding = (GuiElementPadding) json.elementPadding;
      this.fullPanelElement = json.fullPanel;
      this.matchParentWidth = json.matchParentWidth;
      this.matchParentHeight = json.matchParentHeight;
    }

    public GuiElementBase(Vector2 position, Vector2 size)
      : base(position, size)
    {
    }

    public virtual GuiElementBase CloneElement()
    {
      return new GuiElementBase(this);
    }

    public virtual JElementBase Convert2Json()
    {
      return new JElementBase(this);
    }

    public virtual void EditorUpdate()
    {
    }

    public virtual void EditorDraw()
    {
    }

    public virtual void OnShow()
    {
    }

    public virtual void OnHide()
    {
    }

    public virtual T GetEnvironment<T>()
    {
      if (this.environment == null)
        return default (T);
      if (!(this.environment is T))
        throw new Exception("Environment is of wrong type " + (object) this.environment.GetType() + ". Wanted " + (object) typeof (T));
      return (T) this.environment;
    }

    public virtual object GetEnvironment()
    {
      return this.environment;
    }

    public virtual void SetEnvironment(object env)
    {
      this.environment = env;
    }

    public void RemoveThisFromParent()
    {
      if (this.Parent == null)
        return;
      (this.Parent as GuiPanel).RemoveChild(this);
    }

    public void RemoveThisFromParentLater()
    {
      if (this.Parent == null)
        return;
      (this.Parent as GuiPanel).RemoveChildLater(this);
    }

    public override void DoCaching()
    {
      if (this.Parent != null)
      {
        Vector2 vector2 = this.Parent.Size - this.ElementPadding.BottomRight - this.ElementPadding.TopLeft;
        if (this.fullPanelElement || this.matchParentWidth)
        {
          this.SizeX = vector2.x;
          this.PositionX = this.ElementPadding.Left;
        }
        if (this.fullPanelElement || this.matchParentHeight)
        {
          this.SizeY = vector2.y;
          this.PositionY = this.ElementPadding.Top;
        }
      }
      base.DoCaching();
    }

    public virtual void Draw()
    {
      this.EnsurePositioned();
      if (!this.debug || Event.current.type != UnityEngine.EventType.Repaint)
        return;
      Graphics.DrawTexture(new Rect(this.Rect.xMin, this.Rect.yMin, this.Rect.width, this.Rect.height), (Texture) TextureCache.Get(new Color(1f, 0.0f, 0.0f, 0.2f)));
    }

    public virtual void Update()
    {
    }

    public virtual void PeriodicUpdate()
    {
    }

    public virtual bool Contains(float2 point)
    {
      return this.Rect.Contains(point.ToV2());
    }

    public virtual bool MouseDown(float2 position, KeyCode key)
    {
      Inspector2.Selected = (SRect) this;
      return true;
    }

    public virtual bool MouseUp(float2 position, KeyCode key)
    {
      if (key == KeyCode.Mouse0)
      {
        if (this.OnClick != null)
          this.OnClick();
        if (this.OnClickAction != null)
          this.OnClickAction(this);
      }
      return true;
    }

    public virtual bool MouseMove(float2 position, float2 previousPosition)
    {
      bool flag1 = this.Contains(position);
      bool flag2 = this.Contains(previousPosition);
      if (flag1 && !flag2)
        this.MouseEnter(position, previousPosition);
      if (!flag1 && flag2)
        this.MouseLeave(position, previousPosition);
      this.isMouseOver = flag1;
      return flag1;
    }

    public virtual void MouseEnter(float2 position, float2 previousPosition)
    {
      if (this.advancedTooltip == null)
        return;
      this.advancedTooltip.Parent = (SRect) this;
      Game.TooltipManager.ShowTooltip(this.advancedTooltip);
    }

    public virtual void MouseLeave(float2 position, float2 previousPosition)
    {
    }

    public virtual bool KeyDown(KeyCode key, Action action)
    {
      return false;
    }

    public virtual bool KeyUp(KeyCode key, Action action)
    {
      return false;
    }

    public virtual bool ScrollDown(float2 position)
    {
      return false;
    }

    public virtual bool ScrollUp(float2 position)
    {
      return false;
    }

    public void SetTooltip(string text)
    {
      if (this.advancedTooltip == null || !(this.advancedTooltip is GuiAdvancedLabelTooltip))
        this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip(text);
      else
        (this.advancedTooltip as GuiAdvancedLabelTooltip).SetText(text);
    }

    public void SetTooltip(string label, Action hotKey)
    {
      if (this.advancedTooltip == null || !(this.advancedTooltip is GuiAdvancedHotkeyTooltip))
        this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedHotkeyTooltip(label, hotKey);
      else
        (this.advancedTooltip as GuiAdvancedHotkeyTooltip).SetText(label, hotKey);
    }

    public void SetTooltip(ShipItem item)
    {
      if (item == null)
        return;
      if (this.advancedTooltip == null || !(this.advancedTooltip is GuiAdvancedItemTooltip))
        this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedItemTooltip(item);
      else
        (this.advancedTooltip as GuiAdvancedItemTooltip).SetItem(item);
    }

    public void SetTooltip(RewardCard card)
    {
      if (!(this.advancedTooltip is GuiAdvancedRewardTooltip))
        this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedRewardTooltip();
      (this.advancedTooltip as GuiAdvancedRewardTooltip).SetItem(card);
    }

    public void SetStoreToolTip(GameItemCard item, ShopInventoryContainer exchangeType = ShopInventoryContainer.Store, bool condensedItemVersion = false)
    {
      if (item is StarterKit)
      {
        if (!(this.advancedTooltip is GuiAdvancedStarterKitTooltip))
          this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedStarterKitTooltip();
        (this.advancedTooltip as GuiAdvancedStarterKitTooltip).SetStarterKit(item as StarterKit);
      }
      else
      {
        if (!(this.advancedTooltip is GuiAdvancedShopItemTooltip))
          this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedShopItemTooltip();
        (this.advancedTooltip as GuiAdvancedShopItemTooltip).SetShopItem(item, exchangeType, condensedItemVersion, false, false);
      }
    }

    public void SetShipToolTip(ShipCard ship, ShopInventoryContainer exchangeType = ShopInventoryContainer.Store)
    {
      if (!(this.advancedTooltip is GuiAdvancedShipTooltip))
        this.advancedTooltip = (GuiAdvancedTooltipBase) new GuiAdvancedShipTooltip();
      (this.advancedTooltip as GuiAdvancedShipTooltip).SetShip(ship, exchangeType);
    }

    public virtual bool OnShowTooltip(float2 position)
    {
      return false;
    }

    public override bool Equals(object obj)
    {
      if (base.Equals(obj))
        return true;
      if (obj == null || this.GetType() != obj.GetType())
        return false;
      GuiElementBase guiElementBase = obj as GuiElementBase;
      if (guiElementBase != null && this.Name == guiElementBase.Name && (this.Parent == guiElementBase.Parent && this.Position == guiElementBase.Position) && this.Size == guiElementBase.Size)
        return this.Align == guiElementBase.Align;
      return false;
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
  }
}
