﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiScrollBarBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  public class GuiScrollBarBase : GuiPanel
  {
    protected const float mc_scrollSpeed = 20f;
    [HideInInspector]
    protected GuiButton m_slideBar;
    [HideInInspector]
    protected GuiButton m_negativeScrollButton;
    [HideInInspector]
    protected GuiButton m_positiveScrollButton;
    [HideInInspector]
    public System.Action<float> OnScroll;
    [HideInInspector]
    protected float m_scrollDistance;
    [HideInInspector]
    protected float m_sliderDistance;
    protected bool m_alwaysShow;
    [HideInInspector]
    protected float m_scrollPosition;

    public float ScrollDistance
    {
      get
      {
        return this.m_scrollDistance;
      }
      set
      {
        if ((double) this.m_scrollDistance == (double) value)
          return;
        this.m_scrollDistance = value;
        this.ScrollAmount(0.0f);
      }
    }

    public bool AlwaysShow
    {
      get
      {
        return this.m_alwaysShow;
      }
      set
      {
        this.m_alwaysShow = value;
      }
    }

    public float ScrollPosition
    {
      get
      {
        return this.m_scrollPosition * (this.m_scrollDistance / this.m_sliderDistance);
      }
    }

    protected GuiScrollBarBase(string layoutFile)
      : base(layoutFile)
    {
      this.Initialize();
    }

    public GuiScrollBarBase()
      : base("GUI/_NewGui/layout/scrollBarLayout")
    {
      this.Initialize();
    }

    public GuiScrollBarBase(GuiScrollBarBase copy)
      : base((GuiPanel) copy)
    {
      this.OnScroll = copy.OnScroll;
      this.m_alwaysShow = copy.m_alwaysShow;
      this.Initialize();
    }

    public GuiScrollBarBase(JScrollBarPanel json)
      : base((JPanel) json)
    {
      this.m_alwaysShow = json.m_alwaysShow;
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiScrollBarBase(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JScrollBarPanel((GuiElementBase) this);
    }

    public override void EditorDraw()
    {
      GuiElementBase.DrawElement.BeginStencil(this.Rect, true, false);
      base.EditorDraw();
      for (int index = 0; index < this.Children.Count; ++index)
      {
        if (this.Children[index].IsRendered)
          this.Children[index].EditorDraw();
      }
      GuiElementBase.DrawElement.EndStencil();
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
    }

    private void Initialize()
    {
      this.m_slideBar = this.Find<GuiButton>("slideBar");
      this.m_negativeScrollButton = this.Find<GuiButton>("negativeArrow");
      this.m_negativeScrollButton.Pressed = new AnonymousDelegate(this.ScrollNegative);
      this.m_positiveScrollButton = this.Find<GuiButton>("positiveArrow");
      this.m_positiveScrollButton.Pressed = new AnonymousDelegate(this.ScrollPositive);
      this.m_scrollPosition = 0.0f;
      this.ScrollAmount(0.0f);
    }

    protected virtual void ScrollAmount(float amount)
    {
      float num = Mathf.Max(0.0f, Mathf.Min(this.m_sliderDistance - this.m_slideBar.SizeY, this.m_scrollPosition + amount));
      if ((double) this.m_scrollPosition == (double) num || this.OnScroll == null)
        return;
      this.OnScroll(this.m_scrollPosition - num);
      this.m_scrollPosition = num;
    }

    protected virtual void ElementHasChanged()
    {
      this.m_scrollPosition = 0.0f;
      this.m_sliderDistance = 0.0f;
      this.ScrollAmount(0.0f);
    }

    public void ScrollNegative()
    {
      this.ScrollAmount(-20f);
    }

    public void ScrollPositive()
    {
      this.ScrollAmount(20f);
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      if (this.m_slideBar.Contains(position) && this.m_slideBar.MouseDown(position, key))
      {
        this.m_slideBar.IsPressedManual = true;
        return true;
      }
      if (this.m_negativeScrollButton.Contains(position) && this.m_negativeScrollButton.MouseDown(position, key) || this.m_positiveScrollButton.Contains(position) && this.m_positiveScrollButton.MouseDown(position, key))
        return true;
      return this.Contains(position);
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      this.m_slideBar.IsPressedManual = false;
      if (this.m_slideBar.Contains(position) && this.m_slideBar.MouseUp(position, key) || this.m_negativeScrollButton.Contains(position) && this.m_negativeScrollButton.MouseUp(position, key) || this.m_positiveScrollButton.Contains(position) && this.m_positiveScrollButton.MouseUp(position, key))
        return true;
      return this.Contains(position);
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      this.m_slideBar.MouseMove(position, previousPosition);
      this.m_negativeScrollButton.MouseMove(position, previousPosition);
      this.m_positiveScrollButton.MouseMove(position, previousPosition);
      if (this.m_slideBar.IsPressedManual)
        this.ScrollAmount(position.y - previousPosition.y);
      return base.MouseMove(position, previousPosition);
    }

    public override bool ScrollUp(float2 position)
    {
      this.ScrollNegative();
      return true;
    }

    public override bool ScrollDown(float2 position)
    {
      this.ScrollPositive();
      return true;
    }
  }
}
