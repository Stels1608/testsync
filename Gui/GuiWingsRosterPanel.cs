﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiWingsRosterPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/_NewGui/layout/wings/wingsRosterPanel")]
  public class GuiWingsRosterPanel : GuiPanel
  {
    [HideInInspector]
    private System.Action<GuiWingsRosterPanel.WingsListSorting> SortWingList;
    [HideInInspector]
    private System.Action<GuiWingsRosterPanel.WingsListFilters> FilterWingList;
    [HideInInspector]
    private GuiContextMenu m_contextMenu;
    [HideInInspector]
    private GuiButton m_defaultContextButton;

    public GuiWingsRosterPanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiWingsRosterPanel>())
    {
      this.Initialize();
    }

    public GuiWingsRosterPanel(GuiWingsRosterPanel copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiWingsRosterPanel(JWingsRosterPanel json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiWingsRosterPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JWingsRosterPanel((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      this.m_contextMenu.IsRendered = true;
      base.EditorUpdate();
    }

    private void Initialize()
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      GuiWingsRosterPanel.\u003CInitialize\u003Ec__AnonStorey95 initializeCAnonStorey95 = new GuiWingsRosterPanel.\u003CInitialize\u003Ec__AnonStorey95();
      if (this.Find<GuiWingsRosterPanel.GuiRosterHeaderPanel>() == null)
      {
        GuiPanel panel = this.Find<GuiPanel>(GuiWingsRosterPanel.GuiRosterHeaderPanel.ms_PanelName);
        this.RemoveChild((GuiElementBase) panel);
        this.AddChild((GuiElementBase) new GuiWingsRosterPanel.GuiRosterHeaderPanel(panel));
      }
      GuiScrollPanel guiScrollPanel = this.Find<GuiScrollPanel>();
      this.RemoveChild((GuiElementBase) guiScrollPanel);
      this.AddChild((GuiElementBase) guiScrollPanel);
      // ISSUE: reference to a compiler-generated field
      initializeCAnonStorey95.wingmatePanel = guiScrollPanel.FindScrollChild<GuiWingsRosterPanel.GuiWingmateRowPanel>();
      // ISSUE: reference to a compiler-generated field
      if (initializeCAnonStorey95.wingmatePanel == null)
      {
        GuiPanel scrollChild = guiScrollPanel.FindScrollChild<GuiPanel>(GuiWingsRosterPanel.GuiWingmateRowPanel.ms_PanelName);
        guiScrollPanel.RemoveScrollChild((GuiElementBase) scrollChild);
        // ISSUE: reference to a compiler-generated field
        initializeCAnonStorey95.wingmatePanel = new GuiWingsRosterPanel.GuiWingmateRowPanel(scrollChild);
        // ISSUE: reference to a compiler-generated field
        guiScrollPanel.AddScrollChild((GuiElementBase) initializeCAnonStorey95.wingmatePanel);
      }
      GuiWingsRosterPanel.GuiRosterFilterPanel rosterFilterPanel = this.Find<GuiWingsRosterPanel.GuiRosterFilterPanel>();
      if (rosterFilterPanel == null)
      {
        GuiPanel panel = this.Find<GuiPanel>(GuiWingsRosterPanel.GuiRosterFilterPanel.ms_PanelName);
        this.RemoveChild((GuiElementBase) panel);
        rosterFilterPanel = new GuiWingsRosterPanel.GuiRosterFilterPanel(panel);
        this.AddChild((GuiElementBase) rosterFilterPanel);
      }
      // ISSUE: reference to a compiler-generated method
      this.SortWingList = new System.Action<GuiWingsRosterPanel.WingsListSorting>(initializeCAnonStorey95.\u003C\u003Em__111);
      // ISSUE: reference to a compiler-generated method
      this.FilterWingList = new System.Action<GuiWingsRosterPanel.WingsListFilters>(initializeCAnonStorey95.\u003C\u003Em__112);
      GuiWingsRosterPanel.WingsListSorting wingsListSorting = (GuiWingsRosterPanel.WingsListSorting) PlayerPrefs.GetInt("WingsRosterSort", 0);
      // ISSUE: reference to a compiler-generated field
      initializeCAnonStorey95.wingmatePanel.CurrentSorting = wingsListSorting;
      // ISSUE: reference to a compiler-generated field
      initializeCAnonStorey95.wingmatePanel.SortingOrder = PlayerPrefs.GetInt("WingsRosterSortOrder", 1);
      // ISSUE: reference to a compiler-generated field
      initializeCAnonStorey95.wingmatePanel.CurrentFilter = rosterFilterPanel.CurrentFilter;
      this.m_contextMenu = this.Find<GuiContextMenu>();
      if (this.m_contextMenu == null)
      {
        this.m_contextMenu = new GuiContextMenu();
        this.m_contextMenu.Name = "contextMenu";
      }
      this.RemoveChild((GuiElementBase) this.m_contextMenu);
      this.AddChild((GuiElementBase) this.m_contextMenu);
      this.SetupContextMenu();
    }

    private void SetupContextMenu()
    {
      this.m_contextMenu.IsRendered = false;
      GuiPanel elementsPanel = this.m_contextMenu.ElementsPanel;
      this.m_defaultContextButton = new GuiButton(elementsPanel.Find<GuiButton>());
      float num = 0.0f;
      for (int index = 0; index < Enum.GetValues(typeof (GuiWingsRosterPanel.ContextMenuOptions)).Length; ++index)
      {
        GuiButton guiButton;
        if (elementsPanel.Children.Count > index && elementsPanel.Children[index] is GuiButton)
        {
          guiButton = elementsPanel.Children[index] as GuiButton;
        }
        else
        {
          guiButton = new GuiButton(this.m_defaultContextButton);
          elementsPanel.AddChild((GuiElementBase) guiButton);
        }
        GuiWingsRosterPanel.ContextMenuOptions context = (GuiWingsRosterPanel.ContextMenuOptions) index;
        guiButton.Text = this.GetContextActionstring(context);
        guiButton.Name = context.ToString() + "Button";
        guiButton.PositionY = num + guiButton.ElementPadding.Top;
        num = guiButton.BottomRightCornerWithPadding.y;
      }
    }

    public void ShowContextMenuFor(Player player, Vector2 position)
    {
      this.SetContextMenuOptions(player);
      position.x -= this.m_contextMenu.Parent.Rect.x;
      position.y -= this.m_contextMenu.Parent.Rect.y;
      this.m_contextMenu.ShowMenu(position);
    }

    private void SetContextMenuOptions(Player player)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      GuiWingsRosterPanel.\u003CSetContextMenuOptions\u003Ec__AnonStorey96 optionsCAnonStorey96 = new GuiWingsRosterPanel.\u003CSetContextMenuOptions\u003Ec__AnonStorey96();
      // ISSUE: reference to a compiler-generated field
      optionsCAnonStorey96.player = player;
      // ISSUE: reference to a compiler-generated field
      optionsCAnonStorey96.\u003C\u003Ef__this = this;
      GuiPanel elementsPanel = this.m_contextMenu.ElementsPanel;
      float num = 0.0f;
      for (int index = 0; index < elementsPanel.Children.Count; ++index)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GuiWingsRosterPanel.\u003CSetContextMenuOptions\u003Ec__AnonStorey97 optionsCAnonStorey97 = new GuiWingsRosterPanel.\u003CSetContextMenuOptions\u003Ec__AnonStorey97();
        // ISSUE: reference to a compiler-generated field
        optionsCAnonStorey97.\u003C\u003Ef__ref\u0024150 = optionsCAnonStorey96;
        // ISSUE: reference to a compiler-generated field
        optionsCAnonStorey97.\u003C\u003Ef__this = this;
        GuiButton guiButton = elementsPanel.Children[index] as GuiButton;
        // ISSUE: reference to a compiler-generated field
        optionsCAnonStorey97.context = (GuiWingsRosterPanel.ContextMenuOptions) index;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        if (this.ShowCurrentContext(optionsCAnonStorey96.player, optionsCAnonStorey97.context))
        {
          guiButton.PositionY = num + guiButton.ElementPadding.Top;
          num = guiButton.BottomRightCornerWithPadding.y;
          guiButton.IsRendered = true;
          // ISSUE: reference to a compiler-generated method
          guiButton.Pressed = new AnonymousDelegate(optionsCAnonStorey97.\u003C\u003Em__113);
        }
        else
          guiButton.IsRendered = false;
      }
    }

    private string GetContextActionstring(GuiWingsRosterPanel.ContextMenuOptions context)
    {
      switch (context)
      {
        case GuiWingsRosterPanel.ContextMenuOptions.LeaveWing:
          return "%$bgo.Wings.player_context_menu.leave_wing%";
        case GuiWingsRosterPanel.ContextMenuOptions.Whisper:
          return "%$bgo.Wings.player_context_menu.whisper%";
        case GuiWingsRosterPanel.ContextMenuOptions.Invite:
          return "%$bgo.Wings.player_context_menu.invite%";
        case GuiWingsRosterPanel.ContextMenuOptions.Target:
          return "%$bgo.Wings.player_context_menu.target%";
        case GuiWingsRosterPanel.ContextMenuOptions.Ignore:
          return "%$bgo.Wings.player_context_menu.ignore%";
        case GuiWingsRosterPanel.ContextMenuOptions.Promote:
          return "%$bgo.Wings.player_context_menu.promote%";
        case GuiWingsRosterPanel.ContextMenuOptions.Demote:
          return "%$bgo.Wings.player_context_menu.demote%";
        case GuiWingsRosterPanel.ContextMenuOptions.Remove:
          return "%$bgo.Wings.player_context_menu.remove%";
        case GuiWingsRosterPanel.ContextMenuOptions.Cancel:
          return "%$bgo.Wings.player_context_menu.cancel%";
        default:
          return "Current context, " + context.ToString() + ", has not been added to be localized";
      }
    }

    private void PreformContextAction(Player player, GuiWingsRosterPanel.ContextMenuOptions context)
    {
      if (!this.ShowCurrentContext(player, context))
        return;
      switch (context)
      {
        case GuiWingsRosterPanel.ContextMenuOptions.LeaveWing:
          Game.GUIManager.Find<GuiWingsMainPanel>().IsRendered = false;
          Game.GUIManager.Find<MessageBoxManager>().ShowDialogBox(BsgoLocalization.Get("%$bgo.guild.leave_confirm%", (object) Game.Me.Guild.Name), BsgoLocalization.Get("%$bgo.guild.leave_wing_messagebox_title%"), "%$bgo.guild.leave%", "%$bgo.common.cancel%", (System.Action<MessageBoxActionType>) (actionType =>
          {
            if (actionType == MessageBoxActionType.Ok)
              Game.Guilds.LeaveGuild();
            Game.GUIManager.Find<GuiWingsMainPanel>().IsRendered = true;
          }));
          break;
        case GuiWingsRosterPanel.ContextMenuOptions.Whisper:
          Game.GUIManager.Find<GUIChatNew>().StartWhisper(player.Name);
          break;
        case GuiWingsRosterPanel.ContextMenuOptions.Invite:
          Game.Me.Party.Invite(player);
          break;
        case GuiWingsRosterPanel.ContextMenuOptions.Target:
          player.SetMyTarget();
          break;
        case GuiWingsRosterPanel.ContextMenuOptions.Ignore:
          Game.Me.Friends.AddToIgnored(player.Name);
          break;
        case GuiWingsRosterPanel.ContextMenuOptions.Promote:
          Game.Guilds.PromoteDemote(player, player.GuildRole + (byte) 1);
          break;
        case GuiWingsRosterPanel.ContextMenuOptions.Demote:
          Game.Guilds.PromoteDemote(player, player.GuildRole - (byte) 1);
          break;
        case GuiWingsRosterPanel.ContextMenuOptions.Remove:
          Game.Guilds.KickPlayer(player);
          break;
      }
    }

    private bool ShowCurrentContext(Player player, GuiWingsRosterPanel.ContextMenuOptions context)
    {
      try
      {
        switch (context)
        {
          case GuiWingsRosterPanel.ContextMenuOptions.LeaveWing:
            return player.IsMe;
          case GuiWingsRosterPanel.ContextMenuOptions.Whisper:
            return !player.IsMe && player.Online;
          case GuiWingsRosterPanel.ContextMenuOptions.Invite:
            return !player.IsMe && player.Online;
          case GuiWingsRosterPanel.ContextMenuOptions.Target:
            SpaceLevel spaceLevel = GameLevel.Instance as SpaceLevel;
            if ((UnityEngine.Object) spaceLevel != (UnityEngine.Object) null && spaceLevel.IsTournament)
              return false;
            return !player.IsMe && player.Online && player.IsSameSector;
          case GuiWingsRosterPanel.ContextMenuOptions.Ignore:
            return false;
          case GuiWingsRosterPanel.ContextMenuOptions.Promote:
            return !player.IsMe && Game.Me.Guild.GetPermissionFor(Game.Me.Guild.GuildRole, GuildOperation.PromoteDemote) && player.GuildRole < Game.Me.Guild.GuildRole - (byte) 1;
          case GuiWingsRosterPanel.ContextMenuOptions.Demote:
            return !player.IsMe && Game.Me.Guild.GetPermissionFor(Game.Me.Guild.GuildRole, GuildOperation.PromoteDemote) && player.GuildRole < Game.Me.Guild.GuildRole && player.GuildRole > GuildRole.Recruit;
          case GuiWingsRosterPanel.ContextMenuOptions.Remove:
            return !player.IsMe && Game.Me.Guild.GetPermissionFor(Game.Me.Guild.GuildRole, GuildOperation.KickMember) && player.GuildRole < Game.Me.Guild.GuildRole;
          case GuiWingsRosterPanel.ContextMenuOptions.Cancel:
            return true;
          default:
            return true;
        }
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("An error occurred while show the context option, " + context.ToString() + "\n(Player == null) :: " + (object) (player == null) + "\nThis occurred inside Gui.GuiWingsRosterPaenl.ShowCurrentContext(Player, ContextMenuOptions)\n\n" + ex.Message + "\n\n" + ex.StackTrace));
        return false;
      }
    }

    private enum WingsListSorting
    {
      PlayerName,
      PlayerLevel,
      GuildRank,
      PlayerLocation,
    }

    private enum WingsListFilters
    {
      AllPlayers,
      OnlineNow,
      OneDayOld,
      SevenDaysOld,
      TwentyEightDaysOld,
      Older,
    }

    private class GuiRosterFilterPanel : GuiPanel
    {
      public static string ms_PanelName = "filterPanel";
      private GuiDropdownMenu m_filterDropDownMenu;
      private GuiPanel m_dropDownPanel;
      private GuiButton m_defaultFilterButton;
      private GuiWingsRosterPanel.WingsListFilters m_currentFilter;

      public GuiWingsRosterPanel.WingsListFilters CurrentFilter
      {
        get
        {
          return this.m_currentFilter;
        }
      }

      public GuiRosterFilterPanel(GuiPanel panel)
        : base(panel)
      {
        this.m_filterDropDownMenu = this.Find<GuiDropdownMenu>();
        this.m_filterDropDownMenu.Align = Align.UpRight;
        this.m_filterDropDownMenu.PositionX = 15f;
        this.m_dropDownPanel = this.m_filterDropDownMenu.DropDownPanel;
        this.m_defaultFilterButton = new GuiButton(this.m_dropDownPanel.Find<GuiButton>());
        float num = 0.0f;
        for (int index = 0; index < Enum.GetValues(typeof (GuiWingsRosterPanel.WingsListFilters)).Length; ++index)
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          GuiWingsRosterPanel.GuiRosterFilterPanel.\u003CGuiRosterFilterPanel\u003Ec__AnonStorey98 panelCAnonStorey98 = new GuiWingsRosterPanel.GuiRosterFilterPanel.\u003CGuiRosterFilterPanel\u003Ec__AnonStorey98();
          // ISSUE: reference to a compiler-generated field
          panelCAnonStorey98.\u003C\u003Ef__this = this;
          GuiButton guiButton;
          if (this.m_dropDownPanel.Children.Count > index && this.m_dropDownPanel.Children[index] is GuiButton)
          {
            guiButton = this.m_dropDownPanel.Children[index] as GuiButton;
          }
          else
          {
            guiButton = new GuiButton(this.m_defaultFilterButton);
            this.m_dropDownPanel.AddChild((GuiElementBase) guiButton);
          }
          // ISSUE: reference to a compiler-generated field
          panelCAnonStorey98.filter = (GuiWingsRosterPanel.WingsListFilters) index;
          // ISSUE: reference to a compiler-generated field
          guiButton.Text = this.GetFilterOptionString(panelCAnonStorey98.filter);
          // ISSUE: reference to a compiler-generated method
          guiButton.Pressed = new AnonymousDelegate(panelCAnonStorey98.\u003C\u003Em__115);
          // ISSUE: reference to a compiler-generated field
          guiButton.Name = panelCAnonStorey98.filter.ToString() + "FilterButton";
          guiButton.PositionY = num + guiButton.ElementPadding.Top;
          num = guiButton.BottomRightCornerWithPadding.y;
        }
        this.m_currentFilter = (GuiWingsRosterPanel.WingsListFilters) PlayerPrefs.GetInt("WingsRosterFilter", 0);
        this.m_filterDropDownMenu.SetHeaderButtonText(this.GetFilterOptionString(this.m_currentFilter));
      }

      private string GetFilterOptionString(GuiWingsRosterPanel.WingsListFilters filter)
      {
        switch (filter)
        {
          case GuiWingsRosterPanel.WingsListFilters.AllPlayers:
            return "%$bgo.Wings.roster_filter_menu.all_players%";
          case GuiWingsRosterPanel.WingsListFilters.OnlineNow:
            return "%$bgo.Wings.roster_filter_menu.online_now%";
          case GuiWingsRosterPanel.WingsListFilters.OneDayOld:
            return "%$bgo.Wings.roster_filter_menu.one_day_offline%";
          case GuiWingsRosterPanel.WingsListFilters.SevenDaysOld:
            return "%$bgo.Wings.roster_filter_menu.seven_days_offline%";
          case GuiWingsRosterPanel.WingsListFilters.TwentyEightDaysOld:
            return "%$bgo.Wings.roster_filter_menu.twenty_seven_days_offline%";
          case GuiWingsRosterPanel.WingsListFilters.Older:
            return "%$bgo.Wings.roster_filter_menu.over_twenty_six_days_offline%";
          default:
            return "Current filter, " + filter.ToString() + ", has not been added to be localized";
        }
      }

      public override bool Contains(float2 point)
      {
        if (!base.Contains(point))
          return this.m_filterDropDownMenu.Contains(point);
        return true;
      }
    }

    private class GuiRosterHeaderPanel : GuiPanel
    {
      public static string ms_PanelName = "headerPanel";
      private const string mc_nameButtonName = "nameButton";
      private const string mc_levelButtonName = "levelButton";
      private const string mc_rankButtonName = "rankButton";
      private const string mc_locationButtonName = "locationButton";

      public GuiRosterHeaderPanel(GuiPanel panel)
        : base(panel)
      {
        GuiButton guiButton1 = this.Find<GuiButton>("nameButton");
        guiButton1.Text = this.GetSortOptionsString(GuiWingsRosterPanel.WingsListSorting.PlayerName);
        guiButton1.Pressed = (AnonymousDelegate) (() => (this.Parent as GuiWingsRosterPanel).SortWingList(GuiWingsRosterPanel.WingsListSorting.PlayerName));
        GuiButton guiButton2 = this.Find<GuiButton>("levelButton");
        guiButton2.Text = this.GetSortOptionsString(GuiWingsRosterPanel.WingsListSorting.PlayerLevel);
        guiButton2.Pressed = (AnonymousDelegate) (() => (this.Parent as GuiWingsRosterPanel).SortWingList(GuiWingsRosterPanel.WingsListSorting.PlayerLevel));
        GuiButton guiButton3 = this.Find<GuiButton>("rankButton");
        guiButton3.Text = this.GetSortOptionsString(GuiWingsRosterPanel.WingsListSorting.GuildRank);
        guiButton3.Pressed = (AnonymousDelegate) (() => (this.Parent as GuiWingsRosterPanel).SortWingList(GuiWingsRosterPanel.WingsListSorting.GuildRank));
        GuiButton guiButton4 = this.Find<GuiButton>("locationButton");
        guiButton4.Text = this.GetSortOptionsString(GuiWingsRosterPanel.WingsListSorting.PlayerLocation);
        guiButton4.Pressed = (AnonymousDelegate) (() => (this.Parent as GuiWingsRosterPanel).SortWingList(GuiWingsRosterPanel.WingsListSorting.PlayerLocation));
      }

      private string GetSortOptionsString(GuiWingsRosterPanel.WingsListSorting sort)
      {
        switch (sort)
        {
          case GuiWingsRosterPanel.WingsListSorting.PlayerName:
            return "%$bgo.Wings.roster_sort_tabs.player_name%";
          case GuiWingsRosterPanel.WingsListSorting.PlayerLevel:
            return "%$bgo.Wings.roster_sort_tabs.player_level%";
          case GuiWingsRosterPanel.WingsListSorting.GuildRank:
            return "%$bgo.Wings.roster_sort_tabs.guild_rank%";
          case GuiWingsRosterPanel.WingsListSorting.PlayerLocation:
            return "%$bgo.Wings.roster_sort_tabs.plyer_location%";
          default:
            return "Current sort, " + sort.ToString() + ", has not been added to be localized";
        }
      }
    }

    private class GuiWingmateRowPanel : GuiPanel
    {
      public static string ms_PanelName = "rowPanel";
      [HideInInspector]
      private List<GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow> m_wingmateRows = new List<GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow>();
      [HideInInspector]
      private bool m_sortDescending = true;
      private const string mc_darkBGName = "darkBackground";
      private const string mc_lightBGName = "lightBackground";
      [HideInInspector]
      private GuiImage m_darkRowBackground;
      [HideInInspector]
      private GuiImage m_lightRowBackground;
      [HideInInspector]
      private GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow m_defaultRowPanel;
      [HideInInspector]
      private List<Player> m_wingmateList;
      [HideInInspector]
      private GuiWingsRosterPanel.WingsListSorting m_currentSorting;
      [HideInInspector]
      private GuiWingsRosterPanel.WingsListFilters m_currentFilter;

      public int SortingOrder
      {
        get
        {
          return this.m_sortDescending ? 1 : -1;
        }
        set
        {
          this.m_sortDescending = value > 0;
        }
      }

      public GuiWingsRosterPanel.WingsListSorting CurrentSorting
      {
        get
        {
          return this.m_currentSorting;
        }
        set
        {
          this.m_sortDescending = value != this.m_currentSorting || !this.m_sortDescending;
          this.m_currentSorting = value;
          this.UpdatePlayerList();
        }
      }

      public GuiWingsRosterPanel.WingsListFilters CurrentFilter
      {
        get
        {
          return this.m_currentFilter;
        }
        set
        {
          this.m_currentFilter = value;
          this.UpdatePlayerList();
        }
      }

      public GuiWingmateRowPanel(GuiPanel panel)
        : base(panel)
      {
        this.m_darkRowBackground = this.Find<GuiImage>("darkBackground");
        this.m_lightRowBackground = this.Find<GuiImage>("lightBackground");
        this.m_defaultRowPanel = this.Find<GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow>(GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow.ms_PanelName);
        if (this.m_defaultRowPanel == null)
        {
          GuiPanel panel1 = this.Find<GuiPanel>(GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow.ms_PanelName);
          this.RemoveChild((GuiElementBase) panel1);
          this.m_defaultRowPanel = new GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow(panel1);
          this.AddChild((GuiElementBase) this.m_defaultRowPanel);
        }
        this.m_defaultRowPanel.IsRendered = false;
        this.m_darkRowBackground.IsRendered = false;
        this.m_lightRowBackground.IsRendered = false;
        this.m_wingmateList = new List<Player>();
        if (!((UnityEngine.Object) Game.Instance != (UnityEngine.Object) null))
          return;
        this.UpdatePlayerList();
      }

      private void UpdatePlayerList()
      {
        this.m_wingmateList.Clear();
        for (int index = 0; index < Game.Me.Guild.Players.Count; ++index)
        {
          if (Game.Me.Guild.Players[index] == null)
          {
            Debug.LogError((object) ("The player in the Guild players list is null at index " + (object) index + " and is being removed."));
            Game.Me.Guild._RemovePlayer(Game.Me.Guild.Players[index], true);
            return;
          }
          if (this.CheckFiltering(Game.Me.Guild.Players[index]))
            this.m_wingmateList.Add(Game.Me.Guild.Players[index]);
        }
        this.Size = Vector2.zero;
        if (this.m_wingmateList.Count <= 0)
          return;
        this.m_wingmateList.Sort(new Comparison<Player>(this.SortingsList));
        for (int index = 0; index < this.m_wingmateList.Count; ++index)
        {
          if (index >= this.m_wingmateRows.Count)
            this.AddRow(this.m_wingmateList[index]);
          this.m_wingmateRows[index].SetPlayer(this.m_wingmateList[index]);
        }
        if (this.m_wingmateRows.Count <= 0)
          return;
        this.Size = this.m_wingmateRows[this.m_wingmateList.Count - 1].BottomRightCornerWithPadding;
      }

      private int SortingsList(Player p1, Player p2)
      {
        try
        {
          int num = 1;
          switch (this.m_currentSorting)
          {
            case GuiWingsRosterPanel.WingsListSorting.PlayerName:
              num = this.SortByPlayerName(p1, p2);
              break;
            case GuiWingsRosterPanel.WingsListSorting.PlayerLevel:
              num = this.SortByPlayerLevel(p1, p2);
              break;
            case GuiWingsRosterPanel.WingsListSorting.GuildRank:
              num = this.SortByGuildRank(p1, p2);
              break;
            case GuiWingsRosterPanel.WingsListSorting.PlayerLocation:
              num = this.SortByPlayerLocation(p1, p2);
              break;
          }
          if (num != 0 && !this.m_sortDescending)
            num = -num;
          return num;
        }
        catch (Exception ex)
        {
          Debug.LogError((object) ("An error occurred while sorting by " + this.m_currentSorting.ToString() + " the players.\n(Player1 == null) :: " + (object) (p1 == null) + "\n(Player1 == null) :: " + (object) (p1 == null) + "\nThis occurred inside Gui.GuiWingsRosterPaenl.GuiWingmateRowPanel.SortingsList(Player, Player)\n\n" + ex.Message + "\n\n" + ex.StackTrace));
          return 0;
        }
      }

      private int SortByPlayerName(Player p1, Player p2)
      {
        return string.Compare(p1.Name, p2.Name);
      }

      private int SortByPlayerLevel(Player p1, Player p2)
      {
        if ((int) p1.Level == (int) p2.Level)
          return 0;
        return (int) p1.Level > (int) p2.Level ? 1 : -1;
      }

      private int SortByGuildRank(Player p1, Player p2)
      {
        if (p1.GuildRole == p2.GuildRole)
          return 0;
        return p1.GuildRole > p2.GuildRole ? -1 : 1;
      }

      private int SortByPlayerLocation(Player p1, Player p2)
      {
        if (p1.Online && p2.Online)
          return string.Compare(p1.Sector.Name, p2.Sector.Name);
        if (p1.Online)
          return -1;
        if (p2.Online)
          return 1;
        return DateTime.Compare(p2.lastLogout, p1.lastLogout);
      }

      private bool CheckFiltering(Player player)
      {
        try
        {
          TimeSpan timeSpan = Game.TimeSync.ServerDateTime.Subtract(player.lastLogout);
          switch (this.m_currentFilter)
          {
            case GuiWingsRosterPanel.WingsListFilters.OnlineNow:
              return player.Online;
            case GuiWingsRosterPanel.WingsListFilters.OneDayOld:
              return player.Online || timeSpan.Days < 1;
            case GuiWingsRosterPanel.WingsListFilters.SevenDaysOld:
              return player.Online || timeSpan.Days < 8;
            case GuiWingsRosterPanel.WingsListFilters.TwentyEightDaysOld:
              return player.Online || timeSpan.Days < 29;
            case GuiWingsRosterPanel.WingsListFilters.Older:
              return !player.Online && timeSpan.Days > 28;
            default:
              return true;
          }
        }
        catch (Exception ex)
        {
          Debug.LogError((object) ("An error occurred while filtering by " + this.m_currentFilter.ToString() + " the players.\n(Player == null) :: " + (object) (player == null) + "\nThis occurred inside Gui.GuiWingsRosterPaenl.GuiWingmateRowPanel.CheckFiltering(Player)\n\n" + ex.Message + "\n\n" + ex.StackTrace));
          return false;
        }
      }

      private void AddRow(Player player)
      {
        GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow wingmateRow = new GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow((GuiPanel) this.m_defaultRowPanel);
        wingmateRow.IsRendered = true;
        wingmateRow.Parent = (SRect) this;
        wingmateRow.SetPlayer(player);
        int count = this.m_wingmateRows.Count;
        this.m_wingmateRows.Add(wingmateRow);
        float num = this.m_lightRowBackground.SizeY * ((float) count + 0.5f);
        this.m_wingmateRows[count].PositionY = num - this.m_wingmateRows[count].SizeY * 0.5f;
        this.SizeY = num + this.m_lightRowBackground.SizeY * 0.5f;
      }

      private void RemoveRow(Player player)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GuiWingsRosterPanel.GuiWingmateRowPanel.\u003CRemoveRow\u003Ec__AnonStorey99 rowCAnonStorey99 = new GuiWingsRosterPanel.GuiWingmateRowPanel.\u003CRemoveRow\u003Ec__AnonStorey99();
        // ISSUE: reference to a compiler-generated field
        rowCAnonStorey99.player = player;
        // ISSUE: reference to a compiler-generated field
        if (!this.m_wingmateList.Contains(rowCAnonStorey99.player))
          return;
        // ISSUE: reference to a compiler-generated field
        this.m_wingmateList.Remove(rowCAnonStorey99.player);
        // ISSUE: reference to a compiler-generated method
        this.m_wingmateRows.RemoveAll(new Predicate<GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow>(rowCAnonStorey99.\u003C\u003Em__11A));
      }

      public override void EditorUpdate()
      {
        base.EditorUpdate();
        this.m_defaultRowPanel.IsRendered = true;
        this.m_darkRowBackground.IsRendered = true;
        this.m_lightRowBackground.IsRendered = true;
      }

      public override void Update()
      {
        base.Update();
        for (int index = 0; index < this.m_wingmateList.Count; ++index)
        {
          if (this.m_wingmateRows[index].IsUpdated)
            this.m_wingmateRows[index].Update();
        }
      }

      public override void PeriodicUpdate()
      {
        base.PeriodicUpdate();
        this.UpdatePlayerList();
        for (int index = 0; index < this.m_wingmateList.Count; ++index)
        {
          if (this.m_wingmateRows[index].IsUpdated)
            this.m_wingmateRows[index].PeriodicUpdate();
        }
      }

      public override void DoCaching()
      {
        base.DoCaching();
        for (int index = 0; index < this.m_wingmateRows.Count; ++index)
          this.m_wingmateRows[index].DoCaching();
      }

      public override void Draw()
      {
        base.Draw();
        GuiElementBase.DrawElement.BeginStencil(this.Rect, true, false);
        Rect drawRect = new Rect();
        drawRect.x = this.Rect.x;
        drawRect.y = this.Rect.y;
        bool flag = false;
        for (int index = 0; index < this.m_wingmateRows.Count; ++index)
        {
          this.m_wingmateRows[index].IsRendered = index < this.m_wingmateList.Count && this.m_wingmateRows[index].IsRendered;
          if (this.m_wingmateRows[index].IsRendered)
          {
            GuiImage guiImage = !flag ? this.m_darkRowBackground : this.m_lightRowBackground;
            drawRect.width = guiImage.Rect.width;
            drawRect.height = guiImage.Rect.height;
            GuiElementBase.DrawElement.DrawTexture(guiImage.Texture, drawRect, guiImage.NineSliceEdge, new Rect?(guiImage.SourceRect), guiImage.OverlayColor, false);
            drawRect.y += drawRect.height;
            this.m_wingmateRows[index].Draw();
            flag = !flag;
          }
        }
        GuiElementBase.DrawElement.EndStencil();
      }

      public override bool MouseDown(float2 position, KeyCode key)
      {
        for (int index = 0; index < this.m_wingmateRows.Count; ++index)
        {
          if (this.m_wingmateRows[index].Contains(position) && this.m_wingmateRows[index].MouseDown(position, key))
            return true;
        }
        return base.MouseDown(position, key);
      }

      public override bool MouseUp(float2 position, KeyCode key)
      {
        for (int index = 0; index < this.m_wingmateRows.Count; ++index)
        {
          if (this.m_wingmateRows[index].Contains(position) && this.m_wingmateRows[index].MouseUp(position, key))
            return true;
        }
        return base.MouseUp(position, key);
      }

      public override bool MouseMove(float2 position, float2 previousPosition)
      {
        for (int index = 0; index < this.m_wingmateRows.Count; ++index)
        {
          if (this.m_wingmateRows[index].MouseMove(position, previousPosition))
            return true;
        }
        return base.MouseMove(position, previousPosition);
      }

      private class WingmateRow : GuiPanel
      {
        public static string ms_PanelName = "wingmateRowPanel";
        [HideInInspector]
        private string m_iconFilePath = "GUI/_NewGui/icons/Pilot_ranks/";
        private const string mc_nameLabelName = "nameLabel";
        private const string mc_levelLabelName = "levelLabel";
        private const string mc_rankLabelName = "rankLabel";
        private const string mc_rankIconName = "rankIcon";
        private const string mc_locationLabelName = "locationLabel";
        [HideInInspector]
        private GuiLabel m_nameLabel;
        [HideInInspector]
        private GuiLabel m_levelLabel;
        [HideInInspector]
        private GuiLabel m_rankLabel;
        [HideInInspector]
        private GuiImage m_rankIcon;
        [HideInInspector]
        private GuiLabel m_locationLabel;
        [HideInInspector]
        private Player m_player;

        public WingmateRow(GuiPanel panel)
          : base(panel)
        {
          this.m_nameLabel = this.Find<GuiLabel>("nameLabel");
          this.m_levelLabel = this.Find<GuiLabel>("levelLabel");
          this.m_rankLabel = this.Find<GuiLabel>("rankLabel");
          this.m_rankIcon = this.Find<GuiImage>("rankIcon");
          this.m_locationLabel = this.Find<GuiLabel>("locationLabel");
          this.SetPlayer((Player) null);
        }

        public void SetPlayer(Player player)
        {
          this.m_player = player;
          this.IsRendered = this.m_player != null;
          if (!this.IsRendered)
            return;
          this.m_nameLabel.Text = Tools.SubStr(this.m_player.Name, 32);
          this.m_levelLabel.Text = this.m_player.Level.ToString();
          this.m_rankLabel.Text = Tools.FormatWingRole(this.m_player.GuildRole);
          this.SetRankIcon();
          string str1 = string.Empty;
          string str2;
          if (this.m_player.Online && this.m_player.Sector != null)
          {
            str2 = this.m_player.Sector.Name;
          }
          else
          {
            TimeSpan timeSpan = Game.TimeSync.ServerLocalDateTime.Subtract(this.m_player.lastLogout.ToLocalTime());
            str2 = timeSpan.Days <= 1 ? (timeSpan.Days != 1 ? (timeSpan.Hours <= 1 ? (timeSpan.Hours != 1 ? (timeSpan.Minutes >= 15 ? "%$bgo.Wings.extra_labels.less_than_hour%" : "%$bgo.Wings.extra_labels.offline%") : "%$bgo.Wings.extra_labels.one_hour%") : timeSpan.Hours.ToString() + " %$bgo.Wings.extra_labels.hours%") : "%$bgo.Wings.extra_labels.one_day%") : (timeSpan.Days <= 730000 ? timeSpan.Days.ToString() + " %$bgo.Wings.extra_labels.days%" : "%$bgo.Wings.extra_labels.offline%");
          }
          this.m_locationLabel.Text = str2;
        }

        private void SetRankIcon()
        {
          Texture2D texture2D;
          switch (this.m_player.GuildRole)
          {
            case GuildRole.Pilot:
              texture2D = ResourceLoader.Load<Texture2D>(this.m_iconFilePath + "Pilot");
              break;
            case GuildRole.SeniorPilot:
              texture2D = ResourceLoader.Load<Texture2D>(this.m_iconFilePath + "Senior_Pilot");
              break;
            case GuildRole.FlightLeader:
              texture2D = ResourceLoader.Load<Texture2D>(this.m_iconFilePath + "Flight_Leader");
              break;
            case GuildRole.GroupLeader:
              texture2D = ResourceLoader.Load<Texture2D>(this.m_iconFilePath + "Group_Leader");
              break;
            case GuildRole.Leader:
              texture2D = ResourceLoader.Load<Texture2D>(this.m_iconFilePath + "Wing_Leader");
              break;
            default:
              texture2D = ResourceLoader.Load<Texture2D>(this.m_iconFilePath + "Recruit");
              break;
          }
          if (!((UnityEngine.Object) texture2D != (UnityEngine.Object) this.m_rankIcon.Texture))
            return;
          this.m_rankIcon.SetTextureWithoutResize(texture2D);
        }

        public bool Compare(Player player)
        {
          return player == this.m_player;
        }

        public override bool MouseDown(float2 position, KeyCode key)
        {
          if (!this.Contains(position) || key != KeyCode.Mouse1)
            return base.MouseDown(position, key);
          GuiWingsRosterPanel parent = this.FindParent<GuiWingsRosterPanel>();
          if (parent != null)
            parent.ShowContextMenuFor(this.m_player, position.ToV2());
          else
            Debug.LogError((object) "The GuiWingsRosterPanel was not found as a parent to Gui.GuiWingsRosterPanel.GuiWingmateRowPanel.WingmateRow.");
          return true;
        }
      }
    }

    private enum ContextMenuOptions
    {
      LeaveWing,
      Whisper,
      Invite,
      Target,
      Ignore,
      Promote,
      Demote,
      Remove,
      Cancel,
    }
  }
}
