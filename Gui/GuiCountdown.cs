﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiCountdown
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiCountdown : GuiPanel
  {
    public AtlasAnimation m_inner;
    protected GuiElementBase m_srInner;

    public virtual float Progress
    {
      get
      {
        return this.m_inner.PlaybackPosition;
      }
      set
      {
        this.m_inner.PlaybackPosition = value;
      }
    }

    public GuiCountdown(Texture2D texture, uint width, uint height)
    {
      this.m_inner = new AtlasAnimation(texture, width, height);
      this.Position = new Vector2(0.0f, 0.0f);
      this.Size = new Vector2(this.m_inner.ElementSize.x, this.m_inner.ElementSize.y);
      this.m_srInner = new GuiElementBase(this.Position, this.Size);
      this.AddChild(this.m_srInner);
      this.m_inner.PlaybackPosition = 0.0f;
    }

    public void Scale(float scaleFactor)
    {
      GuiElementBase guiElementBase1 = this.m_srInner;
      Vector2 vector2_1 = guiElementBase1.Position * scaleFactor;
      guiElementBase1.Position = vector2_1;
      GuiElementBase guiElementBase2 = this.m_srInner;
      Vector2 vector2_2 = guiElementBase2.Size * scaleFactor;
      guiElementBase2.Size = vector2_2;
    }

    public override void Draw()
    {
      if (Event.current.type != UnityEngine.EventType.Repaint)
        return;
      Graphics.DrawTexture(this.m_srInner.Rect, (Texture) this.m_inner.Texture, this.m_inner.FrameRect, 0, 0, 0, 0);
    }
  }
}
