﻿// Decompiled with JetBrains decompiler
// Type: Gui.Gui2Loader
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Json;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class Gui2Loader
  {
    public static readonly string BASE_DIRECTORY = "Assets/Resources/";
    public static Dictionary<string, Gui2Loader.Delegate> types = new Dictionary<string, Gui2Loader.Delegate>();

    static Gui2Loader()
    {
      Gui2Loader.types.Add("dialog", new Gui2Loader.Delegate(Gui2Loader.LoadGuiDialog));
      Gui2Loader.types.Add("panel", new Gui2Loader.Delegate(Gui2Loader.LoadGuiPanel));
      Gui2Loader.types.Add("elementbase", new Gui2Loader.Delegate(Gui2Loader.LoadGuiElementBase));
      Gui2Loader.types.Add("button", new Gui2Loader.Delegate(Gui2Loader.LoadGuiButton));
      Gui2Loader.types.Add("label", new Gui2Loader.Delegate(Gui2Loader.LoadGuiLabel));
      Gui2Loader.types.Add("image", new Gui2Loader.Delegate(Gui2Loader.LoadGuiImage));
      Gui2Loader.types.Add("checkbox", new Gui2Loader.Delegate(Gui2Loader.LoadGuiCheckbox));
      Gui2Loader.types.Add("healthbar", new Gui2Loader.Delegate(Gui2Loader.LoadGuiHealthbar));
      Gui2Loader.types.Add("verticalscroll", new Gui2Loader.Delegate(Gui2Loader.LoadGuiPanelVerticalScroll));
      Gui2Loader.types.Add("colorbox", new Gui2Loader.Delegate(Gui2Loader.LoadGuiColoredBox));
    }

    public static GuiElementBase Load(string path)
    {
      GuiElementBase el = (GuiElementBase) null;
      Gui2Loader.Load(ref el, path);
      return el;
    }

    public static void Load(GuiElementBase el, string path)
    {
      Gui2Loader.Load(ref el, path);
    }

    private static void Load(ref GuiElementBase el, string path)
    {
      if (path.EndsWith(".txt"))
        path = path.Remove(path.LastIndexOf(".txt"), ".txt".Length);
      JsonData json = JsonReader.Parse(((TextAsset) Resources.Load(path)).text);
      Gui2Loader.Load(ref el, json);
      (el as GuiPanel).LayoutPath = path;
    }

    public static GuiElementBase LoadCDB(string path)
    {
      GuiElementBase el = (GuiElementBase) null;
      Gui2Loader.LoadCDB(ref el, path);
      return el;
    }

    public static void LoadCDB(GuiElementBase el, string path)
    {
      Gui2Loader.LoadCDB(ref el, path);
    }

    private static void LoadCDB(ref GuiElementBase el, string path)
    {
      string[] strArray = path.Split(new char[1]{ '/' }, 2);
      if (strArray.Length < 2 || strArray[1].Length == 0)
        throw new Exception("Invalid CDB path " + path);
      JsonData documentRaw = ContentDB.GetDocumentRaw(strArray[0], strArray[1]);
      Gui2Loader.Load(ref el, documentRaw);
      (el as GuiPanel).LayoutPath = path;
    }

    public static GuiElementBase LoadRaw(string json)
    {
      GuiElementBase el = (GuiElementBase) null;
      Gui2Loader.LoadRaw(ref el, json);
      return el;
    }

    public static void LoadRaw(GuiElementBase el, string json)
    {
      Gui2Loader.LoadRaw(ref el, json);
    }

    private static void LoadRaw(ref GuiElementBase el, string json)
    {
      JsonData json1 = JsonReader.Parse(json);
      Gui2Loader.Load(ref el, json1);
      (el as GuiPanel).LayoutPath = "*** Unknown ***";
    }

    public static GuiElementBase Load(JsonData json)
    {
      GuiElementBase el = (GuiElementBase) null;
      Gui2Loader.Load(ref el, json);
      return el;
    }

    public static void Load(ref GuiElementBase el, JsonData json)
    {
      string @string = json.Object["type"].String;
      if (!Gui2Loader.types.ContainsKey(@string))
        throw new Exception("Can't load type " + @string);
      Gui2Loader.types[@string](ref el, json);
    }

    private static void LoadGuiElementBase(ref GuiElementBase el, JsonData json)
    {
      if (el == null)
        el = new GuiElementBase();
      if (json.Object.ContainsKey("name"))
        el.Name = json.Object["name"].String;
      if (json.Object.ContainsKey("action"))
        el.ActionString = json.Object["action"].String;
      el.Position = Gui2Loader.ReadVector2(json.Object["position"]);
      if (json.Object.ContainsKey("align"))
        el.Align = Gui2Loader.ReadEnum<Align>(json.Object["align"]);
      el.Size = Gui2Loader.ReadVector2(json.Object["size"]);
      if (json.Object.ContainsKey("isrendered"))
        el.IsRendered = json.Object["isrendered"].Boolean;
      if (!json.Object.ContainsKey("tooltip"))
        return;
      el.SetTooltip(json.Object["tooltip"].String);
    }

    private static void FillChildren(GuiPanel el, JsonData json)
    {
      using (List<JsonData>.Enumerator enumerator = json.Array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JsonData current = enumerator.Current;
          el.AddChild(Gui2Loader.Load(current));
        }
      }
    }

    private static void LoadGuiPanel(ref GuiElementBase el, JsonData json)
    {
      if (el == null)
        el = (GuiElementBase) new GuiPanel();
      Gui2Loader.LoadGuiElementBase(ref el, json);
      GuiPanel el1 = el as GuiPanel;
      if (json.Object.ContainsKey("mousetransparent"))
        el1.MouseTransparent = json.Object["mousetransparent"].Boolean;
      Gui2Loader.FillChildren(el1, json.Object["children"]);
    }

    private static void LoadGuiButton(ref GuiElementBase el, JsonData json)
    {
      if (el == null)
        el = (GuiElementBase) new GuiButton(string.Empty);
      Gui2Loader.LoadGuiElementBase(ref el, json);
      GuiButton guiButton = el as GuiButton;
      guiButton.NormalTexture = ResourceLoader.Load<Texture2D>(json.Object["normaltexture"].String);
      guiButton.OverTexture = ResourceLoader.Load<Texture2D>(json.Object["overtexture"].String);
      guiButton.PressedTexture = ResourceLoader.Load<Texture2D>(json.Object["pressedtexture"].String);
      guiButton.Font = ResourceLoader.Load<Font>(json.Object["font"].String);
      guiButton.Text = json.Object["text"].String;
      int int32 = json.Object["padding"].Int32;
      guiButton.ImagePadding = (GuiElementPadding) new Rect((float) int32, (float) int32, (float) int32, (float) int32);
      guiButton.IsPressedManual = json.Object["ispressedmanual"].Boolean;
    }

    private static void LoadGuiLabel(ref GuiElementBase el, JsonData json)
    {
      if (el == null)
        el = (GuiElementBase) new GuiLabel(string.Empty);
      GuiLabel guiLabel = el as GuiLabel;
      guiLabel.WordWrap = json.Object["wordwrap"].Boolean;
      guiLabel.AutoSize = json.Object["autosize"].Boolean;
      Gui2Loader.LoadGuiElementBase(ref el, json);
      guiLabel.Font = ResourceLoader.Load<Font>(json.Object["font"].String);
      guiLabel.Text = json.Object["text"].String;
      guiLabel.Alignment = Gui2Loader.ReadEnum<TextAnchor>(json.Object["alignment"]);
      guiLabel.OverlayColor = Gui2Loader.ReadNullableColor(json.Object["overlaycolor"]);
      guiLabel.NormalColor = Gui2Loader.ReadColor(json.Object["normalcolor"]);
      guiLabel.OverColor = Gui2Loader.ReadColor(json.Object["overcolor"]);
    }

    private static void LoadGuiImage(ref GuiElementBase el, JsonData json)
    {
      if (el == null)
        el = (GuiElementBase) new GuiImage();
      GuiImage guiImage = el as GuiImage;
      Gui2Loader.LoadGuiElementBase(ref el, json);
      guiImage.Texture = ResourceLoader.Load<Texture2D>(json.Object["texture"].String);
      int int32 = json.Object["padding"].Int32;
      guiImage.NineSliceEdge = (GuiElementPadding) new Rect((float) int32, (float) int32, (float) int32, (float) int32);
      guiImage.OverlayColor = Gui2Loader.ReadNullableColor(json.Object["overlaycolor"]);
    }

    private static void LoadGuiCheckbox(ref GuiElementBase el, JsonData json)
    {
      if (el == null)
        el = (GuiElementBase) new GuiCheckbox(string.Empty);
      GuiCheckbox guiCheckbox = el as GuiCheckbox;
      Gui2Loader.LoadGuiElementBase(ref el, json);
      guiCheckbox.IsChecked = json.Object["ischecked"].Boolean;
      guiCheckbox.Text = json.Object["text"].String;
    }

    private static void LoadGuiHealthbar(ref GuiElementBase el, JsonData json)
    {
      if (el == null)
        el = (GuiElementBase) new GuiHealthbar();
      GuiHealthbar guiHealthbar = el as GuiHealthbar;
      Gui2Loader.LoadGuiElementBase(ref el, json);
      guiHealthbar.Progress = json.Object["progress"].Float;
      guiHealthbar.BorderWidth = json.Object["borderwidth"].Float;
      guiHealthbar.FreeSpaceWidth = json.Object["freespacewidth"].Float;
    }

    private static void LoadGuiColoredBox(ref GuiElementBase el, JsonData json)
    {
      if (el == null)
        el = (GuiElementBase) new GuiColoredBox();
      Gui2Loader.LoadGuiElementBase(ref el, json);
      (el as GuiColoredBox).Color = Gui2Loader.ReadColor(json.Object["color"]);
    }

    private static void LoadGuiDialog(ref GuiElementBase el, JsonData json)
    {
      if (el == null)
        el = (GuiElementBase) new GuiDialog();
      Gui2Loader.LoadGuiPanel(ref el, json);
      GuiDialog guiDialog = el as GuiDialog;
      int int32 = json.Object["backgroundpadding"].Int32;
      guiDialog.BackgroundImage.NineSliceEdge = (GuiElementPadding) new Rect((float) int32, (float) int32, (float) int32, (float) int32);
      guiDialog.BackgroundImage.IsRendered = json.Object["showbackground"].Boolean;
      guiDialog.CloseButton.IsRendered = json.Object["showclosebutton"].Boolean;
    }

    private static void LoadGuiPanelVerticalScroll(ref GuiElementBase el, JsonData json)
    {
      if (el == null)
        el = (GuiElementBase) new GuiPanelVerticalScroll();
      Gui2Loader.LoadGuiPanel(ref el, json);
      GuiPanelVerticalScroll panelVerticalScroll = el as GuiPanelVerticalScroll;
      panelVerticalScroll.AutoSizeChildren = json.Object["autosizechildren"].Boolean;
      panelVerticalScroll.RenderScrollLines = json.Object["renderscrolllines"].Boolean;
      panelVerticalScroll.RenderDelimiters = json.Object["renderdelimiters"].Boolean;
      panelVerticalScroll.Space = json.Object["space"].Float;
    }

    public static JsonData WriteVector2(Vector2 v)
    {
      JsonData createObject = JsonData.CreateObject;
      createObject.Object["x"] = new JsonData(v.x);
      createObject.Object["y"] = new JsonData(v.y);
      return createObject;
    }

    public static Vector2 ReadVector2(JsonData json)
    {
      return new Vector2(json.Object["x"].Float, json.Object["y"].Float);
    }

    public static JsonData WriteEnum(Enum e)
    {
      return new JsonData(e.ToString().ToLower());
    }

    public static T ReadEnum<T>(JsonData json)
    {
      return (T) Enum.Parse(typeof (T), json.String, true);
    }

    public static JsonData WriteColor(Color c)
    {
      JsonData createObject = JsonData.CreateObject;
      createObject.Object["r"] = new JsonData(c.r);
      createObject.Object["g"] = new JsonData(c.g);
      createObject.Object["b"] = new JsonData(c.b);
      createObject.Object["a"] = new JsonData(c.a);
      return createObject;
    }

    public static Color ReadColor(JsonData json)
    {
      return new Color(json.Object["r"].Float, json.Object["g"].Float, json.Object["b"].Float, json.Object["a"].Float);
    }

    public static JsonData WriteNullableColor(Color? c)
    {
      if (c.HasValue)
        return Gui2Loader.WriteColor(c.Value);
      return JsonData.CreateNull;
    }

    public static Color? ReadNullableColor(JsonData json)
    {
      if (json.IsNull)
        return new Color?();
      return new Color?(Gui2Loader.ReadColor(json));
    }

    public delegate void Delegate(ref GuiElementBase el, JsonData json);
  }
}
