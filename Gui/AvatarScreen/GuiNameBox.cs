﻿// Decompiled with JetBrains decompiler
// Type: Gui.AvatarScreen.GuiNameBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.AvatarScreen
{
  public class GuiNameBox : GuiPanel
  {
    private AlphaDamp labelAlpha = new AlphaDamp(0.5f);
    public readonly GuiLabel label;
    public readonly GuiTextBox textBox;

    public GuiNameBox(Vector2 size)
    {
      this.Size = size;
      Font fontBgmBt = Gui.Options.FontBGM_BT;
      this.label = new GuiLabel("%$bgo.etc.avatar_enter_name%", fontBgmBt, 12);
      this.label.Size = size;
      this.textBox = new GuiTextBox(fontBgmBt);
      this.textBox.Size = size;
      this.AddChild((GuiElementBase) this.textBox);
      this.AddChild((GuiElementBase) this.label);
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      if (this.textBox.Contains(position))
      {
        this.IsMouseInputGrabber = true;
        this.HideEnterName();
      }
      else
        this.IsMouseInputGrabber = false;
      return base.MouseDown(position, key);
    }

    public override bool OnKeyDown(KeyCode keyboardKey, Action action)
    {
      base.OnKeyDown(keyboardKey, action);
      return true;
    }

    public override bool OnKeyUp(KeyCode keyboardKey, Action action)
    {
      base.OnKeyUp(keyboardKey, action);
      return true;
    }

    public override void Update()
    {
      base.Update();
      this.textBox.Text = this.TrueName(this.textBox.Text);
      this.textBox.Text = Tools.RemoveNoLatins(this.textBox.Text);
      this.labelAlpha.Update(Time.deltaTime);
      Color color = new Color(1f, 1f, 1f, this.labelAlpha.alpha);
      this.label.NormalColor = color;
      this.label.OverColor = color;
      if (this.textBox.Text.Length <= 0)
        return;
      this.HideEnterName();
    }

    private string TrueName(string name)
    {
      string str1 = "1234567890_";
      string str2 = string.Empty;
      foreach (char c in name)
      {
        if (char.IsLetter(c) || str1.Contains(c.ToString()))
          str2 += (string) (object) c;
      }
      return str2;
    }

    public void ShowEnterName()
    {
      this.labelAlpha.alpha = 1f;
      this.label.IsRendered = true;
    }

    public void HideEnterName()
    {
      this.label.IsRendered = false;
    }

    public bool CaseTrueName()
    {
      string text = this.textBox.Text;
      return text.Length > 0 && text.Replace(" ", string.Empty).Length > 0;
    }
  }
}
