﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiTabs
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class GuiTabs : GuiPanel
  {
    private Dictionary<GuiButton, GuiElementBase> dic = new Dictionary<GuiButton, GuiElementBase>();
    public System.Action<GuiElementBase> PlacePanel;
    protected GuiTabsSimple tabs;
    private GuiElementBase selectedContent;

    public Vector2 TabsPosition
    {
      get
      {
        return this.tabs.Position;
      }
      set
      {
        this.tabs.Position = value;
      }
    }

    public GuiTabs()
    {
      this.tabs = new GuiTabsSimple();
      this.AddChild((GuiElementBase) this.tabs);
    }

    public GuiButton AddTab(string name, GuiElementBase content)
    {
      GuiButton key = this.tabs.AddTab(name, new AnonymousDelegate(this.Select));
      this.dic.Add(key, content);
      return key;
    }

    public void SelectByName(string name)
    {
      this.tabs.SelectButtonWithText(name);
    }

    public void SelectByContent(GuiElementBase content)
    {
      using (Dictionary<GuiButton, GuiElementBase>.Enumerator enumerator = this.dic.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<GuiButton, GuiElementBase> current = enumerator.Current;
          if (current.Value == content)
          {
            this.tabs.SelectButton(current.Key);
            break;
          }
        }
      }
    }

    public string GetSelectedText()
    {
      return this.tabs.GetSelectedText();
    }

    public GuiButton GetButton(string name)
    {
      return this.tabs.GetButton(name);
    }

    public GuiElementBase GetSelectedContent()
    {
      return this.selectedContent;
    }

    private void Select()
    {
      GuiButton selected = this.tabs.GetSelected();
      if (this.selectedContent != null)
        this.RemoveChild(this.selectedContent);
      this.selectedContent = selected != null ? this.dic[selected] : (GuiElementBase) null;
      if (this.selectedContent != null)
        this.AddChild(this.selectedContent);
      this.OnSelect(selected, this.selectedContent);
      this.ForceReposition();
    }

    public void SelectIndex(int index)
    {
      this.tabs.SelectIndex(index);
    }

    public void SelectNext()
    {
      this.tabs.SelectNext();
    }

    public void SelectPrevious()
    {
      this.tabs.SelectPrevious();
    }

    protected virtual void OnSelect(GuiButton button, GuiElementBase content)
    {
      if (this.PlacePanel == null)
        return;
      this.PlacePanel(content);
    }

    public override void Reposition()
    {
      base.Reposition();
      this.Size = this.tabs.Size;
    }

    public override bool Contains(float2 point)
    {
      bool flag = this.selectedContent != null && this.selectedContent.Contains(point);
      if (!base.Contains(point))
        return flag;
      return true;
    }
  }
}
