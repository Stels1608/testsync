﻿// Decompiled with JetBrains decompiler
// Type: Gui.AskBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class AskBox : GuiModalDialog
  {
    private readonly GuiLabel textName = new GuiLabel(Options.FontBGM_BT, 20);
    private readonly GuiLabel timer = new GuiLabel(Tools.Color(Options.NormalColor, 100), Options.FontDS_DIGIB, 18);
    private readonly GuiButton okButton = new GuiButton(string.Empty);
    private readonly GuiButton cancelButton = new GuiButton(string.Empty);
    public AnonymousDelegate OnOk;
    public AnonymousDelegate OnCancel;
    [Gui2Editor]
    private float endTime;

    public AskBox(string message)
      : this(message, (AnonymousDelegate) null)
    {
    }

    public AskBox(string message, AnonymousDelegate onOk)
      : this(message, onOk, (AnonymousDelegate) null)
    {
    }

    public AskBox(string message, AnonymousDelegate onOk, AnonymousDelegate onCancel)
      : this(message, "%$bgo.common.ok%", "%$bgo.common.cancel%", onOk, onCancel)
    {
    }

    public AskBox(string message, string ok, string cancel, AnonymousDelegate onOk)
      : this(message, ok, cancel, onOk, (AnonymousDelegate) null)
    {
    }

    public AskBox(string message, string ok, string cancel, AnonymousDelegate onOk, AnonymousDelegate onCancel)
    {
      this.CloseButton.IsRendered = false;
      this.Align = Align.MiddleCenter;
      this.OnOk = onOk;
      this.OnCancel = onCancel;
      this.AddChild((GuiElementBase) this.textName, Align.UpCenter, new Vector2(0.0f, 20f));
      this.textName.Alignment = TextAnchor.UpperCenter;
      this.textName.WordWrap = false;
      this.okButton.Text = ok;
      this.okButton.Font = Options.FontBGM_BT;
      this.okButton.FontSize = 16;
      this.okButton.Pressed = new AnonymousDelegate(this.Ok);
      this.AddChild((GuiElementBase) this.okButton, Align.DownCenter);
      this.cancelButton.Text = cancel;
      this.cancelButton.Font = Options.FontBGM_BT;
      this.cancelButton.FontSize = 16;
      this.cancelButton.Pressed = new AnonymousDelegate(this.Cancel);
      this.AddChild((GuiElementBase) this.cancelButton, Align.DownCenter);
      this.SetText(message);
    }

    public AskBox(string message, float time)
      : this(message, time, (AnonymousDelegate) null)
    {
    }

    public AskBox(string message, float time, AnonymousDelegate onOk)
      : this(message, time, onOk, (AnonymousDelegate) null)
    {
    }

    public AskBox(string message, float time, AnonymousDelegate onOk, AnonymousDelegate onCancel)
      : this(message, "%$bgo.common.ok%", "%$bgo.common.cancel%", time, onOk, onCancel)
    {
    }

    public AskBox(string message, string ok, string cancel, float time, AnonymousDelegate onOk)
      : this(message, ok, cancel, time, onOk, (AnonymousDelegate) null)
    {
    }

    public AskBox(string message, string ok, string cancel, float time, AnonymousDelegate onOk, AnonymousDelegate onCancel)
      : this(message, ok, cancel, onOk, onCancel)
    {
      this.endTime = Time.time + time;
      this.AddChild((GuiElementBase) this.timer, Align.DownCenter);
      this.AddChild((GuiElementBase) new Timer(0.3f));
      this.timer.PositionY = (float) ((double) this.okButton.PositionY - (double) this.okButton.SizeY - 15.0);
      this.SizeY += 20f;
      this.PeriodicUpdate();
    }

    [Gui2Editor]
    private void SetText(string value)
    {
      this.textName.Text = value;
      this.textName.WordWrap = false;
      GuiLabel guiLabel;
      double num;
      for (; (double) this.textName.SizeX > (double) this.textName.SizeY * 20.0 || (double) this.textName.SizeX > 700.0; guiLabel.SizeY = (float) num)
      {
        this.textName.WordWrap = true;
        this.textName.SizeX /= 1.2f;
        guiLabel = this.textName;
        num = (double) guiLabel.SizeY * 1.20000004768372;
      }
      this.Size = new Vector2(this.textName.SizeX + 40f, (float) ((double) this.textName.SizeY + 40.0 + 70.0));
      if ((double) this.SizeX < 300.0)
        this.SizeX = 300f;
      this.okButton.Size = new Vector2((double) this.SizeX <= 300.0 ? 100f : 120f, 40f);
      this.okButton.Position = new Vector2((float) (-(double) this.SizeX / 4.0 * 0.800000011920929), -20f);
      this.cancelButton.Size = this.okButton.Size;
      this.cancelButton.Position = new Vector2(-this.okButton.PositionX, this.okButton.PositionY);
      this.ForceReposition();
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      if ((double) this.endTime <= 0.0)
        return;
      this.timer.Text = Tools.FormatTime(this.endTime - Time.time);
      if ((double) Time.time <= (double) this.endTime)
        return;
      this.Cancel();
    }

    private void Ok()
    {
      this.Close();
      if (this.OnOk == null)
        return;
      this.OnOk();
    }

    public void Cancel()
    {
      this.Close();
      if (this.OnCancel == null)
        return;
      this.OnCancel();
    }

    public override void Reposition()
    {
      base.Reposition();
      if (this.Parent == null)
        return;
      this.PositionY = (float) (-(double) this.Parent.SizeY / 6.0);
    }

    [Gui2Editor]
    public static void ShowBox(string message)
    {
      new AskBox(message).Show();
    }

    [Gui2Editor]
    public static void ShowBox(string message, float timeout)
    {
      new AskBox(message, timeout).Show();
    }

    public void Show()
    {
      this.Show((GuiPanel) null);
    }

    public void Show(GuiPanel parent)
    {
      if (parent == null)
        Game.RegisterDialog((IGUIPanel) this, true);
      else
        parent.ShowModalDialog((GuiModalDialog) this);
    }

    protected override void Close()
    {
      if (this.Parent != null)
        (this.Parent as GuiPanel).CloseModalDialog((GuiModalDialog) this);
      else
        Game.UnregisterDialog((IGUIPanel) this);
    }
  }
}
