﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiLabel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("")]
  public class GuiLabel : GuiElementBase, IDrawable
  {
    private static readonly GUIContent tempContent = new GUIContent();
    private GUIStyle style = new GUIStyle();
    private bool autoSize = true;
    [HideInInspector]
    private float inactiveDefaultAlpha = 0.25f;
    [HideInInspector]
    private bool isActive = true;
    [Gui2Editor]
    public Color? OverlayColor;
    private string text;
    [HideInInspector]
    private string textParsed;
    private Color normal;
    private Color over;
    private Color inactiveColor;
    private bool wordWrap;

    public bool IsActive
    {
      get
      {
        return this.isActive;
      }
      set
      {
        if (this.isActive == value)
          return;
        this.isActive = value;
      }
    }

    [Gui2Editor]
    public string Text
    {
      get
      {
        return this.text;
      }
      set
      {
        if (this.text == value)
          return;
        this.text = value;
        this.TextParsed = Tools.ParseMessage(value);
      }
    }

    [Gui2Editor]
    public string TextParsed
    {
      get
      {
        return this.textParsed;
      }
      set
      {
        if (this.textParsed == value)
          return;
        this.textParsed = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public Font Font
    {
      get
      {
        return this.style.font;
      }
      set
      {
        this.style.font = value;
        this.ForceReposition();
      }
    }

    public int FontSize
    {
      get
      {
        return this.style.fontSize;
      }
      set
      {
        this.style.fontSize = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public Color NormalColor
    {
      get
      {
        return this.normal;
      }
      set
      {
        this.normal = value;
        if (this.IsMouseOver)
          return;
        this.style.normal.textColor = value;
      }
    }

    [Gui2Editor]
    public Color OverColor
    {
      get
      {
        return this.over;
      }
      set
      {
        this.over = value;
        if (!this.IsMouseOver)
          return;
        this.style.normal.textColor = value;
      }
    }

    public Color InactiveColor
    {
      get
      {
        return this.inactiveColor;
      }
      set
      {
        this.inactiveColor = value;
      }
    }

    public Color AllColor
    {
      set
      {
        this.normal = value;
        this.over = value;
        this.inactiveColor = value;
        this.style.normal.textColor = value;
      }
    }

    [Gui2Editor]
    public bool WordWrap
    {
      get
      {
        return this.style.wordWrap;
      }
      set
      {
        this.style.wordWrap = this.wordWrap = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public bool AutoSize
    {
      get
      {
        return this.autoSize;
      }
      set
      {
        this.autoSize = value;
        if (!value)
          return;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public TextAnchor Alignment
    {
      get
      {
        return this.style.alignment;
      }
      set
      {
        this.style.alignment = value;
        this.ForceReposition();
      }
    }

    public GUIStyle Style
    {
      get
      {
        return this.style;
      }
      set
      {
        this.style = value;
      }
    }

    public GuiLabel()
      : this("Label")
    {
    }

    public GuiLabel(Font font)
      : this(string.Empty, font)
    {
    }

    public GuiLabel(Font font, int fontSize)
      : this(string.Empty, font, fontSize)
    {
    }

    public GuiLabel(Font font, bool wordWrap)
      : this(string.Empty, font)
    {
      this.WordWrap = wordWrap;
    }

    public GuiLabel(Font font, int fontSize, bool wordWrap)
      : this(string.Empty, font, fontSize)
    {
      this.WordWrap = wordWrap;
    }

    public GuiLabel(string text)
      : this(text, Options.FontBGM_BT, 11)
    {
    }

    public GuiLabel(string text, Font font)
      : this(text, Options.NormalColor, Options.NormalColor, new Color(), font)
    {
    }

    public GuiLabel(string text, Font font, int fontSize)
      : this(text, Options.NormalColor, Options.NormalColor, new Color(), font)
    {
      this.FontSize = fontSize;
    }

    public GuiLabel(Color normalColor, Font font)
      : this(string.Empty, normalColor, font)
    {
    }

    public GuiLabel(Color normalColor, Font font, int fontSize)
      : this(string.Empty, normalColor, font)
    {
      this.FontSize = fontSize;
    }

    public GuiLabel(string text, Color normalColor, Font font)
      : this(text, normalColor, normalColor, new Color(), font)
    {
    }

    public GuiLabel(string text, Color normalColor, Color inactiveColor, Font font)
      : this(text, normalColor, normalColor, inactiveColor, font)
    {
    }

    public GuiLabel(string text, Color normalColor, Color overColor, Color inactiveColor, Font font)
    {
      this.Text = text;
      this.style.font = font;
      this.style.normal.textColor = normalColor;
      this.style.wordWrap = false;
      this.wordWrap = false;
      this.normal = normalColor;
      this.over = overColor;
      if (inactiveColor == new Color())
        inactiveColor = new Color(normalColor.r, normalColor.g, normalColor.b, this.inactiveDefaultAlpha);
      this.inactiveColor = inactiveColor;
      this.RecalculateTextSize();
    }

    public GuiLabel(GuiLabel copy)
      : base((GuiElementBase) copy)
    {
      this.text = copy.text;
      this.textParsed = copy.textParsed;
      this.normal = copy.normal;
      this.over = copy.over;
      this.inactiveColor = copy.inactiveColor;
      this.style = new GUIStyle(copy.style);
      this.autoSize = copy.autoSize;
      this.wordWrap = copy.wordWrap;
    }

    public GuiLabel(JElementBase json)
      : base(json)
    {
      JLabel jlabel = json as JLabel;
      this.Text = jlabel.text;
      this.style = new GUIStyle();
      this.Font = (Font) ResourceLoader.Load(jlabel.fontPath);
      this.FontSize = jlabel.fontSize;
      this.Alignment = jlabel.textAlignment;
      this.WordWrap = jlabel.useWordWrap;
      this.autoSize = jlabel.useAutoSize;
      this.NormalColor = jlabel.normalColor;
      this.OverColor = jlabel.overColor;
      this.InactiveColor = jlabel.inactiveColor;
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiLabel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JLabel((GuiElementBase) this);
    }

    public override void LocalizeElement()
    {
      base.LocalizeElement();
      if (this.text == null)
        return;
      this.textParsed = Tools.ParseMessage(this.text);
    }

    public override void EditorUpdate()
    {
      if (this.WordWrap != this.wordWrap)
        this.WordWrap = this.wordWrap;
      this.style.normal.textColor = !this.isActive ? this.InactiveColor : this.NormalColor;
      this.TextParsed = Tools.ParseMessage(this.text);
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
      GuiElementBase.DrawElement.BeginStencil(this.Rect, true, true);
      GuiElementBase.DrawElement.DrawLabel(this.textParsed, this.Rect, this.style, this.OverlayColor);
      GuiElementBase.DrawElement.EndStencil();
    }

    public override void OnShow()
    {
      base.OnShow();
      this.style.normal.textColor = this.normal;
    }

    private void RecalculateTextSize()
    {
      if (!this.autoSize)
        return;
      this.Size = this.GetTextSize();
    }

    public Vector2 GetTextSize()
    {
      GuiLabel.tempContent.text = this.textParsed;
      Vector2 vector2;
      if (this.style.wordWrap)
      {
        vector2.x = (float) (int) this.SizeX;
        vector2.y = (float) (int) this.style.CalcHeight(GuiLabel.tempContent, this.SizeX);
      }
      else
        vector2 = this.style.CalcSize(GuiLabel.tempContent);
      return vector2;
    }

    public override void Reposition()
    {
      base.Reposition();
      this.RecalculateTextSize();
    }

    public override void Update()
    {
      base.Update();
      if (this.isActive)
        this.style.normal.textColor = this.NormalColor;
      else
        this.style.normal.textColor = this.InactiveColor;
    }

    public override void Draw()
    {
      base.Draw();
      GuiElementBase.DrawElement.DrawLabel(this.textParsed, this.Rect, this.style, this.OverlayColor);
    }

    public override void MouseEnter(float2 position, float2 previousPosition)
    {
      base.MouseEnter(position, previousPosition);
      this.style.normal.textColor = this.over;
    }

    public override void MouseLeave(float2 position, float2 previousPosition)
    {
      base.MouseLeave(position, previousPosition);
      this.style.normal.textColor = this.normal;
    }

    public override string ToString()
    {
      return "GuiLabel: " + this.Name + "\nText: " + this.textParsed + "\nWordWrap == " + (object) this.wordWrap + "\nAutoSize == " + (object) this.autoSize;
    }
  }
}
