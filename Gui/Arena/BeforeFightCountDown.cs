﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.BeforeFightCountDown
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace Gui.Arena
{
  public class BeforeFightCountDown : GuiPanel
  {
    private GuiLabel arenaTypeLabel = new GuiLabel("%$bgo.arena.prepare%", Gui.Options.FontBGM_BT, 40);
    private GuiLabel timeLabel = new GuiLabel("0", Gui.Options.FontDS_DIGIB, 40);

    public BeforeFightCountDown()
    {
      this.MouseTransparent = true;
      this.Align = Align.MiddleCenter;
      this.Position = new Vector2(0.0f, -110f);
      this.SizeY = 110f;
      this.arenaTypeLabel.AllColor = Tools.Color(this.arenaTypeLabel.NormalColor, 100);
      this.AddChild((GuiElementBase) this.arenaTypeLabel, Align.UpCenter);
      this.timeLabel.AllColor = Tools.Color(this.timeLabel.NormalColor, 100);
      this.AddChild((GuiElementBase) this.timeLabel, Align.DownCenter);
      this.AddChild((GuiElementBase) new Gui.Timer(0.3f));
    }

    public static void Start()
    {
      BeforeFightCountDown beforeFightCountDown = Game.GUIManager.Find<BeforeFightCountDown>();
      if (beforeFightCountDown == null)
      {
        beforeFightCountDown = new BeforeFightCountDown();
        Game.RegisterDialog((IGUIPanel) beforeFightCountDown, true);
      }
      beforeFightCountDown.PeriodicUpdate();
    }

    public static void Stop()
    {
      BeforeFightCountDown beforeFightCountDown = Game.GUIManager.Find<BeforeFightCountDown>();
      if (beforeFightCountDown == null)
        return;
      Game.UnregisterDialog((IGUIPanel) beforeFightCountDown);
    }

    public static void StopAndStartArenaCountdown()
    {
      BeforeFightCountDown.Stop();
      ArenaManager.StartArenaCountdown();
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      TimeSpan time = Game.Me.Arena.TimeBegin - Game.TimeSync.ServerDateTime;
      this.timeLabel.Text = Tools.FormatTime(time);
      if (time.TotalMilliseconds >= 0.0)
        return;
      BeforeFightCountDown.StopAndStartArenaCountdown();
    }
  }
}
