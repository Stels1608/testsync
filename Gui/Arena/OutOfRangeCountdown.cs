﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.OutOfRangeCountdown
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace Gui.Arena
{
  public class OutOfRangeCountdown : GuiPanel
  {
    protected readonly GuiLabel arenaTypeLabel = new GuiLabel(Gui.Options.FontBGM_BT, 16);
    protected readonly GuiLabel timeLabel = new GuiLabel("0", Gui.Options.FontDS_DIGIB, 18);
    protected readonly GuiHealthbar bar = new GuiHealthbar();
    public DateTime beginning;
    public DateTime endTime;

    public OutOfRangeCountdown()
    {
      this.MouseTransparent = true;
      this.Align = Align.UpCenter;
      this.Position = new Vector2(0.0f, 150f);
      this.SizeY = 85f;
      this.arenaTypeLabel.AllColor = Tools.Color(Gui.Options.NormalColor, 128);
      this.arenaTypeLabel.Alignment = TextAnchor.MiddleCenter;
      this.AddChild((GuiElementBase) this.arenaTypeLabel, Align.UpCenter);
      this.timeLabel.AllColor = Tools.Color(Gui.Options.NormalColor, 128);
      this.AddChild((GuiElementBase) this.timeLabel, Align.DownCenter, new Vector2(0.0f, -20f));
      this.bar.Size = new Vector2(250f, 15f);
      this.bar.ColorInner = Tools.Color((int) byte.MaxValue, (int) byte.MaxValue, (int) byte.MaxValue, 40);
      this.bar.ColorBorder = Tools.Color(0, 0, 0, 0);
      this.bar.Color = Tools.Color(this.bar.Color, 140);
      this.AddChild((GuiElementBase) this.bar, Align.DownCenter);
      this.AddChild((GuiElementBase) new Gui.Timer(0.3f));
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      TimeSpan time = this.endTime - Game.TimeSync.ServerDateTime;
      this.bar.Progress = (float) (time.TotalMilliseconds / (this.endTime - this.beginning).TotalMilliseconds);
      this.timeLabel.Text = Tools.FormatTime(time);
      this.arenaTypeLabel.Text = BsgoLocalization.Get("%$bgo.arena.out_of_arena%", (object) (int) time.TotalSeconds);
      if (time.Ticks >= 0L)
        return;
      this.Find<Gui.Timer>().IsRendered = false;
    }

    public static void Show(DateTime time)
    {
      OutOfRangeCountdown ofRangeCountdown = Game.GUIManager.FindStraight<OutOfRangeCountdown>();
      if (ofRangeCountdown == null)
      {
        ofRangeCountdown = new OutOfRangeCountdown();
        Game.GUIManager.AddPanel((IGUIPanel) ofRangeCountdown, 7);
        Game.InputDispatcher.AddListener((InputListener) ofRangeCountdown);
      }
      ofRangeCountdown.beginning = Game.TimeSync.ServerDateTime;
      ofRangeCountdown.endTime = time;
      ofRangeCountdown.PeriodicUpdate();
    }

    public static void Hide()
    {
      OutOfRangeCountdown straight = Game.GUIManager.FindStraight<OutOfRangeCountdown>();
      if (straight == null)
        return;
      Game.UnregisterDialog((IGUIPanel) straight);
    }
  }
}
