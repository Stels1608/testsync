﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.Window
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Core;
using System;
using System.IO;
using UnityEngine;

namespace Gui.Arena
{
  public class Window : GuiDialog
  {
    private GuiPanelVerticalScroll scroll = new GuiPanelVerticalScroll();

    public override bool IsBigWindow
    {
      get
      {
        return true;
      }
    }

    public Window()
    {
      this.Size = new Vector2(925f, 500f);
      this.Align = Align.MiddleCenter;
      this.scroll.Size = new Vector2(387f, 497f);
      this.scroll.AutoSizeChildren = true;
      this.scroll.RenderDelimiters = true;
      this.scroll.OnSelectionChanged = (System.Action<ISelectable>) (entry =>
      {
        this.RemoveChild<ArenaDescription>();
        this.AddChild((GuiElementBase) (entry as ModeBase).Description);
        this.PeriodicUpdate();
      });
      this.AddChild((GuiElementBase) this.scroll);
      this.AddChild((GuiElementBase) new Gui.Timer(0.3f));
      this.scroll.AddChild((GuiElementBase) new Mode1vs1());
      this.scroll.AddChild((GuiElementBase) new Mode3vs3Mixed());
      this.scroll.Selected = this.scroll.Children[0] as ISelectable;
      this.OnClose = (AnonymousDelegate) (() => this.IsRendered = false);
    }

    [Gui2Editor]
    public static void Show()
    {
      Game.RegisterDialog((IGUIPanel) new Window(), true);
    }

    [Gui2Editor]
    public static void ShowCountDown()
    {
      ArenaManager.StartWaitingForOpponentTimer();
    }

    [Gui2Editor]
    public static void ShowInviteConfirmation()
    {
      InviteConfirmation.Show();
    }

    [Gui2Editor]
    public static void ShowArenaCountdown()
    {
      ArenaManager.StartArenaCountdown();
    }

    [Gui2Editor]
    public static void ShowBeforeFightCountDown()
    {
      Game.RegisterDialog((IGUIPanel) new BeforeFightCountDown(), true);
    }

    [Gui2Editor]
    public static void ArenaOutOfRange()
    {
      MemoryStream stream = new MemoryStream();
      new BinaryWriter((Stream) stream).Flush();
      stream.Position = 0L;
      ArenaProtocol.GetProtocol().ParseMessage((ushort) 14, new BgoProtocolReader(stream));
    }

    [Gui2Editor]
    public static void ArenaOuterRangeOk()
    {
      MemoryStream stream = new MemoryStream();
      new BinaryWriter((Stream) stream).Flush();
      stream.Position = 0L;
      ArenaProtocol.GetProtocol().ParseMessage((ushort) 15, new BgoProtocolReader(stream));
    }

    [Gui2Editor]
    public static void ArenaCapturePoint(bool me)
    {
      MemoryStream stream = new MemoryStream();
      BinaryWriter binaryWriter = new BinaryWriter((Stream) stream);
      binaryWriter.Write(!me ? 1U : SpaceLevel.GetLevel().PlayerShip.ObjectID);
      binaryWriter.Write((uint) (Game.TimeSync.ServerDateTime.AddMinutes(1.0) - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);
      binaryWriter.Flush();
      stream.Position = 0L;
      ArenaProtocol.GetProtocol().ParseMessage((ushort) 16, new BgoProtocolReader(stream));
    }

    [Gui2Editor]
    public static void ArenaOutOfCapturePoint()
    {
      MemoryStream stream = new MemoryStream();
      BinaryWriter binaryWriter = new BinaryWriter((Stream) stream);
      binaryWriter.Write(SpaceLevel.GetLevel().PlayerShip.ObjectID);
      binaryWriter.Flush();
      stream.Position = 0L;
      ArenaProtocol.GetProtocol().ParseMessage((ushort) 17, new BgoProtocolReader(stream));
    }

    [Gui2Editor]
    public static void WaitingForMatchMarker()
    {
      MemoryStream stream = new MemoryStream();
      BinaryWriter binaryWriter = new BinaryWriter((Stream) stream);
      binaryWriter.Write(SpaceLevel.GetLevel().PlayerShip.ObjectID);
      binaryWriter.Flush();
      stream.Position = 0L;
      ArenaProtocol.GetProtocol().ParseMessage((ushort) 17, new BgoProtocolReader(stream));
    }

    [Gui2Editor]
    public static void Aaa(string message)
    {
      new InfoBox(message).Show();
    }
  }
}
