﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.WaitingForOpponentTimer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Arena
{
  public class WaitingForOpponentTimer : GuiPanel
  {
    private GuiLabel arenaTypeLabel = new GuiLabel("%$bgo.arena.waiting_for_opponent%", Gui.Options.FontBGM_BT, 16);
    private GuiLabel timeLabel = new GuiLabel("0", Gui.Options.FontDS_DIGIB, 18);

    public WaitingForOpponentTimer()
    {
      this.MouseTransparent = true;
      this.Align = Align.UpCenter;
      this.Position = new Vector2(30f, 150f);
      this.AddChild((GuiElementBase) this.arenaTypeLabel, Align.UpCenter);
      this.AddChild((GuiElementBase) this.timeLabel, Align.DownCenter);
      this.AddChild((GuiElementBase) new Gui.Timer(1f));
      this.SizeY = 45f;
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      this.timeLabel.Text = Tools.FormatTime(Game.TimeSync.ServerDateTime - Game.Me.Arena.InviteBegin);
    }
  }
}
