﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.InviteConfirmation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace Gui.Arena
{
  public class InviteConfirmation : GuiDialog
  {
    private GuiLabel inCombat = new GuiLabel("%$bgo.arena.invite_exit_combat_mode%", Gui.Options.FontBGM_BT, 14);
    private GuiLabel timeLabel = new GuiLabel("0", Gui.Options.FontDS_DIGIB, 18);
    private GuiButton ok = new GuiButton("%$bgo.arena.invite_ok%");

    public InviteConfirmation()
    {
      this.Size = new Vector2(300f, 200f);
      this.Align = Align.MiddleCenter;
      this.PositionY = -40f;
      this.AddChild((GuiElementBase) new GuiLabel("%$bgo.arena.invited%", Gui.Options.FontBGM_BT, 16), Align.UpCenter, new Vector2(0.0f, 40f));
      this.inCombat.WordWrap = true;
      this.inCombat.Alignment = TextAnchor.MiddleCenter;
      this.inCombat.SizeX = this.SizeX - 20f;
      this.AddChild((GuiElementBase) this.inCombat, Align.UpCenter, new Vector2(0.0f, 60f));
      this.AddChild((GuiElementBase) this.timeLabel, Align.UpCenter, new Vector2(0.0f, 120f));
      this.ok.Size = new Vector2(100f, 30f);
      this.AddChild((GuiElementBase) this.ok, Align.DownCenter, new Vector2(-60f, -20f));
      GuiButton guiButton = new GuiButton("%$bgo.arena.invite_cancel%");
      guiButton.Size = new Vector2(100f, 30f);
      this.AddChild((GuiElementBase) guiButton, Align.DownCenter, new Vector2(60f, -20f));
      this.OnClose = (AnonymousDelegate) (() =>
      {
        ArenaProtocol.GetProtocol().RequestArenaInviteCancel();
        Game.Me.Arena.State = ArenaState.None;
        ArenaManager.Changed();
        Game.UnregisterDialog((IGUIPanel) this);
      });
      guiButton.Pressed = this.OnClose;
      this.AddChild((GuiElementBase) new Gui.Timer(0.3f));
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      bool inCombat = Game.Me.Stats.InCombat;
      this.inCombat.IsRendered = inCombat;
      this.ok.Pressed = !inCombat ? (AnonymousDelegate) (() =>
      {
        ArenaProtocol.GetProtocol().RequestArenaInviteOk();
        Game.Me.Arena.State = ArenaState.WaitingForOpponent;
        ArenaManager.Changed();
        Game.UnregisterDialog((IGUIPanel) this);
      }) : (AnonymousDelegate) null;
      TimeSpan time = Game.Me.Arena.InviteTimeout - Game.TimeSync.ServerDateTime;
      this.timeLabel.Text = Tools.FormatTime(time);
      if (time.Ticks >= 0L)
        return;
      Game.UnregisterDialog((IGUIPanel) this);
    }

    [Gui2Editor]
    public static void Show()
    {
      InviteConfirmation inviteConfirmation = Game.GUIManager.Find<InviteConfirmation>();
      if (inviteConfirmation == null)
      {
        inviteConfirmation = new InviteConfirmation();
        Game.RegisterDialog((IGUIPanel) inviteConfirmation, true);
      }
      inviteConfirmation.PeriodicUpdate();
    }

    [Gui2Editor]
    public static void Stop()
    {
      InviteConfirmation inviteConfirmation = Game.GUIManager.Find<InviteConfirmation>();
      if (inviteConfirmation == null)
        return;
      Game.UnregisterDialog((IGUIPanel) inviteConfirmation);
    }
  }
}
