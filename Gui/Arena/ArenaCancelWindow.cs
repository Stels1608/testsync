﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.ArenaCancelWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Arena
{
  public class ArenaCancelWindow : GuiDialog
  {
    private GuiButton ok = new GuiButton("%$bgo.arena.invite_ok%");

    public ArenaCancelWindow()
    {
      this.Size = new Vector2(300f, 200f);
      this.Align = Align.MiddleCenter;
      this.PositionY = -40f;
      GuiLabel guiLabel = new GuiLabel("%$bgo.arena.cancelled%", Gui.Options.FontBGM_BT, 16);
      guiLabel.SizeX = 290f;
      guiLabel.Alignment = TextAnchor.MiddleCenter;
      guiLabel.WordWrap = true;
      this.AddChild((GuiElementBase) guiLabel, new Vector2(0.0f, 30f));
      this.ok.Size = new Vector2(100f, 30f);
      this.AddChild((GuiElementBase) this.ok, Align.DownCenter, new Vector2(0.0f, -20f));
      this.OnClose = (AnonymousDelegate) (() => Game.UnregisterDialog((IGUIPanel) this));
      this.ok.Pressed = this.OnClose;
    }

    [Gui2Editor]
    public static void Show()
    {
      if (Game.GUIManager.Find<ArenaCancelWindow>() != null)
        return;
      Game.RegisterDialog((IGUIPanel) new ArenaCancelWindow(), true);
    }

    [Gui2Editor]
    public static void Stop()
    {
      ArenaCancelWindow arenaCancelWindow = Game.GUIManager.Find<ArenaCancelWindow>();
      if (arenaCancelWindow == null)
        return;
      Game.UnregisterDialog((IGUIPanel) arenaCancelWindow);
    }
  }
}
