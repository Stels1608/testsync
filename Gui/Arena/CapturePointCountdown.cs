﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.CapturePointCountdown
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Arena
{
  public class CapturePointCountdown : OutOfRangeCountdown
  {
    private bool isItMe;

    public CapturePointCountdown()
    {
      this.PositionY = 240f;
      this.handleMouseInput = false;
    }

    public void SetMode(bool isItMe)
    {
      this.isItMe = isItMe;
      Color color = !isItMe ? Gui.Options.NegativeColor : Gui.Options.PositiveColor;
      this.arenaTypeLabel.AllColor = color;
      this.timeLabel.AllColor = color;
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      this.bar.Progress = 1f - this.bar.Progress;
      int num = (int) (this.endTime - Game.TimeSync.ServerDateTime).TotalSeconds;
      if (this.isItMe)
        this.arenaTypeLabel.Text = BsgoLocalization.Get("%$bgo.arena.action_point%", (object) num);
      else
        this.arenaTypeLabel.Text = BsgoLocalization.Get("%$bgo.arena.action_point_opponent%", (object) num);
    }
  }
}
