﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.ModeBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Core;
using UnityEngine;

namespace Gui.Arena
{
  public class ModeBase : GuiPanel, ISelectable
  {
    public ArenaDescription Description;

    public ModeBase(string text)
    {
      Gui2Loader.Load((GuiElementBase) this, "GUI/Arena/entry");
      this.Find<GuiLabel>("name").Text = text;
      this.Description = new ArenaDescription();
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      this.FindParent<ISelectableContainer>().Selected = (ISelectable) this;
      return base.MouseUp(position, key);
    }

    public void OnSelected()
    {
    }

    public void OnDeselected()
    {
    }
  }
}
