﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.Mode1vs1
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Gui.Arena
{
  public class Mode1vs1 : ModeBase
  {
    public Mode1vs1()
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      Mode1vs1.\u003CMode1vs1\u003Ec__AnonStorey70 mode1vs1CAnonStorey70 = new Mode1vs1.\u003CMode1vs1\u003Ec__AnonStorey70();
      // ISSUE: explicit constructor call
      base.\u002Ector("%$bgo.arena.1vs1%");
      // ISSUE: reference to a compiler-generated field
      mode1vs1CAnonStorey70.\u003C\u003Ef__this = this;
      Gui2Loader.Load((GuiElementBase) this.Description, "GUI/Arena/1vs1");
      // ISSUE: reference to a compiler-generated field
      mode1vs1CAnonStorey70.checkinDelegate = (AnonymousDelegate) (() => ArenaProtocol.GetProtocol().RequestArenaCheckIn(ArenaType.Arena1vs1));
      this.Description.Find<GuiButton>("cancelCheckIn").Pressed = (AnonymousDelegate) (() => ArenaProtocol.GetProtocol().RequestArenaCancelCheckIn());
      // ISSUE: reference to a compiler-generated method
      this.Description.PeriodicUpdateDelegate = new AnonymousDelegate(mode1vs1CAnonStorey70.\u003C\u003Em__52);
    }
  }
}
