﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.ArenaManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

namespace Gui.Arena
{
  public class ArenaManager
  {
    public static void Changed()
    {
      Game.GUIManager.SystemButtons.UpdateArenaButton();
      if (Game.Me.Arena.State == ArenaState.Invited)
        InviteConfirmation.Show();
      else
        InviteConfirmation.Stop();
      if (Game.Me.Arena.State == ArenaState.WaitingForOpponent)
        ArenaManager.StartWaitingForOpponentTimer();
      else
        ArenaManager.StopWaitingForOpponentTimer();
    }

    public static void StartCapturePointCountdown(DateTime time, bool isItMe)
    {
      CapturePointCountdown capturePointCountdown = Game.GUIManager.FindStraight<CapturePointCountdown>();
      if (capturePointCountdown == null)
      {
        capturePointCountdown = new CapturePointCountdown();
        Game.GUIManager.AddPanel((IGUIPanel) capturePointCountdown, 7);
        Game.InputDispatcher.AddListener((InputListener) capturePointCountdown);
      }
      capturePointCountdown.beginning = Game.TimeSync.ServerDateTime;
      capturePointCountdown.endTime = time;
      capturePointCountdown.SetMode(isItMe);
      capturePointCountdown.PeriodicUpdate();
    }

    public static void StopCapturePointCountdown()
    {
      CapturePointCountdown straight = Game.GUIManager.FindStraight<CapturePointCountdown>();
      if (straight == null)
        return;
      Game.UnregisterDialog((IGUIPanel) straight);
    }

    public static void StartWaitingForOpponentTimer()
    {
      WaitingForOpponentTimer forOpponentTimer = Game.GUIManager.FindStraight<WaitingForOpponentTimer>();
      if (forOpponentTimer == null)
      {
        forOpponentTimer = new WaitingForOpponentTimer();
        Game.GUIManager.AddPanel((IGUIPanel) forOpponentTimer, 0);
        Game.InputDispatcher.AddListener((InputListener) forOpponentTimer);
      }
      forOpponentTimer.PeriodicUpdate();
      Game.DelayedActions.Awake();
    }

    public static void StopWaitingForOpponentTimer()
    {
      WaitingForOpponentTimer straight = Game.GUIManager.FindStraight<WaitingForOpponentTimer>();
      if (straight != null)
        Game.UnregisterDialog((IGUIPanel) straight);
      Game.DelayedActions.Awake();
    }

    public static void StartArenaCountdown()
    {
      ArenaCountdown arenaCountdown = Game.GUIManager.FindStraight<ArenaCountdown>();
      if (arenaCountdown == null)
      {
        arenaCountdown = new ArenaCountdown();
        Game.GUIManager.AddPanel((IGUIPanel) arenaCountdown, 0);
        Game.InputDispatcher.AddListener((InputListener) arenaCountdown);
      }
      arenaCountdown.PeriodicUpdate();
    }
  }
}
