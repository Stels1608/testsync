﻿// Decompiled with JetBrains decompiler
// Type: Gui.Arena.Mode3vs3Mixed
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Gui.Arena
{
  public class Mode3vs3Mixed : ModeBase
  {
    public Mode3vs3Mixed()
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      Mode3vs3Mixed.\u003CMode3vs3Mixed\u003Ec__AnonStorey72 mixedCAnonStorey72 = new Mode3vs3Mixed.\u003CMode3vs3Mixed\u003Ec__AnonStorey72();
      // ISSUE: explicit constructor call
      base.\u002Ector("%$bgo.arena.3vs3mixed%");
      // ISSUE: reference to a compiler-generated field
      mixedCAnonStorey72.\u003C\u003Ef__this = this;
      Gui2Loader.Load((GuiElementBase) this.Description, "GUI/Arena/3vs3Mixed");
      // ISSUE: reference to a compiler-generated field
      mixedCAnonStorey72.checkinSquadronDelegate = (AnonymousDelegate) (() =>
      {
        if (!Game.Me.Party.ArenaSuitable)
          new InfoBox("%$bgo.arena.error_party_3vs3%").Show();
        else
          ArenaProtocol.GetProtocol().RequestArenaCheckIn(ArenaType.Arena3vs3);
      });
      // ISSUE: reference to a compiler-generated field
      mixedCAnonStorey72.checkinDelegate = (AnonymousDelegate) (() => ArenaProtocol.GetProtocol().RequestArenaCheckIn(ArenaType.Arena3vs3Mixed));
      this.Description.Find<GuiButton>("cancelCheckIn").Pressed = (AnonymousDelegate) (() => ArenaProtocol.GetProtocol().RequestArenaCancelCheckIn());
      // ISSUE: reference to a compiler-generated method
      this.Description.PeriodicUpdateDelegate = new AnonymousDelegate(mixedCAnonStorey72.\u003C\u003Em__59);
    }
  }
}
