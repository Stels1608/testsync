﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiImage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("")]
  public class GuiImage : GuiElementBase
  {
    [Gui2Editor]
    private GuiElementPadding nineSliceEdge = (GuiElementPadding) new Rect(0.0f, 0.0f, 0.0f, 0.0f);
    private Rect sourceRect = new Rect(0.0f, 0.0f, 1f, 1f);
    [Gui2Editor]
    public Color? OverlayColor;
    private Texture2D texture;
    private AnonymousDelegate UseTextureSize;
    private AnonymousDelegate NormalizeSourceRect;

    public GuiElementPadding NineSliceEdge
    {
      get
      {
        return this.nineSliceEdge;
      }
      set
      {
        this.nineSliceEdge = value;
      }
    }

    [Gui2Editor]
    public Rect SourceRect
    {
      get
      {
        return this.sourceRect;
      }
      set
      {
        this.sourceRect = value;
      }
    }

    [Gui2Editor]
    public Texture2D Texture
    {
      get
      {
        return this.texture;
      }
      set
      {
        if ((Object) value != (Object) null && value.name == "GUI/Editor/editor_default_image")
          Debug.LogError((object) "Trying to set the text to the default texture");
        if ((Object) this.texture == (Object) value)
          return;
        this.texture = value;
        Vector2 vector2 = !((Object) this.texture == (Object) null) ? new Vector2((float) this.texture.width, (float) this.texture.height) : Vector2.zero;
        if (this.Size.Equals((object) vector2))
          return;
        this.Size = vector2;
      }
    }

    public string TexturePath
    {
      set
      {
        this.Texture = value != null ? ResourceLoader.Load<Texture2D>(value) : (Texture2D) null;
      }
    }

    public GuiImage()
      : base(Vector2.zero, Vector2.zero)
    {
      this.UseTextureSize = new AnonymousDelegate(this.SetToTextureSize);
      this.NormalizeSourceRect = new AnonymousDelegate(this.SetSourceRectToNormalize);
    }

    public GuiImage(string texture)
      : this(ResourceLoader.Load<Texture2D>(texture))
    {
    }

    public GuiImage(Texture2D texture)
      : base(Vector2.zero, !((Object) texture == (Object) null) ? new Vector2((float) texture.width, (float) texture.height) : Vector2.zero)
    {
      this.texture = texture;
      this.UseTextureSize = new AnonymousDelegate(this.SetToTextureSize);
      this.NormalizeSourceRect = new AnonymousDelegate(this.SetSourceRectToNormalize);
    }

    public GuiImage(GuiImage copy)
      : base((GuiElementBase) copy)
    {
      this.OverlayColor = copy.OverlayColor;
      this.nineSliceEdge = new GuiElementPadding(copy.nineSliceEdge);
      this.texture = copy.texture;
      this.sourceRect = copy.sourceRect;
      this.UseTextureSize = new AnonymousDelegate(this.SetToTextureSize);
      this.NormalizeSourceRect = new AnonymousDelegate(this.SetSourceRectToNormalize);
    }

    public GuiImage(JElementBase json)
      : base(json)
    {
      JImage jimage = json as JImage;
      if (jimage.padding >= 0)
      {
        float num = (float) jimage.padding;
        this.nineSliceEdge = (GuiElementPadding) new Rect(num, num, num, num);
      }
      else
        this.nineSliceEdge = (GuiElementPadding) jimage.nineSliceEdge;
      this.sourceRect = (Rect) jimage.sourceRect;
      this.UseTextureSize = new AnonymousDelegate(this.SetToTextureSize);
      this.NormalizeSourceRect = new AnonymousDelegate(this.SetSourceRectToNormalize);
      if (jimage.texture != null)
      {
        if (this.Parent is GuiTournamentSectorGreeting)
          Debug.LogError((object) ("Loading: " + jimage.texture));
        Texture2D texture2D = ResourceLoader.Load<Texture2D>(jimage.texture);
        if ((Object) texture2D == (Object) null)
          texture2D = ResourceLoader.Load<Texture2D>("GUI/Editor/editor_default_image");
        this.SetTextureWithoutResize(texture2D);
        this.texture.name = jimage.texture;
      }
      if (jimage.overlayColor != null)
        this.OverlayColor = new Color?((Color) jimage.overlayColor);
      else
        this.OverlayColor = new Color?();
    }

    private void SetToTextureSize()
    {
      this.SizeX = (float) this.texture.width;
      this.SizeY = (float) this.texture.height;
      if (this.WhenElementChanges == null)
        return;
      this.WhenElementChanges();
    }

    private void SetSourceRectToNormalize()
    {
      this.sourceRect.width = this.Size.x / (float) this.texture.width;
      this.sourceRect.height = this.Size.y / (float) this.texture.height;
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiImage(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JImage((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      if (!((Object) this.texture == (Object) null))
        return;
      this.texture = ResourceLoader.Load<Texture2D>("GUI/Editor/editor_default_image");
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
      if (Event.current.type != UnityEngine.EventType.Repaint)
        return;
      this.Draw();
    }

    public override void Draw()
    {
      base.Draw();
      if ((Object) this.texture == (Object) null)
        return;
      GuiElementBase.DrawElement.DrawTexture(this.texture, this.Rect, this.nineSliceEdge, new Rect?(this.sourceRect), this.OverlayColor, false);
    }

    public void SetTextureWithoutResize(Texture2D value)
    {
      if ((Object) this.texture == (Object) value)
        return;
      this.texture = value;
    }
  }
}
