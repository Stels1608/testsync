﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiInputTextBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("")]
  public class GuiInputTextBox : GuiElementBase
  {
    private int m_maxLength = 32;
    private string m_emptyStringMessage = "<Enter Text Here>";
    [HideInInspector]
    private string m_localizedEmptyMessage = string.Empty;
    [HideInInspector]
    private string m_text = string.Empty;
    [HideInInspector]
    private Rect m_textRect = new Rect();
    private float m_textPadding = 5f;
    private GUIStyle m_style = new GUIStyle();
    private bool m_isActive = true;
    [HideInInspector]
    public string m_textboxControlName = string.Empty;
    private TextCharactersAllowed m_textCharactersAllowed = TextCharactersAllowed.Anything;
    [HideInInspector]
    private string m_lastStoredText = string.Empty;
    [HideInInspector]
    public System.Action<string> OnTextChanged;
    [HideInInspector]
    public System.Action OnHitEnter;
    private static int ms_uniqueID;
    private GuiImage m_background;
    private bool focused;

    public string EmptyStringMessage
    {
      get
      {
        return this.m_emptyStringMessage;
      }
      set
      {
        this.m_emptyStringMessage = value;
      }
    }

    public float TextPadding
    {
      get
      {
        return this.m_textPadding;
      }
      set
      {
        this.m_textPadding = value;
      }
    }

    public string Text
    {
      get
      {
        return this.m_text;
      }
      set
      {
        this.m_text = value;
      }
    }

    public Font Font
    {
      get
      {
        return this.m_style.font;
      }
      set
      {
        this.m_style.font = value;
      }
    }

    public TextAnchor Alignment
    {
      get
      {
        return this.m_style.alignment;
      }
      set
      {
        this.m_style.alignment = value;
      }
    }

    public Color TextColor
    {
      get
      {
        return this.m_style.normal.textColor;
      }
      set
      {
        this.m_style.normal.textColor = value;
      }
    }

    public int MaxLength
    {
      get
      {
        return this.m_maxLength;
      }
      set
      {
        this.m_maxLength = value;
      }
    }

    public GuiImage BackgroundImage
    {
      get
      {
        return this.m_background;
      }
      set
      {
        this.m_background = value;
      }
    }

    public bool IsActive
    {
      get
      {
        return this.m_isActive;
      }
      set
      {
        this.m_isActive = value;
      }
    }

    public bool IsFocused
    {
      get
      {
        return this.focused;
      }
    }

    public TextCharactersAllowed TextValuesAllowed
    {
      get
      {
        return this.m_textCharactersAllowed;
      }
      set
      {
        this.m_textCharactersAllowed = value;
      }
    }

    public GuiInputTextBox()
    {
      this.m_style.font = Options.FontEurostileT;
      this.m_style.fontSize = 12;
      this.m_style.normal.textColor = Options.NormalColor;
      this.m_style.wordWrap = false;
      this.m_style.alignment = TextAnchor.MiddleCenter;
      this.Size = new Vector2(200f, 20f);
      this.m_background = new GuiImage("GUI/_NewGui/Buttons/btn_enter_name_idle");
      this.m_background.NineSliceEdge = (GuiElementPadding) new Rect(4f, 4f, 4f, 4f);
      this.m_background.IsFullPanel = true;
      this.m_background.ElementPadding = (GuiElementPadding) new Rect();
      this.m_background.Parent = (SRect) this;
      this.m_textCharactersAllowed &= ~TextCharactersAllowed.ConcurrentSpaces;
      this.SetUniqueName();
    }

    public GuiInputTextBox(GuiInputTextBox copy)
      : base((GuiElementBase) copy)
    {
      this.m_maxLength = copy.m_maxLength;
      this.m_emptyStringMessage = copy.m_emptyStringMessage;
      this.m_style = copy.m_style;
      this.m_textPadding = copy.m_textPadding;
      this.OnTextChanged = copy.OnTextChanged;
      this.m_text = copy.m_text;
      this.m_textCharactersAllowed = copy.m_textCharactersAllowed;
      this.m_background = new GuiImage(copy.m_background);
      this.m_background.Parent = (SRect) this;
      this.IsActive = copy.IsActive;
      this.SetUniqueName();
    }

    public GuiInputTextBox(JInputTextBox json)
      : base((JElementBase) json)
    {
      this.m_maxLength = json.m_maxLength;
      this.m_emptyStringMessage = json.m_emptyStringMessage;
      this.m_textPadding = json.m_textPadding;
      this.m_style = new GUIStyle();
      this.Font = (Font) ResourceLoader.Load(json.m_fontPath);
      this.Alignment = json.m_textAlignment;
      this.TextColor = json.m_textColor;
      this.m_textCharactersAllowed = (TextCharactersAllowed) json.m_textCharactersAllowed;
      this.m_background = new GuiImage((JElementBase) json.m_background);
      this.m_background.Parent = (SRect) this;
      this.SetUniqueName();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiInputTextBox(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JInputTextBox((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      this.m_background.EditorUpdate();
      if (this.IsTypeAllowed(TextCharactersAllowed.UpperCase) || this.IsTypeAllowed(TextCharactersAllowed.LowerCase))
        this.m_textCharactersAllowed |= TextCharactersAllowed.Letters;
      else
        this.m_textCharactersAllowed &= ~TextCharactersAllowed.Letters;
      if (string.IsNullOrEmpty(this.m_emptyStringMessage))
        return;
      this.LocalizeElement();
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
      this.m_background.EditorDraw();
      this.m_textRect = this.Rect;
      this.m_textRect.x += this.m_textPadding;
      this.m_textRect.width -= this.m_textPadding * 2f;
      GuiElementBase.DrawElement.BeginStencil(this.Rect, true, true);
      GuiElementBase.DrawElement.DrawTextArea(this.m_localizedEmptyMessage, this.m_textRect, this.m_maxLength, this.m_style, this.m_textboxControlName);
      GuiElementBase.DrawElement.EndStencil();
    }

    public override void LocalizeElement()
    {
      if ((UnityEngine.Object) Game.Instance != (UnityEngine.Object) null)
      {
        base.LocalizeElement();
        if (Game.Localization.TryGetTranslation(this.m_emptyStringMessage, out this.m_localizedEmptyMessage))
          return;
      }
      this.m_localizedEmptyMessage = this.m_emptyStringMessage;
    }

    private void SetUniqueName()
    {
      this.m_textCharactersAllowed &= ~TextCharactersAllowed.ConcurrentSpaces;
      this.m_textboxControlName = "TextBoxControl_" + GuiInputTextBox.ms_uniqueID.ToString();
      ++GuiInputTextBox.ms_uniqueID;
    }

    public override void OnShow()
    {
      base.OnShow();
      this.focused = false;
    }

    public override void DoCaching()
    {
      base.DoCaching();
      this.m_background.DoCaching();
    }

    public override void Update()
    {
      base.Update();
      this.m_background.Update();
      if (!this.IsFocused && string.IsNullOrEmpty(this.m_text))
        this.LocalizeElement();
      if (this.IsFocused)
        return;
      this.m_lastStoredText = this.m_text;
    }

    public override void Draw()
    {
      base.Draw();
      this.m_background.Draw();
      this.m_textRect = this.Rect;
      this.m_textRect.x += this.m_textPadding;
      this.m_textRect.width -= this.m_textPadding * 2f;
      if (this.IsActive)
      {
        if (this.IsFocused || !string.IsNullOrEmpty(this.m_text))
        {
          string str = this.CleanupText(GuiElementBase.DrawElement.DrawTextArea(this.m_text, this.m_textRect, this.m_maxLength, this.m_style, this.m_textboxControlName));
          bool flag = str != this.m_text;
          this.m_text = str;
          if (flag && this.OnTextChanged != null)
            this.OnTextChanged(this.m_text);
        }
        else
          GuiElementBase.DrawElement.DrawTextArea(this.m_localizedEmptyMessage, this.m_textRect, this.m_maxLength, this.m_style, this.m_textboxControlName);
        if (string.IsNullOrEmpty(this.m_text))
          return;
        this.focused = GUI.GetNameOfFocusedControl() == this.m_textboxControlName;
      }
      else if (string.IsNullOrEmpty(this.m_text))
      {
        GuiElementBase.DrawElement.DrawLabel(this.m_localizedEmptyMessage, this.m_textRect, this.m_style, new Color?());
        this.focused = false;
      }
      else
      {
        GuiElementBase.DrawElement.DrawLabel(this.m_text, this.m_textRect, this.m_style, new Color?());
        this.focused = false;
      }
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      bool flag = this.Contains(position);
      if (flag && key == KeyCode.Mouse0)
      {
        GUI.SetNextControlName(this.m_textboxControlName);
        GUI.FocusControl(this.m_textboxControlName);
        this.focused = true;
      }
      else
        this.focused = false;
      if (flag)
        return base.MouseDown(position, key);
      return false;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      if (this.Contains(position))
        return base.MouseUp(position, key);
      return false;
    }

    public override bool KeyUp(KeyCode key, Action action)
    {
      base.KeyUp(key, action);
      return this.focused;
    }

    public override bool KeyDown(KeyCode key, Action action)
    {
      if (key == KeyCode.Return || key == KeyCode.KeypadEnter)
      {
        if (this.OnHitEnter != null)
          this.OnHitEnter();
      }
      else if (key == KeyCode.Escape)
        this.m_text = this.m_lastStoredText;
      base.KeyDown(key, action);
      return this.focused;
    }

    private string CleanupText(string text)
    {
      if (this.m_textCharactersAllowed == TextCharactersAllowed.Anything)
        return text;
      string str = string.Empty;
      char ch = ' ';
      for (int index = 0; index < text.Length; ++index)
      {
        char c = text[index];
        if (this.CheckCharacter(ref c, (int) ch == 32))
        {
          str = Tools.RemoveNoLatinsButSpaces(str + (object) c);
          ch = c;
        }
      }
      return str;
    }

    private bool CheckCharacter(ref char c, bool isLastCharASpace)
    {
      if ((int) c == 32 && !this.IsTypeAllowed(TextCharactersAllowed.ConcurrentSpaces) && isLastCharASpace)
        return false;
      if (char.IsLetter(c))
      {
        if (!this.IsTypeAllowed(TextCharactersAllowed.Letters))
          return false;
        if (!this.IsTypeAllowed(TextCharactersAllowed.UpperCase))
          c = char.ToLower(c);
        if (!this.IsTypeAllowed(TextCharactersAllowed.LowerCase))
          c = char.ToUpper(c);
        return true;
      }
      if (char.IsDigit(c))
        return this.IsTypeAllowed(TextCharactersAllowed.Numbers);
      if ((int) c != 32)
        return this.IsTypeAllowed(TextCharactersAllowed.Symbols);
      return true;
    }

    private bool IsTypeAllowed(TextCharactersAllowed allowed)
    {
      return (this.m_textCharactersAllowed & allowed) == allowed;
    }
  }
}
