﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiLaunchShopPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.DamageWindow;

namespace Gui
{
  public class GuiLaunchShopPanel : GuiPanel
  {
    public AnonymousDelegate HideAllShops;

    public GuiLaunchShopPanel(Gui.ShipShop.ShipShop shipShop, ShopWindow shopWindow, ShipCustomizationWindow shipCustomShop, DWWindow repairWindow)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      GuiLaunchShopPanel.\u003CGuiLaunchShopPanel\u003Ec__AnonStorey91 panelCAnonStorey91 = new GuiLaunchShopPanel.\u003CGuiLaunchShopPanel\u003Ec__AnonStorey91();
      // ISSUE: reference to a compiler-generated field
      panelCAnonStorey91.shipShop = shipShop;
      // ISSUE: reference to a compiler-generated field
      panelCAnonStorey91.shopWindow = shopWindow;
      // ISSUE: reference to a compiler-generated field
      panelCAnonStorey91.repairWindow = repairWindow;
      // ISSUE: reference to a compiler-generated field
      panelCAnonStorey91.shipCustomShop = shipCustomShop;
      // ISSUE: explicit constructor call
      base.\u002Ector("GUI/_NewGui/layout/quickShopLaunchPanel");
      // ISSUE: reference to a compiler-generated method
      this.Find<GuiButton>("buyShipButton").Pressed = new AnonymousDelegate(panelCAnonStorey91.\u003C\u003Em__107);
      // ISSUE: reference to a compiler-generated method
      this.Find<GuiButton>("buyItemsButton").Pressed = new AnonymousDelegate(panelCAnonStorey91.\u003C\u003Em__108);
      // ISSUE: reference to a compiler-generated method
      this.Find<GuiButton>("repairShipButton").Pressed = new AnonymousDelegate(panelCAnonStorey91.\u003C\u003Em__109);
      // ISSUE: reference to a compiler-generated method
      this.Find<GuiButton>("modShipButton").Pressed = new AnonymousDelegate(panelCAnonStorey91.\u003C\u003Em__10A);
      GuiButton guiButton = this.Find<GuiButton>("triadButton");
      guiButton.Text = BsgoLocalization.Get("%$bgo.etc.open_dradis_contact%");
      // ISSUE: reference to a compiler-generated method
      guiButton.Pressed = new AnonymousDelegate(panelCAnonStorey91.\u003C\u003Em__10B);
      // ISSUE: reference to a compiler-generated method
      this.HideAllShops = new AnonymousDelegate(panelCAnonStorey91.\u003C\u003Em__10C);
    }

    public override void OnHide()
    {
      base.OnHide();
      if (this.HideAllShops == null)
        return;
      this.HideAllShops();
    }
  }
}
