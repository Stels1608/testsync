﻿// Decompiled with JetBrains decompiler
// Type: Gui.WaitingForCarrierNotifier
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  internal class WaitingForCarrierNotifier : GuiPanel
  {
    private readonly GuiLabel label = new GuiLabel(Options.FontBGM_BT, 20);
    private const float INTERVAL = 0.03f;
    private const float INTERVAL_RANDOM = 0.6f;
    private readonly char[] arr;
    private int arrPosition;

    public WaitingForCarrierNotifier(string message)
    {
      this.MouseTransparent = true;
      message = message.Length <= 0 ? string.Empty : BsgoLocalization.Get(message);
      this.arr = message.ToCharArray();
      this.arrPosition = 0;
      this.SetLabel(this.label, message);
      this.AddChild((GuiElementBase) new Timer(0.03f, new AnonymousDelegate(this.OnTimer)));
    }

    public static void Show()
    {
      NotificationManager.Show((GuiElementBase) new WaitingForCarrierNotifier("%$bgo.carrier.waiting_for_carrier%"));
    }

    private void SetLabel(GuiLabel label, string text)
    {
      label.Text = text;
      label.PositionX = (float) (-(double) label.SizeX / 2.0);
      label.Text = string.Empty;
      label.NormalColor = new Color(0.9f, 0.9f, 0.9f);
      label.OverColor = label.NormalColor;
      label.Alignment = TextAnchor.UpperCenter;
      this.AddChild((GuiElementBase) label, Align.UpLeft);
    }

    private void OnTimer()
    {
      if ((double) Random.value < 0.600000023841858)
        return;
      if (this.arrPosition < this.arr.Length)
      {
        this.label.Text += (string) (object) this.arr[this.arrPosition];
        ++this.arrPosition;
      }
      else
      {
        this.RemoveChild((GuiElementBase) this.Find<Timer>());
        this.AddChild((GuiElementBase) new TimerSimple(2f, new AnonymousDelegate(this.Close)));
      }
    }

    private void Close()
    {
      this.RemoveThisFromParentLater();
    }

    [Gui2Editor]
    private static SpaceLevel Level()
    {
      return SpaceLevel.GetLevel();
    }

    [Gui2Editor]
    private static bool IsInstance()
    {
      return SpaceLevel.GetLevel().IsStory;
    }
  }
}
