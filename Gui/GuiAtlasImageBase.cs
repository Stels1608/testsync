﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiAtlasImageBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace Gui
{
  public class GuiAtlasImageBase : GuiElementBase
  {
    public static readonly Vector2 smallSize = new Vector2(40f, 35f);
    public static readonly Vector2 middleSize = new Vector2(64f, 64f);
    public static readonly Vector2 bigSize = new Vector2(128f, 128f);
    public static readonly Vector2 skillIconSize = new Vector2(47f, 40f);
    [Gui2Editor]
    public Color? OverlayColor;
    private Texture2D texture;
    private uint textureIndex;
    private uint cachedCount;
    [Gui2Editor(write = false)]
    private Vector2 cachedTextureSize;
    [Gui2Editor]
    private Rect cachedRectInTexture;

    [Gui2Editor(write = false)]
    public uint Count
    {
      get
      {
        return this.cachedCount;
      }
    }

    [Gui2Editor]
    public Texture2D Texture
    {
      get
      {
        return this.texture;
      }
      set
      {
        this.texture = value;
        this.cachedCount = (uint) this.GetImageCount(this.texture, this.cachedTextureSize);
        this.TextureIndex = 0U;
      }
    }

    [Gui2Editor]
    public uint TextureIndex
    {
      get
      {
        return this.textureIndex;
      }
      set
      {
        this.textureIndex = this.Count <= 0U ? value : value % this.Count;
        this.cachedRectInTexture = this.CalcRectInTexture(this.textureIndex);
      }
    }

    public GuiAtlasImageBase(Vector2 textureSize)
      : this((Texture2D) null, textureSize)
    {
    }

    public GuiAtlasImageBase(Texture2D texture, Vector2 textureSize)
    {
      this.cachedTextureSize = textureSize;
      this.Size = this.cachedTextureSize;
      if (!((UnityEngine.Object) texture != (UnityEngine.Object) null))
        return;
      this.Texture = texture;
    }

    public override void Draw()
    {
      if (Event.current.type == EventType.Repaint && (UnityEngine.Object) this.texture != (UnityEngine.Object) null)
      {
        if (this.OverlayColor.HasValue)
          Graphics.DrawTexture(this.Rect, (UnityEngine.Texture) this.texture, this.cachedRectInTexture, 0, 0, 0, 0, this.OverlayColor.Value);
        else
          Graphics.DrawTexture(this.Rect, (UnityEngine.Texture) this.texture, this.cachedRectInTexture, 0, 0, 0, 0);
      }
      base.Draw();
    }

    private int GetImageCount(Texture2D texture, Vector2 imageSize)
    {
      if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
        return 0;
      return (int) ((double) texture.width / (double) imageSize.x) * (int) ((double) texture.height / (double) imageSize.y);
    }

    private Rect CalcRectInTexture(uint index)
    {
      if ((UnityEngine.Object) this.texture == (UnityEngine.Object) null)
        return new Rect();
      if (index >= this.Count)
        throw new ArgumentOutOfRangeException(this.GetType().ToString() + ": there is " + (object) this.Count + " total images. You wanted to set " + (object) index);
      uint num1 = (uint) ((double) this.texture.width / (double) this.cachedTextureSize.x);
      uint num2 = (uint) ((double) this.texture.height / (double) this.cachedTextureSize.y);
      uint num3 = index / num1;
      return new Rect((float) (index - num3 * num1) / (float) num1, (float) (1.0 - (double) (num3 + 1U) / (double) num2), this.cachedTextureSize.x / (float) this.texture.width, this.cachedTextureSize.y / (float) this.texture.height);
    }
  }
}
