﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("")]
  public class GuiPanel : GuiElementBase, IGUIPanel, IGUIRenderable, InputListener
  {
    [HideInInspector]
    private readonly List<GuiElementBase> children = new List<GuiElementBase>();
    [HideInInspector]
    private readonly List<GuiElementBase> toDelete = new List<GuiElementBase>();
    [HideInInspector]
    public string LayoutPath = string.Empty;
    [HideInInspector]
    private float2 previousPosition1 = new float2();
    [HideInInspector]
    private bool m_stencilFromParent = true;
    protected const string mc_backgroundElementName = "background";
    [Gui2Editor]
    [HideInInspector]
    public bool MouseTransparent;
    [HideInInspector]
    protected bool wasMouseDown;
    [Gui2Editor]
    [HideInInspector]
    private bool isMouseInputGrabber;
    [HideInInspector]
    private GuiElementBase focused;
    [HideInInspector]
    private static Dictionary<Action, AnonymousDelegate> hotKeys;
    [HideInInspector]
    protected GuiImage m_backgroundImage;
    [HideInInspector]
    private bool m_stencilOn;
    [HideInInspector]
    private bool m_drawStencilArea;

    public bool StencilOn
    {
      get
      {
        return this.m_stencilOn;
      }
      set
      {
        this.m_stencilOn = value;
      }
    }

    public bool StencilFromParent
    {
      get
      {
        return this.m_stencilFromParent;
      }
      set
      {
        this.m_stencilFromParent = value;
      }
    }

    public bool DrawStencilArea
    {
      get
      {
        return this.m_drawStencilArea;
      }
      set
      {
        this.m_drawStencilArea = value;
      }
    }

    public GuiImage BackgroundImage
    {
      get
      {
        return this.m_backgroundImage;
      }
      set
      {
        this.m_backgroundImage = value;
      }
    }

    public virtual bool IsBigWindow
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsMouseInputGrabber
    {
      get
      {
        return this.isMouseInputGrabber;
      }
      set
      {
        if (value)
          Game.InputDispatcher.Focused = (InputListener) this;
        else if (Game.InputDispatcher.Focused == this)
          Game.InputDispatcher.Focused = (InputListener) null;
        this.isMouseInputGrabber = value;
      }
    }

    public virtual bool HandleKeyboardInput
    {
      get
      {
        return this.IsRendered;
      }
    }

    public virtual bool HandleJoystickAxesInput
    {
      get
      {
        return this.IsRendered;
      }
    }

    public override bool HandleMouseInput
    {
      get
      {
        if (this.IsRendered)
          return this.handleMouseInput;
        return false;
      }
    }

    public virtual ReadOnlyCollection<GuiElementBase> Children
    {
      get
      {
        return this.children.AsReadOnly();
      }
    }

    public List<GuiElementBase> ChildrenVisible
    {
      get
      {
        return new List<GuiElementBase>((IEnumerable<GuiElementBase>) this.Children).FindAll((Predicate<GuiElementBase>) (el => el.IsRendered));
      }
    }

    public GuiPanel()
    {
    }

    public GuiPanel(string layoutPath)
    {
      this.LayoutPath = layoutPath;
      if (!(layoutPath != string.Empty))
        return;
      Loaders.Load(this, this.LayoutPath);
      this.m_backgroundImage = this.Find<GuiImage>("background");
    }

    public GuiPanel(Vector2 size, Vector2 position)
      : base(position, size)
    {
    }

    public GuiPanel(GuiPanel copy)
      : base((GuiElementBase) copy)
    {
      this.MouseTransparent = copy.MouseTransparent;
      this.handleMouseInput = copy.handleMouseInput;
      this.wasMouseDown = copy.wasMouseDown;
      this.LayoutPath = copy.LayoutPath;
      this.isMouseInputGrabber = copy.isMouseInputGrabber;
      this.focused = copy.focused;
      for (int index = 0; index < copy.children.Count; ++index)
      {
        this.children.Add(copy.children[index].CloneElement());
        this.children[index].Parent = (SRect) this;
      }
      for (int index = 0; index < copy.toDelete.Count; ++index)
      {
        this.toDelete.Add(copy.toDelete[index].CloneElement());
        this.toDelete[index].Parent = (SRect) this;
      }
      this.m_backgroundImage = this.Find<GuiImage>("background");
    }

    public GuiPanel(JPanel json)
      : base((JElementBase) json)
    {
      for (int index = 0; index < json.Elements.Count; ++index)
        this.AddChild(json.Elements[index].Convert2Gui());
    }

    public static void ClearHotkeys()
    {
      if (GuiPanel.hotKeys == null)
        return;
      GuiPanel.hotKeys.Clear();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JPanel((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      for (int index = 0; index < this.children.Count; ++index)
        this.children[index].EditorUpdate();
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
      GuiElementBase.DrawElement.BeginStencil(this.Rect, true, true);
      for (int index = 0; index < this.children.Count; ++index)
      {
        if (this.children[index].IsRendered)
        {
          this.children[index].ForceReposition();
          this.children[index].EditorDraw();
        }
      }
      GuiElementBase.DrawElement.EndStencil();
    }

    public override void DoCaching()
    {
      base.DoCaching();
      for (int index = 0; index < this.Children.Count; ++index)
        this.Children[index].DoCaching();
    }

    public override void Draw()
    {
      base.Draw();
      if (this.m_stencilOn)
        GuiElementBase.DrawElement.BeginStencil(this.Rect, this.m_stencilFromParent, this.m_drawStencilArea);
      for (int index = 0; index < this.children.Count; ++index)
      {
        GuiElementBase guiElementBase = this.children[index];
        if (guiElementBase.IsRendered)
        {
          if (!GUIManager.DebugProfile)
            ;
          guiElementBase.Draw();
          if (!GUIManager.DebugProfile)
            ;
        }
      }
      if (!this.m_stencilOn)
        return;
      GuiElementBase.DrawElement.EndStencil();
    }

    public override void OnShow()
    {
      base.OnShow();
      foreach (GuiElementBase child in this.Children)
      {
        if (child.IsRendered)
          child.OnShow();
      }
    }

    public override void OnHide()
    {
      base.OnHide();
      foreach (GuiElementBase child in this.Children)
      {
        if (child.IsRendered)
          child.OnHide();
      }
    }

    public override void ForceReposition()
    {
      base.ForceReposition();
      for (int index = 0; index < this.children.Count; ++index)
      {
        GuiElementBase guiElementBase = this.children[index];
        if (guiElementBase.IsRendered)
          guiElementBase.ForceReposition();
      }
      if (this.m_backgroundImage == null)
        return;
      this.m_backgroundImage.Position = Vector2.zero;
      this.m_backgroundImage.Size = this.Size;
    }

    protected Rect FindElementsContainmentRect()
    {
      if (this.Children.Count < 1)
        return new Rect();
      Rect rect1 = new Rect(float.MaxValue, float.MaxValue, float.MinValue, float.MinValue);
      bool flag = true;
      Rect rect2 = new Rect();
      for (int index = 0; index < this.Children.Count; ++index)
      {
        if (this.Children[index].IsRendered && this.Children[index] != this.m_backgroundImage && !this.Children[index].IgnoreOnLayout)
        {
          rect2.x = this.Children[index].TopLeftCornerWithPadding.x;
          rect2.y = this.Children[index].TopLeftCornerWithPadding.y;
          rect2.width = this.Children[index].ElementWithPaddingSizeX;
          rect2.height = this.Children[index].ElementWithPaddingSizeY;
          Vector2 vector2 = this.Children[index].Position + this.Children[index].Size + this.Children[index].ElementPadding.BottomRight;
          if ((double) rect1.width < (double) vector2.x)
            rect1.width = vector2.x;
          if ((double) rect1.height < (double) vector2.y)
            rect1.height = vector2.y;
          flag = false;
        }
      }
      if (flag)
        rect1.Set(0.0f, 0.0f, 0.0f, 0.0f);
      return rect1;
    }

    public virtual void ResizePanelToFitChildren()
    {
      Rect elementsContainmentRect = this.FindElementsContainmentRect();
      this.Size = new Vector2(elementsContainmentRect.width, elementsContainmentRect.height);
    }

    public override void SetEnvironment(object env)
    {
      base.SetEnvironment(env);
      using (List<GuiElementBase>.Enumerator enumerator = this.children.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.SetEnvironment(env);
      }
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      base.MouseDown(position, key);
      for (int index = this.children.Count - 1; index >= 0; --index)
      {
        if (index < this.children.Count)
        {
          GuiElementBase guiElementBase = this.children[index];
          if (guiElementBase.IsRendered && guiElementBase.HandleMouseInput && (guiElementBase.Contains(position) && guiElementBase.MouseDown(position, key)))
          {
            this.focused = guiElementBase;
            return true;
          }
        }
      }
      return false;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      base.MouseUp(position, key);
      for (int index = this.children.Count - 1; index >= 0; --index)
      {
        if (index < this.children.Count)
        {
          GuiElementBase guiElementBase = this.children[index];
          if (guiElementBase.IsRendered && guiElementBase.HandleMouseInput && (guiElementBase.Contains(position) && guiElementBase.MouseUp(position, key)))
            return true;
        }
      }
      return false;
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      base.MouseMove(position, previousPosition);
      bool flag1 = false;
      for (int index = this.children.Count - 1; index >= 0; --index)
      {
        if (index < this.children.Count)
        {
          GuiElementBase guiElementBase = this.children[index];
          if (guiElementBase.IsRendered && guiElementBase.HandleMouseInput)
          {
            bool flag2 = guiElementBase.Contains(position);
            bool flag3 = guiElementBase.Contains(previousPosition);
            if (!flag1 && flag2 || flag3 && !flag2)
              flag1 = guiElementBase.MouseMove(position, previousPosition) || flag1;
          }
        }
      }
      this.previousPosition1 = position;
      return flag1;
    }

    public override bool KeyDown(KeyCode key, Action action)
    {
      if (this.focused != null && this.focused.IsRendered && this.focused.KeyDown(key, action))
        return true;
      for (int index = this.children.Count - 1; index >= 0; --index)
      {
        if (index < this.children.Count)
        {
          GuiElementBase guiElementBase = this.children[index];
          if (guiElementBase.IsRendered && guiElementBase != this.focused && guiElementBase.KeyDown(key, action))
            return true;
        }
      }
      return false;
    }

    public override bool KeyUp(KeyCode key, Action action)
    {
      if (this.focused != null && this.focused.IsRendered && this.focused.KeyUp(key, action))
        return true;
      for (int index = this.children.Count - 1; index >= 0; --index)
      {
        if (index < this.children.Count)
        {
          GuiElementBase guiElementBase = this.children[index];
          if (guiElementBase.IsRendered && guiElementBase != this.focused && guiElementBase.KeyUp(key, action))
            return true;
        }
      }
      return false;
    }

    public override bool ScrollDown(float2 position)
    {
      for (int index = this.children.Count - 1; index >= 0; --index)
      {
        GuiElementBase guiElementBase = this.children[index];
        if (guiElementBase.IsRendered && guiElementBase.HandleMouseInput && (guiElementBase.Contains(position) && guiElementBase.ScrollDown(position)))
          return true;
      }
      return base.ScrollDown(position);
    }

    public override bool ScrollUp(float2 position)
    {
      for (int index = this.children.Count - 1; index >= 0; --index)
      {
        GuiElementBase guiElementBase = this.children[index];
        if (guiElementBase.IsRendered && guiElementBase.HandleMouseInput && (guiElementBase.Contains(position) && guiElementBase.ScrollUp(position)))
          return true;
      }
      return base.ScrollUp(position);
    }

    public override void Update()
    {
      if (this.toDelete.Count > 0)
      {
        using (List<GuiElementBase>.Enumerator enumerator = this.toDelete.GetEnumerator())
        {
          while (enumerator.MoveNext())
            this.RemoveChild(enumerator.Current);
        }
        this.toDelete.Clear();
      }
      for (int index = 0; index < this.children.Count; ++index)
      {
        GuiElementBase guiElementBase = this.children[index];
        if (guiElementBase.IsUpdated)
        {
          if (!GUIManager.DebugProfile)
            ;
          guiElementBase.Update();
          if (!GUIManager.DebugProfile)
            ;
        }
      }
      base.Update();
    }

    public override void PeriodicUpdate()
    {
      for (int index = 0; index < this.children.Count; ++index)
      {
        GuiElementBase guiElementBase = this.children[index];
        if (guiElementBase.IsUpdated)
        {
          if (!GUIManager.DebugProfile)
            ;
          guiElementBase.PeriodicUpdate();
          if (!GUIManager.DebugProfile)
            ;
        }
      }
      base.PeriodicUpdate();
    }

    public void RegisterHotKey(Action action, AnonymousDelegate handler)
    {
      if (GuiPanel.hotKeys == null)
        GuiPanel.hotKeys = new Dictionary<Action, AnonymousDelegate>();
      GuiPanel.hotKeys[action] = handler;
    }

    public void UnregisterHotKey(Action action)
    {
      if (GuiPanel.hotKeys == null)
        return;
      GuiPanel.hotKeys[action] = (AnonymousDelegate) null;
    }

    public virtual void RecalculateAbsCoords()
    {
      this.ForceReposition();
    }

    public virtual bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
    {
      if (!this.IsRendered)
        return false;
      this.wasMouseDown = false;
      if (!this.Contains(mousePosition) && !this.IsMouseInputGrabber)
        return false;
      this.wasMouseDown = true;
      return this.MouseDown(mousePosition, mouseKey);
    }

    public virtual bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
    {
      if (!this.IsRendered || (!this.Contains(mousePosition) || !this.wasMouseDown) && !this.IsMouseInputGrabber)
        return false;
      this.wasMouseDown = false;
      return this.MouseUp(mousePosition, mouseKey);
    }

    public void OnMouseMove(float2 mousePosition)
    {
      if (!this.IsRendered)
        return;
      this.MouseMove(mousePosition, this.previousPosition1);
    }

    public bool OnMouseScrollDown()
    {
      if (!this.IsRendered)
        return false;
      float2 mousePositionGui = MouseSetup.MousePositionGui;
      if (!this.Contains(mousePositionGui))
        return false;
      return this.ScrollDown(mousePositionGui);
    }

    public bool OnMouseScrollUp()
    {
      if (!this.IsRendered)
        return false;
      float2 mousePositionGui = MouseSetup.MousePositionGui;
      if (!this.Contains(mousePositionGui))
        return false;
      return this.ScrollUp(mousePositionGui);
    }

    public virtual bool OnKeyDown(KeyCode keyboardKey, Action action)
    {
      if (!this.IsRendered)
        return false;
      bool flag = this.KeyDown(keyboardKey, action);
      if (GuiPanel.hotKeys != null && GuiPanel.hotKeys.ContainsKey(action))
        return true;
      return flag;
    }

    public virtual bool OnKeyUp(KeyCode keyboardKey, Action action)
    {
      if (!this.IsRendered)
        return false;
      bool flag = this.KeyUp(keyboardKey, action);
      if (flag || GuiPanel.hotKeys == null || !GuiPanel.hotKeys.ContainsKey(action))
        return flag;
      GuiPanel.hotKeys[action]();
      return true;
    }

    public virtual bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
    {
      return false;
    }

    public T Find<T>(string name) where T : GuiElementBase
    {
      foreach (GuiElementBase child in this.Children)
      {
        if (child.Name == name && child is T)
          return child as T;
      }
      return (T) null;
    }

    public T Find<T>() where T : GuiElementBase
    {
      foreach (GuiElementBase child in this.Children)
      {
        if (child is T)
          return child as T;
      }
      return (T) null;
    }

    public T Find<T>(Predicate<T> match) where T : GuiElementBase
    {
      foreach (GuiElementBase child in this.Children)
      {
        if (child is T && match(child as T))
          return child as T;
      }
      return (T) null;
    }

    public List<T> FindAll<T>() where T : GuiElementBase
    {
      List<T> objList = new List<T>();
      foreach (GuiElementBase child in this.Children)
      {
        if (child is T)
          objList.Add(child as T);
      }
      return objList;
    }

    public T FindParent<T>()
    {
      GuiElementBase guiElementBase = (GuiElementBase) this;
      do
      {
        guiElementBase = guiElementBase.Parent as GuiElementBase;
        if (guiElementBase is T)
          return (T) guiElementBase;
      }
      while (guiElementBase != null);
      return default (T);
    }

    public void ReplaceChild(GuiElementBase currentChild, GuiElementBase newChild)
    {
      int index = this.children.IndexOf(currentChild);
      if (index == -1)
        return;
      this.children.RemoveAt(index);
      this.children.Insert(index, newChild);
    }

    public virtual void AddChildAt(GuiElementBase ch, int index)
    {
      this.children.Insert(index, ch);
      ch.Parent = (SRect) this;
      object environment = this.GetEnvironment();
      if (environment != null)
        ch.SetEnvironment(environment);
      if (this.Debug)
        ch.Debug = this.Debug;
      if (!ch.IsRendered)
        return;
      ch.OnShow();
    }

    public virtual void AddChild(GuiElementBase ch)
    {
      this.children.Add(ch);
      ch.Parent = (SRect) this;
      object environment = this.GetEnvironment();
      if (environment != null)
        ch.SetEnvironment(environment);
      if (this.Debug)
        ch.Debug = this.Debug;
      if (!ch.IsRendered)
        return;
      ch.OnShow();
    }

    public virtual void AddChild(GuiElementBase ch, bool isRendered)
    {
      this.AddChild(ch);
      ch.IsRendered = isRendered;
    }

    public virtual void AddChild(GuiElementBase ch, Align aligh)
    {
      this.AddChild(ch);
      ch.Align = aligh;
    }

    public virtual void AddChild(GuiElementBase ch, Align aligh, bool isRendered)
    {
      this.AddChild(ch, aligh);
      ch.IsRendered = isRendered;
    }

    public virtual void AddChild(GuiElementBase ch, Vector2 position)
    {
      this.AddChild(ch);
      ch.Position = position;
    }

    public virtual void AddChild(GuiElementBase ch, Align aligh, Vector2 position)
    {
      this.AddChild(ch, aligh);
      ch.Position = position;
    }

    public virtual void AddChild(GuiElementBase ch, Align aligh, Vector2 position, bool isRendered)
    {
      this.AddChild(ch, aligh, position);
      ch.IsRendered = isRendered;
    }

    public virtual void RemoveChild(GuiElementBase ch)
    {
      if (ch == null)
        return;
      if (ch.IsRendered)
        ch.OnHide();
      this.children.Remove(ch);
      ch.SetEnvironment((object) null);
      ch.Parent = (SRect) null;
      if (this.focused != ch)
        return;
      this.focused = (GuiElementBase) null;
    }

    public virtual void RemoveChild<T>()
    {
      foreach (GuiElementBase child in this.Children)
      {
        if (child is T)
        {
          this.RemoveChild(child);
          break;
        }
      }
    }

    public void RemoveAll()
    {
      this.RemoveAll((IEnumerable) new List<GuiElementBase>((IEnumerable<GuiElementBase>) this.Children));
    }

    public void RemoveAll(IEnumerable list)
    {
      foreach (GuiElementBase ch in list)
        this.RemoveChild(ch);
    }

    public void RemoveChildLater(GuiElementBase ch)
    {
      this.toDelete.Add(ch);
    }

    public void BringToFront(GuiElementBase ch)
    {
      this.RemoveChild(ch);
      this.AddChild(ch);
    }

    public void EmptyChildren()
    {
      while (this.Children.Count > 0)
        this.RemoveChild(this.Children[0]);
    }

    public void EmptyChildren<T>() where T : GuiElementBase
    {
      List<GuiElementBase> guiElementBaseList = new List<GuiElementBase>();
      foreach (GuiElementBase child in this.Children)
      {
        if (child is T)
          guiElementBaseList.Add(child);
      }
      using (List<GuiElementBase>.Enumerator enumerator = guiElementBaseList.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.RemoveChild(enumerator.Current);
      }
    }

    public List<T> ChildrenOfType<T>() where T : GuiElementBase
    {
      return new List<GuiElementBase>((IEnumerable<GuiElementBase>) this.Children).FindAll((Predicate<GuiElementBase>) (el => el is T)).ConvertAll<T>((Converter<GuiElementBase, T>) (el => el as T));
    }

    public override bool OnShowTooltip(float2 position)
    {
      bool flag = false;
      foreach (GuiElementBase child in this.Children)
      {
        if (child.IsRendered && child.Contains(position))
          flag |= child.OnShowTooltip(position);
      }
      if (!flag)
        flag |= base.OnShowTooltip(position);
      return flag;
    }

    public void ShowModalDialog(GuiModalDialog dialog)
    {
      foreach (GuiElementBase child in this.Children)
        this.SendMouseLeaveToAll(child);
      this.AddChild((GuiElementBase) dialog);
      this.focused = (GuiElementBase) dialog;
    }

    private void SendMouseLeaveToAll(GuiElementBase el)
    {
      if (el.IsMouseOver)
        el.MouseLeave(float2.zero, float2.zero);
      if (!(el is GuiPanel))
        return;
      foreach (GuiElementBase child in (el as GuiPanel).Children)
        this.SendMouseLeaveToAll(child);
    }

    public void CloseModalDialog(GuiModalDialog dialog)
    {
      this.RemoveChild((GuiElementBase) dialog);
      this.focused = (GuiElementBase) null;
    }

    public override bool Contains(float2 point)
    {
      if (this.MouseTransparent)
      {
        using (List<GuiElementBase>.Enumerator enumerator = this.children.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            if (enumerator.Current.Contains(point))
              return true;
          }
        }
        return false;
      }
      bool flag = base.Contains(point);
      GUIManager.MouseOverAnyOldGuiElement |= flag;
      return flag;
    }
  }
}
