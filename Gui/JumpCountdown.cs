﻿// Decompiled with JetBrains decompiler
// Type: Gui.JumpCountdown
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class JumpCountdown : GuiPanel
  {
    private readonly GuiLabel text = new GuiLabel(Options.FontDS_DIGIB, 18);
    [Gui2Editor]
    private float startTime;
    [Gui2Editor]
    private float endTime;

    public override bool IsRendered
    {
      get
      {
        return base.IsRendered;
      }
      set
      {
        base.IsRendered = value && (double) this.endTime > (double) Time.realtimeSinceStartup;
      }
    }

    public JumpCountdown()
    {
      GuiImage guiImage = new GuiImage("GUI/JumpCountdown/ftlbox");
      this.Size = guiImage.Size;
      this.text.AllColor = Game.Me.Faction != Faction.Colonial ? Color.red : Tools.Color(113, 191, 239);
      this.text.Alignment = TextAnchor.MiddleCenter;
      GuiLabel guiLabel = new GuiLabel("%$bgo.jump_countdown.caption%", Options.FontBGM_BT, 12);
      guiLabel.Alignment = TextAnchor.MiddleCenter;
      this.AddChild((GuiElementBase) guiImage);
      this.AddChild((GuiElementBase) this.text, Align.UpCenter, new Vector2(0.0f, 11f));
      this.AddChild((GuiElementBase) guiLabel, Align.UpCenter, new Vector2(0.0f, 36f));
      this.Align = Align.DownCenter;
      this.PositionY = -200f;
    }

    [Gui2Editor]
    public static void Show()
    {
      JumpCountdown.Show(20f);
    }

    [Gui2Editor]
    public static void Show(float time)
    {
      if ((Object) SpaceLevel.GetLevel() == (Object) null)
        return;
      FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.jump_countdown.notification%", NotificationCategory.Neutral));
      JumpCountdown jumpCountdown = Game.GUIManager.Find<JumpCountdown>();
      jumpCountdown.startTime = Time.realtimeSinceStartup;
      jumpCountdown.endTime = (float) ((double) jumpCountdown.startTime + (double) time + 1.0);
      jumpCountdown.IsRendered = true;
    }

    [Gui2Editor]
    public static void Hide()
    {
      if ((Object) SpaceLevel.GetLevel() == (Object) null)
        return;
      SpaceLevel.GetLevel().sectorSfx.StopFTLChargingSound();
      Game.GUIManager.Find<JumpCountdown>().IsRendered = false;
    }

    public override void Update()
    {
      base.Update();
      GalaxyMapMain galaxyMapMain = Game.GUIManager.Find<GalaxyMapMain>();
      if (galaxyMapMain != null && !galaxyMapMain.isJumpActive)
      {
        this.IsRendered = false;
      }
      else
      {
        int num1 = (int) ((double) this.endTime - (double) Time.realtimeSinceStartup);
        int num2 = num1 / 60;
        int num3 = num1 - num2 * 60;
        this.text.Text = string.Format("{0:00}:{1:00}", (object) num2, (object) num3);
        if ((double) this.endTime - (double) Time.realtimeSinceStartup <= 3.0)
        {
          GUICancelGroupJumpWindow cancelGroupJumpWindow = Game.GUIManager.Find<GUICancelGroupJumpWindow>();
          if (cancelGroupJumpWindow != null)
            cancelGroupJumpWindow.Hide();
        }
        if ((double) this.endTime >= (double) Time.realtimeSinceStartup)
          return;
        Game.Me.Party.inGroupJump = false;
        JumpCountdown.Hide();
      }
    }
  }
}
