﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedTooltipBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedTooltipBase : GuiPanel
  {
    private Vector2 m_showPosition = new Vector2();
    private bool m_useMousePosition = true;
    public const int MAX_TOOLTIP_WIDTH = 360;
    protected bool m_focusEnabled;
    private readonly GuiImage backgroundStroke;

    public bool HideManually { get; set; }

    public bool UseMousePosition
    {
      get
      {
        if (!this.m_useMousePosition)
          return this.Parent == null;
        return true;
      }
      set
      {
        this.m_useMousePosition = value;
        if (!this.m_useMousePosition)
          return;
        this.m_showPosition = MouseSetup.MousePositionGui.ToV2();
      }
    }

    protected GuiAdvancedTooltipBase()
    {
      this.m_backgroundImage = new GuiImage("GUI/Tooltips/BG_box1");
      this.m_backgroundImage.IgnoreOnLayout = true;
      this.m_backgroundImage.NineSliceEdge = new GuiElementPadding(new Rect(16f, 16f, 16f, 16f));
      this.m_backgroundImage.Name = "background";
      this.AddChild((GuiElementBase) this.m_backgroundImage);
      this.backgroundStroke = new GuiImage("GUI/Tooltips/BG_box1_stroke");
      this.backgroundStroke.IgnoreOnLayout = true;
      this.backgroundStroke.NineSliceEdge = new GuiElementPadding(new Rect(16f, 16f, 16f, 16f));
      this.backgroundStroke.OverlayColor = new Color?(ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR));
      this.AddChild((GuiElementBase) this.backgroundStroke);
    }

    protected GuiAdvancedTooltipBase(string layout)
      : base(layout)
    {
      if (this.m_backgroundImage != null)
        return;
      this.m_backgroundImage = new GuiImage("GUI/Tooltips/BG_box1");
    }

    public GuiAdvancedTooltipBase(GuiAdvancedTooltipBase copy)
      : this(copy.LayoutPath)
    {
    }

    public override void OnShow()
    {
      base.OnShow();
      this.RepositionPanel(MouseSetup.MousePositionGui.ToV2());
    }

    public void RepositionPanel(Vector2 mousePosition)
    {
      if (this.UseMousePosition)
      {
        this.m_showPosition = mousePosition;
      }
      else
      {
        this.m_showPosition.x = this.Parent.Rect.x + this.Parent.SizeX;
        this.m_showPosition.y = this.Parent.Rect.y;
      }
      Vector2 vector2_1 = new Vector2((float) Screen.width, (float) Screen.height);
      Vector2 vector2_2 = this.m_showPosition + this.Size;
      if ((double) vector2_2.x >= (double) vector2_1.x)
      {
        if (this.UseMousePosition)
          this.m_showPosition.x -= this.SizeX;
        else
          this.m_showPosition.x = -this.SizeX;
      }
      if ((double) vector2_2.y > (double) vector2_1.y)
        this.m_showPosition.y -= Mathf.Min(vector2_2.y - vector2_1.y, this.m_showPosition.y);
      this.Position = this.m_showPosition;
      if (this.Parent == null)
        return;
      this.PositionX -= (float) (double) this.Parent.Rect.x;
      this.PositionY -= (float) (double) this.Parent.Rect.y;
    }

    public override void ForceReposition()
    {
      base.ForceReposition();
      if (this.backgroundStroke == null)
        return;
      this.backgroundStroke.Size = this.m_backgroundImage.Size;
      this.backgroundStroke.Position = this.m_backgroundImage.Position;
    }

    public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
    {
      if (this.Contains(mousePosition))
        this.m_focusEnabled = true;
      return base.OnMouseDown(mousePosition, mouseKey);
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      this.RepositionPanel(position.ToV2());
      this.m_focusEnabled = this.Contains(position);
      if (this.m_focusEnabled)
        base.MouseMove(position, previousPosition);
      return this.m_focusEnabled;
    }
  }
}
