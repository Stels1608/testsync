﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedShipTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedShipTooltip : GuiAdvancedCardTooltip
  {
    private readonly GuiAdvancedValuePanel valuePanel;
    private readonly GuiLabel classRestrictionLabel;
    private readonly GuiAdvancedCostPanel costPanel;
    private readonly GuiAdvancedRequirementsPanel requirementsPanel;

    public GuiAdvancedShipTooltip()
    {
      this.classRestrictionLabel = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
      this.classRestrictionLabel.FontSize = 16;
      this.AddChild((GuiElementBase) this.classRestrictionLabel, Align.UpLeft, new Vector2(12f, 0.0f));
      this.valuePanel = new GuiAdvancedValuePanel();
      this.valuePanel.FontSize = 13;
      this.AddChild((GuiElementBase) this.valuePanel, Align.UpCenter);
      this.costPanel = new GuiAdvancedCostPanel();
      this.AddChild((GuiElementBase) this.costPanel, Align.UpLeft, new Vector2(12f, 0.0f));
      this.requirementsPanel = new GuiAdvancedRequirementsPanel();
      this.AddChild((GuiElementBase) this.requirementsPanel, Align.UpCenter);
    }

    public void SetShip(ShipCard ship, ShopInventoryContainer containerType = ShopInventoryContainer.Store)
    {
      if (ship == null)
      {
        Debug.LogError((object) "Ship is null");
      }
      else
      {
        ExchangeType exchangeType = containerType != ShopInventoryContainer.Store ? ExchangeType.SELL : ExchangeType.BUY;
        this.SetGuiCard(ship.ItemGUICard);
        this.valuePanel.Clear();
        this.SetGameItem((GameItemCard) ship);
        this.descriptionPanel.IsRendered = false;
        this.classRestrictionLabel.Text = BsgoLocalization.Get("%$bgo.shop_tooltip.ship_class%", (object) BsgoLocalization.Get("%$bgo.common.ship_class_tier_" + (object) ship.Tier + "%")).ToUpper();
        this.classRestrictionLabel.PositionY = this.itemIcon.BottomRightCornerWithPadding.y + 6f;
        this.itemLevelLabel.PositionY = this.classRestrictionLabel.PositionY;
        this.systemTypeIcon.PositionY = this.classRestrictionLabel.BottomRightCornerWithPadding.y;
        this.systemTypeLabel.PositionY = this.systemTypeIcon.TopLeftCornerWithPadding.y + 12f;
        this.SetValues(ship);
        this.requirementsPanel.SetItem(ship);
        this.costPanel.SetItem(ship.ShopItemCard, exchangeType);
        this.ResizePanelToFitChildren();
        this.CheckForSale(ship.ItemGUICard.CardGUID);
      }
    }

    public override void ResizePanelToFitChildren()
    {
      this.valuePanel.PositionY = this.systemTypeIcon.BottomRightCornerWithPadding.y;
      base.ResizePanelToFitChildren();
      this.valuePanel.SizeX = this.SizeX - 20f;
      this.requirementsPanel.SizeX = this.SizeX - 20f;
      this.innerBackground.SizeY = (float) ((double) this.costPanel.BottomRightCornerWithPadding.y - (double) this.itemIcon.BottomRightCornerWithPadding.y - 6.0);
    }

    protected override void OnCustomLayout()
    {
      this.requirementsPanel.PositionY = this.descriptionPanel.BottomRightCornerWithPadding.y;
      this.costPanel.PositionY = this.requirementsPanel.BottomRightCornerWithPadding.y + 3f;
      this.SizeY = this.costPanel.BottomRightCornerWithPadding.y + 6f;
    }

    protected override float GetDescriptionYPosition()
    {
      return this.valuePanel.BottomRightCornerWithPadding.y;
    }

    private void SetValues(ShipCard ship)
    {
      List<Tuple<string, float>> printableList = ship.Stats.ToPrintableList();
      using (List<Tuple<string, float>>.Enumerator enumerator = printableList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Tuple<string, float> current = enumerator.Current;
          this.valuePanel.AddEntry(current.First, current.Second.ToString());
        }
      }
      this.valuePanel.IsRendered = printableList.Count > 0;
    }
  }
}
