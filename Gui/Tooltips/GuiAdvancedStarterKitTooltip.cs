﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedStarterKitTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedStarterKitTooltip : GuiAdvancedCardTooltip
  {
    private readonly GuiAdvancedItemListPanel equipmentPanel;
    private readonly GuiAdvancedItemListPanel ammoPanel;
    private readonly GuiAdvancedCostPanel costPanel;

    public GuiAdvancedStarterKitTooltip()
    {
      this.equipmentPanel = new GuiAdvancedItemListPanel("bgo.shop_tooltip.equipment");
      this.AddChild((GuiElementBase) this.equipmentPanel, Align.UpCenter);
      this.ammoPanel = new GuiAdvancedItemListPanel("bgo.shop_tooltip.ammunition_types");
      this.AddChild((GuiElementBase) this.ammoPanel, Align.UpCenter);
      this.costPanel = new GuiAdvancedCostPanel();
      this.AddChild((GuiElementBase) this.costPanel, Align.UpLeft, new Vector2(18f, 0.0f));
    }

    public void SetStarterKit(StarterKit starterKitItem)
    {
      StarterKitCard starterKitCard = starterKitItem.Card;
      this.SetGuiCard(starterKitCard.GUICard);
      this.SetGameItem((GameItemCard) starterKitItem);
      this.equipmentPanel.Clear();
      this.ammoPanel.Clear();
      this.descriptionPanel.IsRendered = false;
      using (List<ShipItem>.Enumerator enumerator = starterKitCard.Items.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ShipItem current = enumerator.Current;
          if (current is ShipSystem)
            this.equipmentPanel.AddItem(current.ItemGUICard);
          else
            this.ammoPanel.AddItem(current.ItemGUICard);
        }
      }
      this.equipmentPanel.PositionY = this.systemTypeIcon.BottomRightCornerWithPadding.y;
      this.ammoPanel.PositionY = this.equipmentPanel.BottomRightCornerWithPadding.y;
      this.costPanel.PositionY = this.ammoPanel.BottomRightCornerWithPadding.y;
      this.costPanel.SetItem(starterKitItem.ShopItemCard, ExchangeType.BUY);
      this.ResizePanelToFitChildren();
      this.CheckForSale(starterKitItem.ItemGUICard.CardGUID);
    }

    public override void ResizePanelToFitChildren()
    {
      base.ResizePanelToFitChildren();
      this.equipmentPanel.SizeX = this.SizeX - 20f;
      this.ammoPanel.SizeX = this.SizeX - 20f;
      this.innerBackground.SizeY = (float) ((double) this.costPanel.BottomRightCornerWithPadding.y - (double) this.itemIcon.BottomRightCornerWithPadding.y - 6.0);
    }
  }
}
