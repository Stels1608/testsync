﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedBuffTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedBuffTooltip : GuiAdvancedTooltipBase
  {
    private float mc_maxPanelWidth = 300f;
    private float m_icon2TitleOffsetX = 5f;
    private float m_icon2DescriptionOffsetY = 5f;
    private float m_description2TimerOffsetY = 5f;
    private const string mc_layout = "GUI/Tooltips/AdvancedBuffTooltipLayout.txt";
    private const string mc_buffLogoElementName = "buffLogo";
    private const string mc_buffTitleElementName = "buffTitle";
    private const string mc_buffDescriptionElementName = "buffDescription";
    private const string mc_buffTimerElementName = "buffTimer";
    private GuiImage m_buffLogo;
    private GuiLabel m_buffTitle;
    private GuiLabel m_buffDescription;
    private GuiLabel m_buffTimer;
    private ShipBuff m_buff;

    public GuiAdvancedBuffTooltip()
      : base("GUI/Tooltips/AdvancedBuffTooltipLayout.txt")
    {
      this.FindElements();
    }

    public GuiAdvancedBuffTooltip(ShipBuff card)
      : base("GUI/Tooltips/AdvancedBuffTooltipLayout.txt")
    {
      this.FindElements();
      this.SetBuff(card);
    }

    private void FindElements()
    {
      this.m_buffLogo = this.Find<GuiImage>("buffLogo");
      this.m_buffTitle = this.Find<GuiLabel>("buffTitle");
      this.m_buffDescription = this.Find<GuiLabel>("buffDescription");
      this.m_buffTimer = this.Find<GuiLabel>("buffTimer");
      this.CalculateElementOffsets();
    }

    private void CalculateElementOffsets()
    {
      this.m_icon2TitleOffsetX = this.m_buffTitle.PositionX - (this.m_buffLogo.PositionX + this.m_buffLogo.SizeX);
      this.m_icon2DescriptionOffsetY = this.m_buffDescription.PositionY - (this.m_buffLogo.PositionY + this.m_buffLogo.SizeY);
      this.m_description2TimerOffsetY = this.m_buffTimer.PositionY - (this.m_buffDescription.PositionY + this.m_buffDescription.SizeY);
    }

    public void SetBuff(ShipBuff buff)
    {
      this.m_buff = buff;
      if (this.m_buff != null && this.m_buff.GuiCard != null)
      {
        GUICard guiCard = this.m_buff.GuiCard;
        if ((Object) guiCard.GUIIconTexture != (Object) null)
        {
          this.m_buffLogo.Texture = guiCard.GUIIconTexture;
          this.m_buffLogo.SourceRect = new Rect(0.0f, 0.0f, 1f, 1f);
        }
        else
        {
          AtlasCache atlasCache = new AtlasCache(float2.FromV2(new Vector2(40f, 35f)));
          AtlasEntry cachedEntryBy = atlasCache.GetCachedEntryBy(guiCard.GUIAtlasTexturePath, (int) guiCard.FrameIndex);
          this.m_buffLogo.Texture = cachedEntryBy.Texture;
          this.m_buffLogo.SourceRect = cachedEntryBy.FrameRect;
          this.m_buffLogo.Size = float2.ToV2(atlasCache.ElementSize);
        }
        this.m_buffTitle.WordWrap = false;
        this.m_buffDescription.WordWrap = false;
        this.m_buffTitle.Text = guiCard.Name;
        this.m_buffDescription.Text = guiCard.Description;
        this.m_buffTimer.Text = this.ConvertTimerToTimeString(this.m_buff.TimeLeft);
      }
      this.ResizePanelToFitChildren();
    }

    private string ConvertTimerToTimeString(float timer)
    {
      float num1 = (double) timer < 0.0 ? 0.0f : timer;
      int num2 = (int) ((double) num1 / 60.0);
      if (num2 > 0)
        return BsgoLocalization.Get("%$bgo.slottooltip.timer_full_format%", (object) num2, (object) (int) ((double) num1 - (double) num2 * 60.0));
      return BsgoLocalization.Get("%$bgo.slottooltip.timer_short_format%", (object) (int) num1);
    }

    public override void Update()
    {
      if (this.m_buff != null)
      {
        this.m_buffTimer.Text = (double) this.m_buff.MaxTime != 3.40282346638529E+38 ? this.ConvertTimerToTimeString(this.m_buff.TimeLeft) : string.Empty;
        this.m_buffTimer.PositionX = this.SizeX - this.m_buffTimer.ElementPadding.Right - this.m_buffTimer.SizeX;
        this.m_buffTimer.PositionY = this.m_buffDescription.PositionY + this.m_buffDescription.SizeY + this.m_description2TimerOffsetY;
      }
      base.Update();
    }

    public override void ResizePanelToFitChildren()
    {
      this.m_buffTitle.PositionX = this.m_icon2TitleOffsetX + (this.m_buffLogo.PositionX + this.m_buffLogo.SizeX);
      this.m_buffDescription.PositionY = this.m_icon2DescriptionOffsetY + (this.m_buffLogo.PositionY + this.m_buffLogo.SizeY);
      float num = Mathf.Max(this.m_buffTitle.PositionX + this.m_buffTitle.SizeX + this.m_buffTitle.ElementPadding.Right, this.m_buffDescription.PositionX + this.m_buffDescription.SizeX + this.m_buffDescription.ElementPadding.Right);
      if ((double) num > (double) this.mc_maxPanelWidth)
      {
        this.m_buffTitle.WordWrap = true;
        this.m_buffTitle.SizeX = this.mc_maxPanelWidth - this.m_buffTitle.PositionX - this.m_buffTitle.ElementPadding.Right;
        this.m_buffDescription.WordWrap = true;
        this.m_buffDescription.SizeX = this.mc_maxPanelWidth - this.m_buffDescription.PositionX - this.m_buffDescription.ElementPadding.Right;
        num = this.mc_maxPanelWidth;
      }
      this.m_buffTimer.PositionX = num - this.m_buffTimer.ElementPadding.Right - this.m_buffTimer.SizeX;
      this.m_buffTimer.PositionY = this.m_buffDescription.PositionY + this.m_buffDescription.SizeY + this.m_description2TimerOffsetY;
      base.ResizePanelToFitChildren();
    }

    public override string ToString()
    {
      return "Type: " + (object) this.GetType() + "\nLabel: " + this.m_buffTitle.TextParsed + "\nDescription: " + this.m_buffDescription.TextParsed + "\nTimer: " + this.m_buffTimer.TextParsed;
    }
  }
}
