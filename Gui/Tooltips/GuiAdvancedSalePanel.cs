﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedSalePanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedSalePanel : GuiPanel
  {
    private readonly GuiLabel reducedLabel;
    private readonly GuiLabel remainingTimeLabel;
    private ShopDiscount discount;

    public GuiAdvancedSalePanel()
    {
      this.BackgroundImage = new GuiImage("GUI/Tooltips/sale_header");
      this.BackgroundImage.NineSliceEdge = new GuiElementPadding(8f, 0.0f, 16f, 0.0f);
      this.BackgroundImage.IgnoreOnLayout = true;
      this.AddChild((GuiElementBase) this.BackgroundImage);
      this.reducedLabel = new GuiLabel(Color.black, Gui.Options.fontEurostileTRegCon);
      this.reducedLabel.FontSize = 16;
      this.AddChild((GuiElementBase) this.reducedLabel, Align.MiddleLeft, new Vector2(8f, 0.0f));
      this.remainingTimeLabel = new GuiLabel(Color.black, Gui.Options.fontEurostileTRegCon);
      this.remainingTimeLabel.FontSize = 16;
      this.AddChild((GuiElementBase) this.remainingTimeLabel, Align.MiddleRight, new Vector2(-24f, 0.0f));
      this.AddChild((GuiElementBase) new Gui.Timer(1f));
      this.ResizePanelToFitChildren();
    }

    public void SetDiscount(ShopDiscount discount)
    {
      this.discount = discount;
      this.reducedLabel.Text = BsgoLocalization.Get("%$bgo.shop_tooltip.percent_off%", (object) (discount.Percentage.ToString() + "% "));
      this.SetSaleTimeLeft(discount);
    }

    private void SetSaleTimeLeft(ShopDiscount discount)
    {
      int num1 = discount == null ? 123456789 : discount.Duration;
      int num2 = num1 / 60;
      int num3 = num1 - num2 * 60;
      int num4 = num2 / 60;
      int num5 = num2 - num4 * 60;
      int num6 = num4 / 24;
      int num7 = num4 - num6 * 24;
      this.remainingTimeLabel.Text = "%$bgo.shop_tooltip.time_remaining% " + num6.ToString("D2") + ":" + num7.ToString("D2") + ":" + num5.ToString("D2") + ":" + num3.ToString("D2");
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      if (this.discount == null)
        return;
      this.SetSaleTimeLeft(this.discount);
    }
  }
}
