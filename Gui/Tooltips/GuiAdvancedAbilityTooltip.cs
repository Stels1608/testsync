﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedAbilityTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedAbilityTooltip : GuiAdvancedCardTooltip
  {
    private readonly GuiLabel primaryStatLabel;
    private readonly GuiAdvancedValuePanel valuePanel;
    private ShipSlot slot;

    public GuiAdvancedAbilityTooltip(ShipAbility ability, GUICard card)
    {
      this.primaryStatLabel = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
      this.primaryStatLabel.FontSize = 20;
      this.primaryStatLabel.Style.richText = true;
      this.AddChild((GuiElementBase) this.primaryStatLabel, Align.UpLeft, new Vector2(166f, 0.0f));
      this.valuePanel = new GuiAdvancedValuePanel();
      this.AddChild((GuiElementBase) this.valuePanel, Align.UpCenter);
      this.AddChild((GuiElementBase) new Gui.Timer(0.5f));
      this.SetItem(ability, card);
    }

    public void SetItem(ShipAbility ability, GUICard card)
    {
      if (card == null || ability == null)
        return;
      this.systemTypeLabel.SizeX = 200f;
      this.SetGuiCard(card);
      this.SetGameItem((GameItemCard) ability.slot.System);
      this.SetPrimaryStat(ability.slot.System);
      this.slot = ability.slot;
      this.SetValues(ability);
      this.systemTypeLabel.PositionY = this.systemTypeIcon.TopLeftCornerWithPadding.y;
      this.itemLevelLabel.PositionY = this.systemTypeLabel.PositionY + 12f;
      this.ResizePanelToFitChildren();
    }

    private void SetPrimaryStat(ShipSystem shipSystem)
    {
      if (SystemsStatsGenerator.HasPrimaryStat(shipSystem))
      {
        this.primaryStatLabel.Text = SystemsStatsGenerator.GetPrimaryStatString(shipSystem, true);
        this.primaryStatLabel.PositionY = this.systemTypeIcon.PositionY + 4f;
        this.systemTypeLabel.SizeX = 100f;
        this.primaryStatLabel.IsRendered = true;
      }
      else
        this.primaryStatLabel.IsRendered = false;
    }

    private void SetValues(ShipAbility ability)
    {
      this.valuePanel.Clear();
      ShipSystem system = ability.slot.System;
      List<SystemsStatsGenerator.StatInfoDesc> descriptions = SystemsStatsGenerator.GenerateDescriptions(system, system.Card.NextCard, true);
      for (int index = 0; index < descriptions.Count; ++index)
        this.valuePanel.AddEntry(descriptions[index].Name, descriptions[index].StatFormatedValue);
      this.valuePanel.IsRendered = descriptions.Count > 0 && system.Card.SlotType != ShipSlotType.ship_paint;
      this.slot.UpdatedStats = false;
    }

    private void SetValuesOld(ShipAbility ability)
    {
      if ((double) ability.GetPPCostPerSecond() > 0.0)
        this.valuePanel.AddEntry("%$bgo.statsdescription.pp_per_sec.name%", string.Format(BsgoLocalization.Get("bgo.statsdescription.pp_per_sec.valueformat"), (object) ability.GetPPCostPerSecond()));
      else
        this.valuePanel.AddEntry("%$bgo.statsdescription.powerpointcost.name%", string.Format("{0:0.00}", (object) ability.GetAbilityPPcost()));
      this.valuePanel.AddEntry("%$bgo.statsdescription.cooldown.name%", string.Format("{0:0.00}", (object) ability.GetAbilityCooldown()) + " %$bgo.common.second%");
      this.valuePanel.AddEntry("%$bgo.statsdescription.minrange.name%", string.Format("{0:0.00}", (object) ability.GetAbilityMinRange()));
      this.valuePanel.AddEntry("%$bgo.statsdescription.maxrange.name%", string.Format("{0:0.00}", (object) ability.GetAbilityMaxRange()));
    }

    public override void Update()
    {
      base.Update();
      if (this.slot == null || !this.slot.UpdatedStats)
        return;
      this.SetPrimaryStat(this.slot.System);
      this.SetValues(this.slot.Ability);
      this.ResizePanelToFitChildren();
    }

    public override void ResizePanelToFitChildren()
    {
      this.valuePanel.PositionY = this.systemTypeIcon.BottomRightCornerWithPadding.y;
      base.ResizePanelToFitChildren();
      this.valuePanel.SizeX = this.SizeX - (float) (2 * this.DistanceFromBorder);
    }

    protected override float GetDescriptionYPosition()
    {
      return this.valuePanel.BottomRightCornerWithPadding.y;
    }
  }
}
