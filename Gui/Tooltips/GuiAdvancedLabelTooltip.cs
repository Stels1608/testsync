﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedLabelTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedLabelTooltip : GuiAdvancedTooltipBase
  {
    private const string mc_labelName = "label";
    private readonly GuiLabel m_label;

    public GuiAdvancedLabelTooltip()
      : this((string) null)
    {
      this.IsRendered = false;
    }

    public GuiAdvancedLabelTooltip(string labelText)
    {
      this.m_label = new GuiLabel(labelText, Color.white, Options.fontEurostileTRegCon);
      this.m_label.FontSize = 16;
      this.m_label.Style.padding = new RectOffset(6, 6, 6, 2);
      this.AddChild((GuiElementBase) this.m_label);
      this.SetText(labelText);
    }

    public void SetText(string label)
    {
      this.m_label.WordWrap = false;
      this.m_label.Text = label;
      this.ResizePanelToFitChildren();
    }

    public override void ResizePanelToFitChildren()
    {
      if ((double) this.m_label.SizeX > 360.0)
      {
        this.m_label.WordWrap = true;
        this.m_label.SizeX = 360f;
      }
      base.ResizePanelToFitChildren();
    }

    public override string ToString()
    {
      return "Type: " + (object) this.GetType() + "\nLabel: " + this.m_label.TextParsed;
    }
  }
}
