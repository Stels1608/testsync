﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedDescriptionPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedDescriptionPanel : GuiPanel
  {
    private readonly GuiLabel descriptionLabel;

    public override bool IsRendered
    {
      get
      {
        return base.IsRendered;
      }
      set
      {
        base.IsRendered = value;
        if (value)
          return;
        this.SizeY = 0.0f;
      }
    }

    public GuiAdvancedDescriptionPanel()
    {
      this.BackgroundImage = new GuiImage("GUI/Tooltips/BG_box_values");
      this.BackgroundImage.NineSliceEdge = (GuiElementPadding) new Rect(16f, 16f, 16f, 16f);
      this.AddChild((GuiElementBase) this.BackgroundImage, Align.UpCenter);
      this.descriptionLabel = new GuiLabel(Gui.Options.fontEurostileTRegCon);
      this.descriptionLabel.Style.padding = new RectOffset(8, 8, 6, 6);
      this.descriptionLabel.FontSize = 16;
      this.descriptionLabel.NormalColor = Color.gray;
      this.AddChild((GuiElementBase) this.descriptionLabel, Align.UpLeft);
    }

    public void SetText(string text)
    {
      this.descriptionLabel.WordWrap = false;
      this.descriptionLabel.Text = text;
      if (string.IsNullOrEmpty(text))
      {
        this.IsRendered = false;
      }
      else
      {
        this.IsRendered = true;
        this.ResizePanelToFitChildren();
      }
    }

    public void Wrap(float width)
    {
      this.descriptionLabel.WordWrap = true;
      this.descriptionLabel.SizeX = width - 35f;
      this.ResizePanelToFitChildren();
    }

    public override void ResizePanelToFitChildren()
    {
      base.ResizePanelToFitChildren();
      if (!this.IsRendered)
        this.SizeY = 0.0f;
      else
        this.SizeY -= 4f;
    }
  }
}
