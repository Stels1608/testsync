﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedTooltipManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedTooltipManager
  {
    private float m_popupDelay = 0.25f;
    private GuiAdvancedTooltipBase m_activeTooltip;
    private float m_popupTime;

    public float PopupDelay
    {
      get
      {
        return this.m_popupDelay;
      }
      set
      {
        this.m_popupDelay = value;
      }
    }

    public GuiAdvancedTooltipManager()
    {
      this.m_activeTooltip = (GuiAdvancedTooltipBase) null;
    }

    public void ShowTooltip(GuiAdvancedTooltipBase tooltip)
    {
      if (this.m_activeTooltip == tooltip)
        return;
      this.m_activeTooltip = tooltip;
      if (this.m_activeTooltip != null)
        this.m_activeTooltip.IsRendered = false;
      this.m_popupTime = this.m_popupDelay + Time.time;
    }

    public void HideTooltip(GuiAdvancedTooltipBase tooltip = null)
    {
      if (this.m_activeTooltip != null && tooltip != null)
      {
        if (tooltip != this.m_activeTooltip)
          return;
        this.m_activeTooltip = (GuiAdvancedTooltipBase) null;
      }
      else
        this.m_activeTooltip = (GuiAdvancedTooltipBase) null;
    }

    public void Update()
    {
      if (this.m_activeTooltip == null)
        return;
      this.m_activeTooltip.Update();
      if (this.m_activeTooltip.IsRendered || (double) Time.time <= (double) this.m_popupTime)
        return;
      this.m_activeTooltip.IsRendered = true;
    }

    public void Draw()
    {
      if (this.m_activeTooltip == null || !this.m_activeTooltip.IsRendered)
        return;
      this.m_activeTooltip.Draw();
    }

    public bool MouseDown(float2 position, KeyCode key)
    {
      if (this.m_activeTooltip == null || !this.m_activeTooltip.Contains(position))
        this.m_activeTooltip = (GuiAdvancedTooltipBase) null;
      return false;
    }

    public bool MouseUp(float2 position, KeyCode key)
    {
      if (this.m_activeTooltip == null || !this.m_activeTooltip.Contains(position))
        this.m_activeTooltip = (GuiAdvancedTooltipBase) null;
      return false;
    }

    public bool MouseMove(float2 position)
    {
      if (this.m_activeTooltip != null)
      {
        this.m_activeTooltip.OnMouseMove(position);
        if (this.m_activeTooltip.Parent == null && this.m_activeTooltip.HideManually || this.m_activeTooltip.Parent != null && this.m_activeTooltip.Parent.Rect.Contains(float2.ToV2(position)))
          return true;
        this.m_activeTooltip = (GuiAdvancedTooltipBase) null;
      }
      return false;
    }
  }
}
