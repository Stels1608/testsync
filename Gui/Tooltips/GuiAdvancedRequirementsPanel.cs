﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedRequirementsPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedRequirementsPanel : GuiPanel
  {
    private const int PADDING = 6;
    private readonly GuiLabel headerLabel;
    private readonly GuiImage requirementsNotMetIcon;

    public override bool IsRendered
    {
      get
      {
        return base.IsRendered;
      }
      set
      {
        base.IsRendered = value;
        if (value)
          return;
        this.SizeY = 0.0f;
      }
    }

    public GuiAdvancedRequirementsPanel()
    {
      this.BackgroundImage = new GuiImage("GUI/Tooltips/BG_box_values");
      this.BackgroundImage.IgnoreOnLayout = true;
      this.BackgroundImage.NineSliceEdge = (GuiElementPadding) new Rect(16f, 16f, 16f, 16f);
      this.headerLabel = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
      this.headerLabel.FontSize = 16;
      this.headerLabel.Text = BsgoLocalization.Get("%$bgo.shop_tooltip.requirements%").ToUpper();
      this.requirementsNotMetIcon = new GuiImage("GUI/Tooltips/exclamation_mark");
    }

    private void Clear()
    {
      this.RemoveAll();
      this.AddChild((GuiElementBase) this.BackgroundImage, Align.UpCenter);
      this.AddChild((GuiElementBase) this.headerLabel, Align.UpLeft, new Vector2(6f, 6f));
    }

    private void ShowRequirementNotMet(bool anyRequirementNotMet)
    {
      if (!anyRequirementNotMet)
        return;
      this.AddChild((GuiElementBase) this.requirementsNotMetIcon, Align.MiddleRight, new Vector2(-40f, 0.0f));
    }

    public void SetItem(ShipSystem shipSystem)
    {
      this.Clear();
      float num1 = this.headerLabel.BottomRightCornerWithPadding.y - 4f;
      bool anyRequirementNotMet = false;
      bool flag1 = shipSystem.PaintCard != null;
      if (flag1)
      {
        GuiLabel guiLabel1 = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
        guiLabel1.FontSize = 16;
        guiLabel1.Text = BsgoLocalization.Get("%$bgo.shop_tooltip.required_ship%") + ":";
        this.AddChild((GuiElementBase) guiLabel1, Align.UpLeft, new Vector2(6f, num1 + 8f));
        GuiLabel guiLabel2 = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
        guiLabel2.FontSize = 16;
        guiLabel2.Text = shipSystem.PaintCard.shipCard.ItemGUICard.Name;
        guiLabel2.WordWrap = true;
        this.AddChild((GuiElementBase) guiLabel2, Align.UpLeft, new Vector2(guiLabel1.BottomRightCornerWithPadding.x, num1 + 8f));
        bool flag2 = (int) shipSystem.PaintCard.shipCard.HangarID == (int) Game.Me.ActiveShip.Card.HangarID;
        anyRequirementNotMet |= !flag2;
        guiLabel2.NormalColor = !flag2 ? ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_NEGATIVE) : Color.white;
        num1 += guiLabel2.SizeY;
      }
      float num2;
      if (shipSystem.Card.ShipObjectKeyRestrictions.Count > 0 && shipSystem.Card.ShipObjectKeyRestrictions.Count < 5 && !flag1)
      {
        GuiLabel guiLabel1 = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
        guiLabel1.FontSize = 16;
        guiLabel1.Text = BsgoLocalization.Get("%$bgo.shop_tooltip.required_ship%") + ":";
        this.AddChild((GuiElementBase) guiLabel1, Align.UpLeft, new Vector2(6f, num1 + 8f));
        GuiLabel guiLabel2 = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
        guiLabel2.FontSize = 16;
        guiLabel2.WordWrap = true;
        guiLabel2.SizeX = 240f;
        this.AddChild((GuiElementBase) guiLabel2, Align.UpLeft, new Vector2(guiLabel1.BottomRightCornerWithPadding.x, num1 + 8f));
        string str1 = string.Empty;
        string str2 = string.Empty;
        bool flag2 = false;
        using (List<ShipCard>.Enumerator enumerator = shipSystem.Card.ShipRestrictions.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            ShipCard current = enumerator.Current;
            if (current.Faction == Game.Me.Faction)
            {
              string shipName = BsgoLocalization.GetShipName(current.ItemGUICard.Key);
              str2 = str2 + str1 + shipName;
              str1 = ", ";
              flag2 |= (int) current.HangarID == (int) Game.Me.ActiveShip.Card.HangarID;
            }
          }
        }
        guiLabel2.Text = str2;
        anyRequirementNotMet |= !flag2;
        guiLabel2.NormalColor = !flag2 ? ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_NEGATIVE) : Color.white;
        num2 = num1 + guiLabel2.SizeY;
      }
      else if (!flag1)
      {
        GuiLabel guiLabel1 = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
        guiLabel1.FontSize = 16;
        guiLabel1.Text = BsgoLocalization.Get("%$bgo.shop_tooltip.required_ship_class%") + ":";
        this.AddChild((GuiElementBase) guiLabel1, Align.UpLeft, new Vector2(6f, num1 + 8f));
        GuiLabel guiLabel2 = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
        guiLabel2.FontSize = 16;
        guiLabel2.Text = BsgoLocalization.Get("%$bgo.common.ship_class_tier_" + (object) shipSystem.Card.Tier + "%");
        guiLabel2.WordWrap = true;
        this.AddChild((GuiElementBase) guiLabel2, Align.UpLeft, new Vector2(guiLabel1.BottomRightCornerWithPadding.x, num1 + 8f));
        bool flag2 = (int) shipSystem.Card.Tier == (int) Game.Me.ActiveShip.Card.Tier;
        anyRequirementNotMet |= !flag2;
        guiLabel2.NormalColor = !flag2 ? ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_NEGATIVE) : Color.white;
        num2 = num1 + guiLabel2.SizeY;
      }
      this.ShowRequirementNotMet(anyRequirementNotMet);
      this.ResizePanelToFitChildren();
    }

    public void SetItem(ShipCard ship)
    {
      this.Clear();
      string str = BsgoLocalization.Get("%$bgo.shop_tooltip.requiredLevel%", (object) ship.RequiredPlayerLevel());
      float num = this.headerLabel.BottomRightCornerWithPadding.y;
      GuiLabel guiLabel = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
      guiLabel.FontSize = 16;
      guiLabel.Text = str;
      this.AddChild((GuiElementBase) guiLabel, Align.UpLeft, new Vector2(6f, num + 8f));
      bool flag = (int) Game.Me.Level >= ship.RequiredPlayerLevel();
      guiLabel.NormalColor = !flag ? ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_NEGATIVE) : Color.white;
      this.ShowRequirementNotMet(!flag);
      this.ResizePanelToFitChildren();
    }
  }
}
