﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedRewardTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.GameItem;

namespace Gui.Tooltips
{
  public class GuiAdvancedRewardTooltip : GuiAdvancedTooltipBase
  {
    private const string mc_layout = "GUI/Tooltips/AdvancedShopItemTooltipLayout.txt";
    private RewardCard m_currentReward;
    private GuiShopItemTitlePanel m_rewardTitlePanel;
    private GuiShopDescriptionPanel m_rewardDescriptionPanel;
    private GuiShopStarterKitPanel m_rewardItemListPanel;

    public GuiAdvancedRewardTooltip()
      : base("GUI/Tooltips/AdvancedShopItemTooltipLayout.txt")
    {
      this.EmptyChildren();
      this.m_backgroundImage.IsRendered = true;
      this.AddChild((GuiElementBase) this.m_backgroundImage);
      this.m_rewardTitlePanel = new GuiShopItemTitlePanel();
      this.AddChild((GuiElementBase) this.m_rewardTitlePanel);
      this.m_rewardDescriptionPanel = new GuiShopDescriptionPanel();
      this.AddChild((GuiElementBase) this.m_rewardDescriptionPanel);
      this.m_rewardItemListPanel = new GuiShopStarterKitPanel();
      this.AddChild((GuiElementBase) this.m_rewardItemListPanel);
    }

    public GuiAdvancedRewardTooltip(RewardCard rewardCard)
      : this()
    {
      this.SetItem(rewardCard);
    }

    public override void Draw()
    {
      if (!this.IsRendered || this.m_currentReward == null)
        return;
      base.Draw();
    }

    public void SetItem(RewardCard rewardCard)
    {
      if (rewardCard != null && this.m_currentReward != rewardCard)
      {
        float positionY = 0.0f;
        if (this.m_rewardTitlePanel.SetProperties(rewardCard.GUICard, GuiItemPanelBase.ms_atlasSourceRectSmall))
          positionY = this.SetPanelPostions((GuiItemPanelBase) this.m_rewardTitlePanel, positionY);
        if (this.m_rewardDescriptionPanel.SetProperties(rewardCard.GUICard, GuiItemPanelBase.ms_atlasSourceRectSmall))
          positionY = this.SetPanelPostions((GuiItemPanelBase) this.m_rewardDescriptionPanel, positionY);
        if (this.m_rewardItemListPanel.SetProperties(rewardCard.Items))
          this.SetPanelPostions((GuiItemPanelBase) this.m_rewardItemListPanel, positionY);
      }
      this.m_currentReward = rewardCard;
      this.ResizePanelToFitChildren();
    }

    private float SetPanelPostions(GuiItemPanelBase panel, float positionY)
    {
      panel.PositionY = positionY + panel.ElementPadding.Top;
      panel.SizeX = this.SizeX - (panel.ElementPadding.Left + panel.ElementPadding.Right);
      positionY += panel.SizeY + panel.ElementPadding.Bottom;
      return positionY;
    }
  }
}
