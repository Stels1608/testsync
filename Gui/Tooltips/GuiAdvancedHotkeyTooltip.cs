﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedHotkeyTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedHotkeyTooltip : GuiAdvancedTooltipBase
  {
    private readonly GuiLabel actionLabel;
    private readonly GuiLabel hotkeyLabel;
    private Action hotkeyAction;
    private readonly float labelGap;

    public GuiAdvancedHotkeyTooltip()
      : this(string.Empty, Action.None)
    {
    }

    public GuiAdvancedHotkeyTooltip(string label, Action key)
    {
      this.actionLabel = new GuiLabel(label, Color.white, Gui.Options.fontEurostileTRegCon);
      this.actionLabel.FontSize = 16;
      this.actionLabel.Style.padding = new RectOffset(6, 6, 6, 2);
      this.AddChild((GuiElementBase) this.actionLabel);
      this.hotkeyLabel = new GuiLabel(string.Empty, Color.white, Gui.Options.fontEurostileTRegCon);
      this.hotkeyLabel.Style.padding = new RectOffset(6, 6, 6, 2);
      this.hotkeyLabel.FontSize = 16;
      this.AddChild((GuiElementBase) this.hotkeyLabel);
      this.labelGap = 20f;
      this.SetText(label, key);
    }

    public void SetText(string label, Action hotkey)
    {
      this.actionLabel.Text = label;
      this.hotkeyAction = hotkey;
      this.UpdateHotkeyText();
      this.hotkeyLabel.Position = new Vector2(this.actionLabel.Position.x + this.actionLabel.SizeX + this.labelGap, this.hotkeyLabel.Position.y);
      this.ResizePanelToFitChildren();
    }

    public override void Update()
    {
      this.UpdateHotkeyText();
    }

    private void UpdateHotkeyText()
    {
      if (this.hotkeyAction != Action.None)
      {
        if (!(Tools.GetHotKeyFor(this.hotkeyAction) != this.hotkeyLabel.Text))
          return;
        this.hotkeyLabel.Text = Tools.GetHotKeyFor(this.hotkeyAction);
      }
      else
        this.hotkeyLabel.Text = string.Empty;
    }

    public override string ToString()
    {
      return "Type: " + (object) this.GetType() + "\nLabel: " + this.actionLabel.TextParsed + "\nHot Key: " + this.hotkeyLabel.TextParsed;
    }
  }
}
