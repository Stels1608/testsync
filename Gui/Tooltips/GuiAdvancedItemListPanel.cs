﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedItemListPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedItemListPanel : GuiPanel
  {
    private float y = 4f;
    private readonly GuiLabel headerLabel;

    public GuiAdvancedItemListPanel(string header)
    {
      this.BackgroundImage = new GuiImage("GUI/Tooltips/BG_box_values");
      this.BackgroundImage.NineSliceEdge = (GuiElementPadding) new Rect(16f, 16f, 16f, 16f);
      this.AddChild((GuiElementBase) this.BackgroundImage, Align.UpCenter);
      this.headerLabel = new GuiLabel(Gui.Options.fontEurostileTRegCon);
      this.headerLabel.Style.padding = new RectOffset(8, 8, 6, 6);
      this.headerLabel.FontSize = 16;
      this.headerLabel.NormalColor = Color.gray;
      this.headerLabel.Text = BsgoLocalization.Get(header).ToUpper();
      this.AddChild((GuiElementBase) this.headerLabel, Align.UpLeft);
    }

    public void AddItem(GUICard item)
    {
      GuiImage image = new GuiImage();
      item.SetGuiImage(ref image, GuiAtlasImageBase.smallSize);
      this.AddChild((GuiElementBase) image, Align.UpLeft, new Vector2(8f, this.y));
      GuiLabel guiLabel = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
      guiLabel.Text = item.Name;
      guiLabel.AutoSize = false;
      guiLabel.SizeY = image.SizeY;
      guiLabel.Alignment = TextAnchor.MiddleLeft;
      this.AddChild((GuiElementBase) guiLabel, Align.UpLeft, new Vector2(image.BottomRightCornerWithPadding.x + 4f, this.y));
      this.y += image.SizeY + 2f;
      this.ResizePanelToFitChildren();
    }

    public void Clear()
    {
      this.RemoveAll();
      this.AddChild((GuiElementBase) this.BackgroundImage);
      this.AddChild((GuiElementBase) this.headerLabel, Align.UpLeft);
      this.y = this.headerLabel.BottomRightCornerWithPadding.y;
      this.ResizePanelToFitChildren();
    }
  }
}
