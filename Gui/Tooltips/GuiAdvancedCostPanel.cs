﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedCostPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedCostPanel : GuiPanel
  {
    public override bool IsRendered
    {
      get
      {
        return base.IsRendered;
      }
      set
      {
        base.IsRendered = value;
        if (value)
          return;
        this.SizeY = 0.0f;
      }
    }

    public void SetItem(ShopItemCard shopItemCard, ExchangeType exchangeType)
    {
      this.RemoveAll();
      Price price = this.GetPrice(shopItemCard, exchangeType);
      this.IsRendered = price.items.Count > 0;
      if (!this.IsRendered)
        return;
      float x = 0.0f;
      bool flag = false;
      using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = price.items.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
          if ((double) current.Value > 0.0)
          {
            flag = true;
            float result = current.Value;
            if (exchangeType != ExchangeType.BUY || !Shop.ApplyDiscount(out result, shopItemCard.CardGUID, current.Value))
              ;
            GuiAdvancedCostPanel.CostElement costElement = new GuiAdvancedCostPanel.CostElement(current.Key, result);
            this.AddChild((GuiElementBase) costElement, Align.UpLeft, new Vector2(x, 0.0f));
            x += costElement.SizeX;
          }
        }
      }
      this.IsRendered = flag;
      if (!this.IsRendered)
        return;
      this.ResizePanelToFitChildren();
    }

    private Price GetPrice(ShopItemCard shopItemCard, ExchangeType exchangeType)
    {
      switch (exchangeType)
      {
        case ExchangeType.BUY:
          return shopItemCard.BuyPrice;
        case ExchangeType.SELL:
          return shopItemCard.SellPrice;
        case ExchangeType.UPGRADE:
          return shopItemCard.UpgradePrice;
        default:
          return shopItemCard.BuyPrice;
      }
    }

    private class CostElement : GuiPanel
    {
      public CostElement(ShipConsumableCard currencyCard, float amount)
      {
        GuiImage guiImage = new GuiImage();
        if ((Object) currencyCard.GUICard.GUIIconTexture != (Object) null)
        {
          guiImage.Texture = currencyCard.GUICard.GUIIconTexture;
          guiImage.SourceRect = new Rect(0.0f, 0.0f, 1f, 1f);
        }
        else
        {
          AtlasCache atlasCache = new AtlasCache(float2.FromV2(new Vector2(40f, 35f)));
          AtlasEntry cachedEntryBy = atlasCache.GetCachedEntryBy(currencyCard.GUICard.GUIAtlasTexturePath, (int) currencyCard.GUICard.FrameIndex);
          guiImage.Texture = cachedEntryBy.Texture;
          guiImage.SourceRect = cachedEntryBy.FrameRect;
          guiImage.Size = float2.ToV2(atlasCache.ElementSize);
        }
        this.AddChild((GuiElementBase) guiImage, Align.UpLeft);
        this.AddChild((GuiElementBase) new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon)
        {
          FontSize = 16,
          Text = amount.ToString("#0.0")
        }, Align.UpLeft, new Vector2(guiImage.BottomRightCornerWithPadding.x, 0.0f));
        this.ResizePanelToFitChildren();
      }
    }
  }
}
