﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedCardTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedCardTooltip : GuiAdvancedTooltipBase
  {
    protected const int PADDING = 6;
    protected readonly GuiImage itemIcon;
    protected readonly GuiLabel nameLabel;
    protected readonly GuiImage innerBackground;
    protected readonly GuiAdvancedDescriptionPanel descriptionPanel;
    protected readonly GuiLabel itemLevelLabel;
    protected readonly GuiImage systemTypeIcon;
    protected readonly GuiLabel systemTypeLabel;
    private GuiAdvancedSalePanel salePanel;

    protected GuiAdvancedSalePanel SalePanel
    {
      get
      {
        if (this.salePanel == null)
        {
          this.salePanel = new GuiAdvancedSalePanel();
          this.salePanel.IgnoreOnLayout = true;
          this.AddChild((GuiElementBase) this.salePanel, Align.UpLeft, new Vector2(0.0f, (float) (-(double) this.salePanel.SizeY + 2.0)));
        }
        return this.salePanel;
      }
    }

    public int DistanceFromBorder
    {
      get
      {
        return 15;
      }
    }

    public GuiAdvancedCardTooltip()
    {
      this.innerBackground = new GuiImage("GUI/Tooltips/BG_box2");
      this.innerBackground.IgnoreOnLayout = true;
      this.innerBackground.NineSliceEdge = (GuiElementPadding) new Rect(16f, 16f, 16f, 16f);
      this.AddChild((GuiElementBase) this.innerBackground, Align.UpCenter);
      this.itemIcon = new GuiImage();
      this.AddChild((GuiElementBase) this.itemIcon, Align.UpLeft, new Vector2(9f, 9f));
      this.nameLabel = new GuiLabel(Gui.Options.fontEurostileTRegCon);
      this.nameLabel.AutoSize = false;
      this.nameLabel.Size = new Vector2(260f, 48f);
      this.nameLabel.Alignment = TextAnchor.MiddleLeft;
      this.nameLabel.FontSize = 18;
      this.AddChild((GuiElementBase) this.nameLabel, Align.UpLeft, new Vector2((float) ((double) this.itemIcon.PositionX + 40.0 + 6.0), 4f));
      this.systemTypeIcon = new GuiImage();
      this.systemTypeIcon.IsRendered = false;
      this.AddChild((GuiElementBase) this.systemTypeIcon, Align.UpLeft, new Vector2(18f, this.itemIcon.BottomRightCornerWithPadding.y));
      this.systemTypeLabel = new GuiLabel(Gui.Options.fontEurostileTRegCon);
      this.systemTypeLabel.FontSize = 16;
      this.systemTypeLabel.IsRendered = false;
      this.systemTypeLabel.AutoSize = false;
      this.systemTypeLabel.WordWrap = true;
      this.systemTypeLabel.Size = new Vector2(200f, 44f);
      this.systemTypeLabel.Alignment = TextAnchor.MiddleLeft;
      this.AddChild((GuiElementBase) this.systemTypeLabel, Align.UpLeft, new Vector2(6f, this.itemIcon.BottomRightCornerWithPadding.y));
      this.itemLevelLabel = new GuiLabel(Gui.Options.fontEurostileTRegCon);
      this.itemLevelLabel.WordWrap = false;
      this.itemLevelLabel.FontSize = 16;
      this.itemLevelLabel.IsRendered = false;
      this.AddChild((GuiElementBase) this.itemLevelLabel, Align.UpRight, new Vector2(-18f, 6f));
      this.descriptionPanel = new GuiAdvancedDescriptionPanel();
      this.AddChild((GuiElementBase) this.descriptionPanel, Align.UpCenter);
    }

    protected void SetGuiCard(GUICard guiCard)
    {
      AtlasCache atlasCache = new AtlasCache(float2.FromV2(new Vector2(40f, 35f)));
      AtlasEntry cachedEntryBy = atlasCache.GetCachedEntryBy(guiCard.GUIAtlasTexturePath, (int) guiCard.FrameIndex);
      this.itemIcon.Texture = cachedEntryBy.Texture;
      this.itemIcon.SourceRect = cachedEntryBy.FrameRect;
      this.itemIcon.Size = float2.ToV2(atlasCache.ElementSize);
      this.nameLabel.WordWrap = false;
      this.nameLabel.Text = guiCard.Name.ToUpper();
      this.descriptionPanel.SetText(this.GetDescriptionText(guiCard));
      this.itemLevelLabel.IsRendered = (int) guiCard.Level > 0;
      this.itemLevelLabel.Text = BsgoLocalization.Get("%$bgo.etc.dw_level%").ToUpper() + " " + (object) guiCard.Level;
      this.systemTypeIcon.PositionY = this.itemIcon.BottomRightCornerWithPadding.y + 6f;
      this.systemTypeLabel.PositionY = this.systemTypeIcon.PositionY;
      this.itemLevelLabel.PositionY = this.systemTypeLabel.PositionY;
    }

    protected void SetGameItem(GameItemCard gameItem)
    {
      ShipSystem shipSystem = gameItem as ShipSystem;
      if (shipSystem != null)
        this.SetShipSystem(shipSystem.Card);
      ItemCountable countable = gameItem as ItemCountable;
      if (countable != null)
        this.SetItemCountable(countable);
      ShipCard ship = gameItem as ShipCard;
      if (ship != null)
        this.SetShip(ship);
      StarterKit starterKit = gameItem as StarterKit;
      if (starterKit == null)
        return;
      this.SetStarterKit(starterKit.Card);
    }

    private void SetShip(ShipCard ship)
    {
      this.systemTypeIcon.Texture = Resources.Load<Texture2D>(ship.Faction != Faction.Colonial ? "GUI/EquipBuyPanel/icons/icon_ship_cylon" : "GUI/EquipBuyPanel/icons/icon_ship_human");
      this.systemTypeIcon.IsRendered = true;
      this.systemTypeLabel.PositionX = this.systemTypeIcon.BottomRightCornerWithPadding.x + 4f;
      this.systemTypeLabel.Text = BsgoLocalization.Get("bgo.shop_tooltip.category.ship").ToUpper();
      this.systemTypeLabel.IsRendered = true;
    }

    private void SetShipSystem(ShipSystemCard systemCard)
    {
      this.systemTypeIcon.Texture = GuiAdvancedCardTooltip.GetSystemIcon(systemCard.SlotType);
      this.systemTypeIcon.IsRendered = true;
      this.systemTypeLabel.PositionX = this.systemTypeIcon.BottomRightCornerWithPadding.x + 4f;
      this.systemTypeLabel.Text = BsgoLocalization.Get("%$bgo.stats." + (object) systemCard.SlotType + "%").ToUpper();
      this.systemTypeLabel.IsRendered = true;
    }

    private void SetItemCountable(ItemCountable countable)
    {
      this.systemTypeIcon.Texture = GuiAdvancedCardTooltip.GetCountableIcon(countable);
      this.systemTypeIcon.IsRendered = true;
      this.systemTypeLabel.PositionX = this.systemTypeIcon.BottomRightCornerWithPadding.x + 4f;
      this.systemTypeLabel.Text = GuiAdvancedCardTooltip.GetCountableName(countable).ToUpper();
      this.systemTypeLabel.IsRendered = true;
    }

    private void SetStarterKit(StarterKitCard starterKit)
    {
      this.systemTypeIcon.Texture = Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_bonus");
      this.systemTypeIcon.IsRendered = true;
      this.systemTypeLabel.PositionX = this.systemTypeIcon.BottomRightCornerWithPadding.x + 4f;
      this.systemTypeLabel.Text = BsgoLocalization.Get("bgo.EquipBuyPanel.gui_typefilter_layout.label_7").ToUpper();
      this.systemTypeLabel.IsRendered = true;
    }

    private static string GetCountableName(ItemCountable itemCountable)
    {
      if (itemCountable.IsResource)
        return BsgoLocalization.Get("%$bgo.EquipBuyPanel.gui_typefilter_layout.label_6%");
      switch (itemCountable.ShopItemCard.ItemType)
      {
        case ShopItemType.Augment:
          return BsgoLocalization.Get("bgo.shop_tooltip.category.booster");
        case ShopItemType.Round:
        case ShopItemType.Flare:
        case ShopItemType.Mine:
        case ShopItemType.Missile:
        case ShopItemType.Power:
        case ShopItemType.Repair:
        case ShopItemType.PointDefense:
        case ShopItemType.Flak:
        case ShopItemType.JumpTargetTransponder:
        case ShopItemType.Radio:
        case ShopItemType.Torpedo:
        case ShopItemType.MetalPlate:
        case ShopItemType.AntiCapitalMissile:
          return BsgoLocalization.Get("bgo.shop_tooltip.category.consumable");
        case ShopItemType.Junk:
          return BsgoLocalization.Get("bgo.shop_tooltip.category.junk");
        case ShopItemType.StarterKit:
          return BsgoLocalization.Get("bgo.shop_tooltip.category.starter_kit");
        default:
          return "Unknown category<PH>";
      }
    }

    private static Texture2D GetCountableIcon(ItemCountable itemCountable)
    {
      if (itemCountable.IsResource)
        return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_resources");
      switch (itemCountable.ShopItemCard.ItemType)
      {
        case ShopItemType.Augment:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_booster");
        case ShopItemType.Round:
        case ShopItemType.Flare:
        case ShopItemType.Mine:
        case ShopItemType.Missile:
        case ShopItemType.Power:
        case ShopItemType.Repair:
        case ShopItemType.PointDefense:
        case ShopItemType.Flak:
        case ShopItemType.JumpTargetTransponder:
        case ShopItemType.Radio:
        case ShopItemType.Torpedo:
        case ShopItemType.MetalPlate:
        case ShopItemType.AntiCapitalMissile:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_consumables");
        case ShopItemType.Junk:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_junk");
        case ShopItemType.StarterKit:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_bonus");
        default:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_unknown");
      }
    }

    private static Texture2D GetSystemIcon(ShipSlotType slotType)
    {
      switch (slotType)
      {
        case ShipSlotType.computer:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_computer");
        case ShipSlotType.engine:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_engine");
        case ShipSlotType.hull:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_hull");
        case ShipSlotType.weapon:
        case ShipSlotType.launcher:
        case ShipSlotType.defensive_weapon:
        case ShipSlotType.gun:
        case ShipSlotType.special_weapon:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_weapons");
        case ShipSlotType.ship_paint:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_paints");
        case ShipSlotType.avionics:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_avionics");
        case ShipSlotType.role:
          return Resources.Load<Texture2D>("GUI/EquipBuyPanel/icons/icon_bonus");
        default:
          throw new ArgumentOutOfRangeException("slotType");
      }
    }

    public override void ResizePanelToFitChildren()
    {
      float num = (float) ((double) this.nameLabel.PositionX + (double) TextUtility.CalcWidth(this.nameLabel.TextParsed, this.nameLabel.Style) + 24.0);
      this.descriptionPanel.Wrap(360f);
      if ((double) num > 360.0)
      {
        this.nameLabel.WordWrap = true;
        this.nameLabel.SizeX = (float) (360.0 - (double) this.nameLabel.PositionX - 24.0);
        Vector2 textSize = this.nameLabel.GetTextSize();
        bool flag = false;
        while ((double) textSize.y > (double) this.nameLabel.Size.y)
        {
          this.nameLabel.Text = this.nameLabel.Text.Substring(0, this.nameLabel.Text.Length - 2);
          textSize = this.nameLabel.GetTextSize();
          flag = true;
        }
        if (flag)
          this.nameLabel.Text += "...";
      }
      this.descriptionPanel.PositionY = this.GetDescriptionYPosition();
      base.ResizePanelToFitChildren();
      if ((double) this.SizeX < 360.0)
        this.SizeX = 360f;
      this.OnCustomLayout();
      this.innerBackground.SizeX = (float) ((double) this.SizeX - 12.0 - 6.0);
      this.innerBackground.SizeY = this.descriptionPanel.BottomRightCornerWithPadding.y - this.itemIcon.BottomRightCornerWithPadding.y;
      this.innerBackground.PositionY = this.itemIcon.BottomRightCornerWithPadding.y + 2f;
    }

    protected virtual void OnCustomLayout()
    {
      this.SizeY += 12f;
    }

    protected virtual string GetDescriptionText(GUICard guiCard)
    {
      return guiCard.Description;
    }

    protected virtual float GetDescriptionYPosition()
    {
      float num = this.itemIcon.BottomRightCornerWithPadding.y + 6f;
      if (this.systemTypeIcon.IsRendered)
        num = this.systemTypeIcon.BottomRightCornerWithPadding.y + 8f;
      return num;
    }

    protected void CheckForSale(uint guid)
    {
      ShopDiscount itemDiscount = Shop.FindItemDiscount(guid);
      bool flag = false;
      foreach (HangarShip ship in Game.Me.Hangar.Ships)
      {
        if ((int) ship.GUID == (int) guid)
        {
          flag = true;
          break;
        }
      }
      if (!flag && itemDiscount != null)
      {
        this.SalePanel.SetDiscount(itemDiscount);
        this.SalePanel.SizeX = this.SizeX - 10f;
        this.SalePanel.IsRendered = true;
      }
      else
        this.SalePanel.IsRendered = false;
    }

    public void ShowUpgradeSaleInfo(ShipItem shipItem)
    {
      this.SalePanel.IsRendered = false;
      ShipSystem shipSystem = shipItem as ShipSystem;
      if (shipSystem == null)
        return;
      ShopDiscount discountInUpgradeLevels = shipSystem.FindDiscountInUpgradeLevels();
      if (discountInUpgradeLevels == null)
        return;
      this.SalePanel.SizeX = this.SizeX - 9f;
      this.SalePanel.SetDiscount(discountInUpgradeLevels);
      this.SalePanel.IsRendered = true;
    }
  }
}
