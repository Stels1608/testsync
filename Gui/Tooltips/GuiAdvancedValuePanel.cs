﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedValuePanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui.Tooltips
{
  internal class GuiAdvancedValuePanel : GuiPanel
  {
    private int y = 4;
    private readonly List<GuiLabel> maxLevelLabels = new List<GuiLabel>();
    private const int PADDING = 8;

    public int FontSize { get; set; }

    public override bool IsRendered
    {
      get
      {
        return base.IsRendered;
      }
      set
      {
        base.IsRendered = value;
        if (value)
          return;
        this.SizeY = 0.0f;
      }
    }

    public GuiAdvancedValuePanel()
    {
      this.BackgroundImage = new GuiImage("GUI/Tooltips/BG_box_values");
      this.BackgroundImage.NineSliceEdge = new GuiElementPadding(12f, 12f, 12f, 12f);
      this.AddChild((GuiElementBase) this.BackgroundImage);
      this.FontSize = 16;
    }

    public void AddEntry(string key, string value, Color color)
    {
      this.AddEntry((Texture2D) null, key, value, color, (string) null);
    }

    public void AddEntry(Texture2D icon, string key, string value, Color color, string maxLevelValue = null)
    {
      GuiLabel label1 = this.CreateLabel(key, color);
      bool flag = false;
      while ((double) label1.SizeX > 150.0)
      {
        label1.Text = label1.Text.Substring(0, label1.Text.Length - 2);
        flag = true;
      }
      if (flag)
        label1.Text += "...";
      GuiLabel label2 = this.CreateLabel(value, color);
      this.AddChild((GuiElementBase) label1, Align.UpLeft, new Vector2(8f, (float) this.y));
      this.AddChild((GuiElementBase) label2, Align.UpLeft, new Vector2(188f, (float) this.y));
      if ((Object) icon != (Object) null)
      {
        GuiImage guiImage = new GuiImage(icon);
        guiImage.Size = new Vector2(12f, 12f);
        this.AddChild((GuiElementBase) guiImage, Align.UpLeft, new Vector2(label1.BottomRightCornerWithPadding.x + 2f, (float) (this.y + 2)));
      }
      if (maxLevelValue != null)
      {
        GuiLabel label3 = this.CreateLabel(maxLevelValue, Color.gray);
        this.AddChild((GuiElementBase) label3, Align.UpLeft, new Vector2(label2.BottomRightCornerWithPadding.x + 8f, (float) this.y));
        this.maxLevelLabels.Add(label3);
      }
      this.y += (int) label1.SizeY - 2;
      this.ResizePanelToFitChildren();
      this.ShowMaxLevelValues(false);
    }

    public void AddEntry(string key, string value)
    {
      this.AddEntry(key, value, Color.white);
    }

    public void ShowMaxLevelValues(bool show)
    {
      using (List<GuiLabel>.Enumerator enumerator = this.maxLevelLabels.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.IsRendered = show;
      }
    }

    public void Clear()
    {
      this.RemoveAll();
      this.maxLevelLabels.Clear();
      this.AddChild((GuiElementBase) this.BackgroundImage);
      this.y = 4;
    }

    private GuiLabel CreateLabel(string text, Color color)
    {
      return new GuiLabel(color, Gui.Options.fontEurostileTRegCon) { Text = text, FontSize = this.FontSize };
    }
  }
}
