﻿// Decompiled with JetBrains decompiler
// Type: Gui.Tooltips.GuiAdvancedShopItemTooltip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui.Tooltips
{
  public class GuiAdvancedShopItemTooltip : GuiAdvancedCardTooltip
  {
    private readonly GuiAdvancedValuePanel valuePanel;
    private readonly GuiLabel classRestrictionLabel;
    private readonly GuiLabel primaryStatLabel;
    private readonly GuiAdvancedCostPanel costPanel;
    private readonly GuiAdvancedRequirementsPanel requirementsPanel;
    private readonly GuiLabel controlTypeLabel;
    private readonly GuiLabel durabilityLabel;
    private readonly GuiLabel maxLevelNoticeLabel;
    private string primaryStatCurrent;
    private string primaryStatMax;
    private GameItemCard lastItem;

    private float MaxY
    {
      get
      {
        float num = this.requirementsPanel.BottomRightCornerWithPadding.y + 8f;
        if (this.maxLevelNoticeLabel.IsRendered)
          num = this.maxLevelNoticeLabel.BottomRightCornerWithPadding.y;
        else if (this.costPanel.IsRendered)
          num = this.costPanel.BottomRightCornerWithPadding.y;
        else if (this.durabilityLabel.IsRendered)
          num = this.durabilityLabel.BottomRightCornerWithPadding.y;
        return num;
      }
    }

    public GuiAdvancedShopItemTooltip()
    {
      this.classRestrictionLabel = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
      this.classRestrictionLabel.FontSize = 16;
      this.AddChild((GuiElementBase) this.classRestrictionLabel, Align.UpLeft, new Vector2(18f, 0.0f));
      this.primaryStatLabel = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
      this.primaryStatLabel.FontSize = 20;
      this.primaryStatLabel.NormalColor = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_SPECIAL);
      this.primaryStatLabel.Style.richText = true;
      this.AddChild((GuiElementBase) this.primaryStatLabel, Align.UpLeft, new Vector2(172f, 0.0f));
      this.valuePanel = new GuiAdvancedValuePanel();
      this.AddChild((GuiElementBase) this.valuePanel, Align.UpCenter);
      this.costPanel = new GuiAdvancedCostPanel();
      this.AddChild((GuiElementBase) this.costPanel, Align.UpLeft, new Vector2(18f, 0.0f));
      this.requirementsPanel = new GuiAdvancedRequirementsPanel();
      this.AddChild((GuiElementBase) this.requirementsPanel, Align.UpCenter);
      this.controlTypeLabel = new GuiLabel(Gui.Options.fontEurostileTRegCon);
      this.controlTypeLabel.WordWrap = false;
      this.controlTypeLabel.FontSize = 16;
      this.controlTypeLabel.NormalColor = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_UPGRADABLE);
      this.AddChild((GuiElementBase) this.controlTypeLabel, Align.UpRight, new Vector2(-18f, 6f));
      this.durabilityLabel = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
      this.durabilityLabel.FontSize = 16;
      this.AddChild((GuiElementBase) this.durabilityLabel, Align.UpRight, new Vector2(-18f, 0.0f));
      this.maxLevelNoticeLabel = new GuiLabel(Color.white, Gui.Options.fontEurostileTRegCon);
      this.maxLevelNoticeLabel.FontSize = 15;
      this.maxLevelNoticeLabel.Style.richText = true;
      this.maxLevelNoticeLabel.Text = BsgoLocalization.Get("bgo.shop_tooltip.show_max_level_values").ToUpper();
      this.AddChild((GuiElementBase) this.maxLevelNoticeLabel, Align.UpRight, new Vector2(-18f, 0.0f));
    }

    public void SetShopItem(GameItemCard item, ShopInventoryContainer containerType = ShopInventoryContainer.Store, bool condensedVersion = false, bool resetItem = false, bool isSlotItem = false)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      GuiAdvancedShopItemTooltip.\u003CSetShopItem\u003Ec__AnonStorey92 itemCAnonStorey92 = new GuiAdvancedShopItemTooltip.\u003CSetShopItem\u003Ec__AnonStorey92();
      // ISSUE: reference to a compiler-generated field
      itemCAnonStorey92.\u003C\u003Ef__this = this;
      if (item == null || item == this.lastItem && !resetItem)
        return;
      ExchangeType exchangeType = containerType != ShopInventoryContainer.Store || isSlotItem ? ExchangeType.SELL : ExchangeType.BUY;
      this.systemTypeLabel.SizeX = 200f;
      this.SetGuiCard(item.ItemGUICard);
      // ISSUE: reference to a compiler-generated field
      itemCAnonStorey92.shipSystem = item as ShipSystem;
      this.valuePanel.Clear();
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      bool flag = itemCAnonStorey92.shipSystem != null && itemCAnonStorey92.shipSystem.Card.SlotType != ShipSlotType.ship_paint;
      this.valuePanel.IsRendered = flag;
      // ISSUE: reference to a compiler-generated field
      this.classRestrictionLabel.IsRendered = itemCAnonStorey92.shipSystem != null;
      this.durabilityLabel.IsRendered = flag;
      // ISSUE: reference to a compiler-generated field
      this.requirementsPanel.IsRendered = itemCAnonStorey92.shipSystem != null;
      this.primaryStatLabel.IsRendered = flag;
      this.maxLevelNoticeLabel.IsRendered = flag;
      this.controlTypeLabel.IsRendered = flag;
      this.SetGameItem(item);
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      if (itemCAnonStorey92.shipSystem != null && itemCAnonStorey92.shipSystem.Card != null && (bool) itemCAnonStorey92.shipSystem.Card.IsLoaded)
      {
        // ISSUE: reference to a compiler-generated field
        this.classRestrictionLabel.Text = BsgoLocalization.Get("%$bgo.shop_tooltip.ship_class%", (object) BsgoLocalization.Get("%$bgo.common.ship_class_tier_" + (object) itemCAnonStorey92.shipSystem.Card.Tier + "%")).ToUpper();
        this.classRestrictionLabel.PositionY = this.itemIcon.BottomRightCornerWithPadding.y + 6f;
        this.itemLevelLabel.PositionY = this.classRestrictionLabel.PositionY;
        this.systemTypeIcon.PositionY = this.classRestrictionLabel.BottomRightCornerWithPadding.y;
        this.systemTypeLabel.PositionY = this.systemTypeIcon.TopLeftCornerWithPadding.y;
        // ISSUE: reference to a compiler-generated field
        this.SetPrimaryStat(itemCAnonStorey92.shipSystem);
        // ISSUE: reference to a compiler-generated field
        this.controlTypeLabel.Text = "[" + this.GetControlTypeText(itemCAnonStorey92.shipSystem).ToUpper() + "]";
        this.controlTypeLabel.PositionY = this.systemTypeIcon.PositionY + 6f;
        // ISSUE: reference to a compiler-generated field
        this.SetValues(itemCAnonStorey92.shipSystem);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        this.maxLevelNoticeLabel.IsRendered = flag && (int) itemCAnonStorey92.shipSystem.Card.Level < (int) itemCAnonStorey92.shipSystem.Card.MaxLevel;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        itemCAnonStorey92.shipSystem.IsLoaded.AddHandler(new SignalHandler(itemCAnonStorey92.\u003C\u003Em__10D));
        // ISSUE: reference to a compiler-generated field
        itemCAnonStorey92.shipSystem.IsLoaded.Set();
        // ISSUE: reference to a compiler-generated field
        SystemsStatsGenerator.StatInfoDesc durabilityDescription = SystemsStatsGenerator.GetDurabilityDescription(itemCAnonStorey92.shipSystem);
        this.durabilityLabel.Text = durabilityDescription.Name + ": " + durabilityDescription.StatFormatedValue;
        // ISSUE: reference to a compiler-generated field
        this.durabilityLabel.IsRendered = (double) itemCAnonStorey92.shipSystem.Card.Durability > 1.0;
      }
      this.costPanel.SetItem(item.ShopItemCard, exchangeType);
      this.maxLevelNoticeLabel.PositionY = this.costPanel.BottomRightCornerWithPadding.y;
      this.ResizePanelToFitChildren();
      this.CheckForSale(item.ItemGUICard.CardGUID);
      this.lastItem = item;
    }

    private void SetPrimaryStat(ShipSystem shipSystem)
    {
      if (SystemsStatsGenerator.HasPrimaryStat(shipSystem))
      {
        this.primaryStatCurrent = SystemsStatsGenerator.GetPrimaryStatString(shipSystem, false);
        if (shipSystem.Card.SlotType != ShipSlotType.ship_paint && (int) shipSystem.Card.Level < (int) shipSystem.Card.MaxLevel)
        {
          ShipSystemCard shipSystemCard = shipSystem.Card;
          ShipSystemCard maxLevelCard = shipSystem.GetMaxLevelCard();
          shipSystem.Card = maxLevelCard;
          this.primaryStatMax = SystemsStatsGenerator.GetPrimaryStatString(shipSystem, false);
          shipSystem.Card = shipSystemCard;
        }
        else
          this.primaryStatMax = string.Empty;
        this.primaryStatLabel.Text = this.primaryStatCurrent;
        this.primaryStatLabel.PositionY = this.systemTypeIcon.PositionY + 4f;
        this.systemTypeLabel.SizeX = 100f;
        this.primaryStatLabel.IsRendered = true;
      }
      else
        this.primaryStatLabel.IsRendered = false;
    }

    private string GetControlTypeText(ShipSystem shipSystem)
    {
      return BsgoLocalization.Get(shipSystem.Card.AbilityCards.Length <= 0 ? "%$bgo.shop_tooltip.passive%" : (!shipSystem.Card.AbilityCards[0].Auto ? "%$bgo.shop_tooltip.active%" : "%$bgo.shop_tooltip.toggle%"));
    }

    private void SetValues(ShipSystem shipSystem)
    {
      List<SystemsStatsGenerator.StatInfoDesc> descriptions1 = SystemsStatsGenerator.GenerateDescriptions(shipSystem, shipSystem.Card.NextCard, false);
      ShipSystemCard shipSystemCard = shipSystem.Card;
      ShipSystemCard maxLevelCard = shipSystem.GetMaxLevelCard();
      shipSystem.Card = maxLevelCard;
      List<SystemsStatsGenerator.StatInfoDesc> descriptions2 = SystemsStatsGenerator.GenerateDescriptions(shipSystem);
      shipSystem.Card = shipSystemCard;
      for (int index = 0; index < descriptions1.Count; ++index)
      {
        Color color = !descriptions1[index].Upgradeable ? Color.white : ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_UPGRADABLE);
        Texture2D icon = !descriptions1[index].Upgradeable ? (Texture2D) null : Resources.Load<Texture2D>("GUI/Tooltips/upgrade");
        if (descriptions1[index].Upgradeable)
          this.valuePanel.AddEntry(icon, descriptions1[index].Name, descriptions1[index].StatFormatedValue, color, descriptions2[index].StatFormatedValue);
        else
          this.valuePanel.AddEntry(icon, descriptions1[index].Name, descriptions1[index].StatFormatedValue, color, (string) null);
      }
      this.valuePanel.IsRendered = descriptions1.Count > 0 && shipSystem.Card.SlotType != ShipSlotType.ship_paint;
    }

    public override void Update()
    {
      base.Update();
      if ((Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift)) && this.maxLevelNoticeLabel.IsRendered)
      {
        this.ShowMaxLevelValues(true);
      }
      else
      {
        if (!Input.GetKeyUp(KeyCode.LeftShift) && !Input.GetKeyUp(KeyCode.RightShift))
          return;
        this.ShowMaxLevelValues(false);
      }
    }

    private void ShowMaxLevelValues(bool show)
    {
      this.valuePanel.ShowMaxLevelValues(show);
      this.primaryStatLabel.Text = !show || string.IsNullOrEmpty(this.primaryStatMax) ? this.primaryStatCurrent : this.primaryStatCurrent + "   <color=grey>" + this.primaryStatMax + "</color>";
      ShipSystem system = this.lastItem as ShipSystem;
      if (show && system != null)
      {
        ShipSystemCard maxLevelCard = system.GetMaxLevelCard();
        this.durabilityLabel.Text = SystemsStatsGenerator.GetDurabilityDescription(system).Name + ": " + (object) maxLevelCard.Durability;
        this.durabilityLabel.NormalColor = Color.grey;
      }
      else if (system != null)
      {
        SystemsStatsGenerator.StatInfoDesc durabilityDescription = SystemsStatsGenerator.GetDurabilityDescription(system);
        this.durabilityLabel.Text = durabilityDescription.Name + ": " + durabilityDescription.StatFormatedValue;
        this.durabilityLabel.NormalColor = Color.white;
      }
      if (string.IsNullOrEmpty(this.primaryStatMax))
        return;
      this.controlTypeLabel.IsRendered = !show;
    }

    public override void ResizePanelToFitChildren()
    {
      this.valuePanel.PositionY = this.systemTypeIcon.BottomRightCornerWithPadding.y + 2f;
      base.ResizePanelToFitChildren();
      this.valuePanel.SizeX = this.SizeX - (float) (2 * this.DistanceFromBorder);
      this.requirementsPanel.SizeX = this.SizeX - (float) (2 * this.DistanceFromBorder);
      float num = this.requirementsPanel.BottomRightCornerWithPadding.y + 8f;
      if (this.costPanel.IsRendered)
        num = this.costPanel.BottomRightCornerWithPadding.y;
      else if (this.durabilityLabel.IsRendered)
        num = this.durabilityLabel.BottomRightCornerWithPadding.y;
      this.innerBackground.SizeY = (float) ((double) num - (double) this.itemIcon.BottomRightCornerWithPadding.y - 6.0);
    }

    protected override void OnCustomLayout()
    {
      this.requirementsPanel.PositionY = this.descriptionPanel.BottomRightCornerWithPadding.y;
      this.costPanel.PositionY = this.requirementsPanel.BottomRightCornerWithPadding.y + 3f;
      this.durabilityLabel.PositionY = this.costPanel.PositionY;
      this.maxLevelNoticeLabel.PositionY = !this.costPanel.IsRendered ? this.durabilityLabel.BottomRightCornerWithPadding.y : this.costPanel.BottomRightCornerWithPadding.y;
      this.SizeY = this.MaxY + 6f;
    }

    protected override float GetDescriptionYPosition()
    {
      if (this.valuePanel.IsRendered)
        return this.valuePanel.BottomRightCornerWithPadding.y;
      return base.GetDescriptionYPosition();
    }

    protected override string GetDescriptionText(GUICard guiCard)
    {
      return guiCard.ShortDescription;
    }
  }
}
