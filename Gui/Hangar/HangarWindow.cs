﻿// Decompiled with JetBrains decompiler
// Type: Gui.Hangar.HangarWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Hangar
{
  public class HangarWindow : GuiDialog
  {
    private readonly ShipIcon[][] shipLayoutArray = new ShipIcon[3][];
    private readonly GuiButton command = new GuiButton("%$bgo.hangar.button_command%");
    private const int HANGAR_COL_NUM = 3;
    private const int HANGAR_ROW_NUM = 5;
    private readonly ShipIcon carrierShipIcon;
    private ShipIcon shipSelected;
    private GuiButton variantSelected;
    private byte selectedHangardId;
    private GUISelectVariant selectVariantWindow;

    public AnonymousDelegate HandlerClose
    {
      set
      {
        this.OnClose = value;
      }
    }

    public HangarWindow()
    {
      this.enabled = false;
      this.IsRendered = false;
      this.Align = Align.MiddleCenter;
      this.Size = new Vector2(640f, 460f);
      this.selectedHangardId = Game.Me.ActiveShip.Card.HangarID;
      this.OnClose = (AnonymousDelegate) (() =>
      {
        this.IsRendered = false;
        Game.UnregisterDialog((IGUIPanel) this);
      });
      this.AddChild((GuiElementBase) new GuiLabel("%$bgo.hangar.button_select_ship%", Gui.Options.FontBGM_BT, 16), Align.UpCenter, new Vector2(0.0f, -25f));
      this.AddChild((GuiElementBase) new Gui.Timer(0.5f));
      StaticCards.Instance.ShipListCard.IsLoaded.AddHandler((SignalHandler) (() => this.FillShips(StaticCards.Instance.ShipListCard)));
    }

    public override void OnShow()
    {
      base.OnShow();
      FeedbackProtocol.GetProtocol().ReportUiElementShown(FeedbackProtocol.UiElementId.HangarWindow);
    }

    public override void OnHide()
    {
      base.OnHide();
      FeedbackProtocol.GetProtocol().ReportUiElementHidden(FeedbackProtocol.UiElementId.HangarWindow);
    }

    private void IsShipSelected(ShipIcon ship)
    {
      if (ship.IsCommanded())
      {
        this.shipSelected = ship;
      }
      else
      {
        for (int index = 0; index < ship.variantIcons.Length; ++index)
        {
          ShipCard variantShipCard = StaticCards.Instance.ShipListCard.GetVariantShipCard(ship.Card, (uint) index);
          if (variantShipCard != null && (int) Game.Me.ActiveShip.Card.HangarID == (int) variantShipCard.HangarID)
            this.shipSelected = ship;
        }
      }
    }

    private void FillShips(ShipListCard cardList)
    {
      Faction faction = !(Game.Me.AvatarDesc.GetItem(AvatarItem.Race) == "human") ? Faction.Cylon : Faction.Colonial;
      float num1 = 40f;
      float num2 = 80f;
      float num3 = (float) (((double) this.SizeX - (double) num1) / 3.0);
      float num4 = (float) (((double) this.SizeY - (double) num2) / 6.0);
      for (int index1 = 0; index1 < this.shipLayoutArray.Length; ++index1)
      {
        this.shipLayoutArray[index1] = new ShipIcon[5];
        for (int index2 = 0; index2 < 5; ++index2)
        {
          ShipCard shipCard = cardList.GetShipCard((byte) (index1 + 1), (ShipRoleDeprecated) (index2 + 1));
          if (index2 == 4)
          {
            if (index1 == 0)
              shipCard = cardList.GetShipCard((byte) 16);
            else if (index1 == 2)
              shipCard = cardList.GetShipCard((byte) 15);
            else
              continue;
          }
          if (shipCard != null)
          {
            this.shipLayoutArray[index1][index2] = new ShipIcon(faction, shipCard, shipCard.HaveShip, shipCard.GetHangarShip != null && shipCard.GetHangarShip.IsUpgraded, Game.Me.ActiveShip.Card, true);
            Vector2 iconPosition = new Vector2(num3 * (float) (index1 - 1), num4 * (float) (index2 - 2));
            this.AddShipIconAndVariant(this.shipLayoutArray[index1][index2], iconPosition);
          }
        }
      }
      this.command.Position = new Vector2(-20f, -10f);
      this.command.Pressed = (AnonymousDelegate) (() => new AskBox("%$bgo.hangar.ask%", (AnonymousDelegate) (() => this.CommandPressed())).Show(this.Parent as GuiPanel));
      this.AddChild((GuiElementBase) this.command, Align.DownRight);
    }

    private bool IsCarrierSpawnLevel()
    {
      return !RoomLevel.GetLevel().WorldCard.PrefabName.Contains("outpost");
    }

    private void AddShipIconAndVariant(ShipIcon icon, Vector2 iconPosition)
    {
      icon.OnSelected = new System.Action<ShipIcon>(this.ItemPressed);
      icon.Position = iconPosition;
      if (this.shipSelected == null)
        this.IsShipSelected(icon);
      this.AddChild((GuiElementBase) icon, Align.MiddleCenter);
      for (int index = 0; index < icon.variantIcons.Length; ++index)
      {
        GuiButton guiButton = icon.variantIcons[index];
        if (icon.Card.VariantHangarIDs.Count > index && Game.Me.Hangar[(ushort) icon.Card.VariantHangarIDs[index]] != null)
          guiButton.NormalTexture = (int) Game.Me.ActiveShip.Card.HangarID != (int) (byte) icon.Card.VariantHangarIDs[index] ? ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Button_light") : ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Buttons_fill");
        guiButton.OverTexture = guiButton.NormalTexture;
        guiButton.PressedTexture = guiButton.NormalTexture;
        this.AddChild((GuiElementBase) guiButton, Align.MiddleCenter);
      }
    }

    private void CommandPressed()
    {
      HangarShip ship = (HangarShip) null;
      if (this.variantSelected != null)
      {
        int varIndex;
        ShipIcon shipIcon = this.GetShipIcon(this.variantSelected, out varIndex);
        if (shipIcon != null)
        {
          ShipCard variantShipCard = StaticCards.Instance.ShipListCard.GetVariantShipCard(shipIcon.Card, (uint) varIndex);
          if (variantShipCard != null)
          {
            ship = variantShipCard.GetHangarShip;
            if (ship != null)
              Game.Me.Hangar.SelectShip(ship);
          }
        }
      }
      if (ship == null)
      {
        HangarShip hangarShip = this.shipSelected.GetHangarShip();
        if (hangarShip != null)
        {
          if (this.shipSelected.Card.ShipRoleDeprecated == ShipRoleDeprecated.Carrier && !this.IsCarrierSpawnLevel())
          {
            new InfoBox("%$bgo.hangar.cannot_undock_carrier_from_outpost%").Show((GuiPanel) this);
            return;
          }
          Game.Me.Hangar.SelectShip(hangarShip);
        }
      }
      this.CloseButton.Pressed();
    }

    private void ItemPressed(ShipIcon icon)
    {
      this.CloseVariantWindow();
      if (this.AnyVariantsOwned(icon.Card))
      {
        this.selectVariantWindow = new GUISelectVariant(icon.GetHangarShip(), new GUISelectVariant.VariantSelectCallback(this.SelectVariant));
        this.selectVariantWindow.Position = new Vector2(this.PositionNormalX + icon.PositionNormalX - this.selectVariantWindow.SizeX, this.PositionNormalY + icon.PositionNormalY);
        Game.RegisterDialog((IGUIPanel) this.selectVariantWindow, true);
      }
      if (this.variantSelected != null)
        this.variantSelected.IsPressedManual = (int) this.selectedHangardId == (int) Game.Me.ActiveShip.Card.HangarID;
      this.variantSelected = (GuiButton) null;
      if (this.shipSelected != null)
        this.shipSelected.IsSelected = false;
      this.shipSelected = icon;
      this.shipSelected.IsSelected = true;
      this.command.IsRendered = this.shipSelected.Card.HaveShip;
    }

    private bool AnyVariantsOwned(ShipCard card)
    {
      for (uint index = 0; (long) index < (long) card.VariantHangarIDs.Count; ++index)
      {
        ShipCard variantShipCard = StaticCards.Instance.ShipListCard.GetVariantShipCard(card, index);
        if (variantShipCard != null && variantShipCard.HaveShip)
          return true;
      }
      return false;
    }

    private void SelectVariant(int index, byte parentHangarId)
    {
      ShipIcon shipIcon = this.GetShipIcon(parentHangarId);
      this.DeselectShipVariants(shipIcon);
      if (index <= -1)
        return;
      shipIcon.variantIcons[index].NormalTexture = ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Buttons_fill");
      shipIcon.variantIcons[index].PressedTexture = shipIcon.variantIcons[index].NormalTexture;
      shipIcon.variantIcons[index].OverTexture = shipIcon.variantIcons[index].NormalTexture;
      this.variantSelected = shipIcon.variantIcons[index];
      this.selectedHangardId = (byte) shipIcon.Card.VariantHangarIDs[index];
    }

    private ShipIcon GetShipIcon(byte hangarId)
    {
      for (int index1 = 0; index1 < this.shipLayoutArray.Length; ++index1)
      {
        for (int index2 = 0; index2 < this.shipLayoutArray[index1].Length; ++index2)
        {
          ShipIcon shipIcon = this.shipLayoutArray[index1][index2];
          if ((int) shipIcon.Card.HangarID == (int) hangarId)
            return shipIcon;
        }
      }
      if (this.carrierShipIcon != null && (int) this.carrierShipIcon.Card.HangarID == (int) hangarId)
        return this.carrierShipIcon;
      return (ShipIcon) null;
    }

    private ShipIcon GetShipIcon(GuiButton button, out int varIndex)
    {
      varIndex = -1;
      for (int index1 = 0; index1 < this.shipLayoutArray.Length; ++index1)
      {
        for (int index2 = 0; index2 < this.shipLayoutArray[index1].Length; ++index2)
        {
          for (int index3 = 0; index3 < this.shipLayoutArray[index1][index2].variantIcons.Length; ++index3)
          {
            this.shipLayoutArray[index1][index2].variantIcons[index3].IsPressedManual = false;
            if (this.shipLayoutArray[index1][index2].variantIcons[index3] == button)
            {
              varIndex = index3;
              return this.shipLayoutArray[index1][index2];
            }
          }
        }
      }
      if (this.carrierShipIcon != null)
      {
        for (int index = 0; index < this.carrierShipIcon.variantIcons.Length; ++index)
        {
          this.carrierShipIcon.variantIcons[index].IsPressedManual = false;
          if (this.carrierShipIcon.variantIcons[index] == button)
          {
            varIndex = index;
            return this.carrierShipIcon;
          }
        }
      }
      return (ShipIcon) null;
    }

    public void DeselectAllShipVariants()
    {
      for (int index1 = 0; index1 < this.shipLayoutArray.Length; ++index1)
      {
        if (this.shipLayoutArray[index1] != null)
        {
          for (int index2 = 0; index2 < this.shipLayoutArray[index1].Length; ++index2)
          {
            ShipIcon shipIcon = this.shipLayoutArray[index1][index2];
            if (shipIcon != null)
              this.DeselectShipVariantsUnlessOwned(shipIcon);
          }
        }
      }
      if (this.carrierShipIcon == null)
        return;
      this.DeselectShipVariantsUnlessOwned(this.carrierShipIcon);
    }

    private void DeselectShipVariantsUnlessOwned(ShipIcon shipIcon)
    {
      if (shipIcon == null)
        return;
      for (int index = 0; index < shipIcon.variantIcons.Length; ++index)
      {
        GuiButton guiButton = shipIcon.variantIcons[index];
        ShipCard variantShipCard = StaticCards.Instance.ShipListCard.GetVariantShipCard(shipIcon.Card, (uint) index);
        if (variantShipCard != null && variantShipCard.HaveShip)
          guiButton.NormalTexture = !Game.Me.ActiveShip.Card.Equals(StaticCards.Instance.ShipListCard.GetVariantShipCard(shipIcon.Card, (uint) index)) ? ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Button_light") : ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Buttons_fill");
        guiButton.OverTexture = guiButton.NormalTexture;
        guiButton.PressedTexture = guiButton.NormalTexture;
      }
    }

    private void DeselectShipVariants(ShipIcon shipIcon)
    {
      for (int index = 0; index < shipIcon.variantIcons.Length; ++index)
      {
        GuiButton guiButton = shipIcon.variantIcons[index];
        ShipCard variantShipCard = StaticCards.Instance.ShipListCard.GetVariantShipCard(shipIcon.Card, (uint) index);
        guiButton.NormalTexture = variantShipCard == null || !variantShipCard.HaveShip ? ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Button_dark") : ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Button_light");
        guiButton.OverTexture = guiButton.NormalTexture;
        guiButton.PressedTexture = guiButton.NormalTexture;
      }
    }

    public void CloseVariantWindow()
    {
      if (this.selectVariantWindow != null)
        Game.UnregisterDialog((IGUIPanel) this.selectVariantWindow);
      this.DeselectAllShipVariants();
    }

    [Gui2Editor]
    public static void ShowHangarWindow()
    {
      HangarWindow hangarWindow = new HangarWindow();
      hangarWindow.IsRendered = true;
      Game.RegisterDialog((IGUIPanel) hangarWindow, true);
    }
  }
}
