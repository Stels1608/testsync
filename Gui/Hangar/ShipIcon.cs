﻿// Decompiled with JetBrains decompiler
// Type: Gui.Hangar.ShipIcon
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.Hangar
{
  public class ShipIcon : GuiPanel
  {
    public GuiButton[] variantIcons = new GuiButton[3];
    private bool isMe = true;
    private bool showOverlay = true;
    private readonly GuiLabel labelName = new GuiLabel();
    private readonly GuiImage image = new GuiImage();
    private readonly GuiImage helmet = new GuiImage("GUI/InfoJournal/helmet");
    public const string varNormal = "GUI/BuyShip/ShipConfig/Button_dark";
    public const string varOver = "GUI/BuyShip/ShipConfig/Button_light";
    public const string varPressed = "GUI/BuyShip/ShipConfig/Buttons_fill";
    public System.Action<ShipIcon> OnSelected;
    public ShipCard Card;
    public ShipCard ActiveShip;
    private bool isOwned;
    private bool isSelected;
    private bool isUpgraded;
    private Faction faction;

    public override Vector2 Position
    {
      get
      {
        return base.Position;
      }
      set
      {
        base.Position = value;
        float y = this.Position.y + 28f;
        float num = this.variantIcons[0].SizeX * 0.8f;
        this.variantIcons[0].Position = new Vector2(this.Position.x - num, y);
        this.variantIcons[1].Position = new Vector2(this.Position.x, y);
        this.variantIcons[2].Position = new Vector2(this.Position.x + num, y);
      }
    }

    public bool IsSelected
    {
      get
      {
        return this.isSelected;
      }
      set
      {
        this.isSelected = value;
        this.Highlight(this.isSelected);
      }
    }

    public ShipIcon(Faction faction, ShipCard card, bool isOwned, bool isUpgraded, ShipCard activeShip, bool isMe)
    {
      this.faction = faction;
      this.Card = card;
      this.ActiveShip = activeShip;
      this.isOwned = isOwned;
      this.isUpgraded = isUpgraded;
      this.Size = new Vector2(112f, 80f);
      this.isMe = isMe;
      this.AddChild((GuiElementBase) this.image, Align.UpCenter);
      this.AddChild((GuiElementBase) this.labelName, Align.UpCenter, new Vector2(0.0f, 5f));
      this.helmet.Position = new Vector2(-14f, -17f);
      this.AddChild((GuiElementBase) this.helmet, Align.DownRight);
      this.helmet.IsRendered = false;
      for (int index = 0; index < 3; ++index)
        this.variantIcons[index] = new GuiButton(string.Empty, "GUI/BuyShip/ShipConfig/Button_dark", "GUI/BuyShip/ShipConfig/Button_dark", "GUI/BuyShip/ShipConfig/Button_dark");
      this.PeriodicUpdate();
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      this.Highlight(true);
      return base.MouseDown(position, key);
    }

    public override void MouseEnter(float2 position, float2 previousPosition)
    {
      this.Hover();
      base.MouseEnter(position, previousPosition);
    }

    private void Hover()
    {
      if (!this.showOverlay)
        return;
      Color color = Game.Me.Faction != Faction.Colonial ? new Color(1f, 0.5f, 0.5f, 0.9f) : new Color(0.5f, 0.5f, 1f, 0.9f);
      this.image.OverlayColor = new Color?(color);
      this.helmet.OverlayColor = new Color?(color);
      this.labelName.OverlayColor = new Color?(color);
    }

    private void Highlight(bool highlight)
    {
      if (highlight)
      {
        if (!this.showOverlay)
          return;
        Color color = Game.Me.Faction != Faction.Colonial ? new Color(1f, 0.5f, 0.5f, 0.8f) : new Color(0.5f, 0.5f, 1f, 0.8f);
        this.image.OverlayColor = new Color?(color);
        this.helmet.OverlayColor = new Color?(color);
        this.labelName.OverlayColor = new Color?(color);
      }
      else
      {
        this.image.OverlayColor = new Color?();
        this.helmet.OverlayColor = new Color?();
        this.labelName.OverlayColor = new Color?();
      }
    }

    public override void MouseLeave(float2 position, float2 previousPosition)
    {
      if (!this.IsSelected)
        this.Highlight(false);
      base.MouseLeave(position, previousPosition);
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      if (this.OnSelected != null)
        this.OnSelected(this);
      return base.MouseUp(position, key);
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      if (this.Card == null)
        return;
      this.helmet.IsRendered = this.IsCommanded() || this.VariantCommanded();
      bool flag = this.isMe && this.Card.HaveShip || this.isOwned;
      this.labelName.Text = flag ? this.Card.ItemGUICard.Name : string.Empty;
      if (flag && this.isUpgraded && this.Card.NextCard != null)
        this.labelName.Text = this.Card.NextCard.ItemGUICard.Name;
      ShipCard shipCard = this.VariantSelected();
      if (shipCard != null)
        this.labelName.Text = shipCard.ItemGUICard.Name;
      string str = "GUI/InfoJournal/Ships/" + (this.faction != Faction.Cylon ? (object) "Human" : (object) "Cylon") + (object) this.Card.HangarID;
      this.image.TexturePath = !flag ? str + "_notbought" : (!this.isUpgraded ? str : str + "_upgraded");
      this.image.PositionY = this.labelName.PositionNormalY + 15f;
    }

    [Gui2Editor]
    public HangarShip GetHangarShip()
    {
      return this.Card.GetHangarShip;
    }

    [Gui2Editor]
    public bool IsCommanded()
    {
      if (this.isMe)
        return (int) Game.Me.ActiveShip.Card.HangarID == (int) this.Card.HangarID;
      return (int) this.ActiveShip.HangarID == (int) this.Card.HangarID;
    }

    public bool VariantCommanded()
    {
      for (int index = 0; index < 3; ++index)
      {
        ShipCard variantShipCard = StaticCards.Instance.ShipListCard.GetVariantShipCard(this.Card, (uint) index);
        if (variantShipCard != null && this.isMe && (int) Game.Me.ActiveShip.Card.HangarID == (int) variantShipCard.HangarID)
          return true;
      }
      return false;
    }

    public ShipCard VariantSelected()
    {
      for (int index = 0; index < 3; ++index)
      {
        ShipCard variantShipCard = StaticCards.Instance.ShipListCard.GetVariantShipCard(this.Card, (uint) index);
        if (variantShipCard != null && this.isMe && (int) Game.Me.ActiveShip.Card.HangarID == (int) variantShipCard.HangarID)
          return variantShipCard;
      }
      return (ShipCard) null;
    }

    public void ShowHelmet(bool show)
    {
      this.helmet.IsRendered = show;
    }

    public void ShowText(bool show)
    {
      this.labelName.IsRendered = show;
    }

    public void ShowOverlay(bool show)
    {
      this.showOverlay = show;
    }

    public enum ShipState
    {
      NotBought,
      Bought,
      Upgraded,
    }
  }
}
