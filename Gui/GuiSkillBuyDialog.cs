﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiSkillBuyDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/SkillBuyPanel/SkillBuyDialog.txt")]
  public class GuiSkillBuyDialog : GuiPanel
  {
    [HideInInspector]
    private GuiImage m_back;
    [HideInInspector]
    private GuiLabel m_description;
    [HideInInspector]
    private GuiButton m_okbtn;
    [HideInInspector]
    private GuiButton m_cancelbtn;
    [HideInInspector]
    private GuiLabel m_headline;

    public GuiImage Back
    {
      get
      {
        return this.m_back;
      }
    }

    public GuiLabel Description
    {
      get
      {
        return this.m_description;
      }
    }

    public GuiButton Okbtn
    {
      get
      {
        return this.m_okbtn;
      }
    }

    public GuiButton Cancelbtn
    {
      get
      {
        return this.m_cancelbtn;
      }
    }

    public GuiLabel Headline
    {
      get
      {
        return this.m_headline;
      }
    }

    public GuiSkillBuyDialog()
      : base(UseInGuiEditor.GetDefaultLayout<GuiSkillBuyDialog>())
    {
      this.Initialize();
      this.m_headline.Text = "%$bgo.instant_skill.complete%";
      this.m_description.Text = Tools.ParseMessage("%$bgo.instant_skill.complete.description%", (object) (Math.Ceiling(TimeSpan.FromSeconds((double) Game.Me.SkillBook.TrainingEndTime - (double) Time.time).TotalMinutes) * 60.0));
      this.IsRendered = true;
      this.m_okbtn.OnClick = new AnonymousDelegate(this.AcceptInstantBuy);
    }

    public GuiSkillBuyDialog(GuiSkillBuyDialog copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiSkillBuyDialog(JSkillBuyDialog json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public GuiSkillBuyDialog(PlayerSkill skill)
      : this()
    {
      this.m_headline.Text = Tools.ParseMessage("%$bgo.instant_skill.reducetime%", (object) 10);
      this.m_description.Text = Tools.ParseMessage("%$bgo.instant_skill.reducetime.description%", (object) 10, (object) (Math.Ceiling(TimeSpan.FromSeconds((double) Game.Me.SkillBook.TrainingEndTime - (double) Time.time).TotalMinutes / 10.0) * 60.0), (object) Tools.FormatTime((float) (((double) Game.Me.SkillBook.TrainingEndTime - (double) Time.time) * 0.899999976158142)));
      this.IsRendered = true;
      this.m_okbtn.OnClick = new AnonymousDelegate(this.AccepReducedBuy);
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiSkillBuyDialog(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JSkillBuyDialog((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
    }

    public void Initialize()
    {
      this.m_back = this.Find<GuiImage>("back");
      this.m_description = this.Find<GuiLabel>("description");
      this.m_okbtn = this.Find<GuiButton>("okbtn");
      this.m_cancelbtn = this.Find<GuiButton>("cancelbtn");
      this.m_headline = this.Find<GuiLabel>("headline");
      this.m_cancelbtn.OnClick = new AnonymousDelegate(this.Close);
      this.IsRendered = false;
    }

    private void AcceptInstantBuy()
    {
      if (Game.Me.SkillBook.CurrentTrainingSkill != null)
        PlayerProtocol.GetProtocol().InstantSkillBuy(Game.Me.SkillBook.CurrentTrainingSkill.ServerID);
      this.Close();
    }

    private void AccepReducedBuy()
    {
      if (Game.Me.SkillBook.CurrentTrainingSkill != null)
        PlayerProtocol.GetProtocol().ReduceSkillLearnTime(Game.Me.SkillBook.CurrentTrainingSkill.ServerID);
      this.Close();
    }

    private void Close()
    {
      GuiDialogPopupManager.CloseDialog();
    }
  }
}
