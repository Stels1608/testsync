﻿// Decompiled with JetBrains decompiler
// Type: Gui.QueueVertical
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class QueueVertical : PanelWithServiceChildren, ISelectableContainer
  {
    public float delimeterSize = 1f;
    public bool DeselectWhenRemoved = true;
    public Action<ISelectable> OnSelectionChanged;
    private float space;
    private bool containerAutoSize;
    private bool childrenAutoSize;
    private bool renderDelimiters;
    public bool DrawSelection;
    public bool SelectionCutEdges;
    private ISelectable selected;

    public ISelectable Selected
    {
      get
      {
        return this.selected;
      }
      set
      {
        if (this.selected != null)
          this.selected.OnDeselected();
        this.selected = value;
        if (this.selected != null)
          this.selected.OnSelected();
        if (this.OnSelectionChanged == null)
          return;
        this.OnSelectionChanged(this.selected);
      }
    }

    [Gui2Editor]
    public float Space
    {
      get
      {
        return this.space;
      }
      set
      {
        this.space = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public bool ContainerAutoSize
    {
      get
      {
        return this.containerAutoSize;
      }
      set
      {
        this.containerAutoSize = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public bool AutoSizeChildren
    {
      get
      {
        return this.childrenAutoSize;
      }
      set
      {
        this.childrenAutoSize = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public bool RenderDelimiters
    {
      get
      {
        return this.renderDelimiters;
      }
      set
      {
        this.renderDelimiters = value;
        this.RepositionElements();
      }
    }

    public QueueVertical()
    {
      this.RequireBackRepositioning = true;
      this.MouseTransparent = true;
    }

    public override void Reposition()
    {
      this.RepositionElements();
      base.Reposition();
    }

    private void RepositionElements()
    {
      this.EmptyServiceChildren();
      float y = 0.0f;
      float x = 0.0f;
      List<GuiElementBase> childrenVisible = this.ChildrenVisible;
      using (List<GuiElementBase>.Enumerator enumerator = childrenVisible.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GuiElementBase current = enumerator.Current;
          current.PositionY = y;
          y += current.Size.y;
          if ((double) current.SizeX > (double) x)
            x = current.SizeX;
          if (this.renderDelimiters && childrenVisible.IndexOf(current) < childrenVisible.Count - 1)
          {
            GuiColoredBox guiColoredBox = new GuiColoredBox();
            guiColoredBox.Position = new Vector2(0.0f, y + this.space / 2f);
            guiColoredBox.Size = new Vector2(this.SizeX, this.delimeterSize);
            this.AddServiceChild((GuiElementBase) guiColoredBox);
            y += this.delimeterSize;
          }
          y += this.space;
        }
      }
      if (this.childrenAutoSize)
      {
        using (List<GuiElementBase>.Enumerator enumerator = childrenVisible.GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.SizeX = this.SizeX;
        }
      }
      if (!this.containerAutoSize)
        return;
      if ((double) y > 0.0)
        y -= this.space + (!this.renderDelimiters ? 0.0f : this.delimeterSize);
      this.Size = new Vector2(x, y);
    }

    public override void AddChild(GuiElementBase ch)
    {
      if (this.childrenAutoSize)
        ch.SizeX = this.SizeX;
      base.AddChild(ch);
      this.ForceReposition();
    }

    public override void RemoveChild(GuiElementBase ch)
    {
      base.RemoveChild(ch);
      if (this.DeselectWhenRemoved && this.Selected != null && this.Selected as GuiElementBase == ch)
        this.Selected = (ISelectable) null;
      this.ForceReposition();
    }

    public override void Draw()
    {
      if (this.DrawSelection)
        this.DrawSelectionTexture(this.selected);
      base.Draw();
    }

    public void DrawSelectionTexture(ISelectable selected)
    {
      if (selected == null || Event.current.type != EventType.Repaint)
        return;
      QueueVertical.ItemPosition position = this.GetPosition(selected);
      if (position == QueueVertical.ItemPosition.NotHere)
        return;
      Texture2D texture2D = Options.QueueSelectTextureNormal;
      if (this.SelectionCutEdges)
      {
        if (position == QueueVertical.ItemPosition.First)
          texture2D = Options.QueueSelectTextureUp;
        else if (position == QueueVertical.ItemPosition.Last)
          texture2D = Options.QueueSelectTextureDown;
        else if (position == QueueVertical.ItemPosition.Middle)
          texture2D = Options.QueueSelectTextureNormal;
        int selectTexturePadding = Options.QueueSelectTexturePadding;
        Graphics.DrawTexture((selected as GuiElementBase).Rect, (Texture) texture2D, 0, 0, position != QueueVertical.ItemPosition.First ? 0 : selectTexturePadding, position != QueueVertical.ItemPosition.Last ? 0 : selectTexturePadding);
      }
      else
        Graphics.DrawTexture((selected as GuiElementBase).Rect, (Texture) texture2D, 0, 0, 0, 0);
    }

    public QueueVertical.ItemPosition GetPosition(ISelectable el)
    {
      GuiElementBase guiElementBase = el as GuiElementBase;
      List<GuiElementBase> childrenVisible = this.ChildrenVisible;
      int count = childrenVisible.Count;
      int num = childrenVisible.IndexOf(guiElementBase);
      if (count == 0 || num == -1)
        return QueueVertical.ItemPosition.NotHere;
      if (num == 0)
        return QueueVertical.ItemPosition.First;
      return num == count - 1 && (double) this.SizeY * 0.930000007152557 <= (double) guiElementBase.PositionNormalY + (double) guiElementBase.SizeY ? QueueVertical.ItemPosition.Last : QueueVertical.ItemPosition.Middle;
    }

    public enum ItemPosition
    {
      First,
      Middle,
      Last,
      NotHere,
    }
  }
}
