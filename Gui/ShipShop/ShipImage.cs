﻿// Decompiled with JetBrains decompiler
// Type: Gui.ShipShop.ShipImage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.ShipShop
{
  public class ShipImage : GuiPanel
  {
    private readonly GuiImage saleImage;
    private readonly GuiImage helmet;

    public GuiImageActive Image { get; private set; }

    public bool IsHelmetRendered
    {
      get
      {
        return this.helmet.IsRendered;
      }
      set
      {
        this.helmet.IsRendered = value;
      }
    }

    public ShipImage()
    {
      this.Image = new GuiImageActive();
      this.AddChild((GuiElementBase) this.Image);
      this.saleImage = new GuiImage("GUI/Common/ShipSaleBanner");
      this.saleImage.Size = new Vector2(18.56f, 16f);
      this.AddChild((GuiElementBase) this.saleImage, Align.UpRight);
      this.helmet = new GuiImage("GUI/InfoJournal/helmet");
      GuiImage guiImage = this.helmet;
      Vector2 vector2 = guiImage.Size * 0.75f;
      guiImage.Size = vector2;
      this.helmet.Parent = (SRect) this;
      this.helmet.IsRendered = false;
      this.AddChild((GuiElementBase) this.helmet);
      this.SetSaleState(false);
      this.MouseTransparent = true;
      this.HandleMouseInput = true;
    }

    public void SetSaleState(bool onSale)
    {
      this.saleImage.IsRendered = onSale;
    }

    public override void ForceReposition()
    {
      base.ForceReposition();
    }
  }
}
