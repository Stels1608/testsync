﻿// Decompiled with JetBrains decompiler
// Type: Gui.ShipShop.ShipQueue
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui.ShipShop
{
  internal class ShipQueue : GuiPanel
  {
    private readonly QueueHorizontalWithRows queue = new QueueHorizontalWithRows();
    private readonly Color selectedColor = Tools.Color(200, 200, 200);
    private readonly Color overColor = Game.Me.Faction != Faction.Colonial ? Tools.Color((int) byte.MaxValue, 184, 120) : Tools.Color(192, 211, 240);
    private readonly Dictionary<GuiImageActive, ShipCard> dic = new Dictionary<GuiImageActive, ShipCard>();
    private readonly List<uint> shipOrder = new List<uint>();
    public const string varNormal = "GUI/BuyShip/ShipConfig/Button_dark";
    public const string varOver = "GUI/BuyShip/ShipConfig/Button_light";
    public const string varPressed = "GUI/BuyShip/ShipConfig/Buttons_fill";
    private ShipListCard card;
    private List<ShipCard> shipCards;
    private ShipImage[] ships;
    private ShipCard selected;
    public GuiButton[,] variantIcons;
    private ShipQueue.SelectCallback onSelect;

    public ShipQueue(Vector2 Position, Vector2 Size, ShipQueue.SelectCallback onSelect)
    {
      this.Position = Position;
      this.Size = Size;
      this.AddChild((GuiElementBase) this.queue, Align.MiddleLeft, new Vector2(0.0f, -14f));
      this.card = StaticCards.Instance.ShipListCard;
      this.onSelect = onSelect;
      this.card.IsLoaded.AddHandler(new SignalHandler(this.InitIcons));
    }

    private void UpdateShipCards()
    {
      this.shipCards.Clear();
      Shop shop = GameLevel.Instance.Shop;
      for (int index = 0; index < this.card.ShipCards.Length; ++index)
      {
        if (shop.GetByGUID(this.card.ShipCards[index].CardGUID) != null)
        {
          if (this.card.ShipCards[index].HaveShip && this.card.ShipCards[index].GetHangarShip.IsUpgraded)
            this.shipCards.Add(this.card.UpgradeShipCards[index]);
          else
            this.shipCards.Add(this.card.ShipCards[index]);
        }
      }
    }

    private void InitIcons()
    {
      this.shipCards = new List<ShipCard>();
      this.UpdateShipCards();
      this.shipOrder.Add(1U);
      this.shipOrder.Add(4U);
      if (this.shipCards.Exists((Predicate<ShipCard>) (shipcard => (int) shipcard.HangarID == 16)))
        this.shipOrder.Add(16U);
      this.shipOrder.Add(7U);
      this.shipOrder.Add(11U);
      if (this.shipCards.Exists((Predicate<ShipCard>) (shipcard => (int) shipcard.HangarID == 17)))
        this.shipOrder.Add(17U);
      this.shipOrder.Add(2U);
      this.shipOrder.Add(5U);
      this.shipOrder.Add(8U);
      this.shipOrder.Add(13U);
      this.shipOrder.Add(3U);
      this.shipOrder.Add(6U);
      this.shipOrder.Add(9U);
      this.shipOrder.Add(14U);
      this.shipOrder.Add(15U);
      this.selected = Game.Me.Hangar.ActiveShip.Card;
      this.variantIcons = new GuiButton[this.shipCards.Count, 3];
      this.ships = new ShipImage[this.shipCards.Count];
      this.Reposition();
      for (int index1 = 0; index1 < this.shipCards.Count; ++index1)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        ShipQueue.\u003CInitIcons\u003Ec__AnonStoreyB8 iconsCAnonStoreyB8 = new ShipQueue.\u003CInitIcons\u003Ec__AnonStoreyB8();
        // ISSUE: reference to a compiler-generated field
        iconsCAnonStoreyB8.\u003C\u003Ef__this = this;
        ShipCard shipCard = this.shipCards[index1];
        // ISSUE: reference to a compiler-generated field
        iconsCAnonStoreyB8.ordered = this.card.Get((byte) this.shipOrder[index1]);
        this.ships[index1] = new ShipImage();
        this.ships[index1].Image.ColorOver = new Color?(this.overColor);
        // ISSUE: reference to a compiler-generated method
        this.ships[index1].Image.OnClickAction = new System.Action<GuiElementBase>(iconsCAnonStoreyB8.\u003C\u003Em__196);
        this.ships[index1].Name = ((int) shipCard.HangarID).ToString() + "_" + shipCard.ItemGUICard.Name;
        this.queue.AddChild((GuiElementBase) this.ships[index1], Align.MiddleLeft);
        for (int index2 = 0; index2 < 3; ++index2)
        {
          this.variantIcons[index1, index2] = new GuiButton(string.Empty, "GUI/BuyShip/ShipConfig/Button_dark", "GUI/BuyShip/ShipConfig/Button_dark", "GUI/BuyShip/ShipConfig/Button_dark");
          this.AddChild((GuiElementBase) this.variantIcons[index1, index2]);
        }
      }
      this.PeriodicUpdate();
    }

    public override void PeriodicUpdate()
    {
      if (this.ships == null)
        return;
      this.UpdateShipCards();
      float num1 = 0.0f;
      string str = "GUI/InfoJournal/Ships/" + (Game.Me.Faction != Faction.Colonial ? "Cylon" : "Human");
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      ShipQueue.\u003CPeriodicUpdate\u003Ec__AnonStoreyB9 updateCAnonStoreyB9 = new ShipQueue.\u003CPeriodicUpdate\u003Ec__AnonStoreyB9();
      using (List<ShipCard>.Enumerator enumerator = this.shipCards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          // ISSUE: reference to a compiler-generated field
          updateCAnonStoreyB9.sc = enumerator.Current;
          // ISSUE: reference to a compiler-generated field
          string file = str + (object) updateCAnonStoreyB9.sc.HangarID;
          // ISSUE: reference to a compiler-generated field
          if (updateCAnonStoreyB9.sc.HaveShip)
          {
            // ISSUE: reference to a compiler-generated field
            if (updateCAnonStoreyB9.sc.GetHangarShip.Card.IsUpgraded)
              file += "_upgraded";
          }
          else
            file += "_notbought";
          Texture2D texture2D = ResourceLoader.Load<Texture2D>(file);
          if ((UnityEngine.Object) texture2D == (UnityEngine.Object) null)
          {
            // ISSUE: reference to a compiler-generated field
            Debug.LogError((object) ("Texture " + file + " not found for " + updateCAnonStoreyB9.sc.ItemGUICard.Key));
          }
          else
            num1 += (float) texture2D.width;
          // ISSUE: reference to a compiler-generated method
          GuiImageActive image = this.ships[this.shipOrder.FindIndex(new Predicate<uint>(updateCAnonStoreyB9.\u003C\u003Em__197))].Image;
          image.Texture = texture2D;
          // ISSUE: reference to a compiler-generated field
          this.dic[image] = updateCAnonStoreyB9.sc;
          // ISSUE: reference to a compiler-generated field
          bool flag = updateCAnonStoreyB9.sc == this.selected;
          image.ColorNormal = !flag ? new Color?() : new Color?(this.selectedColor);
        }
      }
      for (int index1 = 0; index1 < this.ships.Length; ++index1)
      {
        GuiImageActive image = this.ships[index1].Image;
        if (image != null)
        {
          GuiImageActive guiImageActive = image;
          float num2 = (float) image.Texture.width;
          this.ships[index1].SizeX = num2;
          double num3 = (double) num2;
          guiImageActive.SizeX = (float) num3;
        }
        for (int index2 = 0; index2 < 3; ++index2)
        {
          GuiButton guiButton = this.variantIcons[index1, index2];
          ShipCard variantShipCard = this.card.GetVariantShipCard(this.dic[image], (uint) index2);
          guiButton.NormalTexture = variantShipCard == null ? ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Button_dark") : (!variantShipCard.HaveShip ? ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Button_light") : ResourceLoader.Load<Texture2D>("GUI/BuyShip/ShipConfig/Buttons_fill"));
          guiButton.PressedTexture = guiButton.NormalTexture;
          guiButton.OverTexture = guiButton.NormalTexture;
          float num2 = (float) guiButton.NormalTexture.width * 0.7f;
          float num3 = 72f;
          guiButton.PositionCenter = this.ships[index1].PositionCenter + new Vector2(num2 * (float) (index2 - 1), 0.0f);
          guiButton.PositionY = this.ships[index1].PositionY + num3;
        }
        ShipCard ship = this.dic[this.ships[index1].Image];
        this.ships[index1].Image.SetShipToolTip(ship, ShopInventoryContainer.Store);
        if (!ship.HaveShip)
          this.ships[index1].SetSaleState(Shop.FindItemDiscount(ship.ItemGUICard.CardGUID) != null);
        else if (ship.NextCard != null)
          this.ships[index1].SetSaleState(Shop.FindUpgradeDiscount(ShopItemType.Ship, 1) != null);
        else
          this.ships[index1].SetSaleState(false);
        this.ships[index1].IsHelmetRendered = ship.HaveShip && ship.GetHangarShip == Game.Me.ActiveShip;
        this.ships[index1].PeriodicUpdate();
      }
    }

    public override void Reposition()
    {
      base.Reposition();
      this.queue.Size = this.Size;
    }

    private Color? GetHasColor()
    {
      if (Game.Me.Faction == Faction.Colonial)
        return new Color?();
      return new Color?(Tools.Color((int) byte.MaxValue, (int) byte.MaxValue, (int) byte.MaxValue, 145));
    }

    private Color? GetUpgradedColor()
    {
      if (Game.Me.Faction == Faction.Colonial)
        return new Color?(Tools.Color((int) byte.MaxValue, 200, 0));
      return new Color?(Tools.Color((int) byte.MaxValue, (int) byte.MaxValue, (int) byte.MaxValue));
    }

    public delegate void SelectCallback(ShipCard selected, ShipCard actual);
  }
}
