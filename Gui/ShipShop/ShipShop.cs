﻿// Decompiled with JetBrains decompiler
// Type: Gui.ShipShop.ShipShop
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Common;
using Gui.GameItem;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui.ShipShop
{
  public class ShipShop : GuiDialog
  {
    private ShipView shipView = new ShipView();
    private MoneyPlayer money = new MoneyPlayer();
    private GetCubits getCubits = new GetCubits();
    private AnonymousDelegate onCloseOuter;
    private HangarShip selectedShip;
    private ShipCard selectedCard;
    private ShipCard actualCard;
    private ShipStats shipStats;
    private ShipListCard shipListCard;
    private GuiCostPanel costPanel;
    private GuiButton reqButton;
    private GuiButton commandButton;
    private GuiButton resetShipRotation;
    private GuiLabel labelError;
    private GuiLabel labelTitle;
    private GuiLabel labelShipTitle;
    private GuiImage[] shipVariants;
    private List<Gui.ShipShop.ShipShop.VariantImage> variantImageList;
    private int variantListIndex;

    public AnonymousDelegate OnCloseOuter
    {
      get
      {
        return this.onCloseOuter;
      }
      set
      {
        this.onCloseOuter = value;
      }
    }

    public override bool IsBigWindow
    {
      get
      {
        return true;
      }
    }

    public ShipShop()
      : base("GUI/BuyShip/layout")
    {
      this.enabled = false;
      this.shipListCard = StaticCards.Instance.ShipListCard;
      this.shipListCard.IsLoaded.AddHandler(new SignalHandler(this.Init));
      this.ClosePressed();
    }

    private void Init()
    {
      this.Align = Align.MiddleCenter;
      GuiImage guiImage1 = this.Find<GuiImage>("statsRect");
      this.labelShipTitle = new GuiLabel(Gui.Options.FontBGM_BT, 14);
      this.labelShipTitle.Size = new Vector2(150f, 30f);
      this.AddChild((GuiElementBase) this.labelShipTitle, guiImage1.Position + new Vector2(0.0f, 4f));
      this.shipStats = new ShipStats(guiImage1.Size - new Vector2(-3f, 25f));
      this.shipStats.Position = guiImage1.Position + new Vector2(0.0f, 25f);
      this.RemoveChild((GuiElementBase) guiImage1);
      this.AddChild((GuiElementBase) this.shipStats);
      GuiImage guiImage2 = this.Find<GuiImage>("queueRect");
      ShipQueue shipQueue = new ShipQueue(guiImage2.Position, guiImage2.Size, (ShipQueue.SelectCallback) ((sc, actual) =>
      {
        this.selectedCard = sc;
        this.actualCard = actual;
        this.UpdateShipList();
      }));
      this.selectedCard = Game.Me.Hangar.ActiveShip.Card;
      this.RemoveChild((GuiElementBase) guiImage2);
      this.AddChild((GuiElementBase) shipQueue);
      GuiImage guiImage3 = this.Find<GuiImage>("shipViewRect");
      this.shipView.Size = guiImage3.Size;
      this.shipView.Position = guiImage3.Position;
      this.RemoveChild((GuiElementBase) guiImage3);
      this.AddChild((GuiElementBase) this.shipView);
      this.reqButton = this.Find<GuiButton>("reqButton");
      this.reqButton.Pressed = (AnonymousDelegate) (() =>
      {
        if (this.selectedShip == null && this.selectedCard.HasParent)
        {
          HangarShip hangarShip = Game.Me.Hangar[(ushort) this.selectedCard.ParentHangarID];
          if (hangarShip == null || !hangarShip.Card.HaveShip || !hangarShip.Card.IsUpgraded)
          {
            new InfoBox("%$bgo.etc.ship_shop_need_upgraded_ship%").Show((GuiPanel) this);
            return;
          }
        }
        new AskBox(this.selectedShip != null ? "%$bgo.etc.ship_shop_upgrage_confirmation%" : "%$bgo.etc.ship_shop_requisition_confirmation%", (AnonymousDelegate) (() =>
        {
          if (this.selectedCard == null)
            return;
          if (!this.selectedCard.HaveShip)
          {
            Game.Me.Hangar.AddShip(this.selectedCard.CardGUID);
            new AskBox("%$bgo.etc.ship_shop_info8%", (AnonymousDelegate) (() =>
            {
              HangarShip getHangarShip = this.selectedCard.GetHangarShip;
              if (getHangarShip == null)
                return;
              Game.Me.Hangar.SelectShip(getHangarShip);
            })).Show((GuiPanel) this);
            this.reqButton.IsRendered = false;
          }
          else if (this.selectedCard.HaveShip && !this.selectedShip.IsUpgraded)
          {
            Game.Me.Hangar.UpgradeShip(this.selectedShip);
            this.reqButton.IsRendered = false;
            this.selectedCard.IsLoaded.AddHandler(new SignalHandler(this.UpdateShipList));
          }
          else
          {
            if (this.selectedCard.HaveShip || !this.selectedShip.IsVariant)
              return;
            Game.Me.Hangar.AddShip(this.selectedCard.CardGUID);
            this.reqButton.IsRendered = false;
          }
        })).Show((GuiPanel) this);
      });
      this.commandButton = this.Find<GuiButton>("commandButton");
      this.commandButton.Pressed = (AnonymousDelegate) (() => new AskBox("%$bgo.hangar.ask%", (AnonymousDelegate) (() => this.CommandPressed())).Show(this.Parent as GuiPanel));
      this.commandButton.IsRendered = false;
      this.Find<GuiButton>("doneButton").Pressed = new AnonymousDelegate(this.ClosePressed);
      this.resetShipRotation = this.Find<GuiButton>("resetCamButton");
      this.resetShipRotation.SetTooltip("%$bgo.etc.ship_shop_standart_view%");
      this.resetShipRotation.Pressed = (AnonymousDelegate) (() => this.shipView.RestoreView());
      this.BringToFront((GuiElementBase) this.resetShipRotation);
      this.costPanel = this.Find<GuiCostPanel>("costPanel");
      this.labelError = this.Find<GuiLabel>("informLabel");
      this.labelError.IsRendered = false;
      this.labelError.AllColor = Gui.Options.NegativeColor;
      this.labelTitle = this.Find<GuiLabel>("title");
      this.OnClose = new AnonymousDelegate(this.ClosePressed);
      this.AddChild((GuiElementBase) this.getCubits, Align.UpRight, new Vector2(-40f, -82f));
      this.AddChild((GuiElementBase) this.money, Align.UpRight, new Vector2(-160f, -19f));
      this.shipVariants = new GuiImage[3];
      this.shipVariants[0] = this.InitShipConfigurations(1);
      this.shipVariants[1] = this.InitShipConfigurations(2);
      this.shipVariants[2] = this.InitShipConfigurations(3);
      this.UpdateShipList();
    }

    private GuiImage InitShipConfigurations(int index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      Gui.ShipShop.ShipShop.\u003CInitShipConfigurations\u003Ec__AnonStoreyB6 configurationsCAnonStoreyB6 = new Gui.ShipShop.ShipShop.\u003CInitShipConfigurations\u003Ec__AnonStoreyB6();
      // ISSUE: reference to a compiler-generated field
      configurationsCAnonStoreyB6.index = index;
      // ISSUE: reference to a compiler-generated field
      configurationsCAnonStoreyB6.\u003C\u003Ef__this = this;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      configurationsCAnonStoreyB6.button = this.Find<GuiImage>("shipConfigButton" + (object) configurationsCAnonStoreyB6.index);
      // ISSUE: reference to a compiler-generated field
      if (configurationsCAnonStoreyB6.button == null)
      {
        // ISSUE: reference to a compiler-generated field
        Log.Add("shipConfigButton" + (object) configurationsCAnonStoreyB6.index + " not found");
        return (GuiImage) null;
      }
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      configurationsCAnonStoreyB6.button.OnClick = new AnonymousDelegate(configurationsCAnonStoreyB6.\u003C\u003Em__18E);
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      configurationsCAnonStoreyB6.ship = this.shipListCard.GetVariantShipCard(this.selectedCard, (uint) (configurationsCAnonStoreyB6.index - 1));
      // ISSUE: reference to a compiler-generated field
      if (configurationsCAnonStoreyB6.ship != null)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        configurationsCAnonStoreyB6.ship.IsLoaded.AddHandler(new SignalHandler(configurationsCAnonStoreyB6.\u003C\u003Em__18F));
      }
      // ISSUE: reference to a compiler-generated field
      this.AddChild((GuiElementBase) configurationsCAnonStoreyB6.button);
      // ISSUE: reference to a compiler-generated field
      return configurationsCAnonStoreyB6.button;
    }

    private int GetVariantIndex(uint hangarID)
    {
      if (this.variantImageList == null)
      {
        this.variantImageList = new List<Gui.ShipShop.ShipShop.VariantImage>(this.shipListCard.ShipCards.Length);
        for (int index = 0; index < this.variantImageList.Count; ++index)
          this.variantImageList[index] = new Gui.ShipShop.ShipShop.VariantImage((uint) int.MaxValue, (uint) this.shipVariants.Length);
        return -1;
      }
      for (int index = 0; index < this.variantImageList.Count; ++index)
      {
        if ((int) hangarID == (int) this.variantImageList[index].parentHangarID)
          return index;
      }
      return -1;
    }

    private Texture2D LoadVariantTexture(string imageName, Gui.ShipShop.ShipShop.ImageType imageType)
    {
      string path1 = "GUI/InfoJournal/Ships/" + imageName;
      string path2 = path1;
      switch (imageType)
      {
        case Gui.ShipShop.ShipShop.ImageType.FULL:
          path1 += "_full";
          path2 += string.Empty;
          break;
        case Gui.ShipShop.ShipShop.ImageType.EMPTY:
          path1 += "_empty";
          path2 += "_notbought";
          break;
        case Gui.ShipShop.ShipShop.ImageType.UNAVAILABLE:
          path1 += "_unavailable";
          path2 += "_notbought";
          break;
      }
      Texture2D texture2D = (Texture2D) Resources.Load(path1);
      if ((UnityEngine.Object) texture2D == (UnityEngine.Object) null)
      {
        Debug.LogError((object) (path1 + " was not found."));
        texture2D = (Texture2D) Resources.Load(path2);
      }
      return texture2D;
    }

    private void LoadCurrentShipImages()
    {
      if (this.selectedCard.VariantHangarIDs.Count == 0)
        return;
      uint num = (uint) this.selectedCard.HangarID;
      this.variantListIndex = this.GetVariantIndex(num);
      if (this.variantListIndex >= 0)
        return;
      string str = "Cylon";
      if (Game.Me.Faction == Faction.Colonial)
        str = "Human";
      Gui.ShipShop.ShipShop.VariantImage variantImage = new Gui.ShipShop.ShipShop.VariantImage(num, (uint) this.shipVariants.Length);
      for (int index = 0; index < this.shipVariants.Length && index < this.selectedCard.VariantHangarIDs.Count; ++index)
      {
        Log.Add("Load variant hangar ID " + (object) this.selectedCard.VariantHangarIDs[index]);
        string imageName = str + (object) this.selectedCard.VariantHangarIDs[index];
        variantImage.fullImage[index] = this.LoadVariantTexture(imageName, Gui.ShipShop.ShipShop.ImageType.FULL);
        variantImage.emptyImage[index] = this.LoadVariantTexture(imageName, Gui.ShipShop.ShipShop.ImageType.EMPTY);
        variantImage.unavailableImage[index] = this.LoadVariantTexture(imageName, Gui.ShipShop.ShipShop.ImageType.UNAVAILABLE);
        this.variantListIndex = this.variantImageList.Count;
        this.variantImageList.Add(variantImage);
      }
    }

    public override bool Contains(float2 point)
    {
      if (!base.Contains(point) && !this.getCubits.Contains(point))
        return this.money.Contains(point);
      return true;
    }

    private bool TryGetPaintSystem(HangarShip hangarShip, out ShipSystem paintSystem)
    {
      paintSystem = (ShipSystem) null;
      foreach (ShipSlot slot in hangarShip.Slots)
      {
        if (slot.Card.SystemType == ShipSlotType.ship_paint && slot.System != null && slot.System.PaintCard != null)
        {
          paintSystem = slot.System;
          return true;
        }
      }
      return false;
    }

    public void UpdateShipList()
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      Gui.ShipShop.ShipShop.\u003CUpdateShipList\u003Ec__AnonStoreyB7 listCAnonStoreyB7 = new Gui.ShipShop.ShipShop.\u003CUpdateShipList\u003Ec__AnonStoreyB7();
      // ISSUE: reference to a compiler-generated field
      listCAnonStoreyB7.\u003C\u003Ef__this = this;
      this.PeriodicUpdate();
      if (this.selectedCard == null)
        return;
      if (this.selectedShip != null && !(bool) this.selectedShip.SlotsCreated)
      {
        this.selectedShip.SlotsCreated.AddHandler(new SignalHandler(this.UpdateShipList));
      }
      else
      {
        this.selectedShip = this.selectedCard.GetHangarShip;
        // ISSUE: reference to a compiler-generated field
        if (this.selectedShip != null && this.TryGetPaintSystem(this.selectedShip, out listCAnonStoreyB7.paintSystem))
        {
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          listCAnonStoreyB7.paintSystem.IsLoaded.AddHandler(new SignalHandler(listCAnonStoreyB7.\u003C\u003Em__190));
        }
        else
          this.shipView.SetPrefab(this.selectedCard.WorldCard.PrefabName.ToLower());
        this.shipView.SetShipBanner((int) this.selectedCard.HangarID);
        if (this.selectedShip != null)
        {
          if (this.selectedShip.Card.NextCard != null)
          {
            this.shipStats.ShowDifference(this.selectedShip.Card, this.selectedShip.Card.NextCard);
            this.labelTitle.Text = this.selectedShip.Card.ItemGUICard.Name + " > " + this.selectedShip.Card.NextCard.ItemGUICard.Name;
            this.labelShipTitle.Text = !this.selectedCard.IsUpgraded ? this.selectedShip.Card.ItemGUICard.Name : this.selectedShip.Card.NextCard.ItemGUICard.Name;
          }
          else
          {
            this.shipStats.ShowDifference(this.selectedShip.Card, this.selectedShip.Card);
            this.labelTitle.Text = this.selectedShip.Card.ItemGUICard.Name;
            this.labelShipTitle.Text = this.selectedShip.Card.ItemGUICard.Name;
          }
        }
        else
        {
          this.shipStats.ShowDifference(this.selectedCard, this.selectedCard);
          this.labelTitle.Text = this.selectedCard.ItemGUICard.Name;
          this.labelShipTitle.Text = this.selectedCard.ItemGUICard.Name;
        }
        this.UpdateRequisitionButtonState();
        if (this.selectedCard.HaveShip)
        {
          if (this.selectedCard.IsUpgraded || this.selectedShip.IsVariant)
          {
            this.costPanel.IsRendered = false;
          }
          else
          {
            this.costPanel.CurrentExchange = GuiCostPanel.ExchangeType.UPGRADE;
            this.costPanel.SetProperties((GameItemCard) this.selectedCard, false);
            this.costPanel.IsRendered = true;
          }
          this.commandButton.IsRendered = this.selectedCard.GetHangarShip != Game.Me.ActiveShip;
        }
        else
        {
          this.costPanel.CurrentExchange = GuiCostPanel.ExchangeType.BUY;
          this.costPanel.SetProperties((GameItemCard) this.selectedCard, false);
          this.costPanel.IsRendered = true;
          this.commandButton.IsRendered = false;
        }
        this.UpdateShipVariantState();
      }
    }

    private void UpdateRequisitionButtonState()
    {
      if (this.selectedShip == null)
      {
        int num = this.selectedCard.RequiredPlayerLevel();
        if ((int) Game.Me.Level < num)
        {
          this.reqButton.IsRendered = false;
          this.labelError.IsRendered = true;
          this.labelError.Text = BsgoLocalization.Get("%$bgo.etc.ship_shop_info1%", (object) num);
        }
        else if (this.selectedCard.ShopItemCard.BuyPrice.IsEnoughInHangar(this.selectedCard.ShopItemCard))
        {
          this.reqButton.Text = "%$bgo.etc.ship_shop_requisition%";
          this.reqButton.IsRendered = true;
          this.labelError.IsRendered = false;
        }
        else
        {
          this.reqButton.IsRendered = false;
          this.labelError.IsRendered = true;
          this.UpdateErrorText(this.selectedCard.ShopItemCard.BuyPrice);
        }
      }
      else if (!this.selectedShip.IsUpgraded && !this.selectedShip.IsVariant)
      {
        if (Shop.GetDiscountedShipUpgradePrice(this.selectedCard.ShopItemCard.UpgradePrice).IsEnoughInHangar())
        {
          this.reqButton.Text = "%$bgo.etc.ship_shop_upgrade%";
          this.reqButton.IsRendered = true;
          this.labelError.IsRendered = false;
        }
        else
        {
          this.reqButton.IsRendered = false;
          this.labelError.IsRendered = true;
          this.UpdateErrorText(this.selectedCard.ShopItemCard.UpgradePrice);
        }
      }
      else
      {
        this.reqButton.IsRendered = false;
        this.labelError.IsRendered = false;
      }
    }

    private void UpdateErrorText(Price price)
    {
      string str = string.Empty;
      if (!price.IsEnoughInHangar(ResourceType.Cubits, this.selectedCard.ShopItemCard, 1))
        str = BsgoLocalization.Get("%$bgo.etc.ship_shop_cubits%");
      if (!price.IsEnoughInHangar(ResourceType.Tylium, this.selectedCard.ShopItemCard, 1))
      {
        if (str != string.Empty)
          str += BsgoLocalization.Get("%$bgo.etc.ship_shop_and%");
        str += BsgoLocalization.Get("%$bgo.etc.ship_shop_tylium%");
      }
      if (!price.IsEnoughInHangar(ResourceType.Token, this.selectedCard.ShopItemCard, 1))
      {
        if (str != string.Empty)
          str += BsgoLocalization.Get("%$bgo.etc.ship_shop_and%");
        str += BsgoLocalization.Get("%$bgo.etc.ship_shop_merits%");
      }
      this.labelError.Text = BsgoLocalization.Get("%$bgo.etc.ship_shop_havent_enough%", (object) str);
    }

    private void UpdateShipVariantState()
    {
      this.LoadCurrentShipImages();
      for (int index = 0; index < this.shipVariants.Length; ++index)
      {
        if (this.selectedCard.VariantHangarIDs.Count == 0)
          this.shipVariants[index].IsRendered = false;
        else if (this.variantImageList[this.variantListIndex].emptyImage != null)
        {
          Vector2 positionCenter = this.shipVariants[index].PositionCenter;
          ShipCard variantShipCard = this.shipListCard.GetVariantShipCard(this.selectedCard, (uint) index);
          Vector2 zero = Vector2.zero;
          this.shipVariants[index].IsRendered = true;
          if (variantShipCard != null)
          {
            this.shipVariants[index].IsRendered = true;
            if (this.selectedCard.IsUpgraded && variantShipCard.GetHangarShip != null)
            {
              this.shipVariants[index].Texture = this.variantImageList[this.variantListIndex].fullImage[index];
              zero.x = (float) this.variantImageList[this.variantListIndex].fullImage[index].width;
              zero.y = (float) this.variantImageList[this.variantListIndex].fullImage[index].height;
            }
            else if (this.selectedCard.IsUpgraded)
            {
              this.shipVariants[index].Texture = this.variantImageList[this.variantListIndex].emptyImage[index];
              zero.x = (float) this.variantImageList[this.variantListIndex].emptyImage[index].width;
              zero.y = (float) this.variantImageList[this.variantListIndex].emptyImage[index].height;
            }
            else
            {
              this.shipVariants[index].Texture = this.variantImageList[this.variantListIndex].unavailableImage[index];
              zero.x = (float) this.variantImageList[this.variantListIndex].unavailableImage[index].width;
              zero.y = (float) this.variantImageList[this.variantListIndex].unavailableImage[index].height;
            }
          }
          else
            this.shipVariants[index].IsRendered = false;
          if (this.shipVariants[index].IsRendered)
            this.shipVariants[index].Size = zero;
          this.shipVariants[index].PositionCenter = positionCenter;
        }
      }
    }

    public override void OnShow()
    {
      base.OnShow();
      ShopProtocol.GetProtocol().RequestAllSales();
      FeedbackProtocol.GetProtocol().ReportUiElementShown(FeedbackProtocol.UiElementId.ShipShop);
      this.UpdateShipList();
    }

    public override void OnHide()
    {
      base.OnHide();
      FeedbackProtocol.GetProtocol().ReportUiElementHidden(FeedbackProtocol.UiElementId.ShipShop);
    }

    private void ClosePressed()
    {
      this.IsRendered = false;
      this.shipView.Delete();
      if (this.onCloseOuter == null)
        return;
      this.onCloseOuter();
    }

    [Gui2Editor]
    public static void ShowShipShop()
    {
      Gui.ShipShop.ShipShop shipShop = new Gui.ShipShop.ShipShop();
      shipShop.IsRendered = true;
      Game.RegisterDialog((IGUIPanel) shipShop, true);
    }

    private void CommandPressed()
    {
      if (this.selectedShip == null || this.selectedCard == null)
        return;
      if (this.selectedCard.ShipRoleDeprecated == ShipRoleDeprecated.Carrier && !this.IsCarrierSpawnLevel())
      {
        new InfoBox("%$bgo.hangar.cannot_undock_carrier_from_outpost%").Show((GuiPanel) this);
      }
      else
      {
        HangarShip getHangarShip = this.selectedCard.GetHangarShip;
        if (getHangarShip == null)
          return;
        Game.Me.Hangar.SelectShip(getHangarShip);
      }
    }

    private bool IsCarrierSpawnLevel()
    {
      uint serverId = RoomLevel.GetLevel().ServerId;
      switch (serverId)
      {
        case 0:
        case 6:
        case 49:
          return true;
        default:
          return (int) serverId == 50;
      }
    }

    private enum ImageType
    {
      FULL,
      EMPTY,
      UNAVAILABLE,
    }

    private class VariantImage
    {
      public uint parentHangarID;
      public Texture2D[] fullImage;
      public Texture2D[] emptyImage;
      public Texture2D[] unavailableImage;

      public VariantImage(uint parentID, uint variantLength)
      {
        this.parentHangarID = parentID;
        this.fullImage = new Texture2D[(IntPtr) variantLength];
        this.emptyImage = new Texture2D[(IntPtr) variantLength];
        this.unavailableImage = new Texture2D[(IntPtr) variantLength];
      }
    }
  }
}
