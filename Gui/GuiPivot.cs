﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiPivot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("")]
  public class GuiPivot : GuiElementBase
  {
    [HideInInspector]
    private Texture2D m_selectedTexture;
    [HideInInspector]
    private Texture2D m_normalTexture;
    [HideInInspector]
    public bool m_selected;

    public GuiPivot()
    {
      this.CreateTextures();
    }

    public GuiPivot(GuiPivot copy)
      : base((GuiElementBase) copy)
    {
      this.CreateTextures();
    }

    public GuiPivot(JElementBase json)
      : base(json)
    {
      this.CreateTextures();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiPivot(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JPivot((GuiElementBase) this);
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
      if (this.m_selected)
      {
        this.Size = new Vector2((float) this.m_selectedTexture.width, (float) this.m_normalTexture.height);
        GUI.DrawTexture(this.Rect, (Texture) this.m_selectedTexture);
      }
      else
      {
        this.Size = new Vector2((float) this.m_normalTexture.width, (float) this.m_normalTexture.height);
        GUI.DrawTexture(this.Rect, (Texture) this.m_normalTexture);
      }
    }

    private void CreateTextures()
    {
      this.m_selectedTexture = ResourceLoader.Load<Texture2D>("GUI/Common/visualtoggle_pressed");
      this.m_normalTexture = ResourceLoader.Load<Texture2D>("GUI/Common/visualtoggle_normal");
    }
  }
}
