﻿// Decompiled with JetBrains decompiler
// Type: Gui.PopupTutorialTipManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Gui
{
  public class PopupTutorialTipManager : MonoBehaviour
  {
    private IList<PopupTutorialTipManager.PopupTipData> popupTipQueue = (IList<PopupTutorialTipManager.PopupTipData>) new List<PopupTutorialTipManager.PopupTipData>();
    private const string SINGLE_SPACE = "\n";
    private const string DOUBLE_SPACE = "\n\n";
    private const string BULLET_POINT = "\t-\t";
    private const int m_messageCount = 13;
    private static readonly bool mc_showDebugLogs;
    private PopupTutorialTipManager.PopupTipData currentPopupTip;
    private HelpPopup m_textbox;
    private bool[] m_taskCompleteList;
    private static PopupTutorialTipManager instance;

    public bool ShowPopupTips { get; set; }

    public static PopupTutorialTipManager Instance
    {
      get
      {
        return PopupTutorialTipManager.instance ?? (PopupTutorialTipManager.instance = new GameObject("PopupTutorialTipManager").AddComponent<PopupTutorialTipManager>());
      }
    }

    private bool IsRendered
    {
      set
      {
        if (!((UnityEngine.Object) this.m_textbox != (UnityEngine.Object) null))
          return;
        this.m_textbox.gameObject.SetActive(value);
      }
    }

    private PopupTutorialTipManager()
    {
      this.IsRendered = false;
    }

    private static bool IsHidePopupsApplicable(uint popupTipID)
    {
      return popupTipID <= 9U;
    }

    private void OnOkay()
    {
      this.SetTaskToComplete(this.currentPopupTip.PopupID);
      PlayerProtocol.GetProtocol().SetPopupToSeen(this.currentPopupTip.PopupID);
      this.currentPopupTip = (PopupTutorialTipManager.PopupTipData) null;
      if (this.popupTipQueue.Count > 0)
        this.popupTipQueue.RemoveAt(0);
      this.IsRendered = false;
    }

    public void Update()
    {
      if (Game.IsLoadingLevel || Game.IsQuitting)
        this.IsRendered = false;
      else
        this.UpdatePopupList();
    }

    private void UpdatePopupList()
    {
      if (this.popupTipQueue.Count <= 0 || this.currentPopupTip == this.popupTipQueue[0])
        return;
      this.popupTipQueue = (IList<PopupTutorialTipManager.PopupTipData>) this.popupTipQueue.SkipWhile<PopupTutorialTipManager.PopupTipData>((Func<PopupTutorialTipManager.PopupTipData, bool>) (t =>
      {
        if (this.IsTaskComplete(t.PopupID))
          return true;
        if (PopupTutorialTipManager.IsHidePopupsApplicable(t.PopupID))
          return !this.ShowPopupTips;
        return false;
      })).ToList<PopupTutorialTipManager.PopupTipData>();
      if (this.popupTipQueue.Count > 0)
      {
        if ((UnityEngine.Object) this.m_textbox != (UnityEngine.Object) null)
          this.m_textbox.OnCloseWindow();
        this.m_textbox = MessageBoxFactory.CreateHelpPopup();
        this.m_textbox.SetContent((OnMessageBoxClose) null, this.popupTipQueue[0].Title, this.popupTipQueue[0].Body, 0U);
        this.m_textbox.SetOkayDelegate(new AnonymousDelegate(this.OnOkay));
        this.currentPopupTip = this.popupTipQueue[0];
      }
      this.IsRendered = this.popupTipQueue.Count > 0;
    }

    public void CheckBattlespacePopupTip()
    {
      SpaceLevel level = SpaceLevel.GetLevel();
      if (!GameLevel.Instance.IsBattlespaceAvailable() || this.IsTaskComplete(PopupTipType.BattlespaceAvailable) || !((UnityEngine.Object) level == (UnityEngine.Object) null) && level.IsTutorialMission)
        return;
      this.DisplayPopupTip(PopupTipType.BattlespaceAvailable);
    }

    private void SetNextPopup(string title, string body, uint popupTipID)
    {
      this.popupTipQueue.Add(new PopupTutorialTipManager.PopupTipData()
      {
        Title = title,
        Body = body,
        PopupID = popupTipID
      });
    }

    private void InitTaskCompleteList()
    {
      this.m_taskCompleteList = new bool[13];
      for (int index = 0; index < 13; ++index)
        this.m_taskCompleteList[index] = false;
    }

    public bool IsTaskComplete(PopupTipType type)
    {
      return this.IsTaskComplete((uint) type);
    }

    private bool IsTaskComplete(uint popupTipID)
    {
      if (13U <= popupTipID || this.m_taskCompleteList == null)
        return false;
      return this.m_taskCompleteList[(IntPtr) popupTipID];
    }

    public void SetTaskToComplete(PopupTipType type)
    {
      this.SetTaskToComplete((uint) type);
    }

    public void SetTaskToComplete(uint popupTipID)
    {
      if (this.m_taskCompleteList == null)
        this.InitTaskCompleteList();
      if (13U <= popupTipID)
        return;
      this.m_taskCompleteList[(IntPtr) popupTipID] = true;
    }

    public bool DisplayPopupTip(PopupTipType type)
    {
      return this.DisplayPopupTip((uint) type);
    }

    private bool DisplayPopupTip(uint popupTipID)
    {
      if ((UnityEngine.Object) Game.Instance == (UnityEngine.Object) null || Game.Me == null)
        return false;
      if (this.m_taskCompleteList == null || this.m_taskCompleteList.Length != 13)
        this.InitTaskCompleteList();
      if (13U <= popupTipID)
      {
        Debug.LogError((object) ("The index " + (object) popupTipID + " is out of the range.\nIs the message count of " + (object) 13 + " correct?"));
        return false;
      }
      if (this.m_taskCompleteList[(IntPtr) popupTipID] || PopupTutorialTipManager.IsHidePopupsApplicable(popupTipID) && !this.ShowPopupTips)
        return false;
      string str = Game.Me.Faction != Faction.Colonial ? "cylon" : "human";
      this.SetNextPopup(BsgoLocalization.Get("%$bgo.tip_popup_" + str + ".case" + (object) popupTipID + "_title%"), BsgoLocalization.Get("%$bgo.tip_popup_" + str + ".case" + (object) popupTipID + "_body%", (object) "\n\n", (object) "\n\n\t-\t", (object) "\n\t-\t"), popupTipID);
      this.Update();
      return true;
    }

    private class PopupTipData
    {
      public string Title;
      public string Body;
      public uint PopupID;
    }
  }
}
