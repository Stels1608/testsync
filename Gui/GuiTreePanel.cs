﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiTreePanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/TreePanel/TreePanelLayout")]
  public class GuiTreePanel : GuiPanel
  {
    private float m_childIndent = 10f;
    [HideInInspector]
    private List<GuiPanel> m_subPanels = new List<GuiPanel>();
    private GuiTreePanel.PanelState m_currentPanelState = GuiTreePanel.PanelState.Closed;
    private bool m_showHeaderInfo = true;
    private bool m_showBackground = true;
    private bool m_isAlwayOpen;
    [HideInInspector]
    private float m_animationTimer;
    private float m_animationLength;
    private GuiImage m_background;
    private GuiImage[] m_panelStateImage;
    private GuiButton m_headerButton;

    public float ChildIndent
    {
      get
      {
        return this.m_childIndent;
      }
    }

    public GuiImage[] PanelStateImage
    {
      get
      {
        return this.m_panelStateImage;
      }
    }

    public float AnimationLength
    {
      get
      {
        return this.m_animationLength;
      }
    }

    public bool ShowHeaderInfo
    {
      get
      {
        return this.m_showHeaderInfo;
      }
      set
      {
        this.m_showHeaderInfo = value;
      }
    }

    public bool ShowBackground
    {
      get
      {
        return this.m_showBackground;
      }
      set
      {
        this.m_showBackground = value;
      }
    }

    public GuiButton HeaderButton
    {
      get
      {
        return this.m_headerButton;
      }
    }

    public GuiTreePanel.PanelState CurrentPanelState
    {
      get
      {
        return this.m_currentPanelState;
      }
      set
      {
        if (this.m_currentPanelState == value)
          return;
        this.ChangeState();
      }
    }

    public bool IsOpen
    {
      get
      {
        return this.m_currentPanelState == GuiTreePanel.PanelState.Open;
      }
    }

    public bool IsAlwaysOpen
    {
      get
      {
        return this.m_isAlwayOpen;
      }
      set
      {
        this.m_isAlwayOpen = value;
      }
    }

    public GuiLabel HeaderLabel
    {
      get
      {
        return this.m_headerButton.Label;
      }
      set
      {
        this.m_headerButton.Label = value;
      }
    }

    public GuiImage HeaderImage
    {
      get
      {
        return this.m_headerButton.Image;
      }
      set
      {
        this.m_headerButton.Image = value;
      }
    }

    public bool IsAnimating
    {
      get
      {
        if (this.IsOpen)
          return (double) this.m_animationTimer < (double) this.m_animationLength;
        return (double) this.m_animationTimer > 0.0;
      }
    }

    public float ChildPanelLength
    {
      get
      {
        if (this.m_subPanels.Count > 0)
          return this.m_subPanels[this.m_subPanels.Count - 1].TopLeftCornerWithPadding.y + this.m_subPanels[this.m_subPanels.Count - 1].ElementWithPaddingSizeY - this.m_subPanels[0].TopLeftCornerWithPadding.y;
        return 0.0f;
      }
    }

    public float MinPanelLength
    {
      get
      {
        float num = this.m_headerButton.PositionY + this.m_headerButton.SizeY + this.m_headerButton.ElementPadding.Bottom;
        if (this.m_showHeaderInfo)
          return num;
        return 0.0f;
      }
    }

    public GuiTreePanel()
      : this(false)
    {
    }

    public GuiTreePanel(bool alwaysOpen)
      : base(UseInGuiEditor.GetDefaultLayout<GuiTreePanel>())
    {
      this.IsAlwaysOpen = alwaysOpen;
      this.Initialize();
    }

    public GuiTreePanel(GuiTreePanel copy)
      : base((GuiPanel) copy)
    {
      this.m_childIndent = copy.m_childIndent;
      this.m_isAlwayOpen = copy.m_isAlwayOpen;
      this.m_animationLength = copy.m_animationLength;
      this.m_showHeaderInfo = copy.m_showHeaderInfo;
      this.m_showBackground = copy.m_showBackground;
      this.m_background = new GuiImage(copy.m_background);
      this.m_headerButton = new GuiButton(copy.m_headerButton);
      this.m_headerButton.Pressed = (AnonymousDelegate) (() => this.ChangeState());
      this.m_panelStateImage = new GuiImage[copy.m_panelStateImage.Length];
      for (int index = 0; index < copy.m_panelStateImage.Length; ++index)
        this.m_panelStateImage[index] = new GuiImage(copy.m_panelStateImage[index]);
      for (int index = this.Children.Count - 1; index >= 0; --index)
      {
        if (this.Children[index] is GuiPanel)
          this.AddSubPanel(this.Children[index] as GuiPanel);
        this.RemoveChild(this.Children[index]);
      }
      this.m_currentPanelState = !copy.IsOpen ? GuiTreePanel.PanelState.Open : GuiTreePanel.PanelState.Closed;
      this.ChangeState();
      this.WhenElementChanges = new AnonymousDelegate(this.ElementHasChanged);
    }

    public GuiTreePanel(JTreePanel json)
      : base((JPanel) json)
    {
      this.m_childIndent = json.m_childIndent;
      this.m_isAlwayOpen = json.m_isAlwayOpen;
      this.m_animationLength = json.m_animationLength;
      this.m_showHeaderInfo = json.m_showHeaderInfo;
      this.m_showBackground = json.m_showBackground;
      this.Initialize();
    }

    public void SetBGTexture(GuiImage image)
    {
      this.m_background = image;
    }

    private void Initialize()
    {
      if (this.Children.Count > 0)
      {
        this.m_background = this.Find<GuiImage>("background");
        this.RemoveChild((GuiElementBase) this.m_background);
        this.m_background.Parent = (SRect) this;
        this.m_headerButton = this.Find<GuiButton>("headerButton");
        this.RemoveChild((GuiElementBase) this.m_headerButton);
        this.m_headerButton.Parent = (SRect) this;
        this.m_headerButton.Pressed = (AnonymousDelegate) (() => this.ChangeState());
        this.m_headerButton.ElementPadding = (GuiElementPadding) new Rect(0.0f, 0.0f, 0.0f, 0.0f);
        this.m_headerButton.Position = this.m_headerButton.ElementPadding.TopLeft;
        this.m_panelStateImage = new GuiImage[Enum.GetValues(typeof (GuiTreePanel.PanelState)).Length];
        for (int index = 0; index < this.m_panelStateImage.Length; ++index)
        {
          string name = ((GuiTreePanel.PanelState) index).ToString() + "StateImage";
          this.m_panelStateImage[index] = this.Find<GuiImage>(name);
          this.RemoveChild((GuiElementBase) this.m_panelStateImage[index]);
          this.m_panelStateImage[index].Parent = (SRect) this;
          this.m_panelStateImage[index].PositionX = this.m_headerButton.PositionX + this.m_panelStateImage[index].ElementPadding.Left;
          this.m_panelStateImage[index].PositionY = this.m_headerButton.PositionY + this.m_headerButton.SizeY * 0.5f;
          this.m_panelStateImage[index].PositionY -= (float) ((double) this.m_panelStateImage[index].SizeY * 0.5);
        }
        this.m_currentPanelState = GuiTreePanel.PanelState.Open;
        this.ChangeState();
        this.SizeY = this.MinPanelLength;
      }
      for (int index = this.Children.Count - 1; index >= 0; --index)
      {
        if (this.Children[index] is GuiPanel)
          this.AddSubPanel(this.Children[index] as GuiPanel);
        this.RemoveChild(this.Children[index]);
      }
      this.WhenElementChanges = new AnonymousDelegate(this.ElementHasChanged);
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiTreePanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JTreePanel((GuiElementBase) this);
    }

    public override void EditorDraw()
    {
      GuiElementBase.DrawElement.BeginStencil(this.Rect, true, true);
      if (this.IsOpen || this.IsAnimating)
      {
        if (this.m_showBackground)
          this.m_background.EditorDraw();
        for (int index = 0; index < this.m_subPanels.Count; ++index)
          this.m_subPanels[index].EditorDraw();
      }
      if (this.m_showHeaderInfo)
      {
        this.m_headerButton.EditorDraw();
        this.m_panelStateImage[(int) this.m_currentPanelState].EditorDraw();
      }
      GuiElementBase.DrawElement.EndStencil();
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      this.m_isAlwayOpen = !this.m_showHeaderInfo || this.m_isAlwayOpen;
      this.m_background.Position = new Vector2(0.0f, this.MinPanelLength);
      this.m_background.Size = this.Size - this.m_background.Position;
      this.m_background.EditorUpdate();
      this.m_background.IsFullPanel = false;
      this.m_headerButton.SizeX = this.SizeX - this.m_headerButton.PositionX - this.m_headerButton.ElementPadding.Right;
      this.m_headerButton.Position = this.m_headerButton.ElementPadding.TopLeft;
      this.m_headerButton.EditorUpdate();
      int index1 = (int) this.m_currentPanelState;
      this.m_panelStateImage[index1].EditorUpdate();
      this.m_panelStateImage[index1].PositionX = this.m_headerButton.PositionX + this.m_panelStateImage[index1].ElementPadding.Left;
      this.m_panelStateImage[index1].PositionY = this.m_headerButton.PositionY + this.m_headerButton.SizeY * 0.5f;
      this.m_panelStateImage[index1].PositionY -= (float) ((double) this.m_panelStateImage[index1].SizeY * 0.5);
      for (int index2 = 0; index2 < this.m_subPanels.Count; ++index2)
        this.m_subPanels[index2].EditorUpdate();
      if (this.m_isAlwayOpen)
        this.m_currentPanelState = GuiTreePanel.PanelState.Open;
      if (this.IsOpen)
        this.UpdateSubpanelPositions();
      this.SizeY = !this.IsOpen ? this.MinPanelLength : this.ChildPanelLength + this.MinPanelLength;
    }

    private void ElementHasChanged()
    {
      this.m_headerButton.Position = this.m_headerButton.ElementPadding.TopLeft;
      this.m_headerButton.DoCaching();
      this.m_background.DoCaching();
      this.m_panelStateImage[(int) this.m_currentPanelState].DoCaching();
    }

    public override void Update()
    {
      base.Update();
      this.m_isAlwayOpen = !this.m_showHeaderInfo || this.m_isAlwayOpen;
      this.m_headerButton.SizeX = this.SizeX - this.m_headerButton.PositionX - this.m_headerButton.ElementPadding.Right;
      for (int index = 0; index < this.m_subPanels.Count; ++index)
        this.m_subPanels[index].Update();
      if (this.m_isAlwayOpen)
      {
        this.m_currentPanelState = GuiTreePanel.PanelState.Open;
        this.m_animationTimer = this.m_animationLength;
      }
      if (this.IsOpen)
        this.UpdateSubpanelPositions();
      this.SizeY = this.MinPanelLength + (!this.IsOpen ? 0.0f : this.ChildPanelLength);
      if (this.IsAnimating)
        this.UpdateAnimation();
      ++this.SizeY;
      this.m_background.Position = new Vector2(0.0f, this.MinPanelLength);
      this.m_background.Size = this.Size - this.m_background.Position;
    }

    public override void Draw()
    {
      base.Draw();
      GuiElementBase.DrawElement.BeginStencil(this.Rect, true, false);
      if (this.IsOpen || this.IsAnimating)
      {
        if (this.m_showBackground)
          this.m_background.Draw();
        for (int index = 0; index < this.m_subPanels.Count; ++index)
        {
          this.m_subPanels[index].DoCaching();
          this.m_subPanels[index].Draw();
        }
      }
      if (this.m_showHeaderInfo)
      {
        this.m_headerButton.Draw();
        this.m_panelStateImage[(int) this.m_currentPanelState].Draw();
      }
      GuiElementBase.DrawElement.EndStencil();
    }

    public override void DoCaching()
    {
      base.DoCaching();
      this.m_background.Parent = (SRect) this;
      this.m_background.DoCaching();
      this.m_headerButton.Parent = (SRect) this;
      this.m_headerButton.DoCaching();
      this.m_panelStateImage[(int) this.m_currentPanelState].Parent = (SRect) this;
      this.m_panelStateImage[(int) this.m_currentPanelState].DoCaching();
    }

    private void UpdateAnimation()
    {
      if (this.IsOpen)
      {
        this.m_animationTimer += Time.deltaTime;
        if ((double) this.m_animationTimer > (double) this.m_animationLength)
        {
          this.SizeY = this.MinPanelLength + this.ChildPanelLength;
          this.m_animationTimer = this.m_animationLength;
          return;
        }
      }
      else
      {
        this.m_animationTimer -= Time.deltaTime;
        if ((double) this.m_animationTimer < 0.0)
        {
          this.SizeY = this.MinPanelLength;
          this.m_animationTimer = 0.0f;
          return;
        }
      }
      this.SizeY = this.MinPanelLength + this.ChildPanelLength * (this.m_animationTimer / this.m_animationLength);
    }

    private void UpdateSubpanelPositions()
    {
      float y = this.MinPanelLength;
      float b = 0.0f;
      for (int index = 0; index < this.m_subPanels.Count; ++index)
      {
        this.m_subPanels[index].Position = new Vector2(this.m_childIndent, y) + this.m_subPanels[index].ElementPadding.TopLeft;
        Vector2 vector2 = this.m_subPanels[index].Position + this.m_subPanels[index].Size + this.m_subPanels[index].ElementPadding.BottomRight;
        b = Mathf.Max(vector2.x, b);
        y = vector2.y;
      }
      this.SizeX = b;
    }

    private void ChangeState()
    {
      if (this.IsAlwaysOpen)
      {
        this.m_panelStateImage[1].IsRendered = false;
        this.m_panelStateImage[0].IsRendered = false;
        this.m_currentPanelState = GuiTreePanel.PanelState.Open;
      }
      else
      {
        this.m_panelStateImage[(int) this.m_currentPanelState].IsRendered = false;
        this.m_currentPanelState = !this.IsOpen ? GuiTreePanel.PanelState.Open : GuiTreePanel.PanelState.Closed;
        this.m_panelStateImage[(int) this.m_currentPanelState].IsRendered = true;
      }
    }

    public void AddSubPanel(GuiPanel panel)
    {
      this.m_subPanels.Add(panel);
      panel.Parent = (SRect) this;
      this.UpdateSubpanelPositions();
    }

    public void ClearSubPanels()
    {
      this.m_subPanels.Clear();
      this.UpdateSubpanelPositions();
    }

    public override bool MouseDown(float2 mousePosition, KeyCode mouseKey)
    {
      if (!this.Contains(mousePosition))
        return false;
      if (this.m_showHeaderInfo && this.m_headerButton.Contains(mousePosition) && this.m_headerButton.MouseDown(mousePosition, mouseKey))
        return true;
      if (this.IsOpen)
      {
        for (int index = 0; index < this.m_subPanels.Count; ++index)
        {
          if (this.m_subPanels[index].OnMouseDown(mousePosition, mouseKey))
            return true;
        }
      }
      return false;
    }

    public override bool MouseUp(float2 mousePosition, KeyCode mouseKey)
    {
      if (!this.Contains(mousePosition))
        return false;
      if (this.m_showHeaderInfo && this.m_headerButton.Contains(mousePosition) && this.m_headerButton.MouseUp(mousePosition, mouseKey))
        return true;
      if (this.IsOpen)
      {
        for (int index = 0; index < this.m_subPanels.Count; ++index)
        {
          if (this.m_subPanels[index].OnMouseUp(mousePosition, mouseKey))
            return true;
        }
      }
      return false;
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      if (this.m_showHeaderInfo)
        this.m_headerButton.MouseMove(position, previousPosition);
      if (this.IsOpen)
      {
        for (int index = 0; index < this.m_subPanels.Count; ++index)
          this.m_subPanels[index].MouseMove(position, previousPosition);
      }
      return false;
    }

    public override bool OnKeyDown(KeyCode keyboardKey, Action action)
    {
      for (int index = 0; index < this.m_subPanels.Count; ++index)
      {
        if (this.m_subPanels[index].OnKeyDown(keyboardKey, action))
          return true;
      }
      return false;
    }

    public enum PanelState
    {
      Open,
      Closed,
    }
  }
}
