﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiMenuItem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class GuiMenuItem : GuiPanel
  {
    private const string SWITCH_AMMO_ATLAS_TEXTURE_PATH = "GUI/EquipBuyPanel/SwitchAmmo/items_atlas_ammo";
    private const string SWITCH_AMMO_ABILITY_ATLAS_TEXTURE_PATH = "GUI/EquipBuyPanel/SwitchAmmo/items_atlas_abilities";
    private GuiImage m_icon;
    private GuiLabel m_name;
    private GuiLabel m_installed;
    private GuiLabel m_count;
    private GuiButton m_buyAmmo;
    private GuiButton m_switchAll;
    private GuiLabel m_allLabel;
    private GuiLabel m_buyCount;
    private GuiImage m_abilityIcon;
    private GuiImage m_buyAmmoIcon;
    private bool m_isMouseDown;
    private bool m_inserted;
    private ShipAbility m_ability;
    private ItemCountable m_consumable;
    private AtlasCache m_atlasCache;

    public ItemCountable Consumable
    {
      get
      {
        return this.m_consumable;
      }
    }

    public bool ConsumableInserted
    {
      get
      {
        return this.m_inserted;
      }
      private set
      {
        this.m_inserted = value;
      }
    }

    public bool IsInstalled
    {
      get
      {
        return this.m_installed.IsRendered;
      }
      set
      {
        this.m_installed.IsRendered = value;
      }
    }

    public GuiMenuItem(AtlasCache atlas)
      : base("GUI/_NewGui/layout/SwitchAmmo/GuiMenuItemLayout")
    {
      this.m_atlasCache = atlas;
      this.Initialize();
      this.m_allLabel.Text = "All";
      this.m_buyCount.AllColor = Color.white;
      this.m_allLabel.AllColor = Color.white;
      this.m_buyAmmo.IsRendered = true;
      this.Size = this.m_backgroundImage.Size;
    }

    public GuiMenuItem(GuiMenuItem copy)
      : base((GuiPanel) copy)
    {
      this.m_atlasCache = copy.m_atlasCache;
      this.Initialize();
      if (copy.m_ability == null || copy.m_consumable == null)
        return;
      this.SetData(copy.m_ability, copy.m_consumable);
    }

    private void Initialize()
    {
      this.m_backgroundImage = this.Find<GuiImage>("background");
      this.m_icon = this.Find<GuiImage>("icon");
      this.m_name = this.Find<GuiLabel>("name");
      this.m_installed = this.Find<GuiLabel>("installed");
      this.m_count = this.Find<GuiLabel>("count");
      this.m_buyAmmo = this.Find<GuiButton>("buy_ammo");
      this.m_switchAll = this.Find<GuiButton>("switch_all");
      this.m_allLabel = this.Find<GuiLabel>("allLabel");
      this.m_buyCount = this.Find<GuiLabel>("buyCountLabel");
      this.m_abilityIcon = this.Find<GuiImage>("abilityIcon");
      this.m_buyAmmoIcon = this.Find<GuiImage>("buyAmmoImage");
      this.m_isMouseDown = false;
      this.m_inserted = false;
      this.IsRendered = true;
      this.AddChild((GuiElementBase) new Timer(0.5f));
    }

    public void SetData(ShipAbility ability, ItemCountable consumable)
    {
      this.m_inserted = false;
      this.m_ability = ability;
      this.m_consumable = consumable;
      this.m_installed.IsRendered = false;
      if (this.m_consumable == null)
      {
        this.m_name.Text = "%$bgo.etc.default%";
        this.m_installed.IsRendered = (int) this.m_ability.consumableGuid == 0;
      }
      else
      {
        this.m_name.Text = consumable.ItemGUICard.Name;
        this.m_count.Text = consumable.Count <= 0U ? "%$bgo.AbilityToolbar.InsertConsumable.layout.empty%" : "x" + consumable.Count.ToString();
        this.m_buyCount.Text = "x" + consumable.Card.buyCount.ToString();
        if ((bool) ability.card.IsLoaded && (bool) consumable.Card.IsLoaded && (int) ability.consumableGuid == (int) consumable.CardGUID)
          this.m_installed.IsRendered = true;
        this.m_switchAll.SetTooltip("%$bgo.AbilityToolbar.InsertConsumable.layout.SwitchAllAmmo%");
        string text = "%$bgo.AbilityToolbar.InsertConsumable.layout.Price%";
        using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = consumable.ShopItemCard.BuyPrice.items.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
            float result = current.Value * (float) consumable.Card.buyCount;
            Shop.ApplyDiscount(out result, consumable.Card.CardGUID, result);
            text = text + "\n" + (object) result + " " + current.Key.GUICard.Name;
          }
        }
        this.m_buyAmmo.SetTooltip(text);
        this.SetStoreToolTip((GameItemCard) this.m_consumable, ShopInventoryContainer.Store, false);
        this.RecalculateAbsCoords();
      }
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      ItemCountable itemCountable = Game.Me.Hold.Consumables.GetByGUID(this.m_consumable.CardGUID) as ItemCountable;
      if (itemCountable != null)
      {
        this.m_consumable.count = itemCountable.count;
        this.m_count.Text = itemCountable.Count <= 0U ? "%$bgo.AbilityToolbar.InsertConsumable.layout.empty%" : "x" + itemCountable.Count.ToString();
      }
      this.m_ability = Game.Me.ActiveShip.GetSlot(this.m_ability.slot.ServerID).Ability;
      this.m_installed.IsRendered = (int) this.m_ability.consumableGuid == (int) this.m_consumable.CardGUID;
    }

    public override void Draw()
    {
      this.m_backgroundImage.Draw();
      AtlasEntry atlasEntry1 = (AtlasEntry) null;
      if (this.m_consumable != null)
      {
        if (this.m_ability.card != null && this.m_consumable.ItemGUICard != null)
          atlasEntry1 = this.m_atlasCache.GetCachedEntryBy(this.m_consumable.ItemGUICard.GUIAtlasTexturePath, (int) this.m_consumable.ItemGUICard.FrameIndex);
      }
      else if (this.m_ability.guiCard != null)
        atlasEntry1 = this.m_atlasCache.GetCachedEntryBy(this.m_ability.guiCard.GUIAtlasTexturePath, (int) this.m_ability.guiCard.FrameIndex);
      AtlasEntry atlasEntry2 = this.m_atlasCache.GetCachedEntryBy("GUI/EquipBuyPanel/SwitchAmmo/items_atlas_abilities", (int) this.m_ability.guiCard.FrameIndex);
      if (atlasEntry1 != null && Event.current.type == UnityEngine.EventType.Repaint)
        Graphics.DrawTexture(this.m_icon.Rect, (Texture) atlasEntry1.Texture, atlasEntry1.FrameRect, 0, 0, 0, 0);
      this.m_name.Draw();
      this.m_count.Draw();
      this.m_switchAll.Draw();
      if (this.m_installed.IsRendered)
        this.m_installed.Draw();
      if (!this.m_consumable.disableBuying)
      {
        this.m_buyAmmo.Draw();
        this.m_buyAmmoIcon.Draw();
        this.m_buyCount.Draw();
      }
      if (atlasEntry2 == null || atlasEntry2 == this.m_atlasCache.UnknownItem)
        atlasEntry2 = this.FindFallbackIcon();
      if (atlasEntry2 != null && Event.current.type == UnityEngine.EventType.Repaint)
        this.DrawTripleTexture(atlasEntry2);
      this.m_allLabel.Draw();
    }

    private AtlasEntry FindFallbackIcon()
    {
      AbilityActionType abilityActionType = this.m_ability.card.ActionType;
      switch (abilityActionType)
      {
        case AbilityActionType.FireMissle:
          return this.m_atlasCache.GetCachedEntryBy("GUI/EquipBuyPanel/SwitchAmmo/items_atlas_abilities", 39);
        case AbilityActionType.FireCannon:
          return this.m_atlasCache.GetCachedEntryBy("GUI/EquipBuyPanel/SwitchAmmo/items_atlas_abilities", 33);
        case AbilityActionType.Buff:
          if ((int) this.m_ability.guiCard.FrameIndex == 221)
            return this.m_atlasCache.GetCachedEntryBy("GUI/EquipBuyPanel/SwitchAmmo/items_atlas_abilities", 27);
          break;
        case AbilityActionType.Flak:
          return this.m_atlasCache.GetCachedEntryBy("GUI/EquipBuyPanel/SwitchAmmo/items_atlas_abilities", 89);
        case AbilityActionType.PointDefence:
          return this.m_atlasCache.GetCachedEntryBy("GUI/EquipBuyPanel/SwitchAmmo/items_atlas_abilities", 26);
        default:
          if (abilityActionType == AbilityActionType.FireLightMissile || abilityActionType == AbilityActionType.FireHeavyMissile)
            goto case AbilityActionType.FireMissle;
          else
            break;
      }
      return this.m_atlasCache.UnknownItem;
    }

    private void DrawTripleTexture(AtlasEntry atlasEntry)
    {
      Rect rect1 = this.m_abilityIcon.Rect;
      rect1.x += 4f;
      rect1.y += 1.5f;
      Rect rect2 = this.m_abilityIcon.Rect;
      rect2.x -= 4f;
      rect2.y -= 1.5f;
      Graphics.DrawTexture(rect1, (Texture) atlasEntry.Texture, atlasEntry.FrameRect, 0, 0, 0, 0);
      Graphics.DrawTexture(this.m_abilityIcon.Rect, (Texture) atlasEntry.Texture, atlasEntry.FrameRect, 0, 0, 0, 0);
      Graphics.DrawTexture(rect2, (Texture) atlasEntry.Texture, atlasEntry.FrameRect, 0, 0, 0, 0);
    }

    public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
    {
      if (mouseKey == KeyCode.Mouse0 && this.Contains(mousePosition))
        this.m_isMouseDown = true;
      if (!base.OnMouseDown(mousePosition, mouseKey))
        return this.m_isMouseDown;
      return true;
    }

    public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
    {
      if (this.m_isMouseDown && mouseKey == KeyCode.Mouse0)
      {
        if (this.m_buyAmmo.Contains(mousePosition) && this.m_buyAmmo.MouseDown(mousePosition, mouseKey))
        {
          this.m_isMouseDown = false;
          this.ConsumableInserted = true;
          int count = (int) this.m_consumable.Card.buyCount;
          if (!this.m_consumable.ShopItemCard.BuyPrice.IsEnoughInHangar(this.m_consumable.ShopItemCard, count))
            FacadeFactory.GetInstance().SendMessage(Message.ShopResourcesMissing, (object) this.m_consumable.ShopItemCard.BuyPrice.GetMissingResources(count));
          else if (this.IsFilledHold(this.m_consumable))
            new InfoBox("%$bgo.EquipBuyPanel.gui_filledhold_layout.text_label%").Show();
          else
            this.m_ability.slot.SelectConsumable(this.m_consumable);
          this.m_consumable.MoveTo((IContainer) Game.Me.Hold, this.m_consumable.Card.buyCount);
        }
        else if (this.m_switchAll.Contains(mousePosition) && this.m_switchAll.MouseDown(mousePosition, mouseKey))
        {
          this.m_isMouseDown = false;
          this.ConsumableInserted = true;
          Game.Me.ActiveShip.SwitchAmmoAllSlots(this.m_consumable);
          Game.GUIManager.Find<GuiConsumablePanel>().IsRendered = false;
        }
        else if (this.Contains(mousePosition) && this.MouseDown(mousePosition, mouseKey))
        {
          this.m_isMouseDown = false;
          this.ConsumableInserted = true;
          this.m_ability.slot.SelectConsumable(this.m_consumable);
          Game.GUIManager.Find<GuiConsumablePanel>().IsRendered = false;
        }
        return true;
      }
      this.m_isMouseDown = false;
      return false;
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      base.MouseMove(position, previousPosition);
      if (this.m_switchAll.Rect.Contains(position.ToV2()))
      {
        Game.TooltipManager.ShowTooltip(this.m_switchAll.ToolTip);
        return true;
      }
      if (this.m_buyAmmo.Rect.Contains(position.ToV2()))
      {
        Game.TooltipManager.ShowTooltip(this.m_buyAmmo.ToolTip);
        return true;
      }
      if (!this.Rect.Contains(position.ToV2()))
        return false;
      Game.TooltipManager.ShowTooltip(this.ToolTip);
      return true;
    }

    private bool IsFilledHold(ItemCountable item)
    {
      if (Game.Me.Hold.Count < 70)
        return false;
      foreach (Card card in (IEnumerable<ShipItem>) Game.Me.Hold)
      {
        if ((int) card.CardGUID == (int) item.CardGUID)
          return false;
      }
      return true;
    }
  }
}
