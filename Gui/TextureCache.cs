﻿// Decompiled with JetBrains decompiler
// Type: Gui.TextureCache
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class TextureCache
  {
    public Dictionary<Color, Texture2D> cache = new Dictionary<Color, Texture2D>();

    public static Texture2D Get(Color color)
    {
      TextureCache textureCache = !((Object) GameLevel.Instance != (Object) null) || Game.GUIManager == null ? new TextureCache() : Game.GUIManager.TextureCache;
      Texture2D texture2D = (Texture2D) null;
      if (textureCache.cache.TryGetValue(color, out texture2D) && (Object) texture2D != (Object) null)
        return texture2D;
      texture2D = new Texture2D(1, 1);
      if (!Application.isPlaying)
        texture2D.hideFlags = HideFlags.HideAndDontSave;
      texture2D.SetPixel(0, 0, color);
      texture2D.Apply();
      textureCache.cache[color] = texture2D;
      return texture2D;
    }
  }
}
