﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiAtlasImage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiAtlasImage : GuiAtlasImageBase
  {
    private GUICard guiCard;

    public GUICard GuiCard
    {
      get
      {
        return this.guiCard;
      }
      set
      {
        this.guiCard = value;
        if (this.guiCard != null)
          this.guiCard.IsLoaded.AddHandler(new SignalHandler(this.Init));
        else
          this.Init();
      }
    }

    public GuiAtlasImage()
      : this(GuiAtlasImageBase.smallSize)
    {
    }

    public GuiAtlasImage(Vector2 textureSize)
      : base(textureSize)
    {
    }

    private void Init()
    {
      if (this.guiCard != null)
      {
        this.Texture = ResourceLoader.Load<Texture2D>(this.guiCard.GUIAtlasTexturePath);
        this.TextureIndex = (uint) this.guiCard.FrameIndex;
      }
      else
        this.Texture = (Texture2D) null;
    }
  }
}
