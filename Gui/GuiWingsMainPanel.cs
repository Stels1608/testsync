﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiWingsMainPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class GuiWingsMainPanel : GuiPanel
  {
    private const string mc_defaultLayout = "GUI/_NewGui/layout/wings/wingsMainPanel";
    private const string mc_wingNameLabel = "nameOfWing";
    private const string mc_membersOnlineName = "membersOnlineLabel";
    private const string mc_hotkeyLabelName = "hotKey";
    private GuiLabel m_wingName;
    private GuiLabel m_membersOnline;
    private GuiLabel m_hoykeyLabel;
    private GuiWingsCreationPanel m_creationPanel;
    private GuiTabPanel m_tabPanel;
    private GuiWingsManagementPanel m_managementPanel;
    private bool toggleBlocked;

    public override bool IsBigWindow
    {
      get
      {
        return true;
      }
    }

    public override bool IsRendered
    {
      get
      {
        return base.IsRendered;
      }
      set
      {
        base.IsRendered = value;
        if (Game.InputDispatcher.Focused is GUIChatNew)
          return;
        Game.InputDispatcher.Focused = !value ? Game.InputDispatcher.Focused : (InputListener) this;
      }
    }

    public GuiWingsMainPanel()
      : base("GUI/_NewGui/layout/wings/wingsMainPanel")
    {
      if ((Object) Game.Instance != (Object) null)
        this.AddChild((GuiElementBase) new Timer(1f));
      this.Initialize();
    }

    private void Initialize()
    {
      this.IsRendered = false;
      this.Align = Align.MiddleCenter;
      this.Find<GuiButton>("closeButton").Pressed += (AnonymousDelegate) (() => this.IsRendered = false);
      this.m_wingName = this.Find<GuiLabel>("nameOfWing");
      this.m_membersOnline = this.Find<GuiLabel>("membersOnlineLabel");
      this.m_hoykeyLabel = this.Find<GuiLabel>("hotKey");
      this.m_hoykeyLabel.AutoSize = true;
      this.m_creationPanel = this.Find<GuiWingsCreationPanel>();
      this.m_tabPanel = this.Find<GuiTabPanel>();
      foreach (GuiPanel guiPanel in this.m_tabPanel.GetGuiPanelList())
      {
        if (guiPanel is GuiWingsManagementPanel)
          this.m_managementPanel = (GuiWingsManagementPanel) guiPanel;
      }
      this.RegisterHotKey(Action.ToggleWindowWingRoster, new AnonymousDelegate(this.ToggleIsRendered));
      this.PeriodicUpdate();
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      if (!((Object) Game.Instance != (Object) null))
        return;
      this.m_wingName.Text = Game.Me.Guild.Name;
      this.m_membersOnline.Text = BsgoLocalization.Get("%$bgo.Wings.wings_labels.members_online%", (object) this.MembersOnline().Count, (object) Game.Me.Guild.Players.Count);
      this.m_creationPanel.IsRendered = !Game.Me.Guild.Has;
      this.m_tabPanel.IsRendered = !this.m_creationPanel.IsRendered;
      this.m_membersOnline.IsRendered = !this.m_creationPanel.IsRendered;
      this.m_wingName.IsRendered = !this.m_creationPanel.IsRendered;
      this.m_hoykeyLabel.Text = Tools.GetHotKeyFor(Action.ToggleWindowWingRoster);
    }

    private List<Player> MembersOnline()
    {
      List<Player> playerList = new List<Player>();
      for (int index = 0; index < Game.Me.Guild.Players.Count; ++index)
      {
        if (Game.Me.Guild.Players[index].Online)
          playerList.Add(Game.Me.Guild.Players[index]);
      }
      return playerList;
    }

    public void ToggleIsRendered()
    {
      if (!Game.GUIManager.IsRendered)
        return;
      this.IsRendered = !this.IsRendered;
    }

    public override void Draw()
    {
      if (this.m_creationPanel.IsRendered)
        this.toggleBlocked = this.m_creationPanel.HasFocus();
      if (this.m_managementPanel.IsRendered)
        this.toggleBlocked = this.m_managementPanel.HasFocus();
      if (!(Game.InputDispatcher.Focused is GUIChatNew))
        Game.InputDispatcher.Focused = !this.toggleBlocked ? Game.InputDispatcher.Focused : (InputListener) this;
      base.Draw();
    }

    public void AddMeToWing()
    {
      if (!((Object) Game.Instance != (Object) null))
        return;
      this.m_creationPanel.IsRendered = Game.Me.Guild.Players.Count == 0;
      this.m_tabPanel.IsRendered = !this.m_creationPanel.IsRendered;
      this.m_membersOnline.IsRendered = !this.m_creationPanel.IsRendered;
      this.m_wingName.IsRendered = !this.m_creationPanel.IsRendered;
    }
  }
}
