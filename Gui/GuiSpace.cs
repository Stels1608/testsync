﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiSpace
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiSpace : GuiElementBase
  {
    public GuiSpace()
      : this(10f, 10f)
    {
    }

    public GuiSpace(float width, float height)
    {
      this.Size = new Vector2(width, height);
    }
  }
}
