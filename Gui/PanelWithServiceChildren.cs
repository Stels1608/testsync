﻿// Decompiled with JetBrains decompiler
// Type: Gui.PanelWithServiceChildren
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Gui
{
  public class PanelWithServiceChildren : GuiPanel
  {
    private List<GuiElementBase> regilarChildren = new List<GuiElementBase>();
    private List<GuiElementBase> serviceChildren = new List<GuiElementBase>();

    public override ReadOnlyCollection<GuiElementBase> Children
    {
      get
      {
        return this.regilarChildren.AsReadOnly();
      }
    }

    public virtual ReadOnlyCollection<GuiElementBase> ServiceChildren
    {
      get
      {
        return this.serviceChildren.AsReadOnly();
      }
    }

    public override void AddChild(GuiElementBase ch)
    {
      this.regilarChildren.Add(ch);
      base.AddChild(ch);
    }

    public override void RemoveChild(GuiElementBase ch)
    {
      this.regilarChildren.Remove(ch);
      base.RemoveChild(ch);
    }

    public virtual void AddServiceChild(GuiElementBase ch)
    {
      this.serviceChildren.Add(ch);
      base.AddChild(ch);
    }

    public virtual void RemoveServiceChild(GuiElementBase ch)
    {
      this.serviceChildren.Remove(ch);
      base.RemoveChild(ch);
    }

    public void EmptyServiceChildren()
    {
      while (this.ServiceChildren.Count > 0)
        this.RemoveServiceChild(this.ServiceChildren[0]);
    }

    public override bool OnShowTooltip(float2 position)
    {
      bool flag = base.OnShowTooltip(position);
      foreach (GuiElementBase serviceChild in this.ServiceChildren)
      {
        if (serviceChild.IsRendered && serviceChild.Contains(position))
          flag |= serviceChild.OnShowTooltip(position);
      }
      return flag;
    }

    public override bool Contains(float2 point)
    {
      if (this.MouseTransparent)
      {
        for (int index = 0; index < this.Children.Count; ++index)
        {
          if (this.Children[index].Contains(point))
            return true;
        }
        return false;
      }
      bool flag = base.Contains(point);
      GUIManager.MouseOverAnyOldGuiElement |= flag;
      return flag;
    }
  }
}
