﻿// Decompiled with JetBrains decompiler
// Type: Gui.TimerSimple
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class TimerSimple : GuiElementBase
  {
    [Gui2Editor(write = false)]
    public AnonymousDelegate Event;
    [Gui2Editor]
    private float interval;
    private float time;
    private bool autoDestroy;

    public TimerSimple(float interval, bool autoDestroy, AnonymousDelegate action)
      : this(interval, action)
    {
      this.autoDestroy = autoDestroy;
    }

    public TimerSimple(float interval, AnonymousDelegate action)
    {
      this.interval = interval;
      this.Event = action;
      this.Start();
    }

    public override void Update()
    {
      if ((double) Time.realtimeSinceStartup - (double) this.time < (double) this.interval)
        return;
      this.time = Time.realtimeSinceStartup;
      if (this.Event != null)
      {
        if (!GUIManager.DebugProfile)
          ;
        this.Event();
        if (!GUIManager.DebugProfile)
          ;
      }
      if (this.autoDestroy)
        this.RemoveThisFromParentLater();
      else
        this.IsRendered = false;
    }

    private void DefaultAction()
    {
      if (!GUIManager.DebugProfile)
        ;
      (this.Parent as GuiElementBase).PeriodicUpdate();
      if (GUIManager.DebugProfile)
        ;
    }

    public void Start()
    {
      this.IsRendered = true;
      this.time = Time.realtimeSinceStartup;
    }
  }
}
