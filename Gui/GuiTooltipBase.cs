﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiTooltipBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiTooltipBase : GuiDialog
  {
    public virtual Vector2 MousePosition
    {
      set
      {
        this.Position = value;
        this.PositionY -= (float) (double) this.SizeY;
        if ((double) this.PositionX + (double) this.SizeX > (double) Screen.width)
          this.PositionX = (float) ((double) Screen.width - (double) this.SizeX - 10.0);
        if ((double) this.PositionY >= 0.0)
          return;
        this.PositionY = 10f;
      }
    }

    public GuiTooltipBase()
    {
      this.m_backgroundImage.Texture = ResourceLoader.Load<Texture2D>("GUI/Gui2/tooltip");
      this.m_backgroundImage.NineSliceEdge = (GuiElementPadding) new Rect(9f, 9f, 9f, 9f);
      this.CloseButton.IsRendered = false;
      this.Size = this.m_backgroundImage.Size;
    }

    public virtual bool CanShowTooltip()
    {
      return true;
    }
  }
}
