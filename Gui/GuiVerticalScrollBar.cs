﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiVerticalScrollBar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  public class GuiVerticalScrollBar : GuiScrollBarBase
  {
    private const string mc_layoutPath = "GUI/_NewGui/layout/verticalScrollBarLayout";
    private const string mc_layoutPath_oldStyle = "GUI/_NewGui/layout/verticalScrollBarLayout_oldStyle";

    public GuiVerticalScrollBar(bool useOldStyle)
      : base(!useOldStyle ? "GUI/_NewGui/layout/verticalScrollBarLayout" : "GUI/_NewGui/layout/verticalScrollBarLayout_oldStyle")
    {
      this.Initialize();
    }

    public GuiVerticalScrollBar()
      : base("GUI/_NewGui/layout/verticalScrollBarLayout")
    {
      this.Initialize();
    }

    public GuiVerticalScrollBar(GuiVerticalScrollBar copy)
      : base((GuiScrollBarBase) copy)
    {
      this.Initialize();
    }

    public GuiVerticalScrollBar(JVerticalScrollBar json)
      : base((JScrollBarPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiVerticalScrollBar(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JVerticalScrollBar((GuiElementBase) this);
    }

    public override void EditorDraw()
    {
      if ((double) this.m_negativeScrollButton.ElementPaddingRect.height + (double) this.m_slideBar.ElementPaddingRect.height + (double) this.m_positiveScrollButton.ElementPaddingRect.height > (double) this.SizeY)
        return;
      base.EditorDraw();
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      this.ElementHasChanged();
    }

    private void Initialize()
    {
      this.WhenElementChanges = new AnonymousDelegate(this.ElementHasChanged);
      this.StencilOn = true;
    }

    public override void OnShow()
    {
      base.OnShow();
      if (this.WhenElementChanges == null)
        return;
      this.WhenElementChanges();
    }

    public override void Draw()
    {
      if ((double) this.m_negativeScrollButton.ElementPaddingRect.height + (double) this.m_slideBar.ElementPaddingRect.height + (double) this.m_positiveScrollButton.ElementPaddingRect.height > (double) this.SizeY || !this.m_alwaysShow && (double) this.SizeY >= (double) this.m_scrollDistance)
        return;
      base.Draw();
    }

    protected override void ScrollAmount(float amount)
    {
      this.CalculateSliderBar();
      base.ScrollAmount(amount);
      this.m_slideBar.PositionY = this.m_negativeScrollButton.PositionY + this.m_negativeScrollButton.SizeY;
      this.m_slideBar.PositionY += (float) ((double) this.m_negativeScrollButton.ElementPadding.Bottom + (double) this.m_scrollPosition);
    }

    protected override void ElementHasChanged()
    {
      if (this.Parent != null)
      {
        this.Position = new Vector2(this.Parent.SizeX - this.SizeX - this.ElementPadding.Right, this.ElementPadding.Top);
        this.SizeY = this.Parent.SizeY - this.ElementPadding.Top - this.ElementPadding.Bottom;
      }
      this.SizeX = this.m_negativeScrollButton.ElementWithPaddingSizeX;
      this.m_positiveScrollButton.PositionY = this.SizeY - this.m_positiveScrollButton.SizeY - this.m_positiveScrollButton.ElementPadding.Bottom;
      this.m_slideBar.SizeX = this.SizeX - this.m_slideBar.ElementPadding.Left - this.m_slideBar.ElementPadding.Right;
      this.m_negativeScrollButton.PositionX = this.m_negativeScrollButton.ElementPadding.Left;
      this.m_positiveScrollButton.PositionX = this.m_positiveScrollButton.ElementPadding.Left;
      this.m_slideBar.PositionX = this.m_slideBar.ElementPadding.Left;
      this.ScrollAmount(0.0f);
    }

    private void CalculateSliderBar()
    {
      this.m_sliderDistance = this.m_negativeScrollButton.PositionY + this.m_negativeScrollButton.SizeY + this.m_negativeScrollButton.ElementPadding.Bottom;
      this.m_sliderDistance = this.m_positiveScrollButton.PositionY - this.m_positiveScrollButton.ElementPadding.Top - this.m_sliderDistance;
      this.m_sliderDistance -= (float) ((double) this.m_slideBar.ElementPadding.Top - (double) this.m_slideBar.ElementPadding.Bottom);
      if ((double) this.m_scrollDistance > 0.0)
        this.m_slideBar.SizeY = Mathf.Min(this.m_sliderDistance, this.m_sliderDistance * (this.SizeY / this.m_scrollDistance));
      else
        this.m_slideBar.SizeY = this.m_sliderDistance;
      this.m_slideBar.SizeY = Mathf.Max(this.m_slideBar.Image.NineSliceEdge.Top + this.m_slideBar.Image.NineSliceEdge.Bottom, this.m_slideBar.SizeY);
    }
  }
}
