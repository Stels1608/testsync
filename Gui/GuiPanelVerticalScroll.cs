﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiPanelVerticalScroll
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Core;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Gui
{
  public class GuiPanelVerticalScroll : GuiPanel
  {
    protected QueueVertical queue = new QueueVertical();
    protected Scroller scroller = new Scroller();
    protected List<GuiElementBase> allElements = new List<GuiElementBase>();
    private int shownStart;
    private int shownCount;

    [Gui2Editor(write = false)]
    public int ShownStart
    {
      get
      {
        return this.shownStart;
      }
    }

    [Gui2Editor(write = false)]
    public int ShownCount
    {
      get
      {
        return this.shownCount;
      }
    }

    [Gui2Editor]
    public bool AutoSizeChildren
    {
      get
      {
        return this.queue.AutoSizeChildren;
      }
      set
      {
        this.queue.AutoSizeChildren = value;
      }
    }

    [Gui2Editor]
    public bool RenderScrollLines
    {
      get
      {
        return this.scroller.RenderScrollLines;
      }
      set
      {
        this.scroller.RenderScrollLines = value;
      }
    }

    [Gui2Editor]
    public bool RenderDelimiters
    {
      get
      {
        return this.queue.RenderDelimiters;
      }
      set
      {
        this.queue.RenderDelimiters = value;
      }
    }

    [Gui2Editor]
    public float Space
    {
      get
      {
        return this.queue.Space;
      }
      set
      {
        this.queue.Space = value;
      }
    }

    public ReadOnlyCollection<GuiElementBase> Queue
    {
      get
      {
        return this.queue.Children;
      }
    }

    public override ReadOnlyCollection<GuiElementBase> Children
    {
      get
      {
        return this.allElements.AsReadOnly();
      }
    }

    public System.Action<ISelectable> OnSelectionChanged
    {
      get
      {
        return this.queue.OnSelectionChanged;
      }
      set
      {
        this.queue.OnSelectionChanged = value;
      }
    }

    public ISelectable Selected
    {
      get
      {
        return this.queue.Selected;
      }
      set
      {
        this.queue.Selected = value;
      }
    }

    public GuiPanelVerticalScroll()
    {
      this.RequireBackRepositioning = true;
      this.scroller.onScroll = new System.Action<int>(this.ScrollerChanged);
      this.scroller.onScrollPercent = new System.Action<float>(this.ScrollerChangedAbsolutePercent);
      base.AddChild((GuiElementBase) this.scroller);
      this.queue.DrawSelection = true;
      this.queue.SelectionCutEdges = true;
      base.AddChild((GuiElementBase) this.queue);
      this.MouseTransparent = true;
    }

    protected virtual void ScrollerChanged(int delta)
    {
      this.ScrollerChangedAbsolute(Mathf.Clamp(this.shownStart + delta, 0, this.allElements.Count != 0 ? this.allElements.Count - 1 : 0));
    }

    protected virtual void ScrollerChangedAbsolutePercent(float startPercent)
    {
      float num1 = this.scroller.invisibleBeforeHeight + this.scroller.visibleHeight + this.scroller.invisibleAfterHeight;
      float num2 = 0.0f;
      int start;
      for (start = 0; start < this.allElements.Count && (double) num2 < (double) startPercent * (double) num1; ++start)
        num2 += this.allElements[start].SizeY;
      this.ScrollerChangedAbsolute(start);
    }

    [Gui2Editor]
    public void ScrollToEnd()
    {
      if (this.allElements.Count == 0)
        return;
      int start = this.allElements.Count - 1;
      float sizeY = this.allElements[start].SizeY;
      for (; start >= 0; --start)
      {
        GuiElementBase guiElementBase = this.allElements[start];
        sizeY += guiElementBase.Size.y;
        if ((double) sizeY > (double) this.SizeY)
          break;
      }
      this.ScrollerChangedAbsolute(start);
    }

    public void ScrollToStart()
    {
      this.ScrollerChangedAbsolute(0);
    }

    private void ScrollerChangedAbsolute(int start)
    {
      this.shownStart = Mathf.Clamp(start, 0, this.allElements.Count != 0 ? this.allElements.Count - 1 : 0);
      this.queue.DeselectWhenRemoved = false;
      this.queue.EmptyChildren();
      float visibleHeight = 0.0f;
      int index;
      for (index = this.shownStart; index < this.allElements.Count; ++index)
      {
        GuiElementBase ch = this.allElements[index];
        float sizeY = ch.SizeY;
        if ((double) visibleHeight + (double) sizeY <= (double) this.SizeY || index == this.shownStart)
        {
          visibleHeight += sizeY;
          this.queue.AddChild(ch);
          if (this.RenderDelimiters)
            visibleHeight += this.Space + this.queue.delimeterSize;
        }
        else
        {
          if (index == this.shownStart)
          {
            visibleHeight += sizeY;
            break;
          }
          break;
        }
      }
      this.queue.DeselectWhenRemoved = true;
      this.shownCount = index - this.shownStart;
      this.UpdateScroller(visibleHeight);
      this.OnScroll();
    }

    private void UpdateScroller(float visibleHeight)
    {
      this.scroller.visibleHeight = visibleHeight;
      this.scroller.invisibleBeforeHeight = this.CalculateElementsHeight(0, this.shownStart - 1);
      this.scroller.invisibleAfterHeight = this.CalculateElementsHeight(this.shownStart + this.shownCount, this.allElements.Count - 1);
      this.scroller.nextItemHeight = this.shownStart + this.shownCount < this.allElements.Count - 1 ? this.CalculateElementsHeight(this.shownStart + this.shownCount, this.shownStart + this.shownCount) : 0.0f;
      this.scroller.previousItemHeight = this.shownStart != 0 ? this.CalculateElementsHeight(this.shownStart - 1, this.shownStart - 1) : 0.0f;
      this.scroller.ForceReposition();
    }

    protected float CalculateElementsHeight(int start, int end)
    {
      if (start < 0 || end >= this.allElements.Count || start > end)
        return 0.0f;
      float num = 0.0f;
      for (int index = start; index <= end; ++index)
        num += this.allElements[index].Size.y;
      return num;
    }

    public override void Reposition()
    {
      this.queue.Size = new Vector2(this.Size.x - this.scroller.Size.x, this.Size.y);
      this.ScrollerChanged(0);
      base.Reposition();
    }

    public virtual void OnScroll()
    {
    }

    public override void AddChild(GuiElementBase ch)
    {
      this.allElements.Add(ch);
      this.ForceReposition();
    }

    public override void RemoveChild(GuiElementBase ch)
    {
      this.allElements.Remove(ch);
      if (this.Selected as GuiElementBase == ch)
        this.Selected = (ISelectable) null;
      this.ForceReposition();
    }

    public override bool ScrollDown(float2 position)
    {
      this.scroller.ScrollDown();
      base.ScrollDown(position);
      return true;
    }

    public override bool ScrollUp(float2 position)
    {
      this.scroller.ScrollUp();
      base.ScrollUp(position);
      return true;
    }

    public override void Update()
    {
      base.Update();
      for (int index = 0; index < this.allElements.Count && index < this.shownStart; ++index)
        this.allElements[index].Update();
      for (int index = this.shownStart + this.shownCount; index < this.allElements.Count; ++index)
        this.allElements[index].Update();
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      for (int index = 0; index < this.allElements.Count && index < this.shownStart; ++index)
        this.allElements[index].PeriodicUpdate();
      for (int index = this.shownStart + this.shownCount; index < this.allElements.Count; ++index)
        this.allElements[index].PeriodicUpdate();
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      return base.MouseDown(position, key);
    }
  }
}
