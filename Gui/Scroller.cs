﻿// Decompiled with JetBrains decompiler
// Type: Gui.Scroller
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

namespace Gui
{
  public class Scroller : GuiPanel
  {
    private readonly float SPACE = 2f;
    private readonly float LINE_WIDTH = 1f;
    private GuiButton upButton = new GuiButton(string.Empty, "GUI/Common/up_normal", "GUI/Common/up_over", "GUI/Common/up_pressed");
    private GuiButton downButton = new GuiButton(string.Empty, "GUI/Common/down_normal", "GUI/Common/down_over", "GUI/Common/down_pressed");
    private ScrollerBox box = new ScrollerBox();
    private GuiColoredBox leftLine = new GuiColoredBox();
    private GuiColoredBox rightLine = new GuiColoredBox();
    public Action<int> onScroll;
    [Gui2Editor(write = false)]
    public float invisibleBeforeHeight;
    [Gui2Editor(write = false)]
    public float visibleHeight;
    [Gui2Editor(write = false)]
    public float invisibleAfterHeight;
    [Gui2Editor(write = false)]
    public float nextItemHeight;
    [Gui2Editor(write = false)]
    public float previousItemHeight;

    public Action<float> onScrollPercent
    {
      set
      {
        this.box.onScroll = value;
      }
    }

    [Gui2Editor]
    public bool RenderScrollLines
    {
      get
      {
        return this.leftLine.IsRendered;
      }
      set
      {
        this.leftLine.IsRendered = value;
        this.rightLine.IsRendered = value;
      }
    }

    public Scroller()
    {
      this.Align = Align.UpRight;
      this.box.SizeX = this.upButton.SizeX - 2f * this.SPACE;
      this.box.PositionX = this.SPACE + this.LINE_WIDTH;
      this.box.PositionY = this.upButton.SizeY + this.SPACE;
      this.AddChild((GuiElementBase) this.box);
      this.upButton.Pressed = new AnonymousDelegate(this.ScrollUp);
      this.upButton.PositionX = this.LINE_WIDTH;
      this.AddChild((GuiElementBase) this.upButton);
      this.downButton.Pressed = new AnonymousDelegate(this.ScrollDown);
      this.downButton.PositionX = this.LINE_WIDTH;
      this.AddChild((GuiElementBase) this.downButton, Align.DownLeft);
      this.leftLine.SizeX = this.LINE_WIDTH;
      this.AddChild((GuiElementBase) this.leftLine);
      this.rightLine.SizeX = this.LINE_WIDTH;
      this.AddChild((GuiElementBase) this.rightLine, Align.UpRight);
      this.SizeX = this.upButton.SizeX + 2f * this.LINE_WIDTH;
    }

    public void ScrollUp()
    {
      if ((double) this.invisibleBeforeHeight <= 0.0)
        return;
      this.onScroll(-1);
    }

    public void ScrollDown()
    {
      if ((double) this.invisibleAfterHeight <= 0.0)
        return;
      this.onScroll(1);
    }

    private bool Scroll(float value)
    {
      this.onScroll((double) value <= 0.0 ? -1 : 1);
      return true;
    }

    private void SetScrollEnabled(bool enabled)
    {
      this.upButton.IsRendered = enabled;
      this.downButton.IsRendered = enabled;
      this.box.IsRendered = enabled;
    }

    public override void Reposition()
    {
      this.SizeY = this.Parent.SizeY;
      base.Reposition();
      this.leftLine.SizeY = this.SizeY;
      this.rightLine.SizeY = this.SizeY;
      this.box.SizeY = (float) ((double) this.SizeY - (double) this.downButton.SizeY - (double) this.upButton.SizeY - 2.0 * (double) this.SPACE);
      float num = this.invisibleBeforeHeight + this.visibleHeight + this.invisibleAfterHeight;
      this.box.BoxPosition = this.invisibleBeforeHeight / num;
      this.box.BoxHeight = this.visibleHeight / num;
      this.SetScrollEnabled((double) this.invisibleBeforeHeight != 0.0 || (double) this.invisibleAfterHeight != 0.0);
    }
  }
}
