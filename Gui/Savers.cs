﻿// Decompiled with JetBrains decompiler
// Type: Gui.Savers
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using CouchDB;
using Json;
using System.Collections.Generic;
using System.IO;

namespace Gui
{
  public class Savers
  {
    private static readonly string BASE_DIRECTORY = "Assets/Resources/GUI/";

    public static string SavePanel(GuiPanel panel, string fileName)
    {
      fileName = Savers.BASE_DIRECTORY + fileName;
      JsonData jsonData = Savers.SaveBase(panel);
      StreamWriter streamWriter = new StreamWriter(fileName);
      streamWriter.Write(jsonData.ToJsonString(string.Empty));
      streamWriter.Close();
      return fileName;
    }

    private static JsonData SaveBase(GuiPanel panel)
    {
      return JsonSerializator.Serialize((object) new JWindowDescription() { Elements = Savers.GrabElements(panel) });
    }

    private static List<JElementBase> GrabElements(GuiPanel panel)
    {
      return new List<JElementBase>() { panel.Convert2Json() };
    }
  }
}
