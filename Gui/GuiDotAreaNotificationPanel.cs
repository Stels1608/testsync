﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiDotAreaNotificationPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/MessageBox/dotarea_warning.txt")]
  public class GuiDotAreaNotificationPanel : GuiPanel
  {
    private float interval = 1.5f;
    private readonly SpaceLocationMarker safetyMarker = new SpaceLocationMarker();
    private float activationTime;
    [HideInInspector]
    private GuiImage m_BackgroundImage;
    [HideInInspector]
    private GuiLabel m_Headline;
    [HideInInspector]
    private GuiLabel m_Subtitle;
    [HideInInspector]
    private GuiLabel m_Warning;

    public GuiImage NotificationBackgroundImage
    {
      get
      {
        return this.m_BackgroundImage;
      }
    }

    public GuiLabel Headline
    {
      get
      {
        return this.m_Headline;
      }
    }

    public GuiLabel Subtitle
    {
      get
      {
        return this.m_Subtitle;
      }
    }

    public GuiLabel Warning
    {
      get
      {
        return this.m_Warning;
      }
    }

    public GuiDotAreaNotificationPanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiDotAreaNotificationPanel>())
    {
      this.Initialize();
    }

    public GuiDotAreaNotificationPanel(GuiDotAreaNotificationPanel copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiDotAreaNotificationPanel(JDotAreaNotificationPanel json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiDotAreaNotificationPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JDotAreaNotificationPanel((GuiElementBase) this);
    }

    public void Initialize()
    {
      this.m_BackgroundImage = this.Find<GuiImage>("BackgroundImage");
      this.m_Headline = this.Find<GuiLabel>("Headline");
      this.m_Subtitle = this.Find<GuiLabel>("Subtitle");
      this.m_Warning = this.Find<GuiLabel>("Warning");
      this.safetyMarker.Position = Vector3.zero;
    }

    public void Show()
    {
      this.Position = new Vector2((float) (Screen.width / 2) - this.Size.x / 2f, (float) ((double) Screen.height - (double) this.Size.y - 230.0));
      this.activationTime = Time.time + this.interval;
      this.IsRendered = true;
      FacadeFactory.GetInstance().SendMessage(Message.LocationMarkerStatusChanged, (object) new KeyValuePair<ISpaceEntity, bool>((ISpaceEntity) this.safetyMarker, true));
    }

    public override void Update()
    {
      if ((double) Time.time > (double) this.activationTime)
      {
        this.IsRendered = false;
        FacadeFactory.GetInstance().SendMessage(Message.LocationMarkerStatusChanged, (object) new KeyValuePair<ISpaceEntity, bool>((ISpaceEntity) this.safetyMarker, false));
      }
      base.Update();
    }
  }
}
