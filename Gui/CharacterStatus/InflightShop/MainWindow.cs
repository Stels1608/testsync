﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.MainWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Common;
using UnityEngine;

namespace Gui.CharacterStatus.InflightShop
{
  public class MainWindow : GuiDialog
  {
    private ListShop shop = new ListShop();
    private ListLocker locker = new ListLocker();
    private ListHold hold = new ListHold();
    public ShipStats shipStats = new ShipStats(new Vector2(347f, 439f));
    public Paperdoll paperdoll = new Paperdoll();
    private MoneyPlayer money = new MoneyPlayer();
    private GetCubits getCubits = new GetCubits();

    public MainWindow()
    {
      this.MouseTransparent = true;
      this.AddChild((GuiElementBase) this.shop);
      this.AddChild((GuiElementBase) this.locker);
      this.AddChild((GuiElementBase) this.hold);
      this.AddChild((GuiElementBase) this.shipStats);
      this.AddChild((GuiElementBase) this.paperdoll);
      this.AddChild((GuiElementBase) this.money, Align.DownRight, new Vector2(0.0f, 22f));
      this.AddChild((GuiElementBase) this.getCubits, Align.UpRight, new Vector2(-20f, -119f));
    }

    public override void Reposition()
    {
      base.Reposition();
      this.Size = new Vector2(925f, 445f);
      this.Position = new Vector2(0.0f, 0.0f);
      float num = 375f;
      this.shop.Position = new Vector2(3f, 3f);
      this.shop.Size = new Vector2(num - 3f, 439f);
      this.locker.Position = this.shop.Position;
      this.locker.Size = this.shop.Size;
      this.hold.Position = this.shop.Position;
      this.hold.Size = this.shop.Size;
      this.shipStats.Position = new Vector2(this.shop.PositionX + 25f, this.shop.PositionY);
      this.shipStats.Size = this.shop.Size - new Vector2(25f, 9f);
      this.paperdoll.Position = new Vector2(this.shop.Size.x, this.shop.Position.y);
      this.paperdoll.Size = new Vector2((float) ((double) this.Size.x - (double) this.shop.Position.x - (double) this.shop.Size.x - 3.0), this.Size.y - 6f);
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      this.ForceReposition();
      if (!this.shipStats.IsRendered)
        return;
      this.shipStats.ShowMyStatistics();
    }

    public override bool Contains(float2 point)
    {
      if (!base.Contains(point) && !this.money.Contains(point))
        return this.getCubits.Contains(point);
      return true;
    }

    public void SelectStatus()
    {
      this.shipStats.IsRendered = true;
      this.shop.IsRendered = false;
      this.locker.IsRendered = false;
      this.hold.IsRendered = false;
      this.shipStats.ShowMyStatistics();
    }

    public void SelectResupply()
    {
      this.shipStats.IsRendered = false;
      this.shop.IsRendered = true;
      this.locker.IsRendered = false;
      this.hold.IsRendered = false;
      this.shop.PeriodicUpdate();
    }

    public void SelectHold()
    {
      this.shipStats.IsRendered = false;
      this.shop.IsRendered = false;
      this.locker.IsRendered = false;
      this.hold.IsRendered = true;
      this.hold.PeriodicUpdate();
    }

    public void SelectLocker()
    {
      this.shipStats.IsRendered = false;
      this.hold.IsRendered = false;
      this.shop.IsRendered = false;
      this.locker.IsRendered = true;
      this.locker.PeriodicUpdate();
    }
  }
}
