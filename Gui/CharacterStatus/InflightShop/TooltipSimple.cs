﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.TooltipSimple
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui.CharacterStatus.InflightShop
{
  public class TooltipSimple : GuiTooltipBase
  {
    private readonly GuiImage separator = new GuiImage("GUI/EquipBuyPanel/separator");
    private readonly GuiLabel title = new GuiLabel(Gui.Options.FontBGM_BT, 16, true);
    private readonly GuiLabel subTitle = new GuiLabel(Gui.Options.FontBGM_BT, 10);
    private GuiLabel description = new GuiLabel(Gui.Options.FontBGM_BT, 14, true);
    private GuiLabel upAttributes = new GuiLabel(Gui.Options.FontBGM_BT, 12);
    private GuiLabel downAttributes = new GuiLabel(Gui.Options.FontBGM_BT, 12, true);
    private GuiAtlasImage image = new GuiAtlasImage();

    public TooltipSimple(ShipItem item)
    {
      this.m_backgroundImage.Texture = ResourceLoader.Load<Texture2D>("GUI/Gui2/background");
      this.m_backgroundImage.NineSliceEdge = (GuiElementPadding) new Rect(25f, 25f, 25f, 25f);
      this.Size = new Vector2(371f, 339f);
      this.UpdateData(item);
      this.title.SizeX = this.SizeX - 20f;
      this.subTitle.SizeX = this.SizeX - 20f;
      this.subTitle.Name = "subTitle";
      this.description.SizeX = this.SizeX - 20f;
      this.upAttributes.SizeX = this.SizeX - 20f;
      this.downAttributes.SizeX = (float) ((double) this.SizeX - 20.0 - 75.0);
      this.title.Alignment = TextAnchor.UpperCenter;
      this.description.Alignment = TextAnchor.UpperCenter;
      this.AddChild((GuiElementBase) this.title, Align.UpCenter, new Vector2(0.0f, 20f));
      this.AddChild((GuiElementBase) this.subTitle, Align.UpCenter, new Vector2(0.0f, (float) ((double) this.title.PositionY + (double) this.title.SizeY + 10.0)));
      this.AddChild((GuiElementBase) this.description, Align.UpCenter, new Vector2(0.0f, (float) ((double) this.subTitle.PositionY + (double) this.subTitle.SizeY + 10.0)));
      this.AddChild((GuiElementBase) this.upAttributes, Align.UpCenter, new Vector2(0.0f, (float) ((double) this.description.PositionY + (double) this.description.SizeY + 10.0)));
      this.AddChild((GuiElementBase) this.separator, Align.UpCenter, new Vector2(0.0f, (float) ((double) this.upAttributes.PositionY + (double) this.upAttributes.SizeY + 10.0)));
      this.AddChild((GuiElementBase) this.downAttributes, new Vector2(75f, (float) ((double) this.separator.PositionY + (double) this.separator.SizeY + 10.0)));
      this.AddChild((GuiElementBase) this.image, new Vector2(20f, (float) ((double) this.downAttributes.PositionY + (double) this.downAttributes.SizeY / 2.0 - (double) this.image.SizeY / 2.0)));
      this.downAttributes.AllColor = new Color(0.59f, 0.644f, 0.746f);
      this.subTitle.AllColor = Gui.Options.ColorDarkened;
      this.SizeY = (float) ((double) this.downAttributes.PositionY + (double) this.downAttributes.SizeY + 20.0);
    }

    public void UpdateData(ShipItem item)
    {
      this.title.Text = item.ItemGUICard.Name;
      this.description.Text = item.ItemGUICard.Description;
      this.image.GuiCard = item.ItemGUICard;
      if (item is ShipSystem)
        this.subTitle.Text = "%$bgo.etc.info_window_level% " + (object) (item as ShipSystem).Card.Level;
      else if (item is ItemCountable)
        this.subTitle.Text = "%$bgo.etc.info_window_count% " + (object) (item as ItemCountable).Count;
      this.upAttributes.Text = this.GetTextStats(item);
      this.downAttributes.Text = this.GetTextAbilities(item);
    }

    private string GetTextStats(ShipItem item)
    {
      string str = string.Empty;
      ShipSystem shipSystem = item as ShipSystem;
      if (shipSystem != null && (bool) shipSystem.IsLoaded && shipSystem.Card.StaticBuffs != null)
      {
        using (List<Tuple<string, float>>.Enumerator enumerator = shipSystem.Card.StaticBuffs.ToPrintableList().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Tuple<string, float> current = enumerator.Current;
            if ((double) current.Second < 0.0)
              str = str + "- " + Math.Abs(current.Second).ToString("#0.0") + " " + current.First + "\n";
            else
              str = str + "+ " + Math.Abs(current.Second).ToString("#0.0") + " " + current.First + "\n";
          }
        }
      }
      else
        str = !this.IsItemOfWrongType(item) ? "???\n" : "\n";
      if (str.EndsWith("\n"))
        str = str.Substring(0, str.Length - 1);
      return str;
    }

    private string GetTextAbilities(ShipItem item)
    {
      string str = string.Empty;
      ShipSystem shipSystem = item as ShipSystem;
      if (shipSystem != null && (bool) shipSystem.IsLoaded && shipSystem.Card.AbilityCards != null)
      {
        foreach (ShipAbilityCard abilityCard in shipSystem.Card.AbilityCards)
        {
          if (abilityCard != null && (bool) abilityCard.IsLoaded)
          {
            str = str + abilityCard.GUICard.Name.ToUpper() + " " + (object) abilityCard.Level + "\n";
            if ((double) abilityCard.ItemBuffAdd.Cooldown != 0.0)
              str = str + BsgoLocalization.Get("%$bgo.etc.info_window_reload%") + " " + abilityCard.ItemBuffAdd.Cooldown.ToString("#0.0") + "\n";
            if ((double) abilityCard.ItemBuffAdd.DamageLow != 0.0 || (double) abilityCard.ItemBuffAdd.DamageHigh != 0.0)
              str = str + BsgoLocalization.Get("%$bgo.etc.info_window_damage%") + " " + (object) Mathf.FloorToInt(abilityCard.ItemBuffAdd.DamageLow) + " - " + (object) Mathf.FloorToInt(abilityCard.ItemBuffAdd.DamageHigh) + "\n";
            if ((double) abilityCard.ItemBuffAdd.MinRange != 0.0 || (double) abilityCard.ItemBuffAdd.MaxRange != 0.0)
              str = str + BsgoLocalization.Get("%$bgo.etc.info_window_range%") + " " + (object) Mathf.FloorToInt(abilityCard.ItemBuffAdd.MinRange) + " - " + (object) Mathf.FloorToInt(abilityCard.ItemBuffAdd.MaxRange) + "\n";
          }
          else
            str += "???";
        }
      }
      else
        str = !this.IsItemOfWrongType(item) ? "???\n\n\n" : "\n\n\n";
      if (str.EndsWith("\n"))
        str = str.Substring(0, str.Length - 1);
      return str;
    }

    private bool IsItemOfWrongType(ShipItem item)
    {
      return item is ItemCountable;
    }
  }
}
