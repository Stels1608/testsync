﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.SlotHold
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.CharacterStatus.InflightShop
{
  public class SlotHold : SlotBase
  {
    protected readonly GuiLabel slotNumber = new GuiLabel(Gui.Options.FontBGM_BT, 16);
    private readonly GuiHealthbar healthbar = new GuiHealthbar();
    protected readonly GuiButton augmentButton = new GuiButton("%$bgo.inflight_shop.button_augment%");
    protected readonly GuiButton recycleButton = new GuiButton("%$bgo.inflight_shop.button_drop%");
    private const int dropLevel = 8;
    private ShipItem destroyingItem;

    public SlotHold(int i)
    {
      this.AddChild((GuiElementBase) this.slotNumber, this.image.Position + new Vector2(0.0f, -2f));
      this.slotNumber.AutoSize = false;
      this.slotNumber.Alignment = TextAnchor.MiddleCenter;
      this.slotNumber.Size = this.image.Size;
      this.slotNumber.Text = i.ToString();
      this.healthbar.Size = new Vector2(this.Find<GuiAtlasImage>().Size.x, 7f);
      this.AddChild((GuiElementBase) this.healthbar, new Vector2(13f, 49f));
      this.augmentButton.Pressed = new AnonymousDelegate(this.UseAugment);
      this.augmentButton.SizeX *= 1.3f;
      this.AddChild((GuiElementBase) this.augmentButton, Align.UpRight, new Vector2(-5f, 50f), false);
      this.augmentButton.SetTooltip("%$bgo.inflight_shop.button_augment_tooltip%");
      this.recycleButton.Pressed = (AnonymousDelegate) (() =>
      {
        this.destroyingItem = this.Slot;
        new AskBox(BsgoLocalization.Get("%$bgo.inflight_shop.drop_confirm%", (object) this.Slot.ItemGUICard.Name), (AnonymousDelegate) (() =>
        {
          if ((int) this.destroyingItem.ItemGUICard.Level > 8)
            new AskBox(BsgoLocalization.Get("%$bgo.inflight_shop.drop_confirm_level%", (object) 8), new AnonymousDelegate(this.DropItem)).Show((GuiPanel) this.FindParent<MainWindow>());
          else
            this.DropItem();
        })).Show((GuiPanel) this.FindParent<MainWindow>());
      });
      this.recycleButton.SetTooltip("%$bgo.inflight_shop.button_drop_tooltip%");
      this.recycleButton.SizeX *= 1.3f;
      this.AddChild((GuiElementBase) this.recycleButton, Align.UpRight, new Vector2(-5f, 68f));
    }

    private void DropItem()
    {
      ShipItem slot = this.Slot;
      if (slot == this.destroyingItem)
      {
        if (slot is ItemCountable)
          slot.MoveTo((IContainer) null, (slot as ItemCountable).Count);
        else
          slot.MoveTo((IContainer) null);
      }
      this.destroyingItem = (ShipItem) null;
    }

    private static bool Destructible(ShipItem item)
    {
      if (item is ShipSystem && (item as ShipSystem).Card.Trashable)
        return true;
      if (item is ItemCountable)
        return (item as ItemCountable).Card.Trashable;
      return false;
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      ShipItem slot = this.Slot;
      bool flag = slot != null && slot.ItemGUICard != null;
      this.image.IsRendered = true;
      this.slotNumber.IsRendered = !flag;
      this.recycleButton.IsRendered = flag;
      GuiButton guiButton = this.recycleButton;
      int num = guiButton.IsRendered & SlotHold.Destructible(slot) ? 1 : 0;
      guiButton.IsRendered = num != 0;
      if (!flag)
      {
        this.image.Texture = ResourceLoader.Load<Texture2D>("GUI/Inventory/items_atlas");
        this.image.TextureIndex = this.image.Count - 1U;
      }
      ItemCountable countable = slot as ItemCountable;
      this.augmentButton.IsRendered = countable != null && countable.Card.IsAugment && this.ShowAugmentButton(countable);
      if (this.augmentButton.IsRendered)
      {
        if ((slot as ItemCountable).Card.Action == AugmentActionType.LootItem)
        {
          this.augmentButton.Label.Text = "%$bgo.inflight_shop.button_augment_item%";
          this.augmentButton.SetTooltip("%$bgo.inflight_shop.button_augment_item_tooltip%");
        }
        else
        {
          this.augmentButton.Label.Text = "%$bgo.inflight_shop.button_augment%";
          this.augmentButton.SetTooltip("%$bgo.inflight_shop.button_augment_tooltip%");
        }
      }
      if (slot is ShipSystem)
      {
        ShipSystem shipSystem = slot as ShipSystem;
        this.image.IsRendered = shipSystem.Card.SlotType != ShipSlotType.ship_paint;
        this.healthbar.IsRendered = !shipSystem.Card.Indestructible;
        this.healthbar.Progress = shipSystem.Quality;
      }
      else
        this.healthbar.IsRendered = false;
    }

    private bool ShowAugmentButton(ItemCountable countable)
    {
      return (countable.Card.Action != AugmentActionType.LootItem ? 0 : ((Object) SpaceLevel.GetLevel() != (Object) null ? 1 : 0)) == 0;
    }

    protected void UseAugment()
    {
      ItemCountable augment = this.Slot as ItemCountable;
      if (augment == null)
        return;
      if (augment.Card.Action == AugmentActionType.SkillTime)
      {
        if (Game.Me.SkillBook.CurrentTrainingSkill == null)
          new InfoBox("%$bgo.inflight_shop.cant_use_augment%").Show((GuiPanel) this.FindParent<MainWindow>());
        else
          this.UseAugment(augment);
      }
      else if (augment.Card.Action == AugmentActionType.LootItem)
        GuiDialogPopupManager.ShowDialog((GuiPanel) new GuiUIOMassActivationPopup(augment), true);
      else
        this.UseAugment(augment);
    }

    private void UseAugment(ItemCountable augment)
    {
      new AskBox("%$bgo.inflight_shop.use_augment% " + augment.ItemGUICard.Name + "?", new AnonymousDelegate(augment.Use)).Show((GuiPanel) this.FindParent<MainWindow>());
    }

    protected override Price GetPrice()
    {
      return this.Slot.ShopItemCard.SellPrice;
    }

    public override void OnScroll()
    {
      float2 mousePositionGui = MouseSetup.MousePositionGui;
      this.recycleButton.MouseLeave(mousePositionGui, mousePositionGui);
      this.augmentButton.MouseLeave(mousePositionGui, mousePositionGui);
    }
  }
}
