﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.SlotShop
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.CharacterStatus.InflightShop
{
  public class SlotShop : SlotBase
  {
    private GuiButton buyButton = new GuiButton("%$bgo.inflight_shop.requisition%");

    public SlotShop()
    {
      this.buyButton.Pressed = (AnonymousDelegate) (() =>
      {
        ShipItem slot = this.Slot;
        if (((Game.Me.Hold.Count < 70 ? 1 : 0) | (slot is ShipSystem ? 0 : (Game.Me.Hold.GetByGUID(slot.CardGUID) != null ? 1 : 0))) != 0)
          this.FindParent<Gui.CharacterStatus.InflightShop.InflightShop>().buyDialog.Show(slot, this.FindParent<MainWindow>());
        else
          new InfoBox("%$bgo.inflight_shop.no_free_space%").Show((GuiPanel) this.FindParent<MainWindow>());
      });
      this.buyButton.SizeX *= 1.3f;
      this.buyButton.IsRendered = false;
      this.AddChild((GuiElementBase) this.buyButton, Align.UpRight, new Vector2(-5f, 50f));
    }

    public override void OnSelected()
    {
      base.OnSelected();
      this.buyButton.IsRendered = true;
    }

    public override void OnDeselected()
    {
      this.buyButton.IsRendered = false;
      base.OnDeselected();
    }
  }
}
