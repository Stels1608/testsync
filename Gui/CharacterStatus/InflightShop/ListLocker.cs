﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.ListLocker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Gui.CharacterStatus.InflightShop
{
  public class ListLocker : GuiPanelVerticalScroll
  {
    private GetterDelegate<SlotBase, int> slotFactory;

    public ListLocker()
      : this((GetterDelegate<SlotBase, int>) (n => (SlotBase) new SlotHold(n)))
    {
    }

    public ListLocker(GetterDelegate<SlotBase, int> slotFactory)
    {
      this.slotFactory = slotFactory;
      this.AutoSizeChildren = true;
      this.RenderDelimiters = true;
    }

    public override void PeriodicUpdate()
    {
      ItemList itemList = (ItemList) Game.Me.Locker;
      List<SlotBase> slotBaseList1 = this.ChildrenOfType<SlotBase>();
      while (itemList.Count > this.ChildrenOfType<SlotBase>().Count)
        this.AddChild((GuiElementBase) this.slotFactory(this.ChildrenOfType<SlotBase>().Count));
      while (this.ChildrenOfType<SlotBase>().Count > itemList.Count)
      {
        List<SlotBase> slotBaseList2 = this.ChildrenOfType<SlotBase>();
        this.RemoveChild((GuiElementBase) slotBaseList2[slotBaseList2.Count - 1]);
      }
      for (int index = 0; index < slotBaseList1.Count; ++index)
        slotBaseList1[index].Slot = index >= itemList.Count ? (ShipItem) null : itemList[index];
      base.PeriodicUpdate();
    }

    public override void OnScroll()
    {
      List<SlotBase> slotBaseList = this.ChildrenOfType<SlotBase>();
      for (int index = 0; index < slotBaseList.Count; ++index)
        slotBaseList[index].OnScroll();
    }
  }
}
