﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.ListShop
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace Gui.CharacterStatus.InflightShop
{
  public class ListShop : GuiPanelVerticalScroll
  {
    public ListShop()
    {
      this.AutoSizeChildren = true;
      this.RenderDelimiters = true;
      this.enabled = false;
    }

    public override void OnShow()
    {
      base.OnShow();
      FeedbackProtocol.GetProtocol().ReportUiElementShown(FeedbackProtocol.UiElementId.InflightShop);
    }

    public override void OnHide()
    {
      base.OnHide();
      FeedbackProtocol.GetProtocol().ReportUiElementHidden(FeedbackProtocol.UiElementId.InflightShop);
    }

    public override void PeriodicUpdate()
    {
      ItemList items = this.GetItems();
      while (items.Count > this.Children.Count)
        this.AddChild((GuiElementBase) new SlotShop());
      while (items.Count < this.Children.Count)
        this.RemoveChild(this.Children[this.Children.Count - 1]);
      List<SlotBase> slotBaseList = this.ChildrenOfType<SlotBase>();
      for (int index = 0; index < items.Count; ++index)
      {
        slotBaseList[index].Slot = items[index];
        slotBaseList[index].DisplayItemCount = false;
      }
      base.PeriodicUpdate();
    }

    public ItemList GetItems()
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      return SpaceLevel.GetLevel().Shop.Filter(new Predicate<ShipItem>(new ListShop.\u003CGetItems\u003Ec__AnonStorey79() { shipTier = Game.Me.ActiveShip.Card.Tier }.\u003C\u003Em__7C));
    }
  }
}
