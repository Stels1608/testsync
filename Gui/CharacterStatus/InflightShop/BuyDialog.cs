﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.BuyDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Common;
using UnityEngine;

namespace Gui.CharacterStatus.InflightShop
{
  public class BuyDialog : GuiModalDialog
  {
    private readonly GuiLabel title = new GuiLabel(Gui.Options.FontBGM_BT, 15);
    private readonly GuiAtlasImage itemImage = new GuiAtlasImage();
    private readonly MoneyCostVertical money = new MoneyCostVertical();
    private readonly GuiLabel notEnoughMoney = new GuiLabel(string.Empty, Gui.Options.FontBGM_BT, 14);
    private readonly Counter counter = new Counter();
    private GuiButton buyButton = new GuiButton("%$bgo.inflight_shop.buy%");
    private GuiButton cancelButton = new GuiButton("%$bgo.inflight_shop.buy_cancel%");
    private ShipItem shipItem;

    public BuyDialog()
    {
      this.Align = Align.MiddleCenter;
      this.Size = new Vector2(340f, 300f);
      this.CloseButton.IsRendered = false;
      this.title.WordWrap = true;
      this.title.AutoSize = true;
      this.title.Alignment = TextAnchor.UpperCenter;
      this.title.SizeX = this.SizeX;
      this.AddChild((GuiElementBase) this.title, Align.UpCenter, new Vector2(0.0f, 20f));
      this.AddChild((GuiElementBase) this.itemImage, new Vector2(75f, 90f));
      this.AddChild((GuiElementBase) this.money, new Vector2(130f, 90f));
      this.AddChild((GuiElementBase) this.notEnoughMoney, Align.UpCenter, new Vector2(0.0f, 140f), false);
      this.AddChild((GuiElementBase) this.counter, Align.UpCenter, new Vector2(0.0f, 190f));
      this.counter.OnValueChanged = (System.Action<int>) (value =>
      {
        this.notEnoughMoney.Text = !this.shipItem.ShopItemCard.BuyPrice.UsesMerits ? "%$bgo.inflight_shop.notEnoughMoney%" : "%$bgo.inflight_shop.notEnoughMoneyMerits1%";
        this.notEnoughMoney.IsRendered = this.shipItem != null && !this.shipItem.ShopItemCard.BuyPrice.IsEnoughInHangar(this.shipItem.ShopItemCard, this.counter.Value);
        this.money.Ratio = value;
      });
      this.buyButton.Size = new Vector2(100f, 50f);
      this.AddChild((GuiElementBase) this.buyButton, Align.DownCenter, new Vector2(-70f, -30f));
      this.buyButton.PressedAction = (System.Action<GuiButton>) (b =>
      {
        if (this.shipItem == null)
          return;
        if (this.shipItem.ShopItemCard.BuyPrice.IsEnoughInHangar(this.shipItem.ShopItemCard, this.counter.Value))
        {
          b.PressedSound = new GUISoundHandler(GUISound.Instance.OnBuy);
          this.shipItem.MoveTo((IContainer) Game.Me.Hold, (uint) this.counter.Value);
          this.Close();
        }
        else
        {
          b.PressedSound = new GUISoundHandler(GUISound.Instance.OnIllegalAction);
          FacadeFactory.GetInstance().SendMessage(Message.ShopResourcesMissing, (object) this.shipItem.ShopItemCard.BuyPrice.GetMissingResources(this.counter.Value));
        }
      });
      this.cancelButton.Size = new Vector2(100f, 50f);
      this.AddChild((GuiElementBase) this.cancelButton, Align.DownCenter, new Vector2(70f, -30f));
      this.cancelButton.Pressed = (AnonymousDelegate) (() => this.Close());
      this.cancelButton.PressedSound = new GUISoundHandler(GUISound.Instance.OnWindowClose);
    }

    public void Show(ShipItem item, MainWindow parent)
    {
      if (item == null)
        return;
      parent.ShowModalDialog((GuiModalDialog) this);
      this.shipItem = item;
      this.counter.Value = 1;
      this.title.Text = this.shipItem.ItemGUICard.Name;
      this.itemImage.GuiCard = item.ItemGUICard;
      this.money.shopItemCard = item.ShopItemCard;
      this.money.Price = this.shipItem.ShopItemCard.BuyPrice;
    }

    public override void Reposition()
    {
      base.Reposition();
      if (this.Parent != null)
        this.PositionY = (float) (-(double) this.Parent.SizeY / 6.0);
      this.money.PositionY = (float) ((double) this.itemImage.PositionY + (double) this.itemImage.SizeY / 2.0 - (double) this.money.SizeY / 2.0);
    }

    public override bool ScrollDown(float2 position)
    {
      if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        this.counter.VeryDown();
      else
        this.counter.Down();
      return true;
    }

    public override bool ScrollUp(float2 position)
    {
      if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        this.counter.VeryUp();
      else
        this.counter.Up();
      return true;
    }
  }
}
