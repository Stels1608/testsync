﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.Paperdoll
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Paperdolls.PaperdollLayoutBig;
using System;
using System.Collections;
using UnityEngine;

namespace Gui.CharacterStatus.InflightShop
{
  public class Paperdoll : GuiPanel
  {
    private GuiImage paperdoll = new GuiImage();
    private uint shipId;
    private int slotsNum;

    public Paperdoll()
    {
      this.AddChild((GuiElementBase) this.paperdoll, Align.MiddleCenter);
      this.AddChild((GuiElementBase) new PaperdollHullSlot(), Align.UpCenter, new Vector2(0.0f, 20f));
    }

    public Paperdoll(GuiPanel panel)
      : base(panel)
    {
      this.AddChild((GuiElementBase) this.paperdoll, Align.MiddleCenter);
      this.AddChild((GuiElementBase) new PaperdollHullSlot(), Align.UpCenter, new Vector2(0.0f, 20f));
    }

    public override void PeriodicUpdate()
    {
      if ((int) Game.Me.ActiveShip.ServerID != (int) this.shipId)
      {
        this.shipId = (uint) Game.Me.ActiveShip.ServerID;
        this.paperdoll.Texture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/" + Game.Me.ActiveShip.Card.PaperdollLayoutBig.BlueprintTexture);
        this.UpdatePaperdollSlots();
      }
      else if (Game.Me.ActiveShip.Slots.Count != this.slotsNum)
        this.UpdatePaperdollSlots();
      base.PeriodicUpdate();
    }

    private void UpdatePaperdollSlots()
    {
      this.RemoveAll((IEnumerable) this.ChildrenOfType<PaperdollSlot>());
      foreach (ShipSlot slot in Game.Me.ActiveShip.Slots)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        Paperdoll.\u003CUpdatePaperdollSlots\u003Ec__AnonStorey7A slotsCAnonStorey7A = new Paperdoll.\u003CUpdatePaperdollSlots\u003Ec__AnonStorey7A();
        if (slot.Card == null)
          Debug.Log((object) ("Card null: " + (object) slot));
        // ISSUE: reference to a compiler-generated field
        slotsCAnonStorey7A.slotId = slot.Card.SlotId;
        int upgradeLevel = (int) Game.Me.ActiveShip.Card.Level;
        // ISSUE: reference to a compiler-generated method
        SlotLayout slotLayout = Game.Me.ActiveShip.Card.PaperdollLayoutBig.GetSlotLayouts(upgradeLevel).Find(new Predicate<SlotLayout>(slotsCAnonStorey7A.\u003C\u003Em__7D));
        if (slotLayout == null)
        {
          // ISSUE: reference to a compiler-generated field
          Debug.Log((object) ("No SlotLayout for slotId: " + (object) slotsCAnonStorey7A.slotId + "Ship: " + Game.Me.ActiveShip.Name + "UpgradeLevel: " + (object) upgradeLevel));
        }
        Vector2 v2 = slotLayout.Position.ToV2();
        this.AddChild((GuiElementBase) new PaperdollSlot(slot.ServerID), Align.MiddleCenter, v2);
      }
      this.slotsNum = Game.Me.ActiveShip.Slots.Count;
    }
  }
}
