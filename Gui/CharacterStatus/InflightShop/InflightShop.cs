﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.InflightShop
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Gui.CharacterStatus.InflightShop
{
  public class InflightShop : GuiPanel
  {
    private GuiTabsSimple tabs = new GuiTabsSimple();
    private readonly string status = "%$bgo.inflight_shop.status%";
    private readonly string resupply = "%$bgo.inflight_shop.resupply%";
    private readonly string hold = "%$bgo.inflight_shop.hold%";
    private readonly string locker = "%$bgo.inflight_shop.locker%";
    public BuyDialog buyDialog = new BuyDialog();
    private MainWindow mainWindow;
    private bool inSpace;

    public AnonymousDelegate OnClose
    {
      get
      {
        return this.mainWindow.OnClose;
      }
      set
      {
        this.mainWindow.OnClose = value;
      }
    }

    public InflightShop(bool inSpace)
    {
      this.MouseTransparent = true;
      this.inSpace = inSpace;
      this.mainWindow = new MainWindow();
      this.AddChild((GuiElementBase) this.mainWindow);
      this.tabs.Align = Align.DownLeft;
      this.tabs.PositionX = 24f;
      this.tabs.AddTab(this.status, (AnonymousDelegate) (() =>
      {
        this.mainWindow.SelectStatus();
        this.PeriodicUpdate();
      }));
      if (inSpace)
        this.tabs.AddTab(this.resupply, (AnonymousDelegate) (() =>
        {
          this.mainWindow.SelectResupply();
          this.PeriodicUpdate();
        }));
      this.tabs.AddTab(this.hold, (AnonymousDelegate) (() =>
      {
        this.mainWindow.SelectHold();
        this.PeriodicUpdate();
      }));
      if (!inSpace)
        this.tabs.AddTab(this.locker, (AnonymousDelegate) (() =>
        {
          this.mainWindow.SelectLocker();
          this.PeriodicUpdate();
        }));
      this.tabs.SelectButtonWithText(this.status);
      this.AddChild((GuiElementBase) this.tabs);
      this.tabs.GetButton(this.status).SetTooltip("%$bgo.inflight_shop.status_tooltip%");
      if (this.tabs.GetButton(this.resupply) != null)
        this.tabs.GetButton(this.resupply).SetTooltip("%$bgo.inflight_shop.resupply_tooltip%");
      this.tabs.GetButton(this.hold).SetTooltip("%$bgo.inflight_shop.hold_tooltip%");
      if (this.tabs.GetButton(this.locker) != null)
        this.tabs.GetButton(this.locker).SetTooltip("%$bgo.inflight_shop.locker_tooltip%");
      this.Align = Align.UpCenter;
    }

    public override void Reposition()
    {
      base.Reposition();
      this.SizeX = this.mainWindow.SizeX;
      this.SizeY = this.mainWindow.SizeY + 10f + this.tabs.SizeY;
    }

    public void SelectStatus()
    {
      this.tabs.SelectButtonWithText(this.status);
    }

    public void SelectResupply()
    {
      if (!this.inSpace)
        return;
      this.tabs.SelectButtonWithText(this.resupply);
    }

    public void SelectHold()
    {
      this.tabs.SelectButtonWithText(this.hold);
    }

    public void SelectLocker()
    {
      if (this.inSpace)
        return;
      this.tabs.SelectButtonWithText(this.locker);
    }
  }
}
