﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.SlotBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Common;
using Gui.Core;
using UnityEngine;

namespace Gui.CharacterStatus.InflightShop
{
  public class SlotBase : GuiPanel, ISelectable
  {
    protected readonly GuiAtlasImage image = new GuiAtlasImage();
    protected readonly GuiImage iconImage = new GuiImage();
    private readonly GuiLabel title = new GuiLabel();
    private readonly GuiLabel desc = new GuiLabel(Gui.Options.ColorDarkened, Gui.Options.FontBGM_BT, 10);
    private readonly GuiLabel count = new GuiLabel(Gui.Options.ColorDarkened, Gui.Options.FontBGM_BT, 10);
    private readonly MoneyCost money = new MoneyCost("%$bgo.inflight_shop.cost%");
    private bool displayItemCount = true;
    public const int MAX_DESCRIPTION_LENGTH = 60;
    private ShipItem item;

    public ShipItem Slot
    {
      get
      {
        return this.item;
      }
      set
      {
        this.item = value;
      }
    }

    public bool DisplayItemCount
    {
      get
      {
        return this.displayItemCount;
      }
      set
      {
        this.displayItemCount = value;
      }
    }

    public SlotBase()
    {
      this.title.AutoSize = false;
      this.title.WordWrap = true;
      this.title.Size = new Vector2(290f, 25f);
      this.desc.AutoSize = false;
      this.desc.WordWrap = true;
      this.desc.Size = new Vector2(290f, 25f);
      this.AddChild((GuiElementBase) this.image, new Vector2(12f, 12f));
      this.AddChild((GuiElementBase) this.iconImage, new Vector2(12f, 12f));
      this.AddChild((GuiElementBase) this.title, new Vector2(62f, 7f));
      this.AddChild((GuiElementBase) this.desc, new Vector2(62f, 33f));
      this.AddChild((GuiElementBase) this.count, new Vector2(30f, 55f));
      this.AddChild((GuiElementBase) this.money, new Vector2(30f, 68f));
      this.SizeY = 86f;
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      ShipItem slot = this.Slot;
      this.SetTooltip(this.Slot);
      bool flag = slot != null && slot.ItemGUICard != null;
      this.image.IsRendered = flag;
      this.title.IsRendered = flag;
      this.desc.IsRendered = flag;
      this.count.IsRendered = flag;
      this.money.IsRendered = flag;
      if (flag)
      {
        this.iconImage.IsRendered = false;
        this.image.GuiCard = slot.ItemGUICard;
        this.title.Text = slot.ItemGUICard.Name;
        this.desc.Text = slot.ItemGUICard.ShortDescription;
        if (!string.IsNullOrEmpty(this.desc.Text) && this.desc.TextParsed.Length > 60)
          this.desc.TextParsed = this.desc.TextParsed.Substring(0, 60) + "...";
        this.money.shopItemCard = this.Slot.ShopItemCard;
        this.money.Price = this.GetPrice();
        if (slot is ShipSystem)
        {
          ShipSystemCard shipSystemCard = (slot as ShipSystem).Card;
          if (shipSystemCard != null)
            this.count.Text = "%$bgo.inflight_shop.level% " + (object) shipSystemCard.Level + " %$bgo.inflight_shop.max_level% " + (object) shipSystemCard.MaxLevel;
          if ((slot as ShipSystem).Card.SlotType == ShipSlotType.ship_paint)
          {
            this.image.IsRendered = false;
            this.iconImage.IsRendered = true;
            this.iconImage.Texture = slot.ItemGUICard.GUIIconTexture;
            this.iconImage.Size = new Vector2(40f, 40f);
          }
        }
        if (slot is ItemCountable && this.DisplayItemCount)
          this.count.Text = "%$bgo.inflight_shop.count% " + (object) (slot as ItemCountable).Count;
        else
          this.count.Text = string.Empty;
      }
      else
        this.iconImage.IsRendered = false;
    }

    protected virtual Price GetPrice()
    {
      return this.Slot.ShopItemCard.BuyPrice;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      this.FindParent<ISelectableContainer>().Selected = (ISelectable) this;
      return base.MouseUp(position, key);
    }

    public virtual void OnSelected()
    {
    }

    public virtual void OnDeselected()
    {
    }

    public virtual void OnScroll()
    {
    }
  }
}
