﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.PaperdollHullSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace Gui.CharacterStatus.InflightShop
{
  public class PaperdollHullSlot : GuiPanel
  {
    private readonly GuiLabel text = new GuiLabel("%$bgo.inflight_shop.hull%", Gui.Options.FontBGM_BT, 10);
    private readonly GuiLabel text2 = new GuiLabel(Gui.Options.FontBGM_BT, 10);
    private readonly GuiHealthbar healthbar = new GuiHealthbar();
    private const float X_OFFSET = 53f;
    private const float Y_OFFSET = 47f;

    public PaperdollHullSlot()
    {
      this.healthbar.Size = new Vector2(53f, 7f);
      this.AddChild((GuiElementBase) this.text);
      this.AddChild((GuiElementBase) this.text2);
      this.AddChild((GuiElementBase) this.healthbar, new Vector2(0.0f, 48f));
      this.Size = new Vector2(53f, (float) (47.0 + (double) this.healthbar.Size.y + 1.0));
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      HangarShip activeShip = Game.Me.ActiveShip;
      this.text2.Text = ((int) ((double) activeShip.Quality * 100.0)).ToString() + "%";
      this.healthbar.Progress = activeShip.Quality;
      this.SetTooltip("%$bgo.inflight_shop.durability% " + (object) Math.Round((double) activeShip.Durability) + "/" + (object) Math.Round((double) activeShip.Card.Durability));
    }

    public override void Reposition()
    {
      base.Reposition();
      this.text.PositionCenter = this.PositionCenter + new Vector2(3f, -25f);
      this.text2.PositionCenter = this.PositionCenter + new Vector2(3f, -10f);
    }
  }
}
