﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.PaperdollSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Tooltips;
using System;
using UnityEngine;

namespace Gui.CharacterStatus.InflightShop
{
  public class PaperdollSlot : GuiPanel
  {
    private readonly GuiImage openSlot = new GuiImage("GUI/EquipBuyPanel/slot_open");
    private readonly GuiImage closedSlot = new GuiImage("GUI/EquipBuyPanel/slot_closed");
    private readonly GuiAtlasImage image = new GuiAtlasImage();
    private readonly GuiHealthbar healthbar = new GuiHealthbar();
    private readonly GuiLabel emptySlotLetter = new GuiLabel(Gui.Options.FontBGM_BT, 20);
    private readonly ushort slotId;
    private bool textResized;

    public PaperdollSlot(ushort slotId)
    {
      this.slotId = slotId;
      this.healthbar.Size = new Vector2(this.openSlot.Size.x, 7f);
      this.emptySlotLetter.Alignment = TextAnchor.MiddleCenter;
      this.AddChild((GuiElementBase) this.openSlot);
      this.AddChild((GuiElementBase) this.closedSlot, false);
      this.AddChild((GuiElementBase) this.healthbar, new Vector2(0.0f, this.openSlot.Size.y + 1f));
      this.AddChild((GuiElementBase) this.emptySlotLetter, Align.MiddleCenter, new Vector2(0.0f, (float) (-(double) this.healthbar.Size.y + 1.0)));
      this.AddChild((GuiElementBase) this.image, Align.MiddleCenter, new Vector2(1f, -4f));
      this.Size = new Vector2(this.openSlot.Size.x, (float) ((double) this.openSlot.Size.y + (double) this.healthbar.Size.y + 1.0));
    }

    public ShipSlot GetShipSlot()
    {
      for (int index = 0; index < Game.Me.ActiveShip.Slots.Count; ++index)
      {
        ShipSlot shipSlot = Game.Me.ActiveShip.Slots[index];
        if (shipSlot != null && (int) shipSlot.ServerID == (int) this.slotId)
          return shipSlot;
      }
      this.ToolTip = (GuiAdvancedTooltipBase) null;
      return (ShipSlot) null;
    }

    public static string GetEmptySlotFirstLetter(ShipSlotType slotType)
    {
      switch (slotType)
      {
        case ShipSlotType.computer:
          return "%$bgo.inflight_shop.computer_first_letter%";
        case ShipSlotType.engine:
          return "%$bgo.inflight_shop.engine_first_letter%";
        case ShipSlotType.hull:
          return "%$bgo.inflight_shop.hull_first_letter%";
        case ShipSlotType.weapon:
          return "%$bgo.inflight_shop.weapon_first_letter%";
        case ShipSlotType.ship_paint:
          return "%$bgo.inflight_shop.paint_first_letter%";
        case ShipSlotType.avionics:
          return "%$bgo.inflight_shop.avionics_first_letter%";
        case ShipSlotType.launcher:
          return "%$bgo.inflight_shop.launcher_first_letter%";
        case ShipSlotType.defensive_weapon:
          return "%$bgo.inflight_shop.defensive_weapon_first_letter%";
        case ShipSlotType.gun:
          return "%$bgo.inflight_shop.gun_first_letter%";
        case ShipSlotType.role:
          return "%$bgo.inflight_shop.role_first_letter%";
        case ShipSlotType.special_weapon:
          return "%$bgo.inflight_shop.special_weapon_first_letter%";
        default:
          return "[]";
      }
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      ShipSlot shipSlot = this.GetShipSlot();
      if (shipSlot == null || !(bool) shipSlot.IsLoaded)
        return;
      if (shipSlot.System != null)
      {
        ShipSystem shipSystem = shipSlot.System;
        if (!(bool) shipSystem.IsLoaded)
          return;
        this.SetStoreToolTip((GameItemCard) shipSystem, ShopInventoryContainer.Hold, false);
        this.image.IsRendered = true;
        this.image.GuiCard = shipSlot.System.ItemGUICard;
        this.healthbar.Progress = shipSystem.Quality;
        this.healthbar.ForceReposition();
        this.healthbar.IsRendered = true;
        this.healthbar.SetTooltip("%$bgo.inflight_shop.durability% " + (object) Math.Round((double) shipSystem.Durability) + "/" + (object) Math.Round((double) shipSystem.Card.Durability));
        this.emptySlotLetter.IsRendered = false;
      }
      else
      {
        this.image.IsRendered = false;
        this.healthbar.IsRendered = false;
        this.emptySlotLetter.IsRendered = true;
        this.emptySlotLetter.Text = PaperdollSlot.GetEmptySlotFirstLetter(shipSlot.Card.SystemType);
        if (this.textResized || TextUtility.CalcTextSize(Tools.ParseMessage(this.emptySlotLetter.Text), this.emptySlotLetter.Font, 20).width <= 30)
          return;
        this.emptySlotLetter.FontSize = 16;
        if (TextUtility.CalcTextSize(Tools.ParseMessage(this.emptySlotLetter.Text), this.emptySlotLetter.Font, 16).width > 30)
          this.emptySlotLetter.FontSize = 12;
        this.textResized = true;
      }
    }
  }
}
