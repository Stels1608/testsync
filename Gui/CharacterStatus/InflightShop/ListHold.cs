﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.InflightShop.ListHold
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Gui.CharacterStatus.InflightShop
{
  public class ListHold : GuiPanelVerticalScroll
  {
    public ListHold()
      : this((GetterDelegate<SlotBase, int>) (n => (SlotBase) new SlotHold(n)))
    {
    }

    public ListHold(GetterDelegate<SlotBase, int> slotFactory)
    {
      this.AutoSizeChildren = true;
      this.RenderDelimiters = true;
      for (int index = 0; index < 70; ++index)
        this.AddChild((GuiElementBase) slotFactory(index + 1));
    }

    public override void PeriodicUpdate()
    {
      Hold hold = Game.Me.Hold;
      List<SlotBase> slotBaseList = this.ChildrenOfType<SlotBase>();
      for (int index = 0; index < slotBaseList.Count; ++index)
        slotBaseList[index].Slot = index >= hold.Count ? (ShipItem) null : hold[index];
      base.PeriodicUpdate();
    }

    public override void OnScroll()
    {
      List<SlotBase> slotBaseList = this.ChildrenOfType<SlotBase>();
      for (int index = 0; index < slotBaseList.Count; ++index)
        slotBaseList[index].OnScroll();
    }
  }
}
