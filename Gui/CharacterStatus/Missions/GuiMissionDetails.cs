﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.Missions.GuiMissionDetails
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui.CharacterStatus.Missions
{
  public class GuiMissionDetails : GuiPanel
  {
    private readonly GuiLabel name = new GuiLabel(Gui.Options.FontBGM_BT, 20);
    private readonly GuiLabel level = new GuiLabel(Gui.Options.OverColor, Gui.Options.FontBGM_BT, 15);
    private readonly GuiLabel sector = new GuiLabel(Gui.Options.OverColor, Gui.Options.FontBGM_BT, 15);
    private readonly GuiLabel description = new GuiLabel(Gui.Options.FontVerdana, 12);
    private readonly GuiLabel objectivesTitle = new GuiLabel("%$bgo.missions.objectives%", Gui.Options.FontBGM_BT, 12);
    private readonly GuiLabel objectives = new GuiLabel(Gui.Options.FontVerdana, 12);
    private readonly GuiLabel rewardsTitle = new GuiLabel("%$bgo.missions.rewards%", Gui.Options.FontBGM_BT, 12);
    private readonly GuiLabel reward = new GuiLabel(Gui.Options.FontVerdana, 12);
    private const float PADDING = 10f;

    public GuiMissionDetails()
    {
      this.Size = new Vector2(521f, 398f);
      float num = this.SizeX - 20f;
      this.name.WordWrap = true;
      this.name.Alignment = TextAnchor.UpperCenter;
      this.name.SizeX = num;
      this.AddChild((GuiElementBase) this.name, new Vector2(10f, 10f));
      this.level.WordWrap = true;
      this.level.Alignment = TextAnchor.UpperCenter;
      this.level.SizeX = num;
      this.AddChild((GuiElementBase) this.level, new Vector2(10f, 30f));
      this.sector.WordWrap = true;
      this.sector.Alignment = TextAnchor.UpperCenter;
      this.sector.SizeX = num;
      this.AddChild((GuiElementBase) this.sector, new Vector2(10f, 42f));
      this.description.WordWrap = true;
      this.description.Alignment = TextAnchor.UpperLeft;
      this.description.SizeX = num;
      this.AddChild((GuiElementBase) this.description, new Vector2(10f, 75f));
      this.AddChild((GuiElementBase) this.objectivesTitle, new Vector2(10f, 90f));
      this.objectives.SizeX = num / 2f;
      this.AddChild((GuiElementBase) this.objectives, new Vector2((float) (10.0 + (double) num / 2.0), 90f));
      this.AddChild((GuiElementBase) this.rewardsTitle, new Vector2(10f, 110f));
      this.reward.SizeX = num / 2f;
      this.AddChild((GuiElementBase) this.reward, new Vector2((float) (10.0 + (double) num / 2.0), 110f));
    }

    public override void Reposition()
    {
      base.Reposition();
      this.objectivesTitle.PositionY = (float) ((double) this.description.PositionY + (double) this.description.SizeY + 10.0);
      this.objectives.PositionY = (float) ((double) this.description.PositionY + (double) this.description.SizeY + 10.0);
      this.rewardsTitle.PositionY = (float) ((double) this.objectivesTitle.PositionY + (double) this.objectivesTitle.SizeY + 30.0);
      this.reward.PositionY = (float) ((double) this.objectivesTitle.PositionY + (double) this.objectivesTitle.SizeY + 30.0);
    }

    public void TakeInfoFrom(Mission info)
    {
      if (info == null)
        return;
      this.name.Text = info.Name.ToUpper();
      this.level.Text = "%$bgo.missions.challenge_level% " + info.Level.ToString();
      this.description.Text = info.Description;
      this.sector.Text = "%$bgo.missions.sector% " + info.SectorName;
      string str1 = string.Empty;
      using (List<string>.Enumerator enumerator = info.Objectives.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          str1 = str1 + current + "%br%";
        }
      }
      this.objectives.Text = str1;
      string str2 = string.Empty;
      using (List<string>.Enumerator enumerator = info.Rewards.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          str2 = str2 + current + "%br%";
        }
      }
      this.reward.Text = str2;
      this.ForceReposition();
    }
  }
}
