﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.Missions.GuiMissionList
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Core;
using System.Collections.Generic;

namespace Gui.CharacterStatus.Missions
{
  public class GuiMissionList : GuiPanelVerticalScroll
  {
    private GetterDelegate<MissionList> source;
    private MissionList missionList;

    public GuiMissionList()
    {
      this.AutoSizeChildren = true;
      this.RenderDelimiters = true;
    }

    public void UpdateMissionList(MissionList missionList)
    {
      this.missionList = missionList;
      if (this.source == null)
        return;
      this.DisplayMissions(this.source());
    }

    private void DisplayMissions(MissionList missionList)
    {
      this.EmptyChildren();
      foreach (Mission mission in (IEnumerable<Mission>) missionList)
        this.AddChild((GuiElementBase) new MissionItem(mission.ServerID, this.source));
      if (this.Selected != null || this.Children.Count <= 0)
        return;
      this.Selected = this.Children[0] as ISelectable;
    }

    public void SelectMission(ushort missionId)
    {
      foreach (GuiElementBase child in this.Children)
      {
        MissionItem missionItem = child as MissionItem;
        if ((int) missionItem.MissionId == (int) missionId)
          this.Selected = (ISelectable) missionItem;
      }
    }

    private void Select(GetterDelegate<MissionList> source)
    {
      this.source = source;
      if (this.missionList == null)
        return;
      this.DisplayMissions(source());
      this.ScrollToStart();
    }

    public void SelectAll()
    {
      this.Select((GetterDelegate<MissionList>) (() => this.missionList));
    }

    public void SelectInProgress()
    {
      this.Select((GetterDelegate<MissionList>) (() => this.missionList.FilterByState(Mission.State.InProgress)));
    }

    public void SelectCompleted()
    {
      this.Select((GetterDelegate<MissionList>) (() => this.missionList.FilterByState(Mission.State.Completed)));
    }
  }
}
