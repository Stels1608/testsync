﻿// Decompiled with JetBrains decompiler
// Type: Gui.CharacterStatus.Missions.MissionItem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Core;
using UnityEngine;

namespace Gui.CharacterStatus.Missions
{
  public class MissionItem : GuiPanel, ISelectable
  {
    private readonly GuiLabel title;
    private readonly GuiLabel subTitle;
    private readonly GuiLabel status;
    private readonly GuiImage inProgressBackground;
    private readonly GuiButton completeButton;
    private readonly ushort missionId;
    private readonly GetterDelegate<MissionList> source;

    public ushort MissionId
    {
      get
      {
        return this.missionId;
      }
    }

    public MissionItem(ushort missionId, GetterDelegate<MissionList> source)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      MissionItem.\u003CMissionItem\u003Ec__AnonStorey7B itemCAnonStorey7B = new MissionItem.\u003CMissionItem\u003Ec__AnonStorey7B();
      // ISSUE: reference to a compiler-generated field
      itemCAnonStorey7B.missionId = missionId;
      this.title = new GuiLabel(Gui.Options.FontBGM_BT, 14);
      this.subTitle = new GuiLabel(Gui.Options.ColorDarkened, Gui.Options.FontBGM_BT, 12);
      this.status = new GuiLabel(Gui.Options.FontBGM_BT, 12);
      this.inProgressBackground = new GuiImage();
      // ISSUE: explicit constructor call
      base.\u002Ector();
      // ISSUE: reference to a compiler-generated field
      this.missionId = itemCAnonStorey7B.missionId;
      this.source = source;
      this.AddChild((GuiElementBase) this.inProgressBackground);
      this.AddChild((GuiElementBase) this.title, new Vector2(50f, 5f));
      this.AddChild((GuiElementBase) new GuiLabel("%$bgo.missions.sector%", Gui.Options.ColorDarkened, Gui.Options.FontBGM_BT)
      {
        FontSize = 12
      }, new Vector2(50f, 22f));
      this.AddChild((GuiElementBase) this.subTitle, new Vector2(112f, 22f));
      this.AddChild((GuiElementBase) this.status, Align.MiddleLeft, new Vector2(275f, 6f));
      this.completeButton = new GuiButton("%$bgo.missions.hand_in%");
      this.completeButton.Position = this.status.Position;
      this.completeButton.Size = new Vector2(92f, 18f);
      this.completeButton.IsRendered = false;
      // ISSUE: reference to a compiler-generated method
      this.completeButton.Pressed = new AnonymousDelegate(itemCAnonStorey7B.\u003C\u003Em__8A);
      this.AddChild((GuiElementBase) this.completeButton, Align.MiddleLeft);
      this.SizeY = 43f;
      this.PeriodicUpdate();
    }

    public override void Reposition()
    {
      base.Reposition();
      QueueVertical parent = this.FindParent<QueueVertical>();
      if (parent == null)
        return;
      switch (parent.GetPosition((ISelectable) this))
      {
        case QueueVertical.ItemPosition.First:
          this.inProgressBackground.Texture = Gui.Options.QueueHighlightedTextureUp;
          break;
        case QueueVertical.ItemPosition.Middle:
          this.inProgressBackground.Texture = Gui.Options.QueueHighlightedTextureNormal;
          break;
        case QueueVertical.ItemPosition.Last:
          this.inProgressBackground.Texture = Gui.Options.QueueHighlightedTextureDown;
          break;
      }
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      Mission mission = this.GetItem();
      if (mission == null)
        return;
      this.title.Text = mission.Name;
      this.subTitle.Text = mission.SectorName;
      switch (mission.Status)
      {
        case Mission.State.InProgress:
          this.status.Text = "%$bgo.missions.in_progress%";
          break;
        case Mission.State.Completed:
          this.status.IsRendered = false;
          this.completeButton.IsRendered = true;
          break;
      }
      this.inProgressBackground.IsRendered = mission.Status == Mission.State.InProgress;
    }

    public Mission GetItem()
    {
      return this.source().GetByID(this.missionId);
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      this.FindParent<ISelectableContainer>().Selected = (ISelectable) this;
      return base.MouseUp(position, key);
    }

    public virtual void OnSelected()
    {
    }

    public virtual void OnDeselected()
    {
    }
  }
}
