﻿// Decompiled with JetBrains decompiler
// Type: Gui.BridgeGUIToGui2GUIPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class BridgeGUIToGui2GUIPanel : GuiPanel
  {
    public GUIPanel Panel;

    public override bool IsRendered
    {
      get
      {
        return this.Panel.IsRendered;
      }
      set
      {
        this.Panel.IsRendered = value;
      }
    }

    public BridgeGUIToGui2GUIPanel(GUIPanel panel)
    {
      this.Panel = panel;
      panel.IsRendered = true;
      panel.RecalculateAbsCoords();
      this.Size = new Vector2(panel.Rect.width, panel.Rect.height);
    }

    public override void Draw()
    {
      this.Rect.ToString();
      base.Draw();
      if (!this.Panel.IsRendered)
        return;
      this.Panel.Draw();
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      this.Panel.OnMouseMove(position);
      return base.MouseMove(position, previousPosition);
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      if (!this.Panel.OnMouseDown(position, key))
        return base.MouseDown(position, key);
      return true;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      if (!this.Panel.OnMouseUp(position, key))
        return base.MouseUp(position, key);
      return true;
    }

    public override bool KeyDown(KeyCode key, Action action)
    {
      if (!this.Panel.OnKeyDown(key, Action.None))
        return base.KeyDown(key, action);
      return true;
    }

    public override bool KeyUp(KeyCode key, Action action)
    {
      if (!this.Panel.OnKeyUp(key, Action.None))
        return base.KeyUp(key, action);
      return true;
    }

    public override void Update()
    {
      base.Update();
      if (!this.Panel.IsUpdated)
        return;
      this.Panel.Update();
    }

    public override void Reposition()
    {
      base.Reposition();
      this.Panel.Position = new float2(this.Rect.xMin + this.Panel.Rect.width / 2f, this.Rect.yMin + this.Panel.Rect.height / 2f);
      this.Panel.RecalculateAbsCoords();
      this.Size = new Vector2(this.Panel.Rect.width, this.Panel.Rect.height);
    }

    public override bool Contains(float2 point)
    {
      return this.Panel.Contains(point);
    }

    public override bool OnShowTooltip(float2 position)
    {
      return this.Panel.OnShowTooltip(position);
    }
  }
}
