﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiDialogPopupManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class GuiDialogPopupManager
  {
    private static GuiPanel m_currentDialogShowing = (GuiPanel) null;
    private static readonly List<GuiPanel> m_listOfNextToShow = new List<GuiPanel>();

    public static bool ShowDialog(GuiPanel dialogPopup, bool unique = true)
    {
      if (dialogPopup == null)
      {
        Debug.Log((object) ("Didn't ShowDialog: " + dialogPopup.ToString()));
        return false;
      }
      if (unique)
      {
        if (GuiDialogPopupManager.m_listOfNextToShow != null)
        {
          for (int index = 0; index < GuiDialogPopupManager.m_listOfNextToShow.Count; ++index)
          {
            if (GuiDialogPopupManager.m_listOfNextToShow[index].GetType() == dialogPopup.GetType())
              return false;
          }
        }
        if (GuiDialogPopupManager.m_currentDialogShowing != null && GuiDialogPopupManager.m_currentDialogShowing.GetType() == dialogPopup.GetType())
          return false;
      }
      if (GuiDialogPopupManager.m_currentDialogShowing == null)
      {
        GuiDialogPopupManager.m_currentDialogShowing = dialogPopup;
        Game.RegisterDialog((IGUIPanel) GuiDialogPopupManager.m_currentDialogShowing, true);
        return true;
      }
      GuiDialogPopupManager.m_listOfNextToShow.Add(dialogPopup);
      return true;
    }

    public static bool AddDialogToBeShownNext(GuiPanel dialogPopup)
    {
      if (dialogPopup == null)
        return false;
      if (GuiDialogPopupManager.m_currentDialogShowing == null)
      {
        GuiDialogPopupManager.m_currentDialogShowing = dialogPopup;
        Game.RegisterDialog((IGUIPanel) GuiDialogPopupManager.m_currentDialogShowing, true);
        return true;
      }
      GuiDialogPopupManager.m_listOfNextToShow.Insert(0, dialogPopup);
      return true;
    }

    public static void CloseDialog()
    {
      if (GuiDialogPopupManager.m_currentDialogShowing != null)
      {
        Game.UnregisterDialog((IGUIPanel) GuiDialogPopupManager.m_currentDialogShowing);
        GuiDialogPopupManager.m_currentDialogShowing = (GuiPanel) null;
      }
      if (GuiDialogPopupManager.m_listOfNextToShow != null && GuiDialogPopupManager.m_listOfNextToShow.Count > 0)
      {
        GuiDialogPopupManager.m_currentDialogShowing = GuiDialogPopupManager.m_listOfNextToShow[0];
        GuiDialogPopupManager.m_listOfNextToShow.RemoveAt(0);
        Game.RegisterDialog((IGUIPanel) GuiDialogPopupManager.m_currentDialogShowing, true);
      }
      Game.DelayedActions.Awake();
    }
  }
}
