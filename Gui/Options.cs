﻿// Decompiled with JetBrains decompiler
// Type: Gui.Options
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace Gui
{
  public class Options
  {
    public static Color NormalColor = Color.white;
    public static Color OverColor = new Color(0.6431373f, 0.6627451f, 0.6745098f);

    public static Color BackgroundColor
    {
      get
      {
        if (Game.Me.Faction == Faction.Cylon)
          return new Color(1f, 0.0f, 0.0f, 0.5f);
        return new Color(0.2313726f, 0.282353f, 0.3607843f);
      }
    }

    public static Color PositiveColor
    {
      get
      {
        if (Game.Me.Faction == Faction.Cylon)
          return new Color(0.94f, 0.518f, 0.016f);
        return new Color(0.573f, 0.631f, 0.835f);
      }
    }

    public static Color NegativeColor
    {
      get
      {
        if (Game.Me.Faction == Faction.Cylon)
          return new Color(0.855f, 0.035f, 0.09f);
        return new Color(0.92f, 0.196f, 0.435f);
      }
    }

    public static Color StrikeHudElementsColor
    {
      get
      {
        if (Game.Me.Faction == Faction.Cylon)
          return new Color(0.6078432f, 0.1137255f, 0.05098039f);
        return new Color(0.4078431f, 0.4588235f, 0.5960785f);
      }
    }

    public static Color MouseOverInvertColor
    {
      get
      {
        return ImageUtils.HexStringToColor("3e3e3e");
      }
    }

    public static Color MouseOverColor
    {
      get
      {
        return ImageUtils.HexStringToColor("efefef");
      }
    }

    public static Color ColorDarkened
    {
      get
      {
        return new Color(0.578f, 1543f / (926f * Math.E), 0.609f);
      }
    }

    public static Font FontBGM_BT
    {
      get
      {
        return ResourceLoader.Load<Font>("GUI/Fonts/BGM BT");
      }
    }

    public static Font FontDS_DIGIB
    {
      get
      {
        return ResourceLoader.Load<Font>("GUI/Fonts/DS-DIGIB");
      }
    }

    public static Font FontDS_EF_M
    {
      get
      {
        return ResourceLoader.Load<Font>("GUI/Fonts/DS EF M");
      }
    }

    public static Font FontDS_EF_B
    {
      get
      {
        return ResourceLoader.Load<Font>("GUI/Fonts/DS EF B");
      }
    }

    public static Font FontVerdana
    {
      get
      {
        return ResourceLoader.Load<Font>("GUI/Fonts/VERDANA");
      }
    }

    public static Font FontEurostileT
    {
      get
      {
        return ResourceLoader.Load<Font>("GUI/Fonts/EurostileT_Bold");
      }
    }

    public static Font FontEurostileT_Med
    {
      get
      {
        return ResourceLoader.Load<Font>("GUI/Fonts/EurostileTMed");
      }
    }

    public static Font fontEurostileTRegCon
    {
      get
      {
        return ResourceLoader.Load<Font>("GUI/gui_2013/fonts/EurostileTRegCon");
      }
    }

    public static Font fontEurostileTBlaExt
    {
      get
      {
        return ResourceLoader.Load<Font>("GUI/gui_2013/fonts/EurostileTBlaExt");
      }
    }

    public static Texture2D buttonNormal
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Gui2/button_normal");
      }
    }

    public static Texture2D buttonOver
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Gui2/button_over");
      }
    }

    public static Texture2D buttonPressed
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Gui2/button_pressed");
      }
    }

    public static Texture2D buttonInactive
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Gui2/button_inactive");
      }
    }

    public static Texture2D closeButtonNormal
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Common/Buttons/close_normal");
      }
    }

    public static Texture2D closeButtonOver
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Common/Buttons/close_over");
      }
    }

    public static Texture2D closeButtonPressed
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Common/Buttons/close_pressed");
      }
    }

    public static Texture2D checkboxNormal
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/_NewGui/Buttons/btn_checkbox_idle");
      }
    }

    public static Texture2D checkboxOver
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/_NewGui/Buttons/btn_checkbox_over");
      }
    }

    public static Texture2D checkboxPressed
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/_NewGui/Buttons/btn_checkbox_click");
      }
    }

    public static Texture2D checkboxInactive
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/_NewGui/Buttons/btn_checkbox_inactive");
      }
    }

    public static Texture2D checkboxNormal_x
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/_NewGui/Buttons/btn_checkedbox_idle");
      }
    }

    public static Texture2D checkboxOver_x
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/_NewGui/Buttons/btn_checkedbox_over");
      }
    }

    public static Texture2D checkboxPressed_x
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/_NewGui/Buttons/btn_checkedbox_click");
      }
    }

    public static Texture2D checkboxInactive_x
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/_NewGui/Buttons/btn_checkedbox_inactive");
      }
    }

    public static Texture2D QueueHighlightedTextureUp
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Common/queue_highlighted_up");
      }
    }

    public static Texture2D QueueHighlightedTextureNormal
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Common/queue_highlighted_middle");
      }
    }

    public static Texture2D QueueHighlightedTextureDown
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Common/queue_highlighted_down");
      }
    }

    public static Texture2D QueueSelectTextureUp
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Common/skill_selected_up");
      }
    }

    public static Texture2D QueueSelectTextureNormal
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Common/skill_selected_middle");
      }
    }

    public static Texture2D QueueSelectTextureDown
    {
      get
      {
        return ResourceLoader.Load<Texture2D>("GUI/Common/skill_selected_down");
      }
    }

    public static int QueueSelectTexturePadding
    {
      get
      {
        return 23;
      }
    }

    public static Color ProgressBarColor
    {
      get
      {
        if (Game.Me.Faction == Faction.Cylon)
          return new Color(1f, 0.3607843f, 0.0f);
        return new Color(1f, 0.1960784f, 0.3294118f);
      }
    }

    public static GuiButton CreateCloseButton(Faction faction)
    {
      Texture2D normal = faction != Faction.Colonial ? (Texture2D) Resources.Load("GUI/Common/Buttons/_cylon/close_normal", typeof (Texture2D)) : (Texture2D) Resources.Load("GUI/Common/Buttons/close_normal", typeof (Texture2D));
      Texture2D over = faction != Faction.Colonial ? (Texture2D) Resources.Load("GUI/Common/Buttons/_cylon/close_over", typeof (Texture2D)) : (Texture2D) Resources.Load("GUI/Common/Buttons/close_over", typeof (Texture2D));
      Texture2D pressed = faction != Faction.Colonial ? (Texture2D) Resources.Load("GUI/Common/Buttons/_cylon/close_pressed", typeof (Texture2D)) : (Texture2D) Resources.Load("GUI/Common/Buttons/close_pressed", typeof (Texture2D));
      return new GuiButton(string.Empty, normal, over, pressed);
    }
  }
}
