﻿// Decompiled with JetBrains decompiler
// Type: Gui.PlayerRespawnDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class PlayerRespawnDialog : GuiDialog
  {
    private readonly DeathDesc deathDesc;
    private readonly GuiLabel labelName1;
    private readonly GuiLabel labelDesc1;
    private readonly GuiLabel labelName2;
    private readonly GuiLabel labelDesc2;
    private readonly GuiButton button1;
    private readonly GuiButton button2;
    private readonly GuiLabel labelTimer;
    private int time;

    public PlayerRespawnDialog(DeathDesc deathDesc)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      PlayerRespawnDialog.\u003CPlayerRespawnDialog\u003Ec__AnonStoreyB2 dialogCAnonStoreyB2 = new PlayerRespawnDialog.\u003CPlayerRespawnDialog\u003Ec__AnonStoreyB2();
      // ISSUE: reference to a compiler-generated field
      dialogCAnonStoreyB2.deathDesc = deathDesc;
      this.labelName1 = new GuiLabel(Options.FontBGM_BT, 17);
      this.labelDesc1 = new GuiLabel(Options.FontBGM_BT, 15);
      this.labelName2 = new GuiLabel(Options.FontBGM_BT, 17);
      this.labelDesc2 = new GuiLabel(Options.FontBGM_BT, 15);
      this.button1 = new GuiButton("%$bgo.etc.player_death_button%");
      this.button2 = new GuiButton("%$bgo.etc.player_death_button%");
      this.labelTimer = new GuiLabel(Options.FontBGM_BT, 12);
      this.time = 61;
      // ISSUE: explicit constructor call
      base.\u002Ector();
      // ISSUE: reference to a compiler-generated field
      dialogCAnonStoreyB2.\u003C\u003Ef__this = this;
      this.Align = Align.DownCenter;
      this.PositionY = -250f;
      // ISSUE: reference to a compiler-generated field
      this.deathDesc = dialogCAnonStoreyB2.deathDesc;
      this.Size = new Vector2(400f, 170f);
      this.CloseButton.IsRendered = false;
      this.AddChild((GuiElementBase) new GuiLabel("%$bgo.etc.player_death_title%", Options.FontBGM_BT, 20), Align.UpCenter, new Vector2(0.0f, 10f));
      this.labelName1.Alignment = TextAnchor.UpperLeft;
      this.labelDesc1.Alignment = TextAnchor.UpperLeft;
      this.button1.Font = Options.FontBGM_BT;
      this.button1.FontSize = 15;
      this.button1.SizeY = this.labelName1.SizeY;
      this.AddChild((GuiElementBase) this.labelName1, new Vector2(30f, 45f));
      this.AddChild((GuiElementBase) this.labelDesc1, new Vector2(30f, 47f));
      this.AddChild((GuiElementBase) this.button1, Align.UpRight, new Vector2(-30f, this.labelName1.PositionY));
      // ISSUE: reference to a compiler-generated method
      this.button1.Pressed = new AnonymousDelegate(dialogCAnonStoreyB2.\u003C\u003Em__181);
      // ISSUE: reference to a compiler-generated field
      if (dialogCAnonStoreyB2.deathDesc.RespawnLocations.Count > 1)
      {
        this.labelName2.Alignment = TextAnchor.UpperLeft;
        this.labelDesc2.Alignment = TextAnchor.UpperLeft;
        this.button2.Font = Options.FontBGM_BT;
        this.button2.FontSize = 15;
        this.button2.SizeY = this.button1.SizeY;
        this.AddChild((GuiElementBase) this.labelName2, new Vector2(30f, 95f));
        this.AddChild((GuiElementBase) this.labelDesc2, new Vector2(30f, 97f));
        this.AddChild((GuiElementBase) this.button2, Align.UpRight, new Vector2(-30f, this.labelName2.PositionY));
        // ISSUE: reference to a compiler-generated method
        this.button2.Pressed = new AnonymousDelegate(dialogCAnonStoreyB2.\u003C\u003Em__182);
      }
      this.AddChild((GuiElementBase) this.labelTimer, Align.DownCenter, new Vector2(0.0f, -10f));
      this.AddChild((GuiElementBase) new Timer(1f));
    }

    [Gui2Editor]
    public static void Show(uint id1, uint id2)
    {
      DeathDesc deathDesc = new DeathDesc();
      RespawnLocationInfo respawnLocationInfo1 = new RespawnLocationInfo(id1, 0U);
      RespawnLocationInfo respawnLocationInfo2 = new RespawnLocationInfo(id2, 0U);
      deathDesc.RespawnLocations = new List<RespawnLocationInfo>((IEnumerable<RespawnLocationInfo>) new RespawnLocationInfo[2]
      {
        respawnLocationInfo1,
        respawnLocationInfo2
      });
      PlayerRespawnDialog.Show(deathDesc);
    }

    public static void Show(DeathDesc deathDesc)
    {
      Game.RegisterDialog((IGUIPanel) new PlayerRespawnDialog(deathDesc), true);
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      if (this.deathDesc.RespawnLocations.Count > 0)
      {
        this.labelName1.Text = this.GetName(this.deathDesc.RespawnLocations[0].SectorId);
        this.labelDesc1.Text = this.GetText(this.deathDesc.RespawnLocations[0].SectorId, this.deathDesc.RespawnLocations[0].CarrierPlayerId);
      }
      if (this.deathDesc.RespawnLocations.Count > 1)
      {
        this.labelName2.Text = this.GetName(this.deathDesc.RespawnLocations[1].SectorId);
        this.labelDesc2.Text = this.GetText(this.deathDesc.RespawnLocations[1].SectorId, this.deathDesc.RespawnLocations[1].CarrierPlayerId);
      }
      --this.time;
      if (this.time < 0)
      {
        this.button1.Pressed();
      }
      else
      {
        string str = this.GetName(this.deathDesc.RespawnLocations[0].SectorId);
        if ((int) this.deathDesc.RespawnLocations[0].CarrierPlayerId != 0)
          str = str + " (" + BsgoLocalization.Get("%$bgo.common.role_carrier%") + ")";
        this.labelTimer.Text = BsgoLocalization.Get("%$bgo.etc.player_death_timer%", (object) str, (object) this.time);
      }
    }

    private string GetText(uint sectorId, uint carrierPlayerId)
    {
      if ((GameLevel.Instance as SpaceLevel).IsBattlespace || (GameLevel.Instance as SpaceLevel).IsTournament)
        return string.Empty;
      if (!StaticCards.GalaxyMap.Stars.ContainsKey(sectorId))
        return BsgoLocalization.Get("%$bgo.etc.player_death_label%", (object) string.Empty, (object) "0.0");
      MapStarDesc mapStarDesc1 = StaticCards.GalaxyMap.Stars[sectorId];
      MapStarDesc mapStarDesc2 = StaticCards.GalaxyMap.Stars[(GameLevel.Instance as SpaceLevel).ServerID];
      if (mapStarDesc1 == null || mapStarDesc2 == null)
        return string.Empty;
      float num = (mapStarDesc2.Position - mapStarDesc1.Position).magnitude / 20f;
      string str = BsgoLocalization.Get("%$bgo.etc.player_death_label%", (object) string.Empty, (object) num.ToString("0.0"));
      if ((int) carrierPlayerId != 0)
        str = str + " (" + Game.Players.GetPlayer(carrierPlayerId).Name + ")";
      return str;
    }

    private string GetName(uint sectorId)
    {
      if (!StaticCards.GalaxyMap.Stars.ContainsKey(sectorId))
      {
        SpaceLevel spaceLevel = GameLevel.Instance as SpaceLevel;
        if ((int) spaceLevel.ServerID == (int) sectorId)
          return spaceLevel.Card.GUICard.Name;
        return string.Empty;
      }
      SectorCard sectorCard = StaticCards.GalaxyMap.Stars[sectorId].SectorCard;
      if (sectorCard.GUICard == null || !(bool) sectorCard.GUICard.IsLoaded)
        return string.Empty;
      return sectorCard.GUICard.Name;
    }
  }
}
