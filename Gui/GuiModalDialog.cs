﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiModalDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiModalDialog : GuiDialog
  {
    public GuiModalDialog()
    {
      this.OnClose = new AnonymousDelegate(this.Close);
    }

    protected virtual void Close()
    {
      GuiPanel guiPanel = this.Parent as GuiPanel;
      if (guiPanel == null)
        return;
      guiPanel.CloseModalDialog(this);
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      base.MouseMove(position, previousPosition);
      return true;
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      base.MouseDown(position, key);
      return true;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      base.MouseUp(position, key);
      return true;
    }

    public override bool ScrollDown(float2 position)
    {
      base.ScrollDown(position);
      return true;
    }

    public override bool ScrollUp(float2 position)
    {
      base.ScrollUp(position);
      return true;
    }

    public override bool Contains(float2 point)
    {
      GuiPanel guiPanel = this.Parent as GuiPanel;
      if (guiPanel == null)
        return base.Contains(point);
      if (guiPanel.MouseTransparent)
        return guiPanel.Rect.Contains(point.ToV2());
      return guiPanel.Contains(point);
    }
  }
}
