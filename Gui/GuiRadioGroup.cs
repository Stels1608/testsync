﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiRadioGroup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class GuiRadioGroup : QueueHorizontal
  {
    private readonly Dictionary<GuiRadioGroup.RadioElement, AnonymousDelegate> dic = new Dictionary<GuiRadioGroup.RadioElement, AnonymousDelegate>();
    private GuiRadioGroup.RadioElement selected;

    public GuiRadioGroup()
    {
      this.ContainerAutoSize = true;
    }

    public GuiRadioGroup.RadioElement AddTab(string name, AnonymousDelegate action)
    {
      GuiRadioGroup.RadioElement key = new GuiRadioGroup.RadioElement(name, new System.Action<GuiRadioGroup.RadioElement>(this.SelectButton));
      this.AddChild((GuiElementBase) key);
      this.dic.Add(key, action);
      return key;
    }

    public void SelectIndex(int index)
    {
      this.SelectButton(this.Children[index] as GuiRadioGroup.RadioElement);
    }

    public void SelectButtonWithText(string name)
    {
      foreach (GuiElementBase child in this.Children)
      {
        if ((child as GuiRadioGroup.RadioElement).Text == name)
        {
          this.SelectButton(child as GuiRadioGroup.RadioElement);
          break;
        }
      }
    }

    public void Select(GuiElementBase el)
    {
      if (!(el is GuiRadioGroup.RadioElement))
        throw new Exception("Wrong element in tabs requested.");
      this.SelectButton(el as GuiRadioGroup.RadioElement);
    }

    public void SelectNext()
    {
      List<GuiElementBase> guiElementBaseList = new List<GuiElementBase>((IEnumerable<GuiElementBase>) this.Children);
      int index = guiElementBaseList.IndexOf((GuiElementBase) this.selected) + 1;
      this.SelectButton((index < guiElementBaseList.Count ? guiElementBaseList[index] : guiElementBaseList[guiElementBaseList.Count - 1]) as GuiRadioGroup.RadioElement);
    }

    public void SelectPrevious()
    {
      List<GuiElementBase> guiElementBaseList = new List<GuiElementBase>((IEnumerable<GuiElementBase>) this.Children);
      int index = guiElementBaseList.IndexOf((GuiElementBase) this.selected) - 1;
      this.SelectButton((index >= 0 ? guiElementBaseList[index] : guiElementBaseList[0]) as GuiRadioGroup.RadioElement);
    }

    public void SelectButton(GuiRadioGroup.RadioElement button)
    {
      if (this.selected == button)
        return;
      if (this.selected != null)
        this.selected.IsPressed = false;
      this.selected = button;
      this.selected.IsPressed = true;
      AnonymousDelegate anonymousDelegate = this.dic[this.selected];
      if (anonymousDelegate == null)
        return;
      anonymousDelegate();
    }

    public GuiRadioGroup.RadioElement GetSelected()
    {
      return this.selected;
    }

    public int GetSelectedIndex()
    {
      if (this.selected == null)
        return -1;
      return this.Children.IndexOf((GuiElementBase) this.selected);
    }

    public string GetSelectedText()
    {
      if (this.selected == null)
        return (string) null;
      return this.selected.Text;
    }

    public class RadioElement : QueueHorizontal
    {
      private readonly Texture2D normal = ResourceLoader.Load<Texture2D>("GUI/Common/Buttons/visualtoggle_normal");
      private readonly Texture2D over = ResourceLoader.Load<Texture2D>("GUI/Common/Buttons/visualtoggle_over");
      private readonly Texture2D pressed = ResourceLoader.Load<Texture2D>("GUI/Common/Buttons/visualtoggle_pressed");
      private readonly GuiImage image = new GuiImage();
      private readonly GuiLabel text = new GuiLabel(Options.FontBGM_BT, 16);
      private System.Action<GuiRadioGroup.RadioElement> Action;
      private bool isPressed;

      public string Text
      {
        get
        {
          return this.text.Text;
        }
        set
        {
          this.text.Text = value;
          this.ForceReposition();
        }
      }

      public bool IsPressed
      {
        get
        {
          return this.isPressed;
        }
        set
        {
          this.isPressed = value;
          this.UpdateSelection();
        }
      }

      public RadioElement(string title, System.Action<GuiRadioGroup.RadioElement> action)
      {
        this.Action = action;
        this.image.Texture = this.normal;
        this.AddChild((GuiElementBase) this.image);
        this.text.Text = title;
        this.AddChild((GuiElementBase) this.text, Align.MiddleLeft, new Vector2(this.image.SizeX + 10f, 0.0f));
        this.IsPressed = false;
      }

      public override void Reposition()
      {
        base.Reposition();
        this.Size = new Vector2((float) ((double) this.text.PositionNormalX + (double) this.text.SizeX + 10.0), this.image.SizeY);
      }

      public override void MouseEnter(float2 position, float2 previousPosition)
      {
        base.MouseEnter(position, previousPosition);
        if (this.IsPressed)
          return;
        this.image.Texture = this.over;
        this.text.NormalColor = Options.NormalColor;
      }

      public override void MouseLeave(float2 position, float2 previousPosition)
      {
        base.MouseLeave(position, previousPosition);
        this.UpdateSelection();
      }

      public override bool MouseUp(float2 position, KeyCode key)
      {
        if (this.Action != null)
          this.Action(this);
        return base.MouseUp(position, key);
      }

      private void UpdateSelection()
      {
        this.image.Texture = !this.isPressed ? this.normal : this.pressed;
        this.text.NormalColor = !this.isPressed ? Options.OverColor : Options.NormalColor;
      }
    }
  }
}
