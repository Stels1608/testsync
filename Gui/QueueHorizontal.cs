﻿// Decompiled with JetBrains decompiler
// Type: Gui.QueueHorizontal
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class QueueHorizontal : GuiPanel
  {
    protected float space;
    protected bool containerAutoSize;
    protected bool childrenAutoSize;

    [Gui2Editor]
    public float Space
    {
      get
      {
        return this.space;
      }
      set
      {
        this.space = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public bool ContainerAutoSize
    {
      get
      {
        return this.containerAutoSize;
      }
      set
      {
        this.containerAutoSize = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public bool AutoSizeChildren
    {
      get
      {
        return this.childrenAutoSize;
      }
      set
      {
        this.childrenAutoSize = value;
        this.RepositionElements();
      }
    }

    public QueueHorizontal()
    {
      this.RequireBackRepositioning = true;
    }

    public override void Reposition()
    {
      this.RepositionElements();
      base.Reposition();
    }

    protected virtual void RepositionElements()
    {
      float x = 0.0f;
      float y = 0.0f;
      using (List<GuiElementBase>.Enumerator enumerator = this.ChildrenVisible.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GuiElementBase current = enumerator.Current;
          current.PositionX = x;
          x += current.Size.x;
          if ((double) current.SizeY > (double) y)
            y = current.SizeY;
          x += this.space;
        }
      }
      if (this.childrenAutoSize)
      {
        using (List<GuiElementBase>.Enumerator enumerator = this.ChildrenVisible.GetEnumerator())
        {
          while (enumerator.MoveNext())
            enumerator.Current.SizeY = y;
        }
      }
      if (!this.containerAutoSize)
        return;
      if ((double) x > 0.0)
        x -= this.space;
      this.Size = new Vector2(x, y);
    }

    public override void AddChild(GuiElementBase ch)
    {
      base.AddChild(ch);
      this.ForceReposition();
    }

    public override void RemoveChild(GuiElementBase ch)
    {
      base.RemoveChild(ch);
      this.ForceReposition();
    }
  }
}
