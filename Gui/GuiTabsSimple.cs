﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiTabsSimple
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  public class GuiTabsSimple : QueueHorizontal
  {
    protected Dictionary<GuiButton, AnonymousDelegate> dic = new Dictionary<GuiButton, AnonymousDelegate>();
    protected Texture2D lefttab_normal = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/lefttab_normal");
    protected Texture2D lefttab_over = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/lefttab_over");
    protected Texture2D lefttab_pressed = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/lefttab_pressed");
    protected Texture2D middletab_normal = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/middletab_normal");
    protected Texture2D middletab_over = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/middletab_over");
    protected Texture2D middletab_pressed = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/middletab_pressed");
    protected Texture2D righttab_normal = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/righttab_normal");
    protected Texture2D righttab_over = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/righttab_over");
    protected Texture2D righttab_pressed = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/righttab_pressed");
    protected GuiButton selected;

    public GuiTabsSimple()
    {
      this.ContainerAutoSize = true;
    }

    public GuiButton AddTab(string name, AnonymousDelegate action)
    {
      GuiButton key = new GuiButton(name);
      key.PressedAction = new System.Action<GuiButton>(this.SelectButton);
      this.AddChild((GuiElementBase) key);
      this.dic.Add(key, action);
      this.UpdateTabs();
      return key;
    }

    public void SelectButtonWithText(string name)
    {
      foreach (GuiElementBase child in this.Children)
      {
        if ((child as GuiButton).Text == name)
        {
          this.SelectButton(child as GuiButton);
          break;
        }
      }
    }

    public void Select(GuiElementBase el)
    {
      if (!(el is GuiButton))
        throw new Exception("Wrong element in tabs requested.");
      this.SelectButton(el as GuiButton);
    }

    public void SelectNext()
    {
      List<GuiElementBase> guiElementBaseList = new List<GuiElementBase>((IEnumerable<GuiElementBase>) this.Children);
      int index = guiElementBaseList.IndexOf((GuiElementBase) this.selected) + 1;
      this.SelectButton((index < guiElementBaseList.Count ? guiElementBaseList[index] : guiElementBaseList[guiElementBaseList.Count - 1]) as GuiButton);
    }

    public void SelectPrevious()
    {
      List<GuiElementBase> guiElementBaseList = new List<GuiElementBase>((IEnumerable<GuiElementBase>) this.Children);
      int index = guiElementBaseList.IndexOf((GuiElementBase) this.selected) - 1;
      this.SelectButton((index >= 0 ? guiElementBaseList[index] : guiElementBaseList[0]) as GuiButton);
    }

    public void SelectButton(GuiButton button)
    {
      if (this.selected == button)
        return;
      if (this.selected != null)
        this.selected.IsPressedManual = false;
      this.selected = button;
      this.selected.IsPressedManual = true;
      AnonymousDelegate anonymousDelegate = this.dic[this.selected];
      if (anonymousDelegate == null)
        return;
      anonymousDelegate();
    }

    public void SelectIndex(int index)
    {
      this.SelectButton(this.Children[index] as GuiButton);
    }

    public GuiButton GetSelected()
    {
      return this.selected;
    }

    public string GetSelectedText()
    {
      if (this.selected == null)
        return (string) null;
      return this.selected.Text;
    }

    public GuiButton GetButton(string text)
    {
      foreach (GuiElementBase child in this.Children)
      {
        if ((child as GuiButton).Text == text)
          return child as GuiButton;
      }
      return (GuiButton) null;
    }

    protected void UpdateTabs()
    {
      List<GuiElementBase> guiElementBaseList = new List<GuiElementBase>((IEnumerable<GuiElementBase>) this.Children);
      GuiButton guiButton1 = (GuiButton) null;
      GuiButton guiButton2 = (GuiButton) null;
      using (List<GuiElementBase>.Enumerator enumerator = guiElementBaseList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GuiElementBase current = enumerator.Current;
          GuiButton guiButton3 = current as GuiButton;
          if (current.IsRendered)
          {
            if (guiButton1 == null)
              guiButton1 = guiButton3;
            guiButton2 = guiButton3;
            guiButton3.NormalTexture = this.middletab_normal;
            guiButton3.OverTexture = this.middletab_over;
            guiButton3.PressedTexture = this.middletab_pressed;
            guiButton3.Size = new Vector2((float) this.middletab_normal.width, (float) this.middletab_normal.height);
          }
        }
      }
      if (guiButton1 != null)
      {
        guiButton1.NormalTexture = this.lefttab_normal;
        guiButton1.OverTexture = this.lefttab_over;
        guiButton1.PressedTexture = this.lefttab_pressed;
      }
      if (guiButton2 == null)
        return;
      guiButton2.NormalTexture = this.righttab_normal;
      guiButton2.OverTexture = this.righttab_over;
      guiButton2.PressedTexture = this.righttab_pressed;
    }
  }
}
