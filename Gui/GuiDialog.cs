﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/_NewGui/layout/dialogPanel.txt")]
  public class GuiDialog : GuiPanel
  {
    [HideInInspector]
    private GuiButton m_closeButton;

    public override bool IsBigWindow
    {
      get
      {
        return true;
      }
    }

    public GuiButton CloseButton
    {
      get
      {
        return this.m_closeButton;
      }
    }

    public AnonymousDelegate OnClose
    {
      get
      {
        return this.m_closeButton.Pressed;
      }
      set
      {
        this.m_closeButton.Pressed = value;
      }
    }

    public GuiDialog(string layoutPath)
      : base(layoutPath)
    {
      this.Initialize();
    }

    public GuiDialog()
      : base(UseInGuiEditor.GetDefaultLayout<GuiDialog>())
    {
      this.Initialize();
    }

    public GuiDialog(GuiDialog copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiDialog(JDialog json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiDialog(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JDialog((GuiElementBase) this);
    }

    public virtual void Initialize()
    {
      this.m_closeButton = this.Find<GuiButton>("closeButton");
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      if (!base.MouseDown(position, key))
        return this.Contains(position);
      return true;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      if (!base.MouseUp(position, key))
        return this.Contains(position);
      return true;
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      if (!base.MouseMove(position, previousPosition))
        return this.Contains(position);
      return true;
    }
  }
}
