﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiSaleBanner
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/_NewGui/layout/GameItemLayouts/saleBannerLayout")]
  public class GuiSaleBanner : GuiPanel
  {
    [HideInInspector]
    private GuiLabel m_percentLabel;

    public GuiSaleBanner()
      : base(UseInGuiEditor.GetDefaultLayout<GuiSaleBanner>())
    {
      this.Initialize();
    }

    public GuiSaleBanner(GuiSaleBanner copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiSaleBanner(JSaleBanner json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiSaleBanner(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JSaleBanner((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
    }

    private void Initialize()
    {
    }

    public override void Update()
    {
      base.Update();
    }
  }
}
