﻿// Decompiled with JetBrains decompiler
// Type: Gui.InfoBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class InfoBox : GuiModalDialog
  {
    private readonly GuiLabel textName = new GuiLabel(Options.FontBGM_BT, 20);
    private readonly GuiButton okButton = new GuiButton("%$bgo.common.ok%");
    public AnonymousDelegate OnOk;

    public string Text
    {
      get
      {
        return this.textName.Text;
      }
      set
      {
        this.textName.PositionY = 40f;
        this.textName.Text = value;
        this.textName.WordWrap = false;
        GuiLabel guiLabel;
        double num;
        for (; (double) this.textName.SizeX > (double) this.textName.SizeY * 3.0 && (double) this.textName.SizeX > 400.0; guiLabel.SizeX = (float) num)
        {
          this.textName.WordWrap = true;
          guiLabel = this.textName;
          num = (double) guiLabel.SizeX * 0.800000011920929;
        }
        this.SizeX = this.textName.SizeX + 30f;
        this.SizeY = this.textName.SizeY + 120f;
        this.okButton.Size = new Vector2(150f, 40f);
        this.okButton.Position = new Vector2(0.0f, -30f);
        this.ForceReposition();
      }
    }

    public InfoBox(string message)
      : this(message, (AnonymousDelegate) null)
    {
    }

    public InfoBox(string message, AnonymousDelegate onOk)
    {
      this.CloseButton.IsRendered = false;
      this.Align = Align.MiddleCenter;
      this.OnOk = onOk;
      this.AddChild((GuiElementBase) this.textName, Align.UpCenter);
      this.textName.Alignment = TextAnchor.UpperCenter;
      this.okButton.Font = Options.FontBGM_BT;
      this.okButton.FontSize = 16;
      this.okButton.Pressed = new AnonymousDelegate(this.Ok);
      this.AddChild((GuiElementBase) this.okButton, Align.DownCenter);
      this.Text = message;
    }

    private void Ok()
    {
      if (this.OnOk != null)
        this.OnOk();
      this.Close();
    }

    public override void Reposition()
    {
      base.Reposition();
      if (this.Parent == null)
        return;
      this.PositionY = (float) (-(double) this.Parent.SizeY / 6.0);
    }

    public void Show()
    {
      this.Show((GuiPanel) null);
    }

    public void Show(GuiPanel parent)
    {
      if (parent == null)
        Game.RegisterDialog((IGUIPanel) this, true);
      else
        parent.ShowModalDialog((GuiModalDialog) this);
    }

    protected override void Close()
    {
      if (this.Parent != null)
        (this.Parent as GuiPanel).CloseModalDialog((GuiModalDialog) this);
      else
        Game.UnregisterDialog((IGUIPanel) this);
    }
  }
}
