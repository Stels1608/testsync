﻿// Decompiled with JetBrains decompiler
// Type: Gui.BridgeGUIToGui2CustomGUIElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class BridgeGUIToGui2CustomGUIElement : GuiElementBase
  {
    private CustomGUIElement el;

    public BridgeGUIToGui2CustomGUIElement(CustomGUIElement el)
    {
      this.el = el;
      this.Size = new Vector2(el.GetRect().Width, el.GetRect().Height);
    }

    public T Element<T>() where T : CustomGUIElement
    {
      return this.el as T;
    }

    public override void Draw()
    {
      this.Rect.ToString();
      if (this.el.IsRendered)
        this.el.Draw();
      base.Draw();
    }

    public override void MouseEnter(float2 position, float2 previousPosition)
    {
      this.el.SetIsMouseOver(true);
      base.MouseEnter(position, previousPosition);
    }

    public override void MouseLeave(float2 position, float2 previousPosition)
    {
      this.el.SetIsMouseOver(false);
      base.MouseLeave(position, previousPosition);
    }

    public override void Reposition()
    {
      base.Reposition();
      this.el.SetPosition(new float2(this.Rect.xMin + (float) (this.el.GetSize().width / 2), this.Rect.yMin + (float) (this.el.GetSize().height / 2)));
      this.el.RecalculateAbsCoords();
      this.Size = new Vector2(this.el.GetRect().Width, this.el.GetRect().Height);
    }
  }
}
