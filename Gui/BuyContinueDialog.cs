﻿// Decompiled with JetBrains decompiler
// Type: Gui.BuyContinueDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  internal class BuyContinueDialog : GuiDialog
  {
    private const float TIME_TO_CLOSE = 1000f;

    public BuyContinueDialog(uint cubitPrice)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      BuyContinueDialog.\u003CBuyContinueDialog\u003Ec__AnonStorey9A dialogCAnonStorey9A = new BuyContinueDialog.\u003CBuyContinueDialog\u003Ec__AnonStorey9A();
      // ISSUE: reference to a compiler-generated field
      dialogCAnonStorey9A.cubitPrice = cubitPrice;
      // ISSUE: explicit constructor call
      base.\u002Ector();
      // ISSUE: reference to a compiler-generated field
      dialogCAnonStorey9A.\u003C\u003Ef__this = this;
      Gui2Loader.Load((GuiElementBase) this, "GUI/BuyContinueDialog_layout.txt");
      // ISSUE: reference to a compiler-generated field
      this.Find<GuiLabel>("text").Text = BsgoLocalization.Get("%$bgo.buycontinue.do_you_want%", (object) dialogCAnonStorey9A.cubitPrice);
      // ISSUE: reference to a compiler-generated method
      BridgeGUIToGui2GUIPanel guiToGui2GuiPanel = new BridgeGUIToGui2GUIPanel((GUIPanel) new GUIDoubleCountdownNew() { TimeToTick = 1000f, Handler = new AnonymousDelegate(dialogCAnonStorey9A.\u003C\u003Em__11B) });
      guiToGui2GuiPanel.Align = Align.MiddleCenter;
      this.AddChild((GuiElementBase) guiToGui2GuiPanel, new Vector2(0.0f, -20f));
      // ISSUE: reference to a compiler-generated method
      this.Find<GuiButton>("accept").Pressed = new AnonymousDelegate(dialogCAnonStorey9A.\u003C\u003Em__11C);
      // ISSUE: reference to a compiler-generated method
      this.Find<GuiButton>("decline").Pressed = new AnonymousDelegate(dialogCAnonStorey9A.\u003C\u003Em__11D);
      // ISSUE: reference to a compiler-generated method
      this.OnClose = new AnonymousDelegate(dialogCAnonStorey9A.\u003C\u003Em__11E);
      this.StartTick();
    }

    [Gui2Editor]
    public void StartTick()
    {
      (this.Find<BridgeGUIToGui2GUIPanel>().Panel as GUIDoubleCountdownNew).Start();
    }

    [Gui2Editor]
    public static void ShowInstance()
    {
      Game.RegisterDialog((IGUIPanel) new BuyContinueDialog(uint.MaxValue), true);
    }
  }
}
