﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiBlinker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiBlinker : GuiCountdown
  {
    private int blinksLeftToPlaySound;

    public override bool IsRendered
    {
      set
      {
        if (value)
        {
          this.Progress = 0.0f;
          GUISound.Instance.OnBlinkerBlinks();
        }
        base.IsRendered = value;
      }
    }

    public GuiBlinker()
      : base(ResourceLoader.Load<Texture2D>("GUI/Common/flash"), 4U, 4U)
    {
      this.IsRendered = false;
      this.HandleMouseInput = false;
      GuiBlinker guiBlinker = this;
      AnonymousDelegate anonymousDelegate = guiBlinker.WhenElementChanges + (AnonymousDelegate) (() => this.m_srInner.Size = this.Size);
      guiBlinker.WhenElementChanges = anonymousDelegate;
    }

    public override void Update()
    {
      float num = this.Progress + Time.deltaTime;
      if ((double) num > 1.0)
      {
        num = 0.0f;
        --this.blinksLeftToPlaySound;
        if (this.blinksLeftToPlaySound <= 0)
          this.blinksLeftToPlaySound = 3;
      }
      this.Progress = num;
    }
  }
}
