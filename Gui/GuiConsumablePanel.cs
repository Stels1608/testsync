﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiConsumablePanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Gui
{
  public class GuiConsumablePanel : GuiPanel
  {
    private List<GuiMenuItem> m_menuItemList = new List<GuiMenuItem>();
    private const string mc_layout = "GUI/_NewGui/layout/SwitchAmmo/switch_ammo";
    private const float OFFSET_TO_SCREEN_BORDER = 10f;
    private ShipAbility m_ability;
    private AtlasCache m_abilityIcon;
    private GuiScrollPanel m_scrollPanel;
    private GuiTreePanel m_treePanel;
    private GuiMenuItem m_defaultMenuItem;

    public ShipAbility Ability
    {
      get
      {
        return this.m_ability;
      }
      set
      {
        if (this.m_ability != null && value != null && this.m_ability.card == value.card)
        {
          this.IsRendered = false;
          this.m_ability = (ShipAbility) null;
        }
        else
        {
          this.m_ability = value;
          if (this.m_ability == null)
            return;
          this.m_menuItemList.Clear();
          this.m_menuItemList = this.CreateMenuItemList();
          this.m_treePanel.ClearSubPanels();
          Dictionary<string, GuiTreePanel> dictionary = new Dictionary<string, GuiTreePanel>();
          GuiTreePanel guiTreePanel;
          using (List<GuiMenuItem>.Enumerator enumerator = this.m_menuItemList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              GuiMenuItem current = enumerator.Current;
              ConsumableAttribute[] consumableAttributeArray = current.Consumable.Card.sortingAttributes;
              this.m_treePanel.ShowHeaderInfo = false;
              for (int index = 0; index < consumableAttributeArray.Length; ++index)
              {
                if (!dictionary.TryGetValue((string) consumableAttributeArray[index], out guiTreePanel))
                {
                  guiTreePanel = new GuiTreePanel(this.m_treePanel);
                  guiTreePanel.HeaderLabel.Text = (string) consumableAttributeArray[index];
                  dictionary.Add((string) consumableAttributeArray[index], guiTreePanel);
                }
                guiTreePanel.AddSubPanel((GuiPanel) new GuiMenuItem(current));
              }
            }
          }
          using (Dictionary<string, GuiTreePanel>.KeyCollection.Enumerator enumerator = dictionary.Keys.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              string current = enumerator.Current;
              if (dictionary.TryGetValue(current, out guiTreePanel))
              {
                guiTreePanel.ShowHeaderInfo = true;
                guiTreePanel.IsAlwaysOpen = false;
                this.m_treePanel.ShowBackground = true;
                guiTreePanel.CurrentPanelState = GuiTreePanel.PanelState.Closed;
                this.m_treePanel.AddSubPanel((GuiPanel) guiTreePanel);
              }
              else
                Debug.LogError((object) "Setting the ability has failed");
            }
          }
        }
      }
    }

    public GuiConsumablePanel()
      : base("GUI/_NewGui/layout/SwitchAmmo/switch_ammo")
    {
      this.m_abilityIcon = new AtlasCache(new float2(40f, 35f));
      this.m_defaultMenuItem = new GuiMenuItem(this.m_abilityIcon);
      this.m_scrollPanel = this.Find<GuiScrollPanel>();
      this.m_treePanel = this.m_scrollPanel.FindScrollChild<GuiTreePanel>();
      this.m_treePanel.Position = new Vector2(0.0f, 0.0f);
      this.Find<GuiButton>().Pressed = (AnonymousDelegate) (() => this.IsRendered = false);
      this.RecalculateAbsCoords();
      this.IsRendered = false;
      this.WhenElementChanges = new AnonymousDelegate(this.ElementHasChanged);
    }

    public override void Draw()
    {
      GuiElementBase.DrawElement.BeginStencil(this.Rect, true, false);
      base.Draw();
      GuiElementBase.DrawElement.EndStencil();
    }

    public override void OnShow()
    {
      base.OnShow();
      FeedbackProtocol.GetProtocol().ReportUiElementShown(FeedbackProtocol.UiElementId.ChangeAmmoMenu);
      Game.InputDispatcher.Focused = (InputListener) this;
    }

    public override void OnHide()
    {
      base.OnHide();
      FeedbackProtocol.GetProtocol().ReportUiElementHidden(FeedbackProtocol.UiElementId.ChangeAmmoMenu);
      if (Game.InputDispatcher.Focused == this)
        Game.InputDispatcher.Focused = (InputListener) null;
      this.m_ability = (ShipAbility) null;
    }

    public void ElementHasChanged()
    {
      for (int index = 0; index < this.Children.Count; ++index)
      {
        if (this.Children[index].WhenElementChanges != null)
          this.Children[index].WhenElementChanges();
      }
    }

    private ItemList GetItemList(byte tier)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      return SpaceLevel.GetLevel().Shop.Filter(new Predicate<ShipItem>(new GuiConsumablePanel.\u003CGetItemList\u003Ec__AnonStorey8D() { tier = tier, \u003C\u003Ef__this = this }.\u003C\u003Em__FD));
    }

    private List<ItemCountable> GetHoldList(byte tier)
    {
      List<ItemCountable> itemCountableList = new List<ItemCountable>();
      foreach (ShipItem shipItem in (IEnumerable<ShipItem>) Game.Me.Hold)
      {
        if (shipItem is ItemCountable)
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          GuiConsumablePanel.\u003CGetHoldList\u003Ec__AnonStorey8F listCAnonStorey8F = new GuiConsumablePanel.\u003CGetHoldList\u003Ec__AnonStorey8F();
          // ISSUE: reference to a compiler-generated field
          listCAnonStorey8F.consumable = shipItem as ItemCountable;
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          if ((bool) this.m_ability.card.IsLoaded && (bool) listCAnonStorey8F.consumable.Card.IsLoaded && ((int) this.m_ability.card.ConsumableType == (int) listCAnonStorey8F.consumable.Card.ConsumableType && (int) this.m_ability.card.ConsumableTier == (int) listCAnonStorey8F.consumable.Card.Tier) && (((int) listCAnonStorey8F.consumable.Card.Tier == (int) tier || (int) listCAnonStorey8F.consumable.Card.Tier == 0) && !((IEnumerable<ConsumableEffectType>) this.m_ability.card.effectTypeBlacklist).Any<ConsumableEffectType>(new Func<ConsumableEffectType, bool>(listCAnonStorey8F.\u003C\u003Em__FE))))
          {
            // ISSUE: reference to a compiler-generated field
            listCAnonStorey8F.consumable.disableBuying = true;
            itemCountableList.Add(shipItem as ItemCountable);
          }
        }
      }
      return itemCountableList;
    }

    private List<ItemCountable> GetCountableItemList(ItemList shopList, byte tier)
    {
      List<ItemCountable> holdList = this.GetHoldList(tier);
      List<ItemCountable> itemCountableList = new List<ItemCountable>();
      foreach (ShipItem shop in (IEnumerable<ShipItem>) shopList)
      {
        if (shop is ItemCountable)
        {
          ItemCountable itemCountable = shop as ItemCountable;
          for (int index = 0; index < holdList.Count; ++index)
          {
            if ((int) itemCountable.ItemGUICard.CardGUID == (int) holdList[index].ItemGUICard.CardGUID)
            {
              itemCountable.count = holdList[index].count;
              holdList.RemoveAt(index);
              break;
            }
          }
          itemCountableList.Add(itemCountable);
        }
      }
      for (int index = 0; index < holdList.Count; ++index)
        itemCountableList.Add(holdList[index]);
      return itemCountableList;
    }

    private List<GuiMenuItem> CreateMenuItemList()
    {
      byte tier = Game.Me.ActiveShip.Card.Tier;
      List<ItemCountable> countableItemList = this.GetCountableItemList(this.GetItemList(tier), tier);
      countableItemList.Sort(new Comparison<ItemCountable>(this.CompairItems));
      List<GuiMenuItem> guiMenuItemList = new List<GuiMenuItem>();
      for (int index = 0; index < countableItemList.Count; ++index)
      {
        GuiMenuItem guiMenuItem = new GuiMenuItem(this.m_defaultMenuItem);
        guiMenuItem.SetData(this.m_ability, countableItemList[index]);
        guiMenuItemList.Add(guiMenuItem);
      }
      if (this.m_ability.card.ConsumableOption == ShipConsumableOption.Optional)
      {
        GuiMenuItem guiMenuItem = new GuiMenuItem(this.m_abilityIcon);
        guiMenuItemList.Add(guiMenuItem);
      }
      return guiMenuItemList;
    }

    private int CompairItems(ItemCountable item1, ItemCountable item2)
    {
      return string.Compare(item1.Card.GUICard.Name, item2.Card.GUICard.Name);
    }

    public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
    {
      if (mouseKey == KeyCode.Mouse0 && !this.Contains(mousePosition))
      {
        this.IsRendered = false;
        return false;
      }
      if (!this.HandleMouseInput)
        return false;
      return base.OnMouseDown(mousePosition, mouseKey);
    }

    public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
    {
      if (!this.HandleMouseInput)
        return false;
      return base.OnMouseUp(mousePosition, mouseKey);
    }

    public void MakeValidInScreenPosition(Vector2 position)
    {
      this.PositionX = position.x;
      this.PositionY = position.y - this.ElementWithPaddingSizeY;
      if ((double) this.PositionX < (double) this.ElementPadding.Left)
        this.PositionX = this.ElementPadding.Left;
      else if ((double) this.PositionX > (double) Screen.width - (double) this.ElementWithPaddingSizeX)
        this.PositionX = (float) Screen.width - this.ElementWithPaddingSizeX;
      if ((double) this.PositionY < (double) this.ElementPadding.Top)
        this.PositionY = this.ElementPadding.Top;
      else if ((double) this.PositionY > (double) Screen.height - (double) this.ElementWithPaddingSizeY)
        this.PositionY = (float) Screen.height - this.ElementWithPaddingSizeY;
      if (this.WhenElementChanges == null)
        return;
      this.WhenElementChanges();
    }
  }
}
