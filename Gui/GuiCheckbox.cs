﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiCheckbox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("")]
  public class GuiCheckbox : GuiElementBase
  {
    [HideInInspector]
    private bool isActive = true;
    [HideInInspector]
    public GUISoundHandler OverSound = new GUISoundHandler(GUISound.Instance.OnMouseOver);
    [HideInInspector]
    public GUISoundHandler PressedSound = new GUISoundHandler(GUISound.Instance.OnSelect);
    private GuiLabel label = new GuiLabel();
    private GuiImage image = new GuiImage();
    private float gapSpace = 10f;
    private string onToolTip = "%$bgo.checkbox_tooltip.on_tool_tip%";
    private string offToolTip = "%$bgo.checkbox_tooltip.off_tool_tip%";
    private string inactiveToolTip = "%$bgo.checkbox_tooltip.inactive_tool_tip%";
    private GuiCheckbox.CheckboxState state;
    private bool isChecked;
    [HideInInspector]
    public System.Action<bool> OnCheck;
    [HideInInspector]
    public System.Action<GuiCheckbox> OnCheckAction;
    private Texture2D normal;
    private Texture2D over;
    private Texture2D pressed;
    private Texture2D inactive;
    private Texture2D normal_x;
    private Texture2D over_x;
    private Texture2D pressed_x;
    private Texture2D inactive_x;
    [HideInInspector]
    private GuiCheckbox.CheckboxState lastImageSelected;

    public GuiLabel Label
    {
      get
      {
        return this.label;
      }
    }

    public GuiImage Image
    {
      get
      {
        return this.image;
      }
    }

    [Gui2Editor]
    public string Text
    {
      get
      {
        return this.label.Text;
      }
      set
      {
        this.label.Text = value;
        this.ForceReposition();
      }
    }

    [Gui2Editor]
    public bool IsChecked
    {
      get
      {
        return this.isChecked;
      }
      set
      {
        if (this.isChecked == value)
          return;
        this.isChecked = value;
        this.SetTooltip(!this.IsActive ? this.inactiveToolTip : (!this.isChecked ? this.onToolTip : this.offToolTip));
        this.CheckImage();
      }
    }

    public bool IsActive
    {
      get
      {
        return this.isActive;
      }
      set
      {
        if (this.isActive == value)
          return;
        this.isActive = value;
        this.SetTooltip(!this.isActive ? this.inactiveToolTip : (!this.IsChecked ? this.onToolTip : this.offToolTip));
        this.label.IsActive = this.isActive;
        this.state = !this.isActive ? GuiCheckbox.CheckboxState.Inactive : GuiCheckbox.CheckboxState.Normal;
        this.CheckImage();
      }
    }

    public float GapSpace
    {
      get
      {
        return this.gapSpace;
      }
    }

    public string InactiveToolTip
    {
      get
      {
        return this.inactiveToolTip;
      }
    }

    public string OffToolTip
    {
      get
      {
        return this.offToolTip;
      }
    }

    public string OnToolTip
    {
      get
      {
        return this.onToolTip;
      }
    }

    [Gui2Editor]
    public Texture2D NormalTexture
    {
      get
      {
        return this.normal;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.normal)
          this.image.Texture = value;
        this.normal = value;
      }
    }

    [Gui2Editor]
    public Texture2D OverTexture
    {
      get
      {
        return this.over;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.over)
          this.image.Texture = value;
        this.over = value;
      }
    }

    [Gui2Editor]
    public Texture2D PressedTexture
    {
      get
      {
        return this.pressed;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.pressed)
          this.image.Texture = value;
        this.pressed = value;
      }
    }

    [Gui2Editor]
    public Texture2D InactiveTexture
    {
      get
      {
        return this.inactive;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.inactive)
          this.image.Texture = value;
        this.inactive = value;
      }
    }

    [Gui2Editor]
    public Texture2D NormalXTexture
    {
      get
      {
        return this.normal_x;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.normal_x)
          this.image.Texture = value;
        this.normal_x = value;
      }
    }

    [Gui2Editor]
    public Texture2D OverXTexture
    {
      get
      {
        return this.over_x;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.over_x)
          this.image.Texture = value;
        this.over_x = value;
      }
    }

    [Gui2Editor]
    public Texture2D PressedXTexture
    {
      get
      {
        return this.pressed_x;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.pressed_x)
          this.image.Texture = value;
        this.pressed_x = value;
      }
    }

    [Gui2Editor]
    public Texture2D InactiveXTexture
    {
      get
      {
        return this.inactive_x;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.inactive_x)
          this.image.Texture = value;
        this.inactive_x = value;
      }
    }

    public GuiCheckbox()
      : this(string.Empty)
    {
    }

    public GuiCheckbox(string text)
    {
      this.VerifyTextures();
      this.CheckImage();
      this.image.Parent = (SRect) this;
      this.image.Size = new Vector2((float) this.normal.width, (float) this.normal.height);
      this.image.Position = new Vector2(0.0f, 0.0f);
      this.image.WhenElementChanges = new AnonymousDelegate(this.WhenComponentsChange);
      this.label.Parent = (SRect) this;
      this.label.Text = text;
      this.label.Alignment = TextAnchor.MiddleLeft;
      this.label.Position = new Vector2(0.0f, 0.0f);
      this.label.WhenElementChanges = new AnonymousDelegate(this.WhenComponentsChange);
      this.WhenComponentsChange();
      this.SetTooltip(!this.IsActive ? this.inactiveToolTip : (!this.IsChecked ? this.onToolTip : this.offToolTip));
    }

    public GuiCheckbox(GuiCheckbox copy)
      : base((GuiElementBase) copy)
    {
      this.state = copy.state;
      this.isChecked = copy.IsChecked;
      this.isActive = copy.IsActive;
      this.OnCheck = copy.OnCheck;
      this.OnCheckAction = copy.OnCheckAction;
      this.OverSound = copy.OverSound;
      this.PressedSound = copy.PressedSound;
      this.normal = copy.normal;
      this.over = copy.over;
      this.pressed = copy.pressed;
      this.inactive = copy.inactive;
      this.normal_x = copy.normal_x;
      this.over_x = copy.over_x;
      this.pressed_x = copy.pressed_x;
      this.inactive_x = copy.inactive_x;
      this.VerifyTextures();
      this.onToolTip = copy.onToolTip;
      this.offToolTip = copy.offToolTip;
      this.inactiveToolTip = copy.inactiveToolTip;
      this.gapSpace = copy.gapSpace;
      this.image = new GuiImage(copy.image);
      this.image.Parent = (SRect) this;
      this.image.WhenElementChanges = new AnonymousDelegate(this.WhenComponentsChange);
      this.CheckImage();
      this.label = new GuiLabel(copy.label);
      this.label.Parent = (SRect) this;
      this.label.WhenElementChanges = new AnonymousDelegate(this.WhenComponentsChange);
      this.label.IsActive = copy.isActive;
      this.SetTooltip(!this.IsActive ? this.inactiveToolTip : (!this.IsChecked ? this.onToolTip : this.offToolTip));
    }

    public GuiCheckbox(JCheckbox checkbox)
      : base((JElementBase) checkbox)
    {
      this.normal = ResourceLoader.Load<Texture2D>(checkbox.normalTexture);
      this.over = ResourceLoader.Load<Texture2D>(checkbox.overTexture);
      this.pressed = ResourceLoader.Load<Texture2D>(checkbox.pressedTexture);
      this.inactive = ResourceLoader.Load<Texture2D>(checkbox.inactiveTexture);
      this.normal_x = ResourceLoader.Load<Texture2D>(checkbox.normalTexture_x);
      this.over_x = ResourceLoader.Load<Texture2D>(checkbox.overTexture_x);
      this.pressed_x = ResourceLoader.Load<Texture2D>(checkbox.pressedTexture_x);
      this.inactive_x = ResourceLoader.Load<Texture2D>(checkbox.inactiveTexture_x);
      this.VerifyTextures();
      this.inactiveToolTip = checkbox.inactiveToolTip;
      this.onToolTip = checkbox.onToolTip;
      this.offToolTip = checkbox.offToolTip;
      this.gapSpace = checkbox.gapSpace;
      this.image = new GuiImage((JElementBase) checkbox.image);
      this.image.Parent = (SRect) this;
      this.image.WhenElementChanges = new AnonymousDelegate(this.WhenComponentsChange);
      this.CheckImage();
      this.label = new GuiLabel((JElementBase) checkbox.label);
      this.label.WhenElementChanges = new AnonymousDelegate(this.WhenComponentsChange);
      this.label.Parent = (SRect) this;
      this.label.IsActive = this.isActive;
      this.SetTooltip(!this.IsActive ? this.inactiveToolTip : (!this.IsChecked ? this.onToolTip : this.offToolTip));
    }

    private void VerifyTextures()
    {
      this.normal = this.normal ?? Options.checkboxNormal;
      this.over = this.over ?? Options.checkboxOver;
      this.pressed = this.pressed ?? Options.checkboxPressed;
      this.inactive = this.inactive ?? Options.checkboxInactive;
      this.normal_x = this.normal_x ?? Options.checkboxNormal_x;
      this.over_x = this.over_x ?? Options.checkboxOver_x;
      this.pressed_x = this.pressed_x ?? Options.checkboxPressed_x;
      this.inactive_x = this.inactive_x ?? Options.checkboxInactive_x;
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiCheckbox(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JCheckbox((GuiElementBase) this);
    }

    public override void LocalizeElement()
    {
      base.LocalizeElement();
      this.label.LocalizeElement();
    }

    public override void DoCaching()
    {
      base.DoCaching();
      this.image.DoCaching();
      this.label.DoCaching();
    }

    private void CheckImage()
    {
      switch (this.state)
      {
        case GuiCheckbox.CheckboxState.Normal:
          this.image.SetTextureWithoutResize(!this.IsChecked ? this.normal : this.normal_x);
          break;
        case GuiCheckbox.CheckboxState.Over:
          this.image.SetTextureWithoutResize(!this.IsChecked ? this.over : this.over_x);
          break;
        case GuiCheckbox.CheckboxState.Pressed:
          this.image.SetTextureWithoutResize(!this.IsChecked ? this.pressed : this.pressed_x);
          break;
        case GuiCheckbox.CheckboxState.Inactive:
          this.image.SetTextureWithoutResize(!this.IsChecked ? this.inactive : this.inactive_x);
          break;
      }
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      if (this.state != this.lastImageSelected)
        this.CheckImage();
      this.lastImageSelected = this.state;
      this.VerifyTextures();
      this.image.EditorUpdate();
      this.label.EditorUpdate();
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
      this.image.EditorDraw();
      this.label.EditorDraw();
    }

    public override void Update()
    {
      base.Update();
      this.image.Update();
      this.label.Update();
    }

    public override void Draw()
    {
      base.Draw();
      this.image.Draw();
      this.label.Draw();
    }

    private void WhenComponentsChange()
    {
      this.CheckImage();
      this.label.PositionX = this.image.SizeX + this.gapSpace;
      this.label.PositionY = this.image.PositionCenter.y - this.label.SizeY * 0.5f;
      this.image.Position = Vector2.zero;
      this.label.IsRendered = !string.IsNullOrEmpty(this.label.TextParsed);
      if (this.label.IsRendered)
        this.SizeX = this.label.PositionX + this.label.SizeX;
      else
        this.SizeX = this.image.PositionX + this.image.SizeX;
      this.SizeY = this.image.SizeY;
    }

    public override void MouseEnter(float2 position, float2 previousPosition)
    {
      base.MouseEnter(position, previousPosition);
      if (!this.IsActive)
        return;
      this.state = GuiCheckbox.CheckboxState.Over;
      this.CheckImage();
      if (this.OverSound == null)
        return;
      this.OverSound();
    }

    public override void MouseLeave(float2 position, float2 previousPosition)
    {
      base.MouseLeave(position, previousPosition);
      if (!this.IsActive)
        return;
      this.state = GuiCheckbox.CheckboxState.Normal;
      this.CheckImage();
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      if (!this.IsActive)
        return false;
      this.state = GuiCheckbox.CheckboxState.Pressed;
      this.CheckImage();
      base.MouseDown(position, key);
      Inspector2.Selected = (SRect) this;
      return true;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      base.MouseUp(position, key);
      if (!this.IsActive || key != KeyCode.Mouse0)
        return false;
      this.IsChecked = !this.IsChecked;
      this.state = GuiCheckbox.CheckboxState.Over;
      this.CheckImage();
      if (this.OnCheck != null)
        this.OnCheck(this.IsChecked);
      if (this.OnCheckAction != null)
        this.OnCheckAction(this);
      if (this.PressedSound != null)
        this.PressedSound();
      return true;
    }

    [Gui2Editor]
    private void SelectLabel()
    {
      Inspector2.Selected = (SRect) this.label;
    }

    [Gui2Editor]
    private void SelectImage()
    {
      Inspector2.Selected = (SRect) this.image;
    }

    private enum CheckboxState
    {
      Normal,
      Over,
      Pressed,
      Inactive,
    }
  }
}
