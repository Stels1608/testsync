﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiWingsManagementPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/_NewGui/layout/wings/wingsManagementPanel")]
  public class GuiWingsManagementPanel : GuiPanel
  {
    private GuiWingsManagementPanel.GuiRoleNamingPanel rolePanel;

    public GuiWingsManagementPanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiWingsManagementPanel>())
    {
      this.Initialize();
    }

    public GuiWingsManagementPanel(GuiWingsManagementPanel copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiWingsManagementPanel(JWingsManagementPanel json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public bool HasFocus()
    {
      return this.rolePanel.FocusedInput();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiWingsManagementPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JWingsManagementPanel((GuiElementBase) this);
    }

    private void Initialize()
    {
      this.rolePanel = this.Find<GuiWingsManagementPanel.GuiRoleNamingPanel>();
      if (this.rolePanel == null)
      {
        GuiPanel panel = this.Find<GuiPanel>(GuiWingsManagementPanel.GuiRoleNamingPanel.ms_PanelName);
        this.RemoveChild((GuiElementBase) panel);
        this.rolePanel = new GuiWingsManagementPanel.GuiRoleNamingPanel(panel);
        this.AddChild((GuiElementBase) this.rolePanel);
      }
      if (this.Find<GuiWingsManagementPanel.GuiRolePermissionsPanel>() != null)
        return;
      GuiPanel panel1 = this.Find<GuiPanel>(GuiWingsManagementPanel.GuiRolePermissionsPanel.ms_PanelName);
      this.RemoveChild((GuiElementBase) panel1);
      this.AddChild((GuiElementBase) new GuiWingsManagementPanel.GuiRolePermissionsPanel(panel1));
    }

    public class GuiRoleNamingPanel : GuiPanel
    {
      public static string ms_PanelName = "rolePanel";
      [HideInInspector]
      private List<GuiInputTextBox> m_inputboxList = new List<GuiInputTextBox>();
      private const string mc_inputboxName = "inputTextBox";

      public GuiRoleNamingPanel(GuiPanel panel)
        : base(panel)
      {
        for (int index = 0; index < Enum.GetValues(typeof (GuildRole)).Length - 1; ++index)
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          GuiWingsManagementPanel.GuiRoleNamingPanel.\u003CGuiRoleNamingPanel\u003Ec__AnonStorey93 panelCAnonStorey93 = new GuiWingsManagementPanel.GuiRoleNamingPanel.\u003CGuiRoleNamingPanel\u003Ec__AnonStorey93();
          GuiInputTextBox guiInputTextBox = this.Find<GuiInputTextBox>("inputTextBox" + index.ToString());
          // ISSUE: reference to a compiler-generated field
          panelCAnonStorey93.role = (GuildRole) (index + 1);
          // ISSUE: reference to a compiler-generated method
          guiInputTextBox.OnTextChanged = new System.Action<string>(panelCAnonStorey93.\u003C\u003Em__10F);
          if ((UnityEngine.Object) Game.Instance != (UnityEngine.Object) null)
          {
            guiInputTextBox.IsActive = Game.Me.Guild.GetPermissionFor(Game.Me.Guild.GuildRole, GuildOperation.ChangePermissions);
            // ISSUE: reference to a compiler-generated field
            guiInputTextBox.Text = Game.Me.Guild._RankName(panelCAnonStorey93.role);
            // ISSUE: reference to a compiler-generated field
            guiInputTextBox.EmptyStringMessage = Tools.FormatWingRole(panelCAnonStorey93.role);
          }
          this.m_inputboxList.Add(guiInputTextBox);
        }
      }

      public bool FocusedInput()
      {
        bool flag = false;
        using (List<GuiInputTextBox>.Enumerator enumerator = this.m_inputboxList.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            if (enumerator.Current.IsFocused)
              flag = true;
          }
        }
        return flag;
      }

      public override void PeriodicUpdate()
      {
        base.PeriodicUpdate();
        for (int index = 0; index < this.m_inputboxList.Count; ++index)
        {
          GuildRole guildRole = (GuildRole) (index + 1);
          if (!this.m_inputboxList[index].IsFocused)
            this.m_inputboxList[index].Text = Game.Me.Guild._RankName(guildRole);
          this.m_inputboxList[index].IsActive = Game.Me.Guild.GetPermissionFor(Game.Me.Guild.GuildRole, GuildOperation.ChangeRankNames);
          this.m_inputboxList[index].EmptyStringMessage = Tools.FormatWingRole(guildRole);
        }
      }
    }

    public class GuiRolePermissionsPanel : GuiPanel
    {
      public static string ms_PanelName = "permissionsPanel";

      public GuiRolePermissionsPanel(GuiPanel panel)
        : base(panel)
      {
        GuiScrollPanel guiScrollPanel = this.Find<GuiScrollPanel>();
        if (guiScrollPanel.FindScrollChild<GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel>() != null)
          return;
        GuiPanel scrollChild = guiScrollPanel.FindScrollChild<GuiPanel>(GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.ms_PanelName);
        guiScrollPanel.RemoveScrollChild((GuiElementBase) scrollChild);
        GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel permissionsRowPanel = new GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel(scrollChild);
        guiScrollPanel.AddScrollChild((GuiElementBase) permissionsRowPanel);
      }

      private class GuiPermissionsRowPanel : GuiPanel
      {
        public static string ms_PanelName = "rowPanel";
        [HideInInspector]
        private List<GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow> m_permissionRows = new List<GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow>();
        private const string mc_darkBGName = "darkBackground";
        private const string mc_lightBGName = "lightBackground";
        [HideInInspector]
        private GuiImage m_darkRowBackground;
        [HideInInspector]
        private GuiImage m_lightRowBackground;
        [HideInInspector]
        private GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow m_defaultRowPanel;

        public GuiPermissionsRowPanel(GuiPanel panel)
          : base(panel)
        {
          this.m_darkRowBackground = this.Find<GuiImage>("darkBackground");
          this.m_lightRowBackground = this.Find<GuiImage>("lightBackground");
          this.m_defaultRowPanel = this.Find<GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow>(GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow.ms_PanelName);
          if (this.m_defaultRowPanel == null)
          {
            GuiPanel panel1 = this.Find<GuiPanel>(GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow.ms_PanelName);
            this.RemoveChild((GuiElementBase) panel1);
            this.m_defaultRowPanel = new GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow(panel1);
            this.AddChild((GuiElementBase) this.m_defaultRowPanel);
          }
          this.m_defaultRowPanel.IsRendered = false;
          this.m_darkRowBackground.IsRendered = false;
          this.m_lightRowBackground.IsRendered = false;
          this.InitializePermissionRows();
        }

        private void InitializePermissionRows()
        {
          float num = this.m_lightRowBackground.SizeY * 0.5f;
          for (int index = 0; index < Enum.GetValues(typeof (GuildOperation)).Length; ++index)
          {
            GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow permissionRow = new GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow((GuiPanel) this.m_defaultRowPanel);
            permissionRow.IsRendered = true;
            permissionRow.Permission = (GuildOperation) (index + 1);
            permissionRow.Parent = (SRect) this;
            this.m_permissionRows.Add(permissionRow);
            this.m_permissionRows[index].PositionY = num - this.m_permissionRows[index].SizeY * 0.5f;
            num += this.m_lightRowBackground.SizeY;
          }
          this.SizeY = num - this.m_lightRowBackground.SizeY * 0.5f;
        }

        public override void EditorUpdate()
        {
          base.EditorUpdate();
          this.m_defaultRowPanel.IsRendered = true;
          this.m_darkRowBackground.IsRendered = true;
          this.m_lightRowBackground.IsRendered = true;
        }

        public override void Update()
        {
          base.Update();
          for (int index = 0; index < this.m_permissionRows.Count; ++index)
            this.m_permissionRows[index].Update();
        }

        public override void PeriodicUpdate()
        {
          base.PeriodicUpdate();
          for (int index = 0; index < this.m_permissionRows.Count; ++index)
            this.m_permissionRows[index].PeriodicUpdate();
        }

        public override void DoCaching()
        {
          base.DoCaching();
          for (int index = 0; index < this.m_permissionRows.Count; ++index)
            this.m_permissionRows[index].DoCaching();
        }

        public override void Draw()
        {
          base.Draw();
          GuiElementBase.DrawElement.BeginStencil(this.Rect, true, false);
          Rect drawRect = new Rect();
          drawRect.x = this.Rect.x;
          drawRect.y = this.Rect.y;
          bool flag = false;
          for (int index = 0; index < this.m_permissionRows.Count; ++index)
          {
            GuiImage guiImage = !flag ? this.m_darkRowBackground : this.m_lightRowBackground;
            drawRect.width = guiImage.Rect.width;
            drawRect.height = guiImage.Rect.height;
            GuiElementBase.DrawElement.DrawTexture(guiImage.Texture, drawRect, guiImage.NineSliceEdge, new Rect?(guiImage.SourceRect), guiImage.OverlayColor, false);
            drawRect.y += drawRect.height;
            this.m_permissionRows[index].Draw();
            flag = !flag;
          }
          GuiElementBase.DrawElement.EndStencil();
        }

        public override bool MouseDown(float2 position, KeyCode key)
        {
          for (int index = 0; index < this.m_permissionRows.Count; ++index)
          {
            if (this.m_permissionRows[index].Contains(position) && this.m_permissionRows[index].MouseDown(position, key))
              return true;
          }
          return base.MouseDown(position, key);
        }

        public override bool MouseUp(float2 position, KeyCode key)
        {
          for (int index = 0; index < this.m_permissionRows.Count; ++index)
          {
            if (this.m_permissionRows[index].Contains(position) && this.m_permissionRows[index].MouseUp(position, key))
              return true;
          }
          return base.MouseUp(position, key);
        }

        public override bool MouseMove(float2 position, float2 previousPosition)
        {
          for (int index = 0; index < this.m_permissionRows.Count; ++index)
          {
            if (this.m_permissionRows[index].MouseMove(position, previousPosition))
              return true;
          }
          return base.MouseMove(position, previousPosition);
        }

        private class PermissionRow : GuiPanel
        {
          public static string ms_PanelName = "permissionRowPanel";
          [HideInInspector]
          private List<GuiCheckbox> m_checkboxList = new List<GuiCheckbox>();
          private const string mc_permissionName = "permissionLabel";
          private const string mc_checkboxName = "checkbox";
          [HideInInspector]
          private GuiLabel m_permissionLabel;
          [HideInInspector]
          private GuildOperation m_permission;

          public GuildOperation Permission
          {
            get
            {
              return this.m_permission;
            }
            set
            {
              if (this.m_permission == value)
                return;
              this.m_permission = value;
              this.CheckPermissions();
            }
          }

          public PermissionRow(GuiPanel panel)
            : base(panel)
          {
            this.m_permissionLabel = this.Find<GuiLabel>("permissionLabel");
            for (int index = 0; index < Enum.GetValues(typeof (GuildRole)).Length - 1; ++index)
            {
              // ISSUE: object of a compiler-generated type is created
              // ISSUE: variable of a compiler-generated type
              GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow.\u003CPermissionRow\u003Ec__AnonStorey94 rowCAnonStorey94 = new GuiWingsManagementPanel.GuiRolePermissionsPanel.GuiPermissionsRowPanel.PermissionRow.\u003CPermissionRow\u003Ec__AnonStorey94();
              // ISSUE: reference to a compiler-generated field
              rowCAnonStorey94.\u003C\u003Ef__this = this;
              this.m_checkboxList.Add(this.Find<GuiCheckbox>("checkbox" + index.ToString()));
              // ISSUE: reference to a compiler-generated field
              rowCAnonStorey94.role = (GuildRole) (index + 1);
              // ISSUE: reference to a compiler-generated method
              this.m_checkboxList[index].OnCheck = new System.Action<bool>(rowCAnonStorey94.\u003C\u003Em__110);
              if ((UnityEngine.Object) Game.Instance != (UnityEngine.Object) null)
                this.CheckPermission(index);
            }
          }

          private void CheckPermissions()
          {
            if ((UnityEngine.Object) Game.Instance != (UnityEngine.Object) null)
            {
              for (int index = 0; index < this.m_checkboxList.Count; ++index)
                this.CheckPermission(index);
            }
            this.m_permissionLabel.Text = this.GetPermissionString(this.m_permission);
          }

          private void CheckPermission(int index)
          {
            GuildRole role = (GuildRole) (index + 1);
            this.m_checkboxList[index].IsChecked = Game.Me.Guild.GetPermissionFor(role, this.m_permission);
            bool flag = Game.Me.Guild.GetPermissionFor(Game.Me.Guild.GuildRole, GuildOperation.ChangePermissions) && Game.Me.Guild.GuildRole > role && (role != GuildRole.Pilot ? 0 : (this.m_permission == GuildOperation.PromoteDemote ? 1 : 0)) == 0 && (role != GuildRole.Recruit ? 0 : (this.m_permission == GuildOperation.PromoteDemote ? 1 : 0)) == 0 && (role != GuildRole.Recruit ? 0 : (this.m_permission == GuildOperation.KickMember ? 1 : 0)) == 0 && (role != GuildRole.Recruit ? 0 : (this.m_permission == GuildOperation.ChangePermissions ? 1 : 0)) == 0;
            this.m_checkboxList[index].IsActive = flag;
          }

          public override void PeriodicUpdate()
          {
            base.PeriodicUpdate();
            this.CheckPermissions();
          }

          private string GetPermissionString(GuildOperation permission)
          {
            switch (permission)
            {
              case GuildOperation.ChangePermissions:
                return "%$bgo.Wings.permission_labels.change_permission%";
              case GuildOperation.ChangeRankNames:
                return "%$bgo.Wings.permission_labels.change_rank_name%";
              case GuildOperation.Invite:
                return "%$bgo.Wings.permission_labels.invite%";
              case GuildOperation.PromoteDemote:
                return "%$bgo.Wings.permission_labels.promote_demote%";
              case GuildOperation.KickMember:
                return "%$bgo.Wings.permission_labels.kick_member%";
              case GuildOperation.OfficerChat:
                return "%$bgo.Wings.permission_labels.officer_chat%";
              default:
                return "Current permission, " + permission.ToString() + ", has not been added to be localized";
            }
          }
        }
      }
    }
  }
}
