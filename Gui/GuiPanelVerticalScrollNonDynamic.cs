﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiPanelVerticalScrollNonDynamic
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiPanelVerticalScrollNonDynamic : GuiPanelVerticalScroll
  {
    public GuiPanelVerticalScrollNonDynamic()
    {
      this.StencilOn = true;
    }

    protected override void ScrollerChanged(int delta)
    {
      float num = this.AllElementsHeight();
      this.ScrollerChangedAbsolutePercent(Mathf.Clamp(this.PercentagePosition() + ((double) num != 0.0 ? 40f / this.AllElementsHeight() * (float) delta : 0.0f), 0.0f, (double) num != 0.0 ? Mathf.Clamp01((float) (1.0 - (double) this.SizeY / (double) num)) : 0.0f));
    }

    protected float PercentagePosition()
    {
      float num = this.AllElementsHeight();
      if ((double) num == 0.0)
        return 0.0f;
      return -this.queue.PositionY / num;
    }

    protected float AllElementsHeight()
    {
      return this.CalculateElementsHeight(0, this.allElements.Count - 1);
    }

    protected override void ScrollerChangedAbsolutePercent(float percent)
    {
      this.queue.DeselectWhenRemoved = false;
      this.queue.EmptyChildren();
      for (int index = 0; index < this.allElements.Count; ++index)
        this.queue.AddChild(this.allElements[index]);
      this.queue.DeselectWhenRemoved = true;
      this.queue.PositionY = -Mathf.Max(0.0f, this.CalculateElementsHeight(0, this.allElements.Count - 1)) * percent;
      this.UpdateScroller();
      this.OnScroll();
    }

    private void UpdateScroller()
    {
      this.scroller.visibleHeight = Mathf.Min(this.AllElementsHeight(), this.SizeY);
      this.scroller.invisibleBeforeHeight = Mathf.Max(0.0f, -this.queue.PositionY);
      this.scroller.invisibleAfterHeight = Mathf.Max(0.0f, this.AllElementsHeight() - this.SizeY + this.queue.PositionY);
      this.scroller.ForceReposition();
    }
  }
}
