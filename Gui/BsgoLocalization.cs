﻿// Decompiled with JetBrains decompiler
// Type: Gui.BsgoLocalization
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using UnityEngine;

namespace Gui
{
  public class BsgoLocalization
  {
    private static readonly System.Collections.Generic.Dictionary<string, string> m_cultureInfoDict = new System.Collections.Generic.Dictionary<string, string>() { { "bg", "bg-BG" }, { "br", "pt-BR" }, { "cs", "cs-CZ" }, { "da", "da-DK" }, { "de", "de-DE" }, { "el", "el-GR" }, { "en", "en-US" }, { "es", "es-ES" }, { "fi", "fi-FI" }, { "fr", "fr-FR" }, { "hu", "hu-HU" }, { "it", "it-IT" }, { "nl", "nl-NL" }, { "no", "nb-NO" }, { "pl", "pl-PL" }, { "pt", "pt-PT" }, { "ro", "ro-RO" }, { "ru", "ru-RU" }, { "sk", "sk-SK" }, { "sv", "sv-SE" }, { "tr", "tr-TR" }, { "us", "en-US" } };
    private readonly System.Collections.Generic.Dictionary<string, string> dic = new System.Collections.Generic.Dictionary<string, string>();
    private readonly System.Collections.Generic.Dictionary<string, string> dic_en = new System.Collections.Generic.Dictionary<string, string>();
    private List<string> errorMessagesNoLoc = new List<string>();
    private List<string> errorMessagesMissing = new List<string>();
    private static CultureInfo m_cultureInfo;

    public static System.Collections.Generic.Dictionary<string, string> Dictionary
    {
      get
      {
        return Game.Localization.dic;
      }
    }

    public static System.Collections.Generic.Dictionary<string, string> DictionaryEn
    {
      get
      {
        return Game.Localization.dic_en;
      }
    }

    public void InitLanguage(string lang)
    {
      BsgoLocalization.m_cultureInfo = !BsgoLocalization.m_cultureInfoDict.ContainsKey(lang) ? new CultureInfo(BsgoLocalization.m_cultureInfoDict["en"]) : new CultureInfo(BsgoLocalization.m_cultureInfoDict[lang]);
    }

    public void ParseAssetDefault(TextAsset xml)
    {
      BsgoLocalization.ParseAsset(xml, this.dic_en);
    }

    public void ParseAsset(TextAsset xml)
    {
      BsgoLocalization.ParseAsset(xml, this.dic);
    }

    public static void ParseAsset(TextAsset xml, System.Collections.Generic.Dictionary<string, string> dic)
    {
      if ((UnityEngine.Object) xml == (UnityEngine.Object) null)
        Log.DebugInfo((object) "Localization: text asset is null.");
      else if (xml.text == null)
      {
        Log.DebugInfo((object) ("Localization: '" + xml.name + "' doesn.t have text."));
      }
      else
      {
        try
        {
          MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(xml.text));
          XmlTextReader reader = new XmlTextReader((Stream) memoryStream);
          reader.Normalization = true;
          reader.WhitespaceHandling = WhitespaceHandling.None;
          reader.ReadStartElement("language");
          while (reader.NodeType == XmlNodeType.Element)
            BsgoLocalization.ReadCategory(reader, dic);
          if (reader.Name != "language" || reader.NodeType != XmlNodeType.EndElement)
            throw new Exception("Strange Xml. Closing tag category was expected.");
          reader.Close();
          memoryStream.Close();
        }
        catch (Exception ex)
        {
          Debug.LogError((object) (xml.name + ": " + ex.Message + " " + ex.StackTrace));
        }
      }
    }

    private static void ReadCategory(XmlTextReader reader, System.Collections.Generic.Dictionary<string, string> dic)
    {
      if (reader.Name != "category" || reader.NodeType != XmlNodeType.Element)
        throw new Exception("Strange Xml. Item category was expected. Have Name " + reader.Name + ", Type " + (object) reader.NodeType);
      reader.Read();
      if (reader.NodeType == XmlNodeType.EndElement)
      {
        reader.Read();
      }
      else
      {
        if (reader.NodeType != XmlNodeType.Element)
          throw new Exception("Strange Xml. Item item was expected. Found item " + (object) reader.NodeType);
        while (reader.Name == "item")
          BsgoLocalization.ReadItem((XmlReader) reader, dic);
        if (reader.Name != "category" || reader.NodeType != XmlNodeType.EndElement)
          return;
        reader.Read();
      }
    }

    private static void ReadItem(XmlReader reader, System.Collections.Generic.Dictionary<string, string> dic)
    {
      if (reader.Name != "item" || reader.NodeType != XmlNodeType.Element)
        throw new Exception("Strange Xml. Item element was expected.");
      string lower = reader.GetAttribute("name").ToLower();
      string str = reader.ReadElementContentAsString();
      if (dic.ContainsKey(lower))
        Log.DebugInfo((object) ("Localization: duplicate key " + lower + " (" + dic[lower] + " -> " + str + ")"));
      dic[lower] = str;
    }

    public string GetText(string text)
    {
      string str = (string) null;
      if (!this.TryGetTranslation(text, out str))
        this.ErrorMessageOnce(text);
      return str ?? text;
    }

    public static bool TryGet(string key, out string value)
    {
      return Game.Localization.TryGetTranslation(key, out value);
    }

    public static string GetShipName(string key)
    {
      string str;
      if (BsgoLocalization.TryGet("bgo." + key + ".Name_1", out str))
        return str;
      return BsgoLocalization.Get("bgo." + key + ".Name");
    }

    public static string Get(string key)
    {
      if ((UnityEngine.Object) Game.Instance == (UnityEngine.Object) null)
      {
        Game.Localization.ErrorMessageOnceRawText("The game instance is null, therefore Localization will not work");
        return key;
      }
      if (!Game.IsLocaLoaded)
        return key;
      string str = (string) null;
      if (!BsgoLocalization.TryGet(key, out str))
        Game.Localization.ErrorMessageOnce(key);
      return str ?? key;
    }

    public static string Get(string key, params object[] args)
    {
      string format = BsgoLocalization.Get(key);
      if (format != null)
        return string.Format(format, args);
      return (string) null;
    }

    public static string GetForLevel(string key, int desiredLevel)
    {
      int num = desiredLevel;
      string str = (string) null;
      while (!BsgoLocalization.TryGet(key + num.ToString(), out str) && num >= 0)
        --num;
      if (str != null)
        return str;
      Game.Localization.ErrorMessageOnce(key + desiredLevel.ToString());
      return key;
    }

    public static void Error(string key)
    {
      Game.Localization.ErrorMessageOnce(key);
    }

    public bool TryGetTranslation(string key, out string value)
    {
      if (key == null)
      {
        Debug.Log((object) "Translation for null string is impossible.");
        value = (string) null;
        return false;
      }
      if (key.StartsWith("%$") && key.EndsWith("%"))
        key = key.Substring(2, key.Length - 3);
      key = key.ToLower();
      if (this.dic.TryGetValue(key, out value))
        return true;
      if (!this.dic_en.TryGetValue(key, out value))
        return false;
      this.ErrorMessageMissing(key, value);
      return false;
    }

    private void ErrorMessageOnce(string key)
    {
      if (this.errorMessagesNoLoc.Contains(key))
        return;
      this.errorMessagesNoLoc.Add(key);
      Log.DebugInfo((object) ("Text-Entry for '" + key + "' not found"));
    }

    private void ErrorMessageOnceRawText(string errorText)
    {
      if (this.errorMessagesNoLoc.Contains(errorText))
        return;
      this.errorMessagesNoLoc.Add(errorText);
      Debug.LogError((object) errorText);
    }

    private void ErrorMessageMissing(string key, string defaultTranslation)
    {
      if (this.errorMessagesMissing.Contains(key))
        return;
      this.errorMessagesMissing.Add(key);
      Log.DebugInfo((object) ("Translation '" + Game.Language + "' for '" + key + " not found. Default is '" + defaultTranslation + "'"));
    }

    public void ClearWarningCache()
    {
      this.errorMessagesNoLoc.Clear();
      this.errorMessagesMissing.Clear();
    }

    public static string FormatLong(long number)
    {
      return BsgoLocalization.m_cultureInfo == null ? number.ToString() : number.ToString("n0", (IFormatProvider) BsgoLocalization.m_cultureInfo.NumberFormat);
    }

    public static string FormatDouble(double number)
    {
      number = Math.Round(number, 2);
      return BsgoLocalization.m_cultureInfo == null ? number.ToString() : number.ToString((IFormatProvider) BsgoLocalization.m_cultureInfo.NumberFormat);
    }
  }
}
