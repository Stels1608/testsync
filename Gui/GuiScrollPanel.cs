﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiScrollPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("")]
  public class GuiScrollPanel : GuiPanel
  {
    private Vector2 m_lastMousePosition = new Vector2();
    private const string mc_childPanelName = "childPanel";
    private const string mc_verticalScrollBarName = "vertScrollBar";
    [HideInInspector]
    private GuiVerticalScrollBar m_verticalScrollBar;
    public AnonymousDelegate ScrollPanelUp;
    public AnonymousDelegate ScrollPanelDown;
    public AnonymousDelegate ScrollPanelLeft;
    public AnonymousDelegate ScrollPanelRight;
    public AnonymousDelegate UseOldStyleScroller;
    [HideInInspector]
    private GuiPanel m_childPanel;

    public GuiScrollPanel()
    {
      this.Initialize();
    }

    public GuiScrollPanel(GuiScrollPanel copy)
      : base((GuiPanel) copy)
    {
      this.Initialize();
    }

    public GuiScrollPanel(JScrollPanel json)
      : base((JPanel) json)
    {
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiScrollPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JScrollPanel((GuiElementBase) this);
    }

    public override void EditorDraw()
    {
      GuiElementBase.DrawElement.BeginStencil(this.Rect, true, false);
      base.EditorDraw();
      for (int index = 0; index < this.Children.Count; ++index)
      {
        if (this.Children[index].IsRendered)
          this.Children[index].EditorDraw();
      }
      GuiElementBase.DrawElement.EndStencil();
    }

    public override void EditorUpdate()
    {
      this.ElementHasChanged();
      base.EditorUpdate();
      this.m_childPanel.Position = new Vector2(-0.0f, -this.m_verticalScrollBar.ScrollPosition);
      this.m_childPanel.Size = new Vector2(this.SizeX - this.m_verticalScrollBar.ElementWithPaddingSizeX, this.SizeY);
      this.m_childPanel.Name = "childPanel";
      this.m_childPanel.Parent = (SRect) this;
      this.UpdateScrollDistance();
    }

    private void Initialize()
    {
      this.m_childPanel = this.Find<GuiPanel>("childPanel");
      if (this.m_childPanel == null)
      {
        this.m_childPanel = new GuiPanel();
        this.m_childPanel.Name = "childPanel";
        this.AddChild((GuiElementBase) this.m_childPanel);
      }
      this.m_verticalScrollBar = this.Find<GuiVerticalScrollBar>("vertScrollBar");
      if (this.m_verticalScrollBar == null)
      {
        this.m_verticalScrollBar = new GuiVerticalScrollBar();
        this.m_verticalScrollBar.Name = "vertScrollBar";
        this.AddChild((GuiElementBase) this.m_verticalScrollBar);
      }
      this.WhenElementChanges = new AnonymousDelegate(this.ElementHasChanged);
      this.ScrollPanelUp = new AnonymousDelegate(((GuiScrollBarBase) this.m_verticalScrollBar).ScrollNegative);
      this.ScrollPanelDown = new AnonymousDelegate(((GuiScrollBarBase) this.m_verticalScrollBar).ScrollPositive);
      this.ScrollPanelLeft = (AnonymousDelegate) null;
      this.ScrollPanelRight = (AnonymousDelegate) null;
      this.m_verticalScrollBar.OnScroll = new System.Action<float>(this.OnPanelScroll);
      this.UseOldStyleScroller = (AnonymousDelegate) (() =>
      {
        this.RemoveChild((GuiElementBase) this.m_verticalScrollBar);
        this.m_verticalScrollBar = new GuiVerticalScrollBar(true);
        this.AddChild((GuiElementBase) this.m_verticalScrollBar);
      });
      this.StencilOn = true;
      this.m_childPanel.StencilOn = true;
    }

    public override void DoCaching()
    {
      base.DoCaching();
      for (int index = 0; index < this.Children.Count; ++index)
        this.Children[index].DoCaching();
    }

    public override void Update()
    {
      base.Update();
      this.m_childPanel.PositionX = 0.0f;
      this.m_childPanel.PositionY = -this.m_verticalScrollBar.ScrollPosition;
      this.m_childPanel.SizeX = this.SizeX - (!this.m_verticalScrollBar.IsRendered ? 0.0f : this.m_verticalScrollBar.ElementWithPaddingSizeX);
      this.UpdateScrollDistance();
    }

    private void OnPanelScroll(float scrollAmount)
    {
      Vector2 vec = this.m_lastMousePosition;
      vec.y -= scrollAmount;
      this.m_childPanel.OnMouseMove(float2.FromV2(vec));
    }

    private void UpdateScrollDistance()
    {
      Vector2 vector2_1 = new Vector2(this.SizeX - this.m_verticalScrollBar.ElementWithPaddingSizeX, this.SizeY);
      for (int index = 0; index < this.m_childPanel.Children.Count; ++index)
      {
        Vector2 vector2_2 = this.m_childPanel.Children[index].Position + this.m_childPanel.Children[index].Size + this.m_childPanel.Children[index].ElementPadding.BottomLeft;
        vector2_1.y = Mathf.Max(vector2_1.y, vector2_2.y);
      }
      if (!(this.m_childPanel.Size != vector2_1))
        return;
      this.m_childPanel.Size = vector2_1;
      this.m_verticalScrollBar.ScrollDistance = vector2_1.y;
    }

    private void ElementHasChanged()
    {
      if (this.m_verticalScrollBar.WhenElementChanges != null)
        this.m_verticalScrollBar.WhenElementChanges();
      this.m_childPanel.Size = Vector2.zero;
      this.UpdateScrollDistance();
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      this.m_lastMousePosition = position.ToV2();
      return base.MouseMove(position, previousPosition);
    }

    public override bool ScrollUp(float2 position)
    {
      if (!this.IsMouseOver || !this.m_verticalScrollBar.ScrollUp(position))
        return base.ScrollUp(position);
      return true;
    }

    public override bool ScrollDown(float2 position)
    {
      if (!this.IsMouseOver || !this.m_verticalScrollBar.ScrollDown(position))
        return base.ScrollDown(position);
      return true;
    }

    public void AddScrollChild(GuiElementBase ch)
    {
      this.m_childPanel.AddChild(ch);
    }

    public void RemoveScrollChild(GuiElementBase ch)
    {
      if (this.m_childPanel == null)
        return;
      this.m_childPanel.RemoveChild(ch);
    }

    public void ClearScrollChildPanel()
    {
      if (this.m_childPanel == null)
        return;
      this.m_childPanel.RemoveAll();
    }

    public T FindScrollChild<T>() where T : GuiElementBase
    {
      foreach (GuiElementBase child in this.m_childPanel.Children)
      {
        if (child is T)
          return child as T;
      }
      return (T) null;
    }

    public T FindScrollChild<T>(string name) where T : GuiElementBase
    {
      foreach (GuiElementBase child in this.m_childPanel.Children)
      {
        if (child is T && child.Name == name)
          return child as T;
      }
      return (T) null;
    }
  }
}
