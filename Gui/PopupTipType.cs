﻿// Decompiled with JetBrains decompiler
// Type: Gui.PopupTipType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Gui
{
  public enum PopupTipType
  {
    ControlCharacter = 1,
    EnterRecroom = 2,
    Talk2Apollo_No2 = 3,
    Talk2Starbuck_No6 = 4,
    EnterHangar = 5,
    Talk2Edison_No8 = 6,
    Undock = 7,
    SectorMap = 8,
    SectorJump = 9,
    TournamentPopupID = 10,
    TournamentEndingPopupID = 11,
    BattlespaceAvailable = 12,
    COUNT = 13,
  }
}
