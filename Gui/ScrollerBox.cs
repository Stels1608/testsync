﻿// Decompiled with JetBrains decompiler
// Type: Gui.ScrollerBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class ScrollerBox : GuiPanel
  {
    private GuiImage box = new GuiImage("GUI/Common/scroller");
    public System.Action<float> onScroll;
    private bool dragging;

    public float BoxPosition
    {
      get
      {
        return this.box.PositionY / this.SizeY;
      }
      set
      {
        if (this.dragging)
          return;
        this.box.PositionY = value * this.SizeY;
      }
    }

    public float BoxHeight
    {
      get
      {
        return this.box.SizeY / this.SizeY;
      }
      set
      {
        if (this.dragging)
          return;
        this.box.SizeY = value * this.SizeY;
      }
    }

    public ScrollerBox()
    {
      this.AddChild((GuiElementBase) this.box);
    }

    public override void Reposition()
    {
      base.Reposition();
      this.box.SizeX = this.SizeX;
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      if (this.box.Contains(position) && key == KeyCode.Mouse0)
      {
        this.IsMouseInputGrabber = true;
        this.dragging = true;
      }
      base.MouseDown(position, key);
      return true;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      if (key == KeyCode.Mouse0)
      {
        this.IsMouseInputGrabber = false;
        this.dragging = false;
      }
      base.MouseUp(position, key);
      return true;
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      base.MouseMove(position, previousPosition);
      if (!this.dragging)
        return false;
      this.box.PositionY = this.box.PositionY + (Mathf.Clamp(position.y, this.Rect.yMin, this.Rect.yMax) - Mathf.Clamp(previousPosition.y, this.Rect.yMin, this.Rect.yMax));
      if ((double) this.box.PositionY < 0.0)
        this.box.PositionY = 0.0f;
      if ((double) this.box.PositionY + (double) this.box.SizeY > (double) this.SizeY)
        this.box.PositionY = this.SizeY - this.box.SizeY;
      this.onScroll(this.box.PositionY / this.SizeY);
      return true;
    }
  }
}
