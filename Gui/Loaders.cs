﻿// Decompiled with JetBrains decompiler
// Type: Gui.Loaders
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Gui
{
  public class Loaders
  {
    private static Dictionary<string, JWindowDescription> m_layoutCache = new Dictionary<string, JWindowDescription>();

    public static void ClearJsonFromCache(string fileName)
    {
      Loaders.m_layoutCache.Remove(fileName);
    }

    public static void ClearJsonCache()
    {
      Loaders.m_layoutCache.Clear();
    }

    private static GuiPanel LoadJsonResource(string documentName)
    {
      JWindowDescription jwindowDescription;
      if (!Loaders.m_layoutCache.TryGetValue(documentName, out jwindowDescription))
      {
        jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource(documentName);
        Loaders.m_layoutCache.Add(documentName, jwindowDescription);
      }
      GuiPanel guiPanel1 = new GuiPanel();
      if (jwindowDescription.Elements.Count > 0)
      {
        if (jwindowDescription.Elements[0] is JPanel)
        {
          GuiPanel guiPanel2 = jwindowDescription.Elements[0].Convert2Gui() as GuiPanel;
          if (jwindowDescription.Elements.Count > 1)
            Debug.LogError((object) "There is more than one main panel.");
          return guiPanel2;
        }
        if (jwindowDescription.Elements[0] is JImage)
          guiPanel1.Size = (jwindowDescription.Elements[0] as JImage).size.ToV2();
        else
          guiPanel1.Size = new Vector2(600f, 400f);
        using (List<JElementBase>.Enumerator enumerator = jwindowDescription.Elements.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            GuiElementBase guiElementBase = enumerator.Current.Convert2Gui();
            if (guiElementBase != null)
            {
              guiPanel1.AddChild(guiElementBase);
              Loaders.FixPosition((GuiElementBase) guiPanel1, guiElementBase);
            }
          }
        }
      }
      return guiPanel1;
    }

    private static GuiPanel LoadBalsamiqResource(string documentName)
    {
      try
      {
        documentName = "Assets/Resources/" + documentName;
        XMLBalsamiqDecoder xmlBalsamiqDecoder = new XMLBalsamiqDecoder(documentName);
        if (xmlBalsamiqDecoder.Panel == null)
          Debug.Log((object) "Panel is NULL");
        return xmlBalsamiqDecoder.Panel;
      }
      catch
      {
        Debug.LogError((object) documentName);
      }
      return (GuiPanel) null;
    }

    public static void Load(GuiPanel panel, string documentName)
    {
      GuiPanel guiPanel = !documentName.EndsWith(".bmml") ? Loaders.LoadJsonResource(documentName) : Loaders.LoadBalsamiqResource(documentName);
      if (guiPanel == null)
        throw new IOException("The document, " + documentName + ", was not.\nCheck that it is a Gui layout document and it is a valid file.");
      panel.Name = guiPanel.Name;
      panel.Position = guiPanel.Position;
      panel.Size = guiPanel.Size;
      panel.ElementPadding = guiPanel.ElementPadding;
      panel.Align = guiPanel.Align;
      foreach (GuiElementBase child in guiPanel.Children)
        panel.AddChild(child);
    }

    public static void FixPosition(GuiElementBase parent, GuiElementBase el)
    {
      el.Position = el.Position + parent.Size / 2f - el.Size / 2f;
    }
  }
}
