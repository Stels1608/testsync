﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiTabPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System.Collections.Generic;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("GUI/_NewGui/layout/tabPanel")]
  public class GuiTabPanel : GuiPanel
  {
    private float m_maxButtonWidth = 100f;
    [HideInInspector]
    private List<GuiButton> m_buttonList = new List<GuiButton>();
    [HideInInspector]
    private List<GuiPanel> m_panelList = new List<GuiPanel>();
    private const string mc_buttonAreaName = "buttonArea";
    private const string mc_panelAreaName = "panelArea";
    private const string mc_defaultButtonName = "defaultButton";
    private int m_currentPanelIndex;
    [HideInInspector]
    private GuiPanel m_buttonArea;
    [HideInInspector]
    private GuiTabPanel.GuiPanelController m_panelArea;
    private GuiButton m_defaultButton;

    public GuiButton DefaultButton
    {
      get
      {
        return this.m_defaultButton;
      }
    }

    public GuiPanel CurrentPanel
    {
      get
      {
        if (this.m_panelList.Count > this.m_currentPanelIndex && this.m_currentPanelIndex >= 0)
          return this.m_panelList[this.m_currentPanelIndex];
        return (GuiPanel) null;
      }
      set
      {
        if (value == null)
          return;
        for (int index = 0; index < this.m_panelList.Count; ++index)
        {
          if (this.m_panelList[index] == value)
          {
            this.SwitchTo(index);
            break;
          }
        }
      }
    }

    public float MaxButtonWidth
    {
      get
      {
        return this.m_maxButtonWidth;
      }
      set
      {
        this.m_maxButtonWidth = value;
      }
    }

    public GuiTabPanel()
      : base(UseInGuiEditor.GetDefaultLayout<GuiTabPanel>())
    {
      this.Initialize();
    }

    public GuiTabPanel(GuiTabPanel copy)
      : base((GuiPanel) copy)
    {
      this.m_maxButtonWidth = copy.m_maxButtonWidth;
      this.m_defaultButton = new GuiButton(copy.m_defaultButton);
      this.Initialize();
    }

    public GuiTabPanel(JTabPanel json)
      : base((JPanel) json)
    {
      this.m_maxButtonWidth = json.m_maxButtonWidth;
      this.m_defaultButton = new GuiButton((JElementBase) json.m_defaultButton);
      this.Initialize();
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiTabPanel(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JTabPanel((GuiElementBase) this);
    }

    public override void EditorUpdate()
    {
      base.EditorUpdate();
      this.m_defaultButton.EditorUpdate();
      if (this.m_panelList.Count <= this.m_currentPanelIndex || this.m_currentPanelIndex < 0)
        this.m_currentPanelIndex = 0;
      for (int index = 0; index < this.m_panelList.Count; ++index)
        this.m_panelList[index].IsRendered = this.m_currentPanelIndex == index;
    }

    private void Initialize()
    {
      this.m_currentPanelIndex = 0;
      this.m_buttonArea = this.Find<GuiPanel>("buttonArea");
      if (this.m_defaultButton == null)
      {
        this.m_defaultButton = this.m_buttonArea.Find<GuiButton>("defaultButton");
        this.m_buttonArea.RemoveChild((GuiElementBase) this.m_defaultButton);
      }
      this.m_panelArea = this.Find<GuiTabPanel.GuiPanelController>("panelArea");
      if (this.m_panelArea == null)
      {
        GuiPanel panel = this.Find<GuiPanel>("panelArea");
        this.RemoveChild((GuiElementBase) panel);
        this.m_panelArea = new GuiTabPanel.GuiPanelController(panel);
        this.AddChild((GuiElementBase) this.m_panelArea);
      }
      this.m_panelList.Clear();
      for (int index = 0; index < this.m_panelArea.Children.Count; ++index)
      {
        if (this.m_panelArea.Children[index] is GuiPanel)
        {
          this.m_panelArea.Children[index].IsRendered = false;
          this.m_panelList.Add(this.m_panelArea.Children[index] as GuiPanel);
        }
      }
      this.m_buttonList.Clear();
      for (int index = 0; index < this.m_buttonArea.Children.Count; ++index)
      {
        if (this.m_buttonArea.Children[index] is GuiButton)
          this.m_buttonList.Add(this.m_buttonArea.Children[index] as GuiButton);
      }
      this.InitializeButtons();
      this.SwitchTo(this.m_currentPanelIndex);
    }

    private void InitializeButtons()
    {
      if (this.m_panelList.Count != this.m_buttonList.Count)
      {
        this.m_buttonList.Clear();
        this.m_buttonArea.RemoveAll();
        for (int index = 0; index < this.m_panelList.Count; ++index)
        {
          GuiButton guiButton = new GuiButton(this.m_defaultButton);
          guiButton.Name = "tabButton_" + (object) index;
          this.m_buttonList.Add(guiButton);
          this.m_buttonArea.AddChild((GuiElementBase) guiButton);
        }
      }
      this.PositionButtons();
    }

    private void PositionButtons()
    {
      float num1 = Mathf.Min(this.m_buttonArea.SizeX / (float) this.m_buttonList.Count, this.m_maxButtonWidth);
      float num2 = 0.0f;
      for (int index = 0; index < this.m_buttonList.Count; ++index)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        GuiTabPanel.\u003CPositionButtons\u003Ec__AnonStorey90 buttonsCAnonStorey90 = new GuiTabPanel.\u003CPositionButtons\u003Ec__AnonStorey90();
        // ISSUE: reference to a compiler-generated field
        buttonsCAnonStorey90.\u003C\u003Ef__this = this;
        this.m_buttonList[index].SizeX = num1;
        this.m_buttonList[index].SizeY = this.m_buttonArea.SizeY;
        this.m_buttonList[index].PositionX = num2;
        num2 += this.m_buttonList[index].SizeX;
        // ISSUE: reference to a compiler-generated field
        buttonsCAnonStorey90.index = index;
        // ISSUE: reference to a compiler-generated method
        this.m_buttonList[index].Pressed = new AnonymousDelegate(buttonsCAnonStorey90.\u003C\u003Em__101);
      }
    }

    public void AddTabPanel(GuiPanel panel)
    {
      this.m_panelArea.AddChild((GuiElementBase) panel);
    }

    private void InitializeAddedPanel(GuiPanel panel)
    {
      panel.IsRendered = false;
      this.m_panelList.Add(panel);
      if (this.m_defaultButton == null)
        return;
      GuiButton guiButton = new GuiButton(this.m_defaultButton);
      guiButton.Name = "tabButton_" + (object) this.m_buttonList.Count;
      this.m_buttonList.Add(guiButton);
      this.m_buttonArea.AddChild((GuiElementBase) guiButton);
      this.PositionButtons();
    }

    public void RemoveTabPanel(GuiPanel panel)
    {
      this.m_panelArea.RemoveChild((GuiElementBase) panel);
    }

    private void UninitializeRemovedPanel(GuiPanel panel)
    {
      int index = this.m_panelList.IndexOf(panel);
      if (index < 0 || index >= this.m_buttonList.Count)
        return;
      this.m_buttonArea.RemoveChild((GuiElementBase) this.m_buttonList[index]);
      this.m_panelList.RemoveAt(index);
      this.m_buttonList.RemoveAt(index);
      this.PositionButtons();
    }

    public void SwitchTo(int index)
    {
      if (this.m_panelList.Count > this.m_currentPanelIndex)
      {
        this.m_panelList[this.m_currentPanelIndex].IsRendered = false;
        this.m_buttonList[this.m_currentPanelIndex].IsPressedManual = false;
      }
      this.m_currentPanelIndex = index;
      if (this.m_panelList.Count <= this.m_currentPanelIndex)
        return;
      this.m_panelList[this.m_currentPanelIndex].IsRendered = true;
      this.m_buttonList[this.m_currentPanelIndex].IsPressedManual = true;
    }

    public GuiButton GetButtonOfPanel(GuiPanel panel)
    {
      for (int index = 0; index < this.m_panelList.Count; ++index)
      {
        if (this.m_panelList[index] == panel)
          return this.m_buttonList[index];
      }
      return (GuiButton) null;
    }

    public IEnumerable<GuiPanel> GetGuiPanelList()
    {
      return (IEnumerable<GuiPanel>) this.m_panelList;
    }

    private class GuiPanelController : GuiPanel
    {
      public GuiPanelController()
      {
      }

      public GuiPanelController(GuiPanel panel)
        : base(panel)
      {
      }

      public override void AddChild(GuiElementBase ch)
      {
        if (ch is GuiPanel)
          (this.Parent as GuiTabPanel).InitializeAddedPanel(ch as GuiPanel);
        base.AddChild(ch);
      }

      public override void RemoveChild(GuiElementBase ch)
      {
        if (ch is GuiPanel)
          (this.Parent as GuiTabPanel).UninitializeRemovedPanel(ch as GuiPanel);
        base.RemoveChild(ch);
      }

      public override void Draw()
      {
        GuiElementBase.DrawElement.BeginStencil(this.Rect, true, false);
        base.Draw();
        GuiElementBase.DrawElement.EndStencil();
      }
    }
  }
}
