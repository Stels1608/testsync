﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

namespace Gui
{
  [UseInGuiEditor("")]
  public class GuiButton : GuiElementBase
  {
    [HideInInspector]
    public GUISoundHandler OverSound = new GUISoundHandler(GUISound.Instance.OnMouseOver);
    [HideInInspector]
    public GUISoundHandler PressedSound = new GUISoundHandler(GUISound.Instance.OnSelect);
    [HideInInspector]
    public Vector2 LabelOffset = new Vector2(0.0f, 0.0f);
    [HideInInspector]
    private bool isActive = true;
    protected GuiLabel label = new GuiLabel();
    protected GuiImage image = new GuiImage();
    [HideInInspector]
    private bool isPressedManual;
    [HideInInspector]
    public AnonymousDelegate Pressed;
    [HideInInspector]
    public System.Action<GuiButton> PressedAction;
    protected Texture2D normal;
    protected Texture2D over;
    protected Texture2D pressed;
    protected Texture2D inactive;
    private GuiButton.ImageSelected currentImageSelected;
    [HideInInspector]
    private GuiButton.ImageSelected lastImageSelected;
    private GuiBlinker blinker;

    public bool IsActive
    {
      get
      {
        return this.isActive;
      }
      set
      {
        if (this.isActive == value)
          return;
        this.label.IsActive = this.isActive = value;
        this.image.SetTextureWithoutResize(!value ? this.inactive : this.normal);
      }
    }

    public bool IsHighlighted
    {
      get
      {
        if (this.blinker != null)
          return this.blinker.IsRendered;
        return false;
      }
      set
      {
        if (this.blinker == null)
        {
          if (!value)
            return;
          this.blinker = new GuiBlinker();
          this.blinker.Parent = (SRect) this;
          this.adjustBlinker();
          this.blinker.IsRendered = value;
        }
        else
          this.blinker.IsRendered = value;
      }
    }

    [Gui2Editor]
    public Color? OverlayColor
    {
      get
      {
        return this.image.OverlayColor;
      }
      set
      {
        this.image.OverlayColor = value;
        this.label.OverlayColor = value;
      }
    }

    public GuiLabel Label
    {
      get
      {
        return this.label;
      }
      set
      {
        if (value == null)
          return;
        this.label = value;
      }
    }

    public GuiImage Image
    {
      get
      {
        return this.image;
      }
      set
      {
        if (value == null)
          return;
        this.image = value;
      }
    }

    [Gui2Editor]
    public string Text
    {
      get
      {
        return this.label.Text;
      }
      set
      {
        this.label.Text = value;
      }
    }

    [Gui2Editor]
    public Font Font
    {
      get
      {
        return this.label.Font;
      }
      set
      {
        this.label.Font = value;
      }
    }

    [Gui2Editor]
    public int FontSize
    {
      get
      {
        return this.label.FontSize;
      }
      set
      {
        this.label.FontSize = value;
      }
    }

    [Gui2Editor]
    public bool IsPressedManual
    {
      get
      {
        return this.isPressedManual;
      }
      set
      {
        this.isPressedManual = value;
        Vector2 size = this.image.Size;
        if (this.isPressedManual)
          this.image.SetTextureWithoutResize(this.pressed);
        else
          this.image.SetTextureWithoutResize(!this.IsMouseOver ? this.normal : this.over);
        this.image.Size = size;
      }
    }

    [Gui2Editor]
    public GuiElementPadding ImagePadding
    {
      get
      {
        return this.image.NineSliceEdge;
      }
      set
      {
        this.image.NineSliceEdge = value;
      }
    }

    [Gui2Editor]
    public int UniformImagePadding
    {
      get
      {
        return (int) this.image.NineSliceEdge.Left;
      }
      set
      {
        this.image.NineSliceEdge = (GuiElementPadding) new Rect((float) value, (float) value, (float) value, (float) value);
      }
    }

    [Gui2Editor]
    public Texture2D NormalTexture
    {
      get
      {
        return this.normal;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.normal)
          this.image.Texture = value;
        this.normal = value;
      }
    }

    [Gui2Editor]
    public Texture2D OverTexture
    {
      get
      {
        return this.over;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.over)
          this.image.Texture = value;
        this.over = value;
      }
    }

    [Gui2Editor]
    public Texture2D PressedTexture
    {
      get
      {
        return this.pressed;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.pressed)
          this.image.Texture = value;
        this.pressed = value;
      }
    }

    [Gui2Editor]
    public Texture2D InactiveTexture
    {
      get
      {
        return this.inactive;
      }
      set
      {
        if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.inactive)
          this.image.Texture = value;
        this.inactive = value;
      }
    }

    public GuiButton(Texture2D texture)
      : this(string.Empty, texture, texture, texture, texture, TextAnchor.MiddleCenter)
    {
    }

    public GuiButton()
      : this("Button", Options.buttonNormal, Options.buttonOver, Options.buttonPressed, Options.buttonInactive, TextAnchor.MiddleCenter)
    {
      this.image.WhenElementChanges = new AnonymousDelegate(this.WhenImageRectChanges);
    }

    public GuiButton(string text, TextAnchor anchor)
      : this(text, Options.buttonNormal, Options.buttonOver, Options.buttonPressed, Options.buttonInactive, anchor)
    {
    }

    public GuiButton(string text)
      : this(text, Options.buttonNormal, Options.buttonOver, Options.buttonPressed, Options.buttonInactive, TextAnchor.MiddleCenter)
    {
    }

    public GuiButton(string text, string texture)
      : this(text, texture, texture, texture)
    {
    }

    public GuiButton(string text, string normal, string over, string pressed)
      : this(text, normal, over, pressed, Options.buttonInactive.name)
    {
    }

    public GuiButton(string text, string normal, string over, string pressed, string inactive)
      : this(text, ResourceLoader.Load<Texture2D>(normal), ResourceLoader.Load<Texture2D>(over), ResourceLoader.Load<Texture2D>(pressed), !(inactive != string.Empty) ? Options.buttonInactive : ResourceLoader.Load<Texture2D>(inactive), TextAnchor.MiddleCenter)
    {
    }

    public GuiButton(string text, Texture2D normal, Texture2D over, Texture2D pressed)
      : this(text, normal, over, pressed, Options.buttonInactive, TextAnchor.MiddleCenter)
    {
    }

    public GuiButton(string text, Texture2D normal, Texture2D over, Texture2D pressed, Texture2D inactive, TextAnchor anchor = TextAnchor.MiddleCenter)
    {
      this.normal = normal ?? Options.buttonNormal;
      this.over = over ?? Options.buttonOver;
      this.pressed = pressed ?? Options.buttonPressed;
      this.inactive = inactive ?? Options.buttonInactive;
      this.image.SetTextureWithoutResize(normal);
      this.image.NineSliceEdge = (GuiElementPadding) new Rect(6f, 6f, 6f, 6f);
      this.image.Parent = (SRect) this;
      this.Size = new Vector2((float) normal.width, (float) normal.height);
      this.label.Text = text;
      this.label.Parent = (SRect) this;
      this.label.Alignment = anchor;
      this.label.AutoSize = false;
      this.label.IsActive = this.isActive;
      this.image.WhenElementChanges = new AnonymousDelegate(this.WhenImageRectChanges);
      GuiButton guiButton = this;
      AnonymousDelegate anonymousDelegate = guiButton.WhenElementChanges + new AnonymousDelegate(this.adjustBlinker);
      guiButton.WhenElementChanges = anonymousDelegate;
    }

    public GuiButton(GuiButton copy)
      : base((GuiElementBase) copy)
    {
      this.isPressedManual = copy.isPressedManual;
      this.Pressed = copy.Pressed;
      this.PressedAction = copy.PressedAction;
      this.OverSound = copy.OverSound;
      this.PressedSound = copy.PressedSound;
      this.LabelOffset = copy.LabelOffset;
      this.normal = copy.normal;
      this.over = copy.over;
      this.pressed = copy.pressed;
      this.inactive = copy.inactive;
      this.label = new GuiLabel(copy.label);
      this.label.Parent = (SRect) this;
      this.label.IsActive = copy.label.IsActive;
      this.image = new GuiImage(copy.image);
      this.image.Parent = (SRect) this;
      this.image.WhenElementChanges = new AnonymousDelegate(this.WhenImageRectChanges);
      this.isActive = copy.IsActive;
      this.IsHighlighted = copy.IsHighlighted;
    }

    public GuiButton(JElementBase json)
      : base(json)
    {
      JButton jbutton = json as JButton;
      this.label = new GuiLabel((JElementBase) jbutton.label);
      this.label.Parent = (SRect) this;
      this.normal = ResourceLoader.Load<Texture2D>(jbutton.normalTexture);
      this.over = ResourceLoader.Load<Texture2D>(jbutton.overTexture);
      this.pressed = ResourceLoader.Load<Texture2D>(jbutton.pressedTexture);
      this.inactive = ResourceLoader.Load<Texture2D>(jbutton.inactiveTexture);
      this.normal = this.normal ?? Options.buttonNormal;
      this.over = this.over ?? Options.buttonOver;
      this.pressed = this.pressed ?? Options.buttonPressed;
      this.inactive = this.inactive ?? Options.buttonInactive;
      this.image = new GuiImage((JElementBase) jbutton.image);
      this.image.SetTextureWithoutResize(this.normal);
      this.image.Parent = (SRect) this;
      this.image.WhenElementChanges = new AnonymousDelegate(this.WhenImageRectChanges);
    }

    public override GuiElementBase CloneElement()
    {
      return (GuiElementBase) new GuiButton(this);
    }

    public override JElementBase Convert2Json()
    {
      return (JElementBase) new JButton((GuiElementBase) this);
    }

    public override void LocalizeElement()
    {
      base.LocalizeElement();
      this.label.LocalizeElement();
    }

    private void adjustBlinker()
    {
      if (this.blinker == null)
        return;
      this.blinker.PositionCenter = this.Size / 2f;
    }

    public override void DoCaching()
    {
      base.DoCaching();
      this.image.DoCaching();
      this.label.DoCaching();
      if (!this.IsHighlighted)
        return;
      this.blinker.DoCaching();
    }

    public override void OnShow()
    {
      base.OnShow();
      this.image.OnShow();
      this.label.OnShow();
    }

    public override void OnHide()
    {
      base.OnHide();
      this.image.OnHide();
      this.label.OnHide();
    }

    private Texture2D GetSelectedImage(GuiButton.ImageSelected selection)
    {
      switch (selection)
      {
        case GuiButton.ImageSelected.Normal:
          return this.normal;
        case GuiButton.ImageSelected.Over:
          return this.over;
        case GuiButton.ImageSelected.Pressed:
          return this.pressed;
        case GuiButton.ImageSelected.Inactive:
          return this.inactive;
        default:
          return (Texture2D) null;
      }
    }

    private void WhenImageRectChanges()
    {
      this.Size = this.image.Size;
    }

    public override void EditorUpdate()
    {
      if (this.currentImageSelected != this.lastImageSelected || (UnityEngine.Object) this.image.Texture != (UnityEngine.Object) this.GetSelectedImage(this.currentImageSelected))
        this.image.SetTextureWithoutResize(this.GetSelectedImage(this.currentImageSelected));
      this.lastImageSelected = this.currentImageSelected;
      this.label.IsActive = this.isActive;
      this.normal = this.normal ?? Options.buttonNormal;
      this.over = this.over ?? Options.buttonOver;
      this.pressed = this.pressed ?? Options.buttonPressed;
      this.inactive = this.inactive ?? Options.buttonInactive;
      this.label.EditorUpdate();
      this.image.EditorUpdate();
    }

    public override void EditorDraw()
    {
      base.EditorDraw();
      this.image.EditorDraw();
      this.label.EditorDraw();
    }

    public override void Update()
    {
      base.Update();
      this.image.Update();
      this.label.Update();
      if (!this.IsHighlighted)
        return;
      this.blinker.Update();
    }

    public override void Draw()
    {
      base.Draw();
      this.image.Draw();
      this.label.Draw();
      if (!this.IsHighlighted)
        return;
      this.blinker.Draw();
    }

    public override void Reposition()
    {
      base.Reposition();
      this.image.Size = this.Size;
      this.label.Size = this.Size;
      this.image.ForceReposition();
      this.label.ForceReposition();
      if (this.blinker == null)
        return;
      this.blinker.ForceReposition();
    }

    [Gui2Editor]
    public void SetNormalTextureWithoutResize(Texture2D newTexture)
    {
      if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.normal)
        this.image.SetTextureWithoutResize(newTexture);
      this.normal = newTexture;
    }

    [Gui2Editor]
    public void SetOverTextureWithoutResize(Texture2D newTexture)
    {
      if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.over)
        this.image.SetTextureWithoutResize(newTexture);
      this.over = newTexture;
    }

    [Gui2Editor]
    public void SetPressedTextureWithoutResize(Texture2D newTexture)
    {
      if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.pressed)
        this.image.SetTextureWithoutResize(newTexture);
      this.pressed = newTexture;
    }

    [Gui2Editor]
    public void SetInactiveTextureWithoutResize(Texture2D newTexture)
    {
      if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.inactive)
        this.image.SetTextureWithoutResize(newTexture);
      this.inactive = newTexture;
    }

    public override void MouseEnter(float2 position, float2 previousPosition)
    {
      base.MouseEnter(position, previousPosition);
      if (!this.IsActive)
        return;
      this.image.MouseEnter(position, previousPosition);
      this.label.MouseEnter(position, previousPosition);
      if (!this.isPressedManual)
        this.image.SetTextureWithoutResize(this.over);
      if (this.OverSound == null)
        return;
      this.OverSound();
    }

    public override void MouseLeave(float2 position, float2 previousPosition)
    {
      base.MouseLeave(position, previousPosition);
      if (!this.IsActive)
        return;
      this.image.MouseLeave(position, previousPosition);
      this.label.MouseLeave(position, previousPosition);
      if ((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.pressed)
      {
        GuiLabel guiLabel = this.label;
        Vector2 vector2 = guiLabel.Position - this.LabelOffset;
        guiLabel.Position = vector2;
      }
      this.image.SetTextureWithoutResize(!this.isPressedManual ? this.normal : this.pressed);
      this.image.Size = this.Size;
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      base.MouseDown(position, key);
      if (!this.IsActive || key != KeyCode.Mouse0)
        return false;
      GuiLabel guiLabel = this.label;
      Vector2 vector2 = guiLabel.Position + this.LabelOffset;
      guiLabel.Position = vector2;
      this.image.SetTextureWithoutResize(this.pressed);
      this.image.Size = this.Size;
      return true;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      base.MouseUp(position, key);
      if (!this.IsActive || key != KeyCode.Mouse0 || !((UnityEngine.Object) this.image.Texture == (UnityEngine.Object) this.pressed))
        return false;
      if (!this.isPressedManual)
        this.image.SetTextureWithoutResize(this.over);
      GuiLabel guiLabel = this.label;
      Vector2 vector2 = guiLabel.Position - this.LabelOffset;
      guiLabel.Position = vector2;
      this.image.Size = this.Size;
      if (this.Pressed != null)
        this.Pressed();
      if (this.PressedAction != null)
        this.PressedAction(this);
      if (this.PressedSound != null)
        this.PressedSound();
      return true;
    }

    [Gui2Editor]
    private void SelectLabel()
    {
      Inspector2.Selected = (SRect) this.label;
    }

    [Gui2Editor]
    private void SelectImage()
    {
      Inspector2.Selected = (SRect) this.image;
    }

    private enum ImageSelected
    {
      Normal,
      Over,
      Pressed,
      Inactive,
    }
  }
}
