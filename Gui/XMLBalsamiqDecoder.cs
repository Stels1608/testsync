﻿// Decompiled with JetBrains decompiler
// Type: Gui.XMLBalsamiqDecoder
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

namespace Gui
{
  public class XMLBalsamiqDecoder
  {
    private XmlReader m_xmlReader;
    private GuiPanel m_panel;
    private GuiElementBase m_currentElement;

    public GuiPanel Panel
    {
      get
      {
        return this.m_panel;
      }
    }

    public XMLBalsamiqDecoder(string documentName)
    {
      StreamReader streamReader = new StreamReader(documentName);
      XmlReaderSettings settings = new XmlReaderSettings();
      settings.IgnoreWhitespace = true;
      XmlTextReader xmlTextReader = new XmlTextReader((TextReader) streamReader);
      this.m_xmlReader = XmlReader.Create((XmlReader) xmlTextReader, settings);
      this.CreatePanel();
      xmlTextReader.Close();
      streamReader.Close();
    }

    private void CreatePanel()
    {
      this.m_panel = new GuiPanel();
      while (this.NextLine())
      {
        this.DecodeTags();
        this.ReadChildTags();
      }
    }

    private bool NextLine()
    {
      return this.m_xmlReader.Read();
    }

    private void ReadChildTags()
    {
      while (this.NextLine())
        this.DecodeTags();
    }

    private void DecodeTags()
    {
      if (this.m_xmlReader.Name.Length <= 0 || !this.m_xmlReader.IsStartElement() || this.m_xmlReader.IsEmptyElement)
        return;
      string name = this.m_xmlReader.Name;
      string key = name;
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (XMLBalsamiqDecoder.\u003C\u003Ef__switch\u0024mapA == null)
        {
          // ISSUE: reference to a compiler-generated field
          XMLBalsamiqDecoder.\u003C\u003Ef__switch\u0024mapA = new Dictionary<string, int>(9)
          {
            {
              "mockup",
              0
            },
            {
              "controls",
              1
            },
            {
              "control",
              2
            },
            {
              "controlProperties",
              3
            },
            {
              "controlName",
              4
            },
            {
              "groupChildrenDescriptors",
              5
            },
            {
              "customData",
              6
            },
            {
              "src",
              7
            },
            {
              "text",
              8
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (XMLBalsamiqDecoder.\u003C\u003Ef__switch\u0024mapA.TryGetValue(key, out num))
        {
          switch (num)
          {
            case 0:
              this.Mockup();
              return;
            case 1:
              this.Controls();
              return;
            case 2:
              this.Control();
              return;
            case 3:
              this.ControlProperties();
              return;
            case 4:
              this.ControlName();
              return;
            case 5:
              this.GroupChildrenDescriptors();
              return;
            case 6:
              this.CustomData();
              return;
            case 7:
              this.SRC();
              return;
            case 8:
              this.Text();
              return;
          }
        }
      }
      Debug.Log((object) name);
    }

    private void Mockup()
    {
      if (!(this.m_xmlReader["version"] != "1.0"))
        return;
      Debug.Log((object) "The Balsamiq version has changed! Make sure that the formatting is still the same.");
    }

    private void Controls()
    {
    }

    private void Control()
    {
      this.m_currentElement = (GuiElementBase) null;
      string str1 = string.Empty;
      string str2 = this.m_xmlReader["controlTypeID"];
      if (str2.Contains("com.balsamiq.mockups::"))
      {
        str2 = str2.Remove(0, "com.balsamiq.mockups::".Length);
        str1 = str1 + "Found a(n) " + str2 + "\n";
      }
      string key = str2;
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (XMLBalsamiqDecoder.\u003C\u003Ef__switch\u0024mapB == null)
        {
          // ISSUE: reference to a compiler-generated field
          XMLBalsamiqDecoder.\u003C\u003Ef__switch\u0024mapB = new Dictionary<string, int>(4)
          {
            {
              "__group__",
              0
            },
            {
              "Button",
              1
            },
            {
              "Image",
              2
            },
            {
              "TextArea",
              3
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (XMLBalsamiqDecoder.\u003C\u003Ef__switch\u0024mapB.TryGetValue(key, out num))
        {
          switch (num)
          {
            case 0:
              str1 += "The type should be a GuiPanel\n";
              this.m_currentElement = (GuiElementBase) new GuiPanel();
              break;
            case 1:
              str1 += "The type should be a GuiButton\n";
              this.m_currentElement = (GuiElementBase) new GuiButton(string.Empty);
              break;
            case 2:
              str1 += "The type should be a GuiImage\n";
              this.m_currentElement = (GuiElementBase) new GuiImage();
              break;
            case 3:
              str1 += "The type should be a GuiLabel\n";
              this.m_currentElement = (GuiElementBase) new GuiLabel();
              (this.m_currentElement as GuiLabel).AutoSize = false;
              (this.m_currentElement as GuiLabel).WordWrap = true;
              break;
          }
        }
      }
      if (this.m_currentElement == null)
        return;
      this.m_currentElement.Align = Align.UpLeft;
      string str3 = str1 + "Actual type == " + this.m_currentElement.GetType().ToString();
      this.m_currentElement.PositionX = (float) Convert.ToDouble(this.m_xmlReader["x"]);
      this.m_currentElement.PositionY = (float) Convert.ToDouble(this.m_xmlReader["y"]);
      this.m_currentElement.SizeX = (float) Convert.ToDouble(this.m_xmlReader["w"]);
      if ((double) this.m_currentElement.SizeX == -1.0)
        this.m_currentElement.SizeX = (float) Convert.ToDouble(this.m_xmlReader["measuredW"]);
      this.m_currentElement.SizeY = (float) Convert.ToDouble(this.m_xmlReader["h"]);
      if ((double) this.m_currentElement.SizeY == -1.0)
        this.m_currentElement.SizeY = (float) Convert.ToDouble(this.m_xmlReader["measuredH"]);
      Debug.Log((object) ("Type: " + this.m_currentElement.GetType().ToString()));
      if (this.m_currentElement.GetType().ToString() != "GuiPanel")
        this.m_panel.AddChild(this.m_currentElement);
      else
        this.m_panel = (GuiPanel) this.m_currentElement;
    }

    private void ControlProperties()
    {
    }

    private void ControlName()
    {
      this.m_xmlReader.Read();
      this.m_panel.Name = this.m_xmlReader.Value;
      this.m_xmlReader.Read();
    }

    private void GroupChildrenDescriptors()
    {
    }

    private void CustomData()
    {
      this.m_xmlReader.Read();
      string str1 = this.m_xmlReader.Value;
      this.m_xmlReader.Read();
      this.NextLine();
      this.m_xmlReader.Read();
      string str2 = this.m_xmlReader.Value;
      this.m_xmlReader.Read();
      if (this.m_currentElement == null)
        return;
      string key = str2;
      if (key == null)
        return;
      // ISSUE: reference to a compiler-generated field
      if (XMLBalsamiqDecoder.\u003C\u003Ef__switch\u0024mapC == null)
      {
        // ISSUE: reference to a compiler-generated field
        XMLBalsamiqDecoder.\u003C\u003Ef__switch\u0024mapC = new Dictionary<string, int>(1)
        {
          {
            "Name",
            0
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (!XMLBalsamiqDecoder.\u003C\u003Ef__switch\u0024mapC.TryGetValue(key, out num) || num != 0)
        return;
      this.m_currentElement.Name = str1;
    }

    private void SRC()
    {
      this.m_xmlReader.Read();
      string str1 = this.m_xmlReader.Value;
      this.m_xmlReader.Read();
      while (str1.StartsWith("../"))
        str1 = str1.Remove(0, "../".Length);
      string str2 = "GUI/" + str1;
      int startIndex = str2.LastIndexOf(".");
      if (startIndex > 0)
        str2 = str2.Remove(startIndex);
      if (this.m_currentElement == null)
        return;
      (this.m_currentElement as GuiImage).TexturePath = str2;
    }

    private void Text()
    {
      this.m_xmlReader.Read();
      string str1 = this.m_xmlReader.Value;
      this.m_xmlReader.Read();
      string str2 = "%$" + str1 + "%";
      if (this.m_currentElement == null)
        return;
      if (this.m_currentElement.GetType().ToString() == "GuiLabel")
      {
        (this.m_currentElement as GuiLabel).Text = str2;
      }
      else
      {
        if (!(this.m_currentElement.GetType().ToString() == "GuiButton"))
          return;
        (this.m_currentElement as GuiButton).Label.Text = str2;
      }
    }
  }
}
