﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiDialogBonusWeekend
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiDialogBonusWeekend : GuiPanel
  {
    [Gui2Editor]
    public int BackgroundIntensity = 1;
    [Gui2Editor]
    public int BackgroundPadding = 48;
    private bool showBackground = true;
    protected GuiButton closeButton = new GuiButton(string.Empty, Options.closeButtonNormal, Options.closeButtonOver, Options.closeButtonPressed);
    protected Texture2D background;

    [Gui2Editor]
    public bool ShowBackground
    {
      get
      {
        return this.showBackground;
      }
      set
      {
        this.showBackground = value;
      }
    }

    [Gui2Editor]
    public bool ShowCloseButton
    {
      get
      {
        return this.closeButton.IsRendered;
      }
      set
      {
        this.closeButton.IsRendered = value;
      }
    }

    public AnonymousDelegate OnClose
    {
      get
      {
        return this.closeButton.Pressed;
      }
      set
      {
        this.closeButton.Pressed = value;
      }
    }

    public GuiDialogBonusWeekend()
    {
      this.background = (Texture2D) ResourceLoader.Load("GUI/Gui2/background");
      this.Size = new Vector2((float) this.background.width, (float) this.background.height);
      this.closeButton.Align = Align.UpRight;
      this.closeButton.Position = new Vector2(3f, -3f);
      this.closeButton.PressedSound = new GUISoundHandler(GUISound.Instance.OnWindowClose);
      this.closeButton.SaveToLayout = false;
      this.AddChild((GuiElementBase) this.closeButton);
    }

    public void ChangeBackgroundImage(Texture2D bgImage)
    {
      this.background = bgImage;
    }

    public void UpdateSize()
    {
      this.Size = new Vector2((float) this.background.width, (float) this.background.height);
    }

    public override void Draw()
    {
      if (this.showBackground && Event.current.type == UnityEngine.EventType.Repaint)
      {
        for (int index = 0; index < this.BackgroundIntensity; ++index)
          Graphics.DrawTexture(this.Rect, (Texture) this.background, this.BackgroundPadding, this.BackgroundPadding, this.BackgroundPadding, this.BackgroundPadding);
      }
      base.Draw();
    }

    public override bool MouseDown(float2 position, KeyCode key)
    {
      if (!base.MouseDown(position, key))
        return this.Contains(position);
      return true;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      if (!base.MouseUp(position, key))
        return this.Contains(position);
      return true;
    }

    public override bool MouseMove(float2 position, float2 previousPosition)
    {
      if (!base.MouseMove(position, previousPosition))
        return this.Contains(position);
      return true;
    }

    public void EnableCloseButton(bool enabled)
    {
      this.closeButton.IsRendered = enabled;
    }
  }
}
