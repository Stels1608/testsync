﻿// Decompiled with JetBrains decompiler
// Type: Gui.DrawGuiElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

namespace Gui
{
  public class DrawGuiElement
  {
    private static Rect ms_screenRect = new Rect();
    private Stack m_viewportRects = new Stack();
    private Rect m_currentViewport = new Rect(0.0f, 0.0f, (float) Screen.width, (float) Screen.height);
    private Rect m_scaledScreenRect = new Rect();
    [SerializeField]
    private float m_scaleValue = 1f;
    private Texture2D m_stencilBackgroundTexture;

    public static Rect ScreenRect
    {
      get
      {
        DrawGuiElement.ms_screenRect.width = (float) Screen.width;
        DrawGuiElement.ms_screenRect.height = (float) Screen.height;
        return DrawGuiElement.ms_screenRect;
      }
    }

    public float ScaleValue
    {
      get
      {
        return this.m_scaleValue;
      }
      set
      {
        this.m_scaleValue = value;
        this.m_scaleValue = Mathf.Clamp(this.m_scaleValue, 0.5f, 2f);
      }
    }

    public void DrawTexture(Texture2D texture, Rect drawRect, GuiElementPadding border, Rect? texCoords = null, Color? overlayColor = null, bool dontStencil = false)
    {
      if (dontStencil)
      {
        this.PushViewport(this.CreateViewportRect(DrawGuiElement.ms_screenRect, false));
        GL.modelview = this.BuildMatrix(this.m_currentViewport);
      }
      Rect sourceRect = !texCoords.HasValue ? new Rect(0.0f, 0.0f, 1f, 1f) : texCoords.Value;
      if (Event.current.type == UnityEngine.EventType.Repaint)
      {
        if (overlayColor.HasValue)
          Graphics.DrawTexture(drawRect, (Texture) texture, sourceRect, (int) border.Left, (int) border.Right, (int) border.Top, (int) border.Bottom, overlayColor.Value * 0.5f);
        else
          Graphics.DrawTexture(drawRect, (Texture) texture, sourceRect, (int) border.Left, (int) border.Right, (int) border.Top, (int) border.Bottom);
      }
      if (!dontStencil)
        return;
      this.PopViewport();
    }

    public void DrawLabel(string text, Rect drawRect, GUIStyle style, Color? overlayColor = null)
    {
      Color color = GUI.color;
      if (overlayColor.HasValue)
        GUI.color = overlayColor.Value;
      GL.PushMatrix();
      GL.Viewport(DrawGuiElement.ScreenRect);
      Rect position = this.m_currentViewport;
      position.y = (float) Screen.height - position.height - position.y;
      GUI.BeginGroup(position);
      drawRect.x -= position.x;
      drawRect.y -= position.y;
      GUI.Label(drawRect, text, style);
      GUI.EndGroup();
      GL.Viewport(this.m_currentViewport);
      GL.PopMatrix();
      GUI.color = color;
    }

    public string DrawTextArea(string text, Rect drawRect, int maxLength, GUIStyle style, string controlName)
    {
      string str = text;
      if (text != null)
      {
        GL.PushMatrix();
        GL.Viewport(DrawGuiElement.ScreenRect);
        Rect position = this.m_currentViewport;
        position.y = (float) Screen.height - this.m_currentViewport.height - this.m_currentViewport.y;
        GUI.BeginGroup(position);
        drawRect.x -= position.x;
        drawRect.y -= position.y;
        GUI.SetNextControlName(controlName);
        str = GUI.TextArea(drawRect, text, maxLength, style);
        GUI.EndGroup();
        GL.Viewport(this.m_currentViewport);
        GL.PopMatrix();
      }
      return str;
    }

    public void BeginStencil(Rect stencilRect, bool stencilFromParent = true, bool drawStencilBackground = false)
    {
      this.PushViewport(this.CreateViewportRect(stencilRect, stencilFromParent));
      GL.modelview = this.BuildMatrix(this.m_currentViewport);
      if (!drawStencilBackground)
        return;
      Graphics.DrawTexture(stencilRect, (Texture) this.m_stencilBackgroundTexture);
    }

    public void EndStencil()
    {
      this.PopViewport();
    }

    public void StartFrame()
    {
      this.m_scaledScreenRect = new Rect(0.0f, 0.0f, DrawGuiElement.ScreenRect.width * this.m_scaleValue, DrawGuiElement.ScreenRect.height * this.m_scaleValue);
      this.m_currentViewport = DrawGuiElement.ScreenRect;
      if (this.m_viewportRects.Count > 0)
      {
        Debug.LogError((object) "You are pushing more viewports then you are popping\nCheck that all of your BeginStencil and EndStencil calls match");
        this.m_viewportRects.Clear();
        GL.Viewport(this.m_currentViewport);
      }
      if (!((Object) this.m_stencilBackgroundTexture == (Object) null))
        return;
      this.m_stencilBackgroundTexture = TextureCache.Get(new Color(0.5f, 0.5f, 0.5f, 0.25f));
    }

    private void PushViewport(Rect viewport)
    {
      GL.PushMatrix();
      this.m_viewportRects.Push((object) this.m_currentViewport);
      this.m_currentViewport = viewport;
      GL.Viewport(this.m_currentViewport);
    }

    private void PopViewport()
    {
      if (this.m_viewportRects.Count > 0)
      {
        this.m_currentViewport = (Rect) this.m_viewportRects.Pop();
        GL.Viewport(this.m_currentViewport);
      }
      else
      {
        Debug.LogError((object) "You are popping more viewports then you are pushing\nCheck that all of your BeginStencil and EndStencil calls match");
        this.m_currentViewport = DrawGuiElement.ScreenRect;
        GL.Viewport(this.m_currentViewport);
      }
      GL.PopMatrix();
    }

    private Matrix4x4 BuildMatrix(Rect viewport)
    {
      Vector3 s = new Vector3(viewport.width, viewport.height, 1f);
      s.x = this.m_scaledScreenRect.width / s.x;
      s.y = this.m_scaledScreenRect.height / s.y;
      Vector3 pos = new Vector3(-viewport.x, viewport.y - (DrawGuiElement.ScreenRect.height - viewport.height), 0.0f);
      return Matrix4x4.TRS(Vector3.zero, Quaternion.identity, s) * Matrix4x4.TRS(pos, Quaternion.identity, Vector3.one);
    }

    private Rect CreateViewportRect(Rect viewport, bool clampToLastViewport = true)
    {
      Rect rect = new Rect(viewport.x, DrawGuiElement.ScreenRect.height - viewport.y - viewport.height, viewport.width, viewport.height);
      viewport.width *= this.m_scaleValue;
      viewport.height *= this.m_scaleValue;
      if (clampToLastViewport)
        return this.ClampRect(rect, this.m_currentViewport);
      return rect;
    }

    private Rect ClampRect(Rect value, Rect clamp)
    {
      Rect rect = new Rect();
      rect.x = this.ClampFloat(value.x, clamp.x + clamp.width, clamp.x);
      rect.y = this.ClampFloat(value.y, clamp.y + clamp.height, clamp.y);
      value.width -= rect.x - value.x;
      value.height -= rect.y - value.y;
      float max1 = clamp.width - (rect.x - clamp.x);
      float max2 = clamp.height - (rect.y - clamp.y);
      rect.width = this.ClampFloat(value.width, max1, 0.0f);
      rect.height = this.ClampFloat(value.height, max2, 0.0f);
      return rect;
    }

    private float ClampFloat(float value, float max, float min)
    {
      return Mathf.Min(max, Mathf.Max(min, value));
    }
  }
}
