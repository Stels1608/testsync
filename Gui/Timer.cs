﻿// Decompiled with JetBrains decompiler
// Type: Gui.Timer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class Timer : GuiElementBase
  {
    [Gui2Editor(write = false)]
    public AnonymousDelegate Event;
    [Gui2Editor]
    public float interval;
    private float time;

    public Timer(float interval)
      : this(interval, (AnonymousDelegate) null)
    {
      this.Event = new AnonymousDelegate(this.DefaultAction);
    }

    public Timer(string name, float interval, AnonymousDelegate action)
      : this(interval, action)
    {
      this.Name = name;
    }

    public Timer(float interval, AnonymousDelegate action)
    {
      this.interval = interval;
      this.Event = action;
      this.time = Time.realtimeSinceStartup;
    }

    public override void Update()
    {
      if ((double) Time.realtimeSinceStartup - (double) this.time < (double) this.interval)
        return;
      this.time = Time.realtimeSinceStartup;
      this.Call();
    }

    public void DefaultAction()
    {
      if (!GUIManager.DebugProfile)
        ;
      (this.Parent as GuiElementBase).PeriodicUpdate();
      if (GUIManager.DebugProfile)
        ;
    }

    [Gui2Editor]
    private void Call()
    {
      if (this.Event == null)
        return;
      if (!GUIManager.DebugProfile)
        ;
      this.Event();
      if (GUIManager.DebugProfile)
        ;
    }
  }
}
