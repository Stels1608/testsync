﻿// Decompiled with JetBrains decompiler
// Type: Gui.UseInGuiEditor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace Gui
{
  [AttributeUsage(AttributeTargets.Class)]
  public class UseInGuiEditor : Attribute
  {
    private static readonly Dictionary<System.Type, List<string>> ms_layoutCache = new Dictionary<System.Type, List<string>>();
    private string m_defaultLayoutPath = string.Empty;

    public string DefaultLayoutPath
    {
      get
      {
        if (this.m_defaultLayoutPath == string.Empty)
          Log.DebugInfo((object) "The default layout file has not been set");
        return this.m_defaultLayoutPath;
      }
    }

    public UseInGuiEditor(string defaultLayoutPath)
    {
      this.m_defaultLayoutPath = defaultLayoutPath;
    }

    public static string GetDefaultLayout<T>()
    {
      return UseInGuiEditor.GetDefaultLayout(typeof (T));
    }

    public static string GetDefaultLayout(System.Type type)
    {
      List<string> stringList1;
      if (UseInGuiEditor.ms_layoutCache.TryGetValue(type, out stringList1))
      {
        if (stringList1.Count > 0)
          return stringList1[0];
        return "NoPathFound";
      }
      List<string> stringList2 = new List<string>();
      foreach (object customAttribute in type.GetCustomAttributes(typeof (UseInGuiEditor), false))
        stringList2.Add((customAttribute as UseInGuiEditor).DefaultLayoutPath);
      UseInGuiEditor.ms_layoutCache.Add(type, stringList2);
      if (stringList2.Count > 0)
        return stringList2[0];
      return "NoPathFound";
    }
  }
}
