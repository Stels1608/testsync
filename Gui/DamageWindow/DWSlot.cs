﻿// Decompiled with JetBrains decompiler
// Type: Gui.DamageWindow.DWSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.CharacterStatus.InflightShop;
using UnityEngine;

namespace Gui.DamageWindow
{
  public class DWSlot : SlotHold
  {
    private GuiButton buyButton;

    public DWSlot(int n)
      : base(n)
    {
      this.buyButton = new GuiButton("%$bgo.etc.dw_repair%");
      this.buyButton.Pressed = (AnonymousDelegate) (() => this.GetEnvironment<DWEnvironment>().baseDialog.ShowModalDialog((GuiModalDialog) new RepairDialog(this.Slot as ShipSystem)));
      this.buyButton.Size = this.recycleButton.Size;
      this.buyButton.SetTooltip("%$bgo.etc.dw_repair_item_tooltip%");
      this.AddChild((GuiElementBase) this.buyButton, Align.UpRight, new Vector2(-5f, 50f));
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      this.buyButton.IsRendered = this.Slot != null && this.Slot is ShipSystem && !(this.Slot as ShipSystem).Card.Indestructible;
      this.augmentButton.IsRendered = false;
    }

    public override void OnScroll()
    {
      base.OnScroll();
      float2 mousePositionGui = MouseSetup.MousePositionGui;
      this.buyButton.MouseLeave(mousePositionGui, mousePositionGui);
    }
  }
}
