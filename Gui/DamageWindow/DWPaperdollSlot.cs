﻿// Decompiled with JetBrains decompiler
// Type: Gui.DamageWindow.DWPaperdollSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.CharacterStatus.InflightShop;
using UnityEngine;

namespace Gui.DamageWindow
{
  public class DWPaperdollSlot : GuiPanel
  {
    private readonly uint shipSlotId;
    private readonly GuiImage openSlot;
    private readonly GuiImage closedSlot;
    private readonly GuiAtlasImage image;
    private readonly GuiHealthbar healthbar;
    private readonly GuiLabel emptySlotLetter;
    private bool textResized;

    public DWPaperdollSlot(uint shipSlotId, string openSlotTexture, string closedSlotTexture)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      DWPaperdollSlot.\u003CDWPaperdollSlot\u003Ec__AnonStorey85 slotCAnonStorey85 = new DWPaperdollSlot.\u003CDWPaperdollSlot\u003Ec__AnonStorey85();
      // ISSUE: reference to a compiler-generated field
      slotCAnonStorey85.shipSlotId = shipSlotId;
      this.image = new GuiAtlasImage();
      this.emptySlotLetter = new GuiLabel(Gui.Options.FontBGM_BT, 20);
      // ISSUE: explicit constructor call
      base.\u002Ector();
      // ISSUE: reference to a compiler-generated field
      slotCAnonStorey85.\u003C\u003Ef__this = this;
      // ISSUE: reference to a compiler-generated field
      this.shipSlotId = slotCAnonStorey85.shipSlotId;
      this.Align = Align.MiddleCenter;
      this.openSlot = new GuiImage(openSlotTexture);
      this.AddChild((GuiElementBase) this.openSlot);
      this.closedSlot = new GuiImage(closedSlotTexture);
      this.closedSlot.IsRendered = false;
      this.AddChild((GuiElementBase) this.closedSlot);
      this.AddChild((GuiElementBase) this.image, Align.UpCenter, new Vector2(1f, 6f));
      this.healthbar = new GuiHealthbar();
      this.healthbar.Size = new Vector2(this.openSlot.Size.x, 7f);
      this.AddChild((GuiElementBase) this.healthbar, new Vector2(0.0f, this.openSlot.Size.y + 1f));
      this.Size = new Vector2(this.openSlot.Size.x, (float) ((double) this.openSlot.Size.y + (double) this.healthbar.Size.y + 1.0));
      // ISSUE: reference to a compiler-generated method
      this.OnClick = new AnonymousDelegate(slotCAnonStorey85.\u003C\u003Em__BF);
      this.emptySlotLetter.Alignment = TextAnchor.MiddleCenter;
      this.emptySlotLetter.Align = Align.MiddleCenter;
      this.emptySlotLetter.PositionY = (float) (-(double) this.healthbar.Size.y + 2.0);
      this.AddChild((GuiElementBase) this.emptySlotLetter);
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      ShipSlot slot = this.GetSlot(this.shipSlotId);
      if (slot == null)
        return;
      ShipSystem shipSystem = slot.System;
      bool flag = shipSystem != null && (bool) shipSystem.IsLoaded;
      this.healthbar.IsRendered = flag && !shipSystem.Card.Indestructible;
      this.image.IsRendered = flag;
      this.emptySlotLetter.IsRendered = !flag;
      if (flag)
        this.SetTooltip(shipSystem.ItemGUICard.Name + "%br%%$bgo.etc.dw_level% " + (object) shipSystem.Card.Level + "%br%%$bgo.etc.dw_tooltip%");
      else
        this.SetTooltip(GUIShopPaperDollSlot.GetTooltipText(slot));
      this.image.GuiCard = !flag ? (GUICard) null : shipSystem.ItemGUICard;
      if (flag)
      {
        this.healthbar.Progress = shipSystem.Quality;
        this.healthbar.SetTooltip("%$bgo.etc.dw_durability%%$bgo.common.sem% " + (object) (int) shipSystem.Durability + "/" + shipSystem.Card.Durability.ToString());
      }
      else
      {
        this.emptySlotLetter.Text = PaperdollSlot.GetEmptySlotFirstLetter(slot.Card.SystemType);
        if (this.textResized || TextUtility.CalcTextSize(Tools.ParseMessage(this.emptySlotLetter.Text), this.emptySlotLetter.Font, 20).width <= 30)
          return;
        this.emptySlotLetter.FontSize = 16;
        if (TextUtility.CalcTextSize(Tools.ParseMessage(this.emptySlotLetter.Text), this.emptySlotLetter.Font, 16).width > 30)
          this.emptySlotLetter.FontSize = 12;
        this.textResized = true;
      }
    }

    private ShipSlot GetSlot(uint id)
    {
      foreach (ShipSlot slot in Game.Me.ActiveShip.Slots)
      {
        if ((int) slot.ServerID == (int) id)
          return slot;
      }
      return (ShipSlot) null;
    }
  }
}
