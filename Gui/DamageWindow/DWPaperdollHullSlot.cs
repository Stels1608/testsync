﻿// Decompiled with JetBrains decompiler
// Type: Gui.DamageWindow.DWPaperdollHullSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.DamageWindow
{
  public class DWPaperdollHullSlot : GuiPanel
  {
    private readonly GuiButton repairButton = new GuiButton("%$bgo.etc.dw_repair%");
    private const float X_OFFSET = 53f;
    private const float Y_OFFSET = 47f;
    private const float REPAIR_BTN_Y_OFFSET = -40f;
    private readonly GuiLabel text;
    private readonly GuiLabel text2;
    private readonly GuiHealthbar healthbar;

    public DWPaperdollHullSlot(string openSlotTexture)
    {
      this.text = new GuiLabel("%$bgo.etc.dw_hull%", Gui.Options.FontBGM_BT, 11);
      this.text.PositionY = -25f;
      this.text.Align = Align.MiddleCenter;
      this.AddChild((GuiElementBase) this.text);
      this.text2 = new GuiLabel(Gui.Options.FontBGM_BT, 11);
      this.text2.PositionY = -10f;
      this.text2.Align = Align.MiddleCenter;
      this.AddChild((GuiElementBase) this.text2);
      this.repairButton.Pressed = (AnonymousDelegate) (() =>
      {
        DWEnvironment environment = this.GetEnvironment<DWEnvironment>();
        environment.baseDialog.ShowModalDialog((GuiModalDialog) new RepairShipDialog(environment.me.ActiveShip));
      });
      this.repairButton.Align = Align.MiddleCenter;
      this.repairButton.PositionY -= 40f;
      this.repairButton.SetTooltip("%$bgo.etc.dw_repair_tooltip%");
      this.healthbar = new GuiHealthbar();
      this.AddChild((GuiElementBase) this.healthbar);
      this.AddChild((GuiElementBase) this.repairButton);
      this.healthbar.Position = new Vector2(0.0f, 32f);
      this.healthbar.Progress = 0.5f;
      this.healthbar.Size = new Vector2(47f, 7f);
      this.Size = new Vector2(53f, (float) (47.0 + (double) this.healthbar.Size.y + 5.0));
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      HangarShip activeShip = this.GetEnvironment<DWEnvironment>().me.ActiveShip;
      this.text2.Text = ((int) ((double) activeShip.Quality * 100.0)).ToString() + "%";
      this.healthbar.Progress = activeShip.Quality;
      this.SetTooltip(activeShip.GUICard.Name + " %$bgo.etc.dw_hull_1%");
      GuiButton guiButton = this.repairButton;
      bool needsRepair = activeShip.NeedsRepair;
      this.repairButton.HandleMouseInput = needsRepair;
      int num = needsRepair ? 1 : 0;
      guiButton.IsActive = num != 0;
    }

    public override bool Contains(float2 point)
    {
      return this.repairButton.Contains(point);
    }
  }
}
