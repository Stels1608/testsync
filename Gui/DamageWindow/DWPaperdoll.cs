﻿// Decompiled with JetBrains decompiler
// Type: Gui.DamageWindow.DWPaperdoll
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Paperdolls.PaperdollLayoutBig;
using System;
using UnityEngine;

namespace Gui.DamageWindow
{
  public class DWPaperdoll : GuiPanel
  {
    private readonly GuiImage paperdoll = new GuiImage();
    private readonly DWPaperdollHullSlot hullSlot = new DWPaperdollHullSlot("GUI/EquipBuyPanel/slot_open");
    private readonly GuiButton repairAll = new GuiButton("%$bgo.etc.dw_repair_all%");
    private uint shipUuid;

    public DWPaperdoll()
    {
      this.repairAll.OnClick = (AnonymousDelegate) (() => this.GetEnvironment<DWEnvironment>().baseDialog.ShowModalDialog((GuiModalDialog) new RepairAllDialog()));
      this.repairAll.SetTooltip("%$bgo.etc.dw_repair_all_tooltip%");
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      HangarShip activeShip = Game.Me.ActiveShip;
      if (!(bool) activeShip.IsLoaded)
        return;
      this.repairAll.IsActive = this.repairAll.handleMouseInput = activeShip.NeedsRepair || activeShip.AnySystemNeedsRepair;
      this.RefreshPaperdoll(activeShip);
    }

    private void RefreshPaperdoll(HangarShip ship)
    {
      if ((int) this.shipUuid == (int) ship.ServerID)
        return;
      this.shipUuid = (uint) ship.ServerID;
      this.EmptyChildren();
      this.paperdoll.TexturePath = "GUI/EquipBuyPanel/" + ship.Card.PaperdollLayoutBig.BlueprintTexture;
      this.AddChild((GuiElementBase) this.paperdoll, Align.MiddleCenter);
      this.AddChild((GuiElementBase) this.repairAll, Align.UpCenter, new Vector2(0.0f, 5f));
      this.AddChild((GuiElementBase) this.hullSlot, Align.UpCenter, new Vector2(0.0f, 50f));
      foreach (ShipSlot slot in ship.Slots)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        DWPaperdoll.\u003CRefreshPaperdoll\u003Ec__AnonStorey84 paperdollCAnonStorey84 = new DWPaperdoll.\u003CRefreshPaperdoll\u003Ec__AnonStorey84();
        DWPaperdollSlot dwPaperdollSlot = new DWPaperdollSlot((uint) slot.ServerID, "GUI/EquipBuyPanel/slot_open", "GUI/EquipBuyPanel/slot_closed");
        // ISSUE: reference to a compiler-generated field
        paperdollCAnonStorey84.slotId = slot.Card.SlotId;
        // ISSUE: reference to a compiler-generated method
        Vector2 v2 = Game.Me.ActiveShip.Card.PaperdollLayoutBig.GetSlotLayouts((int) Game.Me.ActiveShip.Card.Level).Find(new Predicate<SlotLayout>(paperdollCAnonStorey84.\u003C\u003Em__BD)).Position.ToV2();
        this.AddChild((GuiElementBase) dwPaperdollSlot, v2);
      }
    }
  }
}
