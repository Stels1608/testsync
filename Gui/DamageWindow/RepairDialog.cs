﻿// Decompiled with JetBrains decompiler
// Type: Gui.DamageWindow.RepairDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.DamageWindow
{
  public class RepairDialog : GuiModalDialog
  {
    private readonly ShipSystem system;
    protected RepairDialog.RepairToContainer rc;
    private readonly GuiLabel textName;
    private readonly GuiLabel textCurrentDurability;
    protected GuiButton repairButton;
    protected GuiButton repairCubitsButton;
    protected Texture2D overTexture;
    protected GuiPanel contentPanel;
    private readonly RepairDialog.CostPanel costPanel;

    public RepairDialog(ShipSystem system)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      RepairDialog.\u003CRepairDialog\u003Ec__AnonStorey86 dialogCAnonStorey86 = new RepairDialog.\u003CRepairDialog\u003Ec__AnonStorey86();
      // ISSUE: reference to a compiler-generated field
      dialogCAnonStorey86.system = system;
      this.textName = new GuiLabel(Gui.Options.FontBGM_BT, 17, true);
      // ISSUE: explicit constructor call
      base.\u002Ector();
      // ISSUE: reference to a compiler-generated field
      dialogCAnonStorey86.\u003C\u003Ef__this = this;
      // ISSUE: reference to a compiler-generated field
      this.system = dialogCAnonStorey86.system;
      this.Align = Align.MiddleCenter;
      this.Size = new Vector2(300f, 330f);
      this.textName.Alignment = TextAnchor.UpperCenter;
      this.textName.SizeX = this.SizeX - 20f;
      this.contentPanel = new GuiPanel(new Vector2(300f, 300f), new Vector2(0.0f, 0.0f));
      this.AddChild((GuiElementBase) this.textName, Align.UpCenter, new Vector2(0.0f, 20f));
      this.contentPanel.AddChild((GuiElementBase) new GuiLabel("%$bgo.etc.dw_current_durability%", Gui.Options.FontBGM_BT, 12), Align.UpCenter, new Vector2(0.0f, 78f));
      this.textCurrentDurability = new GuiLabel(Gui.Options.FontBGM_BT, 12);
      this.contentPanel.AddChild((GuiElementBase) this.textCurrentDurability, Align.UpCenter, new Vector2(0.0f, 95f));
      this.contentPanel.AddChild((GuiElementBase) new GuiLabel("%$bgo.etc.dw_repair_to%", Gui.Options.FontBGM_BT, 12), Align.UpCenter, new Vector2(0.0f, 116f));
      this.rc = new RepairDialog.RepairToContainer();
      this.rc.Value = 100;
      // ISSUE: reference to a compiler-generated method
      this.rc.OnChanged = new System.Action<int>(dialogCAnonStorey86.\u003C\u003Em__C7);
      this.contentPanel.AddChild((GuiElementBase) this.rc, Align.UpCenter, new Vector2(0.0f, 133f));
      this.costPanel = new RepairDialog.CostPanel(Game.Me.ActiveShip.Card, this.system == null);
      this.contentPanel.AddChild((GuiElementBase) this.costPanel, Align.UpCenter, new Vector2(0.0f, 165f));
      this.repairButton = new GuiButton("%$bgo.etc.dw_repair%");
      this.repairButton.Font = Gui.Options.FontBGM_BT;
      this.repairButton.FontSize = 16;
      // ISSUE: reference to a compiler-generated method
      this.repairButton.Pressed = new AnonymousDelegate(dialogCAnonStorey86.\u003C\u003Em__C8);
      this.contentPanel.AddChild((GuiElementBase) this.repairButton, Align.UpLeft, new Vector2(15f, 245f));
      this.repairButton.Size = new Vector2(120f, 60f);
      this.overTexture = this.repairButton.OverTexture;
      this.repairCubitsButton = new GuiButton("%$bgo.etc.dw_repair_with_cubits%");
      this.repairCubitsButton.Font = Gui.Options.FontBGM_BT;
      this.repairCubitsButton.FontSize = 16;
      // ISSUE: reference to a compiler-generated method
      this.repairCubitsButton.Pressed = new AnonymousDelegate(dialogCAnonStorey86.\u003C\u003Em__C9);
      this.contentPanel.AddChild((GuiElementBase) this.repairCubitsButton, Align.UpRight, new Vector2(-15f, 245f));
      this.repairCubitsButton.Size = new Vector2(120f, 60f);
      this.AddChild((GuiElementBase) this.contentPanel);
    }

    public override void Update()
    {
      base.Update();
      this.FillValues(this.textName, this.textCurrentDurability, this.rc, this.costPanel.m_textCostTitanium, this.costPanel.m_textCostCubits);
    }

    protected virtual void FillValues(GuiLabel name, GuiLabel currentDurability, RepairDialog.RepairToContainer rc, GuiLabel costTitanium, GuiLabel costCubits)
    {
      if (this.system == null)
        return;
      name.Text = this.system.ItemGUICard.Name;
      this.Size = new Vector2(300f, 300f + name.SizeY);
      this.contentPanel.Position = new Vector2(0.0f, name.SizeY - 30f);
      currentDurability.Text = this.system.Durability.ToString("0.0") + " / " + this.system.Card.Durability.ToString("0.0") + " (" + (this.system.Quality * 100f).ToString("0.") + "%)";
      rc.MinValue = (int) ((double) this.system.Quality * 100.0) + 1;
      if (rc.MinValue > 100)
        rc.MinValue = 100;
      if (this.GetEnvironment<DWEnvironment>().itemTitanium == null)
        return;
      costTitanium.Text = this.GetCost().ToString("0") + " %$bgo.common.titanium_c%";
      costCubits.Text = this.GetCubitsCost().ToString("0") + " %$bgo.common.cubits_c%";
    }

    private float GetDeltaDurability()
    {
      return this.system.Card.Durability * ((float) this.rc.Value / 100f - this.system.Quality);
    }

    private float GetCost()
    {
      return Mathf.Ceil(this.GetDeltaDurability() * StaticCards.GlobalCard.TitaniumRepairCard);
    }

    private float GetCubitsCost()
    {
      return Mathf.Ceil(this.GetDeltaDurability() * StaticCards.GlobalCard.CubitsRepairCard);
    }

    public class CostPanel : GuiPanel
    {
      private readonly Vector2[] m_linePositions = new Vector2[4];
      private readonly GuiLabel m_cost = new GuiLabel("%$bgo.etc.dw_cost%", Gui.Options.FontBGM_BT, 12);
      private readonly GuiLabel m_or = new GuiLabel("%$bgo.etc.dw_or%", Gui.Options.FontBGM_BT, 12);
      private readonly GuiLabel m_notRepairWithTitanium = new GuiLabel("%$bgo.etc.dw_not_repair_hull_with_titanium%", Color.red, Gui.Options.FontBGM_BT);
      public GuiLabel m_textCostTitanium = new GuiLabel(Gui.Options.FontBGM_BT, 12);
      public GuiLabel m_textCostCubits = new GuiLabel(Gui.Options.FontBGM_BT, 12);

      public CostPanel(ShipCard card, bool isShip)
      {
        this.m_notRepairWithTitanium.FontSize = 12;
        for (int index = 0; index < this.m_linePositions.Length; ++index)
          this.m_linePositions[index] = new Vector2(0.0f, (float) index * 16f);
        int num1 = 0;
        GuiLabel guiLabel1 = this.m_cost;
        Vector2[] vector2Array1 = this.m_linePositions;
        int index1 = num1;
        int num2 = 1;
        int num3 = index1 + num2;
        Vector2 vector2_1 = vector2Array1[index1];
        guiLabel1.Position = vector2_1;
        GuiLabel guiLabel2 = this.m_textCostCubits;
        Vector2[] vector2Array2 = this.m_linePositions;
        int index2 = num3;
        int num4 = 1;
        int num5 = index2 + num4;
        Vector2 vector2_2 = vector2Array2[index2];
        guiLabel2.Position = vector2_2;
        if (!isShip || !card.CubitOnlyRepair)
        {
          GuiLabel guiLabel3 = this.m_or;
          Vector2[] vector2Array3 = this.m_linePositions;
          int index3 = num5;
          int num6 = 1;
          int index4 = index3 + num6;
          Vector2 vector2_3 = vector2Array3[index3];
          guiLabel3.Position = vector2_3;
          this.m_textCostTitanium.Position = this.m_linePositions[index4];
        }
        else
          this.m_notRepairWithTitanium.Position = this.m_linePositions[num5 + 1];
        this.AddChild((GuiElementBase) this.m_cost, Align.UpCenter);
        this.AddChild((GuiElementBase) this.m_textCostCubits, Align.UpCenter);
        if (!isShip || !card.CubitOnlyRepair)
        {
          this.AddChild((GuiElementBase) this.m_or, Align.UpCenter);
          this.AddChild((GuiElementBase) this.m_textCostTitanium, Align.UpCenter);
        }
        else
          this.AddChild((GuiElementBase) this.m_notRepairWithTitanium, Align.UpCenter);
      }
    }

    protected class RepairToContainer : QueueHorizontal
    {
      public System.Action<int> OnChanged;
      public int MinValue;
      private readonly GuiLabel percent;
      private int value;

      public int Value
      {
        get
        {
          return this.value;
        }
        set
        {
          this.value = value;
          if (this.value < this.MinValue + 1)
            this.value = this.MinValue + 1;
          if (this.value > 100)
            this.value = 100;
          if (this.OnChanged != null)
            this.OnChanged(this.value);
          this.percent.Text = this.value.ToString("D2") + "%";
          this.ForceReposition();
        }
      }

      public RepairToContainer()
      {
        this.ContainerAutoSize = true;
        Font fontBgmBt = Gui.Options.FontBGM_BT;
        GuiLabel guiLabel1 = new GuiLabel("<<", fontBgmBt);
        guiLabel1.FontSize = 17;
        guiLabel1.OnClick = (AnonymousDelegate) (() => this.Value = 0);
        this.AddChild((GuiElementBase) guiLabel1);
        GuiLabel guiLabel2 = new GuiLabel(" < ", fontBgmBt);
        guiLabel2.FontSize = 17;
        guiLabel2.OnClick = (AnonymousDelegate) (() => --this.Value);
        this.AddChild((GuiElementBase) guiLabel2);
        this.percent = new GuiLabel(fontBgmBt);
        this.percent.FontSize = 17;
        this.AddChild((GuiElementBase) this.percent);
        GuiLabel guiLabel3 = new GuiLabel(" > ", fontBgmBt);
        guiLabel3.FontSize = 17;
        guiLabel3.OnClick = (AnonymousDelegate) (() => ++this.Value);
        this.AddChild((GuiElementBase) guiLabel3);
        GuiLabel guiLabel4 = new GuiLabel(">>", fontBgmBt);
        guiLabel4.FontSize = 17;
        guiLabel4.OnClick = (AnonymousDelegate) (() => this.Value = 100);
        this.AddChild((GuiElementBase) guiLabel4);
        this.Value = this.value;
      }
    }
  }
}
