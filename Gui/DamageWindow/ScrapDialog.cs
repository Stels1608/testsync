﻿// Decompiled with JetBrains decompiler
// Type: Gui.DamageWindow.ScrapDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.DamageWindow
{
  public class ScrapDialog : GuiModalDialog
  {
    private readonly GuiLabel textTyliumTotal;
    protected GuiButton scrapButton;

    public ScrapDialog(HangarShip system)
    {
      float num1 = 20f;
      float num2 = 10f;
      GuiLabel guiLabel1 = new GuiLabel(BsgoLocalization.Get("%$bgo.etc.dw_scrap_info%", (object) system.GUICard.Name), Gui.Options.FontBGM_BT, 20);
      this.AddChild((GuiElementBase) guiLabel1);
      guiLabel1.Align = Align.UpCenter;
      guiLabel1.PositionY = num1;
      guiLabel1.Alignment = TextAnchor.UpperCenter;
      guiLabel1.WordWrap = true;
      guiLabel1.Size = new Vector2(280f, 120f);
      float num3 = num1 + (guiLabel1.Size.y + num2);
      GuiLabel guiLabel2 = new GuiLabel(Gui.Options.FontBGM_BT, 20);
      this.AddChild((GuiElementBase) guiLabel2);
      guiLabel2.Align = Align.UpCenter;
      guiLabel2.PositionY = num3;
      float num4 = num3 + (guiLabel2.Size.y + num2);
      this.textTyliumTotal = new GuiLabel(Gui.Options.FontBGM_BT, 12);
      this.AddChild((GuiElementBase) this.textTyliumTotal);
      this.textTyliumTotal.Align = Align.UpCenter;
      this.textTyliumTotal.PositionY = num4;
      float num5 = num4 + (this.textTyliumTotal.Size.y + num2 * 2f);
      this.scrapButton = new GuiButton("%$bgo.etc.scrap_and_replace%");
      this.scrapButton.Font = Gui.Options.FontBGM_BT;
      this.scrapButton.FontSize = 16;
      this.scrapButton.Pressed = (AnonymousDelegate) (() =>
      {
        PlayerProtocol.GetProtocol().ScrapShip(Game.Me.ActiveShip.ServerID);
        this.Close();
      });
      this.AddChild((GuiElementBase) this.scrapButton);
      this.scrapButton.Align = Align.UpCenter;
      this.scrapButton.PositionY = num5;
      this.scrapButton.Size = new Vector2(240f, 40f);
      float y = num5 + (this.scrapButton.Size.y + num2);
      this.Align = Align.MiddleCenter;
      this.Size = new Vector2(y + 60f, y);
    }

    public override void Update()
    {
      base.Update();
      this.textTyliumTotal.Text = Game.Me.Hold.Tylium.ToString() + " %$bgo.etc.tylium_available%";
    }
  }
}
