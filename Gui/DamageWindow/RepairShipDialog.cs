﻿// Decompiled with JetBrains decompiler
// Type: Gui.DamageWindow.RepairShipDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.DamageWindow
{
  public class RepairShipDialog : RepairDialog
  {
    private HangarShip ship;

    public RepairShipDialog(HangarShip ship)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      RepairShipDialog.\u003CRepairShipDialog\u003Ec__AnonStorey87 dialogCAnonStorey87 = new RepairShipDialog.\u003CRepairShipDialog\u003Ec__AnonStorey87();
      // ISSUE: reference to a compiler-generated field
      dialogCAnonStorey87.ship = ship;
      // ISSUE: explicit constructor call
      base.\u002Ector((ShipSystem) null);
      // ISSUE: reference to a compiler-generated field
      dialogCAnonStorey87.\u003C\u003Ef__this = this;
      // ISSUE: reference to a compiler-generated field
      this.ship = dialogCAnonStorey87.ship;
      this.repairButton.Align = Align.UpLeft;
      this.repairButton.Position = new Vector2(15f, this.repairButton.Position.y);
      // ISSUE: reference to a compiler-generated field
      if (dialogCAnonStorey87.ship.Card.CubitOnlyRepair)
      {
        this.repairButton.OverTexture = this.repairButton.NormalTexture;
        this.repairButton.Label.NormalColor = Color.gray;
        this.repairButton.Label.OverColor = Color.gray;
        this.repairButton.Pressed = (AnonymousDelegate) null;
      }
      else
      {
        this.repairButton.OverTexture = this.overTexture;
        this.repairButton.Label.NormalColor = Color.white;
        this.repairButton.Label.OverColor = Color.white;
        // ISSUE: reference to a compiler-generated method
        this.repairButton.Pressed = new AnonymousDelegate(dialogCAnonStorey87.\u003C\u003Em__CE);
      }
      // ISSUE: reference to a compiler-generated method
      this.repairCubitsButton.Pressed = new AnonymousDelegate(dialogCAnonStorey87.\u003C\u003Em__CF);
    }

    protected override void FillValues(GuiLabel name, GuiLabel currentDurability, RepairDialog.RepairToContainer rc, GuiLabel costTitanium, GuiLabel costCubits)
    {
      if (this.ship == null)
        return;
      name.Text = this.ship.GUICard.Name;
      currentDurability.Text = this.ship.Durability.ToString("0.0") + " / " + this.ship.Card.Durability.ToString("0.0") + " (" + (this.ship.Quality * 100f).ToString("0.") + "%)";
      rc.MinValue = (int) ((double) this.ship.Quality * 100.0) + 1;
      if (rc.MinValue > 100)
        rc.MinValue = 100;
      if (this.GetEnvironment<DWEnvironment>().itemTitanium == null)
        return;
      costTitanium.Text = this.GetCost().ToString("0") + " %$bgo.common.titanium_c%";
      costCubits.Text = this.GetCubitsCost().ToString("0") + " %$bgo.common.cubits_c%";
    }

    [Gui2Editor]
    private float GetDeltaDurability()
    {
      return this.ship.Card.Durability * ((float) this.rc.Value / 100f - this.ship.Quality);
    }

    [Gui2Editor]
    private float GetCost()
    {
      return Mathf.Ceil(this.GetDeltaDurability() * StaticCards.GlobalCard.TitaniumRepairCard);
    }

    [Gui2Editor]
    private float GetCubitsCost()
    {
      return Mathf.Ceil(this.GetDeltaDurability() * StaticCards.GlobalCard.CubitsRepairCard);
    }
  }
}
