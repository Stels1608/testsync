﻿// Decompiled with JetBrains decompiler
// Type: Gui.DamageWindow.DWWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Common;
using System;
using UnityEngine;

namespace Gui.DamageWindow
{
  public class DWWindow : GuiPanel
  {
    private readonly string hold = "%$bgo.etc.dw_hold%";
    private readonly string locker = "%$bgo.etc.dw_locker%";
    private DWMainWindow mainWindow;
    private GuiTabsSimple tabs;
    private MoneyPlayer money;
    private GetCubits getCubits;
    private Shop shop;
    private AnonymousDelegate handlerClose;

    public AnonymousDelegate HandlerClose
    {
      set
      {
        this.handlerClose = value;
      }
    }

    public override bool IsBigWindow
    {
      get
      {
        return true;
      }
    }

    public override bool IsRendered
    {
      get
      {
        return base.IsRendered;
      }
      set
      {
        base.IsRendered = value;
        if (!value)
          return;
        this.tabs.SelectButton(this.tabs.GetSelected());
      }
    }

    public DWWindow()
    {
      this.MouseTransparent = true;
      this.enabled = false;
      this.SetEnvironment((object) new DWEnvironment()
      {
        me = Game.Me,
        baseDialog = this
      });
      this.Align = Align.MiddleCenter;
      this.IsRendered = false;
      this.tabs = new GuiTabsSimple();
      this.tabs.AddTab(this.hold, (AnonymousDelegate) (() =>
      {
        this.mainWindow.listHold.IsRendered = true;
        this.mainWindow.listLocker.IsRendered = false;
      }));
      this.tabs.AddTab(this.locker, (AnonymousDelegate) (() =>
      {
        this.mainWindow.listHold.IsRendered = false;
        this.mainWindow.listLocker.IsRendered = true;
      }));
      this.AddChild((GuiElementBase) this.tabs, new Vector2(48f, 0.0f));
      this.mainWindow = new DWMainWindow();
      this.AddChild((GuiElementBase) this.mainWindow, new Vector2(0.0f, this.tabs.SizeY + 10f));
      this.mainWindow.OnClose = (AnonymousDelegate) (() =>
      {
        if (this.handlerClose != null)
          this.handlerClose();
        this.IsRendered = false;
      });
      this.AddChild((GuiElementBase) new Gui.Timer(0.5f));
      this.getCubits = new GetCubits();
      this.AddChild((GuiElementBase) this.getCubits, Align.UpRight, new Vector2(-40f, -47f));
      this.money = new MoneyPlayer();
      this.AddChild((GuiElementBase) this.money, Align.UpRight, new Vector2(-160f, 15f));
      this.Size = new Vector2(this.mainWindow.SizeX, (float) ((double) this.mainWindow.SizeY + (double) this.tabs.SizeY + 10.0));
      this.tabs.GetButton(this.hold).SetTooltip("%$bgo.etc.dw_hold_tooltip%");
      this.tabs.GetButton(this.locker).SetTooltip("%$bgo.etc.dw_locker_tooltip%");
      this.tabs.SelectButtonWithText(this.hold);
    }

    public override void OnShow()
    {
      base.OnShow();
      FeedbackProtocol.GetProtocol().ReportUiElementShown(FeedbackProtocol.UiElementId.RepairWindow);
    }

    public override void OnHide()
    {
      base.OnHide();
      FeedbackProtocol.GetProtocol().ReportUiElementHidden(FeedbackProtocol.UiElementId.RepairWindow);
    }

    public override void Update()
    {
      base.Update();
      DWEnvironment environment = this.GetEnvironment<DWEnvironment>();
      if (environment.itemTitanium != null)
        return;
      if (this.shop == null)
      {
        this.shop = RoomLevel.GetLevel().Shop;
        if (this.shop == null)
          this.shop = SpaceLevel.GetLevel().Shop;
      }
      ItemList itemList = this.shop.Filter((Predicate<ShipItem>) (si => (int) si.CardGUID == 207047790));
      if (itemList.Count <= 0)
        return;
      environment.itemTitanium = itemList[0] as ItemCountable;
      this.shop = (Shop) null;
    }

    public override bool Contains(float2 point)
    {
      if (!base.Contains(point) && !this.getCubits.Contains(point))
        return this.money.Contains(point);
      return true;
    }
  }
}
