﻿// Decompiled with JetBrains decompiler
// Type: Gui.DamageWindow.RepairAllDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui.DamageWindow
{
  public class RepairAllDialog : GuiModalDialog
  {
    private readonly GuiLabel textName = new GuiLabel(Gui.Options.FontBGM_BT, 17, true);
    protected HangarShip ship;
    private readonly GuiLabel textCurrentDurability;
    protected readonly GuiButton repairButton;
    protected readonly GuiButton repairCubitsButton;
    private float totalCost;
    private readonly RepairAllDialog.CostPanel costPanel;

    public RepairAllDialog()
    {
      this.ship = Game.Me.ActiveShip;
      this.Align = Align.MiddleCenter;
      this.Size = new Vector2(300f, 300f);
      this.textName.Alignment = TextAnchor.UpperCenter;
      this.textName.SizeX = this.SizeX - 20f;
      this.AddChild((GuiElementBase) this.textName, Align.UpCenter, new Vector2(0.0f, 20f));
      this.AddChild((GuiElementBase) new GuiLabel("%$bgo.etc.dw_current_durability%", Gui.Options.FontBGM_BT, 12), Align.UpCenter, new Vector2(0.0f, 58f));
      this.textCurrentDurability = new GuiLabel(Gui.Options.FontBGM_BT, 12);
      this.AddChild((GuiElementBase) this.textCurrentDurability, Align.UpCenter, new Vector2(0.0f, 75f));
      this.costPanel = new RepairAllDialog.CostPanel(Game.Me.ActiveShip.Card);
      this.AddChild((GuiElementBase) this.costPanel, Align.UpCenter, new Vector2(0.0f, 129f));
      this.repairButton = new GuiButton("%$bgo.etc.dw_repair%");
      this.repairButton.Font = Gui.Options.FontBGM_BT;
      this.repairButton.Label.FontSize = 16;
      this.repairButton.Pressed = (AnonymousDelegate) (() =>
      {
        this.totalCost = 0.0f;
        if (!this.ship.Card.CubitOnlyRepair)
          this.totalCost = this.ship.Card.Durability * (1f - this.ship.Quality) * StaticCards.GlobalCard.TitaniumRepairCard;
        foreach (ShipSlot slot in this.ship.Slots)
        {
          if (slot.System != null)
            this.totalCost += this.GetCost(slot.System);
        }
        if ((double) Game.Me.Hold.Titanium >= (double) this.totalCost)
        {
          PlayerProtocol.GetProtocol().RepairAll(this.ship.ServerID, false);
          this.Close();
        }
        else
          new InfoBox(BsgoLocalization.Get("%$bgo.etc.dw_no_titanium%", (object) this.totalCost.ToString("0"))).Show((GuiPanel) this);
      });
      this.AddChild((GuiElementBase) this.repairButton, Align.UpLeft, new Vector2(15f, 225f));
      this.repairButton.Size = new Vector2(120f, 40f);
      this.repairCubitsButton = new GuiButton("%$bgo.etc.dw_repair_with_cubits%");
      this.repairCubitsButton.Font = Gui.Options.FontBGM_BT;
      this.repairCubitsButton.Label.FontSize = 16;
      this.repairCubitsButton.Pressed = (AnonymousDelegate) (() =>
      {
        this.totalCost = this.ship.Card.Durability * (1f - this.ship.Quality) * StaticCards.GlobalCard.CubitsRepairCard;
        foreach (ShipSlot slot in this.ship.Slots)
        {
          if (slot.System != null)
            this.totalCost += this.GetCubitsCost(slot.System);
        }
        if ((double) Game.Me.Hold.Cubits > (double) this.totalCost)
        {
          PlayerProtocol.GetProtocol().RepairAll(this.ship.ServerID, true);
          this.Close();
        }
        else
          new InfoBox(BsgoLocalization.Get("%$bgo.etc.dw_no_cubits%", (object) this.totalCost.ToString("0"))).Show((GuiPanel) this);
      });
      this.AddChild((GuiElementBase) this.repairCubitsButton, Align.UpRight, new Vector2(-15f, 225f));
      this.repairCubitsButton.Size = new Vector2(120f, 40f);
    }

    public override void Update()
    {
      base.Update();
      this.FillValues(this.textName, this.textCurrentDurability, this.costPanel.m_textCostTitanium, this.costPanel.m_textCostCubits);
    }

    public override bool Contains(float2 point)
    {
      return this.Rect.Contains(point.ToV2());
    }

    protected virtual void FillValues(GuiLabel name, GuiLabel currentDurability, GuiLabel costTitanium, GuiLabel costCubits)
    {
      this.ship = Game.Me.ActiveShip;
      if (this.ship == null)
        return;
      name.Text = this.ship.GUICard.Name;
      float durability = this.ship.Durability;
      float num1 = this.ship.Card.Durability;
      float num2 = 0.0f;
      if (!Game.Me.ActiveShip.Card.CubitOnlyRepair)
        num2 = this.ship.Card.Durability * (1f - this.ship.Quality) * StaticCards.GlobalCard.TitaniumRepairCard;
      float num3 = this.ship.Card.Durability * (1f - this.ship.Quality) * StaticCards.GlobalCard.CubitsRepairCard;
      foreach (ShipSlot slot in this.ship.Slots)
      {
        if (slot.System != null)
        {
          num2 += this.GetCost(slot.System);
          num3 += this.GetCubitsCost(slot.System);
          durability += slot.System.Durability;
          num1 += slot.System.Card.Durability;
        }
      }
      float num4 = durability / num1;
      currentDurability.Text = durability.ToString("0.0") + " / " + num1.ToString("0.0") + " (" + (num4 * 100f).ToString("0.") + "%)";
      costTitanium.Text = num2.ToString("0") + " %$bgo.common.titanium_c%";
      if (Game.Me.ActiveShip.Card.CubitOnlyRepair)
        costTitanium.Text += " %$bgo.etc.dw_excludes_hull_repair%";
      costCubits.Text = num3.ToString("0") + " %$bgo.common.cubits_c%";
    }

    private float GetDeltaDurability(ShipSystem system)
    {
      return system.Card.Durability * (1f - system.Quality);
    }

    private float GetCost(ShipSystem system)
    {
      return Mathf.Ceil(this.GetDeltaDurability(system) * StaticCards.GlobalCard.TitaniumRepairCard);
    }

    private float GetCubitsCost(ShipSystem system)
    {
      return Mathf.Ceil(this.GetDeltaDurability(system) * StaticCards.GlobalCard.CubitsRepairCard);
    }

    public class CostPanel : GuiPanel
    {
      private readonly Vector2[] m_linePositions = new Vector2[5];
      private readonly GuiLabel m_cost = new GuiLabel("%$bgo.etc.dw_cost%", Gui.Options.FontBGM_BT, 12);
      private readonly GuiLabel m_or = new GuiLabel("%$bgo.etc.dw_or%", Gui.Options.FontBGM_BT, 12);
      private readonly GuiLabel m_notRepairWithTitanium = new GuiLabel("%$bgo.etc.dw_not_repair_hull_with_titanium%", Color.red, Gui.Options.FontBGM_BT);
      public GuiLabel m_textCostTitanium = new GuiLabel(Gui.Options.FontBGM_BT, 12);
      public GuiLabel m_textCostCubits = new GuiLabel(Gui.Options.FontBGM_BT, 12);

      public CostPanel(ShipCard card)
      {
        this.m_notRepairWithTitanium.FontSize = 12;
        for (int index = 0; index < this.m_linePositions.Length; ++index)
          this.m_linePositions[index] = new Vector2(0.0f, (float) index * 16f);
        int num1 = 0;
        GuiLabel guiLabel1 = this.m_cost;
        Vector2[] vector2Array1 = this.m_linePositions;
        int index1 = num1;
        int num2 = 1;
        int num3 = index1 + num2;
        Vector2 vector2_1 = vector2Array1[index1];
        guiLabel1.Position = vector2_1;
        GuiLabel guiLabel2 = this.m_textCostCubits;
        Vector2[] vector2Array2 = this.m_linePositions;
        int index2 = num3;
        int num4 = 1;
        int num5 = index2 + num4;
        Vector2 vector2_2 = vector2Array2[index2];
        guiLabel2.Position = vector2_2;
        GuiLabel guiLabel3 = this.m_or;
        Vector2[] vector2Array3 = this.m_linePositions;
        int index3 = num5;
        int num6 = 1;
        int num7 = index3 + num6;
        Vector2 vector2_3 = vector2Array3[index3];
        guiLabel3.Position = vector2_3;
        GuiLabel guiLabel4 = this.m_textCostTitanium;
        Vector2[] vector2Array4 = this.m_linePositions;
        int index4 = num7;
        int num8 = 1;
        int index5 = index4 + num8;
        Vector2 vector2_4 = vector2Array4[index4];
        guiLabel4.Position = vector2_4;
        if (card.CubitOnlyRepair)
          this.m_notRepairWithTitanium.Position = this.m_linePositions[index5];
        this.AddChild((GuiElementBase) this.m_cost, Align.UpCenter);
        this.AddChild((GuiElementBase) this.m_textCostCubits, Align.UpCenter);
        this.AddChild((GuiElementBase) this.m_or, Align.UpCenter);
        this.AddChild((GuiElementBase) this.m_textCostTitanium, Align.UpCenter);
        if (!card.CubitOnlyRepair)
          return;
        this.AddChild((GuiElementBase) this.m_notRepairWithTitanium, Align.UpCenter);
      }
    }
  }
}
