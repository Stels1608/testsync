﻿// Decompiled with JetBrains decompiler
// Type: Gui.DamageWindow.DWMainWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.CharacterStatus.InflightShop;
using UnityEngine;

namespace Gui.DamageWindow
{
  public class DWMainWindow : GuiDialog
  {
    public readonly ListHold listHold = new ListHold((GetterDelegate<SlotBase, int>) (n => (SlotBase) new DWSlot(n)));
    public readonly ListLocker listLocker = new ListLocker((GetterDelegate<SlotBase, int>) (n => (SlotBase) new DWSlot(n)));
    public readonly DWPaperdoll paperdoll = new DWPaperdoll();

    public DWMainWindow()
    {
      this.AddChild((GuiElementBase) this.listHold);
      this.AddChild((GuiElementBase) this.listLocker);
      this.AddChild((GuiElementBase) this.paperdoll);
    }

    public override void Reposition()
    {
      this.Size = new Vector2(925f, 445f);
      this.m_backgroundImage.Size = this.Size;
      float num = 375f;
      this.listHold.Position = new Vector2(3f, 3f);
      this.listHold.Size = new Vector2(num - 3f, this.Size.y - 6f);
      this.listLocker.Position = this.listHold.Position;
      this.listLocker.Size = this.listHold.Size;
      this.paperdoll.Position = new Vector2(this.listHold.Size.x, this.listHold.Position.y);
      this.paperdoll.Size = new Vector2((float) ((double) this.Size.x - (double) this.listHold.Position.x - (double) this.listHold.Size.x - 3.0), this.Size.y - 6f);
      base.Reposition();
    }
  }
}
