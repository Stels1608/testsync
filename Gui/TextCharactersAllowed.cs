﻿// Decompiled with JetBrains decompiler
// Type: Gui.TextCharactersAllowed
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

namespace Gui
{
  [Flags]
  public enum TextCharactersAllowed : uint
  {
    LowerCase = 1,
    UpperCase = 2,
    ConcurrentSpaces = 4,
    Numbers = 8,
    Letters = 16,
    Symbols = 32,
    Anything = 4294967295,
  }
}
