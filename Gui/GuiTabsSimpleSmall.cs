﻿// Decompiled with JetBrains decompiler
// Type: Gui.GuiTabsSimpleSmall
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Gui
{
  public class GuiTabsSimpleSmall : GuiTabsSimple
  {
    public GuiTabsSimpleSmall()
    {
      this.lefttab_normal = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/Small/lefttab_normal");
      this.lefttab_over = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/Small/lefttab_over");
      this.lefttab_pressed = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/Small/lefttab_pressed");
      this.middletab_normal = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/Small/middletab_normal");
      this.middletab_over = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/Small/middletab_over");
      this.middletab_pressed = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/Small/middletab_pressed");
      this.righttab_normal = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/Small/righttab_normal");
      this.righttab_over = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/Small/righttab_over");
      this.righttab_pressed = ResourceLoader.Load<Texture2D>("GUI/Common/Tabs/Small/righttab_pressed");
    }
  }
}
