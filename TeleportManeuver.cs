﻿// Decompiled with JetBrains decompiler
// Type: TeleportManeuver
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TeleportManeuver : Maneuver
{
  protected Vector3 position;

  public override string ToString()
  {
    return string.Format("TeleportManeuver: position={0}", (object) this.position);
  }

  public override MovementFrame NextFrame(Tick tick, MovementFrame prevFrame)
  {
    return new MovementFrame(this.position, prevFrame.euler3, Vector3.zero, Vector3.zero, Euler3.zero);
  }

  public override void Read(BgoProtocolReader pr)
  {
    base.Read(pr);
    this.startTick = pr.ReadTick();
    this.position = pr.ReadVector3();
    this.options.Read(pr);
  }
}
