﻿// Decompiled with JetBrains decompiler
// Type: PlayerProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProtocol : BgoProtocol
{
  public PlayerProtocol()
    : base(BgoProtocol.ProtocolID.Player)
  {
  }

  public static PlayerProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Player) as PlayerProtocol;
  }

  public void Create(AvatarDescription desc)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 14);
    bw.Write((IProtocolWrite) desc);
    this.SendMessage(bw);
  }

  public void ChangeAvatar(AvatarDescription desc)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 43);
    bw.Write((IProtocolWrite) desc);
    this.SendMessage(bw);
  }

  public void SwitchFaction()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 41);
    this.SendMessage(bw);
  }

  public void ChangeName(string name)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 42);
    bw.Write(name);
    this.SendMessage(bw);
  }

  public void RequestCharacterServices()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 40);
    this.SendMessage(bw);
  }

  public void RequestResourceHardcap(uint guid)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 44);
    bw.Write(guid);
    this.SendMessage(bw);
  }

  public void RequestMeritCap()
  {
    this.RequestResourceHardcap(130920111U);
  }

  public void CheckNameAvailability(string name)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 28);
    bw.Write(name);
    this.SendMessage(bw);
  }

  public void ChooseName(string name)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 32);
    bw.Write(name);
    this.SendMessage(bw);
  }

  public void SelectFaction(Faction faction)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 15);
    bw.Write((byte) faction);
    this.SendMessage(bw);
  }

  public void AddShip(uint GUID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 7);
    bw.Write(GUID);
    this.SendMessage(bw);
  }

  public void SelectShip(ushort shipID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 9);
    bw.Write(shipID);
    this.SendMessage(bw);
  }

  public void RemoveShip(ushort shipID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 8);
    bw.Write(shipID);
    this.SendMessage(bw);
  }

  public void UpgradeShip(ushort shipID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 10);
    bw.Write(shipID);
    this.SendMessage(bw);
  }

  public void SetShipName(ushort shipID, string name)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 17);
    bw.Write(shipID);
    bw.Write(name);
    this.SendMessage(bw);
  }

  public void ScrapShip(ushort shipID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 13);
    bw.Write(shipID);
    this.SendMessage(bw);
  }

  public void BuySkill(ushort skillID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 2);
    bw.Write(skillID);
    this.SendMessage(bw);
  }

  public void InstantSkillBuy(ushort skillID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 35);
    bw.Write(skillID);
    this.SendMessage(bw);
  }

  public void ReduceSkillLearnTime(ushort skillID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 36);
    bw.Write(skillID);
    this.SendMessage(bw);
  }

  public void SubmitMission(ushort missionId)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 37);
    bw.Write(missionId);
    this.SendMessage(bw);
  }

  public void SelectTitle(ushort dutyID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 3);
    bw.Write(dutyID);
    this.SendMessage(bw);
  }

  public void DeselectTitle()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 45);
    this.SendMessage(bw);
  }

  public void MoveItem(IContainerID from, IContainerID to, ushort itemID, uint count)
  {
    this.MoveItem(from, to, itemID, count, false);
  }

  public void MoveItem(IContainerID from, IContainerID to, ushort itemID, uint count, bool equip)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    bw.Write((IProtocolWrite) from);
    bw.Write(itemID);
    bw.Write(count);
    bw.Write((IProtocolWrite) to);
    bw.Write(equip);
    this.SendMessage(bw);
  }

  public void MoveAll(IContainerID from, IContainerID to)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 22);
    bw.Write((IProtocolWrite) from);
    bw.Write((IProtocolWrite) to);
    this.SendMessage(bw);
  }

  public void SelectConsumable(ushort shipID, uint consumableGUID, ushort slotID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 5);
    bw.Write(shipID);
    bw.Write(consumableGUID);
    bw.Write(slotID);
    this.SendMessage(bw);
  }

  public void UnbindSticker(ushort shipID, ushort spotID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 6);
    bw.Write(shipID);
    bw.Write(spotID);
    this.SendMessage(bw);
  }

  public void BindSticker(ushort shipID, StickerBinding sticker)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 4);
    bw.Write(shipID);
    bw.Write((IProtocolWrite) sticker);
    this.SendMessage(bw);
  }

  public void RepairSystem(ShipSystem system, float repairValue, bool useCubits)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 11);
    bw.Write((IProtocolWrite) system.Container.ContainerID);
    bw.Write(system.ServerID);
    bw.Write(repairValue);
    bw.Write(useCubits);
    this.SendMessage(bw);
  }

  public void UpgradeSystem(ShipSystem system, byte newLevel)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 16);
    bw.Write((IProtocolWrite) system.Container.ContainerID);
    bw.Write(system.ServerID);
    bw.Write(newLevel);
    this.SendMessage(bw);
  }

  public void UpgradeSystemByPack(ShipSystem system, uint packCount)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 21);
    bw.Write((IProtocolWrite) system.Container.ContainerID);
    bw.Write(system.ServerID);
    bw.Write(packCount);
    this.SendMessage(bw);
  }

  public void UseAugment(ItemCountable augment)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 18);
    bw.Write((IProtocolWrite) augment.Container.ContainerID);
    bw.Write(augment.ServerID);
    this.SendMessage(bw);
  }

  public void AugmentMassActivation(ItemCountable augment, uint iterations)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 38);
    bw.Write((IProtocolWrite) augment.Container.ContainerID);
    bw.Write(augment.ServerID);
    bw.Write(iterations);
    this.SendMessage(bw);
  }

  public void RepairShip(ushort shipID, float repairValue, bool useCubits)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 12);
    bw.Write(shipID);
    bw.Write(repairValue);
    bw.Write(useCubits);
    this.SendMessage(bw);
  }

  public void RepairAll(ushort shipID, bool useCubits)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 26);
    bw.Write(shipID);
    bw.Write(useCubits);
    this.SendMessage(bw);
  }

  public void ChooseDailyBonus(uint bonus)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 20);
    bw.Write(bonus);
    this.SendMessage(bw);
  }

  public void ReadMail(ushort MailID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 23);
    bw.Write(MailID);
    this.SendMessage(bw);
  }

  public void RemoveMail(ushort MailID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 24);
    bw.Write(MailID);
    this.SendMessage(bw);
  }

  public void MailAction(ushort MailID, string Action)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 25);
    bw.Write(MailID);
    bw.Write(Action);
    this.SendMessage(bw);
  }

  public void SetPopupToSeen(uint popupID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 30);
    bw.Write(popupID);
    this.SendMessage(bw);
  }

  public void SendDradisData()
  {
    float detectionVisualRadius = Game.Me.Stats.DetectionVisualRadius;
    float detectionInnerRadius = Game.Me.Stats.DetectionInnerRadius;
    float detectionOuterRadius = Game.Me.Stats.DetectionOuterRadius;
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 39);
    bw.Write(detectionVisualRadius);
    bw.Write(detectionInnerRadius);
    bw.Write(detectionOuterRadius);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    PlayerProtocol.Reply reply = (PlayerProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    try
    {
      switch (reply)
      {
        case PlayerProtocol.Reply.Reset:
          Game.Me.Reset();
          break;
        case PlayerProtocol.Reply.Skills:
          bool flag = br.ReadBoolean();
          float trainingEndTime = 0.0f;
          PlayerSkill learning = (PlayerSkill) null;
          if (flag)
          {
            trainingEndTime = br.ReadSingle() + Time.time;
            learning = br.ReadDesc<PlayerSkill>();
          }
          List<PlayerSkill> playerSkillList = br.ReadDescList<PlayerSkill>();
          if (flag)
            playerSkillList.Add(learning);
          Game.Me.SkillBook._SetSkills(learning, trainingEndTime, (IEnumerable<PlayerSkill>) playerSkillList);
          break;
        case PlayerProtocol.Reply.Missions:
          Game.Me.MissionBook._AddItems((IEnumerable<Mission>) br.ReadDescList<Mission>());
          break;
        case PlayerProtocol.Reply.RemoveMissions:
          Game.Me.MissionBook._RemoveItems((IEnumerable<ushort>) br.ReadUInt16List());
          break;
        case PlayerProtocol.Reply.Duties:
          Game.Me.DutyBook._AddItems((IEnumerable<Duty>) br.ReadDescList<Duty>());
          break;
        case PlayerProtocol.Reply.HoldItems:
          Game.Me.Hold._AddItems((SmartList<ShipItem>) ItemFactory.ReadItemList(br));
          break;
        case PlayerProtocol.Reply.RemoveHoldItems:
          Game.Me.Hold._RemoveItems((IEnumerable<ushort>) br.ReadUInt16List());
          break;
        case PlayerProtocol.Reply.LockerItems:
          Game.Me.Locker._AddItems((SmartList<ShipItem>) ItemFactory.ReadItemList(br));
          break;
        case PlayerProtocol.Reply.RemoveLockerItems:
          Game.Me.Locker._RemoveItems((IEnumerable<ushort>) br.ReadUInt16List());
          break;
        case PlayerProtocol.Reply.ShipInfo:
          Game.Me.Hangar[br.ReadUInt16()]._SetDurability(br.ReadSingle());
          FacadeFactory.GetInstance().SendMessage(Message.ShipListChanged);
          break;
        case PlayerProtocol.Reply.Slots:
          ushort index1 = br.ReadUInt16();
          ushort num1 = br.ReadUInt16();
          HangarShip ship = Game.Me.Hangar[index1];
          for (uint index2 = 0; index2 < (uint) num1; ++index2)
          {
            ushort num2 = br.ReadUInt16();
            ShipSlot newSlot = ship.GetSlot(num2);
            if (newSlot == null)
            {
              newSlot = new ShipSlot(ship, num2);
              ship._AddSlot(num2, newSlot);
            }
            newSlot.Read(br);
          }
          ship.AllSlotsCreated();
          ship.UpdateSlotListeners();
          break;
        case PlayerProtocol.Reply.Stickers:
          Game.Me.Hangar[br.ReadUInt16()]._AddStickers((IEnumerable<StickerBinding>) br.ReadDescArray<StickerBinding>());
          break;
        case PlayerProtocol.Reply.RemoveStickers:
          Game.Me.Hangar[br.ReadUInt16()]._RemoveStickers((IEnumerable<ushort>) br.ReadUInt16List());
          break;
        case PlayerProtocol.Reply.AddShip:
          Game.Me.Hangar._AddShip(new HangarShip(br.ReadUInt16(), br.ReadGUID()));
          FacadeFactory.GetInstance().SendMessage(Message.ShipListChanged);
          break;
        case PlayerProtocol.Reply.RemoveShip:
          Game.Me.Hangar._RemoveShip(br.ReadUInt16());
          FacadeFactory.GetInstance().SendMessage(Message.ShipListChanged);
          break;
        case PlayerProtocol.Reply.ActiveShip:
          Game.Me.Hangar._SetActiveShip(br.ReadUInt16());
          FacadeFactory.GetInstance().SendMessage(Message.ShipListChanged);
          break;
        case PlayerProtocol.Reply.ShipName:
          Game.Me.Hangar[br.ReadUInt16()].Name = br.ReadString();
          break;
        case PlayerProtocol.Reply.NameAvailable:
          FacadeFactory.GetInstance().SendMessage(Message.NameAccepted);
          break;
        case PlayerProtocol.Reply.NameNotAvailable:
          FacadeFactory.GetInstance().SendMessage(Message.NameRejected);
          break;
        case PlayerProtocol.Reply.ID:
          Game.Me.ServerID = br.ReadUInt32();
          try
          {
            PlayerPrefs.SetInt("player_id", (int) Game.Me.ServerID);
            Log.Add("Game.Me.ServerID =  " + Game.Me.ServerID.ToString());
            break;
          }
          catch (PlayerPrefsException ex)
          {
            break;
          }
        case PlayerProtocol.Reply.Name:
          Game.Me.Name = br.ReadString();
          try
          {
            PlayerPrefs.SetString("player_name", Game.Me.Name);
            break;
          }
          catch (PlayerPrefsException ex)
          {
            break;
          }
        case PlayerProtocol.Reply.Faction:
          Faction faction = (Faction) br.ReadByte();
          Game.Me.Faction = faction;
          if (Game.Me.AvatarDesc.IsEmpty)
          {
            Debug.LogWarning((object) ("AvatarDesc is empty, falling back to default for " + (object) Game.Me.Faction));
            Game.Me.AvatarDesc = AvatarSelector.GetDefault(Game.Me.Faction);
          }
          FacadeFactory.GetInstance().SendMessage(Message.PlayerFactionReply, (object) faction);
          break;
        case PlayerProtocol.Reply.Experience:
          Game.Me.Experience = br.ReadUInt32();
          break;
        case PlayerProtocol.Reply.SpentExperience:
          Game.Me.SpentExperience = br.ReadUInt32();
          break;
        case PlayerProtocol.Reply.Level:
          FacadeFactory.GetInstance().SendMessage(Message.PlayerLevel, (object) br.ReadByte());
          break;
        case PlayerProtocol.Reply.NormalExperience:
          Game.Me.PrevLevelExperience = br.ReadUInt32();
          Game.Me.NextLevelExperience = br.ReadUInt32();
          break;
        case PlayerProtocol.Reply.Avatar:
          Game.Me.AvatarDesc = br.ReadDesc<AvatarDescription>();
          GUICharacterStatusWindow characterStatusWindow = Game.GUIManager.Find<GUICharacterStatusWindow>();
          if (characterStatusWindow == null)
            break;
          characterStatusWindow.UpdateAvatar(Game.Me.AvatarDesc);
          break;
        case PlayerProtocol.Reply.Loot:
          Debug.LogWarning((object) ("Receiving loot reply with a list of items, but no UI to display it exists! LootId: " + (object) br.ReadUInt16() + " Items: " + (object) ItemFactory.ReadItemList(br)));
          break;
        case PlayerProtocol.Reply.RemoveLootItems:
          Debug.LogWarning((object) ("Receiving remove loot reply with a list of items, but no UI to display it exists! LootId: " + (object) br.ReadUInt16() + " Items: " + (object) br.ReadUInt16List()));
          break;
        case PlayerProtocol.Reply.Stats:
          Game.Me.Stats.Read(br);
          break;
        case PlayerProtocol.Reply.PaymentInfo:
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          PlayerProtocol.\u003CParseMessage\u003Ec__AnonStoreyF8 messageCAnonStoreyF8 = new PlayerProtocol.\u003CParseMessage\u003Ec__AnonStoreyF8();
          uint cardGUID = br.ReadGUID();
          // ISSUE: reference to a compiler-generated field
          messageCAnonStoreyF8.time = br.ReadInt64();
          RewardCard rewardCard = (int) cardGUID != 0 ? (RewardCard) Game.Catalogue.FetchCard(cardGUID, CardView.Reward) : (RewardCard) null;
          Game.SpecialOfferManager.SpecialOfferCard = rewardCard;
          // ISSUE: reference to a compiler-generated field
          Game.SpecialOfferManager.SpecialOfferBonusEndTime = (double) messageCAnonStoreyF8.time;
          Game.SpecialOfferManager.UpdateSpecialOffer(rewardCard != null);
          // ISSUE: reference to a compiler-generated field
          if (messageCAnonStoreyF8.time > 0L)
          {
            // ISSUE: object of a compiler-generated type is created
            // ISSUE: variable of a compiler-generated type
            PlayerProtocol.\u003CParseMessage\u003Ec__AnonStoreyF7 messageCAnonStoreyF7 = new PlayerProtocol.\u003CParseMessage\u003Ec__AnonStoreyF7();
            // ISSUE: reference to a compiler-generated field
            messageCAnonStoreyF7.\u003C\u003Ef__ref\u0024248 = messageCAnonStoreyF8;
            // ISSUE: reference to a compiler-generated field
            messageCAnonStoreyF7.guicard = (int) cardGUID != 0 ? (GUICard) Game.Catalogue.FetchCard(cardGUID, CardView.GUI) : (GUICard) null;
            // ISSUE: reference to a compiler-generated field
            // ISSUE: reference to a compiler-generated method
            messageCAnonStoreyF7.guicard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyF7.\u003C\u003Em__248));
            Game.SpecialOfferManager.UpdateHappyHour(true);
            // ISSUE: reference to a compiler-generated field
            FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new LogToEventStreamMessage("client.happyhour", new object[6]
            {
              (object) "geoRegion",
              (object) Game.GeoRegion,
              (object) "paymentGuid",
              (object) cardGUID,
              (object) "endTime",
              (object) messageCAnonStoreyF8.time
            }));
            break;
          }
          Game.SpecialOfferManager.UpdateHappyHour(false);
          break;
        case PlayerProtocol.Reply.Counters:
          using (List<CounterDesc>.Enumerator enumerator = br.ReadDescList<CounterDesc>().GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              CounterDesc current = enumerator.Current;
              Game.Me.Counters[current.GUID] = current.Value;
              Game.Catalogue.FetchCard(current.GUID, CardView.Counter);
            }
            break;
          }
        case PlayerProtocol.Reply.Title:
          Game.Me.DutyBook._SetSelectedDuty(br.ReadUInt16());
          break;
        case PlayerProtocol.Reply.ResetDuties:
          Game.Me.DutyBook._Clear();
          break;
        case PlayerProtocol.Reply.AllowFactionSwitch:
          br.ReadBoolean();
          int num3 = (int) br.ReadGUID();
          break;
        case PlayerProtocol.Reply.Factors:
          Game.Me.Factors._AddItems((IEnumerable<Factor>) br.ReadDescList<Factor>());
          break;
        case PlayerProtocol.Reply.RemoveFactors:
          Game.Me.Factors._RemoveItems((IEnumerable<ushort>) br.ReadUInt16List());
          break;
        case PlayerProtocol.Reply.Mail:
          Game.Me.MailBox._AddItems((IEnumerable<Mail>) br.ReadDescList<Mail>());
          break;
        case PlayerProtocol.Reply.RemoveMail:
          Game.Me.MailBox._RemoveItems((IEnumerable<ushort>) br.ReadUInt16List());
          break;
        case PlayerProtocol.Reply.Capability:
          Capability capability = (Capability) br.ReadByte();
          switch (br.ReadByte())
          {
            case 0:
              Game.Me.Caps._Reset(capability);
              return;
            case 1:
              Game.Me.Caps._Set(capability, DateTime.MaxValue);
              return;
            case 2:
              Game.Me.Caps._Set(capability, br.ReadDateTime());
              return;
            default:
              return;
          }
        case PlayerProtocol.Reply.AnswerUserBonus:
          this.ApplyGlobalBonus(br.ReadGUID(), br.ReadBoolean());
          break;
        case PlayerProtocol.Reply.FactorModify:
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          PlayerProtocol.\u003CParseMessage\u003Ec__AnonStoreyF9 messageCAnonStoreyF9 = new PlayerProtocol.\u003CParseMessage\u003Ec__AnonStoreyF9();
          // ISSUE: reference to a compiler-generated field
          messageCAnonStoreyF9.id = br.ReadUInt16();
          float num4 = br.ReadSingle();
          uint num5 = br.ReadUInt32();
          // ISSUE: reference to a compiler-generated method
          Factor factor = Game.Me.Factors.Find(new Predicate<Factor>(messageCAnonStoreyF9.\u003C\u003Em__249));
          factor.Value = num4;
          factor.EndTime = num5;
          break;
        case PlayerProtocol.Reply.UpdatePopupSeenList:
          List<uint> uintList = br.ReadUInt32List();
          for (int index2 = 0; index2 < uintList.Count; ++index2)
          {
            if (uintList[index2] < 13U)
              PopupTutorialTipManager.Instance.SetTaskToComplete(uintList[index2]);
          }
          break;
        case PlayerProtocol.Reply.CannotStackBoosters:
          Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox("%$bgo.misc.cannot_stack_boosters%");
          break;
        case PlayerProtocol.Reply.Anchor:
          uint objectID1 = br.ReadUInt32();
          Game.Me.Anchored = true;
          Game.Me.AnchorTarget = objectID1;
          PlayerShip playerShip1 = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID1) as PlayerShip;
          SpaceLevel.GetLevel().SetPlayerShip(playerShip1);
          GuiDockUndock.ShowUndock();
          FacadeFactory.GetInstance().SendMessage(Message.DockAtPlayerShipReply, (object) playerShip1);
          break;
        case PlayerProtocol.Reply.Unanchor:
          uint objectID2 = br.ReadUInt32();
          UnanchorReason unanchorReason = (UnanchorReason) br.ReadByte();
          Game.Me.Anchored = false;
          Game.Me.AnchorTarget = 0U;
          GuiDockUndock.HideUndock();
          if (unanchorReason != UnanchorReason.Killed)
            JumpActionsHandler.CancelJumpSequence();
          if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null)
            SpaceLevel.GetLevel().SetPlayerShip(SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID2) as PlayerShip);
          if (unanchorReason == UnanchorReason.Timeout)
            FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.carrier.carrier_timed_out%", NotificationCategory.Neutral));
          FacadeFactory.GetInstance().SendMessage(Message.UndockFromPlayerShipReply);
          break;
        case PlayerProtocol.Reply.CarrierDradis:
          uint objectID3 = br.ReadUInt32();
          float num6 = br.ReadSingle();
          float num7 = br.ReadSingle();
          float num8 = br.ReadSingle();
          if ((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null)
            break;
          PlayerShip playerShip2 = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID3) as PlayerShip;
          if (playerShip2 == null)
            break;
          playerShip2.Player.Stats.SetObfuscatedFloat(ObjectStat.DetectionOuterRadius, num6);
          playerShip2.Player.Stats.SetObfuscatedFloat(ObjectStat.DetectionInnerRadius, num7);
          playerShip2.Player.Stats.SetObfuscatedFloat(ObjectStat.DetectionVisualRadius, num8);
          break;
        case PlayerProtocol.Reply.HoldOverflow:
          Game.ChatStorage.SystemSays("%$bgo.etc.hold_full%");
          break;
        case PlayerProtocol.Reply.SettingsInfo:
          break;
        case PlayerProtocol.Reply.DotArea:
          switch ((PlayerProtocol.DamageOverTimeEffect) br.ReadByte())
          {
            case PlayerProtocol.DamageOverTimeEffect.SectorBorder:
              GuiDotAreaNotificationPanel notificationPanel = Game.GUIManager.Find<GuiDotAreaNotificationPanel>();
              if (notificationPanel == null)
                return;
              notificationPanel.Show();
              return;
            case PlayerProtocol.DamageOverTimeEffect.MineField:
              return;
            default:
              return;
          }
        case PlayerProtocol.Reply.ActivateOnByDefaultSlots:
          Game.Me.ActiveShip._ActivateOnByDefaultSlots();
          break;
        case PlayerProtocol.Reply.WaterExchangeValues:
          Game.Me.WaterExchangeValues.MaxWaterAmountExchange = br.ReadUInt32();
          Game.Me.WaterExchangeValues.WaterInHold = br.ReadUInt32();
          Game.Me.WaterExchangeValues.WaterAmountExchange = br.ReadUInt32();
          Game.Me.WaterExchangeValues.CubitAmountExchange = br.ReadUInt32();
          Game.Me.WaterExchangeValues.WaterCubitExchangeRate = br.ReadSingle();
          break;
        case PlayerProtocol.Reply.BonusMapParts:
          int num9 = br.ReadLength();
          List<WofBonusMapPart> wofBonusMapPartList = new List<WofBonusMapPart>();
          Dictionary<int, List<int>> dictionary = new Dictionary<int, List<int>>();
          for (int index2 = 0; index2 < num9; ++index2)
          {
            int key = br.ReadInt32();
            if (!dictionary.ContainsKey(key))
              dictionary.Add(key, new List<int>());
            int num2 = br.ReadLength();
            for (int index3 = 0; index3 < num2; ++index3)
              dictionary[key].Add(br.ReadInt32());
          }
          using (Dictionary<int, List<int>>.Enumerator enumerator = dictionary.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              KeyValuePair<int, List<int>> current = enumerator.Current;
              WofBonusMapPart wofBonusMapPart = new WofBonusMapPart(false, current.Key, current.Value, (GUICard) null);
              wofBonusMapPartList.Add(wofBonusMapPart);
            }
          }
          FacadeFactory.GetInstance().SendMessage(Message.WofBonusMapPartsUpdate, (object) dictionary);
          break;
        case PlayerProtocol.Reply.Statistics:
          uint num10 = br.ReadUInt32();
          uint num11 = br.ReadUInt32();
          uint maxWaves = br.ReadUInt32();
          uint wavesCompleted = br.ReadUInt32();
          int num12 = (int) br.ReadUInt32();
          uint enemysKilled = br.ReadUInt32();
          FtlRanks medalIdent = (FtlRanks) br.ReadUInt32();
          int num13 = (int) br.ReadGUID();
          List<ShipItem> rewards = ItemFactory.ReadList<ShipItem>(br);
          DialogBoxFactoryNgui.CreateFtlResultDialog(new FtlResultContainer(wavesCompleted, maxWaves, enemysKilled, num11 - num10, rewards, medalIdent));
          break;
        case PlayerProtocol.Reply.CharacterServices:
          Game.Me.SectorId = br.ReadByte();
          bool eligible1 = br.ReadBoolean();
          br.ReadInt64();
          long cooldown1 = br.ReadInt64();
          long cooldown2 = br.ReadInt64();
          long lastUse1 = br.ReadInt64();
          long lastUse2 = br.ReadInt64();
          int cubitsPrice1 = (int) br.ReadSingle();
          int cubitsPrice2 = (int) br.ReadSingle();
          int num14 = br.ReadLength();
          bool eligible2 = false;
          for (int index2 = 0; index2 < num14; ++index2)
          {
            if ((int) Game.Me.Level >= (int) br.ReadByte() && (int) Game.Me.Level <= (int) br.ReadByte())
              eligible2 = true;
          }
          FacadeFactory.GetInstance().SendMessage(Message.CharacterServiceInfo, (object) new List<CharacterService>()
          {
            new CharacterService(CharacterServiceId.FactionSwitch, cooldown1, lastUse1, cubitsPrice1, eligible2),
            new CharacterService(CharacterServiceId.NameChange, cooldown2, lastUse2, cubitsPrice2, eligible1)
          });
          break;
        case PlayerProtocol.Reply.FactionChangeSuccess:
          Debug.LogWarning((object) "Faction switch success!");
          FacadeFactory.GetInstance().SendMessage(Message.FactionSwitchSuccessful);
          break;
        case PlayerProtocol.Reply.NameChangeSuccess:
          FacadeFactory.GetInstance().SendMessage(Message.NameChangeSuccessful);
          break;
        case PlayerProtocol.Reply.AvatarChangeSuccess:
          break;
        case PlayerProtocol.Reply.CharacterServiceError:
          CharacterServiceId characterServiceId = (CharacterServiceId) br.ReadUInt32();
          string str = br.ReadString();
          NotifyBoxUi notifyBox = MessageBoxFactory.CreateNotifyBox(BsgoCanvas.Windows);
          notifyBox.SetContent((OnMessageBoxClose) (param0 => {}), "ERROR", "Error with character service: " + (object) characterServiceId + "\nReason: " + str, 0U);
          notifyBox.ShowTimer(false);
          break;
        case PlayerProtocol.Reply.ResourceHardcap:
          uint num15 = br.ReadGUID();
          int first = br.ReadInt32();
          int second = br.ReadInt32();
          if ((int) num15 != 130920111)
          {
            Debug.LogWarning((object) ("Received cap for guid " + (object) num15 + ", expected merits"));
            break;
          }
          Game.Me.MeritCap = new Tuple<int, int>(first, second);
          break;
        default:
          Debug.LogError((object) ("Unknown server reply in Player protocol: " + (object) reply));
          break;
      }
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ("ReceiveMessage: " + (object) reply + " error: " + ex.ToString()));
    }
  }

  private void ApplyGlobalBonus(uint bonusGuid, bool nextGenEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    Game.IsGameLevelStarted.AddHandler(new SignalHandler(new PlayerProtocol.\u003CApplyGlobalBonus\u003Ec__AnonStoreyFA()
    {
      nextGenEvent = nextGenEvent,
      bonusGuid = bonusGuid
    }.\u003C\u003Em__24B));
  }

  public enum Request : ushort
  {
    MoveItem = 1,
    BuySkill = 2,
    SelectTitle = 3,
    BindSticker = 4,
    SelectConsumable = 5,
    UnbindSticker = 6,
    AddShip = 7,
    RemoveShip = 8,
    SelectShip = 9,
    UpgradeShip = 10,
    RepairSystem = 11,
    RepairShip = 12,
    ScrapShip = 13,
    CreateAvatar = 14,
    SelectFaction = 15,
    UpgradeSystem = 16,
    SetShipName = 17,
    UseAugment = 18,
    [Obsolete] OldChangeFaction = 19,
    ChooseDailyBonus = 20,
    UpgradeSystemByPack = 21,
    MoveAll = 22,
    ReadMail = 23,
    RemoveMail = 24,
    MailAction = 25,
    RepairAll = 26,
    [Obsolete] CheckUserBonus = 27,
    CheckNameAvailability = 28,
    PopupSeen = 30,
    ChooseName = 32,
    CreateAvatarFactionChange = 33,
    [Obsolete] OldChangeName = 34,
    InstantSkillBuy = 35,
    ReduceSkillLearnTime = 36,
    SubmitMission = 37,
    AugmentMassActivation = 38,
    SendDradisData = 39,
    RequestCharacterServices = 40,
    ChangeFaction = 41,
    ChangeName = 42,
    ChangeAvatar = 43,
    ResourceHardcap = 44,
    DeselectTitle = 45,
  }

  public enum Reply : ushort
  {
    Reset = 1,
    PlayerInfo = 2,
    Skills = 3,
    Missions = 4,
    RemoveMissions = 5,
    Duties = 6,
    HoldItems = 7,
    RemoveHoldItems = 8,
    LockerItems = 9,
    RemoveLockerItems = 10,
    ShipInfo = 11,
    Slots = 12,
    Stickers = 13,
    RemoveStickers = 14,
    AddShip = 15,
    RemoveShip = 16,
    ActiveShip = 17,
    ShipName = 19,
    NameAvailable = 20,
    NameNotAvailable = 21,
    ID = 22,
    Name = 23,
    Faction = 24,
    Experience = 25,
    SpentExperience = 26,
    Level = 27,
    NormalExperience = 28,
    Avatar = 29,
    Loot = 30,
    RemoveLootItems = 31,
    Stats = 32,
    [Obsolete] AbilityValidation = 33,
    PaymentInfo = 34,
    Counters = 35,
    Title = 36,
    ResetDuties = 37,
    AllowFactionSwitch = 38,
    Factors = 39,
    RemoveFactors = 40,
    Mail = 41,
    RemoveMail = 42,
    Capability = 43,
    AnswerUserBonus = 44,
    FactorModify = 45,
    UpdatePopupSeenList = 50,
    CannotStackBoosters = 51,
    Anchor = 52,
    Unanchor = 53,
    CarrierDradis = 54,
    HoldOverflow = 55,
    SettingsInfo = 56,
    DotArea = 57,
    [Obsolete] SlotStats = 58,
    ActivateOnByDefaultSlots = 59,
    WaterExchangeValues = 60,
    BonusMapParts = 61,
    Statistics = 62,
    CharacterServices = 63,
    FactionChangeSuccess = 64,
    NameChangeSuccess = 65,
    AvatarChangeSuccess = 66,
    CharacterServiceError = 67,
    ResourceHardcap = 68,
  }

  public enum DamageOverTimeEffect : ushort
  {
    SectorBorder,
    MineField,
    SpeedDebuff,
    PowerDrain,
    DradisRange,
  }

  public delegate void AnsUserBonusCallback(RewardCard bonus);
}
