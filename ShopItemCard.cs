﻿// Decompiled with JetBrains decompiler
// Type: ShopItemCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class ShopItemCard : Card
{
  public Price BuyPrice = new Price();
  public Price UpgradePrice = new Price();
  public Price SellPrice = new Price();
  public ShopCategory Category;
  public ShopItemType ItemType;
  public byte Tier;
  public string[] SortingNames;
  public int SortingWeight;
  public Faction Faction;
  public bool CanBeSold;

  public ShopItemCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.Category = (ShopCategory) r.ReadByte();
    this.ItemType = (ShopItemType) r.ReadByte();
    this.Tier = r.ReadByte();
    this.Faction = (Faction) r.ReadByte();
    this.SortingNames = r.ReadStringArray();
    this.SortingWeight = (int) r.ReadUInt16();
    this.BuyPrice = r.ReadDesc<Price>();
    this.UpgradePrice = r.ReadDesc<Price>();
    this.SellPrice = r.ReadDesc<Price>();
    this.CanBeSold = r.ReadBoolean();
    this.IsLoaded.Depend((ILoadable) this.BuyPrice, (ILoadable) this.UpgradePrice, (ILoadable) this.SellPrice);
    this.IsLoaded.Set();
  }

  public int CompareTo(ShopItemCard other)
  {
    int num = this.ItemType - other.ItemType;
    if (num == 0)
      num = (int) this.Tier - (int) other.Tier;
    if (num == 0)
      num = this.SortingWeight - other.SortingWeight;
    if (num == 0)
      num = (int) this.CardGUID - (int) other.CardGUID;
    return num;
  }

  public bool MatchesSortingKey(string sortingKey)
  {
    for (int index = 0; index < this.SortingNames.Length; ++index)
    {
      if (this.SortingNames[index] == sortingKey)
        return true;
    }
    return false;
  }

  public override string ToString()
  {
    return string.Format("Category: {0}, ItemType: {1}, Tier: {2}, SortingNames: {3}, SortingWeight: {4}, BuyPrice: {5}, UpgradePrice: {6}, SellPrice: {7}, CanBeSold: {8}", (object) this.Category, (object) this.ItemType, (object) this.Tier, (object) this.SortingNames, (object) this.SortingWeight, (object) this.BuyPrice, (object) this.UpgradePrice, (object) this.SellPrice, (object) this.CanBeSold);
  }
}
