﻿// Decompiled with JetBrains decompiler
// Type: CapabilityList
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class CapabilityList
{
  private Dictionary<Capability, DateTime> times = new Dictionary<Capability, DateTime>();

  public bool IsRestricted(Capability capability)
  {
    return this.times.ContainsKey(capability);
  }

  public TimeSpan Timeout(Capability capability)
  {
    return this.times[capability] - Game.TimeSync.ServerDateTime;
  }

  public void _Clear()
  {
    this.times.Clear();
  }

  public void _Set(Capability capability, DateTime timeout)
  {
    this.times[capability] = timeout;
  }

  public void _Reset(Capability capability)
  {
    this.times.Remove(capability);
  }
}
