﻿// Decompiled with JetBrains decompiler
// Type: InputBindingOptionsJoystickGamepad
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class InputBindingOptionsJoystickGamepad : InputBindingOptionsBase
{
  private const string XBOX_PRESET_STRING_DEFAULT = "Xbox 360® Controller - Default";
  private const string XBOX_PRESET_STRING_ECHELON = "Xbox 360® Controller - Echelon";
  private const string XBOX_PRESET_STRING_STRIKER = "Xbox 360® Controller - Striker";
  private const string XBOX_PRESET_STRING_VANGUARD = "Xbox 360® Controller - Vanguard";
  public InputBindingOptionsJoystickGamepad.ToggleJoystickEnabledHandler ToggleJoystickEnabledDelegate;
  public InputBindingOptionsJoystickGamepad.ToggleXbox360ButtonsHandler ToggleXbox360Buttons;
  private DropDownLabeledWidget presetDropDown;
  private OptionButtonElement showXboxButtons;
  private UILabel dropDownLabel;
  private string selectPresetString;

  protected override void InitOptionsElements()
  {
    OptionButtonElement buttonElement = NguiWidgetFactory.Options.CreateButtonElement(this.content);
    buttonElement.name = "A_" + buttonElement.name;
    this.controls.Add(UserSetting.JoystickGamepadEnabled, (OptionsElement) buttonElement);
    this.controls.Add(UserSetting.InvertedVertical, (OptionsElement) NguiWidgetFactory.Options.CreateButtonElement(this.content));
    OptionsSliderElement sliderElement1 = NguiWidgetFactory.Options.CreateSliderElement(this.content);
    sliderElement1.ShowPercentage = true;
    this.controls.Add(UserSetting.DeadZoneJoystick, (OptionsElement) sliderElement1);
    OptionsSliderElement sliderElement2 = NguiWidgetFactory.Options.CreateSliderElement(this.content);
    sliderElement2.ShowPercentage = true;
    this.controls.Add(UserSetting.SensitivityJoystick, (OptionsElement) sliderElement2);
    bool isXbox360Detected = JoystickSetup.IsXbox360Detected;
    this.showXboxButtons = NguiWidgetFactory.Options.CreateButtonElement(this.content);
    this.showXboxButtons.name = "Zx_" + this.showXboxButtons.name;
    this.controls.Add(UserSetting.ShowXbox360Buttons, (OptionsElement) this.showXboxButtons);
    this.presetDropDown = NguiWidgetFactory.General.CreateDropDownMenuLabeled(this.content);
    this.presetDropDown.name = "Zz_" + this.presetDropDown.name;
    this.presetDropDown.DropDownWidget.gameObject.GetComponent<UIPopupList>().items.Add("Xbox 360® Controller - Default");
    this.presetDropDown.DropDownWidget.gameObject.GetComponent<UIPopupList>().items.Add("Xbox 360® Controller - Echelon");
    this.presetDropDown.DropDownWidget.gameObject.GetComponent<UIPopupList>().items.Add("Xbox 360® Controller - Striker");
    this.presetDropDown.DropDownWidget.gameObject.GetComponent<UIPopupList>().items.Add("Xbox 360® Controller - Vanguard");
    this.presetDropDown.DropDownWidget.gameObject.GetComponent<UIPopupList>().onSelectionChange += new UIPopupList.LegacyEvent(this.OnPresetSelected);
    this.showXboxButtons.gameObject.SetActive(isXbox360Detected);
    this.presetDropDown.gameObject.SetActive(isXbox360Detected);
    this.content.GetComponent<UITable>().Reposition();
  }

  public void OnPresetSelected(string selectedPreset)
  {
    string key = selectedPreset;
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (InputBindingOptionsJoystickGamepad.\u003C\u003Ef__switch\u0024mapE == null)
      {
        // ISSUE: reference to a compiler-generated field
        InputBindingOptionsJoystickGamepad.\u003C\u003Ef__switch\u0024mapE = new Dictionary<string, int>(4)
        {
          {
            "Xbox 360® Controller - Default",
            0
          },
          {
            "Xbox 360® Controller - Echelon",
            1
          },
          {
            "Xbox 360® Controller - Striker",
            2
          },
          {
            "Xbox 360® Controller - Vanguard",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (InputBindingOptionsJoystickGamepad.\u003C\u003Ef__switch\u0024mapE.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            FacadeFactory.GetInstance().SendMessage(Message.LoadBindingPreset, (object) BindingPreset.Xbox360Default);
            return;
          case 1:
            FacadeFactory.GetInstance().SendMessage(Message.LoadBindingPreset, (object) BindingPreset.Xbox360Echelon);
            return;
          case 2:
            FacadeFactory.GetInstance().SendMessage(Message.LoadBindingPreset, (object) BindingPreset.Xbox360Striker);
            return;
          case 3:
            FacadeFactory.GetInstance().SendMessage(Message.LoadBindingPreset, (object) BindingPreset.Xbox360Vanguard);
            return;
        }
      }
    }
    if (string.IsNullOrEmpty(selectedPreset) || !(selectedPreset != this.selectPresetString))
      return;
    Debug.LogError((object) ("(" + selectedPreset + ") is not a valid inputBinding Preset"));
  }

  public void ToggleJoystickGamepadEnabled(bool isEnabled)
  {
    using (Dictionary<UserSetting, OptionsElement>.Enumerator enumerator = this.controls.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<UserSetting, OptionsElement> current = enumerator.Current;
        if (current.Key != UserSetting.JoystickGamepadEnabled)
          current.Value.gameObject.SetActive(isEnabled);
      }
    }
    this.presetDropDown.gameObject.SetActive(isEnabled && JoystickSetup.IsXbox360Detected);
    this.showXboxButtons.gameObject.SetActive(isEnabled && JoystickSetup.IsXbox360Detected);
    this.content.GetComponent<UITable>().Reposition();
  }

  public override void InjectOnChangeDelegate(OnSettingChanged changed)
  {
    base.InjectOnChangeDelegate(changed);
    using (Dictionary<UserSetting, OptionsElement>.Enumerator enumerator = this.controls.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<UserSetting, OptionsElement> current = enumerator.Current;
        if (current.Key == UserSetting.JoystickGamepadEnabled)
          current.Value.OnSettingChanged += (OnSettingChanged) ((setting, value) => this.ToggleJoystickEnabledDelegate((bool) value));
        if (current.Key == UserSetting.ShowXbox360Buttons)
          current.Value.OnSettingChanged += (OnSettingChanged) ((setting, value) => this.ToggleXbox360Buttons((bool) value));
      }
    }
  }

  public override void ReloadLanguageData()
  {
    base.ReloadLanguageData();
    this.presetDropDown.LabelText = Tools.ParseMessage("%$bgo.ui.options.control.controls.load_preset%");
    this.selectPresetString = Tools.ParseMessage("%$bgo.ui.options.control.controls.select_preset%");
    UIPopupList component = this.presetDropDown.DropDownWidget.gameObject.GetComponent<UIPopupList>();
    component.textLabel = this.dropDownLabel = component.textLabel ?? this.dropDownLabel;
    component.value = this.selectPresetString;
    component.textLabel = (UILabel) null;
  }

  public override void UpdateSettings(UserSettings userSettings)
  {
    this.ToggleJoystickGamepadEnabled(userSettings.JoystickGamepadEnabled);
    base.UpdateSettings(userSettings);
  }

  public delegate void ToggleJoystickEnabledHandler(bool isEnabled);

  public delegate void ToggleXbox360ButtonsHandler(bool isEnabled);
}
