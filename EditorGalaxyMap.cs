﻿// Decompiled with JetBrains decompiler
// Type: EditorGalaxyMap
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor;
using BsgoEditor.GUI;
using Loaders;
using System;
using System.Collections.Generic;
using UnityEngine;

public class EditorGalaxyMap : ElementBase
{
  private ElementBase stars = new ElementBase();
  private Label starHintName = new Label(string.Empty);
  private ElementBase starPropertiesPanel = new ElementBase();
  private EditFieldWithCaption sfId = new EditFieldWithCaption();
  private EditFieldWithCaption spNumber = new EditFieldWithCaption();
  private SelectList<int> spSize = new SelectList<int>(3);
  private ElementBase controlButtonPanel = new ElementBase();
  protected Texture2D bigSectorTexture = (Texture2D) ResourceLoader.Load("GUI/GalaxyMap/star_big");
  protected Texture2D middleSectorTexture = (Texture2D) ResourceLoader.Load("GUI/GalaxyMap/star_middle");
  protected Texture2D smallSectorTexture = (Texture2D) ResourceLoader.Load("GUI/GalaxyMap/star_small");
  private List<Texture2D> textures = new List<Texture2D>();
  private bool initialized;
  private Button addButton;
  private Button removeButton;
  private Image background;
  private Button sfOk;
  private Image frame;
  private Button saveButton;
  private Button exitButton;
  private JGalaxyMapDescription.JSector selectedSector;
  private JGalaxyMapDescription desc;
  private bool isDragged;

  public EditorGalaxyMap()
  {
    this.Align = Align.Center;
    this.SizeType = SizeTypes.AllJustified;
    this.Initialize();
  }

  private void Initialize()
  {
    if (this.initialized)
      return;
    Texture2D normal = (Texture2D) ResourceLoader.Load("GUI/GalaxyMap/smallbutton");
    Texture2D pressed = (Texture2D) ResourceLoader.Load("GUI/GalaxyMap/smallbutton_click");
    Texture2D over = (Texture2D) ResourceLoader.Load("GUI/GalaxyMap/smallbutton_over");
    this.background = new Image((Texture2D) ResourceLoader.Load("GUI/GalaxyMap/background"));
    this.background.Align = Align.Center;
    this.AddChild((ElementBase) this.background);
    this.controlButtonPanel.Align = Align.RightDown;
    this.controlButtonPanel.Size = new float2(200f, 200f);
    this.AddChild(this.controlButtonPanel);
    this.saveButton = new Button("Save", normal, pressed, over);
    this.saveButton.Align = Align.CenterCustom;
    this.saveButton.Position = new float2(0.0f, 0.0f);
    this.saveButton.OnClick = (System.Action<KeyCode>) (key => SectorIO.SaveGalaxyMap(this.desc));
    this.controlButtonPanel.AddChild((ElementBase) this.saveButton);
    this.exitButton = new Button("Exit", normal, pressed, over);
    this.exitButton.Align = Align.CenterCustom;
    this.exitButton.Position = new float2(0.0f, 20f);
    this.exitButton.OnClick = (System.Action<KeyCode>) (key => this.Enabled = false);
    this.controlButtonPanel.AddChild((ElementBase) this.exitButton);
    this.textures.Add(this.bigSectorTexture);
    this.textures.Add(this.middleSectorTexture);
    this.textures.Add(this.smallSectorTexture);
    this.stars.SizeType = SizeTypes.AllJustified;
    this.background.AddChild(this.stars);
    this.starHintName.Position = new float2(30f, 15f);
    this.starPropertiesPanel.Align = Align.RightUp;
    this.starPropertiesPanel.Size = new float2(220f, 300f);
    this.sfId.Caption = "Find sector by number: ";
    this.sfId.Position = new float2(0.0f, 220f);
    this.starPropertiesPanel.AddChild((ElementBase) this.sfId);
    this.sfOk = new Button("Ok", normal, pressed, over);
    this.sfOk.Position = new float2(0.0f, 220f);
    this.sfOk.Align = Align.RightCustom;
    this.sfOk.OnClick = (System.Action<KeyCode>) (key =>
    {
      uint result = 0;
      uint.TryParse(this.sfId.Text, out result);
      using (List<JGalaxyMapDescription.JSector>.Enumerator enumerator = this.desc.GetSectors().GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JGalaxyMapDescription.JSector current = enumerator.Current;
          Debug.Log((object) (((int) current.SectorId).ToString() + " " + (object) result));
          if ((int) current.SectorId == (int) result)
          {
            this.SetSelectedSector(current);
            break;
          }
        }
      }
    });
    this.starPropertiesPanel.AddChild((ElementBase) this.sfOk);
    this.spNumber.Caption = "N: ";
    this.spNumber.Position = new float2(0.0f, 20f);
    this.spNumber.Text = "123";
    this.starPropertiesPanel.AddChild((ElementBase) this.spNumber);
    this.spSize.Add("Big", 0);
    this.spSize.Add("Medium", 1);
    this.spSize.Add("Small", 2);
    this.spSize.Position = new float2(0.0f, 60f);
    this.starPropertiesPanel.AddChild((ElementBase) this.spSize);
    this.addButton = new Button("Add", normal, pressed, over);
    this.addButton.Align = Align.LeftCustom;
    this.addButton.Position = new float2(0.0f, 180f);
    this.addButton.OnClick = (System.Action<KeyCode>) (key =>
    {
      JGalaxyMapDescription.JSector sector = this.desc.AddNewSector();
      this.ApplySectorDescirption(sector);
      this.SetSelectedSector(sector);
    });
    this.starPropertiesPanel.AddChild((ElementBase) this.addButton);
    this.removeButton = new Button("Remove", normal, pressed, over);
    this.removeButton.Align = Align.RightCustom;
    this.removeButton.Position = new float2(0.0f, 180f);
    this.removeButton.OnClick = (System.Action<KeyCode>) (key =>
    {
      this.selectedSector.texture.Parent.RemoveChild((ElementBase) this.selectedSector.texture);
      this.desc.Remove(this.selectedSector);
      this.SetSelectedSector((JGalaxyMapDescription.JSector) null);
    });
    this.starPropertiesPanel.AddChild((ElementBase) this.removeButton);
    Button button = new Button("Ok", normal, pressed, over);
    button.Align = Align.CenterDown;
    button.OnClick = (System.Action<KeyCode>) (key =>
    {
      if (this.selectedSector == null)
        return;
      uint result;
      if (uint.TryParse(this.spNumber.Text, out result))
        this.selectedSector.SectorId = result;
      this.selectedSector.textureIndex = this.spSize.SelectedValue;
      this.ApplySectorDescirption(this.selectedSector);
      this.SetSelectedSector(this.selectedSector);
    });
    this.starPropertiesPanel.AddChild((ElementBase) button);
    this.AddChild(this.starPropertiesPanel);
    this.frame = new Image((Texture2D) ResourceLoader.Load("GUI/GalaxyMap/selection_marker"));
    this.frame.Enabled = false;
    this.background.AddChild((ElementBase) this.frame);
    this.initialized = true;
  }

  public void SetSectors(JGalaxyMapDescription desc)
  {
    this.desc = desc;
    this.stars.ClearChildren();
    using (List<JGalaxyMapDescription.JSector>.Enumerator enumerator = desc.GetSectors().GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.ApplySectorDescirption(enumerator.Current);
    }
    if (desc.GetSectors().Count <= 0)
      return;
    this.SetSelectedSector(desc.GetSectors()[0]);
  }

  private void ApplySectorDescirption(JGalaxyMapDescription.JSector sector)
  {
    if (sector.texture == null)
    {
      Image image = new Image(this.textures[sector.textureIndex]);
      sector.texture = image;
      this.background.AddChild((ElementBase) image);
    }
    else
      sector.texture.Texture = this.textures[sector.textureIndex];
    sector.texture.Position = new float2(this.background.Size.x / 2f + sector.Position.x, this.background.Size.y / 2f + sector.Position.y);
  }

  public JGalaxyMapDescription GetSectors()
  {
    return this.desc;
  }

  public override void OnMouseDown(KeyCode key, float2 position)
  {
    base.OnMouseDown(key, position);
    if (key != KeyCode.Mouse0)
      return;
    List<JGalaxyMapDescription.JSector> sectors = this.desc.GetSectors();
    for (int index = 0; index < sectors.Count; ++index)
    {
      if (sectors[index].texture.ContainsAbs(position))
      {
        this.SetSelectedSector(sectors[index]);
        this.isDragged = true;
        break;
      }
    }
  }

  public override void OnMouseUp(KeyCode key, float2 position)
  {
    base.OnMouseUp(key, position);
    this.isDragged = false;
  }

  public override void OnMouseMove(float2 positionAbs, float2 prevPosition)
  {
    base.OnMouseMove(positionAbs, prevPosition);
    if (!this.isDragged)
      return;
    Debug.Log((object) (this.selectedSector.Position.ToString() + " " + (object) (this.selectedSector.Position + positionAbs - prevPosition)));
    this.selectedSector.Position = this.selectedSector.Position + positionAbs - prevPosition;
    if ((double) Math.Abs(this.selectedSector.Position.x) > (double) this.background.Size.x / 2.0)
      this.selectedSector.Position.x = (float) ((double) Math.Sign(this.selectedSector.Position.x) * (double) this.background.Size.x / 2.0);
    if ((double) Math.Abs(this.selectedSector.Position.y) > (double) this.background.Size.y / 2.0)
      this.selectedSector.Position.y = (float) ((double) Math.Sign(this.selectedSector.Position.y) * (double) this.background.Size.y / 2.0);
    this.ApplySectorDescirption(this.selectedSector);
    this.SetSelectedSector(this.selectedSector);
  }

  private void SetSelectedSector(JGalaxyMapDescription.JSector sector)
  {
    this.selectedSector = sector;
    Debug.Log((object) ("Selected sector " + this.selectedSector.SectorId.ToString()));
    if (sector != null)
    {
      this.sfId.Text = sector.SectorId.ToString();
      this.frame.Position = this.selectedSector.texture.Position;
      this.frame.Enabled = true;
      this.starHintName.Enabled = true;
      this.starHintName.SetText(sector.SectorId.ToString());
      sector.texture.AddChild((ElementBase) this.starHintName);
      this.starPropertiesPanel.Enabled = true;
      this.spNumber.Text = sector.SectorId.ToString();
      this.spSize.SelectedValue = sector.textureIndex;
    }
    else
    {
      this.frame.Enabled = false;
      this.starHintName.Enabled = false;
      this.starPropertiesPanel.Enabled = false;
    }
  }
}
