﻿// Decompiled with JetBrains decompiler
// Type: Guild
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class Guild
{
  private GuildRole guildRole = GuildRole.Recruit;
  private string name = string.Empty;
  private List<Player> players = new List<Player>();
  private List<RankDefinition> ranks = new List<RankDefinition>();
  private uint serverID;

  public bool Has
  {
    get
    {
      return (int) this.serverID != 0;
    }
  }

  public uint ServerID
  {
    get
    {
      return this.serverID;
    }
  }

  public GuildRole GuildRole
  {
    get
    {
      return this.guildRole;
    }
  }

  public string Name
  {
    get
    {
      return this.name;
    }
  }

  public ReadOnlyCollection<Player> Players
  {
    get
    {
      return this.players.AsReadOnly();
    }
  }

  private ulong Permission(GuildRole role)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    RankDefinition rankDefinition = this.ranks.Find(new Predicate<RankDefinition>(new Guild.\u003CPermission\u003Ec__AnonStorey102() { role = role }.\u003C\u003Em__257));
    if (rankDefinition != null)
      return rankDefinition.Permissions;
    return 0;
  }

  public bool GetPermissionFor(GuildRole role, GuildOperation permission)
  {
    ulong num = 1UL << (int) (permission - (ushort) 1 & (GuildOperation) 63);
    return ((long) this.Permission(role) & (long) num) != 0L;
  }

  public void SetPermissionFor(GuildRole role, GuildOperation permission, bool isChecked)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Guild.\u003CSetPermissionFor\u003Ec__AnonStorey103 forCAnonStorey103 = new Guild.\u003CSetPermissionFor\u003Ec__AnonStorey103();
    // ISSUE: reference to a compiler-generated field
    forCAnonStorey103.role = role;
    // ISSUE: reference to a compiler-generated method
    RankDefinition rankDefinition = this.ranks.Find(new Predicate<RankDefinition>(forCAnonStorey103.\u003C\u003Em__258));
    if (rankDefinition != null)
    {
      ulong num1 = 1UL << (int) (permission - (ushort) 1 & (GuildOperation) 63);
      if (isChecked)
      {
        rankDefinition.Permissions |= num1;
      }
      else
      {
        ulong num2 = ~num1;
        rankDefinition.Permissions &= num2;
      }
      // ISSUE: reference to a compiler-generated field
      CommunityProtocol.GetProtocol().RequestGuildChangeRankPermissions(forCAnonStorey103.role, rankDefinition.Permissions);
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      Debug.LogError((object) ("The Role, " + (object) forCAnonStorey103.role + " was not found when setting the permission, " + (object) permission + ", to " + (object) isChecked));
    }
  }

  public void _RemovePlayer(Player player, bool leave)
  {
    this.players.Remove(player);
    FacadeFactory.GetInstance().SendMessage(Message.PlayerChangedGuildStatus, (object) player);
    string text;
    if (player.IsMe)
    {
      this._Reset();
      text = !leave ? BsgoLocalization.Get("%$bgo.chat.message_kicked_guild%") : BsgoLocalization.Get("%$bgo.chat.message_left_guild%");
    }
    else if (leave)
      text = BsgoLocalization.Get("%$bgo.chat.message_left_guildmate%", (object) player.Name);
    else
      text = BsgoLocalization.Get("%$bgo.chat.message_kicked_guildmate%", (object) player.Name);
    Game.ChatStorage.SystemSays(text);
  }

  public void AddOrUpdatePlayer(Player player)
  {
    if (this.players.Contains(player))
      return;
    this._AddPlayer(player);
  }

  public void _AddPlayer(Player player)
  {
    if (player.IsMe)
    {
      this.guildRole = player.GuildRole;
      GuiWingsMainPanel guiWingsMainPanel = Game.GUIManager.Find<GuiWingsMainPanel>();
      if (guiWingsMainPanel != null)
        guiWingsMainPanel.AddMeToWing();
    }
    else
      Game.ChatStorage.SystemSays(BsgoLocalization.Get("%$bgo.chat.message_new_guildmate%", (object) player.Name));
    if (this.players.Contains(player))
      return;
    this.players.Add(player);
    FacadeFactory.GetInstance().SendMessage(Message.PlayerChangedGuildStatus, (object) player);
  }

  public void _SetGuildRole(GuildRole role)
  {
    this.guildRole = role;
  }

  public void _SetInfo(uint serverID, string name, List<Player> players, List<RankDefinition> ranks)
  {
    this.serverID = serverID;
    this.name = name;
    this.players = players;
    this.ranks = ranks;
    for (int index = 0; index < players.Count; ++index)
    {
      Player player = players[index];
      if (player != null)
      {
        FacadeFactory.GetInstance().SendMessage(Message.PlayerChangedGuildStatus, (object) player);
        if (player.IsMe)
          this.guildRole = player.GuildRole;
      }
    }
  }

  public void _Reset()
  {
    this.serverID = 0U;
    this.name = string.Empty;
    this.guildRole = GuildRole.Recruit;
    while (this.players.Count > 0)
    {
      Player player = this.players[0];
      this.players.RemoveAt(0);
      FacadeFactory.GetInstance().SendMessage(Message.PlayerChangedGuildStatus, (object) player);
    }
  }

  public string _RankName(GuildRole rank)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    RankDefinition rankDefinition = this.ranks.Find(new Predicate<RankDefinition>(new Guild.\u003C_RankName\u003Ec__AnonStorey104() { rank = rank }.\u003C\u003Em__259));
    if (rankDefinition != null)
      return rankDefinition.Name;
    return string.Empty;
  }

  public void _UpdateRankName(GuildRole rank, string name)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    RankDefinition rankDefinition = this.ranks.Find(new Predicate<RankDefinition>(new Guild.\u003C_UpdateRankName\u003Ec__AnonStorey105() { rank = rank }.\u003C\u003Em__25A));
    if (rankDefinition == null)
      return;
    rankDefinition.Name = name;
  }

  public void _UpdateRankPermissions(GuildRole rank, ulong permissions)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    RankDefinition rankDefinition = this.ranks.Find(new Predicate<RankDefinition>(new Guild.\u003C_UpdateRankPermissions\u003Ec__AnonStorey106() { rank = rank }.\u003C\u003Em__25B));
    if (rankDefinition == null)
      return;
    rankDefinition.Permissions = permissions;
  }
}
