﻿// Decompiled with JetBrains decompiler
// Type: CutsceneProjectileForgeEvents
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class CutsceneProjectileForgeEvents : MonoBehaviour
{
  public void StopProjectilesAndImpacts()
  {
    ProjectileForge.StopAllImpacts();
    ProjectileForge.StopAllProjectileForgeScripts();
  }

  private void StopAllEmittersOnSpaceObject(string jsonKey)
  {
    if (SectorEditorHelper.IsEditorLevel())
    {
      Debug.LogWarning((object) ("StopAllEmittersOnSpaceObject(" + jsonKey + ") ignored as we're in the Sector Editor."));
    }
    else
    {
      foreach (SpaceObject spaceObject in (IEnumerable) SpaceLevel.GetLevel().GetObjectRegistry().GetAll())
      {
        if (string.Equals(spaceObject.GUICard.Key, jsonKey, StringComparison.CurrentCultureIgnoreCase))
        {
          GameObject model = spaceObject.Model;
          if ((UnityEngine.Object) model != (UnityEngine.Object) null)
          {
            foreach (ParticleSystem componentsInChild in model.GetComponentsInChildren<ParticleSystem>())
              componentsInChild.enableEmission = false;
          }
        }
      }
    }
  }
}
