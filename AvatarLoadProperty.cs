﻿// Decompiled with JetBrains decompiler
// Type: AvatarLoadProperty
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Unity5AssetHandling;
using UnityEngine;

public class AvatarLoadProperty
{
  public AvatarTypingProperty typing = new AvatarTypingProperty();
  private Avatar avatar;
  private bool isLoaded;
  public UnityEngine.Object useOn;
  public UnityEngine.Object use;

  public bool IsLoaded
  {
    get
    {
      return this.isLoaded;
    }
  }

  public AvatarLoadProperty(Avatar avatar, AvatarTypingProperty typing)
  {
    this.avatar = avatar;
    this.typing = typing;
    if (typing.type != AvatarPropertyType.Object)
      return;
    this.isLoaded = true;
  }

  public void RequestFulfilled(UnityEngine.Object result)
  {
    this.use = result;
    this.isLoaded = true;
  }

  public void Use(Transform transform)
  {
    if (!this.IsLoaded)
      return;
    switch (this.typing.type)
    {
      case AvatarPropertyType.Object:
        this.UseItem(transform);
        break;
      case AvatarPropertyType.Texture:
        this.UseTexture();
        break;
      case AvatarPropertyType.Material:
        this.UseMaterial();
        break;
    }
  }

  private void UseItem(Transform tr)
  {
    for (int index = 0; index < tr.transform.childCount; ++index)
      this.UseItem(tr.transform.GetChild(index));
    if (!tr.name.Contains(this.typing.prop.name) || !((UnityEngine.Object) tr.GetComponent<Renderer>() != (UnityEngine.Object) null))
      return;
    if (tr.name == this.typing.prop.value)
    {
      tr.GetComponent<Renderer>().enabled = true;
    }
    else
    {
      tr.GetComponent<Renderer>().enabled = false;
      if (!this.avatar.AdditionalObject(tr.name, this.typing.prop))
        return;
      tr.GetComponent<Renderer>().enabled = true;
    }
  }

  private void UseMaterial()
  {
    if (this.use == (UnityEngine.Object) null || this.useOn == (UnityEngine.Object) null)
      return;
    ((Renderer) this.useOn).material = (Material) this.use;
  }

  private void UseTexture()
  {
    if (this.use == (UnityEngine.Object) null || this.useOn == (UnityEngine.Object) null)
      return;
    ((Material) this.useOn).SetTexture("_MainTex", (Texture) this.use);
  }

  public void Load()
  {
    if (this.typing.prop.value == string.Empty)
      this.isLoaded = true;
    else
      AvatarAssetWaiter.Create(AssetCatalogue.Instance.Request(this.typing.prop.value, false), new System.Action<UnityEngine.Object>(this.RequestFulfilled));
  }
}
