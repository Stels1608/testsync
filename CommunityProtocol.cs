﻿// Decompiled with JetBrains decompiler
// Type: CommunityProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System;
using System.Collections.Generic;

public class CommunityProtocol : BgoProtocol
{
  public CommunityProtocol()
    : base(BgoProtocol.ProtocolID.Community)
  {
  }

  public static CommunityProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Community) as CommunityProtocol;
  }

  public void PartyAppointLeader(uint playerID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 4);
    bw.Write(playerID);
    this.SendMessage(bw);
  }

  public void RequestAddFriend(uint playerID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 6);
    bw.Write(playerID);
    this.SendMessage(bw);
  }

  public void RequestAcceptFriend(uint playerID, bool accept)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 7);
    bw.Write(playerID);
    byte num = !accept ? (byte) 2 : (byte) 1;
    bw.Write(num);
    this.SendMessage(bw);
  }

  public void RequestAddToIgnore(string playerName)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 9);
    bw.Write(playerName);
    this.SendMessage(bw);
  }

  public void RequestRemoveFromIgnore(string playerName)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 10);
    bw.Write(playerName);
    this.SendMessage(bw);
  }

  public void RequestClearIgnore()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 35);
    this.SendMessage(bw);
  }

  public void RequestRemoveFriend(uint playerID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 8);
    bw.Write(playerID);
    this.SendMessage(bw);
  }

  public void RequestAcceptParty(uint partyID, uint inviterID, bool accept)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 5);
    bw.Write(partyID);
    bw.Write(inviterID);
    bw.Write(accept);
    this.SendMessage(bw);
  }

  public void RequestPartyMemberFtlState(uint[] MemberPlayerIDs)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 34);
    bw.Write((ushort) MemberPlayerIDs.Length);
    foreach (uint memberPlayerId in MemberPlayerIDs)
      bw.Write(memberPlayerId);
    this.SendMessage(bw);
  }

  public void RequestInviteToParty(uint playerID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    bw.Write(playerID);
    this.SendMessage(bw);
  }

  public void RequestDismissFromParty(uint playerID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 2);
    bw.Write(playerID);
    this.SendMessage(bw);
  }

  public void RequestLeaveParty()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 3);
    this.SendMessage(bw);
  }

  public void RequestGuildStart(string guildName)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 14);
    bw.Write(guildName);
    this.SendMessage(bw);
  }

  public void RequestGuildLeave()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 15);
    this.SendMessage(bw);
  }

  public void RequestPartyChatInvite(string username)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 32);
    bw.Write(username);
    this.SendMessage(bw);
  }

  public void RequestGuildPromote(uint playerID, GuildRole role)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 18);
    bw.Write(playerID);
    bw.Write((uint) role);
    this.SendMessage(bw);
  }

  public void RequestGuildKick(uint playerID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 19);
    bw.Write(playerID);
    this.SendMessage(bw);
  }

  public void RequestGuildChangeRankName(GuildRole role, string name)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 16);
    bw.Write((uint) role);
    bw.Write(name);
    this.SendMessage(bw);
  }

  public void RequestGuildChangeRankPermissions(GuildRole role, ulong permissions)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 17);
    bw.Write((uint) role);
    bw.Write(permissions);
    this.SendMessage(bw);
  }

  public void RequestGuildInvite(uint playerID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 21);
    bw.Write(playerID);
    this.SendMessage(bw);
  }

  public void RequestGuildInviteAccept(uint guildID, uint inviterID, bool accept)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 22);
    bw.Write(guildID);
    bw.Write(inviterID);
    bw.Write(!accept ? (byte) 0 : (byte) 1);
    this.SendMessage(bw);
  }

  public void SendChatIsConnected(string sessionId)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 11);
    bw.Write(sessionId);
    this.SendMessage(bw);
  }

  public void SendChatAuthenticationFail(string sessionId)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 13);
    bw.Write(sessionId);
    this.SendMessage(bw);
  }

  public void RequestCanSendFleetMessage()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 12);
    bw.Write((ushort) 1);
    this.SendMessage(bw);
  }

  public void RequestRecruitInvited()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 24);
    this.SendMessage(bw);
  }

  public void RequestRecruitBonusLevel()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 33);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    CommunityProtocol.Reply reply = (CommunityProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case CommunityProtocol.Reply.Party:
        uint num1 = br.ReadUInt32();
        uint leaderId = br.ReadUInt32();
        int length = br.ReadLength();
        uint[] partyIDs = new uint[length];
        for (int index = 0; index < length; ++index)
          partyIDs[index] = br.ReadUInt32();
        int num2 = br.ReadLength();
        HashSet<uint> carrierIDs = new HashSet<uint>();
        for (int index = 0; index < num2; ++index)
          carrierIDs.Add(br.ReadUInt32());
        if (Game.GUIManager.Find<GalaxyMapMain>() != null)
        {
          for (int index1 = 0; index1 < Game.Me.Party.Members.Count; ++index1)
          {
            Player player = Game.Me.Party.Members[index1];
            bool flag = true;
            for (int index2 = 0; index2 < partyIDs.Length; ++index2)
            {
              if ((int) player.ServerID == (int) partyIDs[index2])
              {
                flag = false;
                break;
              }
            }
            if (flag)
            {
              GUISlotManager guiSlotManager = Game.GUIManager.Find<GUISlotManager>();
              if (guiSlotManager != null && guiSlotManager.Popup != null && guiSlotManager.Popup.Player == player)
                guiSlotManager.Popup.Clear();
            }
          }
        }
        Game.Me.Party.PartyId = num1;
        Game.Me.Party.SetParty(leaderId, partyIDs, carrierIDs);
        break;
      case CommunityProtocol.Reply.PartyIgnore:
        string str1 = br.ReadString();
        Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox(BsgoLocalization.Get((int) br.ReadByte() != 1 ? "bgo.community.party_ignore_2" : "bgo.community.party_ignore_1", (object) str1));
        break;
      case CommunityProtocol.Reply.PartyInvite:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        CommunityProtocol.\u003CParseMessage\u003Ec__AnonStoreyE2 messageCAnonStoreyE2 = new CommunityProtocol.\u003CParseMessage\u003Ec__AnonStoreyE2();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE2.partyID = br.ReadUInt32();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE2.leaderID = br.ReadUInt32();
        string mainText = BsgoLocalization.Get("bgo.community.invite_to_party", (object) br.ReadString());
        if (Game.Me.SocialState == PlayerSocialState.DoNotDisturb)
        {
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          this.RequestAcceptParty(messageCAnonStoreyE2.partyID, messageCAnonStoreyE2.leaderID, false);
          break;
        }
        // ISSUE: reference to a compiler-generated method
        Game.GUIManager.Find<MessageBoxManager>().ShowDialogBox(mainText, "%$bgo.community.invite_to_party_messagebox_title%", "%$bgo.common.yes%", "%$bgo.common.no%", new System.Action<MessageBoxActionType>(messageCAnonStoreyE2.\u003C\u003Em__22F), 30U);
        break;
      case CommunityProtocol.Reply.FriendInvite:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        CommunityProtocol.\u003CParseMessage\u003Ec__AnonStoreyE3 messageCAnonStoreyE3 = new CommunityProtocol.\u003CParseMessage\u003Ec__AnonStoreyE3();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE3.inviteID = br.ReadUInt32();
        if (Game.Me.SocialState == PlayerSocialState.DoNotDisturb)
        {
          // ISSUE: reference to a compiler-generated field
          this.RequestAcceptFriend(messageCAnonStoreyE3.inviteID, false);
          break;
        }
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        Game.GUIManager.Find<MessageBoxManager>().ShowDialogBox(BsgoLocalization.Get("bgo.community.invite_friend", (object) Game.Players[messageCAnonStoreyE3.inviteID].Name), "%$bgo.community.invite_friend_messagebox_title%", "%$bgo.common.yes%", "%$bgo.common.no%", new System.Action<MessageBoxActionType>(messageCAnonStoreyE3.\u003C\u003Em__230), 15U);
        break;
      case CommunityProtocol.Reply.FriendAccept:
        uint ID = br.ReadUInt32();
        byte num3 = br.ReadByte();
        string str2 = Game.Players.GetPlayer(ID).Name;
        string text = (string) null;
        switch (num3)
        {
          case 1:
            text = BsgoLocalization.Get("bgo.community.accept_friend_1", (object) str2);
            break;
          case 2:
            text = BsgoLocalization.Get("bgo.community.accept_friend_2", (object) str2);
            break;
          case 3:
            text = BsgoLocalization.Get("bgo.community.accept_friend_3", (object) str2);
            break;
          case 4:
            text = BsgoLocalization.Get("bgo.community.accept_friend_4", (object) str2);
            break;
        }
        Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox(text);
        break;
      case CommunityProtocol.Reply.FriendRemove:
        Game.Me.Friends.RemoveFriend(br.ReadUInt32());
        break;
      case CommunityProtocol.Reply.FriendAdd:
        Game.Me.Friends.SetFriends(br.ReadDescList<FriendDesc>());
        break;
      case CommunityProtocol.Reply.IgnoreAdd:
        Game.Me.Friends.SetIgnore((IEnumerable<uint>) br.ReadUInt32List(), br.ReadStringArray());
        break;
      case CommunityProtocol.Reply.IgnoreRemove:
        Game.Me.Friends.RemoveIgnore(br.ReadUInt32());
        break;
      case CommunityProtocol.Reply.ChatSessionId:
        Game.Me.ChatSessionId = br.ReadString();
        Game.Me.ChatProjectID = br.ReadUInt32();
        Game.Me.ChatLanguage = br.ReadString();
        Game.Me.ChatserverUrl = br.ReadString();
        Game.CreateChat();
        Game.ChatProtocol.Connect(Game.Me.Name, Game.Me.ServerID);
        break;
      case CommunityProtocol.Reply.ChatFleetAllowed:
        br.ReadBoolean();
        break;
      case CommunityProtocol.Reply.GuildQuit:
        FacadeFactory.GetInstance().SendMessage(Message.GuildQuit);
        break;
      case CommunityProtocol.Reply.GuildRemove:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new GuildMemberRemovedMessage(br.ReadUInt32(), br.ReadBoolean()));
        break;
      case CommunityProtocol.Reply.GuildInvite:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new GuildInviteMessage(br.ReadUInt32(), br.ReadString(), br.ReadUInt32()));
        break;
      case CommunityProtocol.Reply.GuildInfo:
        uint guildId = br.ReadUInt32();
        string guildName = br.ReadString();
        List<RankDefinition> ranks = new List<RankDefinition>();
        int num4 = br.ReadLength();
        for (int index = 0; index < num4; ++index)
        {
          GuildRole id = (GuildRole) br.ReadByte();
          string name = br.ReadString();
          ulong permissions = br.ReadUInt64();
          ranks.Add(new RankDefinition(id, name, permissions));
        }
        List<GuildMemberInfo> members = new List<GuildMemberInfo>();
        int num5 = br.ReadLength();
        for (int index = 0; index < num5; ++index)
          members.Add(this.ReadGuildMemberInfo(br));
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new GuildInfoMessage(guildId, guildName, ranks, members));
        break;
      case CommunityProtocol.Reply.GuildSetPromotion:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new GuildMemberPromotionMessage(br.ReadUInt32(), (GuildRole) br.ReadByte()));
        break;
      case CommunityProtocol.Reply.GuildMemberUpdate:
        FacadeFactory.GetInstance().SendMessage(Message.GuildMemberUpdate, (object) this.ReadGuildMemberInfo(br));
        break;
      case CommunityProtocol.Reply.GuildSetChangeRankName:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new GuildRankNameChangedMessage((GuildRole) br.ReadByte(), br.ReadString()));
        break;
      case CommunityProtocol.Reply.GuildSetChangePermissions:
        Game.Me.Guild._UpdateRankPermissions((GuildRole) br.ReadByte(), br.ReadUInt64());
        break;
      case CommunityProtocol.Reply.GuildStartError:
        Game.GUIManager.Find<GuiWingsMainPanel>().Find<GuiWingsCreationPanel>().NameIsAlreadyTaken(br.ReadString());
        break;
      case CommunityProtocol.Reply.GuildJoinError:
        string str3 = br.ReadString();
        switch (br.ReadByte())
        {
          case 1:
            Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox(BsgoLocalization.Get("%$bgo.guild.wing_doesnt_exists%", (object) str3));
            return;
          case 2:
            Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox(BsgoLocalization.Get("%$bgo.guild.already_joint%", (object) str3));
            return;
          case 3:
            Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox(BsgoLocalization.Get("%$bgo.guild.wrong_guild_faction%", (object) str3));
            return;
          default:
            Log.Add("GuildJoinError: UNKNOWN REASON");
            return;
        }
      case CommunityProtocol.Reply.GuildInviteResult:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new GuildInviteResultMessage(br.ReadUInt32(), (GuildInviteResult) br.ReadByte()));
        break;
      case CommunityProtocol.Reply.GuildOperationResult:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new GuildOperationResultMessage((GuildOperation) br.ReadUInt32(), (GuildOperationResult) br.ReadByte()));
        break;
      case CommunityProtocol.Reply.Recruits:
        Game.Me.Friends.SetRecruits(br.ReadDescList<RecruitDesc>());
        break;
      case CommunityProtocol.Reply.PartyAnchor:
        uint index3 = br.ReadUInt32();
        uint index4 = br.ReadUInt32();
        bool flag1 = br.ReadBoolean();
        if ((int) index4 == (int) Game.Me.ServerID)
          break;
        bool flag2 = false;
        foreach (Player member in Game.Me.Party.Members)
        {
          if ((int) member.ServerID != (int) index3)
            ;
          if ((int) member.ServerID == (int) index4)
            flag2 = true;
        }
        if (flag1)
        {
          Game.Players[index4].AnchorTo(index3);
          if (!Game.Me.Party.Anchored.ContainsKey(index3))
            Game.Me.Party.Anchored.Add(index3, new List<uint>());
          if (!Game.Me.Party.Anchored[index3].Contains(index4))
            Game.Me.Party.Anchored[index3].Add(index4);
          Game.Me.Party.NonAnchored.Remove(index4);
        }
        else
        {
          Game.Players[index4].Unanchor();
          if (Game.Me.Party.Anchored.ContainsKey(index3))
            Game.Me.Party.Anchored[index3].Remove(index4);
          if (flag2 && !Game.Me.Party.NonAnchored.Contains(index4))
            Game.Me.Party.NonAnchored.Add(index4);
        }
        if ((int) index3 != (int) Game.Me.ServerID || !Game.Me.Party.Anchored.ContainsKey(Game.Me.ServerID))
          break;
        if (Game.Me.Party.Anchored[Game.Me.ServerID].Count > 0)
        {
          GuiDockUndock.ShowLaunchStrikes();
          break;
        }
        GuiDockUndock.HideLaunchStrikes();
        break;
      case CommunityProtocol.Reply.PartyChatInviteFailed:
        Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.chat.message_invite_user_not_exists%", (object) br.ReadString()));
        break;
      case CommunityProtocol.Reply.RecruitLevel:
        Game.Me.Friends.RequiredRecruitLevel = (int) br.ReadUInt32();
        break;
      case CommunityProtocol.Reply.PartyMemberFtlState:
        List<Tuple<Player, PartyMemberFtlState>> players = new List<Tuple<Player, PartyMemberFtlState>>();
        int num6 = (int) br.ReadUInt16();
        for (int index1 = 0; index1 < num6; ++index1)
        {
          Tuple<Player, PartyMemberFtlState> tuple = new Tuple<Player, PartyMemberFtlState>(Game.Players[br.ReadUInt32()], (PartyMemberFtlState) br.ReadByte());
          players.Add(tuple);
        }
        GUIGroupJumpWindow guiGroupJumpWindow = Game.GUIManager.Find<GUIGroupJumpWindow>();
        if (guiGroupJumpWindow == null)
          break;
        guiGroupJumpWindow.SetPlayerFtlState(players);
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Community protocol: " + (object) reply);
        break;
    }
  }

  private GuildMemberInfo ReadGuildMemberInfo(BgoProtocolReader reader)
  {
    uint num = reader.ReadUInt32();
    string playerName = reader.ReadString();
    byte playerLevel = reader.ReadByte();
    GuildRole playerRole = (GuildRole) reader.ReadByte();
    DateTime lastLogout = reader.ReadLongDateTime();
    reader.ProcessLocation(num);
    return new GuildMemberInfo(num, playerName, playerLevel, playerRole, lastLogout);
  }

  public enum Request : ushort
  {
    PartyInvitePlayer = 1,
    PartyDismissPlayer = 2,
    PartyLeave = 3,
    PartyAppointLeader = 4,
    PartyAccept = 5,
    FriendInvite = 6,
    FriendAccept = 7,
    FriendRemove = 8,
    IgnoreAdd = 9,
    IgnoreRemove = 10,
    ChatConnected = 11,
    ChatFleetAllowed = 12,
    ChatAuthFailed = 13,
    GuildStart = 14,
    GuildLeave = 15,
    GuildChangeRankName = 16,
    GuildChangeRankPermissions = 17,
    GuildPromote = 18,
    GuildKick = 19,
    GuildInvite = 21,
    GuildAccept = 22,
    RecruitInvited = 24,
    PartyChatInvite = 32,
    RecruitLevel = 33,
    PartyMemberFtlState = 34,
    IgnoreClear = 35,
  }

  public enum Reply : ushort
  {
    Party = 1,
    PartyIgnore = 2,
    PartyInvite = 3,
    FriendInvite = 4,
    FriendAccept = 5,
    FriendRemove = 6,
    FriendAdd = 7,
    IgnoreAdd = 8,
    IgnoreRemove = 9,
    ChatSessionId = 10,
    ChatFleetAllowed = 11,
    GuildQuit = 12,
    GuildRemove = 13,
    GuildInvite = 14,
    GuildInfo = 15,
    GuildSetPromotion = 16,
    GuildMemberUpdate = 17,
    GuildSetChangeRankName = 18,
    GuildSetChangePermissions = 19,
    GuildStartError = 21,
    GuildJoinError = 22,
    GuildInviteResult = 23,
    GuildOperationResult = 24,
    Recruits = 26,
    ActivateJumpTargetTransponder = 37,
    CancelJumpTargetTransponder = 38,
    PartyAnchor = 39,
    PartyChatInviteFailed = 40,
    RecruitLevel = 41,
    PartyMemberFtlState = 42,
  }

  public enum GuildJoinErrorReason : byte
  {
    GuildNotFound = 1,
    AlreadyJoined = 2,
    WrongFaction = 3,
  }
}
