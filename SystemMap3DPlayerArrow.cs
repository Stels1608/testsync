﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DPlayerArrow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using SystemMap3D;
using UnityEngine;

public class SystemMap3DPlayerArrow : MonoBehaviour
{
  private const float FADE_OUT_TIME = 1f;
  private const float FADE_IN_TIME = 2f;
  private LTDescr alphaFadeProcedure;

  public void Show(TransitionMode transitionMode)
  {
    this.SetFactionColor();
    this.AbortCurrentAlphaTween();
    this.AlphaTweenTo(1f, transitionMode != TransitionMode.Instant ? 2f : 0.01f).from.x = 0.0f;
    this.gameObject.SetActive(true);
    this.StopAllCoroutines();
  }

  public void Hide(TransitionMode transitionMode)
  {
    this.AbortCurrentAlphaTween();
    float num = transitionMode != TransitionMode.Instant ? 1f : 0.01f;
    this.AlphaTweenTo(0.0f, num).from.x = 1f;
    if (!this.gameObject.activeSelf)
      return;
    this.StopAllCoroutines();
    this.StartCoroutine(this.DeactivateDelayed(num));
  }

  [DebuggerHidden]
  private IEnumerator DeactivateDelayed(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SystemMap3DPlayerArrow.\u003CDeactivateDelayed\u003Ec__Iterator47() { delay = delay, \u003C\u0024\u003Edelay = delay, \u003C\u003Ef__this = this };
  }

  private void SetFactionColor()
  {
    Color color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
    foreach (Renderer componentsInChild in this.transform.GetComponentsInChildren<Renderer>())
      componentsInChild.material.SetColor("_Color", color);
  }

  private void AbortCurrentAlphaTween()
  {
    if (this.alphaFadeProcedure == null)
      return;
    LeanTween.cancel(this.alphaFadeProcedure.id);
  }

  private LTDescr AlphaTweenTo(float alpha, float time)
  {
    this.alphaFadeProcedure = LeanTween.alpha(this.gameObject, alpha, time);
    this.alphaFadeProcedure.onComplete = (System.Action) (() => this.alphaFadeProcedure = (LTDescr) null);
    return this.alphaFadeProcedure;
  }
}
