﻿// Decompiled with JetBrains decompiler
// Type: DutyCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class DutyCard : Card
{
  public byte Level;
  public byte MaxLevel;
  public CounterCard CounterCard;
  public int CounterValue;
  public int Experience;
  public TitleCard TitleCard;
  public DutyCard NextCard;

  public string Counter
  {
    get
    {
      return BsgoLocalization.Get("bgo.text_counters." + this.CounterCard.Name);
    }
  }

  public DutyCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.Level = r.ReadByte();
    this.MaxLevel = r.ReadByte();
    uint cardGUID1 = r.ReadGUID();
    uint cardGUID2 = r.ReadGUID();
    this.CounterValue = r.ReadInt32();
    this.Experience = r.ReadInt32();
    this.TitleCard = (TitleCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.Title);
    this.CounterCard = (CounterCard) Game.Catalogue.FetchCard(cardGUID2, CardView.Counter);
    this.IsLoaded.Depend((ILoadable) this.CounterCard, (ILoadable) this.TitleCard);
    if ((int) cardGUID1 == 0)
      return;
    this.NextCard = (DutyCard) Game.Catalogue.FetchCard(cardGUID1, CardView.Duty);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.NextCard
    });
  }
}
