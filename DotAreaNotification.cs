﻿// Decompiled with JetBrains decompiler
// Type: DotAreaNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class DotAreaNotification : GuiPanel
{
  private float interval = 1.5f;
  private GUIImage backgroundPicture;
  private GuiLabel Headline;
  private GuiLabel Subtitle;
  private GuiLabel Warning;
  private float activationTime;

  public DotAreaNotification()
    : base("GUI/MessageBox/dotarea_warning")
  {
    this.MouseTransparent = true;
    this.Headline = new GuiLabel(this.Find<GuiLabel>("m_Headline"));
    this.Headline.Text = BsgoLocalization.Get("bgo.Dot_Area_Warning.1.headline");
    this.Subtitle = new GuiLabel(this.Find<GuiLabel>("m_Subtitle"));
    this.Subtitle.Text = BsgoLocalization.Get("bgo.Dot_Area_Warning.1.subtitle");
    this.Warning = new GuiLabel(this.Find<GuiLabel>("m_Warning"));
    this.Warning.Text = BsgoLocalization.Get("bgo.Dot_Area_Warning.1.warning");
    this.Headline.LocalizeElement();
    this.Subtitle.LocalizeElement();
    this.Warning.LocalizeElement();
    Debug.LogError((object) ("GET FROM LOCALIZATION == " + BsgoLocalization.Get("bgo.Dot_Area_Warning.1.headline")));
    this.IsRendered = false;
  }

  public void Show()
  {
    this.Position = new Vector2((float) (Screen.width / 2) - this.Size.x / 2f, (float) ((double) Screen.height - (double) this.Size.y - 230.0));
    this.activationTime = Time.time + this.interval;
    this.IsRendered = true;
  }

  public override void Update()
  {
    if ((double) Time.time > (double) this.activationTime)
      this.IsRendered = false;
    base.Update();
  }
}
