﻿// Decompiled with JetBrains decompiler
// Type: GUIShopUninstallAllWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUIShopUninstallAllWindow : GUIPanel
{
  public GUIShopUninstallAllWindow(SmartRect parent)
    : base(parent)
  {
    Texture2D texture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/buybox");
    GUIImageNew guiImageNew = new GUIImageNew(texture);
    this.root.Width = guiImageNew.Rect.width;
    this.root.Height = guiImageNew.Rect.height;
    this.AddPanel((GUIPanel) guiImageNew);
    GUILabelNew guiLabelNew = new GUILabelNew("%$bgo.shop.uninstall_all_confirm%");
    guiLabelNew.AutoSize = false;
    guiLabelNew.PositionY = -30f;
    guiLabelNew.Size = new float2((float) (texture.width - 60), 80f);
    guiLabelNew.WordWrap = true;
    guiLabelNew.Alignment = TextAnchor.UpperCenter;
    this.AddPanel((GUIPanel) guiLabelNew);
    GUIButtonNew guiButtonNew1 = new GUIButtonNew("%$bgo.common.yes%");
    guiButtonNew1.Position = new float2(-66f, 60f);
    guiButtonNew1.Handler = new AnonymousDelegate(this.OnOkButton);
    this.AddPanel((GUIPanel) guiButtonNew1);
    GUIButtonNew guiButtonNew2 = new GUIButtonNew("%$bgo.common.no%");
    guiButtonNew2.Handler = new AnonymousDelegate(this.OnCancelButton);
    guiButtonNew2.Position = new float2(66f, 60f);
    this.AddPanel((GUIPanel) guiButtonNew2);
    this.IsRendered = true;
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    base.OnMouseDown(mousePosition, mouseKey);
    return this.IsRendered;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    base.OnMouseUp(mousePosition, mouseKey);
    return this.IsRendered;
  }

  public override void Show()
  {
    this.IsRendered = true;
    Game.InputDispatcher.Focused = (InputListener) this;
  }

  public override void Hide()
  {
    this.IsRendered = false;
    Game.InputDispatcher.Focused = (InputListener) null;
  }

  private void OnCancelButton()
  {
    this.Hide();
  }

  private void OnOkButton()
  {
    foreach (ShipSlot slot in Game.Me.ActiveShip.Slots)
    {
      if (slot.System != null && slot.System.Card.UserUpgradeable)
        slot.System.MoveTo((IContainer) Game.Me.Locker);
    }
    this.Hide();
  }
}
