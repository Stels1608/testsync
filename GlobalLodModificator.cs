﻿// Decompiled with JetBrains decompiler
// Type: GlobalLodModificator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class GlobalLodModificator
{
  private int textureModificatorLevelCount = 3;
  private int maxLodValue = 20;
  private int minLodFPS = 35;
  private int maxLodFPS = 62;
  private float timeToSwitchLod = 5f;
  private int textureQualityUpLevel = 9;
  private int textureQualityDownLevel = 15;
  public int lodModificator = 1;
  private int maxTextureQualityLOD = 4;
  public bool bEnableBulletExplosions = true;
  public bool bEnableStarDust = true;
  public bool bEnableStarFog = true;
  public bool ModifyGameEffects;
  public bool performGlobalLodModification;
  public bool ModifyTextureLevel;
  private float lastFpsToLodThink;
  private int textureQualityModificatorCounter;
  private int minTextureQualityLOD;
  private int iNumFPS;
  public int iFPS;
  public float fLastFpsCounting;

  public void OnGameInit()
  {
    this.minTextureQualityLOD = QualitySettings.masterTextureLimit;
    this.maxTextureQualityLOD = this.minTextureQualityLOD + 3;
  }

  public int debugGetModelModificator()
  {
    return this.lodModificator;
  }

  public int debugGetTextureModificator()
  {
    return QualitySettings.masterTextureLimit - this.minTextureQualityLOD;
  }

  public string debugGetEffectState()
  {
    string str = "Performance-based effect switch ";
    if (this.ModifyGameEffects)
      return str + "enabled" + ", StarDust: " + (object) this.bEnableStarDust + ", StarFog: " + (object) this.bEnableStarFog + ", BulletExplosion: " + (object) this.bEnableBulletExplosions;
    return str + "disabled";
  }

  public void FinalizeLevel()
  {
    this.lastFpsToLodThink = Time.time;
    this.lodModificator = 1;
    QualitySettings.masterTextureLimit = this.minTextureQualityLOD;
    this.textureQualityModificatorCounter = 0;
  }

  public void Update()
  {
    if (this.iNumFPS == 0)
      this.iNumFPS = Time.frameCount;
    this.fLastFpsCounting += Time.deltaTime;
    if ((double) this.fLastFpsCounting >= 1.0)
    {
      this.iNumFPS = Time.frameCount - this.iNumFPS;
      this.iFPS = Convert.ToInt32((float) this.iNumFPS / this.fLastFpsCounting);
      this.iNumFPS = Time.frameCount;
      this.fLastFpsCounting = 0.0f;
    }
    if ((double) this.lastFpsToLodThink < (double) Time.time - (double) this.timeToSwitchLod && this.iFPS > 0)
    {
      this.lastFpsToLodThink = Time.time;
      if (this.iFPS < this.minLodFPS && this.lodModificator < this.maxLodValue)
      {
        ++this.lodModificator;
        this.lastFpsToLodThink = Time.time;
        if (this.lodModificator >= this.textureQualityDownLevel && this.ModifyTextureLevel)
        {
          ++this.textureQualityModificatorCounter;
          if (this.textureQualityModificatorCounter == this.textureModificatorLevelCount)
          {
            this.textureQualityModificatorCounter = 0;
            if (QualitySettings.masterTextureLimit < this.maxTextureQualityLOD)
              ++QualitySettings.masterTextureLimit;
          }
        }
      }
      if (this.iFPS > this.maxLodFPS && this.lodModificator != 1)
      {
        --this.lodModificator;
        if (this.lodModificator <= this.textureQualityUpLevel && this.ModifyTextureLevel && QualitySettings.masterTextureLimit > this.minTextureQualityLOD)
          --QualitySettings.masterTextureLimit;
      }
    }
    this.bEnableBulletExplosions = this.lodModificator < 4;
    this.bEnableStarDust = this.lodModificator < 5;
    if (this.lodModificator >= 6)
      this.bEnableStarFog = false;
    else
      this.bEnableStarFog = true;
  }
}
