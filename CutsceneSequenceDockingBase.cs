﻿// Decompiled with JetBrains decompiler
// Type: CutsceneSequenceDockingBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class CutsceneSequenceDockingBase : CutsceneSequenceBase
{
  protected string galacticaPrefabName = "galactica";
  protected string basestarPrefabName = "basestar";
  protected string galacticaStandardName = "cutscene_galactica_landingscene_setup";
  protected string basestarStandardName = "cutscene_basestar_landingscene_setup";
  private SpaceObject dockingTarget;
  private GameObject galactica;
  private GameObject shipCopy;

  public VideoUiHandler UiHandler
  {
    get
    {
      return this.CutsceneSetup.CutsceneEvents.UiHandler;
    }
  }

  protected override bool ExecuteCustomPreparations()
  {
    if (this.cutscenePlayRequestData.TargetObject != null)
    {
      this.dockingTarget = this.cutscenePlayRequestData.TargetObject;
      if (this.dockingTarget.PrefabName == this.GetDockingTargetPrefabName())
      {
        this.galactica = this.dockingTarget.Model;
        GameObject gameObject = this.CutsceneSetup.gameObject;
        gameObject.transform.position = this.galactica.transform.position;
        gameObject.transform.rotation = this.galactica.transform.rotation * gameObject.transform.rotation;
        this.shipCopy = this.CreatePlayerShipCopy();
        this.CutsceneSetup.PlayerShipLocator.SetVisible(false);
        this.shipCopy.transform.position = this.CutsceneSetup.PlayerShipLocator.transform.position;
        this.shipCopy.transform.rotation = this.CutsceneSetup.PlayerShipLocator.transform.rotation;
        this.shipCopy.transform.parent = this.CutsceneSetup.PlayerShipLocator.transform;
        this.ShowRealPlayerShip(false);
        if ((Object) this.CutsceneSetup.AnimationChild == (Object) null)
        {
          Debug.LogError((object) "CutsceneSequenceDockingBase: cutscene setup's animation child is null. Aborting...");
          return false;
        }
        if (!((Object) this.CutsceneSetup.AnimationChild.GetClip(this.GetAnimClipName()) == (Object) null))
          return this.InitAdditionalNeededObjects();
        Debug.LogError((object) ("CutsceneSequenceDockingBase: Clip " + this.GetAnimClipName() + " doesn't exist in animation component of child \"" + this.CutsceneSetup.AnimationChild.name + "\". Aborting..."));
        return false;
      }
      Debug.LogError((object) ("CutsceneSequenceDockingBase: DockingTarget is not " + this.GetDockingTargetPrefabName() + ". Aborting..."));
      return false;
    }
    Debug.LogError((object) "CutsceneSequenceDockingBase: No docking target passed. Aborting...");
    return false;
  }

  protected abstract bool InitAdditionalNeededObjects();

  protected abstract string GetDockingTargetPrefabName();

  protected override void StartScene()
  {
    this.ActiveCamera = this.CutsceneSetup.CutsceneCamera;
    CutsceneVideoFx.FadeInVideoStripes(this.CutsceneSetup.CutsceneEvents.UiHandler, 1f);
    this.ShowRandomDockingSubtitles();
    this.CutsceneSetup.AnimationChild.Play(this.GetAnimClipName());
  }

  protected abstract string GetAnimClipName();

  protected override void Cleanup()
  {
    if ((Object) this.CutsceneSetup != (Object) null)
      Object.Destroy((Object) this.CutsceneSetup);
    if ((Object) this.shipCopy != (Object) null)
      Object.Destroy((Object) this.shipCopy);
    this.ShowRealPlayerShip(true);
  }

  protected abstract void ShowRandomDockingSubtitles();
}
