﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorBracketBigUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HudIndicatorBracketBigUgui : HudIndicatorBaseComponentUgui, IReceivesSetting, IReceivesTarget, IRequiresLateUpdate
{
  private bool showIconSetting = true;
  public const float BracketSmallWidthMinPixels = 19f;
  public const float BracketBigWidthMinPixels = 60f;
  public const float BracketWidthMaxPixels = 270f;
  public const float BracketRatio = 0.6666667f;
  public const float NoRangeBlinkDuration = 2.2f;
  private const string NO_RANGE_TIMER_NAME = "DelayedTargetDeselection";
  [SerializeField]
  private Image bracketImage;
  [SerializeField]
  private GameObject selectionImagesRoot;
  [SerializeField]
  private Image centerCrosshair;
  [SerializeField]
  private Image cornerSelectionImage;
  [SerializeField]
  private Image mapIcon;
  [SerializeField]
  private TextMeshProUGUI noRangeLabel;
  private ISpaceEntity target;
  private float noRangeBlinkStopTime;
  private Timer blinkTimer;

  public ISpaceEntity Target
  {
    get
    {
      return this.target;
    }
  }

  private bool IsCloaked
  {
    get
    {
      if (this.target.TargetType == TargetType.SpaceObject)
        return ((SpaceObject) this.target).IsCloaked;
      return false;
    }
  }

  protected override void ResetComponentStates()
  {
    this.target = (ISpaceEntity) null;
    this.EnableNoRangeLabel(false);
    if ((Object) this.blinkTimer != (Object) null)
      Object.Destroy((Object) this.blinkTimer.gameObject);
    this.noRangeBlinkStopTime = 0.0f;
  }

  protected override void Awake()
  {
    base.Awake();
  }

  private void EnableNoRangeLabel(bool enable)
  {
    if (!((Object) this.noRangeLabel != (Object) null))
      return;
    this.noRangeLabel.gameObject.SetActive(enable);
    this.noRangeLabel.enabled = enable;
  }

  public void RemoteLateUpdate()
  {
    this.UpdateBracketSize();
  }

  private void UpdateBracketSize()
  {
    if (this.InMiniMode)
      return;
    (this.transform as RectTransform).sizeDelta = new Vector2(this.HudIndicator.BracketSize.x, this.HudIndicator.BracketSize.y);
  }

  protected virtual void UpdateBracket()
  {
    bool flag1 = (this.IsMultiselected || this.IsSelected || this.InScannerRange) && this.InScreen;
    bool flag2 = (this.IsMultiselected || this.IsSelected) && this.InScreen;
    this.bracketImage.enabled = flag1;
    this.selectionImagesRoot.gameObject.SetActive(flag2);
    this.mapIcon.gameObject.SetActive(this.showIconSetting && flag1 && !this.InMiniMode && (this.target != null && (Object) this.target.IconBrackets != (Object) null));
    if (this.InMiniMode)
    {
      if ((Object) this.bracketImage.sprite != (Object) this.target.IconBrackets)
      {
        this.bracketImage.sprite = this.target.IconBrackets;
        this.bracketImage.SetNativeSize();
        if ((Object) this.target.IconBrackets == (Object) null)
          this.bracketImage.rectTransform.sizeDelta = Vector2.zero;
      }
    }
    else
    {
      UnityEngine.Sprite sprite = !HudIndicatorInfo.IsSmallProjectile(this.target) || this.IsSelected ? HudIndicatorsSpriteLinker.Instance.Brackets : HudIndicatorsSpriteLinker.Instance.MissileBrackets;
      if ((Object) this.bracketImage.sprite != (Object) sprite)
        this.bracketImage.sprite = sprite;
    }
    this.SetColoring();
  }

  protected override void UpdateView()
  {
    this.UpdateBracket();
  }

  public override void OnScannerVisibilityChange(bool targetIsInScannerRange)
  {
    base.OnScannerVisibilityChange(targetIsInScannerRange);
    this.HandleRangeBlinking();
  }

  private void HandleRangeBlinking()
  {
    if (this.InScannerRange || !this.IsCloaked)
      return;
    this.noRangeBlinkStopTime = Time.time + 2.2f;
    if ((Object) this.bracketImage == (Object) null)
      Debug.LogError((object) ("(LGP-5691) BracketSprite is null. This: " + (object) this + ". This gameObject is null? " + (object) ((Object) this.gameObject == (Object) null)));
    this.EnableNoRangeLabel(false);
    if ((Object) this.blinkTimer == (Object) null)
    {
      this.blinkTimer = Timer.CreateTimer("DelayedTargetDeselection", 0.1f, 0.3f, new Timer.TickHandler(this.RangeBlinkHandler));
      this.blinkTimer.OnFinish = (Timer.OnFinishHandler) (() =>
      {
        this.UpdateBracket();
        this.EnableNoRangeLabel(false);
      });
    }
    this.blinkTimer.DestructionTime = this.noRangeBlinkStopTime;
  }

  private void RangeBlinkHandler()
  {
    if ((Object) this.bracketImage == (Object) null || (Object) this.noRangeLabel == (Object) null)
      return;
    this.bracketImage.enabled = !this.bracketImage.enabled && this.InScreen;
    this.EnableNoRangeLabel((double) Time.time > (double) this.noRangeBlinkStopTime - 1.32000005245209 && this.InScreen);
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.target = target;
    this.mapIcon.sprite = target.IconBrackets;
    this.mapIcon.SetNativeSize();
    this.UpdateColorAndAlpha();
  }

  protected override void SetColoring()
  {
    if (this.target.SpaceEntityType == SpaceEntityType.SectorEvent)
      this.bracketImage.color = TargetColors.sectorEventColor;
    else
      this.bracketImage.color = !this.InMiniMode ? HudIndicatorColorScheme.Instance.BracketColor(this.target) : HudIndicatorColorScheme.Instance.IconColor(this.target);
    this.mapIcon.color = HudIndicatorColorScheme.Instance.IconColor(this.target);
    if ((Object) this.noRangeLabel != (Object) null)
      this.noRangeLabel.color = HudIndicatorColorScheme.Instance.TextColor(this.target);
    Image image = this.centerCrosshair;
    Color color1 = HudIndicatorColorScheme.Instance.SelectionColor(this.target);
    this.cornerSelectionImage.color = color1;
    Color color2 = color1;
    image.color = color2;
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  public void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    switch (userSetting)
    {
      case UserSetting.HudIndicatorColorScheme:
        this.SetColoring();
        break;
      case UserSetting.HudIndicatorShowShipTierIcon:
        this.showIconSetting = (bool) data;
        this.UpdateBracket();
        break;
      case UserSetting.HudIndicatorSelectionCrosshair:
        this.centerCrosshair.gameObject.SetActive((bool) data);
        break;
    }
  }
}
