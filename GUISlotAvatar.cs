﻿// Decompiled with JetBrains decompiler
// Type: GUISlotAvatar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUISlotAvatar : GUIPanel
{
  private Rect innerRect = new Rect(0.0f, 0.0f, 1f, 1f);
  private Texture2D texture;

  public Texture2D Texture
  {
    get
    {
      return this.texture;
    }
    set
    {
      if ((Object) this.texture == (Object) value)
        return;
      this.texture = value;
    }
  }

  public GUISlotAvatar()
  {
    this.IsRendered = true;
  }

  public override void Draw()
  {
    if (Event.current.type != UnityEngine.EventType.Repaint || !((Object) this.texture != (Object) null))
      return;
    Graphics.DrawTexture(this.SmartRect.AbsRect, (UnityEngine.Texture) this.texture, this.innerRect, 0, 0, 0, 0);
  }
}
