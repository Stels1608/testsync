﻿// Decompiled with JetBrains decompiler
// Type: TurnQweasdManeuver
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class TurnQweasdManeuver : Maneuver
{
  protected QWEASD qweasd;

  public override string ToString()
  {
    return string.Format("TurnQweasdManeuver: pitch={0}, yaw={1}, roll{2}", (object) this.qweasd.Pitch, (object) this.qweasd.Yaw, (object) this.qweasd.Roll);
  }

  public override MovementFrame NextFrame(Tick tick, MovementFrame prevFrame)
  {
    if (!prevFrame.valid)
      return MovementFrame.Invalid;
    return Simulation.QWEASD(prevFrame, (float) this.qweasd.Pitch, (float) this.qweasd.Yaw, (float) this.qweasd.Roll, this.options);
  }

  public override void Read(BgoProtocolReader pr)
  {
    base.Read(pr);
    this.startTick = pr.ReadTick();
    this.qweasd = new QWEASD((int) pr.ReadByte());
    this.options.Read(pr);
  }
}
