﻿// Decompiled with JetBrains decompiler
// Type: UIDragResize
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Drag-Resize Widget")]
public class UIDragResize : MonoBehaviour
{
  public UIWidget.Pivot pivot = UIWidget.Pivot.BottomRight;
  public int minWidth = 100;
  public int minHeight = 100;
  public int maxWidth = 100000;
  public int maxHeight = 100000;
  public UIWidget target;
  private Plane mPlane;
  private Vector3 mRayPos;
  private Vector3 mLocalPos;
  private int mWidth;
  private int mHeight;
  private bool mDragging;

  private void OnDragStart()
  {
    if (!((Object) this.target != (Object) null))
      return;
    Vector3[] worldCorners = this.target.worldCorners;
    this.mPlane = new Plane(worldCorners[0], worldCorners[1], worldCorners[3]);
    Ray currentRay = UICamera.currentRay;
    float enter;
    if (!this.mPlane.Raycast(currentRay, out enter))
      return;
    this.mRayPos = currentRay.GetPoint(enter);
    this.mLocalPos = this.target.cachedTransform.localPosition;
    this.mWidth = this.target.width;
    this.mHeight = this.target.height;
    this.mDragging = true;
  }

  private void OnDrag(Vector2 delta)
  {
    if (!this.mDragging || !((Object) this.target != (Object) null))
      return;
    Ray currentRay = UICamera.currentRay;
    float enter;
    if (!this.mPlane.Raycast(currentRay, out enter))
      return;
    Transform cachedTransform = this.target.cachedTransform;
    cachedTransform.localPosition = this.mLocalPos;
    this.target.width = this.mWidth;
    this.target.height = this.mHeight;
    Vector3 vector3_1 = currentRay.GetPoint(enter) - this.mRayPos;
    cachedTransform.position = cachedTransform.position + vector3_1;
    Vector3 vector3_2 = Quaternion.Inverse(cachedTransform.localRotation) * (cachedTransform.localPosition - this.mLocalPos);
    cachedTransform.localPosition = this.mLocalPos;
    NGUIMath.ResizeWidget(this.target, this.pivot, vector3_2.x, vector3_2.y, this.minWidth, this.minHeight, this.maxWidth, this.maxHeight);
  }

  private void OnDragEnd()
  {
    this.mDragging = false;
  }
}
