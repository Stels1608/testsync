﻿// Decompiled with JetBrains decompiler
// Type: DutyUpdatedNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class DutyUpdatedNotification : OnScreenNotification
{
  private readonly Duty duty;

  public Duty Duty
  {
    get
    {
      return this.duty;
    }
  }

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.DutyUpdated;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Neutral;
    }
  }

  public override string TextMessage
  {
    get
    {
      return string.Format("{0}: {1}/{2}", (object) this.duty.Card.Counter, (object) this.duty.CounterValue, (object) this.duty.NextCounterValue);
    }
  }

  public override bool Show
  {
    get
    {
      return false;
    }
  }

  public DutyUpdatedNotification(Duty duty)
  {
    this.duty = duty;
    FacadeFactory.GetInstance().SendMessage(Message.CombatLogOk, (object) this.TextMessage);
  }
}
