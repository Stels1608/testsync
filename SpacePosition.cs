﻿// Decompiled with JetBrains decompiler
// Type: SpacePosition
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class SpacePosition : ISpaceEntity
{
  private GameObject model;
  private Vector3 _position;

  public Vector3 Position
  {
    get
    {
      return this._position;
    }
    set
    {
      this._position = value;
      this.Root.position = value;
    }
  }

  public TargetType TargetType
  {
    get
    {
      return TargetType.SpacePosition;
    }
  }

  public virtual bool IsInMapRange
  {
    get
    {
      return true;
    }
  }

  public virtual string ObjectName { get; set; }

  public virtual string Name { get; set; }

  public Transform Root { get; private set; }

  public bool IsMe
  {
    get
    {
      return false;
    }
  }

  public bool IsTargetable
  {
    get
    {
      return false;
    }
  }

  public bool IsPlayer
  {
    get
    {
      return false;
    }
  }

  public bool IsHostileCongener
  {
    get
    {
      return false;
    }
  }

  public Faction Faction
  {
    get
    {
      return Faction.Neutral;
    }
  }

  public Relation PlayerRelation
  {
    get
    {
      return RelationHelper.GetRelation((ISpaceEntity) this, Game.Me.Faction, Game.Me.FactionGroup, TargetBracketMode.Default);
    }
  }

  public FactionGroup FactionGroup
  {
    get
    {
      return FactionGroup.Group0;
    }
  }

  public bool Dead
  {
    get
    {
      return false;
    }
  }

  public UnityEngine.Sprite Icon3dMap
  {
    get
    {
      if ((int) this.IconIndex < 0)
        return (UnityEngine.Sprite) null;
      return SystemMap3DMapIconLinker.Instance.MapObjectsNPCs[(int) this.IconIndex];
    }
  }

  public UnityEngine.Sprite IconBrackets
  {
    get
    {
      if ((int) this.IconIndex < 0)
        return (UnityEngine.Sprite) null;
      return SystemMap3DMapIconLinker.Instance.MapObjectsNPCs[(int) this.IconIndex];
    }
  }

  protected abstract sbyte IconIndex { get; }

  public abstract SpaceEntityType SpaceEntityType { get; }

  public Bounds MeshBoundsRaw { get; protected set; }

  public bool ShowIndicatorWhenInRange
  {
    get
    {
      return true;
    }
  }

  public virtual bool ForceShowOnMap
  {
    get
    {
      return false;
    }
  }

  public bool IsFriendlyHub
  {
    get
    {
      return false;
    }
  }

  public SpaceObject.MarkObjectType MarkerType
  {
    get
    {
      return SpaceObject.MarkObjectType.None;
    }
  }

  protected SpacePosition()
  {
    this.Root = new GameObject().transform;
    this.Root.name = "SpacePosition";
  }

  public void Remove()
  {
    if (!((Object) this.Root != (Object) null))
      return;
    Object.Destroy((Object) this.Root.gameObject);
  }

  private void DestroyModel()
  {
    if ((Object) this.model != (Object) null)
      Object.Destroy((Object) this.model);
    this.model = (GameObject) null;
  }

  public virtual void SetModel(GameObject prototype)
  {
    this.DestroyModel();
    if ((Object) prototype != (Object) null)
    {
      this.model = (GameObject) Object.Instantiate((Object) prototype, Vector3.zero, Quaternion.identity);
      this.UpdateBounds(this.model);
      this.model.name = prototype.name;
      this.model.transform.parent = this.Root;
      this.model.transform.localPosition = Vector3.zero;
      this.model.transform.localRotation = Quaternion.identity;
      this.model.transform.localScale = Vector3.one;
    }
    this.UpdateBounds(this.model);
  }

  public float GetDistance()
  {
    if (Game.Me.Ship == null)
      return float.MaxValue;
    return (this.Position - Game.Me.Ship.Position).magnitude;
  }

  private void UpdateBounds(GameObject model)
  {
    MeshRenderer[] componentsInChildren = model.GetComponentsInChildren<MeshRenderer>();
    if (componentsInChildren.Length == 0)
      return;
    MeshRenderer meshRenderer1 = componentsInChildren[0];
    foreach (MeshRenderer meshRenderer2 in componentsInChildren)
    {
      if ((double) meshRenderer2.bounds.size.z > (double) meshRenderer1.bounds.size.z)
        meshRenderer1 = meshRenderer2;
    }
    this.MeshBoundsRaw = meshRenderer1.bounds;
  }
}
