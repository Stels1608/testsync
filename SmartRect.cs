﻿// Decompiled with JetBrains decompiler
// Type: SmartRect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SmartRect
{
  private SmartRect parent;
  private Rect currentRect;
  private Rect absRect;
  private float2 position;
  private float2 absPosition;

  public SmartRect Parent
  {
    get
    {
      return this.parent;
    }
    set
    {
      this.parent = value;
      this.RecalculateAbsCoords();
    }
  }

  public float2 Position
  {
    get
    {
      return this.position;
    }
    set
    {
      this.position = value;
      this.currentRect = this.CalcRectAroundPoint(this.Position);
      this.RecalculateAbsCoords();
    }
  }

  public float2 AbsPosition
  {
    get
    {
      return this.absPosition;
    }
  }

  public Rect Rect
  {
    get
    {
      return this.currentRect;
    }
    set
    {
      this.currentRect = value;
      this.position = new float2(this.currentRect.x + this.currentRect.width / 2f, this.currentRect.y + this.currentRect.height / 2f);
      this.RecalculateAbsCoords();
    }
  }

  public float2 Size
  {
    get
    {
      return new float2(this.Width, this.Height);
    }
    set
    {
      this.currentRect.width = value.x;
      this.currentRect.height = value.y;
      this.currentRect = this.CalcRectAroundPoint(this.Position);
      this.RecalculateAbsCoords();
    }
  }

  public float Width
  {
    get
    {
      return this.currentRect.width;
    }
    set
    {
      this.currentRect.width = value;
      this.currentRect = this.CalcRectAroundPoint(this.Position);
      this.RecalculateAbsCoords();
    }
  }

  public float Height
  {
    get
    {
      return this.currentRect.height;
    }
    set
    {
      this.currentRect.height = value;
      this.currentRect = this.CalcRectAroundPoint(this.Position);
      this.RecalculateAbsCoords();
    }
  }

  public Rect AbsRect
  {
    get
    {
      return this.absRect;
    }
  }

  public SmartRect()
    : this((SmartRect) null)
  {
  }

  public SmartRect(SmartRect parent)
  {
    this.Parent = parent;
    this.Position = float2.zero;
    this.Rect = new Rect();
  }

  public SmartRect(Rect initialRect, float2 position, SmartRect parent)
  {
    this.parent = parent;
    this.Rect = new Rect(0.0f, 0.0f, initialRect.width, initialRect.height);
    this.Position = position;
  }

  public void RecalculateAbsCoords()
  {
    this.absPosition = this.CalculateAbsolutePosition();
    this.absRect = this.CalcRectAroundPoint(this.absPosition);
  }

  private Rect CalcRectAroundPoint(float2 point)
  {
    return new Rect(point.x - this.currentRect.width * 0.5f, point.y - this.currentRect.height * 0.5f, this.currentRect.width, this.currentRect.height);
  }

  private float2 CalculateAbsolutePosition()
  {
    if (this.parent != null)
      return this.position + this.parent.CalculateAbsolutePosition();
    return this.position;
  }
}
