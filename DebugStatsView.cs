﻿// Decompiled with JetBrains decompiler
// Type: DebugStatsView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugStatsView : DebugBehaviour<DebugStatsView>
{
  private string info = string.Empty;
  private Vector2 scroll = new Vector2();
  private string[] buttonsName = new string[2]{ "Self", "Target" };
  private int num;

  private void Start()
  {
    this.windowID = 1;
    this.SetSize(300f, 300f);
    this.windowRect = new Rect(0.0f, (float) ((Screen.height - 300) / 2), 300f, 300f);
  }

  protected override void WindowFunc()
  {
    if (GUILayout.Button(this.buttonsName[this.num]))
      this.num = (this.num + 1) % 2;
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.Box(this.info);
    GUILayout.EndScrollView();
  }

  private void Update()
  {
    this.info = Game.Me.Stats.ToString();
  }
}
