﻿// Decompiled with JetBrains decompiler
// Type: DialogManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{
  private List<GameObject> usedDialogOptions = new List<GameObject>();
  private List<Remark> answerList = new List<Remark>();
  private List<Remark> npcTextList = new List<Remark>();
  private int virtualSelection = -1;
  public GameObject dialogHolder;
  public GameObject spacetextHolder;
  private DialogManager.DialogState dialogState;
  private int avaibleAnswers;
  private string npcName;
  private bool continueDialog;

  public string NpcName
  {
    get
    {
      return this.npcName;
    }
    set
    {
      this.npcName = value;
    }
  }

  public void ShowPcMark(List<Remark> pcMark)
  {
    this.answerList = pcMark;
  }

  public void OnStopDialog()
  {
    this.dialogState = DialogManager.DialogState.Waiting;
    this.continueDialog = false;
    this.npcTextList.Clear();
    this.answerList.Clear();
  }

  public void ShowPcAnswerList()
  {
    NGUITools.SetActive(this.spacetextHolder, false);
    this.ClearHolder();
    for (int index = 0; index < this.usedDialogOptions.Count; ++index)
      Object.Destroy((Object) this.usedDialogOptions[index]);
    this.usedDialogOptions.Clear();
    this.virtualSelection = -1;
    this.avaibleAnswers = this.answerList.Count;
    int num = -22;
    for (int index = 0; index < this.answerList.Count; ++index)
    {
      GameObject gameObject = DialogEntity.Create(this.dialogHolder);
      this.usedDialogOptions.Add(gameObject);
      DialogEntity componentInChildren = this.usedDialogOptions[index].GetComponentInChildren<DialogEntity>();
      if ((Object) componentInChildren == (Object) null)
      {
        Debug.LogError((object) "SCRIPT NOT FOUND ON Prefab");
        return;
      }
      componentInChildren.Init(this.answerList[index].phrase, (int) this.answerList[index].index, this.answerList[index].symbol);
      componentInChildren.clickDelegate = new DialogIndexClicked(this.IndexClicked);
      gameObject.transform.localPosition = new Vector3(0.0f, (float) (num - index * 32));
    }
    GameObject gameObject1 = new GameObject("_playerName");
    PositionUtils.CorrectTransform(this.dialogHolder, gameObject1);
    GuiWidgetsFactory.AddLabel(gameObject1, new Vector2(500f, 26f), ColorManager.currentColorScheme.Get(WidgetColorType.DIALOG_NPC_NAME), 10, 16, UIWidget.Pivot.TopLeft, UILabel.Overflow.ResizeHeight).text = Game.Me.Name.ToUpper();
    gameObject1.transform.localPosition = Vector3.zero;
    this.answerList.Clear();
    this.dialogState = DialogManager.DialogState.Waiting;
  }

  private void ClearHolder()
  {
    int childCount = this.dialogHolder.transform.childCount;
    for (int index = 0; index < childCount; ++index)
      Object.Destroy((Object) this.dialogHolder.transform.GetChild(index).gameObject);
  }

  private void ShowNpcText()
  {
    if (this.npcTextList.Count <= 0)
      return;
    this.ClearHolder();
    Remark remark = this.npcTextList[0];
    this.npcTextList.RemoveAt(0);
    DialogNpcEntity.Create(this.npcName.ToUpper(), remark.phrase, this.dialogHolder).GetComponent<DialogNpcEntity>().handleClick = new AnonymousDelegate(this.NpcDialogClick);
    NGUITools.SetActive(this.spacetextHolder, true);
  }

  public void ShowNpcMark(Remark npcMark)
  {
    this.npcTextList.Add(npcMark);
    FacadeFactory.GetInstance().SendMessage(Message.DialogAdvance);
  }

  private void IndexClicked(int index)
  {
    FacadeFactory.GetInstance().SendMessage(Message.DialogClickedIndex, (object) index);
    FacadeFactory.GetInstance().SendMessage(Message.DialogAdvance);
    this.dialogState = DialogManager.DialogState.Waiting;
  }

  private void NpcDialogClick()
  {
    this.continueDialog = true;
  }

  private void Update()
  {
    if (this.dialogState == DialogManager.DialogState.Waiting && this.npcTextList.Count > 0)
    {
      this.dialogState = DialogManager.DialogState.WaintingForSpace;
      this.ShowNpcText();
    }
    if (this.dialogState == DialogManager.DialogState.Waiting && this.npcTextList.Count == 0 && this.answerList.Count > 0)
    {
      this.dialogState = DialogManager.DialogState.Waiting;
      this.ShowPcAnswerList();
    }
    if (this.dialogState == DialogManager.DialogState.Waiting)
    {
      int oldSelected = this.virtualSelection;
      if (Input.GetKeyUp(KeyCode.UpArrow) && this.virtualSelection > 0)
        --this.virtualSelection;
      if (Input.GetKeyUp(KeyCode.DownArrow) && this.virtualSelection < this.avaibleAnswers - 1)
        ++this.virtualSelection;
      if ((Input.GetKeyUp(KeyCode.Alpha1) || Input.GetKeyUp(KeyCode.Keypad1)) && this.usedDialogOptions.Count > 0)
        this.virtualSelection = 0;
      if ((Input.GetKeyUp(KeyCode.Alpha2) || Input.GetKeyUp(KeyCode.Keypad2)) && this.usedDialogOptions.Count > 1)
        this.virtualSelection = 1;
      if ((Input.GetKeyUp(KeyCode.Alpha3) || Input.GetKeyUp(KeyCode.Keypad3)) && this.usedDialogOptions.Count > 2)
        this.virtualSelection = 2;
      if ((Input.GetKeyUp(KeyCode.Alpha4) || Input.GetKeyUp(KeyCode.Keypad4)) && this.usedDialogOptions.Count > 3)
        this.virtualSelection = 3;
      if (oldSelected != this.virtualSelection)
      {
        if (oldSelected != -1)
          this.ChangeColorState(oldSelected, WidgetColorType.DIALOG_ANSWER_COLOR_NORMAL);
        this.ChangeColorState(this.virtualSelection, WidgetColorType.DIALOG_ANSWER_COLOR_HOVER);
      }
      if (Input.GetKeyUp(KeyCode.Space) && this.usedDialogOptions.Count == 1)
        this.usedDialogOptions[0].GetComponentInChildren<DialogEntity>().OnClick();
      if (Input.GetKeyUp(KeyCode.Space) && this.virtualSelection > -1)
        this.usedDialogOptions[this.virtualSelection].GetComponentInChildren<DialogEntity>().OnClick();
    }
    if (this.dialogState != DialogManager.DialogState.WaintingForSpace || !Input.GetKeyUp(KeyCode.Space) && !this.continueDialog)
      return;
    this.continueDialog = false;
    if (this.npcTextList.Count == 0 && this.answerList.Count > 0)
      this.ShowPcAnswerList();
    else
      this.ShowNpcText();
  }

  private void ChangeColorState(int oldSelected, WidgetColorType colorType)
  {
    UILabel componentInChildren1 = this.usedDialogOptions[oldSelected].GetComponentInChildren<UILabel>();
    if ((Object) componentInChildren1 == (Object) null)
      return;
    componentInChildren1.color = ColorManager.currentColorScheme.Get(colorType);
    UISprite componentInChildren2 = this.usedDialogOptions[oldSelected].GetComponentInChildren<UISprite>();
    if ((Object) componentInChildren2 == (Object) null)
      return;
    componentInChildren2.color = ColorManager.currentColorScheme.Get(colorType);
  }

  private enum DialogState
  {
    Waiting,
    WaintingForSpace,
  }
}
