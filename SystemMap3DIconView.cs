﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DIconView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Paths;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using SystemMap3D;
using UnityEngine;

public class SystemMap3DIconView : MonoBehaviour, ISystemMap3DView
{
  private SystemMap3DManager systemMap3DManager;
  private Timer iconTimer;
  private GameObject iconProtoType;
  private Dictionary<ISpaceEntity, SystemMap3DMapIcon> mapIcons;
  private SystemMap3DDradisSphere dradisSphere;
  private SpriteRenderer outerDradisCircle;
  private SystemMap3DPlayerArrow playerArrow;
  private Dictionary<SpaceEntityType, bool> filterSettings;
  private ISpaceEntity hoveredEntity;
  private bool formAsteroidGroupsSetting;
  private SystemMap3DHoverInfo hoverInfo;
  private List<ISpaceEntity> activeWaypoints;
  private IEnumerator _deactivationCoroutine;
  private bool _isRendered;

  public bool IsRendered
  {
    get
    {
      return this._isRendered;
    }
    set
    {
      this._isRendered = value;
      this.gameObject.SetActive(this._isRendered);
      this.UpdateIconVisibilities();
      this.hoverInfo.Show(this._isRendered);
    }
  }

  public bool MouseIsHoveringEntity
  {
    get
    {
      return this.hoveredEntity != null;
    }
  }

  public SystemMap3DHoverInfo HoverInfo
  {
    get
    {
      return this.hoverInfo;
    }
  }

  public static SystemMap3DIconView Create(SystemMap3DManager systemMap3DManager)
  {
    SystemMap3DIconView systemMap3DiconView = new GameObject("SystemMap3DSectorView").AddComponent<SystemMap3DIconView>();
    systemMap3DiconView.Initialize(systemMap3DManager);
    return systemMap3DiconView;
  }

  private void Initialize(SystemMap3DManager systemMap3DManager)
  {
    this.systemMap3DManager = systemMap3DManager;
    this.iconProtoType = Resources.Load<GameObject>(Ui.Prefabs + "/SystemMap3D/SystemMap3DMapIcon");
    this.hoverInfo = UguiTools.CreateChild<SystemMap3DHoverInfo>("SystemMap3D/SystemMap3DHoverInfo", UguiTools.GetCanvas(BsgoCanvas.SystemMapCanvas2D).transform);
    this.hoverInfo.Show(false);
    this.mapIcons = new Dictionary<ISpaceEntity, SystemMap3DMapIcon>();
    this.filterSettings = new Dictionary<SpaceEntityType, bool>();
    this.activeWaypoints = new List<ISpaceEntity>();
  }

  private void LateUpdate()
  {
    if ((Object) SpaceLevel.GetLevel() == (Object) null || SpaceLevel.GetLevel().cameraSwitcher == null)
      return;
    Camera activeCamera = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera();
    Quaternion rotation = activeCamera.transform.rotation;
    using (Dictionary<ISpaceEntity, SystemMap3DMapIcon>.ValueCollection.Enumerator enumerator = this.mapIcons.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.UpdateOrientationAndSize(rotation);
    }
    this.hoverInfo.Show(this.hoveredEntity != null);
    if (this.hoveredEntity == null)
      return;
    this.hoverInfo.transform.position = (Vector3) (Vector2) activeCamera.WorldToScreenPoint(this.hoveredEntity.Position);
    this.hoverInfo.SetEntity(this.hoveredEntity);
  }

  public void TransitionToMap(TransitionMode transitionMode)
  {
    this.StopDelayedDeactivation();
    this.iconTimer = Timer.CreateTimer("MapMarkerUpdater", 0.01f, 0.1f, new Timer.TickHandler(this.UpdateMarkerDisplay));
    this.playerArrow.Show(transitionMode);
    this.IsRendered = true;
  }

  public void TransitionToGame(TransitionMode transitionMode)
  {
    this.playerArrow.Hide(transitionMode);
    this.StartDelayedDeactivation(transitionMode != TransitionMode.Instant ? 1f : 0.0f);
    this.HideIcons();
    this.hoverInfo.HandleTransitionToGame();
    if (!((Object) this.iconTimer != (Object) null))
      return;
    Object.Destroy((Object) this.iconTimer.gameObject);
  }

  public void OnTransitionToGameFinished()
  {
  }

  private void StartDelayedDeactivation(float delay)
  {
    if (!this.gameObject.activeInHierarchy)
      return;
    this._deactivationCoroutine = this.DelayedDeactivationRoutine(delay);
    this.StartCoroutine(this._deactivationCoroutine);
  }

  private void StopDelayedDeactivation()
  {
    if (!this.gameObject.activeInHierarchy || this._deactivationCoroutine == null)
      return;
    this.StopCoroutine(this._deactivationCoroutine);
  }

  [DebuggerHidden]
  private IEnumerator DelayedDeactivationRoutine(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SystemMap3DIconView.\u003CDelayedDeactivationRoutine\u003Ec__Iterator45() { delay = delay, \u003C\u0024\u003Edelay = delay, \u003C\u003Ef__this = this };
  }

  public void ApplySetting(UserSetting setting, object data)
  {
    switch (setting)
    {
      case UserSetting.SystemMap3DShowAsteroids:
        this.SetFilter(SpaceEntityType.Asteroid, (bool) data);
        this.SetFilter(SpaceEntityType.AsteroidGroup, (bool) data);
        break;
      case UserSetting.SystemMap3DShowDynamicMissions:
        this.SetFilter(SpaceEntityType.SectorEvent, (bool) data);
        break;
      case UserSetting.SystemMap3DFormAsteroidGroups:
        this.formAsteroidGroupsSetting = (bool) data;
        break;
    }
  }

  private void SetFilter(SpaceEntityType objectType, bool isVisible)
  {
    if (!this.filterSettings.ContainsKey(objectType))
      this.filterSettings.Add(objectType, isVisible);
    else
      this.filterSettings[objectType] = isVisible;
  }

  private bool IconIsFiltered(SpaceEntityType objectType)
  {
    if (!this.filterSettings.ContainsKey(objectType))
      return false;
    return !this.filterSettings[objectType];
  }

  public void Destroy()
  {
    Object.Destroy((Object) this.gameObject);
  }

  public void RemoveIcon(ISpaceEntity target)
  {
    if (!this.mapIcons.ContainsKey(target))
      return;
    Object.Destroy((Object) this.mapIcons[target].gameObject);
    this.mapIcons.Remove(target);
  }

  public void AddIcon(ISpaceEntity target)
  {
    if (this.mapIcons.ContainsKey(target))
    {
      UnityEngine.Debug.LogError((object) ("SystemMap3DIconView.AddIcon() failed. Target already existed in dictionary: " + target.Name + " Is Waypoint: " + (object) this.mapIcons[target].IsWaypoint));
    }
    else
    {
      SystemMap3DMapIcon systemMap3DmapIcon;
      switch (target.SpaceEntityType)
      {
        case SpaceEntityType.AsteroidGroup:
          systemMap3DmapIcon = this.CreateAsteroidGroupIcon((AsteroidGroup) target);
          break;
        case SpaceEntityType.Player:
          systemMap3DmapIcon = !target.IsMe ? this.CreateStandardIcon(target) : this.CreateMyIcons(target);
          systemMap3DmapIcon.IsPartyMember = Game.Me.Party.Members.Contains(((PlayerShip) target).Player);
          break;
        case SpaceEntityType.SectorEvent:
          systemMap3DmapIcon = this.CreateSectorEventIcon((SectorEvent) target);
          break;
        default:
          systemMap3DmapIcon = this.CreateStandardIcon(target);
          break;
      }
      if ((Object) systemMap3DmapIcon == (Object) null)
        return;
      systemMap3DmapIcon.name = "SectorMap3DMapIcon (" + target.Name + ")";
      this.mapIcons.Add(target, systemMap3DmapIcon);
      if (this.activeWaypoints.Contains(target))
        systemMap3DmapIcon.IsWaypoint = true;
      this.ShowIcon(target, true, true);
    }
  }

  private SystemMap3DMapIcon CreateMyIcons(ISpaceEntity player)
  {
    SystemMap3DMapIcon systemMap3DmapIcon = this.Create3DMapIcon(player, (UnityEngine.Sprite) null);
    systemMap3DmapIcon.ManualColliderScaling = 3.5f;
    if ((Object) this.outerDradisCircle != (Object) null)
      Object.Destroy((Object) this.outerDradisCircle.gameObject);
    this.outerDradisCircle = systemMap3DmapIcon.AddSprite(SystemMap3DMapIconLinker.Instance.DradisCircle, 0.0f, true);
    float h;
    float s;
    float v;
    GuiUtils.ColorToHSV(ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR), out h, out s, out v);
    this.outerDradisCircle.color = GuiUtils.ColorFromHSV(h, s * 0.75f, v, 1f);
    GameObject gameObject = Object.Instantiate<GameObject>((GameObject) Resources.Load(Ui.Prefabs + "/SystemMap3D/SystemMap3DSphereDradis"));
    gameObject.transform.parent = systemMap3DmapIcon.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.zero;
    this.dradisSphere = gameObject.GetComponent<SystemMap3DDradisSphere>();
    if ((Object) this.playerArrow != (Object) null)
      Object.Destroy((Object) this.playerArrow.gameObject);
    this.playerArrow = this.CreatePlayerArrow(Game.Me.Ship.Root.transform);
    return systemMap3DmapIcon;
  }

  private SystemMap3DMapIcon CreateSectorEventIcon(SectorEvent sectorEvent)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SystemMap3DIconView.\u003CCreateSectorEventIcon\u003Ec__AnonStorey130 iconCAnonStorey130 = new SystemMap3DIconView.\u003CCreateSectorEventIcon\u003Ec__AnonStorey130();
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey130.sectorEvent = sectorEvent;
    // ISSUE: reference to a compiler-generated field
    UnityEngine.Sprite icon3dMap = iconCAnonStorey130.sectorEvent.Icon3dMap;
    // ISSUE: reference to a compiler-generated field
    SystemMap3DMapIcon systemMap3DmapIcon = this.Create3DMapIcon((ISpaceEntity) iconCAnonStorey130.sectorEvent, (UnityEngine.Sprite) null);
    systemMap3DmapIcon.AddSizeInvariantSprite(icon3dMap, true);
    Texture2D texture = (Texture2D) Resources.Load(Ui.Images + "/SystemMap3D/dynamic_mission_range");
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey130.sectorEventRadiusSprite = UnityEngine.Sprite.Create(texture, new Rect(0.0f, 0.0f, (float) texture.width, (float) texture.height), new Vector2(0.5f, 0.5f));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey130.radiusSpriteGo = systemMap3DmapIcon.AddSprite(iconCAnonStorey130.sectorEventRadiusSprite, 0.0f, true).gameObject;
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey130.radiusSpriteGo.GetComponent<SpriteRenderer>().color = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_SPECIAL);
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey130.radiusSphere = (GameObject) Resources.Load(Ui.Prefabs + "/SystemMap3D/SystemMap3DSphereSectorEvent");
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey130.radiusSphere = Object.Instantiate<GameObject>(iconCAnonStorey130.radiusSphere);
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey130.radiusSphere.transform.parent = systemMap3DmapIcon.transform;
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey130.radiusSphere.transform.localPosition = Vector3.zero;
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey130.radiusSphere.transform.localScale = Vector3.zero;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    iconCAnonStorey130.sectorEvent.OnStateUpdate += new AnonymousDelegate(iconCAnonStorey130.\u003C\u003Em__2D6);
    return systemMap3DmapIcon;
  }

  private SystemMap3DMapIcon CreateAsteroidGroupIcon(AsteroidGroup asteroidGroup)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SystemMap3DIconView.\u003CCreateAsteroidGroupIcon\u003Ec__AnonStorey131 iconCAnonStorey131 = new SystemMap3DIconView.\u003CCreateAsteroidGroupIcon\u003Ec__AnonStorey131();
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey131.asteroidGroup = asteroidGroup;
    // ISSUE: reference to a compiler-generated field
    SystemMap3DMapIcon standardIcon = this.CreateStandardIcon((ISpaceEntity) iconCAnonStorey131.asteroidGroup);
    standardIcon.ManualColliderScaling = 1.1f;
    // ISSUE: reference to a compiler-generated method
    standardIcon.OnMouseClicked = new AnonymousDelegate(iconCAnonStorey131.\u003C\u003Em__2D7);
    return standardIcon;
  }

  private SystemMap3DMapIcon CreateStandardIcon(ISpaceEntity entity)
  {
    if ((Object) entity.Icon3dMap == (Object) null)
      return (SystemMap3DMapIcon) null;
    UnityEngine.Sprite icon3dMap = entity.Icon3dMap;
    return this.Create3DMapIcon(entity, icon3dMap);
  }

  private SystemMap3DMapIcon Create3DMapIcon(ISpaceEntity target, UnityEngine.Sprite sprite = null)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SystemMap3DIconView.\u003CCreate3DMapIcon\u003Ec__AnonStorey132 iconCAnonStorey132 = new SystemMap3DIconView.\u003CCreate3DMapIcon\u003Ec__AnonStorey132();
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey132.target = target;
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey132.\u003C\u003Ef__this = this;
    GameObject gameObject = Object.Instantiate<GameObject>(this.iconProtoType);
    // ISSUE: reference to a compiler-generated field
    gameObject.transform.SetParent(iconCAnonStorey132.target.Root.transform, false);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    // ISSUE: reference to a compiler-generated field
    gameObject.name = "3DMapIcon - " + iconCAnonStorey132.target.Name;
    SystemMap3DMapIcon componentInChildren = gameObject.GetComponentInChildren<SystemMap3DMapIcon>();
    if ((Object) sprite != (Object) null)
      componentInChildren.AddSizeInvariantSprite(sprite, true);
    // ISSUE: reference to a compiler-generated method
    componentInChildren.OnMouseDoubleClicked += new AnonymousDelegate(iconCAnonStorey132.\u003C\u003Em__2D8);
    // ISSUE: reference to a compiler-generated method
    componentInChildren.OnMouseClicked += new AnonymousDelegate(iconCAnonStorey132.\u003C\u003Em__2D9);
    // ISSUE: reference to a compiler-generated method
    componentInChildren.OnMouseEntered += new AnonymousDelegate(iconCAnonStorey132.\u003C\u003Em__2DA);
    // ISSUE: reference to a compiler-generated method
    componentInChildren.OnMouseLeft += new AnonymousDelegate(iconCAnonStorey132.\u003C\u003Em__2DB);
    return componentInChildren;
  }

  public void SetIconSelectState(ISpaceEntity entity, bool isSelected)
  {
    if (entity == null || !this.mapIcons.ContainsKey(entity))
      return;
    this.mapIcons[entity].IsSelected = isSelected;
  }

  public void UpdateWaypointStatus(ISpaceEntity entity, bool isWaypoint)
  {
    if (!isWaypoint)
      this.activeWaypoints.Remove(entity);
    else if (!this.activeWaypoints.Contains(entity))
      this.activeWaypoints.Add(entity);
    if (!this.mapIcons.ContainsKey(entity) && (Object) entity.Icon3dMap == (Object) null)
    {
      if (!isWaypoint)
        return;
      SystemMap3DMapIcon systemMap3DmapIcon = this.Create3DMapIcon(entity, (UnityEngine.Sprite) null);
      this.mapIcons.Add(entity, systemMap3DmapIcon);
      this.ShowIcon(entity, true, true);
    }
    if (!this.mapIcons.ContainsKey(entity))
      return;
    this.mapIcons[entity].IsWaypoint = isWaypoint;
  }

  private void UpdateMarkerDisplay()
  {
    this.UpdateIconVisibilities();
    this.UpdateDradisCircleScales();
    this.UpdateDradisCircleAlpha();
  }

  private void UpdateIconVisibilities()
  {
    using (Dictionary<ISpaceEntity, SystemMap3DMapIcon>.Enumerator enumerator = this.mapIcons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ISpaceEntity, SystemMap3DMapIcon> current = enumerator.Current;
        ISpaceEntity key = current.Key;
        SystemMap3DMapIcon systemMap3DmapIcon = current.Value;
        bool show = this.IsTargetInDradis(key) || systemMap3DmapIcon.IsSelected;
        if (this.formAsteroidGroupsSetting)
        {
          if (key.SpaceEntityType == SpaceEntityType.Asteroid)
          {
            AsteroidGroup asteroidGroup = AsteroidGroupsManager.Instance.FindAsteroidGroup(key as Asteroid);
            show = (double) (asteroidGroup.Position - this.systemMap3DManager.CameraView.MapCameraTransform.position).sqrMagnitude < 16000000.0 || systemMap3DmapIcon.IsSelected || asteroidGroup.Count == 1;
          }
          else if (key.SpaceEntityType == SpaceEntityType.AsteroidGroup)
            show = (double) (key.Position - this.systemMap3DManager.CameraView.MapCameraTransform.position).sqrMagnitude >= 16000000.0 && ((AsteroidGroup) key).Count > 1;
        }
        else if (key.SpaceEntityType == SpaceEntityType.AsteroidGroup)
          show = false;
        this.ShowIcon(key, show, true);
      }
    }
  }

  public void UpdatePlayerPartyState(Player player)
  {
    if (player.IsMe)
    {
      using (Dictionary<ISpaceEntity, SystemMap3DMapIcon>.Enumerator enumerator = this.mapIcons.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ISpaceEntity, SystemMap3DMapIcon> current = enumerator.Current;
          if (current.Key.IsPlayer)
            current.Value.IsPartyMember = Game.Me.Party.IsMember(((PlayerShip) current.Key).Player);
        }
      }
    }
    else
    {
      if (player.PlayerShip == null || !this.mapIcons.ContainsKey((ISpaceEntity) player.PlayerShip))
        return;
      this.mapIcons[(ISpaceEntity) player.PlayerShip].IsPartyMember = Game.Me.Party.IsMember(player);
    }
  }

  public bool IsTargetInDradis(ISpaceEntity entity)
  {
    if (entity.ForceShowOnMap || entity.MarkerType == SpaceObject.MarkObjectType.Waypoint)
      return true;
    switch (entity.SpaceEntityType)
    {
      case SpaceEntityType.Player:
        PlayerShip playerShip = (PlayerShip) entity;
        if (entity.PlayerRelation == Relation.Friend || entity.IsInMapRange)
          return !playerShip.Player.IsAnchored;
        return false;
      case SpaceEntityType.WeaponPlatform:
      case SpaceEntityType.Cruiser:
      case SpaceEntityType.Outpost:
      case SpaceEntityType.Comet:
        return true;
      default:
        return entity.IsInMapRange;
    }
  }

  private void HideIcons()
  {
    using (Dictionary<ISpaceEntity, SystemMap3DMapIcon>.Enumerator enumerator = this.mapIcons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.ShowIcon(enumerator.Current.Key, false, true);
    }
  }

  private void ShowIcon(ISpaceEntity entity, bool show, bool considerFilter)
  {
    if (considerFilter && this.IconIsFiltered(entity.SpaceEntityType))
      show = false;
    show &= this.IsRendered;
    if (!this.mapIcons.ContainsKey(entity))
      UnityEngine.Debug.LogError((object) "ShowIcon(): Requested spaceObject has no registered icon");
    else
      this.mapIcons[entity].Show(show);
  }

  private SystemMap3DPlayerArrow CreatePlayerArrow(Transform shipTransform)
  {
    GameObject gameObject = Object.Instantiate<GameObject>(Resources.Load<GameObject>(Ui.Prefabs + "/SystemMap3D/SystemMap3DPlayerArrow"));
    gameObject.transform.SetParent(shipTransform, false);
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity * Quaternion.Euler(0.0f, 180f, 0.0f);
    return gameObject.GetComponent<SystemMap3DPlayerArrow>();
  }

  private void UpdateDradisCircleScales()
  {
    this.outerDradisCircle.transform.localScale = Game.Me.Stats.DetectionOuterRadius * Vector3.one * 2f * (this.outerDradisCircle.sprite.pixelsPerUnit / (float) this.outerDradisCircle.sprite.texture.width);
    this.dradisSphere.transform.localScale = 0.925f * Game.Me.Stats.DetectionOuterRadius * Vector3.one * 2f;
  }

  private void UpdateDradisCircleAlpha()
  {
    Camera activeCamera = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera();
    float num1 = Game.Me.Stats.DetectionOuterRadius / (2f * Mathf.Tan((float) (3.14159274101257 / (180.0 / (double) activeCamera.fieldOfView)) / 2f));
    float max = 2f * num1;
    float min = 1f * num1;
    float num2 = Mathf.Max(0.2f, (float) (((double) Mathf.Clamp((activeCamera.transform.position - this.outerDradisCircle.transform.position).magnitude, min, max) - (double) min) / ((double) max - (double) min)));
    Color color = this.outerDradisCircle.color;
    color.a = num2;
    this.outerDradisCircle.color = color;
  }
}
