﻿// Decompiled with JetBrains decompiler
// Type: FlightSimSpaceLevel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Tooltips;

public class FlightSimSpaceLevel : SpaceLevel
{
  protected override void Start()
  {
  }

  protected override void Update()
  {
    this.inputManager.UpdateInputs();
  }

  public override void Initialize(LevelProfile levelProfile)
  {
    this.guiManager = new GUIManager();
    this.inputManager = new InputManager();
    this.tooltipManager = new GuiAdvancedTooltipManager();
    this.cutsceneManager = CutsceneManager.Instance;
    this.cardGuid = 1U;
  }
}
