﻿// Decompiled with JetBrains decompiler
// Type: ShipCustomizationSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Core;
using UnityEngine;

internal abstract class ShipCustomizationSlot : GuiPanel, ISelectable
{
  public ShipCustomizationSlot()
  {
    this.Size = new Vector2(372f, 44f);
  }

  public override bool MouseUp(float2 position, KeyCode key)
  {
    this.FindParent<ISelectableContainer>().Selected = (ISelectable) this;
    return base.MouseUp(position, key);
  }

  public abstract void OnSelected();

  public void OnDeselected()
  {
  }
}
