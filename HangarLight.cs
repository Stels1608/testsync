﻿// Decompiled with JetBrains decompiler
// Type: HangarLight
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HangarLight : MonoBehaviour
{
  public int count = 100;
  [SerializeField]
  private ParticleSystem particleSys;
  [SerializeField]
  private Light lightSource;
  private float startIntensity;

  private void Start()
  {
    this.startIntensity = this.lightSource.intensity;
  }

  private void LateUpdate()
  {
    this.lightSource.intensity = this.startIntensity * ((float) this.particleSys.particleCount / (float) this.count);
  }
}
