﻿// Decompiled with JetBrains decompiler
// Type: AsteroidBreaking
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AsteroidBreaking : MonoBehaviour
{
  public float PieceSpeed = 2f;
  public float LifeTime = 30f;
  private AsteroidBreaking.Piece[] pieces;

  private void Start()
  {
    int childCount = this.transform.childCount;
    this.pieces = new AsteroidBreaking.Piece[childCount];
    for (int index = 0; index < childCount; ++index)
    {
      AsteroidBreaking.Piece piece = new AsteroidBreaking.Piece();
      Transform child = this.transform.GetChild(index);
      piece.Transform = child;
      piece.Direction = child.localPosition.normalized;
      piece.Speed = Random.Range(this.PieceSpeed * 0.5f, this.PieceSpeed * 1.5f);
      this.pieces[index] = piece;
    }
    this.Invoke("DestructItself", this.LifeTime);
  }

  private void Update()
  {
    foreach (AsteroidBreaking.Piece piece in this.pieces)
      piece.Update();
  }

  private void DestructItself()
  {
    Object.Destroy((Object) this.gameObject);
  }

  private class Piece
  {
    public Transform Transform;
    public Vector3 Direction;
    public float Speed;

    public void Update()
    {
      this.Transform.Translate(this.Direction * this.Speed * Time.deltaTime);
    }
  }
}
