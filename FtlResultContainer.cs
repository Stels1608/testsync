﻿// Decompiled with JetBrains decompiler
// Type: FtlResultContainer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class FtlResultContainer
{
  private uint wavesCompleted;
  private uint maxWaves;
  private uint enemysKilled;
  private uint missionTime;
  private FtlRanks rank;
  private List<ShipItem> rewardList;

  public uint WavesCompleted
  {
    get
    {
      return this.wavesCompleted;
    }
  }

  public uint MaxWaves
  {
    get
    {
      return this.maxWaves;
    }
  }

  public uint EnemysKilled
  {
    get
    {
      return this.enemysKilled;
    }
  }

  public uint MissionTime
  {
    get
    {
      return this.missionTime;
    }
  }

  public FtlRanks Rank
  {
    get
    {
      return this.rank;
    }
  }

  public List<ShipItem> RewardList
  {
    get
    {
      return this.rewardList;
    }
  }

  public FtlResultContainer(uint wavesCompleted, uint maxWaves, uint enemysKilled, uint missionTime, List<ShipItem> rewards, FtlRanks medalIdent)
  {
    this.wavesCompleted = wavesCompleted;
    this.maxWaves = maxWaves;
    this.enemysKilled = enemysKilled;
    this.missionTime = missionTime;
    this.rewardList = rewards;
    this.rank = medalIdent;
  }
}
