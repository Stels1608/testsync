﻿// Decompiled with JetBrains decompiler
// Type: EventShopMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class EventShopMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "EventShopMediator";
  private TradeInWindowWidget tradeInWindow;

  public EventShopMediator()
    : base("EventShopMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.ShowTradeInWindow);
    this.AddMessageInterest(Message.EventShopAvailability);
    this.AddMessageInterest(Message.EventShopInventory);
    this.AddMessageInterest(Message.PlayerHoldChanged);
    this.AddMessageInterest(Message.LoadNewLevel);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.EventShopAvailability:
        this.ToggleEventShopAvailability(message.Data as EventShopCard);
        break;
      case Message.EventShopInventory:
        this.UpdateItems((ItemList) message.Data);
        break;
      case Message.UiCreated:
        this.AttachView();
        break;
      case Message.ShowTradeInWindow:
        this.ToggleTradeInWindow();
        break;
      case Message.LoadNewLevel:
        this.tradeInWindow.Close();
        break;
      case Message.PlayerHoldChanged:
        this.tradeInWindow.SetAvailableResources((ItemList) message.Data);
        break;
    }
  }

  private void ToggleEventShopAvailability(EventShopCard shopCard)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    EventShopMediator.\u003CToggleEventShopAvailability\u003Ec__AnonStoreyD5 availabilityCAnonStoreyD5 = new EventShopMediator.\u003CToggleEventShopAvailability\u003Ec__AnonStoreyD5();
    // ISSUE: reference to a compiler-generated field
    availabilityCAnonStoreyD5.shopCard = shopCard;
    // ISSUE: reference to a compiler-generated field
    availabilityCAnonStoreyD5.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated method
    Game.DelayedActions.Add(new DelayedActions.Predicate(availabilityCAnonStoreyD5.\u003C\u003Em__20F));
    // ISSUE: reference to a compiler-generated field
    if (!((Object) GameLevel.Instance != (Object) null) || Game.IsLoadingLevel || (availabilityCAnonStoreyD5.shopCard == null || GameLevel.Instance is TransScene))
      return;
    Game.DelayedActions.Awake();
  }

  private void UpdateItems(ItemList items)
  {
    this.tradeInWindow.UpdateItems(items);
  }

  private void ToggleTradeInWindow()
  {
    if (!this.tradeInWindow.IsOpen)
      this.tradeInWindow.Open();
    else
      this.tradeInWindow.Close();
  }

  private void AttachView()
  {
    this.tradeInWindow = ((GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/ui_elements/event_shop/TradeInWindow"))).GetComponent<TradeInWindowWidget>();
    if ((Object) this.tradeInWindow == (Object) null)
      Debug.LogError((object) "TradeInWindowWidget was not registered correctly");
    (this.OwnerFacade.FetchDataProvider("GuiDataProvider") as GuiDataProvider).AddWindow((WindowWidget) this.tradeInWindow);
  }
}
