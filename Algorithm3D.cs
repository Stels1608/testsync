﻿// Decompiled with JetBrains decompiler
// Type: Algorithm3D
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class Algorithm3D
{
  public const int defaultLayerMask = 1;
  public const int ignoreReycastLayerMask = 4;

  public static Vector3 DistanceToLineSection(Vector3 from, Vector3 lineBegin, Vector3 lineEnd)
  {
    Vector3 vector3 = lineEnd - lineBegin;
    float num1 = Vector3.Dot(from - lineBegin, vector3);
    if ((double) num1 <= 0.0)
      return lineBegin - from;
    float num2 = Vector3.Dot(vector3, vector3);
    if ((double) num2 <= (double) num1)
      return lineEnd - from;
    float num3 = num1 / num2;
    return lineBegin + num3 * vector3 - from;
  }

  public static Vector3 CubicHermiteSpline(Vector3 p0, Vector3 m0, Vector3 p1, Vector3 m1, float t)
  {
    float num1 = t * t;
    float num2 = num1 * t;
    return (float) (2.0 * (double) num2 - 3.0 * (double) num1 + 1.0) * p0 + (num2 - 2f * num1 + t) * m0 + (float) (-2.0 * (double) num2 + 3.0 * (double) num1) * p1 + (num2 - num1) * m1;
  }

  public static Vector3 CubicBezierCurves(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
  {
    float num1 = t * t;
    float num2 = num1 * t;
    float num3 = 1f - t;
    float num4 = num3 * num3;
    return num4 * num3 * p0 + 3f * num4 * t * p1 + 3f * num3 * num1 * p2 + num2 * p3;
  }

  public static Vector3 QuadraticBézierCurves(Vector3 p0, Vector3 p1, Vector3 p2, float t)
  {
    float num1 = t * t;
    float num2 = 1f - t;
    return num2 * num2 * p0 + 2f * num2 * t * p1 + num1 * p2;
  }

  public static Vector3 CubicBezierCurvesTangent(Vector3 point0, Vector3 m0, Vector3 point1, Vector3 m1, float t)
  {
    Vector3 p0 = point0;
    Vector3 p1 = p0 + m0;
    Vector3 p2 = point1 - m1;
    Vector3 p3 = point1;
    return Algorithm3D.CubicBezierCurves(p0, p1, p2, p3, t);
  }

  public static Vector3 Clamp(Vector3 value, Vector3 min, Vector3 max)
  {
    return new Vector3() { x = Mathf.Clamp(value.x, min.x, max.x), y = Mathf.Clamp(value.y, min.y, max.y), z = Mathf.Clamp(value.z, min.z, max.z) };
  }

  public static bool IsNaN(Quaternion q)
  {
    if (!float.IsNaN(q.w) && !float.IsNaN(q.x) && !float.IsNaN(q.y))
      return float.IsNaN(q.z);
    return true;
  }

  public static bool IsInfinity(Quaternion q)
  {
    if (!float.IsInfinity(q.w) && !float.IsInfinity(q.x) && !float.IsInfinity(q.y))
      return float.IsInfinity(q.z);
    return true;
  }

  public static bool Raycast(GameObject gameObject, Vector3 origin, Vector3 direction, float distance, out RaycastHit raycastHit)
  {
    int layer = gameObject.layer;
    gameObject.layer = 4;
    bool flag = Physics.Raycast(origin, direction.normalized, out raycastHit, distance, 1);
    gameObject.layer = layer;
    return flag;
  }

  public static bool Linecast(GameObject gameObject, Vector3 start, Vector3 end, out RaycastHit raycastHit)
  {
    int layer = gameObject.layer;
    gameObject.layer = 4;
    bool flag = Physics.Linecast(start, end, out raycastHit, 1);
    gameObject.layer = layer;
    return flag;
  }

  public static Vector3 GetTrueDirection(Vector3 direction)
  {
    Vector3 from1 = direction.normalized;
    if ((double) Vector3.Angle(from1, Vector3.up) < 40.0)
    {
      Vector3 from2 = from1;
      from2.y = 0.0f;
      from2.Normalize();
      from1 = Vector3.Slerp(from2, Vector3.up, 0.5555556f);
    }
    else if ((double) Vector3.Angle(from1, Vector3.down) < 40.0)
    {
      Vector3 from2 = from1;
      from2.y = 0.0f;
      from2.Normalize();
      from1 = Vector3.Slerp(from2, Vector3.down, 0.5555556f);
    }
    return from1.normalized;
  }

  public static Quaternion RotateQuaternion(Quaternion q1, Quaternion q2, float rotationAngle)
  {
    float a = Quaternion.Angle(q1, q2);
    if (Mathf.Approximately(a, 0.0f) || (double) a <= (double) rotationAngle)
      return q2;
    return Quaternion.Slerp(q1, q2, rotationAngle / a);
  }

  public static Quaternion MirrorQuaternion(Quaternion q)
  {
    float angle = 0.0f;
    Vector3 axis = Vector3.zero;
    q.ToAngleAxis(out angle, out axis);
    axis.x = -axis.x;
    return Quaternion.AngleAxis(-angle, axis);
  }

  public static Vector3 Distance(Vector3 s1p0, Vector3 s1p1, Vector3 s2p0, Vector3 s2p1)
  {
    Vector3 vector3_1 = s1p1 - s1p0;
    Vector3 vector3_2 = s2p1 - s2p0;
    Vector3 rhs = s1p0 - s2p0;
    float num1 = Vector3.Dot(vector3_1, vector3_1);
    float num2 = Vector3.Dot(vector3_1, vector3_2);
    float num3 = Vector3.Dot(vector3_2, vector3_2);
    float num4 = Vector3.Dot(vector3_1, rhs);
    float num5 = Vector3.Dot(vector3_2, rhs);
    float num6 = (float) ((double) num1 * (double) num3 - (double) num2 * (double) num2);
    float num7 = num6;
    float num8 = num6;
    float f1;
    float f2;
    if ((double) num6 < (double) Mathf.Epsilon)
    {
      f1 = 0.0f;
      num7 = 1f;
      f2 = num5;
      num8 = num3;
    }
    else
    {
      f1 = (float) ((double) num2 * (double) num5 - (double) num3 * (double) num4);
      f2 = (float) ((double) num1 * (double) num5 - (double) num2 * (double) num4);
      if ((double) f1 < 0.0)
      {
        f1 = 0.0f;
        f2 = num5;
        num8 = num3;
      }
      else if ((double) f1 > (double) num7)
      {
        f1 = num7;
        f2 = num5 + num2;
        num8 = num3;
      }
    }
    if ((double) f2 < 0.0)
    {
      f2 = 0.0f;
      if (-(double) num4 < 0.0)
        f1 = 0.0f;
      else if (-(double) num4 > (double) num1)
      {
        f1 = num7;
      }
      else
      {
        f1 = -num4;
        num7 = num1;
      }
    }
    else if ((double) f2 > (double) num8)
    {
      f2 = num8;
      if (-(double) num4 + (double) num2 < 0.0)
        f1 = 0.0f;
      else if (-(double) num4 + (double) num2 > (double) num1)
      {
        f1 = num7;
      }
      else
      {
        f1 = -num4 + num2;
        num7 = num1;
      }
    }
    float num9 = (double) Mathf.Abs(f1) >= (double) Mathf.Epsilon ? f1 / num7 : 0.0f;
    float num10 = (double) Mathf.Abs(f2) >= (double) Mathf.Epsilon ? f2 / num8 : 0.0f;
    return rhs + num9 * vector3_1 - num10 * vector3_2;
  }

  public static void Distance(Vector3 s0, Vector3 s1, Vector3 s2, Vector3 s3, out Vector3 p0, out Vector3 p1)
  {
    Vector3 rhs = s1 - s0 - s3 - s2;
    Vector3 lhs = s2 - s0;
    float sqrMagnitude = rhs.sqrMagnitude;
    float num1 = 0.0f;
    if (!Mathf.Approximately(sqrMagnitude, 0.0f))
      num1 = Vector3.Dot(lhs, rhs) / sqrMagnitude;
    float num2 = Mathf.Clamp01(num1);
    p0 = s0 + (s1 - s0) * num2;
    p1 = s2 + (s3 - s2) * num2;
  }

  public static Vector3 NormalizeEuler(Vector3 v)
  {
    return new Vector3(Algorithm3D.NormalizeAngle(v.x), Algorithm3D.NormalizeAngle(v.y), Algorithm3D.NormalizeAngle(v.z));
  }

  public static Vector3 NormalizeEuler(Vector3 v, Vector3 nearest)
  {
    return new Vector3(Algorithm3D.NormalizeAngle(v.x, nearest.x), Algorithm3D.NormalizeAngle(v.y, nearest.y), Algorithm3D.NormalizeAngle(v.z, nearest.z));
  }

  public static float NormalizeAngle(float angle)
  {
    while ((double) angle <= -180.0)
      angle += 360f;
    while ((double) angle > 180.0)
      angle -= 360f;
    return angle;
  }

  public static float NormalizeAngle(float angle, float nearest)
  {
    while ((double) angle - (double) nearest < 0.0)
      angle += 360f;
    while ((double) angle - (double) nearest > 180.0)
      angle -= 360f;
    return angle;
  }
}
