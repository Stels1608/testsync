﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorCombatInfoUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HudIndicatorCombatInfoUgui : HudIndicatorBaseComponentUgui, ICombatInfo, IReceivesSetting, IReceivesSpaceObject
{
  private readonly Queue<HudIndicatorCombatInfoFaderUgui> displayedCombatInfos = new Queue<HudIndicatorCombatInfoFaderUgui>();
  [SerializeField]
  private TextMeshProUGUI PrototypeLabel;
  private float lastDisplayUpdate;
  private int damage;
  private float dpsTime;
  private float timeWithoutDamage;
  private int oldDamage;
  private SpaceObject spaceObject;
  private bool isEnabledInSettings;

  protected override void ResetComponentStates()
  {
    this.spaceObject = (SpaceObject) null;
    this.RemoveRemainingCombatText();
    this.PrototypeLabel.gameObject.SetActive(false);
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    bool flag = this.isEnabledInSettings && (this.InScannerRange || this.IsSelected) && this.InScreen;
    if ((Object) this.gameObject == (Object) null)
      Debug.LogError((object) ("Tried to access destroyed CombatInfo for SpaceObject: " + this.spaceObject.Name));
    this.gameObject.SetActive(flag);
  }

  public void OnSpaceObjectInjection(SpaceObject spaceObject)
  {
    this.spaceObject = spaceObject;
    this.RemoveRemainingCombatText();
  }

  public void Update()
  {
    this.UpdateCombatText();
  }

  public void OnCombatInfo(AbstractCombatInfo combatInfo)
  {
    if (!(combatInfo is DamageCombatInfo) || !this.InScannerRange)
      return;
    DamageCombatInfo damageCombatInfo = (DamageCombatInfo) combatInfo;
    this.damage += damageCombatInfo.Damage;
    if ((double) Time.time - (double) this.lastDisplayUpdate <= 0.449999988079071)
      return;
    this.displayedCombatInfos.Enqueue(new HudIndicatorCombatInfoFaderUgui(this.damage.ToString("n0"), this.CreateCombatInfoLabel(damageCombatInfo.CriticalHit)));
    this.damage = 0;
    this.lastDisplayUpdate = Time.time;
  }

  private TextMeshProUGUI CreateCombatInfoLabel(bool criticalHit)
  {
    GameObject gameObject = Object.Instantiate<GameObject>(this.PrototypeLabel.gameObject);
    gameObject.transform.SetParent(this.transform, false);
    gameObject.SetActive(true);
    ((RectTransform) gameObject.transform).anchoredPosition = (Vector2) Vector3.zero;
    TextMeshProUGUI component = gameObject.GetComponent<TextMeshProUGUI>();
    if (this.spaceObject != null)
      component.color = HudIndicatorColorScheme.Instance.TextColor((ISpaceEntity) this.spaceObject);
    return component;
  }

  private void ResetDps()
  {
    this.dpsTime = (float) (this.damage = this.oldDamage = 0);
    this.timeWithoutDamage = 0.0f;
  }

  private void RemoveRemainingCombatText()
  {
    this.damage = 0;
    while (this.displayedCombatInfos.Count > 0)
      Object.Destroy((Object) this.displayedCombatInfos.Dequeue().ParentLabel.gameObject);
  }

  private void UpdateCombatText()
  {
    using (Queue<HudIndicatorCombatInfoFaderUgui>.Enumerator enumerator = this.displayedCombatInfos.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Update();
    }
    while (this.displayedCombatInfos.Count > 0 && this.displayedCombatInfos.Peek().Expired)
      Object.Destroy((Object) this.displayedCombatInfos.Dequeue().ParentLabel.gameObject);
  }

  private void CalcuateDps()
  {
    this.dpsTime += Time.deltaTime;
    if (this.damage == this.oldDamage)
      this.timeWithoutDamage += Time.deltaTime;
    else
      this.timeWithoutDamage = 0.0f;
    if ((double) this.timeWithoutDamage > 4.0)
      this.ResetDps();
    this.oldDamage = this.damage;
  }

  public void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    switch (userSetting)
    {
      case UserSetting.HudIndicatorTextSize:
        this.PrototypeLabel.fontSize = (float) data;
        break;
      case UserSetting.HudIndicatorColorScheme:
        this.SetColoring();
        break;
      case UserSetting.CombatText:
        this.isEnabledInSettings = (bool) data;
        break;
    }
    this.UpdateView();
  }
}
