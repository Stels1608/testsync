﻿// Decompiled with JetBrains decompiler
// Type: StoryMissionLogUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using TMPro;
using UnityEngine;

public class StoryMissionLogUgui : StoryMissionLogUi
{
  [SerializeField]
  private TextMeshProUGUI messageLabel;
  [SerializeField]
  private TextMeshProUGUI dialogButtonLabel;
  [SerializeField]
  private TextMeshProUGUI skipButtonLabel;

  protected override void ColorizeMissionObjectiveText(ref string objectiveText)
  {
    UguiTools.ApplyHighlightColorTag(ref objectiveText);
  }

  protected override void SetMissionObjectivesText(string objectiveText)
  {
    this.messageLabel.text = objectiveText;
  }

  protected override void SetDialogButtonText(string dialogButtonText)
  {
    this.dialogButtonLabel.text = dialogButtonText;
  }

  protected override void SetSkipButtonText(string skipButtonText)
  {
    this.skipButtonLabel.text = skipButtonText;
  }
}
