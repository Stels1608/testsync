﻿// Decompiled with JetBrains decompiler
// Type: JumpAndIdle
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (AlignmentTracker))]
public class JumpAndIdle : MonoBehaviour
{
  public float fallTimeThreshold = 0.2f;
  public AnimationClip jumpingAnimation;
  public float jumpTimeStart;
  public AnimationClip waitingAnimation;
  private bool doJumping;
  private bool doWaiting;
  private AlignmentTracker align;
  private CharacterMotor cm;
  private bool grounded;
  private bool waiting;
  private float idleTimer;
  private float fallingTimer;

  private void Start()
  {
    this.align = this.GetComponent(typeof (AlignmentTracker)) as AlignmentTracker;
    this.cm = this.GetComponent(typeof (CharacterMotor)) as CharacterMotor;
    this.grounded = false;
    if ((Object) this.jumpingAnimation != (Object) null)
    {
      this.GetComponent<Animation>()[this.jumpingAnimation.name].wrapMode = WrapMode.ClampForever;
      this.doJumping = true;
    }
    if ((Object) this.waitingAnimation != (Object) null)
    {
      this.GetComponent<Animation>()[this.waitingAnimation.name].wrapMode = WrapMode.ClampForever;
      this.doWaiting = true;
    }
    this.GetComponent<Animation>().Play("locomotion");
  }

  private void OnEnable()
  {
    if (!((TrackedReference) this.GetComponent<Animation>()["locomotion"] != (TrackedReference) null))
      return;
    this.GetComponent<Animation>()["locomotion"].weight = 1f;
  }

  private void Update()
  {
    float magnitude = this.align.velocity.magnitude;
    if (this.doJumping)
    {
      if (this.cm.jumping)
      {
        this.grounded = false;
        this.waiting = false;
        this.GetComponent<Animation>().CrossFade(this.jumpingAnimation.name, 0.1f);
        this.GetComponent<Animation>()[this.jumpingAnimation.name].time = this.jumpTimeStart;
        this.GetComponent<Animation>()[this.jumpingAnimation.name].wrapMode = WrapMode.ClampForever;
      }
      else if (this.grounded && !this.cm.grounded)
      {
        this.grounded = false;
        this.waiting = false;
      }
      else if (!this.grounded && this.cm.grounded)
      {
        this.grounded = true;
        this.waiting = false;
        this.fallingTimer = 0.0f;
        this.GetComponent<Animation>().CrossFade("locomotion", 0.1f);
      }
      else if (!this.grounded && (double) this.fallingTimer < (double) this.fallTimeThreshold)
      {
        this.fallingTimer += Time.deltaTime;
        if ((double) this.fallingTimer >= (double) this.fallTimeThreshold)
        {
          this.GetComponent<Animation>().CrossFade(this.jumpingAnimation.name, 0.2f);
          this.GetComponent<Animation>()[this.jumpingAnimation.name].time = this.jumpTimeStart;
          this.GetComponent<Animation>()[this.jumpingAnimation.name].wrapMode = WrapMode.ClampForever;
        }
      }
    }
    if (!this.doWaiting)
      return;
    if ((double) magnitude == 0.0)
    {
      this.idleTimer += Time.deltaTime;
      if ((double) this.idleTimer <= 3.0)
        return;
      if ((double) this.GetComponent<Animation>()[this.waitingAnimation.name].time == 0.0 || (double) this.GetComponent<Animation>()[this.waitingAnimation.name].time >= (double) this.GetComponent<Animation>()[this.waitingAnimation.name].length)
      {
        this.GetComponent<Animation>()[this.waitingAnimation.name].time = 0.0f;
        this.GetComponent<Animation>().CrossFade(this.waitingAnimation.name);
        this.GetComponent<Animation>()[this.waitingAnimation.name].wrapMode = WrapMode.ClampForever;
        this.waiting = true;
      }
      this.idleTimer = (float) -(2.0 + 4.0 * (double) Random.value);
    }
    else
    {
      if ((double) magnitude <= 0.0 || !this.waiting)
        return;
      this.GetComponent<Animation>().CrossFade("locomotion");
      this.waiting = false;
      this.idleTimer = 0.0f;
    }
  }
}
