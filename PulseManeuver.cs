﻿// Decompiled with JetBrains decompiler
// Type: PulseManeuver
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PulseManeuver : Maneuver
{
  protected Vector3 direction;

  public PulseManeuver()
  {
    this.isExclusive = (byte) 0;
  }

  public override string ToString()
  {
    return string.Format("PulseManeuver: direction={0}", (object) this.direction);
  }

  public override MovementFrame NextFrame(Tick tick, MovementFrame prevFrame)
  {
    if (!prevFrame.valid)
      return MovementFrame.Invalid;
    if (tick == this.startTick)
    {
      Vector3 direction1 = this.direction;
      MovementFrame direction2 = this.MoveToDirection(prevFrame, Euler3.Direction(direction1));
      direction2.linearSpeed = direction1;
      direction2.strafeSpeed = Vector3.zero;
      return direction2;
    }
    Euler3 direction = Euler3.Direction(this.direction);
    return this.MoveToDirection(prevFrame, direction);
  }

  public override void Read(BgoProtocolReader pr)
  {
    base.Read(pr);
    this.startTick = pr.ReadTick();
    this.direction = pr.ReadVector3();
    this.options.Read(pr);
  }
}
