﻿// Decompiled with JetBrains decompiler
// Type: GUIBlinker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Tooltips;
using UnityEngine;

public class GUIBlinker : GUIPanel
{
  private GUICountdownNew countdown;
  private int blinksLeftToPlaySound;
  private GuiAdvancedTooltipBase infoBox;

  public override bool IsRendered
  {
    set
    {
      if (value)
      {
        this.countdown.Progress = 0.0f;
        GUISound.Instance.OnBlinkerBlinks();
      }
      if (this.infoBox != null)
      {
        this.infoBox.IsRendered = value;
        this.infoBox.Position = this.SmartRect.AbsPosition.ToV2() + new Vector2(2f, (float) -(2.0 + (double) this.infoBox.SizeY));
        if (Game.GUIManager.panels.Contains((IGUIPanel) this.infoBox))
          Game.GUIManager.RemovePanel((IGUIPanel) this.infoBox);
        Game.GUIManager.AddPanel((IGUIPanel) this.infoBox);
      }
      base.IsRendered = value;
    }
  }

  public override float Width
  {
    get
    {
      return base.Width;
    }
    set
    {
      this.countdown.Width = value;
      base.Width = this.countdown.Width;
    }
  }

  public override float Height
  {
    get
    {
      return base.Height;
    }
    set
    {
      this.countdown.Height = value;
      base.Height = this.countdown.Height;
    }
  }

  public GUIBlinker()
  {
    this.countdown = new GUICountdownNew(ResourceLoader.Load<Texture2D>("GUI/Common/flash"), 4U, 4U);
    this.AddPanel((GUIPanel) this.countdown);
    this.countdown.Width *= 1.2f;
    this.countdown.Height *= 1.2f;
    this.Width = this.countdown.Width;
    this.Height = this.countdown.Height;
  }

  public GUIBlinker(string infoText)
    : this((GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip(infoText))
  {
  }

  public GUIBlinker(string infoText, Action hotkey)
    : this((GuiAdvancedTooltipBase) new GuiAdvancedHotkeyTooltip(infoText, hotkey))
  {
  }

  public GUIBlinker(GuiAdvancedTooltipBase tooltip)
    : this()
  {
    this.infoBox = tooltip;
  }

  public void SetInfoBox(GuiAdvancedTooltipBase tooltip)
  {
    this.infoBox = tooltip;
  }

  public override void Update()
  {
    float num = this.countdown.Progress + Time.deltaTime;
    if ((double) num > 1.0)
    {
      num = 0.0f;
      --this.blinksLeftToPlaySound;
      if (this.blinksLeftToPlaySound <= 0)
      {
        this.blinksLeftToPlaySound = 3;
        GUISound.Instance.OnBlinkerBlinks();
      }
    }
    this.countdown.Progress = num;
  }
}
