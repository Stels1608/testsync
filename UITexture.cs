﻿// Decompiled with JetBrains decompiler
// Type: UITexture
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/UI/NGUI Texture")]
public class UITexture : UIBasicSprite
{
  [HideInInspector]
  [SerializeField]
  private Rect mRect = new Rect(0.0f, 0.0f, 1f, 1f);
  [HideInInspector]
  [SerializeField]
  private Vector4 mBorder = Vector4.zero;
  [NonSerialized]
  private int mPMA = -1;
  [HideInInspector]
  [SerializeField]
  private Texture mTexture;
  [SerializeField]
  [HideInInspector]
  private Material mMat;
  [HideInInspector]
  [SerializeField]
  private Shader mShader;
  [SerializeField]
  [HideInInspector]
  private bool mFixedAspect;

  public override Texture mainTexture
  {
    get
    {
      if ((UnityEngine.Object) this.mTexture != (UnityEngine.Object) null)
        return this.mTexture;
      if ((UnityEngine.Object) this.mMat != (UnityEngine.Object) null)
        return this.mMat.mainTexture;
      return (Texture) null;
    }
    set
    {
      if (!((UnityEngine.Object) this.mTexture != (UnityEngine.Object) value))
        return;
      if ((UnityEngine.Object) this.drawCall != (UnityEngine.Object) null && this.drawCall.widgetCount == 1 && (UnityEngine.Object) this.mMat == (UnityEngine.Object) null)
      {
        this.mTexture = value;
        this.drawCall.mainTexture = value;
      }
      else
      {
        this.RemoveFromPanel();
        this.mTexture = value;
        this.mPMA = -1;
        this.MarkAsChanged();
      }
    }
  }

  public override Material material
  {
    get
    {
      return this.mMat;
    }
    set
    {
      if (!((UnityEngine.Object) this.mMat != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mShader = (Shader) null;
      this.mMat = value;
      this.mPMA = -1;
      this.MarkAsChanged();
    }
  }

  public override Shader shader
  {
    get
    {
      if ((UnityEngine.Object) this.mMat != (UnityEngine.Object) null)
        return this.mMat.shader;
      if ((UnityEngine.Object) this.mShader == (UnityEngine.Object) null)
        this.mShader = Shader.Find("Unlit/Transparent Colored");
      return this.mShader;
    }
    set
    {
      if (!((UnityEngine.Object) this.mShader != (UnityEngine.Object) value))
        return;
      if ((UnityEngine.Object) this.drawCall != (UnityEngine.Object) null && this.drawCall.widgetCount == 1 && (UnityEngine.Object) this.mMat == (UnityEngine.Object) null)
      {
        this.mShader = value;
        this.drawCall.shader = value;
      }
      else
      {
        this.RemoveFromPanel();
        this.mShader = value;
        this.mPMA = -1;
        this.mMat = (Material) null;
        this.MarkAsChanged();
      }
    }
  }

  public override bool premultipliedAlpha
  {
    get
    {
      if (this.mPMA == -1)
      {
        Material material = this.material;
        this.mPMA = !((UnityEngine.Object) material != (UnityEngine.Object) null) || !((UnityEngine.Object) material.shader != (UnityEngine.Object) null) || !material.shader.name.Contains("Premultiplied") ? 0 : 1;
      }
      return this.mPMA == 1;
    }
  }

  public override Vector4 border
  {
    get
    {
      return this.mBorder;
    }
    set
    {
      if (!(this.mBorder != value))
        return;
      this.mBorder = value;
      this.MarkAsChanged();
    }
  }

  public Rect uvRect
  {
    get
    {
      return this.mRect;
    }
    set
    {
      if (!(this.mRect != value))
        return;
      this.mRect = value;
      this.MarkAsChanged();
    }
  }

  public override Vector4 drawingDimensions
  {
    get
    {
      Vector2 pivotOffset = this.pivotOffset;
      float from1 = -pivotOffset.x * (float) this.mWidth;
      float from2 = -pivotOffset.y * (float) this.mHeight;
      float to1 = from1 + (float) this.mWidth;
      float to2 = from2 + (float) this.mHeight;
      if ((UnityEngine.Object) this.mTexture != (UnityEngine.Object) null && this.mType != UIBasicSprite.Type.Tiled)
      {
        int width = this.mTexture.width;
        int height = this.mTexture.height;
        int num1 = 0;
        int num2 = 0;
        float num3 = 1f;
        float num4 = 1f;
        if (width > 0 && height > 0 && (this.mType == UIBasicSprite.Type.Simple || this.mType == UIBasicSprite.Type.Filled))
        {
          if ((width & 1) != 0)
            ++num1;
          if ((height & 1) != 0)
            ++num2;
          num3 = 1f / (float) width * (float) this.mWidth;
          num4 = 1f / (float) height * (float) this.mHeight;
        }
        if (this.mFlip == UIBasicSprite.Flip.Horizontally || this.mFlip == UIBasicSprite.Flip.Both)
          from1 += (float) num1 * num3;
        else
          to1 -= (float) num1 * num3;
        if (this.mFlip == UIBasicSprite.Flip.Vertically || this.mFlip == UIBasicSprite.Flip.Both)
          from2 += (float) num2 * num4;
        else
          to2 -= (float) num2 * num4;
      }
      float num5;
      float num6;
      if (this.mFixedAspect)
      {
        num5 = 0.0f;
        num6 = 0.0f;
      }
      else
      {
        Vector4 border = this.border;
        num5 = border.x + border.z;
        num6 = border.y + border.w;
      }
      return new Vector4(Mathf.Lerp(from1, to1 - num5, this.mDrawRegion.x), Mathf.Lerp(from2, to2 - num6, this.mDrawRegion.y), Mathf.Lerp(from1 + num5, to1, this.mDrawRegion.z), Mathf.Lerp(from2 + num6, to2, this.mDrawRegion.w));
    }
  }

  public bool fixedAspect
  {
    get
    {
      return this.mFixedAspect;
    }
    set
    {
      if (this.mFixedAspect == value)
        return;
      this.mFixedAspect = value;
      this.mDrawRegion = new Vector4(0.0f, 0.0f, 1f, 1f);
      this.MarkAsChanged();
    }
  }

  public override void MakePixelPerfect()
  {
    base.MakePixelPerfect();
    if (this.mType == UIBasicSprite.Type.Tiled)
      return;
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null || this.mType != UIBasicSprite.Type.Simple && this.mType != UIBasicSprite.Type.Filled && this.hasBorder || !((UnityEngine.Object) mainTexture != (UnityEngine.Object) null))
      return;
    int width = mainTexture.width;
    int height = mainTexture.height;
    if ((width & 1) == 1)
      ++width;
    if ((height & 1) == 1)
      ++height;
    this.width = width;
    this.height = height;
  }

  protected override void OnUpdate()
  {
    base.OnUpdate();
    if (!this.mFixedAspect)
      return;
    Texture mainTexture = this.mainTexture;
    if (!((UnityEngine.Object) mainTexture != (UnityEngine.Object) null))
      return;
    int width = mainTexture.width;
    int height = mainTexture.height;
    if ((width & 1) == 1)
      ++width;
    if ((height & 1) == 1)
      ++height;
    float num1 = (float) this.mWidth;
    float num2 = (float) this.mHeight;
    float num3 = num1 / num2;
    float num4 = (float) width / (float) height;
    if ((double) num4 < (double) num3)
    {
      float x = (float) (((double) num1 - (double) num2 * (double) num4) / (double) num1 * 0.5);
      this.drawRegion = new Vector4(x, 0.0f, 1f - x, 1f);
    }
    else
    {
      float y = (float) (((double) num2 - (double) num1 / (double) num4) / (double) num2 * 0.5);
      this.drawRegion = new Vector4(0.0f, y, 1f, 1f - y);
    }
  }

  public override void OnFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    Texture mainTexture = this.mainTexture;
    if ((UnityEngine.Object) mainTexture == (UnityEngine.Object) null)
      return;
    Rect outer = new Rect(this.mRect.x * (float) mainTexture.width, this.mRect.y * (float) mainTexture.height, (float) mainTexture.width * this.mRect.width, (float) mainTexture.height * this.mRect.height);
    Rect inner = outer;
    Vector4 border = this.border;
    inner.xMin += border.x;
    inner.yMin += border.y;
    inner.xMax -= border.z;
    inner.yMax -= border.w;
    float num1 = 1f / (float) mainTexture.width;
    float num2 = 1f / (float) mainTexture.height;
    outer.xMin *= num1;
    outer.xMax *= num1;
    outer.yMin *= num2;
    outer.yMax *= num2;
    inner.xMin *= num1;
    inner.xMax *= num1;
    inner.yMin *= num2;
    inner.yMax *= num2;
    int bufferOffset = verts.size;
    this.Fill(verts, uvs, cols, outer, inner);
    if (this.onPostFill == null)
      return;
    this.onPostFill((UIWidget) this, bufferOffset, verts, uvs, cols);
  }
}
