﻿// Decompiled with JetBrains decompiler
// Type: DebugRandomMovement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugRandomMovement : MonoBehaviour
{
  public float velocity = 50f;
  private float timeStart;
  private float timeEnd;
  private Quaternion rotateStart;
  private Quaternion rotateEnd;

  private void Update()
  {
    if ((double) this.timeEnd < (double) Time.time)
    {
      this.timeStart = Time.time;
      this.timeEnd = this.timeStart + Random.Range(0.5f, 2f);
      this.rotateStart = this.transform.rotation;
      Vector3 vector3 = new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f));
      this.rotateEnd = Quaternion.LookRotation((Vector3.zero - this.transform.position).normalized.normalized, Vector3.up);
    }
    Quaternion q = Quaternion.Slerp(this.rotateStart, this.rotateEnd, (float) (((double) Time.time - (double) this.timeStart) / ((double) this.timeEnd - (double) this.timeStart)));
    if (!Algorithm3D.IsNaN(q))
      this.transform.rotation = q;
    this.transform.position += this.transform.forward * this.velocity * Time.deltaTime;
  }

  private void OnTriggerEnter(Collider colider)
  {
    Log.DebugInfo((object) ("OnTriggerEnter " + colider.name));
  }
}
