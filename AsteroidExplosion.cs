﻿// Decompiled with JetBrains decompiler
// Type: AsteroidExplosion
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("+FX/asteroidExplosion")]
public class AsteroidExplosion : MonoBehaviour
{
  public ParticleSystem AsteroidExplosionPrefab;

  public string PrefabName
  {
    get
    {
      if ((Object) this.AsteroidExplosionPrefab != (Object) null)
        return this.AsteroidExplosionPrefab.name.ToLower() + ".prefab";
      return (string) null;
    }
  }
}
