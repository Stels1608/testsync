﻿// Decompiled with JetBrains decompiler
// Type: CutsceneTextRenderer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

internal class CutsceneTextRenderer : MonoBehaviour
{
  private string currentSubtitleText = string.Empty;
  private string currentShrinkingCenterText = string.Empty;
  private string currentSceneDescriptionText = string.Empty;
  private const float CAPTION_HEIGHT = 0.1f;
  private GUIStyle subtitleFontStyle;
  private SortedList<float, string> subtitleTexts;
  private float currentSubtitleLifeEnd;
  private GUIStyle shrinkingCenterFontStyle;
  private SortedList<float, string> shrinkingCenterTexts;
  private float currentShrinkingCenterTextLifeStart;
  private float currentShrinkingCenterTextLifeEnd;
  private GUIStyle sceneDescriptionFontStyle;
  private SortedList<float, string> sceneDescriptionTexts;
  private float currentSceneDescriptionTextLifeStart;
  private float currentSceneDescriptionTextLifeEnd;

  public void Awake()
  {
    this.subtitleFontStyle = new GUIStyle(GUIStyle.none);
    this.subtitleFontStyle.font = Gui.Options.fontEurostileTRegCon;
    this.subtitleFontStyle.normal.textColor = Color.white;
    this.subtitleFontStyle.fontSize = 16;
    this.subtitleFontStyle.alignment = TextAnchor.MiddleCenter;
    this.subtitleFontStyle.wordWrap = true;
    this.shrinkingCenterFontStyle = new GUIStyle(GUIStyle.none);
    this.shrinkingCenterFontStyle.font = Gui.Options.fontEurostileTRegCon;
    this.shrinkingCenterFontStyle.normal.textColor = Color.white;
    this.shrinkingCenterFontStyle.fontSize = 25;
    this.shrinkingCenterFontStyle.alignment = TextAnchor.MiddleCenter;
    this.shrinkingCenterFontStyle.wordWrap = true;
    this.sceneDescriptionFontStyle = new GUIStyle(GUIStyle.none);
    this.sceneDescriptionFontStyle.font = Gui.Options.fontEurostileTRegCon;
    this.sceneDescriptionFontStyle.normal.textColor = Color.white;
    this.sceneDescriptionFontStyle.fontSize = 25;
    this.sceneDescriptionFontStyle.alignment = TextAnchor.LowerRight;
    this.sceneDescriptionFontStyle.wordWrap = true;
    this.subtitleTexts = new SortedList<float, string>();
    this.shrinkingCenterTexts = new SortedList<float, string>();
    this.sceneDescriptionTexts = new SortedList<float, string>();
  }

  public void SetSubtitleText(string text)
  {
    this.subtitleTexts.Clear();
    this.currentSubtitleLifeEnd = 0.0f;
    float num = 0.0f;
    if (!string.IsNullOrEmpty(this.currentSubtitleText))
    {
      this.currentSubtitleText = string.Empty;
      num = 0.3f;
    }
    this.subtitleTexts.Add(Time.realtimeSinceStartup + num, text);
  }

  public void QueueSubtitleText(string text, float timeDelay = 0.0f)
  {
    this.subtitleTexts.Add(Time.realtimeSinceStartup + timeDelay, text);
  }

  public void QueueZoomingCenterText(string text, float timeDelay = 0.0f)
  {
    this.shrinkingCenterTexts.Add(Time.realtimeSinceStartup + timeDelay, text);
  }

  public void QueueSceneDescriptionText(string text, float timeDelay = 0.0f)
  {
    this.sceneDescriptionTexts.Add(Time.realtimeSinceStartup + timeDelay, text);
  }

  private void OnGUI()
  {
    this.PrintSubtitles();
    this.PrintSceneDescriptions();
    this.PrintShrinkingCenterTexts();
  }

  private void PrintSubtitles()
  {
    if ((double) Time.realtimeSinceStartup > (double) this.currentSubtitleLifeEnd)
      this.currentSubtitleText = string.Empty;
    if (this.subtitleTexts.Count > 0 && (double) Time.realtimeSinceStartup > (double) this.subtitleTexts.Keys[0])
    {
      this.currentSubtitleText = this.subtitleTexts[this.subtitleTexts.Keys[0]];
      this.subtitleTexts.RemoveAt(0);
      this.currentSubtitleLifeEnd = Time.realtimeSinceStartup + Mathf.Max(1f, (float) this.currentSubtitleText.Length * 0.06f);
    }
    Rect position = new Rect(0.0f, 0.9f * (float) Screen.height, (float) Screen.width, 0.1f * (float) Screen.height);
    this.subtitleFontStyle.fontSize = Mathf.RoundToInt(Mathf.Clamp(0.02083333f * (float) Screen.height, 16f, 20f));
    GUI.depth = 0;
    GUI.Label(position, this.currentSubtitleText, this.subtitleFontStyle);
  }

  private void PrintSceneDescriptions()
  {
    float num1 = (float) Screen.height / 768f;
    if ((double) Time.realtimeSinceStartup > (double) this.currentSceneDescriptionTextLifeEnd)
      this.currentSceneDescriptionText = string.Empty;
    if (this.sceneDescriptionTexts.Count > 0 && (double) Time.realtimeSinceStartup > (double) this.sceneDescriptionTexts.Keys[0])
    {
      this.currentSceneDescriptionText = this.sceneDescriptionTexts[this.sceneDescriptionTexts.Keys[0]].Replace("<br>", "\n");
      this.sceneDescriptionTexts.RemoveAt(0);
      this.currentSceneDescriptionTextLifeStart = Time.realtimeSinceStartup;
      this.currentSceneDescriptionTextLifeEnd = Time.realtimeSinceStartup + Mathf.Max(1f, (float) this.currentSceneDescriptionText.Length * 0.06f);
    }
    float left = 10f * num1;
    Rect position = new Rect(left, -left, (float) Screen.width - 2f * left, 0.9f * (float) Screen.height);
    this.sceneDescriptionFontStyle.fontSize = Mathf.RoundToInt(Mathf.Clamp(20f * num1, 16f, 24f));
    float num2 = (float) (1.0 - ((double) this.currentSceneDescriptionTextLifeEnd - (double) Time.realtimeSinceStartup) / ((double) this.currentSceneDescriptionTextLifeEnd - (double) this.currentSceneDescriptionTextLifeStart));
    Color color1 = GUI.color;
    Color color2 = color1;
    color2.a = Mathf.Sin(num2 * 3.14f) * 2f;
    GUI.depth = 0;
    GUI.color = color2;
    GUI.Label(position, this.currentSceneDescriptionText, this.sceneDescriptionFontStyle);
    GUI.color = color1;
  }

  private void PrintShrinkingCenterTexts()
  {
    if ((double) Time.realtimeSinceStartup > (double) this.currentShrinkingCenterTextLifeEnd)
      this.currentShrinkingCenterText = string.Empty;
    if (this.shrinkingCenterTexts.Count > 0 && (double) Time.realtimeSinceStartup > (double) this.shrinkingCenterTexts.Keys[0])
    {
      this.currentShrinkingCenterText = this.shrinkingCenterTexts[this.shrinkingCenterTexts.Keys[0]].Replace("<br>", "\n");
      this.shrinkingCenterTexts.RemoveAt(0);
      this.currentShrinkingCenterTextLifeStart = Time.realtimeSinceStartup;
      this.currentShrinkingCenterTextLifeEnd = Time.realtimeSinceStartup + Mathf.Max(1f, (float) this.currentShrinkingCenterText.Length * 0.12f);
    }
    float num1 = (float) (1.0 - ((double) this.currentShrinkingCenterTextLifeEnd - (double) Time.realtimeSinceStartup) / ((double) this.currentShrinkingCenterTextLifeEnd - (double) this.currentShrinkingCenterTextLifeStart));
    float f = Mathf.Clamp(0.05208333f * (float) Screen.height, 20f, 60f);
    this.shrinkingCenterFontStyle.fontSize = Mathf.RoundToInt(f);
    GUI.depth = 0;
    Matrix4x4 matrix = GUI.matrix;
    float num2 = (float) (1.0 - (double) num1 * 0.200000002980232);
    GUI.matrix = Matrix4x4.TRS(new Vector3((float) Screen.width / 2f, (float) Screen.height / 2f, 0.0f), Quaternion.identity, new Vector3(num2, num2, 1f));
    float num3 = 0.053f * f;
    Rect position1 = new Rect((float) -Screen.width / 2f, (float) -Screen.height / 2f, (float) Screen.width, (float) Screen.height);
    Rect position2 = new Rect((float) -Screen.width / 2f - num3, (float) -Screen.height / 2f - num3, (float) Screen.width, (float) Screen.height);
    Color color = GUI.color;
    Color white = Color.white;
    Color black = Color.black;
    white.a = black.a = Mathf.Sin(num1 * 3.14f) * 2f;
    GUI.color = black;
    GUI.Label(position1, this.currentShrinkingCenterText, this.shrinkingCenterFontStyle);
    GUI.color = white;
    GUI.Label(position2, this.currentShrinkingCenterText, this.shrinkingCenterFontStyle);
    GUI.color = color;
    GUI.matrix = matrix;
  }
}
