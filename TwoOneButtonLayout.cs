﻿// Decompiled with JetBrains decompiler
// Type: TwoOneButtonLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TwoOneButtonLayout : ContentLayout
{
  public override void Awake()
  {
    GameObject gameObject1 = new GameObject("contentArea1");
    GameObject gameObject2 = new GameObject("contentArea2");
    GameObject gameObject3 = new GameObject("buttonArea");
    gameObject1.layer = this.gameObject.layer;
    gameObject2.layer = this.gameObject.layer;
    gameObject3.layer = this.gameObject.layer;
    this.ContentArea.Add(gameObject1);
    this.ContentArea.Add(gameObject2);
    this.ContentArea.Add(gameObject3);
    base.Awake();
    gameObject1.transform.localPosition = new Vector3(24f, -58f, -1f);
    gameObject2.transform.localPosition = new Vector3(715f, -138f, -1f);
    gameObject3.transform.localPosition = this.buttonAreaPosition;
  }
}
