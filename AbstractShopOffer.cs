﻿// Decompiled with JetBrains decompiler
// Type: AbstractShopOffer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

internal class AbstractShopOffer : IShopOffer
{
  protected ShopDiscount Discount;

  protected AbstractShopOffer(int percentage, int expiry)
  {
    if (0 > percentage)
      throw new ArgumentException("You must specify a non-zero percentage value.", "percentage");
    if (100 < percentage)
      throw new ArgumentException("You cannot specify a percentage larger than 100.", "percentage");
    if (0 >= expiry)
      throw new ArgumentException("The expiration timestamp appears to be invalid (" + expiry.ToString() + ").", "expiry");
    this.Discount = new ShopDiscount(percentage, expiry);
  }

  public int Expiry()
  {
    return this.Discount.Expiry;
  }

  public int Percentage()
  {
    return this.Discount.Percentage;
  }
}
