﻿// Decompiled with JetBrains decompiler
// Type: BPDebug
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BPDebug
{
  private static BPDebug.Level m_debugLevel = BPDebug.Level.Info;

  public static BPDebug.Level OutputLevel
  {
    get
    {
      return BPDebug.m_debugLevel;
    }
    set
    {
      BPDebug.m_debugLevel = value;
    }
  }

  public bool isDebugBuild
  {
    get
    {
      return Debug.isDebugBuild;
    }
  }

  public static LogDelegate Log
  {
    get
    {
      if (BPDebug.m_debugLevel >= BPDebug.Level.Info)
        return new LogDelegate(Debug.Log);
      return new LogDelegate(BPDebug.LogNull);
    }
  }

  public static LogDelegate LogSpam
  {
    get
    {
      if (BPDebug.m_debugLevel >= BPDebug.Level.Spam)
        return new LogDelegate(Debug.Log);
      return new LogDelegate(BPDebug.LogNull);
    }
  }

  public static LogDelegate LogWarning
  {
    get
    {
      if (BPDebug.m_debugLevel >= BPDebug.Level.Warning)
        return new LogDelegate(Debug.LogWarning);
      return new LogDelegate(BPDebug.LogNull);
    }
  }

  public static LogDelegate LogError
  {
    get
    {
      if (BPDebug.m_debugLevel >= BPDebug.Level.Error)
        return new LogDelegate(Debug.LogError);
      return new LogDelegate(BPDebug.LogNull);
    }
  }

  public static LogDelegateContext LogContext
  {
    get
    {
      if (BPDebug.m_debugLevel >= BPDebug.Level.Info)
        return new LogDelegateContext(Debug.Log);
      return new LogDelegateContext(BPDebug.LogNullContext);
    }
  }

  public static LogDelegateContext LogSpamContext
  {
    get
    {
      if (BPDebug.m_debugLevel >= BPDebug.Level.Spam)
        return new LogDelegateContext(Debug.Log);
      return new LogDelegateContext(BPDebug.LogNullContext);
    }
  }

  public static LogDelegateContext LogWarningContext
  {
    get
    {
      if (BPDebug.m_debugLevel >= BPDebug.Level.Warning)
        return new LogDelegateContext(Debug.LogWarning);
      return new LogDelegateContext(BPDebug.LogNullContext);
    }
  }

  public static LogDelegateContext LogErrorContext
  {
    get
    {
      if (BPDebug.m_debugLevel >= BPDebug.Level.Error)
        return new LogDelegateContext(Debug.LogError);
      return new LogDelegateContext(BPDebug.LogNullContext);
    }
  }

  private BPDebug()
  {
  }

  private static void LogNull(object msg)
  {
  }

  private static void LogNullContext(object msg, Object obj)
  {
  }

  public enum Level
  {
    None,
    Error,
    Warning,
    Info,
    Spam,
  }
}
