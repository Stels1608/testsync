﻿// Decompiled with JetBrains decompiler
// Type: RandomRotation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RandomRotation : MonoBehaviour
{
  public Vector3 speed = new Vector3((float) Random.Range(0, 3), (float) Random.Range(0, 3), (float) Random.Range(0, 3));

  public void SetSpeed(float rotation)
  {
    this.speed = new Vector3(Random.Range(0.0f, 1f), Random.Range(0.0f, 1f), Random.Range(0.0f, 1f));
    this.speed *= rotation;
  }

  private void Start()
  {
    this.enabled = false;
  }

  private void Update()
  {
    this.transform.Rotate(this.speed * Time.deltaTime, Space.Self);
  }

  private void OnBacameVisible()
  {
    this.enabled = true;
  }

  private void OnBecameInvisible()
  {
    this.enabled = false;
  }

  private void OnRendererBecameVisible()
  {
    this.enabled = true;
  }

  private void OnRendererBecameInvisible()
  {
    this.enabled = false;
  }

  public static Quaternion GetRandomRotation()
  {
    return Quaternion.LookRotation(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)));
  }

  public static Quaternion GetHorizontalRandomRotation()
  {
    return Quaternion.AngleAxis(Random.Range(0.0f, 360f), Vector3.up);
  }
}
