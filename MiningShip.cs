﻿// Decompiled with JetBrains decompiler
// Type: MiningShip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MiningShip : Ship
{
  public MiningShip(uint objectID)
    : base(objectID)
  {
    this.AutoSubscribe = true;
  }

  public override void JumpOut()
  {
    this.GetModelScript<MiningShipScript>().TakeOff();
  }

  public override void UnSubscribe()
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Subscribe();
  }

  protected void SetMarker()
  {
  }

  protected override void SetModel(GameObject prototype)
  {
    float normalizedAnimTime = 0.0f;
    bool flag = false;
    if ((Object) this.Model != (Object) null)
    {
      MiningShipScript componentInChildren = this.Model.GetComponentInChildren<MiningShipScript>();
      if ((Object) componentInChildren != (Object) null)
      {
        normalizedAnimTime = componentInChildren.GetLandingAnimationTime();
        flag = componentInChildren.IsLanding;
      }
    }
    base.SetModel(prototype);
    if (!flag)
      return;
    MiningShipScript componentInChildren1 = this.Model.GetComponentInChildren<MiningShipScript>();
    if (!((Object) componentInChildren1 != (Object) null))
      return;
    componentInChildren1.SetLandingAnimationTime(normalizedAnimTime);
    componentInChildren1.IsLanding = true;
    componentInChildren1.BeginOrContinueLanding();
  }
}
