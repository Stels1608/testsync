﻿// Decompiled with JetBrains decompiler
// Type: StoreShip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class StoreShip : ShipItem
{
  public ShipCard Card;

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Card = (ShipCard) Game.Catalogue.FetchCard(this.CardGUID, CardView.Ship);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.Card
    });
  }
}
