﻿// Decompiled with JetBrains decompiler
// Type: TabPanelOptionWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class TabPanelOptionWidget : TabPanelWidget
{
  private OptionsContent content;
  private TabButtonWidget activatedTabButton;

  public override void OnTabClick(TabButtonWidget tabButton)
  {
    this.activatedTabButton = tabButton;
    if ((Object) this.activeTabButton != (Object) null && (Object) this.activeTabButton.tabContentObject != (Object) null)
      this.content = this.activeTabButton.tabContentObject.GetComponentInChildren<OptionsContent>();
    if ((Object) this.content != (Object) null && this.content.changed)
      DialogBoxFactoryNgui.CreateUnsavedChangesDialogBox(new AnonymousDelegate(this.Apply), new AnonymousDelegate(this.Undo));
    else
      base.OnTabClick(this.activatedTabButton);
  }

  private void Apply()
  {
    if ((Object) this.content != (Object) null)
      this.content.Apply();
    base.OnTabClick(this.activatedTabButton);
  }

  private void Undo()
  {
    if ((Object) this.content != (Object) null)
      this.content.Undo();
    base.OnTabClick(this.activatedTabButton);
  }
}
