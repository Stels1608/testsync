﻿// Decompiled with JetBrains decompiler
// Type: EntryPoint
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
  private void Awake()
  {
    Log.Add("Game / Sector Start: " + (object) DateTime.UtcNow + "-UTC : Processor Info: " + (object) SystemInfo.processorCount + " " + SystemInfo.processorType + " : Memory: " + (object) SystemInfo.systemMemorySize + " : Graphic Memory: " + (object) SystemInfo.graphicsMemorySize + " : Graphic Type: " + SystemInfo.graphicsDeviceName);
    Game.Create();
    Log.Add("Game Created");
    Game.InstantiateLevel();
    Log.Add("Game Level Instantiate");
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    Log.Add("Destroyed GameObject for EntryPoint");
  }
}
