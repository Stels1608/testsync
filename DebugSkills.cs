﻿// Decompiled with JetBrains decompiler
// Type: DebugSkills
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugSkills : DebugBehaviour<DebugSkills>
{
  private Vector2 scroll = new Vector2();

  private void Start()
  {
    this.windowID = 16;
    this.SetSize(400f, 200f);
  }

  protected override void WindowFunc()
  {
    SkillList skillList = Game.Me.SkillBook;
    if (skillList.IsTrainingSkill && (bool) skillList.CurrentTrainingSkill.IsLoaded)
    {
      GUILayout.Label(string.Format("RemainingTime = {0}", (object) (float) ((double) skillList.TrainingEndTime - (double) Time.time)));
      GUILayout.Label(string.Format("CurrentTrainingSkill = {0} {1}/{2}", (object) skillList.CurrentTrainingSkill.Card.GUICard.Name, (object) skillList.CurrentTrainingSkill.Card.Level, (object) Game.Me.SkillBook.CurrentTrainingSkill.Card.MaxLevel));
    }
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    foreach (PlayerSkill playerSkill in (IEnumerable<PlayerSkill>) skillList)
    {
      if ((bool) playerSkill.IsLoaded)
      {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Learn", new GUILayoutOption[1]{ GUILayout.Width(55f) }))
          this.DoCommand("skill_learn", playerSkill.ServerID.ToString());
        if (GUILayout.Button("Unlearn", new GUILayoutOption[1]{ GUILayout.Width(55f) }))
          this.DoCommand("skill_unlearn", playerSkill.ServerID.ToString());
        GUILayout.Label("(" + (object) playerSkill.Level + ") " + playerSkill.Card.GUICard.Key);
        GUILayout.EndHorizontal();
      }
    }
    if (GUILayout.Button("All to max"))
    {
      foreach (PlayerSkill playerSkill in (IEnumerable<PlayerSkill>) skillList)
      {
        if ((bool) playerSkill.IsLoaded)
        {
          for (int index = 0; index < 10; ++index)
            this.DoCommand("skill_learn", playerSkill.ServerID.ToString());
        }
      }
    }
    GUILayout.EndScrollView();
  }

  private void DoCommand(params string[] p)
  {
    DebugProtocol.GetProtocol().CommandList(p);
  }
}
