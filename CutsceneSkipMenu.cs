﻿// Decompiled with JetBrains decompiler
// Type: CutsceneSkipMenu
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class CutsceneSkipMenu : MonoBehaviour
{
  private Texture2D greyAlphaTex;
  private bool isShown;
  private GUIStyle fontStyle;
  private string cutsceneText;
  private string skipText;
  private string continueText;

  private void Awake()
  {
    Color color = new Color(0.05f, 0.05f, 0.05f, 0.8f);
    this.greyAlphaTex = ImageUtils.CreateTexture(Vector2i.one, color);
    this.fontStyle = new GUIStyle(GUIStyle.none);
    this.fontStyle.font = Gui.Options.fontEurostileTRegCon;
    this.fontStyle.normal.textColor = Color.grey;
    this.fontStyle.hover.textColor = Color.white;
    this.fontStyle.fontSize = 40;
    this.fontStyle.alignment = TextAnchor.LowerCenter;
    this.fontStyle.wordWrap = true;
    this.cutsceneText = Tools.ParseMessage("%$bgo.cutscenes.skip_screen.cutscene%").ToUpper();
    this.skipText = Tools.ParseMessage("%$bgo.cutscenes.skip_screen.skip%").ToUpper();
    this.continueText = Tools.ParseMessage("%$bgo.cutscenes.skip_screen.resume%").ToUpper();
  }

  private void OnGUI()
  {
    if (!this.isShown)
      return;
    GUI.depth = -1;
    GUI.DrawTexture(new Rect(0.0f, 0.0f, (float) Screen.width, (float) Screen.height), (Texture) this.greyAlphaTex);
    float num = (float) Screen.height / 768f;
    this.fontStyle.fontSize = (int) (40.0 * (double) num);
    this.fontStyle.alignment = TextAnchor.LowerCenter;
    this.fontStyle.normal.textColor = Color.white;
    GUI.Label(new Rect(0.0f, 0.0f, (float) Screen.width, (float) Screen.height * 0.4f), this.cutsceneText, this.fontStyle);
    this.fontStyle.fontSize = (int) (35.0 * (double) num);
    this.fontStyle.alignment = TextAnchor.UpperLeft;
    GUIContent content1 = new GUIContent(this.continueText);
    Vector2 vector2_1 = this.fontStyle.CalcSize(content1);
    Rect position1 = new Rect((float) ((double) Screen.width * 0.300000011920929 - (double) vector2_1.x / 2.0), (float) Screen.height * 0.6f, vector2_1.x, vector2_1.y);
    this.fontStyle.normal.textColor = !position1.Contains(Event.current.mousePosition) ? Color.grey : Color.white;
    if (GUI.Button(position1, content1, this.fontStyle))
      this.OnContinueClicked();
    this.fontStyle.fontSize = (int) (35.0 * (double) num);
    this.fontStyle.alignment = TextAnchor.UpperLeft;
    GUIContent content2 = new GUIContent(this.skipText);
    Vector2 vector2_2 = this.fontStyle.CalcSize(content2);
    Rect position2 = new Rect((float) ((double) Screen.width * 0.699999988079071 - (double) vector2_2.x / 2.0), (float) Screen.height * 0.6f, vector2_2.x, vector2_2.y);
    this.fontStyle.normal.textColor = !position2.Contains(Event.current.mousePosition) ? Color.grey : Color.white;
    if (!GUI.Button(position2, content2, this.fontStyle))
      return;
    this.OnSkipClicked();
  }

  public void Toggle()
  {
    this.isShown = !this.isShown;
  }

  public void Hide()
  {
    this.isShown = false;
  }

  private void OnSkipClicked()
  {
    CutsceneManager.Instance.AbortAllCutscenes();
    this.Hide();
  }

  private void OnContinueClicked()
  {
    this.Hide();
  }
}
