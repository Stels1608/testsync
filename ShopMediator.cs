﻿// Decompiled with JetBrains decompiler
// Type: ShopMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;

public class ShopMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "ShopMediator";

  public ShopMediator()
    : base("ShopMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.ShopResourcesMissing);
    this.AddMessageInterest(Message.ShopSwitchCategory);
    this.AddMessageInterest(Message.ShopCanOnlyBuyOnce);
    this.AddMessageInterest(Message.ShopSellJunkConfirm);
    this.AddMessageInterest(Message.ShopSellJunk);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.ShopResourcesMissing:
        this.OnResourcesMissing((List<ResourceType>) message.Data);
        break;
      case Message.ShopSwitchCategory:
        this.OnShopSwitchCategory((ShopCategory) message.Data);
        break;
      case Message.ShopCanOnlyBuyOnce:
        this.OnCanOnlyBuyOnce();
        break;
      case Message.ShopSellJunkConfirm:
        this.OnSellJunkCalculatePrice((GUIShopSellJunkWindow) message.Data);
        break;
      case Message.ShopSellJunk:
        this.OnSellJunk();
        break;
    }
  }

  private void OnResourcesMissing(List<ResourceType> missingResources)
  {
    GUIShopCantbuyWindow shopCantbuyWindow = Game.GUIManager.Find<GUIShopCantbuyWindow>();
    if (shopCantbuyWindow.forRepeat && shopCantbuyWindow.IsRendered)
      return;
    shopCantbuyWindow.MissingResources = missingResources;
    shopCantbuyWindow.forRepeat = false;
    shopCantbuyWindow.Show();
  }

  private void OnShopSwitchCategory(ShopCategory category)
  {
    Game.GUIManager.Find<ShopWindow>().SelectCategory(category, ShopItemType.None);
  }

  private void OnCanOnlyBuyOnce()
  {
    GUIShopCantbuyWindow shopCantbuyWindow = Game.GUIManager.Find<GUIShopCantbuyWindow>();
    shopCantbuyWindow.forRepeat = true;
    shopCantbuyWindow.Show();
  }

  private void OnSellJunkCalculatePrice(GUIShopSellJunkWindow confirmWindow)
  {
    Price price = new Price();
    foreach (ShipItem shipItem in (IEnumerable<ShipItem>) Game.Me.Hold)
    {
      if (shipItem.ShopItemCard.ItemType == ShopItemType.Junk)
      {
        uint count = 1;
        if (shipItem is ItemCountable)
          count = ((ItemCountable) shipItem).count;
        price.AddPrice(shipItem.ShopItemCard.SellPrice, count);
      }
    }
    confirmWindow.SellPrice = price;
  }

  private void OnSellJunk()
  {
    foreach (ShipItem shipItem in (IEnumerable<ShipItem>) Game.Me.Hold)
    {
      if (shipItem.ShopItemCard.ItemType == ShopItemType.Junk)
        shipItem.MoveTo((IContainer) Shop.Local);
    }
  }
}
