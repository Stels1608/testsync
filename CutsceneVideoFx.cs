﻿// Decompiled with JetBrains decompiler
// Type: CutsceneVideoFx
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public static class CutsceneVideoFx
{
  public static void FadeIn(VideoUiHandler videoUiHandler, float duration, float delay = 0.0f)
  {
    CutsceneVideoFx.GetFader(videoUiHandler).FadeIn(delay, duration);
  }

  public static void FadeOut(VideoUiHandler videoUiHandler, float duration, float delay = 0.0f)
  {
    CutsceneVideoFx.GetFader(videoUiHandler).FadeOut(delay, duration);
  }

  public static void TurnToBlack(VideoUiHandler videoUiHandler)
  {
    CutsceneVideoFx.GetFader(videoUiHandler).TurnToBlack();
  }

  public static void FadeInVideoStripes(VideoUiHandler videoUiHandler, float fadeInTime = 1f)
  {
    (videoUiHandler.gameObject.GetComponent<CutsceneLetterboxRenderer>() ?? videoUiHandler.gameObject.AddComponent<CutsceneLetterboxRenderer>()).FadeIn(fadeInTime);
  }

  public static void FadeOutVideoStripes(VideoUiHandler videoUiHandler)
  {
    CutsceneLetterboxRenderer letterboxRenderer = videoUiHandler.gameObject.GetComponent<CutsceneLetterboxRenderer>() ?? videoUiHandler.gameObject.AddComponent<CutsceneLetterboxRenderer>();
    CutsceneVideoFx.ShowSubtitleText(videoUiHandler, 0.0f, string.Empty);
    letterboxRenderer.FadeOut();
  }

  public static void ShowSubtitleText(VideoUiHandler videoUiHandler, float delay, string textId)
  {
    string message = Tools.ParseMessage(textId);
    CutsceneTextRenderer cutsceneTextRenderer = videoUiHandler.GetComponent<CutsceneTextRenderer>();
    if ((UnityEngine.Object) cutsceneTextRenderer == (UnityEngine.Object) null)
      cutsceneTextRenderer = videoUiHandler.gameObject.AddComponent<CutsceneTextRenderer>();
    cutsceneTextRenderer.SetSubtitleText(message);
  }

  public static void ShowShrinkingCenterText(VideoUiHandler videoUiHandler, float delay, string textId)
  {
    string message = Tools.ParseMessage(textId);
    Tools.ParseMessage(string.Empty);
    CutsceneTextRenderer cutsceneTextRenderer = videoUiHandler.GetComponent<CutsceneTextRenderer>();
    if ((UnityEngine.Object) cutsceneTextRenderer == (UnityEngine.Object) null)
      cutsceneTextRenderer = videoUiHandler.gameObject.AddComponent<CutsceneTextRenderer>();
    cutsceneTextRenderer.QueueZoomingCenterText(message, delay);
  }

  public static void ShowSectorTextRight(VideoUiHandler videoUiHandler, float delay, string textId)
  {
    string message = Tools.ParseMessage(textId);
    CutsceneTextRenderer cutsceneTextRenderer = videoUiHandler.GetComponent<CutsceneTextRenderer>();
    if ((UnityEngine.Object) cutsceneTextRenderer == (UnityEngine.Object) null)
      cutsceneTextRenderer = videoUiHandler.gameObject.AddComponent<CutsceneTextRenderer>();
    cutsceneTextRenderer.QueueSceneDescriptionText(message, delay);
  }

  public static void HideUI()
  {
    Game.GUIManager.IsRendered = false;
  }

  public static CutsceneScreenFader GetFader(VideoUiHandler videoUiHandler)
  {
    CutsceneScreenFader cutsceneScreenFader = videoUiHandler.gameObject.GetComponent<CutsceneScreenFader>();
    if ((UnityEngine.Object) cutsceneScreenFader == (UnityEngine.Object) null)
      cutsceneScreenFader = videoUiHandler.gameObject.AddComponent<CutsceneScreenFader>();
    return cutsceneScreenFader;
  }

  public static void SetMotionBlur(Camera cam, float blurAmount, bool useExtraBlur)
  {
    if ((UnityEngine.Object) cam == (UnityEngine.Object) null)
    {
      Debug.LogError((object) "SetMotionBlur(): Passed camera is null.");
    }
    else
    {
      MotionBlur component = cam.gameObject.GetComponent<MotionBlur>();
      if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      {
        Debug.LogError((object) "SetMotionBlur(): Passed camera didn't have a motion blur component.");
      }
      else
      {
        component.enabled = true;
        component.blurAmount = blurAmount;
        if ((double) Math.Abs(blurAmount) >= 1.0 / 1000.0)
          return;
        component.enabled = false;
        component.extraBlur = useExtraBlur;
      }
    }
  }

  public static void ShowFadingBsgoLogo(VideoUiHandler videoUiHandler)
  {
    CutsceneBsgoLogoRenderer bsgoLogoRenderer = videoUiHandler.gameObject.GetComponent<CutsceneBsgoLogoRenderer>();
    if ((UnityEngine.Object) bsgoLogoRenderer == (UnityEngine.Object) null)
      bsgoLogoRenderer = videoUiHandler.gameObject.AddComponent<CutsceneBsgoLogoRenderer>();
    bsgoLogoRenderer.ShowLogo();
  }
}
