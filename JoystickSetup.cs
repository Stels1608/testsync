﻿// Decompiled with JetBrains decompiler
// Type: JoystickSetup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class JoystickSetup
{
  public static float DeadZone = 0.05f;
  private static float sensitivityExponent = 1f;
  private static List<Action> dualAxisActions = new List<Action>() { Action.JoystickSpeedController };
  private static List<JoystickButtonCode> dualAxisList = new List<JoystickButtonCode>();
  private static bool showXbox360Buttons = true;
  private const string AXIS_X = "joystick x axis";
  private const string AXIS_Y = "joystick y axis";
  private const string AXIS_3 = "joystick 3rd axis";
  private const string AXIS_4 = "joystick 4th axis";
  private const string AXIS_5 = "joystick 5th axis";
  private const string AXIS_6 = "joystick 6th axis";
  private const string AXIS_7 = "joystick 7th axis";
  private const string AXIS_8 = "joystick 8th axis";
  private const string AXIS_9 = "joystick 9th axis";
  private const string AXIS_10 = "joystick 10th axis";
  private const string BUTTON_0 = "joystick button 0";
  private const string BUTTON_1 = "joystick button 1";
  private const string BUTTON_2 = "joystick button 2";
  private const string BUTTON_3 = "joystick button 3";
  private const string BUTTON_4 = "joystick button 4";
  private const string BUTTON_5 = "joystick button 5";
  private const string BUTTON_6 = "joystick button 6";
  private const string BUTTON_7 = "joystick button 7";
  private const string BUTTON_8 = "joystick button 8";
  private const string BUTTON_9 = "joystick button 9";
  private const string BUTTON_10 = "joystick button 10";
  private const string BUTTON_11 = "joystick button 11";
  private const string BUTTON_12 = "joystick button 12";
  private const string BUTTON_13 = "joystick button 13";
  private const string BUTTON_14 = "joystick button 14";
  private const string BUTTON_15 = "joystick button 15";
  private const string BUTTON_16 = "joystick button 16";
  private const string BUTTON_17 = "joystick button 17";
  private const string BUTTON_18 = "joystick button 18";
  private const string BUTTON_19 = "joystick button 19";
  private readonly Dictionary<JoystickButtonCode, string> inputStringMapping;

  public static float SensitivityExponent
  {
    get
    {
      return JoystickSetup.sensitivityExponent;
    }
  }

  public static float Sensitivity
  {
    set
    {
      JoystickSetup.sensitivityExponent = (float) (3.0 - (double) value * 2.0);
    }
  }

  public static bool Enabled { get; set; }

  public static List<JoystickButtonCode> DualAxisList
  {
    get
    {
      return JoystickSetup.dualAxisList;
    }
  }

  public Dictionary<JoystickButtonCode, string> InputStringMapping
  {
    get
    {
      return this.inputStringMapping;
    }
  }

  public static bool IsXbox360Detected
  {
    get
    {
      return ((IEnumerable<string>) Input.GetJoystickNames()).Any<string>((Func<string, bool>) (joystickName => joystickName.ToLower().Contains("xbox 360")));
    }
  }

  public static bool ShowXbox360Buttons
  {
    get
    {
      if (JoystickSetup.IsXbox360Detected)
        return JoystickSetup.showXbox360Buttons;
      return false;
    }
    set
    {
      JoystickSetup.showXbox360Buttons = value;
    }
  }

  public JoystickSetup()
  {
    this.inputStringMapping = new Dictionary<JoystickButtonCode, string>();
    this.inputStringMapping.Add(JoystickButtonCode.AxisX_Positive, "joystick x axis");
    this.inputStringMapping.Add(JoystickButtonCode.AxisX_Negative, "joystick x axis");
    this.inputStringMapping.Add(JoystickButtonCode.AxisY_Positive, "joystick y axis");
    this.inputStringMapping.Add(JoystickButtonCode.AxisY_Negative, "joystick y axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis3_Positive, "joystick 3rd axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis3_Negative, "joystick 3rd axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis4_Positive, "joystick 4th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis4_Negative, "joystick 4th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis5_Positive, "joystick 5th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis5_Negative, "joystick 5th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis6_Positive, "joystick 6th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis6_Negative, "joystick 6th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis7_Positive, "joystick 7th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis7_Negative, "joystick 7th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis8_Positive, "joystick 8th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis8_Negative, "joystick 8th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis9_Positive, "joystick 9th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis9_Negative, "joystick 9th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis10_Positive, "joystick 10th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Axis10_Negative, "joystick 10th axis");
    this.inputStringMapping.Add(JoystickButtonCode.Button0, "joystick button 0");
    this.inputStringMapping.Add(JoystickButtonCode.Button1, "joystick button 1");
    this.inputStringMapping.Add(JoystickButtonCode.Button2, "joystick button 2");
    this.inputStringMapping.Add(JoystickButtonCode.Button3, "joystick button 3");
    this.inputStringMapping.Add(JoystickButtonCode.Button4, "joystick button 4");
    this.inputStringMapping.Add(JoystickButtonCode.Button5, "joystick button 5");
    this.inputStringMapping.Add(JoystickButtonCode.Button6, "joystick button 6");
    this.inputStringMapping.Add(JoystickButtonCode.Button7, "joystick button 7");
    this.inputStringMapping.Add(JoystickButtonCode.Button8, "joystick button 8");
    this.inputStringMapping.Add(JoystickButtonCode.Button9, "joystick button 9");
    this.inputStringMapping.Add(JoystickButtonCode.Button10, "joystick button 10");
    this.inputStringMapping.Add(JoystickButtonCode.Button11, "joystick button 11");
    this.inputStringMapping.Add(JoystickButtonCode.Button12, "joystick button 12");
    this.inputStringMapping.Add(JoystickButtonCode.Button13, "joystick button 13");
    this.inputStringMapping.Add(JoystickButtonCode.Button14, "joystick button 14");
    this.inputStringMapping.Add(JoystickButtonCode.Button15, "joystick button 15");
    this.inputStringMapping.Add(JoystickButtonCode.Button16, "joystick button 16");
    this.inputStringMapping.Add(JoystickButtonCode.Button17, "joystick button 17");
    this.inputStringMapping.Add(JoystickButtonCode.Button18, "joystick button 18");
    this.inputStringMapping.Add(JoystickButtonCode.Button19, "joystick button 19");
  }

  public static JoystickInputType GetJoystickInputType(JoystickButtonCode joystickButtonCode)
  {
    if (joystickButtonCode == JoystickButtonCode.None)
      return JoystickInputType.None;
    return joystickButtonCode < JoystickButtonCode.Button0 ? JoystickInputType.Axis : JoystickInputType.Button;
  }

  public static void PrintJoystickNames()
  {
    foreach (string joystickName in Input.GetJoystickNames())
      Debug.Log((object) ("Detected Joystick Device: " + joystickName));
  }

  public static void UpdateDualAxisList(InputBinder inputbinder)
  {
    JoystickSetup.dualAxisList.Clear();
    foreach (int num in Enum.GetValues(typeof (JoystickButtonCode)))
    {
      JoystickButtonCode joystickButtonCode = (JoystickButtonCode) num;
      using (List<IInputBinding>.Enumerator enumerator = inputbinder.TryGetTriggerBindingList(InputDevice.Joystick, (ushort) joystickButtonCode).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JoystickBinding joystickBinding = (JoystickBinding) enumerator.Current;
          if (JoystickSetup.dualAxisActions.Contains(joystickBinding.Action) && joystickBinding.TriggerCode != JoystickButtonCode.None)
          {
            JoystickSetup.dualAxisList.Add(joystickBinding.TriggerCode);
            JoystickSetup.dualAxisList.Add(JoystickSetup.GetOtherDualAxis(joystickBinding.TriggerCode));
          }
        }
      }
    }
  }

  public static bool IsAxisNegative(JoystickButtonCode axisCode)
  {
    if (JoystickSetup.GetJoystickInputType(axisCode) != JoystickInputType.Axis)
    {
      Debug.LogWarning((object) "IsAxisNegative: No axis given");
      return false;
    }
    return (int) axisCode % 2 == 0;
  }

  public static bool IsDualAxisAction(Action action)
  {
    return JoystickSetup.dualAxisActions.Contains(action);
  }

  public static JoystickButtonCode GetPartnerAxis(JoystickButtonCode buttonCode)
  {
    if (JoystickSetup.GetJoystickInputType(buttonCode) != JoystickInputType.Axis)
    {
      Debug.LogWarning((object) ("GetPartnerAxis(): Provided JoystickCode is not an axis: " + (object) buttonCode));
      return JoystickButtonCode.None;
    }
    if (JoystickSetup.IsAxisNegative(buttonCode))
      return buttonCode - 1;
    return buttonCode + 1;
  }

  public static JoystickButtonCode GetOtherDualAxis(JoystickButtonCode buttonCode)
  {
    JoystickButtonCode partnerAxis = JoystickSetup.GetPartnerAxis(buttonCode);
    if (!JoystickSetup.dualAxisList.Contains(buttonCode) && !JoystickSetup.dualAxisList.Contains(partnerAxis))
      return JoystickButtonCode.None;
    return partnerAxis;
  }

  public static string GetXboxIconAlias(JoystickButtonCode buttonCode)
  {
    JoystickButtonCode joystickButtonCode = buttonCode;
    switch (joystickButtonCode)
    {
      case JoystickButtonCode.None:
        return "xboxControllerNone";
      case JoystickButtonCode.AxisX_Positive:
        return "xboxControllerLeftThumbstickAxisXpos";
      case JoystickButtonCode.AxisX_Negative:
        return "xboxControllerLeftThumbstickAxisXneg";
      case JoystickButtonCode.AxisY_Positive:
        return "xboxControllerLeftThumbstickAxisYneg";
      case JoystickButtonCode.AxisY_Negative:
        return "xboxControllerLeftThumbstickAxisYpos";
      case JoystickButtonCode.Axis3_Positive:
      case JoystickButtonCode.Axis9_Positive:
        return "xboxControllerLeftTrigger";
      case JoystickButtonCode.Axis3_Negative:
      case JoystickButtonCode.Axis10_Positive:
        return "xboxControllerRightTrigger";
      case JoystickButtonCode.Axis4_Positive:
        return "xboxControllerRightThumbstickAxisXpos";
      case JoystickButtonCode.Axis4_Negative:
        return "xboxControllerRightThumbstickAxisXneg";
      case JoystickButtonCode.Axis5_Positive:
        return "xboxControllerRightThumbstickAxisYneg";
      case JoystickButtonCode.Axis5_Negative:
        return "xboxControllerRightThumbstickAxisYpos";
      case JoystickButtonCode.Axis6_Positive:
        return "xboxControllerDPadAxisXpos";
      case JoystickButtonCode.Axis6_Negative:
        return "xboxControllerDPadAxisXneg";
      case JoystickButtonCode.Axis7_Positive:
        return "xboxControllerDPadAxisYpos";
      case JoystickButtonCode.Axis7_Negative:
        return "xboxControllerDPadAxisYneg";
      default:
        switch (joystickButtonCode - 100)
        {
          case JoystickButtonCode.None:
            return "xboxControllerButtonA";
          case JoystickButtonCode.AxisX_Positive:
            return "xboxControllerButtonB";
          case JoystickButtonCode.AxisX_Negative:
            return "xboxControllerButtonX";
          case JoystickButtonCode.AxisY_Positive:
            return "xboxControllerButtonY";
          case JoystickButtonCode.AxisY_Negative:
            return "xboxControllerLeftShoulder";
          case JoystickButtonCode.Axis3_Positive:
            return "xboxControllerRightShoulder";
          case JoystickButtonCode.Axis3_Negative:
            return "xboxControllerBack";
          case JoystickButtonCode.Axis4_Positive:
            return "xboxControllerStart";
          case JoystickButtonCode.Axis4_Negative:
            return "xboxControllerLeftThumbstickPressed";
          case JoystickButtonCode.Axis5_Positive:
            return "xboxControllerRightThumbstickPressed";
          default:
            return "xboxControllerUndefined";
        }
    }
  }
}
