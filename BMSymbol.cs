﻿// Decompiled with JetBrains decompiler
// Type: BMSymbol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class BMSymbol
{
  public string sequence;
  public string spriteName;
  private UISpriteData mSprite;
  private bool mIsValid;
  private int mLength;
  private int mOffsetX;
  private int mOffsetY;
  private int mWidth;
  private int mHeight;
  private int mAdvance;
  private Rect mUV;

  public int length
  {
    get
    {
      if (this.mLength == 0)
        this.mLength = this.sequence.Length;
      return this.mLength;
    }
  }

  public int offsetX
  {
    get
    {
      return this.mOffsetX;
    }
  }

  public int offsetY
  {
    get
    {
      return this.mOffsetY;
    }
  }

  public int width
  {
    get
    {
      return this.mWidth;
    }
  }

  public int height
  {
    get
    {
      return this.mHeight;
    }
  }

  public int advance
  {
    get
    {
      return this.mAdvance;
    }
  }

  public Rect uvRect
  {
    get
    {
      return this.mUV;
    }
  }

  public void MarkAsChanged()
  {
    this.mIsValid = false;
  }

  public bool Validate(UIAtlas atlas)
  {
    if ((UnityEngine.Object) atlas == (UnityEngine.Object) null)
      return false;
    if (!this.mIsValid)
    {
      if (string.IsNullOrEmpty(this.spriteName))
        return false;
      this.mSprite = !((UnityEngine.Object) atlas != (UnityEngine.Object) null) ? (UISpriteData) null : atlas.GetSprite(this.spriteName);
      if (this.mSprite != null)
      {
        Texture texture = atlas.texture;
        if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
        {
          this.mSprite = (UISpriteData) null;
        }
        else
        {
          this.mUV = new Rect((float) this.mSprite.x, (float) this.mSprite.y, (float) this.mSprite.width, (float) this.mSprite.height);
          this.mUV = NGUIMath.ConvertToTexCoords(this.mUV, texture.width, texture.height);
          this.mOffsetX = this.mSprite.paddingLeft;
          this.mOffsetY = this.mSprite.paddingTop;
          this.mWidth = this.mSprite.width;
          this.mHeight = this.mSprite.height;
          this.mAdvance = this.mSprite.width + (this.mSprite.paddingLeft + this.mSprite.paddingRight);
          this.mIsValid = true;
        }
      }
    }
    return this.mSprite != null;
  }
}
