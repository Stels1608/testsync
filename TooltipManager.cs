﻿// Decompiled with JetBrains decompiler
// Type: TooltipManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class TooltipManager : MonoBehaviour
{
  [SerializeField]
  private GameObject content;
  [SerializeField]
  private UISprite background;
  private Camera cam;
  private GameObject tooltipObject;

  public GameObject TooltipObject
  {
    get
    {
      return this.tooltipObject;
    }
  }

  public void Start()
  {
    this.gameObject.SetActive(false);
    this.GetComponent<UIPanel>().depth = 200;
  }

  public void ShowTooltip(TooltipType type, GameObject tooltip)
  {
    this.gameObject.SetActive(true);
    this.tooltipObject = tooltip;
    Vector3 size = NGUIMath.CalculateRelativeWidgetBounds(this.tooltipObject.transform).size;
    this.tooltipObject.transform.parent = this.content.transform;
    this.tooltipObject.transform.localPosition = Vector3.zero;
    this.tooltipObject.transform.localScale = Vector3.one;
    this.background.width = (int) ((double) size.x + (double) Math.Abs(this.content.transform.localPosition.x * 2f));
    this.background.height = (int) ((double) size.y + (double) Math.Abs(this.content.transform.localPosition.y * 2f));
    this.gameObject.transform.localPosition = (Vector3) this.SetTooltipPosition(this.gameObject);
  }

  public void HideTooltip()
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.tooltipObject);
    this.gameObject.SetActive(false);
  }

  private Vector2 SetTooltipPosition(GameObject ttip)
  {
    Vector3 position = Input.mousePosition;
    int num1 = this.background.width + 30;
    int num2 = this.background.height + 30;
    Camera componentInChildren = GameObject.Find("UIRoot(Clone)").GetComponentInChildren<Camera>();
    if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
    {
      position.x = Mathf.Clamp01(position.x / (float) Screen.width);
      position.y = Mathf.Clamp01(position.y / (float) Screen.height);
      float num3 = (float) Screen.height * 0.5f / (componentInChildren.orthographicSize / ttip.transform.parent.lossyScale.y);
      Vector2 vector2 = new Vector2(num3 * (float) num1 / (float) Screen.width, num3 * (float) num2 / (float) Screen.height);
      position.x = Mathf.Min(position.x, 1f - vector2.x);
      position.y = Mathf.Max(position.y, vector2.y);
      ttip.transform.position = componentInChildren.ViewportToWorldPoint(position);
      position = ttip.transform.localPosition;
      position.x = Mathf.Round(position.x);
      position.y = Mathf.Round(position.y);
      position += new Vector3(10f, -10f, 0.0f);
      ttip.transform.localPosition = position;
    }
    return (Vector2) position;
  }
}
