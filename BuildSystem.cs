﻿// Decompiled with JetBrains decompiler
// Type: BuildSystem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Unity5AssetHandling;
using UnityEngine;

public class BuildSystem : MonoBehaviour
{
  public string PrefabName;
  private AssetRequest assetRequest;

  private void Start()
  {
    this.assetRequest = AssetCatalogue.Instance.Request(this.PrefabName, false);
  }

  private void Update()
  {
    if (!this.assetRequest.IsDone)
      return;
    if (this.assetRequest.Asset != (Object) null)
    {
      GameObject gameObject = (GameObject) Object.Instantiate(this.assetRequest.Asset);
      gameObject.name = this.assetRequest.Asset.name;
      gameObject.transform.position = this.transform.position;
      gameObject.transform.rotation = this.transform.rotation;
      gameObject.transform.parent = this.transform;
      gameObject.transform.localScale = Vector3.one;
      this.SendMessage("OnModelChanged", (object) gameObject, SendMessageOptions.DontRequireReceiver);
    }
    Object.Destroy((Object) this);
  }
}
