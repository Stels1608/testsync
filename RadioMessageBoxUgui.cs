﻿// Decompiled with JetBrains decompiler
// Type: RadioMessageBoxUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RadioMessageBoxUgui : RadioMessageBoxUi
{
  private string mainText = string.Empty;
  private string hintText = string.Empty;
  [SerializeField]
  private GameObject okButton;
  [SerializeField]
  private TextMeshProUGUI okButtonLabel;
  [SerializeField]
  private TextMeshProUGUI messageLabel;
  [SerializeField]
  private TextMeshProUGUI hintLabel;
  [SerializeField]
  private GameObject hintImageParent;
  [SerializeField]
  private Image hintImage;
  [SerializeField]
  private Image npcPortrait;
  [SerializeField]
  private TextMeshProUGUI npcNameLabel;
  [SerializeField]
  private Button navButtonLeft;
  [SerializeField]
  private Button navButtonRight;
  [SerializeField]
  private GameObject pageIndexGroup;
  [SerializeField]
  private Text pageIndexLabel;
  private Coroutine runningCoroutine;
  private bool typewriterFinished;

  protected override void SetOkButtonText(string text)
  {
    this.okButtonLabel.text = text;
  }

  protected override void SetMainText(string mainText)
  {
    this.mainText = mainText;
  }

  protected override void SetHintText(string hintText)
  {
    this.hintText = hintText;
  }

  protected override void SetHintImage2D(Texture2D hintImageTex2D)
  {
    this.hintImage.sprite = UnityEngine.Sprite.Create(hintImageTex2D, new Rect(0.0f, 0.0f, (float) hintImageTex2D.width, (float) hintImageTex2D.height), new Vector2(0.5f, 0.5f));
  }

  protected override void ShowHintImage(bool isVisible)
  {
    this.hintImageParent.gameObject.SetActive(isVisible);
    this.hintImage.enabled = isVisible;
  }

  protected override void ShowOkButton(bool isVisible)
  {
    this.okButton.SetActive(isVisible);
  }

  protected override void EnableLeftNavigationButton(bool isEnabled)
  {
    this.navButtonLeft.interactable = isEnabled;
    this.UpdateNavigationElements();
  }

  protected override void EnableRightNavigationButton(bool isEnabled)
  {
    this.navButtonRight.interactable = isEnabled;
    this.UpdateNavigationElements();
  }

  private void UpdateNavigationElements()
  {
    this.pageIndexGroup.gameObject.SetActive(this.navButtonLeft.interactable || this.navButtonRight.interactable);
  }

  protected override void SetNavigationLabel(string navPageIndex)
  {
    this.pageIndexLabel.text = navPageIndex;
  }

  protected override void SetPilotName(string pilotName)
  {
    this.npcNameLabel.text = pilotName.ToUpper();
  }

  protected override void SetPilotPortrait(Texture2D portrait)
  {
    this.npcPortrait.sprite = UnityEngine.Sprite.Create(portrait, new Rect(0.0f, 0.0f, (float) portrait.width, (float) portrait.height), new Vector2(0.5f, 0.5f));
  }

  protected override void DisplayMessageText()
  {
    if (this.runningCoroutine != null)
      this.StopCoroutine(this.runningCoroutine);
    this.runningCoroutine = this.StartCoroutine(this.DisplayMessageWithFx());
  }

  [DebuggerHidden]
  private IEnumerator DisplayMessageWithFx()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RadioMessageBoxUgui.\u003CDisplayMessageWithFx\u003Ec__Iterator43() { \u003C\u003Ef__this = this };
  }

  private bool TextPrinter(TextMeshProUGUI textfield, string text, float startTime)
  {
    int a = (int) ((double) (Time.time - startTime) / 0.0149999996647239);
    if (a < 0)
      a = 1073741823;
    int length = Mathf.Min(a, text.Length);
    textfield.text = text.Substring(0, length);
    return length != text.Length;
  }

  protected override void OnEnable()
  {
    base.OnEnable();
    if (this.typewriterFinished)
      return;
    this.DisplayMessageText();
  }

  protected override void DestroyWindow()
  {
    Object.Destroy((Object) this.gameObject);
  }
}
