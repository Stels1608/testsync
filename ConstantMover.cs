﻿// Decompiled with JetBrains decompiler
// Type: ConstantMover
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ConstantMover : MonoBehaviour
{
  public float speed = 10f;

  private void Update()
  {
    this.transform.Translate(this.speed * Time.deltaTime, -this.speed * Time.deltaTime, 0.0f);
  }
}
