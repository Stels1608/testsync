﻿// Decompiled with JetBrains decompiler
// Type: GuiUIOMassActivationPopup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Common;
using UnityEngine;

public class GuiUIOMassActivationPopup : GuiPanel
{
  private readonly ItemCountable item;
  private readonly GuiImage cubitsIcon;
  private readonly GuiLabel cubitsCostLabel;
  private readonly Counter activationCounter;
  private readonly GuiButton activateButton;
  private readonly float takCubitsPrice;
  private readonly uint takCount;

  public GuiUIOMassActivationPopup(ItemCountable item)
  {
    this.item = item;
    GuiImage guiImage = new GuiImage("GUI/Gui2/background");
    Vector2 vector2 = new Vector2(300f, 400f);
    guiImage.Size = vector2;
    this.Size = vector2;
    guiImage.NineSliceEdge = new GuiElementPadding(new Rect(32f, 32f, 32f, 32f));
    this.AddChild((GuiElementBase) guiImage);
    GuiButton closeButton = Gui.Options.CreateCloseButton(Game.Me.Faction);
    closeButton.Pressed = new AnonymousDelegate(this.Close);
    this.AddChild((GuiElementBase) closeButton, Align.UpRight, new Vector2(2f, -4f));
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.inflight_shop.button_augment_item%")
    {
      Font = Gui.Options.FontDS_EF_M,
      FontSize = 14
    }, Align.UpCenter, new Vector2(0.0f, 16f));
    this.AddChild((GuiElementBase) new GuiLabel(item.ItemGUICard.Name)
    {
      Font = Gui.Options.FontDS_EF_B,
      FontSize = 16
    }, Align.UpCenter, new Vector2(0.0f, 40f));
    GuiLabel guiLabel1 = new GuiLabel("%$bgo.inflight_shop.button_augment_item_description%");
    guiLabel1.Font = Gui.Options.FontDS_EF_M;
    guiLabel1.FontSize = 14;
    guiLabel1.SizeX = this.SizeX - 32f;
    this.AddChild((GuiElementBase) guiLabel1, Align.UpLeft, new Vector2(16f, 100f));
    GuiLabel guiLabel2 = new GuiLabel(item.ItemGUICard.Description);
    guiLabel2.Font = Gui.Options.FontDS_EF_M;
    guiLabel2.FontSize = 14;
    guiLabel2.SizeX = this.SizeX - 32f;
    guiLabel2.WordWrap = true;
    this.AddChild((GuiElementBase) guiLabel2, Align.UpLeft, new Vector2(16f, 116f));
    this.AddChild((GuiElementBase) new GuiItem(item.ItemGUICard, "x" + (object) item.count), Align.UpCenter, new Vector2(-70f, 200f));
    GUICard guiCard = (GUICard) Game.Catalogue.FetchCard(187088612U, CardView.GUI);
    this.takCount = Game.Me.Hold.GetCountByGUID(guiCard.CardGUID);
    this.AddChild((GuiElementBase) new GuiItem(guiCard, "x" + (object) this.takCount), Align.UpCenter, new Vector2(70f, 200f));
    this.activationCounter = new Counter();
    Counter counter = this.activationCounter;
    int num1 = 1;
    this.activationCounter.Value = num1;
    int num2 = num1;
    counter.MinValue = num2;
    this.activationCounter.MaxValue = Mathf.Min(1000, (int) item.count);
    this.activationCounter.TextboxWidth = 32f;
    this.activationCounter.OnValueChanged += new System.Action<int>(this.OnValueChanged);
    this.AddChild((GuiElementBase) this.activationCounter, Align.DownCenter, new Vector2(0.0f, -80f));
    this.activateButton = new GuiButton("%$bgo.inflight_shop.button_augment_item%");
    this.activateButton.Font = Gui.Options.FontDS_EF_M;
    this.activateButton.FontSize = 14;
    this.activateButton.Label.Alignment = TextAnchor.UpperCenter;
    this.activateButton.SizeY = 44f;
    this.activateButton.Pressed = new AnonymousDelegate(this.AnalyzeAll);
    this.AddChild((GuiElementBase) this.activateButton, Align.DownCenter, new Vector2(0.0f, -20f));
    ShipConsumableCard index = (ShipConsumableCard) Game.Catalogue.FetchCard(264733124U, CardView.ShipConsumable);
    this.takCubitsPrice = ((ShopItemCard) Game.Catalogue.FetchCard(187088612U, CardView.Price)).BuyPrice.items[index];
    this.cubitsIcon = new GuiImage(index.GUICard.GUIIcon);
    this.AddChild((GuiElementBase) this.cubitsIcon, Align.DownCenter, new Vector2(-16f, -24f));
    this.cubitsCostLabel = new GuiLabel(string.Empty);
    this.AddChild((GuiElementBase) this.cubitsCostLabel, Align.DownCenter, new Vector2(16f, -24f));
    this.OnValueChanged(1);
  }

  private void OnValueChanged(int newValue)
  {
    if ((long) this.takCount < (long) newValue)
    {
      this.activateButton.Label.Alignment = TextAnchor.UpperCenter;
      this.activateButton.Label.PositionY = 8f;
      this.activateButton.PressedSound = new GUISoundHandler(GUISound.Instance.OnBuy);
      this.cubitsIcon.IsRendered = true;
      this.cubitsCostLabel.IsRendered = true;
      this.cubitsCostLabel.Text = ((float) (uint) ((ulong) newValue - (ulong) this.takCount) * this.takCubitsPrice).ToString();
    }
    else
    {
      this.activateButton.Label.Alignment = TextAnchor.MiddleCenter;
      this.activateButton.Label.PositionY = 0.0f;
      this.activateButton.PressedSound = new GUISoundHandler(GUISound.Instance.OnSelect);
      this.cubitsIcon.IsRendered = false;
      this.cubitsCostLabel.IsRendered = false;
    }
  }

  public override void Update()
  {
    base.Update();
    this.PositionCenter = new Vector2((float) Screen.width / 2f, (float) Screen.height / 2f);
  }

  public void Close()
  {
    GuiDialogPopupManager.CloseDialog();
  }

  private void AnalyzeAll()
  {
    uint iterations = (uint) this.activationCounter.Value;
    if (iterations > this.takCount)
    {
      uint count = iterations - this.takCount;
      ItemCountable itemCountable = (ItemCountable) ((ShopDataProvider) FacadeFactory.GetInstance().FetchDataProvider("ShopDataProvider")).Shop.GetByGUID(187088612U);
      if (Game.Me.Hold.HaveMoneyBuy((ShipItem) itemCountable, (int) count))
        itemCountable.MoveTo((IContainer) Game.Me.Hold, count);
      else
        ShopWindow.OnGetCubits();
    }
    PlayerProtocol.GetProtocol().AugmentMassActivation(this.item, iterations);
    this.Close();
  }
}
