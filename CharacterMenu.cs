﻿// Decompiled with JetBrains decompiler
// Type: CharacterMenu
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CharacterMenu : MonoBehaviour, IGUIRenderable
{
  public Text titleLabel;
  public Text nameLabel;
  public Text leftLabel;
  public Text rightLabel;
  public Button nameChangeButton;
  public Button factionChangeButton;
  public Button avatarChangeButton;
  public Button deleteButton;
  public Button exitButton;

  public bool IsRendered
  {
    get
    {
      return true;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  public bool IsBigWindow
  {
    get
    {
      return false;
    }
  }

  private void Start()
  {
    this.nameChangeButton.onClick.AddListener((UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.RequestNameChange)));
    this.nameChangeButton.gameObject.AddComponent<UguiTooltip>().SetTooltip("%$bgo.character_menu.tooltip.name_change%");
    this.factionChangeButton.onClick.AddListener((UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.RequestFactionSwitch)));
    this.factionChangeButton.gameObject.AddComponent<UguiTooltip>().SetTooltip("%$bgo.character_menu.tooltip.faction_switch%");
    this.avatarChangeButton.onClick.AddListener((UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.RequestAvatarChange)));
    this.avatarChangeButton.gameObject.AddComponent<UguiTooltip>().SetTooltip("%$bgo.character_menu.tooltip.modification%");
    this.deleteButton.onClick.AddListener((UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.RequestCharacterDeletion)));
    this.deleteButton.gameObject.AddComponent<UguiTooltip>().SetTooltip("%$bgo.character_menu.tooltip.deletion%");
    this.exitButton.onClick.AddListener((UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.LeaveCharacterMenu)));
    this.avatarChangeButton.interactable = false;
  }

  public void SetCharacterInfo(string characterName, Faction faction, byte level)
  {
    this.titleLabel.text = BsgoLocalization.Get("bgo.character_menu.title").ToUpperInvariant();
    this.nameLabel.text = characterName;
    this.leftLabel.text = BsgoLocalization.Get("bgo.character_menu.faction").ToUpperInvariant() + "\n";
    Text text1 = this.leftLabel;
    string str1 = text1.text + BsgoLocalization.Get("bgo.character_menu.level").ToUpperInvariant() + "\n";
    text1.text = str1;
    Text text2 = this.leftLabel;
    string str2 = text2.text + BsgoLocalization.Get("bgo.character_menu.sector").ToUpperInvariant() + "\n";
    text2.text = str2;
    this.rightLabel.text = BsgoLocalization.Get(faction != Faction.Colonial ? "bgo.common.cylon_u" : "bgo.common.colonial_u").ToUpperInvariant() + "\n";
    Text text3 = this.rightLabel;
    string str3 = text3.text + (object) level + "\n";
    text3.text = str3;
    this.rightLabel.text += BsgoLocalization.Get("bgo.sector" + (object) Game.Me.SectorId + ".Name").ToUpperInvariant();
    this.exitButton.GetComponentInChildren<Text>().text = BsgoLocalization.Get("bgo.character_menu.exit").ToUpperInvariant();
  }

  private void OnEnable()
  {
    this.exitButton.transform.parent.gameObject.SetActive(true);
  }

  private void OnDisable()
  {
    this.exitButton.transform.parent.gameObject.SetActive(false);
  }

  public void OnAvatarLoaded()
  {
    this.avatarChangeButton.interactable = true;
  }
}
