﻿// Decompiled with JetBrains decompiler
// Type: GUIShopConfirmWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GUIShopConfirmWindow : GUIPanel
{
  private bool isCostRendered = true;
  protected GUIShopConfirmWindow.ConfirmAction action = GUIShopConfirmWindow.ConfirmAction.Move;
  protected int purchaseUnit = 1;
  private const float BUTTON_SPACE = 4f;
  private const float LABEL_SPACE = 2f;
  private GUILabelNew titleLabel;
  private GUILabelNew itemNameLabel;
  private GUILabelNew titleStatsLabel;
  private GUILabelNew statsLabel;
  private GUILabelNew titleAbilLabel;
  private GUILabelNew abilLabel;
  private GUIButtonNew button1;
  private GUIButtonNew button2;
  private GUIButtonNew button3;
  private GUIImageNew sellPriceImage;
  private GUILabelNew sellPriceLabel;
  private GUIButtonNew arrowLeftButton;
  private GUIButtonNew arrowRightButton;
  private GUIButtonNew dbArrowLeftButton;
  private GUIButtonNew dbArrowRightButton;
  private GUITextBox countTextBox;
  private GUIImageNew editBackgroundImage;
  private GUIImageNew costImage1;
  private GUILabelNew costLabel1;
  private GUIImageNew costImage2;
  private GUILabelNew costLabel2;
  private ShipItem item;
  private ShipSlot toSlot;

  public ShipItem Item
  {
    get
    {
      return this.item;
    }
    set
    {
      this.item = value;
    }
  }

  public ShipSlot ToSlot
  {
    get
    {
      return this.toSlot;
    }
    set
    {
      this.toSlot = value;
    }
  }

  public int Count
  {
    get
    {
      if (!(this.item is ItemCountable))
        return 1;
      int result = 0;
      if (int.TryParse(this.countTextBox.Text, out result))
      {
        result = Mathf.Clamp(result, 0, this.MaxCount);
        this.countTextBox.Text = result.ToString();
        return result;
      }
      this.countTextBox.Text = "0";
      return 0;
    }
    set
    {
      this.countTextBox.Text = value.ToString();
    }
  }

  protected int MaxCount
  {
    get
    {
      if (this.action == GUIShopConfirmWindow.ConfirmAction.Buy)
        return 99999999;
      if (this.item is ItemCountable)
        return (int) (this.item as ItemCountable).Count;
      return 1;
    }
  }

  public GUIShopConfirmWindow(SmartRect parent)
    : base(parent)
  {
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/EquipBuyPanel/gui_confirm_layout");
    GUIImageNew guiImageNew = (jwindowDescription["background_image"] as JImage).CreateGUIImageNew(this.root);
    this.root.Width = guiImageNew.Rect.width;
    this.root.Height = guiImageNew.Rect.height;
    this.AddPanel((GUIPanel) guiImageNew);
    this.button1 = (jwindowDescription["ok_button"] as JButton).CreateGUIButtonNew(this.root);
    this.button1.PositionY += 15f;
    this.AddPanel((GUIPanel) this.button1);
    this.sellPriceImage = (jwindowDescription["tylium_image"] as JImage).CreateGUIImageNew(this.root);
    this.button1.AddPanel((GUIPanel) this.sellPriceImage);
    this.sellPriceLabel = (jwindowDescription["tylium_label"] as JLabel).CreateGUILabelNew(this.root);
    this.button1.AddPanel((GUIPanel) this.sellPriceLabel);
    this.sellPriceImage.PositionY = 8f;
    this.sellPriceLabel.PositionY = 8f;
    this.button2 = (jwindowDescription["ok_button"] as JButton).CreateGUIButtonNew(this.root);
    this.button2.PositionY += 15f;
    this.AddPanel((GUIPanel) this.button2);
    this.button3 = (jwindowDescription["ok_button"] as JButton).CreateGUIButtonNew(this.root);
    this.button3.PositionY += 15f;
    this.AddPanel((GUIPanel) this.button3);
    this.titleLabel = (jwindowDescription["title_label"] as JLabel).CreateGUILabelNew(this.root);
    this.titleLabel.Text = "???";
    this.AddPanel((GUIPanel) this.titleLabel);
    this.itemNameLabel = (jwindowDescription["item_name_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.itemNameLabel);
    this.titleStatsLabel = (jwindowDescription["title_stats_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.titleStatsLabel);
    this.statsLabel = (jwindowDescription["stats_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.statsLabel);
    this.titleAbilLabel = (jwindowDescription["title_abil_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.titleAbilLabel);
    this.abilLabel = (jwindowDescription["abil_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.abilLabel);
    this.arrowLeftButton = (jwindowDescription["arrow_left_button"] as JButton).CreateGUIButtonNew(this.root);
    this.AddPanel((GUIPanel) this.arrowLeftButton);
    this.arrowRightButton = (jwindowDescription["arrow_right_button"] as JButton).CreateGUIButtonNew(this.root);
    this.AddPanel((GUIPanel) this.arrowRightButton);
    this.dbArrowLeftButton = (jwindowDescription["dbarrow_left_button"] as JButton).CreateGUIButtonNew(this.root);
    this.AddPanel((GUIPanel) this.dbArrowLeftButton);
    this.dbArrowRightButton = (jwindowDescription["dbarrow_right_button"] as JButton).CreateGUIButtonNew(this.root);
    this.AddPanel((GUIPanel) this.dbArrowRightButton);
    this.editBackgroundImage = (jwindowDescription["edit_background_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.editBackgroundImage);
    this.countTextBox = new GUITextBox(this.root, string.Empty, Gui.Options.FontBGM_BT, Color.white, (GUIStyle) null);
    this.countTextBox.FontSize = 12;
    this.countTextBox.DigitsOnly = true;
    this.countTextBox.SmartRect.Width = this.editBackgroundImage.Rect.width - 10f;
    this.countTextBox.SmartRect.Height = this.editBackgroundImage.Rect.height;
    this.countTextBox.Position = (jwindowDescription["edit_pivot"] as JPivot).position;
    this.AddPanel((GUIPanel) this.countTextBox);
    this.costImage1 = (jwindowDescription["cubits_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.costImage1);
    this.costLabel1 = (jwindowDescription["cubits_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.costLabel1);
    this.costImage2 = (jwindowDescription["tylium_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.costImage2);
    this.costLabel2 = (jwindowDescription["tylium_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.costLabel2);
  }

  public override void Update()
  {
    string str1 = string.Empty;
    string str2 = string.Empty;
    ShipSystem shipSystem = (ShipSystem) null;
    if (this.toSlot != null)
      shipSystem = this.toSlot.System;
    string textAbilities;
    string textStats;
    if (this.action == GUIShopConfirmWindow.ConfirmAction.Sell || this.action == GUIShopConfirmWindow.ConfirmAction.Uninstall)
    {
      textAbilities = this.GetTextAbilities((ShipItem) shipSystem, this.item);
      textStats = this.GetTextStats((ShipItem) shipSystem, this.item);
    }
    else
    {
      textStats = this.GetTextStats(this.item, (ShipItem) shipSystem);
      textAbilities = this.GetTextAbilities(this.item, (ShipItem) shipSystem);
    }
    this.statsLabel.Text = textStats;
    this.statsLabel.SmartRect.Height = (float) TextUtility.CalcTextSize(this.statsLabel.Text, this.statsLabel.Font, this.statsLabel.SmartRect.Width).height;
    this.abilLabel.Text = textAbilities;
    this.abilLabel.SmartRect.Height = (float) TextUtility.CalcTextSize(this.abilLabel.Text, this.abilLabel.Font, this.abilLabel.SmartRect.Width).height;
    this.statsLabel.PlaceBelowOf((GUIPanel) this.titleStatsLabel, 2f);
    this.titleAbilLabel.PlaceBelowOf((GUIPanel) this.statsLabel, 2f);
    this.abilLabel.PlaceBelowOf((GUIPanel) this.titleAbilLabel, 2f);
    if (this.isCostRendered && this.item != null && (bool) this.item.IsLoaded)
    {
      this.costLabel1.IsRendered = false;
      this.costImage1.IsRendered = false;
      this.costLabel2.IsRendered = false;
      this.costImage2.IsRendered = false;
      int num = 0;
      using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = (this.action != GUIShopConfirmWindow.ConfirmAction.Sell ? this.item.ShopItemCard.BuyPrice.items : this.item.ShopItemCard.SellPrice.items).GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
          float result = (float) ((double) current.Value * (double) this.Count * (!(this.item is ShipSystem) ? 1.0 : (double) (this.item as ShipSystem).Quality));
          if (this.action == GUIShopConfirmWindow.ConfirmAction.Buy)
            Shop.ApplyDiscount(out result, this.item.CardGUID, result);
          if (num == 0)
          {
            if ((double) Math.Abs(result) >= 1.0 / 500.0)
            {
              this.costImage1.Texture = ResourceLoader.Load<Texture2D>(current.Key.GUICard.GUIIcon);
              this.sellPriceImage.Texture = ResourceLoader.Load<Texture2D>(current.Key.GUICard.GUIIcon);
              this.costImage1.TakeTextureSize();
              this.costLabel1.Text = result.ToString("#0.0");
              this.costLabel1.IsRendered = true;
              this.costImage1.IsRendered = true;
              this.sellPriceLabel.Text = result.ToString("#0.0");
            }
            else
              continue;
          }
          else
          {
            this.costImage2.Texture = ResourceLoader.Load<Texture2D>(current.Key.GUICard.GUIIcon);
            this.costImage2.TakeTextureSize();
            this.costLabel2.Text = result.ToString("#0.0");
            this.costLabel2.IsRendered = true;
            this.costImage2.IsRendered = true;
          }
          ++num;
        }
      }
      this.costLabel1.MakeCenteredRect();
      this.costLabel2.MakeCenteredRect();
      this.costImage1.Position = new float2((float) (-(double) ((float) ((double) this.costImage1.Rect.width + 3.0 + (double) this.costLabel1.Rect.width + 5.0 + (double) this.costImage2.Rect.width + 3.0) + this.costLabel2.Rect.width) / 2.0 + (double) this.costImage1.Rect.width / 2.0), this.costImage1.Position.y);
      if (!this.costImage2.IsRendered)
        this.costImage1.Position = new float2(this.costImage1.Position.x + 30f, this.costImage1.Position.y);
      this.costLabel1.PlaceRightOf((GUIPanel) this.costImage1, 3f);
      this.costImage2.PlaceRightOf((GUIPanel) this.costLabel1, 5f);
      this.costLabel2.PlaceRightOf((GUIPanel) this.costImage2, 2f);
      this.sellPriceImage.MakeCenteredRect();
      this.sellPriceLabel.MakeCenteredRect();
      this.sellPriceImage.PositionX = (float) (-(double) (this.sellPriceImage.Rect.width + 3f + this.sellPriceLabel.Rect.width) / 2.0 + (double) this.sellPriceImage.Rect.width / 2.0);
      this.sellPriceLabel.PlaceRightOf((GUIPanel) this.sellPriceImage, 3f);
    }
    this.Count = this.ClampPurchase(this.Count);
    this.itemNameLabel.Text = this.item == null || !(bool) this.item.IsLoaded ? "???" : this.item.ItemGUICard.Name;
    this.itemNameLabel.SmartRect.Height = (float) TextUtility.CalcTextSize(this.itemNameLabel.Text, this.itemNameLabel.Font, this.itemNameLabel.SmartRect.Width).height;
    this.RecalculateAbsCoords();
    base.Update();
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    this.countTextBox.IsFocused = this.countTextBox.Contains(mousePosition);
    Game.InputDispatcher.Focused = !this.countTextBox.IsFocused ? (InputListener) this : (InputListener) this.countTextBox;
    base.OnMouseDown(mousePosition, mouseKey);
    return this.IsRendered;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return base.OnMouseUp(mousePosition, mouseKey);
  }

  public void ShowBuy(ShipItem item, AnonymousDelegate buyHandler)
  {
    this.Show("%$bgo.shop.buying%", item, (ShipSlot) null, GUIShopConfirmWindow.ConfirmAction.Buy);
    this.Show2Buttons();
    this.TweakOKButton(false);
    this.button1.TextLabel.Text = "%$bgo.shop.buy%";
    this.button1.TextLabel.MakeCenteredRect();
    this.button1.Handler = buyHandler;
    this.button2.TextLabel.Text = "%$bgo.common.cancel%";
    this.button2.TextLabel.MakeCenteredRect();
    this.button2.Handler = new AnonymousDelegate(this.Hide);
  }

  public void ShowSell(ShipItem item, AnonymousDelegate sellHandler)
  {
    this.Show("%$bgo.shop.selling%", item, (ShipSlot) null, GUIShopConfirmWindow.ConfirmAction.Sell);
    this.Show2Buttons();
    this.TweakOKButton(!item.ShopItemCard.BuyPrice.UsesMerits);
    this.button1.TextLabel.Text = "%$bgo.shop.salvage_c%";
    this.button1.TextLabel.MakeCenteredRect();
    this.button1.Handler = sellHandler;
    this.button2.TextLabel.Text = "%$bgo.common.cancel%";
    this.button2.TextLabel.MakeCenteredRect();
    this.button2.Handler = new AnonymousDelegate(this.Hide);
  }

  public void ShowBuyHoldInstall(ShipItem item, AnonymousDelegate buyInstallHandler, AnonymousDelegate buyHandler)
  {
    this.Show("%$bgo.shop.buying%", item, (ShipSlot) null, GUIShopConfirmWindow.ConfirmAction.Buy);
    this.Show3Buttons();
    this.TweakOKButton(false);
    this.button1.TextLabel.Text = "%$bgo.shop.buy_and_install%";
    this.button1.TextLabel.MakeCenteredRect();
    this.button1.Handler = buyInstallHandler;
    this.button2.TextLabel.Text = "%$bgo.shop.buy%";
    this.button2.TextLabel.MakeCenteredRect();
    this.button2.Handler = buyHandler;
    this.button3.TextLabel.Text = "%$bgo.common.cancel%";
    this.button3.TextLabel.MakeCenteredRect();
    this.button3.Handler = new AnonymousDelegate(this.Hide);
  }

  public void ShowInstall(ShipItem item, ShipSlot slot, AnonymousDelegate installHandler)
  {
    this.Show("%$bgo.shop.effect_of_instalation%", item, slot, GUIShopConfirmWindow.ConfirmAction.Move);
    this.Show2Buttons();
    this.TweakOKButton(false);
    this.button1.TextLabel.Text = "%$bgo.shop.install%";
    this.button1.TextLabel.MakeCenteredRect();
    this.button1.Handler = installHandler;
    this.button2.TextLabel.Text = "%$bgo.common.cancel%";
    this.button2.TextLabel.MakeCenteredRect();
    this.button2.Handler = new AnonymousDelegate(this.Hide);
  }

  public void ShowUninstall(ShipItem item, AnonymousDelegate installHandler)
  {
    this.Show("%$bgo.shop.effect_of_uninstalation%", item, (ShipSlot) null, GUIShopConfirmWindow.ConfirmAction.Uninstall);
    this.Show2Buttons();
    this.TweakOKButton(false);
    this.button1.TextLabel.Text = "%$bgo.shop.uninstall%";
    this.button1.TextLabel.MakeCenteredRect();
    this.button1.Handler = installHandler;
    this.button2.TextLabel.Text = "%$bgo.common.cancel%";
    this.button2.TextLabel.MakeCenteredRect();
    this.button2.Handler = new AnonymousDelegate(this.Hide);
  }

  public void ShowBuyInstall(ShipItem item, ShipSlot slot, AnonymousDelegate buyInstallHandler, AnonymousDelegate buyHandler)
  {
    this.Show("%$bgo.shop.buy_item%", item, slot, GUIShopConfirmWindow.ConfirmAction.Buy);
    this.Show3Buttons();
    this.TweakOKButton(false);
    this.button1.TextLabel.Text = "%$bgo.shop.buy_and_install%";
    this.button1.TextLabel.MakeCenteredRect();
    this.button1.Handler = buyInstallHandler;
    this.button2.TextLabel.Text = "%$bgo.shop.buy%";
    this.button2.TextLabel.MakeCenteredRect();
    this.button2.Handler = buyHandler;
    this.button3.TextLabel.Text = "%$bgo.common.cancel%";
    this.button3.TextLabel.MakeCenteredRect();
    this.button3.Handler = new AnonymousDelegate(this.Hide);
  }

  public void ShowSellUninstall(ShipItem item, AnonymousDelegate sellHandler)
  {
    this.Show("%$bgo.shop.sell_item%", item, (ShipSlot) null, GUIShopConfirmWindow.ConfirmAction.Sell);
    this.Show2Buttons();
    this.TweakOKButton(true);
    this.button1.TextLabel.Text = "%$bgo.shop.salvage_and_uninstall%";
    this.button1.TextLabel.MakeCenteredRect();
    this.button1.Handler = sellHandler;
    this.button2.TextLabel.Text = "%$bgo.common.cancel%";
    this.button2.TextLabel.MakeCenteredRect();
    this.button2.Handler = new AnonymousDelegate(this.Hide);
  }

  public void ShowSlotToSlot(ShipItem item, ShipSlot slot, AnonymousDelegate okHandler)
  {
    this.Show("%$bgo.shop.move_system%", item, slot, GUIShopConfirmWindow.ConfirmAction.Move);
    this.Show2Buttons();
    this.TweakOKButton(false);
    this.button1.TextLabel.Text = "%$bgo.common.ok%";
    this.button1.TextLabel.MakeCenteredRect();
    this.button1.Handler = okHandler;
    this.button2.TextLabel.Text = "%$bgo.common.cancel%";
    this.button2.TextLabel.MakeCenteredRect();
    this.button2.Handler = new AnonymousDelegate(this.Hide);
  }

  public void ShowMoveToLocker(ShipItem item, AnonymousDelegate moveHandler)
  {
    this.Show("%$bgo.shop.move_to_locker%", item, (ShipSlot) null, GUIShopConfirmWindow.ConfirmAction.Move);
    this.Show2Buttons();
    this.TweakOKButton(false);
    this.button1.TextLabel.Text = "%$bgo.shop.move_and_locker%";
    this.button1.TextLabel.MakeCenteredRect();
    this.button1.Handler = moveHandler;
    this.button2.TextLabel.Text = "%$bgo.common.cancel%";
    this.button2.TextLabel.MakeCenteredRect();
    this.button2.Handler = new AnonymousDelegate(this.Hide);
  }

  public void ShowMoveToHold(ShipItem item, AnonymousDelegate moveHandler)
  {
    this.Show("%$bgo.shop.move_to_hold%", item, (ShipSlot) null, GUIShopConfirmWindow.ConfirmAction.Move);
    this.Show2Buttons();
    this.TweakOKButton(false);
    this.button1.TextLabel.Text = "%$bgo.shop.move_and_hold%";
    this.button1.TextLabel.MakeCenteredRect();
    this.button1.Handler = moveHandler;
    this.button2.TextLabel.Text = "%$bgo.common.cancel%";
    this.button2.TextLabel.MakeCenteredRect();
    this.button2.Handler = new AnonymousDelegate(this.Hide);
  }

  private void TweakOKButton(bool isSellAction)
  {
    this.button1.TextLabel.PositionY = !isSellAction ? 0.0f : -8f;
    this.sellPriceImage.IsRendered = isSellAction;
    this.sellPriceLabel.IsRendered = isSellAction;
  }

  public override void Hide()
  {
    Game.InputDispatcher.Focused = (InputListener) null;
    this.IsUpdated = false;
    this.IsRendered = false;
    this.item = (ShipItem) null;
    this.toSlot = (ShipSlot) null;
    this.HideArrows();
    this.HideCost();
    this.HideButtons();
  }

  protected void Show(string title, ShipItem item, ShipSlot slot, GUIShopConfirmWindow.ConfirmAction action)
  {
    Game.InputDispatcher.Focused = (InputListener) this;
    this.IsUpdated = true;
    this.IsRendered = true;
    this.titleLabel.Text = title;
    this.action = action;
    if (action == GUIShopConfirmWindow.ConfirmAction.Sell || action == GUIShopConfirmWindow.ConfirmAction.Buy)
    {
      this.purchaseUnit = this.GetPurchaseUnit(item, action);
      this.Count = this.purchaseUnit;
      this.ShowCost();
    }
    else
    {
      this.purchaseUnit = this.GetPurchaseUnit(item, action);
      this.countTextBox.Text = this.purchaseUnit.ToString();
      this.HideCost();
    }
    this.item = item;
    this.toSlot = slot;
    if (item is ItemCountable)
      this.ShowArrows();
    else
      this.HideArrows();
  }

  protected void ShowArrows()
  {
    this.countTextBox.IsRendered = true;
    this.editBackgroundImage.IsRendered = true;
    this.arrowLeftButton.IsRendered = true;
    this.arrowLeftButton.Handler = new AnonymousDelegate(this.OnArrowLeft);
    this.arrowRightButton.IsRendered = true;
    this.arrowRightButton.Handler = new AnonymousDelegate(this.OnArrowRight);
    this.dbArrowLeftButton.IsRendered = true;
    this.dbArrowLeftButton.Handler = new AnonymousDelegate(this.OnDbArrowLeft);
    this.dbArrowRightButton.IsRendered = true;
    this.dbArrowRightButton.Handler = new AnonymousDelegate(this.OnDbArrowRight);
  }

  protected void HideArrows()
  {
    this.countTextBox.IsRendered = false;
    this.editBackgroundImage.IsRendered = false;
    this.arrowLeftButton.IsRendered = false;
    this.arrowLeftButton.Handler = (AnonymousDelegate) null;
    this.arrowRightButton.IsRendered = false;
    this.arrowRightButton.Handler = (AnonymousDelegate) null;
    this.dbArrowLeftButton.IsRendered = false;
    this.dbArrowLeftButton.Handler = (AnonymousDelegate) null;
    this.dbArrowRightButton.IsRendered = false;
    this.dbArrowRightButton.Handler = (AnonymousDelegate) null;
  }

  protected void ShowCost()
  {
    this.isCostRendered = true;
    this.costImage1.IsRendered = true;
    this.costLabel1.IsRendered = true;
    this.costImage2.IsRendered = true;
    this.costLabel2.IsRendered = true;
  }

  protected void HideCost()
  {
    this.isCostRendered = false;
    this.costImage1.IsRendered = false;
    this.costLabel1.IsRendered = false;
    this.costImage2.IsRendered = false;
    this.costLabel2.IsRendered = false;
  }

  protected void Show2Buttons()
  {
    this.button1.IsRendered = true;
    this.button1.Position = new float2((float) (-(double) this.button1.SmartRect.Width / 2.0 - 2.0), this.button1.Position.y);
    this.button2.IsRendered = true;
    this.button2.Position = new float2((float) ((double) this.button2.SmartRect.Width / 2.0 + 2.0), this.button2.Position.y);
    this.button3.IsRendered = false;
    this.RecalculateAbsCoords();
  }

  protected void Show3Buttons()
  {
    float width = this.button1.SmartRect.Width;
    this.button1.IsRendered = true;
    this.button1.Position = new float2((float) (-(double) width / 2.0 - 4.0 - (double) width / 2.0), this.button1.Position.y);
    this.button2.IsRendered = true;
    this.button2.Position = new float2(0.0f, this.button2.Position.y);
    this.button3.IsRendered = true;
    this.button3.Position = new float2((float) ((double) width / 2.0 + 4.0 + (double) width / 2.0), this.button3.Position.y);
    this.RecalculateAbsCoords();
  }

  protected void HideButtons()
  {
    this.button1.IsRendered = false;
    this.button1.Handler = (AnonymousDelegate) null;
    this.button2.IsRendered = false;
    this.button2.Handler = (AnonymousDelegate) null;
    this.button3.IsRendered = false;
    this.button3.Handler = (AnonymousDelegate) null;
  }

  protected void OnArrowLeft()
  {
    this.Count = this.ClampPurchase(this.Count - this.purchaseUnit);
  }

  protected void OnArrowRight()
  {
    this.Count = this.ClampPurchase(this.Count + this.purchaseUnit);
  }

  protected void OnDbArrowLeft()
  {
    int num = 100;
    if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
      num = 500;
    this.Count = this.ClampPurchase(this.Count - num * this.purchaseUnit);
  }

  protected void OnDbArrowRight()
  {
    int num = 100;
    if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
      num = 500;
    this.Count = this.ClampPurchase(this.Count + num * this.purchaseUnit);
  }

  protected int ClampPurchase(int desiredTotal)
  {
    if (this.item == null || !(this.item is ItemCountable))
      return desiredTotal;
    if (this.action == GUIShopConfirmWindow.ConfirmAction.Buy && !this.item.ShopItemCard.BuyPrice.IsEnoughInHangar(this.item.ShopItemCard, desiredTotal))
      return Mathf.Clamp(this.item.ShopItemCard.BuyPrice.GetMaxCountWeCanBuy(this.item.ShopItemCard), 0, this.MaxCount);
    return Mathf.Clamp(desiredTotal, this.purchaseUnit, this.MaxCount);
  }

  protected int GetPurchaseUnit(ShipItem item, GUIShopConfirmWindow.ConfirmAction action)
  {
    float num = 1f;
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = (action == GUIShopConfirmWindow.ConfirmAction.Sell || action == GUIShopConfirmWindow.ConfirmAction.Move ? item.ShopItemCard.SellPrice.items : item.ShopItemCard.BuyPrice.items).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        if ((double) current.Value > 0.0 && (double) current.Value < (double) num)
          num = current.Value;
      }
    }
    if ((double) num < 1.0)
      return (int) Math.Round(1.0 / (double) num);
    return 1;
  }

  protected string GetTextStats(ShipItem addItem, ShipItem remItem)
  {
    Dictionary<string, float> dictionary1 = new Dictionary<string, float>();
    Dictionary<string, float> dictionary2 = new Dictionary<string, float>();
    ShipSystem shipSystem1 = addItem as ShipSystem;
    if (shipSystem1 != null && (bool) shipSystem1.IsLoaded && shipSystem1.Card.StaticBuffs != null)
    {
      dictionary1["%$bgo.stats.durability%"] = shipSystem1.Card.Durability;
      using (List<ObjectStat>.Enumerator enumerator = shipSystem1.Card.StaticBuffs.PropList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ObjectStat current = enumerator.Current;
          float obfuscatedFloat = shipSystem1.Card.StaticBuffs.GetObfuscatedFloat(current);
          if ((double) obfuscatedFloat != 0.0)
            dictionary1.Add("%$bgo.statsdescription." + SystemsStatsGenerator.GetLocalizationTag(current) + ".name%", obfuscatedFloat);
        }
      }
      if (shipSystem1.Card.SlotType == ShipSlotType.weapon && shipSystem1.Card.AbilityCards != null)
      {
        foreach (ShipAbilityCard abilityCard in shipSystem1.Card.AbilityCards)
        {
          if (abilityCard != null && (bool) abilityCard.IsLoaded)
          {
            dictionary1["%$bgo.stats.dps%"] = abilityCard.ItemBuffAdd.DPS;
            dictionary1["%$bgo.stats.range%"] = abilityCard.ItemBuffAdd.MaxRange;
            if (abilityCard.ActionType == AbilityActionType.FireCannon)
            {
              dictionary1["%$bgo.stats.accuracy%"] = abilityCard.ItemBuffAdd.Accuracy;
              dictionary1["%$bgo.stats.optimal_range%"] = abilityCard.ItemBuffAdd.OptimalRange;
            }
          }
        }
      }
    }
    if (shipSystem1 != null && (bool) shipSystem1.IsLoaded && shipSystem1.Card.MultiplyBuffs != null)
    {
      using (List<ObjectStat>.Enumerator enumerator = shipSystem1.Card.MultiplyBuffs.PropList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ObjectStat current = enumerator.Current;
          float obfuscatedFloat = shipSystem1.Card.MultiplyBuffs.GetObfuscatedFloat(current);
          if ((double) obfuscatedFloat != 0.0)
          {
            string key = BsgoLocalization.Get("%$bgo.statsdescription." + SystemsStatsGenerator.GetLocalizationTag(current) + ".name%");
            dictionary2.Add(key, obfuscatedFloat);
          }
        }
      }
    }
    ShipSystem shipSystem2 = remItem as ShipSystem;
    if (shipSystem2 != null && (bool) shipSystem2.IsLoaded && shipSystem2.Card.MultiplyBuffs != null)
    {
      using (List<ObjectStat>.Enumerator enumerator = shipSystem2.Card.MultiplyBuffs.PropList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ObjectStat current = enumerator.Current;
          string key = BsgoLocalization.Get("%$bgo.statsdescription." + SystemsStatsGenerator.GetLocalizationTag(current) + ".name%");
          float obfuscatedFloat = shipSystem2.Card.MultiplyBuffs.GetObfuscatedFloat(current);
          if ((double) obfuscatedFloat != 0.0)
          {
            if (dictionary2.ContainsKey(key))
            {
              Dictionary<string, float> dictionary3;
              string index;
              (dictionary3 = dictionary2)[index = key] = dictionary3[index] / obfuscatedFloat;
            }
            else
              dictionary2.Add(key, 1f / obfuscatedFloat);
          }
        }
      }
    }
    if (shipSystem2 != null && (bool) shipSystem2.IsLoaded && shipSystem2.Card.StaticBuffs != null)
    {
      using (List<ObjectStat>.Enumerator enumerator = shipSystem2.Card.StaticBuffs.PropList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ObjectStat current = enumerator.Current;
          string key = BsgoLocalization.Get("%$bgo.statsdescription." + SystemsStatsGenerator.GetLocalizationTag(current) + ".name%");
          float obfuscatedFloat = shipSystem2.Card.StaticBuffs.GetObfuscatedFloat(current);
          if ((double) obfuscatedFloat != 0.0)
          {
            if (dictionary1.ContainsKey(key))
            {
              Dictionary<string, float> dictionary3;
              string index;
              (dictionary3 = dictionary1)[index = key] = dictionary3[index] - obfuscatedFloat;
            }
            else
              dictionary1.Add(key, -obfuscatedFloat);
          }
        }
      }
      if (shipSystem2.Card.SlotType == ShipSlotType.weapon && shipSystem2.Card.AbilityCards != null)
      {
        foreach (ShipAbilityCard abilityCard in shipSystem2.Card.AbilityCards)
        {
          if (abilityCard != null && (bool) abilityCard.IsLoaded)
          {
            if (dictionary1.ContainsKey("%$bgo.stats.dps%"))
            {
              Dictionary<string, float> dictionary3;
              string index;
              (dictionary3 = dictionary1)[index = "%$bgo.stats.dps%"] = dictionary3[index] - abilityCard.ItemBuffAdd.DPS;
            }
            else
              dictionary1["%$bgo.stats.dps%"] = -abilityCard.ItemBuffAdd.DPS;
            if (dictionary1.ContainsKey("%$bgo.stats.range%"))
            {
              Dictionary<string, float> dictionary3;
              string index;
              (dictionary3 = dictionary1)[index = "%$bgo.stats.range%"] = dictionary3[index] - abilityCard.ItemBuffAdd.MaxRange;
            }
            else
              dictionary1["%$bgo.stats.range%"] = -abilityCard.ItemBuffAdd.MaxRange;
            if (abilityCard.ActionType == AbilityActionType.FireCannon)
            {
              if (dictionary1.ContainsKey("%$bgo.stats.accuracy%"))
              {
                Dictionary<string, float> dictionary3;
                string index;
                (dictionary3 = dictionary1)[index = "%$bgo.stats.accuracy%"] = dictionary3[index] - abilityCard.ItemBuffAdd.Accuracy;
              }
              else
                dictionary1["%$bgo.stats.accuracy%"] = -abilityCard.ItemBuffAdd.Accuracy;
              if (dictionary1.ContainsKey("%$bgo.stats.optimal_range%"))
              {
                Dictionary<string, float> dictionary3;
                string index;
                (dictionary3 = dictionary1)[index = "%$bgo.stats.optimal_range%"] = dictionary3[index] - abilityCard.ItemBuffAdd.OptimalRange;
              }
              else
                dictionary1["%$bgo.stats.optimal_range%"] = -abilityCard.ItemBuffAdd.OptimalRange;
            }
          }
        }
      }
    }
    string str1 = string.Empty;
    using (Dictionary<string, float>.Enumerator enumerator = dictionary2.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, float> current = enumerator.Current;
        str1 = str1 + "x " + current.Value.ToString("#0.00") + " " + current.Key + "\n";
      }
    }
    using (Dictionary<string, float>.Enumerator enumerator = dictionary1.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, float> current = enumerator.Current;
        string str2 = "- ";
        if ((double) current.Value >= 0.0)
          str2 = "+ ";
        str1 = str1 + str2 + Mathf.Abs(current.Value).ToString("#0.00") + " " + current.Key + "\n";
      }
    }
    return str1.ToUpper().TrimEnd();
  }

  protected string GetTextAbilities(ShipItem addItem, ShipItem remItem)
  {
    string str = string.Empty;
    ShipSystem shipSystem1 = addItem as ShipSystem;
    if (shipSystem1 != null && (bool) shipSystem1.IsLoaded && shipSystem1.Card.AbilityCards != null)
    {
      foreach (ShipAbilityCard abilityCard in shipSystem1.Card.AbilityCards)
      {
        if (abilityCard != null && (bool) abilityCard.IsLoaded)
          str = str + "+ " + abilityCard.GUICard.Name + " " + (object) abilityCard.Level + "\n";
        else
          str += "???\n";
      }
    }
    ShipSystem shipSystem2 = remItem as ShipSystem;
    if (shipSystem2 != null && (bool) shipSystem2.IsLoaded && shipSystem2.Card.AbilityCards != null)
    {
      foreach (ShipAbilityCard abilityCard in shipSystem2.Card.AbilityCards)
      {
        if (abilityCard != null && (bool) abilityCard.IsLoaded)
          str = str + "- " + abilityCard.GUICard.Name + " " + (object) abilityCard.Level + "\n";
        else
          str += "???\n";
      }
    }
    if (addItem is StarterKit)
      str = "%$bgo.shop.install_will_return_existing_system_to_locker%";
    return str.ToUpper().TrimEnd();
  }

  protected enum ConfirmAction
  {
    Sell,
    Buy,
    Move,
    Uninstall,
  }
}
