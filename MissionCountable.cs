﻿// Decompiled with JetBrains decompiler
// Type: MissionCountable
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class MissionCountable : ILoadable, IProtocolRead
{
  public uint GUID;
  public int Count;
  public int NeedCount;
  public CounterCard Counter;

  public Flag IsLoaded
  {
    get
    {
      return this.Counter.IsLoaded;
    }
  }

  public void Read(BgoProtocolReader r)
  {
    this.GUID = r.ReadGUID();
    this.Count = r.ReadInt32();
    this.NeedCount = r.ReadInt32();
    if (this.Counter != null)
      return;
    this.Counter = (CounterCard) Game.Catalogue.FetchCard(this.GUID, CardView.Counter);
  }
}
