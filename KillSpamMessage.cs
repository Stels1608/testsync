﻿// Decompiled with JetBrains decompiler
// Type: KillSpamMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class KillSpamMessage : GuiPanel
{
  public const float FADEOUT_TIME = 40f;
  protected GuiImage icon;

  public float TimeToLive { get; set; }

  public bool Expired
  {
    get
    {
      return (double) this.TimeToLive <= 0.0;
    }
  }

  public KillSpamMessage(string text)
  {
    this.TimeToLive = 40f;
    this.AddChild((GuiElementBase) new GuiLabel(text, Color.white, Gui.Options.FontEurostileT)
    {
      FontSize = 14,
      Style = {
        richText = true
      }
    });
  }

  public KillSpamMessage(string text, string iconCode, Texture2D iconImage)
  {
    this.TimeToLive = 40f;
    string[] strArray = text.Split(new string[1]{ iconCode }, StringSplitOptions.None);
    GuiLabel guiLabel1 = new GuiLabel(strArray[0], Color.white, Gui.Options.FontEurostileT);
    guiLabel1.FontSize = 14;
    guiLabel1.Style.richText = true;
    this.icon = new GuiImage(iconImage);
    this.icon.ElementPadding = new GuiElementPadding(new Rect());
    GuiImage guiImage = this.icon;
    float num1 = 16f;
    this.icon.SizeY = num1;
    double num2 = (double) num1;
    guiImage.SizeX = (float) num2;
    this.icon.PositionX = guiLabel1.BottomRightCornerWithPadding.x;
    GuiLabel guiLabel2 = new GuiLabel(strArray[1], Color.white, Gui.Options.FontEurostileT);
    guiLabel2.FontSize = 14;
    guiLabel2.Style.richText = true;
    guiLabel2.PositionX = this.icon.BottomRightCornerWithPadding.x;
    this.AddChild((GuiElementBase) guiLabel1);
    this.AddChild((GuiElementBase) this.icon);
    this.AddChild((GuiElementBase) guiLabel2);
  }

  public KillSpamMessage()
  {
  }

  public override void Draw()
  {
    if (Event.current.type == UnityEngine.EventType.Repaint)
      this.TimeToLive -= Time.deltaTime;
    if (this.Expired)
      return;
    Color color1 = GUI.color;
    Color color2 = color1;
    color2.a = this.TimeToLive / 40f;
    GUI.color = color2;
    if (this.icon != null)
      this.icon.OverlayColor = new Color?(color2);
    base.Draw();
    GUI.color = color1;
  }
}
