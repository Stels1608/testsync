﻿// Decompiled with JetBrains decompiler
// Type: DecalSpot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class DecalSpot : Spot
{
  private readonly Renderer renderer;
  private bool textured;

  public Texture Texture
  {
    get
    {
      return this.renderer.material.GetTexture("_MainTex");
    }
    set
    {
      if ((UnityEngine.Object) value != (UnityEngine.Object) null)
      {
        this.renderer.material.SetTexture("_MainTex", value);
        this.textured = true;
        this.Show();
      }
      else
      {
        this.textured = false;
        this.Hide();
      }
    }
  }

  public DecalSpot(SpotDesc spotDesc, GameObject gameObject)
    : base(spotDesc, gameObject)
  {
    this.renderer = gameObject.GetComponent<Renderer>();
    if (!((UnityEngine.Object) this.renderer == (UnityEngine.Object) null))
      return;
    this.renderer = gameObject.GetComponentInChildren<Renderer>();
  }

  public void Hide()
  {
    this.renderer.enabled = false;
  }

  public void Show()
  {
    this.renderer.enabled = this.textured;
  }
}
