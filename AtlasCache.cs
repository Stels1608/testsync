﻿// Decompiled with JetBrains decompiler
// Type: AtlasCache
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class AtlasCache
{
  private Dictionary<AtlasCache.Key, AtlasEntry> atlases = new Dictionary<AtlasCache.Key, AtlasEntry>();
  private float2 elementSize;
  private AtlasEntry unknownItem;
  private AtlasEntry emptyItem;

  public float2 ElementSize
  {
    get
    {
      return this.elementSize;
    }
  }

  public AtlasEntry UnknownItem
  {
    get
    {
      if (this.unknownItem == null)
      {
        Texture2D texture = (Texture2D) ResourceLoader.Load("GUI/Common/unknownItem");
        if ((UnityEngine.Object) texture == (UnityEngine.Object) null)
          throw new Exception("Unknown item texture not exists");
        this.unknownItem = new AtlasEntry(texture, new Rect(0.0f, 0.0f, 1f, 1f));
      }
      return this.unknownItem;
    }
  }

  public AtlasEntry EmptyItem
  {
    get
    {
      if (this.emptyItem == null)
      {
        Texture2D texture2D = (Texture2D) ResourceLoader.Load("GUI/Inventory/items_atlas");
        this.emptyItem = this.GetCachedEntryBy("GUI/Inventory/items_atlas", (int) ((double) texture2D.width / (double) this.ElementSize.x) * (int) ((double) texture2D.height / (double) this.ElementSize.y) - 1);
      }
      return this.emptyItem;
    }
  }

  public AtlasCache(float2 elementSize)
  {
    this.elementSize = elementSize;
  }

  public AtlasEntry GetCachedEntryBy(string texturePath, int frameIndex)
  {
    AtlasCache.Key key = new AtlasCache.Key(texturePath, frameIndex);
    if (this.atlases.ContainsKey(key))
      return this.atlases[key];
    Texture2D texture = (Texture2D) ResourceLoader.Load(texturePath);
    if ((UnityEngine.Object) texture == (UnityEngine.Object) null || !this.IsIndexValid(texture, this.ElementSize, (ushort) key.frameIndex))
      return this.UnknownItem;
    this.atlases.Add(key, new AtlasEntry(texture, this.CalcFrameRectInTexture(texture, this.ElementSize, (ushort) key.frameIndex)));
    return this.atlases[key];
  }

  private bool IsIndexValid(Texture2D texture, float2 frameSize, ushort frameIndex)
  {
    uint num1 = (uint) ((double) texture.width / (double) frameSize.x);
    uint num2 = (uint) ((double) texture.height / (double) frameSize.y);
    return (uint) frameIndex <= (uint) ((int) num1 * (int) num2 - 1);
  }

  private Rect CalcFrameRectInTexture(Texture2D texture, float2 frameSize, ushort frameIndex)
  {
    uint num1 = (uint) ((double) texture.width / (double) frameSize.x);
    uint num2 = (uint) ((double) texture.height / (double) frameSize.y);
    if ((uint) frameIndex > (uint) ((int) num1 * (int) num2 - 1))
    {
      Log.Add("Invalid FrameIndex, value:", (int) frameIndex);
      frameIndex = (ushort) 0;
    }
    Vector2 vector2 = new Vector2();
    vector2.y = (float) ((uint) frameIndex / num1);
    vector2.x = (float) frameIndex - vector2.y * (float) num1;
    return new Rect(0.0f, 0.0f, 1f / (float) num1, 1f / (float) num2) { x = vector2.x / (float) num1, y = (float) (1.0 - ((double) vector2.y + 1.0) / (double) num2) };
  }

  private struct Key
  {
    public string atlasName;
    public int frameIndex;

    public Key(string atlasName, int frameIndex)
    {
      this.atlasName = atlasName;
      this.frameIndex = frameIndex;
    }
  }
}
