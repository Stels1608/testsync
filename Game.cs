﻿// Decompiled with JetBrains decompiler
// Type: Game
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Tooltips;
using System;
using UnityEngine;

public class Game : MonoBehaviour
{
  private static Flag isGameLevelStarted = new Flag();
  public static bool IsLocaLoaded = false;
  private string revision = string.Empty;
  private string language = "en";
  private string geoRegion = string.Empty;
  private LevelProfile levelProfile = (LevelProfile) new LoginLevelProfile();
  private static Game instance;
  private SectorTournament tournament;
  private ProtocolManager protocolManager;
  private ChatProtocol chatProtocol;
  private ChatDataStorage chat;
  private TimeSync timeSync;
  private Me me;
  private Galaxy galaxy;
  private GuiTicker ticker;
  private PaymentInterface payment;
  private SpecialOfferManager _specialOfferManager;
  private Funnel funnel;
  private PlayerRegistry playerRegistry;
  private GuildRegistry guildRegistry;
  private Catalogue catalogue;
  private BsgoLocalization localization;
  private DelayedActions delayedActions;
  private uint serverRevision;
  private bool quitting;
  private bool loadingLevel;
  private bool justJumped;

  public static Game Instance
  {
    get
    {
      return Game.instance;
    }
  }

  public static GraphicsQuality GraphicsQuality { get; set; }

  public static PaymentInterface Payment
  {
    get
    {
      return Game.instance.payment;
    }
  }

  public static SectorTournament Tournament
  {
    get
    {
      return Game.instance.tournament;
    }
  }

  public static Funnel Funnel
  {
    get
    {
      return Game.instance.funnel;
    }
  }

  public static GuiTicker Ticker
  {
    get
    {
      return Game.instance.ticker;
    }
  }

  public static EventShopCard EventShopCard { get; set; }

  public static BsgoLocalization Localization
  {
    get
    {
      return Game.instance.localization;
    }
  }

  public static DelayedActions DelayedActions
  {
    get
    {
      return Game.instance.delayedActions;
    }
  }

  public static GUIManager GUIManager
  {
    get
    {
      return GameLevel.Instance.GUIManager;
    }
  }

  public static InputManager InputManager
  {
    get
    {
      return GameLevel.Instance.InputManager;
    }
  }

  public static InputDispatcher InputDispatcher
  {
    get
    {
      return GameLevel.Instance.InputManager.InputDispatcher;
    }
  }

  public static GuiAdvancedTooltipManager TooltipManager
  {
    get
    {
      if ((UnityEngine.Object) GameLevel.Instance == (UnityEngine.Object) null)
        return (GuiAdvancedTooltipManager) null;
      return GameLevel.Instance.TooltipManager;
    }
  }

  public static PopupTutorialTipManager PopupTipManager
  {
    get
    {
      return PopupTutorialTipManager.Instance;
    }
  }

  public static ChatProtocol ChatProtocol
  {
    get
    {
      return Game.instance.chatProtocol;
    }
    set
    {
      Game.instance.chatProtocol = value;
    }
  }

  public static SpecialOfferManager SpecialOfferManager
  {
    get
    {
      return Game.instance._specialOfferManager;
    }
  }

  public static string Language
  {
    get
    {
      return Game.instance.language;
    }
    set
    {
      Game.instance.language = value;
      Game.Localization.InitLanguage(Game.instance.language);
    }
  }

  public static string ChatLanguage
  {
    get
    {
      string str = Game.instance.language;
      if (str == "sk")
        str = "cs";
      if (str == "en")
        return Game.Me.ChatLanguage;
      if (Array.IndexOf<string>(new string[21]{ "en", "de", "us", "ru", "fr", "it", "es", "pl", "tr", "nl", "hu", "cs", "da", "sv", "fi", "sk", "ro", "bg", "el", "pt", "br" }, str) >= 0)
        return str;
      return Game.Me.ChatLanguage;
    }
  }

  public static string Revision
  {
    get
    {
      return Game.instance.revision;
    }
  }

  public static string GeoRegion
  {
    get
    {
      return Game.instance.geoRegion;
    }
    set
    {
      Game.instance.geoRegion = value;
    }
  }

  public static uint ServerRevision
  {
    get
    {
      return Game.instance.serverRevision;
    }
    set
    {
      Game.instance.serverRevision = value;
    }
  }

  public static ProtocolManager ProtocolManager
  {
    get
    {
      if ((UnityEngine.Object) Game.instance == (UnityEngine.Object) null)
        return (ProtocolManager) null;
      return Game.instance.protocolManager;
    }
  }

  public static Console Console
  {
    get
    {
      return GameLevel.Instance.GUIManager.Find<Console>();
    }
  }

  public static Galaxy Galaxy
  {
    get
    {
      return Game.instance.galaxy;
    }
  }

  public static PlayerRegistry Players
  {
    get
    {
      return Game.instance.playerRegistry;
    }
  }

  public static GuildRegistry Guilds
  {
    get
    {
      return Game.instance.guildRegistry;
    }
  }

  public static ChatDataStorage ChatStorage
  {
    get
    {
      return Game.instance.chat;
    }
  }

  public static bool IsQuitting
  {
    get
    {
      if (!((UnityEngine.Object) Game.instance == (UnityEngine.Object) null))
        return Game.instance.quitting;
      return true;
    }
  }

  public static bool IsLoadingLevel
  {
    get
    {
      if (!((UnityEngine.Object) Game.instance == (UnityEngine.Object) null))
        return Game.instance.loadingLevel;
      return true;
    }
  }

  public static bool JustJumped
  {
    get
    {
      return Game.instance.justJumped;
    }
    set
    {
      Game.instance.justJumped = value;
    }
  }

  public static Catalogue Catalogue
  {
    get
    {
      return Game.instance.catalogue;
    }
  }

  public static Me Me
  {
    get
    {
      if ((UnityEngine.Object) Game.instance != (UnityEngine.Object) null)
        return Game.instance.me;
      return (Me) null;
    }
  }

  public static TimeSync TimeSync
  {
    get
    {
      return Game.instance.timeSync;
    }
  }

  public static Flag IsGameLevelStarted
  {
    get
    {
      return Game.isGameLevelStarted;
    }
  }

  public static ProtocolManager GetProtocolManager()
  {
    return Game.instance.protocolManager;
  }

  public static void Create()
  {
    if (!((UnityEngine.Object) Game.instance == (UnityEngine.Object) null))
      return;
    Game.instance = new GameObject("Game").AddComponent<Game>();
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) Game.instance);
    Game.instance.Init();
  }

  private void Init()
  {
    this.revision = (Resources.Load("revision") as TextAsset).text;
    this.localization = new BsgoLocalization();
    this.delayedActions = new DelayedActions();
    Game.isGameLevelStarted = new Flag();
    this.protocolManager = new ProtocolManager();
    this.timeSync = new TimeSync();
    this.catalogue = new Catalogue();
    this.galaxy = new Galaxy();
    this.playerRegistry = new PlayerRegistry();
    this.guildRegistry = new GuildRegistry();
    this.me = new Me();
    this.chat = new ChatDataStorage();
    this.ticker = new GuiTicker();
    this.funnel = new Funnel();
    this.tournament = new SectorTournament();
    this.payment = this.gameObject.AddComponent<PaymentInterface>();
    this._specialOfferManager = new SpecialOfferManager();
    FacadeFactory.GetInstance().SendMessage(Message.InitUi);
  }

  private void Update()
  {
    if (this.quitting || this.loadingLevel)
      return;
    this.timeSync.Update();
    this.protocolManager.Update();
    if (this.chatProtocol == null)
      return;
    this.chatProtocol.Update();
  }

  private void LateUpdate()
  {
    if (this.quitting || this.loadingLevel)
      return;
    this.protocolManager.LateUpdate();
  }

  private void OnApplicationQuit()
  {
    this.quitting = true;
    this.protocolManager.Disconnect("OnApplicationQuit");
    Game.instance = (Game) null;
  }

  private void OnEnable()
  {
  }

  public static void LoadLevel(LevelProfile levelProfile)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    Game.\u003CLoadLevel\u003Ec__AnonStorey69 levelCAnonStorey69 = new Game.\u003CLoadLevel\u003Ec__AnonStorey69();
    // ISSUE: reference to a compiler-generated field
    levelCAnonStorey69.levelProfile = levelProfile;
    if (Game.IsQuitting || Game.instance.loadingLevel)
      return;
    // ISSUE: reference to a compiler-generated field
    Game.instance.levelProfile = levelCAnonStorey69.levelProfile;
    Game.instance.loadingLevel = true;
    // ISSUE: reference to a compiler-generated field
    Log.Add("LoadLevel " + levelCAnonStorey69.levelProfile.SceneName);
    if (CutsceneManager.IsCutscenePlayed)
    {
      // ISSUE: reference to a compiler-generated method
      CutsceneManager.GetCurrentCutscene().AddOnFinishFunction(new CutsceneManager.OnFinishCutsceneDelegate(levelCAnonStorey69.\u003C\u003Em__41));
    }
    else
    {
      // ISSUE: reference to a compiler-generated field
      Game.LoadScene(levelCAnonStorey69.levelProfile.SceneName);
    }
  }

  private static void LoadScene(string sceneName)
  {
    FacadeFactory.GetInstance().SendMessage(Message.LoadNewLevel);
    Application.LoadLevel(sceneName);
  }

  public static void InstantiateLevel()
  {
    Game.instance.loadingLevel = false;
    Game.IsGameLevelStarted.Unset();
    GameLevel gameLevel = (GameLevel) null;
    if (Game.instance.levelProfile is AvatarLevelProfile)
      gameLevel = (GameLevel) Game.InstantiateScript<AvatarLevel>();
    else if (Game.instance.levelProfile is LoginLevelProfile)
      gameLevel = (GameLevel) Game.InstantiateScript<LoginLevel>();
    else if (Game.instance.levelProfile is RoomLevelProfile)
      gameLevel = (GameLevel) Game.InstantiateScript<RoomLevel>();
    else if (Game.instance.levelProfile is SpaceLevelProfile)
      gameLevel = (GameLevel) Game.InstantiateScript<SpaceLevel>();
    else if (Game.instance.levelProfile is StarterLevelProfile)
      gameLevel = (GameLevel) Game.InstantiateScript<StarterLevel>();
    else if (Game.instance.levelProfile is TransLevelProfile)
      gameLevel = (GameLevel) Game.InstantiateScript<TransScene>();
    else if (Game.instance.levelProfile is DisconnectLevelProfile)
      gameLevel = (GameLevel) Game.InstantiateScript<DisconnectLevel>();
    if (gameLevel.IsIngameLevel)
    {
      Game.IsGameLevelStarted.Depend(new ILoadable[1]
      {
        (ILoadable) gameLevel
      });
      Game.IsGameLevelStarted.Set();
    }
    try
    {
      gameLevel.Initialize(Game.instance.levelProfile);
      Game.instance.levelProfile = (LevelProfile) null;
    }
    catch (Exception ex)
    {
      Debug.LogError((object) "CRITICAL ERROR! Level was not initialized");
      Debug.LogError((object) (ex.Message + ex.StackTrace));
    }
  }

  public static void LoadNextScene(LevelProfile nextLevelProfile, TransSceneType transType)
  {
    Game.LoadLevel((LevelProfile) new TransLevelProfile(nextLevelProfile, transType));
  }

  public static void OnDisconnect(string message)
  {
    if (!Game.IsQuitting)
    {
      if ((int) Game.Me.ServerID == 0)
        Game.Funnel.ReportActivity(Activity.GameclientConnectionFailed);
      if (!((UnityEngine.Object) DisconnectLevel.GetLevel() == (UnityEngine.Object) null))
        return;
      Game.LoadLevel((LevelProfile) new DisconnectLevelProfile(message));
    }
    else
    {
      if (!((UnityEngine.Object) Game.instance != (UnityEngine.Object) null))
        return;
      Game.Funnel.ReportActivity(Activity.Logout);
    }
  }

  public static void CreateChat()
  {
    if (Game.ChatProtocol != null)
    {
      Game.ChatProtocol.Disconnect();
      Game.ChatProtocol = (ChatProtocol) null;
    }
    Game.ChatProtocol = new ChatProtocol();
    Game.ChatProtocol.Initialize();
  }

  public static bool LevelConsoleCommand(string[] messages)
  {
    return GameLevel.Instance.ConsoleCommand(messages[0]);
  }

  public static void DoLogoutEx(string Reason)
  {
    Game.ProtocolManager.Disconnect(Reason);
  }

  public static void DoLogout()
  {
    Game.ProtocolManager.Disconnect("Logout");
  }

  public static void QuitLogout()
  {
    ExternalApi.CallJs("quit", (object) "home");
  }

  public static void QuitUpdate()
  {
    ExternalApi.CallJs("quit", (object) "update");
  }

  public static void QuitDelete()
  {
    ExternalApi.CallJs("quit", (object) "delete");
  }

  public static void RegisterDialog(IGUIPanel dialog, bool addListenerToFront = true)
  {
    Game.GUIManager.AddPanel(dialog);
    if (addListenerToFront)
      Game.InputDispatcher.AddListenerToFront((InputListener) dialog);
    else
      Game.InputDispatcher.AddListener((InputListener) dialog);
  }

  public static void UnregisterDialog(IGUIPanel dialog)
  {
    Game.GUIManager.RemovePanel(dialog);
    Game.InputDispatcher.RemoveListener((InputListener) dialog);
    if (Game.InputDispatcher.Focused != dialog)
      return;
    Game.InputDispatcher.Focused = (InputListener) null;
  }

  public static void Reset()
  {
    Game.instance.loadingLevel = true;
    Game.instance = (Game) null;
  }

  public static void SetFullscreenFromBrowser()
  {
    Screen.fullScreen = true;
  }

  public static T InstantiateScript<T>() where T : MonoBehaviour
  {
    return new GameObject(typeof (T).Name).AddComponent<T>();
  }
}
