﻿// Decompiled with JetBrains decompiler
// Type: DebugProcessStateView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugProcessStateView : DebugBehaviour<DebugProcessStateView>
{
  private Vector2 scroll = new Vector2();
  private float reqTimer = 0.3f;
  private const float REQUEST_DELAY = 0.3f;
  private bool isRequestSend;
  private string stateString;

  private void Start()
  {
    this.windowID = 17;
    this.SetSize(300f, 300f);
    DebugProtocol.GetProtocol().ProcessStateWindow = this;
    DebugProtocol.GetProtocol().RequestProcessState();
  }

  protected override void WindowFunc()
  {
    GUILayout.Label(string.Format("Update time {0:0.0}/{1:0.0}", (object) this.reqTimer, (object) 0.3f));
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    GUILayout.Box(this.stateString);
    GUILayout.EndScrollView();
  }

  public void SetStateString(string value)
  {
    this.stateString = value;
    this.reqTimer = 0.3f;
    this.isRequestSend = false;
  }

  private void Update()
  {
    this.reqTimer -= Time.deltaTime;
    if ((double) this.reqTimer > 0.0 || this.isRequestSend)
      return;
    DebugProtocol.GetProtocol().RequestProcessState();
    this.isRequestSend = true;
  }
}
