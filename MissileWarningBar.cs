﻿// Decompiled with JetBrains decompiler
// Type: MissileWarningBar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class MissileWarningBar : GUIPanel
{
  private GUIImageNew MissileWarningIndent;
  private Texture2D MissileWarningIndentTexNormal;
  private Texture2D MissileWarningIndentTexNuclear;
  private readonly Texture2D missileAlertIconNormal;
  private readonly Texture2D missileAlertIconOver;
  private readonly Texture2D missileAlertIconPressed;
  private Texture2D nukeAlertIconNormal;
  private Texture2D nukeAlertIconOver;
  private Texture2D nukeAlertIconPressed;
  private readonly GUILabelNew titleBar;
  private SpaceObject currentTarget;
  private readonly List<GUIButtonNew> missileButtons;
  private readonly IncomingMissiles incomingMissiles;
  private int nPrevButtonPressed;

  public MissileWarningBar(IncomingMissiles incomingMissiles)
    : base((SmartRect) null)
  {
    this.root = new SmartRect(new Rect(0.0f, 0.0f, 0.0f, 0.0f), new float2((float) (Screen.width / 2 - 50), (float) (Screen.height - 230)), (SmartRect) null);
    this.titleBar = new GUILabelNew("%$bgo.etc.missile_alert%", this.root);
    this.missileAlertIconNormal = (Texture2D) ResourceLoader.Load("GUI/MissileAlertBar/missile_icon_normal");
    this.missileAlertIconOver = (Texture2D) ResourceLoader.Load("GUI/MissileAlertBar/missile_icon_over");
    this.missileAlertIconPressed = (Texture2D) ResourceLoader.Load("GUI/MissileAlertBar/missile_icon_pressed");
    this.nukeAlertIconNormal = (Texture2D) ResourceLoader.Load("GUI/MissileAlertBar/nuke_icon_normal");
    this.nukeAlertIconOver = (Texture2D) ResourceLoader.Load("GUI/MissileAlertBar/nuke_icon_over");
    this.nukeAlertIconPressed = (Texture2D) ResourceLoader.Load("GUI/MissileAlertBar/nuke_icon_normal");
    this.MissileWarningIndentTexNormal = (Texture2D) ResourceLoader.Load("GUI/MissileAlertBar/missilecorner");
    this.MissileWarningIndentTexNuclear = (Texture2D) ResourceLoader.Load("GUI/MissileAlertBar/missilecorner_nuke");
    this.MissileWarningIndent = new GUIImageNew(this.MissileWarningIndentTexNormal, this.root);
    this.MissileWarningIndent.Position = new float2(0.0f, 0.0f);
    this.titleBar.Position = new float2(0.0f, (float) (-(double) this.MissileWarningIndent.Rect.height / 2.0 - 10.0));
    this.titleBar.NormalColor = new Color(1f, 0.0f, 0.0f);
    this.missileButtons = new List<GUIButtonNew>();
    this.incomingMissiles = incomingMissiles;
    this.incomingMissiles.MissileDetected += new MissileDetectedEventHandler(this.AddMissileInBrowse);
    this.incomingMissiles.MissileLost += new MissileLostEventHandler(this.RemoveMissleOutBrowse);
  }

  public void AddMissileInBrowse(Missile missile)
  {
    GUIButtonNew missileButton = this.GetMissileButton(missile);
    missileButton.TextLabel.Text = string.Empty;
    missileButton.Position = new float2((float) (this.missileAlertIconNormal.width + 10 + (this.missileAlertIconNormal.width + 5) * this.incomingMissiles.Count), 0.0f);
    SmartRect smartRect = new SmartRect(new Rect(0.0f, 0.0f, 12f, 32f), missileButton.Position, this.root);
    missileButton.TextLabel.Rect = smartRect.AbsRect;
    missileButton.IsRendered = true;
    missileButton.RecalculateAbsCoords();
    this.missileButtons.Add(missileButton);
  }

  private GUIButtonNew GetMissileButton(Missile missile)
  {
    if (missile.MissileCard.IsNuke)
      return new GUIButtonNew(this.nukeAlertIconNormal, this.nukeAlertIconOver, this.nukeAlertIconPressed, this.root);
    return new GUIButtonNew(this.missileAlertIconNormal, this.missileAlertIconOver, this.missileAlertIconPressed, this.root);
  }

  public void RemoveMissleOutBrowse(int nIndex)
  {
    if (nIndex <= -1)
      return;
    Rect rect1 = this.missileButtons[nIndex].Rect;
    this.missileButtons.Remove(this.missileButtons[nIndex]);
    int count = this.missileButtons.Count;
    for (int index = nIndex; index < count; ++index)
    {
      Rect rect2 = this.missileButtons[index].Rect;
      this.missileButtons[index].Rect = rect1;
      rect1 = rect2;
      this.missileButtons[index].RecalculateAbsCoords();
    }
  }

  public override void Draw()
  {
    if (this.incomingMissiles.IsEmpty)
      return;
    this.MissileWarningIndent.Texture = !this.incomingMissiles.HasNuclearMissiles() ? this.MissileWarningIndentTexNormal : this.MissileWarningIndentTexNuclear;
    this.titleBar.Draw();
    this.MissileWarningIndent.Draw();
    int num = Math.Min(this.missileButtons.Count, 16);
    for (int index = 0; index < num; ++index)
      this.missileButtons[index].Draw();
  }

  public override void RecalculateAbsCoords()
  {
    this.root.Position = new float2((float) (Screen.width / 2 - 50), (float) (Screen.height - 230));
    base.RecalculateAbsCoords();
    this.titleBar.RecalculateAbsCoords();
    this.MissileWarningIndent.RecalculateAbsCoords();
    using (List<GUIButtonNew>.Enumerator enumerator = this.missileButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.RecalculateAbsCoords();
    }
  }

  public override bool Contains(float2 point)
  {
    using (List<GUIButtonNew>.Enumerator enumerator = this.missileButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (enumerator.Current.Contains(point))
          return true;
      }
    }
    return false;
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse0)
    {
      using (List<GUIButtonNew>.Enumerator enumerator = this.missileButtons.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.OnMouseDown(mousePosition, mouseKey))
            return true;
        }
      }
    }
    return false;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse0)
    {
      int count = this.missileButtons.Count;
      for (int innerIndex = 0; innerIndex < count; ++innerIndex)
      {
        if (this.missileButtons[innerIndex].OnMouseUp(mousePosition, mouseKey))
        {
          this.currentTarget = (SpaceObject) this.incomingMissiles.GetMissile(innerIndex);
          this.missileButtons[innerIndex].IsPressed = true;
          TargetSelector.SelectTargetRequest(this.currentTarget, false);
          if (this.nPrevButtonPressed < count)
            this.missileButtons[this.nPrevButtonPressed].IsPressed = false;
          this.nPrevButtonPressed = innerIndex;
          return true;
        }
      }
    }
    return false;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    using (List<GUIButtonNew>.Enumerator enumerator = this.missileButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnMouseMove(mousePosition);
    }
  }
}
