﻿// Decompiled with JetBrains decompiler
// Type: GuiUpgradeWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.CharacterStatus.InflightShop;
using Gui.Common;
using System.Collections.Generic;
using UnityEngine;

public class GuiUpgradeWindow : GuiDialog
{
  private const float SKILL_REQUIREMENTS_ERROR_DURATION = 1f;
  private const int BASE_BOTTOM_BREAKLINE_POS_Y = 465;
  private readonly GuiUpgradeWindowData data;
  private readonly bool canUpgrade;
  private readonly GuiLabel title;
  private readonly GuiLabel bannerPercent;
  private readonly GuiLabel description;
  private readonly GuiUpgradeCounter counter;
  private readonly GuiLabel stats1;
  private readonly GuiLabel stats2;
  private readonly GuiLabel stats3;
  private readonly GuiImage banner;
  private readonly GuiPanel headerPanel;
  private readonly GuiAtlasImage itemIcon;
  private readonly GuiAtlasImage skillIcon;
  private readonly GuiLabel skill;
  private readonly GuiLabel upgradeCostText;
  private readonly GuiLabel saleCostText;
  private readonly MoneyCost moneyCostWidget;
  private readonly MoneyCost moneyCostSaleWidget;
  private readonly GuiButton upgrade;
  private readonly GuiColoredBox bottomBreakline;
  private int prevSelectedLevel;
  private bool displaySkillRequirementsError;
  private float skillRequirementsErrorStartTime;
  private readonly GuiButton close;

  public GuiUpgradeWindow(GuiUpgradeWindowData data, bool canUpgrade)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiUpgradeWindow.\u003CGuiUpgradeWindow\u003Ec__AnonStoreyBD windowCAnonStoreyBd = new GuiUpgradeWindow.\u003CGuiUpgradeWindow\u003Ec__AnonStoreyBD();
    // ISSUE: reference to a compiler-generated field
    windowCAnonStoreyBd.data = data;
    this.counter = new GuiUpgradeCounter();
    this.headerPanel = new GuiPanel();
    this.itemIcon = new GuiAtlasImage();
    this.skillIcon = new GuiAtlasImage(GuiAtlasImageBase.skillIconSize);
    this.moneyCostWidget = new MoneyCost();
    this.moneyCostSaleWidget = new MoneyCost();
    this.upgrade = new GuiButton("%$bgo.EquipBuyPanel.DetailsWindow.gui_window_layout.label_0%");
    // ISSUE: explicit constructor call
    base.\u002Ector();
    // ISSUE: reference to a compiler-generated field
    windowCAnonStoreyBd.\u003C\u003Ef__this = this;
    Loaders.Load((GuiPanel) this, "GUI/EquipBuyPanel/gui_upgrade_item_layout");
    // ISSUE: reference to a compiler-generated field
    this.data = windowCAnonStoreyBd.data;
    this.canUpgrade = canUpgrade;
    this.Align = Align.MiddleCenter;
    this.Position = new Vector2(0.0f, 0.0f);
    // ISSUE: reference to a compiler-generated method
    this.OnClose = new AnonymousDelegate(windowCAnonStoreyBd.\u003C\u003Em__1A5);
    this.title = this.Find<GuiLabel>("title");
    this.RemoveChild((GuiElementBase) this.title);
    this.title.Alignment = TextAnchor.UpperLeft;
    this.title.AutoSize = false;
    this.title.Size = new Vector2(400f, 40f);
    this.headerPanel.AddChild((GuiElementBase) this.itemIcon, Align.UpLeft);
    this.headerPanel.AddChild((GuiElementBase) this.title, Align.UpLeft, new Vector2(this.itemIcon.BottomRightCornerWithPadding.x + 4f, 0.0f));
    this.AddChild((GuiElementBase) this.headerPanel, Align.UpCenter, new Vector2(14f, 12f));
    this.AddChild((GuiElementBase) new GuiColoredBox(Gui.Options.PositiveColor, new Vector2(0.0f, 50f), new Vector2(300f, 1f)), Align.UpCenter);
    this.description = this.Find<GuiLabel>("description");
    this.description.Alignment = TextAnchor.UpperCenter;
    this.AddChild((GuiElementBase) new GuiColoredBox(Gui.Options.PositiveColor, new Vector2(0.0f, 235f), new Vector2(300f, 1f)), Align.UpCenter);
    // ISSUE: reference to a compiler-generated field
    if (windowCAnonStoreyBd.data.Item != null)
    {
      // ISSUE: reference to a compiler-generated field
      this.counter.selectedLevel = (int) windowCAnonStoreyBd.data.Item.Card.Level;
    }
    this.AddChild((GuiElementBase) this.counter, Align.UpCenter, new Vector2(0.0f, 240f));
    this.counter.OnIncrementPressed += new AnonymousDelegate(this.OnCounterIncremented);
    this.counter.OnDecrementPressed += new AnonymousDelegate(this.OnCounterDecremented);
    this.stats1 = this.Find<GuiLabel>("stats1");
    this.stats1.Alignment = TextAnchor.UpperLeft;
    this.stats2 = this.Find<GuiLabel>("stats2");
    this.stats2.Alignment = TextAnchor.UpperLeft;
    this.stats3 = this.Find<GuiLabel>("stats3");
    this.stats3.Alignment = TextAnchor.UpperLeft;
    this.stats3.AllColor = Gui.Options.PositiveColor;
    this.AddChild((GuiElementBase) this.skillIcon, Align.UpLeft, new Vector2(25f, 496f));
    this.skill = this.Find<GuiLabel>("skill");
    this.skill.Alignment = TextAnchor.UpperLeft;
    this.upgradeCostText = this.Find<GuiLabel>("upgradeCostText");
    this.upgradeCostText.Alignment = TextAnchor.UpperLeft;
    this.AddChild((GuiElementBase) this.moneyCostWidget, Align.UpLeft, new Vector2(285f, 515f));
    this.bottomBreakline = new GuiColoredBox(Gui.Options.PositiveColor, new Vector2(0.0f, 465f), new Vector2(300f, 1f));
    this.AddChild((GuiElementBase) this.bottomBreakline, Align.UpCenter);
    this.banner = this.Find<GuiImage>("banner");
    this.banner.handleMouseInput = false;
    this.bannerPercent = this.Find<GuiLabel>("banner_percent");
    this.bannerPercent.Alignment = TextAnchor.UpperCenter;
    this.saleCostText = this.Find<GuiLabel>("saleCostText");
    this.saleCostText.Text = "%$bgo.EquipBuyPanel.DetailsWindow.gui_window_layout.salecost_label%";
    this.saleCostText.Alignment = TextAnchor.UpperLeft;
    this.AddChild((GuiElementBase) this.moneyCostSaleWidget, Align.UpLeft, new Vector2(285f, 480f));
    // ISSUE: reference to a compiler-generated method
    this.upgrade.Pressed = new AnonymousDelegate(windowCAnonStoreyBd.\u003C\u003Em__1A6);
    this.AddChild((GuiElementBase) this.upgrade, Align.DownCenter, new Vector2(0.0f, -20f));
    this.close = new GuiButton("%$bgo.EquipBuyPanel.DetailsWindow.gui_window_layout.label_1%");
    // ISSUE: reference to a compiler-generated method
    this.close.OnClick = new AnonymousDelegate(windowCAnonStoreyBd.\u003C\u003Em__1A7);
    this.AddChild((GuiElementBase) this.close, Align.DownCenter, new Vector2(130f, -20f));
    this.AddChild((GuiElementBase) new Gui.Timer(1f));
    if (!canUpgrade)
      this.upgrade.IsRendered = false;
    this.PeriodicUpdate();
  }

  private void ItemUpgradeSale(bool state)
  {
    this.banner.IsRendered = state;
    this.bannerPercent.IsRendered = state;
    ShopDiscount discountInUpgradeLevels = this.data.Item.FindDiscountInUpgradeLevels();
    if (discountInUpgradeLevels != null)
      this.bannerPercent.Text = discountInUpgradeLevels.Percentage.ToString() + "%";
    this.bottomBreakline.Position = new Vector2(this.bottomBreakline.Position.x, (float) (465 - (!state ? 0 : 10)));
  }

  private void OnCounterIncremented()
  {
    if (this.counter.selectedLevel == this.prevSelectedLevel && this.counter.selectedLevel < (int) this.data.Item.Card.MaxLevel)
    {
      int systemLevel = this.counter.selectedLevel + 1;
      int skillLevel = 0;
      List<PlayerSkill> needSkills = (List<PlayerSkill>) null;
      if (!Game.Me.RequiresSkillsForSystem(this.data.Item, systemLevel, out skillLevel, out needSkills))
      {
        this.displaySkillRequirementsError = true;
        this.skillRequirementsErrorStartTime = Time.time;
        this.UpdateSkillRequirementsText();
      }
    }
    this.prevSelectedLevel = this.counter.selectedLevel;
  }

  private void OnCounterDecremented()
  {
    this.displaySkillRequirementsError = false;
    this.skillRequirementsErrorStartTime = 0.0f;
    this.prevSelectedLevel = this.counter.selectedLevel;
    this.UpdateSkillRequirementsText();
  }

  private void UpdateSkillRequirementsText()
  {
    if (this.data.Item.Card.SkillHashes.Length > 0)
    {
      this.skill.AllColor = Gui.Options.NormalColor;
      PlayerSkill skillByHash = Game.Me.GetSkillByHash(this.data.Item.Card.SkillHashes[0]);
      if (skillByHash != null)
      {
        this.skillIcon.GuiCard = skillByHash.Card.GUICard;
        this.skillIcon.IsRendered = true;
      }
      else
        this.skillIcon.IsRendered = false;
      int systemLevel = this.counter.selectedLevel;
      int skillLevel = 0;
      List<PlayerSkill> needSkills = (List<PlayerSkill>) null;
      Game.Me.RequiresSkillsForSystem(this.data.Item, systemLevel, out skillLevel, out needSkills);
      if ((double) Time.time - (double) this.skillRequirementsErrorStartTime >= 1.0)
        this.displaySkillRequirementsError = false;
      if (this.displaySkillRequirementsError)
      {
        this.skill.AllColor = Gui.Options.NegativeColor;
        Game.Me.RequiresSkillsForSystem(this.data.Item, systemLevel + 1, out skillLevel, out needSkills);
      }
      this.skill.Text = "%$bgo.EquipBuyPanel.DetailsWindow.gui_upgradebar_tooltip_layout.text_label%%br%" + (skillByHash != null ? skillByHash.Name + " " + (object) skillLevel : "%$bgo.shop.no_requires_skill%");
    }
    else
      this.skill.Text = string.Empty;
  }

  public static void UpdateMoneyCostWidget(MoneyCost costWidget, ShipSystem shipSystem, int systemLevel, bool applyDiscount)
  {
    CurrencyAmount priceToUpgrade = shipSystem.GetPriceToUpgrade(systemLevel, applyDiscount);
    ShipConsumableCard key1 = Game.Catalogue.FetchCard(215278030U, CardView.ShipConsumable) as ShipConsumableCard;
    ShipConsumableCard key2 = Game.Catalogue.FetchCard(207047790U, CardView.ShipConsumable) as ShipConsumableCard;
    ShipConsumableCard key3 = Game.Catalogue.FetchCard(264733124U, CardView.ShipConsumable) as ShipConsumableCard;
    ShipConsumableCard key4 = Game.Catalogue.FetchCard(130920111U, CardView.ShipConsumable) as ShipConsumableCard;
    ShipConsumableCard key5 = Game.Catalogue.FetchCard(254909109U, CardView.ShipConsumable) as ShipConsumableCard;
    Price price = new Price();
    if ((double) priceToUpgrade.Cubits > 0.0)
      price.items.Add(key3, priceToUpgrade.Cubits);
    if ((double) priceToUpgrade.Tylium > 0.0)
      price.items.Add(key1, priceToUpgrade.Tylium);
    if ((double) priceToUpgrade.Titanium > 0.0)
      price.items.Add(key2, priceToUpgrade.Titanium);
    if ((double) priceToUpgrade.Token > 0.0)
      price.items.Add(key4, priceToUpgrade.Token);
    if ((double) priceToUpgrade.TuningKits > 0.0)
      price.items.Add(key5, priceToUpgrade.TuningKits);
    bool show = true;
    if (price.items.Count == 0)
    {
      price.items.Add(key3, 0.0f);
      show = false;
    }
    costWidget.Price = price;
    costWidget.ShowIcons(show);
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    if (this.data == null || this.data.Item == null)
      return;
    this.title.Text = this.data.Item.ItemGUICard.Name;
    this.itemIcon.GuiCard = this.data.Item.ItemGUICard;
    this.headerPanel.ResizePanelToFitChildren();
    this.itemIcon.PositionX = this.title.TopLeftCornerWithPadding.x - 44f;
    this.itemIcon.PositionY = this.title.TopLeftCornerWithPadding.y;
    this.description.Text = this.data.Item.ItemGUICard.Description;
    this.counter.UpdateCounter(this.data.Item);
    this.counter.IsRendered = this.data.Item.Card.UserUpgradeable;
    string nameStats = string.Empty;
    string baseStats = string.Empty;
    string diffStats = string.Empty;
    this.GetStatsText(this.data.Item, this.counter.selectedLevel, ref nameStats, ref baseStats, ref diffStats);
    this.stats1.Text = nameStats;
    this.stats2.Text = baseStats;
    this.stats3.Text = diffStats;
    this.UpdateSkillRequirementsText();
    bool flag1 = Shop.IsUpgradeDiscounted(Shop.FindShopCategoryForSlotType(this.data.Item.Card.SlotType), this.counter.selectedLevel);
    GuiUpgradeWindow.UpdateMoneyCostWidget(this.moneyCostWidget, this.data.Item, this.counter.selectedLevel, false);
    GuiUpgradeWindow.UpdateMoneyCostWidget(this.moneyCostSaleWidget, this.data.Item, this.counter.selectedLevel, true);
    bool flag2 = this.counter.selectedLevel > 10;
    this.moneyCostWidget.IsRendered = !flag2;
    bool showLine = !flag2 && flag1;
    this.moneyCostWidget.ShowStrikeoutLine(showLine);
    GuiLabel guiLabel = this.saleCostText;
    bool flag3 = showLine;
    this.moneyCostSaleWidget.IsRendered = flag3;
    int num = flag3 ? 1 : 0;
    guiLabel.IsRendered = num != 0;
    this.upgradeCostText.Text = "%$bgo.EquipBuyPanel.DetailsWindow.gui_window_layout.upgradecost_label%" + (!flag2 ? string.Empty : "%br%%$bgo.upgrade.tuning_kits_only%");
    this.upgrade.IsRendered = this.canUpgrade && this.counter.selectedLevel != (int) this.data.Item.Card.Level;
    this.ItemUpgradeSale(this.data.Item.IsAnyUpgradeLevelOnSale());
  }

  public static void ShowWindow(ShipSlot slot)
  {
    IGUIPanel dialog = slot.Card.SystemType == ShipSlotType.ship_paint ? (IGUIPanel) new GuiPaintsViewWindow(slot.System) : (IGUIPanel) new GuiUpgradeWindow(new GuiUpgradeWindowData(slot.ServerID), true);
    Game.RegisterDialog(dialog, true);
    Game.InputDispatcher.Focused = (InputListener) dialog;
  }

  public static void ShowWindow(ShipItem item, bool canUpgrade)
  {
    if (item is ShipSystem)
    {
      IGUIPanel dialog = (item as ShipSystem).Card.SlotType == ShipSlotType.ship_paint ? (IGUIPanel) new GuiPaintsViewWindow(item as ShipSystem) : (IGUIPanel) new GuiUpgradeWindow(new GuiUpgradeWindowData(item as ShipSystem), canUpgrade);
      Game.RegisterDialog(dialog, true);
      Game.InputDispatcher.Focused = (InputListener) dialog;
    }
    else
    {
      if (!(item is ItemCountable))
        return;
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      GuiUpgradeWindow.\u003CShowWindow\u003Ec__AnonStoreyBE windowCAnonStoreyBe = new GuiUpgradeWindow.\u003CShowWindow\u003Ec__AnonStoreyBE();
      // ISSUE: reference to a compiler-generated field
      windowCAnonStoreyBe.window = (GuiDialog) new TooltipSimple(item);
      // ISSUE: reference to a compiler-generated field
      GuiLabel guiLabel = windowCAnonStoreyBe.window.Find<GuiLabel>("subTitle");
      if (!canUpgrade && guiLabel != null)
        guiLabel.Text = string.Empty;
      // ISSUE: reference to a compiler-generated field
      windowCAnonStoreyBe.window.Align = Align.MiddleCenter;
      // ISSUE: reference to a compiler-generated field
      windowCAnonStoreyBe.window.Position = new Vector2(0.0f, 0.0f);
      GuiButton guiButton = new GuiButton("%$bgo.common.close%");
      // ISSUE: reference to a compiler-generated method
      guiButton.Pressed = new AnonymousDelegate(windowCAnonStoreyBe.\u003C\u003Em__1A8);
      // ISSUE: reference to a compiler-generated field
      windowCAnonStoreyBe.window.SizeY += 20f;
      // ISSUE: reference to a compiler-generated field
      windowCAnonStoreyBe.window.AddChild((GuiElementBase) guiButton, Align.DownCenter, new Vector2(0.0f, -15f));
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      windowCAnonStoreyBe.window.OnClose = new AnonymousDelegate(windowCAnonStoreyBe.\u003C\u003Em__1A9);
      // ISSUE: reference to a compiler-generated field
      Game.RegisterDialog((IGUIPanel) windowCAnonStoreyBe.window, true);
      // ISSUE: reference to a compiler-generated field
      Game.InputDispatcher.Focused = (InputListener) windowCAnonStoreyBe.window;
    }
  }

  private void GetStatsText(ShipSystem system, int newLevel, ref string nameStats, ref string baseStats, ref string diffStats)
  {
    nameStats = string.Empty;
    baseStats = string.Empty;
    diffStats = string.Empty;
    List<SystemsStatsGenerator.StatInfoDesc> descriptions;
    if ((int) system.Card.Level == newLevel)
    {
      descriptions = SystemsStatsGenerator.GenerateDescriptions(system);
      descriptions.Add((SystemsStatsGenerator.StatInfoDesc) SystemsStatsGenerator.GetDurabilityDescription(system, (ShipSystemCard) null));
    }
    else
    {
      descriptions = SystemsStatsGenerator.GenerateDescriptions(system, system.Card.GetNextCardByLevel(newLevel), false);
      descriptions.Add((SystemsStatsGenerator.StatInfoDesc) SystemsStatsGenerator.GetDurabilityDescription(system, system.Card.GetNextCardByLevel(newLevel)));
    }
    using (List<SystemsStatsGenerator.StatInfoDesc>.Enumerator enumerator = descriptions.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SystemsStatsGenerator.StatInfoDesc current = enumerator.Current;
        nameStats = nameStats + current.Name + "\n";
        baseStats = baseStats + current.StatFormatedValue + "\n";
        SystemsStatsGenerator.StatInfoDescWithDiff infoDescWithDiff = current as SystemsStatsGenerator.StatInfoDescWithDiff;
        diffStats = infoDescWithDiff == null || GuiUpgradeWindow.ContainsOnlyZeros(infoDescWithDiff.DiffStatFormat) ? diffStats + "\n" : diffStats + infoDescWithDiff.DiffStatFormat + "\n";
      }
    }
  }

  private static bool ContainsOnlyZeros(string statDiff)
  {
    foreach (char c in statDiff)
    {
      if (char.IsDigit(c) && (int) c != 48)
        return false;
    }
    return true;
  }
}
