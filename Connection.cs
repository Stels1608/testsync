﻿// Decompiled with JetBrains decompiler
// Type: Connection
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class Connection
{
  public Connection.State state = Connection.State.Disconnected;
  private readonly Dictionary<int, uint> messagesReceived = new Dictionary<int, uint>();
  private const int MESSAGE_ENCODING_OFFSET = 100000;
  private Socket socket;
  private int needToReceive;
  private int packetLength;
  private bool isReadLength;
  public uint debugReceiveData;
  public uint debugSendData;
  public uint debugSendDataMax;
  public uint debugReceiveDataMax;

  public IEnumerable<int> MessageReceivedKeys
  {
    get
    {
      return (IEnumerable<int>) this.messagesReceived.Keys;
    }
  }

  public event Connection.ConnectionCallback OnConnectionDropped;

  private bool IsSocketConnected()
  {
    if (this.socket != null)
      return this.socket.Connected;
    return false;
  }

  public Connection.State GetState()
  {
    return this.state;
  }

  public void Connect(string ip, string port)
  {
    Game.Funnel.ReportActivity(Activity.GameclientConnectsToServer);
    try
    {
      IPAddress address = Dns.GetHostAddresses(ip)[0];
      this.state = Connection.State.Connecting;
      this.needToReceive = 0;
      this.packetLength = 0;
      this.isReadLength = false;
      this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
      this.socket.Blocking = true;
      this.socket.ReceiveBufferSize = (int) ushort.MaxValue;
      this.socket.NoDelay = true;
      Security.PrefetchSocketPolicy(address.ToString(), int.Parse(port) + 1);
      this.socket.Connect(address, int.Parse(port));
      this.state = Connection.State.Connected;
      Game.Funnel.ReportActivity(Activity.GameclientConnectedToServer);
    }
    catch (Exception ex)
    {
      this.OnConnectionDropped(ex.Message);
    }
  }

  public void Disconnect(string message)
  {
    this.state = Connection.State.Disconnected;
    if (this.socket != null)
    {
      this.socket.Close();
      this.socket = (Socket) null;
    }
    if (this.OnConnectionDropped == null)
      return;
    this.OnConnectionDropped(message);
  }

  public void SendMessage(BgoProtocolWriter message)
  {
    try
    {
      if (!this.IsSocketConnected())
        throw new Exception("Connected is false");
      this.socket.Send(message.GetBuffer(), 0, message.GetLength(), SocketFlags.None);
      uint val2 = (uint) message.GetLength();
      this.debugSendData += val2;
      this.debugSendDataMax = Math.Max(this.debugSendDataMax, val2);
    }
    catch (Exception ex)
    {
      this.Disconnect("Error sending: " + (object) ex);
    }
  }

  public void MessageReceived(BgoProtocol.ProtocolID protocolId, ushort messageType)
  {
    int key = (int) protocolId * 100000 + (int) messageType;
    if (!this.messagesReceived.ContainsKey(key))
      this.messagesReceived.Add(key, 0U);
    Dictionary<int, uint> dictionary;
    int index;
    (dictionary = this.messagesReceived)[index = key] = dictionary[index] + 1U;
  }

  public uint GetMessageReceivedCount(int messageKey)
  {
    return this.messagesReceived[messageKey];
  }

  public static Tuple<BgoProtocol.ProtocolID, ushort> GetMessageId(int messageKey)
  {
    return new Tuple<BgoProtocol.ProtocolID, ushort>((BgoProtocol.ProtocolID) (messageKey / 100000), (ushort) (messageKey % 100000));
  }

  public List<BgoProtocolReader> RecvMessage()
  {
    List<BgoProtocolReader> bgoProtocolReaderList = new List<BgoProtocolReader>();
    try
    {
      if (!this.IsSocketConnected())
        throw new Exception("Connected is false");
      if (this.socket.Available < 4)
        return bgoProtocolReaderList;
      this.needToReceive = this.socket.Available;
      while (true)
      {
        BgoProtocolReader bgoProtocolReader = this.RecvCurrentMessage();
        if (bgoProtocolReader != null)
          bgoProtocolReaderList.Add(bgoProtocolReader);
        else
          break;
      }
    }
    catch (Exception ex)
    {
      this.Disconnect("Error receiving: " + (object) ex);
    }
    return bgoProtocolReaderList;
  }

  private BgoProtocolReader RecvCurrentMessage()
  {
    if (this.needToReceive < 4)
      return (BgoProtocolReader) null;
    if (!this.isReadLength)
    {
      byte[] numArray = new byte[2];
      int num = this.socket.Receive(numArray, 0, numArray.Length, SocketFlags.None);
      this.needToReceive -= num;
      this.debugReceiveData += (uint) numArray.Length;
      if (num != 2)
        throw new Exception("receive length != 2");
      this.packetLength = (int) BgoProtocolReader.ReadBufferSize(numArray);
      this.isReadLength = true;
    }
    if (this.packetLength > this.socket.Available)
      return (BgoProtocolReader) null;
    byte[] buffer = new byte[this.packetLength];
    this.debugReceiveDataMax = Math.Max(this.debugReceiveDataMax, (uint) this.packetLength);
    int num1 = this.socket.Receive(buffer, 0, buffer.Length, SocketFlags.None);
    this.needToReceive -= num1;
    this.debugReceiveData += (uint) buffer.Length;
    if (num1 != this.packetLength)
      throw new Exception("receive length != packet lenght");
    this.isReadLength = false;
    return new BgoProtocolReader(buffer);
  }

  public enum State
  {
    Connecting,
    Connected,
    Disconnected,
  }

  public delegate void ConnectionCallback(string message);
}
