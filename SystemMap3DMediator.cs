﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SystemMap3DMediator : Bigpoint.Core.Mvc.View.View<Message>, InputListener
{
  public new const string NAME = "SectorMap3DMediator";
  private SystemMap3DManager systemMap3DManager;

  private bool IsSectorMapShown
  {
    get
    {
      if ((UnityEngine.Object) this.systemMap3DManager == (UnityEngine.Object) null)
        BgoUtils.LogOnce("SystemMap3DManager is null. What did you do...?");
      if ((UnityEngine.Object) this.systemMap3DManager != (UnityEngine.Object) null)
        return this.systemMap3DManager.IsSectorMapShown;
      return false;
    }
    set
    {
      this.systemMap3DManager.IsSectorMapShown = value;
    }
  }

  public bool HandleMouseInput
  {
    get
    {
      return true;
    }
  }

  public bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public bool HandleJoystickAxesInput
  {
    get
    {
      return true;
    }
  }

  public SystemMap3DMediator()
    : base("SectorMap3DMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.LevelLoaded);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.Death);
    this.AddMessageInterest(Message.ChangeSetting);
    this.AddMessageInterest(Message.PlayerInThreat);
    this.AddMessageInterest(Message.EntityLoaded);
    this.AddMessageInterest(Message.EntityRemoved);
    this.AddMessageInterest(Message.SelectedTargetChanged);
    this.AddMessageInterest(Message.Show3DSectorMap);
    this.AddMessageInterest(Message.PlayerChangedPartyStatus);
    this.AddMessageInterest(Message.MyJumpOutAnimStarted);
    this.AddMessageInterest(Message.TargetWaypointStatusChanged);
    this.AddMessageInterest(Message.CutscenePlayRequest);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    if ((UnityEngine.Object) GameLevel.Instance == (UnityEngine.Object) null || GameLevel.Instance.LevelType != GameLevelType.SpaceLevel)
      return;
    Message id = message.Id;
    switch (id)
    {
      case Message.Death:
        this.systemMap3DManager.IsSectorMapShown = false;
        break;
      case Message.MyJumpOutAnimStarted:
        this.IsSectorMapShown = false;
        break;
      default:
        if (id != Message.LoadNewLevel)
        {
          if (id != Message.LevelLoaded)
          {
            if (id != Message.EntityLoaded)
            {
              if (id != Message.EntityRemoved)
              {
                if (id != Message.ChangeSetting)
                {
                  if (id != Message.CutscenePlayRequest)
                  {
                    if (id != Message.Show3DSectorMap)
                    {
                      if (id != Message.SelectedTargetChanged)
                      {
                        if (id != Message.TargetWaypointStatusChanged)
                        {
                          if (id != Message.PlayerChangedPartyStatus)
                          {
                            if (id != Message.PlayerInThreat)
                              break;
                            this.systemMap3DManager.WindowView.ShowThreatWarning((bool) message.Data);
                            break;
                          }
                          this.systemMap3DManager.IconView.UpdatePlayerPartyState((Player) message.Data);
                          break;
                        }
                        KeyValuePair<ISpaceEntity, bool> keyValuePair = (KeyValuePair<ISpaceEntity, bool>) message.Data;
                        this.systemMap3DManager.IconView.UpdateWaypointStatus(keyValuePair.Key, keyValuePair.Value);
                        break;
                      }
                      KeyValuePair<ISpaceEntity, ISpaceEntity> keyValuePair1 = (KeyValuePair<ISpaceEntity, ISpaceEntity>) message.Data;
                      ISpaceEntity key = keyValuePair1.Key;
                      ISpaceEntity spaceEntity = keyValuePair1.Value;
                      if (key == spaceEntity)
                        break;
                      this.systemMap3DManager.IconView.SetIconSelectState(key, false);
                      this.systemMap3DManager.IconView.SetIconSelectState(spaceEntity, true);
                      this.systemMap3DManager.IconView.HoverInfo.SetSelectedTarget(spaceEntity);
                      break;
                    }
                    bool flag = (bool) message.Data;
                    if (Game.Me.Dead && flag)
                      break;
                    this.IsSectorMapShown = flag;
                    this.PopupCheck();
                    break;
                  }
                  this.systemMap3DManager.ShutDownImmediately();
                  break;
                }
                this.HandleSettingChange((ChangeSettingMessage) message);
                break;
              }
              this.systemMap3DManager.IconView.RemoveIcon((ISpaceEntity) message.Data);
              break;
            }
            this.systemMap3DManager.IconView.AddIcon((ISpaceEntity) message.Data);
            break;
          }
          this.systemMap3DManager = new GameObject("SectorMap3DManager").AddComponent<SystemMap3DManager>();
          break;
        }
        if (!((UnityEngine.Object) this.systemMap3DManager != (UnityEngine.Object) null))
          break;
        this.systemMap3DManager.Destroy();
        break;
    }
  }

  private void HandleSettingChange(ChangeSettingMessage message)
  {
    this.systemMap3DManager.PropagateSetting(message.Setting, message.Data);
  }

  public bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return this.IsSectorMapShown;
  }

  public bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return this.IsSectorMapShown;
  }

  public void OnMouseMove(float2 mousePosition)
  {
  }

  public bool OnMouseScrollDown()
  {
    return this.IsSectorMapShown;
  }

  public bool OnMouseScrollUp()
  {
    return this.IsSectorMapShown;
  }

  private bool ActionIsMoveAction(Action action)
  {
    Action action1 = action;
    switch (action1)
    {
      case Action.JoystickTurnLeft:
      case Action.JoystickTurnRight:
      case Action.JoystickTurnUp:
      case Action.JoystickTurnDown:
      case Action.JoystickRollLeft:
      case Action.JoystickRollRight:
      case Action.JoystickSpeedController:
      case Action.JoystickStrafeLeft:
      case Action.JoystickStrafeRight:
      case Action.JoystickStrafeUp:
      case Action.JoystickStrafeDown:
label_3:
        return true;
      default:
        switch (action1 - 38)
        {
          case Action.None:
          case (Action) 1:
          case (Action) 2:
          case (Action) 3:
          case (Action) 4:
          case (Action) 5:
          case (Action) 8:
          case (Action) 10:
          case (Action) 12:
            goto label_3;
          default:
            if (action1 != Action.RollLeft && action1 != Action.RollRight)
              return false;
            goto label_3;
        }
    }
  }

  public bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (this.ActionIsMoveAction(action) || CutsceneManager.IsCutscenePlayed)
      return false;
    GalaxyMapMain galaxyMapMain = Game.GUIManager.Find<GalaxyMapMain>();
    if (galaxyMapMain != null && galaxyMapMain.IsRendered || Game.GUIManager.Find<GUISystemMap>().IsRendered)
      return false;
    if (action == Action.ToggleSystemMap3D)
      this.SendMessage(Message.Show3DSectorMap, (object) !this.IsSectorMapShown);
    if (action == Action.Boost)
      this.systemMap3DManager.WindowView.SetBlinkingMessageText(Tools.ParseMessage("%$bgo.sector_map.message.start_engine_boost%"));
    if (action == Action.Map3DFocusYourShip)
      this.systemMap3DManager.CameraView.FocusMyShip();
    if (action == Action.Map3DBackToOverview)
      this.systemMap3DManager.CameraView.GoBackToTopView();
    return this.IsSectorMapShown;
  }

  public bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (this.ActionIsMoveAction(action))
      return false;
    return this.IsSectorMapShown;
  }

  public bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    if (!this.IsSectorMapShown || this.ActionIsMoveAction(action))
      return false;
    if (JoystickLookAroundInputs.ActionIsJoystickLookInput(action))
      this.systemMap3DManager.CameraView.OnJoystickLookInput(triggerCode, modifierCode, action, magnitude, delta, isAxisInverted);
    return this.IsSectorMapShown;
  }

  private void PopupCheck()
  {
    if (PlayerPrefs.HasKey("SystemMap3DNotificationShown") || (DateTime.Now.Year != 2015 || DateTime.Now.Month != 4 && DateTime.Now.Month != 5) || (int) Game.Me.Level <= 1)
      return;
    Timer.CreateTimer("DelayedPopupTimer", 2f, 2f, (Timer.TickParametrizedHandler) (param0 =>
    {
      MessageBoxFactory.CreateNotifyBox(BsgoCanvas.Windows).SetContent((OnMessageBoxClose) null, "%$bgo.message_box.notification_title%", "%$bgo.sector_map.message_box.first_time_opened%", 0U);
      PlayerPrefs.SetInt("SystemMap3DNotificationShown", 0);
    }), (object) true, true);
  }
}
