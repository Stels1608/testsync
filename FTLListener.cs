﻿// Decompiled with JetBrains decompiler
// Type: FTLListener
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FTLListener : MonoBehaviour
{
  public FTLListener.Action onReadyToJump;
  public FTLListener.Action onJumpFlash;

  private void OnReadyToJump()
  {
    if (this.onReadyToJump == null)
      return;
    this.onReadyToJump();
  }

  private void OnJumpFlash()
  {
    if (this.onJumpFlash == null)
      return;
    this.onJumpFlash();
  }

  public delegate void Action();
}
