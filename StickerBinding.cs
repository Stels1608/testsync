﻿// Decompiled with JetBrains decompiler
// Type: StickerBinding
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class StickerBinding : IProtocolWrite, IProtocolRead
{
  public ushort ObjectPointHash;
  public ushort StickerID;

  void IProtocolRead.Read(BgoProtocolReader r)
  {
    this.ObjectPointHash = r.ReadUInt16();
    this.StickerID = r.ReadUInt16();
  }

  void IProtocolWrite.Write(BgoProtocolWriter w)
  {
    w.Write(this.ObjectPointHash);
    w.Write(this.StickerID);
  }
}
