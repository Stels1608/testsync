﻿// Decompiled with JetBrains decompiler
// Type: Vector2i
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public struct Vector2i
{
  public static readonly Vector2i zero = new Vector2i();
  public static readonly Vector2i one = new Vector2i(1, 1);
  public static readonly Vector2i right = new Vector2i(1, 0);
  public static readonly Vector2i up = new Vector2i(0, 1);
  public int x;
  public int y;

  public int Size
  {
    get
    {
      return this.x * this.y;
    }
  }

  public Vector2i(int x, int y)
  {
    this.x = x;
    this.y = y;
  }

  public Vector2i(float x, float y)
  {
    this.x = Mathf.RoundToInt(x);
    this.y = Mathf.RoundToInt(y);
  }

  public static Vector2i operator +(Vector2i v1, Vector2i v2)
  {
    return new Vector2i(v1.x + v2.x, v1.y + v2.y);
  }

  public static Vector2i operator -(Vector2i v1, Vector2i v2)
  {
    return new Vector2i(v1.x - v2.x, v1.y - v2.y);
  }

  public static bool operator ==(Vector2i v1, Vector2i v2)
  {
    if (v1.x == v2.x)
      return v1.y == v2.y;
    return false;
  }

  public static bool operator !=(Vector2i v1, Vector2i v2)
  {
    if (v1.x == v2.x)
      return v1.y != v2.y;
    return true;
  }

  public static Vector2i operator *(float f, Vector2i v)
  {
    return new Vector2i(f * (float) v.x, f * (float) v.y);
  }

  public static Vector2i operator *(Vector2i v, float f)
  {
    return new Vector2i(f * (float) v.x, f * (float) v.y);
  }

  public override bool Equals(object obj)
  {
    if (!(obj is Vector2i))
      return false;
    return this == (Vector2i) obj;
  }

  public override int GetHashCode()
  {
    return this.x ^ this.y;
  }

  public override string ToString()
  {
    return "(x=" + (object) this.x + ", y=" + (object) this.y + ")";
  }
}
