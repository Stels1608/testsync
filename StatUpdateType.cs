﻿// Decompiled with JetBrains decompiler
// Type: StatUpdateType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum StatUpdateType : byte
{
  Unknown = 0,
  Stat = 1,
  Buff = 2,
  Combat = 3,
  Target = 4,
  RemoveBuff = 5,
  PowerPoints = 6,
  HullPoints = 7,
  Reset = 9,
  SlotStat = 12,
  ShipAspects = 13,
  ToggleBuff = 14,
  RemoveToggleBuff = 15,
  StatsModifier = 16,
  RemoveStatsModifier = 17,
  ShortCircuit = 18,
  RemoveShortCircuit = 19,
}
