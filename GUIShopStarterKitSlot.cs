﻿// Decompiled with JetBrains decompiler
// Type: GUIShopStarterKitSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIShopStarterKitSlot : GUIPanel
{
  private readonly GUIImageNew itemImage;
  private readonly GUILabelNew itemDescription;
  private readonly GUILabelNew shortDescription;
  private readonly AtlasCache atlasCache;
  private int slotNumber;

  public GUIShopStarterKitSlot(SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
    this.root.Width = 450f;
    this.root.Height = 35f;
    this.itemDescription = new GUILabelNew();
    this.itemDescription.Width = 300f;
    this.itemDescription.Height = 35f;
    this.itemDescription.PositionX = 125f;
    this.itemDescription.WordWrap = true;
    this.itemDescription.Alignment = TextAnchor.UpperLeft;
    this.itemDescription.Font = Gui.Options.FontBGM_BT;
    this.itemDescription.FontSize = 12;
    this.AddPanel((GUIPanel) this.itemDescription);
    this.shortDescription = new GUILabelNew();
    this.shortDescription.Width = 300f;
    this.shortDescription.Height = 35f;
    this.shortDescription.PositionX = 125f;
    this.shortDescription.PositionY = 17f;
    this.shortDescription.Alignment = TextAnchor.MiddleLeft;
    this.shortDescription.Font = Gui.Options.FontBGM_BT;
    this.shortDescription.FontSize = 10;
    this.shortDescription.NormalColor = Color.grey;
    this.shortDescription.OverColor = Color.grey;
    this.AddPanel((GUIPanel) this.shortDescription);
    this.atlasCache = new AtlasCache(new float2(40f, 35f));
    this.itemImage = new GUIImageNew((Texture2D) ResourceLoader.Load("GUI/Editor/editor_default_image"), this.root);
    this.itemImage.Width = 40f;
    this.itemImage.Height = 35f;
    this.itemImage.PositionX = -60f;
    this.AddPanel((GUIPanel) this.itemImage);
  }

  public void UpdateData(ShipItem item)
  {
    if (item != null && (bool) item.IsLoaded)
    {
      AtlasEntry cachedEntryBy = this.atlasCache.GetCachedEntryBy(item.ItemGUICard.GUIAtlasTexturePath, (int) item.ItemGUICard.FrameIndex);
      this.itemImage.Texture = cachedEntryBy.Texture;
      this.itemImage.InnerRect = cachedEntryBy.FrameRect;
      this.itemImage.IsRendered = true;
      this.itemDescription.Text = (!(item is ItemCountable) ? string.Empty : ((int) (item as ItemCountable).Count).ToString() + "X ") + item.ItemGUICard.Name;
      this.itemDescription.IsRendered = true;
      if (item is ShipSystem)
      {
        this.shortDescription.Text = "Level: " + (object) item.ItemGUICard.Level;
        this.shortDescription.IsRendered = true;
      }
      else
        this.shortDescription.IsRendered = false;
    }
    this.RecalculateAbsCoords();
  }
}
