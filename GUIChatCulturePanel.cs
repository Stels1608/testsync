﻿// Decompiled with JetBrains decompiler
// Type: GUIChatCulturePanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System.Collections.Generic;
using UnityEngine;

internal class GUIChatCulturePanel : GUIPanel
{
  public static readonly string DEFAULT_COUNTRY = "en";
  public static Dictionary<string, string> extend = new Dictionary<string, string>();
  private readonly GUIChatCheckBox globalOne = new GUIChatCheckBox("GUI/Chat/Culture/button_world_inactive", "GUI/Chat/Culture/button_world_over", "GUI/Chat/Culture/button_world_selected");
  private const string localTooltip = "%$bgo.chat.tooltip_culture_local%";
  private const string localStopReading = "%$bgo.chat.tooltip_culture_local_disable%";
  private const string globalTooltip = "%$bgo.chat.tooltip_culture_global%";
  private const string globalStopReading = "%$bgo.chat.tooltip_culture_global_disable%";
  private readonly GUIChatCheckBox localOne;
  private AnonymousDelegate handler;

  public AnonymousDelegate Handler
  {
    get
    {
      return this.handler;
    }
    set
    {
      this.handler = value;
    }
  }

  private int NumPressed
  {
    get
    {
      GUIChatCulturePanel.\u003C\u003Ec__AnonStorey80 cAnonStorey80 = new GUIChatCulturePanel.\u003C\u003Ec__AnonStorey80();
      List<GUIChatCheckBox> all = this.FindAll<GUIChatCheckBox>();
      cAnonStorey80.numChecked = 0;
      all.ForEach(new System.Action<GUIChatCheckBox>(cAnonStorey80.\u003C\u003Em__AF));
      return cAnonStorey80.numChecked;
    }
  }

  public bool Both
  {
    get
    {
      if (this.globalOne.IsPressed)
        return this.localOne.IsPressed;
      return false;
    }
  }

  public bool Local
  {
    get
    {
      return this.localOne.IsPressed;
    }
    set
    {
      this.localOne.IsPressed = value;
    }
  }

  public bool Global
  {
    get
    {
      return this.globalOne.IsPressed;
    }
    set
    {
      this.globalOne.IsPressed = value;
    }
  }

  static GUIChatCulturePanel()
  {
    GUIChatCulturePanel.extend.Add("en", "britain");
    GUIChatCulturePanel.extend.Add("br", "brazil");
    GUIChatCulturePanel.extend.Add("de", "german");
    GUIChatCulturePanel.extend.Add("us", "usa");
    GUIChatCulturePanel.extend.Add("ru", "russia");
    GUIChatCulturePanel.extend.Add("fr", "france");
    GUIChatCulturePanel.extend.Add("it", "italy");
    GUIChatCulturePanel.extend.Add("es", "spain");
    GUIChatCulturePanel.extend.Add("pl", "poland");
    GUIChatCulturePanel.extend.Add("tr", "turkey");
    GUIChatCulturePanel.extend.Add("nl", "netherlands");
    GUIChatCulturePanel.extend.Add("hu", "hungary");
    GUIChatCulturePanel.extend.Add("cs", "czech");
    GUIChatCulturePanel.extend.Add("da", "danish");
    GUIChatCulturePanel.extend.Add("sv", "swedish");
    GUIChatCulturePanel.extend.Add("fi", "finnish");
    GUIChatCulturePanel.extend.Add("sk", "slovak");
    GUIChatCulturePanel.extend.Add("ro", "romanian");
    GUIChatCulturePanel.extend.Add("bg", "bulgarian");
    GUIChatCulturePanel.extend.Add("el", "greek");
    GUIChatCulturePanel.extend.Add("pt", "portugese");
  }

  public GUIChatCulturePanel(string languageShorthand, bool viewLocal, bool viewGlobal)
  {
    this.Name = "culturePanel";
    if (!GUIChatCulturePanel.extend.ContainsKey(languageShorthand))
      languageShorthand = GUIChatCulturePanel.DEFAULT_COUNTRY;
    this.localOne = new GUIChatCheckBox("GUI/Chat/Culture/button_" + GUIChatCulturePanel.extend[languageShorthand] + "_inactive", "GUI/Chat/Culture/button_" + GUIChatCulturePanel.extend[languageShorthand] + "_over", "GUI/Chat/Culture/button_" + GUIChatCulturePanel.extend[languageShorthand] + "_selected");
    this.globalOne.SetTooltip("%$bgo.chat.tooltip_culture_global%");
    this.localOne.SetTooltip("%$bgo.chat.tooltip_culture_local%");
    this.AddPanel((GUIPanel) this.localOne, new float2(0.0f, -10f));
    this.AddPanel((GUIPanel) this.globalOne, new float2(0.0f, 10f));
    this.Size = new float2(this.localOne.Width, 20f + this.localOne.Height);
    this.localOne.IsPressed = viewLocal;
    this.localOne.Handler = (AnonymousDelegate) (() =>
    {
      if (this.localOne.IsPressed && this.NumPressed - 1 <= 0)
        return;
      this.localOne.Toggle();
      if (this.Handler != null)
        this.Handler();
      this.localOne.SetTooltip(!this.localOne.IsPressed ? "%$bgo.chat.tooltip_culture_local%" : "%$bgo.chat.tooltip_culture_local_disable%");
      FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.ChatViewLocal, (object) this.localOne.IsPressed, true));
    });
    this.globalOne.IsPressed = viewGlobal;
    this.globalOne.Handler = (AnonymousDelegate) (() =>
    {
      if (this.globalOne.IsPressed && this.NumPressed - 1 <= 0)
        return;
      this.globalOne.Toggle();
      if (this.Handler != null)
        this.Handler();
      this.globalOne.SetTooltip(!this.globalOne.IsPressed ? "%$bgo.chat.tooltip_culture_global%" : "%$bgo.chat.tooltip_culture_global_disable%");
      FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.ChatViewGlobal, (object) this.globalOne.IsPressed, true));
    });
    this.IsRendered = true;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GUIChatCulturePanel.\u003COnMouseUp\u003Ec__AnonStorey81 mouseUpCAnonStorey81 = new GUIChatCulturePanel.\u003COnMouseUp\u003Ec__AnonStorey81();
    // ISSUE: reference to a compiler-generated field
    mouseUpCAnonStorey81.mousePosition = mousePosition;
    if (!this.HandleMouseInput || !this.wasMouseDown)
      return false;
    this.wasMouseDown = false;
    // ISSUE: reference to a compiler-generated method
    this.FindAll<GUIChatCheckBox>().ForEach(new System.Action<GUIChatCheckBox>(mouseUpCAnonStorey81.\u003C\u003Em__B0));
    using (List<GUIPanel>.Enumerator enumerator = this.Children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIPanel current = enumerator.Current;
        if (!(current is GUIChatCheckBox))
        {
          // ISSUE: reference to a compiler-generated field
          current.OnMouseUp(mouseUpCAnonStorey81.mousePosition, mouseKey);
        }
      }
    }
    // ISSUE: reference to a compiler-generated field
    return this.Contains(mouseUpCAnonStorey81.mousePosition);
  }
}
