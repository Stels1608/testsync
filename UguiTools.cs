﻿// Decompiled with JetBrains decompiler
// Type: UguiTools
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Paths;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public static class UguiTools
{
  public const int LayerUI = 5;
  private static EventSystem eventSystemCached;
  private static Dictionary<BsgoCanvas, Canvas> canvases;

  public static void AddEventTrigger(EventTrigger eventTrigger, UnityAction<BaseEventData> action, EventTriggerType triggerType)
  {
    EventTrigger.TriggerEvent triggerEvent = new EventTrigger.TriggerEvent();
    triggerEvent.AddListener(action);
    EventTrigger.Entry entry = new EventTrigger.Entry() { callback = triggerEvent, eventID = triggerType };
    eventTrigger.triggers.Add(entry);
  }

  public static void CreateCanvases()
  {
    if (UguiTools.canvases != null)
    {
      Debug.LogWarning((object) "CreateCanvases(): uGUI Canvases have already been created.");
    }
    else
    {
      UguiTools.canvases = new Dictionary<BsgoCanvas, Canvas>();
      UguiTools.canvases.Add(BsgoCanvas.Hub, UguiTools.Create2DCanvas("Canvas2D (Hub)", (short) 0));
      UguiTools.canvases.Add(BsgoCanvas.Hud, UguiTools.Create2DCanvas("Canvas2D (Hud)", (short) 16000));
      UguiTools.canvases.Add(BsgoCanvas.HudIndicators, UguiTools.Create2DCanvas("Canvas2D (HudIndicators)", (short) 0));
      UguiTools.canvases.Add(BsgoCanvas.CharacterMenu, UguiTools.Create2DCanvas("Canvas2D (CharacterMenu)", (short) 0));
      UguiTools.canvases.Add(BsgoCanvas.Windows, UguiTools.Create2DCanvas("Canvas2D (Windows)", short.MaxValue));
      UguiTools.canvases.Add(BsgoCanvas.SystemMapCanvas2D, UguiTools.Create2DCanvas("Canvas2D (SystemMapCanvas2D)", (short) 0));
      UguiTools.canvases.Add(BsgoCanvas.SystemMapCanvas3D, UguiTools.Create3DCanvas("Canvas3D (SystemMapCanvas3D)"));
      UguiTools.canvases.Add(BsgoCanvas.Banner, UguiTools.Create2DCanvas("Canvas2D (Banner)", (short) 0));
      UguiTools.canvases[BsgoCanvas.HudIndicators].pixelPerfect = false;
    }
  }

  private static Canvas Create2DCanvas(string name, short sortingOrder = 0)
  {
    GameObject gameObject = Object.Instantiate<GameObject>(Resources.Load<GameObject>(Ui.Prefabs + "/uGUI 2D Canvas (Overlay)"));
    gameObject.name = name;
    Object.DontDestroyOnLoad((Object) gameObject);
    Canvas component = gameObject.GetComponent<Canvas>();
    component.sortingOrder = (int) sortingOrder;
    return component;
  }

  private static Canvas Create3DCanvas(string name)
  {
    GameObject gameObject = Object.Instantiate<GameObject>(Resources.Load<GameObject>(Ui.Prefabs + "/uGUI 3D Canvas"));
    gameObject.name = name;
    Object.DontDestroyOnLoad((Object) gameObject);
    return gameObject.GetComponent<Canvas>();
  }

  public static Canvas GetCanvas(BsgoCanvas bsgoCanvas)
  {
    return UguiTools.canvases[bsgoCanvas];
  }

  public static EventSystem GetEventSystem()
  {
    if ((Object) UguiTools.eventSystemCached == (Object) null)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(Resources.Load<GameObject>(Ui.Prefabs + "/uGUI EventSystem"));
      gameObject.name = "uGUI EventSystem";
      Object.DontDestroyOnLoad((Object) gameObject);
      UguiTools.eventSystemCached = gameObject.GetComponent<EventSystem>();
    }
    return UguiTools.eventSystemCached;
  }

  public static void EnableAllCanvases(bool enable)
  {
    using (Dictionary<BsgoCanvas, Canvas>.Enumerator enumerator = UguiTools.canvases.GetEnumerator())
    {
      while (enumerator.MoveNext())
        UguiTools.EnableCanvas(enumerator.Current.Key, enable);
    }
  }

  public static void EnableCanvas(BsgoCanvas bsgoCanvas, bool enable)
  {
    if ((Object) UguiTools.canvases[bsgoCanvas] == (Object) null)
      Debug.Log((object) ("EnableCanvas(" + (object) bsgoCanvas + ", " + (object) enable + ") failed. Already destroyed?"));
    else
      UguiTools.canvases[bsgoCanvas].enabled = enable;
  }

  public static GameObject CreateChild(GameObject childPrototype, Transform parent)
  {
    GameObject gameObject = Object.Instantiate<GameObject>(childPrototype);
    gameObject.transform.SetParent(parent, false);
    return gameObject;
  }

  public static GameObject CreateChild(string prefabFilename, Transform parent)
  {
    return UguiTools.CreateChild(Resources.Load<GameObject>(Ui.Prefabs + "/" + prefabFilename), parent);
  }

  public static T CreateChild<T>(string prefabFilename, Transform parent) where T : Component
  {
    return UguiTools.CreateChild<T>(Resources.Load<GameObject>(Ui.Prefabs + "/" + prefabFilename), parent);
  }

  public static T CreateChild<T>(GameObject childPrototype, Transform parent) where T : Component
  {
    GameObject child = UguiTools.CreateChild(childPrototype, parent);
    T component = child.GetComponent<T>();
    child.transform.SetParent(parent, false);
    return component;
  }

  public static T CreateChild<T>(Transform parent) where T : Component
  {
    return UguiTools.CreateBasicUiObject(typeof (T).ToString(), parent).gameObject.AddComponent<T>();
  }

  public static RectTransform CreateBasicUiObject(string name, Transform parent = null)
  {
    GameObject gameObject = new GameObject(name);
    gameObject.layer = 5;
    gameObject.transform.SetParent(parent);
    RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
    rectTransform.localScale = Vector3.one;
    return rectTransform;
  }

  public static void ApplyHighlightColorTag(ref string inputString)
  {
    string hexRgb = Tools.ColorToHexRGB((Color32) ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_SPECIAL));
    inputString = inputString.Replace("<highlight>", "<color=#" + hexRgb + ">").Replace("</highlight>", "</color>");
  }

  public static bool IsMouseLeftBtnEvent(BaseEventData baseEventData)
  {
    PointerEventData pointerEventData = baseEventData as PointerEventData;
    if (pointerEventData != null)
      return pointerEventData.button == PointerEventData.InputButton.Left;
    return false;
  }
}
