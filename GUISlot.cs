﻿// Decompiled with JetBrains decompiler
// Type: GUISlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

public class GUISlot : GUIPanel
{
  protected GUIImageNew background;
  protected GUIImageNew redStrip;
  protected GUIImageNew blueStrip;
  private Rect startRed;
  private Rect startBlue;
  private float redLevel;
  private float blueLevel;
  protected GUILabelNew healthLevel;
  protected GUILabelNew energyLevel;
  protected GUILabelNew distance;
  protected GUILabelNew name;
  protected bool nameWasCut;
  protected GUISlotAvatar avatar;
  protected bool avatarClicked;
  protected Texture2D humanAvatar;
  protected Texture2D cylonAvatar;
  protected GUIImageNew leaderMark;

  public static bool ShowStats { get; set; }

  public GUIImageNew Background
  {
    get
    {
      return this.background;
    }
  }

  public string NameString
  {
    set
    {
      this.name.Text = value;
      this.nameWasCut = false;
      if ((double) TextUtility.CalcTextSize(this.name.Text, this.name.Font, this.name.FontSize).width < (double) this.name.SmartRect.Width)
        return;
      int length = this.name.Text.Length - 1;
      for (int index = this.name.Text.Length - 1; index > 3; --index)
      {
        if ((double) TextUtility.CalcTextSize(this.name.Text.Substring(0, index + 1), this.name.Font, this.name.FontSize).width <= (double) this.name.SmartRect.Width)
        {
          length = index;
          break;
        }
      }
      this.name.Text = this.name.Text.Substring(0, length) + "...";
      this.nameWasCut = true;
    }
  }

  public float RedLevel
  {
    get
    {
      return this.redLevel;
    }
    set
    {
      this.redLevel = Mathf.Clamp01(value);
      this.redStrip.Rect = new Rect(this.startRed.x, this.startRed.y, this.startRed.width * this.RedLevel, this.startRed.height);
      this.redStrip.RecalculateAbsCoords();
    }
  }

  public float BlueLevel
  {
    get
    {
      return this.blueLevel;
    }
    set
    {
      this.blueLevel = Mathf.Clamp01(value);
      this.blueStrip.Rect = new Rect(this.startBlue.x, this.startBlue.y, this.BlueLevel * this.startBlue.width, this.startBlue.height);
      this.blueStrip.RecalculateAbsCoords();
    }
  }

  public bool RenderHealthLevel
  {
    get
    {
      return this.healthLevel.IsRendered;
    }
    set
    {
      this.healthLevel.IsRendered = value;
    }
  }

  public bool RenderEnergyLevel
  {
    get
    {
      return this.energyLevel.IsRendered;
    }
    set
    {
      this.energyLevel.IsRendered = value;
    }
  }

  public GUISlot(SmartRect parent)
    : base(parent)
  {
    JWindowDescription jwindowDescription = Loaders.LoadResource("GUI/Slots/layout");
    this.background = (jwindowDescription["background"] as JImage).CreateGUIImageNew(this.root);
    this.root.Width = this.background.SmartRect.Width;
    this.root.Height = this.background.SmartRect.Height;
    this.redStrip = (jwindowDescription["barRed"] as JImage).CreateGUIImageNew(this.root);
    this.startRed = this.redStrip.Rect;
    this.blueStrip = (jwindowDescription["barBlue"] as JImage).CreateGUIImageNew(this.root);
    this.startBlue = this.blueStrip.Rect;
    this.healthLevel = (jwindowDescription["healthLevel"] as JLabel).CreateGUILabelNew(this.root);
    this.energyLevel = (jwindowDescription["energyLevel"] as JLabel).CreateGUILabelNew(this.root);
    this.healthLevel.IsRendered = false;
    this.energyLevel.IsRendered = false;
    GUIImageNew guiImageNew = (jwindowDescription["avatar"] as JImage).CreateGUIImageNew(this.root);
    this.avatar = new GUISlotAvatar();
    this.avatar.Name = guiImageNew.Name;
    this.avatar.SmartRect.Width = guiImageNew.SmartRect.Width;
    this.avatar.SmartRect.Height = guiImageNew.SmartRect.Height;
    this.avatar.Position = guiImageNew.Position;
    this.avatar.IsRendered = false;
    this.humanAvatar = ResourceLoader.Load<Texture2D>("GUI/Slots/avatar_enemy_temp");
    this.cylonAvatar = ResourceLoader.Load<Texture2D>("GUI/Slots/avatar_player_temp");
    this.leaderMark = new GUIImageNew(jwindowDescription["isPartyLeader"] as JImage);
    this.name = (jwindowDescription["name"] as JLabel).CreateGUILabelNew(this.root);
    this.distance = (jwindowDescription["dist"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.background);
    this.AddPanel((GUIPanel) this.redStrip);
    this.AddPanel((GUIPanel) this.blueStrip);
    this.AddPanel((GUIPanel) this.healthLevel);
    this.AddPanel((GUIPanel) this.energyLevel);
    this.AddPanel((GUIPanel) this.avatar);
    this.AddPanel((GUIPanel) this.leaderMark);
    this.AddPanel((GUIPanel) this.name);
    this.AddPanel((GUIPanel) this.distance);
    this.IsRendered = true;
  }

  protected void SetHealthString(int current, int maximum)
  {
    this.healthLevel.Text = string.Format("{0} / {1}", (object) current, (object) maximum);
    this.healthLevel.MakeCenteredRect();
  }

  protected void SetHealthString(string healthString)
  {
    this.healthLevel.Text = healthString;
    this.healthLevel.MakeCenteredRect();
  }

  protected void SetEnergyString(int current, int maximum)
  {
    this.energyLevel.Text = string.Format("{0} / {1}", (object) current, (object) maximum);
    this.energyLevel.MakeCenteredRect();
  }

  protected void SetEnergyString(string energyString)
  {
    this.energyLevel.Text = energyString;
    this.energyLevel.MakeCenteredRect();
  }

  public override bool Contains(float2 point)
  {
    if (!this.name.Contains(point))
      return this.background.Contains(point);
    return true;
  }

  public override void Update()
  {
    base.Update();
    this.UpdateGUISlotAvatar(this.avatar);
    this.avatar.Update();
  }

  protected virtual void UpdateGUISlotAvatar(GUISlotAvatar avatar)
  {
    if (Game.Me.ActualShip == null)
      return;
    avatar.Texture = Game.Me.ActualShip.Avatar;
    avatar.IsRendered = true;
  }

  public override void Draw()
  {
    base.Draw();
    if (this.background.IsRendered)
      this.background.Draw();
    if (this.avatar.IsRendered)
      this.avatar.Draw();
    if (this.leaderMark.IsRendered)
      this.leaderMark.Draw();
    if (this.redStrip.IsRendered)
    {
      this.redStrip.Draw();
      if (this.healthLevel.IsRendered)
        this.healthLevel.Draw();
    }
    if (this.blueStrip.IsRendered)
    {
      this.blueStrip.Draw();
      if (this.energyLevel.IsRendered)
        this.energyLevel.Draw();
    }
    if (!this.name.IsRendered)
      return;
    this.name.Draw();
  }
}
