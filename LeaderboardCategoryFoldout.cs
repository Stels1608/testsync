﻿// Decompiled with JetBrains decompiler
// Type: LeaderboardCategoryFoldout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LeaderboardCategoryFoldout : LeaderboardToggle
{
  private static UnityEngine.Sprite selectedGfx;
  public Text categoryNameLabel;
  public GameObject foldout;
  private bool tournament;
  private LeaderboardSubcategoryButton[] subcategories;

  public string Caption
  {
    set
    {
      this.categoryNameLabel.text = value.ToUpperInvariant();
    }
  }

  public static void LoadIcons()
  {
    LeaderboardCategoryFoldout.selectedGfx = Resources.Load<UnityEngine.Sprite>("GUI/Leaderboard/side_menu_selected");
    LeaderboardSubcategoryButton.LoadIcons();
  }

  protected override void OnAwake()
  {
    this.NotSelectedGfx = this.image.sprite;
    this.SelectedGfx = LeaderboardCategoryFoldout.selectedGfx;
  }

  protected override void OnSelectedChanged(bool newState)
  {
    base.OnSelectedChanged(newState);
    this.label.color = !newState ? Color.white : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
    this.foldout.SetActive(newState);
    if (!newState)
      return;
    this.subcategories[!this.tournament ? 1 : 0].Trigger();
  }

  public void SetSubCategories(TournamentRankingGroup group, TournamentRankingType[] subCategories, LeaderboardCategoryFoldout.OnTournamentRankingTypeSelected callback, List<LeaderboardToggle> toggleGroup)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    LeaderboardCategoryFoldout.\u003CSetSubCategories\u003Ec__AnonStoreyA0 categoriesCAnonStoreyA0 = new LeaderboardCategoryFoldout.\u003CSetSubCategories\u003Ec__AnonStoreyA0();
    // ISSUE: reference to a compiler-generated field
    categoriesCAnonStoreyA0.callback = callback;
    // ISSUE: reference to a compiler-generated field
    categoriesCAnonStoreyA0.group = group;
    this.tournament = true;
    UnityAction[] callbacks = new UnityAction[subCategories.Length];
    string[] texts = new string[subCategories.Length];
    for (int index = 0; index < subCategories.Length; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      LeaderboardCategoryFoldout.\u003CSetSubCategories\u003Ec__AnonStoreyA1 categoriesCAnonStoreyA1 = new LeaderboardCategoryFoldout.\u003CSetSubCategories\u003Ec__AnonStoreyA1();
      // ISSUE: reference to a compiler-generated field
      categoriesCAnonStoreyA1.\u003C\u003Ef__ref\u0024160 = categoriesCAnonStoreyA0;
      // ISSUE: reference to a compiler-generated field
      categoriesCAnonStoreyA1.rankingType = subCategories[index];
      // ISSUE: reference to a compiler-generated method
      callbacks[index] = new UnityAction(categoriesCAnonStoreyA1.\u003C\u003Em__14E);
      // ISSUE: reference to a compiler-generated field
      texts[index] = LeaderboardView.GetTextForTournamentRankingType(categoriesCAnonStoreyA1.rankingType);
    }
    this.SetSubCategories(texts, callbacks, toggleGroup);
  }

  public void SetSubCategories(RankingGroup group, RankingType[] subCategories, LeaderboardCategoryFoldout.OnRankingTypeSelected callback, List<LeaderboardToggle> toggleGroup)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    LeaderboardCategoryFoldout.\u003CSetSubCategories\u003Ec__AnonStoreyA2 categoriesCAnonStoreyA2 = new LeaderboardCategoryFoldout.\u003CSetSubCategories\u003Ec__AnonStoreyA2();
    // ISSUE: reference to a compiler-generated field
    categoriesCAnonStoreyA2.callback = callback;
    // ISSUE: reference to a compiler-generated field
    categoriesCAnonStoreyA2.group = group;
    this.tournament = false;
    UnityAction[] callbacks = new UnityAction[subCategories.Length];
    string[] texts = new string[subCategories.Length];
    for (int index = 0; index < subCategories.Length; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      LeaderboardCategoryFoldout.\u003CSetSubCategories\u003Ec__AnonStoreyA3 categoriesCAnonStoreyA3 = new LeaderboardCategoryFoldout.\u003CSetSubCategories\u003Ec__AnonStoreyA3();
      // ISSUE: reference to a compiler-generated field
      categoriesCAnonStoreyA3.\u003C\u003Ef__ref\u0024162 = categoriesCAnonStoreyA2;
      // ISSUE: reference to a compiler-generated field
      categoriesCAnonStoreyA3.rankingType = subCategories[index];
      // ISSUE: reference to a compiler-generated method
      callbacks[index] = new UnityAction(categoriesCAnonStoreyA3.\u003C\u003Em__14F);
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      texts[index] = LeaderboardView.GetTextForRankingType(categoriesCAnonStoreyA3.rankingType, categoriesCAnonStoreyA2.group);
    }
    this.SetSubCategories(texts, callbacks, toggleGroup);
  }

  private void SetSubCategories(string[] texts, UnityAction[] callbacks, List<LeaderboardToggle> toggleGroup)
  {
    GameObject gameObject1 = this.foldout.transform.GetChild(0).gameObject;
    List<GameObject> gameObjectList = new List<GameObject>();
    gameObjectList.Add(gameObject1);
    for (int index = 1; index < texts.Length; ++index)
    {
      GameObject gameObject2 = Object.Instantiate<GameObject>(gameObject1);
      gameObject2.transform.SetParent(gameObject1.transform.parent);
      gameObjectList.Add(gameObject2);
    }
    this.subcategories = new LeaderboardSubcategoryButton[gameObjectList.Count];
    for (int index = 0; index < gameObjectList.Count; ++index)
    {
      GameObject gameObject2 = gameObjectList[index];
      LeaderboardSubcategoryButton component = gameObject2.GetComponent<LeaderboardSubcategoryButton>();
      this.subcategories[index] = component;
      gameObject2.GetComponent<Button>().onClick.AddListener(callbacks[index]);
      component.SetText(texts[index]);
      component.ToggleGroup = toggleGroup;
      toggleGroup.Add((LeaderboardToggle) component);
    }
  }

  public delegate void OnRankingTypeSelected(RankingGroup group, RankingType rankingType);

  public delegate void OnTournamentRankingTypeSelected(TournamentRankingGroup group, TournamentRankingType rankingType);
}
