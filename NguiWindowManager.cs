﻿// Decompiled with JetBrains decompiler
// Type: NguiWindowManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class NguiWindowManager
{
  private const int DEPTH = 10;

  public NguiMvcRoot MvcRoot { get; protected set; }

  protected List<ContentWindowWidget> ContentWindows { get; set; }

  public ContentWindowWidget FocusedWindow { get; protected set; }

  public DialogBoxWidget DialogBox { get; set; }

  public NguiWindowManager()
  {
    this.ContentWindows = new List<ContentWindowWidget>();
  }

  public void BindMvcRoot(NguiMvcRoot root)
  {
    this.MvcRoot = root;
  }

  public void RegisterContentWindow(ContentWindowWidget window)
  {
    if (!this.ContentWindows.Contains(window))
    {
      this.ContentWindows.Add(window);
      window.gameObject.SetActive(true);
      this.RefreshContentWindowDepths();
    }
    if (this.ContentWindows.Count <= 0 || this.ContentWindows.Count >= 2)
      return;
    this.MvcRoot.Facade.SendMessage(Message.ToggleOldUi, (object) false);
  }

  public void UnregisterContentWindow(ContentWindowWidget window)
  {
    if (this.ContentWindows.Contains(window))
    {
      this.ContentWindows.Remove(window);
      window.gameObject.SetActive(false);
      this.RefreshContentWindowDepths();
    }
    if (this.ContentWindows.Count > 0)
      return;
    this.MvcRoot.Facade.SendMessage(Message.ToggleOldUi, (object) true);
  }

  public void BringContentWindowToFront(ContentWindowWidget window)
  {
    if (!this.ContentWindows.Contains(window) || window.WindowDepth / 10 >= this.ContentWindows.Count - 1)
      return;
    this.ContentWindows.Remove(window);
    this.ContentWindows.Add(window);
    this.RefreshContentWindowDepths();
  }

  public T GetWindowWidget<T>() where T : ContentWindowWidget
  {
    using (List<ContentWindowWidget>.Enumerator enumerator = this.ContentWindows.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ContentWindowWidget current = enumerator.Current;
        if (current is T)
          return (T) current;
      }
    }
    return (T) null;
  }

  public ContentWindowWidget GetContentWindowByType(WindowTypes types)
  {
    using (List<ContentWindowWidget>.Enumerator enumerator = this.ContentWindows.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ContentWindowWidget current = enumerator.Current;
        if (current.type == types)
          return current;
      }
    }
    return (ContentWindowWidget) null;
  }

  public bool WindowHasFocus(ContentWindowWidget window)
  {
    return (Object) window == (Object) this.FocusedWindow;
  }

  public int GetContentWindowAmount()
  {
    if (this.ContentWindows == null)
      return 0;
    return this.ContentWindows.Count;
  }

  public void SetFocus(ContentWindowWidget window)
  {
    if (!((Object) this.FocusedWindow != (Object) window))
      return;
    if ((Object) this.FocusedWindow != (Object) null)
      this.FocusedWindow.OnBlur();
    this.FocusedWindow = window;
    if (!((Object) window != (Object) null) || !this.ContentWindows.Contains(window))
      return;
    window.OnFocus();
  }

  public void RefreshContentWindowDepths()
  {
    this.SetFocus(this.ContentWindows.Count <= 0 ? (ContentWindowWidget) null : this.ContentWindows[this.ContentWindows.Count - 1]);
    UIPanel component = this.DialogBox.GetComponent<UIPanel>();
    if (!((Object) component != (Object) null))
      return;
    component.depth = 100;
  }

  public void RegisterDialogBox(DialogBoxWidget dialogBox)
  {
    this.DialogBox = dialogBox;
  }

  public void ShowDialogBox(DialogBoxData content)
  {
    if (this.ContentWindows.Count == 0)
      FacadeFactory.GetInstance().SendMessage(Message.ToggleOldUi, (object) false);
    FacadeFactory.GetInstance().SendMessage(Message.ToggleInputActive, (object) false);
    this.DialogBox.YPadding = content.yPadding;
    this.DialogBox.AddContent(content.contentData, content.hideDialogBoxBackgrounds);
    this.DialogBox.Open();
  }

  public void HideDialogBox()
  {
    this.DialogBox.Close();
    if (this.ContentWindows.Count == 0)
      FacadeFactory.GetInstance().SendMessage(Message.ToggleOldUi, (object) true);
    FacadeFactory.GetInstance().SendMessage(Message.ToggleInputActive, (object) true);
  }

  public bool IsDrawn()
  {
    if (this.ContentWindows.Count <= 0)
      return this.DialogBox.IsOpen;
    return true;
  }
}
