﻿// Decompiled with JetBrains decompiler
// Type: UILabel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/UI/NGUI Label")]
public class UILabel : UIWidget
{
  private static BetterList<UILabel> mList = new BetterList<UILabel>();
  private static Dictionary<Font, int> mFontUsage = new Dictionary<Font, int>();
  private static BetterList<Vector3> mTempVerts = new BetterList<Vector3>();
  private static BetterList<int> mTempIndices = new BetterList<int>();
  public UILabel.Crispness keepCrispWhenShrunk = UILabel.Crispness.OnDesktop;
  [HideInInspector]
  [Multiline(6)]
  [SerializeField]
  private string mText = string.Empty;
  [SerializeField]
  [HideInInspector]
  private int mFontSize = 16;
  [SerializeField]
  [HideInInspector]
  private bool mEncoding = true;
  [HideInInspector]
  [SerializeField]
  private Color mEffectColor = Color.black;
  [HideInInspector]
  [SerializeField]
  private NGUIText.SymbolStyle mSymbols = NGUIText.SymbolStyle.Normal;
  [HideInInspector]
  [SerializeField]
  private Vector2 mEffectDistance = Vector2.one;
  [HideInInspector]
  [SerializeField]
  private Color mGradientTop = Color.white;
  [SerializeField]
  [HideInInspector]
  private Color mGradientBottom = new Color(0.7f, 0.7f, 0.7f);
  [SerializeField]
  [HideInInspector]
  private bool mMultiline = true;
  private float mDensity = 1f;
  private bool mShouldBeProcessed = true;
  private Vector2 mCalculatedSize = Vector2.zero;
  private float mScale = 1f;
  [SerializeField]
  [HideInInspector]
  private Font mTrueTypeFont;
  [HideInInspector]
  [SerializeField]
  private UIFont mFont;
  [SerializeField]
  [HideInInspector]
  private FontStyle mFontStyle;
  [SerializeField]
  [HideInInspector]
  private NGUIText.Alignment mAlignment;
  [HideInInspector]
  [SerializeField]
  private int mMaxLineCount;
  [HideInInspector]
  [SerializeField]
  private UILabel.Effect mEffectStyle;
  [SerializeField]
  [HideInInspector]
  private UILabel.Overflow mOverflow;
  [SerializeField]
  [HideInInspector]
  private Material mMaterial;
  [HideInInspector]
  [SerializeField]
  private bool mApplyGradient;
  [HideInInspector]
  [SerializeField]
  private int mSpacingX;
  [HideInInspector]
  [SerializeField]
  private int mSpacingY;
  [SerializeField]
  [HideInInspector]
  private bool mShrinkToFit;
  [SerializeField]
  [HideInInspector]
  private int mMaxLineWidth;
  [SerializeField]
  [HideInInspector]
  private int mMaxLineHeight;
  [HideInInspector]
  [SerializeField]
  private float mLineWidth;
  [NonSerialized]
  private Font mActiveTTF;
  private string mProcessedText;
  private bool mPremultiply;
  private int mPrintedSize;
  private int mLastWidth;
  private int mLastHeight;

  private bool shouldBeProcessed
  {
    get
    {
      return this.mShouldBeProcessed;
    }
    set
    {
      if (value)
      {
        this.mChanged = true;
        this.mShouldBeProcessed = true;
      }
      else
        this.mShouldBeProcessed = false;
    }
  }

  public override bool isAnchoredHorizontally
  {
    get
    {
      if (!base.isAnchoredHorizontally)
        return this.mOverflow == UILabel.Overflow.ResizeFreely;
      return true;
    }
  }

  public override bool isAnchoredVertically
  {
    get
    {
      if (!base.isAnchoredVertically && this.mOverflow != UILabel.Overflow.ResizeFreely)
        return this.mOverflow == UILabel.Overflow.ResizeHeight;
      return true;
    }
  }

  public override Material material
  {
    get
    {
      if ((UnityEngine.Object) this.mMaterial != (UnityEngine.Object) null)
        return this.mMaterial;
      if ((UnityEngine.Object) this.mFont != (UnityEngine.Object) null)
        return this.mFont.material;
      if ((UnityEngine.Object) this.mTrueTypeFont != (UnityEngine.Object) null)
        return this.mTrueTypeFont.material;
      return (Material) null;
    }
    set
    {
      if (!((UnityEngine.Object) this.mMaterial != (UnityEngine.Object) value))
        return;
      this.MarkAsChanged();
      this.mMaterial = value;
      this.MarkAsChanged();
    }
  }

  [Obsolete("Use UILabel.bitmapFont instead")]
  public UIFont font
  {
    get
    {
      return this.bitmapFont;
    }
    set
    {
      this.bitmapFont = value;
    }
  }

  public UIFont bitmapFont
  {
    get
    {
      return this.mFont;
    }
    set
    {
      if (!((UnityEngine.Object) this.mFont != (UnityEngine.Object) value))
        return;
      this.RemoveFromPanel();
      this.mFont = value;
      this.mTrueTypeFont = (Font) null;
      this.MarkAsChanged();
    }
  }

  public Font trueTypeFont
  {
    get
    {
      if ((UnityEngine.Object) this.mTrueTypeFont != (UnityEngine.Object) null)
        return this.mTrueTypeFont;
      if ((UnityEngine.Object) this.mFont != (UnityEngine.Object) null)
        return this.mFont.dynamicFont;
      return (Font) null;
    }
    set
    {
      if (!((UnityEngine.Object) this.mTrueTypeFont != (UnityEngine.Object) value))
        return;
      this.SetActiveFont((Font) null);
      this.RemoveFromPanel();
      this.mTrueTypeFont = value;
      this.shouldBeProcessed = true;
      this.mFont = (UIFont) null;
      this.SetActiveFont(value);
      this.ProcessAndRequest();
      if (!((UnityEngine.Object) this.mActiveTTF != (UnityEngine.Object) null))
        return;
      base.MarkAsChanged();
    }
  }

  public UnityEngine.Object ambigiousFont
  {
    get
    {
      if ((UnityEngine.Object) this.mFont != (UnityEngine.Object) null)
        return (UnityEngine.Object) this.mFont;
      return (UnityEngine.Object) this.mTrueTypeFont;
    }
    set
    {
      UIFont uiFont = value as UIFont;
      if ((UnityEngine.Object) uiFont != (UnityEngine.Object) null)
        this.bitmapFont = uiFont;
      else
        this.trueTypeFont = value as Font;
    }
  }

  public string text
  {
    get
    {
      return this.mText;
    }
    set
    {
      if (this.mText == value)
        return;
      if (string.IsNullOrEmpty(value))
      {
        if (!string.IsNullOrEmpty(this.mText))
        {
          this.mText = string.Empty;
          this.MarkAsChanged();
          this.ProcessAndRequest();
        }
      }
      else if (this.mText != value)
      {
        this.mText = value;
        this.MarkAsChanged();
        this.ProcessAndRequest();
      }
      if (!this.autoResizeBoxCollider)
        return;
      this.ResizeCollider();
    }
  }

  public int defaultFontSize
  {
    get
    {
      if ((UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null)
        return this.mFontSize;
      if ((UnityEngine.Object) this.mFont != (UnityEngine.Object) null)
        return this.mFont.defaultSize;
      return 16;
    }
  }

  public int fontSize
  {
    get
    {
      return this.mFontSize;
    }
    set
    {
      value = Mathf.Clamp(value, 0, 256);
      if (this.mFontSize == value)
        return;
      this.mFontSize = value;
      this.shouldBeProcessed = true;
      this.ProcessAndRequest();
    }
  }

  public FontStyle fontStyle
  {
    get
    {
      return this.mFontStyle;
    }
    set
    {
      if (this.mFontStyle == value)
        return;
      this.mFontStyle = value;
      this.shouldBeProcessed = true;
      this.ProcessAndRequest();
    }
  }

  public NGUIText.Alignment alignment
  {
    get
    {
      return this.mAlignment;
    }
    set
    {
      if (this.mAlignment == value)
        return;
      this.mAlignment = value;
      this.shouldBeProcessed = true;
      this.ProcessAndRequest();
    }
  }

  public bool applyGradient
  {
    get
    {
      return this.mApplyGradient;
    }
    set
    {
      if (this.mApplyGradient == value)
        return;
      this.mApplyGradient = value;
      this.MarkAsChanged();
    }
  }

  public Color gradientTop
  {
    get
    {
      return this.mGradientTop;
    }
    set
    {
      if (!(this.mGradientTop != value))
        return;
      this.mGradientTop = value;
      if (!this.mApplyGradient)
        return;
      this.MarkAsChanged();
    }
  }

  public Color gradientBottom
  {
    get
    {
      return this.mGradientBottom;
    }
    set
    {
      if (!(this.mGradientBottom != value))
        return;
      this.mGradientBottom = value;
      if (!this.mApplyGradient)
        return;
      this.MarkAsChanged();
    }
  }

  public int spacingX
  {
    get
    {
      return this.mSpacingX;
    }
    set
    {
      if (this.mSpacingX == value)
        return;
      this.mSpacingX = value;
      this.MarkAsChanged();
    }
  }

  public int spacingY
  {
    get
    {
      return this.mSpacingY;
    }
    set
    {
      if (this.mSpacingY == value)
        return;
      this.mSpacingY = value;
      this.MarkAsChanged();
    }
  }

  private bool keepCrisp
  {
    get
    {
      return (UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null && this.keepCrispWhenShrunk != UILabel.Crispness.Never;
    }
  }

  public bool supportEncoding
  {
    get
    {
      return this.mEncoding;
    }
    set
    {
      if (this.mEncoding == value)
        return;
      this.mEncoding = value;
      this.shouldBeProcessed = true;
    }
  }

  public NGUIText.SymbolStyle symbolStyle
  {
    get
    {
      return this.mSymbols;
    }
    set
    {
      if (this.mSymbols == value)
        return;
      this.mSymbols = value;
      this.shouldBeProcessed = true;
    }
  }

  public UILabel.Overflow overflowMethod
  {
    get
    {
      return this.mOverflow;
    }
    set
    {
      if (this.mOverflow == value)
        return;
      this.mOverflow = value;
      this.shouldBeProcessed = true;
    }
  }

  [Obsolete("Use 'width' instead")]
  public int lineWidth
  {
    get
    {
      return this.width;
    }
    set
    {
      this.width = value;
    }
  }

  [Obsolete("Use 'height' instead")]
  public int lineHeight
  {
    get
    {
      return this.height;
    }
    set
    {
      this.height = value;
    }
  }

  public bool multiLine
  {
    get
    {
      return this.mMaxLineCount != 1;
    }
    set
    {
      if (this.mMaxLineCount != 1 == value)
        return;
      this.mMaxLineCount = !value ? 1 : 0;
      this.shouldBeProcessed = true;
    }
  }

  public override Vector3[] localCorners
  {
    get
    {
      if (this.shouldBeProcessed)
        this.ProcessText();
      return base.localCorners;
    }
  }

  public override Vector3[] worldCorners
  {
    get
    {
      if (this.shouldBeProcessed)
        this.ProcessText();
      return base.worldCorners;
    }
  }

  public override Vector4 drawingDimensions
  {
    get
    {
      if (this.shouldBeProcessed)
        this.ProcessText();
      return base.drawingDimensions;
    }
  }

  public int maxLineCount
  {
    get
    {
      return this.mMaxLineCount;
    }
    set
    {
      if (this.mMaxLineCount == value)
        return;
      this.mMaxLineCount = Mathf.Max(value, 0);
      this.shouldBeProcessed = true;
      if (this.overflowMethod != UILabel.Overflow.ShrinkContent)
        return;
      this.MakePixelPerfect();
    }
  }

  public UILabel.Effect effectStyle
  {
    get
    {
      return this.mEffectStyle;
    }
    set
    {
      if (this.mEffectStyle == value)
        return;
      this.mEffectStyle = value;
      this.shouldBeProcessed = true;
    }
  }

  public Color effectColor
  {
    get
    {
      return this.mEffectColor;
    }
    set
    {
      if (!(this.mEffectColor != value))
        return;
      this.mEffectColor = value;
      if (this.mEffectStyle == UILabel.Effect.None)
        return;
      this.shouldBeProcessed = true;
    }
  }

  public Vector2 effectDistance
  {
    get
    {
      return this.mEffectDistance;
    }
    set
    {
      if (!(this.mEffectDistance != value))
        return;
      this.mEffectDistance = value;
      this.shouldBeProcessed = true;
    }
  }

  [Obsolete("Use 'overflowMethod == UILabel.Overflow.ShrinkContent' instead")]
  public bool shrinkToFit
  {
    get
    {
      return this.mOverflow == UILabel.Overflow.ShrinkContent;
    }
    set
    {
      if (!value)
        return;
      this.overflowMethod = UILabel.Overflow.ShrinkContent;
    }
  }

  public string processedText
  {
    get
    {
      if (this.mLastWidth != this.mWidth || this.mLastHeight != this.mHeight)
      {
        this.mLastWidth = this.mWidth;
        this.mLastHeight = this.mHeight;
        this.mShouldBeProcessed = true;
      }
      if (this.shouldBeProcessed)
        this.ProcessText();
      return this.mProcessedText;
    }
  }

  public Vector2 printedSize
  {
    get
    {
      if (this.shouldBeProcessed)
        this.ProcessText();
      return this.mCalculatedSize;
    }
  }

  public override Vector2 localSize
  {
    get
    {
      if (this.shouldBeProcessed)
        this.ProcessText();
      return base.localSize;
    }
  }

  private bool isValid
  {
    get
    {
      if (!((UnityEngine.Object) this.mFont != (UnityEngine.Object) null))
        return (UnityEngine.Object) this.mTrueTypeFont != (UnityEngine.Object) null;
      return true;
    }
  }

  protected override void OnInit()
  {
    base.OnInit();
    UILabel.mList.Add(this);
    this.SetActiveFont(this.trueTypeFont);
  }

  protected override void OnDisable()
  {
    this.SetActiveFont((Font) null);
    UILabel.mList.Remove(this);
    base.OnDisable();
  }

  protected void SetActiveFont(Font fnt)
  {
    if (!((UnityEngine.Object) this.mActiveTTF != (UnityEngine.Object) fnt))
      return;
    if ((UnityEngine.Object) this.mActiveTTF != (UnityEngine.Object) null)
    {
      int num1;
      if (UILabel.mFontUsage.TryGetValue(this.mActiveTTF, out num1))
      {
        int num2;
        int num3 = Mathf.Max(0, num2 = num1 - 1);
        if (num3 == 0)
        {
          this.mActiveTTF.textureRebuildCallback = (Font.FontTextureRebuildCallback) null;
          UILabel.mFontUsage.Remove(this.mActiveTTF);
        }
        else
          UILabel.mFontUsage[this.mActiveTTF] = num3;
      }
      else
        this.mActiveTTF.textureRebuildCallback = (Font.FontTextureRebuildCallback) null;
    }
    this.mActiveTTF = fnt;
    if (!((UnityEngine.Object) this.mActiveTTF != (UnityEngine.Object) null))
      return;
    int num = 0;
    if (!UILabel.mFontUsage.TryGetValue(this.mActiveTTF, out num))
      this.mActiveTTF.textureRebuildCallback = new Font.FontTextureRebuildCallback(UILabel.OnFontTextureChanged);
    UILabel.mFontUsage[this.mActiveTTF] = ++num;
  }

  private static void OnFontTextureChanged()
  {
    for (int index = 0; index < UILabel.mList.size; ++index)
    {
      UILabel uiLabel = UILabel.mList[index];
      if ((UnityEngine.Object) uiLabel != (UnityEngine.Object) null)
      {
        Font trueTypeFont = uiLabel.trueTypeFont;
        if ((UnityEngine.Object) trueTypeFont != (UnityEngine.Object) null)
          trueTypeFont.RequestCharactersInTexture(uiLabel.mText, uiLabel.mPrintedSize, uiLabel.mFontStyle);
      }
    }
    for (int index = 0; index < UILabel.mList.size; ++index)
    {
      UILabel uiLabel = UILabel.mList[index];
      if ((UnityEngine.Object) uiLabel != (UnityEngine.Object) null && (UnityEngine.Object) uiLabel.trueTypeFont != (UnityEngine.Object) null)
      {
        uiLabel.RemoveFromPanel();
        uiLabel.CreatePanel();
      }
    }
  }

  public override Vector3[] GetSides(Transform relativeTo)
  {
    if (this.shouldBeProcessed)
      this.ProcessText();
    return base.GetSides(relativeTo);
  }

  protected override void UpgradeFrom265()
  {
    this.ProcessText(true, true);
    if (this.mShrinkToFit)
    {
      this.overflowMethod = UILabel.Overflow.ShrinkContent;
      this.mMaxLineCount = 0;
    }
    if (this.mMaxLineWidth != 0)
    {
      this.width = this.mMaxLineWidth;
      this.overflowMethod = this.mMaxLineCount <= 0 ? UILabel.Overflow.ShrinkContent : UILabel.Overflow.ResizeHeight;
    }
    else
      this.overflowMethod = UILabel.Overflow.ResizeFreely;
    if (this.mMaxLineHeight != 0)
      this.height = this.mMaxLineHeight;
    if ((UnityEngine.Object) this.mFont != (UnityEngine.Object) null)
    {
      int defaultSize = this.mFont.defaultSize;
      if (this.height < defaultSize)
        this.height = defaultSize;
      this.fontSize = defaultSize;
    }
    this.mMaxLineWidth = 0;
    this.mMaxLineHeight = 0;
    this.mShrinkToFit = false;
    NGUITools.UpdateWidgetCollider(this.gameObject, true);
  }

  protected override void OnAnchor()
  {
    if (this.mOverflow == UILabel.Overflow.ResizeFreely)
    {
      if (this.isFullyAnchored)
        this.mOverflow = UILabel.Overflow.ShrinkContent;
    }
    else if (this.mOverflow == UILabel.Overflow.ResizeHeight && (UnityEngine.Object) this.topAnchor.target != (UnityEngine.Object) null && (UnityEngine.Object) this.bottomAnchor.target != (UnityEngine.Object) null)
      this.mOverflow = UILabel.Overflow.ShrinkContent;
    base.OnAnchor();
  }

  private void ProcessAndRequest()
  {
    if (!(this.ambigiousFont != (UnityEngine.Object) null))
      return;
    this.ProcessText();
  }

  protected override void OnStart()
  {
    base.OnStart();
    if ((double) this.mLineWidth > 0.0)
    {
      this.mMaxLineWidth = Mathf.RoundToInt(this.mLineWidth);
      this.mLineWidth = 0.0f;
    }
    if (!this.mMultiline)
    {
      this.mMaxLineCount = 1;
      this.mMultiline = true;
    }
    this.mPremultiply = (UnityEngine.Object) this.material != (UnityEngine.Object) null && (UnityEngine.Object) this.material.shader != (UnityEngine.Object) null && this.material.shader.name.Contains("Premultiplied");
    this.ProcessAndRequest();
  }

  public override void MarkAsChanged()
  {
    this.shouldBeProcessed = true;
    base.MarkAsChanged();
  }

  public void ProcessText()
  {
    this.ProcessText(false, true);
  }

  private void ProcessText(bool legacyMode, bool full)
  {
    if (!this.isValid)
      return;
    this.mChanged = true;
    this.shouldBeProcessed = false;
    float num1 = this.mDrawRegion.z - this.mDrawRegion.x;
    float num2 = this.mDrawRegion.w - this.mDrawRegion.y;
    NGUIText.rectWidth = !legacyMode ? this.width : (this.mMaxLineWidth == 0 ? 1000000 : this.mMaxLineWidth);
    NGUIText.rectHeight = !legacyMode ? this.height : (this.mMaxLineHeight == 0 ? 1000000 : this.mMaxLineHeight);
    NGUIText.regionWidth = (double) num1 == 1.0 ? NGUIText.rectWidth : Mathf.RoundToInt((float) NGUIText.rectWidth * num1);
    NGUIText.regionHeight = (double) num2 == 1.0 ? NGUIText.rectHeight : Mathf.RoundToInt((float) NGUIText.rectHeight * num2);
    this.mPrintedSize = Mathf.Abs(!legacyMode ? this.defaultFontSize : Mathf.RoundToInt(this.cachedTransform.localScale.x));
    this.mScale = 1f;
    if (NGUIText.regionWidth < 1 || NGUIText.regionHeight < 0)
    {
      this.mProcessedText = string.Empty;
    }
    else
    {
      bool flag = (UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null;
      if (flag && this.keepCrisp)
      {
        UIRoot root = this.root;
        if ((UnityEngine.Object) root != (UnityEngine.Object) null)
          this.mDensity = !((UnityEngine.Object) root != (UnityEngine.Object) null) ? 1f : root.pixelSizeAdjustment;
      }
      else
        this.mDensity = 1f;
      if (full)
        this.UpdateNGUIText();
      if (this.mOverflow == UILabel.Overflow.ResizeFreely)
      {
        NGUIText.rectWidth = 1000000;
        NGUIText.regionWidth = 1000000;
      }
      if (this.mOverflow == UILabel.Overflow.ResizeFreely || this.mOverflow == UILabel.Overflow.ResizeHeight)
      {
        NGUIText.rectHeight = 1000000;
        NGUIText.regionHeight = 1000000;
      }
      if (this.mPrintedSize > 0)
      {
        bool keepCrisp = this.keepCrisp;
        int num3;
        for (int index = this.mPrintedSize; index > 0; index = num3 - 1)
        {
          if (keepCrisp)
          {
            this.mPrintedSize = index;
            NGUIText.fontSize = this.mPrintedSize;
          }
          else
          {
            this.mScale = (float) index / (float) this.mPrintedSize;
            NGUIText.fontScale = !flag ? (float) this.mFontSize / (float) this.mFont.defaultSize * this.mScale : this.mScale;
          }
          NGUIText.Update(false);
          if (this.mOverflow == UILabel.Overflow.ShrinkContent && !NGUIText.WrapText(this.mText, out this.mProcessedText, true))
          {
            if ((num3 = index - 1) <= 1)
              break;
          }
          else
          {
            if (this.mOverflow == UILabel.Overflow.ResizeFreely)
            {
              this.mCalculatedSize = NGUIText.CalculatePrintedSize(this.mProcessedText);
              this.mWidth = Mathf.Max(this.minWidth, Mathf.RoundToInt(this.mCalculatedSize.x));
              if ((double) num1 != 1.0)
                this.mWidth = Mathf.RoundToInt((float) this.mWidth / num1);
              this.mHeight = Mathf.Max(this.minHeight, Mathf.RoundToInt(this.mCalculatedSize.y));
              if ((double) num2 != 1.0)
                this.mHeight = Mathf.RoundToInt((float) this.mHeight / num2);
              if ((this.mWidth & 1) == 1)
                ++this.mWidth;
              if ((this.mHeight & 1) == 1)
                ++this.mHeight;
            }
            else if (this.mOverflow == UILabel.Overflow.ResizeHeight)
            {
              this.mCalculatedSize = NGUIText.CalculatePrintedSize(this.mProcessedText);
              this.mHeight = Mathf.Max(this.minHeight, Mathf.RoundToInt(this.mCalculatedSize.y));
              if ((double) num2 != 1.0)
                this.mHeight = Mathf.RoundToInt((float) this.mHeight / num2);
              if ((this.mHeight & 1) == 1)
                ++this.mHeight;
            }
            else
              this.mCalculatedSize = NGUIText.CalculatePrintedSize(this.mProcessedText);
            if (legacyMode)
            {
              this.width = Mathf.RoundToInt(this.mCalculatedSize.x);
              this.height = Mathf.RoundToInt(this.mCalculatedSize.y);
              this.cachedTransform.localScale = Vector3.one;
              break;
            }
            break;
          }
        }
      }
      else
      {
        this.cachedTransform.localScale = Vector3.one;
        this.mProcessedText = string.Empty;
        this.mScale = 1f;
      }
      if (!full)
        return;
      NGUIText.bitmapFont = (UIFont) null;
      NGUIText.dynamicFont = (Font) null;
    }
  }

  public override void MakePixelPerfect()
  {
    if (this.ambigiousFont != (UnityEngine.Object) null)
    {
      Vector3 localPosition = this.cachedTransform.localPosition;
      localPosition.x = (float) Mathf.RoundToInt(localPosition.x);
      localPosition.y = (float) Mathf.RoundToInt(localPosition.y);
      localPosition.z = (float) Mathf.RoundToInt(localPosition.z);
      this.cachedTransform.localPosition = localPosition;
      this.cachedTransform.localScale = Vector3.one;
      if (this.mOverflow == UILabel.Overflow.ResizeFreely)
      {
        this.AssumeNaturalSize();
      }
      else
      {
        int width = this.width;
        int height = this.height;
        UILabel.Overflow overflow = this.mOverflow;
        if (overflow != UILabel.Overflow.ResizeHeight)
          this.mWidth = 100000;
        this.mHeight = 100000;
        this.mOverflow = UILabel.Overflow.ShrinkContent;
        this.ProcessText(false, true);
        this.mOverflow = overflow;
        int int1 = Mathf.RoundToInt(this.mCalculatedSize.x);
        int int2 = Mathf.RoundToInt(this.mCalculatedSize.y);
        int b1 = Mathf.Max(int1, this.minWidth);
        int b2 = Mathf.Max(int2, this.minHeight);
        this.mWidth = Mathf.Max(width, b1);
        this.mHeight = Mathf.Max(height, b2);
        this.MarkAsChanged();
      }
    }
    else
      base.MakePixelPerfect();
  }

  public void AssumeNaturalSize()
  {
    if (!(this.ambigiousFont != (UnityEngine.Object) null))
      return;
    this.mWidth = 100000;
    this.mHeight = 100000;
    this.ProcessText(false, true);
    this.mWidth = Mathf.RoundToInt(this.mCalculatedSize.x);
    this.mHeight = Mathf.RoundToInt(this.mCalculatedSize.y);
    if ((this.mWidth & 1) == 1)
      ++this.mWidth;
    if ((this.mHeight & 1) == 1)
      ++this.mHeight;
    this.MarkAsChanged();
  }

  [Obsolete("Use UILabel.GetCharacterAtPosition instead")]
  public int GetCharacterIndex(Vector3 worldPos)
  {
    return this.GetCharacterIndexAtPosition(worldPos, false);
  }

  [Obsolete("Use UILabel.GetCharacterAtPosition instead")]
  public int GetCharacterIndex(Vector2 localPos)
  {
    return this.GetCharacterIndexAtPosition(localPos, false);
  }

  public int GetCharacterIndexAtPosition(Vector3 worldPos, bool precise)
  {
    return this.GetCharacterIndexAtPosition((Vector2) this.cachedTransform.InverseTransformPoint(worldPos), precise);
  }

  public int GetCharacterIndexAtPosition(Vector2 localPos, bool precise)
  {
    if (this.isValid)
    {
      string processedText = this.processedText;
      if (string.IsNullOrEmpty(processedText))
        return 0;
      this.UpdateNGUIText();
      if (precise)
        NGUIText.PrintExactCharacterPositions(processedText, UILabel.mTempVerts, UILabel.mTempIndices);
      else
        NGUIText.PrintApproximateCharacterPositions(processedText, UILabel.mTempVerts, UILabel.mTempIndices);
      if (UILabel.mTempVerts.size > 0)
      {
        this.ApplyOffset(UILabel.mTempVerts, 0);
        int num = !precise ? NGUIText.GetApproximateCharacterIndex(UILabel.mTempVerts, UILabel.mTempIndices, localPos) : NGUIText.GetExactCharacterIndex(UILabel.mTempVerts, UILabel.mTempIndices, localPos);
        UILabel.mTempVerts.Clear();
        UILabel.mTempIndices.Clear();
        NGUIText.bitmapFont = (UIFont) null;
        NGUIText.dynamicFont = (Font) null;
        return num;
      }
      NGUIText.bitmapFont = (UIFont) null;
      NGUIText.dynamicFont = (Font) null;
    }
    return 0;
  }

  public string GetWordAtPosition(Vector3 worldPos)
  {
    return this.GetWordAtCharacterIndex(this.GetCharacterIndexAtPosition(worldPos, true));
  }

  public string GetWordAtPosition(Vector2 localPos)
  {
    return this.GetWordAtCharacterIndex(this.GetCharacterIndexAtPosition(localPos, true));
  }

  public string GetWordAtCharacterIndex(int characterIndex)
  {
    if (characterIndex != -1 && characterIndex < this.mText.Length)
    {
      int startIndex = this.mText.LastIndexOfAny(new char[2]{ ' ', '\n' }, characterIndex) + 1;
      int num = this.mText.IndexOfAny(new char[4]{ ' ', '\n', ',', '.' }, characterIndex);
      if (num == -1)
        num = this.mText.Length;
      if (startIndex != num)
      {
        int length = num - startIndex;
        if (length > 0)
          return NGUIText.StripSymbols(this.mText.Substring(startIndex, length));
      }
    }
    return (string) null;
  }

  public string GetUrlAtPosition(Vector3 worldPos)
  {
    return this.GetUrlAtCharacterIndex(this.GetCharacterIndexAtPosition(worldPos, true));
  }

  public string GetUrlAtPosition(Vector2 localPos)
  {
    return this.GetUrlAtCharacterIndex(this.GetCharacterIndexAtPosition(localPos, true));
  }

  public string GetUrlAtCharacterIndex(int characterIndex)
  {
    if (characterIndex != -1 && characterIndex < this.mText.Length - 6)
    {
      int num1 = (int) this.mText[characterIndex] != 91 || (int) this.mText[characterIndex + 1] != 117 || ((int) this.mText[characterIndex + 2] != 114 || (int) this.mText[characterIndex + 3] != 108) || (int) this.mText[characterIndex + 4] != 61 ? this.mText.LastIndexOf("[url=", characterIndex) : characterIndex;
      if (num1 == -1)
        return (string) null;
      int startIndex1 = num1 + 5;
      int startIndex2 = this.mText.IndexOf("]", startIndex1);
      if (startIndex2 == -1)
        return (string) null;
      int num2 = this.mText.IndexOf("[/url]", startIndex2);
      if (num2 == -1 || characterIndex <= num2)
        return this.mText.Substring(startIndex1, startIndex2 - startIndex1);
    }
    return (string) null;
  }

  public int GetCharacterIndex(int currentIndex, KeyCode key)
  {
    if (this.isValid)
    {
      string processedText = this.processedText;
      if (string.IsNullOrEmpty(processedText))
        return 0;
      int defaultFontSize = this.defaultFontSize;
      this.UpdateNGUIText();
      NGUIText.PrintApproximateCharacterPositions(processedText, UILabel.mTempVerts, UILabel.mTempIndices);
      if (UILabel.mTempVerts.size > 0)
      {
        this.ApplyOffset(UILabel.mTempVerts, 0);
        for (int index = 0; index < UILabel.mTempIndices.size; ++index)
        {
          if (UILabel.mTempIndices[index] == currentIndex)
          {
            Vector2 pos = (Vector2) UILabel.mTempVerts[index];
            if (key == KeyCode.UpArrow)
              pos.y += (float) (defaultFontSize + this.spacingY);
            else if (key == KeyCode.DownArrow)
              pos.y -= (float) (defaultFontSize + this.spacingY);
            else if (key == KeyCode.Home)
              pos.x -= 1000f;
            else if (key == KeyCode.End)
              pos.x += 1000f;
            int approximateCharacterIndex = NGUIText.GetApproximateCharacterIndex(UILabel.mTempVerts, UILabel.mTempIndices, pos);
            if (approximateCharacterIndex != currentIndex)
            {
              UILabel.mTempVerts.Clear();
              UILabel.mTempIndices.Clear();
              return approximateCharacterIndex;
            }
            break;
          }
        }
        UILabel.mTempVerts.Clear();
        UILabel.mTempIndices.Clear();
      }
      NGUIText.bitmapFont = (UIFont) null;
      NGUIText.dynamicFont = (Font) null;
      if (key == KeyCode.UpArrow || key == KeyCode.Home)
        return 0;
      if (key == KeyCode.DownArrow || key == KeyCode.End)
        return processedText.Length;
    }
    return currentIndex;
  }

  public void PrintOverlay(int start, int end, UIGeometry caret, UIGeometry highlight, Color caretColor, Color highlightColor)
  {
    if (caret != null)
      caret.Clear();
    if (highlight != null)
      highlight.Clear();
    if (!this.isValid)
      return;
    string processedText = this.processedText;
    this.UpdateNGUIText();
    int start1 = caret.verts.size;
    Vector2 vector2 = new Vector2(0.5f, 0.5f);
    float num = this.finalAlpha;
    if (highlight != null && start != end)
    {
      int start2 = highlight.verts.size;
      NGUIText.PrintCaretAndSelection(processedText, start, end, caret.verts, highlight.verts);
      if (highlight.verts.size > start2)
      {
        this.ApplyOffset(highlight.verts, start2);
        Color32 color32 = (Color32) new Color(highlightColor.r, highlightColor.g, highlightColor.b, highlightColor.a * num);
        for (int index = start2; index < highlight.verts.size; ++index)
        {
          highlight.uvs.Add(vector2);
          highlight.cols.Add(color32);
        }
      }
    }
    else
      NGUIText.PrintCaretAndSelection(processedText, start, end, caret.verts, (BetterList<Vector3>) null);
    this.ApplyOffset(caret.verts, start1);
    Color32 color32_1 = (Color32) new Color(caretColor.r, caretColor.g, caretColor.b, caretColor.a * num);
    for (int index = start1; index < caret.verts.size; ++index)
    {
      caret.uvs.Add(vector2);
      caret.cols.Add(color32_1);
    }
    NGUIText.bitmapFont = (UIFont) null;
    NGUIText.dynamicFont = (Font) null;
  }

  public override void OnFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
    if (!this.isValid)
      return;
    int num = verts.size;
    Color c = this.color;
    c.a = this.finalAlpha;
    if ((UnityEngine.Object) this.mFont != (UnityEngine.Object) null && this.mFont.premultipliedAlphaShader)
      c = NGUITools.ApplyPMA(c);
    if (QualitySettings.activeColorSpace == ColorSpace.Linear)
    {
      c.r = Mathf.Pow(c.r, 2.2f);
      c.g = Mathf.Pow(c.g, 2.2f);
      c.b = Mathf.Pow(c.b, 2.2f);
    }
    string processedText = this.processedText;
    int start1 = verts.size;
    this.UpdateNGUIText();
    NGUIText.tint = c;
    NGUIText.Print(processedText, verts, uvs, cols);
    NGUIText.bitmapFont = (UIFont) null;
    NGUIText.dynamicFont = (Font) null;
    Vector2 vector2 = this.ApplyOffset(verts, start1);
    if ((UnityEngine.Object) this.mFont != (UnityEngine.Object) null && this.mFont.packedFontShader)
      return;
    if (this.effectStyle != UILabel.Effect.None)
    {
      int end1 = verts.size;
      vector2.x = this.mEffectDistance.x;
      vector2.y = this.mEffectDistance.y;
      this.ApplyShadow(verts, uvs, cols, num, end1, vector2.x, -vector2.y);
      if (this.effectStyle == UILabel.Effect.Outline)
      {
        int start2 = end1;
        int end2 = verts.size;
        this.ApplyShadow(verts, uvs, cols, start2, end2, -vector2.x, vector2.y);
        int start3 = end2;
        int end3 = verts.size;
        this.ApplyShadow(verts, uvs, cols, start3, end3, vector2.x, vector2.y);
        num = end3;
        int end4 = verts.size;
        this.ApplyShadow(verts, uvs, cols, num, end4, -vector2.x, -vector2.y);
      }
    }
    if (this.onPostFill == null)
      return;
    this.onPostFill((UIWidget) this, num, verts, uvs, cols);
  }

  public Vector2 ApplyOffset(BetterList<Vector3> verts, int start)
  {
    Vector2 pivotOffset = this.pivotOffset;
    float f1 = Mathf.Lerp(0.0f, (float) -this.mWidth, pivotOffset.x);
    float f2 = Mathf.Lerp((float) this.mHeight, 0.0f, pivotOffset.y) + Mathf.Lerp(this.mCalculatedSize.y - (float) this.mHeight, 0.0f, pivotOffset.y);
    float x = Mathf.Round(f1);
    float y = Mathf.Round(f2);
    for (int index = start; index < verts.size; ++index)
    {
      verts.buffer[index].x += x;
      verts.buffer[index].y += y;
    }
    return new Vector2(x, y);
  }

  public void ApplyShadow(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols, int start, int end, float x, float y)
  {
    Color c1 = this.mEffectColor;
    c1.a *= this.finalAlpha;
    Color32 color32_1 = (Color32) (!((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null) || !this.bitmapFont.premultipliedAlphaShader ? c1 : NGUITools.ApplyPMA(c1));
    for (int index = start; index < end; ++index)
    {
      verts.Add(verts.buffer[index]);
      uvs.Add(uvs.buffer[index]);
      cols.Add(cols.buffer[index]);
      Vector3 vector3 = verts.buffer[index];
      vector3.x += x;
      vector3.y += y;
      verts.buffer[index] = vector3;
      Color32 color32_2 = cols.buffer[index];
      if ((int) color32_2.a == (int) byte.MaxValue)
      {
        cols.buffer[index] = color32_1;
      }
      else
      {
        Color c2 = c1;
        c2.a = (float) color32_2.a / (float) byte.MaxValue * c1.a;
        cols.buffer[index] = (Color32) (!((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null) || !this.bitmapFont.premultipliedAlphaShader ? c2 : NGUITools.ApplyPMA(c2));
      }
    }
  }

  public int CalculateOffsetToFit(string text)
  {
    this.UpdateNGUIText();
    NGUIText.encoding = false;
    NGUIText.symbolStyle = NGUIText.SymbolStyle.None;
    int offsetToFit = NGUIText.CalculateOffsetToFit(text);
    NGUIText.bitmapFont = (UIFont) null;
    NGUIText.dynamicFont = (Font) null;
    return offsetToFit;
  }

  public void SetCurrentProgress()
  {
    if (!((UnityEngine.Object) UIProgressBar.current != (UnityEngine.Object) null))
      return;
    this.text = UIProgressBar.current.value.ToString("F");
  }

  public void SetCurrentPercent()
  {
    if (!((UnityEngine.Object) UIProgressBar.current != (UnityEngine.Object) null))
      return;
    this.text = Mathf.RoundToInt(UIProgressBar.current.value * 100f).ToString() + "%";
  }

  public void SetCurrentSelection()
  {
    if (!((UnityEngine.Object) UIPopupList.current != (UnityEngine.Object) null))
      return;
    this.text = !UIPopupList.current.isLocalized ? UIPopupList.current.value : Localization.Get(UIPopupList.current.value);
  }

  public bool Wrap(string text, out string final)
  {
    return this.Wrap(text, out final, 1000000);
  }

  public bool Wrap(string text, out string final, int height)
  {
    this.UpdateNGUIText();
    NGUIText.rectHeight = height;
    NGUIText.regionHeight = height;
    bool flag = NGUIText.WrapText(text, out final);
    NGUIText.bitmapFont = (UIFont) null;
    NGUIText.dynamicFont = (Font) null;
    return flag;
  }

  public void UpdateNGUIText()
  {
    Font trueTypeFont = this.trueTypeFont;
    bool flag = (UnityEngine.Object) trueTypeFont != (UnityEngine.Object) null;
    NGUIText.fontSize = this.mPrintedSize;
    NGUIText.fontStyle = this.mFontStyle;
    NGUIText.rectWidth = this.mWidth;
    NGUIText.rectHeight = this.mHeight;
    NGUIText.regionWidth = Mathf.RoundToInt((float) this.mWidth * (this.mDrawRegion.z - this.mDrawRegion.x));
    NGUIText.regionHeight = Mathf.RoundToInt((float) this.mHeight * (this.mDrawRegion.w - this.mDrawRegion.y));
    NGUIText.gradient = this.mApplyGradient && ((UnityEngine.Object) this.mFont == (UnityEngine.Object) null || !this.mFont.packedFontShader);
    NGUIText.gradientTop = this.mGradientTop;
    NGUIText.gradientBottom = this.mGradientBottom;
    NGUIText.encoding = this.mEncoding;
    NGUIText.premultiply = this.mPremultiply;
    NGUIText.symbolStyle = this.mSymbols;
    NGUIText.maxLines = this.mMaxLineCount;
    NGUIText.spacingX = (float) this.mSpacingX;
    NGUIText.spacingY = (float) this.mSpacingY;
    NGUIText.fontScale = !flag ? (float) this.mFontSize / (float) this.mFont.defaultSize * this.mScale : this.mScale;
    if ((UnityEngine.Object) this.mFont != (UnityEngine.Object) null)
    {
      NGUIText.bitmapFont = this.mFont;
      while (true)
      {
        UIFont replacement = NGUIText.bitmapFont.replacement;
        if (!((UnityEngine.Object) replacement == (UnityEngine.Object) null))
          NGUIText.bitmapFont = replacement;
        else
          break;
      }
      if (NGUIText.bitmapFont.isDynamic)
      {
        NGUIText.dynamicFont = NGUIText.bitmapFont.dynamicFont;
        NGUIText.bitmapFont = (UIFont) null;
      }
      else
        NGUIText.dynamicFont = (Font) null;
    }
    else
    {
      NGUIText.dynamicFont = trueTypeFont;
      NGUIText.bitmapFont = (UIFont) null;
    }
    if (flag && this.keepCrisp)
    {
      UIRoot root = this.root;
      if ((UnityEngine.Object) root != (UnityEngine.Object) null)
        NGUIText.pixelDensity = !((UnityEngine.Object) root != (UnityEngine.Object) null) ? 1f : root.pixelSizeAdjustment;
    }
    else
      NGUIText.pixelDensity = 1f;
    if ((double) this.mDensity != (double) NGUIText.pixelDensity)
    {
      this.ProcessText(false, false);
      NGUIText.rectWidth = this.mWidth;
      NGUIText.rectHeight = this.mHeight;
      NGUIText.regionWidth = Mathf.RoundToInt((float) this.mWidth * (this.mDrawRegion.z - this.mDrawRegion.x));
      NGUIText.regionHeight = Mathf.RoundToInt((float) this.mHeight * (this.mDrawRegion.w - this.mDrawRegion.y));
    }
    if (this.alignment == NGUIText.Alignment.Automatic)
    {
      switch (this.pivot)
      {
        case UIWidget.Pivot.Left:
        case UIWidget.Pivot.TopLeft:
        case UIWidget.Pivot.BottomLeft:
          NGUIText.alignment = NGUIText.Alignment.Left;
          break;
        case UIWidget.Pivot.Right:
        case UIWidget.Pivot.TopRight:
        case UIWidget.Pivot.BottomRight:
          NGUIText.alignment = NGUIText.Alignment.Right;
          break;
        default:
          NGUIText.alignment = NGUIText.Alignment.Center;
          break;
      }
    }
    else
      NGUIText.alignment = this.alignment;
    NGUIText.Update();
  }

  public enum Effect
  {
    None,
    Shadow,
    Outline,
  }

  public enum Overflow
  {
    ShrinkContent,
    ClampContent,
    ResizeFreely,
    ResizeHeight,
  }

  public enum Crispness
  {
    Never,
    OnDesktop,
    Always,
  }
}
