﻿// Decompiled with JetBrains decompiler
// Type: CameraFxMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class CameraFxMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "CameraFxMediator";

  public CameraFxMediator()
    : base("CameraFxMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.SettingChangedStarDust);
    this.AddMessageInterest(Message.SettingChangedStarFog);
    this.AddMessageInterest(Message.SettingChangedGlowEffect);
    this.AddMessageInterest(Message.LevelLoaded);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.SettingChangedStarDust:
        this.ToggleStarDust((bool) message.Data);
        break;
      case Message.SettingChangedStarFog:
        this.ToggleStarFog((bool) message.Data);
        break;
      case Message.SettingChangedGlowEffect:
        this.ToggleGlowEffect((bool) message.Data);
        break;
      case Message.LevelLoaded:
        UserSettings currentSettings = (this.OwnerFacade.FetchDataProvider("SettingsDataProvider") as SettingsDataProvider).CurrentSettings;
        this.ToggleStarFog(currentSettings.ShowStarFog);
        this.ToggleStarDust(currentSettings.ShowStarDust);
        this.ToggleGlowEffect(currentSettings.ShowGlowEffect);
        break;
    }
  }

  private bool IsGlobalFogEnabled()
  {
    if ((Object) SpaceLevel.GetLevel() == (Object) null)
      return false;
    return SpaceLevel.GetLevel().Card.GlobalFogDesc.Enabled;
  }

  private void ToggleStarDust(bool showStardust)
  {
    Stardust stardust = Object.FindObjectOfType(typeof (Stardust)) as Stardust;
    if ((Object) stardust == (Object) null)
      return;
    stardust.Rendered = showStardust;
  }

  private void ToggleStarFog(bool showStarfog)
  {
    if ((Object) Camera.main == (Object) null)
      return;
    StarFog2 component = Camera.main.GetComponent<StarFog2>();
    if (!((Object) component != (Object) null))
      return;
    component.SetCloudsVisible(showStarfog);
    component.enabled = showStarfog;
  }

  private void ToggleGlowEffect(bool glowEffect)
  {
    if (this.IsGlobalFogEnabled() || (Object) Camera.main == (Object) null || (Object) Camera.main.GetComponent("BloomAndFlares") == (Object) null)
      return;
    MonoBehaviour monoBehaviour = Camera.main.GetComponent("BloomAndFlares") as MonoBehaviour;
    if (!((Object) monoBehaviour != (Object) null))
      return;
    monoBehaviour.enabled = glowEffect;
  }
}
