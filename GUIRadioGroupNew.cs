﻿// Decompiled with JetBrains decompiler
// Type: GUIRadioGroupNew
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIRadioGroupNew : GUIHorizontalPlacement
{
  public GUIRadioGroupNew()
    : base((SmartRect) null)
  {
  }

  public GUIRadioGroupNew(SmartRect parent)
    : base(parent)
  {
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse0)
    {
      int num = -1;
      for (int index = 0; index < this.Children.Count; ++index)
      {
        GUIButtonNew guiButtonNew = this.Children[index] as GUIButtonNew;
        if (guiButtonNew.Contains(mousePosition) && !guiButtonNew.IsPressed)
        {
          if (guiButtonNew.Handler != null)
            guiButtonNew.Handler();
          guiButtonNew.IsPressed = true;
          num = index;
        }
      }
      if (num != -1)
      {
        for (int index = 0; index < this.Children.Count; ++index)
        {
          if (index != num)
            (this.Children[index] as GUIButtonNew).IsPressed = false;
        }
      }
      if (num != -1)
      {
        this.wasMouseDown = true;
        return true;
      }
    }
    if (!this.Contains(mousePosition))
      return false;
    this.wasMouseDown = true;
    return true;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (!this.wasMouseDown)
      return false;
    for (int index = 0; index < this.Children.Count; ++index)
    {
      GUIButtonNew guiButtonNew = this.Children[index] as GUIButtonNew;
      if (guiButtonNew.Contains(mousePosition) && (Object) guiButtonNew.PressSound != (Object) null)
        GUISound.Instance.PlayOnce(guiButtonNew.PressSound);
    }
    this.wasMouseDown = false;
    return this.Contains(mousePosition);
  }
}
