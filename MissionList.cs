﻿// Decompiled with JetBrains decompiler
// Type: MissionList
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class MissionList : SmartList<Mission>, ILoadable
{
  private Flag isLoaded = new Flag();

  public Flag IsLoaded
  {
    get
    {
      return this.isLoaded;
    }
  }

  public MissionList()
  {
  }

  public MissionList(List<Mission> items)
    : base(items)
  {
  }

  public MissionList Filter(Predicate<Mission> match)
  {
    return new MissionList(this.FilterList(match));
  }

  public MissionList FilterByState(Mission.State state)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.Filter(new Predicate<Mission>(new MissionList.\u003CFilterByState\u003Ec__AnonStoreyDC() { state = state }.\u003C\u003Em__224));
  }

  protected override void Changed()
  {
    base.Changed();
    this.isLoaded = new Flag();
    using (List<Mission>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.isLoaded.Depend(new ILoadable[1]
        {
          (ILoadable) enumerator.Current
        });
    }
    this.isLoaded.AddHandler((SignalHandler) (() => FacadeFactory.GetInstance().SendMessage(Message.MissionListUpdated, (object) this)));
    this.isLoaded.Set();
  }
}
