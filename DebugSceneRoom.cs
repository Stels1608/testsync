﻿// Decompiled with JetBrains decompiler
// Type: DebugSceneRoom
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugSceneRoom : DebugBehaviour<DebugSceneRoom>
{
  private string[] rooms = new string[3]{ "cic", "outpost", "miningfacility" };
  private int oldRoomID = -1;
  private string room = string.Empty;
  private string door = "door1";
  private int roomID;

  private void Start()
  {
    this.windowID = 20;
    this.SetSize(300f, 210f);
  }

  protected override void WindowFunc()
  {
    GameLevel instance = GameLevel.Instance;
    if (instance is LoginLevel || (Object) instance == (Object) null)
    {
      GUILayout.Box("No commands \nfor this level.");
    }
    else
    {
      GUILayout.Label("Select room: ");
      this.roomID = GUILayout.SelectionGrid(this.roomID, this.rooms, 2);
      if (this.oldRoomID != this.roomID)
      {
        this.room = this.rooms[this.roomID];
        this.oldRoomID = this.roomID;
      }
      GUILayout.BeginHorizontal();
      GUILayout.Label("Room: ");
      this.room = GUILayout.TextField(this.room);
      GUILayout.EndHorizontal();
      GUILayout.BeginHorizontal();
      GUILayout.Label("Door: ");
      this.door = GUILayout.TextField(this.door);
      GUILayout.EndHorizontal();
      if (!GUILayout.Button("Enter"))
        return;
      DebugProtocol.GetProtocol().Command("room", this.room, this.door);
      DebugBehaviour<DebugSceneRoom>.Close();
    }
  }
}
