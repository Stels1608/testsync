﻿// Decompiled with JetBrains decompiler
// Type: Json.JsonReader
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Globalization;
using System.IO;
using System.Text;
using UnityEngine;

namespace Json
{
  public class JsonReader
  {
    private static CultureInfo cultureInfo = CultureInfo.GetCultureInfo("en-US");
    private char[] stringTerminators = new char[1]{ '"' };
    private char[] munericOrValueTerminators = new char[3]{ ',', ']', '}' };
    private StringReader reader;

    public JsonReader(string json)
    {
      this.reader = new StringReader(json);
    }

    public JsonItem Peek()
    {
      char ch;
      for (ch = (char) this.reader.Peek(); (int) ch == 32 || (int) ch == 9 || ((int) ch == 10 || (int) ch == 13); ch = (char) this.reader.Peek())
        this.reader.Read();
      if ((int) ch == 91)
        return new JsonItem(JsonItemType.ArrayStart, (string) null);
      if ((int) ch == 44)
        return new JsonItem(JsonItemType.ArrayObjectNext, (string) null);
      if ((int) ch == 93)
        return new JsonItem(JsonItemType.ArrayEnd, (string) null);
      if ((int) ch == 123)
        return new JsonItem(JsonItemType.ObjectStart, (string) null);
      if ((int) ch == 125)
        return new JsonItem(JsonItemType.ObjectEnd, (string) null);
      if ((int) ch == 58)
        return new JsonItem(JsonItemType.KeyValueSeparator, (string) null);
      return (JsonItem) null;
    }

    private JsonItem Read()
    {
      bool flag = false;
      while (!flag)
      {
        char ch = (char) this.reader.Read();
        switch (ch)
        {
          case ' ':
          case '\r':
          case '\n':
          case '\t':
            continue;
          case '[':
            return new JsonItem(JsonItemType.ArrayStart, (string) null);
          case ',':
            return new JsonItem(JsonItemType.ArrayObjectNext, (string) null);
          case ']':
            return new JsonItem(JsonItemType.ArrayEnd, (string) null);
          case '{':
            return new JsonItem(JsonItemType.ObjectStart, (string) null);
          case '}':
            return new JsonItem(JsonItemType.ObjectEnd, (string) null);
          case ':':
            return new JsonItem(JsonItemType.KeyValueSeparator, (string) null);
          case '"':
            return new JsonItem(JsonItemType.String, this.ReadString());
          default:
            if (char.IsDigit(ch) || (int) ch == 45)
              return new JsonItem(JsonItemType.Decimal, this.ReadDecimal(ch));
            if ((int) char.ToLower(ch) == 110 && this.CheckNull(ch))
              return new JsonItem(JsonItemType.Null, (string) null);
            if ((int) char.ToLower(ch) == 116 || (int) char.ToLower(ch) == 102)
              return new JsonItem(JsonItemType.Boolean, this.ReadBoolean(ch));
            throw new Exception("Wrong: \"" + (object) ch + "\"");
        }
      }
      return (JsonItem) null;
    }

    private string ReadString()
    {
      return this.ReadString(this.stringTerminators, true);
    }

    private string ReadDecimal(char firstCharacter)
    {
      string s = firstCharacter.ToString() + this.ReadString(this.munericOrValueTerminators, false);
      try
      {
        Decimal.Parse(s, NumberStyles.Number | NumberStyles.AllowExponent, (IFormatProvider) JsonReader.cultureInfo);
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ex.Message);
      }
      return s;
    }

    private bool CheckNull(char firstCharacter)
    {
      if ((firstCharacter.ToString() + this.ReadString(this.munericOrValueTerminators, false)).ToString().ToLower().Trim() != "null")
        throw new Exception("Wrong");
      return true;
    }

    private string ReadBoolean(char firstCharacter)
    {
      string str = firstCharacter.ToString() + this.ReadString(this.munericOrValueTerminators, false);
      try
      {
        bool.Parse(str);
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ex.Message);
      }
      return str;
    }

    private string ReadString(char[] terminators, bool eatTerminator)
    {
      StringBuilder stringBuilder = new StringBuilder();
      while (true)
      {
        do
        {
          int num1 = !eatTerminator ? this.reader.Peek() : this.reader.Read();
          if (num1 == -1)
            throw new Exception("Unexpected ending");
          char ch = (char) num1;
          if (Array.IndexOf<char>(terminators, ch) != -1)
            return stringBuilder.ToString();
          if ((int) ch == 92)
          {
            int num2 = this.reader.Read();
            switch (num2)
            {
              case -1:
                throw new Exception("Unexpected ending");
              case 117:
                string str = "U+";
                for (int index = 0; index < 4; ++index)
                {
                  str += (string) (object) (char) this.reader.Read();
                  if (num2 == -1)
                    throw new Exception("Unexpected ending");
                }
                try
                {
                  num2 = int.Parse(str.Substring(2), NumberStyles.HexNumber);
                  break;
                }
                catch (Exception ex)
                {
                  Debug.LogError((object) ex.Message);
                  break;
                }
            }
            ch = (char) num2;
          }
          stringBuilder.Append(ch);
        }
        while (eatTerminator);
        this.reader.Read();
      }
    }

    public static JsonData Parse(string json)
    {
      return JsonReader.ReadValue(new JsonReader(json));
    }

    private static JsonData ReadValue(JsonReader reader)
    {
      JsonItem jsonItem1 = reader.Read();
      switch (jsonItem1.token)
      {
        case JsonItemType.ObjectStart:
          JsonData createObject = JsonData.CreateObject;
          while (reader.Peek() == null || reader.Peek().token != JsonItemType.ObjectEnd)
          {
            JsonData jsonData1 = JsonReader.ReadValue(reader);
            if (!jsonData1.IsString)
              throw new Exception("Property name should be a string.");
            if (reader.Read().token != JsonItemType.KeyValueSeparator)
              throw new Exception("Wrong key/value separator.");
            JsonData jsonData2 = JsonReader.ReadValue(reader);
            if (!createObject.Object.ContainsKey(jsonData1.String))
              createObject.Object.Add(jsonData1.String, jsonData2);
            JsonItem jsonItem2 = reader.Read();
            if (jsonItem2.token == JsonItemType.ObjectEnd)
              return createObject;
            if (jsonItem2.token != JsonItemType.ArrayObjectNext)
              throw new Exception("Wrong object separator.");
          }
          reader.Read();
          return createObject;
        case JsonItemType.ArrayStart:
          JsonData createArray = JsonData.CreateArray;
          while (reader.Peek() == null || reader.Peek().token != JsonItemType.ArrayEnd)
          {
            JsonData jsonData = JsonReader.ReadValue(reader);
            createArray.Array.Add(jsonData);
            JsonItem jsonItem2 = reader.Read();
            if (jsonItem2.token == JsonItemType.ArrayEnd)
              return createArray;
            if (jsonItem2.token != JsonItemType.ArrayObjectNext)
              throw new Exception("Wrong array separator.");
          }
          reader.Read();
          return createArray;
        case JsonItemType.String:
        case JsonItemType.Decimal:
        case JsonItemType.Boolean:
        case JsonItemType.Null:
          return new JsonData(jsonItem1);
        default:
          throw new Exception("Wrong Json Format.");
      }
    }
  }
}
