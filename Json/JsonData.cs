﻿// Decompiled with JetBrains decompiler
// Type: Json.JsonData
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Json
{
  public class JsonData : IEquatable<JsonData>
  {
    private static CultureInfo cultureInfo = CultureInfo.GetCultureInfo("en-US");
    private List<JsonData> valueArray;
    private Dictionary<string, JsonData> valueObject;
    private string value;
    private JsonType type;

    public static JsonData CreateArray
    {
      get
      {
        return new JsonData(JsonType.Array);
      }
    }

    public static JsonData CreateObject
    {
      get
      {
        return new JsonData(JsonType.Object);
      }
    }

    public static JsonData CreateNull
    {
      get
      {
        return new JsonData(JsonType.Null);
      }
    }

    public string FileID { get; set; }

    public bool Boolean
    {
      get
      {
        if (this.type != JsonType.Boolean)
          throw new InvalidCastException("Instance of JsonData doesn't hold a boolean. It holds " + (object) this.type);
        return bool.Parse(this.value);
      }
    }

    public float Float
    {
      get
      {
        return (float) this.Double;
      }
    }

    public double Double
    {
      get
      {
        if (this.type != JsonType.Int && this.type != JsonType.Double)
          throw new InvalidCastException("Instance of JsonData doesn't hold a double. It holds " + (object) this.type);
        return double.Parse(this.value, (IFormatProvider) JsonData.cultureInfo);
      }
    }

    public int Int32
    {
      get
      {
        if (this.type != JsonType.Int)
          throw new InvalidCastException("Instance of JsonData doesn't hold an integer. It holds " + (object) this.type);
        return int.Parse(this.value);
      }
    }

    public string String
    {
      get
      {
        return this.value;
      }
    }

    public List<JsonData> Array
    {
      get
      {
        if (this.type == JsonType.Array)
          return this.valueArray;
        throw new InvalidCastException("The JsonData instance has to be initialized first.");
      }
    }

    public Dictionary<string, JsonData> Object
    {
      get
      {
        if (this.type == JsonType.Object)
          return this.valueObject;
        throw new InvalidCastException("Instance of JsonData is not an object. " + this.ToJsonString(string.Empty) + " it is " + (object) this.type);
      }
    }

    public bool IsNull
    {
      get
      {
        return this.type == JsonType.Null;
      }
    }

    public bool IsArray
    {
      get
      {
        return this.type == JsonType.Array;
      }
    }

    public bool IsBoolean
    {
      get
      {
        return this.type == JsonType.Boolean;
      }
    }

    public bool IsDouble
    {
      get
      {
        return this.type == JsonType.Double;
      }
    }

    public bool IsInt
    {
      get
      {
        return this.type == JsonType.Int;
      }
    }

    public bool IsObject
    {
      get
      {
        return this.type == JsonType.Object;
      }
    }

    public bool IsString
    {
      get
      {
        return this.type == JsonType.String;
      }
    }

    public bool IsNumeric
    {
      get
      {
        if (this.type != JsonType.Int)
          return this.type == JsonType.Double;
        return true;
      }
    }

    public JsonType JsonType
    {
      get
      {
        return this.type;
      }
    }

    public JsonData()
    {
      this.type = JsonType.String;
      this.value = string.Empty;
    }

    public JsonData(string obj)
    {
      this.type = JsonType.String;
      this.value = obj;
    }

    public JsonData(int obj)
    {
      this.type = JsonType.Int;
      this.value = obj.ToString();
    }

    public JsonData(float obj)
    {
      this.type = JsonType.Double;
      this.value = obj.ToString((IFormatProvider) JsonData.cultureInfo);
    }

    public JsonData(double obj)
    {
      this.type = JsonType.Double;
      this.value = obj.ToString((IFormatProvider) JsonData.cultureInfo);
    }

    public JsonData(bool obj)
    {
      this.type = JsonType.Boolean;
      this.value = obj.ToString().ToLower();
    }

    public JsonData(Dictionary<string, JsonData> obj)
    {
      this.type = JsonType.Object;
      this.valueObject = obj;
    }

    public JsonData(JsonItem item)
    {
      switch (item.token)
      {
        case JsonItemType.String:
          this.type = JsonType.String;
          this.value = item.Value;
          break;
        case JsonItemType.Decimal:
          int result1 = 0;
          double result2 = 0.0;
          if (!double.TryParse(item.Value, NumberStyles.Float, (IFormatProvider) JsonData.cultureInfo, out result2))
            throw new Exception("Numeric value expected.");
          this.type = !int.TryParse(item.Value, out result1) ? JsonType.Double : JsonType.Int;
          this.value = item.Value;
          break;
        case JsonItemType.Boolean:
          bool result3 = true;
          if (!bool.TryParse(item.Value, out result3))
            throw new Exception("Boolean value expected.");
          this.type = JsonType.Boolean;
          this.value = item.Value;
          break;
        case JsonItemType.Null:
          this.type = JsonType.Null;
          this.value = (string) null;
          break;
        default:
          throw new Exception(typeof (JsonItemType).ToString() + (object) item.token + " is not supported.");
      }
    }

    public JsonData(JsonType type)
    {
      this.type = type;
      switch (type)
      {
        case JsonType.Int:
        case JsonType.Double:
          this.value = 0.ToString();
          break;
        case JsonType.Boolean:
          this.value = true.ToString();
          break;
        case JsonType.Object:
          this.valueObject = new Dictionary<string, JsonData>();
          break;
        case JsonType.Array:
          this.valueArray = new List<JsonData>();
          break;
      }
    }

    public void DeleteDataset(string key)
    {
      if (this.type == JsonType.Object)
        this.valueObject.Remove(key);
      else
        throw new InvalidCastException("Instance of JsonData is not an object. " + this.ToJsonString(string.Empty) + " it is " + (object) this.type);
    }

    public bool Equals(JsonData x)
    {
      if (x == null || x.type != this.type)
        return false;
      switch (this.type)
      {
        case JsonType.Null:
          return true;
        case JsonType.String:
        case JsonType.Int:
        case JsonType.Double:
        case JsonType.Boolean:
          return this.value.Equals(x.value);
        case JsonType.Object:
          return this.valueObject.Equals((object) x.valueObject);
        case JsonType.Array:
          return this.valueArray.Equals((object) x.valueArray);
        default:
          return false;
      }
    }

    private string ReplaceQuotes(string str)
    {
      return str.Replace("\"", "\\\"");
    }

    public string ToJsonString(string indent = "")
    {
      string indent1 = indent + "\t";
      StringBuilder stringBuilder = new StringBuilder();
      switch (this.type)
      {
        case JsonType.Null:
          return "null";
        case JsonType.String:
          return "\"" + this.ReplaceQuotes(this.value) + "\"";
        case JsonType.Int:
        case JsonType.Double:
          return this.value;
        case JsonType.Boolean:
          return this.value.ToLower();
        case JsonType.Object:
          stringBuilder.Append("{");
          using (Dictionary<string, JsonData>.Enumerator enumerator = this.valueObject.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              KeyValuePair<string, JsonData> current = enumerator.Current;
              stringBuilder.Append("\n" + indent1 + "\"" + current.Key + "\": " + current.Value.ToJsonString(indent1) + ",");
            }
          }
          if (this.valueObject.Count > 0)
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
          stringBuilder.Append("\n" + indent + "}");
          return stringBuilder.ToString();
        case JsonType.Array:
          stringBuilder.Append("[");
          using (List<JsonData>.Enumerator enumerator = this.valueArray.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              JsonData current = enumerator.Current;
              stringBuilder.Append("\n" + indent1 + current.ToJsonString(indent1) + ",");
            }
          }
          if (this.valueArray.Count > 0)
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
          stringBuilder.Append("\n" + indent + "]");
          return stringBuilder.ToString();
        default:
          throw new Exception("o_O");
      }
    }

    public void WriteRawValue(string rawValue)
    {
      this.value = rawValue;
    }

    public void WriteRawValue(JsonData rawData)
    {
      this.value = rawData.value;
      this.valueObject = rawData.valueObject;
      this.type = rawData.type;
      this.valueArray = rawData.valueArray;
    }

    public static JsonData ParseJData(List<string> itemPath, JsonData jObj)
    {
      JsonData jsonData1 = jObj;
      if (itemPath.Count == 1)
      {
        if (jObj.Object.ContainsKey(itemPath[0]))
        {
          JsonData jsonData2 = jObj.Object[itemPath[0]];
          if (jsonData2 != null)
            jsonData1 = jsonData2;
        }
      }
      else
      {
        if (jObj.IsArray)
        {
          using (List<JsonData>.Enumerator enumerator = jObj.Array.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              JsonData current = enumerator.Current;
              for (int index = 0; index < itemPath.Count; ++index)
              {
                if (current.Object.ContainsKey(itemPath[index]))
                {
                  JsonData jObj1 = current.Object[itemPath[index]];
                  List<string> itemPath1 = itemPath;
                  itemPath1.Remove(itemPath[index]);
                  jsonData1 = JsonData.ParseJData(itemPath1, jObj1);
                }
              }
            }
          }
        }
        if (jObj.IsObject)
        {
          for (int index = 0; index < itemPath.Count; ++index)
          {
            if (jObj.Object.ContainsKey(itemPath[index]))
            {
              JsonData jObj1 = jObj.Object[itemPath[index]];
              List<string> itemPath1 = itemPath;
              itemPath1.Remove(itemPath[index]);
              jsonData1 = JsonData.ParseJData(itemPath1, jObj1);
            }
          }
        }
      }
      return jsonData1;
    }

    public override string ToString()
    {
      return this.ToJsonString(string.Empty);
    }

    public void SortKeys()
    {
      switch (this.type)
      {
        case JsonType.Object:
          Dictionary<string, JsonData> dictionary = new Dictionary<string, JsonData>();
          List<string> stringList = new List<string>((IEnumerable<string>) this.valueObject.Keys);
          stringList.Sort((IComparer<string>) StringComparer.Ordinal);
          for (int index = 0; index < stringList.Count; ++index)
            dictionary.Add(stringList[index], this.valueObject[stringList[index]]);
          this.valueObject = dictionary;
          break;
      }
    }
  }
}
