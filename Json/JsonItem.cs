﻿// Decompiled with JetBrains decompiler
// Type: Json.JsonItem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Json
{
  public class JsonItem
  {
    public JsonItemType token;
    public string Value;

    public JsonItem(JsonItemType token, string value)
    {
      this.token = token;
      this.Value = value;
    }

    public override string ToString()
    {
      string str = this.token.ToString();
      if (this.token == JsonItemType.String || this.token == JsonItemType.Boolean || this.token == JsonItemType.Decimal)
        str = str + ": " + this.Value;
      return str;
    }
  }
}
