﻿// Decompiled with JetBrains decompiler
// Type: Json.XPathTraverser
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace Json
{
  public class XPathTraverser
  {
    public static JsonData Traverse(JsonData json, string xpath)
    {
      if (json == null)
        throw new ArgumentException("Traverse XPath: json is null.");
      if (string.IsNullOrEmpty(xpath))
        throw new ArgumentException(xpath + " is null or empty.");
      Queue<string> xpath1 = new Queue<string>((IEnumerable<string>) xpath.Split(new char[1]{ '/' }, StringSplitOptions.RemoveEmptyEntries));
      return XPathTraverser.Traverse(json, xpath1);
    }

    public static JsonData Traverse(JsonData json, Queue<string> xpath)
    {
      if (xpath.Count == 0)
        return json;
      string key = xpath.Dequeue();
      if (!json.IsObject)
        throw new Exception("Wrong xpath or json object - can't traverse. " + (object) json);
      if (!json.Object.ContainsKey(key))
        json.Object.Add(key, JsonData.CreateObject);
      return XPathTraverser.Traverse(json.Object[key], xpath);
    }
  }
}
