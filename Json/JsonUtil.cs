﻿// Decompiled with JetBrains decompiler
// Type: Json.JsonUtil
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Json
{
  public static class JsonUtil
  {
    public static void CopyJsonObject(Dictionary<string, JsonData> target, Dictionary<string, JsonData> source)
    {
      using (Dictionary<string, JsonData>.Enumerator enumerator = source.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, JsonData> current = enumerator.Current;
          switch (current.Value.JsonType)
          {
            case JsonType.Object:
              JsonUtil.CopyJsonObject(target[current.Key].Object, source[current.Key].Object);
              continue;
            case JsonType.Array:
              JsonUtil.CopyJsonArray(target[current.Key].Array, source[current.Key].Array);
              continue;
            default:
              JsonUtil.CopyJsonEntry(target[current.Key], source[current.Key]);
              continue;
          }
        }
      }
    }

    public static void CopyJsonArray(List<JsonData> target, List<JsonData> source)
    {
      for (int index = 0; index < source.Count; ++index)
      {
        switch (target[index].JsonType)
        {
          case JsonType.Object:
            JsonUtil.CopyJsonObject(target[index].Object, source[index].Object);
            break;
          case JsonType.Array:
            JsonUtil.CopyJsonArray(target[index].Array, source[index].Array);
            break;
          default:
            JsonUtil.CopyJsonEntry(target[index], source[index]);
            break;
        }
      }
    }

    public static void CopyJsonEntry(JsonData target, JsonData source)
    {
      target.WriteRawValue(source);
    }
  }
}
