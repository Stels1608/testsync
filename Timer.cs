﻿// Decompiled with JetBrains decompiler
// Type: Timer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Timer : MonoBehaviour
{
  private float interval = 1f;
  public float DestructionTime = -1f;
  public Timer.OnFinishHandler OnFinish;
  private object parameter;
  private Timer.TickHandler tickHandler;
  private Timer.TickParametrizedHandler tickParametrizedHandler;
  private bool destroyAfterTick;

  public static Timer CreateTimer(string name, float time, float interval, Timer.TickHandler tickHandler)
  {
    Timer timer = new GameObject(name).AddComponent<Timer>();
    timer.SetInterval(interval);
    timer.SetHandler(tickHandler);
    timer.StartTiming(time);
    return timer;
  }

  public static Timer CreateTimer(string name, float time, float interval, Timer.TickParametrizedHandler tickHandler, object parameter)
  {
    Timer timer = new GameObject(name).AddComponent<Timer>();
    timer.SetInterval(interval);
    timer.SetParameter(parameter);
    timer.SetHandler(tickHandler);
    timer.StartTiming(time);
    return timer;
  }

  public static Timer CreateTimer(string name, float time, float interval, Timer.TickParametrizedHandler tickHandler, object parameter, bool destroyAfterTick)
  {
    Timer timer = Timer.CreateTimer(name, time, interval, tickHandler, parameter);
    timer.destroyAfterTick = destroyAfterTick;
    return timer;
  }

  private void Start()
  {
    this.enabled = false;
  }

  public void SetHandler(Timer.TickHandler newValue)
  {
    this.tickHandler = newValue;
  }

  public void SetHandler(Timer.TickParametrizedHandler newValue)
  {
    this.tickParametrizedHandler = newValue;
  }

  public void SetParameter(object newValue)
  {
    this.parameter = newValue;
  }

  public void StartTiming(float time)
  {
    this.InvokeRepeating("Tick", time, this.interval);
  }

  public void StopTiming()
  {
    this.StopCoroutine("Tick");
  }

  public void SetInterval(float newValue)
  {
    this.interval = newValue;
  }

  private void Tick()
  {
    if (this.parameter == null)
    {
      if (this.tickHandler != null)
        this.tickHandler();
    }
    else if (this.tickParametrizedHandler != null)
      this.tickParametrizedHandler(this.parameter);
    if (!this.destroyAfterTick && ((double) this.DestructionTime <= 0.0 || (double) Time.time <= (double) this.DestructionTime))
      return;
    if (this.OnFinish != null)
      this.OnFinish();
    Object.Destroy((Object) this.gameObject);
  }

  public delegate void TickHandler();

  public delegate void TickParametrizedHandler(object parameter);

  public delegate void OnFinishHandler();
}
