﻿// Decompiled with JetBrains decompiler
// Type: ChatLoginFailure
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

internal class ChatLoginFailure : ChatProtocolParser
{
  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split('#');
    if (strArray.Length != 2)
      return false;
    string key = strArray[0];
    if (key != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (ChatLoginFailure.\u003C\u003Ef__switch\u0024map7 == null)
      {
        // ISSUE: reference to a compiler-generated field
        ChatLoginFailure.\u003C\u003Ef__switch\u0024map7 = new Dictionary<string, int>(3)
        {
          {
            "ey",
            0
          },
          {
            "bw",
            1
          },
          {
            "ex",
            2
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (ChatLoginFailure.\u003C\u003Ef__switch\u0024map7.TryGetValue(key, out num))
      {
        switch (num)
        {
          case 0:
            this.type = ChatProtocolParserType.LoginFailureBanIp;
            break;
          case 1:
            this.type = ChatProtocolParserType.LoginFailureWrongChatVersion;
            break;
          case 2:
            this.type = ChatProtocolParserType.LoginFailureAuthenticationFail;
            break;
          default:
            goto label_10;
        }
        return true;
      }
    }
label_10:
    return false;
  }
}
