﻿// Decompiled with JetBrains decompiler
// Type: GUIScrollBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUIScrollBox : GUIPanel
{
  private int maxRange = 10;
  private int viewRange = 10;
  private int oldValue = -1;
  private GUIVScroller scroller;
  private GUIButtonNew arrowUp;
  private GUIButtonNew arrowDown;
  private GUIScrollBox.Handlr handler;

  public GUIScrollBox.Handlr Handler
  {
    set
    {
      this.handler = value;
    }
  }

  public int MaxRange
  {
    get
    {
      return this.maxRange;
    }
    set
    {
      value = Mathf.Max(1, value);
      if (value == this.maxRange)
        return;
      this.maxRange = value;
      this.RecalcValue();
      this.RecalcScrollerHeight();
    }
  }

  public int ViewRange
  {
    get
    {
      return this.viewRange;
    }
    set
    {
      value = Mathf.Max(0, value);
      if (value == this.viewRange)
        return;
      this.viewRange = value;
      this.RecalcValue();
      this.RecalcScrollerHeight();
    }
  }

  public int MaxViewRange
  {
    get
    {
      return this.MaxRange - this.ViewRange;
    }
  }

  public int Value
  {
    get
    {
      return (int) Mathf.Round(this.scroller.Value * (float) Mathf.Max(0, this.MaxViewRange));
    }
    set
    {
      this.scroller.Value = (float) Mathf.Clamp(value, 0, this.MaxViewRange) / (float) Mathf.Max(1, this.MaxViewRange);
      if (value == this.oldValue || this.handler == null)
        return;
      this.oldValue = value;
      this.handler();
    }
  }

  public GUIScrollBox(float height, SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
    Texture2D normal1 = (Texture2D) ResourceLoader.Load("GUI/Common/up_normal");
    Texture2D over1 = (Texture2D) ResourceLoader.Load("GUI/Common/up_over");
    Texture2D pressed1 = (Texture2D) ResourceLoader.Load("GUI/Common/up_pressed");
    Texture2D normal2 = (Texture2D) ResourceLoader.Load("GUI/Common/down_normal");
    Texture2D over2 = (Texture2D) ResourceLoader.Load("GUI/Common/down_over");
    Texture2D pressed2 = (Texture2D) ResourceLoader.Load("GUI/Common/down_pressed");
    this.arrowUp = new GUIButtonNew(normal1, over1, pressed1, this.root);
    this.arrowUp.Position = new float2(0.0f, (float) (-(double) height / 2.0 + (double) this.arrowUp.Rect.height / 2.0));
    this.arrowUp.TextLabel.Text = string.Empty;
    this.arrowUp.Handler = new AnonymousDelegate(this.OnArrowUp);
    this.AddPanel((GUIPanel) this.arrowUp);
    this.arrowDown = new GUIButtonNew(normal2, over2, pressed2, this.root);
    this.arrowDown.Position = new float2(0.0f, (float) ((double) height / 2.0 - (double) this.arrowUp.Rect.height / 2.0));
    this.arrowDown.TextLabel.Text = string.Empty;
    this.arrowDown.Handler = new AnonymousDelegate(this.OnArrowDown);
    this.AddPanel((GUIPanel) this.arrowDown);
    this.scroller = new GUIVScroller(height - this.arrowUp.Rect.height - this.arrowDown.Rect.height, ResourceLoader.Load<Texture2D>("GUI/Common/scroller"), this.root);
    this.AddPanel((GUIPanel) this.scroller);
    this.root.Width = this.arrowUp.Rect.height;
    this.root.Height = height;
    this.RecalcScrollerHeight();
  }

  private void RecalcScrollerHeight()
  {
    if (this.maxRange <= this.viewRange)
      this.IsRendered = false;
    else
      this.IsRendered = true;
    if (this.maxRange == 0 || this.viewRange / this.maxRange > 1)
      this.scroller.ScrollerHeight = 1f;
    else
      this.scroller.ScrollerHeight = (float) this.viewRange / (float) this.maxRange;
  }

  private void RecalcValue()
  {
    this.Value = this.Value;
  }

  public override bool OnMouseScrollDown()
  {
    this.OnArrowDown();
    return true;
  }

  public override bool OnMouseScrollUp()
  {
    this.OnArrowUp();
    return true;
  }

  public void OnArrowUp()
  {
    if (this.MaxViewRange <= 0)
      return;
    --this.Value;
  }

  public void OnArrowDown()
  {
    if (this.MaxViewRange <= 0)
      return;
    ++this.Value;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    base.OnMouseMove(mousePosition);
    if (!this.scroller.IsScrolling)
      return;
    this.RecalcValue();
  }

  public delegate void Handlr();
}
