﻿// Decompiled with JetBrains decompiler
// Type: AlignmentTracker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AlignmentTracker : MonoBehaviour
{
  private Vector3 m_Position = Vector3.zero;
  private Vector3 m_PositionPrev = Vector3.zero;
  private Vector3 m_Velocity = Vector3.zero;
  private Vector3 m_VelocityPrev = Vector3.zero;
  private Vector3 m_VelocitySmoothed = Vector3.zero;
  private Vector3 m_Acceleration = Vector3.zero;
  private Vector3 m_AccelerationSmoothed = Vector3.zero;
  private Quaternion m_Rotation = Quaternion.identity;
  private Quaternion m_RotationPrev = Quaternion.identity;
  private Vector3 m_AngularVelocity = Vector3.zero;
  private Vector3 m_AngularVelocitySmoothed = Vector3.zero;
  private bool firstFrame = true;
  public bool fixedUpdate;
  private float m_CurrentFixedTime;
  private float m_CurrentLateTime;
  private CharacterController m_CharacterController;
  private Rigidbody m_RigidBody;

  public Vector3 position
  {
    get
    {
      return this.m_Position;
    }
  }

  public Vector3 velocity
  {
    get
    {
      return this.m_Velocity;
    }
  }

  public Vector3 velocitySmoothed
  {
    get
    {
      return this.m_VelocitySmoothed;
    }
  }

  public Vector3 acceleration
  {
    get
    {
      return this.m_Acceleration;
    }
  }

  public Vector3 accelerationSmoothed
  {
    get
    {
      return this.m_AccelerationSmoothed;
    }
  }

  public Quaternion rotation
  {
    get
    {
      return this.m_Rotation;
    }
  }

  public Vector3 angularVelocity
  {
    get
    {
      return this.m_AngularVelocity;
    }
  }

  public Vector3 angularVelocitySmoothed
  {
    get
    {
      return this.m_AngularVelocitySmoothed;
    }
  }

  private void Awake()
  {
    this.m_CharacterController = this.GetComponent(typeof (CharacterController)) as CharacterController;
    this.m_RigidBody = this.GetComponent<Rigidbody>();
    this.m_CurrentLateTime = -1f;
    this.m_CurrentFixedTime = -1f;
  }

  private Vector3 CalculateAngularVelocity(Quaternion prev, Quaternion current)
  {
    Quaternion quaternion = Quaternion.Inverse(prev) * current;
    float angle = 0.0f;
    Vector3 axis = Vector3.zero;
    quaternion.ToAngleAxis(out angle, out axis);
    if ((double) angle > 180.0)
      angle -= 360f;
    float num = angle / Time.deltaTime;
    return axis.normalized * num;
  }

  private void UpdateTracking()
  {
    this.m_Position = this.transform.position;
    this.m_Rotation = this.transform.rotation;
    if (this.firstFrame)
    {
      this.firstFrame = false;
    }
    else
    {
      if ((Object) this.m_CharacterController != (Object) null)
      {
        this.m_Velocity = this.m_CharacterController.velocity;
        this.m_AngularVelocity = this.CalculateAngularVelocity(this.m_RotationPrev, this.m_Rotation);
      }
      else if ((Object) this.m_RigidBody != (Object) null)
      {
        this.m_Velocity = (this.m_Position - this.m_PositionPrev) / Time.deltaTime;
        this.m_AngularVelocity = this.CalculateAngularVelocity(this.m_RotationPrev, this.m_Rotation);
      }
      else
      {
        this.m_Velocity = (this.m_Position - this.m_PositionPrev) / Time.deltaTime;
        this.m_AngularVelocity = this.CalculateAngularVelocity(this.m_RotationPrev, this.m_Rotation);
      }
      this.m_Acceleration = (this.m_Velocity - this.m_VelocityPrev) / Time.deltaTime;
      this.m_PositionPrev = this.m_Position;
      this.m_RotationPrev = this.m_Rotation;
      this.m_VelocityPrev = this.m_Velocity;
    }
  }

  public void ControlledFixedUpdate()
  {
    if ((double) this.m_CurrentFixedTime == (double) Time.time)
      return;
    this.m_CurrentFixedTime = Time.time;
    if (!this.fixedUpdate)
      return;
    this.UpdateTracking();
  }

  public void ControlledLateUpdate()
  {
    if ((double) this.m_CurrentLateTime == (double) Time.time)
      return;
    this.m_CurrentLateTime = Time.time;
    if (!this.fixedUpdate)
      this.UpdateTracking();
    this.m_VelocitySmoothed = Vector3.Lerp(this.m_VelocitySmoothed, this.m_Velocity, Time.deltaTime * 10f);
    this.m_AccelerationSmoothed = Vector3.Lerp(this.m_AccelerationSmoothed, this.m_Acceleration, Time.deltaTime * 3f);
    this.m_AngularVelocitySmoothed = Vector3.Lerp(this.m_AngularVelocitySmoothed, this.m_AngularVelocity, Time.deltaTime * 3f);
    if (!this.fixedUpdate)
      return;
    this.m_Position += this.m_Velocity * Time.deltaTime;
  }
}
