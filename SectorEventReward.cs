﻿// Decompiled with JetBrains decompiler
// Type: SectorEventReward
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class SectorEventReward
{
  public uint EventObjectId { get; private set; }

  public uint SectorEventGuid { get; private set; }

  public SectorEventRanking Ranking { get; private set; }

  public List<ShipItem> Items { get; private set; }

  public SectorEventReward(uint eventObjectId, uint sectorEventGuid, SectorEventRanking ranking, List<ShipItem> rewardItems)
  {
    this.EventObjectId = eventObjectId;
    this.SectorEventGuid = sectorEventGuid;
    this.Ranking = ranking;
    this.Items = rewardItems;
  }

  public override string ToString()
  {
    return string.Format("EventObjectId: {0}, SectorEventGuid: {1}, Ranking: {2}", (object) this.EventObjectId, (object) this.SectorEventGuid, (object) this.Ranking);
  }
}
