﻿// Decompiled with JetBrains decompiler
// Type: ArmedMineIndicator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ArmedMineIndicator : MonoBehaviour
{
  public DateTime timeWhenArmed;
  public DateTime? timeWhenDisarmed;

  private void Start()
  {
    this.Disarm();
    if (this.timeWhenArmed <= Game.TimeSync.ServerDateTime)
    {
      this.Arm();
    }
    else
    {
      this.Invoke("Arm", (float) (this.timeWhenArmed - Game.TimeSync.ServerDateTime).Ticks / 1E+07f);
      if (!this.timeWhenDisarmed.HasValue)
        return;
      this.Invoke("Disarm", (float) (this.timeWhenDisarmed.Value - Game.TimeSync.ServerDateTime).Ticks / 1E+07f);
    }
  }

  private void Disarm()
  {
    foreach (ParticleSystem componentsInChild in this.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Stop();
    foreach (ParticleEmitter componentsInChild in this.GetComponentsInChildren<ParticleEmitter>())
      componentsInChild.emit = false;
  }

  private void Arm()
  {
    foreach (ParticleSystem componentsInChild in this.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.Play();
    foreach (ParticleEmitter componentsInChild in this.GetComponentsInChildren<ParticleEmitter>())
      componentsInChild.emit = true;
  }
}
