﻿// Decompiled with JetBrains decompiler
// Type: GuiInvisibilityCountdown
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class GuiInvisibilityCountdown : GuiPanel
{
  private readonly GuiLabel countdownDesc = new GuiLabel(Gui.Options.FontBGM_BT, 20);
  private readonly GuiLabel countdown = new GuiLabel(Gui.Options.FontBGM_BT, 40);
  private const double TIME_IN_SECONDS = 10.0;
  private readonly DateTime invisibilityTime;

  public GuiInvisibilityCountdown()
  {
    this.Align = Align.UpCenter;
    this.Position = new Vector2(0.0f, 200f);
    this.MouseTransparent = true;
    this.countdownDesc.Alignment = TextAnchor.UpperCenter;
    this.AddChild((GuiElementBase) this.countdownDesc, Align.UpCenter, new Vector2(0.0f, -50f));
    this.countdownDesc.Text = BsgoLocalization.Get("%$bgo.misc.enter_sector_msg%", (object) 10.0);
    this.countdown.Alignment = TextAnchor.UpperCenter;
    this.AddChild((GuiElementBase) this.countdown, Align.UpCenter);
    this.invisibilityTime = Game.TimeSync.ServerDateTime.AddSeconds(10.0);
    this.handleMouseInput = false;
  }

  public override void Update()
  {
    base.Update();
    TimeSpan time = this.invisibilityTime - Game.TimeSync.ServerDateTime;
    this.countdown.Text = !(time > TimeSpan.Zero) ? Tools.FormatTime(TimeSpan.Zero) : Tools.FormatTime(time);
  }

  public static void Show()
  {
    Game.RegisterDialog((IGUIPanel) new GuiInvisibilityCountdown(), true);
  }

  public void Stop()
  {
    Game.UnregisterDialog((IGUIPanel) this);
  }
}
