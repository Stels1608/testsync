﻿// Decompiled with JetBrains decompiler
// Type: DebugMissileLauncher
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugMissileLauncher : MonoBehaviour
{
  public float DebugMissileSpeed = 80f;
  public float DebugFireInterval = 2f;
  public float DebugOffsetRadius = 1f;
  private float lastDebugFireTime = float.NegativeInfinity;
  public SpaceObject DebugTarget;
  public GameObject DebugMissilePrefab;
  public GameObject DebugExplosionPrefab;
  public bool DebugFire;

  private void LateUpdate()
  {
    if (!this.DebugFire || (double) Time.time - (double) this.lastDebugFireTime <= (double) this.DebugFireInterval)
      return;
    this.lastDebugFireTime = Time.time;
    this.Fire(this.DebugTarget);
  }

  private void Fire(SpaceObject target)
  {
    if (!this.DebugFire)
      return;
    DebugMissileScript debugMissileScript = Object.Instantiate<GameObject>(this.DebugMissilePrefab).AddComponent<DebugMissileScript>();
    debugMissileScript.Target = target;
    debugMissileScript.Offset = Random.insideUnitSphere * this.DebugOffsetRadius;
    debugMissileScript.Speed = this.DebugMissileSpeed;
    debugMissileScript.ExplosionPrefab = this.DebugExplosionPrefab;
    debugMissileScript.transform.position = this.transform.position;
    debugMissileScript.transform.rotation = Quaternion.LookRotation(target.Position + debugMissileScript.Offset - this.transform.position);
  }
}
