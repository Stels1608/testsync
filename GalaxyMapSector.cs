﻿// Decompiled with JetBrains decompiler
// Type: GalaxyMapSector
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GalaxyMapSector : GuiPanel
{
  private static readonly Texture2D pegasusTexture = ResourceLoader.Load<Texture2D>("GUI/GalaxyMap/icon_pegasus");
  private static readonly Texture2D baseStarTexture = ResourceLoader.Load<Texture2D>("GUI/GalaxyMap/icon_basestar");
  private readonly GuiImage outPostImage = new GuiImage();
  private readonly GuiImage cylonMiningImage = new GuiImage("GUI/GalaxyMap/cylon_mine");
  private readonly GuiImage colonialMiningImage = new GuiImage("GUI/GalaxyMap/colonial_mine");
  private readonly GuiImage sectorEventBgGlowImage = new GuiImage("GUI/GalaxyMap/sector_event_bg_glow");
  private readonly GuiImage sectorEventImage = new GuiImage("GUI/GalaxyMap/sector_event");
  private readonly GuiImage beaconImage = new GuiImage();
  private readonly GuiImage star = new GuiImage();
  private readonly string texturePath = string.Empty;
  private const uint COLONIAL_BASE_SECTOR = 0;
  private const uint CYLON_BASE_SECTOR = 6;
  private const uint COLONIAL_HANGAR_SECTOR = 49;
  private const uint CYLON_HANGAR_SECTOR = 50;
  private const string ICON_SECTOR_EVENT_PATH = "GUI/GalaxyMap/sector_event";
  private const string ICON_SECTOR_EVENT_GLOW_PATH = "GUI/GalaxyMap/sector_event_bg_glow";
  private const string ICON_DRONE_EVENT_PATH = "GUI/GalaxyMap/drone_event";
  public SectorDesc Desc;
  private bool animOverride;
  private GuiAnimationSimple blowAnimation;
  private readonly GuiLabel name;

  public GalaxyMapSector(SectorDesc desc)
  {
    this.Desc = desc;
    switch (StaticCards.GalaxyMap.Stars[desc.Id].GUIIndex)
    {
      case 0:
        this.texturePath = "GUI/GalaxyMap/anim_star_big";
        break;
      case 1:
        this.texturePath = "GUI/GalaxyMap/anim_star_middle";
        break;
      case 2:
        this.texturePath = "GUI/GalaxyMap/anim_star_small";
        break;
    }
    this.blowAnimation = new GuiAnimationSimple((Texture2D) ResourceLoader.Load(this.texturePath), 5U, 5U);
    this.Size = this.blowAnimation.Size;
    this.AddChild((GuiElementBase) this.outPostImage, Align.MiddleCenter);
    this.AddChild((GuiElementBase) this.beaconImage, Align.MiddleCenter);
    this.beaconImage.IsRendered = false;
    this.AddChild((GuiElementBase) this.cylonMiningImage, Align.MiddleCenter, new Vector2(-16f, 0.0f));
    this.AddChild((GuiElementBase) this.colonialMiningImage, Align.MiddleCenter, new Vector2(16f, 0.0f));
    this.AddChild((GuiElementBase) this.blowAnimation, Align.MiddleCenter);
    this.sectorEventBgGlowImage.IsRendered = false;
    this.sectorEventImage.IsRendered = false;
    this.AddChild((GuiElementBase) this.sectorEventBgGlowImage, Align.MiddleCenter, new Vector2(0.0f, 6f));
    this.AddChild((GuiElementBase) this.sectorEventImage, Align.MiddleCenter, new Vector2(0.0f, 6f));
    this.blowAnimation.isPlaying = true;
    this.name = new GuiLabel(desc.Name, Gui.Options.FontBGM_BT, 10);
    this.name.OverlayColor = new Color?(Game.Me.Faction != Faction.Colonial ? Tools.Color(39, 7, 4) : Tools.Color(0, 8, 31));
    this.AddChild((GuiElementBase) this.name, Align.UpCenter, new Vector2(13f, -1f));
    Vector2 position1 = new Vector2(-20f, 10f);
    Vector2 position2 = new Vector2(20f, 10f);
    if ((int) desc.Id == 0)
      this.AddChild((GuiElementBase) new GuiImage("GUI/GalaxyMap/humanbase"), Align.MiddleCenter, position1);
    else if ((int) desc.Id == 6)
      this.AddChild((GuiElementBase) new GuiImage("GUI/GalaxyMap/cylonbase"), Align.MiddleCenter, position1);
    if ((int) desc.Id == 49)
      this.AddChild((GuiElementBase) new GuiImage("GUI/GalaxyMap/humanbase"), Align.MiddleCenter, position1);
    else if ((int) desc.Id == 50)
      this.AddChild((GuiElementBase) new GuiImage("GUI/GalaxyMap/cylonbase"), Align.MiddleCenter, position1);
    this.AddChild((GuiElementBase) this.star, Align.MiddleCenter, position2);
    this.PeriodicUpdate();
  }

  public void RemoveFlagShipImage()
  {
    this.star.IsRendered = false;
  }

  private Color GetEventGlowColor()
  {
    if ((int) this.Desc.AncientDynamicEvents > 0)
      return GalaxyMapLegend.SectorEventAncientColor;
    if ((int) this.Desc.NeutralDynamicEvents > 0)
      return GalaxyMapLegend.SectorEventNeutralColor;
    Color? nullable = new Color?();
    if ((int) this.Desc.CylonDynamicEvents > 0)
      nullable = new Color?(TargetColors.GetFactionColor(Faction.Cylon));
    if ((int) this.Desc.ColonialDynamicEvents > 0 && (!nullable.HasValue || Game.Me.Faction == Faction.Colonial))
      nullable = new Color?(TargetColors.GetFactionColor(Faction.Colonial));
    if (!nullable.HasValue)
      nullable = new Color?(Color.grey);
    return nullable.Value;
  }

  public override void Update()
  {
    base.Update();
    if (!this.sectorEventBgGlowImage.IsRendered)
      return;
    this.sectorEventBgGlowImage.OverlayColor = new Color?(this.GetEventGlowColor() * (float) (0.5 * (double) Mathf.Sin(Time.realtimeSinceStartup * 2f) + 0.5));
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    this.Position = StaticCards.GalaxyMap.Stars[this.Desc.Id].Position;
    if (this.Desc.HasColonialOutposts && this.Desc.HasCylonOutposts)
      this.outPostImage.TexturePath = "GUI/GalaxyMap/mixed_outpost";
    else if (this.Desc.HasColonialOutposts)
      this.outPostImage.TexturePath = "GUI/GalaxyMap/colonial_outpost";
    else if (this.Desc.HasCylonOutposts)
      this.outPostImage.TexturePath = "GUI/GalaxyMap/cylon_outpost";
    else
      this.outPostImage.Texture = (Texture2D) null;
    this.beaconImage.IsRendered = false;
    if (Game.Me.Faction == Faction.Colonial)
    {
      if ((long) this.Desc.Id == (long) Game.Galaxy.pegasusSectorId && (Object) this.star.Texture != (Object) GalaxyMapSector.pegasusTexture)
        this.star.TexturePath = GalaxyMapSector.pegasusTexture.name;
      this.star.IsRendered = (long) this.Desc.Id == (long) Game.Galaxy.pegasusSectorId;
      if (this.Desc.ColonialIsJumpBeacon)
        this.beaconImage.TexturePath = "GUI/GalaxyMap/jump_beacon_human";
      this.beaconImage.IsRendered = this.Desc.ColonialIsJumpBeacon;
    }
    else if (Game.Me.Faction == Faction.Cylon)
    {
      if ((long) this.Desc.Id == (long) Game.Galaxy.basestarSectorId && (Object) this.star.Texture != (Object) GalaxyMapSector.baseStarTexture)
        this.star.TexturePath = GalaxyMapSector.baseStarTexture.name;
      this.star.IsRendered = (long) this.Desc.Id == (long) Game.Galaxy.basestarSectorId;
      if (this.Desc.CylonIsJumpBeacon)
        this.beaconImage.TexturePath = "GUI/GalaxyMap/jump_beacon_cylon";
      this.beaconImage.IsRendered = this.Desc.CylonIsJumpBeacon;
    }
    List<JumpTargetTransponderDesc> targetTransponders = this.Desc.FilteredJumpTargetTransponders;
    this.StopAnimation();
    if (targetTransponders != null && targetTransponders.Count > 0)
      this.PlayAnimation(Color.green);
    this.cylonMiningImage.IsRendered = this.Desc.HasCylonMines;
    this.colonialMiningImage.IsRendered = this.Desc.HasColonialMines;
    GuiImage guiImage = this.sectorEventImage;
    bool flag = this.Desc.IsAnyDynamicEventActive();
    this.sectorEventBgGlowImage.IsRendered = flag;
    int num = flag ? 1 : 0;
    guiImage.IsRendered = num != 0;
    if ((int) this.Desc.AncientDynamicEvents > 0)
    {
      this.sectorEventImage.TexturePath = "GUI/GalaxyMap/drone_event";
      this.sectorEventBgGlowImage.TexturePath = (string) null;
    }
    else
    {
      this.sectorEventImage.TexturePath = "GUI/GalaxyMap/sector_event";
      this.sectorEventBgGlowImage.TexturePath = "GUI/GalaxyMap/sector_event_bg_glow";
    }
    if (!this.animOverride)
      this.blowAnimation.isPlaying = this.Desc.IsPvPHere;
    this.Size = this.blowAnimation.Size;
  }

  public override bool Contains(float2 point)
  {
    if (!base.Contains(point))
      return this.name.Contains(point);
    return true;
  }

  public float CalculateDistance(GalaxyMapSector galaxyMapSector)
  {
    if (galaxyMapSector == null)
      return 0.0f;
    return (galaxyMapSector.Position - this.Position).magnitude;
  }

  public bool CanJump(GalaxyMapSector galaxyMapSector)
  {
    if (!Game.Me.Anchored)
      return (double) this.CalculateDistance(galaxyMapSector) <= (double) Mathf.Min(Game.Me.FTLTyliumRange, Game.Me.Stats.FTLRange);
    return false;
  }

  public override bool OnShowTooltip(float2 position)
  {
    if (this == this.FindParent<GalaxyMapSectorsLayout>().SelectedGalaxyMapSector)
      return false;
    return base.OnShowTooltip(position);
  }

  public void PlayAnimation(Color blinkColor)
  {
    if (this.animOverride)
      return;
    this.RemoveChild((GuiElementBase) this.blowAnimation);
    this.blowAnimation = new GuiAnimationSimple((Texture2D) ResourceLoader.Load(this.texturePath), 5U, 5U, blinkColor);
    this.AddChild((GuiElementBase) this.blowAnimation);
    this.blowAnimation.isPlaying = true;
    this.animOverride = true;
  }

  public void StopAnimation()
  {
    if (!this.animOverride)
      return;
    this.RemoveChild((GuiElementBase) this.blowAnimation);
    this.blowAnimation = new GuiAnimationSimple((Texture2D) ResourceLoader.Load(this.texturePath), 5U, 5U);
    this.AddChild((GuiElementBase) this.blowAnimation);
    this.blowAnimation.isPlaying = false;
    this.animOverride = false;
  }

  [Gui2Editor]
  public SectorDesc GetDesc()
  {
    return this.Desc;
  }
}
