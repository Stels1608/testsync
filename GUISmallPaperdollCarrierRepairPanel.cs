﻿// Decompiled with JetBrains decompiler
// Type: GUISmallPaperdollCarrierRepairPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.DamageWindow;
using System;
using UnityEngine;

public class GUISmallPaperdollCarrierRepairPanel : GuiPanel
{
  private GuiButton repairAllButton = new GuiButton("%$bgo.etc.dw_repair_all%");
  private readonly GuiLabel text = new GuiLabel("%$bgo.inflight_shop.hull%", Gui.Options.FontBGM_BT, 11);
  private readonly GuiLabel text2 = new GuiLabel(string.Empty, Gui.Options.FontBGM_BT, 11);
  private readonly GuiHealthbar healthbar = new GuiHealthbar();
  private RepairAllDialog repairAllDialog;

  public GUISmallPaperdollCarrierRepairPanel()
  {
    this.Size = new Vector2(128f, 128f);
    this.MouseTransparent = true;
    this.AddChild((GuiElementBase) this.text, Align.UpCenter, new Vector2(0.0f, -10f));
    this.AddChild((GuiElementBase) this.text2, Align.UpCenter, new Vector2(0.0f, 5f));
    this.AddChild((GuiElementBase) this.healthbar, Align.UpCenter, new Vector2(0.0f, 20f));
    this.healthbar.Size = new Vector2(60f, 8f);
    this.repairAllButton.OnClick = (AnonymousDelegate) (() =>
    {
      this.repairAllDialog = new RepairAllDialog();
      this.ShowModalDialog((GuiModalDialog) this.repairAllDialog);
      this.RecalculateAbsCoords();
    });
    this.repairAllButton.SizeX = (float) (TextUtility.CalcTextSize(this.repairAllButton.Label.TextParsed, this.repairAllButton.Font, this.repairAllButton.FontSize).width + 30);
    this.AddChild((GuiElementBase) this.repairAllButton, Align.UpCenter, new Vector2(0.0f, 40f));
    this.RecalculateAbsCoords();
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    this.Position = Game.GUIManager.Find<GUISmallPaperdoll>().SmartRect.AbsPosition.ToV2() - new Vector2((float) ((double) this.SizeX / 2.0 - 5.0), 0.0f);
    this.IsRendered = Game.Me.Anchored;
    HangarShip activeShip = Game.Me.ActiveShip;
    this.text2.Text = ((int) ((double) activeShip.Quality * 100.0)).ToString() + "%";
    this.healthbar.Progress = activeShip.Quality;
    this.healthbar.SetTooltip("%$bgo.inflight_shop.durability% " + (object) Math.Round((double) activeShip.Durability) + "/" + (object) Math.Round((double) activeShip.Card.Durability));
  }

  public override void RecalculateAbsCoords()
  {
    if (this.repairAllDialog != null)
    {
      this.repairAllDialog.PositionX -= (float) ((double) this.repairAllDialog.Rect.xMin - (double) this.Rect.xMin);
      this.repairAllDialog.PositionY -= (float) ((double) this.repairAllDialog.Rect.yMin - (double) this.Rect.yMin);
      RepairAllDialog repairAllDialog1 = this.repairAllDialog;
      Vector2 vector2_1 = repairAllDialog1.Position - this.Position;
      repairAllDialog1.Position = vector2_1;
      RepairAllDialog repairAllDialog2 = this.repairAllDialog;
      Vector2 vector2_2 = repairAllDialog2.Position + new Vector2((float) (((double) Screen.width - (double) this.repairAllDialog.SizeX) / 2.0), (float) (((double) Screen.height - (double) this.repairAllDialog.SizeY) / 2.0));
      repairAllDialog2.Position = vector2_2;
    }
    base.RecalculateAbsCoords();
  }

  public override bool Contains(float2 point)
  {
    if (this.repairAllDialog != null && this.repairAllDialog.Contains(point))
      return true;
    return base.Contains(point);
  }
}
