﻿// Decompiled with JetBrains decompiler
// Type: DebugProtocolView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class DebugProtocolView : DebugBehaviour<DebugProtocolView>
{
  private Vector2 scroll = new Vector2();
  private string info = string.Empty;

  private void Start()
  {
    this.windowID = 13;
    this.SetSize(300f, 400f);
  }

  protected override void WindowFunc()
  {
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    if (GUILayout.Button("Reset"))
    {
      using (Dictionary<BgoProtocol.ProtocolID, BgoProtocol>.Enumerator enumerator = Game.GetProtocolManager().DebugGetProtocols().GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Value.protocolCounts.Clear();
      }
    }
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.Box(this.info);
    GUILayout.EndScrollView();
  }

  private void Update()
  {
    List<string> stringList = new List<string>();
    using (Dictionary<BgoProtocol.ProtocolID, BgoProtocol>.Enumerator enumerator1 = Game.GetProtocolManager().DebugGetProtocols().GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<BgoProtocol.ProtocolID, BgoProtocol> current1 = enumerator1.Current;
        using (Dictionary<Enum, uint>.Enumerator enumerator2 = current1.Value.protocolCounts.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            KeyValuePair<Enum, uint> current2 = enumerator2.Current;
            string str = string.Format("{0}:{1} {2}", (object) current1.Key.ToString(), (object) current2.Key.ToString(), (object) current2.Value);
            stringList.Add(str);
          }
        }
      }
    }
    stringList.Sort();
    StringBuilder stringBuilder = new StringBuilder();
    using (List<string>.Enumerator enumerator = stringList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        stringBuilder.AppendLine(current);
      }
    }
    this.info = stringBuilder.ToString();
  }
}
