﻿// Decompiled with JetBrains decompiler
// Type: Weaponry
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class Weaponry : MultiLODListener
{
  private int flakLOD = 1;
  private readonly Dictionary<ushort, List<Weapon>> weaponCache = new Dictionary<ushort, List<Weapon>>();
  public float TargetLimit;
  public GunSoundPlayer GunSoundPlayer;
  public GunSoundPlayer MachineGunSoundPlayer;
  private Ship ship;
  private Transform mainCamera;
  private float sqrTargetLimit;
  private bool playerCommandsCarrier;

  protected override void Awake()
  {
    base.Awake();
    this.mainCamera = Camera.main.transform;
    this.sqrTargetLimit = this.TargetLimit * this.TargetLimit;
  }

  protected override void OnSpaceObjectInjected()
  {
    base.OnSpaceObjectInjected();
    this.ship = this.SpaceObject as Ship;
  }

  public void FireMissile(ushort objectPointHash)
  {
    this.Fire((SpaceObject) null, objectPointHash, WeaponFxType.MissileLauncher);
  }

  public void Fire(SpaceObject target, ushort objectPointHash, WeaponFxType weaponFxType)
  {
    if (this.SpaceObject == null)
      return;
    bool flag1 = false;
    this.playerCommandsCarrier = this.PlayerCommandsCarrier();
    switch (weaponFxType)
    {
      case WeaponFxType.Gun:
      case WeaponFxType.MachineGun:
      case WeaponFxType.Flechete:
      case WeaponFxType.Railgun:
        bool flag2 = false;
        if (target != null && (Object) this.mainCamera != (Object) null && (double) (target.Position - this.mainCamera.position).sqrMagnitude < (double) this.sqrTargetLimit)
          flag2 = true;
        flag1 = this.Level <= 1 || flag2 || this.playerCommandsCarrier;
        break;
      case WeaponFxType.MissileLauncher:
        flag1 = this.Level == 0 || this.playerCommandsCarrier;
        break;
      case WeaponFxType.PointDefence:
      case WeaponFxType.Flak:
      case WeaponFxType.Shrapnel:
      case WeaponFxType.SpotFlak:
      case WeaponFxType.AoEFlak:
        flag1 = this.Level <= this.flakLOD || this.playerCommandsCarrier;
        break;
    }
    if (!flag1)
      return;
    if (!this.weaponCache.ContainsKey(objectPointHash))
      this.CreateAndCacheWeapon(objectPointHash, weaponFxType);
    this.TriggerWeapons(this.weaponCache[objectPointHash], target, weaponFxType);
  }

  private void TriggerWeapons(List<Weapon> weapons, SpaceObject target, WeaponFxType weaponFxType)
  {
    using (List<Weapon>.Enumerator enumerator = weapons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Weapon current = enumerator.Current;
        current.HighQuality = weaponFxType != WeaponFxType.Flak ? this.Level == 0 || this.playerCommandsCarrier : this.Level <= this.flakLOD;
        current.Fire(target);
      }
    }
  }

  private void CreateAndCacheWeapon(ushort objectPointHash, WeaponFxType weaponFxType)
  {
    if (this.weaponCache.ContainsKey(objectPointHash))
    {
      Debug.LogError((object) ("Weapon of another type has been already instantiated at this spot: " + (object) objectPointHash));
    }
    else
    {
      List<Weapon> weaponList = new List<Weapon>();
      FixingSpot fixingSpot = this.ship.GetObjectPoint(objectPointHash) as FixingSpot;
      if (fixingSpot != null)
      {
        if (fixingSpot.Transform.childCount == 0)
        {
          Weapon weapon = this.CreateWeapon(weaponFxType, fixingSpot.Transform, 0);
          weaponList.Add(weapon);
          if ((Object) weapon == (Object) null)
            Debug.LogError((object) "Weapon was not found and could not be instantiated");
        }
        else
        {
          for (int index = 0; index < fixingSpot.Transform.childCount; ++index)
          {
            Transform child = fixingSpot.Transform.GetChild(index);
            Weapon weapon = this.CreateWeapon(weaponFxType, child, index);
            weaponList.Add(weapon);
            if ((Object) weapon == (Object) null)
              Debug.LogError((object) "Weapon was not found and could not be instantiated");
          }
        }
      }
      this.weaponCache.Add(objectPointHash, weaponList);
    }
  }

  private bool PlayerCommandsCarrier()
  {
    bool flag = false;
    if ((Object) SpaceLevel.GetLevel() != (Object) null && this.ship != null && (this.ship == SpaceLevel.GetLevel().GetPlayerShip() && this.ship.ShipCardLight != null) && this.ship.ShipCardLight.ShipRoleDeprecated == ShipRoleDeprecated.Carrier)
      flag = true;
    return flag;
  }

  public Weapon CreateWeapon(WeaponFxType weaponFxType, Transform weaponLocation, int weaponLocationId = 0)
  {
    string path = (string) null;
    switch (weaponFxType)
    {
      case WeaponFxType.Gun:
        switch (this.ship.Faction)
        {
          case Faction.Colonial:
            switch (this.ship.ShipCardLight.Tier)
            {
              case 1:
                path = "Fx/Guns/ColonialSmallGun";
                break;
              case 2:
                path = "Fx/Guns/ColonialMediumGun";
                break;
              case 3:
                path = "Fx/Guns/ColonialLargeGun";
                break;
              case 4:
                path = "Fx/Guns/ColonialXLargeGun";
                break;
            }
          case Faction.Cylon:
            switch (this.ship.ShipCardLight.Tier)
            {
              case 1:
                path = "Fx/Guns/CylonSmallGun";
                break;
              case 2:
                path = "Fx/Guns/CylonMediumGun";
                break;
              case 3:
                path = "Fx/Guns/CylonLargeGun";
                break;
              case 4:
                path = "Fx/Guns/CylonXLargeGun";
                break;
            }
          case Faction.Ancient:
            switch (this.ship.ShipCardLight.Tier)
            {
              case 1:
                path = "Fx/Guns/AncientSmallGun";
                break;
              case 2:
                path = "Fx/Guns/AncientMediumGun";
                break;
              case 3:
              case 4:
                path = "Fx/Guns/AncientLargeGun";
                break;
            }
        }
      case WeaponFxType.MissileLauncher:
        switch (this.ship.Faction)
        {
          case Faction.Colonial:
            switch (this.ship.ShipCardLight.Tier)
            {
              case 1:
                path = "Fx/MissileLauncher/ColonialSmallMissileLauncher";
                break;
              case 2:
                path = "Fx/MissileLauncher/ColonialMediumMissileLauncher";
                break;
              case 3:
              case 4:
                path = "Fx/MissileLauncher/ColonialLargeMissileLauncher";
                break;
            }
          case Faction.Cylon:
            switch (this.ship.ShipCardLight.Tier)
            {
              case 1:
                path = "Fx/MissileLauncher/CylonSmallMissileLauncher";
                break;
              case 2:
                path = "Fx/MissileLauncher/CylonMediumMissileLauncher";
                break;
              case 3:
              case 4:
                path = "Fx/MissileLauncher/CylonLargeMissileLauncher";
                break;
            }
          case Faction.Ancient:
            switch (this.ship.ShipCardLight.Tier)
            {
              case 1:
                path = "Fx/MissileLauncher/AncientSmallMissileLauncher";
                break;
              case 2:
                path = "Fx/MissileLauncher/AncientMediumMissileLauncher";
                break;
              case 3:
              case 4:
                path = "Fx/MissileLauncher/AncientLargeMissileLauncher";
                break;
            }
        }
      case WeaponFxType.PointDefence:
        switch (this.ship.Faction)
        {
          case Faction.Colonial:
            path = (int) this.ship.ShipCardLight.Tier == 4 ? (this.ship.ShipCardLight.ShipRoleDeprecated != ShipRoleDeprecated.Mothership ? "Fx/PointDefence/ColonialXLargePointDefence" : "Fx/PointDefence/ColonialMothershipPointDefence") : "Fx/PointDefence/ColonialPointDefence";
            break;
          case Faction.Cylon:
            path = (int) this.ship.ShipCardLight.Tier == 4 ? (this.ship.ShipCardLight.ShipRoleDeprecated != ShipRoleDeprecated.Mothership ? "Fx/PointDefence/CylonXLargePointDefence" : "Fx/PointDefence/CylonMothershipPointDefence") : "Fx/PointDefence/CylonPointDefence";
            break;
          case Faction.Ancient:
            path = (int) this.ship.ShipCardLight.Tier == 4 ? "Fx/PointDefence/AncientXLargePointDefence" : "Fx/PointDefence/AncientPointDefence";
            break;
        }
      case WeaponFxType.Flak:
        switch (this.ship.Faction)
        {
          case Faction.Colonial:
            path = "Fx/Flak/ColonialFlak";
            break;
          case Faction.Cylon:
            path = "Fx/Flak/CylonFlak";
            break;
          case Faction.Ancient:
            path = "Fx/Flak/AncientFlak";
            break;
        }
      case WeaponFxType.Shrapnel:
        path = "Fx/Guns/ShrapnelGun";
        break;
      case WeaponFxType.SpotFlak:
        switch (this.ship.Faction)
        {
          case Faction.Colonial:
            path = "Fx/Flak/ColonialSpotFlak";
            break;
          case Faction.Cylon:
            path = "Fx/Flak/CylonSpotFlak";
            break;
          case Faction.Ancient:
            path = "Fx/Flak/AncientSpotFlak";
            break;
        }
      case WeaponFxType.AoEFlak:
        switch (this.ship.Faction)
        {
          case Faction.Colonial:
            path = "Fx/Flak/ColonialAoeFlak";
            break;
          case Faction.Cylon:
            path = "Fx/Flak/CylonAoeFlak";
            break;
          case Faction.Ancient:
            path = "Fx/Flak/AncientAoeFlak";
            break;
        }
      case WeaponFxType.MachineGun:
        switch (this.ship.Faction)
        {
          case Faction.Colonial:
            path = "Fx/Guns/ColonialSmallMachineGun";
            break;
          default:
            path = "Fx/Guns/CylonSmallMachineGun";
            break;
        }
      case WeaponFxType.Flechete:
        switch (this.ship.Faction)
        {
          case Faction.Colonial:
            path = "Fx/Guns/ColonialSmallFlechetteCannon";
            break;
          default:
            path = "Fx/Guns/CylonSmallFlechetteCannon";
            break;
        }
      case WeaponFxType.Railgun:
        switch (this.ship.Faction)
        {
          case Faction.Colonial:
            switch (this.ship.ShipCardLight.Tier)
            {
              case 1:
              case 2:
                path = "Fx/Guns/ColonialSmallRailgun";
                break;
              case 3:
              case 4:
                path = "Fx/Guns/ColonialXLargeRailgun";
                break;
            }
          case Faction.Cylon:
            switch (this.ship.ShipCardLight.Tier)
            {
              case 1:
              case 2:
                path = "Fx/Guns/CylonSmallRailgun";
                break;
              case 3:
              case 4:
                path = "Fx/Guns/CylonXLargeRailgun";
                break;
            }
          default:
            path = "Fx/Guns/CylonSmallRailgun";
            break;
        }
    }
    GameObject original = Resources.Load(path) as GameObject;
    Weapon weapon = (Weapon) null;
    if ((Object) original != (Object) null)
    {
      weapon = Object.Instantiate<GameObject>(original).GetComponent<Weapon>();
      weapon.name = original.name;
      weapon.WeaponLocationId = weaponLocationId;
      this.MountWeaponAtTransform(weapon, weaponLocation);
      if (weapon is BurstGun)
      {
        switch ((weapon as BurstGun).BurstGunType)
        {
          case BurstGunType.MachineGun:
            (weapon as BurstGun).GunSoundPlayer = this.MachineGunSoundPlayer;
            break;
          default:
            (weapon as BurstGun).GunSoundPlayer = this.GunSoundPlayer;
            break;
        }
      }
    }
    else
      Debug.LogError((object) ("Weapon resource " + path + " was not found"));
    return weapon;
  }

  private void MountWeaponAtTransform(Weapon weapon, Transform weaponTargetTransform)
  {
    weapon.transform.parent = weaponTargetTransform;
    weapon.transform.localPosition = Vector3.zero;
    weapon.transform.localRotation = Quaternion.identity;
    weapon.transform.localScale = Vector3.one;
  }

  protected override void Reset()
  {
    this.LevelLimits = new float[2];
  }
}
