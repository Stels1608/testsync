﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorSectorEventNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HudIndicatorSectorEventNgui : HudIndicatorBaseComponentNgui, IReceivesTarget
{
  [SerializeField]
  private UISprite sectorEventIcon;
  protected SectorEvent sectorEvent;

  protected UIAtlas Atlas
  {
    get
    {
      return HudIndicatorsAtlasLinker.Instance.Atlas;
    }
  }

  public UISprite SectorEventIcon
  {
    get
    {
      return this.sectorEventIcon;
    }
  }

  public override bool RequiresAnimationNgui
  {
    get
    {
      return false;
    }
  }

  protected override void ResetComponentStates()
  {
  }

  protected override void Awake()
  {
    base.Awake();
    this.sectorEventIcon = NGUITools.AddSprite(this.gameObject, (UIAtlas) null, (string) null);
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.sectorEvent = target as SectorEvent;
    if (this.sectorEvent == null)
      Debug.LogError((object) "SectorEvent component set to indicator of Non-SectorEvent SpaceObject");
    else
      NGUIToolsExtension.SetSprite(this.sectorEventIcon, this.Atlas, this.sectorEvent.SectorEventCard.IconNguiHudIndicator);
  }

  protected override void UpdateView()
  {
    this.gameObject.SetActive(this.InScreen);
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }
}
