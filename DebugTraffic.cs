﻿// Decompiled with JetBrains decompiler
// Type: DebugTraffic
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class DebugTraffic : DebugBehaviour<DebugTraffic>
{
  private float debugDeltaTime = 10f;
  private string info = string.Empty;
  private string protocolIdLabel = string.Empty;
  private string messageIdLabel = string.Empty;
  private string countLabel = string.Empty;
  private uint debugReceiveDataFixed;
  private float debugReceiveDataDelta;
  private uint debugSendDataFixed;
  private float debugSendDataDelta;
  private float debugFixedTime;
  private uint debugReceiveData;
  private uint debugSendData;
  private Vector2 scrollPos;

  private void Start()
  {
    this.windowID = 0;
    this.windowRect = new Rect((float) ((Screen.width - 600) / 2), 0.0f, 600f, 400f);
  }

  protected override void WindowFunc()
  {
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    GUILayout.Box(this.info);
    this.scrollPos = GUILayout.BeginScrollView(this.scrollPos);
    GUILayout.BeginHorizontal();
    GUILayout.Label(this.protocolIdLabel);
    GUILayout.Label(this.messageIdLabel);
    GUILayout.Label(this.countLabel);
    GUILayout.EndHorizontal();
    GUILayout.EndScrollView();
  }

  private string GetFormatStr(uint data)
  {
    uint num1 = data / 1000000U;
    uint num2 = data % 1000000U / 1000U;
    uint num3 = data % 1000U;
    if ((int) num1 != 0)
      return string.Format("{0:000} {1:000} {2,000}", (object) num1, (object) num2, (object) num3);
    if ((int) num2 == 0)
      return string.Format("{0:000}", (object) num3);
    return string.Format("{0:000} {1:000}", (object) num2, (object) num3);
  }

  private void Update()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    DebugTraffic.\u003CUpdate\u003Ec__AnonStorey63 updateCAnonStorey63 = new DebugTraffic.\u003CUpdate\u003Ec__AnonStorey63();
    // ISSUE: reference to a compiler-generated field
    updateCAnonStorey63.connection = Game.GetProtocolManager().DebugGetConnection();
    // ISSUE: reference to a compiler-generated field
    if (updateCAnonStorey63.connection == null)
      return;
    // ISSUE: reference to a compiler-generated field
    this.debugReceiveData = updateCAnonStorey63.connection.debugReceiveData;
    // ISSUE: reference to a compiler-generated field
    this.debugSendData = updateCAnonStorey63.connection.debugSendData;
    if ((double) Time.time > (double) this.debugFixedTime + (double) this.debugDeltaTime)
    {
      this.debugReceiveDataDelta = (float) (this.debugReceiveData - this.debugReceiveDataFixed) / this.debugDeltaTime;
      this.debugSendDataDelta = (float) (this.debugSendData - this.debugSendDataFixed) / this.debugDeltaTime;
      this.debugFixedTime = Time.time;
      // ISSUE: reference to a compiler-generated field
      this.debugReceiveDataFixed = updateCAnonStorey63.connection.debugReceiveData;
      // ISSUE: reference to a compiler-generated field
      this.debugSendDataFixed = updateCAnonStorey63.connection.debugSendData;
    }
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    this.info = string.Format((IFormatProvider) new NumberFormatInfo()
    {
      NumberDecimalSeparator = "."
    }, "Receive: {0} b\nSend: {1} b\nReceive: {2:0} b/s\nSend: {3:0} b/s\nmax R: {4}; max S: {5}", (object) this.GetFormatStr(this.debugReceiveData), (object) this.GetFormatStr(this.debugSendData), (object) this.debugReceiveDataDelta, (object) this.debugSendDataDelta, (object) updateCAnonStorey63.connection.debugReceiveDataMax, (object) updateCAnonStorey63.connection.debugSendDataMax);
    this.protocolIdLabel = "Protocol\n";
    this.messageIdLabel = "Message Id\n";
    this.countLabel = "Count\n";
    // ISSUE: reference to a compiler-generated field
    List<int> intList = new List<int>(updateCAnonStorey63.connection.MessageReceivedKeys);
    // ISSUE: reference to a compiler-generated method
    intList.Sort(new Comparison<int>(updateCAnonStorey63.\u003C\u003Em__36));
    using (List<int>.Enumerator enumerator = intList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        Tuple<BgoProtocol.ProtocolID, ushort> messageId = Connection.GetMessageId(current);
        DebugTraffic debugTraffic1 = this;
        string str1 = debugTraffic1.protocolIdLabel + (object) messageId.First + "\n";
        debugTraffic1.protocolIdLabel = str1;
        DebugTraffic debugTraffic2 = this;
        string str2 = debugTraffic2.messageIdLabel + (object) messageId.Second + "\n";
        debugTraffic2.messageIdLabel = str2;
        DebugTraffic debugTraffic3 = this;
        // ISSUE: reference to a compiler-generated field
        string str3 = debugTraffic3.countLabel + (object) updateCAnonStorey63.connection.GetMessageReceivedCount(current) + "\n";
        debugTraffic3.countLabel = str3;
      }
    }
  }
}
