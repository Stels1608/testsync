﻿// Decompiled with JetBrains decompiler
// Type: WindowTitleBarWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WindowTitleBarWidget : NguiWidget, ILocalizeable
{
  [SerializeField]
  private string windowTitle = "Untitled";
  [SerializeField]
  private UILabel titleLabel;
  [SerializeField]
  private ContentWindowWidget window;
  public UISprite titleBarBackground;
  public UISprite stripeBar;

  public string WindowTitle
  {
    get
    {
      return this.windowTitle;
    }
    set
    {
      this.windowTitle = value;
      if (!((Object) this.titleLabel != (Object) null))
        return;
      this.titleLabel.text = this.windowTitle;
    }
  }

  public UILabel TitleLabel
  {
    get
    {
      return this.titleLabel;
    }
    set
    {
      this.titleLabel = value;
    }
  }

  public ContentWindowWidget Window
  {
    get
    {
      return this.window;
    }
    set
    {
      this.window = value;
    }
  }

  public override void Start()
  {
    base.Start();
    if (!((Object) null == (Object) this.Window))
      return;
    this.Window = this.transform.parent.GetComponent<ContentWindowWidget>();
  }

  public override void OnPress(bool isDown)
  {
    base.OnPress(isDown);
    if (isDown && (Object) null != (Object) this.window)
    {
      this.Window.BringToFront();
    }
    else
    {
      if (!((Object) this.window != (Object) null))
        return;
      Vector2 vector2 = PositionUtils.GetScreenPixelSize(this.window.gameObject, true) * 0.3f;
      PositionUtils.HoldWithinScreen(this.window.gameObject, 0.0f, vector2.y, vector2.x, vector2.x, false);
    }
  }

  public void ReloadLanguageData()
  {
  }
}
