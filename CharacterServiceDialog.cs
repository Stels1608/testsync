﻿// Decompiled with JetBrains decompiler
// Type: CharacterServiceDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public abstract class CharacterServiceDialog : MonoBehaviour
{
  public Button confirmButton;
  public Button cancelButton;
  public Text titleLabel;
  public Text upperMessageLabel;
  public Text lowerMessageLabel;
  public GameObject cubitsArea;
  public Text cubitsBalanceLabel;
  public Text cubitsBuyPromptLabel;
  public Button cubitsBuyButton;
  protected CharacterService characterService;

  private void Awake()
  {
    this.RegisterUiEventHandlers();
    if (!((Object) this.cubitsBuyButton != (Object) null))
      return;
    this.cubitsBuyButton.onClick.AddListener(new UnityAction(ShopWindow.OnGetCubits));
  }

  protected abstract void RegisterUiEventHandlers();

  protected void Localize(string locaPrefix)
  {
    this.titleLabel.text = BsgoLocalization.Get("bgo.character_menu." + locaPrefix + ".title").ToUpperInvariant();
    this.upperMessageLabel.text = BsgoLocalization.Get("bgo.character_menu." + locaPrefix + ".message");
    if (this.characterService == null)
      return;
    uint cooldownInDays = this.characterService.CooldownInDays;
    if (this.characterService.CubitsPrice == 0)
    {
      this.ShowCubits(false);
      this.lowerMessageLabel.text = BsgoLocalization.Get("bgo.character_menu." + locaPrefix + ".free", (object) cooldownInDays);
    }
    else
    {
      this.ShowCubits(true);
      this.lowerMessageLabel.text = BsgoLocalization.Get("bgo.character_menu." + locaPrefix + ".info", (object) cooldownInDays, (object) this.characterService.CubitsPrice);
    }
    this.OnCubitsBalanceChanged();
  }

  public void OnCubitsBalanceChanged()
  {
    if (this.characterService.CanAfford)
    {
      this.cubitsBalanceLabel.text = BsgoLocalization.Get("bgo.character_menu.currency_balance", (object) ("<color=green>" + (object) Game.Me.Hold.Cubits + "</color>"));
      this.cubitsBuyPromptLabel.gameObject.SetActive(false);
      this.cubitsBuyButton.gameObject.SetActive(false);
      this.confirmButton.interactable = true;
    }
    else
    {
      this.cubitsBalanceLabel.text = BsgoLocalization.Get("bgo.character_menu.currency_balance", (object) ("<color=red>" + (object) Game.Me.Hold.Cubits + "</color>"));
      this.cubitsBuyPromptLabel.gameObject.SetActive(true);
      this.cubitsBuyButton.gameObject.SetActive(true);
      this.cubitsBuyPromptLabel.text = BsgoLocalization.Get("bgo.character_menu.currency_buy");
      this.confirmButton.interactable = false;
    }
  }

  private void ShowCubits(bool show)
  {
    this.cubitsArea.SetActive(show);
  }

  protected abstract void OnCharacterServiceInjected();

  public void SetCharacterService(CharacterService service)
  {
    this.characterService = service;
    this.OnCharacterServiceInjected();
  }
}
