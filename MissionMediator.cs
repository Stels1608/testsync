﻿// Decompiled with JetBrains decompiler
// Type: MissionMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public class MissionMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "MissionMediator";
  private GuiMissions missionLog;
  private AssignmentSummaryWindowUgui assignmentSummary;

  private bool ShowAssignmentTracker
  {
    get
    {
      if (GameLevel.Instance.LevelType == GameLevelType.SpaceLevel)
      {
        SpaceLevel level = SpaceLevel.GetLevel();
        if (level.IsTournament || level.IsStory || level.IsTeaser)
          return false;
      }
      return true;
    }
  }

  public MissionMediator()
    : base("MissionMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.MissionListUpdated);
    this.AddMessageInterest(Message.ShowSelectedMission);
    this.AddMessageInterest(Message.LevelLoaded);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.MissionHandIn);
    this.AddMessageInterest(Message.MissionHandInAll);
    this.AddMessageInterest(Message.TrackMission);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.MissionListUpdated:
        this.UpdateMissionList((MissionList) message.Data);
        break;
      case Message.ShowSelectedMission:
        this.ShowSelectedMission((ushort) message.Data);
        break;
      case Message.TrackMission:
        this.assignmentSummary.TrackMission((Mission) message.Data);
        break;
      case Message.LoadNewLevel:
        if (!((Object) this.assignmentSummary == (Object) null))
          break;
        this.assignmentSummary = this.CreateAssignmentWindow();
        break;
      case Message.LevelLoaded:
        this.OnLevelLoaded((GameLevel) message.Data);
        break;
      case Message.MissionHandIn:
        this.HandInMission((ushort) message.Data);
        break;
      case Message.MissionHandInAll:
        using (IEnumerator<Mission> enumerator = Game.Me.MissionBook.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Mission current = enumerator.Current;
            if (current.Status == Mission.State.Completed)
              this.HandInMission(current.ServerID);
          }
          break;
        }
    }
  }

  private void OnLevelLoaded(GameLevel level)
  {
    this.FetchViews();
    this.UpdateMissionTrackerRenderState(level);
    this.UpdateMissionList(Game.Me.MissionBook);
  }

  private void UpdateMissionTrackerRenderState(GameLevel level)
  {
    this.assignmentSummary.IsRendered = this.ShowAssignmentTracker;
  }

  private void HandInMission(ushort missionId)
  {
    PlayerProtocol.GetProtocol().SubmitMission(missionId);
  }

  private void ShowSelectedMission(ushort missionId)
  {
    GUICharacterStatusWindow characterStatusWindow = Game.GUIManager.Find<GUICharacterStatusWindow>();
    characterStatusWindow.OpenMissions();
    characterStatusWindow.IsRendered = true;
    this.missionLog.SelectAll();
    this.missionLog.SelectItem(missionId);
  }

  private void UpdateMissionList(MissionList missionList)
  {
    if (this.missionLog != null)
      this.missionLog.UpdateMissionList(missionList);
    if (!((Object) this.assignmentSummary != (Object) null))
      return;
    this.assignmentSummary.SetMissions(missionList);
  }

  private void FetchViews()
  {
    GUICharacterStatusWindow characterStatusWindow = Game.GUIManager.Find<GUICharacterStatusWindow>();
    if (characterStatusWindow == null)
      return;
    this.missionLog = characterStatusWindow.GetMissionsPanel();
  }

  private AssignmentSummaryWindowUgui CreateAssignmentWindow()
  {
    AssignmentSummaryWindowUgui child = UguiTools.CreateChild<AssignmentSummaryWindowUgui>("AccordeonSidebarRight/AssignmentSummaryUgui", AccordeonSidebarRightUgui.Instance.AssignmentListRoot.transform);
    child.IsRendered = false;
    child.Collapsed = true;
    return child;
  }
}
