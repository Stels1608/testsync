﻿// Decompiled with JetBrains decompiler
// Type: FastTrail
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class FastTrail : MonoBehaviour
{
  public bool emit = true;
  public float lifeTime = 0.5f;
  private float lifeTimeRatio = 1f;
  public float maxAngle = 2f;
  public float minVertexDistance = 0.1f;
  public float maxVertexDistance = 1f;
  private FastTrail.Point[] points = new FastTrail.Point[200];
  private Vector3[] vertices = new Vector3[400];
  private Vector2[] uv = new Vector2[400];
  private Vector2[] uv2 = new Vector2[400];
  private int[] triangles = new int[1194];
  public int maxPointCount = 199;
  private const int COUNT = 200;
  public Material material;
  private Material trailMaterial;
  public Color[] colors;
  public float width;
  private GameObject trailObj;
  private Mesh mesh;
  private int pointCnt;
  private int lastPointCnt;
  private Transform _transform;
  private Renderer trailRenderer;

  private void Awake()
  {
    this._transform = this.transform;
  }

  private void Start()
  {
    this.trailObj = new GameObject("FastTrail");
    this.trailObj.AddComponent<FadingOut>();
    this.trailObj.transform.parent = (Transform) null;
    this.trailObj.transform.position = Vector3.zero;
    this.trailObj.transform.rotation = Quaternion.identity;
    this.trailObj.transform.localScale = Vector3.one;
    this.mesh = this.trailObj.AddComponent<MeshFilter>().mesh;
    this.trailObj.AddComponent(typeof (MeshRenderer));
    this.trailMaterial = new Material(this.material);
    this.trailRenderer = this.trailObj.GetComponent<Renderer>();
    this.trailRenderer.material = this.trailMaterial;
  }

  private void OnDisable()
  {
    if (!((UnityEngine.Object) this.trailObj != (UnityEngine.Object) null))
      return;
    this.trailObj.SendMessage("FadeOut", (object) this.lifeTime);
  }

  private void LateUpdate()
  {
    if (this.pointCnt == this.maxPointCount)
    {
      this.points[this.pointCnt] = (FastTrail.Point) null;
      --this.pointCnt;
    }
    else
    {
      for (int index = this.pointCnt - 1; index >= 0 && (double) this.points[index].timeAlive > (double) this.lifeTime; --index)
      {
        this.points[index] = (FastTrail.Point) null;
        --this.pointCnt;
      }
    }
    if (this.emit)
    {
      if (this.pointCnt == 0)
      {
        this.points[this.pointCnt++] = new FastTrail.Point(this._transform);
        this.points[this.pointCnt++] = new FastTrail.Point(this._transform);
      }
      if (this.pointCnt == 1)
        this.insertPoint();
      bool flag = false;
      float sqrMagnitude = (this.points[1].position - this._transform.position).sqrMagnitude;
      if ((double) sqrMagnitude > (double) this.minVertexDistance * (double) this.minVertexDistance)
      {
        if ((double) sqrMagnitude > (double) this.maxVertexDistance * (double) this.maxVertexDistance)
          flag = true;
        else if ((double) Quaternion.Angle(this._transform.rotation, this.points[1].rotation) > (double) this.maxAngle)
          flag = true;
      }
      if (flag)
      {
        if (this.pointCnt < this.maxPointCount)
          this.insertPoint();
        else
          flag = false;
      }
      if (!flag)
        this.points[0].update(this._transform);
    }
    if (this.pointCnt < 2)
    {
      this.trailRenderer.enabled = false;
    }
    else
    {
      this.trailRenderer.enabled = true;
      this.lifeTimeRatio = 1f / this.lifeTime;
      int num1 = 0;
      if (this.pointCnt == 2)
        num1 = 1;
      for (int index = 0; index <= num1; ++index)
      {
        Matrix4x4 matrix4x4 = Matrix4x4.TRS(this.points[index].position, this.points[index].rotation, Vector3.one);
        this.vertices[index * 2] = matrix4x4.MultiplyPoint3x4(new Vector3(this.width * 0.5f, 0.0f, 0.0f));
        this.vertices[index * 2 + 1] = matrix4x4.MultiplyPoint3x4(new Vector3((float) (-(double) this.width * 0.5), 0.0f, 0.0f));
      }
      for (int index1 = this.lastPointCnt; index1 < this.pointCnt; ++index1)
      {
        if (index1 != 0)
        {
          int index2 = (index1 - 1) * 6;
          int num2 = index1 * 2;
          this.triangles[index2] = num2 - 2;
          this.triangles[index2 + 1] = num2 - 1;
          this.triangles[index2 + 2] = num2;
          this.triangles[index2 + 3] = num2 + 1;
          this.triangles[index2 + 4] = num2;
          this.triangles[index2 + 5] = num2 - 1;
        }
      }
      for (int index = 0; index < this.pointCnt; ++index)
      {
        FastTrail.Point point = this.points[index];
        float x1 = (float) index / (float) this.pointCnt;
        this.uv[index * 2] = new Vector2(x1, 0.0f);
        this.uv[index * 2 + 1] = new Vector2(x1, 1f);
        float x2 = point.timeAlive * this.lifeTimeRatio;
        this.uv2[index * 2] = new Vector2(x2, 0.0f);
        this.uv2[index * 2 + 1] = new Vector2(x2, 1f);
      }
      for (int index1 = this.pointCnt; index1 < this.lastPointCnt; ++index1)
      {
        if (index1 > 0)
        {
          int index2 = (index1 - 1) * 6;
          this.triangles[index2] = 0;
          this.triangles[index2 + 1] = 0;
          this.triangles[index2 + 2] = 0;
          this.triangles[index2 + 3] = 0;
          this.triangles[index2 + 4] = 0;
          this.triangles[index2 + 5] = 0;
        }
      }
      this.mesh.Clear();
      this.mesh.vertices = this.vertices;
      this.mesh.uv = this.uv;
      this.mesh.uv2 = this.uv2;
      this.mesh.triangles = this.triangles;
      this.trailMaterial.mainTextureScale = new Vector2((float) this.pointCnt, 1f);
      this.lastPointCnt = this.pointCnt;
    }
  }

  private void insertPoint()
  {
    Array.Copy((Array) this.points, 0, (Array) this.points, 1, this.pointCnt);
    Array.Copy((Array) this.vertices, 0, (Array) this.vertices, 2, this.pointCnt * 2);
    this.points[0] = new FastTrail.Point(this._transform);
    ++this.pointCnt;
  }

  private class Point
  {
    public Vector3 position = Vector3.zero;
    public Quaternion rotation = Quaternion.identity;
    public float timeCreated;

    public float timeAlive
    {
      get
      {
        return Time.time - this.timeCreated;
      }
    }

    public Point(Transform trans)
    {
      this.position = trans.position;
      this.rotation = trans.rotation;
      this.timeCreated = Time.time;
    }

    public void update(Transform trans)
    {
      this.position = trans.position;
      this.rotation = trans.rotation;
      this.timeCreated = Time.time;
    }
  }
}
