﻿// Decompiled with JetBrains decompiler
// Type: OptionsWindowWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class OptionsWindowWidget : ContentWindowWidget
{
  [SerializeField]
  private OptionsCreditContent creditsContent;
  [SerializeField]
  private OptionsGeneralContent generalContent;
  [SerializeField]
  private OptionsHudContent hudContent;
  [SerializeField]
  private OptionsControlsKeyboardContent controlsKeyboardContent;
  [SerializeField]
  private OptionsControlsJoystickContent controlsJoystickContent;
  [SerializeField]
  private OptionsVideoContent videoContent;
  [SerializeField]
  private UILabel creditsLabel;
  [SerializeField]
  private UILabel generalLabel;
  [SerializeField]
  private UILabel hudLabel;
  [SerializeField]
  private UILabel controlsLabel;
  [SerializeField]
  private UILabel videoLabel;
  private UserSettings workingCopy;

  public OptionsWindowWidget()
  {
    this.type = WindowTypes.OptionsWindow;
  }

  public override void Start()
  {
    base.Start();
    Transform childWithString = Utils.FindChildWithString(this.transform.root, "VersionLabel");
    if ((Object) childWithString != (Object) null)
      childWithString.GetComponent<UILabel>().text = Game.Revision;
    this.TitleBar.TitleLabel.text = Tools.ParseMessage("%$bgo.ui.options.windowtitle%").ToUpper();
    this.OnScreenResize();
  }

  public override void OnScreenResize()
  {
    PositionUtils.CenterWindowHorizontally((WindowWidget) this);
  }

  private void CheckForUnsavedChanges()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    OptionsWindowWidget.\u003CCheckForUnsavedChanges\u003Ec__AnonStoreyD3 changesCAnonStoreyD3 = new OptionsWindowWidget.\u003CCheckForUnsavedChanges\u003Ec__AnonStoreyD3();
    // ISSUE: reference to a compiler-generated field
    changesCAnonStoreyD3.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    changesCAnonStoreyD3.options = this.gameObject.GetComponentInChildren<OptionsContent>();
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    if ((Object) changesCAnonStoreyD3.options != (Object) null && changesCAnonStoreyD3.options.changed)
    {
      // ISSUE: reference to a compiler-generated method
      // ISSUE: reference to a compiler-generated method
      DialogBoxFactoryNgui.CreateUnsavedChangesDialogBox(new AnonymousDelegate(changesCAnonStoreyD3.\u003C\u003Em__1F8), new AnonymousDelegate(changesCAnonStoreyD3.\u003C\u003Em__1F9));
    }
    else
      this.Close();
  }

  internal void UpdateBindingButtons(List<IInputBinding> list)
  {
    this.controlsKeyboardContent.UpdateBindingButtons(list);
    this.controlsJoystickContent.UpdateBindingButtons(list);
  }

  public void InjectUserSettings(UserSettings settings)
  {
    this.workingCopy = new UserSettings(settings);
    this.generalContent.UpdateSettings(this.workingCopy);
    this.hudContent.UpdateSettings(this.workingCopy);
    this.controlsKeyboardContent.UpdateSettings(this.workingCopy);
    this.controlsJoystickContent.UpdateSettings(this.workingCopy);
    this.videoContent.UpdateSettings(this.workingCopy);
  }

  public void InjectDelegates(OnSettingsApply applySettings, OnSettingsUndo undoSettingsChanged)
  {
    this.CloseButton.handleClick = new AnonymousDelegate(this.CheckForUnsavedChanges);
    this.generalContent.InjectDelegates(applySettings, undoSettingsChanged);
    this.hudContent.InjectDelegates(applySettings, undoSettingsChanged);
    this.controlsKeyboardContent.InjectSettingDelegates(applySettings, undoSettingsChanged);
    this.controlsJoystickContent.InjectSettingDelegates(applySettings, undoSettingsChanged);
    this.videoContent.InjectDelegates(applySettings, undoSettingsChanged);
  }

  internal void SetDogFightStatus(bool p)
  {
    this.controlsKeyboardContent.SetDogFightStatus(p);
  }

  internal void ToggleJoystickGamepadEnabled(bool p)
  {
    this.controlsJoystickContent.ToggleJoystickGamepadEnabled(p);
  }

  public override void OnWindowOpen()
  {
    base.OnWindowOpen();
    SettingsDataProvider settingsDataProvider = FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider") as SettingsDataProvider;
    this.ToggleJoystickGamepadEnabled(settingsDataProvider.CurrentSettings != null && settingsDataProvider.CurrentSettings.JoystickGamepadEnabled);
    UguiTools.EnableCanvas(BsgoCanvas.HudIndicators, false);
    FacadeFactory.GetInstance().SendMessage(Message.OptionsWindowOpened);
  }

  public override void OnWindowClose()
  {
    base.OnWindowClose();
    UguiTools.EnableCanvas(BsgoCanvas.HudIndicators, true);
    FacadeFactory.GetInstance().SendMessage(Message.OptionsWindowClosed);
  }

  public override void ReloadLanguageData()
  {
    base.ReloadLanguageData();
    this.creditsLabel.text = Tools.ParseMessage("%$bgo.ui.options.credits%");
    this.generalLabel.text = Tools.ParseMessage("%$bgo.ui.options.general%");
    this.hudLabel.text = Tools.ParseMessage("%$bgo.ui.options.hud%");
    this.controlsLabel.text = Tools.ParseMessage("%$bgo.ui.options.control%");
    this.videoLabel.text = Tools.ParseMessage("%$bgo.ui.options.video%");
  }
}
