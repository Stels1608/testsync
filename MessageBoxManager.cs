﻿// Decompiled with JetBrains decompiler
// Type: MessageBoxManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using MessageBox;
using System.Collections.Generic;
using UnityEngine;

public class MessageBoxManager : GuiPanel
{
  private const string defaultOkButtonName = "%$bgo.common.ok%";
  private const string defaultCancelButtonName = "%$bgo.common.cancel%";
  private List<BoxesStackItem> boxesTurn;
  private List<BoxesStackItem> pictureNotifyStack;
  private System.Action<MessageBoxActionType> onAction;
  private MessageBoxBase currentBoxNewUi;

  public MessageBoxManager()
  {
    this.IsRendered = true;
    this.onAction = (System.Action<MessageBoxActionType>) null;
    this.boxesTurn = new List<BoxesStackItem>();
    this.pictureNotifyStack = new List<BoxesStackItem>();
    this.MouseTransparent = true;
    this.SizeX = (float) Screen.width;
    this.SizeY = (float) Screen.height;
  }

  private void OnCloseWindow(MessageBoxActionType actionType)
  {
    this.boxesTurn.Remove(this.boxesTurn[0]);
    if (this.onAction != null)
      this.onAction(actionType);
    this.UpdateCurrentBox();
  }

  private void UpdateCurrentBox()
  {
    if ((UnityEngine.Object) this.currentBoxNewUi != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.currentBoxNewUi.gameObject);
    if (this.boxesTurn.Count == 0)
      return;
    switch (this.boxesTurn[0].msgBoxType)
    {
      case MessageBoxType.BannerBox:
        this.onAction = this.boxesTurn[0].onPressed;
        BannerWindowUi bannerWindow = MessageBoxFactory.CreateBannerWindow();
        bannerWindow.SetContent(new OnMessageBoxClose(this.OnCloseWindow), this.boxesTurn[0].card as BannerCard);
        this.currentBoxNewUi = (MessageBoxBase) bannerWindow;
        break;
      case MessageBoxType.Notify:
        this.onAction = (System.Action<MessageBoxActionType>) null;
        NotifyBoxUi notifyBox = MessageBoxFactory.CreateNotifyBox(BsgoCanvas.Windows);
        notifyBox.SetContent(new OnMessageBoxClose(this.OnCloseWindow), this.boxesTurn[0].titleText, this.boxesTurn[0].mainText, this.boxesTurn[0].okButtonName, 0U);
        this.currentBoxNewUi = (MessageBoxBase) notifyBox;
        break;
      case MessageBoxType.NotifyStack:
        this.onAction = (System.Action<MessageBoxActionType>) null;
        OnMessageBoxClose onClose1 = new OnMessageBoxClose(this.OnCloseWindow) + (OnMessageBoxClose) (actionType => StoryProtocol.GetProtocol().MessageBoxOk());
        RadioMessageBoxUi radioMessageBox1 = MessageBoxFactory.CreateRadioMessageBox();
        radioMessageBox1.SetContent(onClose1, this.pictureNotifyStack);
        this.currentBoxNewUi = (MessageBoxBase) radioMessageBox1;
        break;
      case MessageBoxType.Dialog:
        this.onAction = this.boxesTurn[0].onPressed;
        DialogBoxUi dialogBox = MessageBoxFactory.CreateDialogBox(BsgoCanvas.Windows);
        dialogBox.SetContent(new OnMessageBoxClose(this.OnCloseWindow), this.boxesTurn[0].titleText, this.boxesTurn[0].mainText, this.boxesTurn[0].okButtonName, this.boxesTurn[0].cancelButtonName, this.boxesTurn[0].TimeToLive);
        this.currentBoxNewUi = (MessageBoxBase) dialogBox;
        break;
      case MessageBoxType.Picture:
        this.onAction = this.boxesTurn[0].onPressed;
        OnMessageBoxClose onClose2 = new OnMessageBoxClose(this.OnCloseWindow) + (OnMessageBoxClose) (actionType => StoryProtocol.GetProtocol().MessageBoxOk());
        RadioMessageBoxUi radioMessageBox2 = MessageBoxFactory.CreateRadioMessageBox();
        radioMessageBox2.SetContent(onClose2, this.boxesTurn[0]);
        this.currentBoxNewUi = (MessageBoxBase) radioMessageBox2;
        break;
      case MessageBoxType.Help:
        FacadeFactory.GetInstance().SendMessage(Message.ShowHelpScreen, (object) this.boxesTurn[0].helpType);
        this.onAction = (System.Action<MessageBoxActionType>) null;
        break;
      default:
        return;
    }
    this.RecalculateAbsCoords();
  }

  private BoxesStackItem CreateBoxStackItem(MessageBoxType type, string titleText, bool showOk, string mainText, string okButtonName, string cancelButtonName, System.Action<MessageBoxActionType> handler, uint TimeToLive)
  {
    return this.CreateBoxStackItem(type, titleText, showOk, okButtonName, cancelButtonName, mainText, (string) null, (Card) null, handler, HelpScreenType.First, TimeToLive, string.Empty);
  }

  private BoxesStackItem CreateBoxStackItem(MessageBoxType type, string titleText, bool showOk, string okButtonName, string cancelButtonName, string mainText, string extText, Card card, System.Action<MessageBoxActionType> handler, string imagePath)
  {
    return this.CreateBoxStackItem(type, titleText, showOk, okButtonName, cancelButtonName, mainText, extText, card, handler, HelpScreenType.First, imagePath);
  }

  private BoxesStackItem CreateBoxStackItem(MessageBoxType type, string titleText, bool showOk, string okButtonName, string cancelButtonName, string mainText, string extText, Card card, System.Action<MessageBoxActionType> handler, HelpScreenType helpType, string imagePath)
  {
    return this.CreateBoxStackItem(type, titleText, showOk, okButtonName, cancelButtonName, mainText, extText, card, handler, helpType, 0U, imagePath);
  }

  private BoxesStackItem CreateBoxStackItem(MessageBoxType type, string titleText, bool showOk, string okButtonName, string cancelButtonName, string mainText, string extText, Card card, System.Action<MessageBoxActionType> handler, HelpScreenType helpType, uint TimeToLive, string imagePath)
  {
    return new BoxesStackItem() { msgBoxType = type, titleText = Tools.ParseMessage(titleText), showOk = showOk, okButtonName = Tools.ParseMessage(okButtonName), cancelButtonName = Tools.ParseMessage(cancelButtonName), mainText = Tools.ParseMessage(mainText), extText = Tools.ParseMessage(extText), card = card, onPressed = handler, helpType = helpType, TimeToLive = TimeToLive, extraImage = imagePath };
  }

  [Gui2Editor]
  public MessageBoxBase ShowNotifyBox(string text)
  {
    this.boxesTurn.Add(this.CreateBoxStackItem(MessageBoxType.Notify, BsgoLocalization.Get("%$bgo.message_box.notification_title%"), true, "%$bgo.common.ok%", "%$bgo.common.cancel%", text, (string) null, (Card) null, (System.Action<MessageBoxActionType>) null, string.Empty));
    this.UpdateCurrentBox();
    return this.currentBoxNewUi;
  }

  public void ShowNotifyBox(GuildInviteResult guildInviteResult, string playerName)
  {
    string text = string.Empty;
    switch (guildInviteResult)
    {
      case GuildInviteResult.AlreadyInGuild:
        text = BsgoLocalization.Get("%$bgo.community.guild_already_in_guild%", (object) playerName);
        break;
      case GuildInviteResult.Timeout:
        text = BsgoLocalization.Get("%$bgo.community.guild_invite_accept_3%", (object) playerName);
        break;
      case GuildInviteResult.Refuse:
        text = BsgoLocalization.Get("%$bgo.community.guild_invite_accept_2%", (object) playerName);
        break;
      case GuildInviteResult.Accept:
        text = BsgoLocalization.Get("%$bgo.community.guild_invite_accept_1%", (object) playerName);
        break;
      case GuildInviteResult.FullGuild:
        text = BsgoLocalization.Get("%$bgo.community.guild_full_guild%");
        break;
    }
    this.ShowNotifyBox(text);
  }

  public void ShowPictureNotifyHistory()
  {
    if ((UnityEngine.Object) this.currentBoxNewUi != (UnityEngine.Object) null)
    {
      this.DeleteAllBoxes();
    }
    else
    {
      this.DeleteAllBoxes();
      this.boxesTurn.Add(this.CreateBoxStackItem(MessageBoxType.NotifyStack, string.Empty, true, (string) null, (string) null, (string) null, (string) null, (Card) null, (System.Action<MessageBoxActionType>) null, string.Empty));
      this.UpdateCurrentBox();
    }
  }

  public void ShowDialogBox(string mainText, string titleText, string okButonName, string cancelButtonName, System.Action<MessageBoxActionType> handler)
  {
    this.boxesTurn.Add(this.CreateBoxStackItem(MessageBoxType.Dialog, titleText, true, okButonName, cancelButtonName, mainText, (string) null, (Card) null, handler, string.Empty));
    this.UpdateCurrentBox();
  }

  [Gui2Editor]
  public void ShowDialogBox(string mainText, string titleText, string okButonName, string cancelButtonName, System.Action<MessageBoxActionType> handler, uint TimeToLive)
  {
    this.boxesTurn.Add(this.CreateBoxStackItem(MessageBoxType.Dialog, titleText, true, mainText, okButonName, cancelButtonName, handler, TimeToLive));
    this.UpdateCurrentBox();
  }

  public void ShowBanner(BannerCard bannerCard, System.Action<MessageBoxActionType> handler)
  {
    this.boxesTurn.Add(this.CreateBoxStackItem(MessageBoxType.BannerBox, string.Empty, true, "%$bgo.common.ok%", string.Empty, string.Empty, string.Empty, (Card) bannerCard, handler, string.Empty));
    this.UpdateCurrentBox();
  }

  public void ShowPictureNotify(bool showOk, string mainText, string advice, GUICard card)
  {
    this.boxesTurn.Add(this.CreateBoxStackItem(MessageBoxType.Picture, string.Empty, showOk, "%$bgo.common.ok%", "%$bgo.common.cancel%", mainText, advice, (Card) card, (System.Action<MessageBoxActionType>) null, string.Empty));
    this.pictureNotifyStack.Add(this.boxesTurn[this.boxesTurn.Count - 1]);
    this.UpdateCurrentBox();
  }

  public void ShowPictureNotify(bool showOk, string mainText, string advice, GUICard card, string imagePath)
  {
    this.boxesTurn.Add(this.CreateBoxStackItem(MessageBoxType.Picture, string.Empty, showOk, "%$bgo.common.ok%", "%$bgo.common.cancel%", mainText, advice, (Card) card, (System.Action<MessageBoxActionType>) null, imagePath));
    this.pictureNotifyStack.Add(this.boxesTurn[this.boxesTurn.Count - 1]);
    this.UpdateCurrentBox();
  }

  public void ShowHelpScreen(HelpScreenType type)
  {
    this.boxesTurn.Add(this.CreateBoxStackItem(MessageBoxType.Help, string.Empty, true, string.Empty, string.Empty, string.Empty, string.Empty, (Card) null, (System.Action<MessageBoxActionType>) null, type, string.Empty));
    this.UpdateCurrentBox();
  }

  public void DeleteAllBoxes()
  {
    this.boxesTurn.Clear();
    this.UpdateCurrentBox();
  }
}
