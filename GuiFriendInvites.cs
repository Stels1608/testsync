﻿// Decompiled with JetBrains decompiler
// Type: GuiFriendInvites
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GuiFriendInvites : GuiDialog
{
  private readonly GuiLabel bonus = new GuiLabel(Gui.Options.FontBGM_BT, 12);
  private readonly GuiPanelVerticalScroll scroll = new GuiPanelVerticalScroll();
  private readonly int currentRecruitBonusLevel = -1;
  private float stackOffsetX = 8f;
  private float stackOffsetY = 2f;
  private readonly Texture2D imagePlate;
  private readonly GuiLabel waitForRecruitLevel;

  public GuiFriendInvites()
  {
    this.Size = new Vector2(925f, 445f);
    this.Position = new Vector2(0.0f, 0.0f);
    this.imagePlate = Resources.Load<Texture2D>("GUI/InfoJournal/InviteFriends/Hex_Panle");
    this.AddChild((GuiElementBase) new GuiImage("GUI/InfoJournal/InviteFriends/invite_friends_background"));
    this.BringToFront((GuiElementBase) this.CloseButton);
    GuiLabel guiLabel1 = new GuiLabel("%$bgo.friends_invites.invite_friends%", Gui.Options.FontBGM_BT, 14);
    guiLabel1.WordWrap = true;
    guiLabel1.SizeX = 190f;
    this.AddChild((GuiElementBase) guiLabel1, new Vector2(75f, 18f));
    GuiButton guiButton = new GuiButton("%$bgo.friends_invites.invite_friends_button%", "GUI/InfoJournal/InviteFriends/btn_main_idle", "GUI/InfoJournal/InviteFriends/btn_main_hover", "GUI/InfoJournal/InviteFriends/btn_main_press");
    guiButton.Label.AllColor = Color.black;
    guiButton.Font = Gui.Options.FontVerdana;
    guiButton.FontSize = 12;
    guiButton.Size = new Vector2(184f, 42f);
    guiButton.Pressed = (AnonymousDelegate) (() =>
    {
      FacadeFactory.GetInstance().SendMessage(Message.SetFullscreen, (object) false);
      ExternalApi.EvalJs("window.openSocialInvite();");
    });
    this.AddChild((GuiElementBase) guiButton, new Vector2(45f, 227f));
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.friends_invites.invite_info%", Gui.Options.FontBGM_BT, 12), new Vector2(16f, 90f));
    GuiLabel guiLabel2 = new GuiLabel("%$bgo.friends_invites.click_here_to%", Gui.Options.FontBGM_BT);
    guiLabel2.AllColor = new Color(1f, 0.3686275f, 0.08627451f);
    guiLabel2.Alignment = TextAnchor.MiddleCenter;
    guiLabel2.SizeX = 240f;
    guiLabel2.WordWrap = true;
    this.AddChild((GuiElementBase) guiLabel2, new Vector2(15f, 207f));
    GuiLabel guiLabel3 = new GuiLabel("%$bgo.friends_invites.why_play_alone%", Gui.Options.FontBGM_BT, 12);
    guiLabel3.AllColor = new Color(1f, 0.3686275f, 0.08627451f);
    guiLabel3.Alignment = TextAnchor.MiddleCenter;
    guiLabel3.SizeX = 240f;
    guiLabel3.WordWrap = true;
    this.AddChild((GuiElementBase) guiLabel3, new Vector2(15f, 277f));
    CommunityProtocol.GetProtocol().RequestRecruitBonusLevel();
    this.waitForRecruitLevel = new GuiLabel(Gui.Options.FontBGM_BT, 14);
    this.SetRecruitBonusLevel();
    this.waitForRecruitLevel.WordWrap = true;
    this.waitForRecruitLevel.SizeX = 190f;
    this.AddChild((GuiElementBase) this.waitForRecruitLevel, new Vector2(370f, 18f));
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.friends_invites.accepted%", Gui.Options.FontBGM_BT, 12), new Vector2(307f, 90f));
    this.scroll.Size = new Vector2(185f, 280f);
    this.scroll.RenderScrollLines = false;
    this.AddChild((GuiElementBase) this.scroll, new Vector2(312f, 126f));
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.friends_invites.collect_rewards%", Gui.Options.FontBGM_BT, 14), new Vector2(644f, 33f));
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.friends_invites.bonus_received%", Gui.Options.FontBGM_BT, 12), new Vector2(572f, 90f));
    StaticCards.GlobalCard.IsLoaded.AddHandler((SignalHandler) (() =>
    {
      this.GenerateStackedIcon("%$bgo.friends_invites.1_friends%", new Vector2(662f, 125f), StaticCards.GlobalCard.FriendBonus);
      this.GenerateStackedIcon("%$bgo.friends_invites.5_friends%", new Vector2(662f, 210f), StaticCards.GlobalCard.SpecialFriendBonus[5]);
      this.GenerateStackedIcon("%$bgo.friends_invites.10_friends%", new Vector2(662f, 295f), StaticCards.GlobalCard.SpecialFriendBonus[10]);
      this.GenerateStackedIcon("%$bgo.friends_invites.25_friends%", new Vector2(662f, 380f), StaticCards.GlobalCard.SpecialFriendBonus[25]);
    }));
  }

  private void GenerateStackedIcon(string toolTip, Vector2 pos, RewardCard rewardItem)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiFriendInvites.\u003CGenerateStackedIcon\u003Ec__AnonStorey7C iconCAnonStorey7C = new GuiFriendInvites.\u003CGenerateStackedIcon\u003Ec__AnonStorey7C();
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey7C.rewardItem = rewardItem;
    Dictionary<ushort, int> dictionary1 = new Dictionary<ushort, int>();
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey7C.el = new QueueHorizontal();
    // ISSUE: reference to a compiler-generated field
    iconCAnonStorey7C.el.Size = new Vector2(265f, 57f);
    // ISSUE: reference to a compiler-generated field
    if (iconCAnonStorey7C.rewardItem != null)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      iconCAnonStorey7C.rewardItem.GUICard.IsLoaded.AddHandler(new SignalHandler(iconCAnonStorey7C.\u003C\u003Em__8D));
    }
    // ISSUE: reference to a compiler-generated field
    this.AddChild((GuiElementBase) iconCAnonStorey7C.el, pos);
    GuiPanel guiPanel1 = (GuiPanel) null;
    // ISSUE: reference to a compiler-generated field
    for (int index1 = 0; index1 < iconCAnonStorey7C.rewardItem.Items.Count; ++index1)
    {
      // ISSUE: reference to a compiler-generated field
      ShipItem shipItem = iconCAnonStorey7C.rewardItem.Items[index1];
      if (dictionary1.ContainsKey(shipItem.ItemGUICard.FrameIndex))
      {
        Dictionary<ushort, int> dictionary2;
        ushort index2;
        (dictionary2 = dictionary1)[index2 = shipItem.ItemGUICard.FrameIndex] = dictionary2[index2] + 1;
        Dictionary<ushort, int> dictionary3;
        ushort index3;
        (dictionary3 = dictionary1)[index3 = shipItem.ItemGUICard.FrameIndex] = dictionary3[index3] + (int) (shipItem as ItemCountable).Count;
      }
      else
      {
        dictionary1.Add(shipItem.ItemGUICard.FrameIndex, 0);
        Dictionary<ushort, int> dictionary2;
        ushort index2;
        (dictionary2 = dictionary1)[index2 = shipItem.ItemGUICard.FrameIndex] = dictionary2[index2] + (int) (shipItem as ItemCountable).Count;
      }
    }
    // ISSUE: reference to a compiler-generated field
    for (int index1 = 0; index1 < iconCAnonStorey7C.rewardItem.Items.Count; ++index1)
    {
      // ISSUE: reference to a compiler-generated field
      ShipItem shipItem = iconCAnonStorey7C.rewardItem.Items[index1];
      if (dictionary1.ContainsKey(shipItem.ItemGUICard.FrameIndex))
      {
        GuiPanel guiPanel2 = new GuiPanel();
        guiPanel2.AddChild((GuiElementBase) new GuiImage(this.imagePlate));
        GuiAtlasImage guiAtlasImage = new GuiAtlasImage();
        guiAtlasImage.GuiCard = shipItem.ItemGUICard;
        guiPanel2.AddChild((GuiElementBase) guiAtlasImage);
        int num = Mathf.Min(2, dictionary1[shipItem.ItemGUICard.FrameIndex]);
        for (int index2 = 0; index2 < num; ++index2)
        {
          GuiPanel guiPanel3 = new GuiPanel();
          guiPanel3.AddChild((GuiElementBase) new GuiImage(this.imagePlate));
          guiPanel3.AddChild((GuiElementBase) new GuiAtlasImage()
          {
            GuiCard = shipItem.ItemGUICard
          });
          guiPanel3.SizeX = guiAtlasImage.SizeX + 5f;
          guiPanel3.SizeY = guiAtlasImage.SizeY;
          guiPanel2.AddChild((GuiElementBase) guiPanel3, new Vector2(-this.stackOffsetX * (float) (index2 + 1), -this.stackOffsetY * (float) (index2 + 1)));
        }
        if (guiPanel1 != null)
          guiPanel1.SizeX = guiAtlasImage.SizeX + this.stackOffsetX * (float) num;
        guiPanel2.SizeY = guiAtlasImage.SizeY;
        // ISSUE: reference to a compiler-generated field
        iconCAnonStorey7C.el.AddChild((GuiElementBase) guiPanel2);
        // ISSUE: reference to a compiler-generated field
        iconCAnonStorey7C.el.AddChild((GuiElementBase) new GuiSpaceX(10f));
        guiPanel1 = guiPanel2;
        dictionary1.Remove(shipItem.ItemGUICard.FrameIndex);
      }
    }
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    string str = string.Empty;
    using (List<ShipItem>.Enumerator enumerator = StaticCards.GlobalCard.FriendBonus.Items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipItem current = enumerator.Current;
        str = str + (!(current is ItemCountable) ? string.Empty : (current as ItemCountable).Count.ToString() + " ") + current.ItemGUICard.Name + "%br%";
      }
    }
    this.bonus.Text = str;
    List<Player> playerList = new List<Player>((IEnumerable<Player>) Game.Me.Friends.Recruits);
    int count = playerList.Count;
    while (this.scroll.Children.Count > count)
      this.scroll.RemoveChild(this.scroll.Children[0]);
    while (this.scroll.Children.Count < count)
      this.scroll.AddChild((GuiElementBase) new GuiFriendInvites.FriendItem());
    List<GuiFriendInvites.FriendItem> friendItemList = this.scroll.ChildrenOfType<GuiFriendInvites.FriendItem>();
    for (int index = 0; index < count; ++index)
      friendItemList[index].SetPlayer(playerList[index]);
    this.SetRecruitBonusLevel();
  }

  public void SetRecruitBonusLevel()
  {
    if (this.currentRecruitBonusLevel == Game.Me.Friends.RequiredRecruitLevel)
      return;
    this.waitForRecruitLevel.Text = BsgoLocalization.Get("%$bgo.friends_invites.wait_for_friends%", (object) Game.Me.Friends.RequiredRecruitLevel);
  }

  private class FriendItem : GuiPanel
  {
    private readonly GuiImage avatar = new GuiImage("GUI/InfoJournal/InviteFriends/friend_icon");
    private readonly GuiLabel name = new GuiLabel(Gui.Options.FontBGM_BT, 12);
    private readonly GuiLabel level = new GuiLabel(Gui.Options.ColorDarkened, Gui.Options.FontBGM_BT, 12);

    public FriendItem()
    {
      this.Size = new Vector2(150f, 38f);
      this.AddChild((GuiElementBase) this.avatar, Align.MiddleLeft, new Vector2(20f, 0.0f));
      this.AddChild((GuiElementBase) this.name, new Vector2(60f, 6f));
      this.AddChild((GuiElementBase) this.level, new Vector2(60f, 20f));
    }

    public void SetPlayer(Player player)
    {
      this.name.Text = player.Name;
      this.level.Text = BsgoLocalization.Get("%$bgo.friends_invites.level%", (object) player.Level);
    }
  }
}
