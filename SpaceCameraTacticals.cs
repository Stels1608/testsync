﻿// Decompiled with JetBrains decompiler
// Type: SpaceCameraTacticals
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class SpaceCameraTacticals : SpaceCameraBase
{
  private static readonly List<SpaceCameraBase.CameraMode> supportedCameraModes = new List<SpaceCameraBase.CameraMode>() { SpaceCameraBase.CameraMode.Nose, SpaceCameraBase.CameraMode.Free, SpaceCameraBase.CameraMode.Chase, SpaceCameraBase.CameraMode.Target };
  private bool useHeightOffset = true;
  private const float FADE_LENGTH = 2f;
  private PlayerShip player;
  private SpaceObject target;
  private Vector2 lastMousePosition;
  public float minFOV;
  public float maxFOV;
  public float HardTrembleSpeed;
  public float SoftTrembleSpeed;
  public float TargetHeightOffset;
  public float HeightOffset;
  private float heightOffsetDampVelocity;
  private float lastHeightOffset;
  public float TargetDirectionSmoothTime;
  public float FollowSmoothTime;
  public float HeightOffsetSmoothTime;
  public float OrbitDelayTime;
  public float ZoomSmoothTime;
  private float rotationSmoothTime;
  private float waitedTime;
  public Vector2 OrbitRotatingSpeed;
  public Vector3 CurrentNewFlightmodelOrbitingYAxis;
  private Quaternion lastCameraRotation;
  private Quaternion wantedRotation;
  private bool orbiting;

  public static List<SpaceCameraBase.CameraMode> SupportedCameraModes
  {
    get
    {
      return SpaceCameraTacticals.supportedCameraModes;
    }
  }

  public override bool HandleMouseInput
  {
    get
    {
      return true;
    }
  }

  public override bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public override bool HandleJoystickAxesInput
  {
    get
    {
      return true;
    }
  }

  public override List<SpaceCameraBase.CameraMode> GetSupportedCameraModes()
  {
    return SpaceCameraTacticals.supportedCameraModes;
  }

  public override void SetCameraMode(SpaceCameraBase.CameraMode cameraMode)
  {
    base.SetCameraMode(cameraMode);
    SpaceCameraBase.CameraMode cameraMode1 = this.CurrentCameraMode;
    this.SetCurrentBehavior((ISpaceCameraBehavior) new SpaceCameraBehaviorTactical());
  }

  protected override void InitCam()
  {
    base.InitCam();
    this.minFOV = 55f;
    this.maxFOV = 65f;
    this.HardTrembleSpeed = 70f;
    this.SoftTrembleSpeed = 50f;
    this.HeightOffset = 2f;
    this.TargetDirectionSmoothTime = 3.5f;
    this.FollowSmoothTime = 3.5f;
    this.HeightOffsetSmoothTime = 0.75f;
    this.OrbitDelayTime = 2f;
    this.ZoomSmoothTime = 1f;
    this.OrbitRotatingSpeed = new Vector2(0.2f, 0.2f);
    this.CurrentNewFlightmodelOrbitingYAxis = Vector3.zero;
    this.SetAnimations();
    this.SetCurrentBehavior((ISpaceCameraBehavior) new SpaceCameraBehaviorTactical());
  }

  public override void SetCameraCard(CameraCard camCard)
  {
    base.SetCameraCard(camCard);
    if (SpaceCameraBase.cameraCard == null)
      return;
    SpaceCameraBase.ZoomDistanceMin = SpaceCameraBase.cameraCard.MinZoom;
    SpaceCameraBase.ZoomDistanceMax = SpaceCameraBase.cameraCard.MaxZoom;
    this.SoftTrembleSpeed = SpaceCameraBase.cameraCard.SoftTrembleSpeed;
    this.HardTrembleSpeed = SpaceCameraBase.cameraCard.HardTrembleSpeed;
  }

  protected override void Awake()
  {
    base.Awake();
    if ((double) SpaceCameraBase.ZoomDistance < (double) SpaceCameraBase.ZoomDistanceMin)
      SpaceCameraBase.ZoomDistance = SpaceCameraBase.ZoomDistanceMin;
    else if ((double) SpaceCameraBase.ZoomDistance > (double) SpaceCameraBase.ZoomDistanceMax)
      SpaceCameraBase.ZoomDistance = SpaceCameraBase.ZoomDistanceMax;
    SpaceCameraBase.WantedZoomDistance = SpaceCameraBase.ZoomDistance;
    this.lastMousePosition = (Vector2) Input.mousePosition;
  }

  private void SetAnimations()
  {
    this.CameraAnimation = this.GetComponentInChildren<Animation>();
    if (!((Object) this.CameraAnimation != (Object) null))
      return;
    this.CameraAnimation.wrapMode = WrapMode.Loop;
    this.CameraAnimation["hit"].layer = 1;
    this.CameraAnimation["hit"].wrapMode = WrapMode.Once;
    this.CameraAnimation.Stop();
    this.CameraAnimation.Play("idle");
  }

  private void OrbitingMode()
  {
    Vector2 vector2_1 = (Vector2) Input.mousePosition;
    Vector2 vector2_2 = vector2_1 - this.lastMousePosition;
    Camera main = Camera.main;
    Vector3 viewportPoint = main.ScreenToViewportPoint((Vector3) vector2_1);
    if (!main.rect.Contains(viewportPoint))
      vector2_2 = new Vector2(0.0f, 0.0f);
    Vector3 axis1 = this.lastCameraRotation * Vector3.left;
    Vector3 axis2 = Vector3.up;
    if (SpaceCameraBase.AdvancedFlightControls)
      axis2 = this.CurrentNewFlightmodelOrbitingYAxis;
    Quaternion quaternion = Quaternion.AngleAxis(vector2_2.y * this.OrbitRotatingSpeed.x, axis1);
    this.wantedRotation = Quaternion.AngleAxis(vector2_2.x * this.OrbitRotatingSpeed.y, axis2) * quaternion * this.lastCameraRotation;
  }

  private void ChaseMode()
  {
    Vector3 forward = SpaceCameraBase.ZoomDistance * this.player.Forward;
    this.wantedRotation = !SpaceCameraBase.AdvancedFlightControls ? Quaternion.LookRotation(forward) : Quaternion.LookRotation(forward, this.player.Up);
    this.rotationSmoothTime = this.FollowSmoothTime;
  }

  private void TargetMode()
  {
    float num1 = this.TargetHeightOffset * (SpaceCameraBase.ZoomDistance / 10f);
    float num2 = (float) ((double) SpaceCameraBase.ZoomDistance * (double) SpaceCameraBase.ZoomDistance - (double) num1 * (double) num1);
    float sqrMagnitude = (this.target.Position - this.player.Position).sqrMagnitude;
    Vector3 forward = this.target.Position - Mathf.Sqrt(num1 * num1 * sqrMagnitude / num2) * Vector3.up - this.player.Position;
    this.wantedRotation = !SpaceCameraBase.AdvancedFlightControls ? Quaternion.LookRotation(forward) : Quaternion.LookRotation(forward, this.player.Rotation * Vector3.up);
    this.rotationSmoothTime = this.TargetDirectionSmoothTime;
  }

  protected override void LateUpdate()
  {
    base.LateUpdate();
    this.player = SpaceLevel.GetLevel().GetPlayerShip();
    this.target = SpaceLevel.GetLevel().GetPlayerTarget();
    if (this.player == null)
      return;
    float num1 = 0.0f;
    if (SpaceCameraBase.cameraCard != null)
    {
      float a = SpaceCameraBase.cameraCard.MaxZoom - SpaceCameraBase.cameraCard.MinZoom;
      if (!Mathf.Approximately(a, 0.0f))
        num1 = Mathf.Clamp01((SpaceCameraBase.ZoomDistance - SpaceCameraBase.cameraCard.MinZoom) / a);
    }
    if (SpaceCameraBase.cameraCard != this.player.CameraCard && this.player.CameraCard != null && (bool) this.player.CameraCard.IsLoaded)
    {
      SpaceCameraBase.ZoomDistance = SpaceCameraBase.cameraCard != null ? num1 * (this.player.CameraCard.MaxZoom - this.player.CameraCard.MinZoom) + this.player.CameraCard.MinZoom : this.player.CameraCard.DefaultZoom;
      SpaceCameraBase.cameraCard = this.player.CameraCard;
      SpaceCameraBase.WantedZoomDistance = SpaceCameraBase.ZoomDistance;
      SpaceCameraBase.ZoomDistanceMin = SpaceCameraBase.cameraCard.MinZoom;
      SpaceCameraBase.ZoomDistanceMax = SpaceCameraBase.cameraCard.MaxZoom;
      this.SoftTrembleSpeed = SpaceCameraBase.cameraCard.SoftTrembleSpeed;
      this.HardTrembleSpeed = SpaceCameraBase.cameraCard.HardTrembleSpeed;
      this.ChaseMode();
    }
    SpaceCameraBase.ZoomDistance = Mathf.SmoothDamp(SpaceCameraBase.ZoomDistance, SpaceCameraBase.WantedZoomDistance, ref this.ZoomDampVelocity, this.ZoomSmoothTime);
    float num2;
    Quaternion quaternion;
    Vector3 vector3;
    if (this.CurrentCameraMode != SpaceCameraBase.CameraMode.Nose)
    {
      switch (this.CurrentCameraMode)
      {
        case SpaceCameraBase.CameraMode.Target:
        case SpaceCameraBase.CameraMode.Chase:
          this.useHeightOffset = true;
          break;
        case SpaceCameraBase.CameraMode.Free:
          this.useHeightOffset = false;
          break;
      }
      if (this.orbiting)
      {
        this.OrbitingMode();
        this.waitedTime = 0.0f;
      }
      else
      {
        this.waitedTime += Time.deltaTime;
        if ((double) this.waitedTime > (double) this.OrbitDelayTime)
        {
          switch (this.CurrentCameraMode)
          {
            case SpaceCameraBase.CameraMode.Target:
              if (this.target != null)
              {
                this.TargetMode();
                break;
              }
              this.ChaseMode();
              break;
            case SpaceCameraBase.CameraMode.Chase:
              this.ChaseMode();
              break;
          }
        }
      }
      float zoom = (float) (((double) SpaceCameraBase.ZoomDistance - (double) SpaceCameraBase.ZoomDistanceMin) / ((double) SpaceCameraBase.ZoomDistanceMax - (double) SpaceCameraBase.ZoomDistanceMin));
      num2 = this.minFOV + (this.maxFOV - this.minFOV) * zoom;
      quaternion = !this.orbiting ? Quaternion.Slerp(this.lastCameraRotation, this.wantedRotation, this.rotationSmoothTime * Time.deltaTime) : this.wantedRotation;
      float heightOffset = this.GetHeightOffset(zoom);
      vector3 = quaternion * new Vector3(0.0f, heightOffset, -SpaceCameraBase.ZoomDistance) + this.player.Position;
    }
    else
    {
      quaternion = this.player.Rotation;
      vector3 = this.player.Position + this.player.Rotation * new Vector3(0.0f, 0.0f, this.player.ShipBounds.z / 2f);
      num2 = Camera.main.fieldOfView;
    }
    if (!(this.currentBehavior is SpaceCameraBehaviorTactical))
    {
      this.CurrentBehavior.LateUpdate();
      this.ApplyCameraParameters(this.CurrentBehavior.CalculateCurrentCameraParameters());
    }
    else
      this.ApplyCameraParameters(new SpaceCameraParameters()
      {
        Fov = num2,
        CamRotation = quaternion,
        CamPosition = vector3
      });
    this.lastCameraRotation = this.transform.rotation;
    this.lastMousePosition = (Vector2) Input.mousePosition;
    this.UpdateTrembleAnimationState();
  }

  private void UpdateTrembleAnimationState()
  {
    if ((double) this.player.MovementController.CurrentSpeed > (double) this.HardTrembleSpeed)
      this.CameraAnimation.CrossFade("tremble", 2f);
    else if ((double) this.player.MovementController.CurrentSpeed > (double) this.SoftTrembleSpeed)
      this.CameraAnimation.CrossFade("softtremble", 2f);
    else
      this.CameraAnimation.CrossFade("idle", 2f);
  }

  private float GetHeightOffset(float zoom)
  {
    float target = !this.useHeightOffset ? 0.0f : this.player.ShipBounds.y * zoom / SpaceCameraBase.ZoomDistanceMin + this.player.ShipBounds.y * (this.HeightOffset / 10f);
    if ((double) target != (double) this.lastHeightOffset)
    {
      float num = Mathf.Clamp(this.player.ShipBounds.y, 15f, 150f) / 100f;
      target = Mathf.SmoothDamp(this.lastHeightOffset, target, ref this.heightOffsetDampVelocity, this.HeightOffsetSmoothTime + num);
      this.lastHeightOffset = target;
    }
    return target;
  }

  public override void OnHitted()
  {
    base.OnHitted();
    this.CameraAnimation.CrossFade("hit");
  }

  private static Vector3 ClampAngles(Vector3 angles, Vector3 minLimit, Vector3 maxLimit)
  {
    Vector3 vector3;
    vector3.x = SpaceCameraTacticals.ClampAngle(angles.x, minLimit.x, maxLimit.x);
    vector3.y = SpaceCameraTacticals.ClampAngle(angles.y, minLimit.y, maxLimit.y);
    vector3.z = SpaceCameraTacticals.ClampAngle(angles.z, minLimit.z, maxLimit.z);
    return vector3;
  }

  private static float ClampAngle(float angle, float min, float max)
  {
    angle = SpaceCameraTacticals.ClampAngle(angle);
    return Mathf.Clamp(angle, min, max);
  }

  private static float ClampAngle(float angle)
  {
    if ((double) angle < -180.0)
      angle += 360f;
    if ((double) angle > 180.0)
      angle -= 360f;
    return angle;
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse0)
    {
      this.orbiting = true;
      this.CurrentNewFlightmodelOrbitingYAxis = this.transform.rotation * Vector3.up;
    }
    return true;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse0)
      this.orbiting = false;
    return true;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
  }
}
