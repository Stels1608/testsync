﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorOutsideScreenArrowNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HudIndicatorOutsideScreenArrowNgui : HudIndicatorBaseComponentUgui, IOutsideScreenArrow, IReceivesTarget
{
  [SerializeField]
  protected UISprite arrow;
  private float timeLastAlphaFlip;
  private ISpaceEntity target;

  protected UIAtlas Atlas
  {
    get
    {
      return HudIndicatorsAtlasLinker.Instance.Atlas;
    }
  }

  protected virtual bool ScaleDown
  {
    get
    {
      if (this.IsMultiselected)
        return !this.IsSelected;
      return false;
    }
  }

  protected override void ResetComponentStates()
  {
    this.timeLastAlphaFlip = 0.0f;
    this.target = (ISpaceEntity) null;
  }

  protected override void Awake()
  {
    base.Awake();
    UIPanel uiPanel = NGUITools.AddChild<UIPanel>(this.gameObject);
    uiPanel.widgetsAreStatic = false;
    uiPanel.depth = 9999;
    this.arrow = NGUIToolsExtension.AddSprite(uiPanel.gameObject, this.Atlas, "HudIndicator_Locator", true, string.Empty);
  }

  private void Update()
  {
    if (!this.gameObject.activeSelf)
      return;
    this.arrow.transform.localScale = Vector3.one * (!this.ScaleDown ? 1f : 0.5f);
    this.timeLastAlphaFlip += Time.deltaTime;
    if ((double) this.timeLastAlphaFlip <= 0.100000001490116)
      return;
    this.arrow.alpha = (double) this.arrow.alpha != 1.0 ? 1f : 0.7f;
    this.timeLastAlphaFlip = 0.0f;
  }

  public void OnOrientationChanged(float orientation)
  {
    this.arrow.transform.rotation = Quaternion.Euler(0.0f, 0.0f, (float) (-(double) orientation * 360.0));
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.target = target;
    this.UpdateColorAndAlpha();
  }

  protected override void SetColoring()
  {
    this.arrow.color = !this.IsDesignated || this.IsSelected ? (!this.IsActiveWaypoint || this.IsSelected ? HudIndicatorColorInfo.GetTargetColor4LegacyBracket(this.target) : TargetColorsLegacyBrackets.waypointTargetColor) : TargetColorsLegacyBrackets.designatedTargetColor;
  }

  protected override void UpdateView()
  {
    bool flag = !this.InScreen && (this.IsSelected || this.IsMultiselected || this.IsDesignated || this.IsActiveWaypoint);
    this.gameObject.SetActive(flag);
    if (!flag)
      return;
    this.UpdateColorAndAlpha();
  }

  public override bool RequiresScreenBorderClamping()
  {
    return this.gameObject.activeSelf;
  }
}
