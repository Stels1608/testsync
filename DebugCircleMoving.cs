﻿// Decompiled with JetBrains decompiler
// Type: DebugCircleMoving
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugCircleMoving : MonoBehaviour
{
  public float LinearSpeed;
  public float Radius;
  private float storedRadius;
  private float storedLinearSpeed;
  private float angularSpeed;

  private void Update()
  {
    if ((double) this.storedRadius != (double) this.Radius)
    {
      this.storedRadius = this.Radius;
      this.transform.position = new Vector3(-this.Radius, 0.0f, 0.0f);
    }
    if ((double) this.storedLinearSpeed != (double) this.LinearSpeed)
    {
      this.storedLinearSpeed = this.LinearSpeed;
      this.angularSpeed = 57.29578f * this.LinearSpeed / this.Radius;
    }
    this.transform.RotateAround(Vector3.zero, Vector3.up, this.angularSpeed * Time.deltaTime);
  }
}
