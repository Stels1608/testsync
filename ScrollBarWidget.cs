﻿// Decompiled with JetBrains decompiler
// Type: ScrollBarWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ScrollBarWidget : NguiWidget
{
  private UIScrollBar scrollBar;

  public UIScrollBar ScrollBar
  {
    get
    {
      return this.scrollBar ?? (this.scrollBar = this.GetComponent<UIScrollBar>());
    }
  }

  public override void Start()
  {
    base.Start();
    if (!((Object) this.ScrollBar != (Object) null) || !((Object) this.ScrollBar.foregroundWidget != (Object) null))
      return;
    this.ScrollBar.foregroundWidget.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
  }

  public override void OnHover(bool isOver)
  {
  }
}
