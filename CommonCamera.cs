﻿// Decompiled with JetBrains decompiler
// Type: CommonCamera
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CommonCamera : MonoBehaviour
{
  private const string MAIN_CAMERA_NAME = "Main Camera";
  private const string BG_CAMERA_NAME = "BgCamera";
  private const string OVERLAY_CAMERA_NAME = "OverlayCamera";
  protected Transform overlayCamera;
  protected Transform bgCamera;
  protected Transform mainCamera;

  protected virtual void Awake()
  {
    GameObject gameObject1 = GameObject.Find("BgCamera");
    if (!(bool) ((Object) gameObject1))
      Debug.LogError((object) string.Format("There is no camera in the scene named '{0}'", (object) "BgCamera"));
    else
      this.bgCamera = gameObject1.GetComponent<Transform>();
    GameObject gameObject2 = GameObject.Find("OverlayCamera");
    if (!(bool) ((Object) gameObject2))
      Debug.LogError((object) string.Format("There is no camera in the scene named '{0}'", (object) "OverlayCamera"));
    else
      this.overlayCamera = gameObject2.GetComponent<Transform>();
    GameObject gameObject3 = GameObject.Find("Main Camera");
    if (!(bool) ((Object) gameObject3))
      Debug.LogError((object) string.Format("There is no camera in the scene named '{0}'", (object) "Main Camera"));
    else
      this.mainCamera = gameObject3.GetComponent<Transform>();
  }

  protected void UpdateFOV(float fov)
  {
    if ((bool) ((Object) this.bgCamera))
      this.bgCamera.GetComponent<Camera>().fieldOfView = fov;
    if ((bool) ((Object) this.overlayCamera))
      this.overlayCamera.GetComponent<Camera>().fieldOfView = fov;
    if (!(bool) ((Object) this.mainCamera))
      return;
    this.mainCamera.GetComponent<Camera>().fieldOfView = fov;
  }

  protected void UpdateRotation()
  {
    if ((bool) ((Object) this.bgCamera))
      this.bgCamera.rotation = this.mainCamera.rotation;
    if (!(bool) ((Object) this.overlayCamera))
      return;
    this.overlayCamera.rotation = this.mainCamera.rotation;
  }

  public void SetMainCamera(Camera mainCam)
  {
    this.mainCamera = mainCam.transform;
  }
}
