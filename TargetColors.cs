﻿// Decompiled with JetBrains decompiler
// Type: TargetColors
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class TargetColors
{
  public static readonly Color enemyColorFoF = ImageUtils.HexStringToColor("ef6060");
  public static readonly Color enemyColorFactions = ImageUtils.HexStringToColor("ef3636");
  public static readonly Color colonialColor = ImageUtils.HexStringToColor("6486c4");
  public static readonly Color cylonColor = ImageUtils.HexStringToColor("eb5757");
  public static readonly Color ancientColor = ImageUtils.HexStringToColor("a68428");
  public static readonly Color friendlyColor = ImageUtils.HexStringToColor("4ba5b3");
  public static readonly Color neutralColor = ImageUtils.HexStringToColor("8c8c8c");
  public static readonly Color cylonPartyColor = ColorUtility.MakeColorFrom0To255(223U, 131U, 41U);
  public static readonly Color colonialPartyColor = ColorUtility.MakeColorFrom0To255(42U, 208U, 175U);
  public static readonly Color designatedTargetColor = ColorUtility.MakeColorFrom0To255(218U, 212U, 54U);
  public static readonly Color waypointTargetColor = ColorUtility.MakeColorFrom0To255((uint) byte.MaxValue, 94U, 22U);
  public static readonly Color sectorEventColor = ColorUtility.MakeColorFrom0To255(64U, 230U, 128U);

  public static Color GetFactionColor(Faction faction)
  {
    switch (faction)
    {
      case Faction.Neutral:
        return TargetColors.neutralColor;
      case Faction.Colonial:
        return TargetColors.colonialColor;
      case Faction.Cylon:
        return TargetColors.cylonColor;
      case Faction.Ancient:
        return TargetColors.ancientColor;
      default:
        return TargetColors.neutralColor;
    }
  }
}
