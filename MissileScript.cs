﻿// Decompiled with JetBrains decompiler
// Type: MissileScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MissileScript : SingleLODListener
{
  public float Volume = 1f;
  public float MinDistance = 1f;
  private const float hitDistance = 3f;
  private const float terminatingDuration = 2f;
  public AudioClip LoopClip;
  public SpaceObject HitTarget;
  private AudioSource missileAudio;

  private void Start()
  {
    this.MakeMissileAudio();
  }

  protected override void OnSwitched(bool value)
  {
  }

  private void MakeMissileAudio()
  {
    this.missileAudio = this.gameObject.AddComponent<AudioSource>();
    this.missileAudio.playOnAwake = true;
    this.missileAudio.volume = this.Volume;
    this.missileAudio.minDistance = this.MinDistance;
    this.missileAudio.maxDistance = this.missileAudio.minDistance * 256f;
    this.missileAudio.clip = this.LoopClip;
    this.missileAudio.loop = true;
    this.missileAudio.spatialBlend = 1f;
  }

  public void Update()
  {
  }

  public void Terminate()
  {
    this.TriggerExplosion();
    this.RemoveMissileGameObject();
  }

  public void TriggerExplosion()
  {
    Missile missile = this.SpaceObject as Missile;
    MissileExplosionArgs missileExplosionArgs = new MissileExplosionArgs();
    missileExplosionArgs.ExplosionView = missile.MissileCard.ExplosionView;
    missileExplosionArgs.ExplosionTier = (int) missile.Tier;
    missileExplosionArgs.ExplosionRange = missile.EffectRadius;
    Vector3 position = missile.Position;
    if (SpaceLevel.GetLevel().GetPlayerShip() == this.HitTarget)
      SpaceLevel.GetLevel().SpaceCamera.OnHitted();
    FXManager.Instance.PutEffect((EffectState) new ExplosionEffectState(position, missile.Rotation, (ExplosionArgs) missileExplosionArgs, 0.0f));
  }

  public void RemoveMissileGameObject()
  {
    TrailRenderer componentInChildren1 = this.GetComponentInChildren<TrailRenderer>();
    float t = !((Object) componentInChildren1 != (Object) null) ? 5f : componentInChildren1.time;
    MeshRenderer componentInChildren2 = this.gameObject.GetComponentInChildren<MeshRenderer>();
    if ((bool) ((Object) componentInChildren2))
      componentInChildren2.enabled = false;
    foreach (ParticleSystem componentsInChild in this.GetComponentsInChildren<ParticleSystem>())
      componentsInChild.loop = false;
    foreach (Transform transform in this.transform)
    {
      transform.parent = (Transform) null;
      Object.Destroy((Object) transform.gameObject, t);
    }
    Object.Destroy((Object) this.transform.gameObject, t);
    SpaceLevel.GetLevel().GetObjectRegistry().Remove(this.SpaceObject.ObjectID, RemovingCause.Death);
  }
}
