﻿// Decompiled with JetBrains decompiler
// Type: OutpostShip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class OutpostShip : CruiserShip
{
  public OutpostShip(uint objectID)
    : base(objectID)
  {
    this.AutoSubscribe = true;
  }

  public override void UnSubscribe()
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Subscribe();
  }
}
