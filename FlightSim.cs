﻿// Decompiled with JetBrains decompiler
// Type: FlightSim
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class FlightSim : MonoBehaviour
{
  private GameObject ship;
  private ShipRoleSpecificsFactory shipRoleFactory;
  private ShipControlsBase shipControls;
  private FlightSimGameProtocolEmulator flightSimGameProtocolEmulator;
  private float timeLastMovementUpdate;

  private void Start()
  {
    this.CreateFakedGameEnvironment();
    this.flightSimGameProtocolEmulator = new FlightSimGameProtocolEmulator();
    this.LoadGalactica();
    this.LoadShip("HumanT1Fighter", (byte) 1);
    this.CreateSomeRandomObstacles(1000);
    Tick.Reset((double) Time.realtimeSinceStartup);
    FlightSimDebugSelection simDebugSelection = new FlightSimDebugSelection(this.flightSimGameProtocolEmulator.MovementOptions);
    Game.GUIManager.AddPanel((IGUIPanel) simDebugSelection);
    Game.InputDispatcher.AddListener((InputListener) simDebugSelection);
  }

  private void Update()
  {
    this.HandleInputs();
    Tick.Update((double) Time.realtimeSinceStartup);
    if (Tick.IsNewTick())
      this.flightSimGameProtocolEmulator.UpdateMovementFrame(Tick.Current);
    this.UpdateMovement();
  }

  private void CreateFakedGameEnvironment()
  {
    Game.Create();
    FlightSimSpaceLevel flightSimSpaceLevel = new GameObject("FlightSimSpaceLevel").AddComponent<FlightSimSpaceLevel>();
    flightSimSpaceLevel.Initialize((LevelProfile) new SpaceLevelProfile(1U, 1U, GameLocation.Unknown));
    flightSimSpaceLevel.Card.regulationCard = new RegulationCard(1U);
    PlayerShip playerShip = new PlayerShip(0U);
    flightSimSpaceLevel.SetPlayerShip(playerShip);
  }

  private void LoadGalactica()
  {
    GameObject ship = Object.Instantiate<GameObject>(FlightSimUtils.FindPrefabByName("galactica"));
    BgoUtils.CleanupModelForShopView(ship);
    ship.transform.position = new Vector3(-200f, -100f, 1000f);
    ship.transform.rotation = Quaternion.Euler(0.0f, 135f, 0.0f);
  }

  private void LoadShip(string prefabName, byte tier)
  {
    if ((Object) this.ship != (Object) null)
      Object.Destroy((Object) this.ship);
    this.ship = FlightSimUtils.FindPrefabByName(prefabName);
    this.ship = Object.Instantiate<GameObject>(this.ship);
    this.ship.transform.rotation = Quaternion.identity;
    BgoUtils.CleanupModelForShopView(this.ship);
    FlightSimPlayerShip flightSimPlayerShip = new FlightSimPlayerShip(0U);
    flightSimPlayerShip.InjectModel(this.ship);
    SpaceLevel.GetLevel().SetPlayerShip((PlayerShip) flightSimPlayerShip);
    this.shipRoleFactory = new ShipRoleSpecificsFactory((PlayerShip) flightSimPlayerShip);
    this.SetupFlightModel();
  }

  private void SetupFlightModel()
  {
    this.SetupCamera();
    this.SetupProtocols();
    this.SetupInput();
  }

  private void SetupCamera()
  {
    SpaceCameraBase component = Camera.main.gameObject.GetComponent<SpaceCameraBase>();
    if ((Object) component != (Object) null)
      Object.Destroy((Object) component);
    SpaceCameraBase spaceCamera = (SpaceCameraBase) Camera.main.gameObject.AddComponent(this.shipRoleFactory.GetSpecificCameraBehaviour());
    SpaceLevel.GetLevel().InjectCamera(spaceCamera);
    Game.InputDispatcher.AddListener((InputListener) spaceCamera);
    SpaceCameraBase.MouseWheel = MouseWheelBinding.Thrust;
  }

  private void SetupProtocols()
  {
    Game.ProtocolManager.UnregisterProtocol(BgoProtocol.ProtocolID.Game);
    Game.ProtocolManager.RegisterProtocol((BgoProtocol) new FlightSimGameProtocolEmulator());
  }

  private void SetupInput()
  {
    this.shipControls = this.shipRoleFactory.CreateShipSpecificControls();
    this.shipControls.Enabled = true;
    Game.InputDispatcher.AddListener((InputListener) this.shipControls);
  }

  private void CreateSomeRandomObstacles(int amount)
  {
    List<PrimitiveType> primitiveTypeList = new List<PrimitiveType>();
    primitiveTypeList.Add(PrimitiveType.Sphere);
    primitiveTypeList.Add(PrimitiveType.Capsule);
    primitiveTypeList.Add(PrimitiveType.Cube);
    primitiveTypeList.Add(PrimitiveType.Cylinder);
    GameObject gameObject = new GameObject();
    for (int index1 = 0; index1 < amount; ++index1)
    {
      int index2 = Mathf.Clamp((int) ((double) Random.value * 4.0 - 0.5), 0, primitiveTypeList.Count - 1);
      Vector3 vector3 = new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f) * 1000f;
      if ((double) (vector3 - this.ship.transform.position).sqrMagnitude < 0.00999999977648258)
        vector3 += Vector3.one * Random.value * 1000f;
      Quaternion quaternion = Quaternion.Euler(Random.value * 360f, Random.value * 360f, Random.value * 360f);
      GameObject primitive = GameObject.CreatePrimitive(primitiveTypeList[index2]);
      primitive.transform.position = vector3;
      primitive.transform.rotation = quaternion;
      primitive.transform.parent = gameObject.transform;
    }
  }

  private void HandleInputs()
  {
    if (this.shipControls == null)
      return;
    this.shipControls.Update();
    this.shipControls.RequestFlush();
  }

  private void UpdateMovement()
  {
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    float t = realtimeSinceStartup - (float) Tick.Current.Time;
    MovementFrame movementFrame = this.flightSimGameProtocolEmulator.MovementFrame;
    Game.Me.Ship.Position = movementFrame.GetFuturePosition(t);
    float deltaTime = realtimeSinceStartup - this.timeLastMovementUpdate;
    this.timeLastMovementUpdate = realtimeSinceStartup;
    ((FlightSimPlayerShip) SpaceLevel.GetLevel().GetPlayerShip()).InjectMovementUpdateDeltaTime(deltaTime);
    Game.Me.Ship.Rotation = movementFrame.GetFutureRotation(t);
  }
}
