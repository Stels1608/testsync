﻿// Decompiled with JetBrains decompiler
// Type: Simulation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public static class Simulation
{
  public static MovementFrame MoveToDirection(MovementFrame prevFrame, Euler3 targetEuler3, MovementOptions options)
  {
    Euler3 euler3_1 = prevFrame.euler3;
    Euler3 to = options.MaxEulerSpeed(euler3_1);
    Euler3 from = options.MinEulerSpeed(euler3_1);
    Euler3 euler3_2 = (targetEuler3 - euler3_1).Normalized(false);
    float num1 = Mathf.Abs(2f * euler3_2.pitch / options.pitchAcceleration);
    float num2 = Mathf.Abs(2f * euler3_2.yaw / options.yawAcceleration);
    float num3 = 0.0f;
    if ((double) euler3_2.yaw > (double) Mathf.Epsilon)
      num3 = options.yawAcceleration * num2;
    else if ((double) euler3_2.yaw < -(double) Mathf.Epsilon)
      num3 = -options.yawAcceleration * num2;
    float num4 = 0.0f;
    if ((double) euler3_2.pitch > (double) Mathf.Epsilon)
      num4 = options.pitchAcceleration * num1;
    else if ((double) euler3_2.pitch < -(double) Mathf.Epsilon)
      num4 = -options.pitchAcceleration * num1;
    Euler3 zero = Euler3.zero;
    zero.yaw = (float) (((double) num3 - (double) prevFrame.Euler3Speed.yaw) / 0.100000001490116);
    zero.pitch = (float) (((double) num4 - (double) prevFrame.Euler3Speed.pitch) / 0.100000001490116);
    float num5 = Algorithm3D.NormalizeAngle(Mathf.Clamp((float) (-(double) euler3_2.yaw * (double) options.maxRoll / 135.0), -options.maxRoll, options.maxRoll) - euler3_1.roll);
    zero.roll = options.rollAcceleration * (float) ((double) num5 / (double) options.maxRoll - (double) prevFrame.Euler3Speed.roll * (double) options.rollFading / (double) options.rollMaxSpeed);
    zero.Clamp(-options.maxTurnAcceleration, options.maxTurnAcceleration);
    Euler3 euler3Speed = prevFrame.Euler3Speed + zero * 0.1f;
    euler3Speed.Clamp(from, to);
    Vector3 linearSpeed = Simulation.AdvanceLinearSpeed(prevFrame, options);
    Vector3 strafeSpeed = Simulation.AdvanceStrafingSpeed(prevFrame, options, 0.0f, 0.0f);
    Euler3 nextEuler3 = prevFrame.NextEuler3;
    MovementFrame movementFrame = new MovementFrame(prevFrame.nextPosition, nextEuler3, linearSpeed, strafeSpeed, euler3Speed);
    Quaternion rotationDelta = nextEuler3.rotation * Quaternion.Inverse(prevFrame.rotation) * Quaternion.identity;
    movementFrame.ActiveThrusterEffects = Simulation.DetermineThrusterEffects(rotationDelta, 0.0f, 0.0f);
    movementFrame.mode = 0;
    return movementFrame;
  }

  public static MovementFrame WASD(MovementFrame prevFrame, int pitch, int yaw, int roll, MovementOptions options)
  {
    Euler3 euler3 = prevFrame.euler3;
    Euler3 to = options.MaxEulerSpeed(euler3);
    Euler3 from = options.MinEulerSpeed(euler3);
    float num1 = 0.0f;
    if (yaw > 0)
      num1 = -options.maxRoll;
    if (yaw < 0)
      num1 = options.maxRoll;
    float num2 = Algorithm3D.NormalizeAngle(num1 - euler3.roll);
    Euler3 zero = Euler3.zero;
    zero.roll = options.rollAcceleration * (float) ((double) num2 / (double) options.maxRoll - (double) prevFrame.Euler3Speed.roll * (double) options.rollFading / (double) options.rollMaxSpeed);
    if (yaw != 0)
      zero.yaw = (float) yaw * options.yawAcceleration;
    else if ((double) prevFrame.Euler3Speed.yaw > (double) Mathf.Epsilon)
    {
      zero.yaw = -options.yawAcceleration * options.yawFading;
      from.yaw = 0.0f;
    }
    else if ((double) prevFrame.Euler3Speed.yaw < -(double) Mathf.Epsilon)
    {
      zero.yaw = options.yawAcceleration * options.yawFading;
      to.yaw = 0.0f;
    }
    if (pitch != 0)
      zero.pitch = (float) pitch * options.pitchAcceleration;
    else if ((double) prevFrame.Euler3Speed.pitch > (double) Mathf.Epsilon)
    {
      zero.pitch = -options.pitchAcceleration * options.pitchFading;
      from.pitch = 0.0f;
    }
    else if ((double) prevFrame.Euler3Speed.pitch < -(double) Mathf.Epsilon)
    {
      zero.pitch = options.pitchAcceleration * options.pitchFading;
      to.pitch = 0.0f;
    }
    Euler3 euler3Speed = prevFrame.Euler3Speed + zero * 0.1f;
    euler3Speed.Clamp(from, to);
    Vector3 linearSpeed = Simulation.AdvanceLinearSpeed(prevFrame, options);
    Vector3 strafeSpeed = Simulation.AdvanceStrafingSpeed(prevFrame, options, 0.0f, 0.0f);
    Euler3 nextEuler3 = prevFrame.NextEuler3;
    MovementFrame movementFrame = new MovementFrame(prevFrame.nextPosition, nextEuler3, linearSpeed, strafeSpeed, euler3Speed);
    Quaternion rotationDelta = nextEuler3.rotation * Quaternion.Inverse(prevFrame.rotation) * Quaternion.identity;
    movementFrame.ActiveThrusterEffects = Simulation.DetermineThrusterEffects(rotationDelta, 0.0f, 0.0f);
    if (pitch != 0)
      movementFrame.mode = 1;
    return movementFrame;
  }

  private static Vector3 ScalePitchYawSoThatOneOfThemHasLengthOneAndSetZToZero(Vector3 source)
  {
    float num = Mathf.Abs(Mathf.Max(Mathf.Abs(source.x), Mathf.Abs(source.y)));
    if ((double) Math.Abs(num) < 1.0 / 1000.0)
      return source;
    source.x = source.x * (1f / num);
    source.y = source.y * (1f / num);
    source.z = 0.0f;
    return source;
  }

  public static MovementFrame TurnToDirectionStrikes(MovementFrame prevFrame, Euler3 targetEuler3, float roll, float strafeX, float strafeY, MovementOptions options)
  {
    Vector3 vector3_1 = options.maxTurnAcceleration.ComponentsToVector3();
    Vector3 damping = Simulation.FindDamping(vector3_1, options.maxTurnSpeed.ComponentsToVector3());
    float num1 = 3f;
    Vector3 vector3_2 = options.maxTurnSpeed.ComponentsToVector3();
    Vector3 nextPosition = prevFrame.nextPosition;
    Euler3 nextEuler3 = prevFrame.NextEuler3;
    Quaternion rotation = nextEuler3.rotation;
    Vector3 vector3_3 = targetEuler3.rotation * Vector3.forward;
    roll = Mathf.Clamp(roll, -1f, 1f);
    Vector3 linearSpeed = Simulation.AdvanceLinearSpeed(prevFrame, options);
    Quaternion quaternion = Quaternion.Inverse(rotation);
    Vector3 toDirection = quaternion * vector3_3;
    Vector3 vector3_4 = quaternion * prevFrame.Euler3Speed.ComponentsToVector3();
    Vector3 vector3_5 = Simulation.KeepVectorComponentsWithinPlusMinus180(Quaternion.FromToRotation(Vector3.forward, toDirection).eulerAngles);
    Vector3 forRotationDelta = Simulation.FindVelocityForRotationDelta(vector3_5, damping * num1);
    Vector3 scale1 = Vector3.one - damping * 0.1f;
    Vector3 vector3_6 = vector3_4;
    vector3_6.Scale(scale1);
    Vector3 scale2 = Vector3.one - damping * num1 * 0.1f;
    vector3_4.Scale(scale2);
    bool flag1 = false;
    bool flag2 = false;
    if ((double) Mathf.Abs(vector3_6.x) > (double) Mathf.Abs(forRotationDelta.x))
    {
      scale1.x = Mathf.Max(scale2.x, forRotationDelta.x / vector3_4.x);
      flag1 = true;
    }
    if ((double) Mathf.Abs(vector3_6.y) > (double) Mathf.Abs(forRotationDelta.y))
    {
      scale1.y = Mathf.Max(scale2.y, forRotationDelta.y / vector3_4.y);
      flag2 = true;
    }
    vector3_4.Scale(scale1);
    Vector3 vector3_7 = Simulation.ScalePitchYawSoThatOneOfThemHasLengthOneAndSetZToZero(vector3_5);
    Vector3 scale3 = vector3_1;
    vector3_7.Scale(scale3);
    Vector3 vector3_8 = forRotationDelta - vector3_4;
    if ((double) Mathf.Abs(vector3_7.x) > (double) Mathf.Abs(vector3_8.x))
      vector3_7.x = vector3_8.x;
    if ((double) Mathf.Abs(vector3_7.y) > (double) Mathf.Abs(vector3_8.y))
      vector3_7.y = vector3_8.y;
    if (flag1)
      vector3_7.x = 0.0f;
    if (flag2)
      vector3_7.y = 0.0f;
    float num2 = vector3_1.z * roll;
    vector3_4.z += num2 * 0.1f;
    Vector3 vector3_9 = Simulation.Clamp(vector3_4 + vector3_7 * 0.1f, -vector3_2, vector3_2);
    Vector3 vector3_10 = rotation * vector3_9;
    Euler3 euler3Speed = new Euler3(vector3_10.x, vector3_10.y, vector3_10.z);
    Vector3 strafeSpeed = Simulation.AdvanceStrafingSpeed(prevFrame, options, 0.0f, 0.0f);
    MovementFrame movementFrame = new MovementFrame(nextPosition, nextEuler3, linearSpeed, strafeSpeed, euler3Speed);
    movementFrame.mode = 2;
    Quaternion rotationDelta = rotation * Quaternion.Inverse(prevFrame.rotation) * Quaternion.identity;
    movementFrame.ActiveThrusterEffects = Simulation.DetermineThrusterEffects(rotationDelta, strafeX, strafeY);
    return movementFrame;
  }

  private static void ClampVectorComponents(ref Vector3 vec, int min, int max)
  {
    vec.x = Mathf.Clamp(vec.x, (float) min, (float) max);
    vec.y = Mathf.Clamp(vec.y, (float) min, (float) max);
    vec.z = Mathf.Clamp(vec.z, (float) min, (float) max);
  }

  private static void ClampVectorComponents(ref Vector3 vec, Vector3 minVector, Vector3 maxVector)
  {
    vec.x = Mathf.Clamp(vec.x, minVector.x, maxVector.x);
    vec.y = Mathf.Clamp(vec.y, minVector.y, maxVector.y);
    vec.z = Mathf.Clamp(vec.z, minVector.z, maxVector.z);
  }

  public static MovementFrame TurnByPitchYawStrikes(MovementFrame prevFrame, Vector3 pitchYawRollFactor, Vector3 strafeDirection, float strafeMagnitude, MovementOptions options)
  {
    Vector3 vector3_1 = options.maxTurnAcceleration.ComponentsToVector3();
    Vector3 vector3_2 = options.maxTurnSpeed.ComponentsToVector3();
    Vector3 nextPosition = prevFrame.nextPosition;
    Euler3 nextEuler3 = prevFrame.NextEuler3;
    Quaternion rotation = nextEuler3.rotation;
    Simulation.ClampVectorComponents(ref pitchYawRollFactor, -1, 1);
    Vector3 linearSpeed = Simulation.AdvanceLinearSpeed(prevFrame, options);
    float strafeX = strafeDirection.x * strafeMagnitude;
    float strafeY = strafeDirection.y * strafeMagnitude;
    Vector3 strafeSpeed = Simulation.AdvanceStrafingSpeed(prevFrame, options, strafeX, strafeY);
    Vector3 vector3_3 = Quaternion.Inverse(rotation) * prevFrame.Euler3Speed.ComponentsToVector3();
    Vector3 vector3_4 = pitchYawRollFactor;
    vector3_4.Scale(vector3_2);
    Vector3 vec = vector3_4 - vector3_3;
    Vector3 maxVector = vector3_1 * 0.1f;
    Simulation.ClampVectorComponents(ref vec, -maxVector, maxVector);
    Vector3 vector3_5 = vector3_3 + vec;
    if ((double) Mathf.Abs(pitchYawRollFactor.z) < 0.00999999977648258)
      vector3_5.z = Simulation.GetNewSpeed(vector3_5.z, 0.0f, options.rollAcceleration);
    Vector3 vector3_6 = rotation * vector3_5;
    Euler3 euler3Speed = new Euler3(vector3_6.x, vector3_6.y, vector3_6.z);
    MovementFrame movementFrame = new MovementFrame(nextPosition, nextEuler3, linearSpeed, strafeSpeed, euler3Speed);
    movementFrame.mode = 2;
    Quaternion rotationDelta = rotation * Quaternion.Inverse(prevFrame.rotation) * Quaternion.identity;
    movementFrame.ActiveThrusterEffects = Simulation.DetermineThrusterEffects(rotationDelta, strafeX, strafeY);
    return movementFrame;
  }

  private static bool ShipIsStrafing(float strafeX, float strafeY)
  {
    return (double) strafeX * (double) strafeX + (double) strafeY * (double) strafeY > 0.00999999977648258;
  }

  private static List<ThrusterEffect> DetermineThrusterEffects(Quaternion rotationDelta, float strafeX, float strafeY)
  {
    List<ThrusterEffect> thrusterEffectList = new List<ThrusterEffect>();
    if (Simulation.ShipIsStrafing(strafeX, strafeY))
    {
      ThrusterEffect thrusterEffect = (ThrusterEffect) new ThrusterEffectStrafing() { ThrusterDirectionLocal = new Vector3(-strafeX, -strafeY, 0.0f) };
      thrusterEffectList.Add(thrusterEffect);
    }
    thrusterEffectList.Add((ThrusterEffect) new ThrusterEffectTurning()
    {
      ShipRotationDelta = rotationDelta
    });
    return thrusterEffectList;
  }

  private static Vector3 FindVelocityForRotationDelta(Vector3 localDeltaAngles, float damping)
  {
    return Simulation.FindVelocityForRotationDelta(localDeltaAngles, damping * Vector3.one);
  }

  private static Vector3 FindVelocityForRotationDelta(Vector3 localDeltaAngles, Vector3 damping)
  {
    float angle;
    Vector3 axis;
    Quaternion.Euler(localDeltaAngles).ToAngleAxis(out angle, out axis);
    return new Vector3(damping.x * axis.x, damping.y * axis.y, damping.z * axis.z) * angle;
  }

  private static Vector3 FindVelocityForRotationDelta(Quaternion currentRotation, Vector3 globalDeltaAngles, float damping)
  {
    return Simulation.FindVelocityForRotationDelta(Quaternion.Inverse(currentRotation) * globalDeltaAngles, damping);
  }

  private static Vector3 FindVelocityForRotationDelta(Quaternion currentRotation, Vector3 globalDeltaAngles, Vector3 damping)
  {
    return Simulation.FindVelocityForRotationDelta(Quaternion.Inverse(currentRotation) * globalDeltaAngles, damping);
  }

  private static Vector3 KeepVectorComponentsWithinPlusMinus180(Vector3 source)
  {
    source.x = source.x % 360f;
    source.y = source.y % 360f;
    source.z = source.z % 360f;
    source.x = (double) source.x <= 180.0 ? source.x : source.x - 360f;
    source.x = (double) source.x >= -180.0 ? source.x : source.x + 360f;
    source.y = (double) source.y <= 180.0 ? source.y : source.y - 360f;
    source.y = (double) source.y >= -180.0 ? source.y : source.y + 360f;
    source.z = (double) source.z <= 180.0 ? source.z : source.z - 360f;
    source.z = (double) source.z >= -180.0 ? source.z : source.z + 360f;
    return source;
  }

  private static Vector3 FindFinalRotationAngles(Quaternion currentRotation, Vector3 angularVelocity, float damping)
  {
    return (currentRotation * Quaternion.AngleAxis(angularVelocity.magnitude / damping, angularVelocity.normalized)).eulerAngles;
  }

  private static Vector3 FindFinalRotationAngles(Quaternion currentRotation, Vector3 angularVelocity, Vector3 damping)
  {
    Vector3 normalized = angularVelocity.normalized;
    Vector3 eulerAngles = Quaternion.AngleAxis(new Vector3(angularVelocity.x / damping.x, angularVelocity.y / damping.y, angularVelocity.z / damping.z).magnitude, new Vector3(normalized.x / damping.x, normalized.y / damping.y, normalized.z / damping.z).normalized).eulerAngles;
    return currentRotation * eulerAngles;
  }

  private static Vector3 FindMaximumVelocity(Vector3 acceleration, float damping)
  {
    return Simulation.FindMaximumVelocity(acceleration, Vector3.one * damping);
  }

  private static Vector3 FindMaximumVelocity(Vector3 acceleration, Vector3 damping)
  {
    Vector3 scale = new Vector3(1f / damping.x, 1f / damping.y, 1f / damping.z);
    acceleration.Scale(scale);
    return acceleration;
  }

  private static Vector3 FindDamping(Vector3 acceleration, Vector3 maximumVelocity)
  {
    return new Vector3(acceleration.x / maximumVelocity.x, acceleration.y / maximumVelocity.y, acceleration.z / maximumVelocity.z);
  }

  private static float FindDamping(float acceleration, float maximumVelocity)
  {
    return acceleration / maximumVelocity;
  }

  private static Vector3 findShortestAnglesAbsolute(Vector3 eulerRotation)
  {
    return new Vector3((double) eulerRotation.x <= 180.0 ? eulerRotation.x : Mathf.Abs(eulerRotation.x - 360f), (double) eulerRotation.y <= 180.0 ? eulerRotation.y : Mathf.Abs(eulerRotation.y - 360f), (double) eulerRotation.z <= 180.0 ? eulerRotation.z : Mathf.Abs(eulerRotation.z - 360f));
  }

  public static MovementFrame MoveToDirectionWithoutRoll(MovementFrame prevFrame, Euler3 targetEuler3, MovementOptions options)
  {
    Quaternion rotation = prevFrame.Euler3Speed.rotation;
    Quaternion changePerSecond = Quaternion.RotateTowards(Quaternion.identity, Simulation.ScaleWithMax(Simulation.ScaleWithMax(Quaternion.FromToRotation(prevFrame.euler3.direction, targetEuler3.direction), 2f, options.yawMaxSpeed) * Quaternion.Inverse(rotation), 8f, options.yawAcceleration), options.yawAcceleration);
    Euler3 euler3Speed = Euler3.Rotation(Quaternion.RotateTowards(Quaternion.identity, Euler3.RotateOverTime(rotation, changePerSecond, 0.1f), options.yawMaxSpeed));
    Vector3 linearSpeed = Simulation.AdvanceLinearSpeed(prevFrame, options);
    Vector3 strafeSpeed = Simulation.AdvanceStrafingSpeed(prevFrame, options, 0.0f, 0.0f);
    Euler3 nextEuler3 = prevFrame.NextEuler3;
    MovementFrame movementFrame = new MovementFrame(prevFrame.nextPosition, nextEuler3, linearSpeed, strafeSpeed, euler3Speed);
    movementFrame.mode = 2;
    Quaternion rotationDelta = nextEuler3.rotation * Quaternion.Inverse(prevFrame.rotation) * Quaternion.identity;
    movementFrame.ActiveThrusterEffects = Simulation.DetermineThrusterEffects(rotationDelta, 0.0f, 0.0f);
    return movementFrame;
  }

  public static Quaternion ScaleWithMax(Quaternion q, float scale, float max)
  {
    if ((double) q.w < 0.0)
      q = new Quaternion(-q.x, -q.y, -q.z, -q.w);
    float angle;
    Vector3 axis;
    q.ToAngleAxis(out angle, out axis);
    return Quaternion.AngleAxis(Mathf.Clamp(angle * scale, 0.0f, max), axis);
  }

  public static Quaternion RotateOverTimeFromReferenceFrame(Quaternion rotateThis, Quaternion byThis, Quaternion asIfMeasuredFromThis, float dt)
  {
    Quaternion quaternion = Euler3.RotateOverTime(Quaternion.Inverse(asIfMeasuredFromThis) * rotateThis, byThis, dt);
    return asIfMeasuredFromThis * quaternion;
  }

  public static float SlowingThrust(float v, float maxAccelThisFrame)
  {
    return ((double) v >= 0.0 ? -1f : 1f) * Mathf.Min(Mathf.Abs(v), Mathf.Abs(maxAccelThisFrame));
  }

  private static Vector3 Clamp(Vector3 val, Vector3 min, Vector3 max)
  {
    return Vector3.Min(Vector3.Max(val, min), max);
  }

  public static Vector3 ClampToRotatedBox(Vector3 v, Vector3 halfSideLengths, Quaternion boxOrientation)
  {
    Vector3 vector3 = Simulation.Clamp(Quaternion.Inverse(boxOrientation) * v, -halfSideLengths, halfSideLengths);
    return boxOrientation * vector3;
  }

  public static Vector3 ClipAtPitchYawBorderOfRotatedBox(Vector3 v, Vector3 halfSideLengths, Quaternion boxOrientation)
  {
    Vector3 vector3 = Simulation.ClipEulerAgainstMaxPitchYaw(Quaternion.Inverse(boxOrientation) * v, -halfSideLengths, halfSideLengths);
    return boxOrientation * vector3;
  }

  public static MovementFrame QWEASD(MovementFrame prevFrame, float pitch, float yaw, float roll, MovementOptions options)
  {
    Vector3 a = new Vector3(pitch, yaw, roll);
    Vector3 vector3_1 = (options.maxTurnAcceleration * 0.1f).ComponentsToVector3();
    Vector3 vector3_2 = Vector3.Scale(a, vector3_1);
    Vector3 vector3_3 = prevFrame.Euler3Speed.ComponentsToVector3();
    if (true)
    {
      Vector3 vector3_4 = Quaternion.Inverse(prevFrame.rotation) * vector3_3;
      if ((double) vector3_2.x == 0.0)
        vector3_2.x = Simulation.SlowingThrust(vector3_4.x, vector3_1.x);
      if ((double) vector3_2.y == 0.0)
        vector3_2.y = Simulation.SlowingThrust(vector3_4.y, vector3_1.y);
      if ((double) vector3_2.z == 0.0)
        vector3_2.z = Simulation.SlowingThrust(vector3_4.z, vector3_1.z);
    }
    Vector3 vector3_5 = prevFrame.rotation * vector3_2;
    Vector3 rotatedBox = Simulation.ClampToRotatedBox(vector3_3 + vector3_5, options.maxTurnSpeed.ComponentsToVector3(), prevFrame.rotation);
    Euler3 euler3Speed = new Euler3();
    euler3Speed.ComponentsFromVector3(rotatedBox);
    Vector3 linearSpeed = Simulation.AdvanceLinearSpeed(prevFrame, options);
    Vector3 strafeSpeed = Simulation.AdvanceStrafingSpeed(prevFrame, options, 0.0f, 0.0f);
    Euler3 nextEuler3 = prevFrame.NextEuler3;
    MovementFrame movementFrame = new MovementFrame(prevFrame.nextPosition, nextEuler3, linearSpeed, strafeSpeed, euler3Speed);
    movementFrame.mode = 2;
    Quaternion rotationDelta = nextEuler3.rotation * Quaternion.Inverse(prevFrame.rotation) * Quaternion.identity;
    movementFrame.ActiveThrusterEffects = Simulation.DetermineThrusterEffects(rotationDelta, 0.0f, 0.0f);
    return movementFrame;
  }

  public static Vector3 AdvanceLinearSpeed(MovementFrame prevFrame, MovementOptions options)
  {
    Vector3 rhs = prevFrame.linearSpeed;
    Vector3 vector3_1 = rhs;
    if (options.gear != Gear.RCS)
    {
      Vector3 direction = prevFrame.euler3.direction;
      float current = Vector3.Dot(direction, rhs);
      Vector3 vector3_2 = direction * current;
      Vector3 vector3_3 = rhs - vector3_2;
      float magnitude = vector3_3.magnitude;
      vector3_1 = vector3_3.normalized * Simulation.GetNewSpeed(magnitude, 0.0f, options.inertiaCompensation) + direction * Simulation.GetNewSpeed(current, options.speed, options.acceleration);
    }
    return vector3_1;
  }

  public static Vector3 AdvanceStrafingSpeed(MovementFrame prevFrame, MovementOptions options, float strafeX, float strafeY)
  {
    Vector3 vector3_1 = prevFrame.strafeSpeed;
    strafeX = Mathf.Clamp(strafeX, -1f, 1f);
    strafeY = Mathf.Clamp(strafeY, -1f, 1f);
    Vector3 vector3_2 = Quaternion.Inverse(prevFrame.rotation) * vector3_1;
    float target1 = strafeX * options.strafeMaxSpeed;
    float target2 = strafeY * options.strafeMaxSpeed;
    vector3_2.x = Simulation.GetNewSpeed(vector3_2.x, target1, ((double) vector3_2.x * (double) target1 >= -1.0 ? 1f : 2f) * options.strafeAcceleration);
    vector3_2.y = Simulation.GetNewSpeed(vector3_2.y, target2, ((double) vector3_2.y * (double) target2 >= -1.0 ? 1f : 2f) * options.strafeAcceleration);
    vector3_2.z = Simulation.GetNewSpeed(vector3_2.z, 0.0f, options.strafeAcceleration);
    return prevFrame.rotation * vector3_2;
  }

  public static float GetNewSpeed(float current, float target, float acceleration)
  {
    float num = acceleration * 0.1f;
    float f = current - target;
    if ((double) Mathf.Abs(f) < (double) num)
      return target;
    if ((double) f > 0.0)
      return current - num;
    return current + num;
  }

  private static bool IsPowerOfTwoOrZero(Simulation.OutCode x)
  {
    return (x & x - 1) == Simulation.OutCode.INSIDE;
  }

  private static Simulation.OutCode ComputeOutCode(Vector3 euler, Vector3 min, Vector3 max)
  {
    Simulation.OutCode x = Simulation.OutCode.INSIDE;
    if ((double) euler.x < (double) min.x)
      x |= Simulation.OutCode.LEFT;
    else if ((double) euler.x > (double) max.x)
      x |= Simulation.OutCode.RIGHT;
    if ((double) euler.y < (double) min.y)
      x |= Simulation.OutCode.BOTTOM;
    else if ((double) euler.y > (double) max.y)
      x |= Simulation.OutCode.TOP;
    if (!Simulation.IsPowerOfTwoOrZero(x))
    {
      float num1 = euler.y / euler.x;
      switch (x)
      {
        case Simulation.OutCode.LEFT | Simulation.OutCode.BOTTOM:
          float num2 = max.y / max.x;
          x = (double) num1 <= (double) num2 ? Simulation.OutCode.LEFT : Simulation.OutCode.BOTTOM;
          break;
        case Simulation.OutCode.RIGHT | Simulation.OutCode.BOTTOM:
          float num3 = -max.y / max.x;
          x = (double) num1 <= (double) num3 ? Simulation.OutCode.BOTTOM : Simulation.OutCode.RIGHT;
          break;
        case Simulation.OutCode.LEFT | Simulation.OutCode.TOP:
          float num4 = -max.y / max.x;
          x = (double) num1 <= (double) num4 ? Simulation.OutCode.TOP : Simulation.OutCode.LEFT;
          break;
        case Simulation.OutCode.RIGHT | Simulation.OutCode.TOP:
          float num5 = max.y / max.x;
          x = (double) num1 <= (double) num5 ? Simulation.OutCode.RIGHT : Simulation.OutCode.TOP;
          break;
      }
    }
    return x;
  }

  private static Vector3 ClipEulerAgainstMaxPitchYaw(Vector3 val, Vector3 min, Vector3 max)
  {
    Simulation.OutCode outCode1 = Simulation.ComputeOutCode(val, min, max);
    if ((Simulation.OutCode.INSIDE | outCode1) != Simulation.OutCode.INSIDE)
    {
      Simulation.OutCode outCode2 = outCode1;
      if ((outCode2 & Simulation.OutCode.TOP) != Simulation.OutCode.INSIDE)
      {
        val.x = val.x * max.y / val.y;
        val.y = max.y;
      }
      else if ((outCode2 & Simulation.OutCode.BOTTOM) != Simulation.OutCode.INSIDE)
      {
        val.x = val.x * min.y / val.y;
        val.y = min.y;
      }
      else if ((outCode2 & Simulation.OutCode.RIGHT) != Simulation.OutCode.INSIDE)
      {
        val.y = val.y * max.x / val.x;
        val.x = max.x;
      }
      else if ((outCode2 & Simulation.OutCode.LEFT) != Simulation.OutCode.INSIDE)
      {
        val.y = val.y * min.x / val.x;
        val.x = min.x;
      }
    }
    val.z = Mathf.Clamp(val.z, min.z, max.z);
    return val;
  }

  [Flags]
  private enum OutCode
  {
    INSIDE = 0,
    LEFT = 1,
    RIGHT = 2,
    BOTTOM = 4,
    TOP = 8,
  }
}
