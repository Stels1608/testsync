﻿// Decompiled with JetBrains decompiler
// Type: GUIImageNew
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

public class GUIImageNew : GUIPanel
{
  private Rect innerRect = new Rect(0.0f, 0.0f, 1f, 1f);
  private int padding;
  private Texture2D texture;

  public int Padding
  {
    get
    {
      return this.padding;
    }
    set
    {
      this.padding = value;
    }
  }

  public Texture2D Texture
  {
    get
    {
      return this.texture;
    }
    set
    {
      this.texture = value;
    }
  }

  public Rect InnerRect
  {
    get
    {
      return this.innerRect;
    }
    set
    {
      this.innerRect = value;
    }
  }

  public GUIImageNew(JImage image)
  {
    this.IsRendered = true;
    this.Name = image.name;
    this.Texture = ResourceLoader.Load<Texture2D>(image.texture);
    this.SmartRect.Width = image.size.x;
    this.SmartRect.Height = image.size.y;
    this.Position = image.position;
    this.Padding = this.padding;
  }

  public GUIImageNew(Texture2D texture)
    : this(texture, (SmartRect) null)
  {
  }

  public GUIImageNew(Texture2D texture, SmartRect parent)
    : base(parent)
  {
    this.Name = "GUIImageNew";
    this.IsRendered = true;
    this.Texture = texture;
    this.Rect = new Rect((float) -this.Texture.width / 2f, (float) -this.Texture.height / 2f, (float) this.Texture.width, (float) this.Texture.height);
  }

  public void TakeTextureSize()
  {
    this.SmartRect.Width = (float) this.Texture.width;
    this.SmartRect.Height = (float) this.Texture.height;
  }

  public void MakeCenteredRect()
  {
    Size size = new Size(this.Texture.width, this.Texture.height);
    this.Rect = new Rect(this.Position.x - (float) size.width / 2f, this.Position.y - (float) size.height / 2f, (float) size.width, (float) size.height);
  }

  public override void Draw()
  {
    base.Draw();
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    Graphics.DrawTexture(this.SmartRect.AbsRect, (UnityEngine.Texture) this.texture, this.innerRect, this.padding, this.padding, this.padding, this.padding);
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }
}
