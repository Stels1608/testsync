﻿// Decompiled with JetBrains decompiler
// Type: SubscribeProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class SubscribeProtocol : BgoProtocol
{
  public SubscribeProtocol()
    : base(BgoProtocol.ProtocolID.Subscribe)
  {
  }

  public static SubscribeProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Subscribe) as SubscribeProtocol;
  }

  public void SubscribeStats(uint playerID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 4);
    bw.Write(playerID);
    this.SendMessage(bw);
  }

  public void UnsubscribeStats(uint playerID)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 5);
    bw.Write(playerID);
    this.SendMessage(bw);
  }

  public void RequestInfo(uint playerID, uint flags)
  {
    if ((int) flags == 0)
      return;
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    bw.Write(playerID);
    bw.Write(flags);
    this.SendMessage(bw);
  }

  public void SubscribeInfo(uint playerID, uint flags)
  {
    if ((int) flags == 0)
      return;
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 2);
    bw.Write(playerID);
    bw.Write(flags);
    this.SendMessage(bw);
  }

  public void UnsubscribeInfo(uint playerID, uint flags)
  {
    if ((int) flags == 0)
      return;
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 3);
    bw.Write(playerID);
    bw.Write(flags);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    SubscribeProtocol.Reply reply = (SubscribeProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case SubscribeProtocol.Reply.PlayerName:
        Game.Players[br.ReadUInt32()]._SetName(br.ReadString());
        break;
      case SubscribeProtocol.Reply.PlayerFaction:
        Game.Players[br.ReadUInt32()]._SetFaction((Faction) br.ReadByte());
        break;
      case SubscribeProtocol.Reply.PlayerAvatar:
        Game.Players[br.ReadUInt32()]._SetAvatar(br.ReadDesc<AvatarDescription>());
        break;
      case SubscribeProtocol.Reply.PlayerShips:
        uint index1 = br.ReadUInt32();
        string shipName = br.ReadString();
        int length = br.ReadLength();
        uint[] shipGUIDs = new uint[length];
        for (int index2 = 0; index2 < length; ++index2)
          shipGUIDs[index2] = br.ReadGUID();
        Game.Players[index1]._SetShips(shipName, shipGUIDs);
        break;
      case SubscribeProtocol.Reply.PlayerStatus:
        Game.Players[br.ReadUInt32()]._SetOnlineStatus(br.ReadBoolean());
        break;
      case SubscribeProtocol.Reply.PlayerLocation:
        uint playerID = br.ReadUInt32();
        br.ProcessLocation(playerID);
        break;
      case SubscribeProtocol.Reply.PlayerLevel:
        Game.Players[br.ReadUInt32()]._SetLevel(br.ReadByte());
        break;
      case SubscribeProtocol.Reply.PlayerGuild:
        uint index3 = br.ReadUInt32();
        uint wingID = br.ReadUInt32();
        uint num = br.ReadUInt32();
        string wingName = br.ReadString();
        Game.Players[index3]._SetWing(wingID, wingName, (GuildRole) num);
        break;
      case SubscribeProtocol.Reply.PlayerStats:
        Game.Players[br.ReadUInt32()].Stats.Read(br);
        break;
      case SubscribeProtocol.Reply.PlayerTitle:
        Game.Players[br.ReadUInt32()]._SetTitle(br.ReadGUID());
        break;
      case SubscribeProtocol.Reply.PlayerMedal:
        Game.Players[br.ReadUInt32()]._SetMedals((PvpMedal) br.ReadByte(), (TournamentMedal) br.ReadByte(), (KillerMedal) br.ReadByte(), (AssistMedal) br.ReadByte());
        break;
      case SubscribeProtocol.Reply.PlayerLogout:
        uint index4 = br.ReadUInt32();
        Game.Players[index4]._SetLogout(br.ReadLongDateTime());
        if (!(Game.Players[index4].lastLogout != new DateTime()))
          break;
        Debug.Log((object) ("Received information about the logout of " + Game.Players[index4].Name + " logged out at " + (object) Game.Players[index4].lastLogout));
        break;
      case SubscribeProtocol.Reply.PlayerTournamentIndicator:
        Game.Players[br.ReadUInt32()].TournamentIndicator = (TournamentIndicator) br.ReadByte();
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Subscribe Protocol: " + (object) reply);
        break;
    }
  }

  public enum Reply : ushort
  {
    PlayerName = 1,
    PlayerFaction = 2,
    PlayerAvatar = 3,
    PlayerShips = 4,
    PlayerStatus = 5,
    PlayerLocation = 6,
    PlayerLevel = 7,
    PlayerGuild = 8,
    PlayerStats = 9,
    PlayerTitle = 10,
    PlayerMedal = 11,
    PlayerLogout = 12,
    PlayerTournamentIndicator = 13,
  }

  public enum Request : ushort
  {
    Info = 1,
    Subscribe = 2,
    Unsubscribe = 3,
    SubscribeStats = 4,
    UnsubscribeStats = 5,
  }
}
