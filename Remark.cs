﻿// Decompiled with JetBrains decompiler
// Type: Remark
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class Remark : IProtocolRead
{
  public byte index;
  public string animationTag;
  public uint camPosition;
  public byte imageIndex;
  public string symbol;
  public string phraseRaw;

  public string phrase
  {
    get
    {
      return Tools.ParseMessage(this.phraseRaw);
    }
  }

  public void Read(BgoProtocolReader pr)
  {
    this.index = pr.ReadByte();
    this.phraseRaw = pr.ReadString();
    this.animationTag = pr.ReadString();
    this.camPosition = pr.ReadUInt32();
    this.imageIndex = pr.ReadByte();
    this.symbol = pr.ReadString();
  }
}
