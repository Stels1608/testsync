﻿// Decompiled with JetBrains decompiler
// Type: GuiRewardNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GuiRewardNotification : GuiDialog
{
  private readonly GuiLabel title;
  private readonly GuiPanelVerticalScroll scroll;

  public GuiRewardNotification(string text, RewardCard reward, AnonymousDelegate onCloseDelegate)
    : this(text, reward.Items, onCloseDelegate)
  {
    this.scroll.AddChild((GuiElementBase) new GuiItem((GUICard) null, reward.Experience.ToString() + " %$bgo.etc.rewards_experience%"));
    this.scroll.AddChild((GuiElementBase) new GuiSpaceY(5f));
    this.SizeY = (float) ((double) this.scroll.PositionY + (double) this.scroll.SizeY + 60.0);
  }

  public GuiRewardNotification(string text, List<ShipItem> reward, AnonymousDelegate onCloseDelegate)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiRewardNotification.\u003CGuiRewardNotification\u003Ec__AnonStorey9D notificationCAnonStorey9D = new GuiRewardNotification.\u003CGuiRewardNotification\u003Ec__AnonStorey9D();
    // ISSUE: reference to a compiler-generated field
    notificationCAnonStorey9D.onCloseDelegate = onCloseDelegate;
    this.title = new GuiLabel(Gui.Options.fontEurostileTBlaExt);
    this.scroll = new GuiPanelVerticalScroll();
    // ISSUE: explicit constructor call
    base.\u002Ector();
    // ISSUE: reference to a compiler-generated field
    notificationCAnonStorey9D.\u003C\u003Ef__this = this;
    this.Size = new Vector2(430f, 560f);
    this.Align = Align.MiddleCenter;
    this.title.WordWrap = true;
    this.title.Alignment = TextAnchor.UpperCenter;
    this.title.SizeX = this.SizeX - 60f;
    this.title.Text = text;
    this.title.FontSize = 14;
    this.AddChild((GuiElementBase) this.title, Align.UpCenter, new Vector2(0.0f, 20f));
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.etc.assignment_completed_reward%", Gui.Options.fontEurostileTRegCon), Align.UpCenter, new Vector2(0.0f, (float) (20.0 + (double) this.title.SizeY + 20.0)));
    this.scroll.Size = new Vector2(400f, 400f);
    this.AddChild((GuiElementBase) this.scroll, Align.UpCenter);
    // ISSUE: reference to a compiler-generated method
    this.OnClose = new AnonymousDelegate(notificationCAnonStorey9D.\u003C\u003Em__127);
    GuiButton guiButton = new GuiButton("%$bgo.common.ok%");
    guiButton.SizeY = 30f;
    guiButton.Label.Font = Gui.Options.fontEurostileTRegCon;
    guiButton.Pressed = this.OnClose;
    this.AddChild((GuiElementBase) guiButton, Align.DownCenter, new Vector2(0.0f, -15f));
    this.Fill(reward);
  }

  private void Fill(List<ShipItem> reward)
  {
    this.scroll.PositionY = (float) ((double) this.title.PositionY + (double) this.title.SizeY + 20.0 + 30.0);
    float num1 = 0.0f;
    using (List<ShipItem>.Enumerator enumerator = reward.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GuiItem guiItem = new GuiItem(enumerator.Current);
        this.scroll.AddChild((GuiElementBase) guiItem);
        this.scroll.AddChild((GuiElementBase) new GuiSpaceY(4f));
        num1 += guiItem.SizeY + 4f;
      }
    }
    if ((double) num1 >= (double) this.scroll.SizeY)
      return;
    GuiPanelVerticalScroll panelVerticalScroll = this.scroll;
    bool flag = false;
    this.scroll.RenderScrollLines = flag;
    int num2 = flag ? 1 : 0;
    panelVerticalScroll.RenderDelimiters = num2 != 0;
    this.SizeY = this.SizeY - this.scroll.SizeY + num1;
  }

  [Gui2Editor]
  public static void Show1(int i)
  {
    Game.RegisterDialog((IGUIPanel) new GuiRewardNotification("You Got a Reward!", Game.Me.MissionBook.GetByID(Game.Me.MissionBook.IDs[i]).missionCard.RewardCard.Items, (AnonymousDelegate) null), true);
  }

  [Gui2Editor]
  public static void Show2()
  {
    Game.DelayedActions.Add((DelayedActions.Predicate) (() =>
    {
      GuiRewardNotification.Show1(0);
      return true;
    }));
  }
}
