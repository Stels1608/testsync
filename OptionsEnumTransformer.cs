﻿// Decompiled with JetBrains decompiler
// Type: OptionsEnumTransformer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;

public static class OptionsEnumTransformer
{
  public static OptionsEnumWrapper AntiAliasingWrapped(int actualValue)
  {
    return new OptionsEnumWrapper() { valueText = new List<string>() { Tools.ParseMessage("%$bgo.etc.opt_auto_loot_off%"), "2x", "4x", "8x" }, valueList = new List<object>() { (object) 0, (object) 2, (object) 4, (object) 8 }, actualValue = (object) actualValue };
  }

  public static OptionsEnumWrapper MouseWheelWrapped(MouseWheelBinding actualValue)
  {
    return new OptionsEnumWrapper() { valueText = new List<string>() { Tools.ParseMessage("%$bgo.etc.opt_mouse_wheel_thrust%"), Tools.ParseMessage("%$bgo.etc.opt_mouse_wheel_zoom%") }, valueList = new List<object>() { (object) MouseWheelBinding.Thrust, (object) MouseWheelBinding.CameraZoom }, actualValue = (object) actualValue };
  }

  public static OptionsEnumWrapper BracketColorSchemeWrapped(HudIndicatorColorSchemeMode currentValue)
  {
    return new OptionsEnumWrapper() { valueText = new List<string>() { Tools.ParseMessage("%$bgo.etc.opt_color_scheme.friend_or_foe_colors%"), Tools.ParseMessage("%$bgo.etc.opt_color_scheme.faction_colors%") }, valueList = new List<object>() { (object) HudIndicatorColorSchemeMode.FriendOrFoeColors, (object) HudIndicatorColorSchemeMode.FactionColors }, actualValue = (object) currentValue };
  }
}
