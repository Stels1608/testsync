﻿// Decompiled with JetBrains decompiler
// Type: ShortCircuitNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class ShortCircuitNotification : OnScreenNotification
{
  public string TargetName { get; private set; }

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.ShortCircuit;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Negative;
    }
  }

  public override string TextMessage
  {
    get
    {
      return BsgoLocalization.Get("bgo.game_protocol.short_circuit_failed", (object) this.TargetName);
    }
  }

  public override bool Show
  {
    get
    {
      return true;
    }
  }

  public ShortCircuitNotification(string targetName)
  {
    this.TargetName = targetName;
  }
}
