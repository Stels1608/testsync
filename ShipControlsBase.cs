﻿// Decompiled with JetBrains decompiler
// Type: ShipControlsBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public abstract class ShipControlsBase : InputListener
{
  protected Vector3 directionInput = Vector3.zero;
  protected QWEASD qweasd = new QWEASD();
  protected Vector3 wantedDirection = Vector3.zero;
  private bool _enableControl = true;
  private const int FLUSHES_PER_SECOND = 30;
  private const float SPEED_DELTA_ON_MOUSE_WHEEL = 5f;
  private const float SPEED_DELTA_WHILE_BUTTON_PRESSED = 25f;
  public static DataChangeSpeed cachedData;
  public Gear CurrentGear;
  private float currentSpeed;
  public bool Enabled;
  public bool HighlightControl;
  protected bool joystickAxesMovementInputHappened;
  protected GameProtocol gameProtocol;
  private float nextSendOpportunityTime;
  private DataChangeSpeed nextChangeSpeedRequest;
  protected JoystickSteeringInputs joystickSteeringInputs;
  private ShipControlsBase.ConstantSpeedChangeMode constantSpeedChangeMode;

  public static bool AdvancedFlightControls { get; set; }

  public static float DeadZoneMouseRadiusFactor { get; set; }

  public bool SectorMapControlsEnabled { get; set; }

  public static MouseWheelBinding MouseWheelBinding { get; set; }

  public bool EnableControl
  {
    get
    {
      return this._enableControl;
    }
    set
    {
      this._enableControl = value;
      this.ResetMouseStates();
      this.ResetKeyStates();
      GameProtocol.GetProtocol().TurnByPitchYawStrikes(Vector3.zero, Vector2.zero, 0.0f);
    }
  }

  protected bool MouseBtnMiddleUp { get; set; }

  protected bool MouseBtnRightUp { get; set; }

  protected bool MouseBtnRightHeld { get; set; }

  public bool HandleMouseInput
  {
    get
    {
      return this.Enabled;
    }
  }

  public bool HandleKeyboardInput
  {
    get
    {
      return this.Enabled;
    }
  }

  public bool HandleJoystickAxesInput
  {
    get
    {
      return this.Enabled;
    }
  }

  protected ShipControlsBase()
  {
    this.gameProtocol = GameProtocol.GetProtocol();
    this.nextChangeSpeedRequest = new DataChangeSpeed(ShipControlsBase.SpeedMode.None, 0.0f);
    this.constantSpeedChangeMode = ShipControlsBase.ConstantSpeedChangeMode.None;
    this.joystickSteeringInputs = new JoystickSteeringInputs();
    if (ShipControlsBase.cachedData == null)
      return;
    this.ChangeCurrentSpeed(ShipControlsBase.cachedData.SpeedMode, ShipControlsBase.cachedData.SpeedChange);
    ShipControlsBase.cachedData = (DataChangeSpeed) null;
  }

  public void ChangeCurrentSpeed(ShipControlsBase.SpeedMode speedMode, float speedValue)
  {
    float num;
    switch (speedMode)
    {
      case ShipControlsBase.SpeedMode.Abs:
        num = speedValue;
        break;
      case ShipControlsBase.SpeedMode.Delta:
        num = this.currentSpeed + speedValue;
        speedMode = ShipControlsBase.SpeedMode.Abs;
        break;
      case ShipControlsBase.SpeedMode.Stop:
        num = 0.0f;
        break;
      case ShipControlsBase.SpeedMode.Full:
        num = Game.Me.Stats.MaxSpeed;
        break;
      default:
        num = 0.0f;
        Debug.LogError((object) "SpeedMode.None / default: Pathologic error case?");
        break;
    }
    float speedChange = Mathf.Clamp(num, 0.0f, Game.Me.Stats.MaxSpeed);
    if ((double) Math.Abs(this.currentSpeed - speedChange) > 0.00999999977648258)
      this.nextChangeSpeedRequest = new DataChangeSpeed(speedMode, speedChange);
    FacadeFactory.GetInstance().SendMessage(Message.ShipSpeedChangeDone, (object) speedChange);
    this.currentSpeed = speedChange;
  }

  public void ToggleBoost()
  {
    if (this.CurrentGear == Gear.Boost)
      this.SetActiveGear(Gear.Regular);
    else
      this.SetActiveGear(Gear.Boost);
  }

  public Gear GetActiveGear()
  {
    return this.CurrentGear;
  }

  public void SetActiveGear(Gear newActiveGear)
  {
    if (newActiveGear == Gear.Boost && Game.Me.Hold.Tylium <= 0U)
      newActiveGear = Gear.Regular;
    if (this.CurrentGear == newActiveGear)
      return;
    GameProtocol.GetProtocol().SetGear(newActiveGear);
    if (this.CurrentGear == Gear.Boost || newActiveGear == Gear.Boost)
      FacadeFactory.GetInstance().SendMessage(Message.ShipGearToggleBoostDone, (object) (newActiveGear == Gear.Boost));
    this.CurrentGear = newActiveGear;
  }

  protected virtual void FlushInputs()
  {
    this.joystickAxesMovementInputHappened = false;
    this.qweasd.Flush();
    this.MouseButtonsFlush();
  }

  private void MouseButtonsFlush()
  {
    this.MouseBtnRightUp = false;
    this.MouseBtnMiddleUp = false;
  }

  public void ResetKeyStates()
  {
    this.qweasd.ResetKeyStates();
  }

  public void ResetMouseStates()
  {
    bool flag1 = false;
    this.MouseBtnMiddleUp = flag1;
    bool flag2 = flag1;
    this.MouseBtnRightHeld = flag2;
    this.MouseBtnRightUp = flag2;
  }

  public virtual bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    switch (mouseKey)
    {
      case KeyCode.Mouse1:
        this.MouseBtnRightUp = false;
        this.MouseBtnRightHeld = true;
        return true;
      case KeyCode.Mouse2:
        this.MouseBtnMiddleUp = false;
        return true;
      default:
        return false;
    }
  }

  public virtual bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    switch (mouseKey)
    {
      case KeyCode.Mouse1:
        this.MouseBtnRightUp = true;
        this.MouseBtnRightHeld = false;
        return true;
      case KeyCode.Mouse2:
        this.MouseBtnMiddleUp = true;
        return true;
      default:
        return false;
    }
  }

  public void OnMouseMove(float2 mousePosition)
  {
  }

  public bool OnMouseScrollDown()
  {
    if (ShipControlsBase.MouseWheelBinding == MouseWheelBinding.Thrust ^ (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
      FacadeFactory.GetInstance().SendMessage(Message.ShipSpeedChangeCall, (object) new DataChangeSpeed(ShipControlsBase.SpeedMode.Delta, -5f));
    return false;
  }

  public bool OnMouseScrollUp()
  {
    if (ShipControlsBase.MouseWheelBinding == MouseWheelBinding.Thrust ^ (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
      FacadeFactory.GetInstance().SendMessage(Message.ShipSpeedChangeCall, (object) new DataChangeSpeed(ShipControlsBase.SpeedMode.Delta, 5f));
    return false;
  }

  private void SendAutoAlignRequest()
  {
    Vector3 vector3 = Game.Me.Ship.Rotation * Vector3.forward;
    GameProtocol.GetProtocol().MoveToDirection(Euler3.Direction(new Vector3(vector3.x, 0.0f, vector3.z)));
  }

  public virtual bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (!this.EnableControl)
      return false;
    if (this.qweasd.TryToggleAction(action, true))
      return true;
    Action action1 = action;
    switch (action1)
    {
      case Action.SpeedUp:
        this.constantSpeedChangeMode = ShipControlsBase.ConstantSpeedChangeMode.Increase;
        return true;
      case Action.SlowDown:
        this.constantSpeedChangeMode = ShipControlsBase.ConstantSpeedChangeMode.Decrease;
        return true;
      case Action.FullSpeed:
        FacadeFactory.GetInstance().SendMessage(Message.ShipSpeedChangeCall, (object) new DataChangeSpeed(ShipControlsBase.SpeedMode.Full, 0.0f));
        return true;
      case Action.Boost:
        FacadeFactory.GetInstance().SendMessage(Message.ShipGearChangeCall, (object) Gear.Boost);
        return true;
      case Action.Stop:
        FacadeFactory.GetInstance().SendMessage(Message.ShipSpeedChangeCall, (object) new DataChangeSpeed(ShipControlsBase.SpeedMode.Stop, 0.0f));
        return true;
      default:
        if (action1 != Action.AlignToHorizon)
        {
          if (action1 != Action.ToggleBoost)
            return false;
          FacadeFactory.GetInstance().SendMessage(Message.ShipGearToggleBoostCall);
          return true;
        }
        this.SendAutoAlignRequest();
        return false;
    }
  }

  public virtual bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (!this.EnableControl)
      return false;
    if (this.qweasd.TryToggleAction(action, false))
      return true;
    switch (action)
    {
      case Action.SpeedUp:
        this.constantSpeedChangeMode = ShipControlsBase.ConstantSpeedChangeMode.None;
        return true;
      case Action.SlowDown:
        this.constantSpeedChangeMode = ShipControlsBase.ConstantSpeedChangeMode.None;
        return true;
      case Action.Boost:
        FacadeFactory.GetInstance().SendMessage(Message.ShipGearChangeCall, (object) Gear.Regular);
        return true;
      default:
        return false;
    }
  }

  public virtual bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    if (isAxisInverted)
      magnitude *= -1f;
    float num = magnitude;
    if ((double) Mathf.Abs(magnitude) < (double) JoystickSetup.DeadZone)
      magnitude = 0.0f;
    if (action == Action.JoystickSpeedController)
      FacadeFactory.GetInstance().SendMessage(Message.ShipSpeedChangeCall, (object) new DataChangeSpeed(ShipControlsBase.SpeedMode.Abs, (float) (((double) num + 1.0) / 2.0) * Game.Me.Stats.MaxSpeed));
    else if (JoystickSteeringInputs.ActionIsJoystickInput(action))
    {
      if ((double) Mathf.Abs(magnitude) > (double) JoystickSetup.DeadZone || (double) Mathf.Abs(magnitude) < (double) JoystickSetup.DeadZone && (double) Mathf.Abs(this.joystickSteeringInputs.GetActionMagnitude(action)) >= (double) JoystickSetup.DeadZone)
        this.joystickAxesMovementInputHappened = true;
      this.joystickSteeringInputs.UpdateInputValue(action, magnitude);
    }
    return false;
  }

  public abstract void Move(Vector3 direction);

  public virtual void StopTurning()
  {
    GameProtocol.GetProtocol().TurnByPitchYawStrikes(Vector3.zero, Vector2.zero, 0.0f);
  }

  public virtual void Update()
  {
    if (this.constantSpeedChangeMode == ShipControlsBase.ConstantSpeedChangeMode.None)
      return;
    this.ChangeCurrentSpeed(ShipControlsBase.SpeedMode.Delta, (float) this.constantSpeedChangeMode * 25f * Time.deltaTime);
  }

  public void RequestFlush()
  {
    if ((double) Time.time < (double) this.nextSendOpportunityTime)
      return;
    if (this.TrySendNewInputs())
      this.nextSendOpportunityTime = Time.time + 0.03333334f;
    if (!this.TrySendSpeedUpdate())
      return;
    this.nextSendOpportunityTime = Time.time + 0.03333334f;
  }

  protected abstract bool TrySendNewInputs();

  private bool TrySendSpeedUpdate()
  {
    if (this.nextChangeSpeedRequest.SpeedMode == ShipControlsBase.SpeedMode.None)
      return false;
    GameProtocol.GetProtocol().SetSpeed(this.nextChangeSpeedRequest.SpeedMode, this.nextChangeSpeedRequest.SpeedChange);
    this.nextChangeSpeedRequest = new DataChangeSpeed(ShipControlsBase.SpeedMode.None, 0.0f);
    return true;
  }

  protected abstract void SendControl();

  protected void SendStoryProtocol(StoryProtocol.ControlType controlType)
  {
    if (!this.HighlightControl)
      return;
    StoryProtocol.GetProtocol().TriggerControl(controlType);
  }

  public enum SpeedMode
  {
    None = -1,
    Abs = 0,
    Delta = 1,
    Stop = 2,
    Full = 3,
  }

  private enum ConstantSpeedChangeMode
  {
    Decrease = -1,
    None = 0,
    Increase = 1,
  }
}
