﻿// Decompiled with JetBrains decompiler
// Type: PlayAmbientSoundAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class PlayAmbientSoundAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    AudioClip audioClip = (AudioClip) message.Data;
    string name = "ambientSound_" + audioClip.name;
    AudioListener objectOfType = Object.FindObjectOfType<AudioListener>();
    if ((Object) objectOfType == (Object) null)
    {
      Debug.LogWarning((object) ("Cannot play ambient sound " + audioClip.name + ", AudioListener is null"));
    }
    else
    {
      AudioSource audioSource = new GameObject(name).AddComponent<AudioSource>();
      audioSource.clip = audioClip;
      audioSource.loop = true;
      Transform transform = audioSource.gameObject.transform;
      transform.parent = objectOfType.transform;
      transform.localPosition = Vector3.zero;
      audioSource.Play();
    }
  }
}
