﻿// Decompiled with JetBrains decompiler
// Type: CutsceneSequenceTut1CylonDroneActivation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CutsceneSequenceTut1CylonDroneActivation : CutsceneSequenceBase
{
  private const string animClipName = "Tut1_Drone_Activation_Cylon";

  protected override string GetSceneSetupName
  {
    get
    {
      return "cutscene_tut1_drone_activation_cylon";
    }
  }

  protected override CutsceneSkipMode GetSkipMode
  {
    get
    {
      return CutsceneSkipMode.ShowMenu;
    }
  }

  protected override bool ExecuteCustomPreparations()
  {
    if ((Object) this.CutsceneSetup.AnimationChild == (Object) null)
    {
      Debug.LogError((object) "CutsceneSequenceTut1ColonialDroneActivation: cutscene setup's animation child is null. Aborting...");
      return false;
    }
    if (!((Object) this.CutsceneSetup.AnimationChild.GetClip(this.GetAnimClipName()) == (Object) null))
      return true;
    Debug.LogError((object) ("CutsceneSequenceTut1ColonialDroneActivation: Clip " + this.GetAnimClipName() + " doesn't exist in animation component of child \"" + this.CutsceneSetup.AnimationChild.name + "\". Aborting..."));
    return false;
  }

  protected override void StartScene()
  {
    this.CutsceneSetup.AnimationChild.Play(this.GetAnimClipName());
  }

  protected override void Cleanup()
  {
    this.ShowRealPlayerShip(true);
  }

  protected string GetAnimClipName()
  {
    return "Tut1_Drone_Activation_Cylon";
  }
}
