﻿// Decompiled with JetBrains decompiler
// Type: SectorTournament
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class SectorTournament
{
  public uint TournamentScore;
  public uint TournamentDeaths;
  public uint TournamentKills;
  private TournamentCard tournamentCard;

  public bool IsActive { get; set; }

  public DateTime EndTime { get; set; }

  public List<ushort> AllowedTiers { get; set; }

  public ITournamentGraphics Graphics { get; private set; }

  public TournamentCard TournamentCard
  {
    get
    {
      return this.tournamentCard;
    }
    set
    {
      this.tournamentCard = value;
      this.Graphics = (ITournamentGraphics) new DefaultTournamentGraphics();
    }
  }

  public SectorTournament()
  {
    this.IsActive = false;
    this.Graphics = (ITournamentGraphics) new DefaultTournamentGraphics();
  }

  public bool MayParticipate(HangarShip ship)
  {
    if (this.AllowedTiers == null || this.AllowedTiers.Count == 0)
      return false;
    return this.AllowedTiers.Contains((ushort) ship.Card.Tier);
  }
}
