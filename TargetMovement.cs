﻿// Decompiled with JetBrains decompiler
// Type: TargetMovement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TargetMovement : MonoBehaviour
{
  private Transform targetObject;
  private Vector3 startPoint;
  private Vector3 shift;
  private float deltaMoving;
  private float movementSpeed;
  private bool IsDied;
  private bool IsMiss;
  private bool IsDestroying;
  private float timeToDied;

  public void MovingStart(Transform startObject, Transform targetObject, float speed)
  {
    this.movementSpeed = speed;
    this.startPoint = startObject.position;
    this.targetObject = targetObject;
    this.transform.position = startObject.position;
    this.transform.rotation = startObject.rotation;
    float max = (float) (1.0 + (double) (targetObject.position - this.startPoint).magnitude / 100.0);
    this.shift = new Vector3(Random.Range(-max, max), Random.Range(-max, max), Random.Range(-max, max));
    this.IsMiss = (double) Random.Range(0.0f, 1f) > 0.800000011920929;
  }

  private void LateUpdate()
  {
    if (!this.IsDied)
    {
      if ((double) this.deltaMoving >= 1.0 || (Object) this.targetObject == (Object) null)
      {
        this.IsDied = true;
        this.timeToDied = Time.time + 2f;
      }
      else
      {
        Vector3 vector3 = this.targetObject.position + this.shift;
        this.transform.rotation = Quaternion.LookRotation(vector3 - this.transform.position, Vector3.up);
        this.transform.position = Vector3.Lerp(this.startPoint, vector3, this.deltaMoving);
        this.deltaMoving += this.movementSpeed / Vector3.Distance(this.startPoint, vector3) * Time.deltaTime;
      }
    }
    if (this.IsDied)
    {
      this.transform.position = this.transform.position + this.transform.forward * this.movementSpeed * Time.deltaTime;
      this.IsDestroying = (double) Time.time > (double) this.timeToDied || !this.IsMiss;
    }
    if (!this.IsDestroying)
      return;
    Object.Destroy((Object) this.gameObject);
  }
}
