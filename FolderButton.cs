﻿// Decompiled with JetBrains decompiler
// Type: FolderButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FolderButton : MonoBehaviour
{
  public GameObject actionList;
  public UITable table;

  public void OnClick()
  {
    this.actionList.transform.localScale = (double) this.actionList.transform.localScale.x != 0.0 ? Vector3.zero : Vector3.one;
    this.table.Reposition();
  }
}
