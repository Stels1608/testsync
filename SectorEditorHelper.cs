﻿// Decompiled with JetBrains decompiler
// Type: SectorEditorHelper
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SectorEditorHelper
{
  public static string HelperDefaultIP
  {
    get
    {
      return PlayerPrefs.GetString("PlayerDefaultIPAddress");
    }
    set
    {
      PlayerPrefs.SetString("PlayerDefaultIPAddress", value);
    }
  }

  public static string HelperDefaultPort
  {
    get
    {
      return PlayerPrefs.GetString("PlayerDefaultPort");
    }
    set
    {
      PlayerPrefs.SetString("PlayerDefaultPort", value);
    }
  }

  public static string WarningMessage
  {
    get
    {
      return "You should be in\nEditor Level Mode\nto use the editor.";
    }
  }

  public static float BigButtonHeight
  {
    get
    {
      return 35f;
    }
  }

  public static float MeduimButtonHeight
  {
    get
    {
      return 25f;
    }
  }

  public static bool UseEditorModelMode
  {
    get
    {
      return PlayerPrefs.GetInt("UseEditorModelMode") == 1;
    }
    set
    {
      PlayerPrefs.SetInt("UseEditorModelMode", !value ? 0 : 1);
    }
  }

  public static bool IsEditorLevel()
  {
    bool flag = false;
    if (Application.isEditor)
      flag = Application.loadedLevelName.ToLower().Contains("editorlevel");
    return flag;
  }

  public static int MouseScrollArrowKeyAdjustValue(int mobType)
  {
    Event current = Event.current;
    if (current.type == UnityEngine.EventType.KeyDown || current.type == UnityEngine.EventType.ScrollWheel)
    {
      if (current.keyCode == KeyCode.UpArrow || (double) current.delta.y < 0.0)
        --mobType;
      if (current.keyCode == KeyCode.DownArrow || (double) current.delta.y > 0.0)
        ++mobType;
    }
    return mobType;
  }

  public static void EditorModelMode(SpaceObject sObj)
  {
    if (!SectorEditorHelper.IsEditorLevel())
      return;
    GameObject gameObject = sObj.Root.gameObject;
    RootScript[] components = gameObject.GetComponents<RootScript>();
    GameObject model = sObj.Model;
    if (components == null || !SectorEditorHelper.UseEditorModelMode)
      return;
    Body[] componentsInChildren = model.GetComponentsInChildren<Body>();
    if (componentsInChildren.Length == 0)
      return;
    Renderer renderer = componentsInChildren[0].Renderers[0];
    if ((Object) null == (Object) renderer)
      return;
    MeshFilter component1 = renderer.gameObject.GetComponent<MeshFilter>();
    if ((Object) null == (Object) component1)
      return;
    GizmoIcon component2 = model.GetComponent<GizmoIcon>();
    MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
    if ((Object) meshFilter == (Object) null)
      meshFilter = gameObject.AddComponent<MeshFilter>();
    MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
    if ((Object) meshRenderer == (Object) null)
      meshRenderer = gameObject.AddComponent<MeshRenderer>();
    meshFilter.mesh = component1.mesh;
    meshRenderer.materials = renderer.materials;
    if ((Object) null != (Object) component2)
    {
      GizmoIcon gizmoIcon = gameObject.GetComponent<GizmoIcon>();
      if ((Object) gizmoIcon == (Object) null)
        gizmoIcon = gameObject.AddComponent<GizmoIcon>();
      gizmoIcon.Name = component2.Name;
    }
    Object.Destroy((Object) model);
  }
}
