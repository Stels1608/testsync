﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using SystemMap3D;
using UnityEngine;

public class SystemMap3DManager : MonoBehaviour, ISettingsReceiver
{
  private TransitionMode transitionModeOption = TransitionMode.Smooth;
  private SystemMap3DCameraView cameraView;
  private SystemMap3DWindowViewUi windowView;
  private SystemMap3DIconView iconView;
  private SystemMap3DGridView gridView;
  private List<ISystemMap3DView> views;
  private bool isStarFogActivated;
  private bool _isSectorMapShown;

  public bool IsSectorMapShown
  {
    get
    {
      return this._isSectorMapShown;
    }
    set
    {
      if (value == this._isSectorMapShown)
        return;
      this._isSectorMapShown = value;
      if (this._isSectorMapShown)
        this.TransitionToMap();
      else
        this.TransitionToGame();
      SpaceLevel.GetLevel().ShipControls.SectorMapControlsEnabled = this._isSectorMapShown;
      FacadeFactory.GetInstance().SendMessage(Message.SettingChangedStarFog, (object) (bool) (this._isSectorMapShown ? 0 : (this.isStarFogActivated ? 1 : 0)));
      GalaxyMapMain galaxyMapMain = Game.GUIManager.Find<GalaxyMapMain>();
      if (galaxyMapMain == null)
        return;
      galaxyMapMain.IsSystemMap3DShown = this._isSectorMapShown;
    }
  }

  public SystemMap3DWindowViewUi WindowView
  {
    get
    {
      return this.windowView;
    }
  }

  public SystemMap3DIconView IconView
  {
    get
    {
      return this.iconView;
    }
  }

  public SystemMap3DCameraView CameraView
  {
    get
    {
      return this.cameraView;
    }
  }

  public SystemMap3DGridView GridView
  {
    get
    {
      return this.gridView;
    }
  }

  private void Awake()
  {
    this.views = new List<ISystemMap3DView>();
    this.windowView = this.RegisterView((ISystemMap3DView) SystemMap3DWindowViewUi.Create(this)) as SystemMap3DWindowViewUi;
    this.cameraView = this.RegisterView((ISystemMap3DView) SystemMap3DCameraView.Create(this)) as SystemMap3DCameraView;
    this.iconView = this.RegisterView((ISystemMap3DView) SystemMap3DIconView.Create(this)) as SystemMap3DIconView;
    this.gridView = this.RegisterView((ISystemMap3DView) SystemMap3DGridView.Create(this)) as SystemMap3DGridView;
    using (List<ISystemMap3DView>.Enumerator enumerator = this.views.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.IsRendered = false;
    }
    this.SetSectorSize(SpaceLevel.GetLevel().GetSectorSize());
    GameLevel.Instance.IsLoaded.AddHandler((SignalHandler) (() => FacadeFactory.GetInstance().SendMessage(Message.RequestSettings, (object) this)));
  }

  private void Update()
  {
    GUIManager.MouseOverAnyOldGuiElement |= this.IsSectorMapShown;
  }

  private ISystemMap3DView RegisterView(ISystemMap3DView view)
  {
    this.views.Add(view);
    return view;
  }

  public void Destroy()
  {
    using (List<ISystemMap3DView>.Enumerator enumerator = this.views.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Destroy();
    }
  }

  private void SetSectorSize(float2 sectorSize)
  {
    float num = Mathf.Max(sectorSize.x, sectorSize.y);
    this.gridView.SetDiameter(num);
    this.cameraView.SetSectorSize(num);
  }

  public void ShutDownImmediately()
  {
    TransitionMode transitionMode = this.transitionModeOption;
    this.transitionModeOption = TransitionMode.Instant;
    this.IsSectorMapShown = false;
    this.transitionModeOption = transitionMode;
  }

  private void TransitionToMap()
  {
    this.TransitionToMap(this.transitionModeOption);
  }

  private void TransitionToMap(TransitionMode transitionModeParam)
  {
    using (List<ISystemMap3DView>.Enumerator enumerator = this.views.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.TransitionToMap(transitionModeParam);
    }
  }

  private void TransitionToGame()
  {
    this.TransitionToGame(this.transitionModeOption);
  }

  private void TransitionToGame(TransitionMode transitionModeParam)
  {
    using (List<ISystemMap3DView>.Enumerator enumerator = this.views.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.TransitionToGame(transitionModeParam);
    }
  }

  public void TransitionToGameFinished()
  {
    using (List<ISystemMap3DView>.Enumerator enumerator = this.views.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.OnTransitionToGameFinished();
    }
  }

  public void PropagateSetting(UserSetting setting, object data)
  {
    using (List<ISystemMap3DView>.Enumerator enumerator = this.views.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.ApplySetting(setting, data);
    }
    switch (setting)
    {
      case UserSetting.ShowStarFog:
        this.isStarFogActivated = (bool) data;
        break;
      case UserSetting.SystemMap3DTransitionMode:
        this.transitionModeOption = (TransitionMode) data;
        break;
    }
  }

  public void ReceiveSettings(UserSettings userSettings)
  {
    using (List<UserSetting>.Enumerator enumerator = userSettings.DefinedSettings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        this.ReceiveSetting(current, userSettings.Get(current));
      }
    }
  }

  public void ReceiveSetting(UserSetting userSetting, object data)
  {
    this.PropagateSetting(userSetting, data);
  }
}
