﻿// Decompiled with JetBrains decompiler
// Type: GUIHorizontalLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GUIHorizontalLayout : GUIPanel
{
  private float gap;

  public float Gap
  {
    get
    {
      return this.gap;
    }
    set
    {
      this.gap = value;
      this.MakeValidRect();
    }
  }

  public GUIHorizontalLayout()
    : this((SmartRect) null)
  {
    this.Name = "GUIHorizontalLayout";
  }

  public GUIHorizontalLayout(SmartRect parent)
    : base(parent)
  {
    this.Name = "GUIHorizontalLayout";
    this.IsRendered = true;
  }

  public override void AddPanel(GUIPanel panel)
  {
    base.AddPanel(panel);
    this.MakeValidRect();
  }

  public override void RemovePanel(GUIPanel panel)
  {
    base.RemovePanel(panel);
    this.MakeValidRect();
  }

  public void MakeValidRect()
  {
    float num = 0.0f;
    using (List<GUIPanel>.Enumerator enumerator = this.children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIPanel current = enumerator.Current;
        num += current.Width;
      }
    }
    float a = 0.0f;
    using (List<GUIPanel>.Enumerator enumerator = this.children.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIPanel current = enumerator.Current;
        a = Mathf.Max(a, current.Height);
      }
    }
    if (this.children.Count <= 0)
      return;
    this.root.Width = num + (float) (this.children.Count - 1) * this.Gap;
    this.root.Height = a;
    this.children[0].Parent = this.root;
    this.children[0].Position = new float2((float) -((double) this.root.Width / 2.0 - (double) this.children[0].Width / 2.0), 0.0f);
    float2 float2 = new float2(this.children[0].Width, 0.0f);
    SmartRect smartRect = this.children[0].SmartRect;
    for (int index = 1; index < this.children.Count; ++index)
    {
      GUIPanel guiPanel = this.children[index];
      guiPanel.Parent = smartRect;
      guiPanel.Position = new float2((float) ((double) float2.x / 2.0 + (double) this.gap + (double) guiPanel.Width / 2.0), 0.0f);
      smartRect = this.children[index].SmartRect;
      float2 = new float2(guiPanel.Width, 0.0f);
    }
  }
}
