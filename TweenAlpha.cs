﻿// Decompiled with JetBrains decompiler
// Type: TweenAlpha
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[AddComponentMenu("NGUI/Tween/Tween Alpha")]
public class TweenAlpha : UITweener
{
  [Range(0.0f, 1f)]
  public float from = 1f;
  [Range(0.0f, 1f)]
  public float to = 1f;
  private bool mCached;
  private UIRect mRect;
  private Material mMat;

  [Obsolete("Use 'value' instead")]
  public float alpha
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  public float value
  {
    get
    {
      if (!this.mCached)
        this.Cache();
      if ((UnityEngine.Object) this.mRect != (UnityEngine.Object) null)
        return this.mRect.alpha;
      return this.mMat.color.a;
    }
    set
    {
      if (!this.mCached)
        this.Cache();
      if ((UnityEngine.Object) this.mRect != (UnityEngine.Object) null)
      {
        this.mRect.alpha = value;
      }
      else
      {
        Color color = this.mMat.color;
        color.a = value;
        this.mMat.color = color;
      }
    }
  }

  private void Cache()
  {
    this.mCached = true;
    this.mRect = this.GetComponent<UIRect>();
    if (!((UnityEngine.Object) this.mRect == (UnityEngine.Object) null))
      return;
    Renderer component = this.GetComponent<Renderer>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      this.mMat = component.material;
    if (!((UnityEngine.Object) this.mMat == (UnityEngine.Object) null))
      return;
    this.mRect = this.GetComponentInChildren<UIRect>();
  }

  protected override void OnUpdate(float factor, bool isFinished)
  {
    this.value = Mathf.Lerp(this.from, this.to, factor);
  }

  public static TweenAlpha Begin(GameObject go, float duration, float alpha)
  {
    TweenAlpha tweenAlpha = UITweener.Begin<TweenAlpha>(go, duration);
    tweenAlpha.from = tweenAlpha.value;
    tweenAlpha.to = alpha;
    if ((double) duration <= 0.0)
    {
      tweenAlpha.Sample(1f, true);
      tweenAlpha.enabled = false;
    }
    return tweenAlpha;
  }

  public override void SetStartToCurrentValue()
  {
    this.from = this.value;
  }

  public override void SetEndToCurrentValue()
  {
    this.to = this.value;
  }
}
