﻿// Decompiled with JetBrains decompiler
// Type: StaticNotificationView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StaticNotificationView : OnScreenNotificationView
{
  private readonly Queue<OnScreenNotification> queue = new Queue<OnScreenNotification>();
  private readonly Queue<OnScreenNotification> specialActionQueue = new Queue<OnScreenNotification>();
  private readonly List<Text> currentDisplayedMessages = new List<Text>(4);
  private const int MAX_MESSAGES = 4;
  private const float FADE_DURATION = 4f;
  private const float FADE_DURATION_SPECIAL = 1.5f;
  private const float OFFSET = 20f;
  private const float MESSAGE_COOLDOWN = 0.5f;
  private const float SPECIAL_ACTION_COOLDOWN = 2.75f;
  public Text specialDisplayLabel;
  public UiSpriteAnimation flash;
  private int pushTweensRunning;
  private int nextMessageIndex;
  private float lastMessageTimestamp;
  private float lastSpecialActionTimestamp;

  public override void ShowOnScreenNotification(OnScreenNotification notification)
  {
    if (!notification.Show)
      return;
    if (this.queue.Count > 100)
      Debug.LogWarning((object) ("Warning: Too many OnScreenNotifications in queue, discarding: " + (object) notification.GetType()));
    else if (notification is SpecialActionNotification)
      this.specialActionQueue.Enqueue(notification);
    else
      this.queue.Enqueue(notification);
  }

  private void Update()
  {
    if (this.specialActionQueue.Count > 0 && (double) Time.time - (double) this.lastSpecialActionTimestamp > 2.75)
    {
      OnScreenNotification notification = this.specialActionQueue.Dequeue();
      this.ShowSpecialMessage(notification.TextMessage, this.GetMessageColor(notification));
      this.lastSpecialActionTimestamp = Time.time;
    }
    if (this.pushTweensRunning > 0 || this.CheckForExpiredMessages() || (this.queue.Count == 0 || this.nextMessageIndex >= 4) || (double) Time.time - (double) this.lastMessageTimestamp < 0.5)
      return;
    OnScreenNotification notification1 = this.queue.Dequeue();
    if (string.IsNullOrEmpty(notification1.TextMessage))
    {
      Debug.LogWarning((object) ("WARNING: OnScreenNotification " + (object) notification1.GetType() + " has no valid text"));
    }
    else
    {
      this.ShowMessage(notification1.TextMessage, this.GetMessageColor(notification1));
      this.lastMessageTimestamp = Time.time;
    }
  }

  private Color GetMessageColor(OnScreenNotification notification)
  {
    Color white = Color.white;
    switch (notification.Category)
    {
      case NotificationCategory.Positive:
        white = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_POSITIVE);
        break;
      case NotificationCategory.Negative:
        white = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_NEGATIVE);
        break;
      case NotificationCategory.Special:
        white = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_SPECIAL);
        break;
    }
    return white;
  }

  private void ShowMessage(string message, Color color)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    StaticNotificationView.\u003CShowMessage\u003Ec__AnonStoreyD1 messageCAnonStoreyD1 = new StaticNotificationView.\u003CShowMessage\u003Ec__AnonStoreyD1();
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD1.text = this.CreateOnScreenText();
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD1.text.gameObject.SetActive(true);
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD1.text.text = message.ToUpperInvariant();
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD1.text.color = color;
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD1.text.rectTransform.localPosition = new Vector3(0.0f, -20f * (float) this.nextMessageIndex, 0.0f);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    LeanTween.textAlpha(messageCAnonStoreyD1.text.rectTransform, 0.25f, 4f).onComplete = new System.Action(messageCAnonStoreyD1.\u003C\u003Em__1F4);
    // ISSUE: reference to a compiler-generated field
    this.currentDisplayedMessages.Add(messageCAnonStoreyD1.text);
    ++this.nextMessageIndex;
  }

  private void ShowSpecialMessage(string message, Color color)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    StaticNotificationView.\u003CShowSpecialMessage\u003Ec__AnonStoreyD2 messageCAnonStoreyD2 = new StaticNotificationView.\u003CShowSpecialMessage\u003Ec__AnonStoreyD2();
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD2.text = this.specialDisplayLabel;
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD2.text.gameObject.SetActive(true);
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD2.text.text = message.ToUpperInvariant();
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD2.text.color = color;
    // ISSUE: reference to a compiler-generated field
    LTDescr ltDescr = LeanTween.textAlpha(messageCAnonStoreyD2.text.rectTransform, 0.25f, 1.5f);
    ltDescr.setDelay(0.5f);
    // ISSUE: reference to a compiler-generated method
    ltDescr.onComplete = new System.Action(messageCAnonStoreyD2.\u003C\u003Em__1F5);
    this.flash.Play();
  }

  private bool CheckForExpiredMessages()
  {
    Text label = this.currentDisplayedMessages.FirstOrDefault<Text>((Func<Text, bool>) (text => !text.gameObject.activeSelf));
    if ((UnityEngine.Object) label == (UnityEngine.Object) null)
      return false;
    this.OnMessageFaded(label);
    return true;
  }

  private void OnMessageFaded(Text label)
  {
    this.currentDisplayedMessages.Remove(label);
    UnityEngine.Object.Destroy((UnityEngine.Object) label.gameObject);
    using (List<Text>.Enumerator enumerator = this.currentDisplayedMessages.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Text current = enumerator.Current;
        ++this.pushTweensRunning;
        float to = current.rectTransform.localPosition.y + 20f;
        LeanTween.moveLocalY(current.gameObject, to, 0.4f).setOnComplete((System.Action) (() => --this.pushTweensRunning));
      }
    }
    --this.nextMessageIndex;
  }

  private Text CreateOnScreenText()
  {
    GameObject gameObject = new GameObject("onscreen_notification");
    Text text = gameObject.AddComponent<Text>();
    text.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 900f);
    text.alignment = TextAnchor.LowerCenter;
    text.font = Gui.Options.fontEurostileTRegCon;
    text.fontSize = 20;
    gameObject.transform.SetParent(this.transform, false);
    return text;
  }
}
