﻿// Decompiled with JetBrains decompiler
// Type: ShopWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using Gui.Tooltips;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ShopWindow : GUIPanel
{
  private readonly Dictionary<ShopInventoryContainer, GUIButtonNew> tabs = new Dictionary<ShopInventoryContainer, GUIButtonNew>();
  private ShopInventoryContainer selectedTab;
  private readonly GUIShopTypeFilter typeFilter;
  private readonly GUIShopConfirmWindow confirmWindow;
  private readonly GUIShopUpgradeWindow upgradeWindow;
  private readonly GUIShopFilledHoldWindow filledholdWindow;
  private readonly GUIShopItemIsReplaceableOnlyWindow itemIsReplaceableOnlyWindow;
  private readonly GUIShopPaperDoll paperDoll;
  private readonly GUIShopPaperDollStarterKit paperDollStarterKit;
  private readonly GUIShopInventoryList inventoryList;
  private readonly GUIShopDragAndDrop dragAndDrop;
  private readonly GUIShopInfoWindow infoWindow;
  private readonly GUIResourcePanel resourcePanel;
  private readonly GUICheckBox equipableOnlyCheckbox;
  private readonly GUIButtonNew getCubitsButton;
  private readonly GUIButtonNew sellJunkButton;
  private GUIShopSellJunkWindow sellJunkConfirmWindow;
  private GuiFadingImage specialOfferCubits;
  private GuiFadingImagePanel specialOfferCubitsPanel;
  private readonly GUIScrollBox scrollBox;
  private Shop shopModel;
  public ShopWindow.Handlr HandlerClose;

  public override bool IsRendered
  {
    get
    {
      return base.IsRendered;
    }
    set
    {
      if (value && this.shopModel == null)
        this.shopModel = RoomLevel.GetLevel().Shop ?? SpaceLevel.GetLevel().Shop;
      else if (!value && this.shopModel != null)
        this.shopModel = (Shop) null;
      if (this.specialOfferCubitsPanel != null)
        this.specialOfferCubitsPanel.IsRendered = value;
      this.IsUpdated = value;
      base.IsRendered = value;
      if (this.selectedTab != ShopInventoryContainer.Store || this.equipableOnlyCheckbox.IsPressed)
        return;
      this.equipableOnlyCheckbox.Toggle();
    }
  }

  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  private bool ShowOnlyEquipableItems
  {
    get
    {
      return this.equipableOnlyCheckbox.IsPressed;
    }
  }

  public GUIShopConfirmWindow ConfirmWindow
  {
    get
    {
      return this.confirmWindow;
    }
  }

  public ShopWindow()
    : base((SmartRect) null)
  {
    this.IsUpdated = false;
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/EquipBuyPanel/gui_main_layout");
    GUIImageNew guiImageNew = (jwindowDescription["background_image"] as JImage).CreateGUIImageNew(this.root);
    this.root.Width = guiImageNew.Rect.width;
    this.root.Height = guiImageNew.Rect.height;
    this.AddPanel((GUIPanel) guiImageNew);
    this.AddPanel((GUIPanel) (jwindowDescription["tip_label"] as JLabel).CreateGUILabelNew(this.root));
    this.inventoryList = new GUIShopInventoryList(this.root);
    this.inventoryList.Position = (jwindowDescription["inventory_pivot"] as JPivot).position;
    this.inventoryList.HandlerUpperButton = new AnonymousDelegate(this.OnAugmentButton);
    this.inventoryList.HandlerButton = new AnonymousDelegate(this.OnInventoryButton);
    this.inventoryList.HandlerDetailsButton = new AnonymousDelegate(this.OnInventoryDetailsButton);
    this.AddPanel((GUIPanel) this.inventoryList);
    this.paperDoll = new GUIShopPaperDoll(this.root);
    this.paperDoll.Position = (jwindowDescription["papperdoll_pivot"] as JPivot).position;
    this.paperDoll.HandlerUpgradeButton = new GUIShopPaperDollSlot.dHandler(this.OnPapperDollUpgradeButton);
    this.AddPanel((GUIPanel) this.paperDoll);
    this.paperDollStarterKit = new GUIShopPaperDollStarterKit(this.root);
    this.paperDollStarterKit.Position = (jwindowDescription["papperdoll_pivot"] as JPivot).position;
    this.AddPanel((GUIPanel) this.paperDollStarterKit);
    this.paperDollStarterKit.IsRendered = false;
    this.paperDollStarterKit.HandlerButton = new AnonymousDelegate(this.OnInventoryButton);
    this.tabs[ShopInventoryContainer.Store] = (jwindowDescription["tab_store_button"] as JButton).CreateGUIButtonNew(this.root);
    this.tabs[ShopInventoryContainer.Store].Handler = new AnonymousDelegate(this.OnTabStoreButton);
    this.tabs[ShopInventoryContainer.Store].SetTooltip("%$bgo.shop.store_tooltip%");
    this.AddPanel((GUIPanel) this.tabs[ShopInventoryContainer.Store]);
    this.tabs[ShopInventoryContainer.Hold] = (jwindowDescription["tab_hold_button"] as JButton).CreateGUIButtonNew(this.root);
    this.tabs[ShopInventoryContainer.Hold].Handler = new AnonymousDelegate(this.OnTabHoldButton);
    this.tabs[ShopInventoryContainer.Hold].SetTooltip("%$bgo.shop.hold_tooltip%");
    this.AddPanel((GUIPanel) this.tabs[ShopInventoryContainer.Hold]);
    this.tabs[ShopInventoryContainer.Locker] = (jwindowDescription["tab_locker_button"] as JButton).CreateGUIButtonNew(this.root);
    this.tabs[ShopInventoryContainer.Locker].Handler = new AnonymousDelegate(this.OnTabLockerButton);
    this.tabs[ShopInventoryContainer.Locker].SetTooltip("%$bgo.shop.locker_tooltip%");
    this.AddPanel((GUIPanel) this.tabs[ShopInventoryContainer.Locker]);
    this.getCubitsButton = (jwindowDescription["getcubits_button"] as JButton).CreateGUIButtonNew(this.root);
    this.getCubitsButton.Handler = new AnonymousDelegate(ShopWindow.OnGetCubits);
    this.getCubitsButton.SetTooltip("%$bgo.common.get_cubits_tooltip%");
    this.AddPanel((GUIPanel) this.getCubitsButton);
    Game.SpecialOfferManager.OnHappyHourChanged += new SpecialOfferManager.HappyHourDelegate(this.OnHappyHourChanged);
    this.OnHappyHourChanged(Game.SpecialOfferManager.IsHappyHourActive());
    GUIButtonNew guiButtonNew = (jwindowDescription["close_button"] as JButton).CreateGUIButtonNew(this.root);
    guiButtonNew.PressSound = (AudioClip) null;
    guiButtonNew.Handler = new AnonymousDelegate(this.OnCloseButton);
    this.AddPanel((GUIPanel) guiButtonNew);
    this.scrollBox = new GUIScrollBox(437f, this.root);
    this.scrollBox.Position = (jwindowDescription["scrollbox_pivot"] as JPivot).position;
    this.scrollBox.MaxRange = 0;
    this.scrollBox.ViewRange = 5;
    this.scrollBox.Handler = new GUIScrollBox.Handlr(this.OnUpdateScroll);
    this.AddPanel((GUIPanel) this.scrollBox);
    JButton button = new JButton();
    button.normalTexture = "GUI/Common/checkbox";
    button.overTexture = "GUI/Common/checkbox_over";
    button.pressedTexture = "GUI/Common/checkbox_x";
    button.size = new float2(18f, 18f);
    button.alignment = Align.MiddleLeft;
    this.equipableOnlyCheckbox = new GUICheckBox(button);
    this.equipableOnlyCheckbox.Position = (jwindowDescription["equipable_checkbox_pivot"] as JPivot).position;
    this.equipableOnlyCheckbox.Text = "%$bgo.shop.show_only_equipable_items%";
    this.equipableOnlyCheckbox.TextLabel.Alignment = TextAnchor.MiddleLeft;
    this.equipableOnlyCheckbox.TextLabel.Position = new float2(14f, 0.0f);
    this.equipableOnlyCheckbox.TextLabel.Font = Gui.Options.FontBGM_BT;
    this.equipableOnlyCheckbox.TextLabel.FontSize = 14;
    this.equipableOnlyCheckbox.Handler = (AnonymousDelegate) (() => this.OnEquipableOnlyCheckboxClick(this.equipableOnlyCheckbox.IsPressed));
    this.AddPanel((GUIPanel) this.equipableOnlyCheckbox);
    this.sellJunkButton = new GUIButtonNew("%$bgo.shop.sell_junk%");
    this.sellJunkButton.TextLabel.WordWrap = true;
    this.sellJunkButton.TextLabel.Font = Gui.Options.FontBGM_BT;
    this.sellJunkButton.TextLabel.FontSize = 14;
    this.sellJunkButton.Position = this.equipableOnlyCheckbox.Position;
    this.sellJunkButton.PositionX -= 150f;
    this.sellJunkButton.Width = 170f;
    this.sellJunkButton.Height = 32f;
    this.sellJunkButton.TextLabel.Alignment = TextAnchor.MiddleCenter;
    this.sellJunkButton.Handler = (AnonymousDelegate) (() =>
    {
      if (this.sellJunkConfirmWindow == null)
      {
        this.sellJunkConfirmWindow = new GUIShopSellJunkWindow(this.root);
        this.AddPanel((GUIPanel) this.sellJunkConfirmWindow);
      }
      this.sellJunkConfirmWindow.Show();
      FacadeFactory.GetInstance().SendMessage(Message.ShopSellJunkConfirm, (object) this.sellJunkConfirmWindow);
    });
    this.AddPanel((GUIPanel) this.sellJunkButton);
    this.typeFilter = new GUIShopTypeFilter(this.root);
    this.typeFilter.Position = (jwindowDescription["typefilter_pivot"] as JPivot).position;
    this.typeFilter.Handler = new GUIShopTypeFilter.dHandler(this.OnSelectTypeFilter);
    this.typeFilter.Size = new float2(48f, 530f);
    this.AddPanel((GUIPanel) this.typeFilter);
    this.resourcePanel = new GUIResourcePanel(this.root);
    this.resourcePanel.Position = (jwindowDescription["resource_pivot"] as JPivot).position;
    this.AddPanel((GUIPanel) this.resourcePanel);
    this.dragAndDrop = new GUIShopDragAndDrop(this.root);
    this.dragAndDrop.HandlerGetItem = new GUIShopDragAndDrop.dHandlrGetItem(this.OnDADGetItem);
    this.dragAndDrop.HandlerDropItem = new GUIShopDragAndDrop.dHandlrDropItem(this.OnDADDropItem);
    this.AddPanel((GUIPanel) this.dragAndDrop);
    this.confirmWindow = new GUIShopConfirmWindow(this.root);
    this.AddPanel((GUIPanel) this.confirmWindow);
    this.upgradeWindow = new GUIShopUpgradeWindow(this.root);
    this.upgradeWindow.HandlerYes = new AnonymousDelegate(this.OnUpgradeYesButton);
    this.AddPanel((GUIPanel) this.upgradeWindow);
    this.filledholdWindow = new GUIShopFilledHoldWindow(this.root);
    this.AddPanel((GUIPanel) this.filledholdWindow);
    this.itemIsReplaceableOnlyWindow = new GUIShopItemIsReplaceableOnlyWindow(this.root);
    this.AddPanel((GUIPanel) this.itemIsReplaceableOnlyWindow);
    this.infoWindow = new GUIShopInfoWindow(this.root);
    this.AddPanel((GUIPanel) this.infoWindow);
    this.RecalculateAbsCoords();
    this.SelectTab(ShopInventoryContainer.Store);
  }

  private void OnEquipableOnlyCheckboxClick(bool isEnabled)
  {
  }

  private void OnHappyHourChanged(bool activeFlag)
  {
    if (activeFlag && !Game.GUIManager.panels.Contains((IGUIPanel) this.specialOfferCubitsPanel))
    {
      this.RecalculateAbsCoords();
      this.getCubitsButton.RecalculateAbsCoords();
      Vector2 v2 = float2.ToV2(this.getCubitsButton.Size);
      this.specialOfferCubitsPanel = new GuiFadingImagePanel(v2, Vector2.zero);
      this.specialOfferCubits = new GuiFadingImage(ResourceLoader.Load<Texture2D>("GUI/Common/Buttons/getcubits_glow"), this.getCubitsButton.AdvancedTooltip, this.getCubitsButton.Handler, 5f, (IDrawable) this.getCubitsButton.TextLabel);
      this.specialOfferCubits.Size = v2;
      this.RepositionCubitGlow();
      this.specialOfferCubitsPanel.AddChild((GuiElementBase) this.specialOfferCubits);
      this.specialOfferCubitsPanel.IsRendered = this.IsRendered;
    }
    else
    {
      if (activeFlag || !Game.GUIManager.panels.Contains((IGUIPanel) this.specialOfferCubitsPanel))
        return;
      this.specialOfferCubitsPanel.Close();
    }
  }

  private void RepositionCubitGlow()
  {
    Vector2 v2 = float2.ToV2(this.getCubitsButton.Size);
    this.specialOfferCubitsPanel.Position = float2.ToV2(this.getCubitsButton.SmartRect.AbsPosition) - new Vector2(v2.x / 2f, v2.y / 2f);
  }

  protected override void OnShow()
  {
    base.OnShow();
    ShopProtocol.GetProtocol().RequestAllSales();
    FeedbackProtocol.GetProtocol().ReportUiElementShown(FeedbackProtocol.UiElementId.ShopWindow);
  }

  protected override void OnHide()
  {
    base.OnHide();
    FeedbackProtocol.GetProtocol().ReportUiElementHidden(FeedbackProtocol.UiElementId.ShopWindow);
  }

  public override void RecalculateAbsCoords()
  {
    this.root.Position = new float2((float) (Screen.width / 2), (float) (Screen.height / 2));
    if (this.specialOfferCubitsPanel != null)
      this.RepositionCubitGlow();
    base.RecalculateAbsCoords();
  }

  public override void Update()
  {
    ItemList inventoryItemList = this.GetInventoryItemList(this.typeFilter.SelectedFilter);
    this.inventoryList.UpdateData(inventoryItemList, this.selectedTab);
    this.tabs[ShopInventoryContainer.Hold].Text = BsgoLocalization.Get("bgo.EquipBuyPanel.gui_main_layout.label_0") + (" (" + (object) Game.Me.Hold.Count + "/" + (object) 70 + ")");
    this.scrollBox.MaxRange = inventoryItemList.Count;
    this.sellJunkButton.HandleMouseInput = Game.Me.Hold.ContainsJunk();
    StarterKit starterKit = this.inventoryList.StarterKitSelected(inventoryItemList);
    if (starterKit != null)
    {
      this.paperDoll.IsRendered = false;
      this.paperDollStarterKit.IsRendered = true;
      this.paperDollStarterKit.UpdateData(Game.Me.ActiveShip.Card.PaperdollLayoutBig, starterKit);
    }
    else
    {
      this.paperDoll.IsRendered = true;
      this.paperDollStarterKit.IsRendered = false;
      if (Game.Me.ActiveShip.Card != null)
      {
        if (this.dragAndDrop.DraggedItem != null)
          this.paperDoll.UpdateData(Game.Me.ActiveShip.Card, Game.Me.ActiveShip.Slots, this.dragAndDrop.DraggedItem);
        else
          this.paperDoll.UpdateData(Game.Me.ActiveShip.Card, Game.Me.ActiveShip.Slots, this.GetSelectedItemFromInventory<ShipItem>());
      }
    }
    base.Update();
    this.resourcePanel.PlaceLeftOf((GUIPanel) this.getCubitsButton, 10f);
    this.RecalculateAbsCoords();
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    bool flag = false;
    if (!this.confirmWindow.IsRendered)
    {
      if (this.paperDollStarterKit.IsRendered)
      {
        ShipItem itemFromStarterKit = this.GetItemFromStarterKit(mousePosition);
        if (itemFromStarterKit != null)
        {
          this.SetStoreToolTip(itemFromStarterKit, this.selectedTab, false, false);
          this.infoWindow.Hide();
          flag = true;
        }
      }
      else
      {
        ShipItem itemFromInventory = this.GetItemFromInventory<ShipItem>(mousePosition);
        ShipItem shipItem = (ShipItem) this.GetItemFromPapperDoll(mousePosition);
        if (itemFromInventory != null)
        {
          this.SetStoreToolTip(itemFromInventory, this.selectedTab, false, false);
          this.infoWindow.Hide();
          flag = true;
        }
        else if (shipItem != null)
        {
          this.SetStoreToolTip(shipItem, this.selectedTab, true, false);
          this.infoWindow.Hide();
          flag = true;
        }
      }
    }
    if (flag)
    {
      Rect? slotRect = this.inventoryList.GetSlotRect(mousePosition);
      if (slotRect.HasValue)
      {
        this.AdvancedTooltip.Parent = new SRect(slotRect.Value);
        this.AdvancedTooltip.UseMousePosition = false;
      }
    }
    else
      this.advancedTooltip = (GuiAdvancedTooltipBase) null;
    base.OnMouseMove(mousePosition);
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse1)
    {
      ShipItem itemFromInventory = this.GetItemFromInventory<ShipItem>(mousePosition);
      if (itemFromInventory != null && (bool) itemFromInventory.IsLoaded)
      {
        if (this.selectedTab == ShopInventoryContainer.Store)
          GuiUpgradeWindow.ShowWindow(itemFromInventory, false);
        else
          GuiUpgradeWindow.ShowWindow(itemFromInventory, true);
      }
      ShipSlot slotFromPapperDoll = this.GetSlotFromPapperDoll(mousePosition);
      if (slotFromPapperDoll != null && slotFromPapperDoll.System != null && (bool) slotFromPapperDoll.System.IsLoaded)
        GuiUpgradeWindow.ShowWindow(slotFromPapperDoll);
    }
    this.infoWindow.Hide();
    return base.OnMouseDown(mousePosition, mouseKey);
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    this.infoWindow.Hide();
    return base.OnMouseUp(mousePosition, mouseKey);
  }

  public void SelectCategory(ShopCategory category, ShopItemType itemType)
  {
    this.typeFilter.SelectCategory(category, itemType);
    this.OnSelectTypeFilter();
  }

  private void OnDADGetItem(float2 fromPos, out ShipItem item, out GUIPanel fromPanel)
  {
    if (this.inventoryList.Contains(fromPos))
    {
      ShipItem itemFromInventory = this.GetItemFromInventory<ShipItem>(fromPos);
      if (itemFromInventory != null)
      {
        item = itemFromInventory;
        fromPanel = (GUIPanel) this.inventoryList;
        return;
      }
    }
    else if (this.paperDoll.IsRendered && this.paperDoll.Contains(fromPos))
    {
      ShipItem shipItem = (ShipItem) this.GetItemFromPapperDoll(fromPos);
      if (shipItem != null)
      {
        item = shipItem;
        fromPanel = (GUIPanel) this.paperDoll;
        return;
      }
    }
    item = (ShipItem) null;
    fromPanel = (GUIPanel) null;
  }

  private bool ReachAbilityLimit(ShipSystem system)
  {
    if (system.Card.SlotType == ShipSlotType.weapon || system.Card.AbilityCards.Length <= 0 || Game.Me.ActiveShip.AbilityToolbar.Count != 10)
      return false;
    new InfoBox("%$bgo.shop.activation_slots_full%").Show();
    return true;
  }

  private void OnDADDropItem(float2 fromPos, float2 toPos, GUIPanel fromPanel)
  {
    ShipSystem system1 = this.GetItemFromInventory<ShipSystem>(fromPos) ?? this.GetItemFromPapperDoll(fromPos);
    ShipSystem shipSystem = this.GetItemFromInventory<ShipSystem>(toPos) ?? this.GetItemFromPapperDoll(toPos);
    ShipSlot slotFromPapperDoll1 = this.GetSlotFromPapperDoll(toPos);
    if (system1 != null && system1.Card.ReplaceableOnly || shipSystem != null && shipSystem.Card.ReplaceableOnly)
    {
      if (shipSystem == system1)
        return;
      bool flag = this.paperDoll.Contains(fromPos);
      if (system1 != null && !flag && this.tabs[ShopInventoryContainer.Locker].Contains(toPos))
        system1.MoveTo((IContainer) Game.Me.Locker, 0U);
      else if (system1 != null && !flag && this.tabs[ShopInventoryContainer.Hold].Contains(toPos))
        system1.MoveTo((IContainer) Game.Me.Hold, 0U);
      else if (this.CheckSystemForSlot(system1, slotFromPapperDoll1, true) && !this.ReachAbilityLimit(system1))
      {
        GUISound.Instance.OnEquip();
        this.confirmWindow.ShowInstall((ShipItem) system1, slotFromPapperDoll1, new AnonymousDelegate(this.OnConfirmMoveToPapperDoll));
      }
      else if (this.tabs[ShopInventoryContainer.Locker].Contains(toPos))
      {
        system1.MoveTo((IContainer) Game.Me.Locker, 0U);
      }
      else
      {
        GUISound.Instance.OnIllegalAction();
        this.itemIsReplaceableOnlyWindow.Show();
      }
    }
    else if (this.paperDoll.IsRendered && this.paperDoll.Contains(toPos))
    {
      if (fromPanel == this.inventoryList && this.selectedTab == ShopInventoryContainer.Hold)
      {
        ShipSystem itemFromInventory = this.GetItemFromInventory<ShipSystem>(fromPos);
        ShipSlot slotFromPapperDoll2 = this.GetSlotFromPapperDoll(toPos);
        if (this.CheckSystemForSlot(itemFromInventory, slotFromPapperDoll2, true) && !this.ReachAbilityLimit(itemFromInventory))
        {
          GUISound.Instance.OnEquip();
          this.confirmWindow.ShowInstall((ShipItem) itemFromInventory, slotFromPapperDoll2, new AnonymousDelegate(this.OnConfirmMoveToPapperDoll));
        }
        else
          GUISound.Instance.OnIllegalAction();
      }
      else if (fromPanel == this.inventoryList && this.selectedTab == ShopInventoryContainer.Store)
      {
        ShipSystem itemFromInventory = this.GetItemFromInventory<ShipSystem>(fromPos);
        ShipSlot slotFromPapperDoll2 = this.GetSlotFromPapperDoll(toPos);
        if (this.CheckSystemForSlot(itemFromInventory, slotFromPapperDoll2, true) && !this.ReachAbilityLimit(itemFromInventory))
        {
          if (this.CanBuyThisItem((ShipItem) itemFromInventory))
          {
            GUISound.Instance.OnEquip();
            this.confirmWindow.ShowBuyInstall((ShipItem) itemFromInventory, slotFromPapperDoll2, new AnonymousDelegate(this.OnConfirmMoveToPapperDoll), new AnonymousDelegate(this.OnConfirmMoveToHold));
          }
          else
          {
            GUISound.Instance.OnIllegalAction();
            FacadeFactory.GetInstance().SendMessage(Message.ShopResourcesMissing, (object) itemFromInventory.ShopItemCard.BuyPrice.GetMissingResources(1));
          }
        }
        else
          GUISound.Instance.OnIllegalAction();
      }
      else if (fromPanel == this.inventoryList && this.selectedTab == ShopInventoryContainer.Locker)
      {
        ShipSystem itemFromInventory = this.GetItemFromInventory<ShipSystem>(fromPos);
        ShipSlot slotFromPapperDoll2 = this.GetSlotFromPapperDoll(toPos);
        if (this.CheckSystemForSlot(itemFromInventory, slotFromPapperDoll2, true) && !this.ReachAbilityLimit(itemFromInventory))
        {
          GUISound.Instance.OnEquip();
          this.confirmWindow.ShowInstall((ShipItem) itemFromInventory, slotFromPapperDoll2, new AnonymousDelegate(this.OnConfirmMoveToPapperDoll));
        }
        else
          GUISound.Instance.OnIllegalAction();
      }
      else
      {
        if (fromPanel != this.paperDoll)
          return;
        ShipSlot slotFromPapperDoll2 = this.GetSlotFromPapperDoll(fromPos);
        ShipSlot slotFromPapperDoll3 = this.GetSlotFromPapperDoll(toPos);
        if (slotFromPapperDoll2 == slotFromPapperDoll3)
          return;
        ShipSystem system2 = slotFromPapperDoll2.System;
        if (system2 != null && !system2.IsBroken && (slotFromPapperDoll3 != null && !slotFromPapperDoll3.Inoperable) && this.CheckSystemForSlot(system2, slotFromPapperDoll3, false))
        {
          GUISound.Instance.OnEquip();
          this.confirmWindow.ShowSlotToSlot((ShipItem) system2, slotFromPapperDoll3, new AnonymousDelegate(this.OnConfirmMoveToPapperDoll));
        }
        else
          GUISound.Instance.OnIllegalAction();
      }
    }
    else if (this.inventoryList.Contains(toPos))
    {
      ShipItem shipItem = (ShipItem) this.GetItemFromPapperDoll(fromPos);
      if (fromPanel == this.paperDoll && this.selectedTab == ShopInventoryContainer.Hold)
      {
        if (shipItem != null && !this.IsFilledHold(shipItem))
        {
          this.confirmWindow.ShowUninstall(shipItem, new AnonymousDelegate(this.OnConfirmMoveToHold));
        }
        else
        {
          GUISound.Instance.OnIllegalAction();
          this.filledholdWindow.Show();
        }
      }
      else if (fromPanel == this.paperDoll && this.selectedTab == ShopInventoryContainer.Store)
      {
        if (shipItem != null && shipItem.ShopItemCard.CanBeSold && (int) shipItem.ItemGUICard.Level < 8)
          this.confirmWindow.ShowSellUninstall(shipItem, new AnonymousDelegate(this.OnConfirmMoveToStore));
        else
          GUISound.Instance.OnIllegalAction();
      }
      else
      {
        if (fromPanel != this.paperDoll || this.selectedTab != ShopInventoryContainer.Locker || shipItem == null)
          return;
        this.confirmWindow.ShowUninstall(shipItem, new AnonymousDelegate(this.OnConfirmMoveToLocker));
      }
    }
    else if (this.tabs[ShopInventoryContainer.Store].Contains(toPos))
    {
      if (fromPanel == this.inventoryList && this.selectedTab == ShopInventoryContainer.Hold)
      {
        ShipItem itemFromInventory = this.GetItemFromInventory<ShipItem>(fromPos);
        if (itemFromInventory != null)
        {
          if (itemFromInventory.ShopItemCard.CanBeSold && (int) itemFromInventory.ItemGUICard.Level < 8)
            this.confirmWindow.ShowSell(itemFromInventory, new AnonymousDelegate(this.OnConfirmMoveToStore));
          else
            GUISound.Instance.OnIllegalAction();
        }
        else
          GUISound.Instance.OnIllegalAction();
      }
      else if (fromPanel == this.paperDoll)
      {
        ShipItem shipItem = (ShipItem) this.GetItemFromPapperDoll(fromPos);
        if (shipItem == null || !shipItem.ShopItemCard.CanBeSold || (int) shipItem.ItemGUICard.Level >= 8)
          return;
        this.confirmWindow.ShowSellUninstall(shipItem, new AnonymousDelegate(this.OnConfirmMoveToStore));
        GUISound.Instance.OnIllegalAction();
      }
      else
      {
        if (fromPanel != this.inventoryList || this.selectedTab != ShopInventoryContainer.Locker)
          return;
        ShipItem itemFromInventory = this.GetItemFromInventory<ShipItem>(fromPos);
        if (itemFromInventory != null)
        {
          if (itemFromInventory.ShopItemCard.CanBeSold && (int) itemFromInventory.ItemGUICard.Level < 8)
            this.confirmWindow.ShowSell(itemFromInventory, new AnonymousDelegate(this.OnConfirmMoveToStore));
          else
            GUISound.Instance.OnIllegalAction();
        }
        else
          GUISound.Instance.OnIllegalAction();
      }
    }
    else if (this.tabs[ShopInventoryContainer.Hold].Contains(toPos))
    {
      if (fromPanel == this.inventoryList && this.selectedTab == ShopInventoryContainer.Store)
      {
        ShipItem itemFromInventory = this.GetItemFromInventory<ShipItem>(fromPos);
        if (itemFromInventory == null || !Game.Me.Hold.HaveMoneyBuy(itemFromInventory, 1))
          return;
        bool flag = this.CanBuyThisItem(itemFromInventory);
        if (!this.IsFilledHold(itemFromInventory) && flag)
        {
          GUISound.Instance.OnBuy();
          this.confirmWindow.ShowBuy(itemFromInventory, new AnonymousDelegate(this.OnConfirmMoveToHold));
        }
        else
        {
          GUISound.Instance.OnIllegalAction();
          if (!flag)
            FacadeFactory.GetInstance().SendMessage(Message.ShopResourcesMissing, (object) itemFromInventory.ShopItemCard.BuyPrice.GetMissingResources(1));
          else
            this.filledholdWindow.Show();
        }
      }
      else if (fromPanel == this.paperDoll)
      {
        ShipItem shipItem = (ShipItem) this.GetItemFromPapperDoll(fromPos);
        if (shipItem != null && !this.IsFilledHold(shipItem))
        {
          this.confirmWindow.ShowUninstall(shipItem, new AnonymousDelegate(this.OnConfirmMoveToHold));
        }
        else
        {
          GUISound.Instance.OnIllegalAction();
          this.filledholdWindow.Show();
        }
      }
      else
      {
        if (fromPanel != this.inventoryList || this.selectedTab != ShopInventoryContainer.Locker)
          return;
        ShipItem itemFromInventory = this.GetItemFromInventory<ShipItem>(fromPos);
        if (itemFromInventory != null && !this.IsFilledHold(itemFromInventory))
        {
          itemFromInventory.MoveTo((IContainer) Game.Me.Hold, 0U);
        }
        else
        {
          GUISound.Instance.OnIllegalAction();
          this.filledholdWindow.Show();
        }
      }
    }
    else
    {
      if (!this.tabs[ShopInventoryContainer.Locker].Contains(toPos))
        return;
      if (fromPanel == this.inventoryList && this.selectedTab == ShopInventoryContainer.Store)
      {
        ShipItem itemFromInventory = this.GetItemFromInventory<ShipItem>(fromPos);
        if (itemFromInventory == null || !Game.Me.Hold.HaveMoneyBuy(itemFromInventory, 1))
          return;
        if (this.CanBuyThisItem(itemFromInventory))
        {
          GUISound.Instance.OnBuy();
          this.confirmWindow.ShowBuy(itemFromInventory, new AnonymousDelegate(this.OnConfirmMoveToLocker));
        }
        else
        {
          GUISound.Instance.OnIllegalAction();
          FacadeFactory.GetInstance().SendMessage(Message.ShopResourcesMissing, (object) itemFromInventory.ShopItemCard.BuyPrice.GetMissingResources(1));
        }
      }
      else if (fromPanel == this.inventoryList && this.selectedTab == ShopInventoryContainer.Hold)
      {
        ShipItem itemFromInventory = this.GetItemFromInventory<ShipItem>(fromPos);
        if (itemFromInventory == null)
          return;
        this.confirmWindow.ShowMoveToLocker(itemFromInventory, new AnonymousDelegate(this.OnConfirmMoveToLocker));
      }
      else
      {
        if (fromPanel != this.paperDoll)
          return;
        ShipItem shipItem = (ShipItem) this.GetItemFromPapperDoll(fromPos);
        if (shipItem == null)
          return;
        this.confirmWindow.ShowUninstall(shipItem, new AnonymousDelegate(this.OnConfirmMoveToLocker));
      }
    }
  }

  private void OnConfirmMoveToLocker()
  {
    this.confirmWindow.Item.MoveTo((IContainer) Game.Me.Locker, (uint) this.confirmWindow.Count);
    this.confirmWindow.Hide();
  }

  private void OnConfirmMoveToHold()
  {
    if (this.IsFilledHold(this.confirmWindow.Item))
    {
      GUISound.Instance.OnIllegalAction();
      this.filledholdWindow.Show();
    }
    this.confirmWindow.Item.MoveTo((IContainer) Game.Me.Hold, (uint) this.confirmWindow.Count);
    this.confirmWindow.Hide();
  }

  private void OnConfirmMoveToStore()
  {
    this.confirmWindow.Item.MoveTo((IContainer) this.shopModel, (uint) this.confirmWindow.Count);
    this.confirmWindow.Hide();
  }

  private void OnConfirmMoveToPapperDoll()
  {
    this.confirmWindow.Item.MoveTo((IContainer) this.confirmWindow.ToSlot, (uint) this.confirmWindow.Count);
    this.confirmWindow.Hide();
  }

  private void OnConfirmAutoEquipMoveToHold()
  {
    this.confirmWindow.Item.MoveTo((IContainer) Game.Me.Hold, (uint) this.confirmWindow.Count, true);
    this.confirmWindow.Hide();
  }

  private void OnTabStoreButton()
  {
    this.SelectTab(ShopInventoryContainer.Store);
  }

  private void OnTabHoldButton()
  {
    this.SelectTab(ShopInventoryContainer.Hold);
  }

  private void OnTabLockerButton()
  {
    this.SelectTab(ShopInventoryContainer.Locker);
  }

  private void SelectTab(ShopInventoryContainer tabMark)
  {
    using (Dictionary<ShopInventoryContainer, GUIButtonNew>.Enumerator enumerator = this.tabs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShopInventoryContainer, GUIButtonNew> current = enumerator.Current;
        current.Value.IsPressed = current.Key == tabMark;
      }
    }
    this.selectedTab = tabMark;
    this.scrollBox.Value = 0;
    this.inventoryList.ViewFirst = 0;
    if (this.selectedTab == ShopInventoryContainer.Store && !this.equipableOnlyCheckbox.IsPressed)
      this.equipableOnlyCheckbox.Toggle();
    this.typeFilter.IsInShop(this.selectedTab == ShopInventoryContainer.Store);
  }

  private void UseAugmentHandler(ItemCountable augment)
  {
    if (augment.Card.Action == AugmentActionType.SkillTime)
    {
      if (Game.Me.SkillBook.CurrentTrainingSkill == null)
        Game.GUIManager.Find<MessageBoxManager>().ShowNotifyBox("Not possible - no learning in progress");
      else
        this.UseAugmentDialog(augment);
    }
    else if (augment.Card.Action == AugmentActionType.LootItem)
      GuiDialogPopupManager.ShowDialog((GuiPanel) new GuiUIOMassActivationPopup(augment), true);
    else
      this.UseAugmentDialog(augment);
  }

  private void UseAugmentDialog(ItemCountable augment)
  {
    new AskBox("%$bgo.inflight_shop.use_augment% " + augment.ItemGUICard.Name + "?", new AnonymousDelegate(augment.Use)).Show();
  }

  private void OnUpgradeYesButton()
  {
    ShipSystem system = this.upgradeWindow.System;
    if (system != null)
      system.Upgrade(this.upgradeWindow.UpgradeToLevel);
    this.upgradeWindow.Hide();
  }

  public void OnAugmentButton()
  {
    ShipItem itemFromInventory = this.GetSelectedItemFromInventory<ShipItem>();
    if (!(itemFromInventory is ItemCountable) || !(itemFromInventory as ItemCountable).Card.IsAugment)
      return;
    this.UseAugmentHandler(itemFromInventory as ItemCountable);
  }

  private void OnInventoryButton()
  {
    ShipItem itemFromInventory = this.GetSelectedItemFromInventory<ShipItem>();
    if (itemFromInventory == null)
      return;
    if (this.selectedTab == ShopInventoryContainer.Hold)
    {
      if (itemFromInventory.ShopItemCard.CanBeSold)
      {
        GUISound.Instance.OnBuy();
        this.confirmWindow.ShowSell(itemFromInventory, new AnonymousDelegate(this.OnConfirmMoveToStore));
      }
      else
        GUISound.Instance.OnIllegalAction();
    }
    else if (this.selectedTab == ShopInventoryContainer.Locker)
    {
      GUISound.Instance.OnBuy();
      itemFromInventory.MoveTo((IContainer) Game.Me.Hold, 0U);
    }
    else
    {
      if (this.selectedTab != ShopInventoryContainer.Store)
        return;
      if (this.CanBuyThisItem(itemFromInventory))
      {
        if (itemFromInventory is StarterKit)
        {
          GUISound.Instance.OnBuy();
          this.confirmWindow.ShowBuyHoldInstall(itemFromInventory, new AnonymousDelegate(this.OnConfirmAutoEquipMoveToHold), new AnonymousDelegate(this.OnConfirmMoveToHold));
        }
        else if (itemFromInventory is ShipSystem && (itemFromInventory as ShipSystem).Card.SlotType == ShipSlotType.ship_paint)
        {
          GUISound.Instance.OnBuy();
          this.confirmWindow.ShowBuy(itemFromInventory, new AnonymousDelegate(this.OnConfirmMoveToLocker));
        }
        else if (!this.IsFilledHold(itemFromInventory))
        {
          GUISound.Instance.OnBuy();
          this.confirmWindow.ShowBuy(itemFromInventory, new AnonymousDelegate(this.OnConfirmMoveToHold));
        }
        else
        {
          GUISound.Instance.OnIllegalAction();
          this.filledholdWindow.Show();
        }
      }
      else
      {
        GUISound.Instance.OnIllegalAction();
        FacadeFactory.GetInstance().SendMessage(Message.ShopResourcesMissing, (object) itemFromInventory.ShopItemCard.BuyPrice.GetMissingResources(1));
      }
    }
  }

  private void OnInventoryDetailsButton()
  {
    ShipItem itemFromInventory = this.GetSelectedItemFromInventory<ShipItem>();
    if (itemFromInventory == null || !(bool) itemFromInventory.IsLoaded)
      return;
    if (this.selectedTab == ShopInventoryContainer.Store)
      GuiUpgradeWindow.ShowWindow(itemFromInventory, false);
    else
      GuiUpgradeWindow.ShowWindow(itemFromInventory, true);
  }

  private void OnPapperDollUpgradeButton(ShipSlot slot)
  {
    if (slot.System == null || !(bool) slot.System.IsLoaded)
      return;
    GuiUpgradeWindow.ShowWindow(slot);
  }

  private void OnSelectTypeFilter()
  {
    this.scrollBox.Value = 0;
    this.inventoryList.ViewFirst = 0;
  }

  private void OnCloseButton()
  {
    if (this.HandlerClose != null)
      this.HandlerClose();
    this.IsRendered = false;
    GUISound.Instance.OnWindowClose();
  }

  public static void OnGetCubits()
  {
    FacadeFactory.GetInstance().SendMessage(Message.SetFullscreen, (object) false);
    ExternalApi.EvalJs("window.createPaymentSession();");
  }

  private void OnUpdateScroll()
  {
    this.inventoryList.ViewFirst = this.scrollBox.Value;
  }

  private ShipItem GetItemFromStarterKit(float2 pos)
  {
    StarterKit starterKit = this.inventoryList.StarterKitSelected(this.GetInventoryItemList(this.typeFilter.SelectedFilter));
    if (starterKit == null)
      return (ShipItem) null;
    int slotId = this.paperDollStarterKit.GetSlotId(pos);
    if (slotId == -1)
      return (ShipItem) null;
    int index = slotId + this.paperDollStarterKit.ViewFirst;
    return starterKit.Card.Items[index];
  }

  private T GetItemFromInventory<T>(float2 pos) where T : ShipItem
  {
    int slotId = this.inventoryList.GetSlotId(pos);
    if (slotId == -1)
      return (T) null;
    int index = slotId + this.inventoryList.ViewFirst;
    ItemList inventoryItemList = this.GetInventoryItemList(this.typeFilter.SelectedFilter);
    if (inventoryItemList != null && index >= 0 && index < inventoryItemList.Count)
      return inventoryItemList[index] as T;
    return (T) null;
  }

  private T GetSelectedItemFromInventory<T>() where T : ShipItem
  {
    int selectedId = this.inventoryList.SelectedID;
    ItemList inventoryItemList = this.GetInventoryItemList(this.typeFilter.SelectedFilter);
    if (inventoryItemList != null && selectedId >= 0 && selectedId < inventoryItemList.Count)
      return inventoryItemList[selectedId] as T;
    return (T) null;
  }

  private ShipSystem GetItemFromPapperDoll(float2 pos)
  {
    ShipSlot slotFromPapperDoll = this.GetSlotFromPapperDoll(pos);
    if (slotFromPapperDoll == null)
      return (ShipSystem) null;
    return slotFromPapperDoll.System;
  }

  private ShipSlot GetSlotFromPapperDoll(float2 pos)
  {
    int slotId = this.paperDoll.GetSlotId(pos);
    if (slotId >= 0 && slotId < Game.Me.ActiveShip.Slots.Count)
      return Game.Me.ActiveShip.Slots[slotId];
    return (ShipSlot) null;
  }

  private ItemList FilterEquipable(ItemList itemList)
  {
    ShipCard shipCard = Game.Me.ActiveShip.Card;
    itemList = itemList.FilterStarterKits();
    itemList = itemList.FilterByTier(shipCard.Tier);
    itemList = itemList.FilterSystemsByShipRestrictions(shipCard.ShipObjectKey, shipCard.ShipRoles);
    return itemList;
  }

  private ItemList GetInventoryItemList(Predicate<ShipItem> filter)
  {
    ItemList itemList1 = (ItemList) null;
    if (this.selectedTab == ShopInventoryContainer.Hold && (bool) Game.Me.ActiveShip.IsLoaded)
      itemList1 = (ItemList) Game.Me.Hold;
    else if (this.selectedTab == ShopInventoryContainer.Locker && (bool) Game.Me.ActiveShip.IsLoaded)
      itemList1 = (ItemList) Game.Me.Locker;
    else if (this.selectedTab == ShopInventoryContainer.Store && this.shopModel != null && (bool) Game.Me.ActiveShip.IsLoaded)
      itemList1 = this.shopModel.Filter((Predicate<ShipItem>) (item =>
      {
        Faction faction = item.ShopItemCard.Faction;
        if (faction != Faction.Neutral && faction != Game.Me.Faction)
          return false;
        if (item is ShipSystem)
          return (!(item as ShipSystem).Card.Unique ? 0 : (Game.Me.AlreadyHaveThisItem(item) ? 1 : 0)) == 0;
        return true;
      }));
    if (itemList1 == null)
      return (ItemList) null;
    ItemList itemList2 = itemList1.Filter(filter);
    if (this.ShowOnlyEquipableItems)
      itemList2 = this.FilterEquipable(itemList2);
    return itemList2;
  }

  private bool CheckSystemForSlot(ShipSystem system, ShipSlot slot, bool applyMaxCount = true)
  {
    if (system != null && slot != null && (bool) system.IsLoaded && (system.Card.SlotType != ShipSlotType.ship_paint || slot.Card.SystemType != ShipSlotType.ship_paint || system.PaintCard.shipCard.Equals(Game.Me.ActiveShip.Card)))
    {
      byte num = system.Card.MaxCountPerShip;
      if (applyMaxCount && (int) num > 0 && Game.Me.GetEquippedCount(system) >= (int) num)
      {
        new InfoBox(BsgoLocalization.Get("%$bgo.shop.max_count_reached%", (object) num)).Show();
        return false;
      }
      if (!system.IsBroken && !slot.Inoperable && ((int) system.Card.Tier == (int) Game.Me.ActiveShip.Card.Tier && system.Card.SlotType == slot.Card.SystemType) && system.MeetsShipRestrictions(slot.Ship.Card.ShipObjectKey, slot.Ship.Card.ShipRoles))
        return true;
    }
    return false;
  }

  private bool IsFilledHold(ShipItem item)
  {
    if (item is StarterKit)
      return !this.EnoughHoldForStarterKit(item as StarterKit);
    if (Game.Me.Hold.Count < 70)
      return false;
    foreach (ShipItem shipItem in (IEnumerable<ShipItem>) Game.Me.Hold)
    {
      if (shipItem is ItemCountable && (int) shipItem.CardGUID == (int) item.CardGUID)
        return false;
    }
    return true;
  }

  private bool EnoughHoldForStarterKit(StarterKit starterKit)
  {
    List<ShipItem> shipItemList = starterKit.Card.Items;
    int count = shipItemList.Count;
    for (int index = 0; index < shipItemList.Count; ++index)
    {
      if (Game.Me.Hold.GetByGUID(shipItemList[index].CardGUID) != null && shipItemList[index] is ItemCountable)
        --count;
    }
    return Game.Me.Hold.Count + count <= 70;
  }

  private bool CanBuyThisItem(ShipItem item)
  {
    if (!(item is ShipSystem) || !(item as ShipSystem).Card.Unique || !Game.Me.AlreadyHaveThisItem(item))
      return Game.Me.Hold.HaveMoneyBuy(item, 1);
    FacadeFactory.GetInstance().SendMessage(Message.ShopCanOnlyBuyOnce);
    return false;
  }

  public delegate void Handlr();
}
