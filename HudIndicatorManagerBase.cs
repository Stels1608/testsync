﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorManagerBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public abstract class HudIndicatorManagerBase : IGUIRenderable
{
  private readonly Dictionary<ISpaceEntity, HudIndicatorBase> indicatorMapping = new Dictionary<ISpaceEntity, HudIndicatorBase>();
  private readonly Dictionary<UserSetting, object> currentIndicatorSettings = new Dictionary<UserSetting, object>();
  private InputBinder inputBinder;

  public Dictionary<ISpaceEntity, HudIndicatorBase> IndicatorMapping
  {
    get
    {
      return this.indicatorMapping;
    }
  }

  public abstract bool IsRendered { get; set; }

  public bool IsBigWindow
  {
    get
    {
      return false;
    }
  }

  public bool HasIndicatorFor(ISpaceEntity target)
  {
    return this.indicatorMapping.ContainsKey(target);
  }

  public bool TryFindIndicator(ISpaceEntity target, out HudIndicatorBase indicator)
  {
    return this.indicatorMapping.TryGetValue(target, out indicator);
  }

  public bool TryFindIndicatorComponent<T>(ISpaceEntity target, out T indicatorComponent)
  {
    HudIndicatorBase indicator;
    if (this.TryFindIndicator(target, out indicator) && indicator.TryGetHudIndicatorComponent<T>(out indicatorComponent))
      return true;
    indicatorComponent = default (T);
    return false;
  }

  protected abstract HudIndicatorBase CreateIndicatorForMyUiSystem(ISpaceEntity target);

  public HudIndicatorBase CreateIndicator(ISpaceEntity target)
  {
    HudIndicatorBase indicatorForMyUiSystem = this.CreateIndicatorForMyUiSystem(target);
    this.indicatorMapping.Add(target, indicatorForMyUiSystem);
    indicatorForMyUiSystem.Target = target;
    indicatorForMyUiSystem.PositionUpdate();
    using (Dictionary<UserSetting, object>.Enumerator enumerator = this.currentIndicatorSettings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<UserSetting, object> current = enumerator.Current;
        indicatorForMyUiSystem.ApplyOptionsSetting(current.Key, current.Value);
        indicatorForMyUiSystem.OnScannerVisibilityChange();
      }
    }
    indicatorForMyUiSystem.ApplyInputBindings(this.inputBinder);
    return indicatorForMyUiSystem;
  }

  public void UpdateVisibility(ISpaceEntity target)
  {
    if (!this.indicatorMapping.ContainsKey(target))
      return;
    this.indicatorMapping[target].UpdateVisibility();
  }

  public void RemoveIndicator(ISpaceEntity target)
  {
    if (!this.indicatorMapping.ContainsKey(target))
      return;
    HudIndicatorBase hudIndicator = this.indicatorMapping[target];
    this.indicatorMapping.Remove(target);
    if (HudIndicatorCache.Instance.TryCacheIndicator(hudIndicator, target))
      return;
    if ((Object) hudIndicator.gameObject == (Object) null)
      Debug.LogError((object) ("Trying to destroy already destroyed hudIndicator for SpaceObject: " + target.Name));
    Object.Destroy((Object) hudIndicator.gameObject);
  }

  public void RemoveAllIndicators()
  {
    using (Dictionary<ISpaceEntity, HudIndicatorBase>.Enumerator enumerator = this.indicatorMapping.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current.Value.gameObject);
    }
    this.indicatorMapping.Clear();
    HudIndicatorCache.Instance.ClearAllIndicatorCaches();
  }

  public void ApplyInputBindings(InputBinder inputBinder)
  {
    this.inputBinder = inputBinder;
    using (Dictionary<ISpaceEntity, HudIndicatorBase>.Enumerator enumerator = this.IndicatorMapping.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.ApplyInputBindings(inputBinder);
    }
  }

  public void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    if (!this.currentIndicatorSettings.ContainsKey(userSetting))
      this.currentIndicatorSettings.Add(userSetting, (object) null);
    this.currentIndicatorSettings[userSetting] = data;
    using (Dictionary<ISpaceEntity, HudIndicatorBase>.Enumerator enumerator = this.IndicatorMapping.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.ApplyOptionsSetting(userSetting, data);
    }
  }
}
