﻿// Decompiled with JetBrains decompiler
// Type: UpScaleScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class UpScaleScript : MonoBehaviour
{
  public float ScaleFrom = 0.1f;
  public float ScaleTo = 1f;
  public float TimeinSecs = 2f;
  public float DelayInSec = 2f;
  public bool MaterialAlphaFadeIn = true;
  private float startTime;

  private void Start()
  {
    this.startTime = Time.realtimeSinceStartup + this.DelayInSec;
    this.transform.localScale = Vector3.one * this.ScaleFrom;
    if (!this.MaterialAlphaFadeIn)
      return;
    this.SetOpacity(0.0f);
  }

  private void Update()
  {
    if ((double) Time.realtimeSinceStartup < (double) this.startTime)
      return;
    float num = Mathf.Clamp((Time.realtimeSinceStartup - this.startTime) / this.TimeinSecs, 0.0f, 1f);
    if ((double) Math.Abs(num - 1f) < 9.99999997475243E-07)
      UnityEngine.Object.Destroy((UnityEngine.Object) this);
    if (this.MaterialAlphaFadeIn)
      this.SetOpacity(num);
    this.transform.localScale = Vector3.Lerp(Vector3.one * this.ScaleFrom, Vector3.one * this.ScaleTo, num);
  }

  private void SetOpacity(float opacity)
  {
    Color color = this.GetComponent<Renderer>().material.color;
    this.GetComponent<Renderer>().material.color = new Color(color.r, color.g, color.b, opacity);
  }
}
