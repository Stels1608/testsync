﻿// Decompiled with JetBrains decompiler
// Type: UpgradeOfferSchedule
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

internal class UpgradeOfferSchedule : AbstractOfferSchedule
{
  private UpgradeOfferSchedule(List<IShopOffer> list)
    : base(list, "bgo.notif.active_upgrades_sale")
  {
  }

  public static UpgradeOfferSchedule Decode(BgoProtocolReader input)
  {
    List<IShopOffer> list = new List<IShopOffer>();
    for (uint index = (uint) input.ReadUInt16(); index > 0U; --index)
    {
      UpgradeShopOffer upgradeShopOffer = (UpgradeShopOffer) null;
      try
      {
        upgradeShopOffer = UpgradeShopOffer.Decode(input);
      }
      catch (ArgumentException ex)
      {
        Log.Warning((object) ("I failed to decode a shop offer; ignoring. Details: " + ex.Message));
      }
      list.Add((IShopOffer) upgradeShopOffer);
    }
    return new UpgradeOfferSchedule(list);
  }

  public ShopDiscount FindDiscount(ShopItemType category, int level)
  {
    using (List<IShopOffer>.Enumerator enumerator = this.myList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShopDiscount discount = ((UpgradeShopOffer) enumerator.Current).FindDiscount(category, level);
        if (discount != null)
          return discount;
      }
    }
    return (ShopDiscount) null;
  }
}
