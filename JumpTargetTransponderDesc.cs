﻿// Decompiled with JetBrains decompiler
// Type: JumpTargetTransponderDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class JumpTargetTransponderDesc : IProtocolRead
{
  private uint playerId;
  private uint partyId;
  private uint spaceId;
  private uint expires;

  public uint PlayerId
  {
    get
    {
      return this.playerId;
    }
  }

  public uint SpaceId
  {
    get
    {
      return this.spaceId;
    }
  }

  public uint PartyId
  {
    get
    {
      return this.partyId;
    }
  }

  public uint Expires
  {
    get
    {
      return this.expires;
    }
  }

  public void Read(BgoProtocolReader r)
  {
    this.playerId = r.ReadUInt32();
    this.partyId = r.ReadUInt32();
    this.spaceId = r.ReadUInt32();
    this.expires = r.ReadUInt32();
  }
}
