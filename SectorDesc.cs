﻿// Decompiled with JetBrains decompiler
// Type: SectorDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SectorDesc : IProtocolRead
{
  private const float EPSILON = 0.0001f;
  public uint Id;
  public byte ColonialMinesCount;
  public byte CylonMinesCount;
  public int ColonialThreatLevel;
  public int CylonThreatLevel;
  public int ColonialPvPKilled;
  public int CylonPvPKilled;
  public int ColonialOutpostPoints;
  public int CylonOutpostPoints;
  public bool ColonialIsOutpost;
  public bool CylonIsOutpost;
  public bool ColonialIsOutpostBlocked;
  public bool CylonIsOutpostBlocked;
  public float ColonialOutpostBlockedEndtime;
  public float CylonOutpostBlockedEndtime;
  public bool ColonialIsJumpBeacon;
  public bool CylonIsJumpBeacon;
  public bool ColonialIsJumpBeaconBlocked;
  public bool CylonIsJumpBeaconBlocked;
  public float ColonialJumpBeaconBlockedEndtime;
  public float CylonJumpBeaconBlockedEndtime;
  public ushort ColonialDynamicEvents;
  public ushort CylonDynamicEvents;
  public ushort NeutralDynamicEvents;
  public ushort AncientDynamicEvents;
  public SectorSlotData SectorCaps;
  public JumpTargetTransponderDesc[] JumpTransponders;

  public bool IsOutpostBlocked
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return this.ColonialIsOutpostBlocked;
      return this.CylonIsOutpostBlocked;
    }
  }

  public float OutpostBlockedEndtime
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return this.ColonialOutpostBlockedEndtime;
      return this.CylonOutpostBlockedEndtime;
    }
  }

  public string Name
  {
    get
    {
      return BsgoLocalization.Get("bgo.sector" + (object) this.Id + ".Name");
    }
  }

  public int MyFactionPvPKilled
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return this.ColonialPvPKilled;
      return this.CylonPvPKilled;
    }
  }

  public int MyFactionThreatLevel
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return this.ColonialThreatLevel;
      return this.CylonThreatLevel;
    }
  }

  public int MyFactionOutpostPoints
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return this.ColonialOutpostPoints;
      return this.CylonOutpostPoints;
    }
  }

  public int EnemyFactionScore
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return this.CylonOutpostPoints;
      return this.ColonialOutpostPoints;
    }
  }

  public bool HasColonialMines
  {
    get
    {
      return (int) this.ColonialMinesCount > 0;
    }
  }

  public bool HasCylonMines
  {
    get
    {
      return (int) this.CylonMinesCount > 0;
    }
  }

  public bool HasCylonOutposts
  {
    get
    {
      return this.CylonIsOutpost;
    }
  }

  public bool HasColonialOutposts
  {
    get
    {
      return this.ColonialIsOutpost;
    }
  }

  public bool IsPvPHere
  {
    get
    {
      return this.MyFactionPvPKilled >= 3;
    }
  }

  public bool CanOutpost
  {
    get
    {
      MapStarDesc mapStarDesc;
      if (!StaticCards.GalaxyMap.Stars.TryGetValue(this.Id, out mapStarDesc))
        return false;
      return mapStarDesc.CanOutpost;
    }
  }

  public List<JumpTargetTransponderDesc> FilteredJumpTargetTransponders
  {
    get
    {
      JumpTargetTransponderDesc[] targetTransponderDescArray = this.JumpTransponders;
      List<JumpTargetTransponderDesc> targetTransponderDescList = new List<JumpTargetTransponderDesc>();
      if (targetTransponderDescArray == null || Game.Me.ActiveShip == null || !Game.Me.ActiveShip.HasAspect(ShipAspect.TransponderJump))
        return targetTransponderDescList;
      foreach (JumpTargetTransponderDesc targetTransponderDesc in targetTransponderDescArray)
      {
        DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds((double) targetTransponderDesc.Expires);
        if ((targetTransponderDesc.Expires <= 0U || !(dateTime < Game.TimeSync.ServerDateTime)) && (Game.Me.Ship == null || (int) targetTransponderDesc.SpaceId != (int) Game.Me.Ship.ObjectID) && ((int) targetTransponderDesc.PartyId == 0 || (int) targetTransponderDesc.PartyId == (int) Game.Me.Party.PartyId))
          targetTransponderDescList.Add(targetTransponderDesc);
      }
      return targetTransponderDescList;
    }
  }

  public SectorDesc()
  {
  }

  public SectorDesc(uint id)
  {
    this.Id = id;
  }

  public void Read(BgoProtocolReader r)
  {
    this.Id = (uint) r.ReadUInt16();
    this.ColonialThreatLevel = (int) r.ReadUInt16();
    this.CylonThreatLevel = (int) r.ReadUInt16();
    this.ColonialOutpostPoints = (int) r.ReadUInt16();
    this.SetColonialDelta(r.ReadSingle());
    this.CylonOutpostPoints = (int) r.ReadUInt16();
    this.SetCylonDelta(r.ReadSingle());
    this.ColonialPvPKilled = (int) r.ReadUInt16();
    this.CylonPvPKilled = (int) r.ReadUInt16();
    this.ColonialMinesCount = r.ReadByte();
    this.CylonMinesCount = r.ReadByte();
    this.SetColonialJumpBeacon(r.ReadSingle());
    this.SetCylonJumpBeacon(r.ReadSingle());
    this.ColonialDynamicEvents = r.ReadUInt16();
    this.CylonDynamicEvents = r.ReadUInt16();
    this.NeutralDynamicEvents = r.ReadUInt16();
    this.AncientDynamicEvents = r.ReadUInt16();
    this.JumpTransponders = r.ReadDescArray<JumpTargetTransponderDesc>();
  }

  public bool IsAnyDynamicEventActive()
  {
    return (int) this.NeutralDynamicEvents + (int) this.CylonDynamicEvents + (int) this.ColonialDynamicEvents + (int) this.AncientDynamicEvents > 0;
  }

  public void DebugPrintDynamicEvents()
  {
    Debug.Log((object) ("Dynamic Events in Sector " + (object) this.Id + " - Neutral: " + (object) this.NeutralDynamicEvents + "Cylon: " + (object) this.CylonDynamicEvents + "Colonial: " + (object) this.NeutralDynamicEvents + "Ancient: " + (object) this.NeutralDynamicEvents));
  }

  public void SetColonialDelta(float delta)
  {
    this.ColonialIsOutpost = (double) delta == 1.0;
    this.ColonialIsOutpostBlocked = (double) delta < 0.0;
    this.ColonialOutpostBlockedEndtime = Time.time - delta;
  }

  public void SetCylonDelta(float delta)
  {
    this.CylonIsOutpost = (double) delta == 1.0;
    this.CylonIsOutpostBlocked = (double) delta < 0.0;
    this.CylonOutpostBlockedEndtime = Time.time - delta;
  }

  public void SetColonialJumpBeacon(float delta)
  {
    this.ColonialIsJumpBeacon = (double) Math.Abs(delta - 1f) < 9.99999974737875E-05;
    this.ColonialIsJumpBeaconBlocked = (double) delta < 0.0;
    this.ColonialJumpBeaconBlockedEndtime = Time.time - delta;
  }

  public void SetCylonJumpBeacon(float delta)
  {
    this.CylonIsJumpBeacon = (double) Math.Abs(delta - 1f) < 9.99999974737875E-05;
    this.CylonIsJumpBeaconBlocked = (double) delta < 0.0;
    this.CylonJumpBeaconBlockedEndtime = Time.time - delta;
  }

  public void SetOutpostState(Faction faction, float state)
  {
    switch (faction)
    {
      case Faction.Colonial:
        this.SetColonialDelta(state);
        break;
      case Faction.Cylon:
        this.SetCylonDelta(state);
        break;
      default:
        Debug.LogWarning((object) ("Invalid faction for SetOutpostState: " + (object) faction));
        break;
    }
  }

  public void SetBeaconState(Faction faction, float state)
  {
    switch (faction)
    {
      case Faction.Colonial:
        this.SetColonialJumpBeacon(state);
        break;
      case Faction.Cylon:
        this.SetCylonJumpBeacon(state);
        break;
      default:
        Debug.LogWarning((object) ("Invalid faction for SetBeaconState: " + (object) faction));
        break;
    }
  }

  public void SetOutpostPoints(Faction faction, int points)
  {
    switch (faction)
    {
      case Faction.Colonial:
        this.ColonialOutpostPoints = points;
        break;
      case Faction.Cylon:
        this.CylonOutpostPoints = points;
        break;
      default:
        Debug.LogWarning((object) ("Invalid faction for SetOutpostPoints: " + (object) faction));
        break;
    }
  }

  public void SetMiningShipCount(Faction faction, int count)
  {
    switch (faction)
    {
      case Faction.Colonial:
        this.ColonialMinesCount = (byte) count;
        break;
      case Faction.Cylon:
        this.CylonMinesCount = (byte) count;
        break;
      default:
        Debug.LogWarning((object) ("Invalid faction for SetMiningShipCount: " + (object) faction));
        break;
    }
  }

  public void SetPvPKillCount(Faction faction, int count)
  {
    switch (faction)
    {
      case Faction.Colonial:
        this.ColonialPvPKilled = count;
        break;
      case Faction.Cylon:
        this.CylonPvPKilled = count;
        break;
      default:
        Debug.LogWarning((object) ("Invalid faction for SetPvPKillCount: " + (object) faction));
        break;
    }
  }

  public void SetDynamicMissions(Faction faction, int count)
  {
    switch (faction)
    {
      case Faction.Neutral:
        this.NeutralDynamicEvents = (ushort) count;
        break;
      case Faction.Colonial:
        this.ColonialDynamicEvents = (ushort) count;
        break;
      case Faction.Cylon:
        this.CylonDynamicEvents = (ushort) count;
        break;
      case Faction.Ancient:
        this.AncientDynamicEvents = (ushort) count;
        break;
      default:
        Debug.LogWarning((object) ("Invalid faction for SetDynamicMissions: " + (object) faction));
        break;
    }
  }

  public override string ToString()
  {
    return string.Format("Id: {0}, ColonialOutpostPoints: {1}, CylonOutpostPoints: {2}, ColonialIsOutpost: {3}, CylonIsOutpost: {4}, ColonialIsOutpostBlocked: {5}, CylonIsOutpostBlocked: {6}, ColonialOutpostBlockedEndtime: {7}, CylonOutpostBlockedEndtime: {8}, ColonialIsJumpBeacon: {9}, CylonIsJumpBeacon: {10}, ColonialIsJumpBeaconBlocked: {11}, CylonIsJumpBeaconBlocked: {12}, ColonialJumpBeaconBlockedEndtime: {13}, CylonJumpBeaconBlockedEndtime: {14}", (object) this.Id, (object) this.ColonialOutpostPoints, (object) this.CylonOutpostPoints, (object) this.ColonialIsOutpost, (object) this.CylonIsOutpost, (object) this.ColonialIsOutpostBlocked, (object) this.CylonIsOutpostBlocked, (object) this.ColonialOutpostBlockedEndtime, (object) this.CylonOutpostBlockedEndtime, (object) this.ColonialIsJumpBeacon, (object) this.CylonIsJumpBeacon, (object) this.ColonialIsJumpBeaconBlocked, (object) this.CylonIsJumpBeaconBlocked, (object) this.ColonialJumpBeaconBlockedEndtime, (object) this.CylonJumpBeaconBlockedEndtime);
  }
}
