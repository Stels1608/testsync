﻿// Decompiled with JetBrains decompiler
// Type: WofDrawReply
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public struct WofDrawReply
{
  public bool freeWofGame;
  public List<WofRewardItemContainer> rewardItems;
  public List<WofBonusMapPart> bonusMapCards;
  public WofJackpotContainer jackpot;
}
