﻿// Decompiled with JetBrains decompiler
// Type: GuiUpgrading
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GuiUpgrading : GuiDialog
{
  public bool success;
  private readonly GuiLabel resTitle;
  private readonly GuiHealthbar progress;
  private readonly GuiLabel result;
  private readonly GuiLabel label;
  private readonly GuiButton resConfirm;
  private readonly GuiButton resCancel;

  public GuiUpgrading(GuiUpgradeWindowData data)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiUpgrading.\u003CGuiUpgrading\u003Ec__AnonStoreyC0 upgradingCAnonStoreyC0 = new GuiUpgrading.\u003CGuiUpgrading\u003Ec__AnonStoreyC0();
    // ISSUE: reference to a compiler-generated field
    upgradingCAnonStoreyC0.data = data;
    this.resTitle = new GuiLabel(Gui.Options.FontBGM_BT, 17);
    this.progress = new GuiHealthbar();
    this.result = new GuiLabel(Gui.Options.FontBGM_BT, 16);
    this.label = new GuiLabel(Gui.Options.FontBGM_BT, 16);
    this.resConfirm = new GuiButton("%$bgo.upgrade.confirm%");
    this.resCancel = new GuiButton("%$bgo.common.cancel%");
    // ISSUE: explicit constructor call
    base.\u002Ector();
    // ISSUE: reference to a compiler-generated field
    upgradingCAnonStoreyC0.\u003C\u003Ef__this = this;
    this.Align = Align.MiddleCenter;
    this.Position = new Vector2(0.0f, 0.0f);
    this.Size = new Vector2(460f, 200f);
    this.CloseButton.IsRendered = false;
    // ISSUE: reference to a compiler-generated method
    this.OnClose = new AnonymousDelegate(upgradingCAnonStoreyC0.\u003C\u003Em__1AE);
    this.resTitle.WordWrap = true;
    this.resTitle.SizeX = 410f;
    this.resTitle.Alignment = TextAnchor.UpperCenter;
    this.AddChild((GuiElementBase) this.resTitle, Align.UpCenter, new Vector2(0.0f, 20f));
    this.progress.SizeX = this.SizeX - 40f;
    this.progress.SizeY = 20f;
    this.progress.Color = Gui.Options.PositiveColor;
    this.AddChild((GuiElementBase) this.progress, Align.UpCenter, new Vector2(0.0f, 70f));
    this.AddChild((GuiElementBase) this.result, Align.UpCenter, new Vector2(0.0f, 100f), false);
    this.AddChild((GuiElementBase) this.label, Align.UpCenter, new Vector2(0.0f, 125f), false);
    this.AddChild((GuiElementBase) this.resConfirm, Align.DownCenter, new Vector2(-100f, -15f), false);
    this.AddChild((GuiElementBase) this.resCancel, Align.DownCenter, new Vector2(100f, -15f), false);
    // ISSUE: reference to a compiler-generated method
    this.resConfirm.Pressed = new AnonymousDelegate(upgradingCAnonStoreyC0.\u003C\u003Em__1AF);
    // ISSUE: reference to a compiler-generated method
    this.resCancel.Pressed = new AnonymousDelegate(upgradingCAnonStoreyC0.\u003C\u003Em__1B0);
    // ISSUE: reference to a compiler-generated method
    this.AddChild((GuiElementBase) new Gui.Timer(0.3f, new AnonymousDelegate(upgradingCAnonStoreyC0.\u003C\u003Em__1B1)));
    // ISSUE: reference to a compiler-generated field
    this.resTitle.Text = BsgoLocalization.Get("%$bgo.upgrade.upgrading%", (object) upgradingCAnonStoreyC0.data.Item.ItemGUICard.Name);
    this.label.Text = "%$bgo.upgrade.upgrading_conf%";
  }

  [Gui2Editor]
  public static void Show(int i)
  {
    GuiUpgrading guiUpgrading = Game.GUIManager.Find<GuiUpgrading>();
    if (guiUpgrading != null)
    {
      Game.UnregisterDialog((IGUIPanel) guiUpgrading);
      guiUpgrading.OnClose();
    }
    Game.RegisterDialog((IGUIPanel) new GuiUpgrading(new GuiUpgradeWindowData(Game.Me.Hangar.ActiveShip.Slots[i].ServerID)), true);
  }
}
