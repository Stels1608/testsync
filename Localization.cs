﻿// Decompiled with JetBrains decompiler
// Type: Localization
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public static class Localization
{
  public static bool localizationHasBeenSet = false;
  private static string[] mLanguages = (string[]) null;
  private static Dictionary<string, string> mOldDictionary = new Dictionary<string, string>();
  private static Dictionary<string, string[]> mDictionary = new Dictionary<string, string[]>();
  private static int mLanguageIndex = -1;
  public static Localization.LoadFunction loadFunction;
  private static string mLanguage;

  public static Dictionary<string, string[]> dictionary
  {
    get
    {
      if (!Localization.localizationHasBeenSet)
        Localization.language = PlayerPrefs.GetString("Language", "English");
      return Localization.mDictionary;
    }
    set
    {
      Localization.localizationHasBeenSet = value != null;
      Localization.mDictionary = value;
    }
  }

  public static string[] knownLanguages
  {
    get
    {
      if (!Localization.localizationHasBeenSet)
        Localization.LoadDictionary(PlayerPrefs.GetString("Language", "English"));
      return Localization.mLanguages;
    }
  }

  public static string language
  {
    get
    {
      if (string.IsNullOrEmpty(Localization.mLanguage))
      {
        string[] knownLanguages = Localization.knownLanguages;
        Localization.mLanguage = PlayerPrefs.GetString("Language", knownLanguages == null ? "English" : knownLanguages[0]);
        Localization.LoadAndSelect(Localization.mLanguage);
      }
      return Localization.mLanguage;
    }
    set
    {
      if (!(Localization.mLanguage != value))
        return;
      Localization.mLanguage = value;
      Localization.LoadAndSelect(value);
    }
  }

  [Obsolete("Localization is now always active. You no longer need to check this property.")]
  public static bool isActive
  {
    get
    {
      return true;
    }
  }

  private static bool LoadDictionary(string value)
  {
    byte[] bytes = (byte[]) null;
    if (!Localization.localizationHasBeenSet)
    {
      if (Localization.loadFunction == null)
      {
        TextAsset textAsset = Resources.Load<TextAsset>("Localization");
        if ((UnityEngine.Object) textAsset != (UnityEngine.Object) null)
          bytes = textAsset.bytes;
      }
      else
        bytes = Localization.loadFunction("Localization");
      Localization.localizationHasBeenSet = true;
    }
    if (Localization.LoadCSV(bytes))
      return true;
    if (string.IsNullOrEmpty(value))
      return false;
    if (Localization.loadFunction == null)
    {
      TextAsset textAsset = Resources.Load<TextAsset>(value);
      if ((UnityEngine.Object) textAsset != (UnityEngine.Object) null)
        bytes = textAsset.bytes;
    }
    else
      bytes = Localization.loadFunction(value);
    if (bytes == null)
      return false;
    Localization.Set(value, bytes);
    return true;
  }

  private static bool LoadAndSelect(string value)
  {
    if (!string.IsNullOrEmpty(value))
    {
      if (Localization.mDictionary.Count == 0 && !Localization.LoadDictionary(value))
        return false;
      if (Localization.SelectLanguage(value))
        return true;
    }
    if (Localization.mOldDictionary.Count > 0)
      return true;
    Localization.mOldDictionary.Clear();
    Localization.mDictionary.Clear();
    if (string.IsNullOrEmpty(value))
      PlayerPrefs.DeleteKey("Language");
    return false;
  }

  public static void Load(TextAsset asset)
  {
    ByteReader byteReader = new ByteReader(asset);
    Localization.Set(asset.name, byteReader.ReadDictionary());
  }

  public static void Set(string languageName, byte[] bytes)
  {
    ByteReader byteReader = new ByteReader(bytes);
    Localization.Set(languageName, byteReader.ReadDictionary());
  }

  public static bool LoadCSV(TextAsset asset)
  {
    return Localization.LoadCSV(asset.bytes, asset);
  }

  public static bool LoadCSV(byte[] bytes)
  {
    return Localization.LoadCSV(bytes, (TextAsset) null);
  }

  private static bool LoadCSV(byte[] bytes, TextAsset asset)
  {
    if (bytes == null)
      return false;
    ByteReader byteReader = new ByteReader(bytes);
    BetterList<string> values = byteReader.ReadCSV();
    if (values.size < 2)
      return false;
    values[0] = "KEY";
    if (!string.Equals(values[0], "KEY"))
    {
      Debug.LogError((object) ("Invalid localization CSV file. The first value is expected to be 'KEY', followed by language columns.\nInstead found '" + values[0] + "'"), (UnityEngine.Object) asset);
      return false;
    }
    Localization.mLanguages = new string[values.size - 1];
    for (int index = 0; index < Localization.mLanguages.Length; ++index)
      Localization.mLanguages[index] = values[index + 1];
    Localization.mDictionary.Clear();
    for (; values != null; values = byteReader.ReadCSV())
      Localization.AddCSV(values);
    return true;
  }

  private static bool SelectLanguage(string language)
  {
    Localization.mLanguageIndex = -1;
    string[] strArray;
    if (Localization.mDictionary.Count == 0 || !Localization.mDictionary.TryGetValue("KEY", out strArray))
      return false;
    for (int index = 0; index < strArray.Length; ++index)
    {
      if (strArray[index] == language)
      {
        Localization.mOldDictionary.Clear();
        Localization.mLanguageIndex = index;
        Localization.mLanguage = language;
        PlayerPrefs.SetString("Language", Localization.mLanguage);
        UIRoot.Broadcast("OnLocalize");
        return true;
      }
    }
    return false;
  }

  private static void AddCSV(BetterList<string> values)
  {
    if (values.size < 2)
      return;
    string[] strArray = new string[values.size - 1];
    for (int index = 1; index < values.size; ++index)
      strArray[index - 1] = values[index];
    try
    {
      Localization.mDictionary.Add(values[0], strArray);
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ("Unable to add '" + values[0] + "' to the Localization dictionary.\n" + ex.Message));
    }
  }

  public static void Set(string languageName, Dictionary<string, string> dictionary)
  {
    Localization.mLanguage = languageName;
    PlayerPrefs.SetString("Language", Localization.mLanguage);
    Localization.mOldDictionary = dictionary;
    Localization.localizationHasBeenSet = false;
    Localization.mLanguageIndex = -1;
    Localization.mLanguages = new string[1]{ languageName };
    UIRoot.Broadcast("OnLocalize");
  }

  public static string Get(string key)
  {
    if (!Localization.localizationHasBeenSet)
      Localization.language = PlayerPrefs.GetString("Language", "English");
    string[] strArray;
    if (Localization.mLanguageIndex != -1 && Localization.mDictionary.TryGetValue(key, out strArray))
    {
      if (Localization.mLanguageIndex < strArray.Length)
        return strArray[Localization.mLanguageIndex];
    }
    else
    {
      string str;
      if (Localization.mOldDictionary.TryGetValue(key, out str))
        return str;
    }
    return key;
  }

  public static string Format(string key, params object[] parameters)
  {
    return string.Format(Localization.Get(key), parameters);
  }

  [Obsolete("Use Localization.Get instead")]
  public static string Localize(string key)
  {
    return Localization.Get(key);
  }

  public static bool Exists(string key)
  {
    if (!Localization.localizationHasBeenSet)
      Localization.language = PlayerPrefs.GetString("Language", "English");
    if (!Localization.mDictionary.ContainsKey(key))
      return Localization.mOldDictionary.ContainsKey(key);
    return true;
  }

  public delegate byte[] LoadFunction(string path);
}
