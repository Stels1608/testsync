﻿// Decompiled with JetBrains decompiler
// Type: Locker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Locker : ItemList
{
  public Locker()
    : base((IContainerID) new Locker.LockerContainerID())
  {
  }

  protected override void Changed()
  {
    base.Changed();
    if (!((Object) RoomLevel.GetLevel() != (Object) null))
      return;
    this.AutoUse();
  }

  public class LockerContainerID : IContainerID, IProtocolWrite
  {
    void IProtocolWrite.Write(BgoProtocolWriter w)
    {
      w.Write((byte) 2);
    }

    public ContainerType GetContainerType()
    {
      return ContainerType.Locker;
    }
  }
}
