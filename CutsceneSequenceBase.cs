﻿// Decompiled with JetBrains decompiler
// Type: CutsceneSequenceBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public abstract class CutsceneSequenceBase : MonoBehaviour
{
  protected CutscenePlayRequestData cutscenePlayRequestData;
  protected CutsceneManager.OnFinishCutsceneDelegate onFinishDelegate;
  private GameObject cutsceneObject;
  private CutsceneSetup cutsceneSetup;
  private Coroutine loadingCoroutine;
  private CutsceneLoadingScreen cutsceneLoadingScreen;

  protected Camera ActiveCamera
  {
    get
    {
      return GameLevel.Instance.cameraSwitcher.GetActiveCamera();
    }
    set
    {
      GameLevel.Instance.cameraSwitcher.SetActiveCamera(value, false);
    }
  }

  protected abstract string GetSceneSetupName { get; }

  protected abstract CutsceneSkipMode GetSkipMode { get; }

  protected CutsceneSetup CutsceneSetup
  {
    get
    {
      return this.cutsceneSetup;
    }
  }

  protected abstract bool ExecuteCustomPreparations();

  public void Setup(CutscenePlayRequestData playRequestData)
  {
    this.cutscenePlayRequestData = playRequestData;
    if (playRequestData.OnFinishDelegate != null)
      this.onFinishDelegate += playRequestData.OnFinishDelegate;
    this.loadingCoroutine = this.StartCoroutine(this.LoadAndInitCutsceneSetup());
  }

  private void Awake()
  {
    this.ToggleUi(false);
    this.CreateCutsceneLoadingScreen();
  }

  private void OnCutsceneLoaded()
  {
    if (!this.ExecuteCustomPreparations())
    {
      UnityEngine.Debug.LogError((object) ("Couldn't init all necessary objects for Cutscene: " + (object) this));
      this.AbortImmediately();
    }
    try
    {
      this.DestroyCutsceneLoadingScreen();
      this.StartScene();
    }
    catch (Exception ex)
    {
      UnityEngine.Debug.LogError((object) ("Failed Starting Cutscene. Exception: " + (object) ex));
      this.AbortImmediately();
    }
  }

  private void Update()
  {
    if (!Input.GetKeyDown(KeyCode.Escape) && !Input.GetKeyDown(KeyCode.Space) && !Input.GetKeyDown(KeyCode.Mouse1))
      return;
    switch (this.GetSkipMode)
    {
      case CutsceneSkipMode.SkipImmediately:
        this.AbortDelayed();
        break;
      case CutsceneSkipMode.ShowMenu:
        CutsceneManager.Instance.ToggleSkipMenu();
        break;
    }
  }

  protected abstract void StartScene();

  public void AddOnFinishFunction(CutsceneManager.OnFinishCutsceneDelegate onFinishFunction)
  {
    this.onFinishDelegate += onFinishFunction;
  }

  protected abstract void Cleanup();

  public void AbortDelayed()
  {
    this.StartCoroutine(this.AbortDelayedEnumerator());
  }

  public void AbortImmediately()
  {
    this.ProcessAbort();
  }

  [DebuggerHidden]
  private IEnumerator AbortDelayedEnumerator()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CutsceneSequenceBase.\u003CAbortDelayedEnumerator\u003Ec__Iterator15() { \u003C\u003Ef__this = this };
  }

  private void ProcessAbort()
  {
    try
    {
      if (this.loadingCoroutine != null)
        this.StopCoroutine(this.loadingCoroutine);
    }
    catch (Exception ex)
    {
      UnityEngine.Debug.LogError((object) ("Failed at StopCoroutine(loadingCoroutine): Exception:" + (object) ex));
    }
    try
    {
      this.UnhideHiddenObjects();
    }
    catch (Exception ex)
    {
      UnityEngine.Debug.LogError((object) ("Failed at UnhideHiddenObjects(): Exception:" + (object) ex));
    }
    try
    {
      this.Cleanup();
    }
    catch (Exception ex)
    {
      UnityEngine.Debug.LogError((object) ("Failed at Cleanup(): Exception:" + (object) ex));
    }
    try
    {
      this.OnFinish();
    }
    catch (Exception ex)
    {
      UnityEngine.Debug.LogError((object) ("Failed at OnFinish(): Exception:" + (object) ex));
    }
    try
    {
      this.DestroyCutsceneLoadingScreen();
    }
    catch (Exception ex)
    {
      UnityEngine.Debug.LogError((object) ("Failed at DestroyCutsceneLoadingScreen(): Exception:" + (object) ex));
    }
  }

  private void UnhideHiddenObjects()
  {
    if ((UnityEngine.Object) this.cutsceneObject == (UnityEngine.Object) null)
      return;
    foreach (CutsceneShipEvents componentsInChild in this.cutsceneObject.GetComponentsInChildren<CutsceneShipEvents>(true))
      componentsInChild.UnhideAllHiddenObjects();
  }

  private void OnFinish()
  {
    GameLevel.Instance.cameraSwitcher.SetActiveCamera(Camera.main, true);
    this.ToggleUi(true);
    this.onFinishDelegate();
    if (!((UnityEngine.Object) this.cutsceneObject != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.cutsceneObject);
  }

  private void ToggleUi(bool enable)
  {
    Game.GUIManager.IsRendered = enable;
    UICamera.mainCamera.enabled = enable;
    UguiTools.EnableAllCanvases(enable);
  }

  protected void ShowRealPlayerShip(bool show)
  {
    SpaceLevel.GetLevel().PlayerActualShip.Model.SetActive(show);
  }

  protected GameObject CreatePlayerShipCopy()
  {
    return BgoUtils.CreateShipCopyForCutscene(SpaceLevel.GetLevel().PlayerActualShip.Model);
  }

  protected void ActivateMotionBlur(Camera cam, bool activate)
  {
    if ((UnityEngine.Object) cam == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "ActivateMotionBlur(): Passed camera is null.");
    }
    else
    {
      MotionBlur component = cam.gameObject.GetComponent<MotionBlur>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.enabled = activate;
      else
        UnityEngine.Debug.LogWarning((object) ("ActivateMotionBlur(): Wanted to activate motion blur for camera " + cam.name + " but its gameobject lacked the necessary MotionBlur component."));
    }
  }

  [DebuggerHidden]
  private IEnumerator LoadAndInitCutsceneSetup()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CutsceneSequenceBase.\u003CLoadAndInitCutsceneSetup\u003Ec__Iterator16() { \u003C\u003Ef__this = this };
  }

  private void CreateCutsceneLoadingScreen()
  {
    if ((UnityEngine.Object) this.cutsceneLoadingScreen != (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "CreateCutsceneLoadingScreen(): Loading screen already existed.");
      this.DestroyCutsceneLoadingScreen();
    }
    this.cutsceneLoadingScreen = new GameObject("Cutscene Loading Screen").AddComponent<CutsceneLoadingScreen>();
  }

  private void DestroyCutsceneLoadingScreen()
  {
    if ((UnityEngine.Object) this.cutsceneLoadingScreen == (UnityEngine.Object) null)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.cutsceneLoadingScreen.gameObject);
  }
}
