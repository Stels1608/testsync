﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorFactoryNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class HudIndicatorFactoryNgui
{
  public static HudIndicatorBase CreateIndicator(ISpaceEntity target, GameObject parentPanel)
  {
    if (target is SpacePosition)
      return HudIndicatorFactoryNgui.CreateSpacePositionIndicator(target as SpacePosition, parentPanel);
    if (target is SpaceObject)
      return HudIndicatorFactoryNgui.CreateSpaceObjectIndicator(target as SpaceObject, parentPanel);
    Debug.LogError((object) "HudIndicatorFactory.CreateIndicator(): Invalid target type");
    return (HudIndicatorBase) null;
  }

  private static HudIndicatorBase CreateSpacePositionIndicator(SpacePosition target, GameObject parent)
  {
    HudIndicatorSpacePositionNgui spacePositionNgui = NGUITools.AddChild<HudIndicatorSpacePositionNgui>(parent);
    spacePositionNgui.AddHudIndicatorComponent<HudIndicatorWaypointNgui>();
    spacePositionNgui.AddHudIndicatorComponent<HudIndicatorBracketBigNgui>();
    spacePositionNgui.AddHudIndicatorComponent<HudIndicatorOutsideScreenArrowNgui>();
    return (HudIndicatorBase) spacePositionNgui;
  }

  private static HudIndicatorBase CreateSpaceObjectIndicator(SpaceObject target, GameObject parent)
  {
    HudIndicatorBase cachedIndicator;
    if (HudIndicatorCache.Instance.TryPopCachedIndicator(target.SpaceEntityType, out cachedIndicator))
    {
      cachedIndicator.Target = (ISpaceEntity) target;
      cachedIndicator.gameObject.SetActive(true);
      return cachedIndicator;
    }
    HudIndicatorBase hudIndicatorBase = (HudIndicatorBase) NGUITools.AddChild<HudIndicatorSpaceObjectNgui>(parent);
    hudIndicatorBase.name = "HudIndicator (" + target.Name + ")";
    if (target.SpaceEntityType == SpaceEntityType.Trigger)
    {
      hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorWaypointNgui>();
      hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorDistanceDisplayTriggerNgui>();
      hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorOutsideScreenArrowNgui>();
      return hudIndicatorBase;
    }
    hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorCombatInfoNgui>();
    hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorMultiTargetNgui>();
    hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorDesignatorNgui>();
    hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorWaypointNgui>();
    HudIndicatorTopTextNgui indicatorTopTextNgui;
    switch (target.SpaceEntityType)
    {
      case SpaceEntityType.Asteroid:
      case SpaceEntityType.Planetoid:
        indicatorTopTextNgui = (HudIndicatorTopTextNgui) hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorTopTextAsteroidNgui>();
        break;
      case SpaceEntityType.SectorEvent:
        indicatorTopTextNgui = (HudIndicatorTopTextNgui) hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorTopTextSectorEventNgui>();
        break;
      default:
        indicatorTopTextNgui = hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorTopTextNgui>();
        break;
    }
    HudIndicatorBracketBaseNgui indicatorBracketBaseNgui = (HudIndicatorBracketBaseNgui) null;
    switch (target.SpaceEntityType)
    {
      case SpaceEntityType.Missile:
      case SpaceEntityType.Mine:
      case SpaceEntityType.JumpTargetTransponder:
        indicatorBracketBaseNgui = (HudIndicatorBracketBaseNgui) hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorBracketSmallNgui>();
        hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorDistanceDisplayMissilesNgui>();
        break;
      case SpaceEntityType.Asteroid:
      case SpaceEntityType.Planetoid:
        indicatorBracketBaseNgui = (HudIndicatorBracketBaseNgui) hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorBracketAsteroidNgui>();
        hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorDistanceDisplayAsteroidNgui>();
        break;
      case SpaceEntityType.SectorEvent:
        hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorDistanceDisplaySectorEventNgui>();
        break;
      default:
        indicatorBracketBaseNgui = (HudIndicatorBracketBaseNgui) hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorBracketBigNgui>();
        hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorDistanceDisplayNgui>();
        break;
    }
    if (target.SpaceEntityType == SpaceEntityType.Player)
      hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorMedalsNgui>().AnchorToTop((UIWidget) indicatorTopTextNgui.TextLabel);
    HudIndicatorSectorEventNgui indicatorSectorEventNgui = (HudIndicatorSectorEventNgui) null;
    if (target.SpaceEntityType == SpaceEntityType.SectorEvent)
      indicatorSectorEventNgui = hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorSectorEventNgui>();
    if (target.SpaceEntityType == SpaceEntityType.SectorEvent)
      hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorOutsideScreenArrowSectorEventsNgui>();
    else
      hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorOutsideScreenArrowNgui>();
    UIRect targetRect = target.SpaceEntityType == SpaceEntityType.SectorEvent ? (!((Object) indicatorSectorEventNgui != (Object) null) ? (UIRect) null : (UIRect) indicatorSectorEventNgui.SectorEventIcon) : (!((Object) indicatorBracketBaseNgui != (Object) null) ? (UIRect) null : (UIRect) indicatorBracketBaseNgui.BracketSprite);
    NGUIToolsExtension.AnchorTo((UIRect) indicatorTopTextNgui.TextLabel, targetRect, UIAnchor.Side.Top, 0, 0);
    indicatorTopTextNgui.TextLabel.leftAnchor.target = indicatorTopTextNgui.TextLabel.rightAnchor.target = indicatorTopTextNgui.TextLabel.topAnchor.target = (Transform) null;
    switch (target.SpaceEntityType)
    {
      case SpaceEntityType.Asteroid:
      case SpaceEntityType.Planetoid:
        hudIndicatorBase.AddHudIndicatorComponent<HudIndicatorAsteroidResourceNgui>();
        break;
    }
    return hudIndicatorBase;
  }
}
