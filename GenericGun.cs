﻿// Decompiled with JetBrains decompiler
// Type: GenericGun
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GenericGun : AbstractGun
{
  public float MuzzleLifetime = 2f;
  private readonly Vector3 farAway = new Vector3(50000f, 50000f, 50000f);
  public float SoundVolume = 1f;
  public GameObject Muzzle;
  public AudioClip[] FireSounds;
  private AudioSource gunAudio;
  public static int LastSoundIndex;
  public AudioRolloffMode RolloffMode;

  private void Awake()
  {
    this.gunAudio = this.gameObject.AddComponent<AudioSource>();
    this.gunAudio.spatialBlend = 1f;
    this.gunAudio.volume = this.SoundVolume;
    this.gunAudio.rolloffMode = this.RolloffMode;
    this.gunAudio.minDistance = 10f;
    this.gunAudio.maxDistance = 2560f;
    this.gunAudio.playOnAwake = false;
    this.gunAudio.loop = false;
  }

  public override void Fire(SpaceObject target)
  {
    base.Fire(target);
    this.PlayFireSound();
    if ((Object) this.Muzzle != (Object) null)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.Muzzle);
      gameObject.transform.position = this.transform.position;
      gameObject.transform.rotation = this.transform.rotation;
      Object.Destroy((Object) gameObject, this.MuzzleLifetime);
    }
    this.LaunchBullet();
  }

  private void LaunchBullet()
  {
    this.MakeShot();
    this.SaveLastTargetPosition();
  }

  protected override Vector3 GetShotOrigin()
  {
    return this.transform.position;
  }

  protected override BulletScript GetNextBullet()
  {
    BulletScript bulletScript = Object.Instantiate<GameObject>(this.BulletPrefab).AddComponent<BulletScript>();
    bulletScript.Destroyed = new BulletScript.DestroyedEventHandler(this.DestroyBullet);
    bulletScript.transform.position = this.farAway;
    return bulletScript;
  }

  private void DestroyBullet(BulletScript bulletScript)
  {
    Object.Destroy((Object) bulletScript.gameObject);
  }

  private void PlayFireSound()
  {
    if (this.FireSounds == null || this.FireSounds.Length == 0)
      return;
    int index = Random.Range(0, this.FireSounds.Length);
    if (this.FireSounds.Length != 0 && index == GenericGun.LastSoundIndex)
      index = (index + 1) % this.FireSounds.Length;
    GenericGun.LastSoundIndex = index;
    this.gunAudio.clip = this.FireSounds[index];
    this.gunAudio.Play();
  }
}
