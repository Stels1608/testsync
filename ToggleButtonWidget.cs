﻿// Decompiled with JetBrains decompiler
// Type: ToggleButtonWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ToggleButtonWidget : ButtonWidget
{
  [SerializeField]
  public GameObject defaultState;
  [SerializeField]
  public GameObject activeState;
  [SerializeField]
  public GameObject hoverState;
  [SerializeField]
  [HideInInspector]
  protected bool isActive;
  [SerializeField]
  public bool ActiveIsOverlay;

  public bool IsActive
  {
    get
    {
      return this.isActive;
    }
    set
    {
      if (value == this.isActive)
        return;
      this.isActive = value;
      this.Set();
    }
  }

  public ToggleButtonWidget()
  {
    this.ActiveIsOverlay = true;
  }

  public virtual void OnEnable()
  {
    this.Set();
    if (!((Object) this.hoverState != (Object) null))
      return;
    this.hoverState.SetActive(false);
  }

  protected virtual void Set()
  {
    if (!this.ActiveIsOverlay && (Object) null != (Object) this.defaultState)
      this.defaultState.SetActive(!this.isActive);
    if ((Object) null != (Object) this.activeState)
      this.activeState.SetActive(this.isActive);
    if (!((Object) this.TextLabel != (Object) null))
      return;
    this.TextLabel.color = !this.isActive ? ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_RUNNING) : ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_ACTIVE);
  }

  public override void OnClick()
  {
    if (this.IsEnabled && this.enabled)
    {
      this.IsActive = !this.IsActive;
      if ((Object) null != (Object) this.hoverState)
        this.hoverState.SetActive(false);
    }
    base.OnClick();
  }

  public override void OnHover(bool isOver)
  {
    if (!this.IsEnabled || !this.enabled)
      return;
    base.OnHover(isOver);
    if ((Object) null != (Object) this.hoverState && !this.IsActive)
      this.hoverState.SetActive(isOver);
    this.HoverSubSprites(isOver);
  }

  protected virtual void HoverSubSprites(bool isOver)
  {
    foreach (UISprite componentsInChild in this.GetComponentsInChildren<UISprite>(true))
    {
      if (!((Object) componentsInChild == (Object) this.Icon) && !((Object) componentsInChild.gameObject.GetComponent<WidgetColorizer>() != (Object) null))
        componentsInChild.color = !isOver ? ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR) : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_CLICK_COLOR);
    }
  }
}
