﻿// Decompiled with JetBrains decompiler
// Type: GUIShopPaperDollStarterKit
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GUIShopPaperDollStarterKit : GUIPanel
{
  private string oldTexture = string.Empty;
  private readonly GUIShopStarterKitSlot[] slots = new GUIShopStarterKitSlot[6];
  private const int SLOTS_COUNT = 6;
  private const float SLOTS_SPACE = 10f;
  private readonly GUIImageNew papperDollImage;
  private readonly GUILabelNew titleLabel;
  private readonly GUILabelNew descLabel;
  private readonly GUILabelNew costLabel;
  private readonly GUIImageNew costImage1;
  private readonly GUILabelNew costLabel1;
  private readonly GUIImageNew costImage2;
  private readonly GUILabelNew costLabel2;
  private readonly GUIButtonNew requisitionButton;
  private readonly GUIScrollBox scrollBox;
  private int viewFirst;

  public int ViewFirst
  {
    get
    {
      return this.viewFirst;
    }
    set
    {
      this.viewFirst = value;
    }
  }

  public AnonymousDelegate HandlerButton
  {
    set
    {
      this.requisitionButton.Handler = value;
    }
  }

  public GUIShopPaperDollStarterKit(SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
    this.papperDollImage = new GUIImageNew(TextureCache.Get(Color.black), this.root);
    this.papperDollImage.IsRendered = false;
    this.AddPanel((GUIPanel) this.papperDollImage);
    GUIShopStarterKitSlot shopStarterKitSlot1 = new GUIShopStarterKitSlot(this.root);
    shopStarterKitSlot1.IsRendered = false;
    this.slots[0] = shopStarterKitSlot1;
    this.AddPanel((GUIPanel) shopStarterKitSlot1);
    float2 float2 = new float2(shopStarterKitSlot1.Rect.width, shopStarterKitSlot1.Rect.height);
    this.root.Width = float2.x;
    this.root.Height = (float) (((double) float2.y + 10.0) * 6.0);
    float _y = (float) (-(((double) float2.y + 5.0) * 6.0) / 2.0 + (double) float2.y / 2.0 + 10.0);
    shopStarterKitSlot1.Position = new float2(0.0f, _y);
    for (int index = 1; index < this.slots.Length; ++index)
    {
      GUIShopStarterKitSlot shopStarterKitSlot2 = index != 5 ? new GUIShopStarterKitSlot(this.root) : new GUIShopStarterKitSlot(this.root);
      shopStarterKitSlot2.IsRendered = false;
      this.slots[index] = shopStarterKitSlot2;
      this.AddPanel((GUIPanel) shopStarterKitSlot2);
      shopStarterKitSlot2.Position = new float2(0.0f, _y + (float2.y + 5f) * (float) index);
    }
    this.costLabel = new GUILabelNew("%$bgo.EquipBuyPanel.gui_slot_layout.cost_label%");
    this.costLabel.Position = new float2(-34f, 155f);
    this.costLabel.Font = Gui.Options.FontBGM_BT;
    this.costLabel.FontSize = 14;
    this.AddPanel((GUIPanel) this.costLabel);
    this.costImage1 = new GUIImageNew((Texture2D) ResourceLoader.Load("GUI/Common/cubits"), this.root);
    this.costImage1.Position = new float2(-4f, 155f);
    this.costImage1.Size = new float2(8f, 12f);
    this.AddPanel((GUIPanel) this.costImage1);
    this.costLabel1 = new GUILabelNew("%$bgo.EquipBuyPanel.gui_slot_layout.cubits_label%");
    this.costLabel1.Position = new float2(34f, 155f);
    this.costLabel1.AutoSize = true;
    this.costLabel1.Font = Gui.Options.FontBGM_BT;
    this.costLabel1.FontSize = 14;
    this.AddPanel((GUIPanel) this.costLabel1);
    this.costImage2 = new GUIImageNew((Texture2D) ResourceLoader.Load("GUI/Common/cubits"), this.root);
    this.costImage2.Position = new float2(96f, 155f);
    this.costImage2.Size = new float2(8f, 12f);
    this.AddPanel((GUIPanel) this.costImage2);
    this.costLabel2 = new GUILabelNew("%$bgo.EquipBuyPanel.gui_slot_layout.cubits_label%");
    this.costLabel2.Position = new float2(134f, 155f);
    this.costLabel2.AutoSize = true;
    this.costLabel2.Font = Gui.Options.FontBGM_BT;
    this.costLabel2.FontSize = 14;
    this.AddPanel((GUIPanel) this.costLabel2);
    this.requisitionButton = new GUIButtonNew();
    this.requisitionButton.Position = new float2(0.0f, 185f);
    this.requisitionButton.TextLabel.Text = "%$bgo.shop.requisition_pack%";
    this.requisitionButton.TextLabel.AutoSize = true;
    this.requisitionButton.TextLabel.Font = Gui.Options.FontBGM_BT;
    this.requisitionButton.TextLabel.FontSize = 14;
    this.requisitionButton.Width = this.requisitionButton.TextLabel.Width + 80f;
    this.requisitionButton.TextLabel.Width = this.requisitionButton.Width;
    this.requisitionButton.TextLabel.Alignment = TextAnchor.MiddleCenter;
    this.requisitionButton.Height += 10f;
    this.AddPanel((GUIPanel) this.requisitionButton);
    this.titleLabel = new GUILabelNew();
    this.titleLabel.Position = new float2(0.0f, -200f);
    this.titleLabel.Font = Gui.Options.FontBGM_BT;
    this.titleLabel.FontSize = 17;
    this.titleLabel.Width = 500f;
    this.titleLabel.Height = 35f;
    this.titleLabel.Alignment = TextAnchor.MiddleCenter;
    this.AddPanel((GUIPanel) this.titleLabel);
    this.descLabel = new GUILabelNew();
    this.descLabel.Position = new float2(0.0f, -161f);
    this.descLabel.Font = Gui.Options.FontBGM_BT;
    this.descLabel.FontSize = 14;
    this.descLabel.Width = 500f;
    this.descLabel.Height = 60f;
    this.descLabel.WordWrap = true;
    this.AddPanel((GUIPanel) this.descLabel);
    this.scrollBox = new GUIScrollBox(this.root.Rect.height - 10f, this.root);
    this.scrollBox.Position = new float2(261f, 10f);
    this.scrollBox.MaxRange = 0;
    this.scrollBox.ViewRange = this.slots.Length;
    this.scrollBox.Handler = new GUIScrollBox.Handlr(this.OnUpdateScroll);
    this.AddPanel((GUIPanel) this.scrollBox);
    GUIImageNew guiImageNew1 = new GUIImageNew((Texture2D) ResourceLoader.Load("GUI/EquipBuyPanel/StarterPack/SeperatorLine"), this.root);
    guiImageNew1.Position = new float2(0.0f, (float) (-(double) this.scrollBox.Height / 2.0 + 10.0));
    this.AddPanel((GUIPanel) guiImageNew1);
    GUIImageNew guiImageNew2 = new GUIImageNew((Texture2D) ResourceLoader.Load("GUI/EquipBuyPanel/StarterPack/SeperatorLine"), this.root);
    guiImageNew2.Position = guiImageNew1.Position;
    guiImageNew2.PositionY += (float) (double) this.scrollBox.Height;
    this.AddPanel((GUIPanel) guiImageNew2);
  }

  protected void OnUpdateScroll()
  {
    this.ViewFirst = this.scrollBox.Value;
  }

  public int GetSlotId(float2 fromPos)
  {
    for (int index = 0; index < this.slots.Length; ++index)
    {
      if (this.slots[index].Contains(fromPos))
        return index;
    }
    return -1;
  }

  public void UpdateData(Paperdolls.PaperdollLayoutBig.PaperdollLayoutBig papperDoll, StarterKit starterKit)
  {
    if (papperDoll != null && (!this.papperDollImage.IsRendered || this.oldTexture != papperDoll.BlueprintTexture))
    {
      this.oldTexture = papperDoll.BlueprintTexture;
      this.papperDollImage.IsRendered = true;
      this.papperDollImage.Texture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/" + papperDoll.BlueprintTexture);
      float num = (float) (this.papperDollImage.Texture.width / this.papperDollImage.Texture.height);
      this.papperDollImage.Height = this.scrollBox.Height - 20f;
      this.papperDollImage.Width = (float) ((double) num * ((double) this.papperDollImage.Height - 20.0) / 2.0);
      this.papperDollImage.InnerRect = new Rect(0.0f, 0.0f, 0.5f, 1f);
      this.papperDollImage.PositionX = -170f;
      this.RecalculateAbsCoords();
    }
    this.scrollBox.MaxRange = starterKit.Card.Items.Count;
    for (int index1 = 0; index1 < this.slots.Length; ++index1)
    {
      int index2 = index1 + this.viewFirst;
      if (index2 < starterKit.Card.Items.Count)
      {
        this.slots[index1].Show();
        this.slots[index1].UpdateData(starterKit.Card.Items[index2]);
      }
    }
    this.costLabel1.IsRendered = false;
    this.costImage1.IsRendered = false;
    this.costLabel2.IsRendered = false;
    this.costImage2.IsRendered = false;
    Price price = starterKit.ShopItemCard.BuyPrice;
    int num1 = 0;
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = price.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        if (num1 == 0)
        {
          this.costImage1.Texture = ResourceLoader.Load<Texture2D>(current.Key.GUICard.GUIIcon);
          this.costImage1.Size = new float2((float) this.costImage1.Texture.width, (float) this.costImage1.Texture.height);
          this.costLabel1.Text = current.Value.ToString("#0.0");
          this.costLabel1.IsRendered = true;
          this.costImage1.IsRendered = true;
        }
        else
        {
          this.costImage2.Texture = ResourceLoader.Load<Texture2D>(current.Key.GUICard.GUIIcon);
          this.costImage2.Size = new float2((float) this.costImage2.Texture.width, (float) this.costImage2.Texture.height);
          this.costLabel2.Text = current.Value.ToString("#0.0");
          this.costLabel2.IsRendered = true;
          this.costImage2.IsRendered = true;
        }
        ++num1;
      }
    }
    this.titleLabel.Text = starterKit.ItemGUICard.Name;
    this.descLabel.Text = starterKit.ItemGUICard.Description;
    this.costLabel1.MakeCenteredRect();
    this.costLabel2.MakeCenteredRect();
    this.costImage1.PlaceRightOf((GUIPanel) this.costLabel, 3f);
    this.costLabel1.PlaceRightOf((GUIPanel) this.costImage1, 3f);
    this.costImage2.PlaceRightOf((GUIPanel) this.costLabel1, 3f);
    this.costLabel2.PlaceRightOf((GUIPanel) this.costImage2, 3f);
  }
}
