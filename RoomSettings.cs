﻿// Decompiled with JetBrains decompiler
// Type: RoomSettings
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RoomSettings : MonoBehaviour
{
  public Color fogColor = Color.gray;
  public UnityEngine.FogMode fogMode = UnityEngine.FogMode.ExponentialSquared;
  public float fogDensity = 0.01f;
  public float linearFogEnd = 300f;
  public Color ambientLight = Color.black;
  public bool fog;
  public float linearFogStart;
  private bool showParticleFog;

  public bool ShowParticleFog
  {
    get
    {
      return this.showParticleFog;
    }
    set
    {
      this.showParticleFog = value;
      Transform child = this.transform.parent.FindChild("ParticleFX");
      if (!((Object) child != (Object) null))
        return;
      child.gameObject.SetActive(this.showParticleFog);
    }
  }

  public void Start()
  {
    this.showParticleFog = (bool) ((SettingsDataProvider) FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider")).CurrentSettings.Get(UserSetting.HighQualityParticles);
    Transform child = this.transform.parent.FindChild("ParticleFX");
    if ((Object) child != (Object) null)
      child.gameObject.SetActive(this.showParticleFog);
    RenderSettings.fog = this.fog;
    RenderSettings.fogColor = this.fogColor;
    RenderSettings.fogMode = this.fogMode;
    RenderSettings.fogStartDistance = this.linearFogStart;
    RenderSettings.fogEndDistance = this.linearFogEnd;
    RenderSettings.fogDensity = this.fogDensity;
    RenderSettings.ambientLight = this.ambientLight;
  }
}
