﻿// Decompiled with JetBrains decompiler
// Type: GuildInviteMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System;

public class GuildInviteMessage : IMessage<Message>
{
  public uint GuildId { get; private set; }

  public string GuildName { get; private set; }

  public uint InviterId { get; private set; }

  public Message Id
  {
    get
    {
      return Message.GuildInvite;
    }
  }

  public object Data
  {
    get
    {
      throw new NotImplementedException();
    }
    set
    {
      throw new NotImplementedException();
    }
  }

  public GuildInviteMessage(uint guildId, string guildName, uint inviterId)
  {
    this.GuildId = guildId;
    this.GuildName = guildName;
    this.InviterId = inviterId;
  }
}
