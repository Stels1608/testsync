﻿// Decompiled with JetBrains decompiler
// Type: UISoundVolume
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UISlider))]
[AddComponentMenu("NGUI/Interaction/Sound Volume")]
public class UISoundVolume : MonoBehaviour
{
  private UISlider mSlider;

  private void Awake()
  {
    this.mSlider = this.GetComponent<UISlider>();
    this.mSlider.value = NGUITools.soundVolume;
    EventDelegate.Add(this.mSlider.onChange, new EventDelegate.Callback(this.OnChange));
  }

  private void OnChange()
  {
    NGUITools.soundVolume = UIProgressBar.current.value;
  }
}
