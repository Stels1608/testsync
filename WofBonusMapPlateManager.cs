﻿// Decompiled with JetBrains decompiler
// Type: WofBonusMapPlateManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WofBonusMapPlateManager : MonoBehaviour
{
  public int rasterSize = 29;
  public int maxParts = 20;
  private List<GameObject> mapPlates = new List<GameObject>();
  private WofBonusMapLayout layout;
  public bool generate;
  public bool useDefaultMap;
  [SerializeField]
  private UISprite mapBg;

  public WofBonusMapLayout Layout
  {
    get
    {
      return this.layout;
    }
  }

  public void SetMapLayout(WofBonusMapLayout lay)
  {
    this.mapBg.spriteName = "bonus_" + (object) lay.BonusMapId;
    this.layout = lay;
    this.GenerateParts(lay.MapPositions);
  }

  public void GenerateParts(Dictionary<ushort, Vector2> mapPositions)
  {
    using (List<GameObject>.Enumerator enumerator = this.mapPlates.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current);
    }
    this.mapPlates.Clear();
    using (Dictionary<ushort, Vector2>.Enumerator enumerator = mapPositions.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ushort, Vector2> current = enumerator.Current;
        GameObject gameObject = NGUITools.AddChild(this.gameObject, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/Wof/WofMapPlate"));
        this.mapPlates.Add(gameObject);
        UISprite component = gameObject.GetComponent<UISprite>();
        component.width = this.rasterSize + 1;
        component.height = this.rasterSize + 1;
        gameObject.transform.localPosition = (Vector3) (current.Value * (float) this.rasterSize);
        gameObject.gameObject.name = ((int) current.Key).ToString() + "_plate";
      }
    }
  }

  public void UpdateVisibility(int mapIndex, List<int> mapParts)
  {
    if (this.layout == null || this.layout.BonusMapId != mapIndex)
      return;
    using (List<int>.Enumerator enumerator = mapParts.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.mapPlates[enumerator.Current - 1].SetActive(false);
    }
  }
}
