﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorsSpriteLinker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HudIndicatorsSpriteLinker : MonoBehaviour
{
  private const string LINKER_GO_PATH = "uGUI_Resources/UiPrefabs/HudIndicators/HudIndicatorsSpriteLinker";
  public UnityEngine.Sprite Brackets;
  public UnityEngine.Sprite MissileBrackets;
  public UnityEngine.Sprite HealthbarEnergy;
  public UnityEngine.Sprite Healthbar;
  public UnityEngine.Sprite MarkedTargetCenter;
  public UnityEngine.Sprite MarkedTargetTriangle;
  public UnityEngine.Sprite MultiTarget;
  public UnityEngine.Sprite Pointer;
  public UnityEngine.Sprite AsteroidResourceTitanium;
  public UnityEngine.Sprite AsteroidResourceWater;
  public UnityEngine.Sprite AsteroidResourceTylium;
  public UnityEngine.Sprite AsteroidResourceUranium;
  public UnityEngine.Sprite AsteroidResourcePlutonium;
  [SerializeField]
  private UnityEngine.Sprite Award_PvpArena_1st_Colonial;
  [SerializeField]
  private UnityEngine.Sprite Award_PvpArena_1st_Cylon;
  [SerializeField]
  private UnityEngine.Sprite Award_PvpArena_2nd_Colonial;
  [SerializeField]
  private UnityEngine.Sprite Award_PvpArena_2nd_Cylon;
  [SerializeField]
  private UnityEngine.Sprite Award_PvpArena_3rd_Colonial;
  [SerializeField]
  private UnityEngine.Sprite Award_PvpArena_3rd_Cylon;
  [SerializeField]
  private UnityEngine.Sprite Award_PvpArena_4th_to_20th_Colonial;
  [SerializeField]
  private UnityEngine.Sprite Award_PvpArena_4th_to_20th_Cylon;
  [SerializeField]
  private UnityEngine.Sprite Award_PvpArena_21st_to_100th_Colonial;
  [SerializeField]
  private UnityEngine.Sprite Award_PvpArena_21st_to_100th_Cylon;
  [SerializeField]
  private UnityEngine.Sprite Award_Tournament_Ace;
  [SerializeField]
  private UnityEngine.Sprite Award_Tournament_GoldMedal;
  [SerializeField]
  private UnityEngine.Sprite Award_Tournament_Nemesis;
  [SerializeField]
  private UnityEngine.Sprite Award_Tournament_SilverMedal;
  [SerializeField]
  private UnityEngine.Sprite Award_Tournament_Swords;
  [SerializeField]
  private UnityEngine.Sprite Award_Killer_Gold;
  [SerializeField]
  private UnityEngine.Sprite Award_Killer_Silver;
  [SerializeField]
  private UnityEngine.Sprite Award_Killer_Bronze;
  [SerializeField]
  private UnityEngine.Sprite Award_Assist_Gold;
  [SerializeField]
  private UnityEngine.Sprite Award_Assist_Silver;
  [SerializeField]
  private UnityEngine.Sprite Award_Assist_Bronze;
  private static HudIndicatorsSpriteLinker _instance;

  public static HudIndicatorsSpriteLinker Instance
  {
    get
    {
      if ((Object) HudIndicatorsSpriteLinker._instance == (Object) null)
      {
        GameObject original = Resources.Load("uGUI_Resources/UiPrefabs/HudIndicators/HudIndicatorsSpriteLinker", typeof (GameObject)) as GameObject;
        if ((Object) original == (Object) null)
        {
          Debug.LogError((object) "Linker not found: uGUI_Resources/UiPrefabs/HudIndicators/HudIndicatorsSpriteLinker");
          return (HudIndicatorsSpriteLinker) null;
        }
        HudIndicatorsSpriteLinker._instance = Object.Instantiate<GameObject>(original).GetComponent<HudIndicatorsSpriteLinker>();
        if ((Object) HudIndicatorsSpriteLinker._instance == (Object) null)
        {
          Debug.LogError((object) "Linker lacks the required component: uGUI_Resources/UiPrefabs/HudIndicators/HudIndicatorsSpriteLinker");
          return (HudIndicatorsSpriteLinker) null;
        }
      }
      return HudIndicatorsSpriteLinker._instance;
    }
  }

  public UnityEngine.Sprite GetTournamentNemesis
  {
    get
    {
      return this.Award_Tournament_Nemesis;
    }
  }

  public UnityEngine.Sprite GetPvpMedal(PvpMedal pvpMedal, Faction faction)
  {
    switch (pvpMedal)
    {
      case PvpMedal.PvpArena1st:
        if (faction == Faction.Colonial)
          return this.Award_PvpArena_1st_Colonial;
        return this.Award_PvpArena_1st_Cylon;
      case PvpMedal.PvpArena2nd:
        if (faction == Faction.Colonial)
          return this.Award_PvpArena_2nd_Colonial;
        return this.Award_PvpArena_2nd_Cylon;
      case PvpMedal.PvpArena3rd:
        if (faction == Faction.Colonial)
          return this.Award_PvpArena_3rd_Colonial;
        return this.Award_PvpArena_3rd_Cylon;
      case PvpMedal.PvpArena4thTo20th:
        if (faction == Faction.Colonial)
          return this.Award_PvpArena_4th_to_20th_Colonial;
        return this.Award_PvpArena_4th_to_20th_Cylon;
      case PvpMedal.PvpArena21stTo100th:
        if (faction == Faction.Colonial)
          return this.Award_PvpArena_21st_to_100th_Colonial;
        return this.Award_PvpArena_21st_to_100th_Cylon;
      default:
        return (UnityEngine.Sprite) null;
    }
  }

  public UnityEngine.Sprite GetTournamentMedal(TournamentMedal tournamentMedal)
  {
    switch (tournamentMedal)
    {
      case TournamentMedal.GoldMedal:
        return this.Award_Tournament_GoldMedal;
      case TournamentMedal.SilverMedal:
        return this.Award_Tournament_SilverMedal;
      default:
        return (UnityEngine.Sprite) null;
    }
  }

  public UnityEngine.Sprite GetTournamentIndicator(TournamentIndicator tournamentIndicator)
  {
    switch (tournamentIndicator)
    {
      case TournamentIndicator.TopFive:
        return this.Award_Tournament_Swords;
      case TournamentIndicator.KillingSpree:
        return this.Award_Tournament_Ace;
      default:
        return (UnityEngine.Sprite) null;
    }
  }

  public UnityEngine.Sprite GetKillerMedal(KillerMedal killerMedal)
  {
    switch (killerMedal)
    {
      case KillerMedal.GoldMedal:
        return this.Award_Killer_Gold;
      case KillerMedal.SilverMedal:
        return this.Award_Killer_Silver;
      case KillerMedal.BronzeMedal:
        return this.Award_Killer_Bronze;
      default:
        return (UnityEngine.Sprite) null;
    }
  }

  public UnityEngine.Sprite GetAssistMedal(AssistMedal assistMedal)
  {
    switch (assistMedal)
    {
      case AssistMedal.GoldMedal:
        return this.Award_Assist_Gold;
      case AssistMedal.SilverMedal:
        return this.Award_Assist_Silver;
      case AssistMedal.BronzeMedal:
        return this.Award_Assist_Bronze;
      default:
        return (UnityEngine.Sprite) null;
    }
  }
}
