﻿// Decompiled with JetBrains decompiler
// Type: ItemList
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class ItemList : SmartList<ShipItem>, IContainer
{
  private IContainerID containerID;
  private uint tylium;
  private uint water;
  private uint cubits;
  private uint titanium;
  private uint token;

  public IContainerID ContainerID
  {
    get
    {
      return this.containerID;
    }
  }

  public uint Tylium
  {
    get
    {
      return this.tylium;
    }
  }

  public uint Water
  {
    get
    {
      return this.water;
    }
  }

  public uint Titanium
  {
    get
    {
      return this.titanium;
    }
  }

  public uint Cubits
  {
    get
    {
      return this.cubits;
    }
  }

  public uint Token
  {
    get
    {
      return this.token;
    }
  }

  public ItemList Consumables
  {
    get
    {
      return this.Filter((Predicate<ShipItem>) (item => item is ItemCountable));
    }
  }

  public ItemList Systems
  {
    get
    {
      return this.Filter((Predicate<ShipItem>) (item => item is ShipSystem));
    }
  }

  public int ContentHash
  {
    get
    {
      int num = this.Count;
      foreach (ShipItem shipItem in (IEnumerable<ShipItem>) this)
        num = num * 31 + (int) shipItem.ServerID;
      return num;
    }
  }

  public ItemList(IContainerID containerID)
  {
    this.containerID = containerID;
  }

  public ItemList(IContainerID containerID, List<ShipItem> items)
    : base(items)
  {
    this.containerID = containerID;
  }

  public ShipItem GetByGUID(uint GUID)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.Find(new Predicate<ShipItem>(new ItemList.\u003CGetByGUID\u003Ec__AnonStorey51() { GUID = GUID }.\u003C\u003Em__D));
  }

  public ItemList Filter(Predicate<ShipItem> match)
  {
    return new ItemList(this.ContainerID, this.FilterList(match));
  }

  public uint GetCountByGUID(uint GUID)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.FoldList<uint>(new SmartList<ShipItem>.FoldFun<uint>(new ItemList.\u003CGetCountByGUID\u003Ec__AnonStorey52() { GUID = GUID }.\u003C\u003Em__E), 0U);
  }

  protected ItemCountable GetAutoUseItem()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ItemList.\u003CGetAutoUseItem\u003Ec__AnonStorey54 itemCAnonStorey54 = new ItemList.\u003CGetAutoUseItem\u003Ec__AnonStorey54();
    // ISSUE: reference to a compiler-generated field
    itemCAnonStorey54.autoUseItem = (ItemCountable) null;
    foreach (ShipItem shipItem in (IEnumerable<ShipItem>) this)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      ItemList.\u003CGetAutoUseItem\u003Ec__AnonStorey53 itemCAnonStorey53 = new ItemList.\u003CGetAutoUseItem\u003Ec__AnonStorey53();
      // ISSUE: reference to a compiler-generated field
      itemCAnonStorey53.\u003C\u003Ef__ref\u002484 = itemCAnonStorey54;
      if (shipItem is ItemCountable)
      {
        // ISSUE: reference to a compiler-generated field
        if (itemCAnonStorey54.autoUseItem != null)
        {
          // ISSUE: reference to a compiler-generated field
          return itemCAnonStorey54.autoUseItem;
        }
        // ISSUE: reference to a compiler-generated field
        itemCAnonStorey53.consumable = (ItemCountable) shipItem;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        itemCAnonStorey53.consumable.Card.IsLoaded.AddHandler(new SignalHandler(itemCAnonStorey53.\u003C\u003Em__F));
      }
    }
    // ISSUE: reference to a compiler-generated field
    return itemCAnonStorey54.autoUseItem;
  }

  protected void AutoUse()
  {
    ItemCountable autoUseItem = this.GetAutoUseItem();
    if (autoUseItem == null)
      return;
    autoUseItem.Use();
  }

  public ItemList FilterStarterKits()
  {
    return this.Filter((Predicate<ShipItem>) (item =>
    {
      StarterKit starterKit = item as StarterKit;
      return starterKit == null || (bool) starterKit.IsLoaded && (int) Game.Me.ActiveShip.GUID == (int) ((StarterKit) item).Card.shipCard.CardGUID;
    }));
  }

  public ItemList FilterSystemsByShipRestrictions(uint shipObjectKey, ShipRole[] shipRoles)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.Filter(new Predicate<ShipItem>(new ItemList.\u003CFilterSystemsByShipRestrictions\u003Ec__AnonStorey55() { shipObjectKey = shipObjectKey, shipRoles = shipRoles }.\u003C\u003Em__11));
  }

  public ItemList FilterByTier(byte tier)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ItemList.\u003CFilterByTier\u003Ec__AnonStorey56 tierCAnonStorey56 = new ItemList.\u003CFilterByTier\u003Ec__AnonStorey56();
    // ISSUE: reference to a compiler-generated field
    tierCAnonStorey56.tier = tier;
    // ISSUE: reference to a compiler-generated field
    if ((int) tierCAnonStorey56.tier == 0)
      return this;
    // ISSUE: reference to a compiler-generated method
    return this.Filter(new Predicate<ShipItem>(tierCAnonStorey56.\u003C\u003Em__12));
  }

  public ItemList FilterBySlotType(ShipSlotType slotType)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.Filter(new Predicate<ShipItem>(new ItemList.\u003CFilterBySlotType\u003Ec__AnonStorey57() { slotType = slotType }.\u003C\u003Em__13));
  }

  protected override void Updated(ShipItem newItem, ShipItem oldItem)
  {
    newItem.Container = (IContainer) this;
    switch ((ResourceType) newItem.CardGUID)
    {
      case ResourceType.Water:
        this.water = (newItem as ItemCountable).Count;
        break;
      case ResourceType.Token:
        this.token = (newItem as ItemCountable).Count;
        break;
      case ResourceType.Titanium:
        this.titanium = (newItem as ItemCountable).Count;
        break;
      case ResourceType.Tylium:
        this.tylium = (newItem as ItemCountable).Count;
        break;
      case ResourceType.Cubits:
        this.cubits = (newItem as ItemCountable).Count;
        if (this.ContainerID.GetContainerType() == ContainerType.Hold)
        {
          FacadeFactory.GetInstance().SendMessage(Message.PlayerCubitsHold, (object) this.cubits);
          break;
        }
        if (this.ContainerID.GetContainerType() != ContainerType.Locker)
          break;
        FacadeFactory.GetInstance().SendMessage(Message.PlayerCubitsLocker, (object) this.cubits);
        break;
    }
  }

  protected override void Removed(ShipItem item)
  {
    switch ((ResourceType) item.CardGUID)
    {
      case ResourceType.Water:
        this.water = 0U;
        break;
      case ResourceType.Token:
        this.token = 0U;
        break;
      case ResourceType.Titanium:
        this.titanium = 0U;
        break;
      case ResourceType.Tylium:
        this.tylium = 0U;
        break;
      case ResourceType.Cubits:
        this.cubits = 0U;
        break;
    }
  }

  public override void _Clear()
  {
    base._Clear();
    this.tylium = 0U;
    this.titanium = 0U;
    this.water = 0U;
    this.cubits = 0U;
  }

  protected override void Changed()
  {
    Flag.Schedule<ShipItem>(new SignalHandler(this.items.Sort), (IEnumerable<ShipItem>) this.items);
  }

  public void MoveAll(IContainer target)
  {
    PlayerProtocol.GetProtocol().MoveAll(this.ContainerID, target != null ? target.ContainerID : (IContainerID) new BlackHoleContainerID());
  }

  public List<GameItemCard> ConvertToGameItems(int size = -1)
  {
    if (size < 0)
      size = this.Count;
    List<GameItemCard> gameItemCardList = new List<GameItemCard>();
    for (int index = 0; index < size; ++index)
    {
      if (index < this.Count)
        gameItemCardList.Add((GameItemCard) this[index]);
      else
        gameItemCardList.Add((GameItemCard) null);
    }
    return gameItemCardList;
  }

  ShipItem IContainer.GetByID(ushort ID)
  {
    return this.GetByID(ID);
  }
}
