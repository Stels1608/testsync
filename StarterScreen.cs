﻿// Decompiled with JetBrains decompiler
// Type: StarterScreen
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StarterScreen : MonoBehaviour
{
  private FactionSelectionUi factionSelectionUi;

  public StarterLevelProfile Profile { get; set; }

  private void Start()
  {
    this.factionSelectionUi = this.InstantiateFactionSelectionUi();
    this.factionSelectionUi.SetBonuses(this.Profile);
  }

  private void OnDestroy()
  {
    if (!((Object) this.factionSelectionUi != (Object) null))
      return;
    Object.Destroy((Object) this.factionSelectionUi.gameObject);
  }

  private FactionSelectionUi InstantiateFactionSelectionUi()
  {
    return UguiTools.CreateChild<FactionSelectionUi>("FactionSelectionUgui", UguiTools.GetCanvas(BsgoCanvas.Windows).transform);
  }
}
