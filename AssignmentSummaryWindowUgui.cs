﻿// Decompiled with JetBrains decompiler
// Type: AssignmentSummaryWindowUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class AssignmentSummaryWindowUgui : MonoBehaviour
{
  private readonly List<AssignmentEntryUgui> assignmentsEntries = new List<AssignmentEntryUgui>();
  private bool _isRendered = true;
  [SerializeField]
  private UguiCollapseButtonImageSwitcher collapseImageSwitcher;
  [SerializeField]
  private AssignmentEntryUgui assignmentEntryPrototype;
  public AssignmentObjectiveEntryUgui headerObjectiveEntry;
  public GameObject contentRoot;
  private Mission trackedMission;
  private bool _collapsed;

  public bool Collapsed
  {
    get
    {
      return this._collapsed;
    }
    set
    {
      this._collapsed = value;
      this.collapseImageSwitcher.OnCollapseEvent(this._collapsed);
      this.contentRoot.SetActive(!this._collapsed);
    }
  }

  public bool IsRendered
  {
    get
    {
      return this._isRendered;
    }
    set
    {
      this._isRendered = value;
      this.gameObject.SetActive(this._isRendered);
    }
  }

  private void Awake()
  {
    this.assignmentEntryPrototype.IsRendered = false;
  }

  private void Start()
  {
    this.Collapsed = true;
  }

  public void ToggleCollapse()
  {
    this.Collapsed = !this.Collapsed;
  }

  public void SetMissions(MissionList missionList)
  {
    if (missionList.Count == 0)
    {
      this.Collapsed = true;
      this.trackedMission = (Mission) null;
      this.SetHeaderTexts();
    }
    while (this.assignmentsEntries.Count < missionList.Count)
    {
      AssignmentEntryUgui child = UguiTools.CreateChild<AssignmentEntryUgui>(this.assignmentEntryPrototype.gameObject, this.assignmentEntryPrototype.transform.parent);
      child.gameObject.name = "(G) AssignmentEntry (Instance)";
      child.IsRendered = true;
      this.assignmentsEntries.Add(child);
    }
    while (this.assignmentsEntries.Count > missionList.Count)
    {
      Object.Destroy((Object) this.assignmentsEntries[0].gameObject);
      this.assignmentsEntries.RemoveAt(0);
    }
    for (int index = 0; index < missionList.Count; ++index)
      this.assignmentsEntries[index].SetMission(missionList[index]);
    if (missionList.Count <= 0)
      return;
    if (this.trackedMission == null || missionList.GetByID(this.trackedMission.ServerID) == null)
      this.TrackMission(missionList[0]);
    else
      this.TrackMission(missionList.GetByID(this.trackedMission.ServerID));
  }

  public void TrackMission(Mission mission)
  {
    this.trackedMission = mission;
    using (List<AssignmentEntryUgui>.Enumerator enumerator = this.assignmentsEntries.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AssignmentEntryUgui current = enumerator.Current;
        current.Selected = current.Mission == mission;
      }
    }
    this.SetHeaderTexts();
  }

  public void SetHeaderTexts()
  {
    if (this.trackedMission != null)
    {
      this.headerObjectiveEntry.SetMission(this.trackedMission);
      if (this.trackedMission.countables.Count > 0)
        this.headerObjectiveEntry.SetObjective(this.trackedMission.countables[0], this.trackedMission);
      this.headerObjectiveEntry.nameLabel.text = this.headerObjectiveEntry.nameLabel.text.ToUpper();
    }
    else
    {
      this.headerObjectiveEntry.EnableProgress(false);
      this.headerObjectiveEntry.nameLabel.text = BsgoLocalization.Get("%$bgo.missions.no_assignments%").ToUpper();
    }
  }
}
