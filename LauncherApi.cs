﻿// Decompiled with JetBrains decompiler
// Type: LauncherApi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Net.Sockets;
using UnityEngine;

public class LauncherApi
{
  private const int PORT = 57881;

  public static void CallJavascript(string js)
  {
    Debug.Log((object) ("Call JS: " + js));
    LauncherApi.FocusMode focusMode = LauncherApi.FocusMode.Background;
    if (js.Contains("PaymentSession") || js.Contains("CampaignSession"))
      focusMode = LauncherApi.FocusMode.FocusLauncher;
    using (UdpClient udpClient = new UdpClient())
    {
      udpClient.Connect("localhost", 57881);
      MemoryStream memoryStream = new MemoryStream();
      BinaryWriter binaryWriter = new BinaryWriter((Stream) memoryStream);
      binaryWriter.Write((byte) focusMode);
      binaryWriter.Write((byte) 0);
      binaryWriter.Write(js);
      byte[] array = memoryStream.ToArray();
      udpClient.Send(array, array.Length);
    }
  }

  private enum IpcMessage : byte
  {
    JavascriptCall,
  }

  private enum FocusMode : byte
  {
    Background,
    FocusLauncher,
  }
}
