﻿// Decompiled with JetBrains decompiler
// Type: GalaxyMapSectorsLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GalaxyMapSectorsLayout : GuiPanel
{
  private readonly GuiImage warp = new GuiImage("GUI/GalaxyMap/warp_limit");
  private readonly GuiImage playerMark = new GuiImage("GUI/GalaxyMap/player_mark");
  private readonly GuiImage sectorMarker = new GuiImage("GUI/GalaxyMap/selection_marker");
  public readonly GalaxyMapInfoPanel Info = new GalaxyMapInfoPanel();
  private readonly GuiLine line = new GuiLine(Color.red);
  private GalaxyMapSector selected;

  public GalaxyMapSector SelectedGalaxyMapSector
  {
    get
    {
      return this.selected;
    }
  }

  public GalaxyMapSector MyGalaxyMapSector
  {
    get
    {
      if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null)
        return this.FindSector(SpaceLevel.GetLevel().ServerID);
      if ((UnityEngine.Object) RoomLevel.GetLevel() != (UnityEngine.Object) null)
        return this.FindSector(RoomLevel.GetLevel().ServerId);
      return (GalaxyMapSector) null;
    }
  }

  private float WarpRadius
  {
    get
    {
      return Mathf.Min(Game.Me.FTLTyliumRange, Game.Me.Stats.FTLRange);
    }
  }

  public GalaxyMapSectorsLayout()
  {
    this.MouseTransparent = true;
    this.sectorMarker.Name = "sectorMarker";
    this.AddChild((GuiElementBase) this.warp, Align.MiddleCenter, false);
    this.AddChild((GuiElementBase) this.line, Align.MiddleCenter, false);
    this.AddChild((GuiElementBase) this.playerMark, Align.MiddleCenter, false);
    this.AddChild((GuiElementBase) this.sectorMarker, Align.MiddleCenter, false);
    this.AddChild((GuiElementBase) this.Info, Align.MiddleCenter, false);
    this.PeriodicUpdate();
  }

  public override void PeriodicUpdate()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GalaxyMapSectorsLayout.\u003CPeriodicUpdate\u003Ec__AnonStoreyA6 updateCAnonStoreyA6 = new GalaxyMapSectorsLayout.\u003CPeriodicUpdate\u003Ec__AnonStoreyA6();
    // ISSUE: reference to a compiler-generated field
    updateCAnonStoreyA6.\u003C\u003Ef__this = this;
    base.PeriodicUpdate();
    if (!(bool) StaticCards.GalaxyMap.IsLoaded)
      return;
    foreach (SectorDesc star in Game.Galaxy.GetStars())
    {
      GalaxyMapSector sector = this.FindSector(star.Id);
      if (sector == null)
      {
        GalaxyMapSector galaxyMapSector = new GalaxyMapSector(star);
        galaxyMapSector.OnClickAction = new System.Action<GuiElementBase>(this.SelectSector);
        this.AddChild((GuiElementBase) galaxyMapSector, Align.MiddleCenter);
      }
      else
        sector.Desc = star;
    }
    // ISSUE: reference to a compiler-generated field
    updateCAnonStoreyA6.myGalaxyMapSector = this.MyGalaxyMapSector;
    // ISSUE: reference to a compiler-generated field
    bool flag = updateCAnonStoreyA6.myGalaxyMapSector != null;
    this.warp.IsRendered = flag;
    this.playerMark.IsRendered = flag;
    // ISSUE: reference to a compiler-generated method
    this.playerMark.OnClick = new AnonymousDelegate(updateCAnonStoreyA6.\u003C\u003Em__163);
    if (flag)
    {
      // ISSUE: reference to a compiler-generated field
      this.warp.Position = updateCAnonStoreyA6.myGalaxyMapSector.Position;
      this.warp.Size = new Vector2(1f, 1f) * this.WarpRadius * 2f;
      // ISSUE: reference to a compiler-generated field
      this.playerMark.Position = updateCAnonStoreyA6.myGalaxyMapSector.Position;
    }
    this.BringToFront((GuiElementBase) this.playerMark);
    this.BringToFront((GuiElementBase) this.Info);
    this.SelectSector((GuiElementBase) this.SelectedGalaxyMapSector);
  }

  public GalaxyMapSector FindSector(uint id)
  {
    foreach (GuiElementBase child in this.Children)
    {
      if (child is GalaxyMapSector && (int) (child as GalaxyMapSector).Desc.Id == (int) id)
        return child as GalaxyMapSector;
    }
    return (GalaxyMapSector) null;
  }

  public void SelectSector(GuiElementBase el)
  {
    this.selected = el as GalaxyMapSector;
    bool flag = el != null;
    this.Info.IsRendered = flag;
    this.sectorMarker.IsRendered = flag;
    this.line.IsRendered = flag;
    if (!flag)
      return;
    GalaxyMapSector myGalaxyMapSector = this.MyGalaxyMapSector;
    if (myGalaxyMapSector == null)
      return;
    this.sectorMarker.Position = this.selected.Position;
    this.Info.SetInfo(this.selected, myGalaxyMapSector);
    this.Info.Position = this.selected.Position + new Vector2(15f, 0.0f) + this.Info.Size / 2f;
    if ((double) this.Info.Rect.xMax > (double) Screen.width)
      this.Info.PositionX -= (float) ((double) this.Info.Rect.xMax - (double) Screen.width);
    if ((double) this.Info.Rect.yMax > (double) Screen.height)
      this.Info.PositionY -= (float) ((double) this.Info.Rect.yMax - (double) Screen.height);
    this.line.Size = this.selected.Position - myGalaxyMapSector.Position;
    this.line.Position = myGalaxyMapSector.Position + this.line.Size / 2f;
    this.line.IsRendered = this.selected.CanJump(myGalaxyMapSector);
  }
}
