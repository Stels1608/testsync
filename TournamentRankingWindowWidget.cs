﻿// Decompiled with JetBrains decompiler
// Type: TournamentRankingWindowWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class TournamentRankingWindowWidget : ContentWindowWidget
{
  private readonly Dictionary<ushort, RankEntry> cachedRanks = new Dictionary<ushort, RankEntry>();
  public const ushort LIMIT = 14;
  public const ushort LEADING_POSITIONS = 5;
  [SerializeField]
  private UILabel headerRankLabel;
  [SerializeField]
  private UILabel headerNameLabel;
  [SerializeField]
  private UILabel headerScore;
  [SerializeField]
  private UILabel headerKillsLabel;
  [SerializeField]
  private UILabel gapLabel;
  [SerializeField]
  private Transform contentArea;
  private RankEntryWidget[] widgets;

  public ushort Offset { get; private set; }

  public ushort OwnRank
  {
    set
    {
      this.Offset = (ushort) Mathf.Max((int) value - 7, 1);
    }
  }

  private RankEntryWidget[] Widgets
  {
    get
    {
      return this.widgets ?? (this.widgets = this.contentArea.GetComponentsInChildren<RankEntryWidget>(true));
    }
  }

  public TournamentRankingWindowWidget()
  {
    this.type = WindowTypes.TournamentRanking;
    this.Offset = (ushort) 1;
  }

  public void ShowRanking(List<RankEntry> ranking)
  {
    using (List<RankEntry>.Enumerator enumerator = ranking.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        RankEntry current = enumerator.Current;
        this.cachedRanks[current.Index] = current;
      }
    }
    ushort num = (ushort) Mathf.Max((int) this.Offset, 7);
    this.gapLabel.gameObject.SetActive(false);
    for (ushort index = 0; (int) index < this.Widgets.Length; ++index)
    {
      ushort key = (int) index <= 5 ? (ushort) ((int) index + 1) : num++;
      if ((int) index == 5 && (int) this.Offset > 5)
      {
        this.widgets[(int) index].gameObject.SetActive(false);
        this.gapLabel.gameObject.SetActive(true);
      }
      else
      {
        RankEntry rankEntry = !this.cachedRanks.ContainsKey(key) ? (RankEntry) null : this.cachedRanks[key];
        if (rankEntry == null)
        {
          this.widgets[(int) index].gameObject.SetActive(false);
        }
        else
        {
          this.widgets[(int) index].gameObject.SetActive(true);
          this.widgets[(int) index].SetData(rankEntry);
          this.widgets[(int) index].Colorize((int) Game.Me.ServerID == (int) rankEntry.PlayerId);
        }
      }
    }
  }

  public override void OnWindowClose()
  {
    base.OnWindowClose();
    this.cachedRanks.Clear();
  }

  public override void ReloadLanguageData()
  {
    base.ReloadLanguageData();
    this.headerRankLabel.text = BsgoLocalization.Get("bgo.highscore.rank");
    this.headerNameLabel.text = BsgoLocalization.Get("bgo.highscore.name");
    this.headerScore.text = BsgoLocalization.Get("bgo.highscore.score");
    this.headerKillsLabel.text = BsgoLocalization.Get("bgo.highscore.kills_deaths");
    this.TitleBar.TitleLabel.text = BsgoLocalization.Get("bgo.highscore.group_tournament").ToUpperInvariant();
  }
}
