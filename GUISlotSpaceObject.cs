﻿// Decompiled with JetBrains decompiler
// Type: GUISlotSpaceObject
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class GUISlotSpaceObject : GUISlotWithBuffs
{
  private SpaceObject spaceObject;

  public SpaceObject SpaceObject
  {
    get
    {
      return this.spaceObject;
    }
    set
    {
      this.spaceObject = value;
      if (this.spaceObject == null)
        return;
      this.NameString = this.spaceObject.Name;
      this.redStrip.IsRendered = (this.spaceObject is Loot || this.spaceObject is DebrisPile || this.spaceObject is Planetoid ? 1 : (this.spaceObject is Planet ? 1 : 0)) == 0;
      this.healthLevel.IsRendered = GUISlot.ShowStats && !this.spaceObject.HideStats;
      this.blueStrip.IsRendered = (this.spaceObject is Loot || this.spaceObject is DebrisPile || this.spaceObject is Asteroid ? 1 : (this.spaceObject is Planet ? 1 : 0)) == 0;
      this.energyLevel.IsRendered = GUISlot.ShowStats && !this.spaceObject.HideStats;
    }
  }

  public GUISlotSpaceObject(SmartRect parent)
    : base(parent)
  {
    this.leaderMark.IsRendered = false;
  }

  public override void Update()
  {
    base.Update();
    if (this.SpaceObject != null)
    {
      this.RedLevel = this.spaceObject.Props.HullPoints / this.spaceObject.Props.MaxHullPoints;
      this.BlueLevel = this.spaceObject.Props.PowerPoints / this.spaceObject.Props.MaxPowerPoints;
      this.distance.Text = ((int) Mathf.Sqrt(this.SpaceObject.GetSqrDistance())).ToString();
    }
    this.timeToUpdateBuffs -= (float) (double) Time.deltaTime;
    if ((double) this.timeToUpdateBuffs <= 0.0)
    {
      this.LevelString = string.Empty;
      this.LevelColor(1f);
      if (this.healthLevel.IsRendered)
      {
        if (this.spaceObject.HideStats)
          this.SetHealthString(((int) (float) ((double) this.spaceObject.Props.HullPoints / (double) this.spaceObject.Props.MaxHullPoints * 100.0)).ToString() + "%");
        else
          this.SetHealthString((int) this.spaceObject.Props.HullPoints, (int) this.spaceObject.Props.MaxHullPoints);
      }
      if (this.energyLevel.IsRendered)
      {
        if (this.spaceObject.HideStats)
          this.SetEnergyString(((double) this.spaceObject.Props.MaxPowerPoints <= 0.0 ? 0 : (int) (float) ((double) this.spaceObject.Props.PowerPoints / (double) this.spaceObject.Props.MaxPowerPoints * 100.0)).ToString() + "%");
        else
          this.SetEnergyString((int) this.spaceObject.Props.PowerPoints, (int) this.spaceObject.Props.MaxPowerPoints);
      }
      this.buffs.Clear();
      Ship ship = this.SpaceObject as Ship;
      if (ship == null)
        return;
      int num = 0;
      using (List<ShipBuff>.Enumerator enumerator = ship.Props.DisplayedShipBuffs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ShipBuff current = enumerator.Current;
          if ((bool) current.IsLoaded && (double) current.TimeLeft > 0.0)
          {
            this.buffs.Add(current);
            ++num;
          }
          if (num >= 10)
            break;
        }
      }
      this.NameString = this.spaceObject.Name;
      if (ship.HasLevel)
      {
        this.LevelString = !Game.Me.TournamentParticipant ? ship.Level.ToString() : string.Empty;
        this.LevelColor((float) ship.Level / (float) Game.Me.Level);
      }
      this.timeToUpdateBuffs = 0.1f;
    }
    this.drawBuffs = this.buffs.Count > 0;
    Game.Me.Friends.Update();
    Game.Me.Party.Update();
    Game.Guilds.Update();
  }

  protected override void UpdateGUISlotAvatar(GUISlotAvatar avatar)
  {
    avatar.Texture = this.spaceObject.Avatar;
    avatar.IsRendered = true;
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    this.avatarClicked = false;
    if (mouseKey == KeyCode.Mouse1 && this.avatar.Contains(mousePosition))
      this.avatarClicked = true;
    return this.avatarClicked;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (!this.avatarClicked || !this.avatar.Contains(mousePosition) || (mouseKey != KeyCode.Mouse1 || Game.Me.TournamentParticipant))
      return false;
    GUISlotPopup guiSlotPopup = Game.GUIManager.Find<GUISlotManager>().Popup;
    guiSlotPopup.Clear();
    if (this.spaceObject is Ship && this.spaceObject.IsPlayer && (this.SpaceObject as PlayerShip).Player != null)
    {
      guiSlotPopup.Player = (this.SpaceObject as PlayerShip).Player;
      if (this.spaceObject.PlayerRelation == Relation.Enemy || this.spaceObject.PlayerRelation == Relation.Friend)
        guiSlotPopup.AddPlayerInfo();
      if (!Game.Me.ActiveShip.IsCapitalShip)
        guiSlotPopup.AddDuel();
      if (this.spaceObject.PlayerRelation == Relation.Friend)
      {
        guiSlotPopup.AppendPartyMenu();
        guiSlotPopup.ObjectID = this.spaceObject.ObjectID;
        guiSlotPopup.AddFollow();
      }
    }
    if (guiSlotPopup.Children.Count > 0)
    {
      guiSlotPopup.Parent = this.avatar.SmartRect;
      guiSlotPopup.PlaceRightDownCornerOf((GUIPanel) this.avatar, -20f);
      guiSlotPopup.IsRendered = true;
    }
    return true;
  }

  public override bool OnShowTooltip(float2 mousePosition)
  {
    if (base.OnShowTooltip(mousePosition))
      return true;
    if (!this.name.Contains(mousePosition) || !this.nameWasCut)
      return false;
    this.name.SetTooltip(this.spaceObject.Name);
    Game.TooltipManager.ShowTooltip(this.name.AdvancedTooltip);
    return true;
  }
}
