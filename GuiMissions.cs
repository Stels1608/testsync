﻿// Decompiled with JetBrains decompiler
// Type: GuiMissions
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.CharacterStatus.Missions;
using Gui.Core;
using UnityEngine;

public class GuiMissions : GuiDialog
{
  private readonly GuiMissionList missionList = new GuiMissionList();
  private readonly GuiRadioGroup radioGroup = new GuiRadioGroup();
  private readonly GuiMissionDetails description = new GuiMissionDetails();
  private readonly GuiLabel errorMsg = new GuiLabel(Gui.Options.FontBGM_BT, 16);
  private readonly GuiButton completeAllButton;

  public GuiMissions()
  {
    this.MouseTransparent = true;
    this.Size = new Vector2(925f, 443f);
    this.Position = new Vector2(0.0f, 0.0f);
    this.AddChild((GuiElementBase) this.description, new Vector2(391f, 20f));
    this.missionList.Size = new Vector2(387f, 440f);
    this.missionList.OnSelectionChanged = (System.Action<ISelectable>) (el =>
    {
      this.description.IsRendered = el != null;
      this.errorMsg.IsRendered = !this.description.IsRendered;
      if (el == null)
        return;
      this.description.TakeInfoFrom((el as MissionItem).GetItem());
    });
    this.AddChild((GuiElementBase) this.missionList, new Vector2(3f, 3f));
    this.radioGroup.AddTab("%$bgo.missions.all%", new AnonymousDelegate(this.SelectAll));
    this.radioGroup.AddTab("%$bgo.missions.in_progress%", new AnonymousDelegate(this.SelectInProgress));
    this.radioGroup.AddTab("%$bgo.missions.completed%", new AnonymousDelegate(this.SelectCompleted));
    this.AddChild((GuiElementBase) this.radioGroup, Align.DownLeft, new Vector2(30f, 16f));
    this.errorMsg.WordWrap = true;
    this.errorMsg.Alignment = TextAnchor.UpperCenter;
    this.errorMsg.SizeX = 500f;
    this.errorMsg.IsRendered = false;
    this.AddChild((GuiElementBase) this.errorMsg, new Vector2(400f, 199f));
    this.radioGroup.SelectButtonWithText("%$bgo.missions.all%");
    this.completeAllButton = new GuiButton("%$bgo.missions.hand_in_all%");
    this.completeAllButton.Size = new Vector2(120f, 20f);
    this.completeAllButton.Pressed = (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage(Message.MissionHandInAll));
    this.AddChild((GuiElementBase) this.completeAllButton, Align.DownLeft, new Vector2((float) ((double) this.SizeX - (double) this.completeAllButton.SizeX - 24.0), 20f));
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    if (this.missionList.Selected == null)
      return;
    this.description.TakeInfoFrom((this.missionList.Selected as MissionItem).GetItem());
  }

  public void UpdateMissionList(MissionList missions)
  {
    this.missionList.UpdateMissionList(missions);
  }

  public void SelectItem(ushort missionId)
  {
    this.missionList.SelectMission(missionId);
  }

  public void SelectAll()
  {
    this.missionList.SelectAll();
    this.CheckList("%$bgo.missions.no_assignments%");
  }

  public void SelectInProgress()
  {
    this.missionList.SelectInProgress();
    this.CheckList("%$bgo.missions.no_assignments_in_progress%");
  }

  public void SelectCompleted()
  {
    this.missionList.SelectCompleted();
    this.CheckList("%$bgo.missions.no_assignments_completed%");
  }

  private void CheckList(string emptyMessage)
  {
    bool flag = this.missionList.Children.Count == 0;
    this.description.IsRendered = !flag;
    this.errorMsg.IsRendered = flag;
    this.errorMsg.Text = emptyMessage;
  }
}
