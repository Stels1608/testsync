﻿// Decompiled with JetBrains decompiler
// Type: Flag
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class Flag : ILoadable
{
  private SignalHandler handler = (SignalHandler) (() => {});
  private uint deps;
  private bool value;

  public Flag IsLoaded
  {
    get
    {
      return this;
    }
  }

  public static implicit operator bool(Flag flag)
  {
    if (flag.value)
      return (int) flag.deps == 0;
    return false;
  }

  public static void Schedule(SignalHandler handler, params ILoadable[] deps)
  {
    Flag flag = new Flag();
    flag.Depend(deps);
    flag.AddHandler(handler);
    flag.Set();
  }

  public static void Schedule<T>(SignalHandler handler, IEnumerable<T> deps) where T : ILoadable
  {
    Flag flag = new Flag();
    flag.Depend<T>(deps);
    flag.AddHandler(handler);
    flag.Set();
  }

  private void AddDep(Flag depFlag)
  {
    if (depFlag == null || (bool) depFlag)
      return;
    ++this.deps;
    depFlag.AddHandler(new SignalHandler(this.DepSignal));
  }

  public void Depend(params ILoadable[] deps)
  {
    foreach (ILoadable dep in deps)
      this.AddDep(dep.IsLoaded);
  }

  public void Depend<T>(IEnumerable<T> deps) where T : ILoadable
  {
    foreach (T dep in deps)
      this.AddDep(dep.IsLoaded);
  }

  protected void DepSignal()
  {
    --this.deps;
    this.Check();
  }

  private void Check()
  {
    if (!(bool) this)
      return;
    this.handler();
    this.handler = (SignalHandler) (() => {});
  }

  public void Set()
  {
    if (this.value)
      return;
    this.value = true;
    this.Check();
  }

  public void Unset()
  {
    this.value = false;
  }

  public virtual void AddHandler(SignalHandler newHandler)
  {
    if ((bool) this)
      newHandler();
    else
      this.handler += newHandler;
  }
}
