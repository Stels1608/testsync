﻿// Decompiled with JetBrains decompiler
// Type: GuiCameraModeButtonsStrikes
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class GuiCameraModeButtonsStrikes : GUICameraModeButtonsBase
{
  public GuiCameraModeButtonsStrikes()
  {
    this.AddCameraModeButtons();
    this.OnCameraModeChanged(SpaceCameraBase.CameraMode.Chase);
  }

  protected override void LoadSupportedCameraModes()
  {
    this.SupportedCameraModes = SpaceCameraStrikes.SupportedCameraModes;
  }
}
