﻿// Decompiled with JetBrains decompiler
// Type: Interpolator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class Interpolator
{
  public float[][] samples;

  public Interpolator(float[][] samplePoints)
  {
    this.samples = samplePoints;
  }

  public static float SqrMagnitude(float[] a)
  {
    float num = 0.0f;
    for (int index = 0; index < a.Length; ++index)
      num += Mathf.Pow(a[index], 2f);
    return num;
  }

  public static float Magnitude(float[] a)
  {
    return Mathf.Sqrt(Interpolator.SqrMagnitude(a));
  }

  public static float SqrDistance(float[] a, float[] b)
  {
    float num = 0.0f;
    for (int index = 0; index < a.Length; ++index)
      num += Mathf.Pow(a[index] - b[index], 2f);
    return num;
  }

  public static float Distance(float[] a, float[] b)
  {
    return Mathf.Sqrt(Interpolator.SqrDistance(a, b));
  }

  public static float[] Normalized(float[] a)
  {
    return Interpolator.Multiply(a, 1f / Interpolator.Magnitude(a));
  }

  public static bool Equals(float[] a, float[] b)
  {
    return (double) Interpolator.SqrDistance(a, b) == 0.0;
  }

  public static float[] Multiply(float[] a, float m)
  {
    float[] numArray = new float[a.Length];
    for (int index = 0; index < a.Length; ++index)
      numArray[index] = a[index] * m;
    return numArray;
  }

  public static float Dot(float[] a, float[] b)
  {
    float num = 0.0f;
    for (int index = 0; index < a.Length; ++index)
      num += a[index] * b[index];
    return num;
  }

  public static float Angle(float[] a, float[] b)
  {
    float num = Interpolator.Magnitude(a) * Interpolator.Magnitude(b);
    if ((double) num == 0.0)
      return 0.0f;
    return Mathf.Acos(Mathf.Clamp(Interpolator.Dot(a, b) / num, -1f, 1f));
  }

  public static float ClockwiseAngle(float[] a, float[] b)
  {
    float num = Interpolator.Angle(a, b);
    if ((double) a[1] * (double) b[0] - (double) a[0] * (double) b[1] > 0.0)
      num = 6.283185f - num;
    return num;
  }

  public static float[] Add(float[] a, float[] b)
  {
    float[] numArray = new float[a.Length];
    for (int index = 0; index < a.Length; ++index)
      numArray[index] = a[index] + b[index];
    return numArray;
  }

  public float[] Subtract(float[] a, float[] b)
  {
    return Interpolator.Add(a, Interpolator.Multiply(b, -1f));
  }

  public virtual float[] Interpolate(float[] output)
  {
    return this.Interpolate(output, true);
  }

  public virtual float[] Interpolate(float[] output, bool normalize)
  {
    throw new NotImplementedException();
  }

  public float[] BasicChecks(float[] output)
  {
    if (this.samples.Length == 1)
      return new float[1]{ 1f };
    for (int index = 0; index < this.samples.Length; ++index)
    {
      if (Interpolator.Equals(output, this.samples[index]))
      {
        float[] numArray = new float[this.samples.Length];
        numArray[index] = 1f;
        return numArray;
      }
    }
    return (float[]) null;
  }
}
