﻿// Decompiled with JetBrains decompiler
// Type: DropDownLabeledWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DropDownLabeledWidget : MonoBehaviour
{
  [SerializeField]
  private UILabel label;
  [SerializeField]
  private DropDownWidget dropDownWidget;

  public string LabelText
  {
    get
    {
      return this.label.text;
    }
    set
    {
      this.label.text = value;
    }
  }

  public DropDownWidget DropDownWidget
  {
    get
    {
      return this.dropDownWidget;
    }
  }
}
