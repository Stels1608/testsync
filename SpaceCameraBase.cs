﻿// Decompiled with JetBrains decompiler
// Type: SpaceCameraBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpaceCameraBase : MonoBehaviour, InputListener
{
  public static CameraHelper CameraHelper = new CameraHelper();
  protected static float ZoomDistanceMin = 5f;
  protected static float ZoomDistanceMax = 100f;
  private static float zoomPercentage = 0.15f;
  protected static float wantedZoomPercentage = 0.3f;
  protected Animation CameraAnimation;
  protected SpaceCameraBase.CameraMode CurrentCameraMode;
  protected static CameraCard cameraCard;
  protected ISpaceCameraBehavior currentBehavior;
  protected static float ZoomSpeed;
  protected float ZoomDampVelocity;
  private BackgroundCameraManager backgroundCameraManager;

  public static MouseWheelBinding MouseWheel { get; set; }

  public static bool AdvancedFlightControls { get; set; }

  public static float ZoomDistance
  {
    get
    {
      return SpaceCameraBase.ZoomPercentage2AbsDist(SpaceCameraBase.zoomPercentage);
    }
    set
    {
      SpaceCameraBase.zoomPercentage = SpaceCameraBase.ZoomDist2Percentage(value);
    }
  }

  public static float WantedZoomDistance
  {
    get
    {
      return SpaceCameraBase.ZoomPercentage2AbsDist(SpaceCameraBase.wantedZoomPercentage);
    }
    set
    {
      SpaceCameraBase.wantedZoomPercentage = SpaceCameraBase.ZoomDist2Percentage(value);
    }
  }

  protected static float ZoomRange
  {
    get
    {
      return Mathf.Max(0.0f, SpaceCameraBase.ZoomDistanceMax - SpaceCameraBase.ZoomDistanceMin);
    }
  }

  public static float ZoomPercentage
  {
    get
    {
      return SpaceCameraBase.zoomPercentage;
    }
    set
    {
      SpaceCameraBase.zoomPercentage = Mathf.Clamp01(value);
    }
  }

  public ISpaceCameraBehavior CurrentBehavior
  {
    get
    {
      return this.currentBehavior;
    }
  }

  public abstract bool HandleMouseInput { get; }

  public abstract bool HandleKeyboardInput { get; }

  public abstract bool HandleJoystickAxesInput { get; }

  private static float ZoomDist2Percentage(float zoomDistance)
  {
    float num = Mathf.Max(0.0f, zoomDistance - SpaceCameraBase.ZoomDistanceMin);
    if ((double) SpaceCameraBase.ZoomRange == 0.0)
      return 1f;
    return Mathf.Clamp01(num / SpaceCameraBase.ZoomRange);
  }

  private static float ZoomPercentage2AbsDist(float percentage)
  {
    return SpaceCameraBase.ZoomDistanceMin + SpaceCameraBase.ZoomRange * percentage;
  }

  protected void SetCurrentBehavior(ISpaceCameraBehavior newBehavior)
  {
    FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new SpaceCameraBehaviorChangeMessage(this.currentBehavior, newBehavior));
    this.currentBehavior = newBehavior;
  }

  public abstract List<SpaceCameraBase.CameraMode> GetSupportedCameraModes();

  public bool SupportsCamMode(SpaceCameraBase.CameraMode cameraMode)
  {
    return this.GetSupportedCameraModes().Contains(cameraMode);
  }

  public virtual void SetCameraMode(SpaceCameraBase.CameraMode cameraMode)
  {
    if (!this.SupportsCamMode(cameraMode))
    {
      if (this.SupportsCamMode(this.CurrentCameraMode))
        return;
      this.CurrentCameraMode = this.GetSupportedCameraModes()[0];
    }
    else
      this.CurrentCameraMode = cameraMode;
  }

  protected virtual void InitCam()
  {
    SpaceCameraBase.ZoomSpeed = 3f;
  }

  protected virtual void Awake()
  {
    this.backgroundCameraManager = BackgroundCameraManager.Instance();
    this.backgroundCameraManager.SetActiveCam(Camera.main);
    this.InitCam();
  }

  protected void Start()
  {
  }

  protected virtual void Update()
  {
    SpaceCameraBase.CameraHelper.Update();
  }

  protected virtual void LateUpdate()
  {
  }

  public virtual void SetCameraCard(CameraCard camCard)
  {
    SpaceCameraBase.cameraCard = camCard;
    SpaceCameraBase.ZoomDistanceMin = SpaceCameraBase.cameraCard.MinZoom;
    SpaceCameraBase.ZoomDistanceMax = SpaceCameraBase.cameraCard.MaxZoom;
    SpaceCameraBase.ZoomSpeed = 0.1f;
    SpaceCameraBase.wantedZoomPercentage = SpaceCameraBase.zoomPercentage;
  }

  public static CameraCard GetCameraCard()
  {
    return SpaceCameraBase.cameraCard;
  }

  public void AdjustScroll(SpaceCameraBase.ScrollDirection direction)
  {
    if (direction == SpaceCameraBase.ScrollDirection.In)
    {
      this.Zoom(-1f);
    }
    else
    {
      if (direction != SpaceCameraBase.ScrollDirection.Out)
        return;
      this.Zoom(1f);
    }
  }

  protected void Zoom(float sign)
  {
    SpaceCameraBase.wantedZoomPercentage = Mathf.Clamp(SpaceCameraBase.wantedZoomPercentage + sign * SpaceCameraBase.ZoomSpeed, 0.0f, 1f);
    this.CancelInvoke("SaveZoomPercentage");
    this.Invoke("SaveZoomPercentage", 5f);
  }

  private void SaveZoomPercentage()
  {
    FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CameraZoom, (object) SpaceCameraBase.wantedZoomPercentage, true));
  }

  protected void ApplyCameraParameters(SpaceCameraParameters spaceCameraParameters)
  {
    this.ApplyFOV(spaceCameraParameters.Fov);
    this.ApplyRotation(spaceCameraParameters.CamRotation);
    this.ApplyPosition(spaceCameraParameters.CamPosition);
  }

  private void ApplyFOV(float fov)
  {
    Camera.main.fieldOfView = fov;
  }

  private void ApplyRotation(Quaternion rotation)
  {
    this.transform.rotation = rotation;
  }

  private void ApplyPosition(Vector3 position)
  {
    this.transform.position = position;
  }

  public virtual void OnHitted()
  {
  }

  public abstract bool OnMouseDown(float2 mousePosition, KeyCode mouseKey);

  public abstract bool OnMouseUp(float2 mousePosition, KeyCode mouseKey);

  public abstract void OnMouseMove(float2 mousePosition);

  public virtual bool OnMouseScrollDown()
  {
    this.OnMouseScroll(1f);
    return true;
  }

  public virtual bool OnMouseScrollUp()
  {
    this.OnMouseScroll(-1f);
    return true;
  }

  public void OnMouseScroll(float sign)
  {
    bool flag = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
    sign *= Time.deltaTime * 60f;
    if (!(SpaceCameraBase.MouseWheel == MouseWheelBinding.Thrust ^ !flag))
      return;
    this.Zoom(sign);
  }

  public virtual bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (action != Action.ZoomIn)
      return action == Action.ZoomOut;
    return true;
  }

  public virtual bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (action == Action.ZoomIn)
    {
      this.AdjustScroll(SpaceCameraBase.ScrollDirection.In);
      return true;
    }
    if (action != Action.ZoomOut)
      return false;
    this.AdjustScroll(SpaceCameraBase.ScrollDirection.Out);
    return true;
  }

  public virtual bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    float num = Mathf.Max(0.05f, JoystickSetup.DeadZone);
    switch (action)
    {
      case Action.JoystickLookLeft:
      case Action.JoystickLookRight:
      case Action.JoystickLookUp:
      case Action.JoystickLookDown:
        if ((double) Mathf.Abs(magnitude) > (double) num && !(this.currentBehavior is SpaceCameraBehaviorGeneralOrbitJoystick))
        {
          SpaceCameraBehaviorGeneralOrbitJoystick generalOrbitJoystick = new SpaceCameraBehaviorGeneralOrbitJoystick();
          this.SetCurrentBehavior((ISpaceCameraBehavior) generalOrbitJoystick);
          generalOrbitJoystick.OnJoystickAxesInput(triggerCode, modifierCode, action, magnitude, delta, isAxisInverted);
          break;
        }
        break;
      default:
        if ((double) Mathf.Abs(magnitude) > (double) num && this.currentBehavior is SpaceCameraBehaviorGeneralOrbitJoystick)
        {
          this.SetCameraMode(this.CurrentCameraMode);
          break;
        }
        break;
    }
    return false;
  }

  public virtual void OnTargetChanged(ISpaceEntity newTarget)
  {
  }

  public enum CameraMode
  {
    Target,
    Chase,
    Free,
    Nose,
  }

  public enum ScrollDirection
  {
    In,
    Out,
  }
}
