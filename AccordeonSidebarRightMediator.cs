﻿// Decompiled with JetBrains decompiler
// Type: AccordeonSidebarRightMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class AccordeonSidebarRightMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "AccordeonSidebarRightMediator";
  private AccordeonSidebarRightUgui sidebarRight;
  private bool hiddenByBigWindow;

  public AccordeonSidebarRightMediator()
    : base("AccordeonSidebarRightMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.LoadingScreenDestroyed);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.LevelLoaded);
    this.AddMessageInterest(Message.Death);
    this.AddMessageInterest(Message.GuiToggleHubMenuVisibility);
    this.AddMessageInterest(Message.OptionsWindowOpened);
    this.AddMessageInterest(Message.OptionsWindowClosed);
    this.AddMessageInterest(Message.HelpWindowOpened);
    this.AddMessageInterest(Message.HelpWindowClosed);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.OptionsWindowOpened:
      case Message.HelpWindowOpened:
        if (!this.sidebarRight.IsRendered)
          break;
        this.hiddenByBigWindow = true;
        this.sidebarRight.IsRendered = false;
        break;
      case Message.OptionsWindowClosed:
      case Message.HelpWindowClosed:
        if (!this.hiddenByBigWindow)
          break;
        this.hiddenByBigWindow = false;
        this.sidebarRight.IsRendered = true;
        break;
      case Message.LoadNewLevel:
        this.sidebarRight.IsRendered = false;
        this.hiddenByBigWindow = false;
        break;
      case Message.LevelLoaded:
        if (!((Object) RoomLevel.GetLevel() != (Object) null))
          break;
        this.sidebarRight.IsRendered = true;
        break;
      case Message.LoadingScreenDestroyed:
        FacadeFactory.GetInstance().SendMessage(Message.AddToCombatGui, (object) this.sidebarRight);
        break;
      default:
        if (id != Message.UiCreated)
        {
          if (id != Message.GuiToggleHubMenuVisibility)
          {
            if (id != Message.Death)
              break;
            this.sidebarRight.IsRendered = false;
            break;
          }
          this.sidebarRight.IsRendered = (bool) message.Data;
          break;
        }
        this.sidebarRight = AccordeonSidebarRightUgui.Instance;
        break;
    }
  }
}
