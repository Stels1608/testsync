﻿// Decompiled with JetBrains decompiler
// Type: SectorEventPanelCompletedUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SectorEventPanelCompletedUgui : SectorEventPanel
{
  private const string WON_TEXT = "bgo.sector_event.common.won";
  private const string LOST_TEXT = "bgo.sector_event.common.lost";
  private const string NO_REWARD = "bgo.sector_event.common.no_reward";
  [SerializeField]
  private TextMeshProUGUI resultText;
  [SerializeField]
  private Image medalImage;
  [SerializeField]
  private Image medalBg;
  [SerializeField]
  private TextMeshProUGUI medalText;
  [SerializeField]
  private UnityEngine.Sprite REWARD_BRONZE_MEDAL;
  [SerializeField]
  private UnityEngine.Sprite REWARD_SILVER_MEDAL;
  [SerializeField]
  private UnityEngine.Sprite REWARD_GOLD_MEDAL;
  [SerializeField]
  private UnityEngine.Sprite REWARD_PLATINUM_MEDAL;
  [SerializeField]
  private UnityEngine.Sprite REWARD_BRONZE_MEDAL_BG;
  [SerializeField]
  private UnityEngine.Sprite REWARD_SILVER_MEDAL_BG;
  [SerializeField]
  private UnityEngine.Sprite REWARD_GOLD_MEDAL_BG;
  [SerializeField]
  private UnityEngine.Sprite REWARD_PLATINUM_MEDAL_BG;
  [SerializeField]
  private GameObject rewardsRoot;

  private void Awake()
  {
    this.EraseRewards();
    this.RenderMedalItems(false);
  }

  private void EraseRewards()
  {
    foreach (Component component in this.rewardsRoot.transform)
      Object.Destroy((Object) component.gameObject);
  }

  public void RenderMedalItems(bool enable)
  {
    this.medalImage.enabled = enable;
    this.medalBg.enabled = enable;
    this.medalText.enabled = enable;
  }

  public void Initialize(SectorEventState state, Faction ownerFaction)
  {
    this.resultText.text = BsgoLocalization.Get((state != SectorEventState.Success || Game.Me.Faction != ownerFaction) && (state != SectorEventState.Failed || Game.Me.Faction == ownerFaction) ? "bgo.sector_event.common.lost" : "bgo.sector_event.common.won");
  }

  public void ShowReward(SectorEventReward reward)
  {
    if (reward.Ranking == SectorEventRanking.None)
    {
      this.resultText.text = BsgoLocalization.Get("bgo.sector_event.common.no_reward");
    }
    else
    {
      this.RenderMedalItems(true);
      string key = "bgo.sector_event.common.reward." + reward.Ranking.ToString().ToLowerInvariant();
      switch (reward.Ranking)
      {
        case SectorEventRanking.Bronze:
          this.medalImage.sprite = this.REWARD_BRONZE_MEDAL;
          this.medalBg.sprite = this.REWARD_BRONZE_MEDAL_BG;
          break;
        case SectorEventRanking.Silver:
          this.medalImage.sprite = this.REWARD_SILVER_MEDAL;
          this.medalBg.sprite = this.REWARD_SILVER_MEDAL_BG;
          break;
        case SectorEventRanking.Gold:
          this.medalImage.sprite = this.REWARD_GOLD_MEDAL;
          this.medalBg.sprite = this.REWARD_GOLD_MEDAL_BG;
          break;
        case SectorEventRanking.Platinum:
          this.medalImage.sprite = this.REWARD_PLATINUM_MEDAL;
          this.medalBg.sprite = this.REWARD_PLATINUM_MEDAL_BG;
          break;
        default:
          Debug.LogError((object) "ERROR IN GUI SECTOR EVENT COMPLETED");
          break;
      }
      this.medalText.text = BsgoLocalization.Get(key).ToUpperInvariant();
      this.EraseRewards();
      using (List<ShipItem>.Enumerator enumerator = reward.Items.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ShipItem current = enumerator.Current;
          SectorEventRewardItemUgui child = UguiTools.CreateChild<SectorEventRewardItemUgui>("AccordeonSidebarRight/SectorEventRewardItemUgui", this.rewardsRoot.transform);
          string amount = !(current is ItemCountable) ? "1" : (current as ItemCountable).Count.ToString();
          child.SetContent(current.ItemGUICard, amount);
        }
      }
    }
  }
}
