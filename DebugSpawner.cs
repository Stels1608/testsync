﻿// Decompiled with JetBrains decompiler
// Type: DebugSpawner
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugSpawner : MonoBehaviour
{
  public GameObject PrefabToSpawn;
  public float SpawnInterval;

  private void Start()
  {
    this.InvokeRepeating("Spawn", Random.value * this.SpawnInterval, this.SpawnInterval);
  }

  private void Spawn()
  {
    Object.Instantiate((Object) this.PrefabToSpawn, this.transform.position, this.transform.rotation);
  }
}
