﻿// Decompiled with JetBrains decompiler
// Type: GalaxyMapMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class GalaxyMapMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "GalaxyMapMediator";
  private GalaxyMapMain galaxyMapMain;

  public GalaxyMapMediator()
    : base("GalaxyMapMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.CreateLevelUi);
    this.AddMessageInterest(Message.GalaxyMapUpdates);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.CreateLevelUi:
        this.CreateView((GameLevel) message.Data);
        break;
      case Message.GalaxyMapUpdates:
        if (!Game.Galaxy.GalaxyLoaded)
          break;
        foreach (GalaxyMapUpdate update in (GalaxyMapUpdate[]) message.Data)
          this.ApplyGalaxyUpdate(update);
        this.galaxyMapMain.UpdateMap();
        break;
    }
  }

  private void CreateView(GameLevel level)
  {
    bool flag = false;
    if (level is SpaceLevel)
      flag = ((SpaceLevel) level).IsGlobal;
    else if (level is RoomLevel)
      flag = true;
    if (flag)
    {
      this.galaxyMapMain = new GalaxyMapMain();
      Game.GUIManager.AddPanel((IGUIPanel) this.galaxyMapMain);
      Game.InputDispatcher.AddListener((InputListener) this.galaxyMapMain);
    }
    else
      this.galaxyMapMain = (GalaxyMapMain) null;
  }

  private void ApplyGalaxyUpdate(GalaxyMapUpdate update)
  {
    switch (update.UpdateType)
    {
      case GalaxyUpdateType.Rcp:
        GalaxyMapMediator.UpdateRcp((GalaxyRcpUpdate) update);
        break;
      case GalaxyUpdateType.ConquestLocation:
        GalaxyMapMediator.UpdateConquestLocation((ConquestLocationUpdate) update);
        break;
      case GalaxyUpdateType.ConquestPrice:
        Game.Galaxy.SetCapitalShipCost((int) ((ConquestPriceUpdate) update).Price);
        break;
      case GalaxyUpdateType.SectorOutpostPoints:
        GalaxyMapMediator.UpdateOutpostPoints((SectorOutpostPointsUpdate) update);
        break;
      case GalaxyUpdateType.SectorMiningShips:
        GalaxyMapMediator.UpdateMiningShipCount((SectorMiningShipUpdate) update);
        break;
      case GalaxyUpdateType.SectorPvPKills:
        GalaxyMapMediator.UpdatePvPKillCount((SectorPvPKillUpdate) update);
        break;
      case GalaxyUpdateType.SectorDynamicMissions:
        GalaxyMapMediator.UpdateDynamicMissions((SectorDynamicMissionUpdate) update);
        break;
      case GalaxyUpdateType.SectorOutpostState:
        GalaxyMapMediator.UpdateOutpostState((SectorOutpostStateUpdate) update);
        break;
      case GalaxyUpdateType.SectorBeaconState:
        GalaxyMapMediator.UpdateBeaconState((SectorBeaconStateUpdate) update);
        break;
      case GalaxyUpdateType.SectorJumpTargetTransponders:
        GalaxyMapMediator.UpdateJumpTargetTransponders((SectorJumpTargetTransponderUpdate) update);
        break;
      case GalaxyUpdateType.SectorPlayerSlots:
        GalaxyMapMediator.UpdatePlayerSlots((SectorPlayerSlotUpdate) update);
        break;
    }
  }

  private static void UpdatePlayerSlots(SectorPlayerSlotUpdate sectorUpdate)
  {
    Game.Galaxy.GetStar((uint) sectorUpdate.SectorId).SectorCaps = sectorUpdate.SectorSlotData;
  }

  private static void UpdateJumpTargetTransponders(SectorJumpTargetTransponderUpdate sectorUpdate)
  {
    Game.Galaxy.GetStar((uint) sectorUpdate.SectorId).JumpTransponders = sectorUpdate.Descs;
  }

  private static void UpdateBeaconState(SectorBeaconStateUpdate sectorUpdate)
  {
    Game.Galaxy.GetStar((uint) sectorUpdate.SectorId).SetBeaconState(sectorUpdate.Faction, sectorUpdate.State);
  }

  private static void UpdateOutpostState(SectorOutpostStateUpdate sectorUpdate)
  {
    Game.Galaxy.GetStar((uint) sectorUpdate.SectorId).SetOutpostState(sectorUpdate.Faction, sectorUpdate.State);
  }

  private static void UpdateDynamicMissions(SectorDynamicMissionUpdate sectorUpdate)
  {
    Game.Galaxy.GetStar((uint) sectorUpdate.SectorId).SetDynamicMissions(sectorUpdate.Faction, (int) sectorUpdate.DynamicMissionCount);
  }

  private static void UpdatePvPKillCount(SectorPvPKillUpdate sectorUpdate)
  {
    Game.Galaxy.GetStar((uint) sectorUpdate.SectorId).SetPvPKillCount(sectorUpdate.Faction, sectorUpdate.Kills);
  }

  private static void UpdateMiningShipCount(SectorMiningShipUpdate sectorUpdate)
  {
    Game.Galaxy.GetStar((uint) sectorUpdate.SectorId).SetMiningShipCount(sectorUpdate.Faction, sectorUpdate.MiningShipCount);
  }

  private static void UpdateOutpostPoints(SectorOutpostPointsUpdate sectorUpdate)
  {
    Game.Galaxy.GetStar((uint) sectorUpdate.SectorId).SetOutpostPoints(sectorUpdate.Faction, sectorUpdate.OutpostPoints);
  }

  private static void UpdateConquestLocation(ConquestLocationUpdate conquestLocationUpdate)
  {
    if (conquestLocationUpdate.SectorId == -1)
      Game.Galaxy.UnSetShipEvent(conquestLocationUpdate.Faction);
    else
      Game.Galaxy.SetShipEvent(conquestLocationUpdate.Faction, conquestLocationUpdate.SectorId, conquestLocationUpdate.ExpireTime);
  }

  private static void UpdateRcp(GalaxyRcpUpdate update)
  {
    Game.Galaxy.SetRCP(update.Faction, update.Rcp);
  }
}
