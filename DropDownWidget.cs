﻿// Decompiled with JetBrains decompiler
// Type: DropDownWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DropDownWidget : NguiWidget
{
  [SerializeField]
  public DropDownCustomElement dropDown;

  public string Selection
  {
    get
    {
      return this.dropDown.value;
    }
    set
    {
      this.dropDown.value = value;
    }
  }

  public List<string> Items
  {
    get
    {
      return this.dropDown.items;
    }
    set
    {
      this.dropDown.items = value;
    }
  }

  private void Awake()
  {
  }
}
