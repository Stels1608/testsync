﻿// Decompiled with JetBrains decompiler
// Type: UIPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("NGUI/UI/NGUI Panel")]
[ExecuteInEditMode]
public class UIPanel : UIRect
{
  public static List<UIPanel> list = new List<UIPanel>();
  private static float[] mTemp = new float[4];
  private static Vector3[] mCorners = new Vector3[4];
  private static int mUpdateFrame = -1;
  public bool showInPanelTool = true;
  public bool cullWhileDragging = true;
  public bool softBorderPadding = true;
  public int startingRenderQueue = 3000;
  [NonSerialized]
  public List<UIWidget> widgets = new List<UIWidget>();
  [NonSerialized]
  public List<UIDrawCall> drawCalls = new List<UIDrawCall>();
  [NonSerialized]
  public Matrix4x4 worldToLocal = Matrix4x4.identity;
  [NonSerialized]
  public Vector4 drawCallClipRange = new Vector4(0.0f, 0.0f, 1f, 1f);
  [SerializeField]
  [HideInInspector]
  private float mAlpha = 1f;
  [HideInInspector]
  [SerializeField]
  private Vector4 mClipRange = new Vector4(0.0f, 0.0f, 300f, 200f);
  [SerializeField]
  [HideInInspector]
  private Vector2 mClipSoftness = new Vector2(4f, 4f);
  [SerializeField]
  private Vector2 mClipOffset = Vector2.zero;
  private int mMatrixFrame = -1;
  private int mLayer = -1;
  private Vector2 mMin = Vector2.zero;
  private Vector2 mMax = Vector2.zero;
  public UIPanel.OnGeometryUpdated onGeometryUpdated;
  public bool generateNormals;
  public bool widgetsAreStatic;
  public bool alwaysOnScreen;
  public bool anchorOffset;
  public UIPanel.RenderQueue renderQueue;
  public UIPanel.OnClippingMoved onClipMove;
  [HideInInspector]
  [SerializeField]
  private UIDrawCall.Clipping mClipping;
  [HideInInspector]
  [SerializeField]
  private int mDepth;
  [HideInInspector]
  [SerializeField]
  private int mSortingOrder;
  private bool mRebuild;
  private bool mResized;
  private float mCullTime;
  private float mUpdateTime;
  private int mAlphaFrameID;
  private bool mHalfPixelOffset;
  private bool mSortWidgets;
  private bool mUpdateScroll;
  private UIPanel mParentPanel;
  private UIDrawCall.OnRenderCallback mOnRender;
  private bool mForced;

  public static int nextUnusedDepth
  {
    get
    {
      int a = int.MinValue;
      int index = 0;
      for (int count = UIPanel.list.Count; index < count; ++index)
        a = Mathf.Max(a, UIPanel.list[index].depth);
      if (a == int.MinValue)
        return 0;
      return a + 1;
    }
  }

  public override bool canBeAnchored
  {
    get
    {
      return this.mClipping != UIDrawCall.Clipping.None;
    }
  }

  public override float alpha
  {
    get
    {
      return this.mAlpha;
    }
    set
    {
      float num = Mathf.Clamp01(value);
      if ((double) this.mAlpha == (double) num)
        return;
      this.mAlphaFrameID = -1;
      this.mResized = true;
      this.mAlpha = num;
      this.SetDirty();
    }
  }

  public int depth
  {
    get
    {
      return this.mDepth;
    }
    set
    {
      if (this.mDepth == value)
        return;
      this.mDepth = value;
      UIPanel.list.Sort(new Comparison<UIPanel>(UIPanel.CompareFunc));
    }
  }

  public int sortingOrder
  {
    get
    {
      return this.mSortingOrder;
    }
    set
    {
      if (this.mSortingOrder == value)
        return;
      this.mSortingOrder = value;
      this.UpdateDrawCalls();
    }
  }

  public float width
  {
    get
    {
      return this.GetViewSize().x;
    }
  }

  public float height
  {
    get
    {
      return this.GetViewSize().y;
    }
  }

  public bool halfPixelOffset
  {
    get
    {
      return this.mHalfPixelOffset;
    }
  }

  public bool usedForUI
  {
    get
    {
      if ((UnityEngine.Object) this.anchorCamera != (UnityEngine.Object) null)
        return this.mCam.orthographic;
      return false;
    }
  }

  public Vector3 drawCallOffset
  {
    get
    {
      if (!this.mHalfPixelOffset || !((UnityEngine.Object) this.anchorCamera != (UnityEngine.Object) null) || !this.mCam.orthographic)
        return Vector3.zero;
      float y = 1f / this.GetWindowSize().y / this.mCam.orthographicSize;
      return new Vector3(-y, y);
    }
  }

  public UIDrawCall.Clipping clipping
  {
    get
    {
      return this.mClipping;
    }
    set
    {
      if (this.mClipping == value)
        return;
      this.mResized = true;
      this.mClipping = value;
      this.mMatrixFrame = -1;
    }
  }

  public UIPanel parentPanel
  {
    get
    {
      return this.mParentPanel;
    }
  }

  public int clipCount
  {
    get
    {
      int num = 0;
      for (UIPanel uiPanel = this; (UnityEngine.Object) uiPanel != (UnityEngine.Object) null; uiPanel = uiPanel.mParentPanel)
      {
        if (uiPanel.mClipping == UIDrawCall.Clipping.SoftClip)
          ++num;
      }
      return num;
    }
  }

  public bool hasClipping
  {
    get
    {
      return this.mClipping == UIDrawCall.Clipping.SoftClip;
    }
  }

  public bool hasCumulativeClipping
  {
    get
    {
      return this.clipCount != 0;
    }
  }

  [Obsolete("Use 'hasClipping' or 'hasCumulativeClipping' instead")]
  public bool clipsChildren
  {
    get
    {
      return this.hasCumulativeClipping;
    }
  }

  public Vector2 clipOffset
  {
    get
    {
      return this.mClipOffset;
    }
    set
    {
      if ((double) Mathf.Abs(this.mClipOffset.x - value.x) <= 1.0 / 1000.0 && (double) Mathf.Abs(this.mClipOffset.y - value.y) <= 1.0 / 1000.0)
        return;
      this.mClipOffset = value;
      this.InvalidateClipping();
      if (this.onClipMove == null)
        return;
      this.onClipMove(this);
    }
  }

  [Obsolete("Use 'finalClipRegion' or 'baseClipRegion' instead")]
  public Vector4 clipRange
  {
    get
    {
      return this.baseClipRegion;
    }
    set
    {
      this.baseClipRegion = value;
    }
  }

  public Vector4 baseClipRegion
  {
    get
    {
      return this.mClipRange;
    }
    set
    {
      if ((double) Mathf.Abs(this.mClipRange.x - value.x) <= 1.0 / 1000.0 && (double) Mathf.Abs(this.mClipRange.y - value.y) <= 1.0 / 1000.0 && ((double) Mathf.Abs(this.mClipRange.z - value.z) <= 1.0 / 1000.0 && (double) Mathf.Abs(this.mClipRange.w - value.w) <= 1.0 / 1000.0))
        return;
      this.mResized = true;
      this.mCullTime = (double) this.mCullTime != 0.0 ? RealTime.time + 0.15f : 1f / 1000f;
      this.mClipRange = value;
      this.mMatrixFrame = -1;
      UIScrollView component = this.GetComponent<UIScrollView>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.UpdatePosition();
      if (this.onClipMove == null)
        return;
      this.onClipMove(this);
    }
  }

  public Vector4 finalClipRegion
  {
    get
    {
      Vector2 viewSize = this.GetViewSize();
      if (this.mClipping != UIDrawCall.Clipping.None)
        return new Vector4(this.mClipRange.x + this.mClipOffset.x, this.mClipRange.y + this.mClipOffset.y, viewSize.x, viewSize.y);
      return new Vector4(0.0f, 0.0f, viewSize.x, viewSize.y);
    }
  }

  public Vector2 clipSoftness
  {
    get
    {
      return this.mClipSoftness;
    }
    set
    {
      if (!(this.mClipSoftness != value))
        return;
      this.mClipSoftness = value;
    }
  }

  public override Vector3[] localCorners
  {
    get
    {
      if (this.mClipping == UIDrawCall.Clipping.None)
      {
        Vector3[] worldCorners = this.worldCorners;
        Transform cachedTransform = this.cachedTransform;
        for (int index = 0; index < 4; ++index)
          worldCorners[index] = cachedTransform.InverseTransformPoint(worldCorners[index]);
        return worldCorners;
      }
      float x1 = (float) ((double) this.mClipOffset.x + (double) this.mClipRange.x - 0.5 * (double) this.mClipRange.z);
      float y1 = (float) ((double) this.mClipOffset.y + (double) this.mClipRange.y - 0.5 * (double) this.mClipRange.w);
      float x2 = x1 + this.mClipRange.z;
      float y2 = y1 + this.mClipRange.w;
      UIPanel.mCorners[0] = new Vector3(x1, y1);
      UIPanel.mCorners[1] = new Vector3(x1, y2);
      UIPanel.mCorners[2] = new Vector3(x2, y2);
      UIPanel.mCorners[3] = new Vector3(x2, y1);
      return UIPanel.mCorners;
    }
  }

  public override Vector3[] worldCorners
  {
    get
    {
      if (this.mClipping != UIDrawCall.Clipping.None)
      {
        float x1 = (float) ((double) this.mClipOffset.x + (double) this.mClipRange.x - 0.5 * (double) this.mClipRange.z);
        float y1 = (float) ((double) this.mClipOffset.y + (double) this.mClipRange.y - 0.5 * (double) this.mClipRange.w);
        float x2 = x1 + this.mClipRange.z;
        float y2 = y1 + this.mClipRange.w;
        Transform cachedTransform = this.cachedTransform;
        UIPanel.mCorners[0] = cachedTransform.TransformPoint(x1, y1, 0.0f);
        UIPanel.mCorners[1] = cachedTransform.TransformPoint(x1, y2, 0.0f);
        UIPanel.mCorners[2] = cachedTransform.TransformPoint(x2, y2, 0.0f);
        UIPanel.mCorners[3] = cachedTransform.TransformPoint(x2, y1, 0.0f);
      }
      else
      {
        if ((UnityEngine.Object) this.anchorCamera != (UnityEngine.Object) null)
          return this.mCam.GetWorldCorners(this.cameraRayDistance);
        Vector2 viewSize = this.GetViewSize();
        float x1 = -0.5f * viewSize.x;
        float y1 = -0.5f * viewSize.y;
        float x2 = x1 + viewSize.x;
        float y2 = y1 + viewSize.y;
        UIPanel.mCorners[0] = new Vector3(x1, y1);
        UIPanel.mCorners[1] = new Vector3(x1, y2);
        UIPanel.mCorners[2] = new Vector3(x2, y2);
        UIPanel.mCorners[3] = new Vector3(x2, y1);
        if (this.anchorOffset && (UnityEngine.Object) this.mCam == (UnityEngine.Object) null || (UnityEngine.Object) this.mCam.transform.parent != (UnityEngine.Object) this.cachedTransform)
        {
          Vector3 position = this.cachedTransform.position;
          for (int index = 0; index < 4; ++index)
            UIPanel.mCorners[index] += position;
        }
      }
      return UIPanel.mCorners;
    }
  }

  public static int CompareFunc(UIPanel a, UIPanel b)
  {
    if (!((UnityEngine.Object) a != (UnityEngine.Object) b) || !((UnityEngine.Object) a != (UnityEngine.Object) null) || !((UnityEngine.Object) b != (UnityEngine.Object) null))
      return 0;
    return a.mDepth < b.mDepth || a.mDepth <= b.mDepth && a.GetInstanceID() < b.GetInstanceID() ? -1 : 1;
  }

  private void InvalidateClipping()
  {
    this.mResized = true;
    this.mMatrixFrame = -1;
    this.mCullTime = (double) this.mCullTime != 0.0 ? RealTime.time + 0.15f : 1f / 1000f;
    int index = 0;
    for (int count = UIPanel.list.Count; index < count; ++index)
    {
      UIPanel uiPanel = UIPanel.list[index];
      if ((UnityEngine.Object) uiPanel != (UnityEngine.Object) this && (UnityEngine.Object) uiPanel.parentPanel == (UnityEngine.Object) this)
        uiPanel.InvalidateClipping();
    }
  }

  public override Vector3[] GetSides(Transform relativeTo)
  {
    if (this.mClipping != UIDrawCall.Clipping.None)
    {
      float x1 = (float) ((double) this.mClipOffset.x + (double) this.mClipRange.x - 0.5 * (double) this.mClipRange.z);
      float y1 = (float) ((double) this.mClipOffset.y + (double) this.mClipRange.y - 0.5 * (double) this.mClipRange.w);
      float x2 = x1 + this.mClipRange.z;
      float y2 = y1 + this.mClipRange.w;
      float x3 = (float) (((double) x1 + (double) x2) * 0.5);
      float y3 = (float) (((double) y1 + (double) y2) * 0.5);
      Transform cachedTransform = this.cachedTransform;
      UIRect.mSides[0] = cachedTransform.TransformPoint(x1, y3, 0.0f);
      UIRect.mSides[1] = cachedTransform.TransformPoint(x3, y2, 0.0f);
      UIRect.mSides[2] = cachedTransform.TransformPoint(x2, y3, 0.0f);
      UIRect.mSides[3] = cachedTransform.TransformPoint(x3, y1, 0.0f);
      if ((UnityEngine.Object) relativeTo != (UnityEngine.Object) null)
      {
        for (int index = 0; index < 4; ++index)
          UIRect.mSides[index] = relativeTo.InverseTransformPoint(UIRect.mSides[index]);
      }
      return UIRect.mSides;
    }
    if (!((UnityEngine.Object) this.anchorCamera != (UnityEngine.Object) null) || !this.anchorOffset)
      return base.GetSides(relativeTo);
    Vector3[] sides = this.mCam.GetSides(this.cameraRayDistance);
    Vector3 position = this.cachedTransform.position;
    for (int index = 0; index < 4; ++index)
      sides[index] += position;
    if ((UnityEngine.Object) relativeTo != (UnityEngine.Object) null)
    {
      for (int index = 0; index < 4; ++index)
        sides[index] = relativeTo.InverseTransformPoint(sides[index]);
    }
    return sides;
  }

  public override void Invalidate(bool includeChildren)
  {
    this.mAlphaFrameID = -1;
    base.Invalidate(includeChildren);
  }

  public override float CalculateFinalAlpha(int frameID)
  {
    if (this.mAlphaFrameID != frameID)
    {
      this.mAlphaFrameID = frameID;
      this.finalAlpha = !((UnityEngine.Object) this.parent != (UnityEngine.Object) null) ? this.mAlpha : this.parent.CalculateFinalAlpha(frameID) * this.mAlpha;
    }
    return this.finalAlpha;
  }

  public override void SetRect(float x, float y, float width, float height)
  {
    int int1 = Mathf.FloorToInt(width + 0.5f);
    int int2 = Mathf.FloorToInt(height + 0.5f);
    int num1 = int1 >> 1 << 1;
    int num2 = int2 >> 1 << 1;
    Transform cachedTransform = this.cachedTransform;
    Vector3 localPosition = cachedTransform.localPosition;
    localPosition.x = Mathf.Floor(x + 0.5f);
    localPosition.y = Mathf.Floor(y + 0.5f);
    if (num1 < 2)
      num1 = 2;
    if (num2 < 2)
      num2 = 2;
    this.baseClipRegion = new Vector4(localPosition.x, localPosition.y, (float) num1, (float) num2);
    if (!this.isAnchored)
      return;
    Transform parent = cachedTransform.parent;
    if ((bool) ((UnityEngine.Object) this.leftAnchor.target))
      this.leftAnchor.SetHorizontal(parent, x);
    if ((bool) ((UnityEngine.Object) this.rightAnchor.target))
      this.rightAnchor.SetHorizontal(parent, x + width);
    if ((bool) ((UnityEngine.Object) this.bottomAnchor.target))
      this.bottomAnchor.SetVertical(parent, y);
    if (!(bool) ((UnityEngine.Object) this.topAnchor.target))
      return;
    this.topAnchor.SetVertical(parent, y + height);
  }

  public bool IsVisible(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
  {
    this.UpdateTransformMatrix();
    a = this.worldToLocal.MultiplyPoint3x4(a);
    b = this.worldToLocal.MultiplyPoint3x4(b);
    c = this.worldToLocal.MultiplyPoint3x4(c);
    d = this.worldToLocal.MultiplyPoint3x4(d);
    UIPanel.mTemp[0] = a.x;
    UIPanel.mTemp[1] = b.x;
    UIPanel.mTemp[2] = c.x;
    UIPanel.mTemp[3] = d.x;
    float num1 = Mathf.Min(UIPanel.mTemp);
    float num2 = Mathf.Max(UIPanel.mTemp);
    UIPanel.mTemp[0] = a.y;
    UIPanel.mTemp[1] = b.y;
    UIPanel.mTemp[2] = c.y;
    UIPanel.mTemp[3] = d.y;
    float num3 = Mathf.Min(UIPanel.mTemp);
    float num4 = Mathf.Max(UIPanel.mTemp);
    return (double) num2 >= (double) this.mMin.x && (double) num4 >= (double) this.mMin.y && ((double) num1 <= (double) this.mMax.x && (double) num3 <= (double) this.mMax.y);
  }

  public bool IsVisible(Vector3 worldPos)
  {
    if ((double) this.mAlpha < 1.0 / 1000.0)
      return false;
    if (this.mClipping == UIDrawCall.Clipping.None || this.mClipping == UIDrawCall.Clipping.ConstrainButDontClip)
      return true;
    this.UpdateTransformMatrix();
    Vector3 vector3 = this.worldToLocal.MultiplyPoint3x4(worldPos);
    return (double) vector3.x >= (double) this.mMin.x && (double) vector3.y >= (double) this.mMin.y && ((double) vector3.x <= (double) this.mMax.x && (double) vector3.y <= (double) this.mMax.y);
  }

  public bool IsVisible(UIWidget w)
  {
    UIPanel uiPanel = this;
    Vector3[] vector3Array = (Vector3[]) null;
    while ((UnityEngine.Object) uiPanel != (UnityEngine.Object) null)
    {
      if ((uiPanel.mClipping == UIDrawCall.Clipping.None || uiPanel.mClipping == UIDrawCall.Clipping.ConstrainButDontClip) && !w.hideIfOffScreen)
      {
        uiPanel = uiPanel.mParentPanel;
      }
      else
      {
        if (vector3Array == null)
          vector3Array = w.worldCorners;
        if (!uiPanel.IsVisible(vector3Array[0], vector3Array[1], vector3Array[2], vector3Array[3]))
          return false;
        uiPanel = uiPanel.mParentPanel;
      }
    }
    return true;
  }

  public bool Affects(UIWidget w)
  {
    if ((UnityEngine.Object) w == (UnityEngine.Object) null)
      return false;
    UIPanel uiPanel1 = w.panel;
    if ((UnityEngine.Object) uiPanel1 == (UnityEngine.Object) null)
      return false;
    for (UIPanel uiPanel2 = this; (UnityEngine.Object) uiPanel2 != (UnityEngine.Object) null; uiPanel2 = uiPanel2.mParentPanel)
    {
      if ((UnityEngine.Object) uiPanel2 == (UnityEngine.Object) uiPanel1)
        return true;
      if (!uiPanel2.hasCumulativeClipping)
        return false;
    }
    return false;
  }

  [ContextMenu("Force Refresh")]
  public void RebuildAllDrawCalls()
  {
    this.mRebuild = true;
  }

  public void SetDirty()
  {
    int index = 0;
    for (int count = this.drawCalls.Count; index < count; ++index)
      this.drawCalls[index].isDirty = true;
    this.Invalidate(true);
  }

  private void Awake()
  {
    this.mGo = this.gameObject;
    this.mTrans = this.transform;
    this.mHalfPixelOffset = Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.XBOX360 || Application.platform == RuntimePlatform.WindowsWebPlayer || Application.platform == RuntimePlatform.WindowsEditor;
    if (!this.mHalfPixelOffset)
      return;
    this.mHalfPixelOffset = SystemInfo.graphicsShaderLevel < 40;
  }

  private void FindParent()
  {
    Transform parent = this.cachedTransform.parent;
    this.mParentPanel = !((UnityEngine.Object) parent != (UnityEngine.Object) null) ? (UIPanel) null : NGUITools.FindInParents<UIPanel>(parent.gameObject);
  }

  public override void ParentHasChanged()
  {
    base.ParentHasChanged();
    this.FindParent();
  }

  protected override void OnStart()
  {
    this.mLayer = this.mGo.layer;
  }

  protected override void OnEnable()
  {
    this.mRebuild = true;
    this.mAlphaFrameID = -1;
    this.mMatrixFrame = -1;
    this.OnStart();
    base.OnEnable();
    this.mMatrixFrame = -1;
  }

  protected override void OnInit()
  {
    base.OnInit();
    if ((UnityEngine.Object) this.GetComponent<Rigidbody>() == (UnityEngine.Object) null)
    {
      UICamera uiCamera = !((UnityEngine.Object) this.anchorCamera != (UnityEngine.Object) null) ? (UICamera) null : this.mCam.GetComponent<UICamera>();
      if ((UnityEngine.Object) uiCamera != (UnityEngine.Object) null && (uiCamera.eventType == UICamera.EventType.UI_3D || uiCamera.eventType == UICamera.EventType.World_3D))
      {
        Rigidbody rigidbody = this.gameObject.AddComponent<Rigidbody>();
        rigidbody.isKinematic = true;
        rigidbody.useGravity = false;
      }
    }
    this.FindParent();
    this.mRebuild = true;
    this.mAlphaFrameID = -1;
    this.mMatrixFrame = -1;
    UIPanel.list.Add(this);
    UIPanel.list.Sort(new Comparison<UIPanel>(UIPanel.CompareFunc));
  }

  protected override void OnDisable()
  {
    int index = 0;
    for (int count = this.drawCalls.Count; index < count; ++index)
    {
      UIDrawCall dc = this.drawCalls[index];
      if ((UnityEngine.Object) dc != (UnityEngine.Object) null)
        UIDrawCall.Destroy(dc);
    }
    this.drawCalls.Clear();
    UIPanel.list.Remove(this);
    this.mAlphaFrameID = -1;
    this.mMatrixFrame = -1;
    if (UIPanel.list.Count == 0)
    {
      UIDrawCall.ReleaseAll();
      UIPanel.mUpdateFrame = -1;
    }
    base.OnDisable();
  }

  private void UpdateTransformMatrix()
  {
    int frameCount = Time.frameCount;
    if (this.mMatrixFrame == frameCount)
      return;
    this.mMatrixFrame = frameCount;
    this.worldToLocal = this.cachedTransform.worldToLocalMatrix;
    Vector2 vector2 = this.GetViewSize() * 0.5f;
    float num1 = this.mClipOffset.x + this.mClipRange.x;
    float num2 = this.mClipOffset.y + this.mClipRange.y;
    this.mMin.x = num1 - vector2.x;
    this.mMin.y = num2 - vector2.y;
    this.mMax.x = num1 + vector2.x;
    this.mMax.y = num2 + vector2.y;
  }

  protected override void OnAnchor()
  {
    if (this.mClipping == UIDrawCall.Clipping.None)
      return;
    Transform cachedTransform = this.cachedTransform;
    Transform parent = cachedTransform.parent;
    Vector2 viewSize = this.GetViewSize();
    Vector2 vector2_1 = (Vector2) cachedTransform.localPosition;
    float num1;
    float num2;
    float num3;
    float num4;
    if ((UnityEngine.Object) this.leftAnchor.target == (UnityEngine.Object) this.bottomAnchor.target && (UnityEngine.Object) this.leftAnchor.target == (UnityEngine.Object) this.rightAnchor.target && (UnityEngine.Object) this.leftAnchor.target == (UnityEngine.Object) this.topAnchor.target)
    {
      Vector3[] sides = this.leftAnchor.GetSides(parent);
      if (sides != null)
      {
        num1 = NGUIMath.Lerp(sides[0].x, sides[2].x, this.leftAnchor.relative) + (float) this.leftAnchor.absolute;
        num2 = NGUIMath.Lerp(sides[0].x, sides[2].x, this.rightAnchor.relative) + (float) this.rightAnchor.absolute;
        num3 = NGUIMath.Lerp(sides[3].y, sides[1].y, this.bottomAnchor.relative) + (float) this.bottomAnchor.absolute;
        num4 = NGUIMath.Lerp(sides[3].y, sides[1].y, this.topAnchor.relative) + (float) this.topAnchor.absolute;
      }
      else
      {
        Vector2 vector2_2 = (Vector2) this.GetLocalPos(this.leftAnchor, parent);
        num1 = vector2_2.x + (float) this.leftAnchor.absolute;
        num3 = vector2_2.y + (float) this.bottomAnchor.absolute;
        num2 = vector2_2.x + (float) this.rightAnchor.absolute;
        num4 = vector2_2.y + (float) this.topAnchor.absolute;
      }
    }
    else
    {
      if ((bool) ((UnityEngine.Object) this.leftAnchor.target))
      {
        Vector3[] sides = this.leftAnchor.GetSides(parent);
        num1 = sides == null ? this.GetLocalPos(this.leftAnchor, parent).x + (float) this.leftAnchor.absolute : NGUIMath.Lerp(sides[0].x, sides[2].x, this.leftAnchor.relative) + (float) this.leftAnchor.absolute;
      }
      else
        num1 = this.mClipRange.x - 0.5f * viewSize.x;
      if ((bool) ((UnityEngine.Object) this.rightAnchor.target))
      {
        Vector3[] sides = this.rightAnchor.GetSides(parent);
        num2 = sides == null ? this.GetLocalPos(this.rightAnchor, parent).x + (float) this.rightAnchor.absolute : NGUIMath.Lerp(sides[0].x, sides[2].x, this.rightAnchor.relative) + (float) this.rightAnchor.absolute;
      }
      else
        num2 = this.mClipRange.x + 0.5f * viewSize.x;
      if ((bool) ((UnityEngine.Object) this.bottomAnchor.target))
      {
        Vector3[] sides = this.bottomAnchor.GetSides(parent);
        num3 = sides == null ? this.GetLocalPos(this.bottomAnchor, parent).y + (float) this.bottomAnchor.absolute : NGUIMath.Lerp(sides[3].y, sides[1].y, this.bottomAnchor.relative) + (float) this.bottomAnchor.absolute;
      }
      else
        num3 = this.mClipRange.y - 0.5f * viewSize.y;
      if ((bool) ((UnityEngine.Object) this.topAnchor.target))
      {
        Vector3[] sides = this.topAnchor.GetSides(parent);
        num4 = sides == null ? this.GetLocalPos(this.topAnchor, parent).y + (float) this.topAnchor.absolute : NGUIMath.Lerp(sides[3].y, sides[1].y, this.topAnchor.relative) + (float) this.topAnchor.absolute;
      }
      else
        num4 = this.mClipRange.y + 0.5f * viewSize.y;
    }
    float from1 = num1 - (vector2_1.x + this.mClipOffset.x);
    float to1 = num2 - (vector2_1.x + this.mClipOffset.x);
    float from2 = num3 - (vector2_1.y + this.mClipOffset.y);
    float to2 = num4 - (vector2_1.y + this.mClipOffset.y);
    float x = Mathf.Lerp(from1, to1, 0.5f);
    float y = Mathf.Lerp(from2, to2, 0.5f);
    float z = to1 - from1;
    float w = to2 - from2;
    float num5 = Mathf.Max(2f, this.mClipSoftness.x);
    float num6 = Mathf.Max(2f, this.mClipSoftness.y);
    if ((double) z < (double) num5)
      z = num5;
    if ((double) w < (double) num6)
      w = num6;
    this.baseClipRegion = new Vector4(x, y, z, w);
  }

  private void LateUpdate()
  {
    if (UIPanel.mUpdateFrame == Time.frameCount)
      return;
    UIPanel.mUpdateFrame = Time.frameCount;
    int index1 = 0;
    for (int count = UIPanel.list.Count; index1 < count; ++index1)
      UIPanel.list[index1].UpdateSelf();
    int a = 3000;
    int index2 = 0;
    for (int count = UIPanel.list.Count; index2 < count; ++index2)
    {
      UIPanel uiPanel = UIPanel.list[index2];
      if (uiPanel.renderQueue == UIPanel.RenderQueue.Automatic)
      {
        uiPanel.startingRenderQueue = a;
        uiPanel.UpdateDrawCalls();
        a += uiPanel.drawCalls.Count;
      }
      else if (uiPanel.renderQueue == UIPanel.RenderQueue.StartAt)
      {
        uiPanel.UpdateDrawCalls();
        if (uiPanel.drawCalls.Count != 0)
          a = Mathf.Max(a, uiPanel.startingRenderQueue + uiPanel.drawCalls.Count);
      }
      else
      {
        uiPanel.UpdateDrawCalls();
        if (uiPanel.drawCalls.Count != 0)
          a = Mathf.Max(a, uiPanel.startingRenderQueue + 1);
      }
    }
  }

  private void UpdateSelf()
  {
    this.mUpdateTime = RealTime.time;
    this.UpdateTransformMatrix();
    this.UpdateLayers();
    this.UpdateWidgets();
    if (this.mRebuild)
    {
      this.mRebuild = false;
      this.FillAllDrawCalls();
    }
    else
    {
      int index = 0;
      while (index < this.drawCalls.Count)
      {
        UIDrawCall dc = this.drawCalls[index];
        if (dc.isDirty && !this.FillDrawCall(dc))
        {
          UIDrawCall.Destroy(dc);
          this.drawCalls.RemoveAt(index);
        }
        else
          ++index;
      }
    }
    if (!this.mUpdateScroll)
      return;
    this.mUpdateScroll = false;
    UIScrollView component = this.GetComponent<UIScrollView>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.UpdateScrollbars();
  }

  public void SortWidgets()
  {
    this.mSortWidgets = false;
    this.widgets.Sort(new Comparison<UIWidget>(UIWidget.PanelCompareFunc));
  }

  private void FillAllDrawCalls()
  {
    for (int index = 0; index < this.drawCalls.Count; ++index)
      UIDrawCall.Destroy(this.drawCalls[index]);
    this.drawCalls.Clear();
    Material mat = (Material) null;
    Texture tex = (Texture) null;
    Shader shader1 = (Shader) null;
    UIDrawCall uiDrawCall = (UIDrawCall) null;
    int widgetCount = 0;
    if (this.mSortWidgets)
      this.SortWidgets();
    for (int index = 0; index < this.widgets.Count; ++index)
    {
      UIWidget uiWidget = this.widgets[index];
      if (uiWidget.isVisible && uiWidget.hasVertices)
      {
        Material material = uiWidget.material;
        Texture mainTexture = uiWidget.mainTexture;
        Shader shader2 = uiWidget.shader;
        if ((UnityEngine.Object) mat != (UnityEngine.Object) material || (UnityEngine.Object) tex != (UnityEngine.Object) mainTexture || (UnityEngine.Object) shader1 != (UnityEngine.Object) shader2)
        {
          if ((UnityEngine.Object) uiDrawCall != (UnityEngine.Object) null && uiDrawCall.verts.size != 0)
          {
            this.drawCalls.Add(uiDrawCall);
            uiDrawCall.UpdateGeometry(widgetCount);
            uiDrawCall.onRender = this.mOnRender;
            this.mOnRender = (UIDrawCall.OnRenderCallback) null;
            widgetCount = 0;
            uiDrawCall = (UIDrawCall) null;
          }
          mat = material;
          tex = mainTexture;
          shader1 = shader2;
        }
        if ((UnityEngine.Object) mat != (UnityEngine.Object) null || (UnityEngine.Object) shader1 != (UnityEngine.Object) null || (UnityEngine.Object) tex != (UnityEngine.Object) null)
        {
          if ((UnityEngine.Object) uiDrawCall == (UnityEngine.Object) null)
          {
            uiDrawCall = UIDrawCall.Create(this, mat, tex, shader1);
            uiDrawCall.depthStart = uiWidget.depth;
            uiDrawCall.depthEnd = uiDrawCall.depthStart;
            uiDrawCall.panel = this;
          }
          else
          {
            int depth = uiWidget.depth;
            if (depth < uiDrawCall.depthStart)
              uiDrawCall.depthStart = depth;
            if (depth > uiDrawCall.depthEnd)
              uiDrawCall.depthEnd = depth;
          }
          uiWidget.drawCall = uiDrawCall;
          ++widgetCount;
          if (this.generateNormals)
            uiWidget.WriteToBuffers(uiDrawCall.verts, uiDrawCall.uvs, uiDrawCall.cols, uiDrawCall.norms, uiDrawCall.tans);
          else
            uiWidget.WriteToBuffers(uiDrawCall.verts, uiDrawCall.uvs, uiDrawCall.cols, (BetterList<Vector3>) null, (BetterList<Vector4>) null);
          if (uiWidget.mOnRender != null)
          {
            if (this.mOnRender == null)
              this.mOnRender = uiWidget.mOnRender;
            else
              this.mOnRender += uiWidget.mOnRender;
          }
        }
      }
      else
        uiWidget.drawCall = (UIDrawCall) null;
    }
    if (!((UnityEngine.Object) uiDrawCall != (UnityEngine.Object) null) || uiDrawCall.verts.size == 0)
      return;
    this.drawCalls.Add(uiDrawCall);
    uiDrawCall.UpdateGeometry(widgetCount);
    uiDrawCall.onRender = this.mOnRender;
    this.mOnRender = (UIDrawCall.OnRenderCallback) null;
  }

  private bool FillDrawCall(UIDrawCall dc)
  {
    if ((UnityEngine.Object) dc != (UnityEngine.Object) null)
    {
      dc.isDirty = false;
      int widgetCount = 0;
      int index = 0;
      while (index < this.widgets.Count)
      {
        UIWidget uiWidget = this.widgets[index];
        if ((UnityEngine.Object) uiWidget == (UnityEngine.Object) null)
        {
          this.widgets.RemoveAt(index);
        }
        else
        {
          if ((UnityEngine.Object) uiWidget.drawCall == (UnityEngine.Object) dc)
          {
            if (uiWidget.isVisible && uiWidget.hasVertices)
            {
              ++widgetCount;
              if (this.generateNormals)
                uiWidget.WriteToBuffers(dc.verts, dc.uvs, dc.cols, dc.norms, dc.tans);
              else
                uiWidget.WriteToBuffers(dc.verts, dc.uvs, dc.cols, (BetterList<Vector3>) null, (BetterList<Vector4>) null);
              if (uiWidget.mOnRender != null)
              {
                if (this.mOnRender == null)
                  this.mOnRender = uiWidget.mOnRender;
                else
                  this.mOnRender += uiWidget.mOnRender;
              }
            }
            else
              uiWidget.drawCall = (UIDrawCall) null;
          }
          ++index;
        }
      }
      if (dc.verts.size != 0)
      {
        dc.UpdateGeometry(widgetCount);
        dc.onRender = this.mOnRender;
        this.mOnRender = (UIDrawCall.OnRenderCallback) null;
        return true;
      }
    }
    return false;
  }

  private void UpdateDrawCalls()
  {
    Transform cachedTransform1 = this.cachedTransform;
    bool usedForUi = this.usedForUI;
    if (this.clipping != UIDrawCall.Clipping.None)
    {
      this.drawCallClipRange = this.finalClipRegion;
      this.drawCallClipRange.z *= 0.5f;
      this.drawCallClipRange.w *= 0.5f;
    }
    else
      this.drawCallClipRange = Vector4.zero;
    if ((double) this.drawCallClipRange.z == 0.0)
      this.drawCallClipRange.z = (float) Screen.width * 0.5f;
    if ((double) this.drawCallClipRange.w == 0.0)
      this.drawCallClipRange.w = (float) Screen.height * 0.5f;
    if (this.halfPixelOffset)
    {
      this.drawCallClipRange.x -= 0.5f;
      this.drawCallClipRange.y += 0.5f;
    }
    Vector3 vector3;
    if (usedForUi)
    {
      Transform parent = this.cachedTransform.parent;
      Vector3 position = this.cachedTransform.localPosition;
      if ((UnityEngine.Object) parent != (UnityEngine.Object) null)
        position = parent.TransformPoint(position);
      vector3 = position + this.drawCallOffset;
    }
    else
      vector3 = cachedTransform1.position;
    Quaternion rotation = cachedTransform1.rotation;
    Vector3 lossyScale = cachedTransform1.lossyScale;
    for (int index = 0; index < this.drawCalls.Count; ++index)
    {
      UIDrawCall uiDrawCall = this.drawCalls[index];
      Transform cachedTransform2 = uiDrawCall.cachedTransform;
      cachedTransform2.position = vector3;
      cachedTransform2.rotation = rotation;
      cachedTransform2.localScale = lossyScale;
      uiDrawCall.renderQueue = this.renderQueue != UIPanel.RenderQueue.Explicit ? this.startingRenderQueue + index : this.startingRenderQueue;
      uiDrawCall.alwaysOnScreen = this.alwaysOnScreen && (this.mClipping == UIDrawCall.Clipping.None || this.mClipping == UIDrawCall.Clipping.ConstrainButDontClip);
      uiDrawCall.sortingOrder = this.mSortingOrder;
    }
  }

  private void UpdateLayers()
  {
    if (this.mLayer == this.cachedGameObject.layer)
      return;
    this.mLayer = this.mGo.layer;
    NGUITools.SetChildLayer(this.cachedTransform, this.mLayer);
    this.ResetAnchors();
    for (int index = 0; index < this.drawCalls.Count; ++index)
      this.drawCalls[index].gameObject.layer = this.mLayer;
  }

  private void UpdateWidgets()
  {
    bool flag1 = !this.cullWhileDragging && (double) this.mCullTime > (double) this.mUpdateTime;
    bool flag2 = false;
    if (this.mForced != flag1)
    {
      this.mForced = flag1;
      this.mResized = true;
    }
    bool cumulativeClipping = this.hasCumulativeClipping;
    int index = 0;
    for (int count = this.widgets.Count; index < count; ++index)
    {
      UIWidget w = this.widgets[index];
      if ((UnityEngine.Object) w.panel == (UnityEngine.Object) this && w.enabled)
      {
        int frameCount = Time.frameCount;
        if (w.UpdateTransform(frameCount) || this.mResized)
        {
          bool visibleByAlpha = flag1 || (double) w.CalculateCumulativeAlpha(frameCount) > 1.0 / 1000.0;
          w.UpdateVisibility(visibleByAlpha, flag1 || (!cumulativeClipping && !w.hideIfOffScreen || this.IsVisible(w)));
        }
        if (w.UpdateGeometry(frameCount))
        {
          flag2 = true;
          if (!this.mRebuild)
          {
            if ((UnityEngine.Object) w.drawCall != (UnityEngine.Object) null)
              w.drawCall.isDirty = true;
            else
              this.FindDrawCall(w);
          }
        }
      }
    }
    if (flag2 && this.onGeometryUpdated != null)
      this.onGeometryUpdated();
    this.mResized = false;
  }

  public UIDrawCall FindDrawCall(UIWidget w)
  {
    Material material = w.material;
    Texture mainTexture = w.mainTexture;
    int depth = w.depth;
    for (int index = 0; index < this.drawCalls.Count; ++index)
    {
      UIDrawCall uiDrawCall = this.drawCalls[index];
      int num1 = index != 0 ? this.drawCalls[index - 1].depthEnd + 1 : int.MinValue;
      int num2 = index + 1 != this.drawCalls.Count ? this.drawCalls[index + 1].depthStart - 1 : int.MaxValue;
      if (num1 <= depth && num2 >= depth)
      {
        if ((UnityEngine.Object) uiDrawCall.baseMaterial == (UnityEngine.Object) material && (UnityEngine.Object) uiDrawCall.mainTexture == (UnityEngine.Object) mainTexture)
        {
          if (w.isVisible)
          {
            w.drawCall = uiDrawCall;
            if (w.hasVertices)
              uiDrawCall.isDirty = true;
            return uiDrawCall;
          }
        }
        else
          this.mRebuild = true;
        return (UIDrawCall) null;
      }
    }
    this.mRebuild = true;
    return (UIDrawCall) null;
  }

  public void AddWidget(UIWidget w)
  {
    this.mUpdateScroll = true;
    if (this.widgets.Count == 0)
      this.widgets.Add(w);
    else if (this.mSortWidgets)
    {
      this.widgets.Add(w);
      this.SortWidgets();
    }
    else if (UIWidget.PanelCompareFunc(w, this.widgets[0]) == -1)
    {
      this.widgets.Insert(0, w);
    }
    else
    {
      int count = this.widgets.Count;
      while (count > 0)
      {
        if (UIWidget.PanelCompareFunc(w, this.widgets[--count]) != -1)
        {
          this.widgets.Insert(count + 1, w);
          break;
        }
      }
    }
    this.FindDrawCall(w);
  }

  public void RemoveWidget(UIWidget w)
  {
    if (!this.widgets.Remove(w) || !((UnityEngine.Object) w.drawCall != (UnityEngine.Object) null))
      return;
    int depth = w.depth;
    if (depth == w.drawCall.depthStart || depth == w.drawCall.depthEnd)
      this.mRebuild = true;
    w.drawCall.isDirty = true;
    w.drawCall = (UIDrawCall) null;
  }

  public void Refresh()
  {
    this.mRebuild = true;
    UIPanel.mUpdateFrame = -1;
    if (UIPanel.list.Count <= 0)
      return;
    UIPanel.list[0].LateUpdate();
  }

  public virtual Vector3 CalculateConstrainOffset(Vector2 min, Vector2 max)
  {
    Vector4 finalClipRegion = this.finalClipRegion;
    float num1 = finalClipRegion.z * 0.5f;
    float num2 = finalClipRegion.w * 0.5f;
    Vector2 minRect = new Vector2(min.x, min.y);
    Vector2 maxRect = new Vector2(max.x, max.y);
    Vector2 minArea = new Vector2(finalClipRegion.x - num1, finalClipRegion.y - num2);
    Vector2 maxArea = new Vector2(finalClipRegion.x + num1, finalClipRegion.y + num2);
    if (this.softBorderPadding && this.clipping == UIDrawCall.Clipping.SoftClip)
    {
      minArea.x += this.mClipSoftness.x;
      minArea.y += this.mClipSoftness.y;
      maxArea.x -= this.mClipSoftness.x;
      maxArea.y -= this.mClipSoftness.y;
    }
    return (Vector3) NGUIMath.ConstrainRect(minRect, maxRect, minArea, maxArea);
  }

  public bool ConstrainTargetToBounds(Transform target, ref Bounds targetBounds, bool immediate)
  {
    Vector3 constrainOffset = this.CalculateConstrainOffset((Vector2) targetBounds.min, (Vector2) targetBounds.max);
    if ((double) constrainOffset.sqrMagnitude <= 0.0)
      return false;
    if (immediate)
    {
      target.localPosition += constrainOffset;
      targetBounds.center += constrainOffset;
      SpringPosition component = target.GetComponent<SpringPosition>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
        component.enabled = false;
    }
    else
    {
      SpringPosition springPosition = SpringPosition.Begin(target.gameObject, target.localPosition + constrainOffset, 13f);
      springPosition.ignoreTimeScale = true;
      springPosition.worldSpace = false;
    }
    return true;
  }

  public bool ConstrainTargetToBounds(Transform target, bool immediate)
  {
    Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(this.cachedTransform, target);
    return this.ConstrainTargetToBounds(target, ref relativeWidgetBounds, immediate);
  }

  public static UIPanel Find(Transform trans)
  {
    return UIPanel.Find(trans, false, -1);
  }

  public static UIPanel Find(Transform trans, bool createIfMissing)
  {
    return UIPanel.Find(trans, createIfMissing, -1);
  }

  public static UIPanel Find(Transform trans, bool createIfMissing, int layer)
  {
    UIPanel inParents = NGUITools.FindInParents<UIPanel>(trans);
    if ((UnityEngine.Object) inParents != (UnityEngine.Object) null)
      return inParents;
    if (createIfMissing)
      return NGUITools.CreateUI(trans, false, layer);
    return (UIPanel) null;
  }

  private Vector2 GetWindowSize()
  {
    UIRoot root = this.root;
    Vector2 screenSize = NGUITools.screenSize;
    if ((UnityEngine.Object) root != (UnityEngine.Object) null)
      screenSize *= root.GetPixelSizeAdjustment(Mathf.RoundToInt(screenSize.y));
    return screenSize;
  }

  public Vector2 GetViewSize()
  {
    if (this.mClipping != UIDrawCall.Clipping.None)
      return new Vector2(this.mClipRange.z, this.mClipRange.w);
    return NGUITools.screenSize;
  }

  public enum RenderQueue
  {
    Automatic,
    StartAt,
    Explicit,
  }

  public delegate void OnGeometryUpdated();

  public delegate void OnClippingMoved(UIPanel panel);
}
