﻿// Decompiled with JetBrains decompiler
// Type: GUIShopSellJunkWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GUIShopSellJunkWindow : GUIPanel
{
  private readonly List<GUIPanel> pricePanels = new List<GUIPanel>();
  private readonly GUIImageNew tyliumPriceImage;
  private readonly GUILabelNew tyliumPriceLabel;

  public Price SellPrice
  {
    set
    {
      using (List<GUIPanel>.Enumerator enumerator = this.pricePanels.GetEnumerator())
      {
        while (enumerator.MoveNext())
          this.RemovePanel(enumerator.Current);
      }
      this.pricePanels.Clear();
      float num = -20f;
      using (Dictionary<ShipConsumableCard, float>.KeyCollection.Enumerator enumerator = value.items.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ShipConsumableCard current = enumerator.Current;
          GUIImageNew guiImageNew = new GUIImageNew(ResourceLoader.Load<Texture2D>(current.GUICard.GUIIcon), this.root);
          guiImageNew.PositionX = -30f;
          guiImageNew.PositionY = num;
          this.AddPanel((GUIPanel) guiImageNew);
          this.pricePanels.Add((GUIPanel) guiImageNew);
          GUILabelNew guiLabelNew = new GUILabelNew(((int) value.items[current]).ToString());
          guiLabelNew.Alignment = TextAnchor.MiddleRight;
          guiLabelNew.PositionX = (float) ((double) guiImageNew.PositionX + (double) guiLabelNew.Width * 0.5 + 10.0);
          guiLabelNew.PositionY = num;
          this.AddPanel((GUIPanel) guiLabelNew);
          this.pricePanels.Add((GUIPanel) guiLabelNew);
          num += 16f;
        }
      }
    }
  }

  public GUIShopSellJunkWindow(SmartRect parent)
    : base(parent)
  {
    Texture2D texture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/buybox");
    GUIImageNew guiImageNew = new GUIImageNew(texture);
    this.root.Width = guiImageNew.Rect.width;
    this.root.Height = guiImageNew.Rect.height;
    this.AddPanel((GUIPanel) guiImageNew);
    GUILabelNew guiLabelNew = new GUILabelNew("%$bgo.shop.sell_junk_confirm%");
    guiLabelNew.AutoSize = false;
    guiLabelNew.PositionY = -50f;
    guiLabelNew.Size = new float2((float) (texture.width - 80), 60f);
    guiLabelNew.WordWrap = true;
    this.AddPanel((GUIPanel) guiLabelNew);
    GUIButtonNew guiButtonNew1 = new GUIButtonNew("%$bgo.common.yes%");
    guiButtonNew1.Position = new float2(-66f, 60f);
    guiButtonNew1.Handler = new AnonymousDelegate(this.OnOkButton);
    this.AddPanel((GUIPanel) guiButtonNew1);
    GUIButtonNew guiButtonNew2 = new GUIButtonNew("%$bgo.common.no%");
    guiButtonNew2.Handler = new AnonymousDelegate(this.OnCancelButton);
    guiButtonNew2.Position = new float2(66f, 60f);
    this.AddPanel((GUIPanel) guiButtonNew2);
    this.IsRendered = true;
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    base.OnMouseDown(mousePosition, mouseKey);
    return this.IsRendered;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    base.OnMouseUp(mousePosition, mouseKey);
    return this.IsRendered;
  }

  public override void Show()
  {
    this.IsRendered = true;
    Game.InputManager.InputDispatcher.Focused = (InputListener) this;
  }

  public override void Hide()
  {
    this.IsRendered = false;
    Game.InputManager.InputDispatcher.Focused = (InputListener) null;
  }

  private void OnCancelButton()
  {
    this.Hide();
  }

  private void OnOkButton()
  {
    FacadeFactory.GetInstance().SendMessage(Message.ShopSellJunk);
    this.Hide();
  }
}
