﻿// Decompiled with JetBrains decompiler
// Type: ObjectRegistry
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class ObjectRegistry
{
  private readonly Dictionary<int, HashSet<SpaceObject>> loadedObjects = new Dictionary<int, HashSet<SpaceObject>>();
  private readonly HashSet<SpaceObject> allLoaded = new HashSet<SpaceObject>();
  private readonly Dictionary<uint, SpaceObject> spaceObjectCache = new Dictionary<uint, SpaceObject>();
  private readonly Dictionary<uint, PostCreateAction> postCreateActions = new Dictionary<uint, PostCreateAction>();
  private readonly Dictionary<uint, SpaceObjectState> stateBuffer = new Dictionary<uint, SpaceObjectState>();

  public event SpaceObjectEventHandler OnChanged;

  public void CreateSpaceObject(uint objectID, BgoProtocolReader r)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ObjectRegistry.\u003CCreateSpaceObject\u003Ec__AnonStorey11C objectCAnonStorey11C = new ObjectRegistry.\u003CCreateSpaceObject\u003Ec__AnonStorey11C();
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey11C.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    if (!this.spaceObjectCache.TryGetValue(objectID, out objectCAnonStorey11C.obj))
    {
      // ISSUE: reference to a compiler-generated field
      objectCAnonStorey11C.obj = SectorFactory.CreateSpaceObject(objectID);
      // ISSUE: reference to a compiler-generated field
      this.spaceObjectCache.Add(objectID, objectCAnonStorey11C.obj);
      // ISSUE: reference to a compiler-generated field
      objectCAnonStorey11C.obj.Read(r);
      // ISSUE: reference to a compiler-generated field
      objectCAnonStorey11C.obj.OnRead();
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      objectCAnonStorey11C.obj.IsLoaded.AddHandler(new SignalHandler(objectCAnonStorey11C.\u003C\u003Em__2B0));
      // ISSUE: reference to a compiler-generated field
      objectCAnonStorey11C.obj.IsLoaded.Set();
    }
    else
      Log.Warning((object) ("ObjectRegistry create old Object " + (object) objectID));
  }

  public void UpdateState(SpaceObjectState state)
  {
    SpaceObject spaceObject = this.Get(state.ObjectId);
    if (spaceObject != null && (bool) spaceObject.IsLoaded)
    {
      spaceObject.UpdateState(state);
    }
    else
    {
      if (this.stateBuffer.ContainsKey(state.ObjectId))
      {
        if (this.stateBuffer[state.ObjectId].Revision > state.Revision)
          return;
        this.stateBuffer.Remove(state.ObjectId);
      }
      this.stateBuffer.Add(state.ObjectId, state);
    }
  }

  private void CheckPostCreateActions(SpaceObject checkObj)
  {
    if (this.postCreateActions.ContainsKey(checkObj.ObjectID))
    {
      PostCreateAction postCreateAction;
      this.postCreateActions.TryGetValue(checkObj.ObjectID, out postCreateAction);
      if (postCreateAction.ActionType == PostCreateActionType.MarkAsWaypoint)
        checkObj.SetMarkObject(SpaceObject.MarkObjectType.Waypoint);
      this.postCreateActions.Remove(checkObj.ObjectID);
    }
    if (!this.stateBuffer.ContainsKey(checkObj.ObjectID))
      return;
    checkObj.UpdateState(this.stateBuffer[checkObj.ObjectID]);
    this.stateBuffer.Remove(checkObj.ObjectID);
  }

  public void SetPostCreateAction(uint objectID, PostCreateAction postCreateAction)
  {
    this.postCreateActions.Add(objectID, postCreateAction);
  }

  public void RemovePostCreateAction(uint objectID)
  {
    if (!this.postCreateActions.ContainsKey(objectID))
      return;
    this.postCreateActions.Remove(objectID);
  }

  public SpaceObject Get(uint objectID)
  {
    SpaceObject spaceObject = (SpaceObject) null;
    this.spaceObjectCache.TryGetValue(objectID, out spaceObject);
    return spaceObject;
  }

  public T[] GetLoaded<T>() where T : SpaceObject
  {
    int hashCode = typeof (T).GetHashCode();
    HashSet<SpaceObject> spaceObjectSet = (HashSet<SpaceObject>) null;
    if (!this.loadedObjects.TryGetValue(hashCode, out spaceObjectSet))
      return new T[0];
    T[] objArray = new T[spaceObjectSet.Count];
    spaceObjectSet.CopyTo((SpaceObject[]) objArray, 0);
    return objArray;
  }

  public SpaceObject[] GetLoaded(System.Type type)
  {
    int hashCode = type.GetHashCode();
    HashSet<SpaceObject> spaceObjectSet = (HashSet<SpaceObject>) null;
    if (!this.loadedObjects.TryGetValue(hashCode, out spaceObjectSet))
      return new SpaceObject[0];
    SpaceObject[] array = new SpaceObject[spaceObjectSet.Count];
    spaceObjectSet.CopyTo(array, 0);
    return array;
  }

  public SpaceObject[] GetLoaded()
  {
    List<SpaceObject> spaceObjectList = new List<SpaceObject>();
    using (Dictionary<int, HashSet<SpaceObject>>.KeyCollection.Enumerator enumerator1 = this.loadedObjects.Keys.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        using (HashSet<SpaceObject>.Enumerator enumerator2 = this.loadedObjects[enumerator1.Current].GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            SpaceObject current = enumerator2.Current;
            spaceObjectList.Add(current);
          }
        }
      }
    }
    return spaceObjectList.ToArray();
  }

  public IEnumerable<SpaceObject> GetAllLoaded()
  {
    return (IEnumerable<SpaceObject>) this.allLoaded;
  }

  public IEnumerable<SpaceObject> GetAll()
  {
    return (IEnumerable<SpaceObject>) this.spaceObjectCache.Values;
  }

  public void Remove(uint id, RemovingCause removingCause)
  {
    SpaceObject spaceObject;
    if (!this.spaceObjectCache.TryGetValue(id, out spaceObject))
      return;
    FacadeFactory.GetInstance().SendMessage(Message.EntityRemoved, (object) spaceObject);
    spaceObject.Remove(removingCause);
    this.spaceObjectCache.Remove(id);
    using (Dictionary<int, HashSet<SpaceObject>>.ValueCollection.Enumerator enumerator = this.loadedObjects.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Remove(spaceObject);
    }
    this.allLoaded.Remove(spaceObject);
    this.Notify(spaceObject, SpaceObjectEvent.Removed);
    this.RemovePostCreateAction(id);
  }

  public void AddManeuver(uint objectID, Maneuver maneuver, Tick syncTick, MovementFrame syncFrame)
  {
    SpaceObject spaceObject = this.Get(objectID);
    if (spaceObject == null)
      return;
    spaceObject.MovementController.AddManeuver(maneuver);
    if (!syncFrame.valid)
      return;
    spaceObject.MovementController.AddSyncFrame(syncTick, syncFrame);
  }

  public void Advance()
  {
    Tick tick = Tick.Last + 1;
    while (tick <= Tick.Current)
    {
      using (Dictionary<uint, SpaceObject>.ValueCollection.Enumerator enumerator = this.spaceObjectCache.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.MovementController.Advance(tick);
      }
      using (Dictionary<uint, SpaceObject>.ValueCollection.Enumerator enumerator = this.spaceObjectCache.Values.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.MovementController.PostAdvance();
      }
      ++tick;
    }
  }

  public void ObjectLoaded(SpaceObject obj)
  {
    int hashCode = obj.GetType().GetHashCode();
    HashSet<SpaceObject> spaceObjectSet = (HashSet<SpaceObject>) null;
    if (!this.loadedObjects.TryGetValue(hashCode, out spaceObjectSet))
    {
      spaceObjectSet = new HashSet<SpaceObject>();
      this.loadedObjects.Add(hashCode, spaceObjectSet);
    }
    spaceObjectSet.Add(obj);
    this.allLoaded.Add(obj);
    this.Notify(obj, SpaceObjectEvent.Loaded);
    FacadeFactory.GetInstance().SendMessage(Message.EntityLoaded, (object) obj);
  }

  public void Clear()
  {
    using (Dictionary<uint, SpaceObject>.ValueCollection.Enumerator enumerator = this.spaceObjectCache.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Remove(RemovingCause.Disconnection);
    }
    this.spaceObjectCache.Clear();
    this.loadedObjects.Clear();
    this.allLoaded.Clear();
    this.postCreateActions.Clear();
  }

  public void Notify(SpaceObject obj, SpaceObjectEvent notification)
  {
    if (this.OnChanged == null)
      return;
    this.OnChanged(obj, notification);
  }
}
