﻿// Decompiled with JetBrains decompiler
// Type: TournamentProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class TournamentProtocol : BgoProtocol
{
  public TournamentProtocol.TournamentDelegate OnTournamentParticipant;

  public TournamentProtocol()
    : base(BgoProtocol.ProtocolID.Tournament)
  {
  }

  public static TournamentProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Tournament) as TournamentProtocol;
  }

  public void RequestScoreboardPosition(uint playerId)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 13);
    bw.Write(playerId);
    this.SendMessage(bw);
  }

  public void RequestScoreboard(uint position, ushort count)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 11);
    bw.Write(position);
    bw.Write(count);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    TournamentProtocol.Reply reply = (TournamentProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case TournamentProtocol.Reply.Participant:
        bool participantFlag = br.ReadBoolean();
        if (this.OnTournamentParticipant != null)
          this.OnTournamentParticipant(participantFlag);
        Game.Me.TournamentParticipant = participantFlag;
        break;
      case TournamentProtocol.Reply.Active:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        TournamentProtocol.\u003CParseMessage\u003Ec__AnonStorey100 messageCAnonStorey100_1 = new TournamentProtocol.\u003CParseMessage\u003Ec__AnonStorey100();
        // ISSUE: variable of a compiler-generated type
        TournamentProtocol.\u003CParseMessage\u003Ec__AnonStorey100 messageCAnonStorey100_2 = messageCAnonStorey100_1;
        bool flag = br.ReadBoolean();
        Game.Tournament.IsActive = flag;
        int num1 = flag ? 1 : 0;
        // ISSUE: reference to a compiler-generated field
        messageCAnonStorey100_2.active = num1 != 0;
        uint cardGUID = br.ReadUInt32();
        Game.Tournament.EndTime = br.ReadDateTime();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStorey100_1.tournamentCard = (TournamentCard) Game.Catalogue.FetchCard(cardGUID, CardView.Tournament);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStorey100_1.tournamentCard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStorey100_1.\u003C\u003Em__253));
        break;
      case TournamentProtocol.Reply.PersonalMessage:
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new TournamentKillNotification((PersonalTournamentMessage) br.ReadByte(), br.ReadUInt32()));
        break;
      case TournamentProtocol.Reply.GlobalMessage:
        GlobalTournamentMessage tournamentMessage = (GlobalTournamentMessage) br.ReadByte();
        uint index1 = br.ReadUInt32();
        uint index2 = br.ReadUInt32();
        Player player1 = Game.Players[index1];
        Player player2 = Game.Players[index2];
        GuiTournamentEventWindow tournamentEventWindow = Game.GUIManager.Find<GuiTournamentEventWindow>();
        switch (tournamentMessage)
        {
          case GlobalTournamentMessage.Kill:
            tournamentEventWindow.AddKillSpamMessage(KillSpamMessageFactory.CreatePlainKillMessage(player1, player2));
            return;
          case GlobalTournamentMessage.KillingSpree:
            tournamentEventWindow.AddKillSpamMessage(KillSpamMessageFactory.CreateKillingSpreeMessage(player1));
            return;
          case GlobalTournamentMessage.AceKill:
            tournamentEventWindow.AddKillSpamMessage(KillSpamMessageFactory.CreateAceKillMessage(player1, player2));
            return;
          case GlobalTournamentMessage.NemesisKill:
            tournamentEventWindow.AddKillSpamMessage(KillSpamMessageFactory.CreateNemesisKillMessage(player1, player2));
            return;
          case GlobalTournamentMessage.RevengeKill:
            tournamentEventWindow.AddKillSpamMessage(KillSpamMessageFactory.CreateRevengeKillMessage(player1, player2));
            return;
          case GlobalTournamentMessage.Death:
            tournamentEventWindow.AddKillSpamMessage(KillSpamMessageFactory.CreateDeathMessage(player1, player2));
            return;
          default:
            return;
        }
      case TournamentProtocol.Reply.NemesisList:
        using (List<uint>.Enumerator enumerator = br.ReadUInt32List().GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            Player player3 = Game.Players[enumerator.Current];
            if (player3 != null)
              player3.Nemesis = true;
          }
          break;
        }
      case TournamentProtocol.Reply.NemesisAdd:
        Player player4 = Game.Players[br.ReadUInt32()];
        if (player4 == null)
          break;
        player4.Nemesis = true;
        break;
      case TournamentProtocol.Reply.NemesisRemove:
        Player player5 = Game.Players[br.ReadUInt32()];
        if (player5 == null)
          break;
        player5.Nemesis = false;
        break;
      case TournamentProtocol.Reply.ValidationError:
        FacadeFactory.GetInstance().SendMessage(Message.TournamentEnterFailed, (object) (TournamentValidationError) br.ReadByte());
        break;
      case TournamentProtocol.Reply.Scoreboard:
        FacadeFactory.GetInstance().SendMessage(Message.TournamentRankingList, (object) br.ReadDescList<RankEntry>());
        break;
      case TournamentProtocol.Reply.ScoreboardPosition:
        uint num2 = br.ReadUInt32();
        uint num3 = br.ReadUInt32();
        if ((int) num2 != (int) Game.Me.ServerID)
          break;
        FacadeFactory.GetInstance().SendMessage(Message.TournamentOwnRank, (object) (ushort) num3);
        break;
      case TournamentProtocol.Reply.TournamentStats:
        uint num4 = br.ReadUInt32();
        uint num5 = (uint) br.ReadUInt16();
        uint num6 = (uint) br.ReadUInt16();
        GuiTournamentEventWindow straight = Game.GUIManager.FindStraight<GuiTournamentEventWindow>();
        if (straight == null)
          break;
        straight.Score = Game.Tournament.TournamentScore = num4;
        straight.Kills = Game.Tournament.TournamentKills = num6;
        straight.Deaths = Game.Tournament.TournamentDeaths = num5;
        break;
    }
  }

  public enum Reply : ushort
  {
    Medals = 1,
    Participant = 2,
    Active = 3,
    PersonalMessage = 4,
    GlobalMessage = 5,
    NemesisList = 6,
    NemesisAdd = 7,
    NemesisRemove = 8,
    ValidationError = 9,
    Scoreboard = 10,
    ScoreboardPosition = 12,
    TournamentStats = 14,
  }

  private enum Request : ushort
  {
    Scoreboard = 11,
    ScoreboardPosition = 13,
  }

  public delegate void TournamentDelegate(bool participantFlag);
}
