﻿// Decompiled with JetBrains decompiler
// Type: ChatFleetChannelUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

internal class ChatFleetChannelUpdate : ChatProtocolParser
{
  private static readonly string impliedFormat = "by%2499|Fleet-Cylon|0|2|0|0|1}2504|Fleet-Colonial|0|1|0|0|0}2505|Fleet-Cylon|0|2|0|0|0#";
  public ChatFleetChannelUpdate.Room Local;
  public ChatFleetChannelUpdate.Room Global;

  public ChatFleetChannelUpdate()
  {
    this.type = ChatProtocolParserType.FleetChannelUpdate;
  }

  public override bool TryParse(string textMessage)
  {
    textMessage = textMessage.ToLower();
    List<string> input = new List<string>((IEnumerable<string>) textMessage.Split('%', '|', '}', '#'));
    if (input[0] != "by")
      return false;
    input.RemoveAt(0);
    input.RemoveAt(input.Count - 1);
    List<ChatFleetChannelUpdate.Room> roomList = new List<ChatFleetChannelUpdate.Room>();
    int numChunks = 0;
    ChatFleetChannelUpdate.Room room1;
    while (this.ReadRoom(input, out room1, out numChunks))
    {
      roomList.Add(room1);
      input.RemoveRange(0, numChunks);
    }
    roomList.ForEach((System.Action<ChatFleetChannelUpdate.Room>) (room =>
    {
      if (!room.name.Contains("fleet") || !room.name.Contains(Game.Me.Faction.ToString().ToLower()))
        return;
      room.name = "fleet" + (!room.global ? "Local" : "Global");
    }));
    this.Local = roomList.Find((Predicate<ChatFleetChannelUpdate.Room>) (match => match.name == "fleetLocal"));
    this.Global = roomList.Find((Predicate<ChatFleetChannelUpdate.Room>) (match => match.name == "fleetGlobal"));
    return input.Count == 0;
  }

  private bool ReadRoom(List<string> input, out ChatFleetChannelUpdate.Room room, out int numChunks)
  {
    room = new ChatFleetChannelUpdate.Room();
    numChunks = 0;
    if (input.Count < 7 || !int.TryParse(input[0], out room.id))
      return false;
    room.name = input[1];
    if (!Console.ParseBool(input[6], out room.global))
      return false;
    numChunks = 7;
    return true;
  }

  public class Room
  {
    public string name = string.Empty;
    public int id;
    public bool global;

    public override string ToString()
    {
      return string.Format("{0}, id:{1}, global:{2}", (object) this.name, (object) this.id, (object) this.global);
    }
  }
}
