﻿// Decompiled with JetBrains decompiler
// Type: DamageOverlayMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class DamageOverlayMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "DamageOverlayMediator";
  private DamageOverlay view;

  public DamageOverlayMediator()
    : base("DamageOverlayMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.LevelLoaded);
    this.AddMessageInterest(Message.PlayerFactionReply);
    this.AddMessageInterest(Message.DamageReceivedInfo);
    this.AddMessageInterest(Message.Show3DSectorMap);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.PlayerFactionReply:
        this.view.SetFaction((Faction) message.Data);
        break;
      case Message.LoadNewLevel:
        this.view.gameObject.SetActive(false);
        this.view.Set3DSectorMapImplementedByMartinWithVeryGoodCodeOpen(false);
        break;
      case Message.LevelLoaded:
        this.view.gameObject.SetActive(message.Data is SpaceLevel);
        break;
      default:
        if (id != Message.UiCreated)
        {
          if (id != Message.Show3DSectorMap)
          {
            if (id != Message.DamageReceivedInfo)
              break;
            if ((Object) this.view == (Object) null)
            {
              Debug.LogWarning((object) "DamageOverlay not initialized");
              break;
            }
            this.view.VisualizeDamage((DamageCombatInfo) message.Data);
            break;
          }
          this.view.Set3DSectorMapImplementedByMartinWithVeryGoodCodeOpen((bool) message.Data);
          break;
        }
        this.CreateView();
        break;
    }
  }

  private void CreateView()
  {
    this.view = UguiTools.CreateChild("DamageOverlay", UguiTools.GetCanvas(BsgoCanvas.Hud).transform).GetComponent<DamageOverlay>();
  }
}
