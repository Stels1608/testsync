﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DWindowViewUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections;
using System.Diagnostics;
using SystemMap3D;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class SystemMap3DWindowViewUi : Window, ISystemMap3DView
{
  [SerializeField]
  private SystemMap3DSettingsUgui settingsWindow;
  [SerializeField]
  private SettingWidgetsArea filterArea;
  [SerializeField]
  private EventTrigger nav3dAreaEventTrigger;
  [SerializeField]
  private Button exitButton;
  private string messageText;
  private IEnumerator _textBlinkerRoutine;

  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  protected override bool IsInCombatGui
  {
    get
    {
      return false;
    }
  }

  public override bool IsRendered
  {
    get
    {
      return base.IsRendered;
    }
    set
    {
      base.IsRendered = value;
      if (!value)
        return;
      this.OnRenderingEnabled();
    }
  }

  public EventTrigger Nav3DAreaEventTrigger
  {
    get
    {
      return this.nav3dAreaEventTrigger;
    }
  }

  public static SystemMap3DWindowViewUi Create(SystemMap3DManager systemMap3DManager)
  {
    SystemMap3DWindowViewUi child = UguiTools.CreateChild<SystemMap3DWindowViewUi>("SystemMap3D/SystemMap3DWindowViewUgui", UguiTools.GetCanvas(BsgoCanvas.SystemMapCanvas2D).transform);
    child.Initialize();
    return child;
  }

  private void Initialize()
  {
    this.SetMessageText(string.Empty);
    this.SetupSettingAreas();
  }

  private void Update()
  {
    this.UpdateLocationField();
  }

  private void SetupSettingAreas()
  {
    this.settingsWindow.SettingWidgetsArea.AddTextCheckbox(UserSetting.SystemMap3DFormAsteroidGroups);
    this.settingsWindow.SettingWidgetsArea.AddEnumSettingWidget<TransitionMode>(UserSetting.SystemMap3DTransitionMode);
    this.settingsWindow.SettingWidgetsArea.AddEnumSettingWidget<CameraView>(UserSetting.SystemMap3DCameraView);
    this.filterArea.AddIconCheckboxSmall(UserSetting.SystemMap3DShowAsteroids, SystemMap3DMapIconLinker.Instance.MapObjectsNPCs[9]);
    this.filterArea.AddIconCheckboxSmall(UserSetting.SystemMap3DShowDynamicMissions, SystemMap3DMapIconLinker.Instance.SectorEventIcon);
  }

  private void UpdateLocationField()
  {
    Vector3 vector3 = new Vector3();
    Vector3 getSectorSizeV3 = SpaceLevel.GetLevel().GetSectorSizeV3;
    vector3.x = (float) ((double) Game.Me.Ship.Position.x / ((double) getSectorSizeV3.x / 2.0) * 100.0);
    vector3.y = (float) ((double) Game.Me.Ship.Position.z / ((double) getSectorSizeV3.x / 2.0) * 100.0);
    vector3.z = (float) ((double) Game.Me.Ship.Position.y / ((double) getSectorSizeV3.x / 2.0) * 100.0);
    this.SetLocationVector(string.Format("{0:##0.0}, {1:##0.0}, {2:##0.0}", (object) vector3.x, (object) vector3.y, (object) vector3.z));
  }

  public void TransitionToMap(TransitionMode transitionMode)
  {
    this.exitButton.interactable = true;
    this.IsRendered = true;
    this.StopAllCoroutines();
    switch (transitionMode)
    {
      case TransitionMode.Instant:
        this.FadeUi(9999f);
        break;
      case TransitionMode.Smooth:
        this.FadeUi(1f);
        break;
    }
    this.StartCoroutine(this.SetSectorNameWithFx(SpaceLevel.GetLevel().Card.GUICard.Name, 1f));
  }

  public void TransitionToGame(TransitionMode transitionMode)
  {
    this.exitButton.interactable = false;
    this.StopAllCoroutines();
    this.SetBlinkingMessageText(string.Empty);
    switch (transitionMode)
    {
      case TransitionMode.Instant:
        this.IsRendered = false;
        break;
      case TransitionMode.Smooth:
        if (!this.gameObject.activeInHierarchy)
          break;
        this.FadeUi(-1f);
        this.StartCoroutine(this.ToggleRenderingDelayed(1f, false));
        break;
    }
  }

  public void OnTransitionToGameFinished()
  {
  }

  private void FadeUi(float speed)
  {
    Animation component = this.GetComponent<Animation>();
    if ((double) speed < 1.0)
      component.GetComponent<Animation>()["UiFadeIn"].normalizedTime = 1f;
    component.GetComponent<Animation>()["UiFadeIn"].speed = speed;
    component.Play("UiFadeIn");
  }

  public void ApplySetting(UserSetting setting, object data)
  {
  }

  [DebuggerHidden]
  public IEnumerator ToggleRenderingDelayed(float delay, bool isRendered)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SystemMap3DWindowViewUi.\u003CToggleRenderingDelayed\u003Ec__Iterator48() { delay = delay, isRendered = isRendered, \u003C\u0024\u003Edelay = delay, \u003C\u0024\u003EisRendered = isRendered, \u003C\u003Ef__this = this };
  }

  protected abstract void SetSectorNameText(string labelText);

  [DebuggerHidden]
  private IEnumerator SetSectorNameWithFx(string sectorName, float initDelay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SystemMap3DWindowViewUi.\u003CSetSectorNameWithFx\u003Ec__Iterator49() { initDelay = initDelay, sectorName = sectorName, \u003C\u0024\u003EinitDelay = initDelay, \u003C\u0024\u003EsectorName = sectorName, \u003C\u003Ef__this = this };
  }

  protected virtual void SetMessageText(string messageText)
  {
    this.messageText = messageText;
  }

  public void SetBlinkingMessageText(string labelText)
  {
    if (!this.gameObject.activeInHierarchy)
      return;
    if (this._textBlinkerRoutine != null)
      this.StopCoroutine(this._textBlinkerRoutine);
    this._textBlinkerRoutine = this.ProcessBlinkingMessageText(labelText);
    this.StartCoroutine(this._textBlinkerRoutine);
  }

  public void ShowThreatWarning(bool show)
  {
    if (!this.IsRendered)
      return;
    string message = Tools.ParseMessage("%$bgo.sector_map.message.in_threat%");
    if (show)
    {
      this.SetBlinkingMessageText(message);
    }
    else
    {
      if (!(this.messageText == message))
        return;
      this.SetBlinkingMessageText(string.Empty);
    }
  }

  protected abstract void SetLocationVector(string vectorText);

  private void OnRenderingEnabled()
  {
    this.ShowThreatWarning(Game.Me.Stats.InCombat);
  }

  [DebuggerHidden]
  private IEnumerator ProcessBlinkingMessageText(string labelText)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SystemMap3DWindowViewUi.\u003CProcessBlinkingMessageText\u003Ec__Iterator4A() { labelText = labelText, \u003C\u0024\u003ElabelText = labelText, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator Wait(float duration)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SystemMap3DWindowViewUi.\u003CWait\u003Ec__Iterator4B() { duration = duration, \u003C\u0024\u003Eduration = duration };
  }

  public void Destroy()
  {
    Object.Destroy((Object) this.gameObject);
  }

  public void ExitButtonPressed()
  {
    FacadeFactory.GetInstance().SendMessage(Message.Show3DSectorMap, (object) false);
  }
}
