﻿// Decompiled with JetBrains decompiler
// Type: MouseLookCharacterController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MouseLookCharacterController : MonoBehaviour
{
  public float sensitivityX = 15f;
  public float sensitivityY = 15f;
  private CharacterMotor motor;
  private float rotationX;
  private float rotationY;
  private Quaternion originalRotation;

  private void Start()
  {
    this.motor = this.GetComponent(typeof (CharacterMotor)) as CharacterMotor;
    if ((Object) this.motor == (Object) null)
      Debug.Log((object) "Motor is null!!");
    this.originalRotation = this.transform.localRotation;
  }

  private void Update()
  {
    this.rotationX += Input.GetAxis("Mouse X") * this.sensitivityX;
    this.rotationY += Input.GetAxis("Mouse Y") * this.sensitivityY;
    this.rotationX += (float) ((double) Input.GetAxis("Horizontal2") * (double) Time.deltaTime * 300.0);
    this.rotationY += (float) ((double) Input.GetAxis("Vertical2") * (double) Time.deltaTime * 300.0);
    this.rotationX = Mathf.Repeat(this.rotationX, 360f);
    this.rotationY = Mathf.Clamp(this.rotationY, -85f, 85f);
    this.motor.desiredFacingDirection = this.originalRotation * Quaternion.AngleAxis(this.rotationX, Vector3.up) * Quaternion.AngleAxis(this.rotationY, -Vector3.right) * Vector3.forward;
  }
}
