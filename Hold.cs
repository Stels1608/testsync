﻿// Decompiled with JetBrains decompiler
// Type: Hold
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;

public class Hold : ItemList
{
  public const int MaxHoldSize = 70;

  public bool IsFull
  {
    get
    {
      return this.Count >= 70;
    }
  }

  public Hold()
    : base((IContainerID) new Hold.HoldContainerID())
  {
  }

  public bool HaveMoneyBuy(ShipItem item, int count)
  {
    if (item != null && (bool) item.IsLoaded)
      return item.ShopItemCard.BuyPrice.IsEnoughInHangar(item.ShopItemCard, count);
    return false;
  }

  public bool HaveMoneyBuyUpgrade(ShopItemCard shopItemCard, int count)
  {
    if (shopItemCard != null && (bool) shopItemCard.IsLoaded)
      return shopItemCard.UpgradePrice.IsEnoughInHangar(shopItemCard, count);
    return false;
  }

  public bool HaveConsumableToActivate(string key)
  {
    for (int index = 0; index < this.items.Count; ++index)
    {
      if (this.items[index].ItemGUICard.Key == key)
        return true;
    }
    return false;
  }

  public bool IsFilledHold(ItemCountable item)
  {
    if (this.Count < 70)
      return false;
    foreach (Card card in (IEnumerable<ShipItem>) this)
    {
      if ((int) card.CardGUID == (int) item.CardGUID)
        return false;
    }
    return true;
  }

  protected override void Changed()
  {
    base.Changed();
    this.AutoUse();
    FacadeFactory.GetInstance().SendMessage(Message.PlayerHoldChanged, (object) this);
  }

  public bool ContainsJunk()
  {
    return this.Any<ShipItem>((Func<ShipItem, bool>) (item => item.ShopItemCard.ItemType == ShopItemType.Junk));
  }

  public class HoldContainerID : IContainerID, IProtocolWrite
  {
    void IProtocolWrite.Write(BgoProtocolWriter w)
    {
      w.Write((byte) 1);
    }

    public ContainerType GetContainerType()
    {
      return ContainerType.Hold;
    }
  }
}
