﻿// Decompiled with JetBrains decompiler
// Type: GuiUtils
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public static class GuiUtils
{
  public static List<string> CreateNumberSequenceStringList(int from, int to, string prefix = "")
  {
    List<string> stringList = new List<string>();
    for (int index = from; index <= to; ++index)
      stringList.Add(prefix + index.ToString());
    return stringList;
  }

  public static List<GUIContent> CreateNumberSequenceGUIContentList(List<int> intList, string tooltipPrefix, string textPrefix = "")
  {
    List<GUIContent> guiContentList = new List<GUIContent>();
    using (List<int>.Enumerator enumerator = intList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int current = enumerator.Current;
        GUIContent guiContent = new GUIContent(textPrefix + current.ToString(), tooltipPrefix + current.ToString());
        guiContentList.Add(guiContent);
      }
    }
    return guiContentList;
  }

  public static List<GUIContent> CreateNumberSequenceGUIContentList(int from, int to, string tooltipPrefix, string textPrefix = "")
  {
    List<GUIContent> guiContentList = new List<GUIContent>();
    for (int index = from; index <= to; ++index)
    {
      GUIContent guiContent = new GUIContent(textPrefix + index.ToString(), tooltipPrefix + index.ToString());
      guiContentList.Add(guiContent);
    }
    return guiContentList;
  }

  public static List<int> CreateNumberSequenceIntList(int from, int to)
  {
    List<int> intList = new List<int>();
    for (int index = from; index <= to; ++index)
      intList.Add(index);
    return intList;
  }

  public static List<GUIContent> CreateGUIContentFromStringList(List<string> stringList, string tooltipText)
  {
    List<GUIContent> guiContentList = new List<GUIContent>();
    using (List<string>.Enumerator enumerator = stringList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        guiContentList.Add(new GUIContent(current, tooltipText));
      }
    }
    return guiContentList;
  }

  public static Texture2D TintTexture(Texture2D tex, Color color)
  {
    Texture2D texture2D = new Texture2D(tex.width, tex.height);
    for (int x = 0; x < tex.width; ++x)
    {
      for (int y = 0; y < tex.height; ++y)
      {
        Color pixel = tex.GetPixel(x, y);
        texture2D.SetPixel(x, y, pixel * color);
      }
    }
    texture2D.Apply();
    return texture2D;
  }

  public static Color ColorFromHSV(float h, float s, float v, float a = 1)
  {
    if ((double) s == 0.0)
      return new Color(v, v, v, a);
    float num1 = h / 60f;
    int num2 = (int) num1;
    float num3 = num1 - (float) num2;
    float num4 = v * (1f - s);
    float num5 = v * (float) (1.0 - (double) s * (double) num3);
    float num6 = v * (float) (1.0 - (double) s * (1.0 - (double) num3));
    Color color = new Color(0.0f, 0.0f, 0.0f, a);
    switch (num2)
    {
      case 0:
        color.r = v;
        color.g = num6;
        color.b = num4;
        break;
      case 1:
        color.r = num5;
        color.g = v;
        color.b = num4;
        break;
      case 2:
        color.r = num4;
        color.g = v;
        color.b = num6;
        break;
      case 3:
        color.r = num4;
        color.g = num5;
        color.b = v;
        break;
      case 4:
        color.r = num6;
        color.g = num4;
        color.b = v;
        break;
      default:
        color.r = v;
        color.g = num4;
        color.b = num5;
        break;
    }
    return color;
  }

  public static void ColorToHSV(Color color, out float h, out float s, out float v)
  {
    float a = Mathf.Min(Mathf.Min(color.r, color.g), color.b);
    float num1 = Mathf.Max(Mathf.Max(color.r, color.g), color.b);
    float num2 = num1 - a;
    v = num1;
    if (!Mathf.Approximately(num1, 0.0f))
    {
      s = num2 / num1;
      if (Mathf.Approximately(a, num1))
      {
        v = num1;
        s = 0.0f;
        h = -1f;
      }
      else
      {
        h = (double) color.r != (double) num1 ? ((double) color.g != (double) num1 ? (float) (4.0 + ((double) color.r - (double) color.g) / (double) num2) : (float) (2.0 + ((double) color.b - (double) color.r) / (double) num2)) : (color.g - color.b) / num2;
        h = h * 60f;
        if ((double) h >= 0.0)
          return;
        h = h + 360f;
      }
    }
    else
    {
      s = 0.0f;
      h = -1f;
    }
  }
}
