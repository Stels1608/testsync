﻿// Decompiled with JetBrains decompiler
// Type: GuiProgressBar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GuiProgressBar : GuiPanel
{
  private readonly GuiColoredBox background;
  private readonly GuiColoredBox border;
  private readonly GuiColoredBox fill;
  private readonly GuiLabel progressLabel;
  private float progress;

  public float Progress
  {
    get
    {
      return this.progress;
    }
    set
    {
      this.progress = value;
      this.fill.SizeX = this.Progress * (this.SizeX - 4f);
      this.progressLabel.Text = ((int) ((double) value * 100.0)).ToString() + "%";
      if ((double) this.progress > 0.5)
      {
        this.progressLabel.PositionX = this.fill.SizeX - this.progressLabel.SizeX;
        this.progressLabel.NormalColor = this.Over50Color;
      }
      else
      {
        this.progressLabel.PositionX = this.fill.SizeX + 4f;
        this.progressLabel.NormalColor = this.Sub50Color;
      }
    }
  }

  public Color Over50Color { get; set; }

  public Color Sub50Color { get; set; }

  public GuiProgressBar(Vector2 size, Color backgroundColor, Color borderColor, Color fillColor)
  {
    this.Size = size;
    this.border = new GuiColoredBox(borderColor, size);
    this.AddChild((GuiElementBase) this.border);
    this.background = new GuiColoredBox(backgroundColor, Vector2.one, new Vector2(size.x - 2f, size.y - 2f));
    this.AddChild((GuiElementBase) this.background);
    this.fill = new GuiColoredBox(fillColor);
    this.fill.Position = this.background.Position + Vector2.one;
    this.fill.Size = this.background.Size - Vector2.one * 2f;
    this.AddChild((GuiElementBase) this.fill);
    this.progressLabel = new GuiLabel(Gui.Options.FontEurostileT_Med, 13);
    this.progressLabel.PositionY = 2f;
    this.AddChild((GuiElementBase) this.progressLabel);
  }
}
