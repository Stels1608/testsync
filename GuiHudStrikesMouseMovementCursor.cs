﻿// Decompiled with JetBrains decompiler
// Type: GuiHudStrikesMouseMovementCursor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GuiHudStrikesMouseMovementCursor
{
  private readonly Texture2D crosshair;
  private GameObject mouseCursorResetterObj;

  public GuiHudStrikesMouseMovementCursor()
  {
    this.crosshair = (Texture2D) ResourceLoader.Load("GUI/StrikeControlHudElements/MouseMovementCursor");
    this.crosshair = GuiUtils.TintTexture(this.crosshair, Gui.Options.StrikeHudElementsColor);
  }

  public void Activate()
  {
    Cursor.SetCursor(this.crosshair, new Vector2((float) ((double) this.crosshair.width / 2.0 - 1.0), (float) ((double) this.crosshair.height / 2.0 - 1.0)), CursorMode.Auto);
  }

  public void Deactivate()
  {
    Cursor.SetCursor((Texture2D) null, Vector2.zero, CursorMode.Auto);
  }
}
