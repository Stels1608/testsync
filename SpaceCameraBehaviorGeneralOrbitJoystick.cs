﻿// Decompiled with JetBrains decompiler
// Type: SpaceCameraBehaviorGeneralOrbitJoystick
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpaceCameraBehaviorGeneralOrbitJoystick : SpaceCameraBehaviorBase
{
  private float x;
  private float y;
  private float offsetAngle;
  private Quaternion baseRotation;
  private Quaternion rotationOffset;
  private JoystickLookAroundInputs joystickLookInput;
  private float zoomDiff;

  private Vector3 FocusPoint
  {
    get
    {
      return this.PlayerShip.Position + this.PlayerShip.Rotation * Vector3.up * this.PlayerShip.ShipBounds.y * 0.2f;
    }
  }

  public override bool BlocksControls
  {
    get
    {
      return false;
    }
  }

  public override bool ShowStrikesHud
  {
    get
    {
      return false;
    }
  }

  public override KeyCode SelectionMouseButton
  {
    get
    {
      return KeyCode.Mouse0;
    }
  }

  public SpaceCameraBehaviorGeneralOrbitJoystick()
  {
    this.joystickLookInput = new JoystickLookAroundInputs();
    this.zoomDiff = SpaceCameraBase.WantedZoomDistance - (this.FocusPoint - this.CamPosition).magnitude;
    this.baseRotation = Quaternion.LookRotation(this.FocusPoint - this.CamPosition, this.CamRotation * Vector3.up);
    this.rotationOffset = Quaternion.Inverse(this.baseRotation) * this.CamRotation;
  }

  public override SpaceCameraParameters CalculateCurrentCameraParameters()
  {
    Vector2 lookInputs = this.joystickLookInput.GetLookInputs();
    this.x += lookInputs.x * 1.5f;
    this.y += lookInputs.y * 1.5f;
    Quaternion quaternion1 = this.baseRotation * Quaternion.Euler(this.y, this.x, 0.0f);
    Vector3 vector3 = quaternion1 * Vector3.forward * (float) -((double) SpaceCameraBase.WantedZoomDistance - (double) this.zoomDiff) + this.FocusPoint;
    Quaternion quaternion2 = quaternion1 * this.rotationOffset;
    this.rotationOffset = Quaternion.Lerp(this.rotationOffset, Quaternion.identity, Time.deltaTime * 3.5f);
    this.Fov = Camera.main.fieldOfView;
    return new SpaceCameraParameters() { Fov = this.Fov, CamPosition = vector3, CamRotation = quaternion2 };
  }

  public override bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    if ((double) Mathf.Abs(magnitude) < (double) JoystickSetup.DeadZone)
      magnitude = 0.0f;
    if (JoystickLookAroundInputs.ActionIsJoystickLookInput(action))
      this.joystickLookInput.UpdateInputValue(action, magnitude);
    return base.OnJoystickAxesInput(triggerCode, modifierCode, action, magnitude, delta, isAxisInverted);
  }
}
