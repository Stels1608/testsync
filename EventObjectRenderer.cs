﻿// Decompiled with JetBrains decompiler
// Type: EventObjectRenderer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class EventObjectRenderer : MonoBehaviour
{
  public EventObject eventObject;

  private void OnDrawGizmos()
  {
    this.Draw();
  }

  private void OnDrawGizmosSelected()
  {
    this.Draw();
    this.DrawWireframe();
  }

  private void Draw()
  {
    Gizmos.color = new Color(0.854902f, 0.6470588f, 0.1254902f);
    Gizmos.DrawCube(this.transform.position, this.eventObject.SpawnBox);
  }

  private void DrawWireframe()
  {
    Gizmos.color = Color.green;
    Gizmos.DrawWireCube(this.transform.position, this.eventObject.SpawnBox);
  }
}
