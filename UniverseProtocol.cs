﻿// Decompiled with JetBrains decompiler
// Type: UniverseProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class UniverseProtocol : BgoProtocol
{
  private Galaxy galaxy;

  public UniverseProtocol(Galaxy galaxy)
    : base(BgoProtocol.ProtocolID.Universe)
  {
    this.galaxy = galaxy;
  }

  public void SubscribeGalaxyMap()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 5);
    this.SendMessage(bw);
  }

  public void UnsubscribeGalaxyMap()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 6);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    UniverseProtocol.Reply reply = (UniverseProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    if (reply == UniverseProtocol.Reply.Update)
      this.ReadGalaxyUpdates(br);
    else
      DebugUtility.LogError("Unknown MessageType in Universe protocol: " + (object) reply);
  }

  private void ReadGalaxyUpdates(BgoProtocolReader br)
  {
    int length = br.ReadLength();
    GalaxyMapUpdate[] galaxyMapUpdateArray = new GalaxyMapUpdate[length];
    for (int index = 0; index < length; ++index)
      galaxyMapUpdateArray[index] = this.ReadGalaxyUpdate(br);
    FacadeFactory.GetInstance().SendMessage(Message.GalaxyMapUpdates, (object) galaxyMapUpdateArray);
  }

  private GalaxyMapUpdate ReadGalaxyUpdate(BgoProtocolReader br)
  {
    GalaxyUpdateType galaxyUpdateType = (GalaxyUpdateType) br.ReadByte();
    switch (galaxyUpdateType)
    {
      case GalaxyUpdateType.Rcp:
        return (GalaxyMapUpdate) new GalaxyRcpUpdate((Faction) br.ReadByte(), br.ReadSingle());
      case GalaxyUpdateType.ConquestLocation:
        return (GalaxyMapUpdate) new ConquestLocationUpdate((Faction) br.ReadByte(), br.ReadInt32(), br.ReadDateTime());
      case GalaxyUpdateType.ConquestPrice:
        return (GalaxyMapUpdate) new ConquestPriceUpdate((Faction) br.ReadByte(), br.ReadUInt32());
      case GalaxyUpdateType.SectorOutpostPoints:
        uint sectorId1 = br.ReadUInt32();
        Faction faction1 = (Faction) br.ReadByte();
        int outpostPoints = br.ReadInt32();
        return (GalaxyMapUpdate) new SectorOutpostPointsUpdate(faction1, sectorId1, outpostPoints);
      case GalaxyUpdateType.SectorMiningShips:
        uint sectorId2 = br.ReadUInt32();
        Faction faction2 = (Faction) br.ReadByte();
        int miningShipCount = br.ReadInt32();
        return (GalaxyMapUpdate) new SectorMiningShipUpdate(faction2, sectorId2, miningShipCount);
      case GalaxyUpdateType.SectorPvPKills:
        uint sectorId3 = br.ReadUInt32();
        Faction faction3 = (Faction) br.ReadByte();
        int kills = br.ReadInt32();
        return (GalaxyMapUpdate) new SectorPvPKillUpdate(faction3, sectorId3, kills);
      case GalaxyUpdateType.SectorDynamicMissions:
        uint sectorId4 = br.ReadUInt32();
        Faction faction4 = (Faction) br.ReadByte();
        byte dynamicMissionCount = br.ReadByte();
        return (GalaxyMapUpdate) new SectorDynamicMissionUpdate(faction4, sectorId4, dynamicMissionCount);
      case GalaxyUpdateType.SectorOutpostState:
        uint sectorId5 = br.ReadUInt32();
        Faction faction5 = (Faction) br.ReadByte();
        float state1 = br.ReadSingle();
        return (GalaxyMapUpdate) new SectorOutpostStateUpdate(faction5, sectorId5, state1);
      case GalaxyUpdateType.SectorBeaconState:
        uint sectorId6 = br.ReadUInt32();
        Faction faction6 = (Faction) br.ReadByte();
        float state2 = br.ReadSingle();
        return (GalaxyMapUpdate) new SectorBeaconStateUpdate(faction6, sectorId6, state2);
      case GalaxyUpdateType.SectorJumpTargetTransponders:
        uint sectorId7 = br.ReadUInt32();
        Faction faction7 = (Faction) br.ReadByte();
        JumpTargetTransponderDesc[] descs = br.ReadDescArray<JumpTargetTransponderDesc>();
        return (GalaxyMapUpdate) new SectorJumpTargetTransponderUpdate(faction7, sectorId7, descs);
      case GalaxyUpdateType.SectorPlayerSlots:
        uint sectorId8 = br.ReadUInt32();
        Faction faction8 = (Faction) br.ReadByte();
        SectorSlotData sectorSlotData = new SectorSlotData();
        int num = br.ReadLength();
        for (int index = 0; index < num; ++index)
        {
          if ((int) br.ReadByte() == 1)
          {
            uint shipGuid = br.ReadGUID();
            uint current = br.ReadUInt32();
            uint max = br.ReadUInt32();
            sectorSlotData.AddShipSectorCap(shipGuid, current, max);
          }
          else
          {
            SectorSlotCapType sectorSlotCapType = (SectorSlotCapType) br.ReadByte();
            uint current = br.ReadUInt32();
            uint max = br.ReadUInt32();
            switch (sectorSlotCapType)
            {
              case SectorSlotCapType.Colonial:
                sectorSlotData.SetFactionSlots(Faction.Colonial, current, max);
                continue;
              case SectorSlotCapType.Cylon:
                sectorSlotData.SetFactionSlots(Faction.Cylon, current, max);
                continue;
              case SectorSlotCapType.Total:
                sectorSlotData.CurrentUsedSlots = current;
                sectorSlotData.MaxTotalSlots = max;
                continue;
              default:
                continue;
            }
          }
        }
        return (GalaxyMapUpdate) new SectorPlayerSlotUpdate(faction8, sectorId8, sectorSlotData);
      default:
        Debug.LogError((object) ("Unknown galaxy map update type: " + (object) galaxyUpdateType));
        return (GalaxyMapUpdate) null;
    }
  }

  public enum Request : ushort
  {
    SubscribeGalaxyMap = 5,
    UnsubscribeGalaxyMap = 6,
  }

  public enum Reply : ushort
  {
    Update = 7,
  }
}
