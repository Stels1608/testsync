﻿// Decompiled with JetBrains decompiler
// Type: TrajectoryVisualizer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class TrajectoryVisualizer
{
  private List<TimePoint> trajectory = new List<TimePoint>();
  private Color color;
  private float length;

  public TrajectoryVisualizer(Color color, float length)
  {
    this.color = color;
    this.length = length;
  }

  public void AddPoint(float time, Vector3 point)
  {
    this.trajectory.Add(new TimePoint(time, point));
    while ((double) this.trajectory[0].time < (double) time - (double) this.length)
      this.trajectory.RemoveAt(0);
  }

  public void Render()
  {
    if (this.trajectory.Count == 0)
      return;
    DrawArea drawArea = (DrawArea) new DrawArea3D(Vector3.zero, Vector3.one, Matrix4x4.identity);
    float num = this.trajectory[this.trajectory.Count - 1].time;
    GL.Begin(1);
    for (int index = 0; index < this.trajectory.Count - 1; ++index)
    {
      Color c = this.color;
      c.a = (num - this.trajectory[index].time) / this.length;
      c.a = (float) (1.0 - (double) c.a * (double) c.a);
      drawArea.DrawLine(this.trajectory[index].point, this.trajectory[index + 1].point, c);
    }
    GL.End();
  }
}
