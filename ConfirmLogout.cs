﻿// Decompiled with JetBrains decompiler
// Type: ConfirmLogout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class ConfirmLogout : GuiPanel
{
  public ConfirmLogout()
  {
    Loaders.Load((GuiPanel) this, "GUI/Options/confirmation_dialog_layout");
    this.Find<GuiButton>("yes").Pressed = (AnonymousDelegate) (() =>
    {
      SceneProtocol.GetProtocol().RequestDisconnect();
      this.IsRendered = false;
    });
    this.Find<GuiButton>("no").Pressed = (AnonymousDelegate) (() => this.IsRendered = false);
    this.Align = Align.UpCenter;
    this.Position = new Vector2(0.0f, 130f);
  }

  public static void Toggle()
  {
    ConfirmLogout confirmLogout = Game.GUIManager.Find<ConfirmLogout>();
    if (confirmLogout == null)
      Game.RegisterDialog((IGUIPanel) new ConfirmLogout(), true);
    else
      confirmLogout.IsRendered = !confirmLogout.IsRendered;
  }
}
