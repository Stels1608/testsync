﻿// Decompiled with JetBrains decompiler
// Type: TitleScreen
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TitleScreen
{
  private string[] images = new string[2]{ string.Empty, string.Empty };
  private float timeToShowImage = 2f;
  private float timeToFadeImage = 0.5f;
  private Texture img;
  private int imageIndex;
  private TitleScreen.ImageAction action;
  private float actionTime;
  private float actionTimeTotal;
  private float fadeFactor;
  private bool skippingTitles;
  private int numDisplayed;

  public bool IsAllTitlesDisplayed()
  {
    return this.numDisplayed == this.images.Length;
  }

  public void Start()
  {
    this.action = TitleScreen.ImageAction.FadeIn;
    this.actionTimeTotal = this.timeToFadeImage;
    this.numDisplayed = 0;
  }

  public void Update()
  {
    if (Input.GetKeyDown(KeyCode.Space) && this.action != TitleScreen.ImageAction.NoImage)
    {
      this.action = TitleScreen.ImageAction.FadeOut;
      this.skippingTitles = true;
    }
    if (this.action == TitleScreen.ImageAction.NoImage)
      return;
    this.actionTime += Time.deltaTime;
    if ((double) this.actionTime > (double) this.actionTimeTotal)
    {
      this.actionTime = 0.0f;
      this.actionTimeTotal = this.timeToFadeImage;
      if (this.action == TitleScreen.ImageAction.FadeIn)
      {
        if (!this.skippingTitles)
        {
          this.action = TitleScreen.ImageAction.Show;
          this.actionTimeTotal = this.timeToShowImage;
        }
        else
        {
          this.action = TitleScreen.ImageAction.FadeOut;
          this.actionTimeTotal = this.timeToFadeImage / 2f;
        }
      }
      else if (this.action == TitleScreen.ImageAction.Show)
      {
        this.action = TitleScreen.ImageAction.FadeOut;
      }
      else
      {
        ++this.numDisplayed;
        this.action = TitleScreen.ImageAction.NoImage;
        if (this.imageIndex < this.images.Length - 1)
        {
          ++this.imageIndex;
          this.img = ResourceLoader.Load<Texture>(this.images[this.imageIndex]);
          this.action = TitleScreen.ImageAction.FadeIn;
          this.actionTimeTotal = this.timeToFadeImage;
        }
      }
    }
    this.fadeFactor = 1f;
    if (this.action == TitleScreen.ImageAction.FadeIn)
      this.fadeFactor = this.actionTime / this.actionTimeTotal;
    if (this.action != TitleScreen.ImageAction.FadeOut)
      return;
    this.fadeFactor = (float) (1.0 - (double) this.actionTime / (double) this.actionTimeTotal);
  }

  public bool OnGUI()
  {
    if ((Object) this.img != (Object) null && this.action != TitleScreen.ImageAction.NoImage)
    {
      GUI.color = new Color(1f, 1f, 1f, this.fadeFactor);
      GUI.DrawTexture(new Rect(0.0f, 0.0f, (float) Screen.width, (float) Screen.height), this.img);
    }
    return this.IsAllTitlesDisplayed();
  }

  private enum ImageAction
  {
    NoImage,
    FadeIn,
    Show,
    FadeOut,
  }
}
