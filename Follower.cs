﻿// Decompiled with JetBrains decompiler
// Type: Follower
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Follower : MonoBehaviour
{
  public GameObject target;

  private void Update()
  {
    Vector3 v = this.target.transform.position - this.transform.position;
    Vector3 vector3_1 = Util.ProjectOntoPlane(v, this.transform.up).normalized * v.magnitude;
    float num = vector3_1.magnitude * 2f;
    Vector3 vector3_2 = vector3_1.normalized * num;
    CharacterMotor characterMotor = this.GetComponent(typeof (CharacterMotor)) as CharacterMotor;
    characterMotor.desiredMovementDirection = vector3_2;
    characterMotor.desiredFacingDirection = vector3_1;
  }
}
