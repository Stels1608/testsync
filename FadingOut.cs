﻿// Decompiled with JetBrains decompiler
// Type: FadingOut
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FadingOut : MonoBehaviour
{
  private float speed;
  private float alpha;

  private void Awake()
  {
    this.enabled = false;
  }

  private void Update()
  {
    if ((double) this.alpha < 0.0)
    {
      this.DestroyItself();
    }
    else
    {
      Color color = this.GetComponent<Renderer>().material.GetColor("_TintColor");
      color.a = this.alpha;
      this.GetComponent<Renderer>().material.SetColor("_TintColor", color);
      this.alpha -= this.speed * Time.deltaTime;
    }
  }

  public void Execute(float duration)
  {
    this.alpha = this.GetComponent<Renderer>().material.GetColor("_TintColor").a;
    this.speed = this.alpha / duration;
    this.enabled = true;
  }

  private void DestroyItself()
  {
    Object.Destroy((Object) this.GetComponent<MeshFilter>().mesh);
    Object.Destroy((Object) this.GetComponent<Renderer>().material);
    Object.Destroy((Object) this.gameObject);
  }
}
