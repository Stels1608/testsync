﻿// Decompiled with JetBrains decompiler
// Type: PolarGradientBandInterpolator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class PolarGradientBandInterpolator : Interpolator
{
  public PolarGradientBandInterpolator(float[][] samplePoints)
    : base(samplePoints)
  {
    this.samples = samplePoints;
  }

  public override float[] Interpolate(float[] output, bool normalize)
  {
    float[] numArray1 = this.BasicChecks(output);
    if (numArray1 != null)
      return numArray1;
    float[] numArray2 = new float[this.samples.Length];
    Vector3[] vector3Array = new Vector3[this.samples.Length];
    Vector3 vector3_1;
    if (output.Length == 2)
    {
      vector3_1 = new Vector3(output[0], output[1], 0.0f);
      for (int index = 0; index < this.samples.Length; ++index)
        vector3Array[index] = new Vector3(this.samples[index][0], this.samples[index][1], 0.0f);
    }
    else
    {
      if (output.Length != 3)
        return (float[]) null;
      vector3_1 = new Vector3(output[0], output[1], output[2]);
      for (int index = 0; index < this.samples.Length; ++index)
        vector3Array[index] = new Vector3(this.samples[index][0], this.samples[index][1], this.samples[index][2]);
    }
    for (int index1 = 0; index1 < this.samples.Length; ++index1)
    {
      bool flag = false;
      float a = 1f;
      for (int index2 = 0; index2 < this.samples.Length; ++index2)
      {
        if (index1 != index2)
        {
          Vector3 vector3_2 = vector3Array[index1];
          Vector3 vector3_3 = vector3Array[index2];
          float num1 = 2f;
          float num2;
          float num3;
          Vector3 vector3_4;
          if (vector3_2 == Vector3.zero)
          {
            num2 = Vector3.Angle(vector3_1, vector3_3) * ((float) Math.PI / 180f);
            num3 = 0.0f;
            vector3_4 = vector3_1;
            num1 = 1f;
          }
          else if (vector3_3 == Vector3.zero)
          {
            num2 = Vector3.Angle(vector3_1, vector3_2) * ((float) Math.PI / 180f);
            num3 = num2;
            vector3_4 = vector3_1;
            num1 = 1f;
          }
          else
          {
            num2 = Vector3.Angle(vector3_2, vector3_3) * ((float) Math.PI / 180f);
            if ((double) num2 > 0.0)
            {
              if (vector3_1 == Vector3.zero)
              {
                num3 = num2;
                vector3_4 = vector3_1;
              }
              else
              {
                Vector3 vector3_5 = Vector3.Cross(vector3_2, vector3_3);
                vector3_4 = Util.ProjectOntoPlane(vector3_1, vector3_5);
                num3 = Vector3.Angle(vector3_2, vector3_4) * ((float) Math.PI / 180f);
                if ((double) num2 < 3.11017680168152 && (double) Vector3.Dot(Vector3.Cross(vector3_2, vector3_4), vector3_5) < 0.0)
                  num3 *= -1f;
              }
            }
            else
            {
              vector3_4 = vector3_1;
              num3 = 0.0f;
            }
          }
          float magnitude1 = vector3_2.magnitude;
          float magnitude2 = vector3_3.magnitude;
          float magnitude3 = vector3_4.magnitude;
          float num4 = (float) (((double) magnitude1 + (double) magnitude2) / 2.0);
          float num5 = magnitude1 / num4;
          float num6 = magnitude2 / num4;
          float num7 = magnitude3 / num4;
          Vector3 lhs = new Vector3(num2 * num1, num6 - num5, 0.0f);
          Vector3 rhs = new Vector3(num3 * num1, num7 - num5, 0.0f);
          float b = (float) (1.0 - (double) Vector3.Dot(lhs, rhs) / (double) lhs.sqrMagnitude);
          if ((double) b < 0.0)
          {
            flag = true;
            break;
          }
          a = Mathf.Min(a, b);
        }
      }
      if (!flag)
        numArray2[index1] = a;
    }
    if (normalize)
    {
      float num = 0.0f;
      for (int index = 0; index < this.samples.Length; ++index)
        num += numArray2[index];
      for (int index = 0; index < this.samples.Length; ++index)
        numArray2[index] /= num;
    }
    return numArray2;
  }
}
