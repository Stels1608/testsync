﻿// Decompiled with JetBrains decompiler
// Type: StarterLevel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StarterLevel : GameLevel
{
  public override GameLevelType LevelType
  {
    get
    {
      return GameLevelType.StarterLevel;
    }
  }

  public override bool IsIngameLevel
  {
    get
    {
      return false;
    }
  }

  public static StarterLevel GetLevel()
  {
    return GameLevel.Instance as StarterLevel;
  }

  protected override void AddLoadingScreenDependencies()
  {
  }

  public override void Initialize(LevelProfile levelProfile)
  {
    base.Initialize(levelProfile);
    Camera.main.backgroundColor = Color.black;
    Camera.main.gameObject.AddComponent<StarterScreen>().Profile = levelProfile as StarterLevelProfile;
    SceneProtocol.GetProtocol().NotifySceneLoaded();
  }
}
