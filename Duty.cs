﻿// Decompiled with JetBrains decompiler
// Type: Duty
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Duty : IServerItem, IProtocolRead
{
  private ushort serverID;
  public Flag IsLoaded;
  public DutyCard Card;
  public GUICard GUICard;

  public ushort ServerID
  {
    get
    {
      return this.serverID;
    }
  }

  public int NextCounterValue
  {
    get
    {
      if (this.Card.NextCard != null)
        return Mathf.Max(this.Card.NextCard.CounterValue, this.CounterValue);
      return Mathf.Max(this.Card.CounterValue, this.CounterValue);
    }
  }

  public int CounterValue
  {
    get
    {
      return Game.Me.Counter((CounterStore) this.Card.CounterCard.CardGUID);
    }
  }

  public DutyState Status
  {
    get
    {
      if ((int) this.ServerID == (int) Game.Me.DutyBook.SelectedId)
        return DutyState.Active;
      return (int) this.Card.Level != 0 ? DutyState.Available : DutyState.Researching;
    }
  }

  public Duty()
  {
    this.IsLoaded = new Flag();
  }

  public void Read(BgoProtocolReader r)
  {
    this.serverID = r.ReadUInt16();
    uint cardGUID = r.ReadGUID();
    this.Card = (DutyCard) Game.Catalogue.FetchCard(cardGUID, CardView.Duty);
    this.GUICard = (GUICard) Game.Catalogue.FetchCard(cardGUID, CardView.GUI);
    this.IsLoaded.Depend((ILoadable) this.Card, (ILoadable) this.GUICard);
    this.IsLoaded.Set();
  }
}
