﻿// Decompiled with JetBrains decompiler
// Type: CustomizeAvatarOption
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class CustomizeAvatarOption : MonoBehaviour
{
  public Button leftButton;
  public Button rightButton;
  public Text propertyNameText;
  public Text propertyCountText;

  public void UpdateIndex(int index, int size)
  {
    this.propertyCountText.text = index.ToString() + " / " + (object) size;
  }
}
