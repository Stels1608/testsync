﻿// Decompiled with JetBrains decompiler
// Type: BsgoTextCombatMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BsgoTextCombatMessage : TextCombatMessage
{
  private float yOffset = 0.04f;
  private static float lastMessageSpawned;
  private readonly GUIText guiText;
  private readonly float xOffset;
  private Vector3 lastKnownPosition;

  public BsgoTextCombatMessage(string text, Color color, Transform parent, float lifetime)
    : base(text, color, parent, lifetime)
  {
    this.guiText = new GameObject("combat_message").AddComponent<GUIText>();
    this.guiText.text = this.Text;
    this.guiText.font = Gui.Options.FontVerdana;
    this.guiText.anchor = TextAnchor.LowerCenter;
    this.guiText.alignment = TextAlignment.Center;
    this.SetColor(color);
    if ((double) Time.time - (double) BsgoTextCombatMessage.lastMessageSpawned < 0.100000001490116)
    {
      this.xOffset = (double) Random.value >= 0.5 ? -3f / 500f : 3f / 500f;
      this.xOffset *= (float) (text.Length + 1);
      this.yOffset += 0.01f;
    }
    this.Reposition(0.0f);
    BsgoTextCombatMessage.lastMessageSpawned = Time.time;
    this.lastKnownPosition = parent.position;
    this.guiText.enabled = false;
  }

  public override void Reposition(float deltaTime)
  {
    Vector3 vector3 = !((Object) this.Parent != (Object) null) ? this.lastKnownPosition : this.Parent.position;
    float num = (float) (1.0 - (double) this.RemainingTime / (double) this.Lifetime);
    this.guiText.transform.position = Camera.main.WorldToViewportPoint(vector3 + Vector3.up);
    this.guiText.transform.Translate(this.xOffset, this.yOffset + this.EaseOutCubic(0.0f, 0.05f, num), 0.0f);
    if (!((Object) this.Parent != (Object) null))
      return;
    this.lastKnownPosition = this.Parent.position;
  }

  private float EaseOutCubic(float start, float end, float value)
  {
    --value;
    end -= start;
    return end * (float) ((double) value * (double) value * (double) value + 1.0) + start;
  }

  protected override void OnUpdate(float deltaTime)
  {
    float num = this.RemainingTime / this.Lifetime;
    Color color = this.Color;
    color.a = num;
    this.SetColor(color);
    this.guiText.enabled = true;
  }

  private void SetColor(Color color)
  {
    float num = color.a;
    color *= 0.5f;
    color.a = num;
    this.guiText.material.SetColor("_TintColor", color);
  }

  public override void Dispose()
  {
    Object.Destroy((Object) this.guiText.gameObject);
  }
}
