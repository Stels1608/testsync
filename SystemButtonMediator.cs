﻿// Decompiled with JetBrains decompiler
// Type: SystemButtonMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class SystemButtonMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "SystemButtonMediator";
  private SystemButtonWindow systemButtons;

  public SystemButtonWindow SystemButtons
  {
    get
    {
      return this.systemButtons;
    }
  }

  public SystemButtonMediator()
    : base("SystemButtonMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.GuiToggleHubMenuVisibility);
    this.AddMessageInterest(Message.SettingChangedMuteSound);
    this.AddMessageInterest(Message.SimplifyTutorialUi);
    this.AddMessageInterest(Message.LevelStarted);
    this.AddMessageInterest(Message.RecheckSystemButtons);
    this.AddMessageInterest(Message.PlayerFactionReply);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.PlayerFactionReply:
        this.systemButtons.ReloadLanguageData();
        break;
      case Message.LoadNewLevel:
        this.SystemButtons.IsRendered = false;
        break;
      case Message.LevelStarted:
        GameLevel gameLevel = (GameLevel) message.Data;
        if (!gameLevel.IsIngameLevel)
          break;
        if (gameLevel.LevelType == GameLevelType.SpaceLevel)
          FacadeFactory.GetInstance().SendMessage(Message.AddToCombatGui, (object) this.systemButtons);
        this.systemButtons.IsRendered = true;
        break;
      default:
        if (id != Message.SettingChangedMuteSound)
        {
          if (id != Message.UiCreated)
          {
            if (id != Message.SimplifyTutorialUi)
            {
              if (id != Message.RecheckSystemButtons)
              {
                if (id != Message.GuiToggleHubMenuVisibility)
                  break;
                this.SystemButtons.IsRendered = (bool) message.Data;
                break;
              }
              if (!((Object) this.SystemButtons != (Object) null) || !((Object) GameLevel.Instance != (Object) null))
                break;
              GameLevel.Instance.CheckSystemButtons(this.SystemButtons);
              this.systemButtons.Init();
              break;
            }
            Game.GUIManager.SystemButtons.SwitchToTutorialView();
            break;
          }
          this.CreateSystemButtons();
          break;
        }
        this.systemButtons.SetMuteState((bool) message.Data);
        break;
    }
  }

  private void CreateSystemButtons()
  {
    this.systemButtons = UguiTools.CreateChild<SystemButtonWindow>("SystemButtons", UguiTools.GetCanvas(BsgoCanvas.Hud).transform);
    this.systemButtons.IsRendered = false;
  }
}
