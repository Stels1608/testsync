﻿// Decompiled with JetBrains decompiler
// Type: Shockwave
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Shockwave : MonoBehaviour
{
  public float ScaleStart = 1f;
  public float ScaleEnd = 4f;
  public float Duration = 2f;
  public float FadeDuration = 1.2f;
  public float Delay;
  public bool DebugLoop;
  private float fadeTime;
  private float time;
  private Material shockwaveMaterial;

  private void Awake()
  {
    this.shockwaveMaterial = this.gameObject.GetComponent<MeshRenderer>().material;
  }

  public void OnEnable()
  {
    this.InitAnimation();
  }

  public void Update()
  {
    this.time += Time.deltaTime;
    if ((double) this.time < 0.0)
      return;
    float num = this.ScaleStart + this.ScaleEnd * (this.time / this.Duration);
    if ((double) num > (double) this.ScaleEnd)
      num = this.ScaleEnd;
    this.transform.localScale = Vector3.one * num;
    if ((double) this.time > (double) this.fadeTime)
      this.shockwaveMaterial.SetFloat("_Fade", 1f - (this.time - this.fadeTime) / this.FadeDuration);
    if ((double) this.time < (double) this.Duration)
      return;
    if (this.DebugLoop)
      this.InitAnimation();
    else
      this.gameObject.GetComponent<Renderer>().enabled = false;
  }

  private void InitAnimation()
  {
    this.fadeTime = this.Duration - this.FadeDuration;
    this.time = -this.Delay;
    this.shockwaveMaterial.SetFloat("_Fade", 1f);
    this.transform.localScale = Vector3.zero;
    this.gameObject.GetComponent<Renderer>().enabled = true;
  }
}
