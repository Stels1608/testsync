﻿// Decompiled with JetBrains decompiler
// Type: ShipShopMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class ShipShopMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "ShipShopMediator";

  public ShipShopMediator()
    : base("ShipShopMediator")
  {
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Gui.ShipShop.ShipShop shipShop = Game.GUIManager.Find<Gui.ShipShop.ShipShop>();
    if (shipShop == null || !shipShop.IsRendered || message.Id != Message.ShipListChanged)
      return;
    shipShop.UpdateShipList();
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.ShipListChanged);
  }
}
