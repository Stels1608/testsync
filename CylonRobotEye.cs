﻿// Decompiled with JetBrains decompiler
// Type: CylonRobotEye
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[RequireComponent(typeof (AudioSource))]
public class CylonRobotEye : MonoBehaviour
{
  public float MaxOffset = 0.4f;
  public float Volume = 0.9f;
  public bool UseDefaultVolume = true;
  private float sign = 1f;
  private const float DefaultVolume = 0.9f;
  private float lastAudioTime;

  [DebuggerHidden]
  private IEnumerator Start()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CylonRobotEye.\u003CStart\u003Ec__Iterator34() { \u003C\u003Ef__this = this };
  }

  private void Update()
  {
    if ((double) this.GetComponent<AudioSource>().time < (double) this.lastAudioTime)
      this.sign *= -1f;
    this.lastAudioTime = this.GetComponent<AudioSource>().time;
    float x = this.sign * Mathf.Lerp(-this.MaxOffset, this.MaxOffset, this.GetComponent<AudioSource>().time / this.GetComponent<AudioSource>().clip.length);
    if (!this.GetComponent<Renderer>().enabled)
      this.GetComponent<AudioSource>().volume = 0.0f;
    else
      this.GetComponent<AudioSource>().volume = this.GetVolume();
    this.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(x, 0.0f);
  }

  private void Reset()
  {
    this.GetComponent<AudioSource>().clip = (AudioClip) Resources.Load("cylon_robot_eye", typeof (AudioClip));
    this.GetComponent<AudioSource>().playOnAwake = false;
    this.GetComponent<AudioSource>().loop = true;
    this.GetComponent<AudioSource>().volume = 0.9f;
  }

  private float GetVolume()
  {
    if (this.UseDefaultVolume)
      return 0.9f;
    return this.Volume;
  }
}
