﻿// Decompiled with JetBrains decompiler
// Type: GUIShopUpgradeBar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GUIShopUpgradeBar : GUIPanel
{
  private List<GUIImageNew> levelImages = new List<GUIImageNew>();
  private const float BUTTONS_OFFSET = 3f;
  private const int BUTTONS_COUNT = 10;
  private GUIShopUpgradeBarTooltip tooltip;
  private Texture2D availableTex;
  private Texture2D notavailableTex;
  private Texture2D selectedTex;
  private Texture2D upgradedTex;
  private int addedLevels;
  private ShipSystem system;

  public ShipSystem System
  {
    get
    {
      return this.system;
    }
    set
    {
      this.system = value;
      if (this.system != null && !(bool) this.system.IsLoaded)
        this.system = (ShipSystem) null;
      this.addedLevels = 0;
      this.Refresh();
    }
  }

  public int SelectedLevel
  {
    get
    {
      return (int) this.system.Card.Level + this.addedLevels;
    }
  }

  public GUIShopUpgradeBar(SmartRect parent)
  {
    this.IsRendered = true;
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/EquipBuyPanel/DetailsWindow/gui_upgradebar_layout");
    this.availableTex = ResourceLoader.Load<Texture2D>((jwindowDescription["available_image"] as JImage).texture);
    this.notavailableTex = ResourceLoader.Load<Texture2D>((jwindowDescription["notavailable_image"] as JImage).texture);
    this.selectedTex = ResourceLoader.Load<Texture2D>((jwindowDescription["selected_image"] as JImage).texture);
    this.upgradedTex = ResourceLoader.Load<Texture2D>((jwindowDescription["upgraded_image"] as JImage).texture);
    float num1 = (float) this.availableTex.width;
    float num2 = (float) (-(((double) num1 + 3.0) * 10.0 - 3.0) / 2.0 + (double) num1 / 2.0);
    for (int index = 0; index < 10; ++index)
    {
      GUIImageNew guiImageNew = new GUIImageNew(this.availableTex, this.root);
      guiImageNew.Position = new float2(num2 + (float) index * (3f + num1), 0.0f);
      this.levelImages.Add(guiImageNew);
      this.AddPanel((GUIPanel) guiImageNew);
    }
    GUIButtonNew guiButtonNew1 = (jwindowDescription["minus_button"] as JButton).CreateGUIButtonNew(this.root);
    guiButtonNew1.Position = this.levelImages[0].Position + new float2((float) (-(double) this.levelImages[0].Width / 2.0 - (double) guiButtonNew1.Width / 2.0 - 3.0), 0.0f);
    guiButtonNew1.Handler = new AnonymousDelegate(this.OnMinusButton);
    this.AddPanel((GUIPanel) guiButtonNew1);
    GUIButtonNew guiButtonNew2 = (jwindowDescription["plus_button"] as JButton).CreateGUIButtonNew(this.root);
    guiButtonNew2.Position = this.levelImages[this.levelImages.Count - 1].Position + new float2((float) ((double) this.levelImages[this.levelImages.Count - 1].Width / 2.0 + (double) guiButtonNew1.Width / 2.0 + 3.0), 0.0f);
    guiButtonNew2.Handler = new AnonymousDelegate(this.OnPlusButton);
    this.AddPanel((GUIPanel) guiButtonNew2);
    this.root.Width = (float) ((double) guiButtonNew1.Width + (double) guiButtonNew2.Width + (((double) num1 + 3.0) * 10.0 - 3.0) + 6.0);
    this.root.Height = (float) this.availableTex.height;
    this.tooltip = new GUIShopUpgradeBarTooltip(this.root);
    this.AddPanel((GUIPanel) this.tooltip);
    this.RecalculateAbsCoords();
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    if (this.system == null)
      return;
    bool flag = false;
    for (int index = 0; index < this.levelImages.Count; ++index)
    {
      if (this.levelImages[index].Contains(mousePosition))
      {
        this.tooltip.TryToShow(mousePosition, this.system, index + 1);
        flag = true;
      }
    }
    if (!flag)
      this.tooltip.Hide();
    base.OnMouseMove(mousePosition);
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    this.tooltip.Hide();
    return base.OnMouseDown(mousePosition, mouseKey);
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    this.tooltip.Hide();
    return base.OnKeyUp(keyboardKey, action);
  }

  protected void OnMinusButton()
  {
    if (this.system == null)
      return;
    if (this.addedLevels - 1 >= 0)
      --this.addedLevels;
    this.Refresh();
  }

  protected void OnPlusButton()
  {
    if (this.system == null)
      return;
    if ((int) this.system.Card.Level + this.addedLevels + 1 <= 10 && this.AvailableToUpgrade(this.system, (int) this.system.Card.Level + this.addedLevels + 1))
      ++this.addedLevels;
    this.Refresh();
  }

  protected void Refresh()
  {
    if (this.system == null)
      return;
    for (int index = 0; index < this.levelImages.Count; ++index)
      this.levelImages[index].Texture = this.AvailableToUpgrade(this.system, index + 1) ? (!this.AlreadyIsUpgraded(this.system, index + 1) ? (index + 1 > (int) this.system.Card.Level + this.addedLevels ? this.availableTex : this.selectedTex) : this.upgradedTex) : this.notavailableTex;
  }

  protected bool AlreadyIsUpgraded(ShipSystem system, int systemLevel)
  {
    return systemLevel <= (int) system.Card.Level;
  }

  protected bool AvailableToUpgrade(ShipSystem system, int systemLevel)
  {
    if (systemLevel > (int) system.Card.MaxLevel)
      return false;
    int skillLevel = 0;
    List<PlayerSkill> needSkills = (List<PlayerSkill>) null;
    return Game.Me.RequiresSkillsForSystem(system, systemLevel, out skillLevel, out needSkills);
  }
}
