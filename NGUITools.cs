﻿// Decompiled with JetBrains decompiler
// Type: NGUITools
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UnityEngine;

public static class NGUITools
{
  private static bool mLoaded = false;
  private static float mGlobalVolume = 1f;
  private static Vector3[] mSides = new Vector3[4];
  private static AudioListener mListener;

  public static float soundVolume
  {
    get
    {
      if (!NGUITools.mLoaded)
      {
        NGUITools.mLoaded = true;
        NGUITools.mGlobalVolume = PlayerPrefs.GetFloat("Sound", 1f);
      }
      return NGUITools.mGlobalVolume;
    }
    set
    {
      if ((double) NGUITools.mGlobalVolume == (double) value)
        return;
      NGUITools.mLoaded = true;
      NGUITools.mGlobalVolume = value;
      PlayerPrefs.SetFloat("Sound", value);
    }
  }

  public static bool fileAccess
  {
    get
    {
      if (Application.platform != RuntimePlatform.WindowsWebPlayer)
        return Application.platform != RuntimePlatform.OSXWebPlayer;
      return false;
    }
  }

  public static string clipboard
  {
    get
    {
      TextEditor textEditor = new TextEditor();
      textEditor.Paste();
      return textEditor.content.text;
    }
    set
    {
      TextEditor textEditor = new TextEditor();
      textEditor.content = new GUIContent(value);
      textEditor.OnFocus();
      textEditor.Copy();
    }
  }

  public static Vector2 screenSize
  {
    get
    {
      return new Vector2((float) Screen.width, (float) Screen.height);
    }
  }

  public static AudioSource PlaySound(AudioClip clip)
  {
    return NGUITools.PlaySound(clip, 1f, 1f);
  }

  public static AudioSource PlaySound(AudioClip clip, float volume)
  {
    return NGUITools.PlaySound(clip, volume, 1f);
  }

  public static AudioSource PlaySound(AudioClip clip, float volume, float pitch)
  {
    volume *= NGUITools.soundVolume;
    if ((UnityEngine.Object) clip != (UnityEngine.Object) null && (double) volume > 0.00999999977648258)
    {
      if ((UnityEngine.Object) NGUITools.mListener == (UnityEngine.Object) null || !NGUITools.GetActive((Behaviour) NGUITools.mListener))
      {
        AudioListener[] audioListenerArray = UnityEngine.Object.FindObjectsOfType(typeof (AudioListener)) as AudioListener[];
        if (audioListenerArray != null)
        {
          for (int index = 0; index < audioListenerArray.Length; ++index)
          {
            if (NGUITools.GetActive((Behaviour) audioListenerArray[index]))
            {
              NGUITools.mListener = audioListenerArray[index];
              break;
            }
          }
        }
        if ((UnityEngine.Object) NGUITools.mListener == (UnityEngine.Object) null)
        {
          Camera camera = Camera.main;
          if ((UnityEngine.Object) camera == (UnityEngine.Object) null)
            camera = UnityEngine.Object.FindObjectOfType(typeof (Camera)) as Camera;
          if ((UnityEngine.Object) camera != (UnityEngine.Object) null)
            NGUITools.mListener = camera.gameObject.AddComponent<AudioListener>();
        }
      }
      if ((UnityEngine.Object) NGUITools.mListener != (UnityEngine.Object) null && NGUITools.mListener.enabled && NGUITools.GetActive(NGUITools.mListener.gameObject))
      {
        AudioSource audioSource = NGUITools.mListener.GetComponent<AudioSource>();
        if ((UnityEngine.Object) audioSource == (UnityEngine.Object) null)
          audioSource = NGUITools.mListener.gameObject.AddComponent<AudioSource>();
        audioSource.pitch = pitch;
        audioSource.PlayOneShot(clip, volume);
        return audioSource;
      }
    }
    return (AudioSource) null;
  }

  public static int RandomRange(int min, int max)
  {
    if (min == max)
      return min;
    return UnityEngine.Random.Range(min, max + 1);
  }

  public static string GetHierarchy(GameObject obj)
  {
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      return string.Empty;
    string str = obj.name;
    while ((UnityEngine.Object) obj.transform.parent != (UnityEngine.Object) null)
    {
      obj = obj.transform.parent.gameObject;
      str = obj.name + "\\" + str;
    }
    return str;
  }

  public static T[] FindActive<T>() where T : Component
  {
    return UnityEngine.Object.FindObjectsOfType(typeof (T)) as T[];
  }

  public static Camera FindCameraForLayer(int layer)
  {
    int num = 1 << layer;
    for (int index = 0; index < UICamera.list.size; ++index)
    {
      Camera cachedCamera = UICamera.list.buffer[index].cachedCamera;
      if ((bool) ((UnityEngine.Object) cachedCamera) && (cachedCamera.cullingMask & num) != 0)
        return cachedCamera;
    }
    Camera main = Camera.main;
    if ((bool) ((UnityEngine.Object) main) && (main.cullingMask & num) != 0)
      return main;
    Camera[] cameras = new Camera[Camera.allCamerasCount];
    int allCameras = Camera.GetAllCameras(cameras);
    for (int index = 0; index < allCameras; ++index)
    {
      Camera camera = cameras[index];
      if ((bool) ((UnityEngine.Object) camera) && camera.enabled && (camera.cullingMask & num) != 0)
        return camera;
    }
    return (Camera) null;
  }

  public static void AddWidgetCollider(GameObject go)
  {
    NGUITools.AddWidgetCollider(go, false);
  }

  public static void AddWidgetCollider(GameObject go, bool considerInactive)
  {
    if (!((UnityEngine.Object) go != (UnityEngine.Object) null))
      return;
    Collider component1 = go.GetComponent<Collider>();
    BoxCollider box1 = component1 as BoxCollider;
    if ((UnityEngine.Object) box1 != (UnityEngine.Object) null)
    {
      NGUITools.UpdateWidgetCollider(box1, considerInactive);
    }
    else
    {
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
        return;
      BoxCollider2D component2 = go.GetComponent<BoxCollider2D>();
      if ((UnityEngine.Object) component2 != (UnityEngine.Object) null)
      {
        NGUITools.UpdateWidgetCollider(component2, considerInactive);
      }
      else
      {
        UICamera cameraForLayer = UICamera.FindCameraForLayer(go.layer);
        if ((UnityEngine.Object) cameraForLayer != (UnityEngine.Object) null && (cameraForLayer.eventType == UICamera.EventType.World_2D || cameraForLayer.eventType == UICamera.EventType.UI_2D))
        {
          BoxCollider2D box2 = go.AddComponent<BoxCollider2D>();
          box2.isTrigger = true;
          UIWidget component3 = go.GetComponent<UIWidget>();
          if ((UnityEngine.Object) component3 != (UnityEngine.Object) null)
            component3.autoResizeBoxCollider = true;
          NGUITools.UpdateWidgetCollider(box2, considerInactive);
        }
        else
        {
          BoxCollider box2 = go.AddComponent<BoxCollider>();
          box2.isTrigger = true;
          UIWidget component3 = go.GetComponent<UIWidget>();
          if ((UnityEngine.Object) component3 != (UnityEngine.Object) null)
            component3.autoResizeBoxCollider = true;
          NGUITools.UpdateWidgetCollider(box2, considerInactive);
        }
      }
    }
  }

  public static void UpdateWidgetCollider(GameObject go)
  {
    NGUITools.UpdateWidgetCollider(go, false);
  }

  public static void UpdateWidgetCollider(GameObject go, bool considerInactive)
  {
    if (!((UnityEngine.Object) go != (UnityEngine.Object) null))
      return;
    BoxCollider component1 = go.GetComponent<BoxCollider>();
    if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
    {
      NGUITools.UpdateWidgetCollider(component1, considerInactive);
    }
    else
    {
      BoxCollider2D component2 = go.GetComponent<BoxCollider2D>();
      if (!((UnityEngine.Object) component2 != (UnityEngine.Object) null))
        return;
      NGUITools.UpdateWidgetCollider(component2, considerInactive);
    }
  }

  public static void UpdateWidgetCollider(BoxCollider box, bool considerInactive)
  {
    if (!((UnityEngine.Object) box != (UnityEngine.Object) null))
      return;
    GameObject gameObject = box.gameObject;
    UIWidget component = gameObject.GetComponent<UIWidget>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
    {
      Vector4 drawRegion = component.drawRegion;
      if ((double) drawRegion.x != 0.0 || (double) drawRegion.y != 0.0 || ((double) drawRegion.z != 1.0 || (double) drawRegion.w != 1.0))
      {
        Vector4 drawingDimensions = component.drawingDimensions;
        box.center = new Vector3((float) (((double) drawingDimensions.x + (double) drawingDimensions.z) * 0.5), (float) (((double) drawingDimensions.y + (double) drawingDimensions.w) * 0.5));
        box.size = new Vector3(drawingDimensions.z - drawingDimensions.x, drawingDimensions.w - drawingDimensions.y);
      }
      else
      {
        Vector3[] localCorners = component.localCorners;
        box.center = Vector3.Lerp(localCorners[0], localCorners[2], 0.5f);
        box.size = localCorners[2] - localCorners[0];
      }
    }
    else
    {
      Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(gameObject.transform, considerInactive);
      box.center = relativeWidgetBounds.center;
      box.size = new Vector3(relativeWidgetBounds.size.x, relativeWidgetBounds.size.y, 0.0f);
    }
  }

  public static void UpdateWidgetCollider(BoxCollider2D box, bool considerInactive)
  {
    if (!((UnityEngine.Object) box != (UnityEngine.Object) null))
      return;
    GameObject gameObject = box.gameObject;
    UIWidget component = gameObject.GetComponent<UIWidget>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
    {
      Vector3[] localCorners = component.localCorners;
      box.offset = (Vector2) Vector3.Lerp(localCorners[0], localCorners[2], 0.5f);
      box.size = (Vector2) (localCorners[2] - localCorners[0]);
    }
    else
    {
      Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(gameObject.transform, considerInactive);
      box.offset = (Vector2) relativeWidgetBounds.center;
      box.size = new Vector2(relativeWidgetBounds.size.x, relativeWidgetBounds.size.y);
    }
  }

  public static string GetTypeName<T>()
  {
    string str = typeof (T).ToString();
    if (str.StartsWith("UI"))
      str = str.Substring(2);
    else if (str.StartsWith("UnityEngine."))
      str = str.Substring(12);
    return str;
  }

  public static string GetTypeName(UnityEngine.Object obj)
  {
    if (obj == (UnityEngine.Object) null)
      return "Null";
    string str = obj.GetType().ToString();
    if (str.StartsWith("UI"))
      str = str.Substring(2);
    else if (str.StartsWith("UnityEngine."))
      str = str.Substring(12);
    return str;
  }

  public static void RegisterUndo(UnityEngine.Object obj, string name)
  {
  }

  public static void SetDirty(UnityEngine.Object obj)
  {
  }

  public static GameObject AddChild(GameObject parent)
  {
    return NGUITools.AddChild(parent, true);
  }

  public static GameObject AddChild(GameObject parent, bool undo)
  {
    GameObject gameObject = new GameObject();
    if ((UnityEngine.Object) parent != (UnityEngine.Object) null)
    {
      Transform transform = gameObject.transform;
      transform.parent = parent.transform;
      transform.localPosition = Vector3.zero;
      transform.localRotation = Quaternion.identity;
      transform.localScale = Vector3.one;
      gameObject.layer = parent.layer;
    }
    return gameObject;
  }

  public static GameObject AddChild(GameObject parent, GameObject prefab)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(prefab);
    if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null && (UnityEngine.Object) parent != (UnityEngine.Object) null)
    {
      Transform transform = gameObject.transform;
      transform.parent = parent.transform;
      transform.localPosition = Vector3.zero;
      transform.localRotation = Quaternion.identity;
      transform.localScale = Vector3.one;
      gameObject.layer = parent.layer;
    }
    return gameObject;
  }

  public static int CalculateRaycastDepth(GameObject go)
  {
    UIWidget component = go.GetComponent<UIWidget>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      return component.raycastDepth;
    UIWidget[] componentsInChildren = go.GetComponentsInChildren<UIWidget>();
    if (componentsInChildren.Length == 0)
      return 0;
    int a = int.MaxValue;
    int index = 0;
    for (int length = componentsInChildren.Length; index < length; ++index)
    {
      if (componentsInChildren[index].enabled)
        a = Mathf.Min(a, componentsInChildren[index].raycastDepth);
    }
    return a;
  }

  public static int CalculateNextDepth(GameObject go)
  {
    int a = -1;
    UIWidget[] componentsInChildren = go.GetComponentsInChildren<UIWidget>();
    int index = 0;
    for (int length = componentsInChildren.Length; index < length; ++index)
      a = Mathf.Max(a, componentsInChildren[index].depth);
    return a + 1;
  }

  public static int CalculateNextDepth(GameObject go, bool ignoreChildrenWithColliders)
  {
    if (!ignoreChildrenWithColliders)
      return NGUITools.CalculateNextDepth(go);
    int a = -1;
    UIWidget[] componentsInChildren = go.GetComponentsInChildren<UIWidget>();
    int index = 0;
    for (int length = componentsInChildren.Length; index < length; ++index)
    {
      UIWidget uiWidget = componentsInChildren[index];
      if (!((UnityEngine.Object) uiWidget.cachedGameObject != (UnityEngine.Object) go) || !((UnityEngine.Object) uiWidget.GetComponent<Collider>() != (UnityEngine.Object) null) && !((UnityEngine.Object) uiWidget.GetComponent<Collider2D>() != (UnityEngine.Object) null))
        a = Mathf.Max(a, uiWidget.depth);
    }
    return a + 1;
  }

  public static int AdjustDepth(GameObject go, int adjustment)
  {
    if (!((UnityEngine.Object) go != (UnityEngine.Object) null))
      return 0;
    if ((UnityEngine.Object) go.GetComponent<UIPanel>() != (UnityEngine.Object) null)
    {
      foreach (UIPanel componentsInChild in go.GetComponentsInChildren<UIPanel>(true))
        componentsInChild.depth = componentsInChild.depth + adjustment;
      return 1;
    }
    UIPanel inParents = NGUITools.FindInParents<UIPanel>(go);
    if ((UnityEngine.Object) inParents == (UnityEngine.Object) null)
      return 0;
    UIWidget[] componentsInChildren = go.GetComponentsInChildren<UIWidget>(true);
    int index = 0;
    for (int length = componentsInChildren.Length; index < length; ++index)
    {
      UIWidget uiWidget = componentsInChildren[index];
      if (!((UnityEngine.Object) uiWidget.panel != (UnityEngine.Object) inParents))
        uiWidget.depth = uiWidget.depth + adjustment;
    }
    return 2;
  }

  public static void BringForward(GameObject go)
  {
    switch (NGUITools.AdjustDepth(go, 1000))
    {
      case 1:
        NGUITools.NormalizePanelDepths();
        break;
      case 2:
        NGUITools.NormalizeWidgetDepths();
        break;
    }
  }

  public static void PushBack(GameObject go)
  {
    switch (NGUITools.AdjustDepth(go, -1000))
    {
      case 1:
        NGUITools.NormalizePanelDepths();
        break;
      case 2:
        NGUITools.NormalizeWidgetDepths();
        break;
    }
  }

  public static void NormalizeDepths()
  {
    NGUITools.NormalizeWidgetDepths();
    NGUITools.NormalizePanelDepths();
  }

  public static void NormalizeWidgetDepths()
  {
    NGUITools.NormalizeWidgetDepths(NGUITools.FindActive<UIWidget>());
  }

  public static void NormalizeWidgetDepths(GameObject go)
  {
    NGUITools.NormalizeWidgetDepths(go.GetComponentsInChildren<UIWidget>());
  }

  public static void NormalizeWidgetDepths(UIWidget[] list)
  {
    int length = list.Length;
    if (length <= 0)
      return;
    Array.Sort<UIWidget>(list, new Comparison<UIWidget>(UIWidget.FullCompareFunc));
    int num = 0;
    int depth = list[0].depth;
    for (int index = 0; index < length; ++index)
    {
      UIWidget uiWidget = list[index];
      if (uiWidget.depth == depth)
      {
        uiWidget.depth = num;
      }
      else
      {
        depth = uiWidget.depth;
        uiWidget.depth = ++num;
      }
    }
  }

  public static void NormalizePanelDepths()
  {
    UIPanel[] active = NGUITools.FindActive<UIPanel>();
    int length = active.Length;
    if (length <= 0)
      return;
    Array.Sort<UIPanel>(active, new Comparison<UIPanel>(UIPanel.CompareFunc));
    int num = 0;
    int depth = active[0].depth;
    for (int index = 0; index < length; ++index)
    {
      UIPanel uiPanel = active[index];
      if (uiPanel.depth == depth)
      {
        uiPanel.depth = num;
      }
      else
      {
        depth = uiPanel.depth;
        uiPanel.depth = ++num;
      }
    }
  }

  public static UIPanel CreateUI(bool advanced3D)
  {
    return NGUITools.CreateUI((Transform) null, advanced3D, -1);
  }

  public static UIPanel CreateUI(bool advanced3D, int layer)
  {
    return NGUITools.CreateUI((Transform) null, advanced3D, layer);
  }

  public static UIPanel CreateUI(Transform trans, bool advanced3D, int layer)
  {
    UIRoot uiRoot = !((UnityEngine.Object) trans != (UnityEngine.Object) null) ? (UIRoot) null : NGUITools.FindInParents<UIRoot>(trans.gameObject);
    if ((UnityEngine.Object) uiRoot == (UnityEngine.Object) null && UIRoot.list.Count > 0)
    {
      using (List<UIRoot>.Enumerator enumerator = UIRoot.list.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          UIRoot current = enumerator.Current;
          if (current.gameObject.layer == layer)
          {
            uiRoot = current;
            break;
          }
        }
      }
    }
    if ((UnityEngine.Object) uiRoot != (UnityEngine.Object) null)
    {
      UICamera componentInChildren = uiRoot.GetComponentInChildren<UICamera>();
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null && componentInChildren.GetComponent<Camera>().orthographic == advanced3D)
      {
        trans = (Transform) null;
        uiRoot = (UIRoot) null;
      }
    }
    if ((UnityEngine.Object) uiRoot == (UnityEngine.Object) null)
    {
      GameObject gameObject = NGUITools.AddChild((GameObject) null, false);
      uiRoot = gameObject.AddComponent<UIRoot>();
      if (layer == -1)
        layer = LayerMask.NameToLayer("UI");
      if (layer == -1)
        layer = LayerMask.NameToLayer("2D UI");
      gameObject.layer = layer;
      if (advanced3D)
      {
        gameObject.name = "UI Root (3D)";
        uiRoot.scalingStyle = UIRoot.Scaling.Constrained;
      }
      else
      {
        gameObject.name = "UI Root";
        uiRoot.scalingStyle = UIRoot.Scaling.Flexible;
      }
    }
    UIPanel uiPanel = uiRoot.GetComponentInChildren<UIPanel>();
    if ((UnityEngine.Object) uiPanel == (UnityEngine.Object) null)
    {
      Camera[] active1 = NGUITools.FindActive<Camera>();
      float a = -1f;
      bool flag = false;
      int num = 1 << uiRoot.gameObject.layer;
      for (int index = 0; index < active1.Length; ++index)
      {
        Camera camera = active1[index];
        if (camera.clearFlags == CameraClearFlags.Color || camera.clearFlags == CameraClearFlags.Skybox)
          flag = true;
        a = Mathf.Max(a, camera.depth);
        camera.cullingMask = camera.cullingMask & ~num;
      }
      Camera camera1 = NGUITools.AddChild<Camera>(uiRoot.gameObject, false);
      camera1.gameObject.AddComponent<UICamera>();
      camera1.clearFlags = !flag ? CameraClearFlags.Color : CameraClearFlags.Depth;
      camera1.backgroundColor = Color.grey;
      camera1.cullingMask = num;
      camera1.depth = a + 1f;
      if (advanced3D)
      {
        camera1.nearClipPlane = 0.1f;
        camera1.farClipPlane = 4f;
        camera1.transform.localPosition = new Vector3(0.0f, 0.0f, -700f);
      }
      else
      {
        camera1.orthographic = true;
        camera1.orthographicSize = 1f;
        camera1.nearClipPlane = -10f;
        camera1.farClipPlane = 10f;
      }
      AudioListener[] active2 = NGUITools.FindActive<AudioListener>();
      if (active2 == null || active2.Length == 0)
        camera1.gameObject.AddComponent<AudioListener>();
      uiPanel = uiRoot.gameObject.AddComponent<UIPanel>();
    }
    if ((UnityEngine.Object) trans != (UnityEngine.Object) null)
    {
      while ((UnityEngine.Object) trans.parent != (UnityEngine.Object) null)
        trans = trans.parent;
      if (NGUITools.IsChild(trans, uiPanel.transform))
      {
        uiPanel = trans.gameObject.AddComponent<UIPanel>();
      }
      else
      {
        trans.parent = uiPanel.transform;
        trans.localScale = Vector3.one;
        trans.localPosition = Vector3.zero;
        NGUITools.SetChildLayer(uiPanel.cachedTransform, uiPanel.cachedGameObject.layer);
      }
    }
    return uiPanel;
  }

  public static void SetChildLayer(Transform t, int layer)
  {
    for (int index = 0; index < t.childCount; ++index)
    {
      Transform child = t.GetChild(index);
      child.gameObject.layer = layer;
      NGUITools.SetChildLayer(child, layer);
    }
  }

  public static T AddChild<T>(GameObject parent) where T : Component
  {
    GameObject gameObject = NGUITools.AddChild(parent);
    gameObject.name = NGUITools.GetTypeName<T>();
    return gameObject.AddComponent<T>();
  }

  public static T AddChild<T>(GameObject parent, bool undo) where T : Component
  {
    GameObject gameObject = NGUITools.AddChild(parent, undo);
    gameObject.name = NGUITools.GetTypeName<T>();
    return gameObject.AddComponent<T>();
  }

  public static T AddWidget<T>(GameObject go) where T : UIWidget
  {
    int nextDepth = NGUITools.CalculateNextDepth(go);
    T obj = NGUITools.AddChild<T>(go);
    obj.width = 100;
    obj.height = 100;
    obj.depth = nextDepth;
    obj.gameObject.layer = go.layer;
    return obj;
  }

  public static UISprite AddSprite(GameObject go, UIAtlas atlas, string spriteName)
  {
    UISpriteData uiSpriteData = !((UnityEngine.Object) atlas != (UnityEngine.Object) null) ? (UISpriteData) null : atlas.GetSprite(spriteName);
    UISprite uiSprite = NGUITools.AddWidget<UISprite>(go);
    uiSprite.type = uiSpriteData == null || !uiSpriteData.hasBorder ? UIBasicSprite.Type.Simple : UIBasicSprite.Type.Sliced;
    uiSprite.atlas = atlas;
    uiSprite.spriteName = spriteName;
    return uiSprite;
  }

  public static GameObject GetRoot(GameObject go)
  {
    Transform transform = go.transform;
    while (true)
    {
      Transform parent = transform.parent;
      if (!((UnityEngine.Object) parent == (UnityEngine.Object) null))
        transform = parent;
      else
        break;
    }
    return transform.gameObject;
  }

  public static T FindInParents<T>(GameObject go) where T : Component
  {
    if ((UnityEngine.Object) go == (UnityEngine.Object) null)
      return (T) null;
    T component = go.GetComponent<T>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
    {
      for (Transform parent = go.transform.parent; (UnityEngine.Object) parent != (UnityEngine.Object) null && (UnityEngine.Object) component == (UnityEngine.Object) null; parent = parent.parent)
        component = parent.gameObject.GetComponent<T>();
    }
    return component;
  }

  public static T FindInParents<T>(Transform trans) where T : Component
  {
    if ((UnityEngine.Object) trans == (UnityEngine.Object) null)
      return (T) null;
    return trans.GetComponentInParent<T>();
  }

  public static void Destroy(UnityEngine.Object obj)
  {
    if (!(obj != (UnityEngine.Object) null))
      return;
    if (Application.isPlaying)
    {
      if (obj is GameObject)
        (obj as GameObject).transform.parent = (Transform) null;
      UnityEngine.Object.Destroy(obj);
    }
    else
      UnityEngine.Object.DestroyImmediate(obj);
  }

  public static void DestroyImmediate(UnityEngine.Object obj)
  {
    if (!(obj != (UnityEngine.Object) null))
      return;
    if (Application.isEditor)
      UnityEngine.Object.DestroyImmediate(obj);
    else
      UnityEngine.Object.Destroy(obj);
  }

  public static void Broadcast(string funcName)
  {
    GameObject[] gameObjectArray = UnityEngine.Object.FindObjectsOfType(typeof (GameObject)) as GameObject[];
    int index = 0;
    for (int length = gameObjectArray.Length; index < length; ++index)
      gameObjectArray[index].SendMessage(funcName, SendMessageOptions.DontRequireReceiver);
  }

  public static void Broadcast(string funcName, object param)
  {
    GameObject[] gameObjectArray = UnityEngine.Object.FindObjectsOfType(typeof (GameObject)) as GameObject[];
    int index = 0;
    for (int length = gameObjectArray.Length; index < length; ++index)
      gameObjectArray[index].SendMessage(funcName, param, SendMessageOptions.DontRequireReceiver);
  }

  public static bool IsChild(Transform parent, Transform child)
  {
    if ((UnityEngine.Object) parent == (UnityEngine.Object) null || (UnityEngine.Object) child == (UnityEngine.Object) null)
      return false;
    for (; (UnityEngine.Object) child != (UnityEngine.Object) null; child = child.parent)
    {
      if ((UnityEngine.Object) child == (UnityEngine.Object) parent)
        return true;
    }
    return false;
  }

  private static void Activate(Transform t)
  {
    NGUITools.Activate(t, false);
  }

  private static void Activate(Transform t, bool compatibilityMode)
  {
    NGUITools.SetActiveSelf(t.gameObject, true);
    if (!compatibilityMode)
      return;
    int index1 = 0;
    for (int childCount = t.childCount; index1 < childCount; ++index1)
    {
      if (t.GetChild(index1).gameObject.activeSelf)
        return;
    }
    int index2 = 0;
    for (int childCount = t.childCount; index2 < childCount; ++index2)
      NGUITools.Activate(t.GetChild(index2), true);
  }

  private static void Deactivate(Transform t)
  {
    NGUITools.SetActiveSelf(t.gameObject, false);
  }

  public static void SetActive(GameObject go, bool state)
  {
    NGUITools.SetActive(go, state, true);
  }

  public static void SetActive(GameObject go, bool state, bool compatibilityMode)
  {
    if (!(bool) ((UnityEngine.Object) go))
      return;
    if (state)
    {
      NGUITools.Activate(go.transform, compatibilityMode);
      NGUITools.CallCreatePanel(go.transform);
    }
    else
      NGUITools.Deactivate(go.transform);
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  private static void CallCreatePanel(Transform t)
  {
    UIWidget component = t.GetComponent<UIWidget>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.CreatePanel();
    int index = 0;
    for (int childCount = t.childCount; index < childCount; ++index)
      NGUITools.CallCreatePanel(t.GetChild(index));
  }

  public static void SetActiveChildren(GameObject go, bool state)
  {
    Transform transform = go.transform;
    if (state)
    {
      int index = 0;
      for (int childCount = transform.childCount; index < childCount; ++index)
        NGUITools.Activate(transform.GetChild(index));
    }
    else
    {
      int index = 0;
      for (int childCount = transform.childCount; index < childCount; ++index)
        NGUITools.Deactivate(transform.GetChild(index));
    }
  }

  [Obsolete("Use NGUITools.GetActive instead")]
  public static bool IsActive(Behaviour mb)
  {
    if ((UnityEngine.Object) mb != (UnityEngine.Object) null && mb.enabled)
      return mb.gameObject.activeInHierarchy;
    return false;
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  public static bool GetActive(Behaviour mb)
  {
    if ((bool) ((UnityEngine.Object) mb) && mb.enabled)
      return mb.gameObject.activeInHierarchy;
    return false;
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  public static bool GetActive(GameObject go)
  {
    if ((bool) ((UnityEngine.Object) go))
      return go.activeInHierarchy;
    return false;
  }

  [DebuggerHidden]
  [DebuggerStepThrough]
  public static void SetActiveSelf(GameObject go, bool state)
  {
    go.SetActive(state);
  }

  public static void SetLayer(GameObject go, int layer)
  {
    go.layer = layer;
    Transform transform = go.transform;
    int index = 0;
    for (int childCount = transform.childCount; index < childCount; ++index)
      NGUITools.SetLayer(transform.GetChild(index).gameObject, layer);
  }

  public static Vector3 Round(Vector3 v)
  {
    v.x = Mathf.Round(v.x);
    v.y = Mathf.Round(v.y);
    v.z = Mathf.Round(v.z);
    return v;
  }

  public static void MakePixelPerfect(Transform t)
  {
    UIWidget component = t.GetComponent<UIWidget>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.MakePixelPerfect();
    if ((UnityEngine.Object) t.GetComponent<UIAnchor>() == (UnityEngine.Object) null && (UnityEngine.Object) t.GetComponent<UIRoot>() == (UnityEngine.Object) null)
    {
      t.localPosition = NGUITools.Round(t.localPosition);
      t.localScale = NGUITools.Round(t.localScale);
    }
    int index = 0;
    for (int childCount = t.childCount; index < childCount; ++index)
      NGUITools.MakePixelPerfect(t.GetChild(index));
  }

  public static bool Save(string fileName, byte[] bytes)
  {
    if (!NGUITools.fileAccess)
      return false;
    string path = Application.persistentDataPath + "/" + fileName;
    if (bytes == null)
    {
      if (File.Exists(path))
        File.Delete(path);
      return true;
    }
    FileStream fileStream;
    try
    {
      fileStream = File.Create(path);
    }
    catch (Exception ex)
    {
      UnityEngine.Debug.LogError((object) ex.Message);
      return false;
    }
    fileStream.Write(bytes, 0, bytes.Length);
    fileStream.Close();
    return true;
  }

  public static byte[] Load(string fileName)
  {
    if (!NGUITools.fileAccess)
      return (byte[]) null;
    string path = Application.persistentDataPath + "/" + fileName;
    if (File.Exists(path))
      return File.ReadAllBytes(path);
    return (byte[]) null;
  }

  public static Color ApplyPMA(Color c)
  {
    if ((double) c.a != 1.0)
    {
      c.r *= c.a;
      c.g *= c.a;
      c.b *= c.a;
    }
    return c;
  }

  public static void MarkParentAsChanged(GameObject go)
  {
    UIRect[] componentsInChildren = go.GetComponentsInChildren<UIRect>();
    int index = 0;
    for (int length = componentsInChildren.Length; index < length; ++index)
      componentsInChildren[index].ParentHasChanged();
  }

  [Obsolete("Use NGUIText.EncodeColor instead")]
  public static string EncodeColor(Color c)
  {
    return NGUIText.EncodeColor24(c);
  }

  [Obsolete("Use NGUIText.ParseColor instead")]
  public static Color ParseColor(string text, int offset)
  {
    return NGUIText.ParseColor24(text, offset);
  }

  [Obsolete("Use NGUIText.StripSymbols instead")]
  public static string StripSymbols(string text)
  {
    return NGUIText.StripSymbols(text);
  }

  public static T AddMissingComponent<T>(this GameObject go) where T : Component
  {
    T obj = go.GetComponent<T>();
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      obj = go.AddComponent<T>();
    return obj;
  }

  public static Vector3[] GetSides(this Camera cam)
  {
    return cam.GetSides(Mathf.Lerp(cam.nearClipPlane, cam.farClipPlane, 0.5f), (Transform) null);
  }

  public static Vector3[] GetSides(this Camera cam, float depth)
  {
    return cam.GetSides(depth, (Transform) null);
  }

  public static Vector3[] GetSides(this Camera cam, Transform relativeTo)
  {
    return cam.GetSides(Mathf.Lerp(cam.nearClipPlane, cam.farClipPlane, 0.5f), relativeTo);
  }

  public static Vector3[] GetSides(this Camera cam, float depth, Transform relativeTo)
  {
    if (cam.orthographic)
    {
      float orthographicSize = cam.orthographicSize;
      float num1 = -orthographicSize;
      float num2 = orthographicSize;
      float y1 = -orthographicSize;
      float y2 = orthographicSize;
      Rect rect = cam.rect;
      Vector2 screenSize = NGUITools.screenSize;
      float num3 = screenSize.x / screenSize.y * (rect.width / rect.height);
      float x1 = num1 * num3;
      float x2 = num2 * num3;
      Transform transform = cam.transform;
      Quaternion rotation = transform.rotation;
      Vector3 position = transform.position;
      NGUITools.mSides[0] = rotation * new Vector3(x1, 0.0f, depth) + position;
      NGUITools.mSides[1] = rotation * new Vector3(0.0f, y2, depth) + position;
      NGUITools.mSides[2] = rotation * new Vector3(x2, 0.0f, depth) + position;
      NGUITools.mSides[3] = rotation * new Vector3(0.0f, y1, depth) + position;
    }
    else
    {
      NGUITools.mSides[0] = cam.ViewportToWorldPoint(new Vector3(0.0f, 0.5f, depth));
      NGUITools.mSides[1] = cam.ViewportToWorldPoint(new Vector3(0.5f, 1f, depth));
      NGUITools.mSides[2] = cam.ViewportToWorldPoint(new Vector3(1f, 0.5f, depth));
      NGUITools.mSides[3] = cam.ViewportToWorldPoint(new Vector3(0.5f, 0.0f, depth));
    }
    if ((UnityEngine.Object) relativeTo != (UnityEngine.Object) null)
    {
      for (int index = 0; index < 4; ++index)
        NGUITools.mSides[index] = relativeTo.InverseTransformPoint(NGUITools.mSides[index]);
    }
    return NGUITools.mSides;
  }

  public static Vector3[] GetWorldCorners(this Camera cam)
  {
    float depth = Mathf.Lerp(cam.nearClipPlane, cam.farClipPlane, 0.5f);
    return cam.GetWorldCorners(depth, (Transform) null);
  }

  public static Vector3[] GetWorldCorners(this Camera cam, float depth)
  {
    return cam.GetWorldCorners(depth, (Transform) null);
  }

  public static Vector3[] GetWorldCorners(this Camera cam, Transform relativeTo)
  {
    return cam.GetWorldCorners(Mathf.Lerp(cam.nearClipPlane, cam.farClipPlane, 0.5f), relativeTo);
  }

  public static Vector3[] GetWorldCorners(this Camera cam, float depth, Transform relativeTo)
  {
    if (cam.orthographic)
    {
      float orthographicSize = cam.orthographicSize;
      float num1 = -orthographicSize;
      float num2 = orthographicSize;
      float y1 = -orthographicSize;
      float y2 = orthographicSize;
      Rect rect = cam.rect;
      Vector2 screenSize = NGUITools.screenSize;
      float num3 = screenSize.x / screenSize.y * (rect.width / rect.height);
      float x1 = num1 * num3;
      float x2 = num2 * num3;
      Transform transform = cam.transform;
      Quaternion rotation = transform.rotation;
      Vector3 position = transform.position;
      NGUITools.mSides[0] = rotation * new Vector3(x1, y1, depth) + position;
      NGUITools.mSides[1] = rotation * new Vector3(x1, y2, depth) + position;
      NGUITools.mSides[2] = rotation * new Vector3(x2, y2, depth) + position;
      NGUITools.mSides[3] = rotation * new Vector3(x2, y1, depth) + position;
    }
    else
    {
      NGUITools.mSides[0] = cam.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, depth));
      NGUITools.mSides[1] = cam.ViewportToWorldPoint(new Vector3(0.0f, 1f, depth));
      NGUITools.mSides[2] = cam.ViewportToWorldPoint(new Vector3(1f, 1f, depth));
      NGUITools.mSides[3] = cam.ViewportToWorldPoint(new Vector3(1f, 0.0f, depth));
    }
    if ((UnityEngine.Object) relativeTo != (UnityEngine.Object) null)
    {
      for (int index = 0; index < 4; ++index)
        NGUITools.mSides[index] = relativeTo.InverseTransformPoint(NGUITools.mSides[index]);
    }
    return NGUITools.mSides;
  }

  public static string GetFuncName(object obj, string method)
  {
    if (obj == null)
      return "<null>";
    string str = obj.GetType().ToString();
    int num = str.LastIndexOf('/');
    if (num > 0)
      str = str.Substring(num + 1);
    if (string.IsNullOrEmpty(method))
      return str;
    return str + "/" + method;
  }

  public static void Execute<T>(GameObject go, string funcName) where T : Component
  {
    foreach (T component in go.GetComponents<T>())
    {
      MethodInfo method = component.GetType().GetMethod(funcName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
      if (method != null)
        method.Invoke((object) component, (object[]) null);
    }
  }

  public static void ExecuteAll<T>(GameObject root, string funcName) where T : Component
  {
    NGUITools.Execute<T>(root, funcName);
    Transform transform = root.transform;
    int index = 0;
    for (int childCount = transform.childCount; index < childCount; ++index)
      NGUITools.ExecuteAll<T>(transform.GetChild(index).gameObject, funcName);
  }

  public static void ImmediatelyCreateDrawCalls(GameObject root)
  {
    NGUITools.ExecuteAll<UIWidget>(root, "Start");
    NGUITools.ExecuteAll<UIPanel>(root, "Start");
    NGUITools.ExecuteAll<UIWidget>(root, "Update");
    NGUITools.ExecuteAll<UIPanel>(root, "Update");
    NGUITools.ExecuteAll<UIPanel>(root, "LateUpdate");
  }
}
