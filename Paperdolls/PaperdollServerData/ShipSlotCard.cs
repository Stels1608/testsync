﻿// Decompiled with JetBrains decompiler
// Type: Paperdolls.PaperdollServerData.ShipSlotCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Paperdolls.PaperdollServerData
{
  public class ShipSlotCard : IProtocolRead
  {
    public byte Level;
    public ushort SlotId;
    public ShipSlotType SystemType;
    public string ObjectPoint;
    [JsonField(deserialize = false, serialize = false)]
    public ushort ObjectPointServerHash;

    public void Read(BgoProtocolReader r)
    {
      this.SlotId = r.ReadUInt16();
      this.ObjectPoint = r.ReadString();
      this.ObjectPointServerHash = r.ReadUInt16();
      this.SystemType = (ShipSlotType) r.ReadByte();
      this.Level = r.ReadByte();
    }
  }
}
