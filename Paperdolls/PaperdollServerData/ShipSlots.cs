﻿// Decompiled with JetBrains decompiler
// Type: Paperdolls.PaperdollServerData.ShipSlots
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Paperdolls.PaperdollServerData
{
  public class ShipSlots
  {
    public string PaperdollUiLayoutfile = string.Empty;
    public List<ShipSlotCard> Slots;

    public int FindMaxLevel()
    {
      return (int) this.Slots.Select<ShipSlotCard, byte>((Func<ShipSlotCard, byte>) (slot => slot.Level)).Concat<byte>((IEnumerable<byte>) new byte[1]).Max<byte>();
    }

    public ushort FindUnusedSlotId()
    {
      List<ushort> list = this.Slots.Select<ShipSlotCard, ushort>((Func<ShipSlotCard, ushort>) (slot => slot.SlotId)).ToList<ushort>();
      list.Sort();
      ushort num = (ushort) ((uint) list.Last<ushort>() + 1U);
      for (ushort index = 1; (int) index <= (int) num; ++index)
      {
        if (!list.Contains(index))
          return index;
      }
      Debug.LogError((object) "Hm. Something's wrong here...");
      return num;
    }

    public int FindHighestId()
    {
      return (int) this.Slots.Select<ShipSlotCard, ushort>((Func<ShipSlotCard, ushort>) (slot => slot.SlotId)).Max<ushort>();
    }

    public ShipSlotCard AddSlot(ushort id, byte slotAvailabilityLevel)
    {
      ShipSlotCard shipSlotCard = new ShipSlotCard() { Level = slotAvailabilityLevel, SlotId = id, ObjectPoint = "undefined" };
      this.Slots.Add(shipSlotCard);
      return shipSlotCard;
    }

    public void TryRemoveSlot(int slotId, byte selectedUpgradeLevel)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      int num = this.Slots.RemoveAll(new Predicate<ShipSlotCard>(new ShipSlots.\u003CTryRemoveSlot\u003Ec__AnonStoreyB0() { slotId = slotId, selectedUpgradeLevel = selectedUpgradeLevel }.\u003C\u003Em__17E));
      if (num <= 0)
        return;
      Debug.Log((object) ("Removed " + (object) num + " slots from server ship data."));
    }

    public ShipSlotCard GetSlot(int slotId, byte selectedUpgradeLevel)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      return this.Slots.SingleOrDefault<ShipSlotCard>(new Func<ShipSlotCard, bool>(new ShipSlots.\u003CGetSlot\u003Ec__AnonStoreyB1() { slotId = slotId, selectedUpgradeLevel = selectedUpgradeLevel }.\u003C\u003Em__17F));
    }
  }
}
