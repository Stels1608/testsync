﻿// Decompiled with JetBrains decompiler
// Type: Paperdolls.PaperdollServerData.ShipImmutableSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Paperdolls.PaperdollServerData
{
  public class ShipImmutableSlot : IProtocolRead
  {
    public ushort SlotId;
    public ShipSlotType SystemType;
    [JsonField(deserialize = false, serialize = false)]
    public ushort ObjectPointServerHash;
    public uint SystemKey;
    public uint SystemLevel;
    public uint ConsumableKey;
    public ShipImmutableAbility Ability;

    public void Read(BgoProtocolReader r)
    {
      this.SlotId = r.ReadUInt16();
      this.ObjectPointServerHash = r.ReadUInt16();
      this.SystemType = (ShipSlotType) r.ReadByte();
      this.SystemKey = r.ReadGUID();
      this.SystemLevel = (uint) r.ReadUInt16();
      this.ConsumableKey = r.ReadGUID();
      this.Ability = new ShipImmutableAbility(this);
    }
  }
}
