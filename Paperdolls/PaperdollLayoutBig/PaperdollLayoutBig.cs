﻿// Decompiled with JetBrains decompiler
// Type: Paperdolls.PaperdollLayoutBig.PaperdollLayoutBig
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Paperdolls.PaperdollLayoutBig
{
  public class PaperdollLayoutBig : IPaperdollLayout
  {
    [JsonField(deserialize = false, serialize = false)]
    public const string BlueprintTexturePath = "GUI/EquipBuyPanel/";
    public string BlueprintTexture;
    public List<UpgradeLevel> UpgradeLevels;

    public string BlueprintTexture_
    {
      get
      {
        return this.BlueprintTexture;
      }
      set
      {
        this.BlueprintTexture = value;
      }
    }

    public List<SlotLayout> GetSlotLayouts(int upgradeLevel)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      return this.UpgradeLevels.Find(new Predicate<UpgradeLevel>(new Paperdolls.PaperdollLayoutBig.PaperdollLayoutBig.\u003CGetSlotLayouts\u003Ec__AnonStoreyAA() { upgradeLevel = upgradeLevel }.\u003C\u003Em__16F)).SlotLayouts;
    }

    public List<SlotLayoutCommon> GetSlotLayoutsCommon(int upgradeLevel)
    {
      return this.GetSlotLayouts(upgradeLevel).Cast<SlotLayoutCommon>().ToList<SlotLayoutCommon>();
    }

    public SlotLayout AddSlot(float2 slotPos, ushort id, int slotAvailabilityLevel, int shipMaxUpgradeLevel)
    {
      SlotLayout slotLayout1 = new SlotLayout();
      slotLayout1.Position = slotPos;
      slotLayout1.SlotId = id;
      SlotLayout slotLayout2 = slotLayout1;
      for (int upgradeLevel = slotAvailabilityLevel; upgradeLevel <= shipMaxUpgradeLevel; ++upgradeLevel)
        this.GetSlotLayouts(upgradeLevel).Add(slotLayout2);
      return slotLayout2;
    }

    public void TryRemoveSlot(ushort slotId, byte selectedUpgradeLevel)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      Paperdolls.PaperdollLayoutBig.PaperdollLayoutBig.\u003CTryRemoveSlot\u003Ec__AnonStoreyAB slotCAnonStoreyAb = new Paperdolls.PaperdollLayoutBig.PaperdollLayoutBig.\u003CTryRemoveSlot\u003Ec__AnonStoreyAB();
      // ISSUE: reference to a compiler-generated field
      slotCAnonStoreyAb.selectedUpgradeLevel = selectedUpgradeLevel;
      // ISSUE: reference to a compiler-generated field
      slotCAnonStoreyAb.slotId = slotId;
      // ISSUE: reference to a compiler-generated method
      // ISSUE: reference to a compiler-generated method
      int num = this.UpgradeLevels.Where<UpgradeLevel>(new Func<UpgradeLevel, bool>(slotCAnonStoreyAb.\u003C\u003Em__170)).Sum<UpgradeLevel>(new Func<UpgradeLevel, int>(slotCAnonStoreyAb.\u003C\u003Em__171));
      if (num <= 0)
        return;
      // ISSUE: reference to a compiler-generated field
      Debug.Log((object) ("Removed " + (object) num + " slots in " + (object) (this.UpgradeLevels.Count - (int) slotCAnonStoreyAb.selectedUpgradeLevel + 1) + " upgrade levels from Big Paperdoll Layout."));
    }

    public void ChangeSlotId(ushort oldId, ushort newId)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      foreach (SlotLayoutCommon slotLayoutCommon in this.UpgradeLevels.SelectMany<UpgradeLevel, SlotLayout>(new Func<UpgradeLevel, IEnumerable<SlotLayout>>(new Paperdolls.PaperdollLayoutBig.PaperdollLayoutBig.\u003CChangeSlotId\u003Ec__AnonStoreyAC() { oldId = oldId }.\u003C\u003Em__172)))
        slotLayoutCommon.SlotId = newId;
    }
  }
}
