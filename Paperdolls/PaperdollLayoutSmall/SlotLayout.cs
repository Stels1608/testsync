﻿// Decompiled with JetBrains decompiler
// Type: Paperdolls.PaperdollLayoutSmall.SlotLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Paperdolls.PaperdollLayoutSmall
{
  public class SlotLayout : SlotLayoutCommon
  {
    [JsonField(deserialize = false, serialize = false)]
    public static string ArrowTexturePath = "GUI/Paperdoll/";
    public string Arrowtype;
    public int Hotkey;

    public static Vector2 GetHotkeyOffset(string arrowType)
    {
      string lower = arrowType.ToLower();
      if (lower != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (SlotLayout.\u003C\u003Ef__switch\u0024mapD == null)
        {
          // ISSUE: reference to a compiler-generated field
          SlotLayout.\u003C\u003Ef__switch\u0024mapD = new Dictionary<string, int>(4)
          {
            {
              "up",
              0
            },
            {
              "down",
              1
            },
            {
              "left",
              2
            },
            {
              "right",
              3
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (SlotLayout.\u003C\u003Ef__switch\u0024mapD.TryGetValue(lower, out num))
        {
          switch (num)
          {
            case 0:
              return new Vector2(0.0f, -1f) * 32f;
            case 1:
              return new Vector2(0.0f, 1f) * 32f;
            case 2:
              return new Vector2(-1f, 0.0f) * 32f;
            case 3:
              return new Vector2(1f, 0.0f) * 32f;
          }
        }
      }
      return new Vector2(0.0f, 0.0f);
    }
  }
}
