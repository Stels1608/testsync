﻿// Decompiled with JetBrains decompiler
// Type: SpaceObjectMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public class SpaceObjectMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "SpaceObjectMediator";

  public SpaceObjectMediator()
    : base("SpaceObjectMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.SettingChangedHighResModels);
    this.AddMessageInterest(Message.SettingChangedUseProceduralTextures);
    this.AddMessageInterest(Message.SettingChangedShowShipSkins);
    this.AddMessageInterest(Message.SettingChangedShowModules);
    this.AddMessageInterest(Message.PlayerChangedCloakStatus);
    this.AddMessageInterest(Message.TargetWaypointStatusChanged);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.SettingChangedUseProceduralTextures:
        this.ToggleProceduralTextures((bool) message.Data);
        break;
      case Message.SettingChangedShowShipSkins:
        this.ToggleShipSkins((bool) message.Data);
        break;
      case Message.SettingChangedShowModules:
        this.ToggleModules((bool) message.Data);
        break;
      case Message.SettingChangedHighResModels:
        this.UpdateModelQuality((bool) message.Data);
        break;
      case Message.TargetWaypointStatusChanged:
        KeyValuePair<ISpaceEntity, bool> keyValuePair1 = (KeyValuePair<ISpaceEntity, bool>) message.Data;
        ISpaceEntity key = keyValuePair1.Key;
        bool flag1 = keyValuePair1.Value;
        BsgoTrigger bsgoTrigger = key as BsgoTrigger;
        if (bsgoTrigger == null)
          break;
        bsgoTrigger.IsActiveWaypoint = flag1;
        break;
      case Message.PlayerChangedCloakStatus:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        SpaceObjectMediator.\u003CReceiveMessage\u003Ec__AnonStoreyD4 messageCAnonStoreyD4 = new SpaceObjectMediator.\u003CReceiveMessage\u003Ec__AnonStoreyD4();
        KeyValuePair<SpaceObject, bool> keyValuePair2 = (KeyValuePair<SpaceObject, bool>) message.Data;
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyD4.so = keyValuePair2.Key;
        bool flag2 = keyValuePair2.Value;
        // ISSUE: reference to a compiler-generated field
        this.PlayAnim(messageCAnonStoreyD4.so, !flag2 ? ShipAnimType.StealthDisengage : ShipAnimType.StealthEngage);
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyD4.enginesEnabled = !flag2;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        Timer.CreateTimer("ToggleEngines", !messageCAnonStoreyD4.enginesEnabled ? 0.0f : 2f, 0.0f, new Timer.TickParametrizedHandler(messageCAnonStoreyD4.\u003C\u003Em__1FB), (object) true, true);
        break;
    }
  }

  private void UpdateModelQuality(bool highResModels)
  {
    bool flag = !highResModels;
    CutsceneRootScript.ForceLowRes = flag;
    RootScript.ForceLowRes = flag;
    foreach (RootScript rootScript in Object.FindObjectsOfType(typeof (RootScript)))
      rootScript.Construct();
  }

  private void ToggleModules(bool show)
  {
    Modules.ShowModules = show;
    foreach (Modules modules in Object.FindObjectsOfType<Modules>())
    {
      if (modules.SpaceObject != null && !modules.SpaceObject.IsMe)
      {
        if (show)
          modules.EnableModules();
        else
          modules.RemoveModules();
      }
    }
  }

  private void ToggleShipSkins(bool use)
  {
    bool flag = use;
    ShipSkin.ShowShipSkins = flag;
    ShipSkinSubstance.ShowShipSkins = flag;
    foreach (Body body in Object.FindObjectsOfType(typeof (Body)) as Body[])
    {
      if (!((Object) body.ActiveRenderer == (Object) null))
        body.ReloadSkin();
    }
    foreach (MultiBody multiBody in Object.FindObjectsOfType(typeof (MultiBody)) as MultiBody[])
    {
      if (multiBody.ActiveRenderers != null)
        multiBody.ReloadSkin();
    }
    foreach (RootScript rootScript in Object.FindObjectsOfType(typeof (RootScript)))
    {
      if (rootScript.SpaceObject is Ship && ((Ship) rootScript.SpaceObject).HasModelSkinInstalled)
        rootScript.Construct();
    }
  }

  private void ToggleProceduralTextures(bool use)
  {
    ShipSkinSubstance.UseProceduralTextures = use;
    foreach (Body body in Object.FindObjectsOfType(typeof (Body)) as Body[])
    {
      if (!((Object) body.ActiveRenderer == (Object) null))
        body.ReloadSkin();
    }
    foreach (MultiBody multiBody in Object.FindObjectsOfType(typeof (MultiBody)) as MultiBody[])
    {
      if (multiBody.ActiveRenderers != null)
        multiBody.ReloadSkin();
    }
  }

  private void PlayAnim(SpaceObject spaceObject, ShipAnimType animType)
  {
    foreach (ShipAnims componentsInChild in spaceObject.Model.GetComponentsInChildren<ShipAnims>())
    {
      using (List<ShipAnim>.Enumerator enumerator = componentsInChild.Animations.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ShipAnim current = enumerator.Current;
          if (current.AnimType == animType)
          {
            Animation component = componentsInChild.GetComponent<Animation>();
            if ((Object) component == (Object) null)
              Debug.LogError((object) ("Failed playing anim of type: " + (object) current.AnimType + ". " + componentsInChild.gameObject.name + " has no Animation Component!"));
            else if ((Object) component.GetClip(current.AnimClip.name) == (Object) null)
            {
              Debug.LogError((object) ("Failed playing anim " + current.AnimClip.name + ". ClipName not defined in " + componentsInChild.gameObject.name + "'s Animation Component!"));
            }
            else
            {
              string name = current.AnimClip.name;
              AnimationState animationState = component.GetComponent<Animation>()[current.AnimClip.name];
              float num = Mathf.Abs(animationState.speed);
              animationState.speed = num * (!current.ReverseAnim ? 1f : -1f);
              animationState.normalizedTime = !current.ReverseAnim ? 0.0f : 1f;
              component.Play(name);
              AudioSource audioSource = componentsInChild.GetComponent<AudioSource>();
              if ((Object) audioSource == (Object) null)
              {
                audioSource = componentsInChild.gameObject.AddComponent<AudioSource>();
                audioSource.spatialBlend = 1f;
              }
              audioSource.clip = current.AnimSound;
              audioSource.volume = current.AnimSoundVolume;
              audioSource.minDistance = 10f;
              audioSource.Play();
            }
          }
        }
      }
    }
  }
}
