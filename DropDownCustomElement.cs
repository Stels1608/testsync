﻿// Decompiled with JetBrains decompiler
// Type: DropDownCustomElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DropDownCustomElement : UIPopupList
{
  private UILabel dropDownLabel;

  public string Selection
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  public List<string> Items
  {
    get
    {
      return this.items;
    }
    set
    {
      this.items = value;
    }
  }

  private void Start()
  {
    this.dropDownLabel = this.textLabel;
    EventDelegate.Add(this.onChange, new EventDelegate.Callback(this.SelectionChange));
  }

  public void SelectionChange()
  {
    Debug.Log((object) UIPopupList.current.value);
  }

  public void SetTextLabelNonUpdating(string label)
  {
    this.textLabel = this.dropDownLabel = this.textLabel ?? this.dropDownLabel;
    this.value = label;
    this.textLabel = (UILabel) null;
  }
}
