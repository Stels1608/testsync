﻿// Decompiled with JetBrains decompiler
// Type: DebugBehaviour`1
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
  protected Rect windowRect = new Rect(0.0f, 0.0f, 200f, 200f);
  protected bool resizable = true;
  protected static T instance;
  protected string tittle;
  protected int windowID;
  protected GUISkin DebugSkin;
  private int resizeType;

  protected virtual void Awake()
  {
    this.name = typeof (T).ToString();
    this.tittle = this.name;
  }

  protected void SetSize(float width, float height)
  {
    this.windowRect = new Rect((float) (((double) Screen.width - (double) width) / 2.0), (float) (((double) Screen.height - (double) height) / 2.0), width, height);
  }

  private void OnGUI()
  {
    if ((Object) this.DebugSkin == (Object) null)
      this.DebugSkin = BgoDebug.LoadDebugSkin();
    GUI.skin = this.DebugSkin;
    this.windowRect = GUI.Window(this.windowID, this.windowRect, new GUI.WindowFunction(this.WindowFunc), this.tittle);
    GUI.skin = (GUISkin) null;
  }

  private void WindowFunc(int windowID)
  {
    GUILayout.BeginArea(new Rect(this.windowRect.width - 18f, 0.0f, 18f, 18f));
    GUI.color = Color.white;
    if (GUILayout.Button("X"))
      DebugBehaviour<T>.Close();
    GUILayout.EndArea();
    this.WindowFunc();
    this.Resize();
    GUI.DragWindow(new Rect(0.0f, 0.0f, this.windowRect.width, this.windowRect.height));
  }

  private void Resize()
  {
    if (!this.resizable || !Event.current.isMouse || Event.current.button != 0)
      return;
    float num1 = Event.current.mousePosition.x;
    float num2 = Event.current.mousePosition.y;
    if (Event.current.type == UnityEngine.EventType.MouseDown)
    {
      this.resizeType = 0;
      if (0.0 < (double) num1 && (double) num1 < 10.0)
        this.resizeType |= 1;
      if (0.0 < (double) num2 && (double) num2 < 5.0)
        this.resizeType |= 2;
      if ((double) this.windowRect.width - 10.0 < (double) num1 && (double) num1 < (double) this.windowRect.width)
        this.resizeType |= 4;
      if ((double) this.windowRect.height - 10.0 < (double) num2 && (double) num2 < (double) this.windowRect.height)
        this.resizeType |= 8;
      if (this.resizeType == 0)
        return;
      Event.current.Use();
    }
    else if (Event.current.type == UnityEngine.EventType.MouseDrag)
    {
      float num3 = Event.current.delta.x;
      float num4 = Event.current.delta.y;
      float num5 = 50f;
      if ((this.resizeType & 1) != 0 && (double) this.windowRect.width - (double) num3 > (double) num5)
      {
        this.windowRect.x += num3;
        this.windowRect.width -= num3;
      }
      if ((this.resizeType & 2) != 0 && (double) this.windowRect.height - (double) num4 > (double) num5)
      {
        this.windowRect.y += num4;
        this.windowRect.height -= num4;
      }
      if ((this.resizeType & 4) != 0 && (double) this.windowRect.width + (double) num3 > (double) num5)
        this.windowRect.width += num3;
      if ((this.resizeType & 8) != 0 && (double) this.windowRect.height + (double) num4 > (double) num5)
        this.windowRect.height += num4;
      if (this.resizeType == 0)
        return;
      Event.current.Use();
    }
    else
    {
      if (Event.current.type != UnityEngine.EventType.MouseUp)
        return;
      if (this.resizeType != 0)
        Event.current.Use();
      this.resizeType = 0;
    }
  }

  protected virtual void WindowFunc()
  {
  }

  public static void Show()
  {
    if (!((Object) DebugBehaviour<T>.instance == (Object) null))
      return;
    GameObject gameObject = new GameObject();
    Object.DontDestroyOnLoad((Object) gameObject);
    DebugBehaviour<T>.instance = gameObject.AddComponent<T>();
  }

  public static void Close()
  {
    if (!((Object) DebugBehaviour<T>.instance != (Object) null))
      return;
    Object.Destroy((Object) DebugBehaviour<T>.instance.gameObject);
    DebugBehaviour<T>.instance = (T) null;
  }

  public static void Toggle()
  {
    if ((Object) DebugBehaviour<T>.instance == (Object) null)
      DebugBehaviour<T>.Show();
    else
      DebugBehaviour<T>.Close();
  }

  private void OnApplicationQuit()
  {
    if (!(bool) ((Object) DebugBehaviour<T>.instance))
      return;
    Object.DestroyImmediate((Object) DebugBehaviour<T>.instance.gameObject);
  }

  public static bool Command(string[] commands)
  {
    bool res;
    if (commands.Length == 0)
      res = true;
    else if (commands.Length != 1 || !Console.ParseBool(commands[0], out res))
      return false;
    if (res)
      DebugBehaviour<T>.Show();
    else
      DebugBehaviour<T>.Close();
    return true;
  }
}
