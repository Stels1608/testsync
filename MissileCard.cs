﻿// Decompiled with JetBrains decompiler
// Type: MissileCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class MissileCard : Card
{
  public MissileExplosionArgs.MissileExplosionView ExplosionView;
  public MissileType MissileType;

  public bool IsNuke
  {
    get
    {
      if (this.ExplosionView != MissileExplosionArgs.MissileExplosionView.Nuclear && this.ExplosionView != MissileExplosionArgs.MissileExplosionView.NuclearMini)
        return this.ExplosionView == MissileExplosionArgs.MissileExplosionView.Torpedo;
      return true;
    }
  }

  public MissileCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.ExplosionView = (MissileExplosionArgs.MissileExplosionView) r.ReadByte();
    this.MissileType = (MissileType) r.ReadByte();
  }
}
