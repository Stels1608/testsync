﻿// Decompiled with JetBrains decompiler
// Type: GUISpeedControl
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using UnityEngine;

public class GUISpeedControl : GUIPanel
{
  private readonly GUIBlinker boosterBlinker = new GUIBlinker();
  private readonly GUIBlinker throttleBlinker = new GUIBlinker();
  private readonly GUIBlinker matchSpeedBlinker = new GUIBlinker();
  private readonly GUIBlinker followBlinker = new GUIBlinker();
  private const float UPDATE_INTERVAL = 0.2f;
  private const float SPEED_HOLD_ARROW_INCREASE_VALUE = 9f;
  private float timeToUpdate;
  private bool wasBgClick;
  private readonly GUILabelNew speedValueLabel;
  private readonly GUIImageNew background;
  private readonly GUIVScroller scroller;
  private readonly GUIButtonNew arrowUp;
  private readonly GUIButtonNew arrowDown;
  private readonly GUIButtonNew boost;
  private readonly GUIButtonNew follow;
  private readonly GUIButtonNew matchspeed;

  private float SpeedAbsolute
  {
    get
    {
      return (1f - this.scroller.Value) * Game.Me.Stats.MaxSpeed;
    }
  }

  public GUISpeedControl()
  {
    this.IsRendered = true;
    this.speedValueLabel = new GUILabelNew("999.9", this.root, Gui.Options.FontDS_DIGIB, Color.white, Color.white);
    this.speedValueLabel.FontSize = 18;
    this.speedValueLabel.MakeCenteredRect();
    this.speedValueLabel.Alignment = TextAnchor.MiddleLeft;
    this.speedValueLabel.Position = new float2(10f, -70f);
    this.AddPanel((GUIPanel) this.speedValueLabel);
    Texture2D normal1 = (Texture2D) ResourceLoader.Load("GUI/SpeedControl/speedupbutton");
    Texture2D over1 = (Texture2D) ResourceLoader.Load("GUI/SpeedControl/speedupbutton_over");
    Texture2D pressed1 = (Texture2D) ResourceLoader.Load("GUI/SpeedControl/speedupbutton_click");
    Texture2D normal2 = (Texture2D) ResourceLoader.Load("GUI/SpeedControl/speeddownbutton");
    Texture2D over2 = (Texture2D) ResourceLoader.Load("GUI/SpeedControl/speeddownbutton_over");
    Texture2D pressed2 = (Texture2D) ResourceLoader.Load("GUI/SpeedControl/speeddownbutton_click");
    Texture2D texture = ResourceLoader.Load<Texture2D>("GUI/SpeedControl/speed_background");
    this.background = new GUIImageNew(texture);
    this.background.Position = new float2(2f, 0.0f);
    this.background.SmartRect.Width = 15f;
    this.AddPanel((GUIPanel) this.background);
    this.arrowUp = new GUIButtonNew(normal1, over1, pressed1, this.root);
    this.arrowUp.Position = new float2(0.0f, (float) -((double) (texture.height / 2 + 2) + (double) this.arrowUp.Rect.height / 2.0));
    this.arrowUp.TextLabel.IsRendered = false;
    this.arrowUp.Name = "ArrowUp";
    this.arrowDown = new GUIButtonNew(normal2, over2, pressed2, this.root);
    this.arrowDown.Position = new float2(0.0f, (float) (texture.height / 2 + 2) + this.arrowUp.Rect.height / 2f);
    this.arrowDown.TextLabel.IsRendered = false;
    this.arrowUp.Name = "ArrowDown";
    Texture2D sliderTexture = ResourceLoader.Load<Texture2D>("GUI/SpeedControl/thrusthandle");
    this.scroller = new GUIVScroller((float) texture.height + (float) sliderTexture.height / 1.45f, sliderTexture, this.root);
    this.scroller.Position = new float2(0.0f, 0.0f);
    this.scroller.Value = 1f;
    this.AddPanel((GUIPanel) this.scroller);
    this.scroller.scroller.AddPanel((GUIPanel) this.throttleBlinker);
    this.root.Width = this.arrowUp.Rect.height;
    this.root.Height = (float) texture.height;
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/SpeedControl/buttons_layout");
    this.follow = (jwindowDescription["follow"] as JButton).CreateGUIButtonNew(this.root);
    this.follow.Handler = (AnonymousDelegate) (() =>
    {
      SpaceLevel level = SpaceLevel.GetLevel();
      if ((Object) level != (Object) null)
      {
        SpaceObject playerTarget = level.GetPlayerTarget();
        if (playerTarget != null)
          Game.Me.Follow(playerTarget.ObjectID);
      }
      if (!this.followBlinker.IsRendered)
        return;
      StoryProtocol.GetProtocol().TriggerControl(StoryProtocol.ControlType.Follow);
    });
    this.matchspeed = (jwindowDescription["matchspeed"] as JButton).CreateGUIButtonNew(this.root);
    this.matchspeed.Handler = (AnonymousDelegate) (() =>
    {
      SpaceLevel level = SpaceLevel.GetLevel();
      if ((Object) level != (Object) null)
      {
        SpaceObject playerTarget = level.GetPlayerTarget();
        if (playerTarget != null)
          FacadeFactory.GetInstance().SendMessage(Message.ShipSpeedChangeCall, (object) new DataChangeSpeed(ShipControlsBase.SpeedMode.Abs, playerTarget.MovementController.CurrentSpeed));
      }
      if (!this.matchSpeedBlinker.IsRendered)
        return;
      StoryProtocol.GetProtocol().TriggerControl(StoryProtocol.ControlType.MatchSpeed);
    });
    this.boost = (jwindowDescription["boost"] as JButton).CreateGUIButtonNew(this.root);
    this.boost.Handler = (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage(Message.ShipGearToggleBoostCall));
    this.AddPanel((GUIPanel) this.boosterBlinker);
    this.AddPanel((GUIPanel) this.arrowDown);
    this.AddPanel((GUIPanel) this.arrowUp);
    this.AddPanel((GUIPanel) this.follow);
    this.AddPanel((GUIPanel) this.matchspeed);
    this.AddPanel((GUIPanel) this.boost);
    this.follow.AddPanel((GUIPanel) this.followBlinker);
    this.matchspeed.AddPanel((GUIPanel) this.matchSpeedBlinker);
    this.followBlinker.IsRendered = false;
    this.matchSpeedBlinker.IsRendered = false;
    this.throttleBlinker.IsRendered = false;
    this.boosterBlinker.IsRendered = false;
    this.background.SetTooltip("%$bgo.option_buttons.throttle%");
    this.arrowUp.SetTooltip("%$bgo.option_buttons.increase_speed%");
    this.arrowDown.SetTooltip("%$bgo.option_buttons.decrease_speed%");
    this.follow.SetTooltip("%$bgo.option_buttons.follow_friendly_target%", Action.Follow);
    this.matchspeed.SetTooltip("%$bgo.option_buttons.match_target_speed%", Action.MatchSpeed);
    this.boost.SetTooltip("%$bgo.option_buttons.activate_boosters%", Action.Boost);
    this.speedValueLabel.SetTooltip("%$bgo.speedcontrol.speedvalue%");
    this.handlers.Add(Action.Follow, this.follow.Handler);
    this.handlers.Add(Action.MatchSpeed, this.matchspeed.Handler);
  }

  public void SetSpeedAbs(float newSpeedAbs)
  {
    float f = 1f - Mathf.Clamp01(newSpeedAbs / Game.Me.Stats.MaxSpeed);
    if (float.IsNaN(f))
      f = 0.0f;
    this.scroller.Value = f;
    if (!this.throttleBlinker.IsRendered)
      return;
    StoryProtocol.GetProtocol().TriggerControl(StoryProtocol.ControlType.ThrottleBar);
  }

  public void ModifySpeed(float delta)
  {
    FacadeFactory.GetInstance().SendMessage(Message.ShipSpeedChangeCall, (object) new DataChangeSpeed(ShipControlsBase.SpeedMode.Abs, this.SpeedAbsolute + delta));
  }

  public override void Update()
  {
    base.Update();
    this.timeToUpdate -= Time.deltaTime;
    if ((double) this.timeToUpdate <= 0.0 || this.arrowUp.IsPressed || this.arrowDown.IsPressed)
    {
      this.speedValueLabel.TextNoParse = Game.Me.Ship != null ? Game.Me.Ship.MovementController.CurrentSpeed.ToString("F1") : string.Empty;
      if (Game.Me.Hold.Tylium < 1U)
        this.boost.IsPressed = false;
      this.timeToUpdate = 0.2f;
    }
    this.HandleArrowButtons();
    if (!this.throttleBlinker.IsUpdated)
      return;
    this.throttleBlinker.Update();
  }

  private void HandleArrowButtons()
  {
    if (this.arrowUp.IsPressed)
      this.ModifySpeed(9f * Time.deltaTime);
    if (!this.arrowDown.IsPressed)
      return;
    this.ModifySpeed(-9f * Time.deltaTime);
  }

  public override void Draw()
  {
    base.Draw();
    if (this.boost.IsRendered)
      this.boost.Draw();
    if (this.arrowUp.IsRendered)
      this.arrowUp.Draw();
    if (!this.arrowDown.IsRendered)
      return;
    this.arrowDown.Draw();
  }

  private void ScrollerHandler()
  {
    FacadeFactory.GetInstance().SendMessage(Message.ShipSpeedChangeCall, (object) new DataChangeSpeed(ShipControlsBase.SpeedMode.Abs, this.SpeedAbsolute));
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (Game.Me.Anchored)
      return false;
    this.wasBgClick = false;
    if (!base.OnMouseDown(mousePosition, mouseKey))
      return false;
    if (this.background.Contains(mousePosition) && !this.scroller.Contains(mousePosition))
      this.wasBgClick = true;
    return true;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    if (Game.Me.Anchored)
      return;
    base.OnMouseMove(mousePosition);
    this.boost.MouseOver = this.boost.Contains(mousePosition);
    this.arrowUp.MouseOver = this.arrowUp.Contains(mousePosition);
    this.arrowDown.MouseOver = this.arrowDown.Contains(mousePosition);
    if (!this.scroller.IsScrolling)
      return;
    this.ScrollerHandler();
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (Game.Me.Anchored)
      return false;
    if (base.OnMouseUp(mousePosition, mouseKey))
    {
      if (this.wasBgClick && this.background.Contains(mousePosition) && !this.scroller.Contains(mousePosition))
      {
        this.scroller.Value = Mathf.Clamp01((mousePosition.y - this.background.SmartRect.AbsPosition.y) / this.background.SmartRect.Height + 0.5f);
        this.ScrollerHandler();
      }
      this.wasBgClick = false;
      this.arrowUp.OnMouseUp(mousePosition, mouseKey);
      this.arrowDown.OnMouseUp(mousePosition, mouseKey);
      return true;
    }
    this.wasBgClick = false;
    this.arrowUp.IsPressed = false;
    this.arrowDown.IsPressed = false;
    return false;
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    return base.OnKeyDown(keyboardKey, action);
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (base.OnKeyUp(keyboardKey, action))
      return true;
    switch (action)
    {
      case Action.Boost:
      case Action.ToggleBoost:
        if (this.boosterBlinker.IsRendered)
        {
          StoryProtocol.GetProtocol().TriggerControl(StoryProtocol.ControlType.Boosters);
          break;
        }
        break;
    }
    return false;
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.boost.RecalculateAbsCoords();
    this.boosterBlinker.Position = this.boost.Position;
    this.boosterBlinker.RecalculateAbsCoords();
    this.arrowUp.RecalculateAbsCoords();
    this.arrowDown.RecalculateAbsCoords();
  }

  public void ToggleBoost(bool boostActive)
  {
    this.boost.IsPressed = boostActive;
  }

  public void SetBlink(StoryProtocol.ControlType button, bool value)
  {
    switch (button)
    {
      case StoryProtocol.ControlType.ThrottleBar:
        this.throttleBlinker.IsRendered = value;
        break;
      case StoryProtocol.ControlType.Boosters:
        this.boosterBlinker.IsRendered = value;
        break;
      case StoryProtocol.ControlType.MatchSpeed:
        this.matchSpeedBlinker.IsRendered = value;
        break;
      case StoryProtocol.ControlType.Follow:
        this.followBlinker.IsRendered = value;
        break;
    }
  }
}
