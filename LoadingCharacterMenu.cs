﻿// Decompiled with JetBrains decompiler
// Type: LoadingCharacterMenu
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;
using UnityEngine.UI;

public class LoadingCharacterMenu : MonoBehaviour
{
  public Image loadingImage;
  public Text text;

  private void Awake()
  {
    this.loadingImage.sprite = Resources.Load<UnityEngine.Sprite>(Game.Me.Faction != Faction.Cylon ? "GUI/LoadingScene/cic" : "GUI/LoadingScene/_cylon/cic");
    this.loadingImage.enabled = true;
    this.text.text = BsgoLocalization.Get("bgo.highscore.loading");
  }
}
