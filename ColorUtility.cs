﻿// Decompiled with JetBrains decompiler
// Type: ColorUtility
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class ColorUtility
{
  public static Color MakeColorFrom0To255(uint r, uint g, uint b, uint a)
  {
    return new Color(ColorUtility.Clamp01From0To255((float) r), ColorUtility.Clamp01From0To255((float) g), ColorUtility.Clamp01From0To255((float) b), ColorUtility.Clamp01From0To255((float) a));
  }

  public static Color MakeColorFrom0To255(uint r, uint g, uint b)
  {
    return new Color(ColorUtility.Clamp01From0To255((float) r), ColorUtility.Clamp01From0To255((float) g), ColorUtility.Clamp01From0To255((float) b));
  }

  private static float Clamp01From0To255(float value)
  {
    return Mathf.Clamp01(value / (float) byte.MaxValue);
  }
}
