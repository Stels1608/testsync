﻿// Decompiled with JetBrains decompiler
// Type: LooksCamera
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LooksCamera : CommonCamera
{
  public float speedRotate = 100f;
  public float limitLookRotation = 45f;
  public float minZoomDistance = 6f;
  public float maxZoomDistance = 50f;
  private float currentZoomDistance = 10f;
  public float speedMoving = 0.05f;
  private float deltaMoving = 1f;
  private Transform target;
  private Vector3 startPosition;
  private Vector3 endPosition;
  private float xMaxPower;
  private float xLookVelocity;
  public float yMaxPower;
  private float yLookVelocity;

  public void SetTarget(Transform target)
  {
    this.target = target;
    this.transform.LookAt(target.position);
  }

  private void Update()
  {
    if ((Object) this.target == (Object) null)
      return;
    this.Moving();
    if ((double) this.deltaMoving < 1.0)
      return;
    if (Input.GetKey(KeyCode.Mouse1))
    {
      float f1 = Input.GetAxis("Mouse X") * Time.deltaTime * this.speedRotate;
      if ((double) Mathf.Abs(this.xMaxPower) < (double) Mathf.Abs(f1) && (double) Mathf.Abs(f1) < 10.0)
        this.xMaxPower = f1;
      float f2 = Input.GetAxis("Mouse Y") * Time.deltaTime * this.speedRotate;
      if ((double) Mathf.Abs(this.yMaxPower) < (double) Mathf.Abs(f2) && (double) Mathf.Abs(f2) < 10.0)
        this.yMaxPower = f2;
    }
    this.xMaxPower = Mathf.SmoothDamp(this.xMaxPower, 0.0f, ref this.xLookVelocity, 0.3f);
    this.transform.RotateAround(this.target.position, new Vector3(0.0f, 1f, 0.0f), this.xMaxPower);
    Vector3 axis = -Vector3.Cross(this.target.position - this.transform.position, this.target.position - this.transform.TransformPoint(new Vector3(0.0f, 1f, 0.0f)));
    this.yMaxPower = Mathf.SmoothDamp(this.yMaxPower, 0.0f, ref this.yLookVelocity, 0.3f);
    this.transform.RotateAround(this.target.position, axis, this.yMaxPower);
    if ((double) Vector3.Angle(Vector3.Scale(this.target.position - this.transform.position, new Vector3(1f, 0.0f, 1f)), this.target.position - this.transform.position) <= (double) this.limitLookRotation)
      return;
    this.transform.RotateAround(this.target.position, axis, -this.yMaxPower);
  }

  private void LateUpdate()
  {
    if ((Object) this.target != (Object) null && (double) this.deltaMoving > 1.0)
      this.transform.position = this.target.position - this.currentZoomDistance * this.transform.forward;
    this.UpdateRotation();
  }

  private void OnGUI()
  {
    if (Event.current.type == UnityEngine.EventType.ScrollWheel)
    {
      if ((double) Event.current.delta.y < 0.0)
        this.ZoomIn();
      if ((double) Event.current.delta.y > 0.0)
        this.ZoomOut();
    }
    if (Event.current.keyCode == KeyCode.UpArrow && Event.current.type == UnityEngine.EventType.KeyDown && Event.current.type != UnityEngine.EventType.Used)
      this.ZoomIn();
    if (Event.current.keyCode != KeyCode.DownArrow || Event.current.type != UnityEngine.EventType.KeyDown || Event.current.type == UnityEngine.EventType.Used)
      return;
    this.ZoomOut();
  }

  public void StartMoving(Transform targetTransform)
  {
    if ((double) this.deltaMoving < 1.0 || (Object) targetTransform == (Object) this.target)
      return;
    Vector3 vector3 = this.transform.position - this.target.position;
    this.target = targetTransform;
    this.startPosition = this.transform.position;
    this.endPosition = this.target.position + vector3;
    this.deltaMoving = 0.0f;
  }

  private void Moving()
  {
    if ((double) this.deltaMoving >= 1.0)
      return;
    this.transform.position = Vector3.Lerp(this.startPosition, this.endPosition, this.deltaMoving);
    this.deltaMoving += this.speedMoving;
    if ((double) this.deltaMoving < 1.0)
      return;
    this.transform.position = this.endPosition;
    this.transform.LookAt(this.target.position);
  }

  private void ZoomIn()
  {
    if ((double) this.currentZoomDistance <= (double) this.minZoomDistance)
      return;
    --this.currentZoomDistance;
  }

  private void ZoomOut()
  {
    if ((double) this.currentZoomDistance >= (double) this.maxZoomDistance)
      return;
    ++this.currentZoomDistance;
  }
}
