﻿// Decompiled with JetBrains decompiler
// Type: InputBindingOptionsBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputBindingOptionsBase : MonoBehaviour, ILocalizeable
{
  protected Dictionary<UserSetting, OptionsElement> controls = new Dictionary<UserSetting, OptionsElement>();
  [SerializeField]
  private UILabel optionsHeadlineLabel;
  [SerializeField]
  protected GameObject content;

  private void Awake()
  {
    this.InitOptionsElements();
  }

  private void Start()
  {
    this.ReloadLanguageData();
  }

  public List<UserSetting> CoveredSettings()
  {
    return new List<UserSetting>((IEnumerable<UserSetting>) this.controls.Keys);
  }

  public virtual void UpdateSettings(UserSettings userSettings)
  {
    using (Dictionary<UserSetting, OptionsElement>.Enumerator enumerator = this.controls.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<UserSetting, OptionsElement> current = enumerator.Current;
        UserSetting key = current.Key;
        object obj = userSettings.Get(current.Key);
        OptionsElement optionsElement = current.Value;
        if (key == UserSetting.MouseWheelBinding)
          obj = (object) OptionsEnumTransformer.MouseWheelWrapped(userSettings.MouseWheel);
        optionsElement.Init(key, obj);
      }
    }
  }

  protected abstract void InitOptionsElements();

  public void ToggleSettingVisibility(UserSetting setting)
  {
    if (!this.controls.ContainsKey(setting))
      return;
    this.controls[setting].gameObject.SetActive(!this.controls[setting].gameObject.activeSelf);
    this.content.GetComponent<UIGrid>().Reposition();
  }

  public virtual void InjectOnChangeDelegate(OnSettingChanged changed)
  {
    using (Dictionary<UserSetting, OptionsElement>.Enumerator enumerator = this.controls.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.OnSettingChanged = changed;
    }
  }

  public virtual void ReloadLanguageData()
  {
    this.optionsHeadlineLabel.text = Tools.ParseMessage("%$bgo.ui.options.control.controlsetting%");
  }
}
