﻿// Decompiled with JetBrains decompiler
// Type: SpaceObjectState
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class SpaceObjectState : IProtocolRead
{
  public uint ObjectId { get; private set; }

  public uint Revision { get; private set; }

  public bool Marked { get; private set; }

  public bool Fortified { get; private set; }

  public float BaseSignature { get; private set; }

  public bool Cloaked { get; private set; }

  public bool ShortCircuited { get; private set; }

  public void Read(BgoProtocolReader br)
  {
    this.ObjectId = br.ReadUInt32();
    this.Revision = br.ReadUInt32();
    this.Marked = br.ReadBoolean();
    this.Fortified = br.ReadBoolean();
    this.BaseSignature = br.ReadSingle();
    this.Cloaked = br.ReadBoolean();
    this.ShortCircuited = br.ReadBoolean();
  }

  public override string ToString()
  {
    return string.Format("ObjectId: {0}, Revision: {1}, Marked: {2}, Fortified: {3}, BaseSignature: {4}, Cloaked: {5}, ShortCircuited: {6}", (object) this.ObjectId, (object) this.Revision, (object) this.Marked, (object) this.Fortified, (object) this.BaseSignature, (object) this.Cloaked, (object) this.ShortCircuited);
  }
}
