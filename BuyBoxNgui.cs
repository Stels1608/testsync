﻿// Decompiled with JetBrains decompiler
// Type: BuyBoxNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class BuyBoxNgui : BuyBoxUi
{
  [SerializeField]
  private UILabel headerLabel;
  [SerializeField]
  private UIButton okButton;
  [SerializeField]
  private UILabel okButtonLabel;
  [SerializeField]
  private UILabel cancelButtonLabel;
  [SerializeField]
  private UILabel messageLabel;
  [SerializeField]
  private UILabel costLabel;
  [SerializeField]
  private PriceTagNgui priceTagPrototype;
  private List<PriceTagNgui> priceList;

  protected override void Awake()
  {
    this.priceTagPrototype.gameObject.SetActive(false);
    this.priceList = new List<PriceTagNgui>();
  }

  protected override void SetTitleText(string text)
  {
    this.headerLabel.text = text;
  }

  protected override void SetOkButtonText(string text)
  {
    this.okButtonLabel.text = text;
  }

  protected override void EnableOkButton(bool enableOkButton)
  {
    this.okButton.isEnabled = enableOkButton;
  }

  protected override void SetCancelButtonText(string text)
  {
    this.cancelButtonLabel.text = text;
  }

  protected override void SetMainText(string mainText)
  {
    this.messageLabel.text = mainText;
  }

  protected override void SetCostLabel(string costText)
  {
    this.costLabel.text = costText;
  }

  protected override void ClearPrices()
  {
    using (List<PriceTagNgui>.Enumerator enumerator = this.priceList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current.gameObject);
    }
  }

  protected override void AddResourcePrice(string iconPath, float cost)
  {
    GameObject gameObject = NGUITools.AddChild(this.priceTagPrototype.transform.parent.gameObject, this.priceTagPrototype.gameObject);
    gameObject.SetActive(true);
    PriceTagNgui component = gameObject.GetComponent<PriceTagNgui>();
    component.SetCost(cost);
    component.SetResourceIcon(iconPath);
  }
}
