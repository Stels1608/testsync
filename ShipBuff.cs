﻿// Decompiled with JetBrains decompiler
// Type: ShipBuff
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ShipBuff : IProtocolRead
{
  public uint ServerID;
  public uint AbilityGuid;
  public ShipAbilityCard Ability;
  public GUICard GuiCard;
  public float EndTime;
  public float MaxTime;
  private Flag isLoaded;

  public float TimeLeft
  {
    get
    {
      return this.EndTime - Time.time;
    }
  }

  public Flag IsLoaded
  {
    get
    {
      return this.isLoaded;
    }
  }

  public ShipBuff()
  {
    this.isLoaded = new Flag();
  }

  public void Read(BgoProtocolReader r)
  {
    this.ServerID = r.ReadUInt32();
    this.AbilityGuid = r.ReadGUID();
    this.MaxTime = r.ReadSingle();
    this.EndTime = Time.time + this.MaxTime;
    this.Ability = (ShipAbilityCard) Game.Catalogue.FetchCard(this.AbilityGuid, CardView.ShipAbility);
    this.GuiCard = (GUICard) Game.Catalogue.FetchCard(this.AbilityGuid, CardView.GUI);
    this.IsLoaded.Depend((ILoadable) this.Ability, (ILoadable) this.GuiCard);
    this.IsLoaded.Set();
  }
}
