﻿// Decompiled with JetBrains decompiler
// Type: LevelUpNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class LevelUpNotification : OnScreenNotification
{
  private readonly string rank;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.LevelUp;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Special;
    }
  }

  public override string TextMessage
  {
    get
    {
      return BsgoLocalization.Get("%$bgo.notif.rank_promoted%", (object) this.rank);
    }
  }

  public override bool Show
  {
    get
    {
      return true;
    }
  }

  public LevelUpNotification(string rank)
  {
    this.rank = rank;
  }
}
