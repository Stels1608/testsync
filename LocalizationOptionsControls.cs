﻿// Decompiled with JetBrains decompiler
// Type: LocalizationOptionsControls
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class LocalizationOptionsControls : MonoBehaviour
{
  [SerializeField]
  private UILabel headlineCombat;
  [SerializeField]
  private UILabel headlineFlight;
  [SerializeField]
  private UILabel headlineCamera;
  [SerializeField]
  private UILabel headlineSocial;
  [SerializeField]
  private UILabel headlineUI;
  [SerializeField]
  private UILabel headlineHUD;
  [SerializeField]
  private UILabel headlineMisc;
  [SerializeField]
  private UILabel headlinePopup;
  [SerializeField]
  private UILabel targetNearestEnemy;
  [SerializeField]
  private UILabel targetNearestFriendly;
  [SerializeField]
  private UILabel cycleThroughEnemyTargets;
  [SerializeField]
  private UILabel cycleThroughFriendlyTargets;
  [SerializeField]
  private UILabel nextEnemyTarget;
  [SerializeField]
  private UILabel previousEnemyTarget;
  [SerializeField]
  private UILabel nextFriendlyTarget;
  [SerializeField]
  private UILabel previousFriendlyTarget;
  [SerializeField]
  private UILabel cancelTarget;
  [SerializeField]
  private UILabel targetNearestMissile;
  [SerializeField]
  private UILabel toggleAllGuns;
  [SerializeField]
  private UILabel fireAllMissiles;
  [SerializeField]
  private UILabel weaponSlot_1;
  [SerializeField]
  private UILabel weaponSlot_2;
  [SerializeField]
  private UILabel weaponSlot_3;
  [SerializeField]
  private UILabel weaponSlot_4;
  [SerializeField]
  private UILabel weaponSlot_5;
  [SerializeField]
  private UILabel weaponSlot_6;
  [SerializeField]
  private UILabel weaponSlot_7;
  [SerializeField]
  private UILabel weaponSlot_8;
  [SerializeField]
  private UILabel weaponSlot_9;
  [SerializeField]
  private UILabel weaponSlot_10;
  [SerializeField]
  private UILabel abilitySlot_1;
  [SerializeField]
  private UILabel abilitySlot_2;
  [SerializeField]
  private UILabel abilitySlot_3;
  [SerializeField]
  private UILabel abilitySlot_4;
  [SerializeField]
  private UILabel abilitySlot_5;
  [SerializeField]
  private UILabel abilitySlot_6;
  [SerializeField]
  private UILabel abilitySlot_7;
  [SerializeField]
  private UILabel abilitySlot_8;
  [SerializeField]
  private UILabel abilitySlot_9;
  [SerializeField]
  private UILabel abilitySlot_10;
  [SerializeField]
  private UILabel initiateJumpSequence;
  [SerializeField]
  private UILabel ceaseJumpSequence;
  [SerializeField]
  private UILabel pitchUp;
  [SerializeField]
  private UILabel pitchDown;
  [SerializeField]
  private UILabel rollLeft;
  [SerializeField]
  private UILabel rollRight;
  [SerializeField]
  private UILabel increaseSpeed;
  [SerializeField]
  private UILabel decreaseSpeed;
  [SerializeField]
  private UILabel setSpeed;
  [SerializeField]
  private UILabel booster;
  [SerializeField]
  private UILabel fullSpeed;
  [SerializeField]
  private UILabel idleSpeed;
  [SerializeField]
  private UILabel tollowTarget;
  [SerializeField]
  private UILabel matchTargetSpeed;
  [SerializeField]
  private UILabel toggleCameraToTargetMode;
  [SerializeField]
  private UILabel toggleCameraToChaseMode;
  [SerializeField]
  private UILabel toggleCameraToFreeMode;
  [SerializeField]
  private UILabel toggleCameraToNoseMode;
  [SerializeField]
  private UILabel toggleCameraMode;
  [SerializeField]
  private UILabel zoomIn;
  [SerializeField]
  private UILabel zoomOut;
  [SerializeField]
  private UILabel zoom;
  [SerializeField]
  private UILabel moveFocusToChat;
  [SerializeField]
  private UILabel moveFocusOutOfChat;
  [SerializeField]
  private UILabel replyToWhisper;
  [SerializeField]
  private UILabel hold_Inventory;
  [SerializeField]
  private UILabel pilotLog;
  [SerializeField]
  private UILabel optionsMenu;
  [SerializeField]
  private UILabel leaderboard;
  [SerializeField]
  private UILabel wingRoster;
  [SerializeField]
  private UILabel sectorNavigation;
  [SerializeField]
  private UILabel systemMap;
  [SerializeField]
  private UILabel assignments;
  [SerializeField]
  private UILabel duties;
  [SerializeField]
  private UILabel skills;
  [SerializeField]
  private UILabel shipStatus;
  [SerializeField]
  private UILabel inFlightResupply;
  [SerializeField]
  private UILabel toggleNames;
  [SerializeField]
  private UILabel toggleCombatGUI;
  [SerializeField]
  private UILabel takeAllLoot;
  [SerializeField]
  private UILabel popupTextInfo;
  [SerializeField]
  private UILabel popupTextProceed;
  [SerializeField]
  private UILabel popupTextAlreadyMapped;
  [SerializeField]
  private UILabel popupTextUnmapped;
  [SerializeField]
  private UILabel popupButtonCancel;
  [SerializeField]
  private UILabel popupButtonRemove;

  private void Start()
  {
    if ((Object) null != (Object) this.headlineCombat)
      this.headlineCombat.text = Tools.ParseMessage(string.Empty);
    if ((Object) null != (Object) this.headlineFlight)
      this.headlineFlight.text = Tools.ParseMessage(string.Empty);
    if ((Object) null != (Object) this.headlineCamera)
      this.headlineCamera.text = Tools.ParseMessage(string.Empty);
    if ((Object) null != (Object) this.headlineSocial)
      this.headlineSocial.text = Tools.ParseMessage(string.Empty);
    if ((Object) null != (Object) this.headlineUI)
      this.headlineUI.text = Tools.ParseMessage(string.Empty);
    if ((Object) null != (Object) this.headlineHUD)
      this.headlineHUD.text = Tools.ParseMessage(string.Empty);
    if ((Object) null != (Object) this.headlineMisc)
      this.headlineMisc.text = Tools.ParseMessage(string.Empty);
    if (!((Object) null != (Object) this.headlinePopup))
      return;
    this.headlinePopup.text = Tools.ParseMessage(string.Empty);
  }
}
