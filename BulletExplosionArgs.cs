﻿// Decompiled with JetBrains decompiler
// Type: BulletExplosionArgs
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class BulletExplosionArgs : ExplosionArgs
{
  public BulletExplosionArgs.BulletExplosionView ExplosionView;
  public Faction GunnerFaction;
  public Faction TargetFaction;

  public override string ToString()
  {
    return string.Format("BulletExplosionArgs: ExplosionView: {0}, GunnerFaction: {1}, TargetFaction: {2}", (object) this.ExplosionView, (object) this.GunnerFaction, (object) this.TargetFaction);
  }

  public enum BulletExplosionView
  {
    Railgun,
  }
}
