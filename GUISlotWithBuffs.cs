﻿// Decompiled with JetBrains decompiler
// Type: GUISlotWithBuffs
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System.Collections.Generic;
using UnityEngine;

public class GUISlotWithBuffs : GUISlot
{
  private float2 buffPivotPosition = float2.zero;
  protected const float UPDATE_BUFF_INTERVAL = 0.1f;
  protected const float UPDATE_LEVEL_INTERVAL = 0.1f;
  private AtlasCache atlasCache;
  private Color factionColor;
  private SmartRect buffRect;
  protected bool drawBuffs;
  protected List<ShipBuff> buffs;
  private GUICountdown cooldown;
  protected float timeToUpdateBuffs;
  private GUILabelNew level;
  protected float timeToUpdateLevel;

  public string LevelString
  {
    set
    {
      this.level.Text = value;
      this.level.MakeCenteredRect();
      this.level.RecalculateAbsCoords();
    }
  }

  public GUISlotWithBuffs(SmartRect parent)
    : base(parent)
  {
    this.atlasCache = new AtlasCache(new float2(40f, 35f));
    this.buffRect = new SmartRect(this.background.SmartRect);
    this.buffRect.Rect = new Rect(0.0f, 0.0f, this.atlasCache.ElementSize.x / 2f, this.atlasCache.ElementSize.y / 2f);
    this.buffs = new List<ShipBuff>();
    JWindowDescription jwindowDescription = Loaders.LoadResource("GUI/Slots/layout");
    this.level = (jwindowDescription["level"] as JLabel).CreateGUILabelNew(this.root);
    this.buffPivotPosition = (jwindowDescription["buffPivot"] as JPivot).position;
    this.cooldown = new GUICountdown((Texture2D) ResourceLoader.Load("GUI/AbilityToolbar/cooldown_atlas"), 8U, 8U);
    this.factionColor = Game.Me.Faction != Faction.Colonial ? ColorUtility.MakeColorFrom0To255((uint) byte.MaxValue, 117U, 31U) : ColorUtility.MakeColorFrom0To255((uint) byte.MaxValue, 69U, 105U);
    this.factionColor.a = 0.5f;
  }

  public void LevelColor(float modifier)
  {
    Color color = Color.white;
    if ((double) modifier < 0.25)
      color = Color.grey;
    else if ((double) modifier < 0.800000011920929)
      color = Color.yellow;
    else if ((double) modifier > 1.20000004768372)
      color = Color.red;
    this.level.NormalColor = color;
    this.level.OverColor = color;
  }

  public override void Draw()
  {
    base.Draw();
    this.level.Draw();
    if (!this.drawBuffs)
      return;
    float2 float2 = this.buffPivotPosition;
    for (int index = 0; index < this.buffs.Count; ++index)
    {
      AtlasEntry atlasEntry = this.atlasCache.UnknownItem;
      if (this.buffs[index].GuiCard != null)
        atlasEntry = this.atlasCache.GetCachedEntryBy(this.buffs[index].GuiCard.GUIAtlasTexturePath, (int) this.buffs[index].GuiCard.FrameIndex);
      this.buffRect.Position = new float2(float2.x + this.buffRect.Width * (float) index, float2.y);
      this.buffRect.RecalculateAbsCoords();
      if (Event.current.type == UnityEngine.EventType.Repaint)
        Graphics.DrawTexture(this.buffRect.AbsRect, (Texture) atlasEntry.Texture, atlasEntry.FrameRect, 0, 0, 0, 0);
      this.cooldown.Progress = (double) this.buffs[index].MaxTime != 3.40282346638529E+38 ? (float) (1.0 - (double) this.buffs[index].TimeLeft / (double) this.buffs[index].MaxTime) : 1f;
      if (Event.current.type == UnityEngine.EventType.Repaint)
        Graphics.DrawTexture(this.buffRect.AbsRect, (Texture) this.cooldown.inner.Texture, this.cooldown.inner.FrameRect, 0, 0, 0, 0, this.factionColor);
    }
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.level.RecalculateAbsCoords();
  }

  public override bool Contains(float2 point)
  {
    if (base.Contains(point))
      return true;
    float2 float2 = new float2((float) (-(double) this.background.Rect.width / 2.0 + 15.0), (float) ((double) this.background.Rect.height / 2.0 + (double) this.buffRect.Height / 2.0));
    for (int index = 0; index < this.buffs.Count; ++index)
    {
      this.buffRect.Position = new float2(float2.x + this.buffRect.Width * (float) index, float2.y);
      if (this.buffRect.AbsRect.Contains(point.ToV2()))
        return true;
    }
    return false;
  }

  public override bool OnShowTooltip(float2 mousePosition)
  {
    float2 float2 = this.buffPivotPosition;
    for (int index = 0; index < this.buffs.Count; ++index)
    {
      this.buffRect.Position = new float2(float2.x + this.buffRect.Width * (float) index, float2.y);
      if (this.buffRect.AbsRect.Contains(mousePosition.ToV2()))
      {
        this.SetTooltip(this.buffs[index]);
        Game.TooltipManager.ShowTooltip(this.advancedTooltip);
        return true;
      }
    }
    return false;
  }
}
