﻿// Decompiled with JetBrains decompiler
// Type: LegCycleData
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class LegCycleData
{
  public Vector3 cycleCenter;
  public float cycleScaling;
  public Vector3 cycleDirection;
  public float stanceTime;
  public float liftTime;
  public float liftoffTime;
  public float postliftTime;
  public float prelandTime;
  public float strikeTime;
  public float landTime;
  public float cycleDistance;
  public Vector3 stancePosition;
  public Vector3 heelToetipVector;
  public LegCycleSample[] samples;
  public int stanceIndex;
  public CycleDebugInfo debugInfo;
}
