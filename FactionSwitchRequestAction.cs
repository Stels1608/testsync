﻿// Decompiled with JetBrains decompiler
// Type: FactionSwitchRequestAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using Bigpoint.Core.Mvc.View;

public class FactionSwitchRequestAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    CharacterService characterService = ((CharacterServiceDataProvider) this.OwnerFacade.FetchDataProvider("CharacterServiceDataProvider")).GetCharacterService(CharacterServiceId.FactionSwitch);
    if (!characterService.CooldownExpired)
    {
      NotifyBoxUi notifyBox = MessageBoxFactory.CreateNotifyBox(BsgoCanvas.CharacterMenu);
      notifyBox.SetContent((OnMessageBoxClose) (param0 => FacadeFactory.GetInstance().SendMessage(Message.CancelFactionSwitch)), "%$bgo.character_menu.faction_switch.title%", "%$bgo.character_menu.faction_switch.cooldown%", characterService.RemainingTimeInSeconds);
      notifyBox.ShowTimer(true);
    }
    else if (!characterService.Eligible)
    {
      NotifyBoxUi notifyBox = MessageBoxFactory.CreateNotifyBox(BsgoCanvas.CharacterMenu);
      notifyBox.SetContent((OnMessageBoxClose) (param0 => FacadeFactory.GetInstance().SendMessage(Message.CancelFactionSwitch)), "%$bgo.character_menu.faction_switch.title%", "%$bgo.character_menu.faction_switch.not eligible%", 3600000U);
      notifyBox.ShowTimer(false);
    }
    else
      this.OwnerFacade.AttachView((IView<Message>) new FactionSwitchMediator(characterService));
  }
}
