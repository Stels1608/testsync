﻿// Decompiled with JetBrains decompiler
// Type: Tasharen.DataNode
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using UnityEngine;

namespace Tasharen
{
  public class DataNode
  {
    private static object[] mInvokeParams = new object[1];
    private static Dictionary<string, System.Type> mNameToType = new Dictionary<string, System.Type>();
    private static Dictionary<System.Type, string> mTypeToName = new Dictionary<System.Type, string>();
    public List<DataNode> children = new List<DataNode>();
    public string name;
    public object value;

    public System.Type type
    {
      get
      {
        if (this.value != null)
          return this.value.GetType();
        return typeof (void);
      }
    }

    public object Get(System.Type type)
    {
      return DataNode.ConvertValue(this.value, type);
    }

    public T Get<T>()
    {
      if (this.value is T)
        return (T) this.value;
      object obj = this.Get(typeof (T));
      if (this.value != null)
        return (T) obj;
      return default (T);
    }

    public DataNode AddChild()
    {
      DataNode dataNode = new DataNode();
      this.children.Add(dataNode);
      return dataNode;
    }

    public DataNode AddChild(string name)
    {
      DataNode dataNode = this.AddChild();
      dataNode.name = name;
      return dataNode;
    }

    public DataNode AddChild(string name, object value)
    {
      DataNode dataNode = this.AddChild();
      dataNode.name = name;
      dataNode.value = !(value is Enum) ? value : (object) value.ToString();
      return dataNode;
    }

    public DataNode SetChild(string name, object value)
    {
      DataNode dataNode = this.GetChild(name) ?? this.AddChild();
      dataNode.name = name;
      dataNode.value = !(value is Enum) ? value : (object) value.ToString();
      return dataNode;
    }

    public DataNode GetChild(string name)
    {
      for (int index = 0; index < this.children.Count; ++index)
      {
        if (this.children[index].name == name)
          return this.children[index];
      }
      return (DataNode) null;
    }

    public T GetChild<T>(string name)
    {
      DataNode child = this.GetChild(name);
      if (child == null)
        return default (T);
      return child.Get<T>();
    }

    public T GetChild<T>(string name, T defaultValue)
    {
      DataNode child = this.GetChild(name);
      if (child == null)
        return defaultValue;
      return child.Get<T>();
    }

    public void Write(StreamWriter writer)
    {
      this.Write(writer, 0);
    }

    public void Read(StreamReader reader)
    {
      string nextLine = DataNode.GetNextLine(reader);
      int tabs = DataNode.CalculateTabs(nextLine);
      this.Read(reader, nextLine, ref tabs);
    }

    public void Clear()
    {
      this.value = (object) null;
      this.children.Clear();
    }

    private string GetValueDataString()
    {
      if (this.value is float)
        return ((float) this.value).ToString((IFormatProvider) CultureInfo.InvariantCulture);
      if (this.value is Vector2)
      {
        Vector2 vector2 = (Vector2) this.value;
        return vector2.x.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + vector2.y.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      }
      if (this.value is Vector3)
      {
        Vector3 vector3 = (Vector3) this.value;
        return vector3.x.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + vector3.y.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + vector3.z.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      }
      if (this.value is Vector4)
      {
        Vector4 vector4 = (Vector4) this.value;
        return vector4.x.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + vector4.y.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + vector4.z.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + vector4.w.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      }
      if (this.value is Quaternion)
      {
        Vector3 eulerAngles = ((Quaternion) this.value).eulerAngles;
        return eulerAngles.x.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + eulerAngles.y.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + eulerAngles.z.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      }
      if (this.value is Color)
      {
        Color color = (Color) this.value;
        return color.r.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + color.g.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + color.b.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + color.a.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      }
      if (this.value is Color32)
      {
        Color color = (Color) ((Color32) this.value);
        return ((double) color.r).ToString() + ", " + (object) color.g + ", " + (object) color.b + ", " + (object) color.a;
      }
      if (this.value is Rect)
      {
        Rect rect = (Rect) this.value;
        return rect.x.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + rect.y.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + rect.width.ToString((IFormatProvider) CultureInfo.InvariantCulture) + ", " + rect.height.ToString((IFormatProvider) CultureInfo.InvariantCulture);
      }
      if (this.value != null)
        return this.value.ToString().Replace("\n", "\\n");
      return string.Empty;
    }

    private string GetValueString()
    {
      if (this.type == typeof (string))
        return "\"" + this.value + "\"";
      if (this.type == typeof (Vector2) || this.type == typeof (Vector3) || this.type == typeof (Color))
        return "(" + this.GetValueDataString() + ")";
      return string.Format("{0}({1})", (object) DataNode.TypeToName(this.type), (object) this.GetValueDataString());
    }

    private bool SetValue(string text, System.Type type, string[] parts)
    {
      if (type == null || type == typeof (void))
        this.value = (object) null;
      else if (type == typeof (string))
        this.value = (object) text;
      else if (type == typeof (bool))
      {
        bool result;
        if (bool.TryParse(text, out result))
          this.value = (object) result;
      }
      else if (type == typeof (byte))
      {
        byte result;
        if (byte.TryParse(text, out result))
          this.value = (object) result;
      }
      else if (type == typeof (short))
      {
        short result;
        if (short.TryParse(text, out result))
          this.value = (object) result;
      }
      else if (type == typeof (ushort))
      {
        ushort result;
        if (ushort.TryParse(text, out result))
          this.value = (object) result;
      }
      else if (type == typeof (int))
      {
        int result;
        if (int.TryParse(text, out result))
          this.value = (object) result;
      }
      else if (type == typeof (uint))
      {
        uint result;
        if (uint.TryParse(text, out result))
          this.value = (object) result;
      }
      else if (type == typeof (float))
      {
        float result;
        if (float.TryParse(text, NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out result))
          this.value = (object) result;
      }
      else if (type == typeof (double))
      {
        double result;
        if (double.TryParse(text, NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out result))
          this.value = (object) result;
      }
      else if (type == typeof (Vector2))
      {
        if (parts == null)
          parts = text.Split(',');
        Vector2 vector2;
        if (parts.Length == 2 && float.TryParse(parts[0], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector2.x) && float.TryParse(parts[1], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector2.y))
          this.value = (object) vector2;
      }
      else if (type == typeof (Vector3))
      {
        if (parts == null)
          parts = text.Split(',');
        Vector3 vector3;
        if (parts.Length == 3 && float.TryParse(parts[0], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector3.x) && (float.TryParse(parts[1], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector3.y) && float.TryParse(parts[2], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector3.z)))
          this.value = (object) vector3;
      }
      else if (type == typeof (Vector4))
      {
        if (parts == null)
          parts = text.Split(',');
        Vector4 vector4;
        if (parts.Length == 4 && float.TryParse(parts[0], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector4.x) && (float.TryParse(parts[1], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector4.y) && float.TryParse(parts[2], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector4.z)) && float.TryParse(parts[3], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector4.w))
          this.value = (object) vector4;
      }
      else if (type == typeof (Quaternion))
      {
        if (parts == null)
          parts = text.Split(',');
        Quaternion quaternion;
        if (parts.Length == 4 && float.TryParse(parts[0], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out quaternion.x) && (float.TryParse(parts[1], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out quaternion.y) && float.TryParse(parts[2], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out quaternion.z)) && float.TryParse(parts[3], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out quaternion.w))
          this.value = (object) quaternion;
      }
      else if (type == typeof (Color32))
      {
        if (parts == null)
          parts = text.Split(',');
        Color32 color32;
        if (parts.Length == 4 && byte.TryParse(parts[0], out color32.r) && (byte.TryParse(parts[1], out color32.g) && byte.TryParse(parts[2], out color32.b)) && byte.TryParse(parts[3], out color32.a))
          this.value = (object) color32;
      }
      else if (type == typeof (Color))
      {
        if (parts == null)
          parts = text.Split(',');
        Color color;
        if (parts.Length == 4 && float.TryParse(parts[0], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out color.r) && (float.TryParse(parts[1], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out color.g) && float.TryParse(parts[2], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out color.b)) && float.TryParse(parts[3], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out color.a))
          this.value = (object) color;
      }
      else if (type == typeof (Rect))
      {
        if (parts == null)
          parts = text.Split(',');
        Vector4 vector4;
        if (parts.Length == 4 && float.TryParse(parts[0], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector4.x) && (float.TryParse(parts[1], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector4.y) && float.TryParse(parts[2], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector4.z)) && float.TryParse(parts[3], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture, out vector4.w))
          this.value = (object) new Rect(vector4.x, vector4.y, vector4.z, vector4.w);
      }
      else
      {
        if (type.IsSubclassOf(typeof (Component)))
          return false;
        try
        {
          MethodInfo method = type.GetMethod("FromString", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
          if (method == null)
            return false;
          DataNode.mInvokeParams[0] = (object) text.Replace("\\n", "\n");
          this.value = method.Invoke((object) null, DataNode.mInvokeParams);
        }
        catch (Exception ex)
        {
          Debug.LogWarning((object) ex.Message);
          return false;
        }
      }
      return true;
    }

    public override string ToString()
    {
      string data = string.Empty;
      this.Write(ref data, 0);
      return data;
    }

    private void Write(ref string data, int tab)
    {
      if (string.IsNullOrEmpty(this.name))
        return;
      for (int index = 0; index < tab; ++index)
        data = data + "\t";
      data = data + DataNode.Escape(this.name);
      if (this.value != null)
        data = data + " = " + this.GetValueString();
      data = data + "\n";
      for (int index = 0; index < this.children.Count; ++index)
        this.children[index].Write(ref data, tab + 1);
    }

    private void Write(StreamWriter writer, int tab)
    {
      if (string.IsNullOrEmpty(this.name))
        return;
      for (int index = 0; index < tab; ++index)
        writer.Write("\t");
      writer.Write(DataNode.Escape(this.name));
      if (this.value != null)
      {
        writer.Write(" = ");
        writer.Write(this.GetValueString());
      }
      writer.Write("\n");
      for (int index = 0; index < this.children.Count; ++index)
        this.children[index].Write(writer, tab + 1);
    }

    private string Read(StreamReader reader, string line, ref int offset)
    {
      if (line != null)
      {
        int offset1 = offset;
        this.Set(line, offset1);
        line = DataNode.GetNextLine(reader);
        offset = DataNode.CalculateTabs(line);
        while (line != null && offset == offset1 + 1)
          line = this.AddChild().Read(reader, line, ref offset);
      }
      return line;
    }

    private bool Set(string line, int offset)
    {
      int num1 = line.IndexOf("=", offset);
      if (num1 == -1)
      {
        this.name = DataNode.Unescape(line.Substring(offset)).Trim();
        return true;
      }
      this.name = DataNode.Unescape(line.Substring(offset, num1 - offset)).Trim();
      line = line.Substring(num1 + 1).Trim();
      if (line.Length < 3)
        return false;
      if ((int) line[0] == 34 && (int) line[line.Length - 1] == 34)
      {
        this.value = (object) line.Substring(1, line.Length - 2);
        return true;
      }
      if ((int) line[0] == 40 && (int) line[line.Length - 1] == 41)
      {
        line = line.Substring(1, line.Length - 2);
        string[] parts = line.Split(',');
        if (parts.Length == 1)
          return this.SetValue(line, typeof (float), (string[]) null);
        if (parts.Length == 2)
          return this.SetValue(line, typeof (Vector2), parts);
        if (parts.Length == 3)
          return this.SetValue(line, typeof (Vector3), parts);
        if (parts.Length == 4)
          return this.SetValue(line, typeof (Color), parts);
        this.value = (object) line;
        return true;
      }
      System.Type type = typeof (string);
      int num2 = line.IndexOf('(');
      if (num2 != -1)
      {
        int num3 = (int) line[line.Length - 1] != 41 ? line.LastIndexOf(')', num2) : line.Length - 1;
        if (num3 != -1 && line.Length > 2)
        {
          type = DataNode.NameToType(line.Substring(0, num2));
          line = line.Substring(num2 + 1, num3 - num2 - 1);
        }
      }
      return this.SetValue(line, type, (string[]) null);
    }

    private static string GetNextLine(StreamReader reader)
    {
      string str = reader.ReadLine();
      while (str != null && str.Trim().StartsWith("//"))
      {
        str = reader.ReadLine();
        if (str == null)
          return (string) null;
      }
      return str;
    }

    private static int CalculateTabs(string line)
    {
      if (line != null)
      {
        for (int index = 0; index < line.Length; ++index)
        {
          if ((int) line[index] != 9)
            return index;
        }
      }
      return 0;
    }

    private static string Escape(string val)
    {
      if (!string.IsNullOrEmpty(val))
      {
        val = val.Replace("\n", "\\n");
        val = val.Replace("\t", "\\t");
      }
      return val;
    }

    private static string Unescape(string val)
    {
      if (!string.IsNullOrEmpty(val))
      {
        val = val.Replace("\\n", "\n");
        val = val.Replace("\\t", "\t");
      }
      return val;
    }

    private static System.Type NameToType(string name)
    {
      System.Type type;
      if (!DataNode.mNameToType.TryGetValue(name, out type))
      {
        type = System.Type.GetType(name);
        if (type == null)
        {
          if (name == "String")
            type = typeof (string);
          else if (name == "Vector2")
            type = typeof (Vector2);
          else if (name == "Vector3")
            type = typeof (Vector3);
          else if (name == "Vector4")
            type = typeof (Vector4);
          else if (name == "Quaternion")
            type = typeof (Quaternion);
          else if (name == "Color")
            type = typeof (Color);
          else if (name == "Rect")
            type = typeof (Rect);
          else if (name == "Color32")
            type = typeof (Color32);
        }
        DataNode.mNameToType[name] = type;
      }
      return type;
    }

    private static string TypeToName(System.Type type)
    {
      string str;
      if (!DataNode.mTypeToName.TryGetValue(type, out str))
      {
        str = type.ToString();
        if (str.StartsWith("System."))
          str = str.Substring(7);
        if (str.StartsWith("UnityEngine."))
          str = str.Substring(12);
        DataNode.mTypeToName[type] = str;
      }
      return str;
    }

    private static object ConvertValue(object value, System.Type type)
    {
      if (type.IsAssignableFrom(value.GetType()))
        return value;
      if (type.IsEnum)
      {
        if (value.GetType() == typeof (int))
          return value;
        if (value.GetType() == typeof (string))
        {
          string str = (string) value;
          if (!string.IsNullOrEmpty(str))
          {
            string[] names = Enum.GetNames(type);
            for (int index = 0; index < names.Length; ++index)
            {
              if (names[index] == str)
                return Enum.GetValues(type).GetValue(index);
            }
          }
        }
      }
      return (object) null;
    }
  }
}
