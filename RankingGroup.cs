﻿// Decompiled with JetBrains decompiler
// Type: RankingGroup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum RankingGroup : ushort
{
  Kills = 1,
  VictoriesDefeatRatio = 2,
  VictoriesHour = 3,
  Mining = 4,
  Progression = 5,
  Objectives = 6,
  PlanetoidMining = 7,
  Wave = 8,
  Arena1vs1 = 9,
  Arena3vs3 = 10,
  KillActions = 11,
  SupportActions = 12,
  DamageActions = 13,
}
