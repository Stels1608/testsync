﻿// Decompiled with JetBrains decompiler
// Type: AvatarChangeMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class AvatarChangeMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "AvatarChangeMediator";
  private CharacterModification view;

  public AvatarChangeMediator()
    : base("AvatarChangeMediator")
  {
    this.view = Object.FindObjectOfType<CharacterSceneRoot>().characterModification;
    this.view.Show();
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.ConfirmAvatarChange);
    this.AddMessageInterest(Message.CancelAvatarChange);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.CancelAvatarChange:
      case Message.ConfirmAvatarChange:
        this.OwnerFacade.DetachView("AvatarChangeMediator");
        break;
    }
  }

  public override void OnBeforeDetach()
  {
    if (!((Object) this.view != (Object) null))
      return;
    this.view.Hide();
  }
}
