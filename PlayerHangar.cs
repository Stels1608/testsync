﻿// Decompiled with JetBrains decompiler
// Type: PlayerHangar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;

public class PlayerHangar
{
  private readonly Dictionary<ushort, HangarShip> ships = new Dictionary<ushort, HangarShip>();
  private HangarShip activeShip;
  public AnonymousDelegate OnActiveShipChanged;

  public HangarShip this[ushort id]
  {
    get
    {
      HangarShip hangarShip = (HangarShip) null;
      this.ships.TryGetValue(id, out hangarShip);
      return hangarShip;
    }
  }

  public IEnumerable<HangarShip> Ships
  {
    get
    {
      return (IEnumerable<HangarShip>) this.ships.Values;
    }
  }

  public HangarShip ActiveShip
  {
    get
    {
      return this.activeShip;
    }
  }

  public int ShipCount
  {
    get
    {
      return this.ships.Values.Count;
    }
  }

  public int UpgradeShipCount
  {
    get
    {
      int num = 0;
      using (Dictionary<ushort, HangarShip>.Enumerator enumerator = this.ships.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Value.IsUpgraded)
            ++num;
        }
      }
      return num;
    }
  }

  public void SelectShip(HangarShip ship)
  {
    PlayerProtocol.GetProtocol().SelectShip(ship.ServerID);
  }

  public void AddShip(uint GUID)
  {
    PlayerProtocol.GetProtocol().AddShip(GUID);
  }

  public void UpgradeShip(HangarShip ship)
  {
    PlayerProtocol.GetProtocol().UpgradeShip(ship.ServerID);
  }

  public void Clear()
  {
    this.ships.Clear();
    this.activeShip = (HangarShip) null;
  }

  public void _SetActiveShip(ushort activeShipID)
  {
    this.activeShip = this.ships[activeShipID];
    if (this.OnActiveShipChanged == null)
      return;
    this.activeShip.IsLoaded.AddHandler((SignalHandler) (() => this.OnActiveShipChanged()));
  }

  public void _AddShip(HangarShip newShip)
  {
    if (this.activeShip != null && (int) this.activeShip.ServerID == (int) newShip.ServerID)
      this.activeShip = newShip;
    this.ships[newShip.ServerID] = newShip;
  }

  public void _RemoveShip(ushort shipID)
  {
    this.ships.Remove(shipID);
  }
}
