﻿// Decompiled with JetBrains decompiler
// Type: GUIChatButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using UnityEngine;

internal class GUIChatButton : GUIButtonNew
{
  private Color blinkColor = new Color(0.0f, 0.0f, 0.0f, 1f);
  private float delta = 1f;
  protected SmartRect hitRect = new SmartRect();
  protected bool isBlinking;

  public SmartRect HitRect
  {
    get
    {
      return this.hitRect;
    }
  }

  public override bool MouseOver
  {
    set
    {
      if (this.IsRendered && !this.MouseOver && value)
        this.OnMouseOver();
      this.mouseOver = value;
    }
  }

  public override bool IsPressed
  {
    set
    {
      base.IsPressed = value;
      this.TextLabel.StyleColor = !this.IsPressed ? this.TextLabel.NormalColor : this.TextLabel.OverColor;
      if (!this.IsPressed)
        return;
      this.IsBlinking = false;
    }
  }

  public bool IsBlinking
  {
    get
    {
      return this.isBlinking;
    }
    set
    {
      this.isBlinking = value;
    }
  }

  public GUIChatButton()
  {
    this.hitRect.Parent = this.SmartRect;
    this.TextLabel.AutoSize = true;
  }

  public GUIChatButton(GUIChatButton toCopy)
    : base((GUIButtonNew) toCopy)
  {
    this.hitRect.Parent = this.SmartRect;
    this.TextLabel.AutoSize = true;
    this.HitRect.Width = toCopy.HitRect.Width;
    this.HitRect.Height = toCopy.HitRect.Height;
  }

  public GUIChatButton(JButton button)
    : base(button)
  {
    this.hitRect.Parent = this.SmartRect;
    this.TextLabel.AutoSize = true;
    this.AdoptHitRect();
  }

  public void AdoptHitRect()
  {
    this.HitRect.Width = this.SmartRect.Width - 6f;
    this.HitRect.Height = this.SmartRect.Height - 4f;
  }

  public override void Update()
  {
    base.Update();
    if (!this.IsBlinking)
      return;
    this.blinkColor.a += (float) ((double) this.delta * (double) Time.deltaTime * 2.0);
    if ((double) this.blinkColor.a < 0.0 || (double) this.blinkColor.a > 1.0)
      this.delta = -this.delta;
    this.blinkColor.a = Mathf.Clamp01(this.blinkColor.a);
  }

  public override void Draw()
  {
    base.Draw();
    Color color = GUI.color;
    if (Event.current.type == UnityEngine.EventType.Repaint)
    {
      Texture2D texture2D = (Texture2D) null;
      if (this.IsPressed)
        texture2D = this.pressedTexture;
      else if (this.MouseOver && (Object) this.overTexture != (Object) null)
        texture2D = this.overTexture;
      if (this.IsBlinking)
      {
        GUI.color = this.blinkColor;
        if ((Object) this.pressedTexture != (Object) null)
          GUI.DrawTexture(this.root.AbsRect, (Texture) this.pressedTexture);
        GUI.color = color;
      }
      if ((Object) texture2D != (Object) null)
        Graphics.DrawTexture(this.root.AbsRect, (Texture) texture2D, 5, 5, 5, 5);
    }
    if (this.IsPressed && this.IsBlinking)
      GUI.color = this.blinkColor;
    if (this.TextLabel.IsRendered)
      this.TextLabel.Draw();
    if (!this.IsPressed || !this.IsBlinking)
      return;
    GUI.color = color;
  }

  public override bool Contains(float2 point)
  {
    return this.HitRect.AbsRect.Contains(point.ToV2());
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.hitRect.RecalculateAbsCoords();
  }
}
