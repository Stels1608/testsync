﻿// Decompiled with JetBrains decompiler
// Type: StarterKit
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class StarterKit : ShipItem
{
  public StarterKitCard Card;

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Card = (StarterKitCard) Game.Catalogue.FetchCard(this.CardGUID, CardView.StarterKit);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.Card
    });
  }

  public override bool SetItemIcon(ref GuiImage image, Vector2 elementSize)
  {
    image.Texture = this.Card.icon;
    image.SourceRect = new Rect(0.0f, 0.0f, 1f, 1f);
    return (Object) image.Texture != (Object) null;
  }
}
