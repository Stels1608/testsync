﻿// Decompiled with JetBrains decompiler
// Type: AvatarCylonCustomizer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AvatarCylonCustomizer
{
  private static readonly Dictionary<string, string> itemsColor = new Dictionary<string, string>();
  private static readonly Dictionary<string, int> itemsMaterial = new Dictionary<string, int>();
  private static readonly Dictionary<string, string> itemsVersion = new Dictionary<string, string>();
  private const int notFound = -1;
  private const string race = "cylon";
  private static Avatar avatar;
  private static bool init;

  public static Avatar Avatar
  {
    set
    {
      AvatarCylonCustomizer.avatar = value;
      if (!AvatarCylonCustomizer.init)
      {
        AvatarCylonMaterials.Parse(AvatarSelector.GetAllMaterials(AvatarCylonCustomizer.avatar.Race, AvatarCylonCustomizer.avatar.Sex));
        AvatarCylonCustomizer.init = true;
      }
      using (List<string>.Enumerator enumerator = AvatarInfo.GetItemsOfRace("cylon").GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          AvatarCylonCustomizer.itemsColor[current] = string.Empty;
          AvatarCylonCustomizer.NextItemColor(current, true);
          AvatarCylonCustomizer.itemsMaterial[current] = 0;
        }
      }
      AvatarCylonCustomizer.UpdateItemsVersion();
      AvatarCylonCustomizer.UpdateMaterials();
    }
  }

  public static void UpdateItemsVersion()
  {
    using (List<string>.Enumerator enumerator = AvatarInfo.GetItemsOfRace("cylon").GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        AvatarCylonCustomizer.itemsVersion[current] = AvatarCylonCustomizer.GetItemVersion(AvatarCylonCustomizer.avatar.GetProperty(current));
      }
    }
  }

  public static void UpdateMaterials()
  {
    AvatarCylonCustomizer.avatar.Lock();
    using (List<string>.Enumerator enumerator = AvatarInfo.GetItemsOfRace("cylon").GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        string material = AvatarCylonMaterials.GetMaterial(current, AvatarCylonCustomizer.itemsVersion[current], AvatarCylonCustomizer.itemsColor[current], AvatarCylonCustomizer.itemsMaterial[current]);
        AvatarCylonCustomizer.avatar.SetProperty(AvatarInfo.GetMaterialFromItem(current), material);
      }
    }
    AvatarCylonCustomizer.avatar.Unlock();
  }

  public static bool NextItemMaterial(string item, bool next)
  {
    int materialsCount = AvatarCylonMaterials.GetMaterialsCount(item, AvatarCylonCustomizer.itemsVersion[item], AvatarCylonCustomizer.itemsColor[item]);
    bool outRange;
    AvatarCylonCustomizer.itemsMaterial[item] = AvatarCylonCustomizer.NextIndex(AvatarCylonCustomizer.itemsMaterial[item], materialsCount, next, out outRange);
    return outRange;
  }

  public static string GetItemVersion(string objectName)
  {
    using (List<string>.Enumerator enumerator = AvatarCylonMaterials.GetVersions().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        if (objectName.Contains(current))
          return current;
      }
    }
    return "not_version";
  }

  public static void NextItemColor(string item, bool next)
  {
    if (!AvatarCylonCustomizer.itemsColor.ContainsKey(item))
      return;
    List<string> colors = AvatarCylonMaterials.GetColors();
    AvatarCylonCustomizer.itemsColor[item] = AvatarCylonCustomizer.NextItemProperty(colors, AvatarCylonCustomizer.itemsColor[item], next);
  }

  private static int GetIndex(List<string> list, string val)
  {
    for (int index = 0; index < list.Count; ++index)
    {
      if (list[index] == val)
        return index;
    }
    return -1;
  }

  private static string NextItemProperty(List<string> property, string curVal, bool next)
  {
    int count = property.Count;
    if (count == 0)
      return "not_set";
    int index1 = AvatarCylonCustomizer.GetIndex(property, curVal);
    if (index1 == -1)
      return property[0];
    bool outRange;
    int index2 = AvatarCylonCustomizer.NextIndex(index1, count, next, out outRange);
    return property[index2];
  }

  private static int NextIndex(int index, int count, bool next, out bool outRange)
  {
    outRange = false;
    if (next)
      ++index;
    else
      --index;
    if (index >= count)
    {
      index = 0;
      outRange = true;
    }
    if (index < 0)
    {
      index = count - 1;
      outRange = true;
    }
    return index;
  }
}
