﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorOutsideScreenArrowUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class HudIndicatorOutsideScreenArrowUgui : HudIndicatorBaseComponentUgui, IOutsideScreenArrow, IReceivesSetting, IReceivesTarget, IRequiresLateUpdate
{
  private bool sectorEventArrowEnabledInSettings = true;
  [SerializeField]
  protected Image arrow;
  [SerializeField]
  protected Canvas canvas;
  [SerializeField]
  protected CanvasGroup canvasGroup;
  private float timeLastAlphaFlip;
  private ISpaceEntity target;

  protected virtual bool ScaleDown
  {
    get
    {
      if (this.target.SpaceEntityType == SpaceEntityType.SectorEvent)
        return true;
      if (this.IsMultiselected)
        return !this.IsSelected;
      return false;
    }
  }

  protected override void ResetComponentStates()
  {
    this.timeLastAlphaFlip = 0.0f;
    this.target = (ISpaceEntity) null;
  }

  protected override void Awake()
  {
    base.Awake();
    this.canvas.enabled = this.GetComponentInParent<Canvas>().enabled;
  }

  public void RemoteLateUpdate()
  {
    if (!this.gameObject.activeSelf)
      return;
    this.arrow.transform.localScale = Vector3.one * (!this.ScaleDown ? 1f : 0.5f);
    this.timeLastAlphaFlip += Time.deltaTime;
    if ((double) this.timeLastAlphaFlip <= 0.100000001490116)
      return;
    this.canvasGroup.alpha = (double) this.canvasGroup.alpha != 1.0 ? 1f : 0.7f;
    this.timeLastAlphaFlip = 0.0f;
  }

  public void OnOrientationChanged(float orientation)
  {
    this.arrow.transform.rotation = Quaternion.Euler(0.0f, 0.0f, (float) (-(double) orientation * 360.0));
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.target = target;
    this.UpdateColorAndAlpha();
  }

  protected override void SetColoring()
  {
    Color color = this.target.SpaceEntityType != SpaceEntityType.SectorEvent ? (!this.IsDesignated || this.IsSelected ? (!this.IsActiveWaypoint || this.IsSelected ? HudIndicatorColorScheme.Instance.BracketColor(this.target) : TargetColors.waypointTargetColor) : TargetColors.designatedTargetColor) : TargetColors.sectorEventColor;
    color.a = 1f;
    this.arrow.color = color;
  }

  protected override void UpdateView()
  {
    bool flag = this.target.SpaceEntityType == SpaceEntityType.SectorEvent ? !this.InScreen && this.sectorEventArrowEnabledInSettings : !this.InScreen && (this.IsSelected || this.IsMultiselected || this.IsDesignated || this.IsActiveWaypoint);
    this.gameObject.SetActive(flag);
    if (!flag)
      return;
    this.UpdateColorAndAlpha();
  }

  public override bool RequiresScreenBorderClamping()
  {
    return this.gameObject.activeSelf;
  }

  public void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    switch (userSetting)
    {
      case UserSetting.HudIndicatorShowMissionArrow:
        this.sectorEventArrowEnabledInSettings = (bool) data;
        break;
      case UserSetting.HudIndicatorColorScheme:
        this.SetColoring();
        break;
    }
    this.UpdateView();
  }
}
