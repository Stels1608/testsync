﻿// Decompiled with JetBrains decompiler
// Type: GUIShopTooltipBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIShopTooltipBox : GUIPanel
{
  private const float APPEAR_INTERVAL = 1f;
  private float time;

  public GUIShopTooltipBox(SmartRect parent)
    : base(parent)
  {
    this.IsUpdated = false;
  }

  protected void Appear(float2 absPos)
  {
    absPos.x = Mathf.Min(absPos.x, (float) Screen.width - this.SmartRect.Width);
    absPos.y = Mathf.Max(absPos.y, this.SmartRect.Height);
    float2 float2 = this.Parent == null ? absPos : absPos - this.Parent.AbsPosition;
    this.Position = new float2(float2.x + this.SmartRect.Width / 2f, float2.y - this.SmartRect.Height / 2f);
    this.Appear();
  }

  protected void Appear()
  {
    this.IsRendered = false;
    this.IsUpdated = true;
    this.time = 0.0f;
  }

  protected void Disappear()
  {
    this.IsRendered = false;
    this.IsUpdated = false;
    this.time = 0.0f;
  }

  public override void Update()
  {
    if ((double) this.time >= 1.0)
      this.IsRendered = true;
    else
      this.time += Time.deltaTime;
    base.Update();
  }

  public override bool Contains(float2 point)
  {
    return false;
  }
}
