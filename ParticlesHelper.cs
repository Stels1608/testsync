﻿// Decompiled with JetBrains decompiler
// Type: ParticlesHelper
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ParticlesHelper : MonoBehaviour
{
  public float emitStartDelay;
  public float preSimulateTime;
  public float maxEmitTime;
  private ParticlesHelper.EmitState state;
  private float stateTimer;

  private void Start()
  {
    if ((double) this.emitStartDelay > 0.0)
    {
      this.stateTimer = 0.0f;
      this.GetComponent<ParticleEmitter>().GetComponent<Renderer>().enabled = false;
      this.GetComponent<ParticleEmitter>().enabled = false;
      this.state = ParticlesHelper.EmitState.Waiting;
    }
    else
      this.BeginEmit();
  }

  private void BeginEmit()
  {
    this.GetComponent<ParticleEmitter>().enabled = true;
    this.GetComponent<ParticleEmitter>().GetComponent<Renderer>().enabled = true;
    if ((double) this.preSimulateTime > 0.0)
    {
      int @int = Mathf.FloorToInt(this.preSimulateTime);
      for (int index = 0; index < @int; ++index)
        this.GetComponent<ParticleEmitter>().Simulate(1f);
      this.GetComponent<ParticleEmitter>().Simulate(Mathf.Repeat(this.preSimulateTime, 1f));
    }
    this.stateTimer = 0.0f;
    this.state = ParticlesHelper.EmitState.Emitting;
  }

  private void EndEmit()
  {
    this.GetComponent<ParticleEmitter>().emit = false;
    this.state = ParticlesHelper.EmitState.Finished;
  }

  private void Update()
  {
    switch (this.state)
    {
      case ParticlesHelper.EmitState.Waiting:
        this.stateTimer += Time.deltaTime;
        if ((double) this.stateTimer < (double) this.emitStartDelay)
          break;
        this.BeginEmit();
        break;
      case ParticlesHelper.EmitState.Emitting:
        this.stateTimer += Time.deltaTime;
        if ((double) this.stateTimer < (double) this.maxEmitTime)
          break;
        this.EndEmit();
        break;
    }
  }

  private enum EmitState
  {
    Waiting,
    Emitting,
    Finished,
  }
}
