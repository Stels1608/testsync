﻿// Decompiled with JetBrains decompiler
// Type: CharacterServiceDataProvider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Model;
using System;
using System.Collections.Generic;
using System.Linq;

public class CharacterServiceDataProvider : DataProvider<Message>
{
  public List<CharacterService> CharacterServices { get; set; }

  public CharacterServiceDataProvider()
    : base("CharacterServiceDataProvider")
  {
  }

  public CharacterService GetCharacterService(CharacterServiceId serviceId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.CharacterServices.FirstOrDefault<CharacterService>(new Func<CharacterService, bool>(new CharacterServiceDataProvider.\u003CGetCharacterService\u003Ec__AnonStorey76() { serviceId = serviceId }.\u003C\u003Em__69));
  }
}
