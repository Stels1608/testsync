﻿// Decompiled with JetBrains decompiler
// Type: ShipAbstractAbility
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public abstract class ShipAbstractAbility
{
  public ShipAbilityCard card;

  protected bool CheckTarget(SpaceObject target)
  {
    if (target == null || target.Dead || (!this.TargetRelationCheck(target) || !this.TargetTypeCheck(target)) || !this.TargetTierCheck(target))
      return false;
    PlayerShip playerShip = target as PlayerShip;
    if (playerShip != null && !playerShip.IsVisible)
      return playerShip.IsMe;
    return true;
  }

  protected bool TargetRelationCheck(SpaceObject playerTarget)
  {
    HashSet<ShipAbilitySide> shipAbilitySideSet = SpaceLevel.GetLevel().Card.regulationCard.AbilityTargetRelations[this.card.AbilityGroupId];
    if (playerTarget == null || playerTarget.IsMinion)
      return false;
    if (shipAbilitySideSet.Contains(ShipAbilitySide.Any))
      return true;
    switch (playerTarget.PlayerRelation)
    {
      case Relation.Friend:
        return shipAbilitySideSet.Contains(ShipAbilitySide.Friend);
      case Relation.Enemy:
        return shipAbilitySideSet.Contains(ShipAbilitySide.Enemy);
      case Relation.Neutral:
        return shipAbilitySideSet.Contains(ShipAbilitySide.Neutral);
      case Relation.Self:
        return shipAbilitySideSet.Contains(ShipAbilitySide.Self);
      default:
        return false;
    }
  }

  protected bool TargetTypeCheck(SpaceObject playerTarget)
  {
    HashSet<ShipAbilityTarget> shipAbilityTargetSet = SpaceLevel.GetLevel().Card.regulationCard.AbilityTargetTypes[this.card.AbilityGroupId];
    if (shipAbilityTargetSet.Count == 0 || shipAbilityTargetSet.Contains(ShipAbilityTarget.Any))
      return true;
    if (playerTarget is Ship && playerTarget.SpaceEntityType != SpaceEntityType.AsteroidBot)
      return shipAbilityTargetSet.Contains(ShipAbilityTarget.Ship);
    if (playerTarget.SpaceEntityType == SpaceEntityType.Asteroid || playerTarget.SpaceEntityType == SpaceEntityType.AsteroidBot)
      return shipAbilityTargetSet.Contains(ShipAbilityTarget.Asteroid);
    if (playerTarget.SpaceEntityType == SpaceEntityType.Planetoid)
      return shipAbilityTargetSet.Contains(ShipAbilityTarget.Planetoid);
    if (playerTarget.SpaceEntityType == SpaceEntityType.Missile)
      return shipAbilityTargetSet.Contains(ShipAbilityTarget.Missile);
    if (playerTarget.SpaceEntityType == SpaceEntityType.Mine || playerTarget.SpaceEntityType == SpaceEntityType.MineField)
      return shipAbilityTargetSet.Contains(ShipAbilityTarget.Mine);
    if (playerTarget.SpaceEntityType == SpaceEntityType.JumpTargetTransponder)
      return shipAbilityTargetSet.Contains(ShipAbilityTarget.JumpTargetTransponder);
    if (playerTarget.SpaceEntityType == SpaceEntityType.Comet)
      return shipAbilityTargetSet.Contains(ShipAbilityTarget.Comet);
    return false;
  }

  protected bool TargetTierCheck(SpaceObject playerTarget)
  {
    if (!(playerTarget is Ship))
      return true;
    HashSet<ShipAbilityTargetTier> abilityTargetTierSet = this.card.TargetTiers;
    if (abilityTargetTierSet.Contains(ShipAbilityTargetTier.Any))
      return true;
    if (abilityTargetTierSet.Count == 0)
      return false;
    ShipAbilityTargetTier @enum = ShipAbilityCard.TierToEnum((int) (playerTarget as Ship).ShipCardLight.Tier);
    return abilityTargetTierSet.Contains(@enum);
  }

  protected SpaceObject GetTarget()
  {
    SpaceLevel level = SpaceLevel.GetLevel();
    if ((Object) level == (Object) null)
      return (SpaceObject) null;
    return level.GetPlayerTarget();
  }
}
