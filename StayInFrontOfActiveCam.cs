﻿// Decompiled with JetBrains decompiler
// Type: StayInFrontOfActiveCam
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StayInFrontOfActiveCam : MonoBehaviour
{
  private void Awake()
  {
    this.StayInFront();
  }

  private void Update()
  {
    this.StayInFront();
  }

  private void LateUpdate()
  {
    this.StayInFront();
  }

  private void StayInFront()
  {
    Transform transform = !SectorEditorHelper.IsEditorLevel() ? SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera().transform : EditorLevel.GetActiveCamera.transform;
    this.transform.rotation = transform.rotation;
    this.transform.position = transform.position + transform.forward * 10f;
  }
}
