﻿// Decompiled with JetBrains decompiler
// Type: UIDragDropItem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Drag and Drop Item")]
public class UIDragDropItem : MonoBehaviour
{
  [HideInInspector]
  public float pressAndHoldDelay = 1f;
  protected int mTouchID = int.MinValue;
  public UIDragDropItem.Restriction restriction;
  public bool cloneOnDrag;
  protected Transform mTrans;
  protected Transform mParent;
  protected Collider mCollider;
  protected Collider2D mCollider2D;
  protected UIButton mButton;
  protected UIRoot mRoot;
  protected UIGrid mGrid;
  protected UITable mTable;
  protected float mDragStartTime;
  protected UIDragScrollView mDragScrollView;
  protected bool mPressed;
  protected bool mDragging;

  protected virtual void Start()
  {
    this.mTrans = this.transform;
    this.mCollider = this.GetComponent<Collider>();
    this.mCollider2D = this.GetComponent<Collider2D>();
    this.mButton = this.GetComponent<UIButton>();
    this.mDragScrollView = this.GetComponent<UIDragScrollView>();
  }

  protected void OnPress(bool isPressed)
  {
    if (isPressed)
    {
      this.mDragStartTime = RealTime.time + this.pressAndHoldDelay;
      this.mPressed = true;
    }
    else
      this.mPressed = false;
  }

  protected virtual void Update()
  {
    if (this.restriction != UIDragDropItem.Restriction.PressAndHold || !this.mPressed || (this.mDragging || (double) this.mDragStartTime >= (double) RealTime.time))
      return;
    this.StartDragging();
  }

  protected void OnDragStart()
  {
    if (!this.enabled || this.mTouchID != int.MinValue)
      return;
    if (this.restriction != UIDragDropItem.Restriction.None)
    {
      if (this.restriction == UIDragDropItem.Restriction.Horizontal)
      {
        Vector2 vector2 = UICamera.currentTouch.totalDelta;
        if ((double) Mathf.Abs(vector2.x) < (double) Mathf.Abs(vector2.y))
          return;
      }
      else if (this.restriction == UIDragDropItem.Restriction.Vertical)
      {
        Vector2 vector2 = UICamera.currentTouch.totalDelta;
        if ((double) Mathf.Abs(vector2.x) > (double) Mathf.Abs(vector2.y))
          return;
      }
      else if (this.restriction == UIDragDropItem.Restriction.PressAndHold)
        return;
    }
    this.StartDragging();
  }

  protected virtual void StartDragging()
  {
    if (this.mDragging)
      return;
    this.mDragging = true;
    if (this.cloneOnDrag)
    {
      GameObject gameObject = NGUITools.AddChild(this.transform.parent.gameObject, this.gameObject);
      gameObject.transform.localPosition = this.transform.localPosition;
      gameObject.transform.localRotation = this.transform.localRotation;
      gameObject.transform.localScale = this.transform.localScale;
      UIButtonColor component1 = gameObject.GetComponent<UIButtonColor>();
      if ((Object) component1 != (Object) null)
        component1.defaultColor = this.GetComponent<UIButtonColor>().defaultColor;
      UICamera.currentTouch.dragged = gameObject;
      UIDragDropItem component2 = gameObject.GetComponent<UIDragDropItem>();
      component2.Start();
      component2.OnDragDropStart();
    }
    else
      this.OnDragDropStart();
  }

  protected void OnDrag(Vector2 delta)
  {
    if (!this.mDragging || !this.enabled || this.mTouchID != UICamera.currentTouchID)
      return;
    this.OnDragDropMove(delta * this.mRoot.pixelSizeAdjustment);
  }

  protected void OnDragEnd()
  {
    if (!this.enabled || this.mTouchID != UICamera.currentTouchID)
      return;
    this.StopDragging(UICamera.hoveredObject);
  }

  public void StopDragging(GameObject go)
  {
    if (!this.mDragging)
      return;
    this.mDragging = false;
    this.OnDragDropRelease(go);
  }

  protected virtual void OnDragDropStart()
  {
    if ((Object) this.mDragScrollView != (Object) null)
      this.mDragScrollView.enabled = false;
    if ((Object) this.mButton != (Object) null)
      this.mButton.isEnabled = false;
    else if ((Object) this.mCollider != (Object) null)
      this.mCollider.enabled = false;
    else if ((Object) this.mCollider2D != (Object) null)
      this.mCollider2D.enabled = false;
    this.mTouchID = UICamera.currentTouchID;
    this.mParent = this.mTrans.parent;
    this.mRoot = NGUITools.FindInParents<UIRoot>(this.mParent);
    this.mGrid = NGUITools.FindInParents<UIGrid>(this.mParent);
    this.mTable = NGUITools.FindInParents<UITable>(this.mParent);
    if ((Object) UIDragDropRoot.root != (Object) null)
      this.mTrans.parent = UIDragDropRoot.root;
    Vector3 localPosition = this.mTrans.localPosition;
    localPosition.z = 0.0f;
    this.mTrans.localPosition = localPosition;
    TweenPosition component1 = this.GetComponent<TweenPosition>();
    if ((Object) component1 != (Object) null)
      component1.enabled = false;
    SpringPosition component2 = this.GetComponent<SpringPosition>();
    if ((Object) component2 != (Object) null)
      component2.enabled = false;
    NGUITools.MarkParentAsChanged(this.gameObject);
    if ((Object) this.mTable != (Object) null)
      this.mTable.repositionNow = true;
    if (!((Object) this.mGrid != (Object) null))
      return;
    this.mGrid.repositionNow = true;
  }

  protected virtual void OnDragDropMove(Vector2 delta)
  {
    this.mTrans.localPosition += (Vector3) delta;
  }

  protected virtual void OnDragDropRelease(GameObject surface)
  {
    if (!this.cloneOnDrag)
    {
      this.mTouchID = int.MinValue;
      if ((Object) this.mButton != (Object) null)
        this.mButton.isEnabled = true;
      else if ((Object) this.mCollider != (Object) null)
        this.mCollider.enabled = true;
      else if ((Object) this.mCollider2D != (Object) null)
        this.mCollider2D.enabled = true;
      UIDragDropContainer dragDropContainer = !(bool) ((Object) surface) ? (UIDragDropContainer) null : NGUITools.FindInParents<UIDragDropContainer>(surface);
      if ((Object) dragDropContainer != (Object) null)
      {
        this.mTrans.parent = !((Object) dragDropContainer.reparentTarget != (Object) null) ? dragDropContainer.transform : dragDropContainer.reparentTarget;
        Vector3 localPosition = this.mTrans.localPosition;
        localPosition.z = 0.0f;
        this.mTrans.localPosition = localPosition;
      }
      else
        this.mTrans.parent = this.mParent;
      this.mParent = this.mTrans.parent;
      this.mGrid = NGUITools.FindInParents<UIGrid>(this.mParent);
      this.mTable = NGUITools.FindInParents<UITable>(this.mParent);
      if ((Object) this.mDragScrollView != (Object) null)
        this.StartCoroutine(this.EnableDragScrollView());
      NGUITools.MarkParentAsChanged(this.gameObject);
      if ((Object) this.mTable != (Object) null)
        this.mTable.repositionNow = true;
      if ((Object) this.mGrid != (Object) null)
        this.mGrid.repositionNow = true;
      this.OnDragDropEnd();
    }
    else
      NGUITools.Destroy((Object) this.gameObject);
  }

  protected virtual void OnDragDropEnd()
  {
  }

  [DebuggerHidden]
  protected IEnumerator EnableDragScrollView()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UIDragDropItem.\u003CEnableDragScrollView\u003Ec__Iterator0() { \u003C\u003Ef__this = this };
  }

  public enum Restriction
  {
    None,
    Horizontal,
    Vertical,
    PressAndHold,
  }
}
