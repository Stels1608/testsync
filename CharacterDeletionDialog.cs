﻿// Decompiled with JetBrains decompiler
// Type: CharacterDeletionDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CharacterDeletionDialog : CharacterServiceDialog
{
  public InputField confirmationPrompt;
  private string keyword;

  protected override void RegisterUiEventHandlers()
  {
    this.cancelButton.onClick.AddListener((UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.CancelCharacterDeletion)));
    this.confirmButton.onClick.AddListener(new UnityAction(this.Validate));
    EventTrigger eventTrigger = this.confirmationPrompt.gameObject.AddComponent<EventTrigger>();
    EventTrigger.Entry entry = new EventTrigger.Entry() { eventID = EventTriggerType.Select, callback = new EventTrigger.TriggerEvent() };
    entry.callback.AddListener(new UnityAction<BaseEventData>(this.OnConfirmationFieldClicked));
    eventTrigger.triggers = new List<EventTrigger.Entry>();
    eventTrigger.triggers.Add(entry);
    this.confirmationPrompt.placeholder.gameObject.SetActive(false);
  }

  protected override void OnCharacterServiceInjected()
  {
    this.Localize("deletion");
    this.keyword = BsgoLocalization.Get("bgo.character_menu.deletion.keyword").ToLowerInvariant();
    this.upperMessageLabel.text = BsgoLocalization.Get("bgo.character_menu.deletion.message", (object) ("<color=#fcc857>" + this.keyword.ToUpperInvariant() + "</color>"));
  }

  private void OnConfirmationFieldClicked(BaseEventData eventData)
  {
    this.confirmationPrompt.placeholder.gameObject.SetActive(false);
  }

  private void Validate()
  {
    if (this.confirmationPrompt.text.ToLowerInvariant() != this.keyword)
    {
      InputField inputField = this.confirmationPrompt;
      string str1 = string.Empty;
      this.confirmationPrompt.textComponent.text = str1;
      string str2 = str1;
      inputField.text = str2;
      this.confirmationPrompt.placeholder.GetComponent<Text>().text = BsgoLocalization.Get("bgo.character_menu.deletion.invalid_keyword");
      this.confirmationPrompt.placeholder.gameObject.SetActive(true);
    }
    else
      Game.QuitDelete();
  }
}
