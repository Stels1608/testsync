﻿// Decompiled with JetBrains decompiler
// Type: GUISelectVariant
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GUISelectVariant : GuiPanel
{
  private static readonly int INDEX_OF_UPGRADED_SHIP = -1;
  private static readonly float SPACING_FROM_TOP = 15f;
  private static readonly float SPACING_VERTICAL = 30f;
  private static readonly float SPACING_HORIZONTAL = 5f;
  private static readonly Texture2D FILTER_OVERLAY = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/StarterPack/FilterOverlay");
  private List<GuiButton> buttons = new List<GuiButton>();
  private float maxButtonWidth;
  private GuiImage backgroundImage;

  public GUISelectVariant(HangarShip ship, GUISelectVariant.VariantSelectCallback callback)
  {
    this.IsRendered = true;
    this.backgroundImage = new GuiImage(ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/StarterPack/FilterPanel"));
    this.AddChild((GuiElementBase) this.backgroundImage);
    GuiButton guiButton = this.MakeShipButton("%$bgo." + ship.GUICard.Key + ".Name_2%", ship.Card.HangarID, GUISelectVariant.INDEX_OF_UPGRADED_SHIP, callback);
    this.buttons.Add(guiButton);
    this.AddChild((GuiElementBase) guiButton, new Vector2(GUISelectVariant.SPACING_HORIZONTAL, GUISelectVariant.SPACING_FROM_TOP));
    ShipListCard shipList = StaticCards.Instance.GetShipList(Game.Me.Faction);
    for (int index = 0; index < ship.Card.VariantHangarIDs.Count; ++index)
    {
      ShipCard variantShipCard = shipList.GetVariantShipCard(ship.Card, (uint) index);
      if (variantShipCard != null && variantShipCard.HaveShip)
      {
        byte hangarId = variantShipCard.HangarID;
        this.buttons.Add(this.MakeShipButton("%$bgo." + shipList.GetVariantCard(hangarId).ItemGUICard.Key + ".Name%", ship.Card.HangarID, index, callback));
      }
    }
    this.Size = new Vector2(this.maxButtonWidth + 20f, 144f);
    this.backgroundImage.Size = this.Size;
    for (int index = 0; index < this.buttons.Count; ++index)
    {
      this.buttons[index].Size = new Vector2(this.maxButtonWidth, 25f);
      this.AddChild((GuiElementBase) this.buttons[index], new Vector2(GUISelectVariant.SPACING_HORIZONTAL, GUISelectVariant.SPACING_FROM_TOP + GUISelectVariant.SPACING_VERTICAL * (float) (index + 1)));
    }
  }

  private GuiButton MakeShipButton(string text, byte parentHangarId, int index, GUISelectVariant.VariantSelectCallback callback)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GUISelectVariant.\u003CMakeShipButton\u003Ec__AnonStorey9E buttonCAnonStorey9E = new GUISelectVariant.\u003CMakeShipButton\u003Ec__AnonStorey9E();
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey9E.callback = callback;
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey9E.index = index;
    // ISSUE: reference to a compiler-generated field
    buttonCAnonStorey9E.parentHangarId = parentHangarId;
    GuiButton guiButton = new GuiButton(text);
    guiButton.NormalTexture = (Texture2D) null;
    guiButton.OverTexture = GUISelectVariant.FILTER_OVERLAY;
    guiButton.PressedTexture = GUISelectVariant.FILTER_OVERLAY;
    // ISSUE: reference to a compiler-generated method
    guiButton.Pressed = new AnonymousDelegate(buttonCAnonStorey9E.\u003C\u003Em__139);
    guiButton.Font = Gui.Options.FontBGM_BT;
    guiButton.FontSize = 14;
    float num = (float) (TextUtility.CalcTextSize(guiButton.Label.TextParsed, guiButton.Font, guiButton.FontSize).width + 10);
    if ((double) num > (double) this.maxButtonWidth)
      this.maxButtonWidth = num;
    return guiButton;
  }

  public delegate void VariantSelectCallback(int index, byte variantHangarId);
}
