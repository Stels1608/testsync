﻿// Decompiled with JetBrains decompiler
// Type: CatalogueProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class CatalogueProtocol : BgoProtocol
{
  private Dictionary<uint, HashSet<ushort>> requestedCards = new Dictionary<uint, HashSet<ushort>>();
  private Catalogue catalogue;

  public CatalogueProtocol(Catalogue catalogue)
    : base(BgoProtocol.ProtocolID.Catalogue)
  {
    this.catalogue = catalogue;
  }

  public void RequestCard(uint cardGUID, CardView cardView)
  {
    HashSet<ushort> ushortSet;
    if (!this.requestedCards.TryGetValue(cardGUID, out ushortSet))
    {
      ushortSet = new HashSet<ushort>();
      this.requestedCards.Add(cardGUID, ushortSet);
    }
    ushortSet.Add((ushort) cardView);
  }

  public override void UpdateMessage()
  {
    int num = 0;
    using (Dictionary<uint, HashSet<ushort>>.ValueCollection.Enumerator enumerator = this.requestedCards.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HashSet<ushort> current = enumerator.Current;
        num += current.Count;
      }
    }
    if (num == 0)
      return;
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    bw.Write((ushort) num);
    using (Dictionary<uint, HashSet<ushort>>.Enumerator enumerator1 = this.requestedCards.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<uint, HashSet<ushort>> current1 = enumerator1.Current;
        uint key = current1.Key;
        using (HashSet<ushort>.Enumerator enumerator2 = current1.Value.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            ushort current2 = enumerator2.Current;
            bw.Write(key);
            bw.Write(current2);
          }
        }
      }
    }
    this.requestedCards.Clear();
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    CatalogueProtocol.Reply reply = (CatalogueProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    if (reply == CatalogueProtocol.Reply.Card)
      this.catalogue.FetchCard(br.ReadGUID(), (CardView) br.ReadUInt16()).Load(br);
    else
      DebugUtility.LogError("Unknown server reply in Catalogue protocol: " + (object) reply);
  }

  public enum Request : ushort
  {
    Card = 1,
  }

  public enum Reply : ushort
  {
    Card = 2,
  }
}
