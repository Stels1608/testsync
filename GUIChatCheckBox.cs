﻿// Decompiled with JetBrains decompiler
// Type: GUIChatCheckBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;

internal class GUIChatCheckBox : GUICheckBox
{
  protected SmartRect hitRect;

  public SmartRect HitRect
  {
    get
    {
      return this.hitRect;
    }
  }

  public bool WasMouseDown
  {
    get
    {
      return this.wasMouseDown;
    }
    set
    {
      this.wasMouseDown = value;
    }
  }

  public GUIChatCheckBox()
  {
    this.Init();
  }

  public GUIChatCheckBox(string text)
    : base(text)
  {
    this.Init();
  }

  public GUIChatCheckBox(string normal, string over, string pressed)
    : base(normal, over, pressed)
  {
    this.Init();
  }

  public GUIChatCheckBox(string text, string normal, string over, string pressed)
    : base(text, normal, over, pressed)
  {
    this.Init();
  }

  public GUIChatCheckBox(JButton desc)
    : base(desc)
  {
    this.Init();
  }

  private void Init()
  {
    this.Padding = 0;
    this.hitRect = new SmartRect(this.root);
    this.AdoptHitRect();
    this.TextLabel.AutoSize = true;
  }

  public void AdoptHitRect()
  {
    this.HitRect.Width = this.SmartRect.Width - 6f;
    this.HitRect.Height = this.SmartRect.Height - 6f;
  }

  public override bool Contains(float2 point)
  {
    return this.HitRect.AbsRect.Contains(point.ToV2());
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.hitRect.RecalculateAbsCoords();
  }
}
