﻿// Decompiled with JetBrains decompiler
// Type: NormalCharacterMotor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (CharacterController))]
public class NormalCharacterMotor : CharacterMotor
{
  public float maxRotationSpeed = 270f;
  private bool firstframe = true;

  private void UpdateFacingDirection()
  {
    float magnitude = this.desiredFacingDirection.magnitude;
    Vector3 to = this.alignCorrection * Util.ProjectOntoPlane(this.transform.rotation * this.desiredMovementDirection * (1f - magnitude) + this.desiredFacingDirection * magnitude, this.transform.up);
    if ((double) to.sqrMagnitude <= 0.00999999977648258)
      return;
    Vector3 view = Util.ProjectOntoPlane(Util.ConstantSlerp(this.transform.forward, to, this.maxRotationSpeed * Time.deltaTime), this.transform.up);
    Quaternion quaternion = new Quaternion();
    quaternion.SetLookRotation(view, this.transform.up);
    this.transform.rotation = quaternion;
  }

  private void UpdateVelocity()
  {
    CharacterController characterController = this.GetComponent(typeof (CharacterController)) as CharacterController;
    Vector3 v = characterController.velocity;
    if (this.firstframe)
    {
      v = Vector3.zero;
      this.firstframe = false;
    }
    if (this.grounded)
      v = Util.ProjectOntoPlane(v, this.transform.up);
    Vector3 vector3_1 = v;
    this.jumping = false;
    if (this.grounded)
    {
      Vector3 vector3_2 = this.desiredVelocity - v;
      if ((double) vector3_2.magnitude > (double) this.maxVelocityChange)
        vector3_2 = vector3_2.normalized * this.maxVelocityChange;
      vector3_1 += vector3_2;
      if (this.canJump && Input.GetButton("Jump"))
      {
        vector3_1 += this.transform.up * Mathf.Sqrt(2f * this.jumpHeight * this.gravity);
        this.jumping = true;
      }
    }
    Vector3 vector3_3 = vector3_1 + this.transform.up * -this.gravity * Time.deltaTime;
    if (this.jumping)
      vector3_3 -= this.transform.up * -this.gravity * Time.deltaTime / 2f;
    this.grounded = (characterController.Move(vector3_3 * Time.deltaTime) & CollisionFlags.Below) != CollisionFlags.None;
  }

  private void Update()
  {
    this.UpdateFacingDirection();
    this.UpdateVelocity();
  }
}
