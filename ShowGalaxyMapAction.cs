﻿// Decompiled with JetBrains decompiler
// Type: ShowGalaxyMapAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Tooltips;
using System.Collections.Generic;

internal class ShowGalaxyMapAction
{
  private List<IGUIPanel> oldVisiblePanels;

  public List<IGUIPanel> OldVisiblePanels
  {
    get
    {
      return this.oldVisiblePanels;
    }
  }

  public ShowGalaxyMapAction()
  {
    this.oldVisiblePanels = new List<IGUIPanel>();
  }

  public void Show()
  {
    FacadeFactory.GetInstance().SendMessage(Message.SettingChangedCombatGui, (object) false);
    Game.TooltipManager.HideTooltip((GuiAdvancedTooltipBase) null);
    this.oldVisiblePanels.Clear();
    using (List<IGUIPanel>.Enumerator enumerator = Game.GUIManager.panels.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IGUIPanel current = enumerator.Current;
        if (current.IsRendered && !(current is GalaxyMapMain) && (!(current is MessageBoxManager) && !(current is GuiTicker)) && (!(current is NotificationManager) && !(current is GuiAdvancedLabelTooltip)))
        {
          this.oldVisiblePanels.Add(current);
          current.IsRendered = false;
        }
      }
    }
  }

  public void Hide()
  {
    FacadeFactory.GetInstance().SendMessage(Message.ApplyCombatGui);
    using (List<IGUIPanel>.Enumerator enumerator = this.oldVisiblePanels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.IsRendered = true;
    }
    this.oldVisiblePanels.Clear();
  }
}
