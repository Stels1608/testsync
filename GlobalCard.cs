﻿// Decompiled with JetBrains decompiler
// Type: GlobalCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class GlobalCard : Card
{
  public float TitaniumRepairCard = 1f;
  public float CubitsRepairCard = 1f;
  public Dictionary<int, RewardCard> SpecialFriendBonus = new Dictionary<int, RewardCard>();
  public uint CapitalShipPrice;
  public float UndockTimeout;
  public RewardCard FriendBonus;

  public GlobalCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.TitaniumRepairCard = r.ReadSingle();
    this.CubitsRepairCard = r.ReadSingle();
    this.CapitalShipPrice = r.ReadUInt32();
    this.UndockTimeout = r.ReadSingle();
    this.FriendBonus = (RewardCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.Reward);
    int num = r.ReadLength();
    for (int index = 0; index < num; ++index)
      this.SpecialFriendBonus[(int) r.ReadByte()] = (RewardCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.Reward);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.FriendBonus
    });
    this.IsLoaded.Depend<RewardCard>((IEnumerable<RewardCard>) this.SpecialFriendBonus.Values);
  }
}
