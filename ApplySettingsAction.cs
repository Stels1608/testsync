﻿// Decompiled with JetBrains decompiler
// Type: ApplySettingsAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;

public class ApplySettingsAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    SettingsDataProvider settingsDataProvider = (SettingsDataProvider) this.OwnerFacade.FetchDataProvider("SettingsDataProvider");
    UserSettings newSettings = message.Data as UserSettings;
    UserSettings currentSettings = settingsDataProvider.CurrentSettings;
    UserSettings userSettings = newSettings == null ? currentSettings : currentSettings.Diff(newSettings);
    using (List<UserSetting>.Enumerator enumerator = userSettings.DefinedSettings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        this.SendMessage((IMessage<Message>) new ChangeSettingMessage(current, userSettings.Get(current), false));
      }
    }
    this.OwnerFacade.SendMessage(Message.SaveSettings);
  }
}
