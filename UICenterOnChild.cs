﻿// Decompiled with JetBrains decompiler
// Type: UICenterOnChild
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Center Scroll View on Child")]
public class UICenterOnChild : MonoBehaviour
{
  public float springStrength = 8f;
  public float nextPageThreshold;
  public SpringPanel.OnFinished onFinished;
  public UICenterOnChild.OnCenterCallback onCenter;
  private UIScrollView mScrollView;
  private GameObject mCenteredObject;

  public GameObject centeredObject
  {
    get
    {
      return this.mCenteredObject;
    }
  }

  private void Start()
  {
    this.Recenter();
  }

  private void OnEnable()
  {
    if (!(bool) ((Object) this.mScrollView))
      return;
    this.mScrollView.centerOnChild = this;
    this.Recenter();
  }

  private void OnDisable()
  {
    if (!(bool) ((Object) this.mScrollView))
      return;
    this.mScrollView.centerOnChild = (UICenterOnChild) null;
  }

  private void OnDragFinished()
  {
    if (!this.enabled)
      return;
    this.Recenter();
  }

  private void OnValidate()
  {
    this.nextPageThreshold = Mathf.Abs(this.nextPageThreshold);
  }

  [ContextMenu("Execute")]
  public void Recenter()
  {
    if ((Object) this.mScrollView == (Object) null)
    {
      this.mScrollView = NGUITools.FindInParents<UIScrollView>(this.gameObject);
      if ((Object) this.mScrollView == (Object) null)
      {
        Debug.LogWarning((object) (this.GetType().ToString() + " requires " + (object) typeof (UIScrollView) + " on a parent object in order to work"), (Object) this);
        this.enabled = false;
        return;
      }
      if ((bool) ((Object) this.mScrollView))
      {
        this.mScrollView.centerOnChild = this;
        this.mScrollView.onDragFinished = new UIScrollView.OnDragNotification(this.OnDragFinished);
      }
      if ((Object) this.mScrollView.horizontalScrollBar != (Object) null)
        this.mScrollView.horizontalScrollBar.onDragFinished = new UIProgressBar.OnDragFinished(this.OnDragFinished);
      if ((Object) this.mScrollView.verticalScrollBar != (Object) null)
        this.mScrollView.verticalScrollBar.onDragFinished = new UIProgressBar.OnDragFinished(this.OnDragFinished);
    }
    if ((Object) this.mScrollView.panel == (Object) null)
      return;
    Transform transform = this.transform;
    if (transform.childCount == 0)
      return;
    Vector3[] worldCorners = this.mScrollView.panel.worldCorners;
    Vector3 panelCenter = (worldCorners[2] + worldCorners[0]) * 0.5f;
    Vector3 velocity = this.mScrollView.currentMomentum * this.mScrollView.momentumAmount;
    Vector3 vector3_1 = NGUIMath.SpringDampen(ref velocity, 9f, 2f);
    Vector3 vector3_2 = panelCenter - vector3_1 * 0.01f;
    float num1 = float.MaxValue;
    Transform target = (Transform) null;
    int index1 = 0;
    int index2 = 0;
    for (int childCount = transform.childCount; index2 < childCount; ++index2)
    {
      Transform child = transform.GetChild(index2);
      if (child.gameObject.activeInHierarchy)
      {
        float num2 = Vector3.SqrMagnitude(child.position - vector3_2);
        if ((double) num2 < (double) num1)
        {
          num1 = num2;
          target = child;
          index1 = index2;
        }
      }
    }
    if ((double) this.nextPageThreshold > 0.0 && UICamera.currentTouch != null && ((Object) this.mCenteredObject != (Object) null && (Object) this.mCenteredObject.transform == (Object) transform.GetChild(index1)))
    {
      Vector2 vector2 = UICamera.currentTouch.totalDelta;
      float num2;
      switch (this.mScrollView.movement)
      {
        case UIScrollView.Movement.Horizontal:
          num2 = vector2.x;
          break;
        case UIScrollView.Movement.Vertical:
          num2 = vector2.y;
          break;
        default:
          num2 = vector2.magnitude;
          break;
      }
      if ((double) num2 > (double) this.nextPageThreshold)
      {
        if (index1 > 0)
          target = transform.GetChild(index1 - 1);
      }
      else if ((double) num2 < -(double) this.nextPageThreshold && index1 < transform.childCount - 1)
        target = transform.GetChild(index1 + 1);
    }
    this.CenterOn(target, panelCenter);
  }

  private void CenterOn(Transform target, Vector3 panelCenter)
  {
    if ((Object) target != (Object) null && (Object) this.mScrollView != (Object) null && (Object) this.mScrollView.panel != (Object) null)
    {
      Transform cachedTransform = this.mScrollView.panel.cachedTransform;
      this.mCenteredObject = target.gameObject;
      Vector3 vector3 = cachedTransform.InverseTransformPoint(target.position) - cachedTransform.InverseTransformPoint(panelCenter);
      if (!this.mScrollView.canMoveHorizontally)
        vector3.x = 0.0f;
      if (!this.mScrollView.canMoveVertically)
        vector3.y = 0.0f;
      vector3.z = 0.0f;
      SpringPanel.Begin(this.mScrollView.panel.cachedGameObject, cachedTransform.localPosition - vector3, this.springStrength).onFinished = this.onFinished;
    }
    else
      this.mCenteredObject = (GameObject) null;
    if (this.onCenter == null)
      return;
    this.onCenter(this.mCenteredObject);
  }

  public void CenterOn(Transform target)
  {
    if (!((Object) this.mScrollView != (Object) null) || !((Object) this.mScrollView.panel != (Object) null))
      return;
    Vector3[] worldCorners = this.mScrollView.panel.worldCorners;
    Vector3 panelCenter = (worldCorners[2] + worldCorners[0]) * 0.5f;
    this.CenterOn(target, panelCenter);
  }

  public delegate void OnCenterCallback(GameObject centeredObject);
}
