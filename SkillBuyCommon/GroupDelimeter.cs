﻿// Decompiled with JetBrains decompiler
// Type: SkillBuyCommon.GroupDelimeter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

namespace SkillBuyCommon
{
  public class GroupDelimeter : GuiPanel
  {
    public GroupDelimeter(string text)
    {
      Color color = SkillBuyPanel.captionColor;
      GuiLabel guiLabel = new GuiLabel(text, color, color, Gui.Options.FontBGM_BT);
      guiLabel.FontSize = 17;
      guiLabel.Align = Align.MiddleLeft;
      guiLabel.PositionX += 30f;
      this.AddChild((GuiElementBase) guiLabel);
      this.Size = new Vector2(373f, 43f);
    }
  }
}
