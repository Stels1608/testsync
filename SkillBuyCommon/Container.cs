﻿// Decompiled with JetBrains decompiler
// Type: SkillBuyCommon.Container
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Core;
using Gui.Tooltips;
using UnityEngine;

namespace SkillBuyCommon
{
  public class Container : GuiPanel, ISelectable
  {
    private Container.tooltipType TooltipType;
    private GuiAtlasImage book;
    private bool Available;
    private bool IsMaxLevel;
    private bool isSelect;
    private int skillLevel;
    public PlayerSkill skill;
    private int skillIndex;
    private Color skillNameTextColor;
    private Color unavailableColor;
    private GuiLabel skillName;
    private GuiLabel costValue;
    private GuiButton buyButton;
    private GuiLabel costText;
    private GuiHealthbar progressBar;
    private System.Action<PlayerSkill> updateDesc;

    public PlayerSkill Skill
    {
      get
      {
        return this.skill;
      }
    }

    private Color SkillNameAndCostValueColor
    {
      get
      {
        return this.skillName.NormalColor;
      }
      set
      {
        GuiLabel guiLabel = this.skillName;
        Color color1 = value;
        this.costValue.OverColor = color1;
        Color color2 = color1;
        this.costValue.NormalColor = color2;
        Color color3 = color2;
        this.skillName.OverColor = color3;
        Color color4 = color3;
        guiLabel.NormalColor = color4;
      }
    }

    public Container(int skillIndex, System.Action<PlayerSkill> updateDesc, AtlasCache atlasCache)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      Container.\u003CContainer\u003Ec__AnonStoreyC4 containerCAnonStoreyC4 = new Container.\u003CContainer\u003Ec__AnonStoreyC4();
      // ISSUE: reference to a compiler-generated field
      containerCAnonStoreyC4.skillIndex = skillIndex;
      // ISSUE: reference to a compiler-generated field
      containerCAnonStoreyC4.updateDesc = updateDesc;
      this.TooltipType = Container.tooltipType.none;
      this.skillNameTextColor = SkillBuyPanel.captionColor;
      this.unavailableColor = ColorUtility.MakeColorFrom0To255(221U, 47U, 82U);
      this.progressBar = new GuiHealthbar();
      // ISSUE: explicit constructor call
      base.\u002Ector();
      // ISSUE: reference to a compiler-generated field
      containerCAnonStoreyC4.\u003C\u003Ef__this = this;
      // ISSUE: reference to a compiler-generated field
      this.skill = Game.Me.SkillBook[containerCAnonStoreyC4.skillIndex];
      // ISSUE: reference to a compiler-generated field
      this.skillIndex = containerCAnonStoreyC4.skillIndex;
      // ISSUE: reference to a compiler-generated field
      this.updateDesc = containerCAnonStoreyC4.updateDesc;
      this.Available = this.skill.IsAvailable;
      this.IsMaxLevel = this.skill.IsMaxLevel;
      this.book = new GuiAtlasImage(new Vector2(47f, 40f));
      this.book.Align = Align.MiddleLeft;
      this.book.PositionX += 10f;
      if ((int) this.skill.Card.RequireSkillHash != 0)
        this.book.PositionX += 20f;
      this.AddChild((GuiElementBase) this.book);
      this.skillLevel = this.skill.Level;
      this.skillName = new GuiLabel(this.skill.Name + "-" + (object) this.skillLevel, Gui.Options.FontBGM_BT, 17);
      this.skillName.Align = Align.MiddleLeft;
      this.skillName.Position = new Vector2((float) ((double) this.book.SizeX + (double) this.book.PositionX + 10.0), -10f);
      this.AddChild((GuiElementBase) this.skillName);
      this.costText = new GuiLabel("%$bgo.CharacterStatusWindow.skills.layout.costLabel%", Gui.Options.FontBGM_BT, 12);
      this.costText.Align = Align.MiddleLeft;
      this.costText.Position = new Vector2((float) ((double) this.book.SizeX + (double) this.book.PositionX + 10.0), 5f);
      this.AddChild((GuiElementBase) this.costText);
      this.costValue = new GuiLabel(Gui.Options.FontBGM_BT, 12);
      this.costValue.Align = Align.MiddleLeft;
      this.costValue.Position = this.costText.Position + new Vector2(this.costText.SizeX + 5f, 0.0f);
      this.AddChild((GuiElementBase) this.costValue);
      this.buyButton = new GuiButton("%$bgo.etc.skills_learn%");
      this.buyButton.Align = Align.DownRight;
      this.buyButton.SizeX -= 20f;
      GuiButton guiButton = this.buyButton;
      Vector2 vector2 = guiButton.Position - new Vector2(5f, 5f);
      guiButton.Position = vector2;
      // ISSUE: reference to a compiler-generated method
      this.buyButton.Pressed = new AnonymousDelegate(containerCAnonStoreyC4.\u003C\u003Em__1B8);
      this.buyButton.SetTooltip("%$bgo.etc.skills_use_xp%");
      this.AddChild((GuiElementBase) this.buyButton);
      this.Size = new Vector2(375f, 43f);
      this.progressBar.Size = new Vector2(305f, 5f);
      this.AddChild((GuiElementBase) this.progressBar, new Vector2(54f, 35f));
      this.PeriodicUpdate();
      this.MakeViewActual();
    }

    public override void PeriodicUpdate()
    {
      base.PeriodicUpdate();
      bool flag = Game.Me.SkillBook.CurrentTrainingSkill != null && (int) Game.Me.SkillBook.CurrentTrainingSkill.ServerID == (int) this.skill.ServerID;
      this.progressBar.IsRendered = flag;
      if (flag)
        this.progressBar.Progress = (float) (1.0 - ((double) Game.Me.SkillBook.TrainingEndTime - (double) Time.time) / (double) Game.Me.SkillBook.CurrentTrainingSkill.Card.TrainingTime);
      this.skill = Game.Me.SkillBook[this.skillIndex];
      if (this.Available == this.skill.IsAvailable && this.IsMaxLevel == this.skill.IsMaxLevel)
        return;
      this.Available = this.skill.IsAvailable;
      this.IsMaxLevel = this.skill.IsMaxLevel;
      this.MakeViewActual();
      Container.tooltipType tooltipType = this.WhyCantTrainingSkill();
      if (this.TooltipType == tooltipType)
        return;
      this.TooltipType = tooltipType;
      this.UpdateTooltip();
    }

    private Container.tooltipType WhyCantTrainingSkill()
    {
      if (this.Available)
        return Container.tooltipType.none;
      Me me = Game.Me;
      if (me.SkillBook.CurrentTrainingSkill == this.skill)
        return Container.tooltipType.thisSkillTraining;
      if (this.IsMaxLevel)
        return Container.tooltipType.levelUpperLimit;
      if (me.SkillBook.CurrentTrainingSkill != null)
        return Container.tooltipType.anotherTrainig;
      if ((long) me.FreeExperience < (long) this.skill.Cost)
        return Container.tooltipType.enoughtXP;
      return (int) this.skill.Card.RequireSkillHash != 0 ? Container.tooltipType.lowPrimarySkillLevel : Container.tooltipType.none;
    }

    private void UpdateTooltip()
    {
      if (this.Available)
      {
        this.ToolTip = (GuiAdvancedTooltipBase) null;
      }
      else
      {
        string text = string.Empty;
        switch (this.TooltipType)
        {
          case Container.tooltipType.anotherTrainig:
            text = "%$bgo.etc.skills_can_not_learn%" + Game.Me.SkillBook.CurrentTrainingSkill.Name + "-" + (object) Game.Me.SkillBook.CurrentTrainingSkill.Level;
            break;
          case Container.tooltipType.thisSkillTraining:
            text = "%$bgo.etc.skills_can_not_learn3%";
            break;
          case Container.tooltipType.enoughtXP:
            text = BsgoLocalization.Get("%$bgo.etc.skills_can_not_learn_adv%", (object) this.skill.Cost, (object) Game.Me.FreeExperience);
            break;
          case Container.tooltipType.lowPrimarySkillLevel:
            text = "%$bgo.etc.skills_can_not_learn2%";
            break;
          case Container.tooltipType.levelUpperLimit:
            text = "%$bgo.etc.skills_can_not_learn4%";
            break;
        }
        this.SetTooltip(text);
      }
    }

    private void MakeViewActual()
    {
      this.skillName.Text = this.skill.Name + "-" + (object) this.skill.Level;
      this.costValue.Text = !this.IsMaxLevel ? this.skill.Cost.ToString() : "0";
      if (this.skill.Card != null && this.skill.Card.GUICard != null)
        this.book.GuiCard = this.skill.Card.GUICard;
      if (this.IsMaxLevel && Game.Me.SkillBook.CurrentTrainingSkill != this.skill)
      {
        this.SkillNameAndCostValueColor = Gui.Options.PositiveColor;
        GuiLabel guiLabel = this.costValue;
        bool flag = false;
        this.costText.IsRendered = flag;
        int num = flag ? 1 : 0;
        guiLabel.IsRendered = num != 0;
      }
      else
      {
        GuiLabel guiLabel = this.costValue;
        bool flag = true;
        this.costText.IsRendered = flag;
        int num = flag ? 1 : 0;
        guiLabel.IsRendered = num != 0;
        this.SkillNameAndCostValueColor = !this.Available ? this.unavailableColor : this.skillNameTextColor;
      }
      this.buyButton.IsRendered = this.isSelect && this.Available;
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      this.FindParent<ISelectableContainer>().Selected = (ISelectable) this;
      return base.MouseUp(position, key);
    }

    public void OnSelected()
    {
      this.isSelect = true;
      if (this.Available && Game.Me.SkillBook.CurrentTrainingSkill == null && !this.IsMaxLevel)
        this.buyButton.IsRendered = true;
      if (this.updateDesc == null)
        return;
      this.updateDesc(this.skill);
    }

    public void OnDeselected()
    {
      this.isSelect = false;
      this.buyButton.IsRendered = false;
    }

    private enum tooltipType
    {
      anotherTrainig,
      thisSkillTraining,
      enoughtXP,
      lowPrimarySkillLevel,
      levelUpperLimit,
      none,
    }
  }
}
