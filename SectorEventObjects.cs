﻿// Decompiled with JetBrains decompiler
// Type: SectorEventObjects
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Loaders;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SectorEventObjects : MonoBehaviour
{
  public List<string> eventActivation = new List<string>();
  public string eventId;
  public int level;
  public string faction;
  public int interval;
  public int variance;
  public float chance;
  public bool showSpawnPoints;

  public void Init(string name)
  {
    this.eventId = name;
    this.gameObject.name = name;
    this.chance = 1f;
    this.faction = "neutral";
    this.interval = 30;
    this.level = 6;
    this.variance = 120;
    this.eventActivation = new List<string>();
  }

  public void Init(JSectorEvents events)
  {
    this.eventId = events.eventId;
    this.chance = events.chance;
    this.faction = events.faction;
    this.interval = events.interval;
    this.level = events.level;
    this.gameObject.transform.position = events.pos.position;
    this.gameObject.name = events.eventId;
    this.variance = events.variance;
    this.eventActivation = events.eventActivation;
  }

  private void Update()
  {
    if (!this.showSpawnPoints)
      return;
    ContentDB.GetFiles("*");
    JSectorEvent document = ContentDB.GetDocument<JSectorEvent>(this.eventId);
    this.gameObject.transform.localScale = Vector3.one;
    foreach (Component component in this.gameObject.transform)
      UnityEngine.Object.DestroyImmediate((UnityEngine.Object) component.gameObject);
    this.AddPoints(document, this.gameObject);
    Debug.Log((object) "SHOW SPAWN POINTS");
    this.showSpawnPoints = false;
  }

  private void AddSphere(GameObject root)
  {
    GameObject primitive = GameObject.CreatePrimitive(PrimitiveType.Sphere);
    primitive.transform.parent = root.transform;
    primitive.transform.localPosition = Vector3.zero;
    primitive.name = "radius";
    primitive.GetComponent<Renderer>().material.color = new Color(0.5f, 0.5f, 0.1f, 0.5f);
    primitive.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
    primitive.transform.localScale = new Vector3(2000f, 2000f, 2000f);
    UnityEngine.Object.Destroy((UnityEngine.Object) primitive.GetComponent<Collider>());
  }

  public void AddPoints(JSectorEvent data, GameObject root)
  {
    this.AddSphere(root);
    if (data.points != null)
      this.AddPoints("Points", data.points, Color.white, root);
    if (data.ownerSpawnPoints != null)
      this.AddPoints("Owner Spawn Points", data.ownerSpawnPoints, Color.green, root);
    if (data.rivalSpawnPoints == null)
      return;
    this.AddPoints("Rival Spawn Points", data.rivalSpawnPoints, Color.red, root);
  }

  public void AddPoints(string pointsName, Dictionary<string, JTransform> points, Color c, GameObject root)
  {
    GameObject gameObject1 = GameObject.Find(pointsName);
    if ((UnityEngine.Object) gameObject1 == (UnityEngine.Object) null)
      gameObject1 = new GameObject(pointsName);
    using (List<GameObject>.Enumerator enumerator = gameObject1.transform.Cast<Transform>().Select<Transform, GameObject>((Func<Transform, GameObject>) (child => child.gameObject)).ToList<GameObject>().GetEnumerator())
    {
      while (enumerator.MoveNext())
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) enumerator.Current);
    }
    gameObject1.transform.parent = root.transform;
    gameObject1.transform.localPosition = Vector3.zero;
    gameObject1.transform.localRotation = new Quaternion();
    using (Dictionary<string, JTransform>.Enumerator enumerator = points.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, JTransform> current = enumerator.Current;
        GameObject primitive = GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(primitive.transform.GetChild(0).gameObject);
        UnityEngine.Object.DestroyImmediate((UnityEngine.Object) primitive);
        GameObject gameObject3 = gameObject2;
        gameObject3.name = current.Key;
        gameObject3.transform.parent = gameObject1.transform;
        gameObject3.transform.localPosition = current.Value.position;
        gameObject3.transform.localRotation = current.Value.rotation;
        gameObject3.transform.localScale = Vector3.one * 100f;
        gameObject3.GetComponentInChildren<Renderer>().material = new Material(Shader.Find("Diffuse"))
        {
          color = c
        };
        gameObject3.SetActive(true);
      }
    }
  }
}
