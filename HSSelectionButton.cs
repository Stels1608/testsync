﻿// Decompiled with JetBrains decompiler
// Type: HSSelectionButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class HSSelectionButton : GuiButton
{
  public bool selected;

  public HSSelectionButton(string text)
    : base(text)
  {
    this.selected = false;
  }

  public void Pressing()
  {
    this.selected = true;
    if (this.Pressed == null)
      return;
    this.Pressed();
  }

  public override bool MouseDown(float2 position, KeyCode key)
  {
    this.Pressing();
    return base.MouseDown(position, key);
  }
}
