﻿// Decompiled with JetBrains decompiler
// Type: ArrayIdDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class ArrayIdDesc : IProtocolWrite, IProtocolRead
{
  public List<uint> data = new List<uint>();
  private ushort messageType;

  public ArrayIdDesc()
  {
  }

  public ArrayIdDesc(ushort messageType)
  {
    this.messageType = messageType;
  }

  public bool HaveData()
  {
    return this.data.Count > 0;
  }

  public void Add(uint id)
  {
    if (this.data.Contains(id))
      return;
    this.data.Add(id);
  }

  public void Write(BgoProtocolWriter pw)
  {
    pw.Write(this.messageType);
    pw.Write((ushort) this.data.Count);
    using (List<uint>.Enumerator enumerator = this.data.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        uint current = enumerator.Current;
        pw.Write(current);
      }
    }
    this.data.Clear();
  }

  public void Read(BgoProtocolReader pr)
  {
    int num = (int) pr.ReadUInt16();
    for (int index = 0; index < num; ++index)
      this.data.Add(pr.ReadUInt32());
  }
}
