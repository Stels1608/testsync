﻿// Decompiled with JetBrains decompiler
// Type: OptionsControlsBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class OptionsControlsBase : OptionsContent, ILocalizeable
{
  protected InputBindingUi inputBindingUi;
  protected InputBindingOptionsBase InputBindingOptions;
  protected InputBindingFooterButtonBar FooterButtonBar;
  protected TwoOneButtonLayout Layout;
  [SerializeField]
  protected UILabel tabBtnActivLabel;
  [SerializeField]
  protected UILabel tabBtnNormalLabel;

  public void Awake()
  {
    GameObject child = new GameObject("layout");
    child.layer = this.gameObject.layer;
    this.Layout = child.AddComponent<TwoOneButtonLayout>();
    PositionUtils.CorrectTransform(this.gameObject, child);
    this.InitContent();
  }

  public void Start()
  {
  }

  protected abstract void InitContent();

  protected void InitButtonBar()
  {
    GameObject gameObject = NGUITools.AddChild(this.Layout.ContentArea[2], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingFooterButtonBar"));
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      throw new ArgumentNullException("initInputBindingFooterButtonBar");
    this.FooterButtonBar = gameObject.GetComponent<InputBindingFooterButtonBar>();
    this.FooterButtonBar.SetButtonDelegates(new AnonymousDelegate(((OptionsContent) this).Undo), new AnonymousDelegate(((OptionsContent) this).Apply), new AnonymousDelegate(this.ResetDialog));
  }

  internal void UpdateBindingButtons(List<IInputBinding> list)
  {
    if (!((UnityEngine.Object) this.inputBindingUi != (UnityEngine.Object) null))
      return;
    this.inputBindingUi.UpdateBindingButtons(list);
  }

  public override void UpdateSettings(UserSettings _settings)
  {
    base.UpdateSettings(_settings);
    this.InputBindingOptions.UpdateSettings(this.settings);
  }

  public override void RestoreDefaultSettings()
  {
    UserSettings defaultSettings = UserSettings.DefaultSettings;
    using (List<UserSetting>.Enumerator enumerator = this.InputBindingOptions.CoveredSettings().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        this.settings.Set(current, defaultSettings.Get(current));
      }
    }
  }

  public override void Apply()
  {
    base.Apply();
    FacadeFactory.GetInstance().SendMessage(Message.SaveInputBindings);
  }

  public override void Undo()
  {
    base.Undo();
    FacadeFactory.GetInstance().SendMessage(Message.RevertInputBindings);
  }

  private void ResetDialog()
  {
    DialogBoxFactoryNgui.CreateResetSettingsDialogBox(new AnonymousDelegate(this.Reset));
  }

  private void Reset()
  {
    this.RestoreDefaultSettings();
    this.UpdateSettings(this.settings);
    FacadeFactory.GetInstance().SendMessage(Message.ResetInputBindings);
    this.Apply();
  }

  public void InjectSettingDelegates(OnSettingsApply apply, OnSettingsUndo undoSettings)
  {
    this.ApplySettings = apply;
    this.RevertSettings = undoSettings;
  }

  protected abstract string GetTabText();

  public void ReloadLanguageData()
  {
    string tabText = this.GetTabText();
    this.tabBtnActivLabel.text = tabText;
    this.tabBtnNormalLabel.text = tabText;
  }
}
