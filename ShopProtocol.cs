﻿// Decompiled with JetBrains decompiler
// Type: ShopProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class ShopProtocol : BgoProtocol
{
  private Shop shop;

  public ShopProtocol()
    : base(BgoProtocol.ProtocolID.Shop)
  {
  }

  public void RequestItems()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    this.SendMessage(bw);
  }

  public void RequestAllSales()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 17);
    this.SendMessage(bw);
  }

  public void RequestEventShopItems()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 15);
    this.SendMessage(bw);
  }

  public void RequestBoughtShipSaleOffer(uint rewardCardGuid)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 13);
    bw.Write(rewardCardGuid);
    this.SendMessage(bw);
  }

  public void Close()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 11);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    ShopProtocol.Reply reply1 = (ShopProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply1);
    ShopProtocol.Reply reply2 = reply1;
    switch (reply2)
    {
      case ShopProtocol.Reply.BoughtShipSaleOffer:
        uint guid = br.ReadGUID();
        bool flag1 = br.ReadBoolean();
        GuiShipSaleDialog guiShipSaleDialog = Game.GUIManager.Find<GuiShipSaleDialog>();
        if (guiShipSaleDialog == null || !flag1)
          break;
        guiShipSaleDialog.DisableOffer(guid);
        break;
      case ShopProtocol.Reply.EventShopItems:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        ShopProtocol.\u003CParseMessage\u003Ec__AnonStoreyFD messageCAnonStoreyFd = new ShopProtocol.\u003CParseMessage\u003Ec__AnonStoreyFD();
        ItemList itemList = ItemFactory.ReadItemList(br);
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyFd.eventShop = this.FetchEventShop();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyFd.eventShop._Clear();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyFd.eventShop._AddItems((SmartList<ShipItem>) itemList);
        Flag flag2 = new Flag();
        foreach (ShipItem shipItem in (IEnumerable<ShipItem>) itemList)
          flag2.Depend(new ILoadable[1]
          {
            (ILoadable) shipItem
          });
        // ISSUE: reference to a compiler-generated method
        flag2.AddHandler(new SignalHandler(messageCAnonStoreyFd.\u003C\u003Em__24F));
        flag2.Set();
        break;
      case ShopProtocol.Reply.EventShopAvailable:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        ShopProtocol.\u003CParseMessage\u003Ec__AnonStoreyFE messageCAnonStoreyFe = new ShopProtocol.\u003CParseMessage\u003Ec__AnonStoreyFE();
        uint cardGUID = br.ReadGUID();
        bool flag3 = br.ReadBoolean();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyFe.eventShopCard = !flag3 ? (EventShopCard) null : (EventShopCard) Game.Catalogue.FetchCard(cardGUID, CardView.EventShop);
        // ISSUE: reference to a compiler-generated field
        Game.EventShopCard = messageCAnonStoreyFe.eventShopCard;
        // ISSUE: reference to a compiler-generated field
        if (messageCAnonStoreyFe.eventShopCard == null)
        {
          FacadeFactory.GetInstance().SendMessage(Message.EventShopAvailability, (object) null);
          break;
        }
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyFe.eventShopCard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyFe.\u003C\u003Em__250));
        break;
      default:
        switch (reply2)
        {
          case ShopProtocol.Reply.Items:
            this.FetchShop()._AddItems((SmartList<ShipItem>) ItemFactory.ReadItemList(br));
            return;
          case ShopProtocol.Reply.Sales:
            this.FetchShop().DecodeItemOfferSchedule(br);
            return;
          case ShopProtocol.Reply.UpgradeSales:
            this.FetchShop().DecodeUpgradeOfferSchedule(br);
            FacadeFactory.GetInstance().SendMessage(Message.ShipListChanged);
            return;
          default:
            DebugUtility.LogError("Unknown server reply in Shop protocol: " + (object) reply1);
            return;
        }
    }
  }

  private Shop FetchShop()
  {
    return this.shop ?? (this.shop = ((ShopDataProvider) FacadeFactory.GetInstance().FetchDataProvider("ShopDataProvider")).Shop);
  }

  private EventShop FetchEventShop()
  {
    return ((ShopDataProvider) FacadeFactory.GetInstance().FetchDataProvider("ShopDataProvider")).EventShop;
  }

  public static ShopProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Shop) as ShopProtocol;
  }

  public enum Request : ushort
  {
    Items = 1,
    Close = 11,
    BoughtShipSaleOffer = 13,
    EventShopItems = 15,
    AllSales = 17,
  }

  public enum Reply : ushort
  {
    Items = 2,
    Sales = 3,
    UpgradeSales = 4,
    ShopPrices = 8,
    BoughtShipSaleOffer = 12,
    EventShopItems = 14,
    EventShopAvailable = 16,
  }
}
