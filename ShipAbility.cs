﻿// Decompiled with JetBrains decompiler
// Type: ShipAbility
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ShipAbility : ShipAbstractAbility
{
  public AnonymousDelegate OnTouch = (AnonymousDelegate) (() => {});
  public uint consumableGuid;
  public ShipSlot slot;
  public GUICard guiCard;
  private ShipConsumableCard consumableCard;
  private ShipAbility.ToggleStatus toggleStatus;
  private static ShipAbility highlightedAbility;

  public bool ToggleAbilityActivated { get; set; }

  public bool ShortCircuited { get; set; }

  public AbilityValidation validation
  {
    get
    {
      return this.slot.Validation;
    }
  }

  public bool On
  {
    get
    {
      return this.validation.On;
    }
    set
    {
      if (this.card.Affect == ShipAbilityAffect.MultiWeaponTarget)
      {
        if (value && this.CheckAbility())
        {
          SpaceObject target = this.validation.Target ?? this.GetTarget();
          if (this.CheckTarget(target))
          {
            this.validation.Target = target;
            this.validation.On = true;
          }
          else
          {
            this.validation.On = false;
            this.validation.Target = (SpaceObject) null;
          }
        }
        else
        {
          this.validation.On = false;
          this.validation.Target = (SpaceObject) null;
        }
      }
      else
        this.validation.On = value && this.CheckAbility();
      this.OnCheckForAutoAbility();
      FacadeFactory.GetInstance().SendMessage(Message.AutoAbilityToggled, (object) this);
    }
  }

  public ShipConsumableCard ConsumableCard
  {
    get
    {
      return this.consumableCard;
    }
  }

  public float Angle
  {
    get
    {
      return this.slot.GameStats.Angle;
    }
  }

  public SpaceObject Target
  {
    get
    {
      return this.validation.Target;
    }
  }

  public bool IsMultiTargetAbility
  {
    get
    {
      return this.card.Affect == ShipAbilityAffect.MultiWeaponTarget;
    }
  }

  public ushort ServerID
  {
    get
    {
      return this.slot.ServerID;
    }
  }

  public uint ConsumableCount
  {
    get
    {
      return Game.Me.Hold.GetCountByGUID(this.consumableGuid);
    }
  }

  public bool CanCast
  {
    get
    {
      if (!this.IsCooling)
        return this.IsValid;
      return false;
    }
  }

  public bool IsValid
  {
    get
    {
      if (this.CheckAbility() && this.CheckPowerAndConsumable() && this.CheckValidationTarget())
        return this.CheckValidationTargetRange();
      return false;
    }
  }

  public bool IsCooling
  {
    get
    {
      return (double) this.GetCoolingProgress() > 0.0;
    }
  }

  public bool IsBroken
  {
    get
    {
      if (!this.slot.System.IsBroken)
        return this.slot.Inoperable;
      return true;
    }
  }

  public bool IsPowerPointsEnough
  {
    get
    {
      return (double) this.slot.GameStats.PowerPointCost <= (double) Game.Me.Stats.PowerPoints;
    }
  }

  public void InjectSlot(ShipSlot slot)
  {
    this.slot = slot;
    this.consumableGuid = slot.ConsumableGUID;
    this.card = slot.System.Card.AbilityCards[0];
    this.guiCard = (GUICard) Game.Catalogue.FetchCard(this.card.CardGUID, CardView.GUI);
    this.consumableCard = (ShipConsumableCard) Game.Catalogue.FetchCard(this.consumableGuid, CardView.ShipConsumable);
  }

  private void OnCheckForAutoAbility()
  {
    if (this.card.Launch != ShipAbilityLaunch.Auto)
      return;
    if (this.validation.On && this.CheckValidationTarget())
      this.Cast();
    else
      this.TurnOffAutoCast();
  }

  public float GetAbilityPPcost()
  {
    return this.slot.GameStats.PowerPointCost;
  }

  public float GetPPCostPerSecond()
  {
    return this.slot.GameStats.PowerPointCostPerSecond;
  }

  public float GetAbilityMinRange()
  {
    return this.slot.GameStats.MinRange;
  }

  public float GetAbilityMaxRange()
  {
    return this.slot.GameStats.MaxRange;
  }

  public void SetValidationTarget(SpaceObject target)
  {
    if (this.card.Affect != ShipAbilityAffect.Selected)
      return;
    this.validation.Target = !this.CheckTarget(target) ? (SpaceObject) null : target;
    if (this.card.Launch != ShipAbilityLaunch.Auto)
      return;
    if (this.validation.On && this.validation.Target != null)
      this.Cast();
    else
      this.TurnOffAutoCast();
  }

  public void ClearTarget()
  {
    this.validation.Target = (SpaceObject) null;
    this.On = false;
  }

  public void Touch()
  {
    this.OnTouch();
    switch (this.card.Launch)
    {
      case ShipAbilityLaunch.Auto:
        this.On = !this.On;
        break;
      case ShipAbilityLaunch.Manual:
        SpaceLevel level = SpaceLevel.GetLevel();
        if ((UnityEngine.Object) level != (UnityEngine.Object) null && !this.IsPowerPointsEnough)
          level.sectorSfx.PlayNotEnoughPowerSound();
        if (!this.CanCast)
          break;
        this.Cast();
        break;
    }
  }

  public void OnCasted(ShipAbility.CastReply reply)
  {
    if (reply != ShipAbility.CastReply.False)
      this.validation.CastedTime = Time.time;
    this.RefreshAutoAbility();
  }

  private void RefreshAutoAbility()
  {
    if (!this.card.Auto || !this.validation.On || this.toggleStatus != ShipAbility.ToggleStatus.On)
      return;
    switch (this.card.Affect)
    {
      case ShipAbilityAffect.Selected:
        if (this.validation.Target != null && !this.validation.Target.Dead)
          break;
        this.TurnOffAutoCast();
        break;
      case ShipAbilityAffect.Area:
        GameProtocol.GetProtocol().UpdateAbilityTargets(this.ServerID, this.GetObjectsWithinAOE().ToArray());
        break;
    }
  }

  private void TurnOffAutoCast()
  {
    GameProtocol.GetProtocol().ToggleAbilityOff(this.ServerID);
    this.toggleStatus = ShipAbility.ToggleStatus.Off;
  }

  public void Cast()
  {
    switch (this.card.Affect)
    {
      case ShipAbilityAffect.Selected:
        if (!this.CheckValidationTarget())
          break;
        this.DoCast(new uint[1]
        {
          this.validation.Target.ObjectID
        }, this.card.Launch);
        break;
      case ShipAbilityAffect.Ignore:
        this.DoCast(new uint[0], this.card.Launch);
        break;
      case ShipAbilityAffect.Area:
        this.DoCast(this.GetObjectsWithinAOE().ToArray(), this.card.Launch);
        break;
      case ShipAbilityAffect.MultiWeaponTarget:
        if (!this.CheckTarget(this.validation.Target))
          break;
        this.DoCast(new uint[1]
        {
          this.validation.Target.ObjectID
        }, this.card.Launch);
        break;
    }
  }

  protected void DoCast(uint[] idList, ShipAbilityLaunch castType)
  {
    if (castType == ShipAbilityLaunch.Auto)
    {
      if (this.toggleStatus == ShipAbility.ToggleStatus.Off)
      {
        GameProtocol.GetProtocol().ToggleAbilityOn(this.ServerID, idList);
        this.toggleStatus = ShipAbility.ToggleStatus.On;
      }
      else
        GameProtocol.GetProtocol().UpdateAbilityTargets(this.ServerID, idList);
    }
    else
      GameProtocol.GetProtocol().CastSlotAbility(this.ServerID, idList);
  }

  private bool CheckAbility()
  {
    if (Game.Me.Caps.IsRestricted(Capability.Cast) || !this.slot.GameStats.Available || (this.IsBroken || (UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null))
      return false;
    Ship ship = (Ship) SpaceLevel.GetLevel().GetPlayerShip();
    if (ship == null || !(bool) ship.IsLoaded || (ship.Dead || !(bool) this.card.IsLoaded))
      return false;
    if (this.card.ActionType == AbilityActionType.ToggleStealth)
    {
      if (Game.Me.ActualShip.MovementController.Gear == Gear.Boost)
        return false;
      SectorDesc star = Game.Galaxy.GetStar(SpaceLevel.GetLevel().ServerID);
      if (star == null)
        return false;
      switch (Game.Me.Faction)
      {
        case Faction.Colonial:
          return star.ColonialThreatLevel >= 12;
        case Faction.Cylon:
          return star.CylonThreatLevel >= 12;
      }
    }
    return this.card.ActionType != AbilityActionType.ActivateJumpTargetTransponder || (int) Game.Me.Party.PartyId != 0;
  }

  private bool CheckPowerAndConsumable()
  {
    return (this.card.ConsumableOption != ShipConsumableOption.Using || (int) this.ConsumableCount != 0) && this.IsPowerPointsEnough;
  }

  private bool CheckValidationTarget()
  {
    switch (this.card.Affect)
    {
      case ShipAbilityAffect.Selected:
        SpaceObject target = this.validation.Target ?? this.GetTarget();
        if (!this.CheckTarget(target))
          return false;
        this.validation.Target = target;
        return true;
      case ShipAbilityAffect.Ignore:
      case ShipAbilityAffect.Area:
        return true;
      case ShipAbilityAffect.MultiWeaponTarget:
        return this.CheckTarget(this.validation.Target ?? this.GetTarget());
      default:
        return false;
    }
  }

  private bool CheckValidationTargetRange()
  {
    switch (this.card.Affect)
    {
      case ShipAbilityAffect.Selected:
      case ShipAbilityAffect.MultiWeaponTarget:
        SpaceObject playerTarget = this.validation.Target ?? this.GetTarget();
        if (playerTarget == null || playerTarget.Dead)
          return false;
        return this.TargetPositionCheck((Ship) SpaceLevel.GetLevel().GetPlayerShip(), playerTarget);
      case ShipAbilityAffect.Ignore:
      case ShipAbilityAffect.Area:
        return true;
      default:
        return false;
    }
  }

  protected bool TargetPositionCheck(Ship playerShip, SpaceObject playerTarget)
  {
    if ((double) Math.Abs(this.slot.GameStats.MaxRange) < 0.00999999977648258 || playerShip == playerTarget)
      return true;
    Spot objectPoint = playerShip.GetObjectPoint(this.slot.Card.ObjectPointServerHash);
    Vector3 from;
    Vector3 to;
    if (objectPoint != null)
    {
      from = objectPoint.Rotation * Vector3.forward;
      to = playerTarget.Position - objectPoint.Position;
    }
    else
    {
      from = playerShip.Forward;
      to = playerTarget.Position - playerShip.Position;
    }
    float minRange = this.slot.GameStats.MinRange;
    float maxRange = this.slot.GameStats.MaxRange;
    bool flag1 = (double) to.sqrMagnitude <= (double) maxRange * (double) maxRange && (double) to.sqrMagnitude >= (double) minRange * (double) minRange;
    bool flag2 = (double) this.Angle == 0.0 || (double) Vector3.Angle(from, to) <= (double) this.Angle;
    if (flag1)
      return flag2;
    return false;
  }

  public float GetAbilityCooldown()
  {
    return this.slot.GameStats.Cooldown;
  }

  public float GetCooldown()
  {
    if (!this.card.Auto)
    {
      if (this.slot.System.TimeOfLastUse <= 0.0)
        return 0.0f;
      return (float) (this.slot.System.TimeOfLastUse - Game.TimeSync.ServerTime) + this.slot.GameStats.Cooldown;
    }
    if ((double) this.validation.CastedTime <= 0.0)
      return 0.0f;
    return this.validation.CastedTime - Time.time + this.slot.GameStats.Cooldown;
  }

  public float GetCoolingProgress()
  {
    if (!this.card.Auto)
    {
      if (this.slot.System.TimeOfLastUse <= 0.0)
        return 0.0f;
      return 1f - (float) (Game.TimeSync.ServerTime - this.slot.System.TimeOfLastUse) / this.slot.GameStats.Cooldown;
    }
    if ((double) this.validation.CastedTime <= 0.0)
      return 0.0f;
    return (float) (1.0 - ((double) Time.time - (double) this.validation.CastedTime) / (double) this.slot.GameStats.Cooldown);
  }

  private List<uint> GetObjectsWithinAOE()
  {
    List<uint> uintList = new List<uint>();
    SpaceLevel level = SpaceLevel.GetLevel();
    if ((UnityEngine.Object) level != (UnityEngine.Object) null)
    {
      Ship playerShip = (Ship) level.GetPlayerShip();
      foreach (SpaceObject spaceObject in level.GetObjectRegistry().GetAllLoaded())
      {
        if (this.CheckTarget(spaceObject) && this.TargetPositionCheck(playerShip, spaceObject))
          uintList.Add(spaceObject.ObjectID);
      }
    }
    return uintList;
  }

  public void DrawFiringArc(bool drawing)
  {
    if (Game.Me.Ship == null)
      return;
    FiringArc modelScript = Game.Me.Ship.GetModelScript<FiringArc>();
    if (!((UnityEngine.Object) modelScript != (UnityEngine.Object) null))
      return;
    if (drawing && this != ShipAbility.highlightedAbility)
    {
      ShipAbility.highlightedAbility = this;
      modelScript.ChangeFiringArc(ShipAbility.highlightedAbility);
    }
    else
    {
      if (drawing || this != ShipAbility.highlightedAbility)
        return;
      ShipAbility.highlightedAbility = (ShipAbility) null;
      modelScript.ChangeFiringArc(ShipAbility.highlightedAbility);
    }
  }

  public enum CastReply
  {
    False,
    Ok,
  }

  public enum ToggleStatus
  {
    On,
    Off,
  }
}
