﻿// Decompiled with JetBrains decompiler
// Type: WorldCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class WorldCard : Card
{
  public sbyte FrameIndex = -1;
  public bool Targetable = true;
  public bool ShowBracketWhenInRange = true;
  public string PrefabName;
  public int LODCount;
  public float Radius;
  public SpotDesc[] Spots;
  public string SystemMapTexture;
  public sbyte SecondaryFrameIndex;
  public bool ForceShowOnMap;

  public WorldCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.PrefabName = r.ReadString();
    this.LODCount = (int) r.ReadByte();
    this.Radius = r.ReadSingle();
    this.Spots = r.ReadDescArray<SpotDesc>();
    this.SystemMapTexture = r.ReadString();
    this.FrameIndex = r.ReadSByte();
    this.SecondaryFrameIndex = r.ReadSByte();
    this.Targetable = r.ReadBoolean();
    this.ShowBracketWhenInRange = r.ReadBoolean();
    this.ForceShowOnMap = r.ReadBoolean();
  }

  public override string ToString()
  {
    return "[WorldCard] PrefabName: " + this.PrefabName + " Radius: " + (object) this.Radius + " Targetable: " + (object) this.Targetable + " ShowBracketWhenInRange: " + (object) this.ShowBracketWhenInRange + " ForceShowOnMap: " + (object) this.ForceShowOnMap + " LODCount: " + (object) this.LODCount;
  }
}
