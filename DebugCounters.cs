﻿// Decompiled with JetBrains decompiler
// Type: DebugCounters
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class DebugCounters : DebugBehaviour<DebugCounters>
{
  private Vector2 scroll = new Vector2();
  private int days = 1;
  private string bonusKey = "bonus_death_deal1";
  private string buffer = string.Empty;

  private void Start()
  {
    this.windowID = 8;
    this.SetSize(800f, 400f);
  }

  protected override void WindowFunc()
  {
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    List<DebugCounters.DebugCounterData> debugCounterDataList = new List<DebugCounters.DebugCounterData>(Game.Me.Counters.Count);
    using (Dictionary<uint, int>.Enumerator enumerator = Game.Me.Counters.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<uint, int> current = enumerator.Current;
        CounterCard counterCard = (CounterCard) Game.Catalogue.FetchCard(current.Key, CardView.Counter);
        if (counterCard != null)
          debugCounterDataList.Add(new DebugCounters.DebugCounterData()
          {
            name = counterCard.Name,
            value = current.Value
          });
      }
    }
    debugCounterDataList.Sort((Comparison<DebugCounters.DebugCounterData>) ((c1, c2) => c1.name.CompareTo(c2.name)));
    using (List<DebugCounters.DebugCounterData>.Enumerator enumerator = debugCounterDataList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DebugCounters.DebugCounterData current = enumerator.Current;
        GUILayout.BeginHorizontal();
        GUILayout.Label(current.name + ": " + (object) current.value);
        if (GUILayout.Button("1", new GUILayoutOption[1]{ GUILayout.Width(25f) }))
          this.UpdateCounter(current.name, 1);
        if (GUILayout.Button("10", new GUILayoutOption[1]{ GUILayout.Width(25f) }))
          this.UpdateCounter(current.name, 10);
        if (GUILayout.Button("100", new GUILayoutOption[1]{ GUILayout.Width(25f) }))
          this.UpdateCounter(current.name, 100);
        if (GUILayout.Button("-1", new GUILayoutOption[1]{ GUILayout.Width(25f) }))
          this.UpdateCounter(current.name, -1);
        if (GUILayout.Button("x", new GUILayoutOption[1]{ GUILayout.Width(25f) }))
          DebugProtocol.GetProtocol().Command("clear_counter", current.name);
        GUILayout.EndHorizontal();
      }
    }
    GUILayout.Label("Last Payment");
    GUILayout.BeginHorizontal(GUILayout.Width(600f));
    GUILayout.Label("days in the past:");
    this.buffer = GUILayout.TextField(this.days.ToString());
    int.TryParse(this.buffer, out this.days);
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal(GUILayout.Width(600f));
    GUILayout.Label("bonus-key:");
    this.bonusKey = GUILayout.TextField(this.bonusKey.ToString());
    GUILayout.EndHorizontal();
    if (GUILayout.Button("normal payment (only session)", new GUILayoutOption[1]{ GUILayout.Width(250f) }))
      DebugProtocol.GetProtocol().Command("set_last_payment", this.days.ToString());
    if (GUILayout.Button("package payment (persists)", new GUILayoutOption[1]{ GUILayout.Width(250f) }))
      DebugProtocol.GetProtocol().Command("add_offer_package_booking", this.bonusKey.ToString(), this.days.ToString());
    if (GUILayout.Button("reset package payments", new GUILayoutOption[1]{ GUILayout.Width(250f) }))
      DebugProtocol.GetProtocol().Command("reset_offer_package_bookings");
    if (GUILayout.Button("print payment counter remotly", new GUILayoutOption[1]{ GUILayout.Width(250f) }))
      DebugProtocol.GetProtocol().Command("print_payment_counters");
    GUILayout.EndScrollView();
  }

  private void UpdateCounter(string name, int value)
  {
    DebugProtocol.GetProtocol().Command("update_counter", name, value.ToString());
  }

  private struct DebugCounterData
  {
    public string name;
    public int value;
  }
}
