﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorMultiTargetNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class HudIndicatorMultiTargetNgui : HudIndicatorBaseComponentNgui, IReceivesInputBindings, IReceivesSpaceObject
{
  private readonly List<ShipAbility> weaponAbilities = new List<ShipAbility>();
  private UISprite multiTargetIcon;
  private SpaceObject spaceObject;
  private InputBinder inputBinder;
  private UILabel weaponNumberLabel;

  protected UIAtlas Atlas
  {
    get
    {
      return HudIndicatorsAtlasLinker.Instance.Atlas;
    }
  }

  public override bool RequiresAnimationNgui
  {
    get
    {
      return false;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.multiTargetIcon = NGUIToolsExtension.AddSprite(this.gameObject, this.Atlas, "HudIndicator_Multi_Target", true, string.Empty);
    this.weaponNumberLabel = NGUIToolsExtension.AddLabel(this.gameObject, Gui.Options.FontDS_EF_M, 9, UIWidget.Pivot.TopLeft, string.Empty);
    this.weaponNumberLabel.cachedTransform.localPosition = (Vector3) new Vector2(-39f, -31f);
  }

  protected override void ResetComponentStates()
  {
    this.spaceObject = (SpaceObject) null;
    this.weaponAbilities.Clear();
    if (!((UnityEngine.Object) this.weaponNumberLabel != (UnityEngine.Object) null))
      return;
    this.weaponNumberLabel.text = string.Empty;
  }

  private void UpdateHotkeyLabel()
  {
    List<string> stringList = new List<string>();
    using (List<ShipAbility>.Enumerator enumerator = this.weaponAbilities.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipAbility current = enumerator.Current;
        IInputBinding binding = this.inputBinder.TryGetBinding(InputDevice.KeyboardMouse, current.slot.Action);
        if (binding != null && !binding.IsUnbound)
        {
          string triggerAlias = binding.TriggerAlias;
          stringList.Add(triggerAlias);
        }
        else
          stringList.Add("*" + ((int) current.slot.ServerID + 1).ToString());
      }
    }
    stringList.Sort((Comparison<string>) ((p1, p2) =>
    {
      if (p1.StartsWith("*") && !p2.StartsWith("*"))
        return 1;
      if (!p1.StartsWith("*") && p2.StartsWith("*"))
        return -1;
      Regex regex = new Regex("\\*?(\\d+)");
      Match match1 = regex.Match(p1);
      Match match2 = regex.Match(p2);
      if (match1.Success && match2.Success)
        return int.Parse(match1.Groups[1].Value).CompareTo(int.Parse(match2.Groups[1].Value));
      return p1.CompareTo(p2);
    }));
    this.weaponNumberLabel.text = string.Join(", ", stringList.ToArray());
  }

  public void OnSpaceObjectInjection(SpaceObject spaceObject)
  {
    this.spaceObject = spaceObject;
    this.UpdateColorAndAlpha();
  }

  public void OnInputBinderInjection(InputBinder inputBinder)
  {
    this.inputBinder = inputBinder;
    this.UpdateView();
  }

  protected override void SetColoring()
  {
    UISprite uiSprite = this.multiTargetIcon;
    Color color4LegacyBracket = HudIndicatorColorInfo.GetTargetColor4LegacyBracket((ISpaceEntity) this.spaceObject);
    this.weaponNumberLabel.color = color4LegacyBracket;
    Color color = color4LegacyBracket;
    uiSprite.color = color;
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  public override void OnMultiTargetSelectionChange(bool isAbilityTarget, ShipAbility ability)
  {
    if (isAbilityTarget)
      this.OnMultiTargetSelected(ability);
    else
      this.OnMultiTargetDeselected(ability);
    base.OnMultiTargetSelectionChange(isAbilityTarget, ability);
  }

  private void OnMultiTargetSelected(ShipAbility ability)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    HudIndicatorMultiTargetNgui.\u003COnMultiTargetSelected\u003Ec__AnonStoreyCC selectedCAnonStoreyCc = new HudIndicatorMultiTargetNgui.\u003COnMultiTargetSelected\u003Ec__AnonStoreyCC();
    // ISSUE: reference to a compiler-generated field
    selectedCAnonStoreyCc.ability = ability;
    // ISSUE: reference to a compiler-generated field
    selectedCAnonStoreyCc.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated method
    if (this.weaponAbilities.Any<ShipAbility>(new Func<ShipAbility, bool>(selectedCAnonStoreyCc.\u003C\u003Em__1ED)))
      return;
    // ISSUE: reference to a compiler-generated field
    this.weaponAbilities.Add(selectedCAnonStoreyCc.ability);
  }

  private void OnMultiTargetDeselected(ShipAbility ability)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.weaponAbilities.RemoveAll(new Predicate<ShipAbility>(new HudIndicatorMultiTargetNgui.\u003COnMultiTargetDeselected\u003Ec__AnonStoreyCD()
    {
      ability = ability,
      \u003C\u003Ef__this = this
    }.\u003C\u003Em__1EE));
  }

  private bool IsSameAbility(ShipAbility ability1, ShipAbility ability2)
  {
    return (int) ability1.ServerID == (int) ability2.ServerID;
  }

  protected override void UpdateView()
  {
    this.UpdateHotkeyLabel();
    this.gameObject.SetActive(this.IsMultiselected && this.InScreen);
  }
}
