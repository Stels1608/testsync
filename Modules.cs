﻿// Decompiled with JetBrains decompiler
// Type: Modules
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Modules : SingleLODListener
{
  private readonly List<Renderer> renderers = new List<Renderer>();
  private readonly List<GameObject> moduleInstances = new List<GameObject>();
  private Ship ship;

  public static bool ShowModules { get; set; }

  private void Start()
  {
    this.ship = this.SpaceObject as Ship;
    if (this.SpaceObject != null && this.SpaceObject.Faction != Faction.Cylon && this.SpaceObject.Faction != Faction.Colonial)
      UnityEngine.Debug.LogWarning((object) ("No module bindings defined for faction: " + (object) this.SpaceObject.Faction + " Object: " + this.SpaceObject.PrefabName));
    else if (this.ship == null && !SectorEditorHelper.IsEditorLevel())
    {
      UnityEngine.Debug.LogError((object) ("Modules have to be attached to Ship. GameObject: " + this.gameObject.name + " SpaceObject: " + (object) this.SpaceObject));
    }
    else
    {
      if (!Modules.ShowModules && !this.SpaceObject.IsMe)
        return;
      this.StartCoroutine(this.BuildModules());
    }
  }

  [DebuggerHidden]
  private IEnumerator BuildModules()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Modules.\u003CBuildModules\u003Ec__Iterator17() { \u003C\u003Ef__this = this };
  }

  public void EnableModules()
  {
    this.Start();
  }

  public void RemoveModules()
  {
    if (this.moduleInstances == null || this.moduleInstances.Count == 0)
      return;
    using (List<GameObject>.Enumerator enumerator = this.moduleInstances.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current);
    }
    this.moduleInstances.Clear();
    this.renderers.Clear();
  }

  private void InstallModule(Object module, Transform target)
  {
    GameObject gameObject = (GameObject) Object.Instantiate(module);
    gameObject.transform.parent = target;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    this.renderers.Add(gameObject.GetComponentInChildren<Renderer>());
    this.moduleInstances.Add(gameObject);
  }

  protected override void OnSwitched(bool value)
  {
    for (int index = 0; index < this.renderers.Count; ++index)
      this.renderers[index].enabled = value;
  }
}
