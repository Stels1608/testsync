﻿// Decompiled with JetBrains decompiler
// Type: ShipImmutableAbility
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Paperdolls.PaperdollServerData;
using UnityEngine;

public class ShipImmutableAbility : ShipAbstractAbility
{
  public readonly uint consumableGuid;
  public readonly ShipImmutableSlot slot;
  public readonly ShipSystemCard systemCard;

  public bool IsValid
  {
    get
    {
      if (Game.Me.Caps.IsRestricted(Capability.Cast) || !(bool) this.card.IsLoaded || (Object) SpaceLevel.GetLevel() == (Object) null)
        return false;
      Ship ship = (Ship) SpaceLevel.GetLevel().GetPlayerShip();
      if (ship == null || !(bool) ship.IsLoaded)
        return false;
      switch (this.card.Affect)
      {
        case ShipAbilityAffect.Selected:
        case ShipAbilityAffect.MultiWeaponTarget:
          return this.CheckTarget(this.GetTarget());
        case ShipAbilityAffect.Ignore:
        case ShipAbilityAffect.Area:
          return true;
        default:
          return false;
      }
    }
  }

  public ShipImmutableAbility(ShipImmutableSlot slot)
  {
    this.slot = slot;
    this.consumableGuid = slot.ConsumableKey;
    this.systemCard = (ShipSystemCard) Game.Catalogue.FetchCard(slot.SystemKey, CardView.ShipSystem);
    this.systemCard.IsLoaded.AddHandler(new SignalHandler(this.OnSystemLoaded));
  }

  public void Cast()
  {
    if (this.card == null || !this.IsValid)
      return;
    switch (this.card.Affect)
    {
      case ShipAbilityAffect.Selected:
      case ShipAbilityAffect.MultiWeaponTarget:
        this.DoCast(new uint[1]
        {
          this.GetTarget().ObjectID
        }, this.card.Launch);
        break;
      case ShipAbilityAffect.Ignore:
        this.DoCast(new uint[0], this.card.Launch);
        break;
    }
  }

  protected void DoCast(uint[] idList, ShipAbilityLaunch castType)
  {
    GameProtocol.GetProtocol().CastImmutableSlotAbility(this.slot.SlotId, idList);
  }

  private void OnSystemLoaded()
  {
    this.card = this.systemCard.AbilityCards[0];
  }
}
