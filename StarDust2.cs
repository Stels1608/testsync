﻿// Decompiled with JetBrains decompiler
// Type: StarDust2
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class StarDust2 : MonoBehaviour
{
  public Material Material;
  public int SpeckCount;
  public float SpeckSize;
  public float Radius;
  public Color Color;
  public Transform Target;
  private Transform _camera;
  private StarDust2.Speck[] specks;
  private float sqrRadius;
  private float halfRadius;
  private float frontDist;
  private GameObject root;
  private Vector3 lastPosition;

  public void Recreate()
  {
    Object.Destroy((Object) this.root);
    this.specks = (StarDust2.Speck[]) null;
    this.Start();
  }

  private void Start()
  {
    this.Target = this.transform;
    this._camera = Camera.main.transform;
    this.lastPosition = this.Target.position;
    this.sqrRadius = this.Radius * this.Radius;
    this.halfRadius = 0.5f * this.Radius;
    this.frontDist = 0.86f * this.Radius;
    this.GenerateClouds();
  }

  private void GenerateClouds()
  {
    Material material = new Material(this.Material);
    material.SetColor("_TintColor", this.Color);
    GameObject original = new GameObject("Speck");
    original.AddComponent<MeshFilter>().mesh = this.MakeMesh(this.SpeckSize);
    original.AddComponent<MeshRenderer>().material = material;
    this.root = new GameObject("Specks");
    this.specks = new StarDust2.Speck[this.SpeckCount];
    for (int index = 0; index < this.specks.Length; ++index)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(original);
      gameObject.name = original.name;
      gameObject.transform.parent = this.root.transform;
      this.specks[index] = new StarDust2.Speck(gameObject)
      {
        transform = {
          position = this.Target.position + Random.insideUnitSphere * this.Radius
        }
      };
    }
    Object.Destroy((Object) original);
  }

  private Mesh MakeMesh(float size)
  {
    float num = 0.5f * size;
    Mesh mesh = new Mesh();
    mesh.vertices = new Vector3[4]
    {
      new Vector3(num, num),
      new Vector3(-num, num),
      new Vector3(-num, -num),
      new Vector3(num, -num)
    };
    mesh.triangles = new int[6]{ 0, 1, 2, 2, 3, 0 };
    mesh.RecalculateNormals();
    mesh.uv = new Vector2[4]
    {
      new Vector2(0.0f, 1f),
      new Vector2(1f, 1f),
      new Vector2(1f, 0.0f),
      new Vector2(0.0f, 0.0f)
    };
    return mesh;
  }

  private void LateUpdate()
  {
    Vector3 position = this.Target.position;
    Vector3 forward = position - this.lastPosition;
    if ((double) forward.sqrMagnitude > 0.100000001490116)
    {
      Quaternion quaternion = Quaternion.LookRotation(forward);
      foreach (StarDust2.Speck speck in this.specks)
      {
        if ((double) (speck.transform.position - position).sqrMagnitude > (double) this.sqrRadius)
        {
          Vector3 vector3 = (Vector3) (Random.insideUnitCircle * this.halfRadius);
          vector3.z = Random.Range(this.halfRadius, this.frontDist);
          speck.transform.position = position + quaternion * vector3;
        }
      }
    }
    Quaternion rotation = this._camera.rotation;
    foreach (StarDust2.Speck speck in this.specks)
      speck.transform.rotation = rotation;
    this.lastPosition = position;
  }

  private class Speck
  {
    public Transform transform;

    public Speck(GameObject gameObject)
    {
      this.transform = gameObject.transform;
    }

    public void Destroy()
    {
      Object.Destroy((Object) this.transform.gameObject);
    }
  }
}
