﻿// Decompiled with JetBrains decompiler
// Type: GuiUpgradeWindow1
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Common;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GuiUpgradeWindow1 : GuiDialog
{
  public readonly GuiUpgradeWindowData data;
  public readonly int wantedLevel;
  private readonly bool isTkUpgradable;
  private readonly GuiLabel resTitle;
  private readonly MoneyCost resMoney;
  private readonly MoneyCost resMoneyDiscounted;
  private readonly GuiLabel res1;
  private readonly GuiButton resConfirm;
  private readonly GuiButton resCancel;
  private readonly GuiLabel resError;
  private readonly Counter tkCounter;
  private readonly GuiLabel tk1;
  private readonly GuiLabel successChance;
  private readonly GuiLabel tk3;
  private readonly GuiLabel tk4;
  private readonly GuiLabel tk5;
  private readonly GuiLabel tkMoney;
  private readonly GuiButton tkConfirm;
  private readonly GuiButton tkCancel;
  private readonly GuiLabel tkError;
  private readonly GuiLabel successChanceDiscounted;
  private readonly GuiImage banner;
  private readonly GuiLabel banner_percent;
  private readonly GuiColoredBox strikeOriginalSuccessChanceLine;
  private readonly GuiColoredBox strikeOriginalResMoneyLine;

  public GuiUpgradeWindow1(GuiUpgradeWindowData data, int level)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    GuiUpgradeWindow1.\u003CGuiUpgradeWindow1\u003Ec__AnonStoreyBF window1CAnonStoreyBf = new GuiUpgradeWindow1.\u003CGuiUpgradeWindow1\u003Ec__AnonStoreyBF();
    // ISSUE: reference to a compiler-generated field
    window1CAnonStoreyBf.data = data;
    this.resTitle = new GuiLabel(Gui.Options.FontBGM_BT, 17);
    this.resMoney = new MoneyCost();
    this.resMoneyDiscounted = new MoneyCost();
    this.res1 = new GuiLabel("%$bgo.upgrade.are_you_sure%", Gui.Options.FontBGM_BT, 14);
    this.resConfirm = new GuiButton("%$bgo.upgrade.confirm%");
    this.resCancel = new GuiButton("%$bgo.common.cancel%");
    this.resError = new GuiLabel("%$bgo.upgrade.error_1%", Gui.Options.FontBGM_BT, 16);
    this.tkCounter = new Counter();
    this.tk1 = new GuiLabel("%$bgo.upgrade.message1%", Gui.Options.FontBGM_BT, 17);
    this.successChance = new GuiLabel("00%", Gui.Options.FontBGM_BT, 40);
    this.tk3 = new GuiLabel("%$bgo.upgrade.use%", Gui.Options.FontBGM_BT, 16);
    this.tk4 = new GuiLabel("%$bgo.upgrade.tuning_kits%", Gui.Options.FontBGM_BT, 16);
    this.tk5 = new GuiLabel("%$bgo.upgrade.are_you_sure%", Gui.Options.FontBGM_BT, 14);
    this.tkMoney = new GuiLabel(Gui.Options.FontBGM_BT, 14);
    this.tkConfirm = new GuiButton("%$bgo.upgrade.confirm%");
    this.tkCancel = new GuiButton("%$bgo.common.cancel%");
    this.tkError = new GuiLabel(Gui.Options.FontBGM_BT, 16);
    this.successChanceDiscounted = new GuiLabel("00%", Gui.Options.FontBGM_BT, 40);
    // ISSUE: explicit constructor call
    base.\u002Ector();
    // ISSUE: reference to a compiler-generated field
    window1CAnonStoreyBf.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    this.data = window1CAnonStoreyBf.data;
    this.wantedLevel = level;
    this.Align = Align.MiddleCenter;
    this.Position = new Vector2(0.0f, 0.0f);
    this.Size = new Vector2(460f, 460f);
    // ISSUE: reference to a compiler-generated method
    this.OnClose = new AnonymousDelegate(window1CAnonStoreyBf.\u003C\u003Em__1AA);
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.upgrade.upgrage_with_resources%", Gui.Options.FontBGM_BT, 20), Align.UpCenter, new Vector2(0.0f, 15f));
    this.AddChild((GuiElementBase) new GuiColoredBox(Gui.Options.PositiveColor, new Vector2(0.0f, 50f), new Vector2(300f, 1f)), Align.UpCenter);
    this.resTitle.WordWrap = true;
    this.resTitle.SizeX = 420f;
    this.resTitle.Alignment = TextAnchor.UpperCenter;
    this.AddChild((GuiElementBase) this.resTitle, Align.UpCenter, new Vector2(0.0f, 60f));
    this.AddChild((GuiElementBase) this.resMoney, Align.UpCenter, new Vector2(0.0f, 120f));
    this.AddChild((GuiElementBase) this.resMoneyDiscounted, Align.UpCenter, new Vector2(0.0f, 105f));
    this.AddChild((GuiElementBase) this.res1, Align.UpCenter, new Vector2(0.0f, 140f));
    this.resConfirm.SizeY = 21f;
    this.resConfirm.Font = Gui.Options.FontBGM_BT;
    this.resConfirm.FontSize = 14;
    this.AddChild((GuiElementBase) this.resConfirm, Align.UpCenter, new Vector2(-100f, 170f));
    this.resCancel.SizeY = 21f;
    this.resCancel.Font = Gui.Options.FontBGM_BT;
    this.resCancel.FontSize = 14;
    this.AddChild((GuiElementBase) this.resCancel, Align.UpCenter, new Vector2(100f, 170f));
    this.resError.WordWrap = true;
    this.resError.SizeX = 380f;
    this.resError.Alignment = TextAnchor.UpperCenter;
    this.AddChild((GuiElementBase) this.resError, Align.UpCenter, new Vector2(0.0f, 100f));
    // ISSUE: reference to a compiler-generated field
    this.isTkUpgradable = (double) window1CAnonStoreyBf.data.Item.GetPriceToUpgrade(level, false).Cubits > 0.0;
    if (this.isTkUpgradable)
    {
      this.AddChild((GuiElementBase) new GuiColoredBox(Gui.Options.PositiveColor, new Vector2(0.0f, 210f), new Vector2(300f, 1f)), Align.UpCenter);
      this.AddChild((GuiElementBase) new GuiLabel("%$bgo.upgrade.upgrage_with_tuning_kits%", Gui.Options.FontBGM_BT, 20), Align.UpCenter, new Vector2(0.0f, 223f));
      this.AddChild((GuiElementBase) new GuiColoredBox(Gui.Options.PositiveColor, new Vector2(0.0f, 260f), new Vector2(300f, 1f)), Align.UpCenter);
      this.tkCounter.TextboxWidth = 22f;
      this.AddChild((GuiElementBase) this.tkCounter, Align.UpCenter, new Vector2(0.0f, 275f));
      this.AddChild((GuiElementBase) this.tk3, Align.UpCenter, new Vector2(0.0f, 271f));
      this.AddChild((GuiElementBase) this.tk4, Align.UpCenter, new Vector2(0.0f, 271f));
      this.AddChild((GuiElementBase) this.tk1, Align.UpCenter, new Vector2(0.0f, 300f));
      this.AddChild((GuiElementBase) this.tkMoney, Align.UpCenter, new Vector2(0.0f, 360f));
      this.AddChild((GuiElementBase) this.tk5, Align.UpCenter, new Vector2(0.0f, 380f));
      this.tkConfirm.SizeY = 21f;
      this.tkConfirm.Font = Gui.Options.FontBGM_BT;
      this.tkConfirm.FontSize = 14;
      this.AddChild((GuiElementBase) this.tkConfirm, Align.UpCenter, new Vector2(-100f, 410f));
      this.tkCancel.SizeY = 21f;
      this.tkCancel.Font = Gui.Options.FontBGM_BT;
      this.tkCancel.FontSize = 14;
      this.AddChild((GuiElementBase) this.tkCancel);
      this.tkError.WordWrap = true;
      this.tkError.SizeX = 380f;
      this.tkError.Alignment = TextAnchor.UpperCenter;
      this.AddChild((GuiElementBase) this.tkError, Align.UpCenter, new Vector2(0.0f, 300f));
      this.AddChild((GuiElementBase) this.successChance);
    }
    this.strikeOriginalResMoneyLine = new GuiColoredBox(Gui.Options.PositiveColor);
    this.strikeOriginalResMoneyLine.Size = new Vector2(150f, 2f);
    this.AddChild((GuiElementBase) this.strikeOriginalResMoneyLine, Align.UpCenter, new Vector2(0.0f, 126f));
    this.successChanceDiscounted.NormalColor = Gui.Options.PositiveColor;
    this.AddChild((GuiElementBase) this.successChanceDiscounted);
    this.strikeOriginalSuccessChanceLine = new GuiColoredBox(Gui.Options.PositiveColor);
    this.AddChild((GuiElementBase) this.strikeOriginalSuccessChanceLine, Align.UpLeft);
    this.banner = new GuiImage("GUI/EquipBuyPanel/DetailsWindow/banner");
    this.banner.Position = new Vector2(385f, 2f);
    this.banner.handleMouseInput = false;
    this.AddChild((GuiElementBase) this.banner);
    this.banner_percent = new GuiLabel(Gui.Options.FontBGM_BT, 15);
    this.banner_percent.Position = new Vector2(418f, 19f);
    this.banner_percent.Size = new Vector2(34f, 16f);
    this.AddChild((GuiElementBase) this.banner_percent);
    // ISSUE: reference to a compiler-generated method
    this.resCancel.Pressed = new AnonymousDelegate(window1CAnonStoreyBf.\u003C\u003Em__1AB);
    this.tkCancel.Pressed = this.resCancel.Pressed;
    // ISSUE: reference to a compiler-generated method
    this.resConfirm.Pressed = new AnonymousDelegate(window1CAnonStoreyBf.\u003C\u003Em__1AC);
    // ISSUE: reference to a compiler-generated method
    this.tkConfirm.Pressed = new AnonymousDelegate(window1CAnonStoreyBf.\u003C\u003Em__1AD);
    this.AddChild((GuiElementBase) new Gui.Timer(1f));
    this.PeriodicUpdate();
  }

  public List<ResourceType> GetMissingRessources(ShipSystem system, int wantedLevel)
  {
    List<ResourceType> resourceTypeList = new List<ResourceType>();
    CurrencyAmount priceToUpgrade = system.GetPriceToUpgrade(wantedLevel, true);
    float num1 = priceToUpgrade.Cubits;
    float num2 = priceToUpgrade.Tylium;
    float num3 = priceToUpgrade.Token;
    if ((double) Game.Me.Hold.GetCountByGUID(264733124U) < (double) num1)
      resourceTypeList.Add(ResourceType.Cubits);
    if ((double) Game.Me.Hold.GetCountByGUID(215278030U) < (double) num2)
      resourceTypeList.Add(ResourceType.Tylium);
    if ((double) Game.Me.Hold.GetCountByGUID(130920111U) < (double) num3)
      resourceTypeList.Add(ResourceType.Token);
    return resourceTypeList;
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    this.tk3.PositionX = (float) (-(double) this.tk3.SizeX / 2.0 - (double) this.tkCounter.SizeX / 2.0 - 10.0);
    this.tk4.PositionX = (float) ((double) this.tk4.SizeX / 2.0 + (double) this.tkCounter.SizeX / 2.0 + 10.0);
    this.resTitle.Text = BsgoLocalization.Get("%$bgo.upgrade.cost_1%", (object) this.data.Item.ItemGUICard.Name, (object) this.wantedLevel);
    bool flag1 = this.wantedLevel <= 10;
    bool flag2 = this.wantedLevel - (int) this.data.Item.Card.Level == 1;
    this.resTitle.IsRendered = flag1;
    this.resMoney.IsRendered = flag1;
    this.res1.IsRendered = flag1;
    this.resConfirm.IsRendered = flag1;
    this.resCancel.IsRendered = flag1;
    this.resError.IsRendered = !flag1;
    int b = (int) Game.Me.Hold.GetCountByGUID(254909109U);
    bool flag3 = flag2 && b > 0;
    this.tk1.IsRendered = flag3;
    this.successChance.IsRendered = flag3;
    this.tk3.IsRendered = flag3;
    this.tk4.IsRendered = flag3;
    this.tk5.IsRendered = flag3;
    this.tkCounter.IsRendered = flag3;
    this.tkMoney.IsRendered = flag3;
    this.tkConfirm.IsRendered = flag3;
    this.tkError.IsRendered = !flag3;
    if (!flag3)
    {
      if (!flag2)
        this.tkError.Text = BsgoLocalization.Get("%$bgo.upgrade.error_2%", (object) this.data.Item.ItemGUICard.Name, (object) ((int) this.data.Item.ItemGUICard.Level + 1));
      else
        this.tkError.Text = "%$bgo.upgrade.error_no_tuning_kits%";
    }
    float c1 = this.data.Item.GetPriceToUpgrade(this.wantedLevel, false).Cubits;
    float c2 = this.data.Item.GetPriceToUpgrade(this.wantedLevel, true).Cubits;
    GuiUpgradeWindow.UpdateMoneyCostWidget(this.resMoney, this.data.Item, this.wantedLevel, false);
    GuiUpgradeWindow.UpdateMoneyCostWidget(this.resMoneyDiscounted, this.data.Item, this.wantedLevel, true);
    bool flag4 = Shop.IsUpgradeDiscounted(Shop.FindShopCategoryForSlotType(this.data.Item.Card.SlotType), this.wantedLevel);
    GuiImage guiImage = this.banner;
    bool flag5 = flag4;
    this.banner_percent.IsRendered = flag5;
    int num1 = flag5 ? 1 : 0;
    guiImage.IsRendered = num1 != 0;
    MoneyCost moneyCost = this.resMoneyDiscounted;
    bool flag6 = flag1 && flag4;
    this.strikeOriginalResMoneyLine.IsRendered = flag6;
    int num2 = flag6 ? 1 : 0;
    moneyCost.IsRendered = num2 != 0;
    GuiLabel guiLabel1 = this.successChanceDiscounted;
    bool flag7 = flag4 && flag3 && this.isTkUpgradable;
    this.strikeOriginalSuccessChanceLine.IsRendered = flag7;
    int num3 = flag7 ? 1 : 0;
    guiLabel1.IsRendered = num3 != 0;
    if (flag4)
    {
      this.successChance.Align = Align.UpLeft;
      this.successChance.Position = new Vector2((float) ((double) this.SizeX / 2.0 + 10.0), 320f);
      this.successChanceDiscounted.Position = new Vector2((float) ((double) this.SizeX / 2.0 - 110.0), 320f);
      this.banner_percent.Text = this.data.Item.FindDiscountInUpgradeLevels().Percentage.ToString() + "%";
    }
    else
    {
      this.successChance.Align = Align.UpCenter;
      this.successChance.Position = new Vector2(0.0f, 320f);
    }
    if ((double) Math.Abs(c1) < 0.00999999977648258)
    {
      GuiLabel guiLabel2 = this.successChance;
      string str1 = this.tkCounter.Value <= 0 ? "0%" : "100%";
      this.successChanceDiscounted.Text = str1;
      string str2 = str1;
      guiLabel2.Text = str2;
    }
    else
    {
      this.successChance.Text = this.GetSuccessrateString(c1);
      this.successChanceDiscounted.Text = this.GetSuccessrateString(c2);
    }
    if (flag4)
    {
      this.successChance.Text = "(" + this.successChance.Text + ")";
      this.strikeOriginalSuccessChanceLine.Position = new Vector2(this.SizeX / 2f, 342f);
      this.strikeOriginalSuccessChanceLine.Size = new Vector2((float) (30 + this.successChance.Text.Length * 20), 4f);
    }
    int a = Mathf.CeilToInt((float) ((!flag4 ? (double) c1 : (double) c2) / 1000.0));
    if (a == 0)
      a = 1;
    this.tkCounter.MaxValue = Mathf.Min(a, b);
    this.tkMoney.Text = BsgoLocalization.Get("%$bgo.upgrade.cost_2%", (object) this.tkCounter.Value, (object) b);
    this.tkCancel.Align = flag1 || flag3 ? Align.UpCenter : Align.DownCenter;
    this.tkCancel.Position = flag1 || flag3 ? new Vector2(100f, 410f) : new Vector2(0.0f, -15f);
  }

  private string GetSuccessrateString(float c)
  {
    return ((double) c != 0.0 ? Mathf.Clamp((int) Mathf.Floor((float) ((double) this.tkCounter.Value * 1000.0 * 100.0) / c), 0, 100) : 101).ToString() + "%";
  }

  [Gui2Editor]
  public static void Show(int i)
  {
    Game.RegisterDialog((IGUIPanel) new GuiUpgradeWindow1(new GuiUpgradeWindowData(Game.Me.Hangar.ActiveShip.Slots[i].ServerID), (int) Game.Me.Hangar.ActiveShip.Slots[i].System.Card.Level + 1), true);
  }
}
