﻿// Decompiled with JetBrains decompiler
// Type: LoadingSceneWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System.Collections.Generic;
using UnityEngine;

public class LoadingSceneWindow : GUIPanel
{
  private Dictionary<TransSceneType, string> textures;
  private Dictionary<TransSceneType, List<string>> colonialTips;
  private Dictionary<TransSceneType, List<string>> cylonTips;
  private readonly string tooltipText;
  private readonly GUILabelNew tipLabel;
  private readonly string loadingLabelInitialText;
  private readonly GUILabelNew loadingLabel;
  private readonly GUIDoubleCountdownNew countdown;

  public LoadingSceneWindow(TransSceneType transSceneType)
    : base((SmartRect) null)
  {
    this.InitializePictures();
    this.InitTips();
    if (transSceneType == TransSceneType.None)
      transSceneType = (TransSceneType) Random.Range(1, 8);
    JWindowDescription jwindowDescription = Loaders.LoadResource("GUI/LoadingScene/gui_loading_layout");
    GUIImageNew guiImageNew = (jwindowDescription["picture_image"] as JImage).CreateGUIImageNew(this.root);
    guiImageNew.Texture = this.GetPicture(transSceneType);
    guiImageNew.MakeCenteredRect();
    this.AddPanel((GUIPanel) guiImageNew);
    this.tipLabel = (jwindowDescription["tip_label"] as JLabel).CreateGUILabelNew(this.root);
    this.tooltipText = this.GetTip(transSceneType);
    this.tipLabel.Text = this.tooltipText;
    this.tipLabel.MakeCenteredRect();
    this.AddPanel((GUIPanel) this.tipLabel);
    this.loadingLabel = (jwindowDescription["loading_label"] as JLabel).CreateGUILabelNew(this.root);
    this.loadingLabelInitialText = this.loadingLabel.Text;
    this.AddPanel((GUIPanel) this.loadingLabel);
    this.countdown = new GUIDoubleCountdownNew(this.root);
    this.countdown.Position = (jwindowDescription["progress_pivot"] as JPivot).position;
    this.countdown.IsUpdated = false;
    this.AddPanel((GUIPanel) this.countdown);
  }

  public override void Update()
  {
    base.Update();
    this.tipLabel.Text = this.tooltipText;
    this.tipLabel.MakeCenteredRect();
  }

  public override void RecalculateAbsCoords()
  {
    this.Position = new float2((float) (Screen.width / 2), (float) (Screen.height / 2));
    base.RecalculateAbsCoords();
  }

  public void SetProgress(float progress, bool addPercents)
  {
    this.countdown.Progress = progress;
    this.loadingLabel.Text = !addPercents ? this.loadingLabelInitialText : string.Format("{0} {1:P0}", (object) this.loadingLabelInitialText, (object) progress);
  }

  protected void InitializePictures()
  {
    this.textures = new Dictionary<TransSceneType, string>();
    this.textures[TransSceneType.Undock] = "GUI/LoadingScene/undock";
    this.textures[TransSceneType.Ftl] = "GUI/LoadingScene/ftl";
    this.textures[TransSceneType.Hangar] = "GUI/LoadingScene/dock_hangar";
    this.textures[TransSceneType.CIC] = "GUI/LoadingScene/cic";
    this.textures[TransSceneType.Recroom] = "GUI/LoadingScene/recroom";
    this.textures[TransSceneType.Outpost] = "GUI/LoadingScene/outpost";
    this.textures[TransSceneType.Minigfacility] = "GUI/LoadingScene/outpost";
    this.textures[TransSceneType.Die] = "GUI/LoadingScene/die";
    this.textures[TransSceneType.FirstStory] = "GUI/LoadingScene/mission1end";
    this.textures[TransSceneType.Dock] = "GUI/LoadingScene/cic";
    this.textures[TransSceneType.Arena] = "GUI/LoadingScene/arena";
    this.textures[TransSceneType.Battlespace] = "GUI/LoadingScene/battlespace";
    this.textures[TransSceneType.Teaser] = "GUI/LoadingScene/teaser";
    this.textures[TransSceneType.Tournament] = Game.Tournament.Graphics.LoadingScreen;
  }

  protected Texture2D GetPicture(TransSceneType transSceneType)
  {
    if (this.textures.ContainsKey(transSceneType))
      return ResourceLoader.Load<Texture2D>(this.textures[transSceneType]);
    return ResourceLoader.Load<Texture2D>("GUI/LoadingScene/ftl");
  }

  private string GetTip(TransSceneType transSceneType)
  {
    List<string> stringList;
    switch (Game.Me.Faction)
    {
      case Faction.Colonial:
        stringList = !this.colonialTips.ContainsKey(transSceneType) ? this.colonialTips[TransSceneType.Ftl] : this.colonialTips[transSceneType];
        break;
      case Faction.Cylon:
        stringList = !this.cylonTips.ContainsKey(transSceneType) ? this.cylonTips[TransSceneType.Ftl] : this.cylonTips[transSceneType];
        break;
      default:
        stringList = !this.colonialTips.ContainsKey(transSceneType) ? this.colonialTips[TransSceneType.Ftl] : this.colonialTips[transSceneType];
        break;
    }
    int index = Random.Range(0, stringList.Count);
    return stringList[index];
  }

  protected void InitTips()
  {
    this.cylonTips = new Dictionary<TransSceneType, List<string>>();
    this.colonialTips = new Dictionary<TransSceneType, List<string>>();
    int[] numArray1 = new int[3]{ 2, 4, 15 };
    int[] numArray2 = new int[4]{ 3, 16, 17, 30 };
    int[] numArray3 = new int[3]{ 19, 32, 34 };
    int[] numArray4 = new int[3]{ 21, 22, 24 };
    int[] numArray5 = new int[3]{ 5, 6, 7 };
    int[] numArray6 = new int[1]{ 33 };
    int[] numArray7 = new int[8]{ 20, 23, 25, 26, 27, 28, 29, 31 };
    int[] numArray8 = new int[8]{ 8, 9, 10, 11, 12, 13, 14, 18 };
    this.AddTips(ref this.cylonTips, TransSceneType.Die, new List<int[]>()
    {
      numArray1,
      numArray2
    });
    this.AddTips(ref this.cylonTips, TransSceneType.Undock, new List<int[]>()
    {
      numArray1,
      numArray2,
      numArray3
    });
    this.AddTips(ref this.cylonTips, TransSceneType.Outpost, new List<int[]>()
    {
      numArray7
    });
    this.AddTips(ref this.cylonTips, TransSceneType.CIC, new List<int[]>()
    {
      numArray7
    });
    this.AddTips(ref this.cylonTips, TransSceneType.Ftl, new List<int[]>()
    {
      numArray1,
      numArray2,
      numArray3,
      numArray4
    });
    this.AddTips(ref this.cylonTips, TransSceneType.Dock, new List<int[]>()
    {
      numArray4,
      numArray7
    });
    this.AddTips(ref this.cylonTips, TransSceneType.Arena, new List<int[]>()
    {
      numArray2
    });
    this.AddTips(ref this.cylonTips, TransSceneType.Battlespace, new List<int[]>()
    {
      numArray2
    });
    this.AddTips(ref this.cylonTips, TransSceneType.Tournament, new List<int[]>()
    {
      numArray2,
      numArray6
    });
    this.AddTips(ref this.colonialTips, TransSceneType.Die, new List<int[]>()
    {
      numArray1,
      numArray2
    });
    this.AddTips(ref this.colonialTips, TransSceneType.Undock, new List<int[]>()
    {
      numArray1,
      numArray2,
      numArray3
    });
    this.AddTips(ref this.colonialTips, TransSceneType.Outpost, new List<int[]>()
    {
      numArray8
    });
    this.AddTips(ref this.colonialTips, TransSceneType.CIC, new List<int[]>()
    {
      numArray8
    });
    this.AddTips(ref this.colonialTips, TransSceneType.Ftl, new List<int[]>()
    {
      numArray1,
      numArray2,
      numArray3,
      numArray5
    });
    this.AddTips(ref this.colonialTips, TransSceneType.Dock, new List<int[]>()
    {
      numArray5,
      numArray8
    });
    this.AddTips(ref this.colonialTips, TransSceneType.Arena, new List<int[]>()
    {
      numArray2
    });
    this.AddTips(ref this.colonialTips, TransSceneType.Battlespace, new List<int[]>()
    {
      numArray2
    });
    this.AddTips(ref this.colonialTips, TransSceneType.Tournament, new List<int[]>()
    {
      numArray2,
      numArray6
    });
  }

  private void AddTips(ref Dictionary<TransSceneType, List<string>> dict, TransSceneType type, List<int[]> tipsIndexList)
  {
    if (!dict.ContainsKey(type))
      dict.Add(type, new List<string>());
    using (List<int[]>.Enumerator enumerator = tipsIndexList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        foreach (int num in enumerator.Current)
          dict[type].Add("%$bgo.tips." + (object) num + "%");
      }
    }
  }
}
