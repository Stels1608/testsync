﻿// Decompiled with JetBrains decompiler
// Type: GUIOptionsButtons
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;

public class GUIOptionsButtons : GUIPanel
{
  private GUIBlinker blinker = new GUIBlinker();
  private GUIButtonNew systemMap;
  private GUIButtonNew restock;
  private GUIButtonNew galaxyMap;

  public bool IsBlinking
  {
    get
    {
      return this.blinker.IsRendered;
    }
    set
    {
      this.blinker.IsRendered = value;
    }
  }

  public GUIOptionsButtons()
  {
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/Options/options_layout");
    this.systemMap = (jwindowDescription["systemMap"] as JButton).CreateGUIButtonNew(this.root);
    this.restock = (jwindowDescription["restock"] as JButton).CreateGUIButtonNew(this.root);
    this.galaxyMap = (jwindowDescription["galaxyMap"] as JButton).CreateGUIButtonNew(this.root);
    this.systemMap.Handler = (AnonymousDelegate) (() =>
    {
      if (!Game.GUIManager.IsRendered)
        return;
      FacadeFactory.GetInstance().SendMessage(Message.Show3DSectorMap, (object) true);
      if (!this.IsBlinking)
        return;
      StoryProtocol.GetProtocol().TriggerControl(StoryProtocol.ControlType.SystemMap);
    });
    this.restock.Handler = (AnonymousDelegate) (() => Game.GUIManager.Find<GUICharacterStatusWindow>().OpenRestock());
    this.galaxyMap.Handler = (AnonymousDelegate) (() =>
    {
      if (!Game.GUIManager.IsRendered)
        return;
      Game.GUIManager.Find<GalaxyMapMain>().IsRendered = true;
      Game.PopupTipManager.DisplayPopupTip(PopupTipType.SectorMap);
    });
    this.systemMap.SetTooltip("%$bgo.option_buttons.system_map%", Action.ToggleSystemMap3D);
    this.restock.SetTooltip("%$bgo.option_buttons.inflight_resupply%", Action.ToggleWindowInFlightSupply);
    this.galaxyMap.SetTooltip("%$bgo.option_buttons.sector_navigation%", Action.ToggleWindowGalaxyMap);
    this.AddPanel((GUIPanel) this.systemMap);
    this.AddPanel((GUIPanel) this.restock);
    if (SpaceLevel.GetLevel().IsGlobal)
      this.AddPanel((GUIPanel) this.galaxyMap);
    this.systemMap.AddPanel((GUIPanel) this.blinker);
    this.IsBlinking = false;
  }
}
