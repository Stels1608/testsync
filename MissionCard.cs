﻿// Decompiled with JetBrains decompiler
// Type: MissionCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class MissionCard : Card
{
  public string Action = string.Empty;
  public int Level;
  public int LevelRequirement;
  public int LevelUpperLimit;
  public RewardCard RewardCard;
  public GUICard ReceiverGUICard;

  public MissionCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.Level = (int) r.ReadByte();
    this.LevelRequirement = (int) r.ReadByte();
    this.LevelUpperLimit = (int) r.ReadByte();
    uint cardGUID1 = r.ReadGUID();
    uint cardGUID2 = r.ReadGUID();
    this.Action = r.ReadString();
    if (this.RewardCard == null)
    {
      this.RewardCard = (RewardCard) Game.Catalogue.FetchCard(cardGUID1, CardView.Reward);
      this.IsLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) this.RewardCard
      });
    }
    if ((int) cardGUID2 == 0 || this.ReceiverGUICard != null)
      return;
    this.ReceiverGUICard = (GUICard) Game.Catalogue.FetchCard(cardGUID2, CardView.GUI);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.ReceiverGUICard
    });
  }
}
