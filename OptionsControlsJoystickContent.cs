﻿// Decompiled with JetBrains decompiler
// Type: OptionsControlsJoystickContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System;
using UnityEngine;

public class OptionsControlsJoystickContent : OptionsControlsBase
{
  protected override void InitContent()
  {
    GameObject gameObject1 = NGUITools.AddChild(this.Layout.ContentArea[0], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingUi"));
    if ((UnityEngine.Object) gameObject1 == (UnityEngine.Object) null)
      throw new ArgumentNullException("initInputBindingContent");
    this.inputBindingUi = gameObject1.GetComponent<InputBindingUi>();
    this.inputBindingUi.Setup(InputDevice.Joystick);
    GameObject gameObject2 = NGUITools.AddChild(this.Layout.ContentArea[1], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingOptionsJoystickGamepad"));
    if ((UnityEngine.Object) gameObject2 == (UnityEngine.Object) null)
      throw new ArgumentNullException("initInputBindingOptionsContent");
    this.InputBindingOptions = (InputBindingOptionsBase) gameObject2.GetComponent<InputBindingOptionsJoystickGamepad>();
    if ((UnityEngine.Object) this.InputBindingOptions == (UnityEngine.Object) null)
      throw new ArgumentNullException("InputBindingOptions");
    (this.InputBindingOptions as InputBindingOptionsJoystickGamepad).ToggleJoystickEnabledDelegate = new InputBindingOptionsJoystickGamepad.ToggleJoystickEnabledHandler(this.ToggleJoystickGamepadEnabled);
    (this.InputBindingOptions as InputBindingOptionsJoystickGamepad).ToggleXbox360Buttons = new InputBindingOptionsJoystickGamepad.ToggleXbox360ButtonsHandler(this.ToggleXbox360Buttons);
    this.InputBindingOptions.InjectOnChangeDelegate(new OnSettingChanged(((OptionsContent) this).ChangeSetting));
    this.InitButtonBar();
  }

  public void ToggleJoystickGamepadEnabled(bool p)
  {
    this.inputBindingUi.ShowBindingElements(p);
    (this.InputBindingOptions as InputBindingOptionsJoystickGamepad).ToggleJoystickGamepadEnabled(p);
  }

  public void ToggleXbox360Buttons(bool p)
  {
    FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.ShowXbox360Buttons, (object) p, false));
    this.inputBindingUi.RepaintBindingElements();
  }

  protected override string GetTabText()
  {
    return Tools.ParseMessage("%$bgo.ui.options.control.controls.headline.joystick%");
  }
}
