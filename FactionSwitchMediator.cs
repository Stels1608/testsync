﻿// Decompiled with JetBrains decompiler
// Type: FactionSwitchMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class FactionSwitchMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "FactionSwitchMediator";
  private FactionSwitchDialog view;

  public FactionSwitchMediator(CharacterService factionSwitchService)
    : base("FactionSwitchMediator")
  {
    this.view = this.CreateView();
    this.view.SetCharacterService(factionSwitchService);
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.FactionSwitchSuccessful);
    this.AddMessageInterest(Message.CancelFactionSwitch);
    this.AddMessageInterest(Message.PlayerCubitsHold);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.CancelFactionSwitch:
        this.OwnerFacade.DetachView("FactionSwitchMediator");
        break;
      case Message.FactionSwitchSuccessful:
        Game.QuitUpdate();
        break;
      case Message.PlayerCubitsHold:
        if (!((Object) this.view != (Object) null))
          break;
        this.view.OnCubitsBalanceChanged();
        break;
    }
  }

  public override void OnBeforeDetach()
  {
    if (!((Object) this.view != (Object) null))
      return;
    Object.Destroy((Object) this.view.gameObject);
  }

  private FactionSwitchDialog CreateView()
  {
    return UguiTools.CreateChild<FactionSwitchDialog>("CharacterMenu/FactionSwitchDialog", UguiTools.GetCanvas(BsgoCanvas.CharacterMenu).transform);
  }
}
