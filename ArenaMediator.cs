﻿// Decompiled with JetBrains decompiler
// Type: ArenaMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class ArenaMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "ArenaMediator";

  public ArenaMediator()
    : base("ArenaMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.PlayerInThreat);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    if (message.Id != Message.PlayerInThreat || Game.Me == null || Game.Me.Arena == null)
      return;
    Game.Me.Arena.CancelPendingInvites();
  }
}
