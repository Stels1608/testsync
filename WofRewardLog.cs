﻿// Decompiled with JetBrains decompiler
// Type: WofRewardLog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class WofRewardLog : MonoBehaviour, ILocalizeable
{
  private Queue<GameObject> rewardQueue = new Queue<GameObject>();
  private const int MAX_MESSAGES = 60;
  [SerializeField]
  private GameObject scrollArea;
  [SerializeField]
  private UILabel headline;

  public void Start()
  {
    this.ReloadLanguageData();
  }

  public void OnEnable()
  {
    this.Invoke("Reposition", 0.05f);
  }

  public void AddRewardMessage(WofDrawReply reply)
  {
    int num = reply.rewardItems.Count + reply.bonusMapCards.Count;
    while (this.rewardQueue.Count + num > 60)
      Object.Destroy((Object) this.rewardQueue.Dequeue());
    this.scrollArea.GetComponent<UIGrid>().Reposition();
    using (List<WofRewardItemContainer>.Enumerator enumerator = reply.rewardItems.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WofRewardItemContainer current = enumerator.Current;
        this.AddRewardMessage(current.itemCard, (uint) current.amount);
      }
    }
    using (List<WofBonusMapPart>.Enumerator enumerator = reply.bonusMapCards.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WofBonusMapPart current = enumerator.Current;
        for (int index = 0; index < current.BonusMapCardList.Count; ++index)
          this.AddRewardMessage(current.Guicard, 1U);
      }
    }
    this.RenameRewardMessages();
    this.Invoke("Reposition", 0.05f);
  }

  private void Reposition()
  {
    this.scrollArea.GetComponent<UIGrid>().Reposition();
    this.scrollArea.transform.parent.GetComponent<UIScrollView>().ResetPosition();
  }

  public void AddRewardMessage(GUICard itemCard, uint amount)
  {
    GameObject gameObject = NGUITools.AddChild(this.scrollArea, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/general/ItemListElement"));
    ItemListElement component = gameObject.GetComponent<ItemListElement>();
    if ((Object) component != (Object) null)
      component.SetItem(itemCard, amount, 450);
    this.rewardQueue.Enqueue(gameObject);
  }

  private void RenameRewardMessages()
  {
    for (int count = this.rewardQueue.Count; count > 0; --count)
      this.rewardQueue.ElementAt<GameObject>(count - 1).name = (70 - count).ToString() + "_reward";
  }

  public void ReloadLanguageData()
  {
    this.headline.text = Tools.ParseMessage("%$bgo.wof.reward_log%").ToUpper();
  }
}
