﻿// Decompiled with JetBrains decompiler
// Type: UIScrollView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (UIPanel))]
[AddComponentMenu("NGUI/Interaction/Scroll View")]
[ExecuteInEditMode]
public class UIScrollView : MonoBehaviour
{
  public static BetterList<UIScrollView> list = new BetterList<UIScrollView>();
  public UIScrollView.DragEffect dragEffect = UIScrollView.DragEffect.MomentumAndSpring;
  public bool restrictWithinPanel = true;
  public bool smoothDragStart = true;
  public bool iOSDragEmulation = true;
  public float scrollWheelFactor = 0.25f;
  public float momentumAmount = 35f;
  public UIScrollView.ShowCondition showScrollBars = UIScrollView.ShowCondition.OnlyIfNeeded;
  public Vector2 customMovement = new Vector2(1f, 0.0f);
  [SerializeField]
  [HideInInspector]
  private Vector3 scale = new Vector3(1f, 0.0f, 0.0f);
  [SerializeField]
  [HideInInspector]
  private Vector2 relativePositionOnReset = Vector2.zero;
  protected Vector3 mMomentum = Vector3.zero;
  protected int mDragID = -10;
  protected Vector2 mDragStartOffset = Vector2.zero;
  public UIScrollView.Movement movement;
  public bool disableDragIfFits;
  public UIProgressBar horizontalScrollBar;
  public UIProgressBar verticalScrollBar;
  public UIWidget.Pivot contentPivot;
  public UIScrollView.OnDragNotification onDragStarted;
  public UIScrollView.OnDragNotification onDragFinished;
  public UIScrollView.OnDragNotification onMomentumMove;
  public UIScrollView.OnDragNotification onStoppedMoving;
  protected Transform mTrans;
  protected UIPanel mPanel;
  protected Plane mPlane;
  protected Vector3 mLastPos;
  protected bool mPressed;
  protected float mScroll;
  protected Bounds mBounds;
  protected bool mCalculatedBounds;
  protected bool mShouldMove;
  protected bool mIgnoreCallbacks;
  protected bool mDragStarted;
  [HideInInspector]
  public UICenterOnChild centerOnChild;

  public UIPanel panel
  {
    get
    {
      return this.mPanel;
    }
  }

  public bool isDragging
  {
    get
    {
      if (this.mPressed)
        return this.mDragStarted;
      return false;
    }
  }

  public virtual Bounds bounds
  {
    get
    {
      if (!this.mCalculatedBounds)
      {
        this.mCalculatedBounds = true;
        this.mTrans = this.transform;
        this.mBounds = NGUIMath.CalculateRelativeWidgetBounds(this.mTrans, this.mTrans);
      }
      return this.mBounds;
    }
  }

  public bool canMoveHorizontally
  {
    get
    {
      if (this.movement == UIScrollView.Movement.Horizontal || this.movement == UIScrollView.Movement.Unrestricted)
        return true;
      if (this.movement == UIScrollView.Movement.Custom)
        return (double) this.customMovement.x != 0.0;
      return false;
    }
  }

  public bool canMoveVertically
  {
    get
    {
      if (this.movement == UIScrollView.Movement.Vertical || this.movement == UIScrollView.Movement.Unrestricted)
        return true;
      if (this.movement == UIScrollView.Movement.Custom)
        return (double) this.customMovement.y != 0.0;
      return false;
    }
  }

  public virtual bool shouldMoveHorizontally
  {
    get
    {
      float num = this.bounds.size.x;
      if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
        num += this.mPanel.clipSoftness.x * 2f;
      return Mathf.RoundToInt(num - this.mPanel.width) > 0;
    }
  }

  public virtual bool shouldMoveVertically
  {
    get
    {
      float num = this.bounds.size.y;
      if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
        num += this.mPanel.clipSoftness.y * 2f;
      return Mathf.RoundToInt(num - this.mPanel.height) > 0;
    }
  }

  protected virtual bool shouldMove
  {
    get
    {
      if (!this.disableDragIfFits)
        return true;
      if ((Object) this.mPanel == (Object) null)
        this.mPanel = this.GetComponent<UIPanel>();
      Vector4 finalClipRegion = this.mPanel.finalClipRegion;
      Bounds bounds = this.bounds;
      float num1 = (double) finalClipRegion.z != 0.0 ? finalClipRegion.z * 0.5f : (float) Screen.width;
      float num2 = (double) finalClipRegion.w != 0.0 ? finalClipRegion.w * 0.5f : (float) Screen.height;
      return this.canMoveHorizontally && ((double) bounds.min.x < (double) finalClipRegion.x - (double) num1 || (double) bounds.max.x > (double) finalClipRegion.x + (double) num1) || this.canMoveVertically && ((double) bounds.min.y < (double) finalClipRegion.y - (double) num2 || (double) bounds.max.y > (double) finalClipRegion.y + (double) num2);
    }
  }

  public Vector3 currentMomentum
  {
    get
    {
      return this.mMomentum;
    }
    set
    {
      this.mMomentum = value;
      this.mShouldMove = true;
    }
  }

  private void Awake()
  {
    this.mTrans = this.transform;
    this.mPanel = this.GetComponent<UIPanel>();
    if (this.mPanel.clipping == UIDrawCall.Clipping.None)
      this.mPanel.clipping = UIDrawCall.Clipping.ConstrainButDontClip;
    if (this.movement != UIScrollView.Movement.Custom && (double) this.scale.sqrMagnitude > 1.0 / 1000.0)
    {
      if ((double) this.scale.x == 1.0 && (double) this.scale.y == 0.0)
        this.movement = UIScrollView.Movement.Horizontal;
      else if ((double) this.scale.x == 0.0 && (double) this.scale.y == 1.0)
        this.movement = UIScrollView.Movement.Vertical;
      else if ((double) this.scale.x == 1.0 && (double) this.scale.y == 1.0)
      {
        this.movement = UIScrollView.Movement.Unrestricted;
      }
      else
      {
        this.movement = UIScrollView.Movement.Custom;
        this.customMovement.x = this.scale.x;
        this.customMovement.y = this.scale.y;
      }
      this.scale = Vector3.zero;
    }
    if (this.contentPivot != UIWidget.Pivot.TopLeft || !(this.relativePositionOnReset != Vector2.zero))
      return;
    this.contentPivot = NGUIMath.GetPivot(new Vector2(this.relativePositionOnReset.x, 1f - this.relativePositionOnReset.y));
    this.relativePositionOnReset = Vector2.zero;
  }

  private void OnEnable()
  {
    UIScrollView.list.Add(this);
  }

  private void OnDisable()
  {
    UIScrollView.list.Remove(this);
  }

  protected virtual void Start()
  {
    if (!Application.isPlaying)
      return;
    if ((Object) this.horizontalScrollBar != (Object) null)
    {
      EventDelegate.Add(this.horizontalScrollBar.onChange, new EventDelegate.Callback(this.OnScrollBar));
      this.horizontalScrollBar.alpha = this.showScrollBars == UIScrollView.ShowCondition.Always || this.shouldMoveHorizontally ? 1f : 0.0f;
    }
    if (!((Object) this.verticalScrollBar != (Object) null))
      return;
    EventDelegate.Add(this.verticalScrollBar.onChange, new EventDelegate.Callback(this.OnScrollBar));
    this.verticalScrollBar.alpha = this.showScrollBars == UIScrollView.ShowCondition.Always || this.shouldMoveVertically ? 1f : 0.0f;
  }

  public bool RestrictWithinBounds(bool instant)
  {
    return this.RestrictWithinBounds(instant, true, true);
  }

  public bool RestrictWithinBounds(bool instant, bool horizontal, bool vertical)
  {
    Bounds bounds = this.bounds;
    Vector3 constrainOffset = this.mPanel.CalculateConstrainOffset((Vector2) bounds.min, (Vector2) bounds.max);
    if (!horizontal)
      constrainOffset.x = 0.0f;
    if (!vertical)
      constrainOffset.y = 0.0f;
    if ((double) constrainOffset.sqrMagnitude <= 0.100000001490116)
      return false;
    if (!instant && this.dragEffect == UIScrollView.DragEffect.MomentumAndSpring)
    {
      Vector3 pos = this.mTrans.localPosition + constrainOffset;
      pos.x = Mathf.Round(pos.x);
      pos.y = Mathf.Round(pos.y);
      SpringPanel.Begin(this.mPanel.gameObject, pos, 13f).strength = 8f;
    }
    else
    {
      this.MoveRelative(constrainOffset);
      if ((double) Mathf.Abs(constrainOffset.x) > 0.00999999977648258)
        this.mMomentum.x = 0.0f;
      if ((double) Mathf.Abs(constrainOffset.y) > 0.00999999977648258)
        this.mMomentum.y = 0.0f;
      if ((double) Mathf.Abs(constrainOffset.z) > 0.00999999977648258)
        this.mMomentum.z = 0.0f;
      this.mScroll = 0.0f;
    }
    return true;
  }

  public void DisableSpring()
  {
    SpringPanel component = this.GetComponent<SpringPanel>();
    if (!((Object) component != (Object) null))
      return;
    component.enabled = false;
  }

  public void UpdateScrollbars()
  {
    this.UpdateScrollbars(true);
  }

  public virtual void UpdateScrollbars(bool recalculateBounds)
  {
    if ((Object) this.mPanel == (Object) null)
      return;
    if ((Object) this.horizontalScrollBar != (Object) null || (Object) this.verticalScrollBar != (Object) null)
    {
      if (recalculateBounds)
      {
        this.mCalculatedBounds = false;
        this.mShouldMove = this.shouldMove;
      }
      Bounds bounds = this.bounds;
      Vector2 vector2_1 = (Vector2) bounds.min;
      Vector2 vector2_2 = (Vector2) bounds.max;
      if ((Object) this.horizontalScrollBar != (Object) null && (double) vector2_2.x > (double) vector2_1.x)
      {
        Vector4 finalClipRegion = this.mPanel.finalClipRegion;
        int @int = Mathf.RoundToInt(finalClipRegion.z);
        if ((@int & 1) != 0)
          --@int;
        float num1 = Mathf.Round((float) @int * 0.5f);
        if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
          num1 -= this.mPanel.clipSoftness.x;
        float contentSize = vector2_2.x - vector2_1.x;
        float viewSize = num1 * 2f;
        float num2 = vector2_1.x;
        float num3 = vector2_2.x;
        float num4 = finalClipRegion.x - num1;
        float num5 = finalClipRegion.x + num1;
        this.UpdateScrollbars(this.horizontalScrollBar, num4 - num2, num3 - num5, contentSize, viewSize, false);
      }
      if (!((Object) this.verticalScrollBar != (Object) null) || (double) vector2_2.y <= (double) vector2_1.y)
        return;
      Vector4 finalClipRegion1 = this.mPanel.finalClipRegion;
      int int1 = Mathf.RoundToInt(finalClipRegion1.w);
      if ((int1 & 1) != 0)
        --int1;
      float num6 = Mathf.Round((float) int1 * 0.5f);
      if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
        num6 -= this.mPanel.clipSoftness.y;
      float contentSize1 = vector2_2.y - vector2_1.y;
      float viewSize1 = num6 * 2f;
      float num7 = vector2_1.y;
      float num8 = vector2_2.y;
      float num9 = finalClipRegion1.y - num6;
      float num10 = finalClipRegion1.y + num6;
      this.UpdateScrollbars(this.verticalScrollBar, num9 - num7, num8 - num10, contentSize1, viewSize1, true);
    }
    else
    {
      if (!recalculateBounds)
        return;
      this.mCalculatedBounds = false;
    }
  }

  protected void UpdateScrollbars(UIProgressBar slider, float contentMin, float contentMax, float contentSize, float viewSize, bool inverted)
  {
    if ((Object) slider == (Object) null)
      return;
    this.mIgnoreCallbacks = true;
    float num;
    if ((double) viewSize < (double) contentSize)
    {
      contentMin = Mathf.Clamp01(contentMin / contentSize);
      contentMax = Mathf.Clamp01(contentMax / contentSize);
      num = contentMin + contentMax;
      slider.value = !inverted ? ((double) num <= 1.0 / 1000.0 ? 1f : contentMin / num) : ((double) num <= 1.0 / 1000.0 ? 0.0f : (float) (1.0 - (double) contentMin / (double) num));
    }
    else
    {
      contentMin = Mathf.Clamp01(-contentMin / contentSize);
      contentMax = Mathf.Clamp01(-contentMax / contentSize);
      num = contentMin + contentMax;
      slider.value = !inverted ? ((double) num <= 1.0 / 1000.0 ? 1f : contentMin / num) : ((double) num <= 1.0 / 1000.0 ? 0.0f : (float) (1.0 - (double) contentMin / (double) num));
      if ((double) contentSize > 0.0)
      {
        contentMin = Mathf.Clamp01(contentMin / contentSize);
        contentMax = Mathf.Clamp01(contentMax / contentSize);
        num = contentMin + contentMax;
      }
    }
    UIScrollBar uiScrollBar = slider as UIScrollBar;
    if ((Object) uiScrollBar != (Object) null)
      uiScrollBar.barSize = 1f - num;
    this.mIgnoreCallbacks = false;
  }

  public virtual void SetDragAmount(float x, float y, bool updateScrollbars)
  {
    if ((Object) this.mPanel == (Object) null)
      this.mPanel = this.GetComponent<UIPanel>();
    this.DisableSpring();
    Bounds bounds = this.bounds;
    if ((double) bounds.min.x == (double) bounds.max.x || (double) bounds.min.y == (double) bounds.max.y)
      return;
    Vector4 finalClipRegion = this.mPanel.finalClipRegion;
    float num1 = finalClipRegion.z * 0.5f;
    float num2 = finalClipRegion.w * 0.5f;
    float from1 = bounds.min.x + num1;
    float to1 = bounds.max.x - num1;
    float to2 = bounds.min.y + num2;
    float from2 = bounds.max.y - num2;
    if (this.mPanel.clipping == UIDrawCall.Clipping.SoftClip)
    {
      from1 -= this.mPanel.clipSoftness.x;
      to1 += this.mPanel.clipSoftness.x;
      to2 -= this.mPanel.clipSoftness.y;
      from2 += this.mPanel.clipSoftness.y;
    }
    float num3 = Mathf.Lerp(from1, to1, x);
    float num4 = Mathf.Lerp(from2, to2, y);
    if (!updateScrollbars)
    {
      Vector3 localPosition = this.mTrans.localPosition;
      if (this.canMoveHorizontally)
        localPosition.x += finalClipRegion.x - num3;
      if (this.canMoveVertically)
        localPosition.y += finalClipRegion.y - num4;
      this.mTrans.localPosition = localPosition;
    }
    if (this.canMoveHorizontally)
      finalClipRegion.x = num3;
    if (this.canMoveVertically)
      finalClipRegion.y = num4;
    Vector4 baseClipRegion = this.mPanel.baseClipRegion;
    this.mPanel.clipOffset = new Vector2(finalClipRegion.x - baseClipRegion.x, finalClipRegion.y - baseClipRegion.y);
    if (!updateScrollbars)
      return;
    this.UpdateScrollbars(this.mDragID == -10);
  }

  public void InvalidateBounds()
  {
    this.mCalculatedBounds = false;
  }

  [ContextMenu("Reset Clipping Position")]
  public void ResetPosition()
  {
    if (!NGUITools.GetActive((Behaviour) this))
      return;
    this.mCalculatedBounds = false;
    Vector2 pivotOffset = NGUIMath.GetPivotOffset(this.contentPivot);
    this.SetDragAmount(pivotOffset.x, 1f - pivotOffset.y, false);
    this.SetDragAmount(pivotOffset.x, 1f - pivotOffset.y, true);
  }

  public void UpdatePosition()
  {
    if (this.mIgnoreCallbacks || !((Object) this.horizontalScrollBar != (Object) null) && !((Object) this.verticalScrollBar != (Object) null))
      return;
    this.mIgnoreCallbacks = true;
    this.mCalculatedBounds = false;
    Vector2 pivotOffset = NGUIMath.GetPivotOffset(this.contentPivot);
    this.SetDragAmount(!((Object) this.horizontalScrollBar != (Object) null) ? pivotOffset.x : this.horizontalScrollBar.value, !((Object) this.verticalScrollBar != (Object) null) ? 1f - pivotOffset.y : this.verticalScrollBar.value, false);
    this.UpdateScrollbars(true);
    this.mIgnoreCallbacks = false;
  }

  public void OnScrollBar()
  {
    if (this.mIgnoreCallbacks)
      return;
    this.mIgnoreCallbacks = true;
    this.SetDragAmount(!((Object) this.horizontalScrollBar != (Object) null) ? 0.0f : this.horizontalScrollBar.value, !((Object) this.verticalScrollBar != (Object) null) ? 0.0f : this.verticalScrollBar.value, false);
    this.mIgnoreCallbacks = false;
  }

  public virtual void MoveRelative(Vector3 relative)
  {
    this.mTrans.localPosition += relative;
    Vector2 clipOffset = this.mPanel.clipOffset;
    clipOffset.x -= relative.x;
    clipOffset.y -= relative.y;
    this.mPanel.clipOffset = clipOffset;
    this.UpdateScrollbars(false);
  }

  public void MoveAbsolute(Vector3 absolute)
  {
    this.MoveRelative(this.mTrans.InverseTransformPoint(absolute) - this.mTrans.InverseTransformPoint(Vector3.zero));
  }

  public void Press(bool pressed)
  {
    if (UICamera.currentScheme == UICamera.ControlScheme.Controller)
      return;
    if (this.smoothDragStart && pressed)
    {
      this.mDragStarted = false;
      this.mDragStartOffset = Vector2.zero;
    }
    if (!this.enabled || !NGUITools.GetActive(this.gameObject))
      return;
    if (!pressed && this.mDragID == UICamera.currentTouchID)
      this.mDragID = -10;
    this.mCalculatedBounds = false;
    this.mShouldMove = this.shouldMove;
    if (!this.mShouldMove)
      return;
    this.mPressed = pressed;
    if (pressed)
    {
      this.mMomentum = Vector3.zero;
      this.mScroll = 0.0f;
      this.DisableSpring();
      this.mLastPos = UICamera.lastWorldPosition;
      this.mPlane = new Plane(this.mTrans.rotation * Vector3.back, this.mLastPos);
      Vector2 clipOffset = this.mPanel.clipOffset;
      clipOffset.x = Mathf.Round(clipOffset.x);
      clipOffset.y = Mathf.Round(clipOffset.y);
      this.mPanel.clipOffset = clipOffset;
      Vector3 localPosition = this.mTrans.localPosition;
      localPosition.x = Mathf.Round(localPosition.x);
      localPosition.y = Mathf.Round(localPosition.y);
      this.mTrans.localPosition = localPosition;
      if (this.smoothDragStart)
        return;
      this.mDragStarted = true;
      this.mDragStartOffset = Vector2.zero;
      if (this.onDragStarted == null)
        return;
      this.onDragStarted();
    }
    else
    {
      if (this.restrictWithinPanel && this.mPanel.clipping != UIDrawCall.Clipping.None)
        this.RestrictWithinBounds(this.dragEffect == UIScrollView.DragEffect.None, this.canMoveHorizontally, this.canMoveVertically);
      if (this.onDragFinished != null)
        this.onDragFinished();
      if (this.mShouldMove || this.onStoppedMoving == null)
        return;
      this.onStoppedMoving();
    }
  }

  public void Drag()
  {
    if (UICamera.currentScheme == UICamera.ControlScheme.Controller || !this.enabled || (!NGUITools.GetActive(this.gameObject) || !this.mShouldMove))
      return;
    if (this.mDragID == -10)
      this.mDragID = UICamera.currentTouchID;
    UICamera.currentTouch.clickNotification = UICamera.ClickNotification.BasedOnDelta;
    if (this.smoothDragStart && !this.mDragStarted)
    {
      this.mDragStarted = true;
      this.mDragStartOffset = UICamera.currentTouch.totalDelta;
      if (this.onDragStarted != null)
        this.onDragStarted();
    }
    Ray ray = !this.smoothDragStart ? UICamera.currentCamera.ScreenPointToRay((Vector3) UICamera.currentTouch.pos) : UICamera.currentCamera.ScreenPointToRay((Vector3) (UICamera.currentTouch.pos - this.mDragStartOffset));
    float enter = 0.0f;
    if (!this.mPlane.Raycast(ray, out enter))
      return;
    Vector3 point = ray.GetPoint(enter);
    Vector3 vector3 = point - this.mLastPos;
    this.mLastPos = point;
    if ((double) vector3.x != 0.0 || (double) vector3.y != 0.0 || (double) vector3.z != 0.0)
    {
      vector3 = this.mTrans.InverseTransformDirection(vector3);
      if (this.movement == UIScrollView.Movement.Horizontal)
      {
        vector3.y = 0.0f;
        vector3.z = 0.0f;
      }
      else if (this.movement == UIScrollView.Movement.Vertical)
      {
        vector3.x = 0.0f;
        vector3.z = 0.0f;
      }
      else if (this.movement == UIScrollView.Movement.Unrestricted)
        vector3.z = 0.0f;
      else
        vector3.Scale((Vector3) this.customMovement);
      vector3 = this.mTrans.TransformDirection(vector3);
    }
    this.mMomentum = this.dragEffect != UIScrollView.DragEffect.None ? Vector3.Lerp(this.mMomentum, this.mMomentum + vector3 * (0.01f * this.momentumAmount), 0.67f) : Vector3.zero;
    if (!this.iOSDragEmulation || this.dragEffect != UIScrollView.DragEffect.MomentumAndSpring)
      this.MoveAbsolute(vector3);
    else if ((double) this.mPanel.CalculateConstrainOffset((Vector2) this.bounds.min, (Vector2) this.bounds.max).magnitude > 1.0)
    {
      this.MoveAbsolute(vector3 * 0.5f);
      this.mMomentum *= 0.5f;
    }
    else
      this.MoveAbsolute(vector3);
    if (!this.restrictWithinPanel || this.mPanel.clipping == UIDrawCall.Clipping.None || this.dragEffect == UIScrollView.DragEffect.MomentumAndSpring)
      return;
    this.RestrictWithinBounds(true, this.canMoveHorizontally, this.canMoveVertically);
  }

  public void Scroll(float delta)
  {
    if (!this.enabled || !NGUITools.GetActive(this.gameObject) || (double) this.scrollWheelFactor == 0.0)
      return;
    this.DisableSpring();
    this.mShouldMove |= this.shouldMove;
    if ((double) Mathf.Sign(this.mScroll) != (double) Mathf.Sign(delta))
      this.mScroll = 0.0f;
    this.mScroll += delta * this.scrollWheelFactor;
  }

  private void LateUpdate()
  {
    if (!Application.isPlaying)
      return;
    float deltaTime = RealTime.deltaTime;
    if (this.showScrollBars != UIScrollView.ShowCondition.Always && ((bool) ((Object) this.verticalScrollBar) || (bool) ((Object) this.horizontalScrollBar)))
    {
      bool flag1 = false;
      bool flag2 = false;
      if (this.showScrollBars != UIScrollView.ShowCondition.WhenDragging || this.mDragID != -10 || (double) this.mMomentum.magnitude > 0.00999999977648258)
      {
        flag1 = this.shouldMoveVertically;
        flag2 = this.shouldMoveHorizontally;
      }
      if ((bool) ((Object) this.verticalScrollBar))
      {
        float num = Mathf.Clamp01(this.verticalScrollBar.alpha + (!flag1 ? (float) (-(double) deltaTime * 3.0) : deltaTime * 6f));
        if ((double) this.verticalScrollBar.alpha != (double) num)
          this.verticalScrollBar.alpha = num;
      }
      if ((bool) ((Object) this.horizontalScrollBar))
      {
        float num = Mathf.Clamp01(this.horizontalScrollBar.alpha + (!flag2 ? (float) (-(double) deltaTime * 3.0) : deltaTime * 6f));
        if ((double) this.horizontalScrollBar.alpha != (double) num)
          this.horizontalScrollBar.alpha = num;
      }
    }
    if (!this.mShouldMove)
      return;
    if (!this.mPressed)
    {
      if ((double) this.mMomentum.magnitude > 9.99999974737875E-05 || (double) this.mScroll != 0.0)
      {
        if (this.movement == UIScrollView.Movement.Horizontal)
          this.mMomentum -= this.mTrans.TransformDirection(new Vector3(this.mScroll * 0.05f, 0.0f, 0.0f));
        else if (this.movement == UIScrollView.Movement.Vertical)
          this.mMomentum -= this.mTrans.TransformDirection(new Vector3(0.0f, this.mScroll * 0.05f, 0.0f));
        else if (this.movement == UIScrollView.Movement.Unrestricted)
          this.mMomentum -= this.mTrans.TransformDirection(new Vector3(this.mScroll * 0.05f, this.mScroll * 0.05f, 0.0f));
        else
          this.mMomentum -= this.mTrans.TransformDirection(new Vector3((float) ((double) this.mScroll * (double) this.customMovement.x * 0.0500000007450581), (float) ((double) this.mScroll * (double) this.customMovement.y * 0.0500000007450581), 0.0f));
        this.mScroll = NGUIMath.SpringLerp(this.mScroll, 0.0f, 20f, deltaTime);
        this.MoveAbsolute(NGUIMath.SpringDampen(ref this.mMomentum, 9f, deltaTime));
        if (this.restrictWithinPanel && this.mPanel.clipping != UIDrawCall.Clipping.None)
        {
          if (NGUITools.GetActive((Behaviour) this.centerOnChild))
            this.centerOnChild.Recenter();
          else
            this.RestrictWithinBounds(false, this.canMoveHorizontally, this.canMoveVertically);
        }
        if (this.onMomentumMove == null)
          return;
        this.onMomentumMove();
      }
      else
      {
        this.mScroll = 0.0f;
        this.mMomentum = Vector3.zero;
        SpringPanel component = this.GetComponent<SpringPanel>();
        if ((Object) component != (Object) null && component.enabled)
          return;
        this.mShouldMove = false;
        if (this.onStoppedMoving == null)
          return;
        this.onStoppedMoving();
      }
    }
    else
    {
      this.mScroll = 0.0f;
      NGUIMath.SpringDampen(ref this.mMomentum, 9f, deltaTime);
    }
  }

  public enum Movement
  {
    Horizontal,
    Vertical,
    Unrestricted,
    Custom,
  }

  public enum DragEffect
  {
    None,
    Momentum,
    MomentumAndSpring,
  }

  public enum ShowCondition
  {
    Always,
    OnlyIfNeeded,
    WhenDragging,
  }

  public delegate void OnDragNotification();
}
