﻿// Decompiled with JetBrains decompiler
// Type: BgoProtocolWriter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Text;
using UnityEngine;

public class BgoProtocolWriter : BinaryWriter
{
  private MemoryStream memoryStream;

  public BgoProtocolWriter()
    : base((Stream) new MemoryStream())
  {
    this.memoryStream = (MemoryStream) this.BaseStream;
    this.Write((ushort) 0);
  }

  private void WriteDataLength(byte[] data)
  {
    ushort num = (ushort) (this.GetLength() - 2);
    data[0] = (byte) ((int) num >> 8 & (int) byte.MaxValue);
    data[1] = (byte) ((uint) num & (uint) byte.MaxValue);
  }

  public override void Write(string value)
  {
    byte[] bytes = Encoding.UTF8.GetBytes(value);
    this.Write((ushort) bytes.Length);
    if (bytes.Length <= 0)
      return;
    this.Write(bytes, 0, bytes.Length);
  }

  public void Write(Vector2 value)
  {
    this.Write(value.x);
    this.Write(value.y);
  }

  public void Write(Vector3 value)
  {
    this.Write(value.x);
    this.Write(value.y);
    this.Write(value.z);
  }

  public void Write(Euler3 value)
  {
    this.Write(value.pitch);
    this.Write(value.yaw);
    this.Write(value.roll);
  }

  public void Write(Color value)
  {
    this.Write((byte) ((double) value.r * (double) byte.MaxValue));
    this.Write((byte) ((double) value.g * (double) byte.MaxValue));
    this.Write((byte) ((double) value.b * (double) byte.MaxValue));
    this.Write((byte) ((double) value.a * (double) byte.MaxValue));
  }

  public void Write(IProtocolWrite desc)
  {
    desc.Write(this);
  }

  public byte[] GetBuffer()
  {
    byte[] buffer = this.memoryStream.GetBuffer();
    this.WriteDataLength(buffer);
    return buffer;
  }

  public int GetLength()
  {
    return (int) this.memoryStream.Length;
  }
}
