﻿// Decompiled with JetBrains decompiler
// Type: CutscenePlayRequestData
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class CutscenePlayRequestData
{
  private readonly CutsceneId cutsceneIdToPlay;
  private readonly CutsceneManager.OnFinishCutsceneDelegate onFinishDelegate;
  private readonly SpaceObject targetObject;

  public CutsceneId CutsceneIdToPlay
  {
    get
    {
      return this.cutsceneIdToPlay;
    }
  }

  public CutsceneManager.OnFinishCutsceneDelegate OnFinishDelegate
  {
    get
    {
      return this.onFinishDelegate;
    }
  }

  public SpaceObject TargetObject
  {
    get
    {
      return this.targetObject;
    }
  }

  public CutscenePlayRequestData(CutsceneId cutsceneId, CutsceneManager.OnFinishCutsceneDelegate onFinishDelegate, SpaceObject targetObject)
  {
    this.cutsceneIdToPlay = cutsceneId;
    this.onFinishDelegate = onFinishDelegate;
    this.targetObject = targetObject;
  }

  public override string ToString()
  {
    return string.Format("CutsceneIdToPlay: {0}, OnFinishDelegate: {1}, TargetObject: {2}", (object) this.cutsceneIdToPlay, (object) this.onFinishDelegate, (object) this.targetObject);
  }
}
