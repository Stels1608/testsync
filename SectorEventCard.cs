﻿// Decompiled with JetBrains decompiler
// Type: SectorEventCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SectorEventCard : Card
{
  private const string NORMAL_ICON_PATH = "uGUI_Resources/Images/DynamicEvents/icon_event_normal";
  private const string NORMAL_ICON_SPRITENAME_NGUI = "HudIndicator_SectorEvent_Normal";

  private string NameCylon { get; set; }

  private string NameColonial { get; set; }

  public string DescriptionCylon { get; private set; }

  public string DescriptionColonial { get; private set; }

  public SectorEventTaskType TaskType { get; private set; }

  public bool IsElite { get; private set; }

  public float Radius { get; private set; }

  public string Name
  {
    get
    {
      if (Game.Me.Faction == Faction.Cylon)
        return this.NameCylon;
      if (Game.Me.Faction == Faction.Colonial)
        return this.NameColonial;
      return "<no name for" + (object) Game.Me.Faction + ">";
    }
  }

  public Texture2D Icon
  {
    get
    {
      return ResourceLoader.Load<Texture2D>("uGUI_Resources/Images/DynamicEvents/icon_event_normal");
    }
  }

  public string IconNguiHudIndicator
  {
    get
    {
      return "HudIndicator_SectorEvent_Normal";
    }
  }

  public SectorEventCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.NameCylon = r.ReadString();
    this.NameColonial = r.ReadString();
    this.DescriptionCylon = r.ReadString();
    this.DescriptionColonial = r.ReadString();
    this.TaskType = (SectorEventTaskType) r.ReadByte();
    this.IsElite = r.ReadBoolean();
    this.Radius = r.ReadSingle();
    this.IsLoaded.Set();
  }
}
