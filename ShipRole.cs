﻿// Decompiled with JetBrains decompiler
// Type: ShipRole
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum ShipRole : byte
{
  Fighter = 1,
  Bomber = 2,
  Command = 3,
  ElectronicWarfare = 4,
  Engineer = 5,
  Interceptor = 6,
  Gunship = 7,
  Picket = 8,
  Destroyer = 9,
  Artillery = 10,
  Assault = 11,
  Stealth = 12,
}
