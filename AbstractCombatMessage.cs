﻿// Decompiled with JetBrains decompiler
// Type: AbstractCombatMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public abstract class AbstractCombatMessage : IDisposable
{
  private readonly Transform parent;
  private readonly float lifetime;

  public float Lifetime
  {
    get
    {
      return this.lifetime;
    }
  }

  public Transform Parent
  {
    get
    {
      return this.parent;
    }
  }

  public bool Expired
  {
    get
    {
      return (double) this.RemainingTime <= 0.0;
    }
  }

  public float RemainingTime { get; private set; }

  public AbstractCombatMessage(Transform parent, float lifetime)
  {
    this.parent = parent;
    float num = lifetime;
    this.RemainingTime = num;
    this.lifetime = num;
  }

  public void Update(float deltaTime)
  {
    this.RemainingTime -= deltaTime;
    this.Reposition(deltaTime);
    this.OnUpdate(deltaTime);
  }

  protected virtual void OnUpdate(float deltaTime)
  {
  }

  public abstract void Reposition(float deltaTime);

  public abstract void Dispose();
}
