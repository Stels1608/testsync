﻿// Decompiled with JetBrains decompiler
// Type: DebugServer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugServer : DebugBehaviour<DebugServer>
{
  private Vector2 scroll = new Vector2();

  private void Start()
  {
    this.windowID = 15;
    this.SetSize(200f, 300f);
  }

  protected override void WindowFunc()
  {
    GUILayout.Label("Vital danger! Only for expert");
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.Label("Use in sector!");
    if (GUILayout.Button("God Mode " + (!DebugUtility.HasAccess(BgoAdminRoles.GodMode) ? "Off" : "On")))
    {
      if (DebugUtility.HasAccess(BgoAdminRoles.GodMode))
        DebugProtocol.GetProtocol().Command("god_mode", "false");
      else
        DebugProtocol.GetProtocol().Command("god_mode", "true");
      DebugBehaviour<DebugServer>.Close();
    }
    if (GUILayout.Button("Suicide"))
    {
      DebugProtocol.GetProtocol().Command("died");
      DebugBehaviour<DebugServer>.Close();
    }
    if (GUILayout.Button("Spawn missile to target"))
      DebugProtocol.GetProtocol().Command("spawn_missile");
    if (GUILayout.Button("Spawn flare"))
      DebugProtocol.GetProtocol().Command("spawn_flare");
    if (GUILayout.Button("Spawn mine"))
      DebugProtocol.GetProtocol().Command("spawn_mine");
    if (GUILayout.Button("Kill target"))
      DebugProtocol.GetProtocol().Command("kill_target");
    if (GUILayout.Button("Loot Target"))
      DebugProtocol.GetProtocol().Command("loot_target");
    if (GUILayout.Button("Loot Target (x10)"))
      DebugProtocol.GetProtocol().Command("loot_target_x10");
    if (GUILayout.Button("Self Buff HP"))
      DebugProtocol.GetProtocol().Command("self_buff", "hp");
    if (GUILayout.Button("Self Buff HEAL"))
      DebugProtocol.GetProtocol().Command("self_buff", "heal");
    if (GUILayout.Button("Self Buff DMG"))
      DebugProtocol.GetProtocol().Command("self_buff", "dmg");
    if (GUILayout.Button("Self Buff SPEED"))
      DebugProtocol.GetProtocol().Command("self_buff", "speed");
    if (GUILayout.Button("Self Buff RANGE"))
      DebugProtocol.GetProtocol().Command("self_buff", "range");
    if (GUILayout.Button("Self Buff COOLDOWN"))
      DebugProtocol.GetProtocol().Command("self_buff", "cooldown");
    if (GUILayout.Button("dispell"))
      DebugProtocol.GetProtocol().Command("self_buff", "dispell");
    if (GUILayout.Button("U.B.E.R."))
    {
      DebugProtocol.GetProtocol().Command("self_buff", "uber");
      Game.Me.Stats.SetObfuscatedFloat(ObjectStat.DetectionVisualRadius, 10000f);
    }
    if (GUILayout.Button("Short Circuit Me"))
      DebugProtocol.GetProtocol().Command("short_circuit", "me");
    if (GUILayout.Button("Short Circuit Target"))
      DebugProtocol.GetProtocol().Command("short_circuit", "target");
    if (GUILayout.Button("Print my stats remotly"))
      DebugProtocol.GetProtocol().Command("stats_debug_dump");
    if (GUILayout.Button("Spawn Comet"))
      DebugProtocol.GetProtocol().Command("spawn_comet");
    if (GUILayout.Button("SpaceDetach on dead"))
      DebugProtocol.GetProtocol().Command("detach", "death");
    if (GUILayout.Button("SpaceDetach on jumped"))
      DebugProtocol.GetProtocol().Command("detach", "jumped");
    if (GUILayout.Button("SpaceReattach"))
      DebugProtocol.GetProtocol().Command("detach", "reset");
    GUILayout.Label("Use in sector!");
    if (GUILayout.Button("Inc ColOP"))
      DebugProtocol.GetProtocol().Command("sector_op", "colonial", "100");
    if (GUILayout.Button("Dec ColOP"))
      DebugProtocol.GetProtocol().Command("sector_op", "colonial", "-100");
    if (GUILayout.Button("Inc CylOP"))
      DebugProtocol.GetProtocol().Command("sector_op", "cylon", "100");
    if (GUILayout.Button("Dec CylOP"))
      DebugProtocol.GetProtocol().Command("sector_op", "cylon", "-100");
    GUILayout.Label("Use with caution!");
    if (GUILayout.Button("Player save & crash"))
    {
      DebugProtocol.GetProtocol().Command("save_and_crash_player");
      DebugBehaviour<DebugServer>.Close();
    }
    if (GUILayout.Button("Player reset"))
    {
      DebugProtocol.GetProtocol().Command("reset_player");
      DebugBehaviour<DebugServer>.Close();
    }
    if (GUILayout.Button("Change faction (when in room)"))
    {
      DebugProtocol.GetProtocol().Command("change_faction");
      DebugBehaviour<DebugServer>.Close();
    }
    GUILayout.Label("Vital danger!");
    if (GUILayout.Button("Kill em all!"))
      DebugProtocol.GetProtocol().Command("kill_em_all");
    if (GUILayout.Button("Reset mobs in my sector"))
    {
      DebugProtocol.GetProtocol().Command("reset_mobs");
      DebugBehaviour<DebugServer>.Close();
    }
    GUILayout.Label("Vital danger!\nWill disconnect you :)");
    if (GUILayout.Button("Restart my sector"))
    {
      DebugProtocol.GetProtocol().Command("restart_sector");
      DebugBehaviour<DebugServer>.Close();
    }
    GUILayout.Label("Vital danger!\nNever press it :)");
    if (GUILayout.Button("Restart server"))
    {
      DebugProtocol.GetProtocol().Command("restart");
      DebugBehaviour<DebugServer>.Close();
    }
    GUILayout.EndScrollView();
  }
}
