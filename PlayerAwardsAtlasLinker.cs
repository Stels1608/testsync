﻿// Decompiled with JetBrains decompiler
// Type: PlayerAwardsAtlasLinker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAwardsAtlasLinker
{
  private static readonly PlayerAwardsAtlasLinker instance = new PlayerAwardsAtlasLinker();
  public string GetTournamentNemesis = "Award_Tournament_Nemesis";
  private const string ATLAS_PATH = "GUI/gui_2013/atlas/PlayerAwardsAtlas/PlayerAwardsAtlas";
  private readonly UIAtlas uiAtlas;

  public static PlayerAwardsAtlasLinker Instance
  {
    get
    {
      return PlayerAwardsAtlasLinker.instance;
    }
  }

  public UIAtlas Atlas
  {
    get
    {
      return this.uiAtlas;
    }
  }

  private PlayerAwardsAtlasLinker()
  {
    GameObject gameObject = Resources.Load("GUI/gui_2013/atlas/PlayerAwardsAtlas/PlayerAwardsAtlas", typeof (GameObject)) as GameObject;
    if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      Debug.LogError((object) "Atlas not found: GUI/gui_2013/atlas/PlayerAwardsAtlas/PlayerAwardsAtlas");
    this.uiAtlas = gameObject.GetComponent<UIAtlas>();
    this.TestAtlas();
  }

  private string FactionString(Faction faction)
  {
    return faction == Faction.Cylon ? "_Cylon" : "_Colonial";
  }

  public string GetPvpMedal(PvpMedal pvpMedal, Faction faction)
  {
    switch (pvpMedal)
    {
      case PvpMedal.PvpArena1st:
        return "Award_PvpArena_1st" + this.FactionString(faction);
      case PvpMedal.PvpArena2nd:
        return "Award_PvpArena_2nd" + this.FactionString(faction);
      case PvpMedal.PvpArena3rd:
        return "Award_PvpArena_3rd" + this.FactionString(faction);
      case PvpMedal.PvpArena4thTo20th:
        return "Award_PvpArena_4th-20th" + this.FactionString(faction);
      case PvpMedal.PvpArena21stTo100th:
        return "Award_PvpArena_21st-100th" + this.FactionString(faction);
      default:
        return string.Empty;
    }
  }

  public string GetTournamentMedal(TournamentMedal tournamentMedal)
  {
    switch (tournamentMedal)
    {
      case TournamentMedal.GoldMedal:
        return "Award_Tournament_GoldMedal";
      case TournamentMedal.SilverMedal:
        return "Award_Tournament_SilverMedal";
      default:
        return string.Empty;
    }
  }

  public string GetTournamentIndicator(TournamentIndicator tournamentIndicator)
  {
    switch (tournamentIndicator)
    {
      case TournamentIndicator.TopFive:
        return "Award_Tournament_Swords";
      case TournamentIndicator.KillingSpree:
        return "Award_Tournament_Ace";
      default:
        return string.Empty;
    }
  }

  public void TestAtlas()
  {
    List<string> stringList = new List<string>();
    stringList.Add(this.GetTournamentMedal(TournamentMedal.GoldMedal));
    stringList.Add(this.GetTournamentMedal(TournamentMedal.SilverMedal));
    stringList.Add(this.GetTournamentIndicator(TournamentIndicator.KillingSpree));
    stringList.Add(this.GetTournamentIndicator(TournamentIndicator.TopFive));
    stringList.Add(this.GetTournamentNemesis);
    foreach (int num in Enum.GetValues(typeof (Faction)))
    {
      Faction faction = (Faction) num;
      switch (faction)
      {
        case Faction.Colonial:
        case Faction.Cylon:
          stringList.Add(this.GetPvpMedal(PvpMedal.PvpArena1st, faction));
          stringList.Add(this.GetPvpMedal(PvpMedal.PvpArena2nd, faction));
          stringList.Add(this.GetPvpMedal(PvpMedal.PvpArena3rd, faction));
          stringList.Add(this.GetPvpMedal(PvpMedal.PvpArena4thTo20th, faction));
          stringList.Add(this.GetPvpMedal(PvpMedal.PvpArena21stTo100th, faction));
          continue;
        default:
          continue;
      }
    }
    using (List<string>.Enumerator enumerator = stringList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        if (this.Atlas.GetSprite(current) == null)
          Debug.LogError((object) ("HudIndicatorsAtlasLinker: Can't load defined sprite " + current + " from Atlas GUI/gui_2013/atlas/PlayerAwardsAtlas/PlayerAwardsAtlas"));
      }
    }
  }
}
