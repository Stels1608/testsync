﻿// Decompiled with JetBrains decompiler
// Type: CutsceneShipSpawner
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CutsceneShipSpawner : MonoBehaviour
{
  public bool SpawnWithJumpIn = true;
  public bool PlayJumpInSound = true;
  public ShipType PrefabToSpawn;
  public JumpEffectNew.JumpEffectSize JumpEffectSize;
  private bool hasSpawned;

  public void OnEnable()
  {
    if (this.hasSpawned)
      return;
    this.hasSpawned = true;
    CutsceneAssetSpawnerRemote.SpawnAssetAtLocator(PrefabNames.GetShipPrefabName(this.PrefabToSpawn), this.gameObject, this.SpawnWithJumpIn, this.PlayJumpInSound, this.JumpEffectSize);
  }
}
