﻿// Decompiled with JetBrains decompiler
// Type: ItemOfferSchedule
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

internal class ItemOfferSchedule : AbstractOfferSchedule
{
  private ItemOfferSchedule(List<IShopOffer> list)
    : base(list, "bgo.notif.active_sale")
  {
  }

  public static ItemOfferSchedule Decode(BgoProtocolReader input)
  {
    List<IShopOffer> list = new List<IShopOffer>();
    for (uint index = (uint) input.ReadUInt16(); index > 0U; --index)
    {
      ItemShopOffer itemShopOffer = (ItemShopOffer) null;
      try
      {
        itemShopOffer = ItemShopOffer.Decode(input);
      }
      catch (ArgumentException ex)
      {
        Log.Warning((object) ("I failed to decode a shop offer; ignoring. Details: " + ex.Message));
      }
      list.Add((IShopOffer) itemShopOffer);
    }
    return new ItemOfferSchedule(list);
  }

  public ShopDiscount FindDiscount(uint guid)
  {
    using (List<IShopOffer>.Enumerator enumerator = this.myList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShopDiscount discount = ((ItemShopOffer) enumerator.Current).FindDiscount(guid);
        if (discount != null)
          return discount;
      }
    }
    return (ShopDiscount) null;
  }
}
