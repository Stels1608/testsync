﻿// Decompiled with JetBrains decompiler
// Type: TargetSelectionChangeAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;

public class TargetSelectionChangeAction : Action<Message>
{
  private TargetSelectionDataProvider selectionDataProvider;

  public override void Execute(IMessage<Message> message)
  {
    this.selectionDataProvider = (TargetSelectionDataProvider) this.OwnerFacade.FetchDataProvider("TargetSelectionProvider");
    SpaceObject newTarget = message.Data as SpaceObject;
    ISpaceEntity key = (ISpaceEntity) this.selectionDataProvider.GetMainTarget();
    if (!this.selectionDataProvider.TargettingEnabled)
    {
      FacadeFactory.GetInstance().SendMessage(Message.SelectedTargetChanged, (object) new KeyValuePair<ISpaceEntity, ISpaceEntity>(key, (ISpaceEntity) null));
    }
    else
    {
      if (!this.TrySetPlayerTarget(newTarget))
        return;
      FacadeFactory.GetInstance().SendMessage(Message.SelectedTargetChanged, (object) new KeyValuePair<ISpaceEntity, ISpaceEntity>(key, (ISpaceEntity) newTarget));
    }
  }

  private bool TrySetPlayerTarget(SpaceObject newTarget)
  {
    if (newTarget != null && (!newTarget.IsTargetable || !newTarget.SpawnedInSector) || newTarget is Planet && !(newTarget as Planet).CanDock(true))
      return false;
    PlayerShip playerShip = SpaceLevel.GetLevel().PlayerShip;
    if (newTarget != null && newTarget.IsMe || playerShip != null && playerShip.CanDock(true) || newTarget != null && newTarget == playerShip)
      return false;
    SpaceObject mainTarget = this.selectionDataProvider.GetMainTarget();
    if (mainTarget != newTarget)
    {
      if (mainTarget != null && !mainTarget.AutoSubscribe)
        mainTarget.UnSubscribe();
      this.selectionDataProvider.SetMainTarget(newTarget);
      if (newTarget != null)
      {
        if (!newTarget.AutoSubscribe)
          newTarget.Subscribe();
        GameProtocol.GetProtocol().SetTarget(newTarget.ObjectID);
        SpaceLevel.GetLevel().sectorSfx.PlayTargetLockSound();
      }
      else
        GameProtocol.GetProtocol().SetTarget(0U);
      this.SetAbilityTarget(newTarget);
    }
    return true;
  }

  private void SetAbilityTarget(SpaceObject target)
  {
    foreach (ShipSlot slot in Game.Me.ActiveShip.Slots)
    {
      if (slot.Ability != null)
      {
        if (target != null && (!target.WorldCard.Targetable || !target.SpawnedInSector || target.Dead))
          slot.Ability.SetValidationTarget((SpaceObject) null);
        else
          slot.Ability.SetValidationTarget(target);
      }
    }
  }
}
