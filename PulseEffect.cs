﻿// Decompiled with JetBrains decompiler
// Type: PulseEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PulseEffect : MonoBehaviour
{
  public float Progress;
  public float FinalSphereRadius;
  public float AngularVelocity;
  public PulseEffect.AxisMode Mode;
  private Transform _transform;
  private Renderer _renderer;
  private Material _material;
  private Vector3 revolvingAxis;

  private void Start()
  {
    this._transform = this.transform;
    this._renderer = this.GetComponent<Renderer>();
    this._material = this._renderer.material;
    switch (this.Mode)
    {
      case PulseEffect.AxisMode.Random:
        this.revolvingAxis = Random.onUnitSphere;
        break;
      case PulseEffect.AxisMode.Camera:
        this.revolvingAxis = Camera.main.transform.position - this.transform.position;
        break;
    }
  }

  private void Update()
  {
    this.Progress = Mathf.Clamp01(this.Progress);
    this._transform.localScale = Vector3.one * this.FinalSphereRadius * this.Progress;
    this.transform.Rotate(this.revolvingAxis, this.AngularVelocity * Time.deltaTime, Space.Self);
    Color color = this.GetComponent<Renderer>().material.GetColor("_TintColor");
    color.a = 1f - this.Progress;
    this._material.SetColor("_TintColor", color);
  }

  public enum AxisMode
  {
    Random,
    Camera,
  }
}
