﻿// Decompiled with JetBrains decompiler
// Type: ArenaProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Arena;
using System;

public class ArenaProtocol : BgoProtocol
{
  public AnonymousDelegate CloseArenaInvite;

  public ArenaProtocol()
    : base(BgoProtocol.ProtocolID.Arena)
  {
  }

  public static ArenaProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Arena) as ArenaProtocol;
  }

  public void RequestArenaCheckIn(ArenaType type)
  {
    BgoProtocolWriter bw = this.NewMessage();
    switch (type)
    {
      case ArenaType.Arena1vs1:
        bw.Write((ushort) 1);
        break;
      case ArenaType.Arena3vs3:
        bw.Write((ushort) 2);
        break;
      case ArenaType.Arena3vs3Mixed:
        bw.Write((ushort) 3);
        break;
      case ArenaType.Duel1vs1:
        bw.Write((ushort) 4);
        break;
      default:
        throw new Exception("Type " + (object) type + " is not supported");
    }
    this.SendMessage(bw);
    if (!global::Arena.Debug)
      return;
    Log.Add("RequestArenaCheckIn " + (object) type);
  }

  public void RequestArenaDuelCheckIn(uint pid)
  {
    if (Game.Me.Ship.ShipCardLight.ShipRoleDeprecated == ShipRoleDeprecated.Carrier)
    {
      FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.arena.error_capitalship2%", NotificationCategory.Neutral));
    }
    else
    {
      BgoProtocolWriter bw = this.NewMessage();
      bw.Write((ushort) 4);
      bw.Write(pid);
      this.SendMessage(bw);
      if (!global::Arena.Debug)
        return;
      Log.Add("RequestDuelCheckIn " + (object) pid);
    }
  }

  public void RequestArenaCancelCheckIn()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 5);
    this.SendMessage(bw);
    if (!global::Arena.Debug)
      return;
    Log.Add("RequestArenaCancelCheckIn");
  }

  public void RequestArenaInviteOk()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 6);
    this.SendMessage(bw);
    if (!global::Arena.Debug)
      return;
    Log.Add("RequestArenaInviteOk");
  }

  public void RequestArenaInviteCancel()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 7);
    this.SendMessage(bw);
    if (!global::Arena.Debug)
      return;
    Log.Add("RequestArenaInviteCancel");
  }

  public void ArenaClose()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 8);
    this.SendMessage(bw);
    if (!global::Arena.Debug)
      return;
    Log.Add("ArenaClose");
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    ArenaProtocol.Reply reply = (ArenaProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case ArenaProtocol.Reply.Arena1vs1CheckedIn:
        if (global::Arena.Debug)
          Log.Add("ReplyArenaCheckedIn");
        Game.Me.Arena.State = ArenaState.Waiting;
        Game.Me.Arena.ArenaType = ArenaType.Arena1vs1;
        Game.Me.Arena.TimeWaitingBegin = Game.TimeSync.ServerDateTime;
        ArenaManager.Changed();
        break;
      case ArenaProtocol.Reply.Arena3vs3MixedCheckedIn:
        if (global::Arena.Debug)
          Log.Add("Arena3vs3MixedCheckedIn");
        Game.Me.Arena.State = ArenaState.Waiting;
        Game.Me.Arena.ArenaType = ArenaType.Arena3vs3;
        Game.Me.Arena.TimeWaitingBegin = Game.TimeSync.ServerDateTime;
        ArenaManager.Changed();
        break;
      case ArenaProtocol.Reply.Arena3vs3MixedRandomCheckedIn:
        if (global::Arena.Debug)
          Log.Add("Arena3vs3MixedRandomCheckedIn");
        Game.Me.Arena.State = ArenaState.Waiting;
        Game.Me.Arena.ArenaType = ArenaType.Arena3vs3Mixed;
        Game.Me.Arena.TimeWaitingBegin = Game.TimeSync.ServerDateTime;
        ArenaManager.Changed();
        break;
      case ArenaProtocol.Reply.ArenaDuelCheckedIn:
        Game.Me.Arena.State = ArenaState.Waiting;
        Game.Me.Arena.ArenaType = ArenaType.Duel1vs1;
        Game.Me.Arena.TimeWaitingBegin = Game.TimeSync.ServerDateTime;
        ArenaManager.Changed();
        break;
      case ArenaProtocol.Reply.ArenaDuelInviteSlave:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        ArenaProtocol.\u003CParseMessage\u003Ec__AnonStoreyDE messageCAnonStoreyDe = new ArenaProtocol.\u003CParseMessage\u003Ec__AnonStoreyDE();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyDe.\u003C\u003Ef__this = this;
        if (Game.Me.SocialState == PlayerSocialState.DoNotDisturb)
        {
          this.RequestArenaInviteCancel();
          break;
        }
        uint index = br.ReadUInt32();
        uint timeToLive = (uint) (br.ReadDateTime() - Game.TimeSync.ServerDateTime).TotalSeconds;
        Player player = Game.Players[index];
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyDe.dialogBox = MessageBoxFactory.CreateDialogBox(BsgoCanvas.Windows);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyDe.dialogBox.SetContent(new OnMessageBoxClose(messageCAnonStoreyDe.\u003C\u003Em__228), "%$bgo.arena.duel_invite_title%", BsgoLocalization.Get("%$bgo.arena.duel_invite%", (object) player.Name), timeToLive);
        // ISSUE: reference to a compiler-generated method
        this.CloseArenaInvite = new AnonymousDelegate(messageCAnonStoreyDe.\u003C\u003Em__229);
        Game.Me.Arena.State = ArenaState.Invited;
        break;
      case ArenaProtocol.Reply.ArenaPartyFound:
        if (global::Arena.Debug)
          Log.Add("ArenaPartyFound");
        new InfoBox("%$bgo.arena.party_found%").Show();
        break;
      case ArenaProtocol.Reply.ArenaInvite:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        ArenaProtocol.\u003CParseMessage\u003Ec__AnonStoreyDF messageCAnonStoreyDf = new ArenaProtocol.\u003CParseMessage\u003Ec__AnonStoreyDF();
        DateTime dateTime1 = br.ReadDateTime();
        if (global::Arena.Debug)
          Log.Add("ArenaInvite");
        Game.Me.Arena.State = ArenaState.Invited;
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyDf.window = new AskBox("%$bgo.arena.you_are_invited%", (float) (dateTime1 - Game.TimeSync.ServerDateTime).TotalSeconds, new AnonymousDelegate(this.RequestArenaInviteOk), new AnonymousDelegate(this.RequestArenaInviteCancel));
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyDf.window.PositionY += 120f;
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyDf.window.Show();
        // ISSUE: reference to a compiler-generated method
        this.CloseArenaInvite = new AnonymousDelegate(messageCAnonStoreyDf.\u003C\u003Em__22A);
        break;
      case ArenaProtocol.Reply.ArenaBackToQueue:
        uint playerID1 = br.ReadUInt32();
        ArenaProtocol.ArenaFailedReason reason1 = (ArenaProtocol.ArenaFailedReason) br.ReadByte();
        if (global::Arena.Debug)
          Log.Add("ArenaBackToQueue " + (object) playerID1 + " " + (object) reason1);
        this.ArenaShowError(playerID1, reason1, "%$bgo.arena.arena_error_second_line_in_queue%");
        break;
      case ArenaProtocol.Reply.ArenaFailed:
        uint playerID2 = br.ReadUInt32();
        ArenaProtocol.ArenaFailedReason reason2 = (ArenaProtocol.ArenaFailedReason) br.ReadByte();
        if (global::Arena.Debug)
          Log.Add("ArenaFailed " + (object) playerID2 + " " + (object) reason2);
        this.ArenaShowError(playerID2, reason2, "%$bgo.arena.arena_error_second_line_out_of_queue%");
        break;
      case ArenaProtocol.Reply.ArenaClosed:
        if (global::Arena.Debug)
          Log.Add("ArenaClosed");
        Game.Me.Arena.State = ArenaState.None;
        ArenaManager.Changed();
        break;
      case ArenaProtocol.Reply.ArenaWon:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        ArenaProtocol.\u003CParseMessage\u003Ec__AnonStoreyE0 messageCAnonStoreyE0 = new ArenaProtocol.\u003CParseMessage\u003Ec__AnonStoreyE0();
        uint cardGUID = br.ReadGUID();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE0.type = (CounterStore) br.ReadUInt32();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE0.oldValue = br.ReadInt32();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE0.newValue = br.ReadInt32();
        if (global::Arena.Debug)
        {
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated field
          Log.Add("ArenaWon " + (object) cardGUID + " " + (object) messageCAnonStoreyE0.type + " " + (object) messageCAnonStoreyE0.oldValue + " " + (object) messageCAnonStoreyE0.newValue);
        }
        BeforeFightCountDown.Stop();
        if ((int) cardGUID != 0)
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          ArenaProtocol.\u003CParseMessage\u003Ec__AnonStoreyE1 messageCAnonStoreyE1 = new ArenaProtocol.\u003CParseMessage\u003Ec__AnonStoreyE1();
          // ISSUE: reference to a compiler-generated field
          messageCAnonStoreyE1.\u003C\u003Ef__ref\u0024224 = messageCAnonStoreyE0;
          // ISSUE: reference to a compiler-generated field
          messageCAnonStoreyE1.reward = (RewardCard) Game.Catalogue.FetchCard(cardGUID, CardView.Reward);
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          messageCAnonStoreyE1.reward.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyE1.\u003C\u003Em__22B));
          break;
        }
        new InfoBox("%$bgo.arena.won%", (AnonymousDelegate) (() => ArenaProtocol.GetProtocol().ArenaClose())).Show();
        break;
      case ArenaProtocol.Reply.ArenaLost:
        CounterStore counterStore = (CounterStore) br.ReadUInt32();
        int num1 = br.ReadInt32();
        int num2 = br.ReadInt32();
        if (global::Arena.Debug)
          Log.Add("ArenaLost " + (object) counterStore + " " + (object) num1 + " " + (object) num2);
        BeforeFightCountDown.Stop();
        string key = string.Empty;
        if (counterStore == CounterStore.AP1x1Tier1 || counterStore == CounterStore.AP3x3MixedTier1)
          key = "%$bgo.arena.tier1%";
        if (counterStore == CounterStore.AP1x1Tier2 || counterStore == CounterStore.AP3x3MixedTier2)
          key = "%$bgo.arena.tier2%";
        if (counterStore == CounterStore.AP1x1Tier3 || counterStore == CounterStore.AP3x3MixedTier3)
          key = "%$bgo.arena.tier3%";
        new InfoBox("%$bgo.arena.lost%%br%" + BsgoLocalization.Get(key, (object) (num2.ToString() + " (" + (object) (num2 - num1) + ")")), (AnonymousDelegate) (() => ArenaProtocol.GetProtocol().ArenaClose())).Show();
        break;
      case ArenaProtocol.Reply.ArenaInit:
        global::Arena arena = Game.Me.Arena;
        arena.TimeBegin = br.ReadDateTime();
        arena.TimeEnd = br.ReadDateTime();
        uint objectID = br.ReadUInt32();
        uint num3 = br.ReadUInt32();
        SpaceObject spaceObject = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID);
        if (spaceObject != null)
          spaceObject.SetMarkObject(SpaceObject.MarkObjectType.Waypoint);
        if (global::Arena.Debug)
          Log.Add("ArenaInit " + (object) arena.TimeBegin + " " + (object) arena.TimeEnd + " " + (object) arena.Center + " " + (object) objectID + " " + (object) num3 + " Now " + (object) Game.TimeSync.ServerDateTime);
        BeforeFightCountDown.Start();
        break;
      case ArenaProtocol.Reply.ArenaOutOfRange:
        if (global::Arena.Debug)
          Log.Add("ArenaOutOfRange");
        uint num4 = br.ReadUInt32();
        DateTime time1 = br.ReadDateTime();
        if ((int) num4 != (int) SpaceLevel.GetLevel().PlayerShip.ObjectID)
          break;
        OutOfRangeCountdown.Show(time1);
        break;
      case ArenaProtocol.Reply.ArenaOuterRangeOk:
        if (global::Arena.Debug)
          Log.Add("ArenaOuterRangeOk");
        OutOfRangeCountdown.Hide();
        break;
      case ArenaProtocol.Reply.ArenaCapturePoint:
        uint num5 = br.ReadUInt32();
        DateTime time2 = br.ReadDateTime();
        PlayerShip playerShip1 = (PlayerShip) null;
        foreach (PlayerShip playerShip2 in SpaceLevel.GetLevel().GetObjectRegistry().GetLoaded<PlayerShip>())
        {
          if ((int) playerShip2.ObjectID == (int) num5)
          {
            playerShip1 = playerShip2;
            break;
          }
        }
        if (global::Arena.Debug)
          Log.Add("ArenaCapturePoint " + (object) num5 + " Now " + (object) Game.TimeSync.ServerDateTime + " " + (object) time2);
        ArenaManager.StartCapturePointCountdown(time2, playerShip1 == null || Game.Me.Party.Members.Count <= 0 ? (int) num5 == (int) SpaceLevel.GetLevel().PlayerShip.ObjectID : Game.Me.Party.IsIdMemberOrMe(playerShip1.Player.ServerID));
        break;
      case ArenaProtocol.Reply.ArenaOutOfCapturePoint:
        uint num6 = br.ReadUInt32();
        if (global::Arena.Debug)
          Log.Add("ArenaInnerRangeOk " + (object) num6);
        ArenaManager.StopCapturePointCountdown();
        break;
      case ArenaProtocol.Reply.ArenaRespawn:
        uint playerId = br.ReadUInt32();
        DateTime dateTime2 = br.ReadDateTime();
        if (global::Arena.Debug)
          Log.Add("ArenaRespawn PlayerId: " + (object) playerId + " Time: " + (object) dateTime2);
        if ((int) Game.Me.ServerID == (int) playerId)
        {
          FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification(BsgoLocalization.Get("%$bgo.arena.player_resurrection_me%", (object) Tools.FormatTime(dateTime2 - Game.TimeSync.ServerDateTime + TimeSpan.FromSeconds(1.0))), NotificationCategory.Negative));
          break;
        }
        NotificationCategory category = !Game.Me.Party.IsIdMemberOrMe(playerId) ? NotificationCategory.Positive : NotificationCategory.Negative;
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification(BsgoLocalization.Get("%$bgo.arena.player_resurrection%", (object) Game.Players[playerId].Name, (object) Tools.FormatTime(dateTime2 - Game.TimeSync.ServerDateTime + TimeSpan.FromSeconds(1.0))), category));
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Game protocol: " + (object) reply);
        break;
    }
  }

  public void ArenaShowError(uint playerID, ArenaProtocol.ArenaFailedReason reason, string secondLine)
  {
    if (reason == ArenaProtocol.ArenaFailedReason.Unknown)
      this.ArenaShowError(0U, "%$bgo.arena.error%", string.Empty, secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.Leave)
      this.ArenaShowError(playerID, "%$bgo.arena.error_leave%", "%$bgo.arena.error_leave1%", secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.Disconnect)
      this.ArenaShowError(playerID, "%$bgo.arena.error_disconnect%", "%$bgo.arena.error_disconnect1%", secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.ChangedShip)
      this.ArenaShowError(playerID, "%$bgo.arena.error_changedship%", "%$bgo.arena.error_changedship1%", secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.StoryStarted)
      this.ArenaShowError(playerID, "$$bgo.arena.error_storystarted%", "%$bgo.arena.error_storystarted1%", secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.nCombat)
      this.ArenaShowError(playerID, "%$bgo.arena.error_inCombat%", "%$bgo.arena.error_inCombat1%", secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.AlreadyCheckedIn)
      this.ArenaShowError(playerID, "%$bgo.arena.error_alreadycheckedin%", "%$bgo.arena.error_alreadycheckedin1%", secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.InMission)
      this.ArenaShowError(playerID, "%$bgo.arena.error_inmission%", "%$bgo.arena.error_inmission1%", secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.Offline)
      this.ArenaShowError(playerID, "%$bgo.arena.error_offline%", "%$bgo.arena.error_offline1%", secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.CancelPressed)
      this.ArenaShowError(playerID, "%$bgo.arena.error_cancelpressed%", "%$bgo.arena.error_cancelpressed1%", secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.Timeout)
      this.ArenaShowError(playerID, "%$bgo.arena.error_timeout%", "%$bgo.arena.error_timeout1%", secondLine);
    else if (reason == ArenaProtocol.ArenaFailedReason.CapitalShip)
      this.ArenaShowError(playerID, "%$bgo.arena.error_capitalship%", "%$bgo.arena.error_capitalship1%", secondLine);
    else
      new InfoBox(BsgoLocalization.Get("%$bgo.arena.error_unknown%", (object) reason)).Show();
  }

  public void ArenaShowError(uint pid, string message1, string message2, string secondLine)
  {
    secondLine = !string.IsNullOrEmpty(secondLine) ? "%br%" + secondLine : string.Empty;
    if ((int) pid == 0)
      new InfoBox(message1 + secondLine).Show();
    else
      new InfoBox(BsgoLocalization.Get(message2, (object) Game.Players[pid].Name) + secondLine).Show();
  }

  public enum ArenaFailedReason
  {
    Unknown,
    CancelPressed,
    Timeout,
    Leave,
    Disconnect,
    ChangedShip,
    StoryStarted,
    nCombat,
    AlreadyCheckedIn,
    InMission,
    Offline,
    CapitalShip,
    SelfInflictedThreat,
  }

  public enum Request : ushort
  {
    Arena1vs1CheckIn = 1,
    Arena3vs3MixedCheckIn = 2,
    Arena3vs3MixedRandomCheckIn = 3,
    ArenaDuelCheckIn = 4,
    ArenaCancelCheckIn = 5,
    ArenaInviteOk = 6,
    ArenaInviteCancel = 7,
    ArenaClose = 8,
  }

  public enum Reply : ushort
  {
    Arena1vs1CheckedIn = 1,
    Arena3vs3MixedCheckedIn = 2,
    Arena3vs3MixedRandomCheckedIn = 3,
    ArenaDuelCheckedIn = 4,
    ArenaDuelInviteSlave = 5,
    ArenaPartyFound = 6,
    ArenaInvite = 7,
    ArenaBackToQueue = 8,
    ArenaFailed = 9,
    ArenaClosed = 10,
    ArenaWon = 11,
    ArenaLost = 12,
    ArenaInit = 13,
    ArenaOutOfRange = 14,
    ArenaOuterRangeOk = 15,
    ArenaCapturePoint = 16,
    ArenaOutOfCapturePoint = 17,
    ArenaRespawn = 18,
  }
}
