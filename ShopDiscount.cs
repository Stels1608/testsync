﻿// Decompiled with JetBrains decompiler
// Type: ShopDiscount
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class ShopDiscount
{
  private readonly int percentage;
  private readonly int expiry;

  public int Percentage
  {
    get
    {
      return this.percentage;
    }
  }

  public int Expiry
  {
    get
    {
      return this.expiry;
    }
  }

  public int Duration
  {
    get
    {
      return this.CalculateDuration((int) Game.TimeSync.ServerTime);
    }
  }

  public ShopDiscount(int percentage, int expiry)
  {
    this.percentage = percentage;
    this.expiry = expiry;
  }

  public override string ToString()
  {
    return string.Format("Percentage: {0}, Expiry: {1}", (object) this.percentage, (object) this.expiry);
  }

  public float ApplyDiscount(float price)
  {
    return price - (float) ((double) price * (double) this.percentage / 100.0);
  }

  public bool HasExpired(int now)
  {
    return ShopDiscount.HasExpired(this.expiry, now);
  }

  public bool HasExpired()
  {
    return this.HasExpired((int) Game.TimeSync.ServerTime);
  }

  public static bool HasExpired(int expiry, int now)
  {
    return ShopDiscount.CalculateDuration(expiry, now) == 0;
  }

  public static int CalculateDuration(int expiry, int now)
  {
    return Math.Max(expiry - now, 0);
  }

  private int CalculateDuration(int now)
  {
    return ShopDiscount.CalculateDuration(this.expiry, now);
  }
}
