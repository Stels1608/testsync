﻿// Decompiled with JetBrains decompiler
// Type: AbilityTargetChangedData
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class AbilityTargetChangedData
{
  private readonly ShipAbility ability;
  private readonly ISpaceEntity oldTarget;
  private readonly ISpaceEntity newTarget;

  public ShipAbility Ability
  {
    get
    {
      return this.ability;
    }
  }

  public ISpaceEntity OldTarget
  {
    get
    {
      return this.oldTarget;
    }
  }

  public ISpaceEntity NewTarget
  {
    get
    {
      return this.newTarget;
    }
  }

  public AbilityTargetChangedData(ShipAbility ability, ISpaceEntity oldTarget, ISpaceEntity newTarget)
  {
    this.ability = ability;
    this.oldTarget = oldTarget;
    this.newTarget = newTarget;
  }
}
