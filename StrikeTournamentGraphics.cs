﻿// Decompiled with JetBrains decompiler
// Type: StrikeTournamentGraphics
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class StrikeTournamentGraphics : ITournamentGraphics
{
  public UnityEngine.Sprite EnterIconNormal
  {
    get
    {
      return ResourceLoader.Load<UnityEngine.Sprite>("GUI/Options/button_tournament_strike_normal");
    }
  }

  public UnityEngine.Sprite EnterIconOver
  {
    get
    {
      return ResourceLoader.Load<UnityEngine.Sprite>("GUI/Options/button_tournament_strike_over");
    }
  }

  public string LoadingScreen
  {
    get
    {
      return "GUI/LoadingScene/tournament_strike";
    }
  }
}
