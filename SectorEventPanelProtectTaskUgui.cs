﻿// Decompiled with JetBrains decompiler
// Type: SectorEventPanelProtectTaskUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using TMPro;
using UnityEngine;

public class SectorEventPanelProtectTaskUgui : SectorEventPanelTaskUgui
{
  private static readonly ProtectTaskLocalizer localizer = new ProtectTaskLocalizer();
  private uint vipObjectId;
  private DateTime endTime;
  private SpaceObject vipObject;
  [SerializeField]
  private TextMeshProUGUI progressBarLabel;
  [SerializeField]
  private ProgressBarUgui progressBar;
  [SerializeField]
  private TextMeshProUGUI timeLabel;
  [SerializeField]
  private TextMeshProUGUI remainingTimeLabel;

  private SpaceObject VipObject
  {
    get
    {
      if (this.vipObject == null)
      {
        this.vipObject = SpaceLevel.GetLevel().GetObjectRegistry().Get(this.vipObjectId);
        if (this.vipObject != null)
        {
          this.vipObject.AutoSubscribe = true;
          this.vipObject.Subscribe();
        }
      }
      return this.vipObject;
    }
  }

  public void Initialize(SectorEventProtectTask task, Faction ownerFaction)
  {
    this.vipObjectId = task.VipObjectId;
    this.endTime = task.EndTime;
    this.SetDescriptionText(SectorEventPanelProtectTaskUgui.localizer.GetDescription(task.SubType, ownerFaction, Game.Me.Faction));
    this.remainingTimeLabel.text = SectorEventPanelProtectTaskUgui.localizer.GetTimeText(task.SubType);
    this.progressBarLabel.text = SectorEventPanelProtectTaskUgui.localizer.GetProgressbarText(task.SubType);
  }

  public void OnQuickSelectClick()
  {
    if (this.VipObject == null)
      return;
    TargetSelector.SelectTargetRequest(this.VipObject, false);
  }

  private void Update()
  {
    if (this.VipObject == null)
      return;
    this.progressBar.SetProgress01(this.VipObject.Props.HullPoints / this.VipObject.Props.MaxHullPoints);
    if (this.endTime < Game.TimeSync.ServerDateTime)
      this.timeLabel.text = Tools.FormatTime(TimeSpan.FromTicks(0L));
    else
      this.timeLabel.text = Tools.FormatTime(this.endTime - Game.TimeSync.ServerDateTime);
  }
}
