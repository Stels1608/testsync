﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorTopTextUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using TMPro;
using UnityEngine;

public class HudIndicatorTopTextUgui : HudIndicatorBaseComponentUgui, IReceivesSetting, IReceivesTarget, IRequiresLateUpdate, IHudIndicatorTopText
{
  private bool showTargetName = true;
  private bool showShipName = true;
  private bool showWing = true;
  private bool showTitle = true;
  [SerializeField]
  private TextMeshProUGUI textLabel;
  [SerializeField]
  private TMP_SubMeshUI inlineGraphics;
  private ISpaceEntity target;
  private float originalFontSize;
  private bool? inRenderRange;

  public TextMeshProUGUI TextLabel
  {
    get
    {
      return this.textLabel;
    }
  }

  private bool IsVisible
  {
    get
    {
      if (this.target.SpaceEntityType == SpaceEntityType.SectorEvent)
        return this.InScreen;
      if (!this.inRenderRange.HasValue || (!this.InScannerRange || !this.inRenderRange.Value && !HudIndicatorInfo.IsNeverInMiniMode(this.target)) && (!this.IsSelected && !this.IsMultiselected))
        return false;
      return this.InScreen;
    }
  }

  protected override void ResetComponentStates()
  {
    this.target = (ISpaceEntity) null;
    if (!((Object) this.textLabel != (Object) null))
      return;
    this.textLabel.text = string.Empty;
  }

  protected override void Awake()
  {
    base.Awake();
    this.originalFontSize = this.textLabel.fontSize;
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.target = target;
    this.UpdateLabelText();
    this.UpdateColorAndAlpha();
  }

  private void UpdateFontSize()
  {
    this.TextLabel.fontSize = !HudIndicatorInfo.IsSmallProjectile(this.target) || this.IsSelected ? this.originalFontSize : this.originalFontSize * 0.8f;
  }

  public void UpdateLabelText()
  {
    this.UpdateFontSize();
    string text1 = this.ColorizeTextString(!this.showTargetName ? string.Empty : this.target.Name.ToUpper());
    if (Game.Me.TournamentParticipant)
    {
      this.SetLabelText(this.ColorizeTextString(text1));
    }
    else
    {
      PlayerShip playerShip = this.target as PlayerShip;
      if (playerShip == null)
      {
        this.SetLabelText(this.ColorizeTextString(text1));
      }
      else
      {
        if (!string.IsNullOrEmpty(text1) && playerShip.Player.HasGuild && (int) Game.Me.Guild.ServerID == (int) playerShip.Player.guildId)
          text1 = "<sprite=0>" + text1 + "<sprite=1>";
        string text2 = text1 + (!string.IsNullOrEmpty(text1) ? "\n" : string.Empty);
        string str1 = string.Empty;
        if (this.showShipName)
        {
          string upper = playerShip.Player.ShipName.ToUpper();
          str1 = string.IsNullOrEmpty(upper) ? str1 + playerShip.GUICard.Name.ToUpper() : str1 + upper;
        }
        string str2 = str1 + (!string.IsNullOrEmpty(str1) ? "\n" : string.Empty);
        string str3 = string.Empty;
        if (this.showTitle && !string.IsNullOrEmpty(playerShip.Player.Title))
          str3 = str3 + "[ " + playerShip.Player.Title.ToUpper() + " ]";
        string str4 = str3 + (!string.IsNullOrEmpty(str3) ? "\n" : string.Empty);
        string str5 = string.Empty;
        if (this.showWing && !string.IsNullOrEmpty(playerShip.Player.guildName))
          str5 = str5 + "<" + Tools.SubStr(playerShip.Player.guildName.ToUpper(), 30) + ">";
        string str6 = string.Empty;
        if (!string.IsNullOrEmpty(text2))
          str6 += this.ColorizeTextString(text2);
        if (!string.IsNullOrEmpty(str2) || !string.IsNullOrEmpty(str4) || !string.IsNullOrEmpty(str5))
          str6 += this.ColorizeTextString(str2 + str4 + str5);
        this.TextLabel.text = str6;
      }
    }
  }

  private string ColorizeTextString(string text)
  {
    if (string.IsNullOrEmpty(text))
      return text;
    return "<color=#" + Tools.ColorToHexRGB((Color32) HudIndicatorColorScheme.Instance.TextColor(this.target)) + ">" + text + "</color>";
  }

  private void SetLabelText(string text)
  {
    this.TextLabel.text = text;
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    this.inlineGraphics.enabled = this.IsVisible;
    this.textLabel.enabled = this.IsVisible;
    this.UpdateFontSize();
  }

  public void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    switch (userSetting)
    {
      case UserSetting.HudIndicatorShowTargetNames:
        this.showTargetName = (bool) data;
        break;
      case UserSetting.HudIndicatorTextSize:
        this.originalFontSize = (float) data;
        break;
      case UserSetting.HudIndicatorColorScheme:
        this.UpdateLabelText();
        break;
      case UserSetting.HudIndicatorShowShipNames:
        this.showShipName = (bool) data;
        break;
      case UserSetting.HudIndicatorShowWingNames:
        this.showWing = (bool) data;
        break;
      case UserSetting.HudIndicatorShowTitles:
        this.showTitle = (bool) data;
        break;
    }
    this.UpdateLabelText();
  }

  public void RemoteLateUpdate()
  {
    bool flag = (double) this.target.GetDistance() <= (double) HudIndicatorInfo.TopTextRenderRangeThreshold;
    bool? nullable = this.inRenderRange;
    if ((nullable.GetValueOrDefault() != flag ? 1 : (!nullable.HasValue ? 1 : 0)) == 0)
      return;
    this.inRenderRange = new bool?(flag);
    this.UpdateView();
  }
}
