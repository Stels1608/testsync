﻿// Decompiled with JetBrains decompiler
// Type: ContentLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ContentLayout : MonoBehaviour, ILocalizeable
{
  protected Vector3 buttonAreaPosition = new Vector3(20f, -675f, -1f);
  private List<GameObject> contentArea = new List<GameObject>();

  public List<GameObject> ContentArea
  {
    get
    {
      return this.contentArea;
    }
  }

  public virtual void Awake()
  {
    this.CorrectContentAreas();
  }

  public virtual void Start()
  {
    this.ReloadLanguageData();
  }

  private void CorrectContentAreas()
  {
    using (List<GameObject>.Enumerator enumerator = this.contentArea.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        current.transform.parent = this.gameObject.transform;
        current.transform.localScale = Vector3.one;
        current.layer = this.gameObject.layer;
      }
    }
  }

  public virtual void ReloadLanguageData()
  {
  }
}
