﻿// Decompiled with JetBrains decompiler
// Type: DynamicEventTracker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DynamicEventTracker : MonoBehaviour
{
  private const string ATTACK_ICON_PATH = "GUI/DynamicEvents/icon_attack";
  private const string DEFEND_ICON_PATH = "GUI/DynamicEvents/icon_defend";
  public EventFoldOutWidget parentFoldout;
  protected UILabel taskDescriptionLabel;

  public void ShowProtectTask(SectorEventProtectTask task, Faction OwnerFaction)
  {
    this.RemoveChildren();
  }

  public void ShowEventCompleted(SectorEventState eventState, Faction OwnerFaction, SectorEventReward reward)
  {
    this.RemoveChildren();
    this.parentFoldout.ActivateCloseButton();
  }

  public Texture2D GetTaskIcon(SectorEvent sectorEvent)
  {
    if (Game.Me.Faction == sectorEvent.OwnerFaction)
      return ResourceLoader.Load<Texture2D>("GUI/DynamicEvents/icon_defend");
    return ResourceLoader.Load<Texture2D>("GUI/DynamicEvents/icon_attack");
  }

  public void RemoveChildren()
  {
    foreach (Object @object in this.transform)
      Object.Destroy(@object);
  }
}
