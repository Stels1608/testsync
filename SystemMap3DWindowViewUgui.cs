﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DWindowViewUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class SystemMap3DWindowViewUgui : SystemMap3DWindowViewUi
{
  [SerializeField]
  private Text sectorNameLabel;
  [SerializeField]
  private Text blinkingMessageLabel;
  [SerializeField]
  private Text locationVector;

  protected override void SetSectorNameText(string labelText)
  {
    this.sectorNameLabel.text = labelText;
  }

  protected override void SetMessageText(string messageText)
  {
    base.SetMessageText(messageText);
    this.blinkingMessageLabel.GetComponent<Animation>().Stop();
    this.blinkingMessageLabel.GetComponent<Animation>().Play("UiTextAlphaPingPong");
    this.blinkingMessageLabel.text = messageText;
  }

  protected override void SetLocationVector(string vectorText)
  {
    this.locationVector.text = vectorText;
  }
}
