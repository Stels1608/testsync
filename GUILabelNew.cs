﻿// Decompiled with JetBrains decompiler
// Type: GUILabelNew
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using UnityEngine;

public class GUILabelNew : GUIPanel, IDrawable
{
  private string text;
  private Color normal;
  private Color over;
  private readonly GUIStyle style;
  private bool mouseOver;
  private bool autoSize;

  public string Text
  {
    get
    {
      return this.text;
    }
    set
    {
      this.text = Tools.ParseMessage(value);
      this.AdjustSize();
    }
  }

  public GUIStyle Style
  {
    get
    {
      return this.style;
    }
  }

  public string TextNoParse
  {
    get
    {
      return this.Text;
    }
    set
    {
      this.text = value;
    }
  }

  public Font Font
  {
    get
    {
      return this.style.font;
    }
    set
    {
      this.style.font = value;
      this.AdjustSize();
    }
  }

  public int FontSize
  {
    get
    {
      return this.style.fontSize;
    }
    set
    {
      this.style.fontSize = value;
      this.AdjustSize();
    }
  }

  public Color NormalColor
  {
    get
    {
      return this.normal;
    }
    set
    {
      this.normal = value;
      this.StyleColor = !this.mouseOver ? this.NormalColor : this.OverColor;
    }
  }

  public Color OverColor
  {
    get
    {
      return this.over;
    }
    set
    {
      this.over = value;
      this.StyleColor = !this.mouseOver ? this.NormalColor : this.OverColor;
    }
  }

  public Color AllColor
  {
    set
    {
      Color color = value;
      this.OverColor = color;
      this.NormalColor = color;
    }
  }

  public Color StyleColor
  {
    set
    {
      this.style.normal.textColor = value;
    }
  }

  public bool AutoSize
  {
    get
    {
      return this.autoSize;
    }
    set
    {
      this.autoSize = value;
      this.AdjustSize();
    }
  }

  public bool WordWrap
  {
    get
    {
      return this.style.wordWrap;
    }
    set
    {
      this.style.wordWrap = value;
      this.AdjustSize();
    }
  }

  public TextAnchor Alignment
  {
    get
    {
      return this.style.alignment;
    }
    set
    {
      this.style.alignment = value;
    }
  }

  public bool MouseOver
  {
    get
    {
      return this.mouseOver;
    }
    set
    {
      this.mouseOver = value;
      this.style.normal.textColor = !this.MouseOver ? this.NormalColor : this.OverColor;
    }
  }

  public GUILabelNew()
  {
    this.Name = "LabelNew";
    this.IsUpdated = false;
    this.IsRendered = true;
    this.style = new GUIStyle();
    this.style.font = Gui.Options.FontVerdana;
    this.style.fontSize = 12;
    this.Text = string.Empty;
    this.AllColor = Gui.Options.NormalColor;
  }

  public GUILabelNew(JLabel desc)
  {
    this.style = new GUIStyle();
    this.Name = desc.name;
    this.Font = !string.IsNullOrEmpty(desc.fontPath) ? (Font) ResourceLoader.Load(desc.fontPath) : Gui.Options.FontVerdana;
    this.NormalColor = desc.normalColor;
    this.OverColor = desc.overColor;
    this.Text = desc.text;
    this.Alignment = desc.textAlignment;
    this.WordWrap = desc.useWordWrap;
    this.SmartRect.Width = desc.size.x;
    this.SmartRect.Height = desc.size.y;
    this.Position = desc.position;
    this.IsUpdated = false;
    this.IsRendered = true;
  }

  public GUILabelNew(GUILabelNew toCopy)
  {
    this.style = new GUIStyle();
    this.Name = toCopy.Name;
    this.Font = toCopy.Font;
    this.NormalColor = toCopy.NormalColor;
    this.OverColor = toCopy.OverColor;
    this.Text = toCopy.Text;
    this.Alignment = toCopy.Alignment;
    this.WordWrap = toCopy.WordWrap;
    this.AutoSize = toCopy.AutoSize;
    this.Size = toCopy.Size;
    this.Position = toCopy.Position;
    this.IsUpdated = toCopy.IsUpdated;
    this.IsRendered = toCopy.IsRendered;
  }

  public GUILabelNew(string text)
    : this(text, (SmartRect) null)
  {
  }

  public GUILabelNew(string text, SmartRect parent)
    : this(text, parent, Gui.Options.FontVerdana, Color.white, Color.white)
  {
  }

  public GUILabelNew(string text, SmartRect parent, Font font, Color normalColor, Color overColor)
    : base(parent)
  {
    this.Name = "LabelNew";
    this.IsUpdated = false;
    this.IsRendered = true;
    this.style = new GUIStyle();
    this.style.font = font;
    this.Text = text;
    this.NormalColor = normalColor;
    this.OverColor = overColor;
    this.MakeCenteredRect();
  }

  public void MakeCenteredRect()
  {
    Size size = TextUtility.CalcTextSize(this.Text, this.style.font, this.style.fontSize);
    this.Rect = new Rect(this.Position.x - (float) size.width / 2f, this.Position.y - (float) size.height / 2f, (float) size.width, (float) size.height);
  }

  public override void Draw()
  {
    base.Draw();
    GUI.Label(this.SmartRect.AbsRect, this.Text, this.style);
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    this.MouseOver = this.Contains(mousePosition);
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  private void AdjustSize()
  {
    if (!this.autoSize || this.WordWrap)
      return;
    this.Size = TextUtility.CalcTextSize(this.Text, this.Font, this.FontSize).ToF2();
    this.RecalculateAbsCoords();
  }
}
