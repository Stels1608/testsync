﻿// Decompiled with JetBrains decompiler
// Type: CutsceneTriggerEventMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class CutsceneTriggerEventMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "CutsceneTriggerEventMediator";

  public CutsceneTriggerEventMediator()
    : base("CutsceneTriggerEventMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.DockAtNpcShipRequest);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    if (message.Id != Message.DockAtNpcShipRequest)
      return;
    FacadeFactory.GetInstance().SendMessage(Message.CutscenePlayRequest, (object) new CutscenePlayRequestData(CutsceneId.Docking, (CutsceneManager.OnFinishCutsceneDelegate) null, (SpaceObject) message.Data));
  }
}
