﻿// Decompiled with JetBrains decompiler
// Type: Gearbox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class Gearbox : SingleLODListener
{
  private Gear lastGear = Gear.None;
  public List<ShipEngine> Engines;
  private ShipEngine.EngineState lastEngineState;

  protected override void OnSwitched(bool value)
  {
    this.enabled = value;
    using (List<ShipEngine>.Enumerator enumerator = this.Engines.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipEngine current = enumerator.Current;
        if ((Object) current != (Object) null)
          current.enabled = value;
      }
    }
  }

  private void Update()
  {
    if (this.SpaceObject == null || this.SpaceObject.MovementController == null)
      return;
    Gear gear = this.SpaceObject.MovementController.Gear;
    if (gear != Gear.Regular && gear == this.lastGear)
      return;
    this.lastGear = gear;
    ShipEngine.EngineState engineState = ShipEngine.EngineState.Off;
    switch (gear)
    {
      case Gear.Regular:
        float marchSpeed = this.SpaceObject.MovementController.MarchSpeed;
        if ((double) marchSpeed > 0.0)
        {
          engineState = (double) marchSpeed >= 10.0 ? ShipEngine.EngineState.Normal : ShipEngine.EngineState.Weak;
          break;
        }
        break;
      case Gear.Boost:
        engineState = ShipEngine.EngineState.Boost;
        break;
    }
    if (engineState == this.lastEngineState)
      return;
    this.SetEngineState(engineState);
    this.lastEngineState = engineState;
  }

  public void SetEngineState(ShipEngine.EngineState engineState)
  {
    for (int index = 0; index < this.Engines.Count; ++index)
    {
      if (!((Object) this.Engines[index] == (Object) null))
        this.Engines[index].State = engineState;
    }
  }

  private void Reset()
  {
    this.Engines = new List<ShipEngine>();
    foreach (ShipEngine componentsInChild in this.gameObject.GetComponentsInChildren<ShipEngine>())
    {
      if (componentsInChild.engineComponentType != ShipEngine.EngineComponentType.Glow)
        this.Engines.Add(componentsInChild);
    }
  }
}
