﻿// Decompiled with JetBrains decompiler
// Type: Arena
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class Arena
{
  public static bool Debug = true;
  public ArenaType ArenaType = ArenaType.Arena1vs1;
  public DateTime TimeWaitingBegin = Game.TimeSync.ServerDateTime;
  public DateTime TimeBegin = Game.TimeSync.ServerDateTime + new TimeSpan(0, 10, 0);
  public DateTime TimeEnd = Game.TimeSync.ServerDateTime + new TimeSpan(1, 10, 0);
  public DateTime InviteBegin = Game.TimeSync.ServerDateTime;
  public DateTime InviteTimeout = Game.TimeSync.ServerDateTime.AddMinutes(10.0);
  public ArenaState State;
  public Vector3 Center;

  public void CancelPendingInvites()
  {
    if (this.State != ArenaState.Invited || ArenaProtocol.GetProtocol().CloseArenaInvite == null)
      return;
    this.State = ArenaState.None;
    ArenaProtocol.GetProtocol().CloseArenaInvite();
  }
}
