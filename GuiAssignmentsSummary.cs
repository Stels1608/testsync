﻿// Decompiled with JetBrains decompiler
// Type: GuiAssignmentsSummary
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using Gui.Tooltips;
using System.Collections.Generic;
using UnityEngine;

public class GuiAssignmentsSummary : GuiPanel
{
  public readonly GuiPanelVerticalScroll scroll = new GuiPanelVerticalScroll();
  private readonly GuiButton assignments = new GuiButton("%$bgo.etc.assignments_summary_title%", "GUI/Common/smallbutton", "GUI/Common/smallbutton_over", "GUI/Common/smallbutton_click");

  public override bool IsRendered
  {
    get
    {
      return base.IsRendered;
    }
    set
    {
      bool flag = value;
      this.scroll.HandleMouseInput = flag;
      base.IsRendered = flag;
    }
  }

  public bool IsCollapsed
  {
    get
    {
      return !this.scroll.IsRendered;
    }
    set
    {
      this.scroll.IsRendered = !value;
    }
  }

  public GuiAssignmentsSummary()
  {
    this.Name = "AssignmentsSummary";
    this.assignments.Size = new Vector2(120f, 29f);
    this.assignments.UniformImagePadding = 5;
    this.assignments.Label.PositionY = -1f;
    this.assignments.Pressed = (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.AssignmentsCollapsed, (object) !this.IsCollapsed, true)));
    this.assignments.SetTooltip(BsgoLocalization.Get("%$bgo.etc.assignments_tooltip%", (object) Tools.GetHotKeyFor(Action.ToggleWindowStatusAssignments)));
    this.AddChild((GuiElementBase) this.assignments, Align.UpRight);
    this.scroll.AutoSizeChildren = true;
    this.scroll.RenderDelimiters = true;
    this.scroll.RenderScrollLines = false;
    this.scroll.HandleMouseInput = true;
    this.scroll.Size = new Vector2(258f, 300f);
    this.AddChild((GuiElementBase) this.scroll, Align.UpRight, new Vector2(0.0f, 40f));
    this.Size = this.assignments.Size;
    this.IsCollapsed = false;
  }

  public void UpdateAssignments(MissionList missionList)
  {
    MissionList missionList1 = missionList;
    bool flag = missionList1.Count > 0;
    this.assignments.IsRendered = flag;
    GuiPanelVerticalScroll panelVerticalScroll = this.scroll;
    int num = panelVerticalScroll.IsRendered & flag ? 1 : 0;
    panelVerticalScroll.IsRendered = num != 0;
    int count;
    for (count = this.scroll.Children.Count; count < missionList1.Count; ++count)
    {
      this.scroll.AddChild((GuiElementBase) new GuiAssignmentsSummary.Entry());
      this.scroll.ScrollToEnd();
    }
    for (; count > missionList1.Count; --count)
    {
      this.scroll.RemoveChild(this.scroll.Children[0]);
      this.scroll.ScrollToEnd();
    }
    List<GuiAssignmentsSummary.Entry> entryList = this.scroll.ChildrenOfType<GuiAssignmentsSummary.Entry>();
    for (int index = 0; index < missionList1.Count; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      GuiAssignmentsSummary.\u003CUpdateAssignments\u003Ec__AnonStorey89 assignmentsCAnonStorey89 = new GuiAssignmentsSummary.\u003CUpdateAssignments\u003Ec__AnonStorey89();
      // ISSUE: reference to a compiler-generated field
      assignmentsCAnonStorey89.entry = entryList[index];
      // ISSUE: reference to a compiler-generated field
      assignmentsCAnonStorey89.mission = missionList1[index];
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated method
      assignmentsCAnonStorey89.mission.missionCard.IsLoaded.AddHandler(new SignalHandler(assignmentsCAnonStorey89.\u003C\u003Em__D4));
    }
  }

  private class Entry : GuiPanel
  {
    private readonly GuiLabel score = new GuiLabel(Gui.Options.FontBGM_BT, 12);
    private readonly GuiButton quicklinkButton;

    public Entry()
    {
      this.quicklinkButton = this.CreateMissionQuicklinkButton();
      this.Name = "AssignmentsEntry";
      this.quicklinkButton.Label.Alignment = TextAnchor.MiddleLeft;
      this.score.Alignment = TextAnchor.MiddleRight;
      this.AddChild((GuiElementBase) this.quicklinkButton, Align.UpLeft);
      this.AddChild((GuiElementBase) this.score, Align.UpRight);
    }

    private GuiButton CreateMissionQuicklinkButton()
    {
      Texture2D normal = Resources.Load("GUI/AssignmentSummary/ButtonBackground_normal") as Texture2D;
      Texture2D over = Resources.Load("GUI/AssignmentSummary/ButtonBackground_over") as Texture2D;
      Texture2D pressed = Resources.Load("GUI/AssignmentSummary/ButtonBackground_clicked") as Texture2D;
      Texture2D inactive = normal;
      return new GuiButton(string.Empty, normal, over, pressed, inactive, TextAnchor.MiddleCenter) { Label = { Style = new GUIStyle(GUIStyle.none) }, Font = Gui.Options.FontBGM_BT, FontSize = 12 };
    }

    public void SetMission(Mission mission)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      GuiAssignmentsSummary.Entry.\u003CSetMission\u003Ec__AnonStorey8A missionCAnonStorey8A = new GuiAssignmentsSummary.Entry.\u003CSetMission\u003Ec__AnonStorey8A();
      // ISSUE: reference to a compiler-generated field
      missionCAnonStorey8A.mission = mission;
      if ((double) this.SizeX == 0.0)
        return;
      // ISSUE: reference to a compiler-generated field
      this.quicklinkButton.Text = missionCAnonStorey8A.mission.Name.ToUpper();
      // ISSUE: reference to a compiler-generated field
      this.score.Text = this.GetMissionScore(missionCAnonStorey8A.mission).ToUpper();
      // ISSUE: reference to a compiler-generated field
      Color color = missionCAnonStorey8A.mission.Status != Mission.State.Completed ? Gui.Options.NormalColor : (Game.Me.Faction != Faction.Colonial ? TargetColorsLegacyBrackets.cylonColor : TargetColorsLegacyBrackets.colonialColor);
      this.quicklinkButton.Label.AllColor = color;
      this.score.AllColor = color;
      this.SizeY = (double) this.quicklinkButton.SizeY <= (double) this.score.SizeY ? this.score.SizeY : this.quicklinkButton.SizeY;
      float num = this.SizeX - (this.score.SizeX + 10f);
      string text = this.quicklinkButton.Label.Text;
      if ((double) TextUtility.CalcTextSize(text, this.quicklinkButton.Font, this.quicklinkButton.FontSize).width > (double) num)
      {
        while ((double) TextUtility.CalcTextSize(text, this.quicklinkButton.Font, this.quicklinkButton.FontSize).width > (double) num)
          text = text.Substring(0, text.Length - 2);
        this.quicklinkButton.Label.Text = text + "...";
        // ISSUE: reference to a compiler-generated field
        this.SetTooltip(missionCAnonStorey8A.mission.Name);
      }
      else
        this.ToolTip = (GuiAdvancedTooltipBase) null;
      // ISSUE: reference to a compiler-generated method
      this.quicklinkButton.Pressed = new AnonymousDelegate(missionCAnonStorey8A.\u003C\u003Em__D5);
    }

    private string GetMissionScore(Mission mission)
    {
      string str = string.Empty;
      if (mission.Status == Mission.State.InProgress)
      {
        using (List<MissionCountable>.Enumerator enumerator = mission.countables.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            MissionCountable current = enumerator.Current;
            int num1 = current.Count;
            int num2 = current.NeedCount;
            if (mission.Status == Mission.State.Completed || num1 > num2)
              num1 = num2;
            str += string.Format("%br%{0}/{1}", (object) num1, (object) num2);
          }
        }
      }
      if (str.StartsWith("%br%"))
        str = str.Remove(0, 4);
      return str;
    }
  }
}
