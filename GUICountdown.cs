﻿// Decompiled with JetBrains decompiler
// Type: GUICountdown
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUICountdown : CustomGUIElement
{
  public AtlasAnimation inner;
  private SmartRect srInner;

  public virtual float Progress
  {
    set
    {
      this.inner.PlaybackPosition = value;
    }
  }

  public GUICountdown(Texture2D texture, uint width, uint height)
  {
    this.inner = new AtlasAnimation(texture, width, height);
    this.rect = new SmartRect(new Rect(0.0f, 0.0f, this.inner.ElementSize.x, this.inner.ElementSize.y), float2.zero, (SmartRect) null);
    this.srInner = new SmartRect(new Rect(0.0f, 0.0f, this.inner.ElementSize.x, this.inner.ElementSize.y), float2.zero, this.rect);
    this.inner.PlaybackPosition = 0.0f;
  }

  public void Scale(float scaleFactor)
  {
    this.srInner.Position *= scaleFactor;
    this.srInner.Size *= scaleFactor;
  }

  public override void Draw()
  {
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    Graphics.DrawTexture(this.srInner.AbsRect, (Texture) this.inner.Texture, this.inner.FrameRect, 0, 0, 0, 0);
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.srInner.RecalculateAbsCoords();
  }
}
