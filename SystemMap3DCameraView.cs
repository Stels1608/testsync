﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DCameraView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using SystemMap3D;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SystemMap3DCameraView : MonoBehaviour, ISystemMap3DView
{
  private Quaternion topViewRot = Quaternion.Euler(90f, 0.0f, 0.0f);
  private SystemMap3DCameraView.TransitionDelegate transitionHandler = (SystemMap3DCameraView.TransitionDelegate) (() => {});
  private SystemMap3DManager systemMap3DManager;
  private Camera mapCamera;
  private Transform mapCameraTransform;
  private Camera iconCamera;
  private CameraView cameraViewSetting;
  private bool mouseOrbitFlipX;
  private Vector3 mouseOrbitLocalRot;
  private float yGlobalRot;
  private ISpaceEntity _focusObject;
  private SystemMap3DFocusPoint dummyFocusObject;
  private bool _focusObjectChanged;
  private Vector3 _lastFocusPosition;
  private JoystickLookAroundInputs _joystickLookAroundInputs;
  private float _sectorSize;

  private ISpaceEntity FocusObject
  {
    get
    {
      return this._focusObject;
    }
    set
    {
      this._focusObject = value;
      this._focusObjectChanged = true;
    }
  }

  private CameraSwitcher CameraSwitcher
  {
    get
    {
      return GameLevel.Instance.cameraSwitcher;
    }
  }

  private bool IsInTransition
  {
    get
    {
      return (MulticastDelegate) this.transitionHandler != (MulticastDelegate) new SystemMap3DCameraView.TransitionDelegate(this.NoTransition);
    }
  }

  private bool IsInputAllowed
  {
    get
    {
      if (this.systemMap3DManager.IsSectorMapShown)
        return !this.IsInTransition;
      return false;
    }
  }

  private bool IsCameraFocusingAnObject
  {
    get
    {
      return this._focusObject != this.dummyFocusObject;
    }
  }

  private float MaxCamDistFromCenter
  {
    get
    {
      return this._sectorSize / (2f * Mathf.Tan((float) (3.14159274101257 / (180.0 / (double) SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera().fieldOfView)) / 2f)) * 1.1f;
    }
  }

  private Vector3 TopViewPos
  {
    get
    {
      return new Vector3(0.0f, this.MaxCamDistFromCenter, 0.0f);
    }
  }

  public bool IsRendered { get; set; }

  public Camera MapCamera
  {
    get
    {
      return this.mapCamera;
    }
  }

  public Camera IconCamera
  {
    get
    {
      return this.iconCamera;
    }
  }

  public Transform MapCameraTransform
  {
    get
    {
      return this.mapCameraTransform;
    }
  }

  public static SystemMap3DCameraView Create(SystemMap3DManager systemMap3DManager)
  {
    SystemMap3DCameraView systemMap3DcameraView = new GameObject("SectorMap3DCameraView").AddComponent<SystemMap3DCameraView>();
    systemMap3DcameraView.Initialize(systemMap3DManager);
    return systemMap3DcameraView;
  }

  public void Initialize(SystemMap3DManager systemMap3DManager)
  {
    this.systemMap3DManager = systemMap3DManager;
    this.SetupCameras();
    this.SetupTriggerEvents();
    this.dummyFocusObject = new SystemMap3DFocusPoint();
    this.dummyFocusObject.Root.name = "3D Sector Map - ArbitraryFocusPoint";
    this._focusObject = (ISpaceEntity) this.dummyFocusObject;
    this._joystickLookAroundInputs = new JoystickLookAroundInputs();
  }

  private void LateUpdate()
  {
    this.transitionHandler();
    this.FollowObject();
    this.ProcessJoystickOrbiting();
  }

  private void NoTransition()
  {
  }

  public void FocusMyShip()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SystemMap3DCameraView.\u003CFocusMyShip\u003Ec__AnonStorey12B shipCAnonStorey12B = new SystemMap3DCameraView.\u003CFocusMyShip\u003Ec__AnonStorey12B();
    // ISSUE: reference to a compiler-generated field
    shipCAnonStorey12B.\u003C\u003Ef__this = this;
    Camera activeCamera = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera();
    float objectSize = Game.Me.Stats.DetectionOuterRadius * 2f;
    // ISSUE: reference to a compiler-generated field
    shipCAnonStorey12B.fillScreenDistance = this.CalculateCameraDistanceToFocusObject(activeCamera, objectSize);
    // ISSUE: reference to a compiler-generated field
    shipCAnonStorey12B.tData = this.CreateTransitionData();
    // ISSUE: reference to a compiler-generated method
    this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(shipCAnonStorey12B.\u003C\u003Em__2CC);
  }

  public void TravelToObject(ISpaceEntity target)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SystemMap3DCameraView.\u003CTravelToObject\u003Ec__AnonStorey12C objectCAnonStorey12C = new SystemMap3DCameraView.\u003CTravelToObject\u003Ec__AnonStorey12C();
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey12C.target = target;
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey12C.\u003C\u003Ef__this = this;
    if (this.IsInTransition)
      return;
    float num1 = (float) (3.14159274101257 / (180.0 / (double) SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera().fieldOfView));
    // ISSUE: reference to a compiler-generated field
    Vector3 extents = objectCAnonStorey12C.target.MeshBoundsRaw.extents;
    // ISSUE: reference to a compiler-generated field
    float num2 = Mathf.Max(extents.x, extents.y, extents.z) * objectCAnonStorey12C.target.Root.lossyScale.z;
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey12C.goodDistance = (float) (4.0 * (double) num2 / (2.0 * (double) Mathf.Tan(num1 / 2f)));
    // ISSUE: reference to a compiler-generated field
    objectCAnonStorey12C.tData = this.CreateTransitionData();
    // ISSUE: reference to a compiler-generated method
    this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(objectCAnonStorey12C.\u003C\u003Em__2CD);
  }

  private void TravelToObjectUpdate(SystemMap3DCameraView.TransitionData tData, ISpaceEntity target, float lookAtDistance)
  {
    if (target.Dead || (UnityEngine.Object) target.Root == (UnityEngine.Object) null)
    {
      this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(this.NoTransition);
      this.SetMouseOrbitVariables(this.MapCamera.transform, this.MapCamera.transform.forward * lookAtDistance);
    }
    else
    {
      LODScript componentInChildren = target.Root.GetComponentInChildren<LODScript>();
      if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null)
        componentInChildren.UpdateDistances();
      float val = (float) (((double) Time.realtimeSinceStartup - (double) tData.StartTime) / 1.0);
      Quaternion to1 = Quaternion.LookRotation(tData.InitMapCamRot * Vector3.forward, Vector3.up);
      Vector3 to2 = target.Position + tData.InitMapCamRot * Vector3.back * lookAtDistance;
      this.MapCamera.transform.position = Vector3.Lerp(tData.InitMapCamPos, to2, this.EaseOutQuart(0.0f, 1f, val));
      this.MapCamera.transform.rotation = Quaternion.Lerp(tData.InitMapCamRot, to1, this.EaseOutQuart(0.0f, 1f, val));
      if ((double) (this.MapCamera.transform.position - to2).sqrMagnitude >= 0.100000001490116)
        return;
      this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(this.NoTransition);
      this.SetMouseOrbitVariables(this.MapCamera.transform, target);
    }
  }

  public void TransitionToMap(TransitionMode transitionMode)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SystemMap3DCameraView.\u003CTransitionToMap\u003Ec__AnonStorey12D mapCAnonStorey12D = new SystemMap3DCameraView.\u003CTransitionToMap\u003Ec__AnonStorey12D();
    // ISSUE: reference to a compiler-generated field
    mapCAnonStorey12D.transitionMode = transitionMode;
    // ISSUE: reference to a compiler-generated field
    mapCAnonStorey12D.\u003C\u003Ef__this = this;
    Camera main = Camera.main;
    this.MapCamera.transform.position = main.transform.position;
    this.MapCamera.transform.rotation = main.transform.rotation;
    this.CameraSwitcher.SetActiveCamera(this.MapCamera, true);
    this.EnableOverlayCam(true);
    // ISSUE: reference to a compiler-generated field
    mapCAnonStorey12D.tData = this.CreateTransitionData();
    switch (this.cameraViewSetting)
    {
      case CameraView.Top:
        // ISSUE: reference to a compiler-generated method
        this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(mapCAnonStorey12D.\u003C\u003Em__2D0);
        break;
      case CameraView.Iso:
        if (SpaceLevel.GetLevel().PlayerIsInEmptySpace)
        {
          this.systemMap3DManager.WindowView.SetBlinkingMessageText(Tools.ParseMessage("%$bgo.sector_map.message.combat_zone_left%"));
          // ISSUE: reference to a compiler-generated method
          this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(mapCAnonStorey12D.\u003C\u003Em__2CE);
          break;
        }
        // ISSUE: reference to a compiler-generated method
        this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(mapCAnonStorey12D.\u003C\u003Em__2CF);
        break;
    }
    FacadeFactory.GetInstance().SendMessage(Message.SettingChangedCombatGui, (object) false);
    this.transitionHandler();
  }

  private void TransitionToIsoViewUpdate(TransitionMode transitionMode, SystemMap3DCameraView.TransitionData transitionData)
  {
    Vector3 intersectionPoint;
    if (!BgoUtils.RaySphereIntersection(out intersectionPoint, transitionData.InitMapCamPos, transitionData.InitMapCamRot * Vector3.back, Vector3.zero, this.MaxCamDistFromCenter))
    {
      Debug.LogWarning((object) "TransitionToIsoViewUpdate(): No intersection with outer sphere found.");
      intersectionPoint = transitionData.InitMapCamPos + transitionData.InitMapCamRot * Vector3.back * Game.Me.Ship.MeshBoundsRaw.extents.z;
    }
    float num = (float) (((double) Time.realtimeSinceStartup - (double) transitionData.StartTime) / 1.0);
    if (transitionMode == TransitionMode.Instant)
      num = 1f;
    Vector3 view = transitionData.InitMapCamRot * Vector3.forward;
    Quaternion to = Quaternion.LookRotation(transitionData.InitMapCamRot * new Vector3(view.x, 0.0f, view.z), Vector3.up);
    to.SetLookRotation(view);
    float val = Mathf.Clamp01(num);
    if ((double) val >= 0.0 && (double) val <= 1.0)
    {
      float t = this.EaseInOutQuart(0.0f, 1f, val);
      Vector3 vector3 = intersectionPoint - transitionData.InitMapCamPos;
      this.MapCamera.transform.position = transitionData.InitMapCamPos + vector3 * t;
      this.MapCamera.transform.rotation = Quaternion.Lerp(transitionData.InitMapCamRot, to, t);
    }
    if ((double) Math.Abs(val - 1f) >= 1.0 / 1000.0)
      return;
    this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(this.NoTransition);
    this.SetMouseOrbitVariables(this.MapCamera.transform, (ISpaceEntity) Game.Me.Ship);
    this.EnableOverlayCam(true);
  }

  private void TransitionToTopViewUpdate(TransitionMode transitionMode, SystemMap3DCameraView.TransitionData transitionData)
  {
    if (transitionMode == TransitionMode.Instant)
    {
      this.MapCamera.transform.position = this.TopViewPos;
      this.MapCamera.transform.rotation = this.topViewRot;
      this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(this.NoTransition);
      this.SetMouseOrbitVariables(this.MapCamera.transform, Vector3.zero);
      this.EnableOverlayCam(true);
    }
    else
    {
      float num1 = transitionData.StartTime;
      float realtimeSinceStartup = Time.realtimeSinceStartup;
      float val1 = (float) (((double) realtimeSinceStartup - (double) num1) / 0.699999988079071);
      if ((double) val1 <= 1.0)
      {
        float t = this.EaseOutQuart(0.0f, 1f, val1);
        Quaternion quaternion1 = Quaternion.LookRotation(Game.Me.Ship.Position - transitionData.InitMapCamPos, transitionData.InitMapCamRot * Vector3.up);
        Quaternion from = Quaternion.Inverse(quaternion1) * transitionData.InitMapCamRot;
        float num2 = transitionData.InitCamDistance;
        Quaternion quaternion2 = Quaternion.Lerp(quaternion1, this.topViewRot, t) * Quaternion.Lerp(from, Quaternion.identity, t);
        this.MapCamera.transform.position = quaternion2 * Vector3.back * num2 + Game.Me.Ship.Position;
        this.MapCamera.transform.rotation = quaternion2;
      }
      else
      {
        if (transitionData.TransitionPhase == 0)
        {
          transitionData.TransitionPhase = 1;
          transitionData.InitMapCamPos = this.MapCamera.transform.position;
          transitionData.InitMapCamRot = this.MapCamera.transform.rotation;
        }
        float val2 = (float) (((double) realtimeSinceStartup - (double) num1 - 0.699999988079071) / 1.0);
        if ((double) val2 >= 0.0 && (double) val2 <= 1.0)
        {
          float t = this.EaseInOutQuart(0.0f, 1f, val2);
          Vector3 vector3 = this.TopViewPos - transitionData.InitMapCamPos;
          this.MapCamera.transform.position = transitionData.InitMapCamPos + vector3 * t;
          this.MapCamera.transform.rotation = Quaternion.Lerp(transitionData.InitMapCamRot, this.topViewRot, t);
        }
        else
        {
          this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(this.NoTransition);
          this.SetMouseOrbitVariables(this.MapCamera.transform, Vector3.zero);
        }
      }
    }
  }

  private void InmapBackTransitionToTopUpdate(SystemMap3DCameraView.TransitionData transitionData)
  {
    float val = (float) (((double) Time.realtimeSinceStartup - (double) transitionData.StartTime) / 1.0);
    if ((double) val >= 0.0 && (double) val <= 1.0)
    {
      float t = this.EaseOutQuart(0.0f, 1f, val);
      Vector3 vector3 = this.TopViewPos - transitionData.InitMapCamPos;
      this.MapCamera.transform.position = transitionData.InitMapCamPos + vector3 * t;
      this.MapCamera.transform.rotation = Quaternion.Lerp(transitionData.InitMapCamRot, this.topViewRot, t);
    }
    else
    {
      this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(this.NoTransition);
      this.SetMouseOrbitVariables(this.MapCamera.transform, Vector3.zero);
    }
  }

  public void TransitionToGame(TransitionMode transitionMode)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(new SystemMap3DCameraView.\u003CTransitionToGame\u003Ec__AnonStorey12E()
    {
      transitionMode = transitionMode,
      \u003C\u003Ef__this = this,
      tData = this.CreateTransitionData()
    }.\u003C\u003Em__2D1);
    this.transitionHandler();
  }

  public void OnTransitionToGameFinished()
  {
    this.EnableOverlayCam(false);
  }

  private void TransitionToGameUpdate(TransitionMode transitionMode, SystemMap3DCameraView.TransitionData transitionData)
  {
    float num1 = transitionData.StartTime;
    float realtimeSinceStartup = Time.realtimeSinceStartup;
    Transform transform = Camera.main.transform;
    Vector3 position = transform.position;
    Quaternion rotation = transform.rotation;
    float val1 = Mathf.Min(1f, (float) (((double) realtimeSinceStartup - (double) num1) / 1.0));
    float val2 = (float) (((double) realtimeSinceStartup - (double) num1 - 1.0) / 0.699999988079071);
    if ((double) val2 >= 1.0 || transitionMode == TransitionMode.Instant)
    {
      this.CameraSwitcher.SetActiveCamera(Camera.main, true);
      FacadeFactory.GetInstance().SendMessage(Message.ApplyCombatGui);
      this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(this.NoTransition);
      this.systemMap3DManager.TransitionToGameFinished();
    }
    else
    {
      float magnitude = (Game.Me.Ship.Position - position).magnitude;
      Vector3 vector3_1 = transform.position + magnitude * transform.forward;
      if (transitionData.TransitionPhase == 0)
      {
        Vector3 vector3_2 = (this.MapCamera.transform.position - vector3_1).normalized * magnitude;
        Vector3 to = vector3_1 + vector3_2;
        this.MapCamera.transform.position = Vector3.Lerp(transitionData.InitMapCamPos, to, this.EaseOutQuart(0.0f, 1f, val1));
        Quaternion quaternion = Quaternion.LookRotation((vector3_1 - to).normalized, transitionData.InitMapCamRot * Vector3.up);
        float num2 = Quaternion.Angle(transitionData.InitMapCamRot, quaternion);
        this.MapCamera.transform.rotation = Quaternion.RotateTowards(transitionData.InitMapCamRot, quaternion, val1 * num2);
        if ((double) Math.Abs(val1 - 1f) < 1.0 / 1000.0)
        {
          transitionData.TransitionPhase = 1;
          transitionData.InitMapCamRot = this.MapCamera.transform.rotation;
          transitionData.InitMapCamPos = this.MapCamera.transform.position;
        }
      }
      if (transitionData.TransitionPhase != 1)
        return;
      this.MapCamera.transform.rotation = Quaternion.Lerp(transitionData.InitMapCamRot, rotation, this.EaseOutQuart(0.0f, 1f, val2));
      this.MapCamera.transform.position = this.MapCamera.transform.rotation * Vector3.back * magnitude + vector3_1;
    }
  }

  public void GoBackToTopView()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.transitionHandler = new SystemMap3DCameraView.TransitionDelegate(new SystemMap3DCameraView.\u003CGoBackToTopView\u003Ec__AnonStorey12F()
    {
      \u003C\u003Ef__this = this,
      tData = this.CreateTransitionData()
    }.\u003C\u003Em__2D2);
  }

  private SystemMap3DCameraView.TransitionData CreateTransitionData()
  {
    return new SystemMap3DCameraView.TransitionData() { StartTime = Time.realtimeSinceStartup, InitMapCamPos = this.MapCamera.transform.position, InitMapCamRot = this.MapCamera.transform.rotation, InitCamDistance = (this.MapCamera.transform.position - Game.Me.Ship.Position).magnitude };
  }

  private float EaseOutQuart(float start, float end, float val)
  {
    --val;
    end -= start;
    return (float) (-(double) end * ((double) val * (double) val * (double) val * (double) val - 1.0)) + start;
  }

  private float EaseOutQuad(float start, float end, float val)
  {
    end -= start;
    return (float) (-(double) end * (double) val * ((double) val - 2.0)) + start;
  }

  private float EaseInOutQuart(float start, float end, float val)
  {
    val /= 0.5f;
    end -= start;
    if ((double) val < 1.0)
      return end / 2f * val * val * val * val + start;
    val -= 2f;
    return (float) (-(double) end / 2.0 * ((double) val * (double) val * (double) val * (double) val - 2.0)) + start;
  }

  private float EaseOutExpo(float start, float end, float val)
  {
    end -= start;
    return end * (float) (-(double) Mathf.Pow(2f, (float) (-10.0 * (double) val / 1.0)) + 1.0) + start;
  }

  private float EaseOutCirc(float start, float end, float val)
  {
    --val;
    end -= start;
    return end * Mathf.Sqrt((float) (1.0 - (double) val * (double) val)) + start;
  }

  private void SetMouseOrbitVariables(Transform cameraTransform, Vector3 focusPosition)
  {
    this.SwitchToDummyFocusObject(focusPosition);
    this.SetMouseOrbitVariables(cameraTransform, (ISpaceEntity) this.dummyFocusObject);
  }

  private void SetMouseOrbitVariables(Transform cameraTransform, ISpaceEntity newFocusObject)
  {
    this.yGlobalRot = cameraTransform.rotation.eulerAngles.y;
    this.mouseOrbitLocalRot = (Quaternion.Inverse(Quaternion.Euler(0.0f, this.yGlobalRot, 0.0f) * this.topViewRot) * cameraTransform.rotation).eulerAngles;
    this.mouseOrbitFlipX = (double) Math.Abs(this.mouseOrbitLocalRot.y - 180f) < 0.100000001490116;
    this.FocusObject = newFocusObject;
  }

  private void ProcessZooming(Vector2 scrollDelta)
  {
    if (!this.IsInputAllowed)
      return;
    int meters = 0;
    float num1 = scrollDelta.y;
    if ((double) num1 < 0.0)
      meters = -500;
    else if ((double) num1 > 0.0)
      meters = 500;
    if (this.IsCameraFocusingAnObject && (UnityEngine.Object) this.FocusObject.Root.transform != (UnityEngine.Object) null)
    {
      float magnitude = (this.MapCamera.transform.position - this.FocusObject.Position).magnitude;
      float num2 = Mathf.Sin(Mathf.Clamp(0.000785f * magnitude, 0.01f, 1.57f));
      int num3 = (int) ((double) meters * (double) num2);
      float num4 = this.FocusObject.MeshBoundsRaw.extents.z * 3f * this.FocusObject.Root.transform.lossyScale.z;
      meters = (int) Mathf.Min(magnitude - num4, (float) num3);
    }
    this.Zoom(meters);
  }

  private void ProcessOrbiting(Vector2 delta)
  {
    if (!this.IsInputAllowed)
      return;
    this.yGlobalRot += delta.x * 0.25f;
    this.mouseOrbitLocalRot.x -= (float) ((double) delta.y * 0.25 * (!this.mouseOrbitFlipX ? 1.0 : -1.0));
    if (this.cameraViewSetting == CameraView.Top)
    {
      if ((double) this.mouseOrbitLocalRot.x > 180.0)
        this.mouseOrbitLocalRot.x -= 360f;
      this.mouseOrbitLocalRot.x = Mathf.Clamp(this.mouseOrbitLocalRot.x, -180f, 0.0f);
    }
    this.MapCamera.transform.rotation = Quaternion.Euler(0.0f, this.yGlobalRot, 0.0f) * this.topViewRot * Quaternion.Euler(this.mouseOrbitLocalRot);
    Vector3 position = this.FocusObject.Position;
    this.MapCamera.transform.position = position + (this.MapCamera.transform.position - position).magnitude * -this.MapCamera.transform.forward;
  }

  private void SwitchToDummyFocusObject(Vector3 newPosition)
  {
    this.dummyFocusObject.Position = newPosition;
    this.FocusObject = (ISpaceEntity) this.dummyFocusObject;
  }

  private void ProcessLateralMovement(Vector2 delta)
  {
    if (this.FocusObject != this.dummyFocusObject)
      this.SwitchToDummyFocusObject(this.FocusObject.Position);
    Vector3 vector3 = -20f * delta.x * this.MapCamera.transform.right + -20f * delta.y * this.MapCamera.transform.up;
    this.MapCamera.transform.position += vector3;
    this.FocusObject.Position += vector3;
  }

  private void Zoom(int meters)
  {
    Vector3 position = this.MapCamera.transform.position;
    Vector3 newPosition = this.MapCamera.transform.forward * (float) meters + this.MapCamera.transform.position;
    if ((double) newPosition.magnitude >= (double) this.MaxCamDistFromCenter * 1.33000004291534 && (double) newPosition.magnitude >= (double) position.magnitude)
      return;
    this.MapCamera.transform.position = newPosition;
    if ((double) Vector3.Dot(this._focusObject.Position - newPosition, this.MapCamera.transform.forward) > 0.0)
      return;
    this.SwitchToDummyFocusObject(newPosition);
  }

  public void SetSectorSize(float largestSideOfBox)
  {
    this._sectorSize = largestSideOfBox;
  }

  public void ApplySetting(UserSetting setting, object data)
  {
    if (setting != UserSetting.SystemMap3DCameraView)
      return;
    this.cameraViewSetting = (CameraView) data;
  }

  private void SetupCameras()
  {
    GameObject gameObject1 = UnityEngine.Object.Instantiate<GameObject>(Resources.Load<GameObject>("Cameras/SystemMap3DCamera (Scene)"));
    gameObject1.transform.parent = this.transform;
    this.mapCamera = gameObject1.GetComponent<Camera>();
    this.mapCameraTransform = gameObject1.transform;
    GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(Resources.Load<GameObject>("Cameras/SystemMap3DCamera (Icons)"));
    gameObject2.transform.parent = gameObject1.transform;
    this.iconCamera = gameObject2.GetComponent<Camera>();
  }

  public void DisableAllCameras()
  {
    this.EnableOverlayCam(false);
    this.mapCamera.enabled = false;
  }

  private void EnableOverlayCam(bool enable)
  {
    this.IconCamera.enabled = enable;
    this.IconCamera.fieldOfView = this.MapCamera.fieldOfView;
  }

  private void FollowObject()
  {
    if (!this.IsCameraFocusingAnObject || (MulticastDelegate) this.transitionHandler != (MulticastDelegate) new SystemMap3DCameraView.TransitionDelegate(this.NoTransition))
      return;
    if (this._focusObjectChanged)
    {
      this._focusObjectChanged = false;
      this._lastFocusPosition = this._focusObject.Position;
    }
    if (!this.systemMap3DManager.IconView.IsTargetInDradis(this._focusObject))
    {
      this.SwitchToDummyFocusObject(this._focusObject.Position);
      this.systemMap3DManager.WindowView.SetBlinkingMessageText(Tools.ParseMessage("%$bgo.sector_map.message.chased_target_left%"));
    }
    if (this._focusObject.Dead)
      this.SwitchToDummyFocusObject(this._focusObject.Position);
    Vector3 vector3 = this._focusObject.Position - this._lastFocusPosition;
    this._lastFocusPosition = this._focusObject.Position;
    if (!((UnityEngine.Object) this.MapCamera != (UnityEngine.Object) null))
      return;
    this.MapCamera.transform.position += vector3;
  }

  private float CalculateCameraDistanceToFocusObject(Camera cam, float objectSize)
  {
    float num = (float) (3.14159274101257 / (180.0 / (double) cam.fieldOfView));
    return objectSize / (2f * Mathf.Tan(num / 2f));
  }

  private void SetupTriggerEvents()
  {
    this.AddTriggerEvent(EventTriggerType.Drag, (UnityAction<BaseEventData>) (b =>
    {
      PointerEventData pointerEventData = (PointerEventData) b;
      if (pointerEventData.button == PointerEventData.InputButton.Left)
        this.ProcessOrbiting(pointerEventData.delta);
      if (pointerEventData.button != PointerEventData.InputButton.Middle)
        return;
      this.ProcessLateralMovement(pointerEventData.delta);
    }));
    this.AddTriggerEvent(EventTriggerType.Scroll, (UnityAction<BaseEventData>) (b => this.ProcessZooming(((PointerEventData) b).scrollDelta)));
  }

  private void AddTriggerEvent(EventTriggerType triggerType, UnityAction<BaseEventData> call)
  {
    EventTrigger.Entry entry = new EventTrigger.Entry();
    entry.eventID = triggerType;
    entry.callback = new EventTrigger.TriggerEvent();
    entry.callback.AddListener(call);
    this.systemMap3DManager.WindowView.Nav3DAreaEventTrigger.triggers.Add(entry);
  }

  public void Destroy()
  {
  }

  public void OnJoystickLookInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    if ((double) Mathf.Abs(magnitude) < (double) JoystickSetup.DeadZone)
      magnitude = 0.0f;
    this._joystickLookAroundInputs.UpdateInputValue(action, magnitude);
  }

  private void ProcessJoystickOrbiting()
  {
    Vector2 lookInputs = this._joystickLookAroundInputs.GetLookInputs();
    lookInputs.y = -lookInputs.y;
    this.ProcessOrbiting(lookInputs * 2f);
  }

  private class TransitionData
  {
    public float StartTime;
    public Vector3 InitMapCamPos;
    public Quaternion InitMapCamRot;
    public float InitCamDistance;
    public int TransitionPhase;
  }

  private delegate void TransitionDelegate();
}
