﻿// Decompiled with JetBrains decompiler
// Type: NameChangeRequestAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using Bigpoint.Core.Mvc.View;

public class NameChangeRequestAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    CharacterService characterService = ((CharacterServiceDataProvider) this.OwnerFacade.FetchDataProvider("CharacterServiceDataProvider")).GetCharacterService(CharacterServiceId.NameChange);
    if (!characterService.Eligible)
    {
      NotifyBoxUi notifyBox = MessageBoxFactory.CreateNotifyBox(BsgoCanvas.CharacterMenu);
      notifyBox.SetContent((OnMessageBoxClose) (param0 => FacadeFactory.GetInstance().SendMessage(Message.CancelNameChange)), "%$bgo.character_menu.name_change.title%", "%$bgo.character_menu.name_change.not_available%", 0U);
      notifyBox.ShowTimer(false);
    }
    else if (!characterService.CooldownExpired)
    {
      NotifyBoxUi notifyBox = MessageBoxFactory.CreateNotifyBox(BsgoCanvas.CharacterMenu);
      notifyBox.SetContent((OnMessageBoxClose) (param0 => FacadeFactory.GetInstance().SendMessage(Message.CancelNameChange)), "%$bgo.character_menu.name_change.title%", "%$bgo.character_menu.name_change.cooldown%", characterService.RemainingTimeInSeconds);
      notifyBox.ShowTimer(true);
    }
    else
      this.OwnerFacade.AttachView((IView<Message>) new NameChangeMediator(characterService));
  }
}
