﻿// Decompiled with JetBrains decompiler
// Type: LoginLevel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.CrashLog;

public class LoginLevel : GameLevel
{
  public const uint SERVER_REVISION = 4578;
  private LoginScreen loginScreen;

  public override GameLevelType LevelType
  {
    get
    {
      return GameLevelType.LoginLevel;
    }
  }

  public override bool IsIngameLevel
  {
    get
    {
      return false;
    }
  }

  public string AssetMapVersion { get; set; }

  public static LoginLevel GetLevel()
  {
    return GameLevel.Instance as LoginLevel;
  }

  protected override void AddLoadingScreenDependencies()
  {
  }

  protected override void Start()
  {
    Game.Funnel.ReportActivity(Activity.GameclientStarted);
    this.loginScreen = this.gameObject.AddComponent<LoginScreen>();
    base.Start();
  }

  public bool CheckServer()
  {
    return 4578 == (int) Game.ServerRevision;
  }

  public void OnInit()
  {
    CrashReporting.Init("78b78024-993c-4a91-b2b8-121242a8dc8c", Game.Revision + "(" + Application.unityVersion + ")", ((int) Game.Me.ServerID).ToString() + string.Empty);
    this.StartCoroutine(this.Loop());
  }

  public void OnLogin()
  {
    SceneProtocol.GetProtocol().RequestQuitLogin();
  }

  public void OnWait(uint slots)
  {
    this.loginScreen.OnWait(slots);
  }

  [DebuggerHidden]
  private IEnumerator Loop()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LoginLevel.\u003CLoop\u003Ec__Iterator2A() { \u003C\u003Ef__this = this };
  }

  private string[] GetBundlesFor(string language)
  {
    return new string[7]{ "Locale/" + language + "/locale_0", "Locale/" + language + "/locale_1", "Locale/" + language + "/locale_2", "Locale/" + language + "/locale_cdb_objects", "Locale/" + language + "/locale_layouts", "Locale/" + language + "/locale_dialogs", "Locale/" + language + "/locale_sector_events" };
  }

  public bool IsConnected()
  {
    return Game.GetProtocolManager().GetConnectionState() == Connection.State.Connected;
  }

  private void ReportHWStats()
  {
    Application.ExternalCall("GameStart_HW_Stats", (object) SystemInfo.operatingSystem, (object) SystemInfo.processorType, (object) SystemInfo.processorCount, (object) SystemInfo.systemMemorySize, (object) SystemInfo.graphicsMemorySize, (object) SystemInfo.graphicsDeviceName, (object) SystemInfo.graphicsDeviceVendor, (object) SystemInfo.graphicsDeviceID, (object) SystemInfo.graphicsDeviceVendorID, (object) SystemInfo.graphicsDeviceVersion, (object) SystemInfo.graphicsShaderLevel, (object) 0, (object) SystemInfo.supportsShadows, (object) SystemInfo.supportsRenderTextures, (object) SystemInfo.supportsImageEffects);
    FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new LogToEventStreamMessage("client.system.stats", new object[28]
    {
      (object) "operatingSystem",
      (object) SystemInfo.operatingSystem,
      (object) "processorType",
      (object) SystemInfo.processorType,
      (object) "processorCount",
      (object) SystemInfo.processorCount,
      (object) "systemMemorySize",
      (object) SystemInfo.systemMemorySize,
      (object) "graphicsMemorySize",
      (object) SystemInfo.graphicsMemorySize,
      (object) "graphicsDeviceName",
      (object) SystemInfo.graphicsDeviceName,
      (object) "graphicsDeviceVendor",
      (object) SystemInfo.graphicsDeviceVendor,
      (object) "graphicsDeviceVersion",
      (object) SystemInfo.graphicsDeviceVersion,
      (object) "graphicsShaderLevel",
      (object) SystemInfo.graphicsShaderLevel,
      (object) "supportsRenderTextures",
      (object) SystemInfo.supportsRenderTextures,
      (object) "supportsImageEffects",
      (object) SystemInfo.supportsImageEffects,
      (object) "unityVersion",
      (object) Application.unityVersion,
      (object) "systemLanguage",
      (object) Application.systemLanguage.ToString(),
      (object) "desktopResolution",
      (object) (Screen.currentResolution.width.ToString() + "x" + (object) Screen.currentResolution.height)
    }));
  }
}
