﻿// Decompiled with JetBrains decompiler
// Type: Properties
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class Properties
{
  protected readonly Dictionary<ObjectStat, float> props = new Dictionary<ObjectStat, float>((IEqualityComparer<ObjectStat>) new PropertiesComparer());
  private readonly float OBFUSCATION_CONST;

  public bool Available
  {
    get
    {
      return this.props.Count > 0;
    }
  }

  public Properties()
  {
    this.OBFUSCATION_CONST = (float) UnityEngine.Random.Range(1, 10000);
  }

  public float GetObfuscatedFloat(ObjectStat key)
  {
    float num;
    if (this.props.TryGetValue(key, out num))
      return (float) Math.Round((double) (num + this.OBFUSCATION_CONST), 3);
    return 0.0f;
  }

  public void SetObfuscatedFloat(ObjectStat key, float value)
  {
    this.props[key] = value - this.OBFUSCATION_CONST;
  }
}
