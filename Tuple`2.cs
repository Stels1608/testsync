﻿// Decompiled with JetBrains decompiler
// Type: Tuple`2
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class Tuple<T1, T2>
{
  public T1 First { get; set; }

  public T2 Second { get; set; }

  public Tuple(T1 first, T2 second)
  {
    this.First = first;
    this.Second = second;
  }
}
