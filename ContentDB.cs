﻿// Decompiled with JetBrains decompiler
// Type: ContentDB
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using CouchDB;
using Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class ContentDB
{
  private static readonly Dictionary<string, string> fileDict = new Dictionary<string, string>();
  public static List<string> updatedFiles = new List<string>();

  public static string ContentDBPath
  {
    get
    {
      return BSGOConfig.ContentDBPath;
    }
  }

  public static bool DocumentExists(string docid)
  {
    string path = ContentDB.ContentDBPath + docid + ".json";
    if (ContentDB.fileDict.ContainsKey(docid))
      path = ContentDB.fileDict[docid] + docid + ".json";
    return File.Exists(path);
  }

  public static JsonData CreateDocument(JsonData json, string name)
  {
    return ContentDB.CreateDocumentAtFilePath(json, name, ContentDB.ContentDBPath);
  }

  public static JsonData CreateDocumentAtFilePath(JsonData json, string name, string filePath)
  {
    if (json.Object.ContainsKey("_rev"))
      json.Object.Remove("_rev");
    json.Object["_id"] = new JsonData(name);
    File.Create(filePath + name + ".json").Close();
    json.SortKeys();
    return ContentDB.UpdateDocumentRawAtFilepath(name, json, filePath, ".json");
  }

  public static void CreateDocument<T>(T obj, string name)
  {
    JsonData document = ContentDB.CreateDocument(JsonSerializator.Serialize((object) obj), name);
    if (!((object) obj is CouchDocumentBase))
      return;
    ((object) obj as CouchDocumentBase).SetId(document.Object["_id"].String);
  }

  public static void CreateDocument<T>(T obj, string name, string path)
  {
    JsonData documentAtFilePath = ContentDB.CreateDocumentAtFilePath(JsonSerializator.Serialize((object) obj), name, path);
    if (!((object) obj is CouchDocumentBase))
      return;
    ((object) obj as CouchDocumentBase).SetId(documentAtFilePath.Object["_id"].String);
  }

  public static T GetDocument<T>(string docid)
  {
    return ContentDB.GetDocument<T>(docid, "/");
  }

  public static T GetDocument<T>(string docid, string xpath)
  {
    return JsonSerializator.Deserialize<T>(ContentDB.GetDocumentRaw(docid, xpath));
  }

  public static T GetDocument<T>(string docid, string xpath, T objectToFill)
  {
    JsonData documentRaw = ContentDB.GetDocumentRaw(docid, xpath);
    return (T) JsonSerializator.DeserializeObject(objectToFill.GetType(), documentRaw, (object) objectToFill);
  }

  public static JsonData GetDocumentRaw(string docid, string xpath)
  {
    return XPathTraverser.Traverse(ContentDB.GetDocumentRaw(docid), xpath);
  }

  public static JsonData GetDocumentRaw(string docid)
  {
    if (ContentDB.fileDict.ContainsKey(docid))
      return ContentDB.GetDocumentRawFromFilepath(docid, ContentDB.fileDict[docid], ".json");
    ContentDB.GetFilesFromAllFolders(docid + ".json");
    if (ContentDB.fileDict.ContainsKey(docid))
      return ContentDB.GetDocumentRawFromFilepath(docid, ContentDB.fileDict[docid], ".json");
    return ContentDB.GetDocumentRawFromFilepath(docid, ContentDB.ContentDBPath, ".json");
  }

  public static JsonData GetDocumentRawFromFilepath(string filename, string filepath, string fileExtension = ".json")
  {
    FileStream fileStream = File.OpenRead(filepath + filename + fileExtension);
    byte[] numArray = new byte[1024];
    UTF8Encoding utF8Encoding = new UTF8Encoding(true);
    string json = string.Empty;
    while (fileStream.Read(numArray, 0, numArray.Length) > 0)
      json += utF8Encoding.GetString(numArray);
    fileStream.Close();
    JsonData jsonData;
    try
    {
      jsonData = JsonReader.Parse(json);
    }
    catch (Exception ex)
    {
      Debug.Log((object) ("PARSE ERROR == in " + filename + "\n" + ex.ToString()));
      Debug.Log((object) json);
      throw ex;
    }
    jsonData.FileID = filename;
    return jsonData;
  }

  public static JsonData GetDocumentRawFromTextAsset(TextAsset textAsset, string filename)
  {
    JsonData jsonData = JsonReader.Parse(textAsset.text);
    jsonData.FileID = filename;
    return jsonData;
  }

  public static JsonData UpdateDocument(string docid, string xpath, object obj)
  {
    JsonData data = JsonSerializator.Serialize(obj);
    JsonData documentRaw = ContentDB.GetDocumentRaw(docid);
    CouchServer.MergeJson(documentRaw, xpath, data);
    return ContentDB.UpdateDocumentRaw(docid, documentRaw);
  }

  public static JsonData UpdateDocumentJson(string docid, string xpath, JsonData obj)
  {
    if (ContentDB.fileDict.ContainsKey(docid))
      return ContentDB.UpdateDocumentJsonAtFilepath(docid, xpath, obj, ContentDB.fileDict[docid], ".json");
    return ContentDB.UpdateDocumentJsonAtFilepath(docid, xpath, obj, ContentDB.ContentDBPath, ".json");
  }

  public static JsonData UpdateDocumentJsonAtFilepath(string docid, string xpath, JsonData obj, string filepath, string fileExtension = ".json")
  {
    JsonData documentRawFromFilepath = ContentDB.GetDocumentRawFromFilepath(docid, filepath, fileExtension);
    CouchServer.MergeJson(documentRawFromFilepath, xpath, obj);
    return ContentDB.UpdateDocumentRawAtFilepath(docid, documentRawFromFilepath, filepath, fileExtension);
  }

  public static JsonData UpdateDocumentRawAtFilepath(string docid, JsonData json, string filepath, string fileExtension = ".json")
  {
    json.SortKeys();
    FileStream fileStream = File.Create(filepath + docid + fileExtension);
    byte[] bytes = new UTF8Encoding(true).GetBytes(new JsonFormatter(json.ToJsonString(string.Empty)).Format());
    fileStream.Write(bytes, 0, bytes.Length);
    fileStream.Close();
    ContentDB.updatedFiles.Add(docid);
    return ContentDB.GetDocumentRawFromFilepath(docid, filepath, fileExtension);
  }

  public static JsonData UpdateDocumentRaw(string docid, JsonData json)
  {
    if (ContentDB.fileDict.ContainsKey(docid))
      return ContentDB.UpdateDocumentRawAtFilepath(docid, json, ContentDB.fileDict[docid], ".json");
    return ContentDB.UpdateDocumentRawAtFilepath(docid, json, ContentDB.ContentDBPath, ".json");
  }

  public static void ReplaceDocument<T>(T obj, string name)
  {
    JsonData json = JsonSerializator.Serialize((object) obj);
    if (json.Object.ContainsKey("_id"))
      json.Object.Remove("_id");
    if (json.Object.ContainsKey("_rev"))
      json.Object.Remove("_rev");
    if (ContentDB.DocumentExists(name))
      ContentDB.UpdateDocumentJson(name, "/", json);
    else
      ContentDB.CreateDocument(json, name);
  }

  public static void DeleteDocument(string name)
  {
    File.Delete(ContentDB.ContentDBPath + name + ".json");
  }

  public static List<string> GetFilesFromAllFolders(string pattern)
  {
    return ContentDB.GetFilesFromAllFolders(ContentDB.ContentDBPath, pattern, string.Empty);
  }

  public static List<string> GetFilesFromAllFolders(string folder, string pattern, string direc = "")
  {
    List<string> stringList = new List<string>();
    string[] directories = Directory.GetDirectories(folder);
    foreach (string file in Directory.GetFiles(folder, pattern))
    {
      if (folder != ContentDB.ContentDBPath && !ContentDB.fileDict.ContainsKey(Path.GetFileNameWithoutExtension(file)))
        ContentDB.fileDict.Add(Path.GetFileNameWithoutExtension(file), Path.GetDirectoryName(file) + "\\");
      stringList.Add(Path.GetFileNameWithoutExtension(file));
    }
    foreach (string folder1 in directories)
      stringList.AddRange((IEnumerable<string>) ContentDB.GetFilesFromAllFolders(folder1, pattern, string.Empty));
    return stringList;
  }

  public static string[] GetFiles(string pattern, string dir)
  {
    Debug.Log((object) (ContentDB.ContentDBPath + dir));
    return ContentDB.GetFilesFromAllFolders(ContentDB.ContentDBPath + dir, pattern, string.Empty).ToArray();
  }

  public static string[] GetFiles(string pattern)
  {
    return ContentDB.GetFilesFromAllFolders(pattern).ToArray();
  }

  public static string[] GetFilesWithUndefined(string pattern)
  {
    string[] files = Directory.GetFiles(ContentDB.ContentDBPath, pattern);
    string[] strArray = new string[files.Length + 1];
    strArray[0] = "undefined";
    for (int index = 0; index < files.Length; ++index)
    {
      Debug.Log((object) files[index]);
      strArray[index + 1] = Path.GetFileNameWithoutExtension(files[index]);
    }
    return strArray;
  }

  public static List<T> GetFilesWithMatchingParameters<T>(string parameter, string value)
  {
    return ContentDB.GetFilesWithMatchingParameters<T>("*", parameter, value);
  }

  public static List<T> GetFilesWithMatchingParameters<T>(string pattern, string parameter, string value)
  {
    List<T> objList = new List<T>();
    string[] files = ContentDB.GetFiles(pattern);
    int length = files.Length;
    for (int index = 0; index < length; ++index)
    {
      JsonData documentRaw = ContentDB.GetDocumentRaw(files[index]);
      if (documentRaw.Object.ContainsKey(parameter) && documentRaw.Object[parameter].IsString && documentRaw.Object[parameter].String.ToLower() == value.ToLower())
        objList.Add(JsonSerializator.Deserialize<T>(documentRaw));
    }
    return objList;
  }

  public static List<T> GetFilesWithParameter<T>(string pattern, string parameter)
  {
    List<T> objList = new List<T>();
    foreach (string file in ContentDB.GetFiles(pattern))
    {
      JsonData documentRaw = ContentDB.GetDocumentRaw(file);
      if (documentRaw.Object.ContainsKey(parameter))
        objList.Add(JsonSerializator.Deserialize<T>(documentRaw));
    }
    return objList;
  }
}
