﻿// Decompiled with JetBrains decompiler
// Type: HubMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class HubMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "HubMediator";
  private GameObject hubMenuObject;
  private HubMenuWindow hubMenu;
  private GameObject undockButton;

  public HubMediator()
    : base("HubMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.LevelLoaded);
    this.AddMessageInterest(Message.GuiToggleHubMenuVisibility);
    this.AddMessageInterest(Message.LoadNewLevel);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.LoadNewLevel:
        this.ShowHubMenu(false);
        break;
      case Message.LevelLoaded:
        if ((GameLevel) message.Data is RoomLevel)
        {
          this.ShowHubMenu(true);
          break;
        }
        this.ShowHubMenu(false);
        break;
      case Message.UiCreated:
        this.CreateHubMenu();
        this.CreateUndockButton();
        break;
      case Message.GuiToggleHubMenuVisibility:
        this.ShowHubMenu((bool) message.Data);
        break;
      default:
        Debug.LogError((object) ("ERROR IN HUB MEDIATOR Unknown Message = " + (object) message.Id));
        break;
    }
  }

  private void CreateUndockButton()
  {
    GuiHookPoint objectOfType = Object.FindObjectOfType<GuiHookPoint>();
    if ((Object) objectOfType == (Object) null)
    {
      Debug.LogError((object) "Could not locate GuiHookPoint for attaching the UndockButton");
    }
    else
    {
      this.undockButton = NGUITools.AddChild(objectOfType.gameObject, Resources.Load<GameObject>("GUI/gui_2013/ui_elements/hub_menu/UndockButton"));
      this.undockButton.SetActive(false);
    }
  }

  private void ShowHubMenu(bool show)
  {
    if ((Object) this.hubMenuObject == (Object) null)
      return;
    this.hubMenuObject.SetActive(show);
    this.undockButton.SetActive(show);
    if (show)
      this.hubMenu.Open();
    else
      this.hubMenu.Close();
  }

  private void CreateHubMenu()
  {
    if ((Object) this.hubMenuObject != (Object) null)
      return;
    this.hubMenuObject = (GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/ui_elements/hub_menu/HubMenuWindow"));
    this.hubMenu = this.hubMenuObject.GetComponent<HubMenuWindow>();
    if ((Object) this.hubMenu == (Object) null)
      Debug.LogError((object) "HubMenuWindow was not registered correctly");
    (this.OwnerFacade.FetchDataProvider("GuiDataProvider") as GuiDataProvider).AddWindow((WindowWidget) this.hubMenu);
  }
}
