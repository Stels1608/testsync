﻿// Decompiled with JetBrains decompiler
// Type: EventStream
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class EventStream : MonoBehaviour
{
  private readonly ArrayList events = new ArrayList();
  public static uint ProjectID;
  public static uint UserID;
  public static string SessionID;
  public static string TrackingID;
  private static EventStream instance;
  private Hashtable context;
  private Dictionary<string, string> headers;

  public static EventStream Instance
  {
    get
    {
      if ((UnityEngine.Object) EventStream.instance == (UnityEngine.Object) null)
      {
        EventStream.instance = new GameObject("EventStream").AddComponent<EventStream>();
        UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) EventStream.instance.gameObject);
      }
      return EventStream.instance;
    }
  }

  private long TimestampNow
  {
    get
    {
      return (long) (DateTime.Now - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
    }
  }

  private Hashtable Context
  {
    get
    {
      Hashtable hashtable = this.context;
      if (hashtable != null)
        return hashtable;
      return this.context = new Hashtable() { { (object) "pid", (object) EventStream.ProjectID }, { (object) "uid", (object) EventStream.UserID } };
    }
  }

  private Dictionary<string, string> Headers
  {
    get
    {
      Dictionary<string, string> dictionary = this.headers;
      if (dictionary != null)
        return dictionary;
      return this.headers = new Dictionary<string, string>() { { "Content-Type", "text/json" } };
    }
  }

  private void Start()
  {
    this.InvokeRepeating("SendAll", 2f, 10f);
  }

  public void Track(Hashtable attributes)
  {
    attributes[(object) "time"] = (object) this.TimestampNow;
    attributes[(object) "generator"] = (object) "bsgo.client.unity";
    this.events.Add((object) attributes);
  }

  private void SendAll()
  {
    if (EventStream.ProjectID <= 0U)
      EventStream.ProjectID = Game.Me.ChatProjectID;
    if (EventStream.UserID <= 0U)
      EventStream.UserID = Game.Me.ServerID;
    if (this.events.Count == 0 || (int) EventStream.ProjectID == 0 || (int) EventStream.UserID == 0)
      return;
    Hashtable hashtable = new Hashtable();
    hashtable[(object) "context"] = (object) this.Context;
    hashtable[(object) "time"] = (object) this.TimestampNow;
    hashtable[(object) "events"] = (object) this.events;
    string content = JSON.JsonEncode((object) hashtable);
    this.events.Clear();
    this.StartCoroutine(this.Upload(content));
  }

  [DebuggerHidden]
  private IEnumerator Upload(string content)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EventStream.\u003CUpload\u003Ec__Iterator1C() { content = content, \u003C\u0024\u003Econtent = content, \u003C\u003Ef__this = this };
  }
}
