﻿// Decompiled with JetBrains decompiler
// Type: MotionAnalyzerBackwards
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class MotionAnalyzerBackwards : IMotionAnalyzer
{
  public MotionAnalyzer orig;
  public LegCycleData[] m_cycles;
  public float m_cycleOffset;

  public override LegCycleData[] cycles
  {
    get
    {
      return this.m_cycles;
    }
  }

  public override int samples
  {
    get
    {
      return this.orig.samples;
    }
  }

  public override Vector3 cycleDirection
  {
    get
    {
      return -this.orig.cycleDirection;
    }
  }

  public override float cycleDistance
  {
    get
    {
      return this.orig.cycleDistance;
    }
  }

  public override Vector3 cycleVector
  {
    get
    {
      return -this.orig.cycleVector;
    }
  }

  public override float cycleDuration
  {
    get
    {
      return this.orig.cycleDuration;
    }
  }

  public override float cycleSpeed
  {
    get
    {
      return this.orig.cycleSpeed;
    }
  }

  public override Vector3 cycleVelocity
  {
    get
    {
      return -this.orig.cycleVelocity;
    }
  }

  public override float cycleOffset
  {
    get
    {
      return this.m_cycleOffset;
    }
    set
    {
      this.m_cycleOffset = value;
    }
  }

  public override Vector3 GetFlightFootPosition(int leg, float flightTime, int phase)
  {
    Vector3 flightFootPosition = this.orig.GetFlightFootPosition(leg, 1f - flightTime, 2 - phase);
    return new Vector3(-flightFootPosition.x, flightFootPosition.y, 1f - flightFootPosition.z);
  }

  public override void Analyze(GameObject o)
  {
    GameObject gameObject = o;
    this.animation = this.orig.animation;
    this.name = this.animation.name + "_bk";
    this.motionType = this.orig.motionType;
    this.motionGroup = this.orig.motionGroup;
    int length = (gameObject.GetComponent(typeof (LegController)) as LegController).legs.Length;
    this.m_cycles = new LegCycleData[length];
    for (int index = 0; index < length; ++index)
    {
      this.cycles[index] = new LegCycleData();
      this.cycles[index].cycleCenter = this.orig.cycles[index].cycleCenter;
      this.cycles[index].cycleScaling = this.orig.cycles[index].cycleScaling;
      this.cycles[index].cycleDirection = -this.orig.cycles[index].cycleDirection;
      this.cycles[index].stanceTime = 1f - this.orig.cycles[index].stanceTime;
      this.cycles[index].liftTime = 1f - this.orig.cycles[index].landTime;
      this.cycles[index].liftoffTime = 1f - this.orig.cycles[index].strikeTime;
      this.cycles[index].postliftTime = 1f - this.orig.cycles[index].prelandTime;
      this.cycles[index].prelandTime = 1f - this.orig.cycles[index].postliftTime;
      this.cycles[index].strikeTime = 1f - this.orig.cycles[index].liftoffTime;
      this.cycles[index].landTime = 1f - this.orig.cycles[index].liftTime;
      this.cycles[index].cycleDistance = this.orig.cycles[index].cycleDistance;
      this.cycles[index].stancePosition = this.orig.cycles[index].stancePosition;
      this.cycles[index].heelToetipVector = this.orig.cycles[index].heelToetipVector;
    }
  }
}
