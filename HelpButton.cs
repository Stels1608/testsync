﻿// Decompiled with JetBrains decompiler
// Type: HelpButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using UnityEngine;

public class HelpButton : TabButtonWidget, ILocalizeable
{
  public string tutname = string.Empty;
  public string title = string.Empty;
  public string main = string.Empty;
  public string subMain = string.Empty;
  public string step1 = string.Empty;
  public string step2 = string.Empty;
  public string step3 = string.Empty;
  public string spriteMainName = string.Empty;
  public string sprite1Name = string.Empty;
  public string sprite2Name = string.Empty;
  public string sprite3Name = string.Empty;
  public HelpScreenType helpType;
  public HelpContent parent;
  public UISprite DefaultImg;
  public UILabel tLabel;
  public UILabel titleLabel;
  public UILabel mainLabel;
  public UILabel subMainLabel;
  public UILabel step1Label;
  public UILabel step2Label;
  public UILabel step3Label;
  public UISprite mainSprite;
  public UISprite step1Sprite;
  public UISprite step2Sprite;
  public UISprite step3Sprite;
  private bool isLoca;

  public override void Start()
  {
    base.Start();
    this.ActiveIsOverlay = false;
    this.ReloadLanguageData();
  }

  public override void ShowTabContent()
  {
    if (!this.isLoca)
      this.ReloadLanguageData();
    if (this.parent.ShowTutorial && !this.parent.CompletedTutorials.Contains(this.helpType))
    {
      this.parent.CompletedTutorials.Add(this.helpType);
      FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.CompletedTutorials, (object) this.parent.CompletedTutorials, true));
    }
    if ((Object) this.titleLabel != (Object) null)
      this.titleLabel.text = this.title;
    if ((Object) this.mainLabel != (Object) null)
      this.mainLabel.text = this.main;
    if ((Object) this.subMainLabel != (Object) null)
      this.subMainLabel.text = this.subMain;
    if ((Object) this.step1Label != (Object) null)
      this.step1Label.text = this.step1;
    if ((Object) this.step2Label != (Object) null)
      this.step2Label.text = this.step2;
    if ((Object) this.step3Label != (Object) null)
      this.step3Label.text = this.step3;
    if ((Object) this.mainSprite != (Object) null)
    {
      this.mainSprite.spriteName = this.spriteMainName;
      this.mainSprite.MakePixelPerfect();
    }
    if ((Object) this.step1Sprite != (Object) null)
    {
      this.step1Sprite.spriteName = this.sprite1Name;
      this.step1Sprite.MakePixelPerfect();
    }
    if ((Object) this.step2Sprite != (Object) null)
    {
      this.step2Sprite.spriteName = this.sprite2Name;
      this.step2Sprite.MakePixelPerfect();
    }
    if (!((Object) this.step3Sprite != (Object) null))
      return;
    this.step3Sprite.spriteName = this.sprite3Name;
    this.step3Sprite.MakePixelPerfect();
  }

  protected override void Set()
  {
    base.Set();
    if (!((Object) null != (Object) this.DefaultImg))
      return;
    this.DefaultImg.enabled = !this.isActive;
  }

  public void ReloadLanguageData()
  {
    if (this.isLoca)
      return;
    this.isLoca = true;
    this.tutname = Tools.ParseMessage(this.tutname);
    this.tLabel.text = this.tutname;
    this.name += this.tutname;
    this.title = Tools.ParseMessage(this.title).ToUpper();
    this.main = Tools.ParseMessage(this.main);
    this.subMain = Tools.ParseMessage(this.subMain);
    this.step1 = Tools.ParseMessage(this.step1);
    this.step2 = Tools.ParseMessage(this.step2);
    this.step3 = Tools.ParseMessage(this.step3);
  }
}
