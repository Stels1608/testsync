﻿// Decompiled with JetBrains decompiler
// Type: WofGameNukeState
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WofGameNukeState : MonoBehaviour
{
  public GameObject activeState;
  public GameObject defaultState;
  public GameObject usedState;
  public bool isActiveOnStart;

  public void Awake()
  {
    if (this.isActiveOnStart)
      this.SetActive();
    else
      this.SetDefault();
  }

  public void SetActive()
  {
    this.activeState.SetActive(true);
    this.defaultState.SetActive(false);
    this.usedState.SetActive(false);
  }

  public void SetDefault()
  {
    this.activeState.SetActive(false);
    this.defaultState.SetActive(true);
    this.usedState.SetActive(false);
  }

  public void SetUsed()
  {
    this.activeState.SetActive(false);
    this.defaultState.SetActive(false);
    this.usedState.SetActive(true);
  }
}
