﻿// Decompiled with JetBrains decompiler
// Type: RevolvingOverrider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RevolvingOverrider : Revolving
{
  private bool childRevolvingDestroyed;

  protected override void Awake()
  {
    base.Awake();
  }

  protected override void Update()
  {
    if (!this.childRevolvingDestroyed && this.transform.childCount > 0 && this.TryChildRevolvingRemoval())
      this.childRevolvingDestroyed = true;
    this.Rotate();
  }

  private bool TryChildRevolvingRemoval()
  {
    foreach (Revolving componentsInChild in this.GetComponentsInChildren<Revolving>())
    {
      if ((Object) componentsInChild != (Object) this)
      {
        componentsInChild.enabled = false;
        this.SpaceObject = componentsInChild.SpaceObject;
        Object.Destroy((Object) componentsInChild);
        return true;
      }
    }
    return false;
  }
}
