﻿// Decompiled with JetBrains decompiler
// Type: DebugPlayerInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Text;
using UnityEngine;

public class DebugPlayerInfo : DebugBehaviour<DebugPlayerInfo>
{
  private Vector2 scroll = new Vector2();
  private string info = string.Empty;

  private void Start()
  {
    this.windowID = 11;
    this.windowRect = new Rect((float) ((Screen.width - 120) / 2), (float) ((Screen.height - 120) / 2), 300f, 400f);
    this.SetSize(300f, 400f);
  }

  protected override void WindowFunc()
  {
    TextAnchor alignment = GUI.skin.box.alignment;
    GUI.skin.box.alignment = TextAnchor.MiddleLeft;
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    GUILayout.Box(this.info);
    GUILayout.EndScrollView();
    GUI.skin.box.alignment = alignment;
  }

  private void Update()
  {
    if ((int) Game.Me.ServerID == 0)
      return;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.AppendFormat("ID = {0}\n", (object) Game.Me.ServerID);
    stringBuilder.AppendFormat("Name = {0}\n", (object) Game.Me.Name);
    stringBuilder.AppendFormat("Faction = {0}\n", (object) Game.Me.Faction);
    stringBuilder.AppendFormat("Rank = {0}\n", (object) Game.Me.Rank);
    stringBuilder.AppendFormat("Title = {0}\n", (object) Game.Me.Title);
    stringBuilder.Append("-----------\n");
    stringBuilder.AppendFormat("Level = {0}\n", (object) Game.Me.Level);
    stringBuilder.AppendFormat("Experience = {0}\n", (object) Game.Me.Experience);
    stringBuilder.AppendFormat("SpentExperience = {0}\n", (object) Game.Me.SpentExperience);
    stringBuilder.AppendFormat("NextLevelExperience = {0}\n", (object) Game.Me.NextLevelExperience);
    stringBuilder.AppendFormat("PrevLevelExperience = {0}\n", (object) Game.Me.PrevLevelExperience);
    stringBuilder.Append("----------- Friends\n");
    foreach (Player player in Game.Me.Friends.Players)
      stringBuilder.AppendFormat("{0}-{1}\n", (object) player.ServerID, (object) player.Name);
    stringBuilder.Append("----------- Ignores\n");
    foreach (Player player in Game.Me.Friends.Ignore)
      stringBuilder.AppendFormat("{0}-{1}\n", (object) player.ServerID, (object) player.Name);
    stringBuilder.Append("----------- Guild\n");
    if (Game.Me.Guild.Has)
    {
      stringBuilder.AppendFormat("GuildID = {0}\n", (object) Game.Me.Guild.ServerID);
      stringBuilder.AppendFormat("Name = {0}\n", (object) Game.Me.Guild.Name);
      foreach (Player player in Game.Me.Guild.Players)
        stringBuilder.AppendFormat("{0}-{1}\n", (object) player.ServerID, (object) player.Name);
    }
    stringBuilder.Append("----------- Party\n");
    stringBuilder.AppendFormat("HasParty = {0}\n", (object) Game.Me.Party.HasParty);
    stringBuilder.AppendFormat("IsLeader = {0}\n", (object) Game.Me.Party.IsLeader);
    foreach (Player member in Game.Me.Party.Members)
      stringBuilder.AppendFormat("{0}-{1}\n", (object) member.ServerID, (object) member.Name);
    this.info = stringBuilder.ToString();
  }
}
