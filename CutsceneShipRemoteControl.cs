﻿// Decompiled with JetBrains decompiler
// Type: CutsceneShipRemoteControl
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (CutsceneActor))]
public class CutsceneShipRemoteControl : MonoBehaviour
{
  private readonly CutsceneShipRemoteControl.ThrusterInfo thrusterInfoCached = new CutsceneShipRemoteControl.ThrusterInfo();
  public bool DestroyChildrenOnAwake;
  private ShipEngine.EngineState engineStateCached;

  public void Awake()
  {
    if (!this.DestroyChildrenOnAwake)
      return;
    this.DestroyChildren();
  }

  public void OnModelChange()
  {
    this.SetEngineState(this.engineStateCached);
    this.SetThrusterEffect(this.thrusterInfoCached.NormalizedDirection, this.thrusterInfoCached.TimeEnd);
  }

  public void SetEngineState(ShipEngine.EngineState engineState)
  {
    this.engineStateCached = engineState;
    Gearbox componentInChildren = this.GetComponentInChildren<Gearbox>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
      Debug.LogError((object) ("SetEngineState(): gearbox null. Caching engine state. (" + this.gameObject.name + ")"));
    else
      componentInChildren.SetEngineState(engineState);
  }

  public void SetThrusterEffect(Vector3 normalizedDir, float timeEnd)
  {
    this.thrusterInfoCached.NormalizedDirection = normalizedDir;
    this.thrusterInfoCached.TimeEnd = timeEnd;
    if ((double) Time.realtimeSinceStartup > (double) timeEnd)
      return;
    Thrusters componentInChildren = this.GetComponentInChildren<Thrusters>();
    if (!((UnityEngine.Object) null != (UnityEngine.Object) componentInChildren))
      return;
    float duration = timeEnd - Time.realtimeSinceStartup;
    componentInChildren.FireThrustersManually(normalizedDir, duration);
  }

  private void DestroyChildren()
  {
    List<GameObject> gameObjectList = new List<GameObject>();
    foreach (Transform transform in this.transform)
      gameObjectList.Add(transform.gameObject);
    if (gameObjectList.Count == 0)
      return;
    Debug.LogWarning((object) ("Destroying all children of " + this.transform.name));
    gameObjectList.ForEach((System.Action<GameObject>) (child => UnityEngine.Object.Destroy((UnityEngine.Object) child)));
  }

  private class ThrusterInfo
  {
    public float TimeEnd = -1f;
    public Vector3 NormalizedDirection;
  }
}
