﻿// Decompiled with JetBrains decompiler
// Type: UvOffsetAnimator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class UvOffsetAnimator : MonoBehaviour
{
  public float scrollSpeed = 1f;
  public Vector2 direction = new Vector2(1f, 0.0f);

  private void Update()
  {
    if (!(bool) ((Object) this.GetComponent<Renderer>()))
      return;
    this.GetComponent<Renderer>().material.mainTextureOffset += this.direction * this.scrollSpeed * Time.deltaTime;
  }
}
