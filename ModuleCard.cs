﻿// Decompiled with JetBrains decompiler
// Type: ModuleCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class ModuleCard : Card
{
  public string ColonialPrefab = string.Empty;
  public string CylonPrefab = string.Empty;

  public ModuleCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.ColonialPrefab = r.ReadString();
    this.CylonPrefab = r.ReadString();
  }
}
