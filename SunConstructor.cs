﻿// Decompiled with JetBrains decompiler
// Type: SunConstructor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SunConstructor : NonLODObjectConstructor
{
  protected SunDesc desc;

  public SunConstructor(SunDesc desc)
    : base(new GameObject("Sun"))
  {
    this.desc = desc;
  }

  public override void Construct()
  {
    base.Construct();
    this.root.transform.rotation = this.desc.rotation;
    this.root.transform.position = this.desc.position;
    this.root.transform.localScale = this.desc.scale;
    this.root.AddComponent<CustomSupply>().Action = (System.Action<GameObject>) (obj =>
    {
      Sun componentInChildren1 = obj.GetComponentInChildren<Sun>();
      componentInChildren1.sunDiscColor = this.desc.discColor;
      componentInChildren1.sunGlowColor = this.desc.glowColor;
      componentInChildren1.sunRaysColor = this.desc.raysColor;
      componentInChildren1.sunStreakColor = this.desc.streakColor;
      componentInChildren1.sunOcclusionFade = this.desc.occlusionFade;
      foreach (GameObject gameObject in GameObject.FindGameObjectsWithTag(Planet.PLANET_TAG))
      {
        PlanetAtmosphere2 componentInChildren2 = gameObject.GetComponentInChildren<PlanetAtmosphere2>();
        if ((UnityEngine.Object) componentInChildren2 != (UnityEngine.Object) null && !componentInChildren2.enabled)
        {
          componentInChildren2.SunPosition = obj.GetComponentInChildren<Sun>().transform;
          componentInChildren2.enabled = true;
        }
      }
    });
  }

  protected override string GetPrefabName()
  {
    return "sun_1.prefab";
  }
}
