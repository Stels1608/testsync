﻿// Decompiled with JetBrains decompiler
// Type: DebugTestAlgorithm3D
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugTestAlgorithm3D : MonoBehaviour
{
  private List<Vector3> list = new List<Vector3>();
  private Vector3 a = new Vector3(0.0f, 0.0f, 0.0f);
  private Vector3 b = new Vector3(0.0f, 0.0f, 10f);
  private Vector3 c = new Vector3(0.0f, 0.0f, 1f);
  private Vector3 d = new Vector3(10f, 0.0f, 1f);
  private float t;
  private float max;

  public void Start()
  {
    List<Vector3> vector3List = new List<Vector3>();
    float num = 0.0f;
    while ((double) num <= 1.0)
    {
      Vector3 vector3 = this.a + (this.b - this.a) * num;
      float magnitude = (this.c + (this.d - this.c) * num - vector3).magnitude;
      this.max = Mathf.Max(this.max, magnitude);
      vector3List.Add(new Vector3((float) ((double) num * 10.0 - 15.0), 0.0f, magnitude));
      num += 0.02f;
    }
    using (List<Vector3>.Enumerator enumerator = vector3List.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Vector3 current = enumerator.Current;
        this.list.Add(new Vector3(current.x, 0.0f, (float) ((double) current.z / (double) this.max * 10.0)));
      }
    }
  }

  public void Update()
  {
    Debug.DrawLine(this.a, this.b, Color.green);
    Debug.DrawLine(this.c, this.d, Color.green);
    Vector3 p0;
    Vector3 p1;
    Algorithm3D.Distance(this.a, this.b, this.c, this.d, out p0, out p1);
    Debug.DrawLine(p0, p1, Color.red);
    if ((double) this.t > 1.0)
      this.t = 0.0f;
    Debug.DrawLine(this.a + (this.b - this.a) * this.t, this.c + (this.d - this.c) * this.t, Color.yellow);
    this.t += Time.deltaTime / 2f;
    float x1 = -15f;
    float x2 = -5f;
    Debug.DrawLine(new Vector3(x1, 0.0f, 0.0f), new Vector3(x1, 0.0f, 10f), Color.gray);
    Debug.DrawLine(new Vector3(x2, 0.0f, 0.0f), new Vector3(x2, 0.0f, 10f), Color.gray);
    BgoDebug.DrawPath(this.list.ToArray(), Color.magenta);
  }
}
