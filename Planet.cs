﻿// Decompiled with JetBrains decompiler
// Type: Planet
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Planet : SpaceObject
{
  public static readonly string PLANET_TAG = "Planet";
  public static readonly string LIGHTDIRNAME = "lightDirection";
  public Color Color;
  public Color SpecularColor;
  public float Shininess;
  public Vector3 lightDirection;

  public float Radius
  {
    get
    {
      return this.Scale * 10f;
    }
  }

  public override bool ShowIndicatorWhenInRange
  {
    get
    {
      if (!this.IsFriendlyHub)
        return base.ShowIndicatorWhenInRange;
      return true;
    }
  }

  public Planet(uint objectID)
    : base(objectID)
  {
    this.HideStats = true;
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Position = r.ReadVector3();
    this.Rotation = r.ReadQuaternion();
    this.Scale = r.ReadSingle();
    this.Color = r.ReadColor();
    this.SpecularColor = r.ReadColor();
    this.Shininess = r.ReadSingle();
  }
}
