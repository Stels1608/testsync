﻿// Decompiled with JetBrains decompiler
// Type: GUIShopPaperDoll
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class GUIShopPaperDoll : GUIPanel
{
  private readonly List<GUIShopPaperDollSlot> slots = new List<GUIShopPaperDollSlot>();
  private string oldTexture = string.Empty;
  private readonly GUIImageNew paperDollImage;
  private readonly GUIImageNew banner;
  private readonly GUILabelNew bannerPercent;
  private GUIShopUninstallAllWindow uninstallAllConfirm;
  private GUIShopPaperDollSlot.dHandler handlerUpgradeButton;

  public GUIShopPaperDollSlot.dHandler HandlerUpgradeButton
  {
    set
    {
      this.handlerUpgradeButton = value;
      using (List<GUIShopPaperDollSlot>.Enumerator enumerator = this.slots.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.HandlerUpgradeButton = value;
      }
    }
  }

  public GUIShopPaperDoll(SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
    this.paperDollImage = new GUIImageNew(TextureCache.Get(Color.black), this.root);
    this.paperDollImage.IsRendered = false;
    this.AddPanel((GUIPanel) this.paperDollImage);
    this.banner = new GUIImageNew(ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow/banner"));
    this.banner.Position = new float2(231f, -182f);
    this.AddPanel((GUIPanel) this.banner);
    this.bannerPercent = new GUILabelNew(string.Empty);
    this.bannerPercent.Font = Gui.Options.FontBGM_BT;
    this.bannerPercent.FontSize = 15;
    this.bannerPercent.Size = new float2(50f, 40f);
    this.bannerPercent.Text = string.Empty;
    this.bannerPercent.Alignment = TextAnchor.UpperLeft;
    this.AddPanel((GUIPanel) this.bannerPercent);
    GUIButtonNew guiButtonNew = new GUIButtonNew("%$bgo.shop.uninstall_all%");
    guiButtonNew.TextLabel.Font = Gui.Options.FontBGM_BT;
    guiButtonNew.TextLabel.FontSize = 12;
    guiButtonNew.Position = new float2(100f, 242f);
    guiButtonNew.Size = new float2(280f, 18f);
    guiButtonNew.TextLabel.Alignment = TextAnchor.MiddleCenter;
    guiButtonNew.Handler = (AnonymousDelegate) (() =>
    {
      if (this.uninstallAllConfirm == null)
      {
        this.uninstallAllConfirm = new GUIShopUninstallAllWindow(this.root);
        this.AddPanel((GUIPanel) this.uninstallAllConfirm);
      }
      this.uninstallAllConfirm.Show();
    });
    this.AddPanel((GUIPanel) guiButtonNew);
  }

  private void UpdateSaleBanner(ReadOnlyCollection<ShipSlot> dataSlots)
  {
    bool flag1 = false;
    float a1 = -1f;
    float a2 = 100000f;
    foreach (ShipSlot dataSlot in dataSlots)
    {
      if (dataSlot.System != null && dataSlot.System.IsAnyUpgradeLevelOnSale())
      {
        flag1 = true;
        a1 = Mathf.Max(a1, (float) dataSlot.System.FindDiscountInUpgradeLevels().Percentage);
        a2 = Mathf.Min(a2, (float) dataSlot.System.FindDiscountInUpgradeLevels().Percentage);
      }
    }
    string str = a1.ToString();
    this.bannerPercent.Font = Gui.Options.FontBGM_BT;
    if ((double) Math.Abs(a2 - a1) > 0.00999999977648258)
    {
      this.bannerPercent.FontSize = 12;
      this.bannerPercent.Position = new float2(253f, -180f);
      str = ((double) a2).ToString() + "+";
    }
    else
    {
      this.bannerPercent.FontSize = 15;
      this.bannerPercent.Position = new float2((float) byte.MaxValue, -180f);
    }
    this.bannerPercent.Text = str + "%";
    GUIImageNew guiImageNew = this.banner;
    bool flag2 = flag1;
    this.bannerPercent.IsRendered = flag2;
    int num = flag2 ? 1 : 0;
    guiImageNew.IsRendered = num != 0;
  }

  public void UpdateData(ShipCard shipCard, ReadOnlyCollection<ShipSlot> dataSlots, ShipItem closedExceptItem)
  {
    this.UpdateSaleBanner(dataSlots);
    Paperdolls.PaperdollLayoutBig.PaperdollLayoutBig paperdollLayoutBig = shipCard.PaperdollLayoutBig;
    if (paperdollLayoutBig != null && (!this.paperDollImage.IsRendered || this.oldTexture != paperdollLayoutBig.BlueprintTexture))
    {
      this.oldTexture = paperdollLayoutBig.BlueprintTexture;
      this.paperDollImage.IsRendered = true;
      this.paperDollImage.Texture = Resources.Load("GUI/EquipBuyPanel/" + BgoUtils.FactionUiFolder(shipCard.Faction) + paperdollLayoutBig.BlueprintTexture_) as Texture2D;
      this.paperDollImage.TakeTextureSize();
      this.root.Width = this.paperDollImage.Rect.width;
      this.root.Height = this.paperDollImage.Rect.height;
      this.RecalculateAbsCoords();
    }
    if (dataSlots.Count > this.slots.Count)
    {
      int num = dataSlots.Count - this.slots.Count;
      for (int index = 0; index < num; ++index)
      {
        GUIShopPaperDollSlot shopPaperDollSlot = new GUIShopPaperDollSlot(this.root);
        shopPaperDollSlot.HandlerUpgradeButton = this.handlerUpgradeButton;
        this.AddPanel((GUIPanel) shopPaperDollSlot);
        this.slots.Add(shopPaperDollSlot);
      }
    }
    for (int index = 0; index < this.slots.Count; ++index)
    {
      if (index < dataSlots.Count)
      {
        ShipSlot slot = dataSlots[index];
        this.slots[index].UpdateData(slot);
        this.slots[index].IsRendered = true;
        this.slots[index].IsClosed = this.IsSlotClosed(shipCard, closedExceptItem, slot);
      }
      else
        this.slots[index].IsRendered = false;
    }
  }

  private bool IsSlotClosed(ShipCard shipCard, ShipItem closedExceptItem, ShipSlot slot)
  {
    if (slot.Card == null || closedExceptItem == null || !(closedExceptItem is ShipSystem))
      return true;
    ShipSystem shipSystem = closedExceptItem as ShipSystem;
    return !(bool) shipSystem.IsLoaded || shipSystem.IsBroken || ((int) shipCard.Tier != (int) shipSystem.Card.Tier || slot.Card.SystemType != shipSystem.Card.SlotType) || (!shipSystem.MeetsShipRestrictions(shipCard.ShipObjectKey, shipCard.ShipRoles) || slot.Card.SystemType == ShipSlotType.ship_paint && !shipSystem.PaintCard.shipCard.Equals(Game.Me.ActiveShip.Card));
  }

  public int GetSlotId(float2 fromPos)
  {
    for (int index = 0; index < this.slots.Count; ++index)
    {
      if (this.slots[index].Contains(fromPos))
        return index;
    }
    return -1;
  }
}
