﻿// Decompiled with JetBrains decompiler
// Type: GalaxyMapLegend
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GalaxyMapLegend : GuiPanel
{
  private static Vector2 bgOffset = new Vector2(-132f, -13f);
  public static readonly Color SectorEventNeutralColor = new Color(0.0f, 1f, 0.0f);
  public static readonly Color SectorEventAncientColor = new Color(1f, 1f, 0.0f);

  public GalaxyMapLegend()
  {
    string texture = "GUI/GalaxyMap/MapPanel";
    if (Game.Me.Faction == Faction.Cylon)
      texture = "GUI/GalaxyMap/_cylon/MapPanel";
    GuiImage guiImage1 = new GuiImage(texture);
    guiImage1.SizeX = 140f;
    this.AddChild((GuiElementBase) guiImage1, new Vector2(GalaxyMapLegend.bgOffset.x, GalaxyMapLegend.bgOffset.y));
    GuiImage guiImage2 = new GuiImage("GUI/GalaxyMap/colonial_outpost");
    guiImage2.Size = guiImage2.Size * 0.65f;
    this.AddChild((GuiElementBase) guiImage2, new Vector2(GalaxyMapLegend.bgOffset.x + 16f, GalaxyMapLegend.bgOffset.y + 15f));
    this.AddChild((GuiElementBase) this.CreateLabel("%$bgo.galaxy_map.colonial_outpost%"), new Vector2(GalaxyMapLegend.bgOffset.x + 50f, GalaxyMapLegend.bgOffset.y + 15f));
    GuiImage guiImage3 = new GuiImage("GUI/GalaxyMap/cylon_outpost");
    guiImage3.Size = guiImage3.Size * 0.65f;
    this.AddChild((GuiElementBase) guiImage3, new Vector2(GalaxyMapLegend.bgOffset.x + 16f, GalaxyMapLegend.bgOffset.y + 58f));
    this.AddChild((GuiElementBase) this.CreateLabel("%$bgo.galaxy_map.cylon_outpost%"), new Vector2(GalaxyMapLegend.bgOffset.x + 50f, GalaxyMapLegend.bgOffset.y + 58f));
    GuiImage guiImage4 = new GuiImage("GUI/GalaxyMap/colonial_mine");
    guiImage4.Size = guiImage4.Size * 0.8f;
    this.AddChild((GuiElementBase) guiImage4, new Vector2(GalaxyMapLegend.bgOffset.x + 27f, GalaxyMapLegend.bgOffset.y + 100f));
    this.AddChild((GuiElementBase) this.CreateLabel("%$bgo.galaxy_map.colonial_mine%"), new Vector2(GalaxyMapLegend.bgOffset.x + 50f, GalaxyMapLegend.bgOffset.y + 100f));
    GuiImage guiImage5 = new GuiImage("GUI/GalaxyMap/cylon_mine");
    guiImage5.Size = guiImage5.Size * 0.8f;
    this.AddChild((GuiElementBase) guiImage5, new Vector2(GalaxyMapLegend.bgOffset.x + 27f, GalaxyMapLegend.bgOffset.y + 141f));
    this.AddChild((GuiElementBase) this.CreateLabel("%$bgo.galaxy_map.cylon_mine%"), new Vector2(GalaxyMapLegend.bgOffset.x + 50f, GalaxyMapLegend.bgOffset.y + 141f));
    GuiImage guiImage6 = new GuiImage("GUI/GalaxyMap/sector_event_bg_glow");
    GuiImage guiImage7 = new GuiImage("GUI/GalaxyMap/sector_event");
    guiImage6.Size = guiImage6.Size * 0.8f;
    guiImage6.OverlayColor = new Color?(GalaxyMapLegend.SectorEventNeutralColor);
    guiImage7.Size = guiImage7.Size * 0.8f;
    this.AddChild((GuiElementBase) guiImage6, new Vector2(GalaxyMapLegend.bgOffset.x + 21f, GalaxyMapLegend.bgOffset.y + 173f));
    this.AddChild((GuiElementBase) guiImage7, new Vector2(GalaxyMapLegend.bgOffset.x + 21f, GalaxyMapLegend.bgOffset.y + 173f));
    this.AddChild((GuiElementBase) this.CreateLabel("%$bgo.galaxy_map.sector_event%"), new Vector2(GalaxyMapLegend.bgOffset.x + 50f, GalaxyMapLegend.bgOffset.y + 173f));
  }

  private GuiLabel CreateLabel(string text)
  {
    GuiLabel guiLabel1 = new GuiLabel(text);
    GuiLabel guiLabel2 = guiLabel1;
    Color black = Color.black;
    guiLabel1.OverColor = black;
    Color color = black;
    guiLabel2.NormalColor = color;
    guiLabel1.Font = Gui.Options.FontBGM_BT;
    guiLabel1.FontSize = 10;
    guiLabel1.Alignment = TextAnchor.MiddleLeft;
    return guiLabel1;
  }
}
