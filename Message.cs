﻿// Decompiled with JetBrains decompiler
// Type: Message
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum Message
{
  None,
  ChangeSetting,
  ApplySettings,
  SaveSettings,
  LoadSettings,
  RequestSettings,
  SetFullscreen,
  RegisterSettingsReceiver,
  UnregisterSettingsReceiver,
  SettingChangedCombatGui,
  SettingChangedLootPanelPosition,
  SettingChangedAssignmentsCollapsed,
  SettingChangedShowTutorials,
  SettingChangedCompletedTutorials,
  SettingChangedSoundVolume,
  SettingChangedMusicVolume,
  SettingChangedCameraMode,
  SettingChangedCameraZoom,
  SettingChangedChatViewGlobal,
  SettingChangedChatViewLocal,
  SettingChangedChatShowPrefix,
  SettingChangedAutoLoot,
  SettingChangedShowPopups,
  SettingChangedStarDust,
  SettingChangedStarFog,
  SettingChangedGlowEffect,
  SettingChangedUseProceduralTextures,
  SettingChangedShowShipSkins,
  SettingChangedShowModules,
  SettingChangedHighResModels,
  SettingChangedShowFps,
  SettingChangedCombatText,
  SettingChangedShowCutscenes,
  SettingsLoaded,
  SettingChangedMuteSound,
  LogToEventStream,
  TournamentStart,
  TournamentStop,
  TournamentJoined,
  UpdateSectorRegulation,
  CutscenePlayRequest,
  CutsceneMuteGameMusic,
  CharacterCreationStarted,
  NameRejected,
  NameAccepted,
  EnterCharacterMenu,
  LeaveCharacterMenu,
  RegisterCharacterMenu,
  CharacterServiceInfo,
  AvatarLoaded,
  RequestNameChange,
  CancelNameChange,
  NameChangeSuccessful,
  RequestFactionSwitch,
  CancelFactionSwitch,
  FactionSwitchSuccessful,
  RequestCharacterDeletion,
  CancelCharacterDeletion,
  RequestAvatarChange,
  CancelAvatarChange,
  ConfirmAvatarChange,
  InitUi,
  UiCreated,
  CreateLevelUi,
  LocalizationData,
  AddToCombatGui,
  RemoveFromCombatGui,
  ApplyCombatGui,
  ScreenResolutionChanged,
  SimplifyTutorialUi,
  ShowAbandonMissionButton,
  EnableTargetting,
  EnableGear,
  RecheckSystemButtons,
  ShowOnScreenNotification,
  ShowTickerMessage,
  ClearTicker,
  ShowOptionsWindow,
  ShowHelpWindow,
  ShowHelpScreen,
  ShowWoFWindow,
  ShowWofWindowWithDialogHandling,
  ShowTournamentRanking,
  ShowTradeInWindow,
  Show3DSectorMap,
  ShowLeaderboards,
  ToggleOldUi,
  ToggleNgui,
  ToggleUGuiHubCanvas,
  ToggleUGuiHudCanvas,
  ToggleUGuiWindowsCanvas,
  ToggleUGuiHudIndicatorsCanvas,
  ToggleInputActive,
  OptionsWindowOpened,
  OptionsWindowClosed,
  HelpWindowOpened,
  HelpWindowClosed,
  PlayerFactionReply,
  PlayershipAspectsUpdated,
  FactionGroupChanged,
  LoadNewLevel,
  LevelLoaded,
  LevelStarted,
  LoadingScreenDestroyed,
  LevelUiInstalled,
  SelectTargetRequest,
  SelectedTargetChanged,
  TargetDesignationChangeRequest,
  TargetDesignationChanged,
  TargetWaypointStatusChangeRequest,
  TargetWaypointStatusChanged,
  LocationMarkerStatusChanged,
  AutoAbilityToggled,
  AutoAbilityTargetChanged,
  AsteroidScanned,
  PlayerChangedCloakStatus,
  TargetPassedMapRange,
  PlayerPassedSectorEventBorder,
  PlayerChangedGuildStatus,
  PlayerChangedPartyStatus,
  PlayersMedalsChanged,
  PlayersTitleChanged,
  ShowCombatText,
  DamageDoneInfo,
  DamageReceivedInfo,
  NotificationServerError,
  GuiContentWindowVisibility,
  GuiToggleHubMenuVisibility,
  ChatSystemNegative,
  ChatSystemPositive,
  CombatLogOuch,
  CombatLogOk,
  CombatLogNice,
  ChangeInput,
  RegisterWindowManager,
  RegisterHelpContent,
  ShowDialogBox,
  HideDialogBox,
  Play2dSound,
  PlayAmbientSound,
  StopAmbientSound,
  ToggleJoystickGamepadEnabled,
  InitInputBindingData,
  LoadBindingPreset,
  ChangeInputBinding,
  SaveInputBindings,
  RevertInputBindings,
  ResetInputBindings,
  LoadInputBindings,
  InputBindingDataChanged,
  WofVisibleMaps,
  WofBonusMapPartsUpdate,
  GetDradisGameDetails,
  ShowRewad,
  WofMapInformation,
  WofShowConfirmation,
  EntityLoaded,
  EntityRemoved,
  PlayerShipSpawnStateChanged,
  FirstLoginEv0r,
  PlayerLevel,
  PlayerHoldChanged,
  PlayerCubitsHold,
  PlayerCubitsLocker,
  PlayerInThreat,
  FtlMissionsOff,
  AutoSubscribe,
  SectorEventStateChanged,
  SectorEventTaskList,
  SectorEventReward,
  SectorEventTerminated,
  Death,
  MyShipCreated,
  MyJumpOutAnimStarted,
  CameraBehaviorChanged,
  ShipInputMouseAutoTurnToggle,
  ShipInputMouseManualTurnToggle,
  ShipSpeedChangeCall,
  ShipSpeedChangeDone,
  ShipGearToggleBoostCall,
  ShipGearToggleBoostDone,
  ShipGearChangeCall,
  DockAtNpcShipRequest,
  DockAtPlayerShipRequest,
  TournamentRankingList,
  TournamentOwnRank,
  TournamentEnterFailed,
  RankingCounterUpdate,
  RankingCounterPlayerUpdate,
  RankingTournamentUpdate,
  RankingTournamentPlayerUpdate,
  GlobalBonusEventStarted,
  EventShopAvailability,
  EventShopInventory,
  EventShopBuy,
  DockAtPlayerShipReply,
  UndockFromPlayerShipReply,
  ShipListChanged,
  GuildQuit,
  GuildInfo,
  GuildInvite,
  GuildInviteResult,
  GuildMemberUpdate,
  GuildMemberRemoved,
  GuildOperationResult,
  GuildMemberPromotion,
  GuildRankNameChanged,
  MissionListUpdated,
  ShowSelectedMission,
  TrackMission,
  ShowTooltip,
  HideTooltip,
  ShopResourcesMissing,
  ShopSwitchCategory,
  ShopCanOnlyBuyOnce,
  ShopSellJunkConfirm,
  ShopSellJunk,
  MissionHandIn,
  MissionHandInAll,
  FlareReleased,
  DialogReplyNpcRemark,
  DialogReplyPcRemarks,
  DialogReplyStopped,
  DialogReplyAction,
  DialogClickedIndex,
  DialogAdvance,
  DialogRegister,
  DialogToggleVisibility,
  DialogStop,
  StoryMissionLogAddObjective,
  StoryMissionLogDestroyWindow,
  MeritCapUpdate,
  ShowSpecialOffer,
  GalaxyMapUpdates,
  LocalizationReady,
  FtlJumpStarted,
  FtlJumpQueued,
  FtlJumpCanceled,
  ExecuteWhenIngame,
}
