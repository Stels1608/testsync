﻿// Decompiled with JetBrains decompiler
// Type: PriceTagUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PriceTagUgui : MonoBehaviour
{
  [SerializeField]
  private Image resourceIcon;
  [SerializeField]
  private TextMeshProUGUI cost;

  public void SetResourceIcon(string iconPath)
  {
    Texture2D texture = (Texture2D) Resources.Load(iconPath);
    this.resourceIcon.sprite = UnityEngine.Sprite.Create(texture, new Rect(0.0f, 0.0f, (float) texture.width, (float) texture.height), new Vector2(0.5f, 0.5f));
  }

  public void SetCost(float amount)
  {
    this.cost.text = amount.ToString("#0.0");
  }
}
