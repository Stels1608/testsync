﻿// Decompiled with JetBrains decompiler
// Type: TextCombatMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class TextCombatMessage : AbstractCombatMessage
{
  private readonly string text;
  private readonly Color color;

  public Color Color
  {
    get
    {
      return this.color;
    }
  }

  public string Text
  {
    get
    {
      return this.text;
    }
  }

  public TextCombatMessage(string text, Color color, Transform parent, float lifetime)
    : base(parent, lifetime)
  {
    this.color = color;
    this.text = text;
  }
}
