﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorBracketBaseNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class HudIndicatorBracketBaseNgui : HudIndicatorBaseComponentNgui, IReceivesTarget
{
  public const float NoRangeBlinkDuration = 2.2f;
  private const string NO_RANGE_TIMER_NAME = "DelayedTargetDeselection";
  [SerializeField]
  private UISprite bracketSprite;
  [SerializeField]
  private UILabel noRangeLabel;
  private ISpaceEntity target;
  private BoxCollider clickCollider;
  private Rigidbody rigidBody;
  private float noRangeBlinkStopTime;
  private Timer blinkTimer;

  protected UIAtlas Atlas
  {
    get
    {
      return HudIndicatorsAtlasLinker.Instance.Atlas;
    }
  }

  public ISpaceEntity Target
  {
    get
    {
      return this.target;
    }
  }

  public UISprite BracketSprite
  {
    get
    {
      return this.bracketSprite;
    }
  }

  private bool IsCloaked
  {
    get
    {
      if (this.target.TargetType == TargetType.SpaceObject)
        return ((SpaceObject) this.target).IsCloaked;
      return false;
    }
  }

  public override bool RequiresAnimationNgui
  {
    get
    {
      return false;
    }
  }

  protected override void ResetComponentStates()
  {
    this.target = (ISpaceEntity) null;
    if ((Object) this.noRangeLabel != (Object) null)
      this.noRangeLabel.enabled = false;
    if ((Object) this.blinkTimer != (Object) null)
      Object.Destroy((Object) this.blinkTimer.gameObject);
    this.EnableClickCollider(true);
    this.noRangeBlinkStopTime = 0.0f;
  }

  protected override void Awake()
  {
    base.Awake();
    this.bracketSprite = NGUIToolsExtension.AddSprite(this.gameObject, this.Atlas, this.GetBracketInRangeAtlasSpriteName(), true, string.Empty);
    this.noRangeLabel = NGUIToolsExtension.AddLabel(this.gameObject, Gui.Options.FontBGM_BT, 12, UIWidget.Pivot.Center, "No Range");
    this.noRangeLabel.overflowMethod = UILabel.Overflow.ResizeFreely;
    NGUIToolsExtension.AnchorTo((UIRect) this.noRangeLabel, (UIRect) this.bracketSprite, UIAnchor.Side.Bottom, 0, 0);
    this.noRangeLabel.enabled = false;
  }

  protected abstract string GetBracketSelectedAtlasSpriteName();

  protected abstract string GetBracketInRangeAtlasSpriteName();

  protected virtual void UpdateBracket()
  {
    bool flag1 = (this.IsMultiselected || this.InScannerRange) && this.InScreen && !this.IsSelected;
    bool flag2 = this.InScreen & this.IsSelected;
    string spriteName = (string) null;
    if (flag2)
      spriteName = this.GetBracketSelectedAtlasSpriteName();
    else if (flag1)
      spriteName = this.GetBracketInRangeAtlasSpriteName();
    if (spriteName != null)
    {
      this.EnableBracketComponents(true);
      NGUIToolsExtension.SetSprite(this.bracketSprite, this.Atlas, spriteName);
    }
    else
      this.EnableBracketComponents(false);
  }

  protected override void UpdateView()
  {
    this.UpdateBracket();
    if (!this.target.IsTargetable || this.IsCloaked)
      return;
    BoxCollider boxCollider = this.clickCollider;
    bool flag = !this.IsSelected && this.InScannerRange;
    this.rigidBody.detectCollisions = flag;
    int num = flag ? 1 : 0;
    boxCollider.enabled = num != 0;
  }

  public override void OnScannerVisibilityChange(bool targetIsInScannerRange)
  {
    base.OnScannerVisibilityChange(targetIsInScannerRange);
    this.HandleRangeBlinking();
  }

  private void HandleRangeBlinking()
  {
    if (this.InScannerRange || !this.IsCloaked)
      return;
    this.noRangeBlinkStopTime = Time.time + 2.2f;
    if ((Object) this.bracketSprite != (Object) null)
      this.bracketSprite.spriteName = this.GetBracketInRangeAtlasSpriteName();
    else
      Debug.LogError((object) ("(LGP-5691) BracketSprite is null. This: " + (object) this + ". This gameObject is null? " + (object) ((Object) this.gameObject == (Object) null)));
    if ((Object) this.noRangeLabel != (Object) null)
      this.noRangeLabel.enabled = false;
    else
      Debug.LogError((object) ("(LGP-5691) NoRangeLabel is null. This: " + (object) this + ". This gameObject is null? " + (object) ((Object) this.gameObject == (Object) null)));
    if ((Object) this.blinkTimer == (Object) null)
    {
      this.blinkTimer = Timer.CreateTimer("DelayedTargetDeselection", 0.1f, 0.3f, new Timer.TickHandler(this.RangeBlinkHandler));
      this.blinkTimer.OnFinish = (Timer.OnFinishHandler) (() =>
      {
        this.UpdateBracket();
        if (!((Object) this.noRangeLabel != (Object) null))
          return;
        this.noRangeLabel.enabled = false;
      });
    }
    this.blinkTimer.DestructionTime = this.noRangeBlinkStopTime;
  }

  private void EnableClickCollider(bool enable)
  {
    if (!((Object) this.clickCollider != (Object) null))
      return;
    this.clickCollider.enabled = enable;
  }

  protected void EnableBracketComponents(bool enable)
  {
    this.EnableClickCollider(enable);
    if (!((Object) this.bracketSprite != (Object) null))
      return;
    this.bracketSprite.enabled = enable;
  }

  private void EnableAllComponents(bool enable)
  {
    this.EnableBracketComponents(enable);
    this.noRangeLabel.enabled = enable;
  }

  private void RangeBlinkHandler()
  {
    if ((Object) this.bracketSprite == (Object) null || (Object) this.noRangeLabel == (Object) null)
      return;
    this.bracketSprite.enabled = !this.bracketSprite.enabled && this.InScreen;
    this.noRangeLabel.enabled = (double) Time.time > (double) this.noRangeBlinkStopTime - 1.32000005245209 && this.InScreen;
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.target = target;
    if (target.IsTargetable)
      this.SetClickCollider(new Vector2((float) this.bracketSprite.width, (float) this.bracketSprite.height));
    this.UpdateColorAndAlpha();
  }

  protected override void SetColoring()
  {
    UISprite uiSprite = this.bracketSprite;
    Color color4LegacyBracket = HudIndicatorColorInfo.GetTargetColor4LegacyBracket(this.target);
    this.noRangeLabel.color = color4LegacyBracket;
    Color color = color4LegacyBracket;
    uiSprite.color = color;
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected virtual void SetClickCollider(Vector2 colliderSize)
  {
    this.clickCollider = this.GetComponent<BoxCollider>();
    if ((Object) this.clickCollider == (Object) null)
      this.clickCollider = this.gameObject.AddComponent<BoxCollider>();
    this.clickCollider.size = (Vector3) colliderSize;
    this.rigidBody = this.GetComponent<Rigidbody>();
    if ((Object) this.rigidBody == (Object) null)
      this.rigidBody = this.gameObject.AddComponent<Rigidbody>();
    this.rigidBody.useGravity = false;
    this.rigidBody.isKinematic = true;
  }

  protected override void ApplyOpacity()
  {
    foreach (UIWidget componentsInChild in this.GetComponentsInChildren<UIWidget>(true))
      componentsInChild.alpha = this.opacity;
  }
}
