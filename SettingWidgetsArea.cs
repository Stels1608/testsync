﻿// Decompiled with JetBrains decompiler
// Type: SettingWidgetsArea
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class SettingWidgetsArea : MonoBehaviour
{
  [SerializeField]
  private Transform settingWidgetsParent;

  private void Awake()
  {
    this.DestroyAllSettingWidgets();
  }

  public void DestroyAllSettingWidgets()
  {
    for (int index = this.settingWidgetsParent.childCount - 1; index >= 0; --index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.settingWidgetsParent.GetChild(index).gameObject);
  }

  public void AddEnumSettingWidget<T>(UserSetting setting) where T : struct, IConvertible
  {
    UguiTools.CreateChild<SettingRadioButtonsUgui>("Settings/Setting (RadioButtons)", this.settingWidgetsParent).Initialize<T>(setting);
  }

  public void AddTextCheckbox(UserSetting setting)
  {
    UguiTools.CreateChild<SettingTextCheckboxUgui>("Settings/Setting (Checkbox)", this.settingWidgetsParent).Initialize(setting);
  }

  public void AddTextCheckboxSmall(UserSetting setting)
  {
    UguiTools.CreateChild<SettingTextCheckboxUgui>("Settings/SettingSmall (Text Checkbox)", this.settingWidgetsParent).Initialize(setting);
  }

  public void AddIconCheckboxSmall(UserSetting setting, UnityEngine.Sprite icon)
  {
    UguiTools.CreateChild<SettingIconCheckboxUgui>("Settings/SettingSmall (Icon Checkbox)", this.settingWidgetsParent).Initialize(setting, icon);
  }
}
