﻿// Decompiled with JetBrains decompiler
// Type: BsgoTrigger
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BsgoTrigger : SpaceObject
{
  private string name;
  public float Radius;
  private bool _isActiveWaypoint;

  public override string PrefabName
  {
    get
    {
      return "WaypointSphere";
    }
  }

  public override string Name
  {
    get
    {
      return this.name;
    }
  }

  public bool IsActiveWaypoint
  {
    get
    {
      return this._isActiveWaypoint;
    }
    set
    {
      BsgoTrigger.\u003C\u003Ec__AnonStorey118 cAnonStorey118 = new BsgoTrigger.\u003C\u003Ec__AnonStorey118();
      cAnonStorey118.value = value;
      cAnonStorey118.\u003C\u003Ef__this = this;
      this._isActiveWaypoint = cAnonStorey118.value;
      this.IsConstructed.AddHandler(new SignalHandler(cAnonStorey118.\u003C\u003Em__2A9));
    }
  }

  public override bool IsTargetable
  {
    get
    {
      return false;
    }
  }

  public BsgoTrigger(uint objectID)
    : base(objectID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.name = r.ReadString();
    this.Position = r.ReadVector3();
    this.Radius = r.ReadSingle();
    this.OnLocate();
  }

  protected override void SetModel(GameObject prototype)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    BsgoTrigger.\u003CSetModel\u003Ec__AnonStorey119 modelCAnonStorey119 = new BsgoTrigger.\u003CSetModel\u003Ec__AnonStorey119();
    // ISSUE: reference to a compiler-generated field
    modelCAnonStorey119.\u003C\u003Ef__this = this;
    base.SetModel(prototype);
    this.Root.name = "BsgoTrigger - " + this.name;
    PlayerShip actualPlayerShip = SpaceLevel.GetLevel().GetActualPlayerShip();
    // ISSUE: reference to a compiler-generated field
    modelCAnonStorey119.size = !SpaceLevel.GetLevel().IsTutorialMission ? this.Radius * 2f : 35f;
    // ISSUE: reference to a compiler-generated method
    actualPlayerShip.IsConstructed.AddHandler(new SignalHandler(modelCAnonStorey119.\u003C\u003Em__2AA));
    this.Model.SetActive(false);
  }
}
