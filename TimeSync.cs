﻿// Decompiled with JetBrains decompiler
// Type: TimeSync
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class TimeSync
{
  private double[] timeDeltas = new double[3];
  private double[] latencies = new double[3];
  private int syncStep = -1;
  private const int syncStepCount = 3;
  private const double syncInterval = 10.0;
  private double serverDelta;
  private double lastClientSendTime;
  private bool nextSyncTimerActive;
  private double nextSyncTimer;
  private double shift;
  private TimeSpan span;
  private TimeSpan local_server_span;
  private SyncProtocol protocol;

  public double SectorTime
  {
    get
    {
      return this.ServerTime - 0.001 * (double) SpaceLevel.GetLevel().TimeOrigin;
    }
  }

  public double ServerTime
  {
    get
    {
      return this.ClientTime + this.serverDelta - this.shift;
    }
  }

  public DateTime ServerDateTime
  {
    get
    {
      return DateTime.Now + this.span;
    }
  }

  public DateTime ServerLocalDateTime
  {
    get
    {
      return DateTime.Now + this.local_server_span;
    }
  }

  public TimeSpan TimeTillMidnight
  {
    get
    {
      return (this.ServerDateTime + new TimeSpan(1, 0, 0, 0)).Date - this.ServerDateTime;
    }
  }

  public double ClientTime
  {
    get
    {
      return (double) Time.realtimeSinceStartup;
    }
  }

  public TimeSync()
  {
    this.protocol = new SyncProtocol(this);
    Game.ProtocolManager.RegisterProtocol((BgoProtocol) this.protocol);
  }

  public void Init(long serverconnectionTime, int years, int months, int days, int hours, int minutes, int seconds)
  {
    this.local_server_span = new DateTime(years, months, days, hours, minutes, seconds) - DateTime.Now;
    this.span = new DateTime(1970, 1, 1) + TimeSpan.FromMilliseconds((double) serverconnectionTime) - DateTime.Now;
    this.serverDelta = 0.001 * (double) serverconnectionTime - this.ClientTime;
    this.StartSync();
  }

  public void StartSync()
  {
    if (this.syncStep >= 0)
      return;
    this.syncStep = 0;
    this.SendSync();
  }

  private void SendSync()
  {
    this.lastClientSendTime = this.ClientTime;
    ++this.syncStep;
    SyncProtocol.GetProtocol().SendSyncRequest();
  }

  private double GetAverageDelta(double[] deltas)
  {
    double num = 0.0;
    foreach (double delta in deltas)
      num += delta;
    return num / 3.0;
  }

  public void ServerReply(long currentServerMilliseconds)
  {
    if (this.syncStep > 3)
      return;
    double num1 = this.ClientTime - this.lastClientSendTime;
    double num2 = this.lastClientSendTime + num1 / 2.0;
    this.timeDeltas[this.syncStep - 1] = 0.001 * (double) currentServerMilliseconds - num2;
    this.latencies[this.syncStep - 1] = num1;
    if (this.syncStep >= 3)
    {
      this.serverDelta = this.GetAverageDelta(this.timeDeltas);
      this.syncStep = -1;
      this.nextSyncTimer = this.ClientTime + 10.0;
      this.nextSyncTimerActive = true;
      this.shift = this.GetAverageDelta(this.latencies) + 0.1;
    }
    else
      this.SendSync();
  }

  public void Update()
  {
    if (!this.nextSyncTimerActive || this.ClientTime <= this.nextSyncTimer)
      return;
    this.nextSyncTimerActive = false;
    this.StartSync();
  }
}
