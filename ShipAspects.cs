﻿// Decompiled with JetBrains decompiler
// Type: ShipAspects
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class ShipAspects : IProtocolRead
{
  private readonly List<ShipAspect> shipAspectList;

  public ShipAspects()
  {
    this.shipAspectList = new List<ShipAspect>();
  }

  public bool ContainsAspect(ShipAspect aspect)
  {
    return this.shipAspectList.Contains(aspect);
  }

  public void Read(BgoProtocolReader r)
  {
    this.shipAspectList.Clear();
    int num = r.ReadLength();
    for (int index = 0; index < num; ++index)
      this.shipAspectList.Add((ShipAspect) r.ReadByte());
  }

  public override string ToString()
  {
    string str = string.Empty;
    using (List<ShipAspect>.Enumerator enumerator = this.shipAspectList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipAspect current = enumerator.Current;
        str = str + (object) current + " ";
      }
    }
    return string.Format("ShipAspectList: {0} ShipAspectList Size: {1}", (object) str, (object) this.shipAspectList.Count);
  }
}
