﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public class HudIndicatorMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public static bool UseUguiBrackets = true;
  public new const string NAME = "HudIndicatorMediator";
  private HudIndicatorManagerBase hudIndicatorManager;
  private TargetSelectionDataProvider targetSelectionDataProvider;
  private SettingsDataProvider settingsDataProvider;

  public HudIndicatorMediator()
    : base("HudIndicatorMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.LoadingScreenDestroyed);
    this.AddMessageInterest(Message.EntityLoaded);
    this.AddMessageInterest(Message.EntityRemoved);
    this.AddMessageInterest(Message.PlayerShipSpawnStateChanged);
    this.AddMessageInterest(Message.TargetPassedMapRange);
    this.AddMessageInterest(Message.PlayerPassedSectorEventBorder);
    this.AddMessageInterest(Message.TargetDesignationChanged);
    this.AddMessageInterest(Message.TargetWaypointStatusChanged);
    this.AddMessageInterest(Message.LocationMarkerStatusChanged);
    this.AddMessageInterest(Message.SelectedTargetChanged);
    this.AddMessageInterest(Message.PlayersMedalsChanged);
    this.AddMessageInterest(Message.PlayersTitleChanged);
    this.AddMessageInterest(Message.PlayerChangedPartyStatus);
    this.AddMessageInterest(Message.PlayerChangedGuildStatus);
    this.AddMessageInterest(Message.ChangeSetting);
    this.AddMessageInterest(Message.DamageDoneInfo);
    this.AddMessageInterest(Message.AutoAbilityTargetChanged);
    this.AddMessageInterest(Message.UpdateSectorRegulation);
    this.AddMessageInterest(Message.FactionGroupChanged);
    this.AddMessageInterest(Message.TournamentJoined);
    this.AddMessageInterest(Message.InputBindingDataChanged);
    this.AddMessageInterest(Message.AsteroidScanned);
    this.targetSelectionDataProvider = (TargetSelectionDataProvider) this.OwnerFacade.FetchDataProvider("TargetSelectionProvider");
    this.settingsDataProvider = (SettingsDataProvider) this.OwnerFacade.FetchDataProvider("SettingsDataProvider");
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.FactionGroupChanged:
        this.HandleFactionGroupChanged((SpaceObject) message.Data);
        break;
      case Message.LoadNewLevel:
        this.hudIndicatorManager.RemoveAllIndicators();
        this.hudIndicatorManager.IsRendered = false;
        break;
      case Message.LoadingScreenDestroyed:
        this.hudIndicatorManager.IsRendered = true;
        break;
      case Message.SelectedTargetChanged:
        KeyValuePair<ISpaceEntity, ISpaceEntity> keyValuePair1 = (KeyValuePair<ISpaceEntity, ISpaceEntity>) message.Data;
        ISpaceEntity key = keyValuePair1.Key;
        ISpaceEntity target1 = keyValuePair1.Value;
        this.UpdateTargetSelection(key, false);
        this.UpdateTargetSelection(target1, true);
        break;
      case Message.TargetDesignationChanged:
        KeyValuePair<ISpaceEntity, bool> keyValuePair2 = (KeyValuePair<ISpaceEntity, bool>) message.Data;
        this.HandleTargetDesignation(keyValuePair2.Key, keyValuePair2.Value);
        break;
      case Message.TargetWaypointStatusChanged:
        KeyValuePair<ISpaceEntity, bool> keyValuePair3 = (KeyValuePair<ISpaceEntity, bool>) message.Data;
        this.HandleTargetWaypointStatus(keyValuePair3.Key, keyValuePair3.Value);
        break;
      case Message.LocationMarkerStatusChanged:
        KeyValuePair<ISpaceEntity, bool> keyValuePair4 = (KeyValuePair<ISpaceEntity, bool>) message.Data;
        this.HandleLocationMarkerStatus(keyValuePair4.Key, keyValuePair4.Value);
        break;
      case Message.AutoAbilityTargetChanged:
        AbilityTargetChangedData targetChangedData = (AbilityTargetChangedData) message.Data;
        if (targetChangedData.Ability.card.Affect != ShipAbilityAffect.MultiWeaponTarget)
          break;
        ISpaceEntity oldTarget = targetChangedData.OldTarget;
        ISpaceEntity newTarget = targetChangedData.NewTarget;
        ShipAbility ability = targetChangedData.Ability;
        this.UpdateMultiTargetSelection(oldTarget, false, ability);
        this.UpdateMultiTargetSelection(newTarget, true, ability);
        break;
      case Message.AsteroidScanned:
        this.HandleAsteroidScanned((Asteroid) message.Data);
        break;
      case Message.TargetPassedMapRange:
        this.SendScannerBorderEvent((ISpaceEntity) message.Data);
        break;
      case Message.PlayerPassedSectorEventBorder:
        KeyValuePair<SectorEvent, bool> keyValuePair5 = (KeyValuePair<SectorEvent, bool>) message.Data;
        this.SendPlayerPassedSectorEventBorderEvent(keyValuePair5.Key, keyValuePair5.Value);
        break;
      case Message.PlayerChangedGuildStatus:
      case Message.PlayerChangedPartyStatus:
        if ((Object) SpaceLevel.GetLevel() == (Object) null)
          break;
        Player player = (Player) message.Data;
        if (player.IsMe)
        {
          this.UpdateAllPlayerIndicatorColors();
          this.UpdateAllPlayerIndicatorTexts();
          break;
        }
        this.UpdatePlayersColoring(player);
        this.UpdatePlayersTextLabel(player);
        break;
      case Message.PlayersMedalsChanged:
        this.UpdatePlayersMedals((Player) message.Data);
        break;
      case Message.PlayersTitleChanged:
        this.UpdatePlayersTextLabel((Player) message.Data);
        break;
      case Message.DamageDoneInfo:
        if (!this.settingsDataProvider.CurrentSettings.CombatText)
          break;
        AbstractCombatInfo combatInfo = (AbstractCombatInfo) message.Data;
        ISpaceEntity target2 = (ISpaceEntity) combatInfo.Target;
        this.ShowCombatInfo(combatInfo, target2);
        break;
      default:
        switch (id - 156)
        {
          case Message.None:
            ISpaceEntity target3 = (ISpaceEntity) message.Data;
            if (!HudIndicatorInfo.HasStaticIndicator(target3))
              return;
            this.hudIndicatorManager.CreateIndicator(target3);
            return;
          case Message.ChangeSetting:
            this.hudIndicatorManager.RemoveIndicator((ISpaceEntity) message.Data);
            return;
          case Message.ApplySettings:
            this.hudIndicatorManager.UpdateVisibility((ISpaceEntity) ((KeyValuePair<PlayerShip, bool>) message.Data).Key);
            return;
          default:
            if (id != Message.TournamentJoined)
            {
              if (id != Message.UpdateSectorRegulation)
              {
                if (id != Message.ChangeSetting)
                {
                  if (id != Message.UiCreated)
                  {
                    if (id != Message.InputBindingDataChanged)
                      return;
                    this.hudIndicatorManager.ApplyInputBindings((InputBinder) message.Data);
                    return;
                  }
                  this.CreateHudIndicatorPanel();
                  this.hudIndicatorManager.IsRendered = false;
                  return;
                }
                this.HandleOptionChange((ChangeSettingMessage) message);
                return;
              }
              this.HandleSectorRegulationUpdate();
              return;
            }
            this.UpdateAllPlayerIndicatorTexts();
            this.UpdateAllPlayerMedals();
            return;
        }
    }
  }

  private void CreateHudIndicatorPanel()
  {
    if (this.hudIndicatorManager != null)
    {
      Debug.LogWarning((object) "Hud Indicator Panel was tried to be created again...?!");
    }
    else
    {
      this.hudIndicatorManager = !HudIndicatorMediator.UseUguiBrackets ? (HudIndicatorManagerBase) HudIndicatorManagerNgui.CreateInstance() : (HudIndicatorManagerBase) HudIndicatorManagerUgui.CreateInstance();
      this.OwnerFacade.SendMessage(Message.AddToCombatGui, (object) this.hudIndicatorManager);
    }
  }

  private void SendScannerBorderEvent(ISpaceEntity target)
  {
    HudIndicatorBase indicator;
    if (!this.hudIndicatorManager.TryFindIndicator(target, out indicator))
      return;
    indicator.OnScannerVisibilityChange();
  }

  private void SendPlayerPassedSectorEventBorderEvent(SectorEvent sectorEvent, bool playerInRadius)
  {
    HudIndicatorBase indicator;
    if (!this.hudIndicatorManager.TryFindIndicator((ISpaceEntity) sectorEvent, out indicator))
      return;
    float opacity = !playerInRadius ? 1f : 0.5f;
    indicator.SetOpacity(opacity);
  }

  private void HandleAsteroidScanned(Asteroid asteroid)
  {
    HudIndicatorBase indicator;
    if (!this.hudIndicatorManager.TryFindIndicator((ISpaceEntity) asteroid, out indicator))
      return;
    indicator.SetScanned(asteroid.Resource);
  }

  private void HandleTargetDesignation(ISpaceEntity target, bool setAsDesignated)
  {
    HudIndicatorBase indicator;
    if (!this.hudIndicatorManager.TryFindIndicator(target, out indicator))
      return;
    indicator.SetDesignated(setAsDesignated);
  }

  private void HandleTargetWaypointStatus(ISpaceEntity target, bool setAsWaypoint)
  {
    this.HandleLentBrackets(target);
    HudIndicatorBase indicator;
    if (!this.hudIndicatorManager.TryFindIndicator(target, out indicator))
      return;
    indicator.SetWaypointStatus(setAsWaypoint);
  }

  private void HandleLocationMarkerStatus(ISpaceEntity target, bool setAsActive)
  {
    if (setAsActive)
    {
      if (this.hudIndicatorManager.HasIndicatorFor(target))
        return;
      this.hudIndicatorManager.CreateIndicator(target).SetWaypointStatus(true);
    }
    else
      this.hudIndicatorManager.RemoveIndicator(target);
  }

  private void UpdateTargetsIndicatorColor(ISpaceEntity target)
  {
    HudIndicatorBase indicator;
    if (!this.hudIndicatorManager.TryFindIndicator(target, out indicator))
      return;
    indicator.UpdateColoring();
  }

  private void UpdateAllPlayerIndicatorColors()
  {
    using (Dictionary<ISpaceEntity, HudIndicatorBase>.Enumerator enumerator = this.hudIndicatorManager.IndicatorMapping.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ISpaceEntity, HudIndicatorBase> current = enumerator.Current;
        if (current.Key is PlayerShip)
          current.Value.UpdateColoring();
      }
    }
  }

  private void UpdateAllPlayerIndicatorTexts()
  {
    using (Dictionary<ISpaceEntity, HudIndicatorBase>.Enumerator enumerator = this.hudIndicatorManager.IndicatorMapping.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ISpaceEntity, HudIndicatorBase> current = enumerator.Current;
        IHudIndicatorTopText hudIndicatorComponent;
        if (current.Key is PlayerShip && current.Value.TryGetHudIndicatorComponent<IHudIndicatorTopText>(out hudIndicatorComponent))
          hudIndicatorComponent.UpdateLabelText();
      }
    }
  }

  private void UpdateAllPlayerMedals()
  {
    using (Dictionary<ISpaceEntity, HudIndicatorBase>.Enumerator enumerator = this.hudIndicatorManager.IndicatorMapping.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ISpaceEntity, HudIndicatorBase> current = enumerator.Current;
        IHudIndicatorMedal hudIndicatorComponent;
        if (current.Key is PlayerShip && current.Value.TryGetHudIndicatorComponent<IHudIndicatorMedal>(out hudIndicatorComponent))
          hudIndicatorComponent.UpdateMedals();
      }
    }
  }

  private void UpdatePlayersColoring(Player player)
  {
    HudIndicatorBase foundIndicator;
    if (!this.TryFindIndicatorForPlayer(player, out foundIndicator))
      return;
    foundIndicator.UpdateColoring();
  }

  private void UpdatePlayersTextLabel(Player player)
  {
    IHudIndicatorTopText foundComponent;
    if (!this.TryFindComponentForPlayer<IHudIndicatorTopText>(player, out foundComponent))
      return;
    foundComponent.UpdateLabelText();
  }

  private void UpdatePlayersMedals(Player player)
  {
    HudIndicatorMedalsNgui foundComponent;
    if (!this.TryFindComponentForPlayer<HudIndicatorMedalsNgui>(player, out foundComponent))
      return;
    foundComponent.UpdateMedals();
  }

  private bool TryFindComponentForPlayer<T>(Player player, out T foundComponent)
  {
    PlayerShip playerShip;
    if (this.TryFindPlayerShip(player, out playerShip) && this.hudIndicatorManager.TryFindIndicatorComponent<T>((ISpaceEntity) playerShip, out foundComponent))
      return true;
    foundComponent = default (T);
    return false;
  }

  private bool TryFindIndicatorForPlayer(Player player, out HudIndicatorBase foundIndicator)
  {
    PlayerShip playerShip;
    if (this.TryFindPlayerShip(player, out playerShip) && this.hudIndicatorManager.TryFindIndicator((ISpaceEntity) playerShip, out foundIndicator))
      return true;
    foundIndicator = (HudIndicatorBase) null;
    return false;
  }

  private bool TryFindPlayerShip(Player player, out PlayerShip playerShip)
  {
    playerShip = player.PlayerShip;
    return playerShip != null;
  }

  private void HandleLentBrackets(ISpaceEntity target)
  {
    if (HudIndicatorInfo.HasStaticIndicator(target))
      return;
    if (this.targetSelectionDataProvider.IsMultiTarget(target) || this.targetSelectionDataProvider.IsMainTarget(target) || this.targetSelectionDataProvider.IsWaypoint(target))
    {
      if (this.hudIndicatorManager.HasIndicatorFor(target))
        return;
      this.hudIndicatorManager.CreateIndicator(target);
    }
    else
      this.hudIndicatorManager.RemoveIndicator(target);
  }

  private void UpdateTargetSelection(ISpaceEntity target, bool isSelected)
  {
    if (target == null)
      return;
    this.HandleLentBrackets(target);
    HudIndicatorBase indicator;
    if (!this.hudIndicatorManager.TryFindIndicator(target, out indicator))
      return;
    indicator.SetSelected(isSelected);
  }

  private void UpdateMultiTargetSelection(ISpaceEntity target, bool isMultiselected, ShipAbility ability)
  {
    if (target == null || target.Dead)
      return;
    this.HandleLentBrackets(target);
    HudIndicatorBase indicator;
    if (!this.hudIndicatorManager.TryFindIndicator(target, out indicator))
      return;
    indicator.SetMultiSelected(isMultiselected, ability);
  }

  private void ShowCombatInfo(AbstractCombatInfo combatInfo, ISpaceEntity target)
  {
    ICombatInfo indicatorComponent;
    if (!this.hudIndicatorManager.TryFindIndicatorComponent<ICombatInfo>(target, out indicatorComponent))
      return;
    indicatorComponent.OnCombatInfo(combatInfo);
  }

  private void HandleOptionChange(ChangeSettingMessage settingMessage)
  {
    UserSetting setting = settingMessage.Setting;
    switch (setting)
    {
      case UserSetting.HudIndicatorShowTargetNames:
      case UserSetting.HudIndicatorTextSize:
      case UserSetting.HudIndicatorColorScheme:
      case UserSetting.HudIndicatorShowShipTierIcon:
      case UserSetting.HudIndicatorBracketResizing:
      case UserSetting.HudIndicatorDescriptionDisplayDistance:
      case UserSetting.HudIndicatorSelectionCrosshair:
      case UserSetting.HudIndicatorHealthBar:
      case UserSetting.CombatText:
      case UserSetting.ShowEnemyIndication:
      case UserSetting.ShowFriendIndication:
      case UserSetting.HudIndicatorShowMissionArrow:
        this.hudIndicatorManager.ApplyOptionsSetting(settingMessage.Setting, settingMessage.Data);
        break;
      default:
        if (setting != UserSetting.HudIndicatorShowShipNames && setting != UserSetting.HudIndicatorShowWingNames && setting != UserSetting.HudIndicatorShowTitles)
          break;
        goto case UserSetting.HudIndicatorShowTargetNames;
    }
  }

  private void HandleSectorRegulationUpdate()
  {
    this.UpdateAllPlayerIndicatorColors();
  }

  private void HandleFactionGroupChanged(SpaceObject spaceObject)
  {
    this.UpdateTargetsIndicatorColor((ISpaceEntity) spaceObject);
  }
}
