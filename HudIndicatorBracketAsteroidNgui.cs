﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorBracketAsteroidNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class HudIndicatorBracketAsteroidNgui : HudIndicatorBracketBaseNgui
{
  protected override string GetBracketSelectedAtlasSpriteName()
  {
    return "HudIndicator_Bracket_Big_Selected";
  }

  protected override string GetBracketInRangeAtlasSpriteName()
  {
    return "HudIndicator_Bracket_Big";
  }

  protected override void UpdateBracket()
  {
    bool enable = this.InScreen & this.IsSelected;
    this.EnableBracketComponents(enable);
    if (!enable)
      return;
    NGUIToolsExtension.SetSprite(this.BracketSprite, this.Atlas, this.GetBracketSelectedAtlasSpriteName());
  }
}
