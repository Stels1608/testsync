﻿// Decompiled with JetBrains decompiler
// Type: PlayerManeuverController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class PlayerManeuverController : ManeuverController, IMovementController, IPlayerMovementController
{
  private double timeLastMovementUpdate;
  private double movementUpdateDeltaTime;

  public double MovementUpdateDeltaTime
  {
    get
    {
      return this.movementUpdateDeltaTime;
    }
    set
    {
      this.movementUpdateDeltaTime = value;
    }
  }

  public PlayerManeuverController(SpaceObject spaceObject, MovementCard card)
    : base(spaceObject, card)
  {
    this.timeLastMovementUpdate = Game.TimeSync.SectorTime;
  }

  public override bool Move(double time)
  {
    this.movementUpdateDeltaTime = time - this.timeLastMovementUpdate;
    this.timeLastMovementUpdate = time;
    return base.Move(time);
  }
}
