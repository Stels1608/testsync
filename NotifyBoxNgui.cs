﻿// Decompiled with JetBrains decompiler
// Type: NotifyBoxNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class NotifyBoxNgui : NotifyBoxUi
{
  [SerializeField]
  private UILabel headerLabel;
  [SerializeField]
  private UILabel okButtonLabel;
  [SerializeField]
  private UILabel messageLabel;
  [SerializeField]
  private UILabel timerLabel;

  protected override void SetTitleText(string text)
  {
    this.headerLabel.text = text;
  }

  protected override void SetOkButtonText(string text)
  {
    this.okButtonLabel.text = text;
  }

  protected override void SetMainText(string mainText)
  {
    this.messageLabel.text = mainText;
  }

  protected override void SetTimerText(string timerText)
  {
    this.timerLabel.text = timerText;
  }

  public override void ShowTimer(bool show)
  {
    this.timerLabel.enabled = show;
  }
}
