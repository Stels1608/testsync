﻿// Decompiled with JetBrains decompiler
// Type: LeaderboardView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LeaderboardView : MonoBehaviour
{
  private const uint ENTRIES_PER_PAGE = 16;
  private const uint NUMBER_OF_BRACKETS = 6;
  public Transform navigationAnchor;
  public Transform rankingAnchor;
  public Transform tabsAnchor;
  public Text titleLabel;
  public Text navigationLabel;
  public Text noDataLabel;
  public Text lastUpdateLabel;
  public Text contentHeaderLabel;
  public Text playerColumnLabel;
  public Text rankColumnLabel;
  public Text score1Label;
  public Text score2Label;
  public Text score3Label;
  public Toggle sortByScore1Toggle;
  public Toggle sortByScore2Toggle;
  public Toggle sortByScore3Toggle;
  public Button closeButton;
  public Text pageLabel;
  public SystemButton nextPageButton;
  public SystemButton previousPageButton;
  public SystemButton firstPageButton;
  public SystemButton lastPageButton;
  public LeaderboardEntryWidget myPersonalRanking;
  private LeaderboardEntryWidget[] entryWidgets;
  private LeaderboardTab[] tabs;
  private Button jumpToMyRankingButton;
  [NonSerialized]
  private RankingGroup selectedGroup;
  [NonSerialized]
  private RankingType selectedRankingType;
  [NonSerialized]
  private TournamentRankingGroup selectedTournamentGroup;
  [NonSerialized]
  private TournamentRankingType selectedTournamentRankingType;
  private uint page;
  private byte sortBy;
  private uint lastPage;
  private ushort bracketId;
  private RankingMode? mode;
  private RankDescription myRanking;

  private void Awake()
  {
    this.selectedGroup = RankingGroup.Kills;
    this.selectedRankingType = RankingType.Regular;
    this.page = 0U;
    this.lastPage = 1U;
    this.sortBy = (byte) 1;
    LeaderboardEntryWidget.LoadIcons();
    LeaderboardCategoryFoldout.LoadIcons();
    LeaderboardTab.LoadIcons();
    this.entryWidgets = this.rankingAnchor.GetComponentsInChildren<LeaderboardEntryWidget>();
    if (this.entryWidgets == null || (long) this.entryWidgets.Length != 16L)
      UnityEngine.Debug.LogError((object) ("Expecting " + (object) 16U + " ranking entry widgets, but found: " + (object) (this.entryWidgets != null ? this.entryWidgets.Length : 0)));
    this.jumpToMyRankingButton = this.myPersonalRanking.GetComponent<Button>();
    this.InitTabs();
    this.InitButtonHandlers();
  }

  private void InitTabs()
  {
    this.tabs = this.tabsAnchor.GetComponentsInChildren<LeaderboardTab>();
    if (this.tabs == null || (long) this.tabs.Length != 6L)
      UnityEngine.Debug.LogError((object) ("Expecting " + (object) 6U + " tabs, but found: " + (object) (this.tabs != null ? this.tabs.Length : 0)));
    List<LeaderboardToggle> leaderboardToggleList = new List<LeaderboardToggle>((IEnumerable<LeaderboardToggle>) this.tabs);
    foreach (LeaderboardToggle tab in this.tabs)
      tab.ToggleGroup = leaderboardToggleList;
  }

  private void InitButtonHandlers()
  {
    this.closeButton.onClick.AddListener(new UnityAction(this.Close));
    this.nextPageButton.OnClicked = new UnityAction(this.PageNext);
    this.previousPageButton.OnClicked = new UnityAction(this.PagePrevious);
    this.firstPageButton.OnClicked = new UnityAction(this.PageFirst);
    this.lastPageButton.OnClicked = new UnityAction(this.PageLast);
    this.sortByScore1Toggle.onValueChanged.AddListener((UnityAction<bool>) (selected =>
    {
      if (!selected)
        return;
      this.OnResultSortingChanged((byte) 1);
    }));
    this.sortByScore2Toggle.onValueChanged.AddListener((UnityAction<bool>) (selected =>
    {
      if (!selected)
        return;
      this.OnResultSortingChanged((byte) 2);
    }));
    this.sortByScore3Toggle.onValueChanged.AddListener((UnityAction<bool>) (selected =>
    {
      if (!selected)
        return;
      this.OnResultSortingChanged((byte) 3);
    }));
    for (ushort index = 0; (uint) index < 6U; ++index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      LeaderboardView.\u003CInitButtonHandlers\u003Ec__AnonStoreyA4 handlersCAnonStoreyA4 = new LeaderboardView.\u003CInitButtonHandlers\u003Ec__AnonStoreyA4();
      // ISSUE: reference to a compiler-generated field
      handlersCAnonStoreyA4.\u003C\u003Ef__this = this;
      LeaderboardTab leaderboardTab = this.tabs[(int) index];
      // ISSUE: reference to a compiler-generated field
      handlersCAnonStoreyA4.tabBracketId = index;
      // ISSUE: reference to a compiler-generated method
      leaderboardTab.GetComponent<Button>().onClick.AddListener(new UnityAction(handlersCAnonStoreyA4.\u003C\u003Em__153));
    }
    this.jumpToMyRankingButton.onClick.AddListener(new UnityAction(this.JumpToMyRanking));
  }

  private void Start()
  {
    List<LeaderboardToggle> categoryToggleGroup = new List<LeaderboardToggle>();
    List<LeaderboardToggle> subCategoryToggleGroup = new List<LeaderboardToggle>();
    foreach (ushort num in Enum.GetValues(typeof (RankingGroup)))
    {
      RankingGroup group = (RankingGroup) num;
      this.AddCategory(LeaderboardView.GetTextForRankingGroup(group), group, categoryToggleGroup, subCategoryToggleGroup, group == this.selectedGroup);
    }
    this.UpdateLabels(this.selectedGroup, this.selectedRankingType);
    this.tabs[0].Selected = true;
    this.ChangeMode(RankingMode.Counter);
    this.noDataLabel.text = BsgoLocalization.Get("bgo.leaderboard.no_data");
    this.navigationLabel.text = BsgoLocalization.Get("bgo.leaderboard.navigation").ToUpperInvariant();
    this.titleLabel.text = BsgoLocalization.Get("bgo.leaderboard.title").ToUpperInvariant();
    this.rankColumnLabel.text = BsgoLocalization.Get("bgo.leaderboard.rank");
    this.playerColumnLabel.text = BsgoLocalization.Get("bgo.leaderboard.player");
  }

  private void AddCategory(string name, RankingGroup group, List<LeaderboardToggle> categoryToggleGroup, List<LeaderboardToggle> subCategoryToggleGroup, bool select)
  {
    GameObject child1 = UguiTools.CreateChild("Leaderboard/LeaderboardCategory", this.navigationAnchor);
    GameObject child2 = UguiTools.CreateChild("Leaderboard/LeaderboardSubcategory", this.navigationAnchor);
    LeaderboardCategoryFoldout component = child1.GetComponent<LeaderboardCategoryFoldout>();
    component.foldout = child2;
    component.ToggleGroup = categoryToggleGroup;
    component.SetSubCategories(group, new RankingType[2]
    {
      RankingType.Regular,
      RankingType.Delta
    }, new LeaderboardCategoryFoldout.OnRankingTypeSelected(this.OnRankingTypeChanged), subCategoryToggleGroup);
    child2.SetActive(false);
    component.Caption = name;
    categoryToggleGroup.Add((LeaderboardToggle) component);
    if (!select)
      return;
    component.Selected = true;
  }

  private void AddTournamentCategory(string name, TournamentRankingGroup group, List<LeaderboardToggle> categoryToggleGroup, List<LeaderboardToggle> subCategoryToggleGroup)
  {
    GameObject child1 = UguiTools.CreateChild("Leaderboard/LeaderboardCategory", this.navigationAnchor);
    GameObject child2 = UguiTools.CreateChild("Leaderboard/LeaderboardSubcategory", this.navigationAnchor);
    LeaderboardCategoryFoldout component = child1.GetComponent<LeaderboardCategoryFoldout>();
    component.foldout = child2;
    component.ToggleGroup = categoryToggleGroup;
    component.SetSubCategories(group, new TournamentRankingType[3]
    {
      TournamentRankingType.Current,
      TournamentRankingType.Last,
      TournamentRankingType.SecondLast
    }, new LeaderboardCategoryFoldout.OnTournamentRankingTypeSelected(this.OnTournamentRankingTypeChanged), subCategoryToggleGroup);
    child2.SetActive(false);
    component.Caption = name;
    categoryToggleGroup.Add((LeaderboardToggle) component);
  }

  public void RankingTournamentUpdate(RankingTournamentData data)
  {
    if (data.Group != this.selectedTournamentGroup || data.RankingType != this.selectedTournamentRankingType)
    {
      UnityEngine.Debug.LogWarning((object) "Receiving ranking data for a leaderboard which is currently not displayed");
    }
    else
    {
      if ((long) data.Ranks.Count > 16L)
        UnityEngine.Debug.LogWarning((object) ("Server sent too many leaderboard entries, expected: " + (object) 16U + " received: " + (object) data.Ranks.Count));
      this.lastPage = data.TotalEntries / 17U;
      this.UpdatePageButtons();
      this.SetLastUpdate(data.LastUpdate);
      this.UpdateEntries(data.Ranks, new RankingGroup?());
    }
  }

  public void RankingCounterUpdate(RankingCounterData data)
  {
    if (data.Group != this.selectedGroup || data.RankingType != this.selectedRankingType)
    {
      UnityEngine.Debug.LogWarning((object) "Receiving ranking data for a leaderboard which is currently not displayed");
    }
    else
    {
      if ((long) data.Ranks.Count > 16L)
        UnityEngine.Debug.LogWarning((object) ("Server sent too many leaderboard entries, expected: " + (object) 16U + " received: " + (object) data.Ranks.Count));
      this.lastPage = data.TotalEntries / 17U;
      this.UpdatePageButtons();
      this.SetLastUpdate(data.LastUpdate);
      this.UpdateEntries(data.Ranks, new RankingGroup?(data.Group));
    }
  }

  private void UpdateEntries(List<RankDescription> ranks, RankingGroup? group)
  {
    for (int index = 0; index < this.entryWidgets.Length; ++index)
    {
      LeaderboardEntryWidget leaderboardEntryWidget = this.entryWidgets[index];
      if (index >= ranks.Count)
      {
        leaderboardEntryWidget.gameObject.SetActive(false);
      }
      else
      {
        leaderboardEntryWidget.gameObject.SetActive(true);
        leaderboardEntryWidget.Display(ranks[index], this.selectedRankingType == RankingType.Delta && LeaderboardView.IsRatioRankingGroup(group.GetValueOrDefault()));
      }
    }
    bool flag = ranks.Count > 0;
    this.noDataLabel.gameObject.SetActive(!flag);
    this.rankingAnchor.gameObject.SetActive(flag);
  }

  public void RankingCounterPlayerUpdate(RankDescription data)
  {
    LeaderboardEntryWidget leaderboardEntryWidget = this.myPersonalRanking;
    RankDescription rank = data;
    RankingMode? nullable = this.mode;
    int num = (nullable.GetValueOrDefault() != RankingMode.Counter ? 0 : (nullable.HasValue ? 1 : 0)) == 0 || this.selectedRankingType != RankingType.Delta ? 0 : (LeaderboardView.IsRatioRankingGroup(this.selectedGroup) ? 1 : 0);
    leaderboardEntryWidget.Display(rank, num != 0);
    this.myRanking = data;
    this.jumpToMyRankingButton.interactable = (int) data.rank != 0;
  }

  private void RequestData()
  {
    RankingMode? nullable = this.mode;
    if (!nullable.HasValue)
      return;
    switch (nullable.Value)
    {
      case RankingMode.Counter:
        this.RequestCounterData();
        break;
      case RankingMode.Tournament:
        this.RequestTournamentData();
        break;
    }
  }

  private void RequestCounterData()
  {
    RankingProtocol.GetProtocol().RequestRankingCounter(this.selectedGroup, this.selectedRankingType, this.page, this.sortBy);
    RankingProtocol.GetProtocol().RequestRankingCounterPlayer(this.selectedGroup, this.selectedRankingType, this.sortBy);
  }

  private void RequestTournamentData()
  {
    RankingProtocol.GetProtocol().RequestRankingTournament(this.selectedTournamentGroup, this.selectedTournamentRankingType, this.bracketId, this.page, this.sortBy);
    RankingProtocol.GetProtocol().RequestRankingTournamentPlayer(this.selectedTournamentGroup, this.selectedTournamentRankingType, this.bracketId, this.sortBy);
  }

  private void JumpToMyRanking()
  {
    this.page = (this.myRanking.position - 1U) / 16U;
    this.UpdatePageButtons();
    this.RequestData();
  }

  public void OnShow()
  {
    this.rankingAnchor.gameObject.SetActive(false);
    this.StartCoroutine(this.RequestInitialData());
  }

  [DebuggerHidden]
  private IEnumerator RequestInitialData()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LeaderboardView.\u003CRequestInitialData\u003Ec__Iterator24() { \u003C\u003Ef__this = this };
  }

  public void Close()
  {
    FacadeFactory.GetInstance().SendMessage(Message.ShowLeaderboards);
  }

  private void OnRankingTypeChanged(RankingGroup group, RankingType rankingType)
  {
    this.ChangeMode(RankingMode.Counter);
    this.page = 0U;
    this.selectedGroup = group;
    this.selectedRankingType = rankingType;
    this.UpdateLabels(group, rankingType);
    if ((int) this.sortBy == 1)
    {
      this.RequestData();
    }
    else
    {
      this.sortByScore1Toggle.isOn = true;
      this.sortByScore2Toggle.isOn = false;
      this.sortByScore3Toggle.isOn = false;
    }
  }

  private void OnTournamentRankingTypeChanged(TournamentRankingGroup group, TournamentRankingType rankingType)
  {
    this.ChangeMode(RankingMode.Tournament);
    this.page = 0U;
    this.selectedTournamentGroup = group;
    this.selectedTournamentRankingType = rankingType;
    this.UpdateLabels(group, rankingType);
    if ((int) this.sortBy == 1)
    {
      this.RequestData();
    }
    else
    {
      this.sortByScore1Toggle.isOn = true;
      this.sortByScore2Toggle.isOn = false;
      this.sortByScore3Toggle.isOn = false;
    }
  }

  private void UpdateLabels(RankingGroup group, RankingType rankingType)
  {
    this.contentHeaderLabel.text = LeaderboardView.GetTextForRankingGroup(group).ToUpperInvariant() + " - " + LeaderboardView.GetTextForRankingType(rankingType, group).ToUpperInvariant();
    Vector3 localPosition = this.contentHeaderLabel.rectTransform.localPosition;
    localPosition.y = 235f;
    this.contentHeaderLabel.rectTransform.localPosition = localPosition;
    string[] scoreLabelsForGroup = LeaderboardView.GetScoreLabelsForGroup(group);
    this.score1Label.text = scoreLabelsForGroup[0];
    this.score2Label.text = scoreLabelsForGroup[1];
    this.score3Label.text = scoreLabelsForGroup[2];
  }

  private void UpdateLabels(TournamentRankingGroup group, TournamentRankingType rankingType)
  {
    this.contentHeaderLabel.text = LeaderboardView.GetTextForTournamentRankingGroup(group).ToUpperInvariant() + " - " + LeaderboardView.GetTextForTournamentRankingType(rankingType).ToUpperInvariant();
    Vector3 localPosition = this.contentHeaderLabel.rectTransform.localPosition;
    localPosition.y = 265f;
    this.contentHeaderLabel.rectTransform.localPosition = localPosition;
    string[] forTournamentGroup = LeaderboardView.GetScoreLabelsForTournamentGroup(group);
    this.score1Label.text = forTournamentGroup[0];
    this.score2Label.text = forTournamentGroup[1];
    this.score3Label.text = forTournamentGroup[2];
  }

  private void OnResultSortingChanged(byte sortBy)
  {
    this.sortBy = sortBy;
    this.RequestData();
  }

  private void OnBracketChanged(ushort bracket)
  {
    this.bracketId = bracket;
    this.page = 0U;
    this.RequestData();
  }

  private void UpdatePageButtons()
  {
    SystemButton systemButton1 = this.firstPageButton;
    bool flag1 = this.page <= 0U;
    this.previousPageButton.Disabled = flag1;
    int num1 = flag1 ? 1 : 0;
    systemButton1.Disabled = num1 != 0;
    SystemButton systemButton2 = this.lastPageButton;
    bool flag2 = this.page >= this.lastPage;
    this.nextPageButton.Disabled = flag2;
    int num2 = flag2 ? 1 : 0;
    systemButton2.Disabled = num2 != 0;
    this.pageLabel.text = ((int) this.page + 1).ToString() + " / " + (object) (uint) ((int) this.lastPage + 1);
  }

  private void PageNext()
  {
    ++this.page;
    this.UpdatePageButtons();
    this.RequestData();
  }

  private void PagePrevious()
  {
    --this.page;
    this.UpdatePageButtons();
    this.RequestData();
  }

  private void PageFirst()
  {
    this.page = 0U;
    this.UpdatePageButtons();
    this.RequestData();
  }

  private void PageLast()
  {
    this.page = this.lastPage;
    this.UpdatePageButtons();
    this.RequestData();
  }

  private void ChangeMode(RankingMode newMode)
  {
    RankingMode? nullable1 = this.mode;
    if ((nullable1.GetValueOrDefault() != newMode ? 0 : (nullable1.HasValue ? 1 : 0)) != 0)
      return;
    this.mode = new RankingMode?(newMode);
    RankingMode? nullable2 = this.mode;
    if (!nullable2.HasValue)
      return;
    switch (nullable2.Value)
    {
      case RankingMode.Counter:
        this.SetColumnEnabled(LeaderboardColumn.Column3, true);
        this.tabsAnchor.gameObject.SetActive(false);
        break;
      case RankingMode.Tournament:
        this.SetColumnEnabled(LeaderboardColumn.Column3, false);
        this.tabsAnchor.gameObject.SetActive(true);
        break;
    }
  }

  private void SetLastUpdate(DateTime lastUpdate)
  {
    this.lastUpdateLabel.text = BsgoLocalization.Get("bgo.leaderboard.last_update") + "\n" + string.Format("{0:g}", (object) lastUpdate);
  }

  private void SetColumnEnabled(LeaderboardColumn column, bool enable)
  {
    Text text = (Text) null;
    Toggle toggle = (Toggle) null;
    switch (column)
    {
      case LeaderboardColumn.Column1:
        text = this.score1Label;
        toggle = this.sortByScore1Toggle;
        break;
      case LeaderboardColumn.Column2:
        text = this.score2Label;
        toggle = this.sortByScore2Toggle;
        break;
      case LeaderboardColumn.Column3:
        text = this.score3Label;
        toggle = this.sortByScore3Toggle;
        break;
    }
    text.gameObject.SetActive(enable);
    toggle.gameObject.SetActive(enable);
    foreach (LeaderboardEntryWidget entryWidget in this.entryWidgets)
      entryWidget.SetColumnEnabled(column, enable);
    this.myPersonalRanking.SetColumnEnabled(column, enable);
  }

  public static string GetTextForRankingGroup(RankingGroup group)
  {
    return BsgoLocalization.Get("bgo.leaderboard.group." + group.ToString().ToLowerInvariant() + ".name");
  }

  public static string GetTextForTournamentRankingGroup(TournamentRankingGroup group)
  {
    return BsgoLocalization.Get("bgo.leaderboard.group_tournament." + group.ToString().ToLowerInvariant() + ".name");
  }

  public static string[] GetScoreLabelsForTournamentGroup(TournamentRankingGroup group)
  {
    return new string[3]{ BsgoLocalization.Get("bgo.leaderboard.group_tournament.1"), BsgoLocalization.Get("bgo.leaderboard.group_tournament.2"), "EMPTY" };
  }

  public static string[] GetScoreLabelsForGroup(RankingGroup group)
  {
    string str = "bgo.leaderboard.group." + group.ToString().ToLowerInvariant();
    return new string[3]{ BsgoLocalization.Get(str + ".1"), BsgoLocalization.Get(str + ".2"), BsgoLocalization.Get(str + ".3") };
  }

  private static bool IsRatioRankingGroup(RankingGroup group)
  {
    if (group != RankingGroup.VictoriesDefeatRatio)
      return group == RankingGroup.VictoriesHour;
    return true;
  }

  public static string GetTextForRankingType(RankingType rankingType, RankingGroup group)
  {
    if (LeaderboardView.IsRatioRankingGroup(group) && rankingType == RankingType.Delta)
      return BsgoLocalization.Get("bgo.leaderboard.type.delta_ratio");
    return BsgoLocalization.Get("bgo.leaderboard.type." + rankingType.ToString().ToLowerInvariant());
  }

  public static string GetTextForTournamentRankingType(TournamentRankingType rankingType)
  {
    return BsgoLocalization.Get("bgo.leaderboard.type." + rankingType.ToString().ToLowerInvariant());
  }
}
