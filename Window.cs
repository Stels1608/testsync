﻿// Decompiled with JetBrains decompiler
// Type: Window
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class Window : MonoBehaviour, IGUIRenderable
{
  public virtual bool IsRendered
  {
    get
    {
      return this.gameObject.activeInHierarchy;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  public abstract bool IsBigWindow { get; }

  protected virtual bool IsInCombatGui
  {
    get
    {
      return true;
    }
  }

  protected virtual void Awake()
  {
    WindowManager.Instance.RegisterWindow(this);
    if (!this.IsInCombatGui)
      return;
    FacadeFactory.GetInstance().SendMessage(Message.AddToCombatGui, (object) this);
  }

  protected virtual void Start()
  {
  }

  protected virtual void OnDestroy()
  {
    WindowManager.Instance.UnregisterWindow(this);
    FacadeFactory.GetInstance().SendMessage(Message.RemoveFromCombatGui, (object) this);
  }

  protected virtual void OnEnable()
  {
  }
}
