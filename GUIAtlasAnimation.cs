﻿// Decompiled with JetBrains decompiler
// Type: GUIAtlasAnimation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIAtlasAnimation : GUIPanel
{
  private float period = 1f;
  private AtlasAnimation animation;

  public float Period
  {
    get
    {
      return this.period;
    }
    set
    {
      this.period = value;
      this.animation.PlaybackPosition = 0.0f;
    }
  }

  public GUIAtlasAnimation(Texture2D atlasTexture, uint width, uint height)
  {
    this.animation = new AtlasAnimation(atlasTexture, width, height);
    this.Size = new float2((float) ((long) atlasTexture.width / (long) width), (float) ((long) atlasTexture.height / (long) height));
    this.IsRendered = true;
  }

  public override void Update()
  {
    base.Update();
    this.animation.PlaybackPosition = (this.animation.PlaybackPosition + Time.deltaTime * this.Period) % 1f;
  }

  public override void Draw()
  {
    base.Draw();
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    Graphics.DrawTexture(this.SmartRect.AbsRect, (Texture) this.animation.Texture, this.animation.FrameRect, 0, 0, 0, 0);
  }
}
