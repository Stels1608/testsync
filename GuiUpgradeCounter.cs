﻿// Decompiled with JetBrains decompiler
// Type: GuiUpgradeCounter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GuiUpgradeCounter : GuiPanel
{
  private readonly float space = 3f;
  private readonly float space_sale_glow = -4f;
  private List<GuiButton> images = new List<GuiButton>();
  private List<GuiImage> sale_images_glow = new List<GuiImage>();
  [Gui2Editor]
  public int selectedLevel;
  public int cardLevel;
  public AnonymousDelegate OnIncrementPressed;
  public AnonymousDelegate OnDecrementPressed;

  public GuiUpgradeCounter()
  {
    this.SizeY = 24f;
    GuiButton guiButton1 = new GuiButton(string.Empty, "GUI/EquipBuyPanel/DetailsWindow/minus", "GUI/EquipBuyPanel/DetailsWindow/minus_over", "GUI/EquipBuyPanel/DetailsWindow/minus_click");
    guiButton1.Pressed = (AnonymousDelegate) (() =>
    {
      --this.selectedLevel;
      (this.Parent as GuiElementBase).PeriodicUpdate();
      if (this.OnDecrementPressed == null)
        return;
      this.OnDecrementPressed();
    });
    this.AddChild((GuiElementBase) guiButton1, Align.MiddleLeft);
    float x = guiButton1.SizeX + this.space;
    for (int i = 0; i < 15; ++i)
    {
      GuiImage guiImage = i >= 10 ? new GuiImage("GUI/EquipBuyPanel/DetailsWindow/Stars/btn_upgrade_glow_star") : new GuiImage("GUI/EquipBuyPanel/DetailsWindow/btn_upgrade_glow");
      guiImage.Size = new Vector2((float) guiImage.Texture.width, (float) guiImage.Texture.height);
      this.sale_images_glow.Add(guiImage);
      this.AddChild((GuiElementBase) guiImage, Align.MiddleLeft, new Vector2(x + this.space_sale_glow, 0.0f));
      if (i <= 9)
        guiImage.Size = new Vector2(26f, 26f);
      GuiButton guiButton2 = new GuiButton(string.Empty);
      this.images.Add(guiButton2);
      this.AddChild((GuiElementBase) guiButton2, Align.MiddleLeft, new Vector2(x, 0.0f));
      this.MarkAvailable(i);
      x += guiButton2.SizeX + this.space;
    }
    GuiButton guiButton3 = new GuiButton(string.Empty, "GUI/EquipBuyPanel/DetailsWindow/plus", "GUI/EquipBuyPanel/DetailsWindow/plus_over", "GUI/EquipBuyPanel/DetailsWindow/plus_click");
    guiButton3.Pressed = (AnonymousDelegate) (() =>
    {
      if (this.cardLevel < 10)
      {
        if (this.selectedLevel < 10)
          ++this.selectedLevel;
      }
      else
        ++this.selectedLevel;
      (this.Parent as GuiElementBase).PeriodicUpdate();
      if (this.OnIncrementPressed == null)
        return;
      this.OnIncrementPressed();
    });
    this.AddChild((GuiElementBase) guiButton3, Align.MiddleLeft, new Vector2(x, 0.0f));
    this.SizeX = x + guiButton3.SizeX;
  }

  public void UpdateCounter(ShipSystem system)
  {
    if (system == null)
      return;
    this.cardLevel = (int) system.Card.Level;
    this.selectedLevel = Mathf.Clamp(this.selectedLevel, this.cardLevel, (int) system.Card.MaxLevel);
    while (this.selectedLevel > 0 && !this.AvailableToUpgrade(system, this.selectedLevel))
      --this.selectedLevel;
    for (int index = 0; index < this.images.Count; ++index)
    {
      if (!this.AvailableToUpgrade(system, index + 1))
        this.MarkNotAvailable(index);
      else if (this.AlreadyIsUpgraded(system, index + 1))
        this.MarkUpgraded(index);
      else if (index + 1 <= this.selectedLevel)
        this.MarkSelected(index);
      else
        this.MarkAvailable(index);
      this.UpdateDiscountFrame(Shop.IsUpgradeDiscounted(Shop.FindShopCategoryForSlotType(system.Card.SlotType), index), index);
    }
  }

  private bool AlreadyIsUpgraded(ShipSystem system, int level)
  {
    return level <= (int) system.Card.Level;
  }

  private bool AvailableToUpgrade(ShipSystem system, int level)
  {
    if (level > (int) system.Card.MaxLevel)
      return false;
    int skillLevel = 0;
    List<PlayerSkill> needSkills = (List<PlayerSkill>) null;
    return Game.Me.RequiresSkillsForSystem(system, level, out skillLevel, out needSkills);
  }

  private void MarkUpgraded(int i)
  {
    GuiButton guiButton = this.images[i];
    guiButton.NormalTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? string.Empty : "/Stars") + "/darkblue");
    guiButton.OverTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? string.Empty : "/Stars") + "/darkblue_over");
    guiButton.PressedTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? string.Empty : "/Stars") + "/darkblue_click");
    guiButton.Size = new Vector2((float) guiButton.NormalTexture.width, (float) guiButton.NormalTexture.height);
    if (i > 9)
      return;
    guiButton.Size = new Vector2(19f, 18f);
  }

  private void MarkSelected(int i)
  {
    GuiButton guiButton = this.images[i];
    guiButton.NormalTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? "/lightbluebox" : "/Stars/lightblue"));
    guiButton.OverTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? "/lightbluebox_over" : "/Stars/lightblue_over"));
    guiButton.PressedTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? "/lightbluebox_click" : "/Stars/lightblue_click"));
    guiButton.Size = new Vector2((float) guiButton.NormalTexture.width, (float) guiButton.NormalTexture.height);
    if (i > 9)
      return;
    guiButton.Size = new Vector2(19f, 18f);
  }

  private void MarkAvailable(int i)
  {
    GuiButton guiButton = this.images[i];
    guiButton.NormalTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? "/whitebox" : "/Stars/white"));
    guiButton.OverTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? "/whitebox_over" : "/Stars/white_over"));
    guiButton.PressedTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? "/whitebox_click" : "/Stars/white_click"));
    guiButton.Size = new Vector2((float) guiButton.NormalTexture.width, (float) guiButton.NormalTexture.height);
    if (i > 9)
      return;
    guiButton.Size = new Vector2(19f, 18f);
  }

  private void MarkNotAvailable(int i)
  {
    GuiButton guiButton = this.images[i];
    guiButton.NormalTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? "/redbox" : "/Stars/red"));
    guiButton.OverTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? "/redbox_over" : "/Stars/red_over"));
    guiButton.PressedTexture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/DetailsWindow" + (i <= 9 ? "/redbox_click" : "/Stars/red_click"));
    guiButton.Size = new Vector2((float) guiButton.NormalTexture.width, (float) guiButton.NormalTexture.height);
    if (i > 9)
      return;
    guiButton.Size = new Vector2(19f, 18f);
  }

  private void UpdateDiscountFrame(bool isOnDiscount, int level)
  {
    GuiImage guiImage = this.sale_images_glow[level];
    bool flag = isOnDiscount;
    this.sale_images_glow[level].IsRendered = flag;
    int num = flag ? 1 : 0;
    guiImage.IsRendered = num != 0;
  }
}
