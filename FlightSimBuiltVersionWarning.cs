﻿// Decompiled with JetBrains decompiler
// Type: FlightSimBuiltVersionWarning
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FlightSimBuiltVersionWarning : MonoBehaviour
{
  public void OnGUI()
  {
    GUIContent content = new GUIContent("FlightSim: You're running in webplayer mode. Switch to standalone to make this work.");
    GUI.Label(new Rect((float) ((double) Screen.width / 2.0 - (double) GUI.skin.label.CalcSize(content).x / 2.0), (float) Screen.height / 2f, (float) Screen.width, (float) Screen.height), content);
  }
}
