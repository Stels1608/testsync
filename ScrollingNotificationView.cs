﻿// Decompiled with JetBrains decompiler
// Type: ScrollingNotificationView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollingNotificationView : OnScreenNotificationView
{
  private readonly Queue<OnScreenNotification> queue = new Queue<OnScreenNotification>();
  private readonly List<Text> textPool = new List<Text>();
  private const int MAX_MESSAGES = 5;
  private const float MESSAGE_COOLDOWN = 0.45f;
  private const float FADE_DURATION = 2f;
  private const float OFFSET = 100f;
  private float lastMessageTimestamp;

  private void Awake()
  {
    for (int index = 0; index < 5; ++index)
    {
      Text onScreenText = this.CreateOnScreenText();
      onScreenText.gameObject.SetActive(false);
      this.textPool.Add(onScreenText);
    }
  }

  public override void ShowOnScreenNotification(OnScreenNotification notification)
  {
    if (!notification.Show)
      return;
    if (this.queue.Count > 100)
      Debug.LogWarning((object) ("Warning: Too many OnScreenNotifications in queue, discarding: " + (object) notification.GetType()));
    else
      this.queue.Enqueue(notification);
  }

  private void Update()
  {
    if (this.queue.Count == 0 || (double) Time.time - (double) this.lastMessageTimestamp < 0.449999988079071)
      return;
    Text textFromPool = this.GetTextFromPool();
    if ((UnityEngine.Object) textFromPool == (UnityEngine.Object) null)
      return;
    OnScreenNotification notification = this.queue.Dequeue();
    if (string.IsNullOrEmpty(notification.TextMessage))
    {
      Debug.LogWarning((object) ("WARNING: OnScreenNotification " + (object) notification.GetType() + " has no valid text"));
    }
    else
    {
      this.ShowMessage(textFromPool, notification.TextMessage, this.GetMessageColor(notification));
      this.lastMessageTimestamp = Time.time;
    }
  }

  private Color GetMessageColor(OnScreenNotification notification)
  {
    Color white = Color.white;
    switch (notification.Category)
    {
      case NotificationCategory.Positive:
        white = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_POSITIVE);
        break;
      case NotificationCategory.Negative:
        white = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_NEGATIVE);
        break;
      case NotificationCategory.Special:
        white = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_SPECIAL);
        break;
    }
    return white;
  }

  private Text GetTextFromPool()
  {
    for (int index = 0; index < this.textPool.Count; ++index)
    {
      Text text = this.textPool[index];
      if (!text.gameObject.activeSelf)
        return text;
    }
    return (Text) null;
  }

  private void ShowMessage(Text text, string message, Color color)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ScrollingNotificationView.\u003CShowMessage\u003Ec__AnonStoreyD0 messageCAnonStoreyD0 = new ScrollingNotificationView.\u003CShowMessage\u003Ec__AnonStoreyD0();
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD0.text = text;
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD0.text.gameObject.SetActive(true);
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD0.text.text = message.ToUpperInvariant();
    color.a = 0.25f;
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD0.text.color = color;
    // ISSUE: reference to a compiler-generated field
    messageCAnonStoreyD0.text.rectTransform.localPosition = new Vector3(0.0f, -100f, 0.0f);
    // ISSUE: reference to a compiler-generated field
    float to = messageCAnonStoreyD0.text.transform.position.y + 100f;
    // ISSUE: reference to a compiler-generated field
    LeanTween.moveY(messageCAnonStoreyD0.text.gameObject, to, 2f);
    // ISSUE: reference to a compiler-generated field
    LTDescr ltDescr1 = LeanTween.scale(messageCAnonStoreyD0.text.rectTransform, Vector3.one * 1.1f, 1f);
    ltDescr1.loopCount = 2;
    ltDescr1.setLoopPingPong();
    // ISSUE: reference to a compiler-generated field
    LTDescr ltDescr2 = LeanTween.textAlpha(messageCAnonStoreyD0.text.rectTransform, 1f, 1f);
    ltDescr2.setLoopPingPong();
    ltDescr2.loopCount = 2;
    // ISSUE: reference to a compiler-generated method
    ltDescr2.onComplete = new System.Action(messageCAnonStoreyD0.\u003C\u003Em__1F3);
  }

  private Text CreateOnScreenText()
  {
    GameObject gameObject = new GameObject("onscreen_notification");
    Text text = gameObject.AddComponent<Text>();
    text.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 900f);
    text.alignment = TextAnchor.LowerCenter;
    text.font = Gui.Options.fontEurostileTRegCon;
    text.fontSize = 20;
    gameObject.transform.SetParent(this.transform, false);
    return text;
  }
}
