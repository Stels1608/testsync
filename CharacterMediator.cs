﻿// Decompiled with JetBrains decompiler
// Type: CharacterMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "CharacterMediator";
  private CharacterMenu view;

  public CharacterMediator()
    : base("CharacterMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.EnterCharacterMenu);
    this.AddMessageInterest(Message.LeaveCharacterMenu);
    this.AddMessageInterest(Message.CharacterServiceInfo);
    this.AddMessageInterest(Message.RegisterCharacterMenu);
    this.AddMessageInterest(Message.RequestNameChange);
    this.AddMessageInterest(Message.CancelNameChange);
    this.AddMessageInterest(Message.RequestFactionSwitch);
    this.AddMessageInterest(Message.CancelFactionSwitch);
    this.AddMessageInterest(Message.RequestCharacterDeletion);
    this.AddMessageInterest(Message.CancelCharacterDeletion);
    this.AddMessageInterest(Message.RequestAvatarChange);
    this.AddMessageInterest(Message.CancelAvatarChange);
    this.AddMessageInterest(Message.ConfirmAvatarChange);
    this.AddMessageInterest(Message.AvatarLoaded);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.EnterCharacterMenu:
        this.OwnerFacade.SendMessage(Message.ToggleOldUi, (object) false);
        this.OwnerFacade.SendMessage(Message.ToggleNgui, (object) false);
        this.OwnerFacade.SendMessage(Message.ToggleUGuiHubCanvas, (object) false);
        this.OwnerFacade.SendMessage(Message.ToggleUGuiHudCanvas, (object) false);
        this.OwnerFacade.SendMessage(Message.ToggleUGuiWindowsCanvas, (object) false);
        Application.LoadLevelAdditive("CharacterScene");
        PlayerProtocol.GetProtocol().RequestCharacterServices();
        break;
      case Message.LeaveCharacterMenu:
        Object.FindObjectOfType<CharacterSceneRoot>().Destroy();
        Game.GUIManager.DialogBlockers.Remove((IGUIRenderable) this.view);
        this.OwnerFacade.SendMessage(Message.ToggleOldUi, (object) true);
        this.OwnerFacade.SendMessage(Message.ToggleNgui, (object) true);
        this.OwnerFacade.SendMessage(Message.ToggleUGuiHubCanvas, (object) true);
        this.OwnerFacade.SendMessage(Message.ToggleUGuiHudCanvas, (object) true);
        this.OwnerFacade.SendMessage(Message.ToggleUGuiWindowsCanvas, (object) true);
        this.view = (CharacterMenu) null;
        Object.FindObjectOfType<RoomSettings>().Start();
        break;
      case Message.RegisterCharacterMenu:
        this.view = (CharacterMenu) message.Data;
        this.view.SetCharacterInfo(Game.Me.Name, Game.Me.Faction, Game.Me.Level);
        Game.GUIManager.DialogBlockers.Add((IGUIRenderable) this.view);
        break;
      case Message.CharacterServiceInfo:
        ((CharacterServiceDataProvider) this.OwnerFacade.FetchDataProvider("CharacterServiceDataProvider")).CharacterServices = (List<CharacterService>) message.Data;
        Object.FindObjectOfType<CharacterSceneRoot>().OnCharacterServiceInfoReceived();
        break;
      case Message.AvatarLoaded:
        this.view.OnAvatarLoaded();
        break;
      case Message.RequestNameChange:
      case Message.RequestFactionSwitch:
      case Message.RequestCharacterDeletion:
      case Message.RequestAvatarChange:
        this.view.gameObject.SetActive(false);
        break;
      case Message.CancelNameChange:
      case Message.CancelFactionSwitch:
      case Message.CancelCharacterDeletion:
      case Message.CancelAvatarChange:
      case Message.ConfirmAvatarChange:
        this.view.gameObject.SetActive(true);
        break;
    }
  }
}
