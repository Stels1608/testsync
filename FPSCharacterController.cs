﻿// Decompiled with JetBrains decompiler
// Type: FPSCharacterController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class FPSCharacterController : MonoBehaviour
{
  public float walkMultiplier = 0.5f;
  private CharacterMotor motor;
  public bool defaultIsWalk;

  private void Start()
  {
    this.motor = this.GetComponent(typeof (CharacterMotor)) as CharacterMotor;
    if ((Object) this.motor == (Object) null)
      Debug.Log((object) "Motor is null!!");
    this.motor.desiredFacingDirection = this.transform.forward;
  }

  private void Update()
  {
    Vector3 vector3 = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
    if ((double) vector3.magnitude > 1.0)
      vector3 = vector3.normalized;
    if ((double) this.walkMultiplier != 1.0 && (Input.GetKey("left shift") ? 1 : (Input.GetKey("right shift") ? 1 : 0)) != (this.defaultIsWalk ? 1 : 0))
      vector3 *= this.walkMultiplier;
    this.motor.desiredMovementDirection = vector3;
  }
}
