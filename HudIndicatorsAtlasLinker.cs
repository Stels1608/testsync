﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorsAtlasLinker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class HudIndicatorsAtlasLinker
{
  private static readonly HudIndicatorsAtlasLinker instance = new HudIndicatorsAtlasLinker();
  private const string ATLAS_PATH = "GUI/gui_2013/atlas/HudIndicatorsAtlas/HudIndicatorsAtlas";
  public const string BracketBig = "HudIndicator_Bracket_Big";
  public const string BracketBigSelected = "HudIndicator_Bracket_Big_Selected";
  public const string BracketSmall = "HudIndicator_Bracket_Small";
  public const string BracketSmallSelected = "HudIndicator_Bracket_Small_Selected";
  public const string DesignatedTargetInnerTriangle = "HudIndicator_DesignatedTarget_InnerTriangle";
  public const string DesignatedTargetOuterTriangle = "HudIndicator_DesignatedTarget_OuterTriangle";
  public const string DistanceLine = "HudIndicator_Distance";
  public const string Dps = "HudIndicator_Dps";
  public const string Arrow = "HudIndicator_Locator";
  public const string MissionWaypoint = "HudIndicator_Mission_Waypoint";
  public const string MultiTarget = "HudIndicator_Multi_Target";
  public const string SectorEventNormal = "HudIndicator_SectorEvent_Normal";
  public const string AsteroidFrame = "HudIndicator_Asteroid_Frame";
  public const string AsteroidBlank = "HudIndicator_Asteroid_Blank";
  public const string AsteroidTylium = "HudIndicator_Asteroid_Tylium";
  public const string AsteroidTitanium = "HudIndicator_Asteroid_Titanium";
  public const string AsteroidWater = "HudIndicator_Asteroid_Water";
  public const string AsteroidPlutonium = "HudIndicator_Asteroid_Plutonium";
  public const string AsteroidUranium = "HudIndicator_Asteroid_Uranium";
  private readonly UIAtlas uiAtlas;

  public static HudIndicatorsAtlasLinker Instance
  {
    get
    {
      return HudIndicatorsAtlasLinker.instance;
    }
  }

  public UIAtlas Atlas
  {
    get
    {
      return this.uiAtlas;
    }
  }

  private HudIndicatorsAtlasLinker()
  {
    GameObject gameObject = Resources.Load("GUI/gui_2013/atlas/HudIndicatorsAtlas/HudIndicatorsAtlas", typeof (GameObject)) as GameObject;
    if ((Object) gameObject == (Object) null)
      Debug.LogError((object) "Atlas not found: GUI/gui_2013/atlas/HudIndicatorsAtlas/HudIndicatorsAtlas");
    this.uiAtlas = gameObject.GetComponent<UIAtlas>();
    this.TestAtlas();
  }

  public void TestAtlas()
  {
    using (List<string>.Enumerator enumerator = new List<string>() { "HudIndicator_DesignatedTarget_InnerTriangle", "HudIndicator_DesignatedTarget_OuterTriangle", "HudIndicator_Distance", "HudIndicator_Dps", "HudIndicator_Locator", "HudIndicator_Bracket_Small", "HudIndicator_Bracket_Small_Selected", "HudIndicator_Mission_Waypoint", "HudIndicator_Multi_Target", "HudIndicator_Bracket_Big", "HudIndicator_Bracket_Big_Selected", "HudIndicator_SectorEvent_Normal", "HudIndicator_Asteroid_Frame", "HudIndicator_Asteroid_Blank", "HudIndicator_Asteroid_Tylium", "HudIndicator_Asteroid_Titanium", "HudIndicator_Asteroid_Water", "HudIndicator_Asteroid_Plutonium", "HudIndicator_Asteroid_Uranium" }.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        if (this.Atlas.GetSprite(current) == null)
          Debug.LogError((object) ("HudIndicatorsAtlasLinker: Can't load defined sprite " + current + " from Atlas GUI/gui_2013/atlas/HudIndicatorsAtlas/HudIndicatorsAtlas"));
      }
    }
  }
}
