﻿// Decompiled with JetBrains decompiler
// Type: CutsceneEvents
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CutsceneEvents : MonoBehaviour
{
  private VideoUiHandler videoUiHandler;

  public VideoUiHandler UiHandler
  {
    get
    {
      return this.videoUiHandler;
    }
  }

  private void Awake()
  {
    this.videoUiHandler = new GameObject("VideoUiHandler")
    {
      transform = {
        parent = this.transform
      }
    }.AddComponent<VideoUiHandler>();
  }

  public void AbortCurrentCutscene()
  {
    CutsceneManager.GetCurrentCutscene().AbortDelayed();
  }

  public void FadeOut()
  {
    this.FadeOutF(2f);
  }

  public void FadeOutF(float fadeOutTime)
  {
    CutsceneVideoFx.FadeOut(this.videoUiHandler, fadeOutTime, 0.0f);
  }

  public void FadeIn()
  {
    this.FadeInF(2f);
  }

  public void FadeInF(float fadeInTime)
  {
    CutsceneVideoFx.FadeIn(this.videoUiHandler, fadeInTime, 0.0f);
  }

  public void TurnToBlack()
  {
    CutsceneVideoFx.TurnToBlack(this.videoUiHandler);
  }

  public void TurnToBlackS(string cameraId)
  {
    CutsceneVideoFx.TurnToBlack(this.videoUiHandler);
  }

  public void FadeInVideoStripes()
  {
    CutsceneVideoFx.FadeInVideoStripes(this.videoUiHandler, 1f);
  }

  public void FadeInVideoStripesF(float fadeInTime)
  {
    CutsceneVideoFx.FadeInVideoStripes(this.videoUiHandler, fadeInTime);
  }

  public void FadeOutVideoStripes()
  {
    CutsceneVideoFx.FadeOutVideoStripes(this.videoUiHandler);
  }

  public void ShowShrinkingCenterText(string text)
  {
    CutsceneVideoFx.ShowShrinkingCenterText(this.videoUiHandler, 0.0f, text);
  }

  public void ShowSubtitleText(string text)
  {
    CutsceneVideoFx.ShowSubtitleText(this.videoUiHandler, 0.0f, text);
  }

  public void ShowSubtitleTextCylon(string text)
  {
    if (Game.Me.Faction != Faction.Cylon)
      return;
    this.ShowSubtitleText(text);
  }

  public void ShowSubtitleTextColonial(string text)
  {
    if (Game.Me.Faction != Faction.Colonial && !SectorEditorHelper.IsEditorLevel())
      return;
    this.ShowSubtitleText(text);
  }

  public void ShowSectorTextRight(string text)
  {
    CutsceneVideoFx.ShowSectorTextRight(this.videoUiHandler, 0.0f, text);
  }

  public void ShowFadingBsgoLogo()
  {
    CutsceneVideoFx.ShowFadingBsgoLogo(this.videoUiHandler);
  }

  public void SwitchToCameraId(string cameraId)
  {
    CutsceneCamera cutsceneCamera = this.FindCutsceneCamera(cameraId);
    if ((UnityEngine.Object) cutsceneCamera != (UnityEngine.Object) null)
    {
      if (SectorEditorHelper.IsEditorLevel())
        EditorLevel.SwitchCameraTo(cutsceneCamera.Camera);
      if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null)
        SpaceLevel.GetLevel().cameraSwitcher.SetActiveCamera(cutsceneCamera.Camera, true);
    }
    this.ForceLodUpdate();
  }

  private CutsceneCamera FindCutsceneCamera(string cameraId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    CutsceneEvents.\u003CFindCutsceneCamera\u003Ec__AnonStorey5F cameraCAnonStorey5F = new CutsceneEvents.\u003CFindCutsceneCamera\u003Ec__AnonStorey5F();
    // ISSUE: reference to a compiler-generated field
    cameraCAnonStorey5F.cameraId = cameraId;
    // ISSUE: reference to a compiler-generated method
    CutsceneCamera cutsceneCamera = ((IEnumerable<CutsceneCamera>) this.GetComponentsInChildren<CutsceneCamera>()).FirstOrDefault<CutsceneCamera>(new Func<CutsceneCamera, bool>(cameraCAnonStorey5F.\u003C\u003Em__2D));
    if ((UnityEngine.Object) cutsceneCamera == (UnityEngine.Object) null)
    {
      // ISSUE: reference to a compiler-generated field
      Debug.LogError((object) ("FindCutsceneCamera(): Didn't find Cutscene Cam with id " + cameraCAnonStorey5F.cameraId));
    }
    return cutsceneCamera;
  }

  public void DiveIntoGameCam(string parameterString)
  {
    string[] strArray = this.SplitParametersFromString(parameterString);
    if (strArray.Length != 4)
    {
      Debug.LogError((object) ("ZoomInToGameCam(): Wrong amount of parameters (" + (object) strArray.Length + ")"));
    }
    else
    {
      string cutsceneCamId;
      Vector3 vector3;
      float transitionTime;
      bool scaleDistanceWithShipLength;
      try
      {
        cutsceneCamId = strArray[0];
        vector3 = this.StringVectorToVector3(strArray[1]);
        transitionTime = float.Parse(strArray[2]);
        scaleDistanceWithShipLength = bool.Parse(strArray[3]);
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("MotionBlur(): Couldn't parse parameterString " + parameterString + ". Exception: " + ex.Message));
        return;
      }
      this.ExecuteZoomInToGameCam(cutsceneCamId, vector3, transitionTime, scaleDistanceWithShipLength);
    }
  }

  private void ExecuteZoomInToGameCam(string cutsceneCamId, Vector3 initOffset, float transitionTime, bool scaleDistanceWithShipLength)
  {
    Camera camera;
    float num;
    Transform transform1;
    if (SectorEditorHelper.IsEditorLevel())
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(Resources.Load<GameObject>("Debug/DebugCameraSetup/DebugCutscenePlayerShipCameraSetup"));
      camera = gameObject.GetComponentsInChildren<Camera>(true)[0];
      num = 8f;
      transform1 = gameObject.GetComponentsInChildren<CutsceneShipSpawner>(true)[0].transform;
    }
    else
    {
      camera = Camera.main;
      transform1 = SpaceLevel.GetLevel().PlayerActualShip.Model.transform;
      num = SpaceLevel.GetLevel().PlayerActualShip.MeshBoundsRaw.size.z;
    }
    Transform transform2 = camera.transform;
    Vector3 vector3 = transform2.position + camera.transform.rotation * initOffset * (!scaleDistanceWithShipLength ? 1f : num);
    CutsceneCamera cutsceneCamera = this.FindCutsceneCamera(cutsceneCamId);
    cutsceneCamera.transform.position = vector3;
    cutsceneCamera.transform.rotation = Quaternion.LookRotation(transform1.position - vector3, transform1.up);
    LeanTween.move(cutsceneCamera.gameObject, transform2.position, transitionTime).setEase(LeanTweenType.easeOutQuad);
    LeanTween.rotate(cutsceneCamera.gameObject, transform2.rotation.eulerAngles, transitionTime).setEase(LeanTweenType.easeOutQuad);
    cutsceneCamera.GetComponent<Camera>().fieldOfView = camera.fieldOfView;
  }

  private string[] SplitParametersFromString(string parameterString)
  {
    StringBuilder stringBuilder = new StringBuilder(parameterString);
    bool flag = false;
    for (int index = 0; index < stringBuilder.Length; ++index)
    {
      if ((int) stringBuilder[index] == 44 && !flag)
        stringBuilder[index] = '§';
      if ((int) stringBuilder[index] == 40)
      {
        stringBuilder[index] = ' ';
        flag = true;
      }
      if ((int) stringBuilder[index] == 41)
      {
        stringBuilder[index] = ' ';
        flag = false;
      }
    }
    return stringBuilder.ToString().Replace(" ", string.Empty).Split('§');
  }

  private Vector3 StringVectorToVector3(string vectorString)
  {
    vectorString = vectorString.Replace("(", string.Empty).Replace(")", string.Empty);
    string[] strArray = this.SplitParametersFromString(vectorString);
    if (strArray.Length == 3)
      return new Vector3(float.Parse(strArray[0]), float.Parse(strArray[1]), float.Parse(strArray[2]));
    Debug.LogError((object) ("StringVectorToVector3(): Couldn't parse Vector String " + vectorString + " to Vector3."));
    return Vector3.zero;
  }

  private Camera GetActiveCamera()
  {
    return ((IEnumerable<Camera>) this.GetComponentsInChildren<Camera>()).FirstOrDefault<Camera>((Func<Camera, bool>) (childCamera => childCamera.enabled));
  }

  private void ForceLodUpdate()
  {
    foreach (LODScript lodScript in UnityEngine.Object.FindObjectsOfType<LODScript>())
      lodScript.UpdateDistances();
  }

  public void MuteGameMusic()
  {
    FacadeFactory.GetInstance().SendMessage(Message.CutsceneMuteGameMusic, (object) true);
  }
}
