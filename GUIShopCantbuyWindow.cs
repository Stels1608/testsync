﻿// Decompiled with JetBrains decompiler
// Type: GUIShopCantbuyWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GUIShopCantbuyWindow : GUIPanel
{
  private List<ResourceType> missingResources = new List<ResourceType>();
  private const string DEFAULT_TEXT = "%$bgo.inflight_shop.notEnoughMoney%";
  private const string MERITS_TEXT = "%$bgo.inflight_shop.notEnoughMoneyMerits%";
  private const string REPEAT_TEXT = "%$bgo.inflight_shop.no_repeat_item%";
  private const string TYLIUM_TEXT = "%$bgo.inflight_shop.notEnoughTylium%";
  private const string BUY_NOW_TEXT = "%$bgo.deathpopup.buy_now%";
  private const string CUBITS_AND_TYLIUM_TEXT = "%$bgo.inflight_shop.notEnoughTyliumAndCubits%";
  public bool forRepeat;
  private readonly GUILabelNew textLabel;
  private readonly string cubitsText;
  private readonly GUIButtonNew yesButton;
  private readonly GUIButtonNew cancelButton;

  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  public List<ResourceType> MissingResources
  {
    set
    {
      this.missingResources = value;
    }
  }

  public GUIShopCantbuyWindow(SmartRect parent)
    : base(parent)
  {
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/EquipBuyPanel/gui_cantbuy_layout");
    GUIImageNew guiImageNew = (jwindowDescription["background_image"] as JImage).CreateGUIImageNew(this.root);
    this.root.Width = guiImageNew.Rect.width;
    this.root.Height = guiImageNew.Rect.height;
    this.AddPanel((GUIPanel) guiImageNew);
    this.textLabel = (jwindowDescription["text_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.textLabel);
    this.cubitsText = this.textLabel.Text;
    this.yesButton = (jwindowDescription["yes_button"] as JButton).CreateGUIButtonNew(this.root);
    this.AddPanel((GUIPanel) this.yesButton);
    this.cancelButton = (jwindowDescription["cancel_button"] as JButton).CreateGUIButtonNew(this.root);
    this.cancelButton.Handler = new AnonymousDelegate(this.OnCancelButton);
    this.AddPanel((GUIPanel) this.cancelButton);
    this.Position = new float2(0.0f, 0.0f);
    this.yesButton.Handler = new AnonymousDelegate(this.OpenPayment);
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    base.OnMouseDown(mousePosition, mouseKey);
    return this.IsRendered;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    base.OnMouseUp(mousePosition, mouseKey);
    return this.IsRendered;
  }

  public override void Show()
  {
    bool flag = this.missingResources.Contains(ResourceType.Token) || this.forRepeat || this.missingResources.Count == 0 || this.missingResources.Contains(ResourceType.Tylium) && GameLevel.Instance is SpaceLevel;
    this.yesButton.IsRendered = !flag;
    this.yesButton.Handler = new AnonymousDelegate(this.OpenPayment);
    if (this.missingResources.Contains(ResourceType.Token))
      this.textLabel.Text = "%$bgo.inflight_shop.notEnoughMoneyMerits%";
    else if (this.forRepeat)
      this.textLabel.Text = "%$bgo.inflight_shop.no_repeat_item%";
    else if (this.missingResources.Contains(ResourceType.Cubits) && !this.missingResources.Contains(ResourceType.Tylium))
    {
      this.textLabel.Text = this.cubitsText;
      this.yesButton.Text = "%$bgo.deathpopup.buy_now%";
    }
    else if (this.missingResources.Contains(ResourceType.Tylium) && !this.missingResources.Contains(ResourceType.Cubits))
      this.OnTyliumMissing();
    else if (this.missingResources.Contains(ResourceType.Tylium) && this.missingResources.Contains(ResourceType.Cubits))
    {
      this.textLabel.Text = "%$bgo.inflight_shop.notEnoughTyliumAndCubits%";
      this.yesButton.Text = "%$bgo.deathpopup.buy_now%";
    }
    else
      this.textLabel.Text = "%$bgo.inflight_shop.notEnoughMoney%";
    this.cancelButton.Position = new float2(!flag ? 50f : 0.0f, this.cancelButton.Position.y);
    this.cancelButton.Text = !flag ? "%$bgo.EquipBuyPanel.gui_cantbuy_layout.label_0%" : "%$bgo.common.ok%";
    this.IsRendered = true;
    Game.InputDispatcher.Focused = (InputListener) this;
    if (this.Parent != null)
      this.Position = this.Parent.Size * 0.5f;
    else
      this.Position = new float2((float) Screen.width * 0.5f, (float) Screen.height * 0.5f);
    for (int index = 0; index < this.Children.Count; ++index)
      this.Children[index].RecalculateAbsCoords();
  }

  private void OnTyliumMissing()
  {
    this.textLabel.Text = "%$bgo.inflight_shop.notEnoughTylium%";
    this.yesButton.Text = "%$bgo.deathpopup.buy_now%";
    this.yesButton.Handler = new AnonymousDelegate(this.OpenShopResourcesCategory);
  }

  private void OpenPayment()
  {
    ExternalApi.EvalJs("window.createPaymentSession();");
    this.Hide();
  }

  private void OpenShopResourcesCategory()
  {
    FacadeFactory.GetInstance().SendMessage(Message.ShopSwitchCategory, (object) ShopCategory.Resource);
    this.Hide();
  }

  public override void Hide()
  {
    this.IsRendered = false;
    Game.InputDispatcher.Focused = (InputListener) null;
    this.missingResources.Clear();
  }

  private void OnCancelButton()
  {
    this.Hide();
  }
}
