﻿// Decompiled with JetBrains decompiler
// Type: DutyPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Core;
using System.Collections.Generic;
using UnityEngine;

public class DutyPanel : GuiPanel
{
  private GuiPanelVerticalScroll scroll;
  private GuiLabel title;
  private GuiLabel currentTitle;
  private GuiLabel curentBonus;
  private GuiLabel nextTitle;
  private GuiLabel nextBonus;
  private GuiLabel objectives;
  private GuiLabel currentTitleLabel;
  private GuiLabel curentBonusLabel;
  private GuiLabel nextTitleLabel;
  private GuiLabel nextBonusLabel;
  private GuiLabel objectivesLabel;
  private GuiLabel desc;
  private GuiLabel dontHave;
  private int dutiesCount;

  public bool DescGroupRendered
  {
    get
    {
      return !this.dontHave.IsRendered;
    }
    set
    {
      GuiLabel guiLabel = this.title;
      bool flag1 = value;
      this.desc.IsRendered = flag1;
      bool flag2 = flag1;
      this.objectivesLabel.IsRendered = flag2;
      bool flag3 = flag2;
      this.nextBonusLabel.IsRendered = flag3;
      bool flag4 = flag3;
      this.nextTitleLabel.IsRendered = flag4;
      bool flag5 = flag4;
      this.curentBonusLabel.IsRendered = flag5;
      bool flag6 = flag5;
      this.objectives.IsRendered = flag6;
      bool flag7 = flag6;
      this.currentTitleLabel.IsRendered = flag7;
      bool flag8 = flag7;
      this.nextBonus.IsRendered = flag8;
      bool flag9 = flag8;
      this.nextTitle.IsRendered = flag9;
      bool flag10 = flag9;
      this.curentBonus.IsRendered = flag10;
      bool flag11 = flag10;
      this.currentTitle.IsRendered = flag11;
      int num = flag11 ? 1 : 0;
      guiLabel.IsRendered = num != 0;
      this.dontHave.IsRendered = !value;
    }
  }

  public DutyPanel()
  {
    Loaders.Load((GuiPanel) this, "GUI/CharacterStatusWindow/duties/layout.txt");
    GuiImage guiImage = this.Find<GuiImage>("scrollRect");
    this.RemoveChild((GuiElementBase) guiImage);
    this.scroll = new GuiPanelVerticalScroll();
    this.scroll.Position = guiImage.Position;
    this.scroll.Size = guiImage.Size;
    this.scroll.AutoSizeChildren = true;
    this.scroll.RenderDelimiters = true;
    this.AddChild((GuiElementBase) this.scroll);
    this.title = this.Find<GuiLabel>("title");
    this.desc = this.Find<GuiLabel>("desc");
    this.currentTitle = this.Find<GuiLabel>("currentTitleText");
    this.curentBonus = this.Find<GuiLabel>("currentBonusText");
    this.nextTitle = this.Find<GuiLabel>("nextTitleText");
    this.nextBonus = this.Find<GuiLabel>("nextBonusText");
    this.objectives = this.Find<GuiLabel>("objectivesText");
    this.dontHave = this.Find<GuiLabel>("donthave");
    this.currentTitleLabel = this.Find<GuiLabel>("currentTitle");
    this.curentBonusLabel = this.Find<GuiLabel>("currentBonus");
    this.nextTitleLabel = this.Find<GuiLabel>("nextTitle");
    this.nextBonusLabel = this.Find<GuiLabel>("nextBonus");
    this.objectivesLabel = this.Find<GuiLabel>("objectives");
    this.AddChild((GuiElementBase) new Gui.Timer(0.25f));
    this.updateScroll();
  }

  private void updateDesc(Duty duty)
  {
    this.DescGroupRendered = true;
    this.title.Text = duty.GUICard.Name;
    this.desc.Text = duty.GUICard.Description;
    GuiLabel guiLabel1 = this.currentTitleLabel;
    bool flag1 = false;
    this.currentTitle.IsRendered = flag1;
    bool flag2 = flag1;
    this.curentBonus.IsRendered = flag2;
    bool flag3 = flag2;
    this.curentBonusLabel.IsRendered = flag3;
    int num1 = flag3 ? 1 : 0;
    guiLabel1.IsRendered = num1 != 0;
    if ((int) duty.Card.Level != 0)
    {
      GuiLabel guiLabel2 = this.curentBonus;
      bool flag4 = true;
      this.curentBonusLabel.IsRendered = flag4;
      bool flag5 = flag4;
      this.currentTitleLabel.IsRendered = flag5;
      bool flag6 = flag5;
      this.currentTitle.IsRendered = flag6;
      int num2 = flag6 ? 1 : 0;
      guiLabel2.IsRendered = num2 != 0;
      this.Find<GuiLabel>("currentTitle").IsRendered = true;
      this.currentTitle.Text = duty.Card.TitleCard.Name;
      string[] strArray = this.ReadBonus(duty.Card.TitleCard);
      this.curentBonus.Text = strArray.Length != 0 ? strArray[0] : string.Empty;
    }
    GuiLabel guiLabel3 = this.nextTitleLabel;
    bool flag7 = false;
    this.nextTitle.IsRendered = flag7;
    bool flag8 = flag7;
    this.nextBonus.IsRendered = flag8;
    bool flag9 = flag8;
    this.nextBonusLabel.IsRendered = flag9;
    int num3 = flag9 ? 1 : 0;
    guiLabel3.IsRendered = num3 != 0;
    if (duty.Card.NextCard != null)
    {
      GuiLabel guiLabel2 = this.nextTitleLabel;
      bool flag4 = true;
      this.nextTitle.IsRendered = flag4;
      bool flag5 = flag4;
      this.nextBonus.IsRendered = flag5;
      bool flag6 = flag5;
      this.nextBonusLabel.IsRendered = flag6;
      int num2 = flag6 ? 1 : 0;
      guiLabel2.IsRendered = num2 != 0;
      this.nextTitle.Text = duty.Card.NextCard.TitleCard.Name;
      string[] strArray = this.ReadBonus(duty.Card.NextCard.TitleCard);
      this.nextBonus.Text = strArray.Length != 0 ? strArray[0] : string.Empty;
    }
    this.objectives.Text = string.Format("{0} {1}/{2}", (object) duty.Card.Counter, (object) duty.CounterValue, (object) duty.NextCounterValue);
  }

  private string[] ReadBonus(TitleCard titleCard)
  {
    List<string> stringList = new List<string>();
    using (List<Tuple<string, float>>.Enumerator enumerator = titleCard.MultiplyBuff.ToPrintableList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Tuple<string, float> current = enumerator.Current;
        stringList.Add(current.First + " " + (object) current.Second);
      }
    }
    using (List<Tuple<string, float>>.Enumerator enumerator = titleCard.StaticBuff.ToPrintableList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Tuple<string, float> current = enumerator.Current;
        stringList.Add(current.First + " " + (object) current.Second);
      }
    }
    return stringList.ToArray();
  }

  private void updateScroll()
  {
    this.scroll.EmptyChildren();
    this.dutiesCount = Game.Me.DutyBook.Count;
    this.dontHave.Text = this.dutiesCount != 0 ? "%$bgo.inflight_shop.no_duty_selected%" : "%$bgo.inflight_shop.no_duties%";
    this.DescGroupRendered = false;
    for (int dutyIndex = 0; dutyIndex < this.dutiesCount; ++dutyIndex)
      this.scroll.AddChild((GuiElementBase) new DutyPanel.DutiesItem(dutyIndex, new System.Action<Duty>(this.updateDesc)));
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    if (this.dutiesCount == Game.Me.DutyBook.Count)
      return;
    this.updateScroll();
  }

  public class DutiesItem : GuiPanel, ISelectable
  {
    private GuiButton setActive;
    private GuiLabel title;
    private int dutyIndex;
    public System.Action<Duty> updateDesc;
    private DutyState cachedDutyState;

    public DutiesItem(int dutyIndex, System.Action<Duty> updateDescription)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      DutyPanel.DutiesItem.\u003CDutiesItem\u003Ec__AnonStorey77 itemCAnonStorey77 = new DutyPanel.DutiesItem.\u003CDutiesItem\u003Ec__AnonStorey77();
      // ISSUE: explicit constructor call
      base.\u002Ector();
      // ISSUE: reference to a compiler-generated field
      itemCAnonStorey77.duty = Game.Me.DutyBook[dutyIndex];
      this.dutyIndex = dutyIndex;
      this.SizeY = 43f;
      this.updateDesc = updateDescription;
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      this.title = new GuiLabel(itemCAnonStorey77.duty.GUICard.Name + " %$bgo.inflight_shop.level_l% " + (object) itemCAnonStorey77.duty.Card.Level);
      this.title.WordWrap = true;
      this.title.Align = Align.MiddleLeft;
      this.title.PositionX += 20f;
      this.title.Font = Gui.Options.FontBGM_BT;
      this.title.FontSize = 14;
      this.AddChild((GuiElementBase) this.title);
      this.setActive = new GuiButton("%$bgo.inflight_shop.activate%");
      // ISSUE: reference to a compiler-generated field
      this.setActive.IsRendered = itemCAnonStorey77.duty.Status == DutyState.Available;
      this.setActive.SizeX = 140f;
      // ISSUE: reference to a compiler-generated method
      this.setActive.Pressed = new AnonymousDelegate(itemCAnonStorey77.\u003C\u003Em__71);
      this.AddChild((GuiElementBase) this.setActive, Align.MiddleRight);
    }

    public override void Reposition()
    {
      this.title.SizeX = (float) ((double) this.SizeX - (double) this.setActive.SizeX - 22.0);
      base.Reposition();
    }

    public void OnSelected()
    {
      if (this.updateDesc == null)
        return;
      this.updateDesc(Game.Me.DutyBook[this.dutyIndex]);
    }

    public void OnDeselected()
    {
    }

    public override bool MouseUp(float2 position, KeyCode key)
    {
      this.FindParent<ISelectableContainer>().Selected = (ISelectable) this;
      return base.MouseUp(position, key);
    }

    private void SmartButton(Duty duty)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      DutyPanel.DutiesItem.\u003CSmartButton\u003Ec__AnonStorey78 buttonCAnonStorey78 = new DutyPanel.DutiesItem.\u003CSmartButton\u003Ec__AnonStorey78();
      // ISSUE: reference to a compiler-generated field
      buttonCAnonStorey78.duty = duty;
      // ISSUE: reference to a compiler-generated field
      if (this.cachedDutyState == buttonCAnonStorey78.duty.Status)
        return;
      // ISSUE: reference to a compiler-generated field
      if (buttonCAnonStorey78.duty.Status == DutyState.Researching)
      {
        this.setActive.IsRendered = false;
      }
      else
      {
        this.setActive.IsRendered = true;
        // ISSUE: reference to a compiler-generated field
        if (buttonCAnonStorey78.duty.Status == DutyState.Active)
        {
          this.setActive.Text = "%$bgo.inflight_shop.deactivate%";
          this.setActive.Pressed = new AnonymousDelegate(PlayerProtocol.GetProtocol().DeselectTitle);
        }
        else
        {
          this.setActive.Text = "%$bgo.inflight_shop.activate%";
          // ISSUE: reference to a compiler-generated method
          this.setActive.Pressed = new AnonymousDelegate(buttonCAnonStorey78.\u003C\u003Em__72);
        }
        // ISSUE: reference to a compiler-generated field
        this.cachedDutyState = buttonCAnonStorey78.duty.Status;
      }
    }

    public override void PeriodicUpdate()
    {
      Duty duty1 = Game.Me.DutyBook[this.dutyIndex];
      base.PeriodicUpdate();
      Duty duty2 = Game.Me.DutyBook[this.dutyIndex];
      this.title.Text = duty2.GUICard.Name + " %$bgo.inflight_shop.level_l% " + (object) duty2.Card.Level;
      this.SmartButton(duty2);
    }
  }
}
