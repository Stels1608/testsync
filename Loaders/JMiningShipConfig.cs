﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JMiningShipConfig
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Loaders
{
  public class JMiningShipConfig
  {
    [JsonField(JsonName = "extractPerSecond")]
    public int extractPerSecond = 10;
    [JsonField(JsonName = "price")]
    public int price = 100;
    [JsonField(JsonName = "spawnMob")]
    public bool spawnMob = true;
    [JsonField(JsonName = "areaOfLoot")]
    public float areaOfLoot = -1f;
  }
}
