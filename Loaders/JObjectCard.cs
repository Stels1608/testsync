﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JObjectCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using CouchDB;
using System.Text;

namespace Loaders
{
  public class JObjectCard : CouchDocumentBase
  {
    [JsonField(JsonName = "Category")]
    public readonly string category = string.Empty;
    [JsonField(JsonName = "Description")]
    public string description = string.Empty;
    [JsonField(JsonName = "PrefabName")]
    public string prefabName = string.Empty;
    [JsonField(JsonName = "Faction")]
    public string faction = string.Empty;
    [JsonField(JsonName = "Ship")]
    public string ship = string.Empty;
    [JsonField(JsonName = "Name", SkipNull = true)]
    public string name;
    [JsonField(JsonName = "Levels")]
    public int levels;
    [JsonField(JsonName = "Tier")]
    public int tier;
    [JsonField(JsonName = "MaxCombatDistance")]
    public int maxCombatDistance;
    [JsonField(JsonName = "MinCombatDistance")]
    public int minCombatDistance;
    [JsonField(JsonName = "SeparationDistance")]
    public int separationDistance;

    public virtual string GetComment()
    {
      if (!string.IsNullOrEmpty(this.name))
        return this.name;
      if (!string.IsNullOrEmpty(this.prefabName))
        return this.prefabName;
      return this.Id;
    }

    public virtual string GetTooltip()
    {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.Append("Name: " + this.GetComment() + "\n");
      if (!string.IsNullOrEmpty(this.category))
        stringBuilder.Append("Category: " + this.category + "\n");
      if (!string.IsNullOrEmpty(this.faction))
        stringBuilder.Append("Faction: " + this.faction + "\n");
      if (!string.IsNullOrEmpty(this.ship))
        stringBuilder.Append("Ship: " + this.ship + "\n");
      if (this.levels > 0)
        stringBuilder.Append("Levels: " + (object) this.levels + "\n");
      if (this.tier > 0)
        stringBuilder.Append("Tier: " + (object) this.tier + "\n");
      if (this.separationDistance > 0)
        stringBuilder.Append("Separation Distance: " + (object) this.separationDistance + "\n");
      if (this.maxCombatDistance > 0)
        stringBuilder.Append("Max Combat Distance: " + (object) this.maxCombatDistance + "\n");
      if (this.minCombatDistance > 0)
        stringBuilder.Append("Min Combat Distance: " + (object) this.minCombatDistance + "\n");
      if (!string.IsNullOrEmpty(this.description))
        stringBuilder.Append("Description: " + this.description + "\n");
      return stringBuilder.ToString();
    }
  }
}
