﻿// Decompiled with JetBrains decompiler
// Type: Loaders.ProgressionPoint
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Loaders
{
  public class ProgressionPoint : IJsonEvent
  {
    [JsonField(JsonName = "Outpost")]
    public ProgressionPointEntity outpost;
    [JsonField(JsonName = "Beacon")]
    public ProgressionPointEntity beacon;
    [JsonField(JsonName = "PlatformOne")]
    public ProgressionPointEntity platformOne;
    [JsonField(JsonName = "PlatformTwo")]
    public ProgressionPointEntity platformTwo;
    [JsonField(JsonName = "PlatformThree")]
    public ProgressionPointEntity platformThree;
    [JsonField(JsonName = "PlatformFour")]
    public ProgressionPointEntity platformFour;

    public void AfterSerialize()
    {
      if (this.outpost != null)
        this.outpost.gameObject.name = "Outpost";
      if (this.beacon != null)
        this.beacon.gameObject.name = "Beacon";
      if (this.platformOne != null)
        this.platformOne.gameObject.name = "PlatformOne";
      if (this.platformTwo != null)
        this.platformTwo.gameObject.name = "PlatformTwo";
      if (this.platformThree != null)
        this.platformThree.gameObject.name = "PlatformThree";
      if (this.platformThree == null)
        return;
      this.platformFour.gameObject.name = "PlatformFour";
    }

    public void Clear()
    {
      this.outpost = (ProgressionPointEntity) null;
      this.beacon = (ProgressionPointEntity) null;
      this.platformOne = (ProgressionPointEntity) null;
      this.platformTwo = (ProgressionPointEntity) null;
      this.platformThree = (ProgressionPointEntity) null;
      this.platformFour = (ProgressionPointEntity) null;
    }
  }
}
