﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JStory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Loaders
{
  public class JStory
  {
    [JsonField(JsonName = "name")]
    public string name = "Story";
    [JsonField(JsonName = "action")]
    public string action = string.Empty;
    [JsonField(JsonName = "_description", SkipNull = true)]
    public string description = string.Empty;
    [JsonField(JsonName = "condition")]
    public string condition = string.Empty;
    [JsonField(JsonName = "active")]
    public bool active;
    [JsonField(JsonName = "_deactivated")]
    public bool deactivated;

    public JStory Copy()
    {
      return new JStory() { name = this.name, action = this.action, condition = this.condition, active = this.active };
    }

    public override string ToString()
    {
      return string.Format("Name: {0}, Action: {1}, Condition: {2}, Active: {3}", (object) this.name, (object) this.action, (object) this.condition, (object) this.active);
    }
  }
}
