﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JSectorEvents
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Loaders
{
  public class JSectorEvents : IJsonEvent
  {
    [JsonField(JsonName = "event_activation")]
    public List<string> eventActivation = new List<string>();
    [JsonField(JsonName = "event")]
    public string eventId;
    [JsonField(JsonName = "level")]
    public int level;
    [JsonField(JsonName = "faction")]
    public string faction;
    [JsonField(JsonName = "origin")]
    public JSectorEvents.Origin pos;
    [JsonField(JsonName = "interval")]
    public int interval;
    [JsonField(JsonName = "variance")]
    public int variance;
    [JsonField(JsonName = "chance")]
    public float chance;

    public JSectorEvents()
    {
    }

    public JSectorEvents(string eventname)
    {
    }

    public void Init(SectorEventObjects events)
    {
      this.eventId = events.eventId;
      this.chance = events.chance;
      this.faction = events.faction;
      this.interval = events.interval;
      this.level = events.level;
      this.pos = new JSectorEvents.Origin();
      this.pos.position = events.gameObject.transform.position;
      this.variance = events.variance;
      this.eventActivation = events.eventActivation;
    }

    public override string ToString()
    {
      return string.Format("EventId: {0}, Level: {1}, Faction: {2}, Origin: {3}, Interval: {4}, Variance: {5}, Chance: {6}", (object) this.eventId, (object) this.level, (object) this.faction, (object) this.pos.position.ToString(), (object) this.interval, (object) this.variance, (object) this.chance);
    }

    public void AfterSerialize()
    {
    }

    public class Origin
    {
      [JsonField(JsonName = "position")]
      public Vector3 position;
    }
  }
}
