﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JGalaxyMapDescription
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUI;
using CouchDB;
using System.Collections.Generic;

namespace Loaders
{
  public class JGalaxyMapDescription : CouchDocumentBase
  {
    [JsonField(JsonName = "Stars")]
    private List<JGalaxyMapDescription.JSector> Stars = new List<JGalaxyMapDescription.JSector>();

    public List<JGalaxyMapDescription.JSector> GetSectors()
    {
      return this.Stars;
    }

    public JGalaxyMapDescription.JSector AddNewSector()
    {
      JGalaxyMapDescription.JSector jsector = new JGalaxyMapDescription.JSector();
      jsector.SectorId = this.GetNextId();
      this.GetSectors().Add(jsector);
      return jsector;
    }

    private uint GetNextId()
    {
      uint id = 0;
      while (this.TryIdExists(id))
        ++id;
      return id;
    }

    private bool TryIdExists(uint id)
    {
      using (List<JGalaxyMapDescription.JSector>.Enumerator enumerator = this.Stars.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if ((int) enumerator.Current.SectorId == (int) id)
            return true;
        }
      }
      return false;
    }

    public void Remove(JGalaxyMapDescription.JSector sector)
    {
      this.Stars.Remove(sector);
    }

    public class JSector
    {
      [JsonField(JsonName = "key")]
      public string SectorKey = string.Empty;
      [JsonField(JsonName = "position")]
      public float2 Position = new float2(0.0f, 0.0f);
      [JsonField(JsonName = "id")]
      public uint sectorId;
      [JsonField(JsonName = "type")]
      public int textureIndex;
      [JsonField(deserialize = false, serialize = false)]
      public Image texture;

      public uint SectorId
      {
        get
        {
          return this.sectorId;
        }
        set
        {
          this.sectorId = value;
          this.SectorKey = "sector" + (object) this.sectorId;
        }
      }

      public class RaceChar<T>
      {
        public T colonial = default (T);
        public T cylon = default (T);
      }
    }
  }
}
