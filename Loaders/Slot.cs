﻿// Decompiled with JetBrains decompiler
// Type: Loaders.Slot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUI;

namespace Loaders
{
  public class Slot
  {
    public int level = 1;
    public string spot_id = string.Empty;
    public int place_id;
    public ShipSlotType system_type;
    [JsonField(JsonName = "slot_layout_big_paperdoll")]
    public SlotLayoutBigPaperdoll slotLayoutBigPaperdoll;
    [JsonField(deserialize = false, serialize = false)]
    public SlotGui image;

    public Slot()
    {
    }

    public Slot(Slot slot)
    {
      this.place_id = slot.place_id;
      this.system_type = slot.system_type;
      this.slotLayoutBigPaperdoll = slot.slotLayoutBigPaperdoll;
      this.spot_id = slot.spot_id;
    }
  }
}
