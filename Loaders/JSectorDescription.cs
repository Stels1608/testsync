﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JSectorDescription
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor;
using CouchDB;
using System.Collections.Generic;
using UnityEngine;

namespace Loaders
{
  public class JSectorDescription : CouchDocumentBase
  {
    public JMiningProperties MiningProperties = new JMiningProperties();
    public List<Asteroid> Asteroids = new List<Asteroid>();
    public List<JPlanetoid> Planetoids = new List<JPlanetoid>();
    public List<JDecorationDescription> Decorations = new List<JDecorationDescription>();
    public List<AsteroidBelt> Belts = new List<AsteroidBelt>();
    public List<SunDesc> Suns = new List<SunDesc>();
    public Color AmbientColor = new Color(0.2745098f, 0.5686275f, 0.454902f);
    public List<JLightDescription> Lights = new List<JLightDescription>();
    public List<PlanetReal> PlanetsReal = new List<PlanetReal>();
    public BackgroundDesc Nebula = new BackgroundDesc("nebula1");
    public BackgroundDesc Stars = new BackgroundDesc("stars");
    public BackgroundDesc StarsMultiply = new BackgroundDesc("starsmultiply_mid");
    public BackgroundDesc StarsVariance = new BackgroundDesc("starsvariances");
    public List<MovingNebulaDesc> MovingNebulas = new List<MovingNebulaDesc>();
    public JStarfog Starfog = new JStarfog();
    public JStardust Stardust = new JStardust();
    public JGlobalFog GlobalFog = new JGlobalFog();
    [JsonField(JsonName = "Size")]
    public Vector3 Size = new Vector3(50000f, 1000f, 50000f);
    [JsonField(JsonName = "LootRespawn")]
    public RespawnProperty Respawn = new RespawnProperty();
    [JsonField(JsonName = "StaticAreaMobs")]
    public List<MobRespawnPoint> StaticMobRespawn = new List<MobRespawnPoint>();
    [JsonField(JsonName = "RandomAreaMobs")]
    public List<MobRespawnPoint> RandomMobRespawn = new List<MobRespawnPoint>();
    [JsonField(JsonName = "StartingPoints")]
    public JStartingPoints StartingPoints = new JStartingPoints();
    [JsonField(JsonName = "Cruisers")]
    public List<CruiserSP> CruiserSPs = new List<CruiserSP>();
    [JsonField(JsonName = "PositionTriggers")]
    public List<TriggerPoint> TriggerPoints = new List<TriggerPoint>();
    [JsonField(JsonName = "DotAreas")]
    public List<DotArea> DotAreas = new List<DotArea>();
    [JsonField(JsonName = "ConvoyMobs")]
    public List<ConvoyGo> ConvoyPoints = new List<ConvoyGo>();
    [JsonField(JsonName = "WayPoints")]
    public List<WayPointPath> WayPoints = new List<WayPointPath>();
    [JsonField(JsonName = "EventObjects")]
    public List<JEventObject> EventObjects = new List<JEventObject>();
    [JsonField(JsonName = "CylonProgression")]
    public string cylonProgression = "sector_progression_default_cylon";
    [JsonField(JsonName = "CylonProgressionPoints")]
    public ProgressionPoint cylonProgressionPoints = new ProgressionPoint();
    [JsonField(JsonName = "ColonialProgression")]
    public string colonialProgression = "sector_progression_default_colonial";
    [JsonField(JsonName = "ColonialProgressionPoints")]
    public ProgressionPoint colonialProgressionPoints = new ProgressionPoint();
    [JsonField(JsonName = "ThreatLevel")]
    public JSectorDescription.RaceChar<int> Threatlevel = new JSectorDescription.RaceChar<int>();
    [JsonField(JsonName = "Faction")]
    public string faction = "colonial";
    [JsonField(JsonName = "Name")]
    public string Name = "New Sector";
    [JsonField(JsonName = "Regulation")]
    public string regulation = "sector_regulation_global";
    [JsonField(JsonName = "RequiredAssets")]
    public List<string> RequiredAssets = new List<string>();
    [JsonField(JsonName = "MaxCometCount")]
    public int MaxCometCount;
    [JsonField(JsonName = "Events")]
    public List<JSectorEvents> sectorEvents;

    public JSectorDescription()
    {
      this._category = "sector";
    }

    public class RaceChar<T>
    {
      public T colonial = default (T);
      public T cylon = default (T);
    }
  }
}
