﻿// Decompiled with JetBrains decompiler
// Type: Loaders.StoryMissionLoaders
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Json;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Loaders
{
  public class StoryMissionLoaders
  {
    public static JDescription Load(string name)
    {
      if (!ContentDB.DocumentExists(name))
      {
        Debug.Log((object) ("Document " + name + " doesn't exist."));
        return (JDescription) null;
      }
      try
      {
        JDescription document = ContentDB.GetDocument<JDescription>(name);
        Debug.Log((object) ("Document " + name + " loaded"));
        return document;
      }
      catch (Exception ex)
      {
        Debug.Log((object) ("Exception happenned: " + ex.Message));
        return (JDescription) null;
      }
    }

    public static void Save(JDescription desc, string name)
    {
      string path = ContentDB.ContentDBPath + "editor/story/";
      desc._category = "story_editor_meta";
      if (ContentDB.DocumentExists(name + "_editor"))
        ContentDB.UpdateDocument(name + "_editor", "/", (object) desc);
      else
        ContentDB.CreateDocument<JDescription>(desc, name + "_editor", path);
      Debug.Log((object) ("Document " + name + " saved"));
      desc._category = "story";
      if (ContentDB.DocumentExists(name))
        ContentDB.UpdateDocument(name, "/", (object) desc);
      else
        ContentDB.CreateDocument<JDescription>(desc, name);
      JsonData documentRaw = ContentDB.GetDocumentRaw(name);
      List<JsonData> array = documentRaw.Object["Triggers"].Array;
      List<JsonData> jsonDataList = new List<JsonData>();
      using (List<JsonData>.Enumerator enumerator = array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JsonData current = enumerator.Current;
          if (current.Object["_deactivated"].Boolean)
            jsonDataList.Add(current);
          current.Object.Remove("_deactivated");
        }
      }
      using (List<JsonData>.Enumerator enumerator = jsonDataList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JsonData current = enumerator.Current;
          array.Remove(current);
        }
      }
      ContentDB.UpdateDocumentRaw(name, documentRaw);
      Debug.Log((object) ("Document " + name + " saved"));
    }
  }
}
