﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JStoryMissionEditorDescription
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor;
using CouchDB;
using System.Collections.Generic;

namespace Loaders
{
  public class JStoryMissionEditorDescription : CouchDocumentBase
  {
    [JsonField(JsonName = "PositionTriggers")]
    public List<TriggerPointStoryMissionEditor> TriggerPoints = new List<TriggerPointStoryMissionEditor>();
    [JsonField(JsonName = "WayPoints")]
    public List<WayPointStoryMissionEditor> WayPoints = new List<WayPointStoryMissionEditor>();
  }
}
