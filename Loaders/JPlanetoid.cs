﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JPlanetoid
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Loaders
{
  public class JPlanetoid
  {
    public Vector3 position = Vector3.zero;
    public Quaternion rotation = Quaternion.identity;
    public float radius = 10f;
    [JsonField(JsonName = "key")]
    public string key = string.Empty;
  }
}
