﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JMobCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Text;

namespace Loaders
{
  public class JMobCard : JObjectCard
  {
    [JsonField(JsonName = "ThreatLevel")]
    public Dictionary<string, int> threadLevel = new Dictionary<string, int>();
    [JsonField(JsonName = "ThreadLevels", SkipNull = true)]
    public Dictionary<int, List<int>> threadLevels = new Dictionary<int, List<int>>();
    [JsonField(JsonName = "Ships")]
    public Dictionary<string, List<Dictionary<string, string>>> ships;
    [JsonField(JsonName = "AI")]
    public string AI;
    [JsonField(JsonName = "Levels")]
    public int Levels;

    public override string ToString()
    {
      StringBuilder stringBuilder = new StringBuilder();
      using (Dictionary<string, List<Dictionary<string, string>>>.Enumerator enumerator1 = this.ships.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          KeyValuePair<string, List<Dictionary<string, string>>> current1 = enumerator1.Current;
          stringBuilder.AppendLine(current1.Key);
          using (List<Dictionary<string, string>>.Enumerator enumerator2 = current1.Value.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              using (Dictionary<string, string>.Enumerator enumerator3 = enumerator2.Current.GetEnumerator())
              {
                while (enumerator3.MoveNext())
                {
                  KeyValuePair<string, string> current2 = enumerator3.Current;
                  stringBuilder.AppendLine("\t{" + current2.Key + " | " + current2.Value + "}");
                }
              }
            }
          }
        }
      }
      string str1 = string.Empty;
      using (Dictionary<string, int>.Enumerator enumerator = this.threadLevel.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<string, int> current = enumerator.Current;
          str1 = str1 + "[" + current.Key + " | " + (object) current.Value + "]\n";
        }
      }
      string str2 = string.Empty;
      using (Dictionary<int, List<int>>.Enumerator enumerator1 = this.threadLevels.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          KeyValuePair<int, List<int>> current1 = enumerator1.Current;
          str2 = str2 + "\n[" + (object) current1.Key + "]{";
          using (List<int>.Enumerator enumerator2 = current1.Value.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              int current2 = enumerator2.Current;
              str2 = str2 + (object) current2 + ",";
            }
          }
          str2 += "}\n";
        }
      }
      return string.Format("[JMobCard] Ai: {0},\n Levels: {1},\n ThreadLevels: {2},\n ThreadLevel: {3},\n Ships: {4}\n", (object) this.AI, (object) this.Levels, (object) str2, (object) str1, (object) stringBuilder.ToString());
    }
  }
}
