﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JAsteroidCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using CouchDB;

namespace Loaders
{
  public class JAsteroidCard : CouchDocumentBase
  {
    [JsonField(JsonName = "Category")]
    public readonly string category = "asteroid";
    [JsonField(JsonName = "PrefabName")]
    public string prefabName = string.Empty;
    [JsonField(JsonName = "LodCount")]
    public int lodCount = 3;

    public WorldCard FormWorldCard()
    {
      WorldCard worldCard = new WorldCard(0U);
      worldCard.PrefabName = this.prefabName;
      worldCard.LODCount = this.lodCount;
      worldCard.IsLoaded.Set();
      return worldCard;
    }
  }
}
