﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JEnemyCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;

namespace Loaders
{
  public class JEnemyCard : JObjectCard
  {
    [JsonField(JsonName = "Name")]
    public Dictionary<string, string> names;
    [JsonField(JsonName = "Experience", serialize = false)]
    public int experience;
    [JsonField(JsonName = "Experience")]
    public Dictionary<string, int> experiences;
    [JsonField(JsonName = "Loot", serialize = false)]
    public string lootSingle;
    [JsonField(JsonName = "Loot")]
    public Dictionary<string, string> lootMulti;
    [JsonField(JsonName = "Equipment")]
    public Dictionary<string, List<Dictionary<string, string>>> equipment;
    [JsonField(JsonName = "Stats")]
    public Dictionary<string, Dictionary<string, string>> stats;
    [JsonField(JsonName = "ThreatLevel", serialize = false)]
    public int threatLevel;
    [JsonField(JsonName = "ThreatLevel")]
    public Dictionary<string, int> threatLevels;

    public override string ToString()
    {
      string string1 = Tools.IEnumToString(this.lootMulti);
      string string2 = Tools.IEnumToString(this.stats);
      string string3 = Tools.IEnumToString(this.equipment);
      return string.Format("Names: {0}, Experience: {1}, Experiences: {2}, LootSingle: {3}, LootMulti: {4}, Equipment: {5}, Stats: {6}, ThreatLevel: {7}, ThreatLevels: {8}", (object) (this.names == null ? this.name : Tools.IEnumToString(this.names)), (object) this.experience, (object) Tools.IEnumToString(this.experiences), (object) this.lootSingle, (object) string1, (object) string3, (object) string2, (object) this.threatLevel, (object) this.threatLevels);
    }
  }
}
