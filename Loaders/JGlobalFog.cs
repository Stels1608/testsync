﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JGlobalFog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Loaders
{
  public class JGlobalFog : IProtocolRead
  {
    [JsonField(JsonName = "color")]
    public Color Color = new Color(0.427f, 0.534f, 0.538f, 0.314f);
    [JsonField(JsonName = "density")]
    public float Density = 0.0015f;
    [JsonField(JsonName = "enabled")]
    public bool Enabled;
    [JsonField(JsonName = "startDistance")]
    public float StartDistance;

    public void Apply()
    {
      Camera camera = Camera.main ?? GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Camera>(true)[0];
      GlobalFog globalFog = camera.GetComponent<GlobalFog>();
      if ((Object) globalFog == (Object) null)
        globalFog = camera.gameObject.AddComponent<GlobalFog>();
      globalFog.enabled = this.Enabled;
      globalFog.fogShader = Shader.Find("Hidden/GlobalFog");
      globalFog.globalFogColor = this.Color;
      globalFog.globalDensity = this.Density;
      globalFog.fogMode = GlobalFog.FogMode.Distance;
      globalFog.startDistance = this.StartDistance;
      this.DisableConflictingEffects();
    }

    private void DisableConflictingEffects()
    {
      if (!this.Enabled || SectorEditorHelper.IsEditorLevel())
        return;
      this.Enabled = false;
      FacadeFactory.GetInstance().SendMessage(Message.SettingChangedGlowEffect, (object) false);
      this.Enabled = true;
    }

    public void Read(BgoProtocolReader r)
    {
      this.Enabled = r.ReadBoolean();
      this.Color = r.ReadColor();
      this.Density = r.ReadSingle();
      this.StartDistance = r.ReadSingle();
    }
  }
}
