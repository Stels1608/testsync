﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JLightDescription
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Loaders
{
  public class JLightDescription
  {
    public string name = string.Empty;
    public Vector3 position = new Vector3();
    public Quaternion rotation = new Quaternion();
    public Vector3 scale = new Vector3();
    public Color color = Color.white;
    public float intensity = 1.2f;
  }
}
