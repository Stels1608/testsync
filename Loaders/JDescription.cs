﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JDescription
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Loaders
{
  public class JDescription
  {
    [JsonField(JsonName = "Category")]
    public string _category = "story";
    [JsonField(JsonName = "Description")]
    public string description = "Description";
    [JsonField(JsonName = "Name")]
    public string name = "Name";
    [JsonField(JsonName = "Receiver")]
    public string receiver = "undefined";
    [JsonField(JsonName = "TemplateShip")]
    public string shipTemplate = "undefined";
    [JsonField(JsonName = "Triggers")]
    public List<JStory> Stories = new List<JStory>();
    [JsonField(JsonName = "RewardBronze")]
    public Dictionary<string, int> rewardBronze = new Dictionary<string, int>();
    [JsonField(JsonName = "RewardSilver")]
    public Dictionary<string, int> rewardSilver = new Dictionary<string, int>();
    [JsonField(JsonName = "RewardGold")]
    public Dictionary<string, int> rewardGold = new Dictionary<string, int>();
    [JsonField(JsonName = "RewardPlatinum")]
    public Dictionary<string, int> rewardPlatinum = new Dictionary<string, int>();
    [JsonField(JsonName = "Sector")]
    public int sector;
    [JsonField(JsonName = "LockPlayerInDialog")]
    public bool lockPlayerInDialog;
    [JsonField(JsonName = "SpecialShip")]
    public bool useSpecialShip;
    [JsonField(JsonName = "GearScore")]
    public bool useGearScore;
    [JsonField(JsonName = "Statistics")]
    public bool useStatistics;
    [JsonField(JsonName = "MinPointsSilver")]
    public int minPointsSilver;
    [JsonField(JsonName = "MinPointsGold")]
    public int minPointsGold;
    [JsonField(JsonName = "MinPointsPlatinum")]
    public int minPointsPlatinum;

    public override string ToString()
    {
      return string.Format("Category: {0}\n Description: {1}\n Name: {2}\n Sector: {3}\n Receiver: {4}\n ShipTemplate: {5}\n LockPlayerInDialog: {6}\n UseSpecialShip: {7}\n UseGearScore: {8}\n UseStatistics: {9}", (object) this._category, (object) this.description, (object) this.name, (object) this.sector, (object) this.receiver, (object) this.shipTemplate, (object) this.lockPlayerInDialog, (object) this.useSpecialShip, (object) this.useGearScore, (object) this.useStatistics);
    }
  }
}
