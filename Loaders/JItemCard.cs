﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JItemCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Text;

namespace Loaders
{
  public class JItemCard : JObjectCard
  {
    [JsonField(JsonName = "Object")]
    public string objectName;
    [JsonField(JsonName = "NameCylon")]
    public string nameCylon;
    [JsonField(JsonName = "ShortDescription")]
    public string shortDescr;
    [JsonField(JsonName = "Type")]
    public string type;
    [JsonField(JsonName = "Sorting")]
    public string sorting;
    [JsonField(JsonName = "SortWeight")]
    public int sortweight;
    [JsonField(JsonName = "Price")]
    public Dictionary<string, int> price;
    [JsonField(JsonName = "Size")]
    public int size;
    [JsonField(JsonName = "Class")]
    public string className;

    public override string GetComment()
    {
      if (!string.IsNullOrEmpty(this.nameCylon))
        return this.nameCylon;
      return base.GetComment();
    }

    public override string GetTooltip()
    {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.Append(base.GetTooltip());
      if (!string.IsNullOrEmpty(this.shortDescr))
        stringBuilder.Append("Short Description: " + this.shortDescr + "\n");
      if (!string.IsNullOrEmpty(this.objectName))
        stringBuilder.Append("Object: " + this.objectName + "\n");
      if (!string.IsNullOrEmpty(this.type))
        stringBuilder.Append("Type: " + this.type + "\n");
      if (!string.IsNullOrEmpty(this.sorting))
        stringBuilder.Append("Sorting: " + this.sorting + "\n");
      if (this.sortweight > 0)
        stringBuilder.Append("Weight: " + (object) this.sortweight + "\n");
      if (this.size > 0)
        stringBuilder.Append("Size: " + (object) this.size + "\n");
      if (!string.IsNullOrEmpty(this.className))
        stringBuilder.Append("Class: " + this.className + "\n");
      if (this.price != null && this.price.Count > 0)
      {
        stringBuilder.Append("\nPrice:\n");
        using (Dictionary<string, int>.KeyCollection.Enumerator enumerator = this.price.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            stringBuilder.Append(" " + current + ": " + this.price[current].ToString() + "\n");
          }
        }
        stringBuilder.Append("\n");
      }
      return stringBuilder.ToString();
    }
  }
}
