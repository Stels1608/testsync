﻿// Decompiled with JetBrains decompiler
// Type: Loaders.JStartingPoints
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor;
using System.Collections.Generic;

namespace Loaders
{
  public class JStartingPoints
  {
    [JsonField(JsonName = "colonial")]
    public List<PlayerRespawnPoint> Colonial = new List<PlayerRespawnPoint>();
    [JsonField(JsonName = "colonial_outpost")]
    public List<PlayerRespawnPoint> ColonialOutpost = new List<PlayerRespawnPoint>();
    [JsonField(JsonName = "cylon")]
    public List<PlayerRespawnPoint> Cylon = new List<PlayerRespawnPoint>();
    [JsonField(JsonName = "cylon_outpost")]
    public List<PlayerRespawnPoint> CylonOutpost = new List<PlayerRespawnPoint>();
  }
}
