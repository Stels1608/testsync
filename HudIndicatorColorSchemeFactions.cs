﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorColorSchemeFactions
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HudIndicatorColorSchemeFactions : IHudIndicatorColorScheme
{
  public const float alpha = 0.6f;

  private Color AddAlpha(Color color)
  {
    color.a = 0.6f;
    return color;
  }

  public Color BracketColor(ISpaceEntity entity)
  {
    return this.AddAlpha(HudIndicatorColorInfo.GetTargetColor4FactionMode(entity));
  }

  public Color TextColor(ISpaceEntity entity)
  {
    return HudIndicatorColorInfo.GetTargetColor4FactionMode(entity);
  }

  public Color HealthBarColor(ISpaceEntity entity)
  {
    return HudIndicatorColorsShared.HealthBarColor;
  }

  public Color HealthBarOutlineColor(ISpaceEntity entity)
  {
    return this.AddAlpha(this.BracketColor(entity));
  }

  public Color SelectionColor(ISpaceEntity entity)
  {
    return this.AddAlpha(entity.PlayerRelation != Relation.Enemy ? TargetColors.neutralColor : TargetColors.enemyColorFactions);
  }

  public Color IconColor(ISpaceEntity entity)
  {
    return this.AddAlpha(HudIndicatorColorInfo.GetTargetColor4FactionMode(entity));
  }
}
