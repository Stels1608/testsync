﻿// Decompiled with JetBrains decompiler
// Type: CapturedLoot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class CapturedLoot
{
  private Dictionary<ushort, CapturedLoot.LootContainer> lootEntries = new Dictionary<ushort, CapturedLoot.LootContainer>();
  private List<ushort> lootList = new List<ushort>();
  private int current = -1;

  public bool Empty
  {
    get
    {
      return this.Count == 0;
    }
  }

  public CapturedLoot.LootContainer Current
  {
    get
    {
      if (this.Count == 0)
        return (CapturedLoot.LootContainer) null;
      return this.lootEntries[this.lootList[this.current]];
    }
  }

  public int CurrentNo
  {
    get
    {
      return this.current + 1;
    }
  }

  public int Count
  {
    get
    {
      if (this.lootList != null)
        return this.lootList.Count;
      return 0;
    }
  }

  public CapturedLoot.LootContainer GetContainer(ushort lootID)
  {
    CapturedLoot.LootContainer lootContainer;
    if (!this.lootEntries.TryGetValue(lootID, out lootContainer))
    {
      lootContainer = new CapturedLoot.LootContainer(lootID);
      this.lootEntries.Add(lootID, lootContainer);
      this.lootList.Add(lootID);
      this.current = this.lootList.Count - 1;
    }
    return lootContainer;
  }

  public void AddItems(ushort lootID, ItemList items)
  {
    if (items.Count <= 0)
      return;
    this.GetContainer(lootID)._AddItems((SmartList<ShipItem>) items);
  }

  public void RemoveItems(ushort lootID, List<ushort> itemIDs)
  {
    CapturedLoot.LootContainer container = this.GetContainer(lootID);
    container._RemoveItems((IEnumerable<ushort>) itemIDs);
    if (container.Count != 0)
      return;
    this.RemoveCurrent();
  }

  public void RemoveCurrent()
  {
    if (this.lootList.Count == 0)
      return;
    this.lootEntries.Remove(this.lootList[this.current]);
    this.lootList.RemoveAt(this.current);
    this.current = this.lootList.Count - 1;
  }

  public void TakeAllItems()
  {
    CapturedLoot.LootContainer current = this.Current;
    if (current == null)
      return;
    if (current.Count == 0)
      this.RemoveCurrent();
    else
      current.MoveAll((IContainer) Game.Me.Hold);
  }

  public class LootContainer : ItemList
  {
    public LootContainer(ushort id)
      : base((IContainerID) new CapturedLoot.LootContainer.LootContainerID(id))
    {
    }

    public class LootContainerID : IContainerID, IProtocolWrite
    {
      private ushort id;

      public LootContainerID(ushort id)
      {
        this.id = id;
      }

      void IProtocolWrite.Write(BgoProtocolWriter w)
      {
        w.Write((byte) 5);
        w.Write(this.id);
      }

      public ContainerType GetContainerType()
      {
        return ContainerType.Loot;
      }
    }
  }
}
