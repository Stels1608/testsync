﻿// Decompiled with JetBrains decompiler
// Type: ChatProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Chat;
using Gui;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class ChatProtocol
{
  private static bool showWelcomeMessage = true;
  private List<ChatProtocolParser> parsers = new List<ChatProtocolParser>();
  private Dictionary<ChannelName, int> id = new Dictionary<ChannelName, int>();
  private Dictionary<string, string> separators = new Dictionary<string, string>();
  public string lastUserWhispered = string.Empty;
  public bool IsDebug;
  private Socket s;
  private byte eofMsgCode;
  private List<byte> buffer;
  private bool initialized;

  public bool IsConnected
  {
    get
    {
      if (this.s != null)
        return this.s.Connected;
      return false;
    }
  }

  public void Initialize()
  {
    if (this.initialized)
      return;
    this.eofMsgCode = Encoding.UTF8.GetBytes("#")[0];
    this.parsers.Add((ChatProtocolParser) new ChatLoggedIn());
    this.parsers.Add((ChatProtocolParser) new ChatLoginFailureBanLogin());
    this.parsers.Add((ChatProtocolParser) new ChatLoginFailure());
    this.parsers.Add((ChatProtocolParser) new ChatTextYouWhispered());
    this.parsers.Add((ChatProtocolParser) new ChatTextWhisperedToYou());
    this.parsers.Add((ChatProtocolParser) new ChatTextWhisperedToYouEx());
    this.parsers.Add((ChatProtocolParser) new ChatTextMessage());
    this.parsers.Add((ChatProtocolParser) new ChatTextMessageModerator());
    this.parsers.Add((ChatProtocolParser) new ChatTextMessageClanTag());
    this.parsers.Add((ChatProtocolParser) new ChatTextSendFailure());
    this.parsers.Add((ChatProtocolParser) new ChatFleetChannelUpdate());
    this.parsers.Add((ChatProtocolParser) new ChatSystemOpenChannelsUpdate());
    this.parsers.Add((ChatProtocolParser) new ChatDynamicRoomCreated());
    this.parsers.Add((ChatProtocolParser) new ChatDynamicRoomClosed());
    this.parsers.Add((ChatProtocolParser) new ChatKickBan());
    this.separators.Add("MSG_SEPERATOR", "%");
    this.separators.Add("PARAM_SEPERATOR", "@");
    this.separators.Add("ATRIBUTE_SEPERATOR", "|");
    this.separators.Add("OBJECT_SEPERATOR", "}");
    this.separators.Add("LINE_SEPERATOR", "#");
    this.initialized = true;
  }

  public void Connect(string userName, uint userId)
  {
    this.buffer = new List<byte>();
    try
    {
      if (this.IsDebug)
        Log.Add("chat protocol: trying to establish connection to a server...");
      this.s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
      if (this.IsDebug)
        Log.Add("Chat url: " + Game.Me.ChatserverUrl);
      this.s.Connect(IPAddress.Parse(Game.Me.ChatserverUrl), 9338);
      if (!this.s.Connected)
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.message_protocol_connection_failure%"));
      if ((int) Game.Me.ChatProjectID == 0 || !(Game.Me.ChatSessionId != "-1"))
        return;
      this.SendLoginRequest(userName, userId);
    }
    catch (Exception ex)
    {
      Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.message_protocol_connection_failure%"));
      Game.ChatStorage.Danger("Exception happened: " + ex.Message);
      Debug.LogError((object) ("Exception happened: " + ex.Message));
    }
  }

  public void Disconnect()
  {
    if (this.s != null)
      this.s.Close();
    this.s = (Socket) null;
    this.buffer = (List<byte>) null;
  }

  public void SendLoginRequest(string userName, uint userId)
  {
    int num = Game.Me.Faction != Faction.Colonial ? 2 : 1;
    string str = Game.Me.ChatSessionId.Replace("@", "PARAM_SEPERATOR");
    this.SendString("bu%-1%" + userName + "@" + userId.ToString() + "@" + str + "@" + (object) Game.Me.ChatProjectID + "@" + Game.ChatLanguage + "@noclan@2.3.0@0@" + num.ToString() + "@1");
  }

  private void SendGetRoomList()
  {
    this.SendString("bx%-1");
  }

  private void SendJoinRoom(ChannelName channel)
  {
    if (this.id.ContainsKey(channel))
      this.SendString("bz%-1%" + this.id[channel].ToString() + "@");
    else
      Log.Info((object) ("Room: " + channel.ToString() + ", not exists"));
  }

  public void SendText(string text, ChannelName channel)
  {
    if (!this.IsConnected)
      Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.message_protocol_unavailable%", (object) channel));
    else if (!this.id.ContainsKey(channel))
    {
      if (channel == ChannelName.Squadron)
        Game.ChatStorage.SystemSaysSquadron(BsgoLocalization.Get("%$bgo.etc.have_no_party%"));
      else if (channel == ChannelName.Wing)
        Game.ChatStorage.SystemSaysWing(BsgoLocalization.Get("%$bgo.etc.have_no_wing%"));
      else if (channel == ChannelName.Officer)
        Game.ChatStorage.SystemSaysWing(BsgoLocalization.Get("%$bgo.etc.have_no_officer%"));
      else
        Game.ChatStorage.SystemSays(BsgoLocalization.Get("%$bgo.etc.room_is_unavailable%", (object) channel.Name));
    }
    else if ((channel == ChannelName.FleetLocal || channel == ChannelName.FleetGlobal) && !Game.Me.Hold.HaveConsumableToActivate("consumable_radio"))
    {
      if (!this.id.ContainsKey(channel))
      {
        Log.Add("Fleet channel not found, this signals about protocol error");
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.etc.room_is_unavailable%", (object) channel));
      }
      this.OnFleetMessageSendFailed();
    }
    else
    {
      using (Dictionary<ChannelName, int>.KeyCollection.Enumerator enumerator = this.id.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ChannelName current = enumerator.Current;
          if (current == channel)
          {
            this.SendString("a%" + this.id[current].ToString() + "%" + this.EncodeText(text) + "@");
            break;
          }
        }
      }
    }
  }

  public void OnFleetMessageSendFailed()
  {
    Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.etc.you_lack_the_radio_transmission%"));
  }

  public void SendTextWhisper(string userName, string text, ChannelName channel)
  {
    using (Dictionary<ChannelName, int>.KeyCollection.Enumerator enumerator = this.id.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ChannelName current = enumerator.Current;
        if (current == channel)
        {
          this.SendString("a%" + this.id[current].ToString() + "%/w " + userName + " " + this.EncodeText(text) + "@");
          return;
        }
      }
    }
    Log.Add("there are no channels available, this is serious protocol error");
    Game.ChatStorage.SystemSays(BsgoLocalization.Get("%$bgo.etc.cannot_whisper%"));
  }

  private string EncodeText(string text)
  {
    string str = text;
    using (Dictionary<string, string>.Enumerator enumerator = this.separators.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        str = str.Replace(current.Value, current.Key);
      }
    }
    return str;
  }

  private string DecodeText(string text)
  {
    string str = text;
    using (Dictionary<string, string>.Enumerator enumerator = this.separators.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, string> current = enumerator.Current;
        str = str.Replace(current.Key, current.Value);
      }
    }
    return str;
  }

  private void SendString(string str)
  {
    if (!this.IsConnected)
      return;
    try
    {
      if (this.IsDebug)
        Log.Add("Chat proto, sends:" + str);
      str += (string) (object) '\n';
      byte[] bytes = Encoding.UTF8.GetBytes(str);
      if (this.s.Send(bytes) == bytes.Length)
        return;
      Log.Add("bytes send to server is less, than length of buffer");
    }
    catch (Exception ex)
    {
      Log.Add("Exception happened:" + ex.Message);
    }
  }

  public void Update()
  {
    if (!this.IsConnected)
      return;
    int available = this.s.Available;
    if (available <= 0)
      return;
    byte[] buffer = new byte[available];
    int num1 = this.s.Receive(buffer, available, SocketFlags.None);
    for (int index = 0; index < num1; ++index)
    {
      if ((int) buffer[index] != 0)
        this.buffer.Add(buffer[index]);
    }
    int num2;
    do
    {
      num2 = -1;
      for (int index1 = 0; index1 < this.buffer.Count; ++index1)
      {
        if ((int) this.buffer[index1] == (int) this.eofMsgCode)
        {
          num2 = index1;
          byte[] numArray = new byte[num2 + 1];
          this.buffer.CopyTo(0, numArray, 0, num2 + 1);
          this.buffer.RemoveRange(0, num2 + 1);
          string @string = Encoding.UTF8.GetString(numArray);
          if (this.IsDebug)
            Log.Add("ChatProto, read string:>" + @string + "<");
          for (int index2 = 0; index2 < this.parsers.Count; ++index2)
          {
            if (this.parsers[index2].TryParse(@string))
            {
              this.OnMessage(this.parsers[index2]);
              break;
            }
          }
          break;
        }
      }
    }
    while (num2 != -1);
  }

  private void OnMessage(ChatProtocolParser msg)
  {
    switch (msg.type)
    {
      case ChatProtocolParserType.LoginOk:
        this.SendGetRoomList();
        if (ChatProtocol.showWelcomeMessage)
        {
          Game.ChatStorage.Greetings(BsgoLocalization.Get("%$bgo.etc.welcome_to_battlestar_galactica_online%"));
          ChatProtocol.showWelcomeMessage = false;
        }
        CommunityProtocol.GetProtocol().SendChatIsConnected(Game.Me.ChatSessionId);
        break;
      case ChatProtocolParserType.LoginFailureAuthenticationFail:
        CommunityProtocol.GetProtocol().SendChatAuthenticationFail(Game.Me.ChatSessionId);
        break;
      case ChatProtocolParserType.LoginFailureBanLogin:
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.message_protocol_unavailable%"));
        Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.etc.your_account_has_been_banned%"));
        Game.ChatStorage.SystemSays(BsgoLocalization.Get("%$bgo.etc.ban_expires%", (object) ((ChatLoginFailureBanLogin) msg).endDate));
        break;
      case ChatProtocolParserType.LoginFailureBanIp:
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.message_protocol_unavailable%"));
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.etc.your_ip_has_been_banned%"));
        break;
      case ChatProtocolParserType.LoginFailureWrongChatVersion:
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.message_protocol_unavailable%"));
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.message_protocol_version_is_incorrect%"));
        break;
      case ChatProtocolParserType.FleetChannelUpdate:
        ChatFleetChannelUpdate fleetChannelUpdate = (ChatFleetChannelUpdate) msg;
        this.id.Remove(ChannelName.FleetLocal);
        this.id.Remove(ChannelName.FleetGlobal);
        if (fleetChannelUpdate.Local != null)
          this.id.Add(ChannelName.FleetLocal, fleetChannelUpdate.Local.id);
        if (fleetChannelUpdate.Global != null)
          this.id.Add(ChannelName.FleetGlobal, fleetChannelUpdate.Global.id);
        this.SendJoinRoom(ChannelName.FleetLocal);
        this.SendJoinRoom(ChannelName.FleetGlobal);
        break;
      case ChatProtocolParserType.Text:
        ChatTextMessage chatTextMessage = (ChatTextMessage) msg;
        if (Game.Me.Friends.IsIgnored(chatTextMessage.userName) || !this.id.ContainsValue(chatTextMessage.roomId))
          break;
        ChannelName channel1 = new ChannelName(-1, "this is for compiler");
        using (Dictionary<ChannelName, int>.Enumerator enumerator = this.id.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<ChannelName, int> current = enumerator.Current;
            if (current.Value == chatTextMessage.roomId)
              channel1 = current.Key;
          }
        }
        string what1 = this.DecodeText(chatTextMessage.text);
        Game.ChatStorage.Proto(channel1, chatTextMessage.userName, what1);
        break;
      case ChatProtocolParserType.TextFromModerator:
        ChatTextMessageModerator messageModerator = (ChatTextMessageModerator) msg;
        if (Game.Me.Friends.IsIgnored(messageModerator.userName) || !this.id.ContainsValue(messageModerator.roomId))
          break;
        ChannelName channel2 = new ChannelName(-1, "this is for compiler");
        using (Dictionary<ChannelName, int>.Enumerator enumerator = this.id.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<ChannelName, int> current = enumerator.Current;
            if (current.Value == messageModerator.roomId)
              channel2 = current.Key;
          }
        }
        string what2 = this.DecodeText(messageModerator.text);
        Game.ChatStorage.Mod(channel2, messageModerator.userName, what2);
        break;
      case ChatProtocolParserType.TextClanTagged:
        ChatTextMessageClanTag textMessageClanTag = (ChatTextMessageClanTag) msg;
        if (Game.Me.Friends.IsIgnored(textMessageClanTag.userName) || !this.id.ContainsValue(textMessageClanTag.roomId))
          break;
        ChannelName channel3 = new ChannelName(-1, "this is for compiler");
        using (Dictionary<ChannelName, int>.Enumerator enumerator = this.id.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<ChannelName, int> current = enumerator.Current;
            if (current.Value == textMessageClanTag.roomId)
              channel3 = current.Key;
          }
        }
        string what3 = this.DecodeText(textMessageClanTag.text);
        Game.ChatStorage.Proto(channel3, textMessageClanTag.userName, what3);
        break;
      case ChatProtocolParserType.TextYouWhisper:
        ChatTextYouWhispered textYouWhispered = (ChatTextYouWhispered) msg;
        Game.ChatStorage.Whisper(Game.Me.Name, this.DecodeText(textYouWhispered.text), textYouWhispered.receiverName);
        break;
      case ChatProtocolParserType.TextWhisperToYou:
        ChatTextWhisperedToYou textWhisperedToYou = (ChatTextWhisperedToYou) msg;
        if (Game.Me.Friends.IsIgnored(textWhisperedToYou.userName))
          break;
        Game.ChatStorage.WhisperedToYou(textWhisperedToYou.userName, this.DecodeText(textWhisperedToYou.text));
        this.lastUserWhispered = textWhisperedToYou.userName;
        break;
      case ChatProtocolParserType.TextFailureCannotWhisperSelf:
        Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.chat.message_protocol_cannot_whisper_self%"));
        break;
      case ChatProtocolParserType.TextFailureBan:
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.message_protocol_unavailable%"));
        Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.etc.you_has_been_banned%"));
        break;
      case ChatProtocolParserType.TextFailureKick:
        Game.ChatStorage.SystemSays(BsgoLocalization.Get("%$bgo.etc.your_message_has_been_declined%"));
        break;
      case ChatProtocolParserType.TextFailureFlood:
        Game.ChatStorage.SystemSays(BsgoLocalization.Get("%$bgo.etc.you_are_flooding_chat%"));
        break;
      case ChatProtocolParserType.TextFailureUserNotExists:
        Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.etc.user_not_exists%"));
        break;
      case ChatProtocolParserType.SystemOpenChannelsUpdate:
        ChatSystemOpenChannelsUpdate openChannelsUpdate = (ChatSystemOpenChannelsUpdate) msg;
        this.id.Remove(ChannelName.OpenLocal);
        this.id.Remove(ChannelName.SystemLocal);
        this.id.Remove(ChannelName.OpenGlobal);
        this.id.Remove(ChannelName.SystemGlobal);
        if (openChannelsUpdate.SystemLocal != null)
          this.id.Add(ChannelName.SystemLocal, openChannelsUpdate.SystemLocal.id);
        if (openChannelsUpdate.SystemGlobal != null)
          this.id.Add(ChannelName.SystemGlobal, openChannelsUpdate.SystemGlobal.id);
        if (openChannelsUpdate.OpenLocal != null)
          this.id.Add(ChannelName.OpenLocal, openChannelsUpdate.OpenLocal.id);
        if (openChannelsUpdate.OpenGlobal != null)
          this.id.Add(ChannelName.OpenGlobal, openChannelsUpdate.OpenGlobal.id);
        if (!this.IsDebug)
          break;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("[chat] received SystemOpenChannelsUpdate: sector.SystemLocal = ");
        if (openChannelsUpdate.SystemLocal == null)
          stringBuilder.Append("*null*");
        else
          stringBuilder.Append(openChannelsUpdate.SystemLocal.ToString());
        stringBuilder.Append("; sector.SystemGlobal = ");
        if (openChannelsUpdate.SystemGlobal == null)
          stringBuilder.Append("*null*");
        else
          stringBuilder.Append(openChannelsUpdate.SystemGlobal.ToString());
        stringBuilder.Append("; sector.OpenLocal = ");
        if (openChannelsUpdate.OpenLocal == null)
          stringBuilder.Append("*null*");
        else
          stringBuilder.Append(openChannelsUpdate.OpenLocal.ToString());
        stringBuilder.Append("; sector.OpenGlobal = ");
        if (openChannelsUpdate.OpenGlobal == null)
          stringBuilder.Append("*null*");
        else
          stringBuilder.Append(openChannelsUpdate.OpenGlobal.ToString());
        stringBuilder.Append(".");
        Log.Add(stringBuilder.ToString());
        break;
      case ChatProtocolParserType.DynamicRoomCreateOk:
        ChatDynamicRoomCreated dynamicRoomCreated = (ChatDynamicRoomCreated) msg;
        string str = dynamicRoomCreated.roomName.Remove(dynamicRoomCreated.roomName.IndexOf('_'));
        if (str.Contains("squadron"))
        {
          this.id.Add(ChannelName.Squadron, dynamicRoomCreated.roomID);
          break;
        }
        if (str.Contains("officers"))
        {
          this.id.Add(ChannelName.Officer, dynamicRoomCreated.roomID);
          break;
        }
        if (str.Contains("wing"))
        {
          this.id.Add(ChannelName.Wing, dynamicRoomCreated.roomID);
          break;
        }
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.etc.incorrect_room_name%") + str);
        break;
      case ChatProtocolParserType.DynamicRoomClosed:
        ChatDynamicRoomClosed dynamicRoomClosed = (ChatDynamicRoomClosed) msg;
        using (Dictionary<ChannelName, int>.Enumerator enumerator = this.id.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<ChannelName, int> current = enumerator.Current;
            if (current.Value == dynamicRoomClosed.roomID)
            {
              this.id.Remove(current.Key);
              break;
            }
          }
          break;
        }
      case ChatProtocolParserType.KickUserWithReason:
        ChatKickBan chatKickBan1 = (ChatKickBan) msg;
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.message_protocol_unavailable%"));
        Game.ChatStorage.Ouch(Tools.ParseMessage("%$bgo.etc.chat.kicked%", !(chatKickBan1.reason == string.Empty) ? (object) chatKickBan1.reason : (object) "%$bgo.etc.chat.no_reason%"));
        break;
      case ChatProtocolParserType.BannUserWithReason:
        ChatKickBan chatKickBan2 = (ChatKickBan) msg;
        Game.ChatStorage.Danger(BsgoLocalization.Get("%$bgo.chat.message_protocol_unavailable%"));
        Game.ChatStorage.Ouch(Tools.ParseMessage("%$bgo.etc.chat.banned%", !(chatKickBan2.reason == string.Empty) ? (object) chatKickBan2.reason : (object) "%$bgo.etc.chat.no_reason%"));
        break;
      default:
        Game.ChatStorage.Ouch(BsgoLocalization.Get("%$bgo.etc.developer_error%", (object) msg));
        break;
    }
  }
}
