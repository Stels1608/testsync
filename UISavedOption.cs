﻿// Decompiled with JetBrains decompiler
// Type: UISavedOption
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Saved Option")]
public class UISavedOption : MonoBehaviour
{
  public string keyName;
  private UIPopupList mList;
  private UIToggle mCheck;

  private string key
  {
    get
    {
      if (string.IsNullOrEmpty(this.keyName))
        return "NGUI State: " + this.name;
      return this.keyName;
    }
  }

  private void Awake()
  {
    this.mList = this.GetComponent<UIPopupList>();
    this.mCheck = this.GetComponent<UIToggle>();
  }

  private void OnEnable()
  {
    if ((Object) this.mList != (Object) null)
      EventDelegate.Add(this.mList.onChange, new EventDelegate.Callback(this.SaveSelection));
    if ((Object) this.mCheck != (Object) null)
      EventDelegate.Add(this.mCheck.onChange, new EventDelegate.Callback(this.SaveState));
    if ((Object) this.mList != (Object) null)
    {
      string @string = PlayerPrefs.GetString(this.key);
      if (string.IsNullOrEmpty(@string))
        return;
      this.mList.value = @string;
    }
    else if ((Object) this.mCheck != (Object) null)
    {
      this.mCheck.value = PlayerPrefs.GetInt(this.key, 1) != 0;
    }
    else
    {
      string @string = PlayerPrefs.GetString(this.key);
      UIToggle[] componentsInChildren = this.GetComponentsInChildren<UIToggle>(true);
      int index = 0;
      for (int length = componentsInChildren.Length; index < length; ++index)
      {
        UIToggle uiToggle = componentsInChildren[index];
        uiToggle.value = uiToggle.name == @string;
      }
    }
  }

  private void OnDisable()
  {
    if ((Object) this.mCheck != (Object) null)
      EventDelegate.Remove(this.mCheck.onChange, new EventDelegate.Callback(this.SaveState));
    if ((Object) this.mList != (Object) null)
      EventDelegate.Remove(this.mList.onChange, new EventDelegate.Callback(this.SaveSelection));
    if (!((Object) this.mCheck == (Object) null) || !((Object) this.mList == (Object) null))
      return;
    UIToggle[] componentsInChildren = this.GetComponentsInChildren<UIToggle>(true);
    int index = 0;
    for (int length = componentsInChildren.Length; index < length; ++index)
    {
      UIToggle uiToggle = componentsInChildren[index];
      if (uiToggle.value)
      {
        PlayerPrefs.SetString(this.key, uiToggle.name);
        break;
      }
    }
  }

  public void SaveSelection()
  {
    PlayerPrefs.SetString(this.key, UIPopupList.current.value);
  }

  public void SaveState()
  {
    PlayerPrefs.SetInt(this.key, !UIToggle.current.value ? 0 : 1);
  }
}
