﻿// Decompiled with JetBrains decompiler
// Type: ProtectTaskLocalizer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class ProtectTaskLocalizer
{
  private const byte FREIGHTER = 1;
  private const byte DRONE_INSURGENT = 2;

  public string GetDescription(SectorEventTaskSubType subType, Faction ownerFaction, Faction myFaction)
  {
    return BsgoLocalization.Get(this.GetTranslationPrefix(subType) + (myFaction != ownerFaction ? "rival." : "owner.") + myFaction.ToString().ToLowerInvariant());
  }

  public string GetTimeText(SectorEventTaskSubType subType)
  {
    return BsgoLocalization.Get(this.GetTranslationPrefix(subType) + "progression.time");
  }

  public string GetProgressbarText(SectorEventTaskSubType subType)
  {
    return BsgoLocalization.Get(this.GetTranslationPrefix(subType) + "progression.hp");
  }

  private string GetTranslationPrefix(SectorEventTaskSubType subType)
  {
    switch (subType)
    {
      case SectorEventTaskSubType.FREIGHTER:
        return "bgo.sector_event.freighter_in_distress.task.1.";
      case SectorEventTaskSubType.DRONE_INSURGENT:
        return "bgo.sector_event.drone_insurgent.task.1.";
      default:
        return string.Empty;
    }
  }
}
