﻿// Decompiled with JetBrains decompiler
// Type: CombatTextFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CombatTextFactory
{
  private static readonly Color OWN_DAMAGE_COLOR = new Color(1f, 0.5019608f, 0.0f);
  private static readonly Color HEAL_COLOR = new Color(0.0f, 1f, 0.0f);
  private const float MESSAGE_DISPLAY_TIME = 1.5f;

  public static AbstractCombatMessage CreateDamageMessage(SpaceObject victim, float rawDamage)
  {
    int num = (int) Mathf.Abs(rawDamage);
    Color color = Color.white;
    switch (victim.Faction)
    {
      case Faction.Neutral:
        color = TargetColorsLegacyBrackets.neutralColor;
        break;
      case Faction.Colonial:
        color = TargetColorsLegacyBrackets.colonialColor;
        break;
      case Faction.Cylon:
        color = TargetColorsLegacyBrackets.cylonColor;
        break;
      case Faction.Ancient:
        color = TargetColorsLegacyBrackets.ancientColor;
        break;
    }
    return (AbstractCombatMessage) new BsgoTextCombatMessage(num.ToString(), color, victim.Model.transform, 1.5f);
  }

  public static AbstractCombatMessage CreateSelfDamageMessage(SpaceObject victim, float rawDamage)
  {
    return (AbstractCombatMessage) new BsgoTextCombatMessage(((int) Mathf.Abs(rawDamage)).ToString(), CombatTextFactory.OWN_DAMAGE_COLOR, victim.Model.transform, 1.5f);
  }

  public static AbstractCombatMessage CreateHealMessage(SpaceObject spaceObject, float rawHealing)
  {
    return (AbstractCombatMessage) new BsgoTextCombatMessage("+" + (object) (int) Mathf.Abs(rawHealing), CombatTextFactory.HEAL_COLOR, spaceObject.Model.transform, 1.5f);
  }
}
