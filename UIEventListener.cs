﻿// Decompiled with JetBrains decompiler
// Type: UIEventListener
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Internal/Event Listener")]
public class UIEventListener : MonoBehaviour
{
  public object parameter;
  public UIEventListener.VoidDelegate onSubmit;
  public UIEventListener.VoidDelegate onClick;
  public UIEventListener.VoidDelegate onDoubleClick;
  public UIEventListener.BoolDelegate onHover;
  public UIEventListener.BoolDelegate onPress;
  public UIEventListener.BoolDelegate onSelect;
  public UIEventListener.FloatDelegate onScroll;
  public UIEventListener.VoidDelegate onDragStart;
  public UIEventListener.VectorDelegate onDrag;
  public UIEventListener.VoidDelegate onDragOver;
  public UIEventListener.VoidDelegate onDragOut;
  public UIEventListener.VoidDelegate onDragEnd;
  public UIEventListener.ObjectDelegate onDrop;
  public UIEventListener.KeyCodeDelegate onKey;
  public UIEventListener.BoolDelegate onTooltip;

  private void OnSubmit()
  {
    if (this.onSubmit == null)
      return;
    this.onSubmit(this.gameObject);
  }

  private void OnClick()
  {
    if (this.onClick == null)
      return;
    this.onClick(this.gameObject);
  }

  private void OnDoubleClick()
  {
    if (this.onDoubleClick == null)
      return;
    this.onDoubleClick(this.gameObject);
  }

  private void OnHover(bool isOver)
  {
    if (this.onHover == null)
      return;
    this.onHover(this.gameObject, isOver);
  }

  private void OnPress(bool isPressed)
  {
    if (this.onPress == null)
      return;
    this.onPress(this.gameObject, isPressed);
  }

  private void OnSelect(bool selected)
  {
    if (this.onSelect == null)
      return;
    this.onSelect(this.gameObject, selected);
  }

  private void OnScroll(float delta)
  {
    if (this.onScroll == null)
      return;
    this.onScroll(this.gameObject, delta);
  }

  private void OnDragStart()
  {
    if (this.onDragStart == null)
      return;
    this.onDragStart(this.gameObject);
  }

  private void OnDrag(Vector2 delta)
  {
    if (this.onDrag == null)
      return;
    this.onDrag(this.gameObject, delta);
  }

  private void OnDragOver()
  {
    if (this.onDragOver == null)
      return;
    this.onDragOver(this.gameObject);
  }

  private void OnDragOut()
  {
    if (this.onDragOut == null)
      return;
    this.onDragOut(this.gameObject);
  }

  private void OnDragEnd()
  {
    if (this.onDragEnd == null)
      return;
    this.onDragEnd(this.gameObject);
  }

  private void OnDrop(GameObject go)
  {
    if (this.onDrop == null)
      return;
    this.onDrop(this.gameObject, go);
  }

  private void OnKey(KeyCode key)
  {
    if (this.onKey == null)
      return;
    this.onKey(this.gameObject, key);
  }

  private void OnTooltip(bool show)
  {
    if (this.onTooltip == null)
      return;
    this.onTooltip(this.gameObject, show);
  }

  public static UIEventListener Get(GameObject go)
  {
    UIEventListener uiEventListener = go.GetComponent<UIEventListener>();
    if ((Object) uiEventListener == (Object) null)
      uiEventListener = go.AddComponent<UIEventListener>();
    return uiEventListener;
  }

  public delegate void VoidDelegate(GameObject go);

  public delegate void BoolDelegate(GameObject go, bool state);

  public delegate void FloatDelegate(GameObject go, float delta);

  public delegate void VectorDelegate(GameObject go, Vector2 delta);

  public delegate void ObjectDelegate(GameObject go, GameObject obj);

  public delegate void KeyCodeDelegate(GameObject go, KeyCode key);
}
