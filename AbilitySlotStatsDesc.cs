﻿// Decompiled with JetBrains decompiler
// Type: AbilitySlotStatsDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class AbilitySlotStatsDesc : IProtocolRead
{
  public ushort SlotID;
  public ObjectStats Stats;

  void IProtocolRead.Read(BgoProtocolReader r)
  {
    this.SlotID = r.ReadUInt16();
    this.Stats = r.ReadDesc<ObjectStats>();
  }

  public override string ToString()
  {
    return "[AbilityGameStatsDesc] SlotID: " + (object) this.SlotID + " ObjectStats: " + (object) this.Stats;
  }
}
