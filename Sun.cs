﻿// Decompiled with JetBrains decompiler
// Type: Sun
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Sun : MonoBehaviour
{
  public Color sunDiscColor = Color.white;
  public Color sunRaysColor = Color.white;
  public Color sunGlowColor = Color.white;
  public Color sunStreakColor = Color.white;
  public bool sunOcclusionFade = true;
  private float fadeValue = 1f;
  private UpdateLimiter updateLimiter = new UpdateLimiter(25);
  public const string SUN_TAG = "Sun";
  private const float fadeDelta = 10f;
  private const float angleThreshold = 0.8f;
  public GameObject sunDisc;
  public GameObject sunRays;
  public GameObject sunGlow;
  public GameObject sunStreak;

  public void LateUpdate()
  {
    Camera camera = (Camera) null;
    if (SectorEditorHelper.IsEditorLevel())
    {
      if ((Object) EditorLevel.activeCoolCam != (Object) null)
        camera = EditorLevel.activeCoolCam.GetComponentInChildren<Camera>();
    }
    else if ((Object) SpaceLevel.GetLevel() != (Object) null && SpaceLevel.GetLevel().cameraSwitcher != null)
      camera = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera();
    if (!((Object) camera != (Object) null))
      return;
    this.transform.rotation = camera.transform.rotation;
    if (!this.updateLimiter.CanUpdate())
      return;
    Vector3 normalized1 = camera.transform.forward.normalized;
    Vector3 normalized2 = this.transform.position.normalized;
    this.sunDisc.GetComponent<Renderer>().material.color = this.sunDiscColor;
    float num1 = Mathf.Clamp01((float) (((double) Mathf.Clamp(Vector3.Dot(normalized1, normalized2), 0.8f, 1f) - 0.800000011920929) / 0.199999988079071));
    if (this.sunOcclusionFade)
    {
      RaycastHit hitInfo;
      if (Physics.Raycast(camera.transform.position, normalized2, out hitInfo))
        this.fadeValue -= Time.deltaTime * 10f;
      else
        this.fadeValue += Time.deltaTime * 10f;
      this.fadeValue = Mathf.Clamp01(this.fadeValue);
    }
    else
      this.fadeValue = 1f;
    this.sunRays.GetComponent<Renderer>().material.color = this.sunRaysColor * num1 * this.fadeValue;
    this.sunStreak.GetComponent<Renderer>().material.color = this.sunStreakColor * num1 * this.fadeValue;
    float min = 0.17f;
    float num2 = Mathf.Clamp01((float) (((double) Mathf.Clamp(Vector3.Dot(normalized1, normalized2), min, 1f) - (double) min) / (1.0 - (double) min)));
    this.sunGlow.transform.localScale = new Vector3((float) (1.0 + 10.0 * (double) num2), 1f, (float) (1.0 + 10.0 * (double) num2));
    this.sunGlow.GetComponent<Renderer>().material.color = this.sunGlowColor * num2 * this.fadeValue;
  }
}
