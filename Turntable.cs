﻿// Decompiled with JetBrains decompiler
// Type: Turntable
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.EventSystems;

public class Turntable : MonoBehaviour
{
  private EventSystem eventSystem;

  private void Start()
  {
    this.eventSystem = Object.FindObjectOfType<EventSystem>();
  }

  private void Update()
  {
    if (!Input.GetMouseButton(0) || this.eventSystem.IsPointerOverGameObject())
      return;
    float axis = Input.GetAxis("Mouse X");
    if ((double) Mathf.Abs(axis) < 0.00999999977648258)
      return;
    this.transform.RotateAround(this.transform.position, this.transform.up, (float) (-(double) axis * 5.0));
  }
}
