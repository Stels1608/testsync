﻿// Decompiled with JetBrains decompiler
// Type: IInputBinding
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public interface IInputBinding
{
  Action Action { get; }

  ushort DeviceTriggerCode { get; }

  ushort DeviceModifierCode { get; }

  InputDevice Device { get; }

  byte Flags { get; }

  byte ProfileNo { get; }

  bool IsUnbound { get; }

  string TriggerAlias { get; }

  string ModifierAlias { get; }

  string BindingAlias { get; }

  IInputBinding Copy();

  void Unmap();

  bool CombinationCollidesWith(IInputBinding otherInputBinding);

  bool IsBoundByCombination(InputDevice device, ushort deviceTriggerCode, ushort deviceModifierCode);

  bool IsBoundByCombinationOf(IInputBinding otherInputBinding);

  bool IsBoundByTrigger(InputDevice device, ushort deviceTriggerCode);

  bool IsBoundByModifier(InputDevice device, ushort deviceModifierCode);
}
