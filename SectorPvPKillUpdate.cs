﻿// Decompiled with JetBrains decompiler
// Type: SectorPvPKillUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class SectorPvPKillUpdate : GalaxyMapUpdate
{
  public int Kills { get; private set; }

  public SectorPvPKillUpdate(Faction faction, uint sectorId, int kills)
    : base(GalaxyUpdateType.SectorPvPKills, faction, (int) sectorId)
  {
    this.Kills = kills;
  }

  public override string ToString()
  {
    return "[SectorPvPKillUpdate] (Faction: " + (object) this.Faction + ", SectorId: " + (object) this.SectorId + ", Kills:" + (object) this.Kills + ")";
  }
}
