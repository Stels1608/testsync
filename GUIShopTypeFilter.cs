﻿// Decompiled with JetBrains decompiler
// Type: GUIShopTypeFilter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;

public class GUIShopTypeFilter : GUIVerticalLayout
{
  private readonly List<GUIShopCategoryButton> buttons = new List<GUIShopCategoryButton>();
  private readonly GUIShopCategoryButton allButton;
  private readonly GUIShopCategoryButton weaponButton;
  private readonly GUIShopCategoryButton computerButton;
  private readonly GUIShopCategoryButton hullButton;
  private readonly GUIShopCategoryButton engineButton;
  private readonly GUIShopCategoryButton resourceButton;
  private readonly GUIShopCategoryButton consumableButton;
  private readonly GUIShopCategoryButton boosterButton;
  private readonly GUIShopCategoryButton paintButton;
  private readonly GUIShopCategoryButton avionicsButton;
  private Predicate<ShipItem> selectedFilter;
  protected GUIShopTypeFilter.dHandler handler;

  public Predicate<ShipItem> SelectedFilter
  {
    get
    {
      return this.selectedFilter;
    }
    set
    {
      this.selectedFilter = value;
      if (this.handler == null)
        return;
      this.handler();
    }
  }

  public GUIShopTypeFilter.dHandler Handler
  {
    set
    {
      this.handler = value;
    }
  }

  public GUIShopTypeFilter(SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
    this.allButton = new GUIShopCategoryButton("icon_bonus", new AnonymousDelegate(this.SelectSpecialOffers), new GUIShopSubtypeFilter("%$bgo.shop.category.hot%", this, true));
    GUIShopSubtypeFilter subtypeFilter1 = new GUIShopSubtypeFilter("%$bgo.shop.category.weapon%", this, false);
    subtypeFilter1.AddButton("%$bgo.shop.category.weapon.cannon%", ShopCategory.System, ShopItemType.Weapon, "cannon");
    subtypeFilter1.AddButton("%$bgo.shop.category.weapon.launcher%", ShopCategory.System, ShopItemType.Weapon, "missile");
    subtypeFilter1.AddButton("%$bgo.shop.category.weapon.torpedo_launcher%", ShopCategory.System, ShopItemType.Weapon, "torpedo");
    subtypeFilter1.AddButton("%$bgo.shop.category.weapon.flak%", ShopCategory.System, ShopItemType.Weapon, "flak");
    subtypeFilter1.AddButton("%$bgo.shop.category.weapon.pd%", ShopCategory.System, ShopItemType.Weapon, "pd");
    subtypeFilter1.AddButton("%$bgo.shop.category.weapon.deployable%", ShopCategory.System, ShopItemType.Weapon, "deployable");
    subtypeFilter1.AddButton("%$bgo.shop.category.weapon.mining%", ShopCategory.System, ShopItemType.Weapon, "mining");
    this.weaponButton = new GUIShopCategoryButton("icon_weapons", new AnonymousDelegate(this.SelectWeapons), subtypeFilter1);
    GUIShopSubtypeFilter subtypeFilter2 = new GUIShopSubtypeFilter("%$bgo.shop.category.computer%", this, false);
    subtypeFilter2.AddButton("%$bgo.shop.category.computer.navigation%", ShopCategory.System, ShopItemType.Computer, "navigation");
    subtypeFilter2.AddButton("%$bgo.shop.category.computer.electronic_warfare%", ShopCategory.System, ShopItemType.Computer, "e_war");
    subtypeFilter2.AddButton("%$bgo.shop.category.computer.electronic_support%", ShopCategory.System, ShopItemType.Computer, "e_support");
    subtypeFilter2.AddButton("%$bgo.shop.category.computer.power%", ShopCategory.System, ShopItemType.Computer, "power");
    subtypeFilter2.AddButton("%$bgo.shop.category.computer.scanner%", ShopCategory.System, ShopItemType.Computer, "scanner");
    this.computerButton = new GUIShopCategoryButton("icon_computer", new AnonymousDelegate(this.SelectComputer), subtypeFilter2);
    GUIShopSubtypeFilter subtypeFilter3 = new GUIShopSubtypeFilter("%$bgo.shop.category.hull%", this, false);
    subtypeFilter3.AddButton("%$bgo.shop.category.hull.armor%", ShopCategory.System, ShopItemType.Hull, "armor");
    subtypeFilter3.AddButton("%$bgo.shop.category.hull.flare_launcher%", ShopCategory.System, ShopItemType.Hull, "flare");
    subtypeFilter3.AddButton("%$bgo.shop.category.hull.mine_launcher%", ShopCategory.System, ShopItemType.Hull, "mine");
    subtypeFilter3.AddButton("%$bgo.shop.category.hull.system_modifications%", ShopCategory.System, ShopItemType.Hull, "system_modification");
    subtypeFilter3.AddButton("%$bgo.shop.category.hull.repair%", ShopCategory.System, ShopItemType.Hull, "repair");
    this.hullButton = new GUIShopCategoryButton("icon_hull", new AnonymousDelegate(this.SelectHull), subtypeFilter3);
    GUIShopSubtypeFilter subtypeFilter4 = new GUIShopSubtypeFilter("%$bgo.shop.category.engine%", this, false);
    subtypeFilter4.AddButton("%$bgo.shop.category.engine.maneuver%", ShopCategory.System, ShopItemType.Engine, "gyro", "slide");
    subtypeFilter4.AddButton("%$bgo.shop.category.engine.thruster%", ShopCategory.System, ShopItemType.Engine, "thruster");
    subtypeFilter4.AddButton("%$bgo.shop.category.engine.evasion%", ShopCategory.System, ShopItemType.Engine, "evasion");
    subtypeFilter4.AddButton("%$bgo.shop.category.engine.ftl%", ShopCategory.System, ShopItemType.Engine, "ftl");
    this.engineButton = new GUIShopCategoryButton("icon_engine", new AnonymousDelegate(this.SelectEngine), subtypeFilter4);
    GUIShopSubtypeFilter subtypeFilter5 = new GUIShopSubtypeFilter("%$bgo.shop.category.consumable%", this, false);
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.round%", ShopCategory.Consumable, ShopItemType.Round, new string[0]);
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.missile%", ShopCategory.Consumable, ShopItemType.Missile, new string[0]);
    Predicate<ShipItem> filter1 = (Predicate<ShipItem>) (shipItem =>
    {
      if (shipItem.ShopItemCard.ItemType != ShopItemType.Torpedo)
        return shipItem.ShopItemCard.ItemType == ShopItemType.AntiCapitalMissile;
      return true;
    });
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.torpedo%", ShopCategory.Consumable, filter1);
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.repair%", ShopCategory.Consumable, ShopItemType.Repair, new string[0]);
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.power%", ShopCategory.Consumable, ShopItemType.Power, new string[0]);
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.flare%", ShopCategory.Consumable, ShopItemType.Flare, new string[0]);
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.mine%", ShopCategory.Consumable, ShopItemType.Mine, new string[0]);
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.flak%", ShopCategory.Consumable, ShopItemType.Flak, new string[0]);
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.pd%", ShopCategory.Consumable, ShopItemType.PointDefense, new string[0]);
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.metal_plate%", ShopCategory.Consumable, ShopItemType.MetalPlate, new string[0]);
    Predicate<ShipItem> filter2 = (Predicate<ShipItem>) (shipItem =>
    {
      if (shipItem.ShopItemCard.ItemType != ShopItemType.Radio && shipItem.ShopItemCard.ItemType != ShopItemType.TechAnalysis)
        return shipItem.ShopItemCard.ItemType == ShopItemType.JumpTargetTransponder;
      return true;
    });
    subtypeFilter5.AddButton("%$bgo.shop.category.consumable.misc%", ShopCategory.Consumable, filter2);
    this.consumableButton = new GUIShopCategoryButton("icon_consumables", new AnonymousDelegate(this.SelectConsumables), subtypeFilter5);
    this.resourceButton = new GUIShopCategoryButton("icon_resources", new AnonymousDelegate(this.SelectResources), new GUIShopSubtypeFilter("%$bgo.shop.category.resource%", this, true));
    this.boosterButton = new GUIShopCategoryButton("icon_booster", new AnonymousDelegate(this.SelectBooster), new GUIShopSubtypeFilter("%$bgo.shop.category.booster%", this, true));
    this.paintButton = new GUIShopCategoryButton("icon_paints", new AnonymousDelegate(this.SelectPaints), new GUIShopSubtypeFilter("%$bgo.shop.category.paint%", this, true));
    this.avionicsButton = new GUIShopCategoryButton("icon_avionics", new AnonymousDelegate(this.SelectAvionics), new GUIShopSubtypeFilter("%$bgo.shop.category.avionics%", this, true));
    this.buttons.Add(this.resourceButton);
    this.buttons.Add(this.consumableButton);
    this.buttons.Add(this.boosterButton);
    this.buttons.Add(this.weaponButton);
    this.buttons.Add(this.computerButton);
    this.buttons.Add(this.hullButton);
    this.buttons.Add(this.engineButton);
    this.buttons.Add(this.paintButton);
    this.buttons.Add(this.avionicsButton);
    this.buttons.Add(this.allButton);
    using (List<GUIShopCategoryButton>.Enumerator enumerator = this.buttons.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.AddPanel((GUIPanel) enumerator.Current);
    }
    this.SelectResources();
  }

  public void IsInShop(bool value)
  {
    if (value)
    {
      this.allButton.Header = "%$bgo.shop.category.hot%";
      this.allButton.Button.Handler = new AnonymousDelegate(this.SelectSpecialOffers);
    }
    else
    {
      this.allButton.Header = "%$bgo.shop.category.all%";
      this.allButton.Button.Handler = new AnonymousDelegate(this.SelectAll);
    }
    this.allButton.IconPath = !value ? "icon_all" : "icon_bonus";
    if (!this.allButton.Button.IsPressed)
      return;
    this.allButton.Button.Handler();
  }

  private void SelectAll()
  {
    this.SelectedFilter = this.GetAllFilter();
    this.SelectButton(this.allButton);
  }

  private void SelectWeapons()
  {
    this.SelectedFilter = this.GetItemTypeFilter(ShopItemType.Weapon);
    this.SelectButton(this.weaponButton);
  }

  private void SelectHull()
  {
    this.SelectedFilter = this.GetItemTypeFilter(ShopItemType.Hull);
    this.SelectButton(this.hullButton);
  }

  private void SelectComputer()
  {
    this.SelectedFilter = this.GetItemTypeFilter(ShopItemType.Computer);
    this.SelectButton(this.computerButton);
  }

  private void SelectEngine()
  {
    this.SelectedFilter = this.GetItemTypeFilter(ShopItemType.Engine);
    this.SelectButton(this.engineButton);
  }

  public void SelectResources()
  {
    this.SelectedFilter = this.GetItemTypeFilter(ShopItemType.Resource, ShopItemType.Junk);
    this.SelectButton(this.resourceButton);
  }

  private void SelectBooster()
  {
    this.SelectedFilter = this.GetItemTypeFilter(ShopItemType.Augment);
    this.SelectButton(this.boosterButton);
  }

  private void SelectConsumables()
  {
    this.SelectedFilter = this.GetItemTypeFilter(ShopItemType.Round, ShopItemType.PointDefense, ShopItemType.Flak, ShopItemType.Mine, ShopItemType.Missile, ShopItemType.Flare, ShopItemType.Power, ShopItemType.Repair, ShopItemType.Radio, ShopItemType.JumpTargetTransponder, ShopItemType.TechAnalysis, ShopItemType.Torpedo, ShopItemType.MetalPlate, ShopItemType.AntiCapitalMissile);
    this.SelectButton(this.consumableButton);
  }

  private void SelectSpecialOffers()
  {
    this.SelectedFilter = (Predicate<ShipItem>) (item =>
    {
      if (item.ShopItemCard.ItemType == ShopItemType.StarterKit)
        return true;
      if (Shop.FindItemDiscount(item.CardGUID) != null)
        return item.ShopItemCard.Category != ShopCategory.Ship;
      return false;
    });
    this.SelectButton(this.allButton);
  }

  private void SelectPaints()
  {
    this.SelectedFilter = this.GetItemTypeFilter(ShopItemType.ShipPaint);
    this.SelectButton(this.paintButton);
  }

  private void SelectAvionics()
  {
    this.SelectedFilter = this.GetItemTypeFilter(ShopItemType.Avionics);
    this.SelectButton(this.avionicsButton);
  }

  private void SelectButton(GUIShopCategoryButton button)
  {
    using (List<GUIShopCategoryButton>.Enumerator enumerator = this.buttons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GUIShopCategoryButton current = enumerator.Current;
        current.Button.IsPressed = current == button;
      }
    }
  }

  public void SelectCategory(ShopCategory category, ShopItemType itemType)
  {
    switch (category)
    {
      case ShopCategory.Resource:
        this.SelectResources();
        break;
      case ShopCategory.Augment:
        this.SelectBooster();
        break;
      case ShopCategory.Consumable:
        this.SelectConsumables();
        break;
      case ShopCategory.System:
        switch (itemType)
        {
          case ShopItemType.Weapon:
          case ShopItemType.Missile:
            this.SelectWeapons();
            return;
          case ShopItemType.Computer:
            this.SelectComputer();
            return;
          case ShopItemType.Hull:
            this.SelectHull();
            return;
          case ShopItemType.Engine:
            this.SelectEngine();
            return;
          case ShopItemType.ShipPaint:
            this.SelectPaints();
            return;
          case ShopItemType.Avionics:
            this.SelectAvionics();
            return;
          default:
            return;
        }
      case ShopCategory.StarterKit:
        this.SelectSpecialOffers();
        break;
    }
  }

  private Predicate<ShipItem> GetItemTypeFilter(ShopItemType itemType)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return new Predicate<ShipItem>(new GUIShopTypeFilter.\u003CGetItemTypeFilter\u003Ec__AnonStoreyBB() { itemType = itemType }.\u003C\u003Em__19D);
  }

  private Predicate<ShipItem> GetItemTypeFilter(params ShopItemType[] itemTypes)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return new Predicate<ShipItem>(new GUIShopTypeFilter.\u003CGetItemTypeFilter\u003Ec__AnonStoreyBC() { itemTypes = itemTypes }.\u003C\u003Em__19E);
  }

  private Predicate<ShipItem> GetAllFilter()
  {
    return (Predicate<ShipItem>) (item => true);
  }

  public delegate void dHandler();
}
