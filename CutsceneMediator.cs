﻿// Decompiled with JetBrains decompiler
// Type: CutsceneMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class CutsceneMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  private bool showCutscenes = true;
  public new const string NAME = "CutsceneMediator";

  public CutsceneMediator()
    : base("CutsceneMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.CutscenePlayRequest);
    this.AddMessageInterest(Message.SettingChangedShowCutscenes);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.SettingChangedShowCutscenes:
        this.showCutscenes = (bool) message.Data;
        break;
      case Message.CutscenePlayRequest:
        this.TriggerCutscene((CutscenePlayRequestData) message.Data);
        break;
    }
  }

  private void TriggerCutscene(CutscenePlayRequestData playRequestData)
  {
    if (!this.showCutscenes)
      StoryProtocol.GetProtocol().CutsceneFinished();
    else
      GameLevel.Instance.CutsceneManager.EnqueueCutscene(playRequestData);
  }
}
