﻿// Decompiled with JetBrains decompiler
// Type: AsteroidGroup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGroup : SpacePosition
{
  public const float GroupRadius = 300f;
  private readonly List<Asteroid> asteroids;
  private Asteroid representative;

  public override string ObjectName
  {
    get
    {
      return BsgoLocalization.Get("%$bgo.common.asteroid_group%");
    }
  }

  public int Count
  {
    get
    {
      return this.asteroids.Count;
    }
  }

  public Asteroid Representative
  {
    get
    {
      return this.representative;
    }
  }

  protected override sbyte IconIndex
  {
    get
    {
      return 30;
    }
  }

  public override SpaceEntityType SpaceEntityType
  {
    get
    {
      return SpaceEntityType.AsteroidGroup;
    }
  }

  public AsteroidGroup()
  {
    this.asteroids = new List<Asteroid>();
    this.MeshBoundsRaw = new Bounds(Vector3.zero, 300f * Vector3.one);
    this.Root.transform.parent = AsteroidGroupsManager.Instance.AsteroidGroupsRoot.transform;
  }

  public void AddAsteroid(Asteroid asteroid)
  {
    this.asteroids.Add(asteroid);
    this.UpdateRepresentative();
  }

  public void RemoveAsteroid(Asteroid asteroid)
  {
    if ((Object) SpaceLevel.GetLevel() == (Object) null)
      return;
    this.asteroids.Remove(asteroid);
    this.UpdateRepresentative();
    if (this.asteroids.Count != 0)
      return;
    SpaceLevel.GetLevel().GetSpacePositionRegistry().RemoveSpacePosition((SpacePosition) this);
  }

  private void UpdateRepresentative()
  {
    int count = this.asteroids.Count;
    if (count == 0)
      return;
    Vector3 zero = Vector3.zero;
    for (int index = 0; index < count; ++index)
      zero += this.asteroids[index].Position;
    Vector3 vector3 = zero / (float) count;
    float num = float.PositiveInfinity;
    using (List<Asteroid>.Enumerator enumerator = this.asteroids.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Asteroid current = enumerator.Current;
        float sqrMagnitude = (current.Position - vector3).sqrMagnitude;
        if ((double) sqrMagnitude < (double) num)
        {
          num = sqrMagnitude;
          this.representative = current;
          this.Position = current.Position;
        }
      }
    }
  }
}
