﻿// Decompiled with JetBrains decompiler
// Type: TurnByPitchYawStrikes
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class TurnByPitchYawStrikes : Maneuver
{
  private Vector3 pitchYawFactor;
  private Vector2 strafeDirection;
  private float strafeMagnitude;

  public TurnByPitchYawStrikes()
  {
  }

  public TurnByPitchYawStrikes(Vector3 pitchYawFactor, Vector2 strafeDirection, float strafeMagnitude, MovementOptions movementOptions)
  {
    this.pitchYawFactor = pitchYawFactor;
    this.strafeDirection = strafeDirection;
    this.strafeMagnitude = strafeMagnitude;
    this.options = movementOptions;
  }

  public override MovementFrame NextFrame(Tick tick, MovementFrame prevFrame)
  {
    return Simulation.TurnByPitchYawStrikes(prevFrame, this.pitchYawFactor, (Vector3) this.strafeDirection, this.strafeMagnitude, this.options);
  }

  public override void Read(BgoProtocolReader pr)
  {
    base.Read(pr);
    this.startTick = pr.ReadTick();
    this.pitchYawFactor = pr.ReadVector3();
    this.strafeDirection = pr.ReadVector2();
    this.strafeMagnitude = pr.ReadSingle();
    this.options.Read(pr);
  }

  public override string ToString()
  {
    return string.Format("PitchYawFactor: {0}, StrafeDirection: {1}, StrafeMagnitude: {2}", (object) this.pitchYawFactor, (object) this.strafeDirection, (object) this.strafeMagnitude);
  }
}
