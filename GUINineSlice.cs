﻿// Decompiled with JetBrains decompiler
// Type: GUINineSlice
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUINineSlice : GUIPanel
{
  public const int NORTH_WEST = 0;
  public const int NORTH = 1;
  public const int NORTH_EAST = 2;
  public const int EAST = 3;
  public const int SOUTH_EAST = 4;
  public const int SOUTH = 5;
  public const int SOUTH_WEST = 6;
  public const int WEST = 7;
  public const int CENTER = 8;
  private readonly Texture2D nineSliceImage;
  private Rect[] sourceRects;
  private readonly GUIImageNew[] tiles;

  public Rect[] SourceRects
  {
    get
    {
      return this.sourceRects;
    }
    set
    {
      this.sourceRects = value;
    }
  }

  public override float2 Size
  {
    get
    {
      return base.Size;
    }
    set
    {
      base.Size = value;
      this.RearrangeTiles();
    }
  }

  public GUINineSlice(Texture2D nineSliceImage)
  {
    this.nineSliceImage = nineSliceImage;
    this.SourceRects = new Rect[9];
    this.sourceRects[6] = new Rect(0.0f, 0.0f, 32f, 32f);
    this.sourceRects[5] = new Rect(30f, 0.0f, 4f, 32f);
    this.sourceRects[4] = new Rect((float) (nineSliceImage.width - 32), 0.0f, 32f, 32f);
    this.sourceRects[3] = new Rect((float) (nineSliceImage.width - 32), 30f, 32f, 4f);
    this.sourceRects[2] = new Rect((float) (nineSliceImage.width - 32), (float) (nineSliceImage.height - 32), 32f, 32f);
    this.sourceRects[1] = new Rect(30f, (float) (nineSliceImage.height - 32), 4f, 32f);
    this.sourceRects[0] = new Rect(0.0f, (float) (nineSliceImage.height - 32), 32f, 32f);
    this.sourceRects[7] = new Rect(0.0f, 30f, 32f, 4f);
    this.sourceRects[8] = new Rect(30f, 30f, 4f, 4f);
    this.tiles = new GUIImageNew[this.SourceRects.Length];
    for (int index = 0; index < this.tiles.Length; ++index)
    {
      this.tiles[index] = new GUIImageNew(nineSliceImage);
      this.AddPanel((GUIPanel) this.tiles[index]);
    }
    this.Size = new float2(this.Rect.width, this.Rect.height);
  }

  private void RearrangeTiles()
  {
    this.tiles[0].Rect = new Rect(0.0f, 0.0f, this.sourceRects[0].width, this.sourceRects[0].height);
    this.tiles[2].Size = this.GetSizeCorner(this.sourceRects[2]);
    this.tiles[1].Size = new float2(this.Width - this.sourceRects[0].width - this.sourceRects[2].width, this.sourceRects[1].height);
    this.tiles[1].PlaceRightOf((GUIPanel) this.tiles[0]);
    this.tiles[2].PlaceRightOf((GUIPanel) this.tiles[1]);
    this.tiles[7].Size = new float2(this.sourceRects[7].width, this.Height - this.sourceRects[0].height - this.sourceRects[6].height);
    this.tiles[7].PlaceBelowOf((GUIPanel) this.tiles[0]);
    this.tiles[3].Size = new float2(this.sourceRects[3].width, this.Height - this.sourceRects[0].height - this.sourceRects[6].height);
    this.tiles[3].PlaceBelowOf((GUIPanel) this.tiles[2]);
    this.tiles[8].Size = new float2(this.Width - this.sourceRects[7].width - this.sourceRects[3].width, this.Height - this.sourceRects[1].height - this.sourceRects[5].height);
    this.tiles[8].PlaceRightOf((GUIPanel) this.tiles[7]);
    this.tiles[6].Rect = new Rect(0.0f, this.Height - this.sourceRects[6].height, this.sourceRects[6].width, this.sourceRects[6].height);
    this.tiles[4].Size = this.GetSizeCorner(this.sourceRects[4]);
    this.tiles[5].Size = new float2(this.Width - this.sourceRects[6].width - this.sourceRects[4].width, this.sourceRects[5].height);
    this.tiles[5].PlaceRightOf((GUIPanel) this.tiles[6]);
    this.tiles[4].PlaceRightOf((GUIPanel) this.tiles[5]);
    for (int index = 0; index < this.sourceRects.Length; ++index)
      this.tiles[index].InnerRect = this.GetInnerRect(this.sourceRects[index]);
  }

  private float2 GetSizeCorner(Rect rect)
  {
    return new float2(rect.width, rect.height);
  }

  private Rect GetInnerRect(Rect sourceRect)
  {
    Rect rect = sourceRect;
    rect.x /= (float) this.nineSliceImage.width;
    rect.y /= (float) this.nineSliceImage.height;
    rect.width /= (float) this.nineSliceImage.width;
    rect.height /= (float) this.nineSliceImage.height;
    return rect;
  }
}
