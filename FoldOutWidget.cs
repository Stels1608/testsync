﻿// Decompiled with JetBrains decompiler
// Type: FoldOutWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class FoldOutWidget : NguiWidget, ILocalizeable
{
  private const float ROTATION = 90f;
  public GameObject foldOutContent;
  public UISprite background;
  public UISprite contentBackground;
  [SerializeField]
  public UISprite collapsedIcon;
  [SerializeField]
  public UISprite expandedIcon;
  public UILabel groupname;
  public UITable parentTable;
  private bool contentShown;

  public override void Start()
  {
    base.Start();
    this.ReloadLanguageData();
    if ((Object) null != (Object) this.contentBackground)
      this.contentBackground.gameObject.SetActive(false);
    this.ShowContent(false);
  }

  public void OnEnable()
  {
    this.ShowContent(this.contentShown);
  }

  public override void OnClick()
  {
    this.ShowContent(!this.contentShown);
  }

  public void ShowContent(bool show)
  {
    this.contentShown = show;
    this.foldOutContent.SetActive(show);
    this.background.spriteName = !show ? "list_btn_normal" : "list_btn_activ";
    if ((Object) null != (Object) this.expandedIcon)
    {
      this.collapsedIcon.gameObject.SetActive(!show);
      this.expandedIcon.gameObject.SetActive(show);
    }
    else if ((Object) null != (Object) this.collapsedIcon)
      this.collapsedIcon.transform.localRotation = new Quaternion()
      {
        eulerAngles = new Vector3(0.0f, 0.0f, !this.contentShown ? 90f : 0.0f)
      };
    UITable component = this.foldOutContent.GetComponent<UITable>();
    if ((Object) null != (Object) component)
      component.Reposition();
    else
      Debug.Log((object) "UI TABLE WAS NOT FOUND");
    if ((Object) null != (Object) this.contentBackground)
    {
      this.contentBackground.gameObject.SetActive(show);
      if (show)
        this.contentBackground.height = (int) PositionUtils.GetScreenPixelSizeUnscaled(this.foldOutContent).y + 6;
    }
    if (!((Object) null != (Object) this.parentTable))
      return;
    this.Invoke("Reposition", 0.02f);
  }

  private void Reposition()
  {
    this.parentTable.Reposition();
  }

  public void ReloadLanguageData()
  {
    this.groupname.text = Tools.ParseMessage(this.groupname.text);
  }
}
