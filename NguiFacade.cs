﻿// Decompiled with JetBrains decompiler
// Type: NguiFacade
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc;
using Bigpoint.Core.Mvc.Model;
using Bigpoint.Core.Mvc.View;

public class NguiFacade : Facade<Message>
{
  public NguiFacade()
  {
    this.AttachActionForMessage<InitUiAction>(Message.InitUi);
    this.AttachActionForMessage<SetFullScreenAction>(Message.SetFullscreen);
    this.AttachActionForMessage<ChangeSettingAction>(Message.ChangeSetting);
    this.AttachActionForMessage<ApplySettingsAction>(Message.ApplySettings);
    this.AttachActionForMessage<InjectSettingsAction>(Message.RequestSettings);
    this.AttachActionForMessage<RegisterSettingsReceiverAction>(Message.RegisterSettingsReceiver);
    this.AttachActionForMessage<UnregisterSettingsReceiverAction>(Message.UnregisterSettingsReceiver);
    this.AttachActionForMessage<ChangeInputHandlingAction>(Message.ChangeInput);
    this.AttachActionForMessage<LoadSettingsAction>(Message.LoadSettings);
    this.AttachActionForMessage<LoadInputBindingsAction>(Message.LoadInputBindings);
    this.AttachActionForMessage<SaveSettingsAction>(Message.SaveSettings);
    this.AttachActionForMessage<AutoSubscribeAction>(Message.AutoSubscribe);
    this.AttachActionForMessage<LogToEventStreamAction>(Message.LogToEventStream);
    this.AttachActionForMessage<InputBindingDataChangedAction>(Message.InputBindingDataChanged);
    this.AttachActionForMessage<LevelLoadAction>(Message.LoadNewLevel);
    this.AttachActionForMessage<EntityRemovedAction>(Message.EntityRemoved);
    this.AttachActionForMessage<TargetSelectionChangeAction>(Message.SelectTargetRequest);
    this.AttachActionForMessage<TargetSelectionEnableAction>(Message.EnableTargetting);
    this.AttachActionForMessage<TargetPassedMapRangeAction>(Message.TargetPassedMapRange);
    this.AttachActionForMessage<TargetDesignationChangeAction>(Message.TargetDesignationChangeRequest);
    this.AttachActionForMessage<TargetWaypointChangeAction>(Message.TargetWaypointStatusChangeRequest);
    this.AttachActionForMessage<AutoAbilityToggledAction>(Message.AutoAbilityToggled);
    this.AttachActionForMessage<SpawnFlareAction>(Message.FlareReleased);
    this.AttachActionForMessage<BuyEventItemAction>(Message.EventShopBuy);
    this.AttachActionForMessage<PlayerVisibilityChangedAction>(Message.PlayerShipSpawnStateChanged);
    this.AttachActionForMessage<IngameLevelStartedAction>(Message.LevelUiInstalled);
    this.AttachActionForMessage<AddIngameLevelStartedCommandAction>(Message.ExecuteWhenIngame);
    this.AttachActionForMessage<Play2dSoundAction>(Message.Play2dSound);
    this.AttachActionForMessage<PlayAmbientSoundAction>(Message.PlayAmbientSound);
    this.AttachActionForMessage<StopAmbientSoundAction>(Message.StopAmbientSound);
    this.AttachActionForMessage<NameChangeRequestAction>(Message.RequestNameChange);
    this.AttachActionForMessage<FactionSwitchRequestAction>(Message.RequestFactionSwitch);
    this.AttachActionForMessage<CharacterDeletionRequestAction>(Message.RequestCharacterDeletion);
    this.AttachActionForMessage<AvatarChangeRequestAction>(Message.RequestAvatarChange);
    this.AttachDataProvider((IDataProvider<Message>) new SettingsDataProvider());
    this.AttachDataProvider((IDataProvider<Message>) new TargetSelectionDataProvider());
    this.AttachDataProvider((IDataProvider<Message>) new GuiDataProvider());
    this.AttachDataProvider((IDataProvider<Message>) new InputDataProvider());
    this.AttachDataProvider((IDataProvider<Message>) new ShopDataProvider());
    this.AttachDataProvider((IDataProvider<Message>) new CharacterServiceDataProvider());
    this.AttachDataProvider((IDataProvider<Message>) new IngameLevelStartedCommandsDataProvider());
    this.AttachDataProvider((IDataProvider<Message>) new LeaderboardDataProvider());
    this.AttachView((IView<Message>) new LeaderboardMediator());
    this.AttachView((IView<Message>) new GuiMediator());
    this.AttachView((IView<Message>) new ChatMediator());
    this.AttachView((IView<Message>) new GuiTickerMediator());
    this.AttachView((IView<Message>) new NotificationMediator());
    this.AttachView((IView<Message>) new TutorialMediator());
    this.AttachView((IView<Message>) new GuildMediator());
    this.AttachView((IView<Message>) new TournamentMediator());
    this.AttachView((IView<Message>) new ShipShopMediator());
    this.AttachView((IView<Message>) new EventShopMediator());
    this.AttachView((IView<Message>) new OptionsMediator());
    this.AttachView((IView<Message>) new HelpMediator());
    this.AttachView((IView<Message>) new WoFMediator());
    this.AttachView((IView<Message>) new TooltipMediator());
    this.AttachView((IView<Message>) new CameraFxMediator());
    this.AttachView((IView<Message>) new SpaceObjectMediator());
    this.AttachView((IView<Message>) new SoundMediator());
    this.AttachView((IView<Message>) new HudIndicatorMediator());
    this.AttachView((IView<Message>) new AccordeonSidebarRightMediator());
    this.AttachView((IView<Message>) new SectorEventMediator());
    this.AttachView((IView<Message>) new CamsHudMediator());
    this.AttachView((IView<Message>) new ShipControlsMediator());
    this.AttachView((IView<Message>) new SpaceCameraMediator());
    this.AttachView((IView<Message>) new MissionMediator());
    this.AttachView((IView<Message>) new ShopMediator());
    this.AttachView((IView<Message>) new HubMediator());
    this.AttachView((IView<Message>) new SpecialOfferMediator());
    this.AttachView((IView<Message>) new SystemButtonMediator());
    this.AttachView((IView<Message>) new AbandonMissionButtonMediator());
    this.AttachView((IView<Message>) new JumpMediator());
    this.AttachView((IView<Message>) new DialogMediator());
    this.AttachView((IView<Message>) new CutsceneTriggerEventMediator());
    this.AttachView((IView<Message>) new HudMediator());
    this.AttachView((IView<Message>) new StoryMissionLogMediator());
    this.AttachView((IView<Message>) new DamageOverlayMediator());
    this.AttachView((IView<Message>) new SystemMap3DMediator());
    this.AttachView((IView<Message>) new CutsceneMediator());
    this.AttachView((IView<Message>) new CharacterMediator());
    this.AttachView((IView<Message>) new GalaxyMapMediator());
    this.AttachView((IView<Message>) new ArenaMediator());
  }
}
