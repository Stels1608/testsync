﻿// Decompiled with JetBrains decompiler
// Type: GUIShopCategoryButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUIShopCategoryButton : GUIPanel
{
  private readonly GUIButtonNew button;
  private readonly GUIImageNew icon;
  private readonly GUIShopSubtypeFilter subtypeFilter;

  public GUIButtonNew Button
  {
    get
    {
      return this.button;
    }
  }

  public string IconPath
  {
    set
    {
      this.icon.Texture = ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/icons/" + value);
    }
  }

  public string Header
  {
    set
    {
      this.subtypeFilter.HeaderLabel.Text = value;
    }
  }

  public GUIShopCategoryButton(string iconPath, AnonymousDelegate filterDelegate, GUIShopSubtypeFilter subtypeFilter = null)
  {
    this.subtypeFilter = subtypeFilter;
    this.button = new GUIButtonNew();
    this.Button.TextLabel.AutoSize = false;
    this.Button.Text = string.Empty;
    this.Button.Size = new float2(45f, 42f);
    this.Button.PositionX = 2f;
    this.Button.Handler = filterDelegate;
    this.icon = new GUIImageNew(ResourceLoader.Load<Texture2D>("GUI/EquipBuyPanel/icons/" + iconPath));
    this.icon.Size = new float2(35f, 35f);
    this.icon.PositionX = 1f;
    this.Size = new float2(44f, 46f);
    this.IsRendered = true;
    this.AddPanel((GUIPanel) this.Button);
    this.AddPanel((GUIPanel) this.icon);
    if (subtypeFilter == null)
      return;
    subtypeFilter.IsRendered = false;
    float2 position = this.Button.Position;
    position.x += this.Button.Width * 0.5f;
    position.y -= subtypeFilter.Height * 0.5f;
    subtypeFilter.Rect = new Rect(position.x, position.y, subtypeFilter.Width, subtypeFilter.Height);
    this.AddPanel((GUIPanel) subtypeFilter);
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    base.OnMouseMove(mousePosition);
    if (this.subtypeFilter == null)
      return;
    if (this.subtypeFilter.IsRendered)
      this.subtypeFilter.IsRendered = this.Button.Contains(mousePosition) || this.subtypeFilter.Contains(mousePosition);
    else
      this.subtypeFilter.IsRendered = this.Button.Contains(mousePosition);
  }
}
