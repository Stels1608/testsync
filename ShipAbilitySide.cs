﻿// Decompiled with JetBrains decompiler
// Type: ShipAbilitySide
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum ShipAbilitySide : byte
{
  Self = 1,
  Any = 2,
  Neutral = 4,
  Friend = 8,
  Enemy = 16,
}
