﻿// Decompiled with JetBrains decompiler
// Type: DialogProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class DialogProtocol : BgoProtocol
{
  public DialogProtocol()
    : base(BgoProtocol.ProtocolID.Dialog)
  {
  }

  public void Say(int index)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 0);
    bw.Write((byte) index);
    this.SendMessage(bw);
  }

  public void Advance()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    this.SendMessage(bw);
  }

  public void Stop()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 2);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    DialogProtocol.Reply reply = (DialogProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case DialogProtocol.Reply.NpcRemark:
        FacadeFactory.GetInstance().SendMessage(Message.DialogReplyNpcRemark, (object) br.ReadDesc<Remark>());
        break;
      case DialogProtocol.Reply.PcRemarks:
        FacadeFactory.GetInstance().SendMessage(Message.DialogReplyPcRemarks, (object) br.ReadDescList<Remark>());
        break;
      case DialogProtocol.Reply.Stopped:
        FacadeFactory.GetInstance().SendMessage(Message.DialogReplyStopped);
        break;
      case DialogProtocol.Reply.Action:
        FacadeFactory.GetInstance().SendMessage(Message.DialogReplyAction, (object) (DialogAction) br.ReadByte());
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Hub protocol: " + (object) reply);
        break;
    }
  }

  public enum Reply : ushort
  {
    NpcRemark,
    PcRemarks,
    Stopped,
    Action,
  }

  public enum Request : ushort
  {
    Say,
    Advance,
    Stop,
  }
}
