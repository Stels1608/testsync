﻿// Decompiled with JetBrains decompiler
// Type: JsonFormatter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Text;

public class JsonFormatter
{
  private IndentWriter _writer = new IndentWriter();
  private StringBuilder _currentLine = new StringBuilder();
  private char lastChar = ' ';
  private StringWalker _walker;
  private bool _quoted;

  public JsonFormatter(string json)
  {
    this._walker = new StringWalker(json);
    this.ResetLine();
  }

  public void ResetLine()
  {
    this._currentLine.Length = 0;
  }

  public string Format()
  {
    while (this.MoveNextChar())
    {
      if (!this._quoted && this.IsOpenBracket())
      {
        this.WriteCurrentLine();
        this.AddCharToLine();
        this.WriteCurrentLine();
        this._writer.Indent();
      }
      else if (!this._quoted && this.IsCloseBracket())
      {
        this.WriteCurrentLine();
        this._writer.UnIndent();
        this.AddCharToLine();
      }
      else if (!this._quoted && this.IsColon())
      {
        this.AddCharToLine();
        this.WriteCurrentLine();
      }
      else
        this.AddCharToLine();
    }
    this.WriteCurrentLine();
    return this._writer.ToString();
  }

  private bool MoveNextChar()
  {
    bool flag = this._walker.MoveNext();
    if (this.IsApostrophe())
      this._quoted = !this._quoted;
    this.lastChar = this._walker.CharAtIndex();
    return flag;
  }

  public bool IsApostrophe()
  {
    if ((int) this._walker.CharAtIndex() == 34)
      return (int) this.lastChar != 92;
    return false;
  }

  public bool IsOpenBracket()
  {
    if ((int) this._walker.CharAtIndex() != 123)
      return (int) this._walker.CharAtIndex() == 91;
    return true;
  }

  public bool IsCloseBracket()
  {
    if ((int) this._walker.CharAtIndex() != 125)
      return (int) this._walker.CharAtIndex() == 93;
    return true;
  }

  public bool IsColon()
  {
    return (int) this._walker.CharAtIndex() == 44;
  }

  private void AddCharToLine()
  {
    this._currentLine.Append(this._walker.CharAtIndex());
  }

  private void WriteCurrentLine()
  {
    string line = this._currentLine.ToString().Trim();
    if (line.Length > 0)
      this._writer.WriteLine(line);
    this.ResetLine();
  }
}
