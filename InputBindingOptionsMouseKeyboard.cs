﻿// Decompiled with JetBrains decompiler
// Type: InputBindingOptionsMouseKeyboard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class InputBindingOptionsMouseKeyboard : InputBindingOptionsBase
{
  protected override void InitOptionsElements()
  {
    using (List<UserSetting>.Enumerator enumerator = new List<UserSetting>() { UserSetting.AdvancedFlightControls, UserSetting.InvertedVertical }.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        UserSetting current = enumerator.Current;
        OptionButtonElement buttonElement = NguiWidgetFactory.Options.CreateButtonElement(this.content);
        buttonElement.Init(current, (object) false);
        this.controls.Add(current, (OptionsElement) buttonElement);
      }
    }
    this.controls.Add(UserSetting.MouseWheelBinding, (OptionsElement) NGUITools.AddChild(this.content, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/Keybinding/KeyBindRadioBtnGroup")).GetComponent<OptionsRadioButtonGroup>());
    OptionsSliderElement sliderElement = NguiWidgetFactory.Options.CreateSliderElement(this.content);
    sliderElement.ShowPercentage = true;
    this.controls.Add(UserSetting.DeadZoneMouse, (OptionsElement) sliderElement);
    this.content.GetComponent<UITable>().Reposition();
  }

  internal void SetDogFightStatus(bool p)
  {
    if (!this.controls.ContainsKey(UserSetting.AdvancedFlightControls))
      return;
    this.controls[UserSetting.AdvancedFlightControls].IsEnabled = !p;
    this.content.GetComponent<UITable>().Reposition();
  }
}
