﻿// Decompiled with JetBrains decompiler
// Type: SingletonBehaviour`1
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
  private static T instance;

  protected virtual void Awake()
  {
    this.name = typeof (T).ToString();
  }

  public static T GetInstance()
  {
    if (!(bool) ((Object) SingletonBehaviour<T>.instance))
    {
      GameObject gameObject = new GameObject();
      Object.DontDestroyOnLoad((Object) gameObject);
      SingletonBehaviour<T>.instance = gameObject.AddComponent<T>();
    }
    return SingletonBehaviour<T>.instance;
  }

  private void Reset()
  {
    this.hideFlags = HideFlags.DontSave;
  }

  private void OnApplicationQuit()
  {
    if (!(bool) ((Object) SingletonBehaviour<T>.instance))
      return;
    Object.DestroyImmediate((Object) SingletonBehaviour<T>.instance.gameObject);
  }
}
