﻿// Decompiled with JetBrains decompiler
// Type: CharacterDeletionMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class CharacterDeletionMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "CharacterDeletionMediator";
  private CharacterDeletionDialog view;

  public CharacterDeletionMediator()
    : base("CharacterDeletionMediator")
  {
    this.view = this.CreateView();
    this.view.SetCharacterService((CharacterService) null);
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.CancelCharacterDeletion);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    if (message.Id != Message.CancelCharacterDeletion)
      return;
    this.OwnerFacade.DetachView("CharacterDeletionMediator");
  }

  public override void OnBeforeDetach()
  {
    if (!((Object) this.view != (Object) null))
      return;
    Object.Destroy((Object) this.view.gameObject);
  }

  private CharacterDeletionDialog CreateView()
  {
    return UguiTools.CreateChild<CharacterDeletionDialog>("CharacterMenu/DeleteDialog", UguiTools.GetCanvas(BsgoCanvas.CharacterMenu).transform);
  }
}
