﻿// Decompiled with JetBrains decompiler
// Type: PlayerSkill
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class PlayerSkill : IServerItem, ILoadable, IComparable<PlayerSkill>, IProtocolRead
{
  private ushort serverID;
  public SkillCard Card;
  private Flag isLoaded;

  public ushort ServerID
  {
    get
    {
      return this.serverID;
    }
  }

  public Flag IsLoaded
  {
    get
    {
      return this.isLoaded;
    }
  }

  public bool IsAvailable
  {
    get
    {
      if (!Game.Me.SkillBook.IsTrainingSkill && this.Card.NextCard != null && ((long) this.Card.NextCard.Price <= (long) Game.Me.FreeExperience && (int) this.Card.Level < (int) this.Card.MaxLevel))
        return this.IsLevelAvailable;
      return false;
    }
  }

  public bool IsLevelAvailable
  {
    get
    {
      if ((int) this.Card.RequireSkillHash == 0)
        return true;
      int skillLevel = Game.Me.GetSkillLevel(this.Card.RequireSkillHash);
      if (skillLevel != -1)
        return this.NextLevel <= skillLevel;
      return false;
    }
  }

  public string Name
  {
    get
    {
      if (this.Card == null)
        return string.Empty;
      if (this.Card.NextCard != null)
        return this.Card.NextCard.GUICard.Name;
      return this.Card.GUICard.Name;
    }
  }

  public int Cost
  {
    get
    {
      return this.Card.NextCard.Price;
    }
  }

  public int TrainingTime
  {
    get
    {
      return (int) ((double) this.Card.NextCard.TrainingTime / 60.0);
    }
  }

  public int NextLevel
  {
    get
    {
      return (int) this.Card.NextCard.Level;
    }
  }

  public int Level
  {
    get
    {
      return (int) this.Card.Level;
    }
  }

  public string Description
  {
    get
    {
      return this.Card.NextCard.GUICard.Description;
    }
  }

  public bool IsMaxLevel
  {
    get
    {
      return this.Card.NextCard == null;
    }
  }

  public PlayerSkill()
  {
    this.isLoaded = new Flag();
  }

  public void Read(BgoProtocolReader r)
  {
    this.serverID = r.ReadUInt16();
    this.Card = (SkillCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.Skill);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.Card
    });
    this.IsLoaded.Set();
  }

  public int CompareTo(PlayerSkill other)
  {
    return this.Card.SortWeight - other.Card.SortWeight;
  }
}
