﻿// Decompiled with JetBrains decompiler
// Type: ChatKickBan
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

internal class ChatKickBan : ChatProtocolParser
{
  public string reason = string.Empty;

  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split(new char[3]{ '%', '@', '#' });
    if (strArray.Length != 3)
      return false;
    if (strArray[0] == "at")
    {
      this.type = ChatProtocolParserType.BannUserWithReason;
    }
    else
    {
      if (!(strArray[0] == "as"))
        return false;
      this.type = ChatProtocolParserType.KickUserWithReason;
    }
    this.reason = strArray[1];
    return true;
  }
}
