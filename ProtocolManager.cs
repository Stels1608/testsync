﻿// Decompiled with JetBrains decompiler
// Type: ProtocolManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class ProtocolManager
{
  private readonly Connection connection;
  private readonly Dictionary<BgoProtocol.ProtocolID, BgoProtocol> protocols;
  private string host;

  public string Host
  {
    get
    {
      return this.host;
    }
  }

  public ProtocolManager()
  {
    this.connection = new Connection();
    this.connection.OnConnectionDropped += new Connection.ConnectionCallback(this.OnConnectionDropped);
    this.protocols = new Dictionary<BgoProtocol.ProtocolID, BgoProtocol>();
    this.RegisterProtocol((BgoProtocol) new LoginProtocol());
    this.RegisterProtocol((BgoProtocol) new PlayerProtocol());
    this.RegisterProtocol((BgoProtocol) new SettingProtocol());
    this.RegisterProtocol((BgoProtocol) new DebugProtocol());
    this.RegisterProtocol((BgoProtocol) new SceneProtocol());
    this.RegisterProtocol((BgoProtocol) new CommunityProtocol());
    this.RegisterProtocol((BgoProtocol) new NotificationProtocol());
    this.RegisterProtocol((BgoProtocol) new SubscribeProtocol());
    this.RegisterProtocol((BgoProtocol) new ArenaProtocol());
    this.RegisterProtocol((BgoProtocol) new GameProtocol());
    this.RegisterProtocol((BgoProtocol) new StoryProtocol());
    this.RegisterProtocol((BgoProtocol) new RoomProtocol());
    this.RegisterProtocol((BgoProtocol) new RankingProtocol());
    this.RegisterProtocol((BgoProtocol) new FeedbackProtocol());
    this.RegisterProtocol((BgoProtocol) new TournamentProtocol());
    this.RegisterProtocol((BgoProtocol) new ShopProtocol());
    this.RegisterProtocol((BgoProtocol) new WofProtocol());
  }

  public void Update()
  {
    if (this.GetConnectionState() != Connection.State.Connected)
      return;
    using (List<BgoProtocolReader>.Enumerator enumerator = this.connection.RecvMessage().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BgoProtocolReader current = enumerator.Current;
        BgoProtocol.ProtocolID protoID = (BgoProtocol.ProtocolID) current.ReadByte();
        ushort msgType = current.ReadUInt16();
        try
        {
          this.GetProtocol(protoID).ReceiveMessage(msgType, current);
        }
        catch (Exception ex)
        {
          string str = "Couldn't handle message for " + (object) protoID + " Protocol (msgType:" + (object) msgType + "). ";
          if (this.GetProtocol(protoID) == null)
            str = str + (object) protoID + " Protocol is not (any more) registered. ";
          Debug.LogError((object) (str + "Caught Exception: " + ex.ToString()));
        }
      }
    }
  }

  public void LateUpdate()
  {
    if (this.GetConnectionState() != Connection.State.Connected)
      return;
    this.UpdateMessage();
  }

  public Connection.State GetConnectionState()
  {
    return this.connection.GetState();
  }

  public Connection DebugGetConnection()
  {
    return this.connection;
  }

  public Dictionary<BgoProtocol.ProtocolID, BgoProtocol> DebugGetProtocols()
  {
    return this.protocols;
  }

  private void OnConnectionDropped(string message)
  {
    Log.Info((object) ("CONNECTION DROPPED: " + message));
    this.protocols[BgoProtocol.ProtocolID.Login].SetEnabled(true);
    this.protocols[BgoProtocol.ProtocolID.Game].SetEnabled(false);
    Game.OnDisconnect(message);
  }

  public void ConnectTo(string ip, string port)
  {
    if (this.connection.GetState() != Connection.State.Disconnected)
      return;
    this.host = ip;
    this.connection.Connect(ip, port);
  }

  public void Disconnect(string message)
  {
    this.connection.Disconnect(message);
    Application.Quit();
    Game.QuitLogout();
  }

  public void RegisterProtocol(BgoProtocol protocol)
  {
    protocol.SetConnection(this.connection);
    this.protocols.Add(protocol.protocolID, protocol);
  }

  public void UnregisterProtocol(BgoProtocol.ProtocolID protocolId)
  {
    this.protocols.Remove(protocolId);
  }

  public void UnregisterProtocol(BgoProtocol protocol)
  {
    this.protocols.Remove(protocol.protocolID);
  }

  public BgoProtocol GetProtocol(BgoProtocol.ProtocolID protoID)
  {
    if (!this.protocols.ContainsKey(protoID))
      return (BgoProtocol) null;
    return this.protocols[protoID];
  }

  private void UpdateMessage()
  {
    using (Dictionary<BgoProtocol.ProtocolID, BgoProtocol>.ValueCollection.Enumerator enumerator = this.protocols.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        BgoProtocol current = enumerator.Current;
        if (current != null && current.IsEnabled())
          current.UpdateMessage();
      }
    }
  }
}
