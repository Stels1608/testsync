﻿// Decompiled with JetBrains decompiler
// Type: PreloaderGUI
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class PreloaderGUI : MonoBehaviour
{
  public PreloaderState State = new PreloaderState();
  public TransSceneType TransSceneType;
  private LoadingSceneWindow loadingSceneWindow;
  private float screenWidth;
  private float screenHeight;
  private GUIStyle promptStyle;

  private void Start()
  {
    this.loadingSceneWindow = new LoadingSceneWindow(this.TransSceneType);
    this.loadingSceneWindow.IsRendered = true;
    this.loadingSceneWindow.RecalculateAbsCoords();
    this.promptStyle = new GUIStyle();
    this.promptStyle.clipping = TextClipping.Overflow;
    this.promptStyle.normal.textColor = Color.white;
  }

  private void Update()
  {
    this.loadingSceneWindow.SetProgress(this.State.Progress, false);
    if ((double) this.screenWidth == (double) Screen.width && (double) this.screenHeight == (double) Screen.height)
      return;
    this.screenWidth = (float) Screen.width;
    this.screenHeight = (float) Screen.height;
    this.loadingSceneWindow.RecalculateAbsCoords();
  }

  private void OnGUI()
  {
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    this.loadingSceneWindow.Draw();
    GUI.Label(new Rect(0.0f, (float) (Screen.height - 20), 100f, 20f), this.State.Prompt, this.promptStyle);
  }

  private void OnDestroy()
  {
  }
}
