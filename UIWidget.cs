﻿// Decompiled with JetBrains decompiler
// Type: UIWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Diagnostics;
using UnityEngine;

[AddComponentMenu("NGUI/UI/NGUI Widget")]
[ExecuteInEditMode]
public class UIWidget : UIRect
{
  [HideInInspector]
  [SerializeField]
  protected Color mColor = Color.white;
  [SerializeField]
  [HideInInspector]
  protected UIWidget.Pivot mPivot = UIWidget.Pivot.Center;
  [SerializeField]
  [HideInInspector]
  protected int mWidth = 100;
  [HideInInspector]
  [SerializeField]
  protected int mHeight = 100;
  public float aspectRatio = 1f;
  [NonSerialized]
  public UIGeometry geometry = new UIGeometry();
  [NonSerialized]
  public bool fillGeometry = true;
  [NonSerialized]
  protected bool mPlayMode = true;
  [NonSerialized]
  protected Vector4 mDrawRegion = new Vector4(0.0f, 0.0f, 1f, 1f);
  [NonSerialized]
  private bool mIsVisibleByAlpha = true;
  [NonSerialized]
  private bool mIsVisibleByPanel = true;
  [NonSerialized]
  private bool mIsInFront = true;
  [NonSerialized]
  protected Vector3[] mCorners = new Vector3[4];
  [NonSerialized]
  private int mAlphaFrameID = -1;
  private int mMatrixFrame = -1;
  [SerializeField]
  [HideInInspector]
  protected int mDepth;
  public UIWidget.OnDimensionsChanged onChange;
  public UIWidget.OnPostFillCallback onPostFill;
  public UIDrawCall.OnRenderCallback mOnRender;
  public bool autoResizeBoxCollider;
  public bool hideIfOffScreen;
  public UIWidget.AspectRatioSource keepAspectRatio;
  public UIWidget.HitCheck hitCheck;
  [NonSerialized]
  public UIPanel panel;
  [NonSerialized]
  private Matrix4x4 mLocalToPanel;
  [NonSerialized]
  private float mLastAlpha;
  [NonSerialized]
  private bool mMoved;
  [NonSerialized]
  public UIDrawCall drawCall;
  private Vector3 mOldV0;
  private Vector3 mOldV1;

  public UIDrawCall.OnRenderCallback onRender
  {
    get
    {
      return this.mOnRender;
    }
    set
    {
      if (!((MulticastDelegate) this.mOnRender != (MulticastDelegate) value))
        return;
      if ((UnityEngine.Object) this.drawCall != (UnityEngine.Object) null && this.drawCall.onRender != null && this.mOnRender != null)
        this.drawCall.onRender -= this.mOnRender;
      this.mOnRender = value;
      if (!((UnityEngine.Object) this.drawCall != (UnityEngine.Object) null))
        return;
      this.drawCall.onRender += value;
    }
  }

  public Vector4 drawRegion
  {
    get
    {
      return this.mDrawRegion;
    }
    set
    {
      if (!(this.mDrawRegion != value))
        return;
      this.mDrawRegion = value;
      if (this.autoResizeBoxCollider)
        this.ResizeCollider();
      this.MarkAsChanged();
    }
  }

  public Vector2 pivotOffset
  {
    get
    {
      return NGUIMath.GetPivotOffset(this.pivot);
    }
  }

  public int width
  {
    get
    {
      return this.mWidth;
    }
    set
    {
      int minWidth = this.minWidth;
      if (value < minWidth)
        value = minWidth;
      if (this.mWidth == value || this.keepAspectRatio == UIWidget.AspectRatioSource.BasedOnHeight)
        return;
      if (this.isAnchoredHorizontally)
      {
        if ((UnityEngine.Object) this.leftAnchor.target != (UnityEngine.Object) null && (UnityEngine.Object) this.rightAnchor.target != (UnityEngine.Object) null)
        {
          if (this.mPivot == UIWidget.Pivot.BottomLeft || this.mPivot == UIWidget.Pivot.Left || this.mPivot == UIWidget.Pivot.TopLeft)
            NGUIMath.AdjustWidget(this, 0.0f, 0.0f, (float) (value - this.mWidth), 0.0f);
          else if (this.mPivot == UIWidget.Pivot.BottomRight || this.mPivot == UIWidget.Pivot.Right || this.mPivot == UIWidget.Pivot.TopRight)
          {
            NGUIMath.AdjustWidget(this, (float) (this.mWidth - value), 0.0f, 0.0f, 0.0f);
          }
          else
          {
            int num1 = value - this.mWidth;
            int num2 = num1 - (num1 & 1);
            if (num2 == 0)
              return;
            NGUIMath.AdjustWidget(this, (float) -num2 * 0.5f, 0.0f, (float) num2 * 0.5f, 0.0f);
          }
        }
        else if ((UnityEngine.Object) this.leftAnchor.target != (UnityEngine.Object) null)
          NGUIMath.AdjustWidget(this, 0.0f, 0.0f, (float) (value - this.mWidth), 0.0f);
        else
          NGUIMath.AdjustWidget(this, (float) (this.mWidth - value), 0.0f, 0.0f, 0.0f);
      }
      else
        this.SetDimensions(value, this.mHeight);
    }
  }

  public int height
  {
    get
    {
      return this.mHeight;
    }
    set
    {
      int minHeight = this.minHeight;
      if (value < minHeight)
        value = minHeight;
      if (this.mHeight == value || this.keepAspectRatio == UIWidget.AspectRatioSource.BasedOnWidth)
        return;
      if (this.isAnchoredVertically)
      {
        if ((UnityEngine.Object) this.bottomAnchor.target != (UnityEngine.Object) null && (UnityEngine.Object) this.topAnchor.target != (UnityEngine.Object) null)
        {
          if (this.mPivot == UIWidget.Pivot.BottomLeft || this.mPivot == UIWidget.Pivot.Bottom || this.mPivot == UIWidget.Pivot.BottomRight)
            NGUIMath.AdjustWidget(this, 0.0f, 0.0f, 0.0f, (float) (value - this.mHeight));
          else if (this.mPivot == UIWidget.Pivot.TopLeft || this.mPivot == UIWidget.Pivot.Top || this.mPivot == UIWidget.Pivot.TopRight)
          {
            NGUIMath.AdjustWidget(this, 0.0f, (float) (this.mHeight - value), 0.0f, 0.0f);
          }
          else
          {
            int num1 = value - this.mHeight;
            int num2 = num1 - (num1 & 1);
            if (num2 == 0)
              return;
            NGUIMath.AdjustWidget(this, 0.0f, (float) -num2 * 0.5f, 0.0f, (float) num2 * 0.5f);
          }
        }
        else if ((UnityEngine.Object) this.bottomAnchor.target != (UnityEngine.Object) null)
          NGUIMath.AdjustWidget(this, 0.0f, 0.0f, 0.0f, (float) (value - this.mHeight));
        else
          NGUIMath.AdjustWidget(this, 0.0f, (float) (this.mHeight - value), 0.0f, 0.0f);
      }
      else
        this.SetDimensions(this.mWidth, value);
    }
  }

  public Color color
  {
    get
    {
      return this.mColor;
    }
    set
    {
      if (!(this.mColor != value))
        return;
      bool includeChildren = (double) this.mColor.a != (double) value.a;
      this.mColor = value;
      this.Invalidate(includeChildren);
    }
  }

  public override float alpha
  {
    get
    {
      return this.mColor.a;
    }
    set
    {
      if ((double) this.mColor.a == (double) value)
        return;
      this.mColor.a = value;
      this.Invalidate(true);
    }
  }

  public bool isVisible
  {
    get
    {
      if (this.mIsVisibleByPanel && this.mIsVisibleByAlpha && (this.mIsInFront && (double) this.finalAlpha > 1.0 / 1000.0))
        return NGUITools.GetActive((Behaviour) this);
      return false;
    }
  }

  public bool hasVertices
  {
    get
    {
      if (this.geometry != null)
        return this.geometry.hasVertices;
      return false;
    }
  }

  public UIWidget.Pivot rawPivot
  {
    get
    {
      return this.mPivot;
    }
    set
    {
      if (this.mPivot == value)
        return;
      this.mPivot = value;
      if (this.autoResizeBoxCollider)
        this.ResizeCollider();
      this.MarkAsChanged();
    }
  }

  public UIWidget.Pivot pivot
  {
    get
    {
      return this.mPivot;
    }
    set
    {
      if (this.mPivot == value)
        return;
      Vector3 vector3_1 = this.worldCorners[0];
      this.mPivot = value;
      this.mChanged = true;
      Vector3 vector3_2 = this.worldCorners[0];
      Transform cachedTransform = this.cachedTransform;
      Vector3 vector3_3 = cachedTransform.position;
      float num = cachedTransform.localPosition.z;
      vector3_3.x += vector3_1.x - vector3_2.x;
      vector3_3.y += vector3_1.y - vector3_2.y;
      this.cachedTransform.position = vector3_3;
      vector3_3 = this.cachedTransform.localPosition;
      vector3_3.x = Mathf.Round(vector3_3.x);
      vector3_3.y = Mathf.Round(vector3_3.y);
      vector3_3.z = num;
      this.cachedTransform.localPosition = vector3_3;
    }
  }

  public int depth
  {
    get
    {
      return this.mDepth;
    }
    set
    {
      if (this.mDepth == value)
        return;
      if ((UnityEngine.Object) this.panel != (UnityEngine.Object) null)
        this.panel.RemoveWidget(this);
      this.mDepth = value;
      if (!((UnityEngine.Object) this.panel != (UnityEngine.Object) null))
        return;
      this.panel.AddWidget(this);
      if (Application.isPlaying)
        return;
      this.panel.SortWidgets();
      this.panel.RebuildAllDrawCalls();
    }
  }

  public int raycastDepth
  {
    get
    {
      if ((UnityEngine.Object) this.panel == (UnityEngine.Object) null)
        this.CreatePanel();
      if ((UnityEngine.Object) this.panel != (UnityEngine.Object) null)
        return this.mDepth + this.panel.depth * 1000;
      return this.mDepth;
    }
  }

  public override Vector3[] localCorners
  {
    get
    {
      Vector2 pivotOffset = this.pivotOffset;
      float x1 = -pivotOffset.x * (float) this.mWidth;
      float y1 = -pivotOffset.y * (float) this.mHeight;
      float x2 = x1 + (float) this.mWidth;
      float y2 = y1 + (float) this.mHeight;
      this.mCorners[0] = new Vector3(x1, y1);
      this.mCorners[1] = new Vector3(x1, y2);
      this.mCorners[2] = new Vector3(x2, y2);
      this.mCorners[3] = new Vector3(x2, y1);
      return this.mCorners;
    }
  }

  public virtual Vector2 localSize
  {
    get
    {
      Vector3[] localCorners = this.localCorners;
      return (Vector2) (localCorners[2] - localCorners[0]);
    }
  }

  public Vector3 localCenter
  {
    get
    {
      Vector3[] localCorners = this.localCorners;
      return Vector3.Lerp(localCorners[0], localCorners[2], 0.5f);
    }
  }

  public override Vector3[] worldCorners
  {
    get
    {
      Vector2 pivotOffset = this.pivotOffset;
      float x1 = -pivotOffset.x * (float) this.mWidth;
      float y1 = -pivotOffset.y * (float) this.mHeight;
      float x2 = x1 + (float) this.mWidth;
      float y2 = y1 + (float) this.mHeight;
      Transform cachedTransform = this.cachedTransform;
      this.mCorners[0] = cachedTransform.TransformPoint(x1, y1, 0.0f);
      this.mCorners[1] = cachedTransform.TransformPoint(x1, y2, 0.0f);
      this.mCorners[2] = cachedTransform.TransformPoint(x2, y2, 0.0f);
      this.mCorners[3] = cachedTransform.TransformPoint(x2, y1, 0.0f);
      return this.mCorners;
    }
  }

  public Vector3 worldCenter
  {
    get
    {
      return this.cachedTransform.TransformPoint(this.localCenter);
    }
  }

  public virtual Vector4 drawingDimensions
  {
    get
    {
      Vector2 pivotOffset = this.pivotOffset;
      float from1 = -pivotOffset.x * (float) this.mWidth;
      float from2 = -pivotOffset.y * (float) this.mHeight;
      float to1 = from1 + (float) this.mWidth;
      float to2 = from2 + (float) this.mHeight;
      return new Vector4((double) this.mDrawRegion.x != 0.0 ? Mathf.Lerp(from1, to1, this.mDrawRegion.x) : from1, (double) this.mDrawRegion.y != 0.0 ? Mathf.Lerp(from2, to2, this.mDrawRegion.y) : from2, (double) this.mDrawRegion.z != 1.0 ? Mathf.Lerp(from1, to1, this.mDrawRegion.z) : to1, (double) this.mDrawRegion.w != 1.0 ? Mathf.Lerp(from2, to2, this.mDrawRegion.w) : to2);
    }
  }

  public virtual Material material
  {
    get
    {
      return (Material) null;
    }
    set
    {
      throw new NotImplementedException(this.GetType().ToString() + " has no material setter");
    }
  }

  public virtual Texture mainTexture
  {
    get
    {
      Material material = this.material;
      if ((UnityEngine.Object) material != (UnityEngine.Object) null)
        return material.mainTexture;
      return (Texture) null;
    }
    set
    {
      throw new NotImplementedException(this.GetType().ToString() + " has no mainTexture setter");
    }
  }

  public virtual Shader shader
  {
    get
    {
      Material material = this.material;
      if ((UnityEngine.Object) material != (UnityEngine.Object) null)
        return material.shader;
      return (Shader) null;
    }
    set
    {
      throw new NotImplementedException(this.GetType().ToString() + " has no shader setter");
    }
  }

  [Obsolete("There is no relative scale anymore. Widgets now have width and height instead")]
  public Vector2 relativeSize
  {
    get
    {
      return Vector2.one;
    }
  }

  public bool hasBoxCollider
  {
    get
    {
      if ((UnityEngine.Object) (this.GetComponent<Collider>() as BoxCollider) != (UnityEngine.Object) null)
        return true;
      return (UnityEngine.Object) this.GetComponent<BoxCollider2D>() != (UnityEngine.Object) null;
    }
  }

  public virtual int minWidth
  {
    get
    {
      return 2;
    }
  }

  public virtual int minHeight
  {
    get
    {
      return 2;
    }
  }

  public virtual Vector4 border
  {
    get
    {
      return Vector4.zero;
    }
    set
    {
    }
  }

  public void SetDimensions(int w, int h)
  {
    if (this.mWidth == w && this.mHeight == h)
      return;
    this.mWidth = w;
    this.mHeight = h;
    if (this.keepAspectRatio == UIWidget.AspectRatioSource.BasedOnWidth)
      this.mHeight = Mathf.RoundToInt((float) this.mWidth / this.aspectRatio);
    else if (this.keepAspectRatio == UIWidget.AspectRatioSource.BasedOnHeight)
      this.mWidth = Mathf.RoundToInt((float) this.mHeight * this.aspectRatio);
    else if (this.keepAspectRatio == UIWidget.AspectRatioSource.Free)
      this.aspectRatio = (float) this.mWidth / (float) this.mHeight;
    this.mMoved = true;
    if (this.autoResizeBoxCollider)
      this.ResizeCollider();
    this.MarkAsChanged();
  }

  public override Vector3[] GetSides(Transform relativeTo)
  {
    Vector2 pivotOffset = this.pivotOffset;
    float x1 = -pivotOffset.x * (float) this.mWidth;
    float y1 = -pivotOffset.y * (float) this.mHeight;
    float x2 = x1 + (float) this.mWidth;
    float y2 = y1 + (float) this.mHeight;
    float x3 = (float) (((double) x1 + (double) x2) * 0.5);
    float y3 = (float) (((double) y1 + (double) y2) * 0.5);
    Transform cachedTransform = this.cachedTransform;
    this.mCorners[0] = cachedTransform.TransformPoint(x1, y3, 0.0f);
    this.mCorners[1] = cachedTransform.TransformPoint(x3, y2, 0.0f);
    this.mCorners[2] = cachedTransform.TransformPoint(x2, y3, 0.0f);
    this.mCorners[3] = cachedTransform.TransformPoint(x3, y1, 0.0f);
    if ((UnityEngine.Object) relativeTo != (UnityEngine.Object) null)
    {
      for (int index = 0; index < 4; ++index)
        this.mCorners[index] = relativeTo.InverseTransformPoint(this.mCorners[index]);
    }
    return this.mCorners;
  }

  public override float CalculateFinalAlpha(int frameID)
  {
    if (this.mAlphaFrameID != frameID)
    {
      this.mAlphaFrameID = frameID;
      this.UpdateFinalAlpha(frameID);
    }
    return this.finalAlpha;
  }

  protected void UpdateFinalAlpha(int frameID)
  {
    if (!this.mIsVisibleByAlpha || !this.mIsInFront)
      this.finalAlpha = 0.0f;
    else
      this.finalAlpha = !((UnityEngine.Object) this.parent != (UnityEngine.Object) null) ? this.mColor.a : this.parent.CalculateFinalAlpha(frameID) * this.mColor.a;
  }

  public override void Invalidate(bool includeChildren)
  {
    this.mChanged = true;
    this.mAlphaFrameID = -1;
    if (!((UnityEngine.Object) this.panel != (UnityEngine.Object) null))
      return;
    this.UpdateVisibility((double) this.CalculateCumulativeAlpha(Time.frameCount) > 1.0 / 1000.0, !this.hideIfOffScreen && !this.panel.hasCumulativeClipping || this.panel.IsVisible(this));
    this.UpdateFinalAlpha(Time.frameCount);
    if (!includeChildren)
      return;
    base.Invalidate(true);
  }

  public float CalculateCumulativeAlpha(int frameID)
  {
    UIRect parent = this.parent;
    if ((UnityEngine.Object) parent != (UnityEngine.Object) null)
      return parent.CalculateFinalAlpha(frameID) * this.mColor.a;
    return this.mColor.a;
  }

  public override void SetRect(float x, float y, float width, float height)
  {
    Vector2 pivotOffset = this.pivotOffset;
    float num1 = Mathf.Lerp(x, x + width, pivotOffset.x);
    float num2 = Mathf.Lerp(y, y + height, pivotOffset.y);
    int num3 = Mathf.FloorToInt(width + 0.5f);
    int num4 = Mathf.FloorToInt(height + 0.5f);
    if ((double) pivotOffset.x == 0.5)
      num3 = num3 >> 1 << 1;
    if ((double) pivotOffset.y == 0.5)
      num4 = num4 >> 1 << 1;
    Transform cachedTransform = this.cachedTransform;
    Vector3 localPosition = cachedTransform.localPosition;
    localPosition.x = Mathf.Floor(num1 + 0.5f);
    localPosition.y = Mathf.Floor(num2 + 0.5f);
    if (num3 < this.minWidth)
      num3 = this.minWidth;
    if (num4 < this.minHeight)
      num4 = this.minHeight;
    cachedTransform.localPosition = localPosition;
    this.width = num3;
    this.height = num4;
    if (!this.isAnchored)
      return;
    Transform parent = cachedTransform.parent;
    if ((bool) ((UnityEngine.Object) this.leftAnchor.target))
      this.leftAnchor.SetHorizontal(parent, x);
    if ((bool) ((UnityEngine.Object) this.rightAnchor.target))
      this.rightAnchor.SetHorizontal(parent, x + width);
    if ((bool) ((UnityEngine.Object) this.bottomAnchor.target))
      this.bottomAnchor.SetVertical(parent, y);
    if (!(bool) ((UnityEngine.Object) this.topAnchor.target))
      return;
    this.topAnchor.SetVertical(parent, y + height);
  }

  public void ResizeCollider()
  {
    if (!NGUITools.GetActive((Behaviour) this))
      return;
    NGUITools.UpdateWidgetCollider(this.gameObject);
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  public static int FullCompareFunc(UIWidget left, UIWidget right)
  {
    int num = UIPanel.CompareFunc(left.panel, right.panel);
    if (num == 0)
      return UIWidget.PanelCompareFunc(left, right);
    return num;
  }

  [DebuggerStepThrough]
  [DebuggerHidden]
  public static int PanelCompareFunc(UIWidget left, UIWidget right)
  {
    if (left.mDepth < right.mDepth)
      return -1;
    if (left.mDepth > right.mDepth)
      return 1;
    Material material1 = left.material;
    Material material2 = right.material;
    if ((UnityEngine.Object) material1 == (UnityEngine.Object) material2)
      return 0;
    return (UnityEngine.Object) material1 != (UnityEngine.Object) null || !((UnityEngine.Object) material2 != (UnityEngine.Object) null) && material1.GetInstanceID() < material2.GetInstanceID() ? -1 : 1;
  }

  public Bounds CalculateBounds()
  {
    return this.CalculateBounds((Transform) null);
  }

  public Bounds CalculateBounds(Transform relativeParent)
  {
    if ((UnityEngine.Object) relativeParent == (UnityEngine.Object) null)
    {
      Vector3[] localCorners = this.localCorners;
      Bounds bounds = new Bounds(localCorners[0], Vector3.zero);
      for (int index = 1; index < 4; ++index)
        bounds.Encapsulate(localCorners[index]);
      return bounds;
    }
    Matrix4x4 worldToLocalMatrix = relativeParent.worldToLocalMatrix;
    Vector3[] worldCorners = this.worldCorners;
    Bounds bounds1 = new Bounds(worldToLocalMatrix.MultiplyPoint3x4(worldCorners[0]), Vector3.zero);
    for (int index = 1; index < 4; ++index)
      bounds1.Encapsulate(worldToLocalMatrix.MultiplyPoint3x4(worldCorners[index]));
    return bounds1;
  }

  public void SetDirty()
  {
    if ((UnityEngine.Object) this.drawCall != (UnityEngine.Object) null)
    {
      this.drawCall.isDirty = true;
    }
    else
    {
      if (!this.isVisible || !this.hasVertices)
        return;
      this.CreatePanel();
    }
  }

  public void RemoveFromPanel()
  {
    if ((UnityEngine.Object) this.panel != (UnityEngine.Object) null)
    {
      this.panel.RemoveWidget(this);
      this.panel = (UIPanel) null;
    }
    this.drawCall = (UIDrawCall) null;
  }

  public virtual void MarkAsChanged()
  {
    if (!NGUITools.GetActive((Behaviour) this))
      return;
    this.mChanged = true;
    if (!((UnityEngine.Object) this.panel != (UnityEngine.Object) null) || !this.enabled || (!NGUITools.GetActive(this.gameObject) || this.mPlayMode))
      return;
    this.SetDirty();
    this.CheckLayer();
  }

  public UIPanel CreatePanel()
  {
    if (this.mStarted && (UnityEngine.Object) this.panel == (UnityEngine.Object) null && (this.enabled && NGUITools.GetActive(this.gameObject)))
    {
      this.panel = UIPanel.Find(this.cachedTransform, true, this.cachedGameObject.layer);
      if ((UnityEngine.Object) this.panel != (UnityEngine.Object) null)
      {
        this.mParentFound = false;
        this.panel.AddWidget(this);
        this.CheckLayer();
        this.Invalidate(true);
      }
    }
    return this.panel;
  }

  public void CheckLayer()
  {
    if (!((UnityEngine.Object) this.panel != (UnityEngine.Object) null) || this.panel.gameObject.layer == this.gameObject.layer)
      return;
    UnityEngine.Debug.LogWarning((object) "You can't place widgets on a layer different than the UIPanel that manages them.\nIf you want to move widgets to a different layer, parent them to a new panel instead.", (UnityEngine.Object) this);
    this.gameObject.layer = this.panel.gameObject.layer;
  }

  public override void ParentHasChanged()
  {
    base.ParentHasChanged();
    if (!((UnityEngine.Object) this.panel != (UnityEngine.Object) null) || !((UnityEngine.Object) this.panel != (UnityEngine.Object) UIPanel.Find(this.cachedTransform, true, this.cachedGameObject.layer)))
      return;
    this.RemoveFromPanel();
    this.CreatePanel();
  }

  protected virtual void Awake()
  {
    this.mGo = this.gameObject;
    this.mPlayMode = Application.isPlaying;
  }

  protected override void OnInit()
  {
    base.OnInit();
    this.RemoveFromPanel();
    this.mMoved = true;
    if (this.mWidth == 100 && this.mHeight == 100 && (double) this.cachedTransform.localScale.magnitude > 8.0)
    {
      this.UpgradeFrom265();
      this.cachedTransform.localScale = Vector3.one;
    }
    this.Update();
  }

  protected virtual void UpgradeFrom265()
  {
    Vector3 localScale = this.cachedTransform.localScale;
    this.mWidth = Mathf.Abs(Mathf.RoundToInt(localScale.x));
    this.mHeight = Mathf.Abs(Mathf.RoundToInt(localScale.y));
    NGUITools.UpdateWidgetCollider(this.gameObject, true);
  }

  protected override void OnStart()
  {
    this.CreatePanel();
  }

  protected override void OnAnchor()
  {
    Transform cachedTransform = this.cachedTransform;
    Transform parent = cachedTransform.parent;
    Vector3 localPosition = cachedTransform.localPosition;
    Vector2 pivotOffset = this.pivotOffset;
    float from1;
    float to1;
    float from2;
    float to2;
    if ((UnityEngine.Object) this.leftAnchor.target == (UnityEngine.Object) this.bottomAnchor.target && (UnityEngine.Object) this.leftAnchor.target == (UnityEngine.Object) this.rightAnchor.target && (UnityEngine.Object) this.leftAnchor.target == (UnityEngine.Object) this.topAnchor.target)
    {
      Vector3[] sides = this.leftAnchor.GetSides(parent);
      if (sides != null)
      {
        from1 = NGUIMath.Lerp(sides[0].x, sides[2].x, this.leftAnchor.relative) + (float) this.leftAnchor.absolute;
        to1 = NGUIMath.Lerp(sides[0].x, sides[2].x, this.rightAnchor.relative) + (float) this.rightAnchor.absolute;
        from2 = NGUIMath.Lerp(sides[3].y, sides[1].y, this.bottomAnchor.relative) + (float) this.bottomAnchor.absolute;
        to2 = NGUIMath.Lerp(sides[3].y, sides[1].y, this.topAnchor.relative) + (float) this.topAnchor.absolute;
        this.mIsInFront = true;
      }
      else
      {
        Vector3 localPos = this.GetLocalPos(this.leftAnchor, parent);
        from1 = localPos.x + (float) this.leftAnchor.absolute;
        from2 = localPos.y + (float) this.bottomAnchor.absolute;
        to1 = localPos.x + (float) this.rightAnchor.absolute;
        to2 = localPos.y + (float) this.topAnchor.absolute;
        this.mIsInFront = !this.hideIfOffScreen || (double) localPos.z >= 0.0;
      }
    }
    else
    {
      this.mIsInFront = true;
      if ((bool) ((UnityEngine.Object) this.leftAnchor.target))
      {
        Vector3[] sides = this.leftAnchor.GetSides(parent);
        from1 = sides == null ? this.GetLocalPos(this.leftAnchor, parent).x + (float) this.leftAnchor.absolute : NGUIMath.Lerp(sides[0].x, sides[2].x, this.leftAnchor.relative) + (float) this.leftAnchor.absolute;
      }
      else
        from1 = localPosition.x - pivotOffset.x * (float) this.mWidth;
      if ((bool) ((UnityEngine.Object) this.rightAnchor.target))
      {
        Vector3[] sides = this.rightAnchor.GetSides(parent);
        to1 = sides == null ? this.GetLocalPos(this.rightAnchor, parent).x + (float) this.rightAnchor.absolute : NGUIMath.Lerp(sides[0].x, sides[2].x, this.rightAnchor.relative) + (float) this.rightAnchor.absolute;
      }
      else
        to1 = localPosition.x - pivotOffset.x * (float) this.mWidth + (float) this.mWidth;
      if ((bool) ((UnityEngine.Object) this.bottomAnchor.target))
      {
        Vector3[] sides = this.bottomAnchor.GetSides(parent);
        from2 = sides == null ? this.GetLocalPos(this.bottomAnchor, parent).y + (float) this.bottomAnchor.absolute : NGUIMath.Lerp(sides[3].y, sides[1].y, this.bottomAnchor.relative) + (float) this.bottomAnchor.absolute;
      }
      else
        from2 = localPosition.y - pivotOffset.y * (float) this.mHeight;
      if ((bool) ((UnityEngine.Object) this.topAnchor.target))
      {
        Vector3[] sides = this.topAnchor.GetSides(parent);
        to2 = sides == null ? this.GetLocalPos(this.topAnchor, parent).y + (float) this.topAnchor.absolute : NGUIMath.Lerp(sides[3].y, sides[1].y, this.topAnchor.relative) + (float) this.topAnchor.absolute;
      }
      else
        to2 = localPosition.y - pivotOffset.y * (float) this.mHeight + (float) this.mHeight;
    }
    Vector3 vector3 = new Vector3(Mathf.Lerp(from1, to1, pivotOffset.x), Mathf.Lerp(from2, to2, pivotOffset.y), localPosition.z);
    int num1 = Mathf.FloorToInt((float) ((double) to1 - (double) from1 + 0.5));
    int num2 = Mathf.FloorToInt((float) ((double) to2 - (double) from2 + 0.5));
    if (this.keepAspectRatio != UIWidget.AspectRatioSource.Free && (double) this.aspectRatio != 0.0)
    {
      if (this.keepAspectRatio == UIWidget.AspectRatioSource.BasedOnHeight)
        num1 = Mathf.RoundToInt((float) num2 * this.aspectRatio);
      else
        num2 = Mathf.RoundToInt((float) num1 / this.aspectRatio);
    }
    if (num1 < this.minWidth)
      num1 = this.minWidth;
    if (num2 < this.minHeight)
      num2 = this.minHeight;
    if ((double) Vector3.SqrMagnitude(localPosition - vector3) > 1.0 / 1000.0)
    {
      this.cachedTransform.localPosition = vector3;
      if (this.mIsInFront)
        this.mChanged = true;
    }
    if (this.mWidth == num1 && this.mHeight == num2)
      return;
    this.mWidth = num1;
    this.mHeight = num2;
    if (this.mIsInFront)
      this.mChanged = true;
    if (!this.autoResizeBoxCollider)
      return;
    this.ResizeCollider();
  }

  protected override void OnUpdate()
  {
    if (!((UnityEngine.Object) this.panel == (UnityEngine.Object) null))
      return;
    this.CreatePanel();
  }

  private void OnApplicationPause(bool paused)
  {
    if (paused)
      return;
    this.MarkAsChanged();
  }

  protected override void OnDisable()
  {
    this.RemoveFromPanel();
    base.OnDisable();
  }

  private void OnDestroy()
  {
    this.RemoveFromPanel();
  }

  public bool UpdateVisibility(bool visibleByAlpha, bool visibleByPanel)
  {
    if (this.mIsVisibleByAlpha == visibleByAlpha && this.mIsVisibleByPanel == visibleByPanel)
      return false;
    this.mChanged = true;
    this.mIsVisibleByAlpha = visibleByAlpha;
    this.mIsVisibleByPanel = visibleByPanel;
    return true;
  }

  public bool UpdateTransform(int frame)
  {
    if (!this.mMoved && !this.panel.widgetsAreStatic && this.cachedTransform.hasChanged)
    {
      this.mTrans.hasChanged = false;
      this.mLocalToPanel = this.panel.worldToLocal * this.cachedTransform.localToWorldMatrix;
      this.mMatrixFrame = frame;
      Vector2 pivotOffset = this.pivotOffset;
      float x1 = -pivotOffset.x * (float) this.mWidth;
      float y1 = -pivotOffset.y * (float) this.mHeight;
      float x2 = x1 + (float) this.mWidth;
      float y2 = y1 + (float) this.mHeight;
      Transform cachedTransform = this.cachedTransform;
      Vector3 v1 = cachedTransform.TransformPoint(x1, y1, 0.0f);
      Vector3 v2 = cachedTransform.TransformPoint(x2, y2, 0.0f);
      Vector3 vector3_1 = this.panel.worldToLocal.MultiplyPoint3x4(v1);
      Vector3 vector3_2 = this.panel.worldToLocal.MultiplyPoint3x4(v2);
      if ((double) Vector3.SqrMagnitude(this.mOldV0 - vector3_1) > 9.99999997475243E-07 || (double) Vector3.SqrMagnitude(this.mOldV1 - vector3_2) > 9.99999997475243E-07)
      {
        this.mMoved = true;
        this.mOldV0 = vector3_1;
        this.mOldV1 = vector3_2;
      }
    }
    if (this.mMoved && this.onChange != null)
      this.onChange();
    if (!this.mMoved)
      return this.mChanged;
    return true;
  }

  public bool UpdateGeometry(int frame)
  {
    float finalAlpha = this.CalculateFinalAlpha(frame);
    if (this.mIsVisibleByAlpha && (double) this.mLastAlpha != (double) finalAlpha)
      this.mChanged = true;
    this.mLastAlpha = finalAlpha;
    if (this.mChanged)
    {
      this.mChanged = false;
      if (this.mIsVisibleByAlpha && (double) finalAlpha > 1.0 / 1000.0 && (UnityEngine.Object) this.shader != (UnityEngine.Object) null)
      {
        bool hasVertices = this.geometry.hasVertices;
        if (this.fillGeometry)
        {
          this.geometry.Clear();
          this.OnFill(this.geometry.verts, this.geometry.uvs, this.geometry.cols);
        }
        if (!this.geometry.hasVertices)
          return hasVertices;
        if (this.mMatrixFrame != frame)
        {
          this.mLocalToPanel = this.panel.worldToLocal * this.cachedTransform.localToWorldMatrix;
          this.mMatrixFrame = frame;
        }
        this.geometry.ApplyTransform(this.mLocalToPanel);
        this.mMoved = false;
        return true;
      }
      if (this.geometry.hasVertices)
      {
        if (this.fillGeometry)
          this.geometry.Clear();
        this.mMoved = false;
        return true;
      }
    }
    else if (this.mMoved && this.geometry.hasVertices)
    {
      if (this.mMatrixFrame != frame)
      {
        this.mLocalToPanel = this.panel.worldToLocal * this.cachedTransform.localToWorldMatrix;
        this.mMatrixFrame = frame;
      }
      this.geometry.ApplyTransform(this.mLocalToPanel);
      this.mMoved = false;
      return true;
    }
    this.mMoved = false;
    return false;
  }

  public void WriteToBuffers(BetterList<Vector3> v, BetterList<Vector2> u, BetterList<Color32> c, BetterList<Vector3> n, BetterList<Vector4> t)
  {
    this.geometry.WriteToBuffers(v, u, c, n, t);
  }

  public virtual void MakePixelPerfect()
  {
    Vector3 localPosition = this.cachedTransform.localPosition;
    localPosition.z = Mathf.Round(localPosition.z);
    localPosition.x = Mathf.Round(localPosition.x);
    localPosition.y = Mathf.Round(localPosition.y);
    this.cachedTransform.localPosition = localPosition;
    Vector3 localScale = this.cachedTransform.localScale;
    this.cachedTransform.localScale = new Vector3(Mathf.Sign(localScale.x), Mathf.Sign(localScale.y), 1f);
  }

  public virtual void OnFill(BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols)
  {
  }

  public enum Pivot
  {
    TopLeft,
    Top,
    TopRight,
    Left,
    Center,
    Right,
    BottomLeft,
    Bottom,
    BottomRight,
  }

  public enum AspectRatioSource
  {
    Free,
    BasedOnWidth,
    BasedOnHeight,
  }

  public delegate void OnDimensionsChanged();

  public delegate void OnPostFillCallback(UIWidget widget, int bufferOffset, BetterList<Vector3> verts, BetterList<Vector2> uvs, BetterList<Color32> cols);

  public delegate bool HitCheck(Vector3 worldPos);
}
