﻿// Decompiled with JetBrains decompiler
// Type: NguiWidgetFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class NguiWidgetFactory
{
  public static class Options
  {
    public static OptionButtonElement CreateButtonElement(GameObject parent)
    {
      return NGUITools.AddChild(parent, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/OptionsElement")).GetComponent<OptionButtonElement>();
    }

    public static OptionsSliderElement CreateSliderElement(GameObject parent)
    {
      return NGUITools.AddChild(parent, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/OptionsElementSlider")).GetComponent<OptionsSliderElement>();
    }

    public static OptionsDropDownElement CreateDropDownElement(GameObject parent)
    {
      return NGUITools.AddChild(parent, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/general/OptionsDropDownElement")).GetComponent<OptionsDropDownElement>();
    }
  }

  public static class General
  {
    public static DropDownLabeledWidget CreateDropDownMenuLabeled(GameObject parent)
    {
      return NGUITools.AddChild(parent, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/general/DropDownMenuLabeled")).GetComponent<DropDownLabeledWidget>();
    }
  }

  public static class Hud
  {
  }
}
