﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorManagerNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class HudIndicatorManagerNgui : HudIndicatorManagerBase
{
  private readonly UIPanel mainPanel;

  public override bool IsRendered
  {
    get
    {
      return (double) this.mainPanel.alpha > 0.0;
    }
    set
    {
      this.mainPanel.alpha = !value ? 0.0f : 1f;
    }
  }

  public HudIndicatorManagerNgui(UIPanel mainPanel)
  {
    this.mainPanel = mainPanel;
  }

  public static HudIndicatorManagerNgui CreateInstance()
  {
    return new HudIndicatorManagerNgui(NGUITools.AddChild<UIPanel>(((GuiDataProvider) FacadeFactory.GetInstance().FetchDataProvider("GuiDataProvider")).uiHook));
  }

  protected override HudIndicatorBase CreateIndicatorForMyUiSystem(ISpaceEntity target)
  {
    return HudIndicatorFactoryNgui.CreateIndicator(target, this.mainPanel.gameObject);
  }
}
