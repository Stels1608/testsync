﻿// Decompiled with JetBrains decompiler
// Type: GUICameraPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUICameraPanel : GUIPanel
{
  private readonly GUIButtonNew zoomIn;
  private readonly GUIButtonNew zoomOut;
  private readonly GUICameraModeButtonsBase cameraModeButtons;

  public bool IsBlinking
  {
    get
    {
      return this.cameraModeButtons.IsBlinking;
    }
    set
    {
      this.cameraModeButtons.IsBlinking = value;
    }
  }

  public GUICameraPanel(GUICameraModeButtonsBase cameraCameraModeButtonButtons)
  {
    this.cameraModeButtons = cameraCameraModeButtonButtons;
    this.AddPanel((GUIPanel) this.cameraModeButtons);
    Texture2D normal1 = ResourceLoader.Load<Texture2D>("GUI/CameraToggles/camera_zoomin");
    Texture2D pressed1 = ResourceLoader.Load<Texture2D>("GUI/CameraToggles/camera_zoomin_click");
    Texture2D over1 = ResourceLoader.Load<Texture2D>("GUI/CameraToggles/camera_zoomin_over");
    this.zoomIn = new GUIButtonNew(normal1, over1, pressed1, this.root);
    this.zoomIn.TextLabel.IsRendered = false;
    this.zoomIn.Handler = (AnonymousDelegate) (() => SpaceLevel.GetLevel().SpaceCamera.AdjustScroll(SpaceCameraBase.ScrollDirection.In));
    this.zoomIn.Position = new float2((float) -((double) this.cameraModeButtons.Width / 2.0 + 5.0 + (double) this.zoomIn.Width / 2.0), 0.0f);
    this.zoomIn.SetTooltip("%$bgo.etc.camera_in_toggle_tooltip%", Action.ZoomIn);
    this.AddPanel((GUIPanel) this.zoomIn);
    Texture2D normal2 = ResourceLoader.Load<Texture2D>("GUI/CameraToggles/camera_zoomout");
    Texture2D pressed2 = ResourceLoader.Load<Texture2D>("GUI/CameraToggles/camera_zoomout_click");
    Texture2D over2 = ResourceLoader.Load<Texture2D>("GUI/CameraToggles/camera_zoomout_over");
    this.zoomOut = new GUIButtonNew(normal2, over2, pressed2, this.root);
    this.zoomOut.TextLabel.IsRendered = false;
    this.zoomOut.Handler = (AnonymousDelegate) (() => SpaceLevel.GetLevel().SpaceCamera.AdjustScroll(SpaceCameraBase.ScrollDirection.Out));
    this.zoomOut.Position = new float2((float) ((double) this.cameraModeButtons.Width / 2.0 + 5.0 + (double) this.zoomOut.Width / 2.0), 0.0f);
    this.zoomOut.SetTooltip("%$bgo.etc.camera_out_toggle_tooltip%", Action.ZoomOut);
    this.AddPanel((GUIPanel) this.zoomOut);
    this.Height = this.cameraModeButtons.Height;
    this.Width = (float) ((double) this.zoomIn.Width + (double) this.cameraModeButtons.Width + (double) this.zoomOut.Width + 10.0);
    this.PositionY = (float) (10.0 + (double) this.Height / 2.0);
    this.IsRendered = true;
  }

  public void OnCameraModeChanged(SpaceCameraBase.CameraMode cameraMode)
  {
    this.cameraModeButtons.OnCameraModeChanged(cameraMode);
  }

  public override void RecalculateAbsCoords()
  {
    this.PositionX = (float) Screen.width - (float) ((double) this.Width / 2.0 + 240.0);
    base.RecalculateAbsCoords();
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    base.OnKeyDown(keyboardKey, action);
    return false;
  }
}
