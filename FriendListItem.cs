﻿// Decompiled with JetBrains decompiler
// Type: FriendListItem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class FriendListItem : GuiPanel
{
  private GuiImage image = new GuiImage("GUI/InfoJournal/friend_offline");
  private GuiLabel name = new GuiLabel();
  private GuiLabel party = new GuiLabel();
  private GuiButton chat = new GuiButton(string.Empty, "GUI/InfoJournal/goto_chat");
  private GuiButton remove = new GuiButton("X");
  public Player player;

  public FriendListItem(Player p)
  {
    this.player = p;
    this.AddChild((GuiElementBase) this.image, Align.MiddleLeft, new Vector2(6f, 0.0f));
    this.AddChild((GuiElementBase) this.name, Align.MiddleLeft, new Vector2(25f, 0.0f));
    this.AddChild((GuiElementBase) this.party, Align.MiddleRight, new Vector2(-28f, 0.0f));
    this.AddChild((GuiElementBase) this.chat, Align.MiddleRight);
    this.AddChild((GuiElementBase) this.remove, Align.MiddleRight, new Vector2((float) (-28.0 - (double) this.chat.Size.x / 2.0 - (double) this.chat.Size.y * 2.0 - 5.0), 0.0f));
    this.remove.Size = new Vector2(this.remove.Size.y, this.remove.Size.y);
    this.chat.Pressed = (AnonymousDelegate) (() => Game.GUIManager.Find<GUIChatNew>().StartWhisper(this.player.Name));
    this.remove.Pressed = (AnonymousDelegate) (() =>
    {
      if (this.player == null)
        return;
      Game.Me.Friends.RemoveFriend(this.player);
    });
    this.SizeY = this.image.SizeY + 6f;
  }

  public override void PeriodicUpdate()
  {
    base.PeriodicUpdate();
    this.name.Text = this.player.Name;
    this.chat.IsRendered = this.player.Online;
    this.image.TexturePath = !this.player.Online ? "GUI/InfoJournal/friend_offline" : "GUI/InfoJournal/friend_online";
    this.party.Text = !this.IsInParty() ? string.Empty : "%$bgo.pilot_log.party_party%";
    if (this.player.Online)
      this.SetTooltip(this.player.Sector != null ? "%$bgo.pilot_log.party_location% " + this.player.Sector.Name : string.Empty);
    else
      this.SetTooltip("%$bgo.pilot_log.party_offline%");
  }

  private bool IsInParty()
  {
    return Game.Me.Party.IsMember(this.player);
  }

  public override bool MouseUp(float2 position, KeyCode key)
  {
    if (key == KeyCode.Mouse1 && this.player.Online && !this.IsInParty())
    {
      this.party.Text = "%$bgo.pilot_log.party_requested%";
      Game.Me.Party.Invite(this.player);
      this.AddChild((GuiElementBase) new TimerSimple(1f, true, (AnonymousDelegate) (() =>
      {
        this.party.Text = string.Empty;
        this.PeriodicUpdate();
      })));
    }
    return base.MouseUp(position, key);
  }
}
