﻿// Decompiled with JetBrains decompiler
// Type: CoolCamera
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CoolCamera : CommonCamera
{
  private Quaternion xRotation = Quaternion.identity;
  private Quaternion yRotation = Quaternion.identity;
  public float moveSpeed = 1f;
  public float xRotationSpeed = 3f / 16f;
  public float yRotationSpeed = 0.25f;
  public float slaveDistance = 10f;
  public float cameraFOV = 56f;
  private Vector3 lastMouse;
  public GameObject masterObject;

  private void Start()
  {
    this.lastMouse = Input.mousePosition;
    this.LateUpdate();
  }

  private void Update()
  {
    this.xRotation = Quaternion.identity;
    this.yRotation = Quaternion.identity;
    Vector3 mousePosition = Input.mousePosition;
    if (Input.GetMouseButtonDown(0))
      this.lastMouse = mousePosition;
    if (!Input.GetMouseButtonUp(0))
      ;
    if (Input.GetMouseButton(0))
      this.UpdateRotation(-(this.lastMouse.x - mousePosition.x) * this.xRotationSpeed, (this.lastMouse.y - mousePosition.y) * this.yRotationSpeed);
    this.slaveDistance += (float) ((double) this.slaveDistance * (double) Input.GetAxis("Mouse ScrollWheel") / 10.0);
    this.lastMouse = mousePosition;
  }

  private void LateUpdate()
  {
    this.UpdateFOV(this.cameraFOV);
    this.CheckRotation();
    Transform transform1 = this.transform;
    Transform transform2 = !((Object) this.masterObject == (Object) null) ? this.masterObject.transform : (Transform) null;
    transform1.rotation = this.xRotation * this.yRotation * transform1.rotation;
    Vector3 deltaPosition = this.GetDeltaPosition();
    if ((double) deltaPosition.sqrMagnitude > 0.001)
      transform1.position += deltaPosition;
    if ((Object) transform2 != (Object) null)
    {
      transform2.position = transform1.position + transform1.forward * this.slaveDistance - transform1.up * 1f;
      transform2.rotation = transform1.rotation;
    }
    this.UpdateRotation();
  }

  private Vector3 GetDeltaPosition()
  {
    Vector3 zero = Vector3.zero;
    if (Input.GetKey(KeyCode.A))
      zero -= this.transform.right;
    if (Input.GetKey(KeyCode.D))
      zero += this.transform.right;
    if (Input.GetKey(KeyCode.W))
      zero += this.transform.forward;
    if (Input.GetKey(KeyCode.S))
      zero -= this.transform.forward;
    if (Input.GetKey(KeyCode.Space))
      zero += this.transform.up;
    if (Input.GetKey(KeyCode.Tab))
      zero -= this.transform.up;
    Vector3 vector3 = zero * this.moveSpeed;
    if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.LeftControl))
      return vector3 * 100f;
    if (Input.GetKey(KeyCode.LeftShift))
      return vector3 * 10f;
    return vector3;
  }

  private void OnGUI()
  {
  }

  private void CheckRotation()
  {
    if (Algorithm3D.IsNaN(this.xRotation) || Algorithm3D.IsInfinity(this.xRotation))
      this.xRotation = Quaternion.identity;
    if (Algorithm3D.IsNaN(this.yRotation) || Algorithm3D.IsInfinity(this.yRotation))
      this.yRotation = Quaternion.identity;
    Vector3 to = this.yRotation * this.transform.forward;
    Vector3 vector3 = this.yRotation * this.transform.up;
    float num = 90f - Vector3.Angle(Vector3.up, to);
    if (-80.0 < (double) num && (double) num < 80.0 && (double) vector3.y > 0.0)
      return;
    this.yRotation = Quaternion.identity;
  }

  private void UpdateRotation(float dxAngle, float dyAngle)
  {
    float max = 90f;
    dxAngle = Mathf.Clamp(dxAngle, -max, max);
    dyAngle = Mathf.Clamp(dyAngle, -max, max);
    Vector3 up = Vector3.up;
    Vector3 right = this.transform.right;
    this.xRotation = Quaternion.AngleAxis(dxAngle, up);
    this.yRotation = Quaternion.AngleAxis(dyAngle, right);
  }
}
