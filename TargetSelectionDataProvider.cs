﻿// Decompiled with JetBrains decompiler
// Type: TargetSelectionDataProvider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Model;
using System;
using System.Collections.Generic;
using System.Linq;

public class TargetSelectionDataProvider : DataProvider<Message>
{
  public bool TargettingEnabled = true;
  private readonly Dictionary<ShipAbility, ISpaceEntity> abilityTargets;
  private SpaceObject currentMainTarget;
  private readonly List<ISpaceEntity> designatedTargets;
  private readonly List<ISpaceEntity> waypointTargets;

  public TargetSelectionDataProvider()
    : base("TargetSelectionProvider")
  {
    this.currentMainTarget = (SpaceObject) null;
    this.abilityTargets = new Dictionary<ShipAbility, ISpaceEntity>();
    this.designatedTargets = new List<ISpaceEntity>();
    this.waypointTargets = new List<ISpaceEntity>();
  }

  public void SetMainTarget(SpaceObject target)
  {
    this.currentMainTarget = target;
  }

  public SpaceObject GetMainTarget()
  {
    return this.currentMainTarget;
  }

  public bool IsMainTarget(ISpaceEntity target)
  {
    return this.currentMainTarget == target;
  }

  public bool IsWaypoint(ISpaceEntity target)
  {
    return this.waypointTargets.Contains(target);
  }

  public bool IsMultiTarget(ISpaceEntity target)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    return this.abilityTargets.Any<KeyValuePair<ShipAbility, ISpaceEntity>>(new Func<KeyValuePair<ShipAbility, ISpaceEntity>, bool>(new TargetSelectionDataProvider.\u003CIsMultiTarget\u003Ec__AnonStorey6A() { target = target }.\u003C\u003Em__42));
  }

  public ISpaceEntity GetAbilityTarget(ShipAbility shipAbility)
  {
    if (this.abilityTargets.ContainsKey(shipAbility))
      return this.abilityTargets[shipAbility];
    return (ISpaceEntity) null;
  }

  public void SetAbilityTarget(ShipAbility shipAbility, ISpaceEntity target)
  {
    if (!this.abilityTargets.ContainsKey(shipAbility))
      this.abilityTargets.Add(shipAbility, (ISpaceEntity) null);
    this.abilityTargets[shipAbility] = target;
  }

  public void AddDesignatedTarget(ISpaceEntity target)
  {
    if (this.designatedTargets.Contains(target))
      return;
    this.designatedTargets.Add(target);
  }

  public void RemoveDesignatedTarget(ISpaceEntity target)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.designatedTargets.RemoveAll(new Predicate<ISpaceEntity>(new TargetSelectionDataProvider.\u003CRemoveDesignatedTarget\u003Ec__AnonStorey6B()
    {
      target = target
    }.\u003C\u003Em__43));
  }

  public void AddWaypointTarget(ISpaceEntity target)
  {
    if (this.waypointTargets.Contains(target))
      return;
    this.waypointTargets.Add(target);
  }

  public void RemoveWaypointTarget(ISpaceEntity target)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.waypointTargets.RemoveAll(new Predicate<ISpaceEntity>(new TargetSelectionDataProvider.\u003CRemoveWaypointTarget\u003Ec__AnonStorey6C()
    {
      target = target
    }.\u003C\u003Em__44));
  }
}
