﻿// Decompiled with JetBrains decompiler
// Type: WofProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class WofProtocol : BgoProtocol
{
  public WofProtocol()
    : base(BgoProtocol.ProtocolID.Wof)
  {
  }

  public static WofProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Wof) as WofProtocol;
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    WofProtocol.Reply reply = (WofProtocol.Reply) msgType;
    switch (reply)
    {
      case WofProtocol.Reply.ReplyInit:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        WofProtocol.\u003CParseMessage\u003Ec__AnonStorey101 messageCAnonStorey101 = new WofProtocol.\u003CParseMessage\u003Ec__AnonStorey101();
        WofProtocol.JackpotType type1 = (WofProtocol.JackpotType) br.ReadUInt16();
        uint cardGUID1 = br.ReadGUID();
        int number1 = br.ReadInt32();
        bool flag1 = br.ReadBoolean();
        int num1 = br.ReadLength();
        List<int> intList1 = new List<int>();
        for (int index = 0; index < num1; ++index)
        {
          int num2 = br.ReadInt32();
          intList1.Add(num2);
        }
        GUICard card1 = (GUICard) Game.Catalogue.FetchCard(cardGUID1, CardView.GUI);
        WofJackpotContainer jackpotContainer1 = new WofJackpotContainer(type1, card1, number1);
        // ISSUE: reference to a compiler-generated field
        messageCAnonStorey101.content = new WofInitReply()
        {
          jackPotCard = jackpotContainer1,
          isFreeGame = flag1,
          costsPerStep = intList1
        };
        // ISSUE: reference to a compiler-generated method
        card1.IsLoaded.AddHandler(new SignalHandler(messageCAnonStorey101.\u003C\u003Em__254));
        break;
      case WofProtocol.Reply.ReplyDraw:
        WofDrawReply wofDrawReply = new WofDrawReply();
        WofProtocol.JackpotType type2 = (WofProtocol.JackpotType) br.ReadUInt16();
        uint cardGUID2 = br.ReadGUID();
        int number2 = br.ReadInt32();
        GUICard card2 = (GUICard) Game.Catalogue.FetchCard(cardGUID2, CardView.GUI);
        WofJackpotContainer jackpotContainer2 = new WofJackpotContainer(type2, card2, number2);
        wofDrawReply.jackpot = jackpotContainer2;
        wofDrawReply.freeWofGame = br.ReadBoolean();
        List<WofRewardItemContainer> rewardItemContainerList = new List<WofRewardItemContainer>();
        int num3 = br.ReadLength();
        for (int index = 0; index < num3; ++index)
        {
          bool flag2 = br.ReadBoolean();
          GUICard guiCard = (GUICard) Game.Catalogue.FetchCard(br.ReadGUID(), CardView.GUI);
          int num2 = br.ReadInt32();
          rewardItemContainerList.Add(new WofRewardItemContainer()
          {
            amount = num2,
            itemCard = guiCard,
            jackpotItem = flag2
          });
        }
        wofDrawReply.rewardItems = rewardItemContainerList;
        int num4 = br.ReadLength();
        List<WofBonusMapPart> wofBonusMapPartList = new List<WofBonusMapPart>();
        for (int index1 = 0; index1 < num4; ++index1)
        {
          bool jackp = br.ReadBoolean();
          int map = br.ReadInt32();
          uint cardGUID3 = br.ReadGUID();
          int num2 = br.ReadLength();
          List<int> cards = new List<int>();
          for (int index2 = 0; index2 < num2; ++index2)
          {
            int num5 = br.ReadInt32();
            cards.Add(num5);
          }
          GUICard card3 = (GUICard) Game.Catalogue.FetchCard(cardGUID3, CardView.GUI);
          wofBonusMapPartList.Add(new WofBonusMapPart(jackp, map, cards, card3));
        }
        wofDrawReply.bonusMapCards = wofBonusMapPartList;
        FacadeFactory.GetInstance().SendMessage(Message.ShowRewad, (object) wofDrawReply);
        break;
      case WofProtocol.Reply.ReplyVisibleMaps:
        int num6 = br.ReadLength();
        List<int> intList2 = new List<int>();
        for (int index = 0; index < num6; ++index)
        {
          int num2 = br.ReadInt32();
          intList2.Add(num2);
        }
        FacadeFactory.GetInstance().SendMessage(Message.WofVisibleMaps, (object) intList2);
        break;
      case WofProtocol.Reply.ReplyMapInfo:
        int num7 = br.ReadLength();
        Dictionary<int, GUICard> dictionary = new Dictionary<int, GUICard>();
        for (int index = 0; index < num7; ++index)
        {
          int key = br.ReadInt32();
          GUICard guiCard = (GUICard) Game.Catalogue.FetchCard(br.ReadGUID(), CardView.GUI);
          dictionary.Add(key, guiCard);
        }
        FacadeFactory.GetInstance().SendMessage(Message.WofMapInformation, (object) dictionary);
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Wof protocol: " + (object) reply);
        break;
    }
  }

  public void RequestWofInit()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    this.SendMessage(bw);
  }

  public void RequestWofVisibleMaps()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 5);
    this.SendMessage(bw);
  }

  public void RequestWofDraw(int number)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 3);
    bw.Write(number);
    this.SendMessage(bw);
  }

  public void RequestMapStart(int number)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 7);
    bw.Write(number);
    this.SendMessage(bw);
  }

  public void RequestMapInfo()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 8);
    this.SendMessage(bw);
  }

  public enum JackpotType : ushort
  {
    Item = 1,
    MapPart = 2,
  }

  public enum Request : ushort
  {
    RequestInit = 1,
    RequestDraw = 3,
    RequestVisibleMaps = 5,
    RequestMapStart = 7,
    RequestMapInfo = 8,
  }

  public enum Reply : ushort
  {
    ReplyInit = 2,
    ReplyDraw = 4,
    ReplyVisibleMaps = 6,
    ReplyMapInfo = 9,
  }
}
