﻿// Decompiled with JetBrains decompiler
// Type: Shop
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class Shop : ItemList
{
  private ItemOfferSchedule itemOfferSchedule;
  private UpgradeOfferSchedule upgradeOfferSchedule;

  public static Shop Local
  {
    get
    {
      RoomLevel level1 = RoomLevel.GetLevel();
      Shop shop = (Shop) null;
      if ((Object) null != (Object) level1)
        shop = level1.Shop;
      if (shop == null)
      {
        SpaceLevel level2 = SpaceLevel.GetLevel();
        if ((Object) null != (Object) level2)
          shop = level2.Shop;
      }
      return shop;
    }
  }

  public Shop()
    : base((IContainerID) new Shop.ShopContainerID())
  {
  }

  public void RequestItems()
  {
    ShopProtocol.GetProtocol().RequestItems();
  }

  public void Close()
  {
  }

  public static ShopDiscount FindItemDiscount(uint guid)
  {
    Shop local = Shop.Local;
    if (local == null || local.itemOfferSchedule == null)
      return (ShopDiscount) null;
    return local.itemOfferSchedule.FindDiscount(guid);
  }

  public static ShopDiscount FindUpgradeDiscount(ShopItemType shopCategory, int level)
  {
    Shop local = Shop.Local;
    if (local == null || local.upgradeOfferSchedule == null)
      return (ShopDiscount) null;
    return local.upgradeOfferSchedule.FindDiscount(shopCategory, level);
  }

  public static bool IsUpgradeDiscounted(ShopItemType shopCategory, int level)
  {
    return Shop.FindUpgradeDiscount(shopCategory, level) != null;
  }

  public static ShopItemType FindShopCategoryForSlotType(ShipSlotType slotType)
  {
    switch (slotType)
    {
      case ShipSlotType.computer:
        return ShopItemType.Computer;
      case ShipSlotType.engine:
        return ShopItemType.Engine;
      case ShipSlotType.hull:
        return ShopItemType.Hull;
      case ShipSlotType.weapon:
      case ShipSlotType.launcher:
      case ShipSlotType.defensive_weapon:
      case ShipSlotType.gun:
      case ShipSlotType.special_weapon:
        return ShopItemType.Weapon;
      case ShipSlotType.ship_paint:
        return ShopItemType.ShipPaint;
      case ShipSlotType.avionics:
        return ShopItemType.Avionics;
      default:
        return ShopItemType.None;
    }
  }

  public static Price GetDiscountedShipUpgradePrice(Price price)
  {
    Price price1 = new Price();
    ShopDiscount upgradeDiscount = Shop.FindUpgradeDiscount(ShopItemType.Ship, 1);
    if (upgradeDiscount == null)
      return price;
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = price.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
        if ((double) current.Value > 0.0)
        {
          float num = upgradeDiscount.ApplyDiscount(current.Value);
          price1.items.Add(current.Key, num);
        }
      }
    }
    return price1;
  }

  public void DecodeItemOfferSchedule(BgoProtocolReader input)
  {
    if (this.itemOfferSchedule != null)
      this.itemOfferSchedule.RemoveTickerMessages();
    this.itemOfferSchedule = ItemOfferSchedule.Decode(input);
  }

  public void DecodeUpgradeOfferSchedule(BgoProtocolReader input)
  {
    if (this.upgradeOfferSchedule != null)
      this.upgradeOfferSchedule.RemoveTickerMessages();
    this.upgradeOfferSchedule = UpgradeOfferSchedule.Decode(input);
  }

  public static bool ApplyDiscount(out float result, uint guid, float price)
  {
    ShopDiscount itemDiscount = Shop.FindItemDiscount(guid);
    if (itemDiscount == null)
    {
      result = price;
      return false;
    }
    result = itemDiscount.ApplyDiscount(price);
    return true;
  }

  public class ShopContainerID : IContainerID, IProtocolWrite
  {
    void IProtocolWrite.Write(BgoProtocolWriter w)
    {
      w.Write((byte) 4);
    }

    public ContainerType GetContainerType()
    {
      return ContainerType.Shop;
    }
  }
}
