﻿// Decompiled with JetBrains decompiler
// Type: GUIDoubleCountdownNew
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUIDoubleCountdownNew : GUIPanel
{
  private float timeToTick = 1f;
  private const float grayPiePercent = 0.3f;
  private const int numChunks = 12;
  private GUIImageNew innerImage;
  private GUIImageNew outerImage;
  private AtlasCache atlasCashe;
  private int iOuter;
  private int iInner;
  private float timeLeft;
  public AnonymousDelegate Handler;

  public override bool IsUpdated
  {
    get
    {
      return this.isUpdated;
    }
    set
    {
      this.isUpdated = value;
    }
  }

  public float TimeLeft
  {
    get
    {
      return this.timeLeft;
    }
    private set
    {
      this.timeLeft = value;
    }
  }

  public float TimeToTick
  {
    get
    {
      return this.timeToTick;
    }
    set
    {
      this.timeToTick = value;
    }
  }

  public float Progress
  {
    set
    {
      int num1 = (int) ((double) value * 12.0);
      if (num1 == 12)
        --num1;
      float num2 = Mathf.Clamp01(value * 12f - (float) num1);
      int num3 = num1 * 2;
      this.iInner = num3 + 1;
      if ((double) num2 < 0.300000011920929)
        this.iInner = num3;
      this.iOuter = (int) ((double) num2 * 24.0);
      if (this.iOuter != 24)
        return;
      --this.iOuter;
    }
  }

  public GUIDoubleCountdownNew()
    : this((SmartRect) null)
  {
  }

  public GUIDoubleCountdownNew(SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
    this.atlasCashe = new AtlasCache(new float2(128f, 128f));
    Texture2D texture = TextureCache.Get(Color.black);
    this.innerImage = new GUIImageNew(texture, this.root);
    this.innerImage.SmartRect.Width = 128f;
    this.innerImage.SmartRect.Height = 128f;
    this.innerImage.IsRendered = false;
    this.AddPanel((GUIPanel) this.innerImage);
    this.outerImage = new GUIImageNew(texture, this.root);
    this.outerImage.SmartRect.Width = 128f;
    this.outerImage.SmartRect.Height = 128f;
    this.outerImage.IsRendered = false;
    this.AddPanel((GUIPanel) this.outerImage);
    this.Name = "doubleCountdown";
  }

  public void Start()
  {
    this.TimeLeft = this.TimeToTick;
    this.IsUpdated = true;
  }

  public override void Update()
  {
    base.Update();
    this.TimeLeft -= Time.deltaTime;
    this.Progress = Mathf.Clamp01(this.TimeLeft / this.TimeToTick);
    if ((double) this.TimeLeft > 0.0)
      return;
    this.Progress = 0.0f;
    if (this.Handler != null)
      this.Handler();
    this.IsUpdated = false;
  }

  public override void Draw()
  {
    AtlasEntry cachedEntryBy1 = this.atlasCashe.GetCachedEntryBy("GUI/Common/inner", this.iInner);
    this.innerImage.Texture = cachedEntryBy1.Texture;
    this.innerImage.InnerRect = cachedEntryBy1.FrameRect;
    this.innerImage.IsRendered = true;
    AtlasEntry cachedEntryBy2 = this.atlasCashe.GetCachedEntryBy("GUI/Common/outer", this.iOuter);
    this.outerImage.Texture = cachedEntryBy2.Texture;
    this.outerImage.InnerRect = cachedEntryBy2.FrameRect;
    this.outerImage.IsRendered = true;
    base.Draw();
  }
}
