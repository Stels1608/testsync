﻿// Decompiled with JetBrains decompiler
// Type: SettingProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class SettingProtocol : BgoProtocol
{
  public SettingProtocol()
    : base(BgoProtocol.ProtocolID.Setting)
  {
  }

  public static SettingProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Setting) as SettingProtocol;
  }

  public void RequestSaveSettings(SettingsDataProvider settingsDataProvider)
  {
    BgoProtocolWriter bgoProtocolWriter = this.NewMessage();
    bgoProtocolWriter.Write((ushort) 1);
    settingsDataProvider.WriteServerSavedSettings(bgoProtocolWriter);
    this.SendMessage(bgoProtocolWriter);
  }

  public void RequestSaveKeys(SettingsDataProvider settingsDataProvider)
  {
    BgoProtocolWriter bgoProtocolWriter = this.NewMessage();
    bgoProtocolWriter.Write((ushort) 2);
    settingsDataProvider.SaveControls(bgoProtocolWriter);
    this.SendMessage(bgoProtocolWriter);
  }

  public void RequestSyfyShip(bool on)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 5);
    bw.Write(on);
    this.SendMessage(bw);
  }

  public void FullScreen()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 6);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader r)
  {
    SettingProtocol.Reply reply = (SettingProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case SettingProtocol.Reply.Settings:
        FacadeFactory.GetInstance().SendMessage(Message.LoadSettings, (object) r);
        break;
      case SettingProtocol.Reply.Keys:
        FacadeFactory.GetInstance().SendMessage(Message.LoadInputBindings, (object) r);
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Settings protocol: " + (object) reply);
        break;
    }
  }

  public enum Request : ushort
  {
    SaveSettings = 1,
    SaveKeys = 2,
    SetSyfyShip = 5,
    SetFullScreen = 6,
  }

  public enum Reply : ushort
  {
    Settings = 3,
    Keys = 4,
  }
}
