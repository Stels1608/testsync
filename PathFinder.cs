﻿// Decompiled with JetBrains decompiler
// Type: PathFinder
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public sealed class PathFinder
{
  private GameObject owner;
  private float colliderRaduis;

  public PathFinder(GameObject owner)
  {
    this.colliderRaduis = 5f;
    this.owner = owner;
  }

  private bool Linecast(Vector3 begin, Vector3 end, out RaycastHit hitInfo)
  {
    return Algorithm3D.Linecast(this.owner, begin, end, out hitInfo);
  }

  private void FindPath(ref List<Vector3> path, Vector3 end, int count)
  {
    Vector3 begin = path[path.Count - 1];
    RaycastHit hitInfo;
    if (this.Linecast(begin, end, out hitInfo) && count < 20 && path.Count < 20)
    {
      SphereCollider sphereCollider = hitInfo.collider as SphereCollider;
      if ((Object) sphereCollider != (Object) null)
      {
        Vector3 center = sphereCollider.bounds.center;
        Vector3 vector3_1 = begin - center;
        Vector3 vector3_2 = end - center;
        float num = sphereCollider.radius + this.colliderRaduis;
        if ((double) vector3_2.magnitude < (double) num)
        {
          Vector3 normalized = (end - center).normalized;
          this.FindPath(ref path, center + normalized * num, count + 1);
          return;
        }
        if ((double) vector3_1.magnitude < (double) num)
        {
          Vector3 normalized = (begin - center).normalized;
          path.Add(center + normalized * num);
          this.FindPath(ref path, end, count + 1);
          return;
        }
        Vector3 vector3_3 = hitInfo.point - center;
        Vector3 vector3_4 = end - begin;
        Vector3 normalized1 = vector3_3.normalized;
        Vector3 normalized2 = vector3_4.normalized;
        Vector3 normalized3 = Vector3.Cross(Vector3.Cross(normalized2, normalized1).normalized, normalized2).normalized;
        Vector3 end1 = center + num * normalized3;
        this.FindPath(ref path, end1, count + 1);
        this.FindPath(ref path, end, count + 1);
        return;
      }
    }
    path.Add(end);
  }

  public List<Vector3> GetPath(Vector3 target)
  {
    List<Vector3> path = new List<Vector3>();
    path.Add(this.owner.transform.position);
    this.FindPath(ref path, target, 0);
    return path;
  }
}
