﻿// Decompiled with JetBrains decompiler
// Type: GUISlotPopup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;

public class GUISlotPopup : GUIVerticalLayout
{
  private const float HEIGHT_OFFSET = 3f;
  private GUIButtonNew playerInfo;
  private GUIButtonNew invite;
  private GUIButtonNew mute;
  private GUIButtonNew dismiss;
  private GUIButtonNew makeLeader;
  private GUIButtonNew guildInvite;
  private GUIButtonNew friendInvite;
  private GUIButtonNew friendDismiss;
  private GUIButtonNew follow;
  private GUIButtonNew leave;
  public GUIButtonNew duel;
  private GUIButtonNew whisper;
  private Player player;
  public uint ObjectID;

  public override bool HandleKeyboardInput
  {
    get
    {
      return false;
    }
  }

  public override float2 Position
  {
    set
    {
      base.Position = value;
      this.RecalculateAbsCoords();
    }
  }

  public override float PositionX
  {
    set
    {
      base.PositionX = value;
      this.RecalculateAbsCoords();
    }
  }

  public override float PositionY
  {
    set
    {
      base.PositionY = value;
      this.RecalculateAbsCoords();
    }
  }

  public Player Player
  {
    get
    {
      return this.player;
    }
    set
    {
      this.player = value;
    }
  }

  public GUISlotPopup()
    : base((SmartRect) null)
  {
    this.Name = "SlotPopup";
    this.playerInfo = new GUIButtonNew(BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/Slots/popup_layout")["button"] as JButton);
    this.playerInfo.TextLabel.AutoSize = true;
    this.invite = new GUIButtonNew(this.playerInfo);
    this.mute = new GUIButtonNew(this.playerInfo);
    this.dismiss = new GUIButtonNew(this.playerInfo);
    this.makeLeader = new GUIButtonNew(this.playerInfo);
    this.guildInvite = new GUIButtonNew(this.playerInfo);
    this.friendInvite = new GUIButtonNew(this.playerInfo);
    this.friendDismiss = new GUIButtonNew(this.playerInfo);
    this.follow = new GUIButtonNew(this.playerInfo);
    this.leave = new GUIButtonNew(this.playerInfo);
    this.duel = new GUIButtonNew(this.playerInfo);
    this.whisper = new GUIButtonNew(this.playerInfo);
    this.playerInfo.Text = "%$bgo.etc.slot_info%";
    this.invite.Text = "%$bgo.etc.slot_invite%";
    this.mute.Text = "%$bgo.etc.slot_mute%";
    this.dismiss.Text = "%$bgo.etc.slot_dismiss%";
    this.makeLeader.Text = "%$bgo.etc.slot_appoint_leader%";
    this.guildInvite.Text = "%$bgo.etc.slot_guild_invite%";
    this.friendInvite.Text = "%$bgo.etc.slot_friend_invite%";
    this.friendDismiss.Text = "%$bgo.etc.slot_friend_dismiss%";
    this.follow.Text = "%$bgo.etc.slot_follow%";
    this.leave.Text = "%$bgo.etc.slot_leave%";
    this.duel.Text = "%$bgo.etc.slot_duel%";
    this.whisper.Text = "%$bgo.Wings.player_context_menu.whisper%";
    this.playerInfo.Handler = (AnonymousDelegate) (() => Game.RegisterDialog((IGUIPanel) new PlayerInfo(this.player), true));
    this.invite.Handler = (AnonymousDelegate) (() => Game.Me.Party.Invite(this.player));
    this.mute.Handler = (AnonymousDelegate) (() => Game.Me.Friends.ToggleIsIgnored(this.player.Name));
    this.dismiss.Handler = (AnonymousDelegate) (() => Game.Me.Party.Dismiss(this.player));
    this.makeLeader.Handler = (AnonymousDelegate) (() => Game.Me.Party.AppointLeader(this.player));
    this.guildInvite.Handler = (AnonymousDelegate) (() => Game.Guilds.AddPlayer(this.player));
    this.friendInvite.Handler = (AnonymousDelegate) (() => Game.Me.Friends.AddFriend(this.player));
    this.friendDismiss.Handler = (AnonymousDelegate) (() => Game.Me.Friends.RemoveFriend(this.player));
    this.follow.Handler = (AnonymousDelegate) (() => Game.Me.Follow(this.ObjectID));
    this.leave.Handler = (AnonymousDelegate) (() => Game.Me.Party.Leave());
    this.duel.Handler = (AnonymousDelegate) (() => ArenaProtocol.GetProtocol().RequestArenaDuelCheckIn(this.player.ServerID));
    this.whisper.Handler = (AnonymousDelegate) (() => Game.GUIManager.Find<GUIChatNew>().StartWhisper(this.player.Name));
    this.IsRendered = true;
  }

  public void AddPlayerInfo()
  {
    this.AddPanel((GUIPanel) this.playerInfo);
  }

  public void AddInvite()
  {
    this.AddPanel((GUIPanel) this.invite);
  }

  public void AddDismiss()
  {
    this.AddPanel((GUIPanel) this.dismiss);
  }

  public void AddMakeLeader()
  {
    this.AddPanel((GUIPanel) this.makeLeader);
  }

  public void AddLeave()
  {
    this.AddPanel((GUIPanel) this.leave);
  }

  public void AddGuildInvite()
  {
    this.AddPanel((GUIPanel) this.guildInvite);
  }

  public void AddFriendInvite()
  {
    this.AddPanel((GUIPanel) this.friendInvite);
  }

  public void AddFriendDismiss()
  {
    this.AddPanel((GUIPanel) this.friendDismiss);
  }

  public void AddFollow()
  {
    this.AddPanel((GUIPanel) this.follow);
  }

  public void AddDuel()
  {
    this.AddPanel((GUIPanel) this.duel);
  }

  public void AddWhisper()
  {
    this.AddPanel((GUIPanel) this.whisper);
  }

  private void AddMute()
  {
    this.mute.Text = !Game.Me.Friends.IsOnIgnoreList(this.player) ? "%$bgo.etc.slot_mute%" : "%$bgo.etc.slot_unmute%";
    this.AddPanel((GUIPanel) this.mute);
  }

  public void AppendPartyMenu()
  {
    if (Game.Me.Party.HasParty)
    {
      if (Game.Me.Party.IsLeader)
      {
        if (!Game.Me.Party.IsMember(this.Player))
        {
          this.AddInvite();
        }
        else
        {
          this.AddMakeLeader();
          this.AddDismiss();
        }
      }
    }
    else
      this.AddInvite();
    this.AddMute();
    if (Game.Me.Guild.Has)
      this.AddGuildInvite();
    if (!Game.Me.Friends.IsFriend(this.Player))
      this.AddFriendInvite();
    if (Game.Me.Friends.IsFriend(this.Player))
      this.AddFriendDismiss();
    this.AddWhisper();
  }
}
