﻿// Decompiled with JetBrains decompiler
// Type: GUIColoredImage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GUIColoredImage : GUIImage
{
  public Color Color;
  private Rect fullRect;

  public GUIColoredImage(Texture2D texture, SmartRect parent)
    : base(texture, parent)
  {
    this.Color = new Color(1f, 1f, 1f, 0.9f);
    this.fullRect = new Rect(0.0f, 0.0f, 1f, 1f);
  }

  public GUIColoredImage(Texture2D texture, Color color, SmartRect parent)
    : base(texture, parent)
  {
    this.Color = color;
  }

  public override void Draw()
  {
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    Graphics.DrawTexture(this.rect.AbsRect, (Texture) this.texture, this.fullRect, this.padding, this.padding, this.padding, this.padding, this.Color);
  }
}
