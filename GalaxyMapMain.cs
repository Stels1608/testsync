﻿// Decompiled with JetBrains decompiler
// Type: GalaxyMapMain
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class GalaxyMapMain : GuiPanel
{
  private readonly GalaxyMapLegend legend = new GalaxyMapLegend();
  private readonly GalaxyMapSectorsLayout layout = new GalaxyMapSectorsLayout();
  private readonly GalaxyMapRcpProgress progress = new GalaxyMapRcpProgress();
  private readonly GuiButton exitButton = new GuiButton("%$bgo.common.close%");
  private readonly ShowGalaxyMapAction showGalaxyMapAction = new ShowGalaxyMapAction();
  private readonly GuiColoredBox blackScreen = new GuiColoredBox(Color.black);
  private const uint UPDATE_GALAXY_MAP_DISPLAY_TIMEOUT_SEC = 60;
  public bool isJumpActive;
  public bool IsSystemMap3DShown;

  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  public static bool JumpActive
  {
    get
    {
      GalaxyMapMain galaxyMapMain = Game.GUIManager.Find<GalaxyMapMain>();
      if (galaxyMapMain != null)
        return galaxyMapMain.isJumpActive;
      return false;
    }
    set
    {
      GalaxyMapMain galaxyMapMain = Game.GUIManager.Find<GalaxyMapMain>();
      if (galaxyMapMain == null)
        return;
      galaxyMapMain.isJumpActive = value;
    }
  }

  public uint SelectedId
  {
    get
    {
      if (this.SelectedGalaxyMapSector == null)
        return 0;
      return this.SelectedGalaxyMapSector.Desc.Id;
    }
  }

  public override bool IsRendered
  {
    set
    {
      base.IsRendered = value;
      if (value)
      {
        this.showGalaxyMapAction.Show();
        this.layout.SelectSector((GuiElementBase) this.layout.SelectedGalaxyMapSector);
        Game.Galaxy.SubscribeGalaxyMap();
      }
      else
      {
        this.showGalaxyMapAction.Hide();
        Game.Galaxy.UnsubscribeGalaxyMap();
      }
    }
  }

  public GalaxyMapSector SelectedGalaxyMapSector
  {
    get
    {
      return this.layout.SelectedGalaxyMapSector;
    }
  }

  public GalaxyMapMain()
  {
    this.enabled = false;
    this.MouseTransparent = true;
    this.Align = Align.MiddleCenter;
    this.PositionY = -20f;
    GuiImage guiImage = new GuiImage("GUI/GalaxyMap/background");
    this.Size = guiImage.Size;
    this.exitButton.Pressed = new AnonymousDelegate(this.ToggleIsRendered);
    this.exitButton.Size = new Vector2(100f, 20f);
    this.blackScreen.Size = new Vector2(3000f, 3000f);
    this.AddChild((GuiElementBase) this.blackScreen, Align.MiddleCenter);
    this.AddChild((GuiElementBase) guiImage, Align.MiddleCenter);
    this.AddChild((GuiElementBase) new GuiLabel("%$bgo.galaxy_map.nebula%", Gui.Options.FontBGM_BT, 20)
    {
      OverlayColor = new Color?(Tools.Color(128, 128, 128))
    }, new Vector2(34f, 56f));
    this.AddChild((GuiElementBase) this.legend, Align.UpRight, new Vector2(-20f, 67f));
    this.AddChild((GuiElementBase) this.layout, Align.MiddleCenter, new Vector2(0.0f, 21f));
    this.AddChild((GuiElementBase) this.progress, Align.UpLeft, new Vector2(10f, 5f));
    this.AddChild((GuiElementBase) new Gui.Timer(1f));
    this.AddChild((GuiElementBase) this.exitButton, Align.UpRight, new Vector2(-27f, 15f));
    this.RegisterHotKey(Action.ToggleWindowGalaxyMap, (AnonymousDelegate) (() =>
    {
      if (!Game.GUIManager.IsRendered)
        return;
      SpaceLevel level = SpaceLevel.GetLevel();
      if (!((Object) level == (Object) null) && (!level.IsGlobal || this.IsSystemMap3DShown || (level.IsBattlespace || level.IsTournament)))
        return;
      this.ToggleIsRendered();
    }));
  }

  [DebuggerHidden]
  private static IEnumerator UpdateGalaxyMapDisplayCoroutine(GalaxyMapMain.UpdateDisplayCallback callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GalaxyMapMain.\u003CUpdateGalaxyMapDisplayCoroutine\u003Ec__Iterator25() { callback = callback, \u003C\u0024\u003Ecallback = callback };
  }

  public bool IsLayoutLoaded()
  {
    return this.layout.Info != null;
  }

  public void RemoveFlagShipImage(uint sectorId)
  {
    this.layout.FindSector(sectorId).RemoveFlagShipImage();
  }

  public bool IsSelectedSectorWithinWarpLimit()
  {
    if ((int) this.layout.MyGalaxyMapSector.Desc.Id != (int) this.layout.SelectedGalaxyMapSector.Desc.Id)
      return this.layout.SelectedGalaxyMapSector.CanJump(this.layout.MyGalaxyMapSector);
    return false;
  }

  public void ToggleIsRendered()
  {
    this.IsRendered = !this.IsRendered;
  }

  public void UpdateMap()
  {
    if (!this.IsLayoutLoaded())
      UnityEngine.Debug.LogError((object) "Layout not loaded!");
    else
      this.layout.SelectSector((GuiElementBase) this.layout.SelectedGalaxyMapSector);
  }

  public override bool MouseUp(float2 position, KeyCode key)
  {
    bool flag = base.MouseUp(position, key);
    if (!this.layout.Contains(position) && !this.exitButton.Contains(position))
      this.layout.SelectSector((GuiElementBase) null);
    return flag;
  }

  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    base.OnKeyDown(keyboardKey, action);
    return this.IsRendered;
  }

  public override bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    if (action == Action.ToggleWindowGalaxyMap)
      return false;
    base.OnKeyUp(keyboardKey, action);
    if (!Game.Me.Anchored)
    {
      if (action == Action.Jump)
        JumpActionsHandler.InitJumpSequence();
      if (action == Action.CancelJump)
        JumpActionsHandler.CancelJumpSequence();
    }
    return this.IsRendered;
  }

  public delegate void UpdateDisplayCallback(GalaxyMapMain mapMain);
}
