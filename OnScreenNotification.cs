﻿// Decompiled with JetBrains decompiler
// Type: OnScreenNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public abstract class OnScreenNotification
{
  public static bool ShowMiningShipMessages { get; set; }

  public static bool ShowExperienceMessages { get; set; }

  public static bool ShowHeavyFightingMessages { get; set; }

  public static bool ShowOutpostMessages { get; set; }

  public static bool ShowAugmentMessages { get; set; }

  public static bool ShowAssignmentMessages { get; set; }

  public abstract OnScreenNotificationType NotificationType { get; }

  public abstract NotificationCategory Category { get; }

  public abstract string TextMessage { get; }

  public abstract bool Show { get; }
}
