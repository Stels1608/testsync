﻿// Decompiled with JetBrains decompiler
// Type: OptionsRadioElement
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class OptionsRadioElement : OptionsElement
{
  [SerializeField]
  private RadioButtonGroupWidget radioButtonGroupWidget;
  private UserSetting setting;
  private OnSettingChanged onSettingChanged;

  public override OnSettingChanged OnSettingChanged
  {
    get
    {
      return this.onSettingChanged;
    }
    set
    {
      this.onSettingChanged = value;
      this.radioButtonGroupWidget.OnValueChanged = new AnonymousDelegate(this.SettingsChanged);
    }
  }

  public override void Init(UserSetting sett, object value)
  {
    this.setting = sett;
    this.radioButtonGroupWidget.OnValueChanged = (AnonymousDelegate) null;
    this.radioButtonGroupWidget.SetActiveButton((int) value);
    this.radioButtonGroupWidget.OnValueChanged = new AnonymousDelegate(this.SettingsChanged);
    this.radioButtonGroupWidget.SetActiveButtonSilent((int) value);
  }

  public void SettingsChanged()
  {
    if (this.onSettingChanged == null)
      return;
    this.onSettingChanged(this.setting, (object) (GraphicsQuality) this.radioButtonGroupWidget.GetIndexOfActiveButton());
  }
}
