﻿// Decompiled with JetBrains decompiler
// Type: GUIShopDragAndDrop
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUIShopDragAndDrop : GUIPanel
{
  private GUIImageNew dragedImage;
  private GUIPanel fromPanel;
  private float2 fromPos;
  private ShipItem prevDragedItem;
  private ShipItem draggedItem;
  public GUIShopDragAndDrop.dHandlrGetItem HandlerGetItem;
  public GUIShopDragAndDrop.dHandlrDropItem HandlerDropItem;
  private AtlasCache atlasCache;

  public GUIPanel FromPanel
  {
    get
    {
      return this.fromPanel;
    }
  }

  public ShipItem DraggedItem
  {
    get
    {
      return this.draggedItem;
    }
  }

  public GUIShopDragAndDrop(SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
    this.dragedImage = new GUIImageNew(TextureCache.Get(Color.black), this.root);
    this.dragedImage.SmartRect.Width = 40f;
    this.dragedImage.SmartRect.Height = 35f;
    this.dragedImage.IsRendered = false;
    this.AddPanel((GUIPanel) this.dragedImage);
    this.atlasCache = new AtlasCache(new float2(40f, 35f));
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (this.HandlerGetItem == null || mouseKey != KeyCode.Mouse0)
      return false;
    this.fromPos = mousePosition;
    this.HandlerGetItem(mousePosition, out this.prevDragedItem, out this.fromPanel);
    return true;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (this.draggedItem != null)
    {
      if (this.HandlerDropItem != null)
        this.HandlerDropItem(this.fromPos, mousePosition, this.fromPanel);
      this.dragedImage.IsRendered = false;
      this.draggedItem = (ShipItem) null;
      return true;
    }
    this.prevDragedItem = (ShipItem) null;
    return false;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    if (this.prevDragedItem != null && (bool) this.prevDragedItem.IsLoaded && !(this.prevDragedItem is StarterKit))
    {
      this.draggedItem = this.prevDragedItem;
      this.prevDragedItem = (ShipItem) null;
      AtlasEntry cachedEntryBy = this.atlasCache.GetCachedEntryBy(this.draggedItem.ItemGUICard.GUIAtlasTexturePath, (int) this.draggedItem.ItemGUICard.FrameIndex);
      this.dragedImage.IsRendered = true;
      this.dragedImage.Texture = cachedEntryBy.Texture;
      this.dragedImage.InnerRect = cachedEntryBy.FrameRect;
      this.dragedImage.Position = mousePosition - this.root.AbsPosition;
      this.RecalculateAbsCoords();
    }
    else
    {
      if (this.dragedImage.IsRendered)
      {
        this.dragedImage.Position = mousePosition - this.root.AbsPosition;
        this.RecalculateAbsCoords();
      }
      base.OnMouseMove(mousePosition);
    }
  }

  public delegate void dHandlrGetItem(float2 fromPos, out ShipItem item, out GUIPanel fromPanel);

  public delegate void dHandlrDropItem(float2 fromPos, float2 toPos, GUIPanel fromPanel);
}
