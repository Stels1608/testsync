﻿// Decompiled with JetBrains decompiler
// Type: PlanetAtmosphere
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PlanetAtmosphere : MonoBehaviour
{
  private UpdateLimiter updateLimiter = new UpdateLimiter(25);
  public Camera movingCamera;
  public GameObject atmosphereLayer1;
  public Transform sunPosition;
  public Color atmospehreColor;

  private void Start()
  {
  }

  private void Update()
  {
    if (!this.updateLimiter.CanUpdate() || !((Object) this.movingCamera != (Object) null))
      return;
    this.transform.rotation = this.movingCamera.transform.rotation;
    if (!((Object) this.atmosphereLayer1 != (Object) null) || !((Object) this.sunPosition != (Object) null))
      return;
    this.atmosphereLayer1.GetComponent<Renderer>().material.color = this.atmospehreColor * Mathf.Clamp(Vector3.Dot(-this.movingCamera.transform.forward, (this.sunPosition.position - this.transform.position).normalized), 0.5f, 1f);
  }
}
