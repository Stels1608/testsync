﻿// Decompiled with JetBrains decompiler
// Type: LoginScreen
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Net;
using UnityEngine;

public class LoginScreen : MonoBehaviour
{
  private string playerName = string.Empty;
  private uint playerID = 2;
  private Rect windowRect = new Rect(0.0f, 0.0f, 500f, 270f);
  private TitleScreen title = new TitleScreen();
  private LoginScreen.Server[] serverDesc = new LoginScreen.Server[10]{ new LoginScreen.Server("127.0.0.1", "27050", "Localhost"), new LoginScreen.Server("10.188.230.80", "27050", "Martins Rumpelkiste"), new LoginScreen.Server("10.188.209.193", "27050", "Manu"), new LoginScreen.Server("10.188.189.118", "27050", "Markus Knofe"), new LoginScreen.Server("battlestargalactica-704-game-01.bigpoint.net", "27050", "Franciscoton"), new LoginScreen.Server("battlestargalactica-819-game-01.bigpoint.net", "27050", "Earth"), new LoginScreen.Server("battlestargalactica-475-game-01.bigpoint.net", "27050", "Hamburgton"), new LoginScreen.Server("10.184.30.25", "27050", "bpdevsys ham"), new LoginScreen.Server("10.184.30.25", "30299", "tutorial branch"), new LoginScreen.Server(SectorEditorHelper.HelperDefaultIP, SectorEditorHelper.HelperDefaultPort, "EditorDefault") };
  private const float CHECK_SERVER_INTERVAL = 1f;
  private static GUIStyle errorLabelStyle;
  public string BaseURL;
  private GUISkin debugSkin;
  private uint slots;
  private bool isWebLogin;
  private bool showTitles;
  private bool isConnected;
  private bool isLogin;
  private bool invalidCommandLineArgs;
  private bool isLookingUpDns;
  private float checkServerTimeToUpdate;
  private int serverSelected;

  private void Start()
  {
    try
    {
      this.GetSessionParamsFromArgs();
    }
    catch (Exception ex)
    {
      this.invalidCommandLineArgs = true;
    }
    if (string.IsNullOrEmpty(this.playerName))
      this.playerName = string.Format("player{0:00}", (object) UnityEngine.Random.Range(0, 100));
    this.title.Start();
    this.isWebLogin = !Application.isEditor;
    if (Debug.isDebugBuild)
      this.isWebLogin = false;
    LoginScreen.errorLabelStyle = new GUIStyle();
    LoginScreen.errorLabelStyle.normal.textColor = Color.white;
    LoginScreen.errorLabelStyle.alignment = TextAnchor.MiddleCenter;
    LoginScreen.errorLabelStyle.fontSize = 44;
    FacadeFactory.GetInstance().SendMessage(Message.SetFullscreen, (object) true);
  }

  private void Update()
  {
    if (this.showTitles)
    {
      this.title.Update();
      if (!this.title.IsAllTitlesDisplayed())
        return;
    }
    if (Input.GetKeyDown(KeyCode.DownArrow))
    {
      ++this.serverSelected;
      this.serverSelected = this.serverSelected < this.serverDesc.Length ? this.serverSelected : 0;
    }
    if (Input.GetKeyDown(KeyCode.UpArrow))
    {
      --this.serverSelected;
      this.serverSelected = this.serverSelected >= 0 ? this.serverSelected : this.serverDesc.Length - 1;
    }
    this.isConnected = LoginLevel.GetLevel().IsConnected();
    this.playerName = this.TrueName(this.playerName);
    if (this.isWebLogin)
      return;
    this.checkServerTimeToUpdate -= Time.deltaTime;
    if ((double) this.checkServerTimeToUpdate > 0.0)
      return;
    this.checkServerTimeToUpdate = 1f;
    this.isLookingUpDns = true;
    Dns.BeginGetHostAddresses(this.serverDesc[this.serverSelected].ip, new AsyncCallback(this.GetHostAddressesCallback), (object) null);
  }

  private void GetHostAddressesCallback(IAsyncResult result)
  {
    this.isLookingUpDns = false;
    try
    {
      if (Dns.EndGetHostAddresses(result).Length <= 0)
        return;
      new ServerPing().Ping(this.serverDesc[this.serverSelected]);
    }
    catch
    {
      this.serverDesc[this.serverSelected].ping = -1;
      Debug.Log((object) "IP lookup failed or address not valid");
    }
  }

  private void OnGUI()
  {
    if ((UnityEngine.Object) this.debugSkin == (UnityEngine.Object) null)
      this.debugSkin = BgoDebug.LoadBlackSkin();
    GUI.skin = this.debugSkin;
    if (this.showTitles && !this.title.OnGUI())
      return;
    if (this.invalidCommandLineArgs && !Debug.isDebugBuild)
    {
      GUI.Label(new Rect(0.0f, 0.0f, (float) Screen.width, (float) Screen.height), "Please start the game using the Launcher!", LoginScreen.errorLabelStyle);
    }
    else
    {
      this.windowRect = new Rect((float) (((double) Screen.width - (double) this.windowRect.width) / 2.0), (float) (((double) Screen.height - (double) this.windowRect.height) / 2.0), this.windowRect.width, this.windowRect.height);
      GUI.Window(0, this.windowRect, new GUI.WindowFunction(this.WindowFunc), "Battlestar Galactica Online");
      this.title.OnGUI();
    }
  }

  private void WindowFunc(int windowID)
  {
    if (this.isConnected)
    {
      if (this.isLogin)
      {
        if (this.slots > 0U)
          GUILayout.Box("The server is full.\nYour place in the queue: " + (object) this.slots);
        else
          GUILayout.Box("Loading...\nPlease wait.");
      }
      else
        GUILayout.Box("Connecting...\nPlease wait.");
    }
    else if (this.isWebLogin)
      GUILayout.Box("Welcome to Battlestar Galactica Online");
    else
      this.DrawMultiplayer();
  }

  private void DrawMultiplayer()
  {
    string str = !this.serverDesc[this.serverSelected].IsOnline ? "offline" : "online";
    if (this.isLookingUpDns)
      str = "DNS Lookup...";
    GUILayout.Box("Server status: " + str);
    GUILayout.BeginHorizontal();
    GUILayout.Label(this.serverDesc[this.serverSelected].desc);
    this.serverDesc[this.serverSelected].ip = GUILayout.TextField(this.serverDesc[this.serverSelected].ip, GUILayout.Width(200f));
    this.serverDesc[this.serverSelected].port = GUILayout.TextField(this.serverDesc[this.serverSelected].port, GUILayout.Width(50f));
    GUILayout.EndHorizontal();
    GUILayout.Space(20f);
    GUILayout.BeginHorizontal();
    Game.Language = GUILayout.TextField(Game.Language, GUILayout.Width(35f));
    GUILayout.Box("Login");
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Label("PlayerID: ");
    uint.TryParse(GUILayout.TextField(this.playerID.ToString(), GUILayout.Width(150f)), out this.playerID);
    if (GUILayout.Button("Login as " + (object) this.playerID, new GUILayoutOption[1]{ GUILayout.Width(200f) }))
      this.ButtonConnect(ConnectType.DebugPlayerId);
    GUILayout.EndHorizontal();
    GUILayout.BeginHorizontal();
    GUILayout.Label("Name: ");
    this.playerName = this.TrueName(GUILayout.TextField(this.playerName, GUILayout.Width(150f)));
    if (GUILayout.Button("Login as " + this.playerName, new GUILayoutOption[1]{ GUILayout.Width(200f) }) || Input.GetKeyDown(KeyCode.Return))
      this.ButtonConnect(ConnectType.DebugName);
    GUILayout.EndHorizontal();
    GUILayout.Space(20f);
    GUILayout.Box("Create new player");
    if (GUILayout.Button("Create"))
      this.ButtonConnect(ConnectType.DebugNew);
    if (!GUILayout.Button("Reset player " + (object) this.playerID))
      return;
    this.ButtonConnect(ConnectType.DebugResetByPlayerId);
  }

  private string TrueName(string str)
  {
    string str1 = " \\\n\t\r'\"/|.,:;`~=@}{#&%$^";
    string str2 = string.Empty;
    foreach (char ch in str)
    {
      if (!str1.Contains(ch.ToString()))
        str2 += (string) (object) ch;
    }
    return str2;
  }

  private void ButtonConnect(ConnectType connect)
  {
    try
    {
      PlayerPrefs.SetInt("player_id", (int) this.playerID);
      PlayerPrefs.SetString("player_name", this.playerName);
      PlayerPrefs.SetString("lang", Game.Language);
      PlayerPrefs.SetInt("server_type", this.serverSelected);
    }
    catch (PlayerPrefsException ex)
    {
    }
    this.playerName = this.TrueName(this.playerName);
    LoginProtocol protocol = LoginProtocol.GetProtocol();
    protocol.PlayerName = this.playerName;
    protocol.PlayerId = this.playerID;
    protocol.ConnectType = connect;
    protocol.SessionCode = string.Empty;
    Game.ProtocolManager.ConnectTo(this.serverDesc[this.serverSelected].ip, this.serverDesc[this.serverSelected].port);
  }

  public void OnWait(uint slots)
  {
    this.slots = slots;
  }

  public void OnLogin()
  {
    this.isLogin = true;
  }

  private void SetSessionParams2(string Params)
  {
    string[] strArray = Params.Split(' ');
    Log.Add(strArray[2]);
    EventStream.ProjectID = uint.Parse(strArray[0]);
    EventStream.UserID = uint.Parse(strArray[1]);
    EventStream.SessionID = strArray[2];
    EventStream.TrackingID = strArray[3];
  }

  private void SetSessionParams(string Params)
  {
    string[] strArray = Params.Split(' ');
    LoginProtocol protocol = LoginProtocol.GetProtocol();
    protocol.SessionCode = strArray[3];
    Game.Language = strArray[2];
    this.BaseURL = strArray[1];
    protocol.ConnectType = ConnectType.Web;
    Game.ProtocolManager.ConnectTo(strArray[0], "27050");
    LoginLevel.GetLevel().AssetMapVersion = strArray[4];
  }

  private void SetGeoTarget(string geoTarget)
  {
    Game.GeoRegion = geoTarget == null ? string.Empty : geoTarget.ToLower();
  }

  private void GetSessionParamsFromArgs()
  {
    Log.Add("getting command line args");
    string[] commandLineArgs = Environment.GetCommandLineArgs();
    this.invalidCommandLineArgs = this.GetValueFromFlag("+userID", commandLineArgs) == null;
    string Params1 = this.GetValueFromFlag("+projectID", commandLineArgs) + " " + this.GetValueFromFlag("+userID", commandLineArgs) + " " + this.GetValueFromFlag("+sessionID", commandLineArgs) + " " + this.GetValueFromFlag("+trackingID", commandLineArgs);
    string Params2 = this.GetValueFromFlag("+gameServer", commandLineArgs) + " " + this.GetValueFromFlag("+cdn", commandLineArgs) + " " + this.GetValueFromFlag("+language", commandLineArgs) + " " + this.GetValueFromFlag("+session", commandLineArgs) + " " + this.GetValueFromFlag("+version", commandLineArgs);
    this.SetSessionParams2(Params1);
    this.SetSessionParams(Params2);
  }

  private string GetValueFromFlag(string flag, string[] args)
  {
    for (int index = 0; index < args.Length - 1; ++index)
    {
      if (args[index] == flag)
        return args[index + 1];
    }
    return (string) null;
  }

  public class Server
  {
    public int ping = -2;
    public string ip;
    public string port;
    public string desc;

    public string info
    {
      get
      {
        if (this.ping == -2)
          return "-";
        if (this.ping == -1)
          return "off";
        return this.ping.ToString();
      }
    }

    public bool IsOnline
    {
      get
      {
        return this.ping > -1;
      }
    }

    public Server(string ip, string port, string desc)
    {
      this.ip = ip;
      this.port = port;
      this.desc = desc;
    }
  }
}
