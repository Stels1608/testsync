﻿// Decompiled with JetBrains decompiler
// Type: DebugSceneSector
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugSceneSector : DebugBehaviour<DebugSceneSector>
{
  private string sectorID = "3";

  private void Start()
  {
    this.windowID = 19;
    this.SetSize(780f, 530f);
  }

  private void Map()
  {
    float num1 = 850f;
    float num2 = 600f;
    using (Dictionary<uint, MapStarDesc>.ValueCollection.Enumerator enumerator = StaticCards.GalaxyMap.Stars.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MapStarDesc current = enumerator.Current;
        Rect position = new Rect(current.Position.x + num1 / 2f, current.Position.y + num2 / 2f, 25f, 20f);
        position.x *= 0.8f;
        position.y *= 0.8f;
        if (GUI.Button(position, current.Id.ToString()))
        {
          DebugProtocol.GetProtocol().Command("sector", current.Id.ToString());
          DebugBehaviour<DebugSceneSector>.Close();
        }
        position.x += 25f;
        position.width = 100f;
        GUI.Label(position, (Game.Catalogue.FetchCard(current.SectorGUID, CardView.GUI) as GUICard).Name);
      }
    }
  }

  protected override void WindowFunc()
  {
    GameLevel instance = GameLevel.Instance;
    if (instance is LoginLevel || (Object) instance == (Object) null)
    {
      GUILayout.Box("No commands \nfor this level.");
    }
    else
    {
      GUILayout.BeginHorizontal();
      this.sectorID = GUILayout.TextField(this.sectorID, GUILayout.Width(30f));
      if (GUILayout.Button("Jump to " + this.sectorID.ToString(), new GUILayoutOption[1]{ GUILayout.Width(150f) }))
        this.Jump();
      GUILayout.EndHorizontal();
      this.Map();
    }
  }

  private void Jump()
  {
    uint result = 0;
    uint.TryParse(this.sectorID, out result);
    DebugProtocol.GetProtocol().Command("sector", result.ToString());
    DebugBehaviour<DebugSceneSector>.Close();
  }
}
