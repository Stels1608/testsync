﻿// Decompiled with JetBrains decompiler
// Type: ConquestLocationUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class ConquestLocationUpdate : GalaxyMapUpdate
{
  public const int NO_CONQUEST = -1;

  public DateTime ExpireTime { get; private set; }

  public ConquestLocationUpdate(Faction faction, int sectorId, DateTime expireTime)
    : base(GalaxyUpdateType.ConquestLocation, faction, sectorId)
  {
    this.ExpireTime = expireTime;
  }

  public override string ToString()
  {
    return "[ConquestLocationUpdate] (Faction: " + (object) this.Faction + ", SectorId: " + (object) this.SectorId + ")";
  }
}
