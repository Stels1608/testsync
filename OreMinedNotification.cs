﻿// Decompiled with JetBrains decompiler
// Type: OreMinedNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class OreMinedNotification : OnScreenNotification
{
  private readonly ItemCountable resource;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.OreMined;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Neutral;
    }
  }

  public override string TextMessage
  {
    get
    {
      return BsgoLocalization.Get("%$bgo.notif.ore_mined%", (object) this.resource.Count, (object) this.resource.ItemGUICard.Name);
    }
  }

  public override bool Show
  {
    get
    {
      return OnScreenNotification.ShowMiningShipMessages;
    }
  }

  public OreMinedNotification(ItemCountable resource)
  {
    this.resource = resource;
  }
}
