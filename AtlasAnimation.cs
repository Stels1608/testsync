﻿// Decompiled with JetBrains decompiler
// Type: AtlasAnimation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AtlasAnimation : TextureAtlas
{
  private float playbackPosition;
  private Rect currentFrameRect;

  public float PlaybackPosition
  {
    get
    {
      return this.playbackPosition;
    }
    set
    {
      this.playbackPosition = Mathf.Clamp01(value);
      int num = (int) ((double) this.playbackPosition * (double) (this.width * this.height));
      if ((long) num == (long) (this.width * this.height))
        --num;
      this.currentFrameRect = this.GetFrameRectByIndex((uint) num);
    }
  }

  public Rect FrameRect
  {
    get
    {
      return this.currentFrameRect;
    }
  }

  public AtlasAnimation(Texture2D texture, uint width, uint height)
    : base(texture, width, height)
  {
    this.PlaybackPosition = 0.0f;
  }
}
