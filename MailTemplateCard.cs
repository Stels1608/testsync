﻿// Decompiled with JetBrains decompiler
// Type: MailTemplateCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class MailTemplateCard : Card
{
  public uint Expire;
  public GUICard SenderColonial;
  public GUICard SenderCylon;
  public string BodyColonial;
  public string BodyCylon;
  public GUICard GUI;
  public ushort MailLimit;

  public string InboxLimitMessage
  {
    get
    {
      string str;
      BsgoLocalization.TryGet("bgo." + this.GUI.Key + ".InboxLimitMessage", out str);
      return str;
    }
  }

  public GUICard SenderCard
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return this.SenderColonial;
      return this.SenderCylon;
    }
  }

  public string Body
  {
    get
    {
      if (Game.Me.Faction == Faction.Cylon && !string.IsNullOrEmpty(this.BodyCylon))
        return this.BodyCylon;
      return this.BodyColonial;
    }
  }

  public MailTemplateCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    uint cardGUID1 = r.ReadUInt32();
    uint cardGUID2 = r.ReadUInt32();
    this.SenderColonial = (GUICard) Game.Catalogue.FetchCard(cardGUID1, CardView.GUI);
    this.SenderCylon = (GUICard) Game.Catalogue.FetchCard(cardGUID2, CardView.GUI);
    this.Expire = (uint) r.ReadUInt16();
    this.BodyColonial = r.ReadString();
    this.BodyCylon = r.ReadString();
    this.MailLimit = r.ReadUInt16();
    this.GUI = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
    this.IsLoaded.Depend((ILoadable) this.SenderColonial, (ILoadable) this.SenderCylon, (ILoadable) this.GUI);
  }
}
