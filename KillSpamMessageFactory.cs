﻿// Decompiled with JetBrains decompiler
// Type: KillSpamMessageFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public static class KillSpamMessageFactory
{
  private static readonly Texture2D killingSpreeIcon = ResourceLoader.Load<Texture2D>("GUI/Events/Tournament/TournamentAce");
  private static readonly Texture2D nemesisIcon = ResourceLoader.Load<Texture2D>("GUI/Events/Tournament/TournamentNemesis");
  private const string ICON_CODE = "%icon%";
  private const string TXT_KILL = "%$bgo.tournament.global.kill%";
  private const string TXT_KILLING_SPREE = "%$bgo.tournament.global.killing_spree%";
  private const string TXT_ACE_KILL = "%$bgo.tournament.global.ace_kill%";
  private const string TXT_NEMESIS_KILL = "%$bgo.tournament.global.nemesis_kill%";
  private const string TXT_REVENGE_KILL = "%$bgo.tournament.global.revenge_kill%";
  private const string TXT_DEATH = "%$bgo.tournament.global.death%";

  public static KillSpamMessage CreatePlainKillMessage(Player player1, Player player2)
  {
    return new KillSpamMessage(BsgoLocalization.Get("%$bgo.tournament.global.kill%", (object) KillSpamMessageFactory.FormatPlayerName(player1), (object) KillSpamMessageFactory.FormatPlayerName(player2)));
  }

  public static KillSpamMessage CreateKillingSpreeMessage(Player player)
  {
    return new KillSpamMessage(BsgoLocalization.Get("%$bgo.tournament.global.killing_spree%", (object) KillSpamMessageFactory.FormatPlayerName(player)), "%icon%", KillSpamMessageFactory.killingSpreeIcon);
  }

  public static KillSpamMessage CreateAceKillMessage(Player player1, Player player2)
  {
    return new KillSpamMessage(BsgoLocalization.Get("%$bgo.tournament.global.ace_kill%", (object) KillSpamMessageFactory.FormatPlayerName(player2), (object) KillSpamMessageFactory.FormatPlayerName(player1)));
  }

  public static KillSpamMessage CreateNemesisKillMessage(Player player1, Player player2)
  {
    return new KillSpamMessage(BsgoLocalization.Get("%$bgo.tournament.global.nemesis_kill%", (object) KillSpamMessageFactory.FormatPlayerName(player1), (object) KillSpamMessageFactory.FormatPlayerName(player2)), "%icon%", KillSpamMessageFactory.nemesisIcon);
  }

  public static KillSpamMessage CreateRevengeKillMessage(Player player1, Player player2)
  {
    return new KillSpamMessage(BsgoLocalization.Get("%$bgo.tournament.global.revenge_kill%", (object) KillSpamMessageFactory.FormatPlayerName(player1), (object) KillSpamMessageFactory.FormatPlayerName(player2)), "%icon%", KillSpamMessageFactory.nemesisIcon);
  }

  public static KillSpamMessage CreateDeathMessage(Player player1, Player player2)
  {
    return new KillSpamMessage(BsgoLocalization.Get("%$bgo.tournament.global.death%", (object) KillSpamMessageFactory.FormatPlayerName(player2)));
  }

  private static string FormatPlayerName(Player player)
  {
    return "<color=" + (player.Faction != Faction.Colonial ? "#c92e47" : "#7aa3ee") + ">" + player.Name + "</color>";
  }
}
