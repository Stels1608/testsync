﻿// Decompiled with JetBrains decompiler
// Type: GuiHudStrikesMouseMovementIndicators
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class GuiHudStrikesMouseMovementIndicators : GuiPanel
{
  private static GuiHudStrikesMouseMovementIndicators instance;
  private readonly GuiHudStrikesMouseMovementArrows arrows;
  private readonly GuiHudStrikesMouseMovementCursor cursor;

  public static GuiHudStrikesMouseMovementIndicators Instance
  {
    get
    {
      return GuiHudStrikesMouseMovementIndicators.instance ?? (GuiHudStrikesMouseMovementIndicators.instance = new GuiHudStrikesMouseMovementIndicators());
    }
  }

  public GuiHudStrikesMouseMovementIndicators()
  {
    this.arrows = new GuiHudStrikesMouseMovementArrows();
    this.cursor = new GuiHudStrikesMouseMovementCursor();
  }

  public void Activate()
  {
    this.arrows.Activate();
    this.cursor.Activate();
  }

  public void Deactivate()
  {
    this.arrows.Deactivate();
    this.cursor.Deactivate();
  }

  public override void Draw()
  {
    base.Draw();
    this.arrows.Draw();
  }
}
