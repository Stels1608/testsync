﻿// Decompiled with JetBrains decompiler
// Type: GUIButtonNew
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using UnityEngine;

public class GUIButtonNew : GUIPanel
{
  public AudioClip MouseOverSound = GUISound.Instance.MouseOver;
  public AudioClip PressSound = GUISound.Instance.Select;
  public int Padding = 5;
  protected bool isPressed;
  protected bool mouseOver;
  protected Texture2D normalTexture;
  protected Texture2D pressedTexture;
  protected Texture2D overTexture;
  protected GUILabelNew textLabel;
  private AnonymousDelegate handler;

  public virtual bool IsPressed
  {
    get
    {
      return this.isPressed;
    }
    set
    {
      this.isPressed = value;
    }
  }

  public virtual bool MouseOver
  {
    get
    {
      return this.mouseOver;
    }
    set
    {
      if (this.IsRendered && !this.MouseOver && value)
        this.OnMouseOver();
      this.mouseOver = value;
      this.TextLabel.MouseOver = value;
    }
  }

  public Texture2D NormalTexture
  {
    get
    {
      return this.normalTexture;
    }
    set
    {
      this.normalTexture = value;
    }
  }

  public Texture2D OverTexture
  {
    get
    {
      return this.overTexture;
    }
    set
    {
      this.overTexture = value;
    }
  }

  public Texture2D PressedTexture
  {
    get
    {
      return this.pressedTexture;
    }
    set
    {
      this.pressedTexture = value;
    }
  }

  public GUILabelNew TextLabel
  {
    get
    {
      return this.textLabel;
    }
    set
    {
      this.textLabel = value;
      this.TextLabel.Parent = this.SmartRect;
      this.TextLabel.MouseOver = this.MouseOver;
    }
  }

  public string Text
  {
    get
    {
      return this.TextLabel.Text;
    }
    set
    {
      this.TextLabel.Text = value;
    }
  }

  public AnonymousDelegate Handler
  {
    get
    {
      return this.handler;
    }
    set
    {
      this.handler = value;
    }
  }

  public override bool HandleKeyboardInput
  {
    get
    {
      return false;
    }
  }

  public GUIButtonNew()
    : this("button", Gui.Options.buttonNormal, Gui.Options.buttonOver, Gui.Options.buttonPressed, (SmartRect) null)
  {
  }

  public GUIButtonNew(string text)
    : this(text, Gui.Options.buttonNormal, Gui.Options.buttonOver, Gui.Options.buttonPressed, (SmartRect) null)
  {
  }

  public GUIButtonNew(string normal, string over, string pressed)
    : this(string.Empty, normal, over, pressed)
  {
  }

  public GUIButtonNew(string text, string normal, string over, string pressed)
    : this(text, ResourceLoader.Load<Texture2D>(normal), ResourceLoader.Load<Texture2D>(over), ResourceLoader.Load<Texture2D>(pressed), (SmartRect) null)
  {
  }

  public GUIButtonNew(GUIButtonNew toCopy)
  {
    this.Name = toCopy.Name;
    this.SmartRect.Size = toCopy.Size;
    this.SmartRect.Position = toCopy.Position;
    this.NormalTexture = toCopy.NormalTexture;
    this.OverTexture = toCopy.OverTexture;
    this.PressedTexture = toCopy.PressedTexture;
    this.textLabel = new GUILabelNew(toCopy.TextLabel);
    this.textLabel.Parent = this.SmartRect;
    this.IsRendered = toCopy.IsRendered;
    this.IsUpdated = toCopy.IsUpdated;
    this.IsPressed = toCopy.IsPressed;
  }

  public GUIButtonNew(JButton desc)
  {
    this.Name = desc.name;
    this.SmartRect.Width = desc.size.x;
    this.SmartRect.Height = desc.size.y;
    this.SmartRect.Position = desc.position;
    Texture2D texture2D1 = ResourceLoader.Load<Texture2D>(desc.normalTexture);
    Texture2D texture2D2 = ResourceLoader.Load<Texture2D>(desc.overTexture);
    Texture2D texture2D3 = ResourceLoader.Load<Texture2D>(desc.pressedTexture);
    this.normalTexture = texture2D1;
    this.overTexture = texture2D2;
    this.pressedTexture = texture2D3;
    this.textLabel = new GUILabelNew(desc.label);
    this.textLabel.Parent = this.SmartRect;
    this.IsRendered = true;
  }

  public GUIButtonNew(Texture2D normal, Texture2D over, Texture2D pressed)
    : this(string.Empty, normal, over, pressed, (SmartRect) null)
  {
  }

  public GUIButtonNew(Texture2D normal, Texture2D over, Texture2D pressed, SmartRect parent)
    : this("button", normal, over, pressed, parent)
  {
  }

  public GUIButtonNew(string text, Texture2D normal, Texture2D over, Texture2D pressed, SmartRect parent)
    : base(parent)
  {
    this.Rect = new Rect(0.0f, 0.0f, (float) normal.width, (float) normal.height);
    this.Position = float2.zero;
    this.IsRendered = true;
    this.normalTexture = normal;
    this.overTexture = over;
    this.pressedTexture = pressed;
    this.textLabel = new GUILabelNew(text, this.root);
  }

  public virtual void Toggle()
  {
    this.IsPressed = !this.IsPressed;
  }

  public override void Draw()
  {
    bool enabled = GUI.enabled;
    GUI.enabled = this.HandleMouseInput;
    if (Event.current.type == UnityEngine.EventType.Repaint)
    {
      Texture2D texture2D = !this.IsPressed ? (!this.MouseOver ? this.normalTexture : (!((Object) this.overTexture != (Object) null) || !this.HandleMouseInput ? this.normalTexture : this.overTexture)) : this.pressedTexture;
      if ((Object) texture2D != (Object) null)
        Graphics.DrawTexture(this.root.AbsRect, (Texture) texture2D, this.Padding, this.Padding, this.Padding, this.Padding);
    }
    if (this.TextLabel.IsRendered)
      this.TextLabel.Draw();
    base.Draw();
    GUI.enabled = enabled;
  }

  public override bool Contains(float2 point)
  {
    if (!this.HandleMouseInput)
      return false;
    bool flag = false;
    if (this.TextLabel.HandleMouseInput)
      flag = this.TextLabel.Contains(point);
    if (!this.SmartRect.AbsRect.Contains(point.ToV2()))
      return flag;
    return true;
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.TextLabel.RecalculateAbsCoords();
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (!this.HandleMouseInput)
      return false;
    this.wasMouseDown = false;
    if (mouseKey != KeyCode.Mouse0 || !this.Contains(mousePosition))
      return false;
    this.wasMouseDown = true;
    this.IsPressed = true;
    return true;
  }

  public override void OnMouseMove(float2 mousePosition)
  {
    this.MouseOver = this.Contains(mousePosition);
  }

  protected virtual void OnMouseOver()
  {
    if (!((Object) this.MouseOverSound != (Object) null))
      return;
    GUISound.Instance.PlayOnce(this.MouseOverSound);
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (!this.HandleMouseInput || mouseKey != KeyCode.Mouse0 || !this.wasMouseDown)
      return false;
    this.wasMouseDown = false;
    bool flag = false;
    if (this.IsPressed && this.Contains(mousePosition))
      flag = true;
    this.IsPressed = false;
    if (flag && this.Handler != null)
      this.Handler();
    if ((Object) this.PressSound != (Object) null)
      GUISound.Instance.PlayOnce(this.PressSound);
    return flag;
  }
}
