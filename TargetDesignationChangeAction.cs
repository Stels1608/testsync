﻿// Decompiled with JetBrains decompiler
// Type: TargetDesignationChangeAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;

public class TargetDesignationChangeAction : Action<Message>
{
  private TargetSelectionDataProvider selectionDataProvider;

  public override void Execute(IMessage<Message> message)
  {
    this.selectionDataProvider = (TargetSelectionDataProvider) this.OwnerFacade.FetchDataProvider("TargetSelectionProvider");
    KeyValuePair<ISpaceEntity, bool> keyValuePair = (KeyValuePair<ISpaceEntity, bool>) message.Data;
    if (keyValuePair.Value)
      this.selectionDataProvider.AddDesignatedTarget(keyValuePair.Key);
    else
      this.selectionDataProvider.RemoveDesignatedTarget(keyValuePair.Key);
    this.OwnerFacade.SendMessage(Message.TargetDesignationChanged, (object) keyValuePair);
  }
}
