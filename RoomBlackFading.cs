﻿// Decompiled with JetBrains decompiler
// Type: RoomBlackFading
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class RoomBlackFading : GUIPanel
{
  private float currentFadingAlpha;
  private Texture2D blackTexture;
  private RoomBlackFading.dHandler handlerFinish;

  public RoomBlackFading()
    : base((SmartRect) null)
  {
    this.blackTexture = TextureCache.Get(Color.black);
  }

  public void ActiveFading(float alpha)
  {
    this.currentFadingAlpha = alpha;
    this.IsRendered = true;
  }

  public void DeactiveFading()
  {
    this.IsRendered = false;
  }

  public void StartFading(RoomBlackFading.dHandler finish)
  {
    this.handlerFinish = finish;
    this.currentFadingAlpha = 1f;
    this.IsRendered = true;
    this.IsUpdated = true;
  }

  public override void Update()
  {
    this.currentFadingAlpha -= Time.deltaTime;
    if ((double) this.currentFadingAlpha < 0.100000001490116)
    {
      this.IsRendered = false;
      this.IsUpdated = false;
      this.currentFadingAlpha = 0.0f;
      if (this.handlerFinish != null)
        this.handlerFinish();
      this.handlerFinish = (RoomBlackFading.dHandler) null;
    }
    base.Update();
  }

  public override void Draw()
  {
    GUI.color = new Color(0.0f, 0.0f, 0.0f, this.currentFadingAlpha);
    GUI.DrawTexture(new Rect(0.0f, 0.0f, (float) Screen.width, (float) Screen.height), (Texture) this.blackTexture);
    GUI.color = Color.white;
    base.Draw();
  }

  public delegate void dHandler();
}
