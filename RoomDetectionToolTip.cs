﻿// Decompiled with JetBrains decompiler
// Type: RoomDetectionToolTip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;

public class RoomDetectionToolTip : GUIPanel
{
  private GUILabelNew textLabel;

  public string Text
  {
    get
    {
      return this.textLabel.Text;
    }
    set
    {
      this.textLabel.Text = value;
    }
  }

  public override bool IsRendered
  {
    get
    {
      return base.IsRendered;
    }
    set
    {
      base.IsRendered = value;
      if (!value)
        return;
      this.Text = string.Empty;
    }
  }

  public RoomDetectionToolTip()
    : base((SmartRect) null)
  {
    this.IsUpdated = true;
    JWindowDescription jwindowDescription = Loaders.LoadResource("GUI/RoomGUI/gui_mouseover_layout");
    GUIImageNew guiImageNew = (jwindowDescription["background_image"] as JImage).CreateGUIImageNew(this.root);
    this.root.Width = guiImageNew.Rect.width;
    this.root.Height = guiImageNew.Rect.height;
    this.AddPanel((GUIPanel) guiImageNew);
    this.textLabel = (jwindowDescription["text_label"] as JLabel).CreateGUILabelNew(this.root);
    this.textLabel.AutoSize = true;
    this.AddPanel((GUIPanel) this.textLabel);
  }

  public override void Update()
  {
    if (this.IsRendered)
    {
      this.Position = MouseSetup.MousePositionGui + new float2(this.SmartRect.Width / 2f, this.SmartRect.Height / 2f);
      this.RecalculateAbsCoords();
    }
    base.Update();
  }

  public override bool Contains(float2 point)
  {
    return false;
  }
}
