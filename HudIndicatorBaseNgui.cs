﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorBaseNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class HudIndicatorBaseNgui : HudIndicatorBase
{
  protected UIRoot uiRoot;
  private UIPanel uiPanel;

  protected virtual int PanelDepth
  {
    get
    {
      return -9999;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.uiPanel = this.gameObject.AddComponent<UIPanel>();
    this.uiPanel.depth = this.PanelDepth;
    this.uiRoot = NGUITools.FindInParents<UIRoot>(this.gameObject);
    this.borderOffset = new Vector2(25f, 25f);
  }

  protected void LateUpdate()
  {
    this.RemoteLateUpdate();
  }

  public override void RemoteLateUpdate()
  {
    this.uiPanel.widgetsAreStatic = !this.hudIndicatorComponents.Exists((Predicate<IHudIndicatorComponent>) (comp =>
    {
      if (comp is HudIndicatorBaseComponentNgui)
        return ((HudIndicatorBaseComponentNgui) comp).RequiresAnimationNgui;
      return false;
    }));
    base.RemoteLateUpdate();
  }

  public override T AddHudIndicatorComponent<T>()
  {
    T hudIndicatorComponent = NGUITools.AddChild<T>(this.transform.gameObject);
    this.RegisterHudIndicatorComponent<T>(hudIndicatorComponent);
    return hudIndicatorComponent;
  }

  protected override void UpdateIndicatorPositionAndOrientation(Vector3 unityScreenCoordinates)
  {
    Vector3 vector3;
    if (this.isVisibleOnScreen.Value)
    {
      vector3 = unityScreenCoordinates;
    }
    else
    {
      float indicatorOrientation = this.GetIndicatorOrientation(unityScreenCoordinates);
      this.UpdateArrowOrientation(indicatorOrientation);
      vector3 = this.GetBorderClampedPosition(indicatorOrientation);
    }
    this.cachedIndicatorTransform.localPosition = (vector3 - new Vector3((float) Screen.width / 2f, (float) Screen.height / 2f, 0.0f)) * Mathf.Max(1f, (float) this.uiRoot.manualHeight / (float) Screen.height) + 0.5f * Vector3.one;
  }

  protected override bool IsInScreenArea(Vector3 targetScreenPos)
  {
    if ((double) targetScreenPos.x > 0.0 && (double) targetScreenPos.x < (double) Screen.width && (double) targetScreenPos.y > 0.0)
      return (double) targetScreenPos.y < (double) Screen.height;
    return false;
  }

  public override void SetOpacity(float alpha)
  {
    using (List<IHudIndicatorComponent>.Enumerator enumerator = this.hudIndicatorComponents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        HudIndicatorBaseComponentNgui baseComponentNgui = enumerator.Current as HudIndicatorBaseComponentNgui;
        if ((UnityEngine.Object) baseComponentNgui != (UnityEngine.Object) null)
          baseComponentNgui.ChangeOpacity(alpha);
      }
    }
  }

  protected override void EnableCombatGui(bool enable)
  {
    this.uiPanel.enabled = enable;
  }
}
