﻿// Decompiled with JetBrains decompiler
// Type: ObjectiveEntryOld
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class ObjectiveEntryOld : MonoBehaviour
{
  public UILabel nameLabel;
  public UILabel progressCurrentLabel;
  public UILabel progressSeparatorLabel;
  public UILabel progressMaxLabel;
  private int nameLabelWidth;

  private void Awake()
  {
    this.nameLabelWidth = this.nameLabel.width;
  }

  public void SetMission(Mission mission)
  {
    if (!string.IsNullOrEmpty(mission.missionCard.Action) && mission.missionCard.Action != "undefined")
    {
      this.EnableProgress(false);
      NGUIToolsExtension.SetTextClamped(this.nameLabel, BsgoLocalization.Get("bgo.text_actions." + mission.missionCard.Action));
    }
    else if (mission.countables.Count == 0)
    {
      this.EnableProgress(false);
      NGUIToolsExtension.SetTextClamped(this.nameLabel, mission.Name);
    }
    else
      this.EnableProgress(true);
    Color color1 = mission.Status == Mission.State.Completed || mission.Status == Mission.State.Submitting ? ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_POSITIVE) : Color.white;
    UILabel uiLabel = this.nameLabel;
    Color color2 = color1;
    this.progressMaxLabel.color = color2;
    Color color3 = color2;
    this.progressSeparatorLabel.color = color3;
    Color color4 = color3;
    this.progressCurrentLabel.color = color4;
    Color color5 = color4;
    uiLabel.color = color5;
  }

  public void SetObjective(MissionCountable countable, Mission mission)
  {
    string text = BsgoLocalization.Get("bgo.text_counters." + countable.Counter.Name);
    if (mission.sectorCard != null)
      text = text + ": " + mission.SectorName;
    NGUIToolsExtension.SetTextClamped(this.nameLabel, text);
    int num1 = countable.Count;
    int num2 = countable.NeedCount;
    if (mission.Status == Mission.State.Completed || num1 > num2)
      num1 = num2;
    this.progressCurrentLabel.text = num1.ToString() + string.Empty;
    this.progressMaxLabel.text = num2.ToString() + string.Empty;
    Color color1 = num1 != num2 ? Color.white : ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_POSITIVE);
    UILabel uiLabel = this.nameLabel;
    Color color2 = color1;
    this.progressMaxLabel.color = color2;
    Color color3 = color2;
    this.progressSeparatorLabel.color = color3;
    Color color4 = color3;
    this.progressCurrentLabel.color = color4;
    Color color5 = color4;
    uiLabel.color = color5;
  }

  public void EnableProgress(bool enable)
  {
    UILabel uiLabel = this.progressCurrentLabel;
    bool flag1 = enable;
    this.progressMaxLabel.enabled = flag1;
    bool flag2 = flag1;
    this.progressSeparatorLabel.enabled = flag2;
    int num = flag2 ? 1 : 0;
    uiLabel.enabled = num != 0;
    this.nameLabel.width = !enable ? this.nameLabelWidth + 60 : this.nameLabelWidth;
  }
}
