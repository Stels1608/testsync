﻿// Decompiled with JetBrains decompiler
// Type: AsteroidDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AsteroidDesc : Desc, IProtocolRead
{
  public float rotationSpeed = 3f;
  public float radius;
  public string modelName;
  public string matSuffix;
  public int version;

  public AsteroidDesc()
    : base("Asteroid")
  {
  }

  public AsteroidDesc(Vector3 position, float radius, int version)
    : this()
  {
    this.position = position;
    this.radius = radius;
    this.version = version;
  }

  public void Read(BgoProtocolReader pr)
  {
    this.id = pr.ReadUInt32();
    this.position = pr.ReadVector3();
    this.radius = pr.ReadSingle();
    this.modelName = pr.ReadString();
    this.matSuffix = pr.ReadString();
    this.rotationSpeed = pr.ReadSingle();
  }
}
