﻿// Decompiled with JetBrains decompiler
// Type: AsteroidMiningNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class AsteroidMiningNotification : OnScreenNotification
{
  private readonly string message;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.AsteroidMining;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Neutral;
    }
  }

  public override string TextMessage
  {
    get
    {
      return this.message;
    }
  }

  public override bool Show
  {
    get
    {
      return true;
    }
  }

  public AsteroidMiningNotification(ItemCountable resource)
  {
    if (resource == null || (int) resource.Count == 0)
    {
      this.message = BsgoLocalization.Get("%$bgo.notif.mineral_analysis_nothing%");
    }
    else
    {
      this.message = BsgoLocalization.Get("%$bgo.notif.mineral_analysis%") + " " + (object) resource.Count;
      switch ((ResourceType) resource.CardGUID)
      {
        case ResourceType.Water:
          AsteroidMiningNotification miningNotification1 = this;
          string str1 = miningNotification1.message + " " + BsgoLocalization.Get("%$bgo.common.water_c%");
          miningNotification1.message = str1;
          break;
        case ResourceType.Titanium:
          AsteroidMiningNotification miningNotification2 = this;
          string str2 = miningNotification2.message + " " + BsgoLocalization.Get("%$bgo.common.titanium_c%");
          miningNotification2.message = str2;
          break;
        case ResourceType.Tylium:
          AsteroidMiningNotification miningNotification3 = this;
          string str3 = miningNotification3.message + " " + BsgoLocalization.Get("%$bgo.common.tylium_c%");
          miningNotification3.message = str3;
          break;
        case ResourceType.Cubits:
          AsteroidMiningNotification miningNotification4 = this;
          string str4 = miningNotification4.message + " " + BsgoLocalization.Get("%$bgo.common.cubits_c%");
          miningNotification4.message = str4;
          break;
        default:
          this.message += "<UNKNOWN RESOURCE>";
          break;
      }
    }
  }
}
