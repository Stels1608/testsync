﻿// Decompiled with JetBrains decompiler
// Type: DebugShipScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class DebugShipScript : MonoBehaviour
{
  public float Volume = 1f;
  public float MinDistance = 1f;
  public AudioClip[] HitClips;
  public int ActiveClipCount;
  private AudioSource hitAudio;
  private int clipIndex;
  private float endTime;
  private bool playingNow;

  private void Hit(float duration)
  {
    if ((Object) this.hitAudio == (Object) null)
      this.hitAudio = this.gameObject.AddComponent<AudioSource>();
    this.hitAudio.volume = this.Volume;
    this.hitAudio.minDistance = this.MinDistance;
    this.hitAudio.maxDistance = this.hitAudio.minDistance * 256f;
    this.endTime = Time.time + duration;
    if (this.playingNow)
      return;
    this.StartCoroutine(this.SoundLoop());
  }

  [DebuggerHidden]
  private IEnumerator SoundLoop()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new DebugShipScript.\u003CSoundLoop\u003Ec__Iterator31() { \u003C\u003Ef__this = this };
  }
}
