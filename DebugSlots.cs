﻿// Decompiled with JetBrains decompiler
// Type: DebugSlots
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugSlots : DebugBehaviour<DebugSlots>
{
  private string str = string.Empty;
  private Vector2 scroll = new Vector2();

  private void Start()
  {
    this.windowID = 23;
    this.SetSize(430f, 200f);
  }

  protected override void WindowFunc()
  {
    this.scroll = GUILayout.BeginScrollView(this.scroll);
    foreach (ShipSlot slot in Game.Me.ActiveShip.Slots)
    {
      if (slot.System != null)
      {
        GUILayout.Label(slot.System.ItemGUICard.Name + " Lvl ");
        this.str = GUILayout.TextField(slot.System.Card.Level.ToString());
        byte result = slot.System.Card.Level;
        byte.TryParse(this.str, out result);
        if ((int) result != (int) slot.System.Card.Level)
        {
          if ((int) result < 1)
            result = (byte) 1;
          else if ((int) result > (int) slot.System.Card.MaxLevel)
            result = slot.System.Card.MaxLevel;
          DebugProtocol.GetProtocol().UpgradeSystem(slot.System, result);
        }
      }
    }
    GUILayout.EndScrollView();
  }
}
