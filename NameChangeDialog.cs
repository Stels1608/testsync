﻿// Decompiled with JetBrains decompiler
// Type: NameChangeDialog
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NameChangeDialog : CharacterServiceDialog
{
  public InputField nameField;
  private string cachedName;

  protected override void RegisterUiEventHandlers()
  {
    this.cancelButton.onClick.AddListener((UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.CancelNameChange)));
    this.confirmButton.onClick.AddListener(new UnityAction(this.Validate));
    EventTrigger eventTrigger = this.nameField.gameObject.AddComponent<EventTrigger>();
    EventTrigger.Entry entry = new EventTrigger.Entry() { eventID = EventTriggerType.Select, callback = new EventTrigger.TriggerEvent() };
    entry.callback.AddListener(new UnityAction<BaseEventData>(this.OnNameFieldClicked));
    eventTrigger.triggers = new List<EventTrigger.Entry>();
    eventTrigger.triggers.Add(entry);
    this.nameField.placeholder.enabled = false;
    this.nameField.onValidateInput += new InputField.OnValidateInput(this.OnValidateInput);
  }

  private char OnValidateInput(string text, int charIndex, char addedChar)
  {
    if (Tools.ValidNameChar(addedChar))
      return addedChar;
    return char.MinValue;
  }

  protected override void OnCharacterServiceInjected()
  {
    this.Localize("name_change");
  }

  private void OnNameFieldClicked(BaseEventData eventData)
  {
    if (this.cachedName == null)
      return;
    this.nameField.placeholder.enabled = false;
    this.nameField.text = this.cachedName;
    this.cachedName = (string) null;
  }

  public void InvalidName()
  {
    this.gameObject.SetActive(true);
    this.cachedName = this.nameField.text;
    InputField inputField = this.nameField;
    string str1 = string.Empty;
    this.nameField.textComponent.text = str1;
    string str2 = str1;
    inputField.text = str2;
    this.nameField.placeholder.GetComponent<Text>().text = BsgoLocalization.Get("bgo.gui_avatar_common.name_rejected");
    this.nameField.placeholder.enabled = true;
  }

  private void Validate()
  {
    if (string.IsNullOrEmpty(this.nameField.text))
      this.InvalidName();
    else if (this.nameField.text.ToLowerInvariant() == Game.Me.Name.ToLowerInvariant())
    {
      this.InvalidName();
    }
    else
    {
      this.gameObject.SetActive(false);
      DialogBoxUi dialogBox = MessageBoxFactory.CreateDialogBox(BsgoCanvas.CharacterMenu);
      dialogBox.SetContent((OnMessageBoxClose) (actionType =>
      {
        if (actionType == MessageBoxActionType.Ok)
          PlayerProtocol.GetProtocol().ChangeName(this.nameField.text);
        else
          FacadeFactory.GetInstance().SendMessage(Message.CancelNameChange);
      }), "%$bgo.character_menu.name_change.title%", BsgoLocalization.Get("bgo.character_menu.name_change.confirm", (object) ("<color=#fcc857><size=20>" + this.nameField.text + "</size></color>")), 0U);
      dialogBox.ShowTimer(false);
    }
  }
}
