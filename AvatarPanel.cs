﻿// Decompiled with JetBrains decompiler
// Type: AvatarPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class AvatarPanel : GuiPanel
{
  public override bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    this.KeyDown(keyboardKey, action);
    if (keyboardKey != KeyCode.J)
      return false;
    this.IsRendered = !this.IsRendered;
    return true;
  }
}
