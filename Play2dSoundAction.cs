﻿// Decompiled with JetBrains decompiler
// Type: Play2dSoundAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class Play2dSoundAction : Action<Message>
{
  public override void Execute(IMessage<Message> message)
  {
    AudioSource.PlayClipAtPoint((AudioClip) Resources.Load((string) message.Data), Camera.main.transform.position);
  }
}
