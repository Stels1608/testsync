﻿// Decompiled with JetBrains decompiler
// Type: HelpScreenType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum HelpScreenType
{
  BasicControls = 0,
  First = 0,
  AdvancedControls = 1,
  NPCInteraction = 2,
  DailyAssignments = 3,
  FTLJump = 4,
  Mining = 5,
  StoryMissions = 6,
  IndustrialMining = 7,
  Duties = 8,
  BuyingNewShip = 9,
  Attacking = 10,
  Shop = 11,
  Repair = 12,
  UpgradeSystems = 13,
  BuyingCubits = 14,
  DradisContact = 15,
  FTLMission = 16,
  Last = 16,
}
