﻿// Decompiled with JetBrains decompiler
// Type: LoadAllAssets
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class LoadAllAssets : MonoBehaviour
{
  private readonly HashSet<string> existingAssets = new HashSet<string>();
  private const string LOCAL_BUNDLE_FOLDER = "/../assetbundles/";

  private void Start()
  {
    this.StartCoroutine(this.WaitForAssetMap());
  }

  [DebuggerHidden]
  private IEnumerator WaitForAssetMap()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LoadAllAssets.\u003CWaitForAssetMap\u003Ec__IteratorC() { \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator LoadAll()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new LoadAllAssets.\u003CLoadAll\u003Ec__IteratorD() { \u003C\u003Ef__this = this };
  }
}
