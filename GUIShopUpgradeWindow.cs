﻿// Decompiled with JetBrains decompiler
// Type: GUIShopUpgradeWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GUIShopUpgradeWindow : GUIPanel
{
  private readonly Texture2D tyliumImg = Resources.Load("GUI/Common/thylium") as Texture2D;
  private readonly Texture2D titaniumImg = Resources.Load("GUI/Common/titanium") as Texture2D;
  private readonly Texture2D cubitsImg = Resources.Load("GUI/Common/cubits") as Texture2D;
  private readonly Texture2D tokenImg = Resources.Load("GUI/Common/token") as Texture2D;
  private readonly Texture2D tuningKitsImg = Resources.Load("GUI/Common/tuningKit") as Texture2D;
  private List<GUIImageNew> currencyImages = new List<GUIImageNew>();
  private List<GUILabelNew> currencyLabels = new List<GUILabelNew>();
  private readonly GUILabelNew itemNameLabel;
  private readonly GUIImageNew currency1Image;
  private readonly GUILabelNew currency1Label;
  private readonly GUIImageNew currency2Image;
  private readonly GUILabelNew currency2Label;
  private readonly GUIButtonNew yesButton;
  private ShipSystem system;
  private int upgradeToLevel;

  public AnonymousDelegate HandlerYes
  {
    set
    {
      this.yesButton.Handler = value;
    }
  }

  public ShipSystem System
  {
    get
    {
      return this.system;
    }
  }

  public int UpgradeToLevel
  {
    get
    {
      return this.upgradeToLevel;
    }
  }

  public GUIShopUpgradeWindow(SmartRect parent)
  {
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/EquipBuyPanel/gui_upgrade_layout");
    GUIImageNew guiImageNew = (jwindowDescription["background_image"] as JImage).CreateGUIImageNew(this.root);
    this.root.Width = guiImageNew.SmartRect.Width;
    this.root.Height = guiImageNew.SmartRect.Height;
    this.AddPanel((GUIPanel) guiImageNew);
    this.AddPanel((GUIPanel) (jwindowDescription["title_label"] as JLabel).CreateGUILabelNew(this.root));
    this.itemNameLabel = (jwindowDescription["item_name_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.itemNameLabel);
    this.currency1Image = (jwindowDescription["currency1_image"] as JImage).CreateGUIImageNew(this.root);
    this.currencyImages.Add(this.currency1Image);
    this.AddPanel((GUIPanel) this.currency1Image);
    this.currency1Label = (jwindowDescription["currency1_label"] as JLabel).CreateGUILabelNew(this.root);
    this.currencyLabels.Add(this.currency1Label);
    this.AddPanel((GUIPanel) this.currency1Label);
    this.currency2Image = (jwindowDescription["currency2_image"] as JImage).CreateGUIImageNew(this.root);
    this.currencyImages.Add(this.currency2Image);
    this.AddPanel((GUIPanel) this.currency2Image);
    this.currency2Label = (jwindowDescription["currency2_label"] as JLabel).CreateGUILabelNew(this.root);
    this.currencyLabels.Add(this.currency2Label);
    this.AddPanel((GUIPanel) this.currency2Label);
    this.yesButton = (jwindowDescription["yes_button"] as JButton).CreateGUIButtonNew(this.root);
    this.AddPanel((GUIPanel) this.yesButton);
    GUIButtonNew guiButtonNew = (jwindowDescription["cancel_button"] as JButton).CreateGUIButtonNew(this.root);
    guiButtonNew.Handler = new AnonymousDelegate(this.OnCancelButton);
    this.AddPanel((GUIPanel) guiButtonNew);
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    base.OnMouseDown(mousePosition, mouseKey);
    return this.IsRendered;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    base.OnMouseUp(mousePosition, mouseKey);
    return this.IsRendered;
  }

  private void FillCurrencyField(int fieldNo, float amount, Texture2D currencyTex)
  {
    if (fieldNo >= this.currencyLabels.Count)
      return;
    GUILabelNew guiLabelNew = this.currencyLabels[fieldNo];
    GUIImageNew guiImageNew = this.currencyImages[fieldNo];
    if ((double) amount == 0.0)
    {
      guiLabelNew.Hide();
      guiImageNew.Hide();
    }
    else
    {
      guiLabelNew.Show();
      guiImageNew.Show();
      guiLabelNew.Text = amount.ToString("#0.");
      guiImageNew.Texture = currencyTex;
    }
  }

  private void HideCurrencyFieldsStartingAt(int fieldNo)
  {
    for (int fieldNo1 = fieldNo; fieldNo1 < this.currencyLabels.Count; ++fieldNo1)
      this.FillCurrencyField(fieldNo1, 0.0f, (Texture2D) null);
  }

  private void UpdateCurrencyWidget()
  {
    CurrencyAmount priceToUpgrade = this.system.GetPriceToUpgrade(this.upgradeToLevel, true);
    int fieldNo = 0;
    if ((double) priceToUpgrade.Cubits > 0.0)
    {
      this.FillCurrencyField(fieldNo, priceToUpgrade.Cubits, this.cubitsImg);
      ++fieldNo;
    }
    if ((double) priceToUpgrade.Tylium > 0.0)
    {
      this.FillCurrencyField(fieldNo, priceToUpgrade.Tylium, this.tyliumImg);
      ++fieldNo;
    }
    if ((double) priceToUpgrade.Titanium > 0.0)
    {
      this.FillCurrencyField(fieldNo, priceToUpgrade.Titanium, this.titaniumImg);
      ++fieldNo;
    }
    if ((double) priceToUpgrade.Token > 0.0)
    {
      this.FillCurrencyField(fieldNo, priceToUpgrade.Token, this.tokenImg);
      ++fieldNo;
    }
    if ((double) priceToUpgrade.TuningKits > 0.0)
    {
      this.FillCurrencyField(fieldNo, priceToUpgrade.TuningKits, this.tuningKitsImg);
      ++fieldNo;
    }
    this.HideCurrencyFieldsStartingAt(fieldNo);
  }

  public override void Update()
  {
    if (this.system != null && (bool) this.system.IsLoaded)
    {
      this.itemNameLabel.Text = this.system.ItemGUICard.Name;
      this.UpdateCurrencyWidget();
    }
    else
    {
      this.itemNameLabel.Text = "???";
      this.HideCurrencyFieldsStartingAt(0);
    }
    this.itemNameLabel.SmartRect.Height = (float) TextUtility.CalcTextSize(this.itemNameLabel.Text, this.itemNameLabel.Font, this.itemNameLabel.SmartRect.Width).height;
    this.currency1Label.MakeCenteredRect();
    this.currency2Label.MakeCenteredRect();
    this.currency1Image.Position = new float2((float) (-(double) ((float) ((double) this.currency1Image.SmartRect.Width + 3.0 + (double) this.currency1Label.SmartRect.Width + 3.0 + (double) this.currency2Image.SmartRect.Width + 3.0) + this.currency2Label.SmartRect.Width) / 2.0 + (double) this.currency1Image.SmartRect.Width / 2.0), this.currency1Image.Position.y);
    this.currency1Label.PlaceRightOf((GUIPanel) this.currency1Image, 3f);
    this.currency2Image.PlaceRightOf((GUIPanel) this.currency1Label, 3f);
    this.currency2Label.PlaceRightOf((GUIPanel) this.currency2Image, 3f);
    this.RecalculateAbsCoords();
    base.Update();
  }

  public void ShowWindow(ShipSystem system, int level)
  {
    this.system = system;
    this.upgradeToLevel = level;
    this.IsRendered = true;
    this.IsUpdated = true;
    Game.InputDispatcher.Focused = (InputListener) this;
  }

  public override void Hide()
  {
    this.system = (ShipSystem) null;
    this.IsRendered = false;
    this.IsUpdated = false;
    Game.InputDispatcher.Focused = (InputListener) null;
  }

  protected void OnCancelButton()
  {
    this.Hide();
  }
}
