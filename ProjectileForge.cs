﻿// Decompiled with JetBrains decompiler
// Type: ProjectileForge
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ProjectileForge : MonoBehaviour
{
  public float shotDelayMin = 1f;
  public float shotDelayMax = 2f;
  public float projectileSpeed = 200f;
  public float burstAmount = 1f;
  private bool findNewtarget = true;
  public GameObject projectilePrefab;
  public GameObject impact;
  public Transform target;
  private Transform targetLast;
  public bool targetUseRandomChild;
  public float burstDelayMin;
  public float burstDelayMax;
  public float maxRandomFirecone;
  public float AutotargPreSpeed;
  public float AutotargTriggertimeMin;
  public float AutotargTriggertimeMax;
  private float time;
  private float timeNextSpawn;
  public Transform MuzzleSpot;
  public GameObject MuzzlePrefab;
  private static Transform _projectileForgeRoot;

  internal void Start()
  {
    this.time = 0.0f;
    this.timeNextSpawn = this.GetNextSpawnTime();
    this.targetLast = this.target;
  }

  internal void Update()
  {
    this.SanityChecks();
    this.time += Time.deltaTime;
    if ((double) this.time <= (double) this.timeNextSpawn)
      return;
    this.time = 0.0f;
    this.timeNextSpawn = this.GetNextSpawnTime();
    float time = 0.0f;
    this.findNewtarget = true;
    for (int index = 0; (double) index < (double) this.burstAmount; ++index)
    {
      this.Invoke("SpawnProjectile", time);
      time += (double) this.burstDelayMin > 0.0 ? Random.Range(this.burstDelayMin, this.burstDelayMax) : this.burstDelayMax;
    }
  }

  private float GetNextSpawnTime()
  {
    if ((double) this.shotDelayMin <= 0.0)
      return this.shotDelayMax;
    return Random.Range(this.shotDelayMin, this.shotDelayMax);
  }

  private void SanityChecks()
  {
    this.burstAmount = Mathf.Max(1f, this.burstAmount);
    this.shotDelayMax = Mathf.Clamp(this.shotDelayMax, 0.1f, 60f);
    this.projectileSpeed = Mathf.Clamp(this.projectileSpeed, 10f, 10000f);
  }

  private Transform NextTarget()
  {
    Transform transform = this.targetLast;
    if (this.targetUseRandomChild && this.findNewtarget)
    {
      if (this.target.childCount > 0)
        transform = this.target.GetChild(Random.Range(0, this.target.childCount));
      this.findNewtarget = false;
    }
    if (!this.targetUseRandomChild)
      transform = this.target;
    this.targetLast = transform;
    return transform;
  }

  public static Transform ProjectileForgeRoot()
  {
    if ((Object) ProjectileForge._projectileForgeRoot == (Object) null)
      ProjectileForge._projectileForgeRoot = new GameObject("ProjectileForgeRoot").transform;
    return ProjectileForge._projectileForgeRoot;
  }

  private void SpawnProjectile()
  {
    if ((Object) this.projectilePrefab != (Object) null && (Object) this.target != (Object) null)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.projectilePrefab);
      gameObject.transform.parent = ProjectileForge.ProjectileForgeRoot();
      gameObject.transform.position = this.transform.position;
      BallisticProjectile ballisticProjectile = gameObject.AddComponent<BallisticProjectile>();
      ballisticProjectile.speed = this.projectileSpeed;
      ballisticProjectile.impact = this.impact;
      ballisticProjectile.MaxRandomFirecone = this.maxRandomFirecone;
      ballisticProjectile.AutoTargetTime = Random.Range(this.AutotargTriggertimeMin, this.AutotargTriggertimeMax);
      ballisticProjectile.AutoTargetTimePreSpeed = this.AutotargPreSpeed;
      Transform target = this.NextTarget();
      ballisticProjectile.SetTarget(target);
    }
    if (!((Object) this.MuzzlePrefab != (Object) null))
      return;
    (Object.Instantiate((Object) this.MuzzlePrefab, !((Object) this.MuzzleSpot != (Object) null) ? this.transform.position : this.MuzzleSpot.position, !((Object) this.MuzzleSpot != (Object) null) ? this.transform.rotation : this.MuzzleSpot.rotation) as GameObject).transform.parent = ProjectileForge.ProjectileForgeRoot();
  }

  public static void StopAllProjectileForgeScripts()
  {
    foreach (Behaviour behaviour in Object.FindObjectsOfType<ProjectileForge>())
      behaviour.enabled = false;
  }

  public static void StopAllImpacts()
  {
    foreach (AbstractProjectile abstractProjectile in Object.FindObjectsOfType<BallisticProjectile>())
      abstractProjectile.impact = (GameObject) null;
  }
}
