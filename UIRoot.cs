﻿// Decompiled with JetBrains decompiler
// Type: UIRoot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("NGUI/UI/Root")]
[ExecuteInEditMode]
public class UIRoot : MonoBehaviour
{
  public static List<UIRoot> list = new List<UIRoot>();
  public int manualWidth = 1280;
  public int manualHeight = 720;
  public int minimumHeight = 320;
  public int maximumHeight = 1536;
  public bool fitHeight = true;
  public UIRoot.Scaling scalingStyle;
  public bool fitWidth;
  public bool adjustByDPI;
  public bool shrinkPortraitUI;
  private Transform mTrans;

  public UIRoot.Constraint constraint
  {
    get
    {
      if (this.fitWidth)
        return this.fitHeight ? UIRoot.Constraint.Fit : UIRoot.Constraint.FitWidth;
      return this.fitHeight ? UIRoot.Constraint.FitHeight : UIRoot.Constraint.Fill;
    }
  }

  public UIRoot.Scaling activeScaling
  {
    get
    {
      UIRoot.Scaling scaling = this.scalingStyle;
      if (scaling == UIRoot.Scaling.ConstrainedOnMobiles)
        return UIRoot.Scaling.Flexible;
      return scaling;
    }
  }

  public int activeHeight
  {
    get
    {
      if (this.activeScaling == UIRoot.Scaling.Flexible)
      {
        Vector2 screenSize = NGUITools.screenSize;
        float num = screenSize.x / screenSize.y;
        if ((double) screenSize.y < (double) this.minimumHeight)
        {
          screenSize.y = (float) this.minimumHeight;
          screenSize.x = screenSize.y * num;
        }
        else if ((double) screenSize.y > (double) this.maximumHeight)
        {
          screenSize.y = (float) this.maximumHeight;
          screenSize.x = screenSize.y * num;
        }
        int @int = Mathf.RoundToInt(!this.shrinkPortraitUI || (double) screenSize.y <= (double) screenSize.x ? screenSize.y : screenSize.y / num);
        if (this.adjustByDPI)
          return NGUIMath.AdjustByDPI((float) @int);
        return @int;
      }
      UIRoot.Constraint constraint = this.constraint;
      if (constraint == UIRoot.Constraint.FitHeight)
        return this.manualHeight;
      Vector2 screenSize1 = NGUITools.screenSize;
      float num1 = screenSize1.x / screenSize1.y;
      float num2 = (float) this.manualWidth / (float) this.manualHeight;
      switch (constraint)
      {
        case UIRoot.Constraint.Fit:
          if ((double) num2 > (double) num1)
            return Mathf.RoundToInt((float) this.manualWidth / num1);
          return this.manualHeight;
        case UIRoot.Constraint.Fill:
          if ((double) num2 < (double) num1)
            return Mathf.RoundToInt((float) this.manualWidth / num1);
          return this.manualHeight;
        case UIRoot.Constraint.FitWidth:
          return Mathf.RoundToInt((float) this.manualWidth / num1);
        default:
          return this.manualHeight;
      }
    }
  }

  public float pixelSizeAdjustment
  {
    get
    {
      return this.GetPixelSizeAdjustment(Mathf.RoundToInt(NGUITools.screenSize.y));
    }
  }

  public static float GetPixelSizeAdjustment(GameObject go)
  {
    UIRoot inParents = NGUITools.FindInParents<UIRoot>(go);
    if ((Object) inParents != (Object) null)
      return inParents.pixelSizeAdjustment;
    return 1f;
  }

  public float GetPixelSizeAdjustment(int height)
  {
    height = Mathf.Max(2, height);
    if (this.activeScaling == UIRoot.Scaling.Constrained)
      return (float) this.activeHeight / (float) height;
    if (height < this.minimumHeight)
      return (float) this.minimumHeight / (float) height;
    if (height > this.maximumHeight)
      return (float) this.maximumHeight / (float) height;
    return 1f;
  }

  protected virtual void Awake()
  {
    this.mTrans = this.transform;
  }

  protected virtual void OnEnable()
  {
    UIRoot.list.Add(this);
  }

  protected virtual void OnDisable()
  {
    UIRoot.list.Remove(this);
  }

  protected virtual void Start()
  {
    UIOrthoCamera componentInChildren = this.GetComponentInChildren<UIOrthoCamera>();
    if ((Object) componentInChildren != (Object) null)
    {
      Debug.LogWarning((object) "UIRoot should not be active at the same time as UIOrthoCamera. Disabling UIOrthoCamera.", (Object) componentInChildren);
      Camera component = componentInChildren.gameObject.GetComponent<Camera>();
      componentInChildren.enabled = false;
      if (!((Object) component != (Object) null))
        return;
      component.orthographicSize = 1f;
    }
    else
      this.Update();
  }

  private void Update()
  {
    if (!((Object) this.mTrans != (Object) null))
      return;
    float num1 = (float) this.activeHeight;
    if ((double) num1 <= 0.0)
      return;
    float num2 = 2f / num1;
    Vector3 localScale = this.mTrans.localScale;
    if ((double) Mathf.Abs(localScale.x - num2) <= 1.40129846432482E-45 && (double) Mathf.Abs(localScale.y - num2) <= 1.40129846432482E-45 && (double) Mathf.Abs(localScale.z - num2) <= 1.40129846432482E-45)
      return;
    this.mTrans.localScale = new Vector3(num2, num2, num2);
  }

  public static void Broadcast(string funcName)
  {
    int index = 0;
    for (int count = UIRoot.list.Count; index < count; ++index)
    {
      UIRoot uiRoot = UIRoot.list[index];
      if ((Object) uiRoot != (Object) null)
        uiRoot.BroadcastMessage(funcName, SendMessageOptions.DontRequireReceiver);
    }
  }

  public static void Broadcast(string funcName, object param)
  {
    if (param == null)
    {
      Debug.LogError((object) "SendMessage is bugged when you try to pass 'null' in the parameter field. It behaves as if no parameter was specified.");
    }
    else
    {
      int index = 0;
      for (int count = UIRoot.list.Count; index < count; ++index)
      {
        UIRoot uiRoot = UIRoot.list[index];
        if ((Object) uiRoot != (Object) null)
          uiRoot.BroadcastMessage(funcName, param, SendMessageOptions.DontRequireReceiver);
      }
    }
  }

  public enum Scaling
  {
    Flexible,
    Constrained,
    ConstrainedOnMobiles,
  }

  public enum Constraint
  {
    Fit,
    Fill,
    FitWidth,
    FitHeight,
  }
}
