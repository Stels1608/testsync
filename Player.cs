﻿// Decompiled with JetBrains decompiler
// Type: Player
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;

public class Player
{
  public string Name = string.Empty;
  public string ShipName = string.Empty;
  private uint serverID;
  private uint requestSent;
  private uint subscriptions;
  private PlayerShip playerShip;
  public BgoAdminRoles Roles;
  public Flag IsLoaded;
  public Flag NameLoaded;
  public Faction Faction;
  public DateTime lastLogout;
  public AvatarDescription AvatarDesc;
  public uint guildId;
  public string guildName;
  public GuildRole GuildRole;
  public ShipCard[] ShipCards;
  public bool Online;
  public byte Level;
  public TitleCard ActiveTitleCard;
  private bool nemesis;
  private TournamentIndicator tournamentIndicator;
  public SpaceSubscribeInfo Stats;
  public GameLocation Location;
  public GUICard Sector;
  public GUICard Room;
  private uint anchorTargetPlayerId;
  private Flag isPlayerShipLoaded;

  public PlayerShip PlayerShip
  {
    get
    {
      return this.playerShip;
    }
  }

  public PvpMedal PvpMedal { get; private set; }

  public TournamentMedal TournamentMedal { get; private set; }

  public KillerMedal KillerMedal { get; private set; }

  public AssistMedal AssistMedal { get; private set; }

  public bool Nemesis
  {
    get
    {
      return this.nemesis;
    }
    set
    {
      bool flag = this.nemesis != value;
      this.nemesis = value;
      if (this.IsMe || !flag)
        return;
      FacadeFactory.GetInstance().SendMessage(Message.PlayersMedalsChanged, (object) this);
    }
  }

  public TournamentIndicator TournamentIndicator
  {
    get
    {
      return this.tournamentIndicator;
    }
    set
    {
      bool flag = this.tournamentIndicator != value;
      this.tournamentIndicator = value;
      if (!this.IsMe && flag)
        FacadeFactory.GetInstance().SendMessage(Message.PlayersMedalsChanged, (object) this);
      this.InfoReceived(Player.InfoType.TournamentIndicator);
    }
  }

  public bool IsAnchored
  {
    get
    {
      return (int) this.anchorTargetPlayerId != 0;
    }
  }

  public bool HasGuild
  {
    get
    {
      return (int) this.guildId != 0;
    }
  }

  public string Guild
  {
    get
    {
      if (this.HasGuild)
        return this.guildName;
      return (string) null;
    }
  }

  public bool IsMe
  {
    get
    {
      return (int) this.serverID == (int) Game.Me.ServerID;
    }
  }

  public bool IsModerator
  {
    get
    {
      return (this.Roles & BgoAdminRoles.Mod) == BgoAdminRoles.Mod;
    }
  }

  public bool IsSameSector
  {
    get
    {
      SpaceLevel level = SpaceLevel.GetLevel();
      if (this.Sector != null && (UnityEngine.Object) level != (UnityEngine.Object) null)
        return (int) this.Sector.CardGUID == (int) level.Card.CardGUID;
      return false;
    }
  }

  public uint ServerID
  {
    get
    {
      return this.serverID;
    }
  }

  public string Title
  {
    get
    {
      if (this.ActiveTitleCard != null && (bool) this.ActiveTitleCard.IsLoaded)
        return this.ActiveTitleCard.Name;
      return string.Empty;
    }
  }

  public string Rank
  {
    get
    {
      return BsgoLocalization.GetForLevel("bgo.common.rank." + (this.Faction != Faction.Colonial ? "cylon" : "colonial"), (int) this.Level);
    }
  }

  public Player(uint serverID)
  {
    this.serverID = serverID;
    this.IsLoaded = new Flag();
    this.NameLoaded = new Flag();
    this.isPlayerShipLoaded = new Flag();
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.isPlayerShipLoaded
    });
    this.IsLoaded.Set();
    this.Stats = new SpaceSubscribeInfo(serverID);
  }

  public void SetPlayerShip(PlayerShip playerShip)
  {
    this.playerShip = playerShip;
    playerShip.IsLoaded.AddHandler((SignalHandler) (() => this.isPlayerShipLoaded.Set()));
    playerShip.IsLoaded.Set();
  }

  public void AnchorTo(uint carrierId)
  {
    this.anchorTargetPlayerId = carrierId;
    this.IsLoaded.AddHandler((SignalHandler) (() => this.PlayerShip.ChangeVisibility(false, ChangeVisibilityReason.Anchor)));
    this.IsLoaded.Set();
  }

  public void Unanchor()
  {
    this.anchorTargetPlayerId = 0U;
    this.PlayerShip.ChangeVisibility(true, ChangeVisibilityReason.Anchor);
  }

  public string GetTaggedName(string origName)
  {
    if ((this.Roles & BgoAdminRoles.Developer) == BgoAdminRoles.Developer)
      return "(DEV) " + origName;
    if ((this.Roles & BgoAdminRoles.CommunityManager) == BgoAdminRoles.CommunityManager)
      return "(CM) " + origName;
    if ((this.Roles & BgoAdminRoles.Mod) == BgoAdminRoles.Mod)
      return "(MOD)" + origName;
    return origName;
  }

  public void SetMyTarget()
  {
    SpaceLevel level = SpaceLevel.GetLevel();
    if (!((UnityEngine.Object) level != (UnityEngine.Object) null))
      return;
    foreach (PlayerShip playerShip in level.GetObjectRegistry().GetLoaded<PlayerShip>())
    {
      if (playerShip.Player == this)
        TargetSelector.SelectTargetRequest((SpaceObject) playerShip, false);
    }
  }

  public void Request(Player.InfoType types)
  {
    uint flags = (uint) (types & (Player.InfoType) ~(int) this.requestSent);
    SubscribeProtocol.GetProtocol().RequestInfo(this.ServerID, flags);
    this.requestSent |= flags;
  }

  public void Subscribe(Player.InfoType types)
  {
    uint flags = (uint) (types & (Player.InfoType) ~(int) this.subscriptions);
    SubscribeProtocol.GetProtocol().SubscribeInfo(this.ServerID, flags);
    this.subscriptions |= flags;
  }

  public void Unsubscribe(Player.InfoType types)
  {
    uint flags = (uint) (types & (Player.InfoType) this.subscriptions);
    SubscribeProtocol.GetProtocol().UnsubscribeInfo(this.ServerID, flags);
    this.subscriptions &= ~flags;
  }

  public void _SetName(string name)
  {
    this.Name = name;
    this.InfoReceived(Player.InfoType.Name);
    this.IsLoaded.Set();
    this.NameLoaded.Set();
  }

  public void _SetFaction(Faction faction)
  {
    this.Faction = faction;
    this.InfoReceived(Player.InfoType.Faction);
    this.IsLoaded.Set();
  }

  public void _SetAvatar(AvatarDescription desc)
  {
    this.AvatarDesc = desc;
    this.InfoReceived(Player.InfoType.Avatar);
  }

  public void _SetLevel(byte level)
  {
    this.Level = level;
    this.InfoReceived(Player.InfoType.Level);
  }

  public void _SetLogout(DateTime logout)
  {
    this.lastLogout = logout;
    this.InfoReceived(Player.InfoType.Logout);
  }

  public void _SetTitle(uint titleGUID)
  {
    this.ActiveTitleCard = (TitleCard) Game.Catalogue.FetchCard(titleGUID, CardView.Title);
    if (this.ActiveTitleCard != null && !this.IsMe)
      this.ActiveTitleCard.IsLoaded.AddHandler((SignalHandler) (() => FacadeFactory.GetInstance().SendMessage(Message.PlayersTitleChanged, (object) this)));
    this.InfoReceived(Player.InfoType.Title);
  }

  public void _SetShips(string shipName, uint[] shipGUIDs)
  {
    this.ShipName = shipName;
    this.ShipCards = new ShipCard[shipGUIDs.Length];
    for (int index = 0; index < shipGUIDs.Length; ++index)
      this.ShipCards[index] = (ShipCard) Game.Catalogue.FetchCard(shipGUIDs[index], CardView.Ship);
    this.InfoReceived(Player.InfoType.Ships);
  }

  public void _SetWing(uint wingID, string wingName, GuildRole role)
  {
    this.guildId = wingID;
    this.guildName = wingName;
    this.GuildRole = role;
    if (this.IsMe)
      Game.Me.Guild._SetGuildRole(role);
    else
      FacadeFactory.GetInstance().SendMessage(Message.PlayerChangedGuildStatus, (object) this);
    this.InfoReceived(Player.InfoType.Wing);
  }

  public void _SetOnlineStatus(bool newOnlineStatus)
  {
    if (this.Online != newOnlineStatus && !this.IsMe)
    {
      if (Game.Me.Friends.IsFriend(this))
      {
        string text;
        if (newOnlineStatus)
          text = BsgoLocalization.Get("%$bgo.chat.message_friend_joined%", (object) this.Name);
        else
          text = BsgoLocalization.Get("%$bgo.chat.message_friend_left%", (object) this.Name);
        Game.ChatStorage.SystemSays(text);
      }
      if (Game.Me.Guild != null && Game.Me.Guild.Players.Contains(this))
      {
        string text;
        if (newOnlineStatus)
          text = BsgoLocalization.Get("%$bgo.chat.message_guildmate_joined%", (object) this.Name);
        else
          text = BsgoLocalization.Get("%$bgo.chat.message_guildmate_left%", (object) this.Name);
        Game.ChatStorage.SystemSays(text);
      }
    }
    this.Online = newOnlineStatus;
    this.InfoReceived(Player.InfoType.Status);
  }

  public void _SetLocation(GameLocation location, uint sectorGUID, uint roomGUID)
  {
    this.Location = location;
    this.Sector = (GUICard) Game.Catalogue.FetchCard(sectorGUID, CardView.GUI);
    this.Room = (GUICard) Game.Catalogue.FetchCard(roomGUID, CardView.GUI);
    this._SetOnlineStatus(location != GameLocation.Disconnect && location != GameLocation.Unknown);
    this.InfoReceived(Player.InfoType.Location);
  }

  public void _SetMedals(PvpMedal arena, TournamentMedal tournament, KillerMedal killerMedal, AssistMedal assistMedal)
  {
    PvpMedal pvpMedal = !Game.Me.TournamentParticipant ? arena : PvpMedal.None;
    bool flag = this.PvpMedal != pvpMedal && this.TournamentMedal != tournament && this.KillerMedal != killerMedal && assistMedal != this.AssistMedal;
    this.PvpMedal = pvpMedal;
    this.TournamentMedal = tournament;
    this.KillerMedal = killerMedal;
    this.AssistMedal = assistMedal;
    if (!this.IsMe && flag)
      FacadeFactory.GetInstance().SendMessage(Message.PlayersMedalsChanged, (object) this);
    this.InfoReceived(Player.InfoType.Medal);
  }

  private bool IsRequestSent(Player.InfoType type)
  {
    return ((Player.InfoType) this.requestSent & type) != (Player.InfoType) 0;
  }

  private bool IsSubscribed(Player.InfoType type)
  {
    return ((Player.InfoType) this.subscriptions & type) != (Player.InfoType) 0;
  }

  private void InfoReceived(Player.InfoType type)
  {
    if ((type & Player.InfoType.ConstantInfo) != (Player.InfoType) 0)
      return;
    Player player = this;
    int num = (int) ((Player.InfoType) player.requestSent & ~type);
    player.requestSent = (uint) num;
  }

  public enum InfoType : uint
  {
    Name = 1,
    Faction = 2,
    Avatar = 4,
    ConstantInfo = 7,
    Wing = 8,
    Ships = 16,
    [Obsolete("@deprecated: use Location instead")] Status = 32,
    Level = 64,
    Title = 128,
    Location = 256,
    FriendWindowInfo = 320,
    PartyMemberSubscription = 336,
    PartyMemberRequest = 339,
    Medal = 512,
    ProfileRequest = 735,
    Stats = 1024,
    Logout = 2048,
    TournamentIndicator = 4096,
    SpaceSubscription = 4808,
    SpaceRequest = 4827,
  }
}
