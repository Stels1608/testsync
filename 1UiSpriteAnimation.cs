﻿// Decompiled with JetBrains decompiler
// Type: UiSpriteAnimation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class UiSpriteAnimation : MonoBehaviour
{
  public UnityEngine.Sprite[] frames;
  public float delayBetweenFrames;
  public bool loop;
  private float lastFrameTimestamp;
  private int index;
  private Image image;

  private void Awake()
  {
    this.image = this.GetComponent<Image>();
    this.SetIndex(0);
    this.lastFrameTimestamp = Time.time;
  }

  private void Update()
  {
    if ((double) Time.time - (double) this.lastFrameTimestamp < (double) this.delayBetweenFrames)
      return;
    if (!this.loop && this.index + 1 >= this.frames.Length)
    {
      bool flag = false;
      this.image.enabled = flag;
      this.enabled = flag;
    }
    else
    {
      this.SetIndex((this.index + 1) % this.frames.Length);
      this.lastFrameTimestamp = Time.time;
    }
  }

  private void SetIndex(int index)
  {
    if (this.frames == null || this.frames.Length < 1)
    {
      Debug.LogWarning((object) ("Frames is " + (object) this.frames));
    }
    else
    {
      this.image.sprite = this.frames[this.index = index];
      this.image.SetNativeSize();
    }
  }

  public void Play()
  {
    bool flag = true;
    this.image.enabled = flag;
    this.enabled = flag;
    this.SetIndex(0);
    this.lastFrameTimestamp = Time.time;
  }
}
