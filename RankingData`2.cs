﻿// Decompiled with JetBrains decompiler
// Type: RankingData`2
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class RankingData<TGroup, TType>
{
  public TGroup Group { get; private set; }

  public TType RankingType { get; private set; }

  public List<RankDescription> Ranks { get; private set; }

  public uint TotalEntries { get; private set; }

  public DateTime LastUpdate { get; private set; }

  public RankingData(TGroup group, TType rankingType, List<RankDescription> ranks, uint totalEntries, DateTime lastUpdate)
  {
    this.Group = group;
    this.RankingType = rankingType;
    this.Ranks = ranks;
    this.TotalEntries = totalEntries;
    this.LastUpdate = lastUpdate;
  }
}
