﻿// Decompiled with JetBrains decompiler
// Type: PlayAll
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class PlayAll : MonoBehaviour
{
  private Queue<string> animationNames;
  private string currentAnimation;

  private void Start()
  {
    if ((Object) this.GetComponent<Animation>() == (Object) null)
    {
      this.enabled = false;
    }
    else
    {
      this.animationNames = new Queue<string>();
      foreach (AnimationState animationState in this.GetComponent<Animation>())
        this.animationNames.Enqueue(animationState.name);
      if (this.animationNames.Count == 0)
        this.enabled = false;
      else
        this.GetComponent<Animation>().playAutomatically = false;
    }
  }

  private void Update()
  {
    if (this.GetComponent<Animation>().isPlaying || this.animationNames.Count <= 0)
      return;
    string animation = this.animationNames.Dequeue();
    Debug.Log((object) ("Playing: " + animation));
    this.GetComponent<Animation>().Play(animation);
  }
}
