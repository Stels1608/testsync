﻿// Decompiled with JetBrains decompiler
// Type: SectorEventMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class SectorEventMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  private bool showCombatGui = true;
  public new const string NAME = "SectorEventMediator";
  private SectorEventManager eventManager;
  private DynamicEventPanel dynamicEventPanel;

  public SectorEventMediator()
    : base("SectorEventMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.SectorEventStateChanged);
    this.AddMessageInterest(Message.SectorEventTaskList);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.SectorEventTerminated);
    this.AddMessageInterest(Message.SectorEventReward);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.SectorEventStateChanged:
        this.eventManager.UpdateEventState((SectorEventStateUpdate) message.Data);
        break;
      case Message.SectorEventTaskList:
        SectorEventTaskMessage eventTaskMessage = (SectorEventTaskMessage) message;
        this.eventManager.UpdateEventTasks(eventTaskMessage.EventObjectId, eventTaskMessage.Tasks);
        break;
      case Message.SectorEventReward:
        this.eventManager.ShowReward((SectorEventReward) message.Data);
        break;
      case Message.SectorEventTerminated:
        this.eventManager.RemoveEvent((uint) message.Data);
        break;
      case Message.LoadNewLevel:
        if (this.eventManager == null)
          this.eventManager = new SectorEventManager();
        this.eventManager.RemoveAllTrackers();
        break;
    }
  }
}
