﻿// Decompiled with JetBrains decompiler
// Type: GuiTicker
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GuiTicker : QueueVertical
{
  private readonly QueueHorizontal train = new QueueHorizontal();
  private readonly List<GuiLabel> pendingMessages = new List<GuiLabel>();
  private const float MOVE_SPEED = 160f;

  public GuiTicker()
  {
    this.Name = "GuiTicker";
    this.train.Align = Align.MiddleRight;
    this.train.ContainerAutoSize = true;
    this.train.RequireBackRepositioning = false;
    this.AddChild((GuiElementBase) this.train);
  }

  private void PopulateTrain()
  {
    if (this.train.Children.Count == 0)
      this.train.AddChild((GuiElementBase) new GuiTicker.Delimeter());
    using (List<GuiLabel>.Enumerator enumerator = this.pendingMessages.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GuiLabel current = enumerator.Current;
        if (this.train.Children.Count > 22)
        {
          this.train.RemoveChild(this.train.Children[0]);
          this.train.RemoveChild(this.train.Children[0]);
        }
        this.train.AddChild((GuiElementBase) current);
        this.train.AddChild((GuiElementBase) new GuiTicker.Delimeter());
      }
    }
    this.pendingMessages.Clear();
    this.train.PositionX = this.train.SizeX;
  }

  public void AddPendingMessage(GuiTickerMessage tickerMessage)
  {
    string text = tickerMessage.Text;
    if (string.IsNullOrEmpty(text))
      throw new ArgumentException("Text for GuiTicker is NULL or empty: " + text);
    float timeToLive = tickerMessage.TimeToLive;
    this.pendingMessages.Add((GuiLabel) new GuiTicker.Message(text, timeToLive));
    if (this.train.Children.Count != 0)
      return;
    this.PopulateTrain();
  }

  public GuiTicker.IMessage AddPendingCountdown(string text, long endTime)
  {
    GuiTicker.CountdownMessage countdownMessage = new GuiTicker.CountdownMessage(text, (double) endTime);
    this.pendingMessages.Add((GuiLabel) countdownMessage);
    if (this.train.Children.Count == 0)
      this.PopulateTrain();
    return (GuiTicker.IMessage) countdownMessage;
  }

  public void Clear()
  {
    this.train.EmptyChildren();
  }

  public override void Update()
  {
    base.Update();
    if (this.train.Children.Count == 0)
      return;
    this.train.PositionX -= (float) (160.0 * (double) Time.deltaTime);
    if ((double) this.train.PositionX >= -(double) this.SizeX)
      return;
    foreach (GuiLabel child in this.train.Children)
    {
      if (child is GuiTicker.Message && (child as GuiTicker.Message).TimeToLive < 0.0)
      {
        if (this.train.Children.Count == 3)
        {
          this.train.EmptyChildren();
          break;
        }
        this.train.RemoveChild(this.train.Children[0]);
        this.train.RemoveChild(this.train.Children[0]);
        break;
      }
    }
    if (this.pendingMessages.Count != 0)
      this.PopulateTrain();
    if (this.train.Children.Count <= 0)
      return;
    this.train.PositionX = this.train.SizeX;
  }

  public override void Reposition()
  {
    this.SizeX = (float) Screen.width;
    this.SizeY = 16f;
    this.PositionY = (float) Screen.height - this.SizeY;
    base.Reposition();
  }

  private class Delimeter : GuiLabel
  {
    public Delimeter()
      : base("*", Gui.Options.FontBGM_BT, 14)
    {
    }
  }

  public interface IMessage
  {
    void Cancel();
  }

  private class Message : GuiLabel, GuiTicker.IMessage
  {
    protected double end;

    public double TimeToLive
    {
      get
      {
        return this.end - Game.TimeSync.ServerTime;
      }
    }

    public Message(string text, float timeToLive)
      : base(text.ToUpper(), Gui.Options.FontBGM_BT, 14)
    {
      this.end = Game.TimeSync.ServerTime + (double) timeToLive;
    }

    public void Cancel()
    {
      this.end = Game.TimeSync.ServerTime;
    }
  }

  private class CountdownMessage : GuiTicker.Message
  {
    private int lastSeconds = -1;
    protected readonly string baseText;
    private readonly double countdownEnd;

    public CountdownMessage(string text, double endTime)
      : base(text, (float) endTime)
    {
      this.end = endTime;
      this.baseText = text;
      this.countdownEnd = endTime;
    }

    public override void Update()
    {
      int num1 = (int) (this.countdownEnd - Game.TimeSync.ServerTime);
      if (this.lastSeconds == num1)
        return;
      this.lastSeconds = num1;
      int num2 = num1 / 60;
      int num3 = num1 % 60;
      this.Text = string.Format(this.baseText, (object) (num2 / 60), (object) (num2 % 60), (object) num3);
    }
  }
}
