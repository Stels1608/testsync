﻿// Decompiled with JetBrains decompiler
// Type: SystemButtonWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using Gui.Arena;
using Gui.Tooltips;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Events;

public class SystemButtonWindow : MonoBehaviour, IGUIRenderable
{
  private bool expanded = true;
  private const int BUTTON_WIDTH = 32;
  public SystemButton expandButton;
  public SystemButton eventShopButton;
  public SystemButton tournamentButton;
  public SystemButton battlespaceButton;
  public SystemButton characterMenuButton;
  public SystemButton wingRosterButton;
  public SystemButton pilotLogButton;
  public SystemButton leaderboardButton;
  public SystemButton buyCubitsButton;
  public SystemButton holdButton;
  public SystemButton arenaButton;
  public SystemButton galaxyMapButton;
  public SystemButton optionsFoldout;
  public SystemButton tournamentRankingButton;
  public SystemButton helpButton;
  public SystemButton settingsButton;
  public SystemButton soundButton;
  public SystemButton fullscreenButton;
  public SystemButton logoutButton;
  public GameObject foldout;
  public UnityEngine.Sprite expandSprite;
  public UnityEngine.Sprite minimizeSprite;
  private LogoutTimer logoutTimer;
  private List<SystemButton> mainButtons;
  private bool eventShopHighlight;
  private bool soundMuted;

  private LogoutTimer LogoutTimer
  {
    get
    {
      return this.logoutTimer;
    }
  }

  public bool IsRendered
  {
    get
    {
      return this.gameObject.activeSelf;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  public bool IsBigWindow
  {
    get
    {
      return false;
    }
  }

  private void Awake()
  {
    this.InitButtonHandlers();
    this.mainButtons = new List<SystemButton>();
    this.mainButtons.Add(this.expandButton);
    this.mainButtons.Add(this.eventShopButton);
    this.mainButtons.Add(this.tournamentButton);
    this.mainButtons.Add(this.battlespaceButton);
    this.mainButtons.Add(this.characterMenuButton);
    this.mainButtons.Add(this.wingRosterButton);
    this.mainButtons.Add(this.pilotLogButton);
    this.mainButtons.Add(this.leaderboardButton);
    this.mainButtons.Add(this.buyCubitsButton);
    this.mainButtons.Add(this.holdButton);
    this.mainButtons.Add(this.arenaButton);
    this.mainButtons.Add(this.galaxyMapButton);
    this.mainButtons.Add(this.fullscreenButton);
    this.mainButtons.Add(this.optionsFoldout);
    this.mainButtons.TrimExcess();
  }

  public void Init()
  {
    this.ShowFoldout(false);
    if (!this.gameObject.activeInHierarchy)
      return;
    this.StartCoroutine(this.Reposition());
  }

  private void OnEnable()
  {
    this.StartCoroutine(this.Reposition());
  }

  [DebuggerHidden]
  private IEnumerator Reposition()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SystemButtonWindow.\u003CReposition\u003Ec__Iterator27() { \u003C\u003Ef__this = this };
  }

  private void InitButtonHandlers()
  {
    this.expandButton.OnClicked = new UnityAction(this.ToggleExpand);
    this.arenaButton.OnClicked = (UnityAction) (() =>
    {
      if (!Game.Me.ActiveShip.IsCapitalShip)
      {
        Gui.Arena.Window window1 = Game.GUIManager.Find<Gui.Arena.Window>();
        if (window1 == null)
        {
          Gui.Arena.Window window2 = new Gui.Arena.Window();
          Game.GUIManager.DialogBlockers.Add((IGUIRenderable) window2);
          Game.RegisterDialog((IGUIPanel) window2, true);
        }
        else
          window1.IsRendered = !window1.IsRendered;
      }
      else
        Game.RegisterDialog((IGUIPanel) new ArenaNotAllowedWindow(), true);
    });
    this.helpButton.OnClicked = (UnityAction) (() =>
    {
      FacadeFactory.GetInstance().SendMessage(Message.ShowHelpWindow);
      this.ShowFoldout(false);
    });
    this.wingRosterButton.OnClicked = (UnityAction) (() => Game.GUIManager.Find<GuiWingsMainPanel>().ToggleIsRendered());
    this.holdButton.OnClicked = (UnityAction) (() => Game.GUIManager.Find<GUICharacterStatusWindow>().OpenInventory());
    this.pilotLogButton.OnClicked = (UnityAction) (() => Game.GUIManager.Find<GUICharacterStatusWindow>().OpenPilotLog());
    this.leaderboardButton.OnClicked = (UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.ShowLeaderboards));
    this.buyCubitsButton.OnClicked = new UnityAction(ShopWindow.OnGetCubits);
    this.characterMenuButton.OnClicked = (UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.EnterCharacterMenu));
    this.battlespaceButton.OnClicked = new UnityAction(this.OnBattlespaceClick);
    this.tournamentButton.OnClicked = new UnityAction(this.OnTournamentClick);
    this.galaxyMapButton.OnClicked = (UnityAction) (() => Game.GUIManager.Find<GalaxyMapMain>().ToggleIsRendered());
    this.eventShopButton.OnClicked = (UnityAction) (() =>
    {
      this.eventShopHighlight = false;
      FacadeFactory.GetInstance().SendMessage(Message.ShowTradeInWindow);
    });
    this.eventShopButton.SetVisible(false);
    this.optionsFoldout.OnClicked = (UnityAction) (() => this.ShowFoldout(!this.optionsFoldout.Selected));
    this.settingsButton.OnClicked = (UnityAction) (() =>
    {
      FacadeFactory.GetInstance().SendMessage(Message.ShowOptionsWindow);
      this.ShowFoldout(false);
    });
    this.soundButton.OnClicked = (UnityAction) (() =>
    {
      this.SetMuteState(!this.soundMuted);
      FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.MuteSound, (object) this.soundMuted, true));
      this.ShowFoldout(false);
    });
    this.logoutButton.OnClicked = (UnityAction) (() =>
    {
      if (this.LogoutTimer == null || !this.LogoutTimer.IsRendered)
        ConfirmLogout.Toggle();
      this.ShowFoldout(false);
    });
    this.fullscreenButton.OnClicked = (UnityAction) (() =>
    {
      SettingProtocol.GetProtocol().FullScreen();
      FacadeFactory.GetInstance().SendMessage(Message.SetFullscreen, (object) !Screen.fullScreen);
    });
    this.tournamentRankingButton.OnClicked = (UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.ShowTournamentRanking));
  }

  public void ReloadLanguageData()
  {
    this.arenaButton.SetTooltip("%$bgo.common.find_arena_match%");
    this.helpButton.SetTooltip("%$bgo.option_buttons.tutorials%", Action.ToggleWindowTutorial);
    this.wingRosterButton.SetTooltip("%$bgo.option_buttons.guild%", Action.ToggleWindowWingRoster);
    this.holdButton.SetTooltip("%$bgo.option_buttons.hold%", Action.ToggleWindowInventory);
    this.pilotLogButton.SetTooltip("%$bgo.option_buttons.journal%", Action.ToggleWindowPilotLog);
    this.leaderboardButton.SetTooltip("%$bgo.option_buttons.highscores%", Action.ToggleWindowLeaderboard);
    this.buyCubitsButton.SetTooltip("%$bgo.common.get_cubits_tooltip%");
    this.characterMenuButton.SetTooltip("%$bgo.option_buttons.character_menu%");
    this.battlespaceButton.SetTooltip("%$bgo.option_buttons.battlespace%");
    this.tournamentButton.SetTooltip("%$bgo.option_buttons.tournament%");
    this.galaxyMapButton.SetTooltip("%$bgo.option_buttons.system_navigation% ", Action.ToggleWindowGalaxyMap);
    this.eventShopButton.SetTooltip("%$bgo.option_buttons.event_shop%");
    this.settingsButton.SetTooltip("%$bgo.option_buttons.settings%", Action.ToggleWindowOptions);
    this.logoutButton.SetTooltip("%$bgo.option_buttons.logout%");
    this.fullscreenButton.SetTooltip("%$bgo.common.get_full_screen_tooltip%");
    this.tournamentRankingButton.SetTooltip("%$bgo.tournament_greeting.show_results%");
    this.soundButton.SetTooltip("%$bgo.option_buttons.toggle_sound%");
    this.expandButton.SetTooltip(!this.expanded ? "%$bgo.option_buttons.expand%" : "%$bgo.option_buttons.minimize%");
  }

  private void OnBattlespaceClick()
  {
    if (Game.JustJumped)
      return;
    GameProtocol.GetProtocol().RequestBattlespace();
  }

  private void OnTournamentClick()
  {
    SpaceLevel spaceLevel = GameLevel.Instance as SpaceLevel;
    bool flag = (UnityEngine.Object) spaceLevel != (UnityEngine.Object) null && spaceLevel.IsTournament;
    if (Game.JustJumped)
      return;
    if (flag)
      GameProtocol.GetProtocol().RequestQuitTournament();
    else
      GameProtocol.GetProtocol().RequestTournament();
  }

  private void SetButtonVisibility(SystemButton button, bool visible)
  {
    button.SetVisible(visible);
  }

  private void ShowFoldout(bool show)
  {
    this.optionsFoldout.Selected = show;
    this.foldout.SetActive(show);
    Game.TooltipManager.HideTooltip((GuiAdvancedTooltipBase) null);
  }

  public void SetBlink(StoryProtocol.ControlType control, bool highlighted)
  {
  }

  public void SetMuteState(bool muted)
  {
    this.soundMuted = muted;
    this.soundButton.Selected = muted;
    this.soundButton.icon.sprite = !this.soundButton.Selected ? this.soundButton.normal : this.soundButton.mouseOver;
  }

  public void UpdateArenaButton()
  {
    this.arenaButton.Selected = Game.Me.Arena.State != ArenaState.None;
    if (this.arenaButton.Selected)
    {
      this.InvokeRepeating("UpdateArenaTooltip", 0.0f, 5f);
    }
    else
    {
      this.CancelInvoke("UpdateArenaTooltip");
      this.arenaButton.SetTooltip("%$bgo.common.find_arena_match%");
    }
  }

  private void UpdateArenaTooltip()
  {
    string text = "%$bgo.option_buttons.arena%";
    if (Game.Me.Arena.State == ArenaState.Waiting)
      text = text + "%br%%$bgo.arena.marker_tooltip% " + Tools.FormatTime(Game.TimeSync.ServerDateTime - Game.Me.Arena.TimeWaitingBegin);
    this.arenaButton.SetTooltip(text);
  }

  public void ShowLogoutTimer(float timeLeft)
  {
    this.logoutTimer = new LogoutTimer();
    this.LogoutTimer.Delay = timeLeft;
    this.LogoutTimer.Show();
  }

  public void ShowBuyCubit(bool visible)
  {
    this.SetButtonVisibility(this.buyCubitsButton, visible);
  }

  public void ShowLeaderboards(bool visible)
  {
    this.SetButtonVisibility(this.leaderboardButton, visible);
  }

  public void ShowSectorMap(bool visible)
  {
    this.SetButtonVisibility(this.galaxyMapButton, visible);
  }

  public void ShowGuild(bool visible)
  {
    this.SetButtonVisibility(this.wingRosterButton, visible);
  }

  public void ShowJournal(bool visible)
  {
    this.SetButtonVisibility(this.pilotLogButton, visible);
  }

  public void ShowHold(bool visible)
  {
    this.SetButtonVisibility(this.holdButton, visible);
  }

  public void ShowArena(bool visible)
  {
    this.SetButtonVisibility(this.arenaButton, visible);
  }

  public void SetArenaTooltip(string tooltip)
  {
    this.arenaButton.SetTooltip(tooltip);
  }

  public void EnableBattlespaceButton(bool isEnabled)
  {
    this.battlespaceButton.Disabled = !isEnabled;
  }

  public void ShowBattlespace(bool show)
  {
    this.SetButtonVisibility(this.battlespaceButton, show);
  }

  public void SetBattlespaceTooltip(string tooltip)
  {
    this.battlespaceButton.SetTooltip(tooltip);
  }

  public void SwitchToTutorialView()
  {
    using (List<SystemButton>.Enumerator enumerator = this.mainButtons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        SystemButton current = enumerator.Current;
        if (!((UnityEngine.Object) current == (UnityEngine.Object) this.expandButton) && !((UnityEngine.Object) current == (UnityEngine.Object) this.fullscreenButton) && !((UnityEngine.Object) current == (UnityEngine.Object) this.optionsFoldout))
          this.SetButtonVisibility(current, false);
      }
    }
  }

  public void ShowTournament(bool visible)
  {
    this.tournamentButton.icon.sprite = Game.Tournament.Graphics.EnterIconNormal;
    this.SetButtonVisibility(this.tournamentButton, visible);
  }

  public void ShowEventShop(bool visible)
  {
    this.SetButtonVisibility(this.eventShopButton, visible);
  }

  public void ShowTournamentLeaderboards(bool visible)
  {
    this.tournamentRankingButton.SetVisible(visible);
  }

  public void SetTournamentTooltip(string tooltip)
  {
    this.tournamentButton.SetTooltip(tooltip);
  }

  public void ShowCharacterMenu(bool isEnabled)
  {
    this.characterMenuButton.SetVisible(isEnabled);
  }

  private void ToggleExpand()
  {
    this.ToggleExpand(false);
  }

  private void ToggleExpand(bool withoutTween)
  {
    this.ShowFoldout(false);
    this.expandButton.Disabled = true;
    this.expanded = !this.expanded;
    RectTransform component = this.gameObject.GetComponent<RectTransform>();
    float num = (float) (((double) component.sizeDelta.x - 32.0 + 2.0) * (!this.expanded ? 1.0 : -1.0));
    if (withoutTween)
    {
      component.anchoredPosition = component.anchoredPosition + Vector2.right * num;
      this.expandButton.Disabled = false;
    }
    else
      LeanTween.moveX(component, component.anchoredPosition.x + num, 0.5f).onComplete = new System.Action(this.OnExpandStateChanged);
    this.expandButton.icon.sprite = !this.expanded ? this.expandSprite : this.minimizeSprite;
    this.expandButton.SetTooltip(!this.expanded ? "%$bgo.option_buttons.expand%" : "%$bgo.option_buttons.minimize%");
  }

  private void OnExpandStateChanged()
  {
    this.expandButton.Disabled = false;
  }
}
