﻿// Decompiled with JetBrains decompiler
// Type: OptionsControlsKeyboardContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class OptionsControlsKeyboardContent : OptionsControlsBase
{
  protected override void InitContent()
  {
    GameObject gameObject1 = NGUITools.AddChild(this.Layout.ContentArea[0], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingUi"));
    if ((UnityEngine.Object) gameObject1 == (UnityEngine.Object) null)
      throw new ArgumentNullException("initInputBindingContent");
    this.inputBindingUi = gameObject1.GetComponent<InputBindingUi>();
    this.inputBindingUi.Setup(InputDevice.KeyboardMouse);
    GameObject gameObject2 = NGUITools.AddChild(this.Layout.ContentArea[1], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingOptionsMouseKeyboard"));
    if ((UnityEngine.Object) gameObject2 == (UnityEngine.Object) null)
      throw new ArgumentNullException("initInputBindingOptionsContent");
    this.InputBindingOptions = (InputBindingOptionsBase) gameObject2.GetComponent<InputBindingOptionsMouseKeyboard>();
    if ((UnityEngine.Object) this.InputBindingOptions == (UnityEngine.Object) null)
      throw new ArgumentNullException("InputBindingOptions");
    this.InputBindingOptions.InjectOnChangeDelegate(new OnSettingChanged(((OptionsContent) this).ChangeSetting));
    this.InitButtonBar();
  }

  internal void SetDogFightStatus(bool p)
  {
    ((InputBindingOptionsMouseKeyboard) this.InputBindingOptions).SetDogFightStatus(p);
  }

  protected override string GetTabText()
  {
    return Tools.ParseMessage("%$bgo.ui.options.control.controls.headline.keyboard%");
  }
}
