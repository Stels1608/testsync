﻿// Decompiled with JetBrains decompiler
// Type: CutsceneShipEvents
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class CutsceneShipEvents : MonoBehaviour
{
  private List<ObjectHider> objectHiders = new List<ObjectHider>();
  private List<SpaceObject> hiddenSpaceObjects = new List<SpaceObject>();
  private List<Coroutine> shipHidingCoroutines = new List<Coroutine>();

  public void JumpOut(string actorId)
  {
    this.JumpOutExec(actorId, true);
  }

  public void JumpOutNoSound(string actorId)
  {
    this.JumpOutExec(actorId, false);
  }

  private void JumpOutExec(string actorId, bool playSound)
  {
    GameObject actorById = CutsceneActor.FindActorById(actorId);
    if ((UnityEngine.Object) actorById == (UnityEngine.Object) null)
      return;
    JumpEffectNew componentInChildren = actorById.GetComponentInChildren<JumpEffectNew>();
    if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) ("CutsceneShipEvents:JumpOut(): No JumpEffectNew found on actor " + actorId + ". Is jump effect size undefined?"));
    }
    else
    {
      if (!playSound)
        componentInChildren.MuteSound();
      componentInChildren.JumpOut();
    }
  }

  public void FireThrusters(string stringParas)
  {
    string[] strArray = stringParas.Replace(" ", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Split(',');
    if (strArray.Length != 5)
    {
      UnityEngine.Debug.LogError((object) ("FireThrusters(): Wrong amount of parameters. Passed Parameters: " + stringParas + " Length: " + (object) strArray.Length));
    }
    else
    {
      string actorId = strArray[0];
      Vector3 outputVector;
      if (!BgoUtils.Vector3FromString(strArray[1], strArray[2], strArray[3], out outputVector))
      {
        UnityEngine.Debug.LogError((object) ("FireThrusters(): Couldn't parse given Vector: " + strArray[1] + ", " + strArray[2] + ", " + strArray[3]));
      }
      else
      {
        float result;
        if (!float.TryParse(strArray[4], out result))
        {
          UnityEngine.Debug.LogError((object) ("FireThrusters(): Couldn't parse duration parameter: " + strArray[4]));
        }
        else
        {
          GameObject actorById = CutsceneActor.FindActorById(actorId);
          if ((UnityEngine.Object) actorById == (UnityEngine.Object) null)
            return;
          CutsceneShipRemoteControl componentInChildren = actorById.GetComponentInChildren<CutsceneShipRemoteControl>();
          if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
          {
            UnityEngine.Debug.Log((object) ("SetShipGear(): " + actorById.name + " has no cutsceneShipRemoteControl!"));
          }
          else
          {
            float timeEnd = Time.realtimeSinceStartup + result;
            componentInChildren.SetThrusterEffect(outputVector.normalized, timeEnd);
          }
        }
      }
    }
  }

  public void SetShipEngineState(string stringParas)
  {
    string[] strArray = stringParas.Replace(" ", string.Empty).Split(',');
    if (strArray.Length != 2)
      UnityEngine.Debug.LogError((object) ("SetShipEngineState(): Expecting only two parameters. Given parameter string: " + stringParas));
    GameObject actorById = CutsceneActor.FindActorById(strArray[0]);
    if ((UnityEngine.Object) actorById == (UnityEngine.Object) null)
      return;
    string lower = strArray[1].ToLower();
    if (lower != null)
    {
      // ISSUE: reference to a compiler-generated field
      if (CutsceneShipEvents.\u003C\u003Ef__switch\u0024map5 == null)
      {
        // ISSUE: reference to a compiler-generated field
        CutsceneShipEvents.\u003C\u003Ef__switch\u0024map5 = new Dictionary<string, int>(4)
        {
          {
            "off",
            0
          },
          {
            "weak",
            1
          },
          {
            "normal",
            2
          },
          {
            "boost",
            3
          }
        };
      }
      int num;
      // ISSUE: reference to a compiler-generated field
      if (CutsceneShipEvents.\u003C\u003Ef__switch\u0024map5.TryGetValue(lower, out num))
      {
        ShipEngine.EngineState engineState;
        switch (num)
        {
          case 0:
            engineState = ShipEngine.EngineState.Off;
            break;
          case 1:
            engineState = ShipEngine.EngineState.Weak;
            break;
          case 2:
            engineState = ShipEngine.EngineState.Normal;
            break;
          case 3:
            engineState = ShipEngine.EngineState.Boost;
            break;
          default:
            goto label_13;
        }
        CutsceneShipRemoteControl componentInChildren = actorById.GetComponentInChildren<CutsceneShipRemoteControl>();
        if ((UnityEngine.Object) componentInChildren == (UnityEngine.Object) null)
        {
          UnityEngine.Debug.Log((object) ("SetShipGear(): " + actorById.name + " has no cutsceneShipRemoteControl!"));
          return;
        }
        componentInChildren.SetEngineState(engineState);
        return;
      }
    }
label_13:
    UnityEngine.Debug.LogError((object) ("Unknown gear parameter provided: " + strArray[1] + ". All string parameters: " + stringParas));
  }

  public void HidePlayership()
  {
    if (SectorEditorHelper.IsEditorLevel())
    {
      UnityEngine.Debug.LogWarning((object) "HidePlayership() ignored as we're in the Sector Editor.");
    }
    else
    {
      SpaceObject spaceObject = (SpaceObject) SpaceLevel.GetLevel().GetActualPlayerShip();
      spaceObject.HideModel();
      this.hiddenSpaceObjects.Add(spaceObject);
    }
  }

  public void UnhidePlayership()
  {
    if (SectorEditorHelper.IsEditorLevel())
    {
      UnityEngine.Debug.LogWarning((object) "UnhidePlayership() ignored as we're in the Sector Editor.");
    }
    else
    {
      SpaceObject spaceObject = (SpaceObject) SpaceLevel.GetLevel().GetActualPlayerShip();
      spaceObject.UnhideModel();
      this.hiddenSpaceObjects.Remove(spaceObject);
    }
  }

  public void HideSpaceObjectByKey(string jsonKey)
  {
    if (this.TryHideSpaceObject(jsonKey))
      return;
    UnityEngine.Debug.LogError((object) ("Couldn't find spaceObject: " + jsonKey + ". Adding it to shipHidingCoroutine"));
    this.shipHidingCoroutines.Add(this.StartCoroutine(this.SpaceObjectHidingCoroutine(jsonKey)));
  }

  public void UnhideSpaceObjectByKey(string jsonKey)
  {
    UnityEngine.Debug.LogWarning((object) ("Unhide SpaceObject: " + jsonKey));
    foreach (SpaceObject spaceObject in (IEnumerable) SpaceLevel.GetLevel().GetObjectRegistry().GetAll())
    {
      if (string.Equals(spaceObject.GUICard.Key, jsonKey, StringComparison.CurrentCultureIgnoreCase))
      {
        spaceObject.UnhideModel();
        this.hiddenSpaceObjects.Remove(spaceObject);
      }
    }
  }

  private bool TryHideSpaceObject(string jsonKey)
  {
    if (SectorEditorHelper.IsEditorLevel())
    {
      UnityEngine.Debug.LogWarning((object) ("TryHideSpaceObject(" + jsonKey + ") ignored as we're in the Sector Editor."));
      return true;
    }
    foreach (SpaceObject spaceObject in (IEnumerable) SpaceLevel.GetLevel().GetObjectRegistry().GetAll())
    {
      if (string.Equals(spaceObject.GUICard.Key, jsonKey, StringComparison.CurrentCultureIgnoreCase))
      {
        spaceObject.HideModel();
        this.hiddenSpaceObjects.Add(spaceObject);
        return true;
      }
    }
    return false;
  }

  [DebuggerHidden]
  private IEnumerator SpaceObjectHidingCoroutine(string jsonKey)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CutsceneShipEvents.\u003CSpaceObjectHidingCoroutine\u003Ec__IteratorE() { jsonKey = jsonKey, \u003C\u0024\u003EjsonKey = jsonKey, \u003C\u003Ef__this = this };
  }

  public void UnhideAllHiddenObjects()
  {
    for (int index = 0; index < this.objectHiders.Count; ++index)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.objectHiders[index]);
    this.objectHiders.Clear();
    for (int index = 0; index < this.hiddenSpaceObjects.Count; ++index)
      this.hiddenSpaceObjects[index].UnhideModel();
    this.hiddenSpaceObjects.Clear();
    using (List<Coroutine>.Enumerator enumerator = this.shipHidingCoroutines.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.StopCoroutine(enumerator.Current);
    }
    this.shipHidingCoroutines.Clear();
  }
}
