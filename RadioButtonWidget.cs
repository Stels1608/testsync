﻿// Decompiled with JetBrains decompiler
// Type: RadioButtonWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RadioButtonWidget : NguiWidget
{
  [SerializeField]
  public Color activeColor = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_YELLOW);
  [SerializeField]
  public Color inactiveColor = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_RUNNING);
  [SerializeField]
  public UISprite backgroundSprite;
  [SerializeField]
  public UISprite activeSprite;
  [SerializeField]
  public UILabel descLabel;
  [SerializeField]
  [HideInInspector]
  private bool isActive;
  [SerializeField]
  public RadioButtonGroupWidget radioButtonGroup;

  public bool IsActive
  {
    get
    {
      return this.isActive;
    }
    set
    {
      if (value == this.isActive)
        return;
      this.isActive = value;
      this.Set();
    }
  }

  public override void OnClick()
  {
    if (this.IsActive)
      return;
    this.isActive = !this.isActive;
    this.Set();
    this.radioButtonGroup.SetActiveButton(this);
  }

  public override void Start()
  {
    base.Start();
    this.Set();
  }

  private void Set()
  {
    if ((Object) this.activeSprite != (Object) null)
      this.activeSprite.enabled = this.isActive;
    this.descLabel.color = !this.isActive ? this.inactiveColor : this.activeColor;
  }
}
