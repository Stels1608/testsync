﻿// Decompiled with JetBrains decompiler
// Type: InitUiAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

internal class InitUiAction : Action<Message>
{
  private GuiDataProvider guiDataProvider;

  public override void Execute(IMessage<Message> message)
  {
    this.InitUgui();
    this.InitNgui();
    this.OwnerFacade.SendMessage(Message.UiCreated);
  }

  private void InitNgui()
  {
    this.guiDataProvider = (GuiDataProvider) this.OwnerFacade.FetchDataProvider("GuiDataProvider");
    if ((Object) this.guiDataProvider.guiRootGameObject != (Object) null)
    {
      Debug.LogWarning((object) "InitNgui(): Looks like the guiDataProvider has already been initiated before. Aborting...");
    }
    else
    {
      this.guiDataProvider.guiRootGameObject = (GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/UIRoot"));
      GuiHookPoint componentInChildren = this.guiDataProvider.guiRootGameObject.GetComponentInChildren<GuiHookPoint>();
      if ((Object) componentInChildren != (Object) null && (Object) componentInChildren.gameObject != (Object) null)
        this.guiDataProvider.uiHook = componentInChildren.gameObject;
      this.guiDataProvider.MvcRoot = this.guiDataProvider.guiRootGameObject.GetComponent<NguiMvcRoot>();
      this.InitControlsMessageBoxWidget();
    }
  }

  private void InitUgui()
  {
    UguiTools.GetEventSystem();
    UguiTools.CreateCanvases();
  }

  private void InitControlsMessageBoxWidget()
  {
    DialogBoxWidget component = ((GameObject) Object.Instantiate(Resources.Load("GUI/gui_2013/ui_elements/keybinding/DialogBox"))).GetComponent<DialogBoxWidget>();
    if ((Object) component == (Object) null)
      Debug.LogError((object) "MessageBoxMainWindow was not registered correctly");
    this.guiDataProvider.AddWindow((WindowWidget) component);
    this.guiDataProvider.MvcRoot.WindowManager.RegisterDialogBox(component);
  }
}
