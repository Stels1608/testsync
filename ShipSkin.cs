﻿// Decompiled with JetBrains decompiler
// Type: ShipSkin
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class ShipSkin : SingleLODListener
{
  public List<ShipSkinMaterials> skins = new List<ShipSkinMaterials>() { new ShipSkinMaterials("default") };
  private readonly List<Renderer> renderers = new List<Renderer>();
  public const string DEFAULT_SKIN_KEY = "default";
  public const string ADVANCED_SKIN_KEY = "advanced";
  public ShipSkinDefault[] defaultSkinSetup;
  private bool renderTableInit;
  private string appliedSkin;

  public static bool ShowShipSkins { get; set; }

  public bool Loaded { get; set; }

  public void InitRendererTable()
  {
    this.renderers.Clear();
    foreach (Renderer componentsInChild in this.gameObject.GetComponentsInChildren<Renderer>())
      this.renderers.Add(componentsInChild);
    this.renderTableInit = true;
  }

  public static bool ShowSkin(SpaceObject spaceObject)
  {
    bool flag = (Object) RoomLevel.GetLevel() != (Object) null;
    if (!ShipSkin.ShowShipSkins && (spaceObject == null || !spaceObject.IsMe))
      return flag;
    return true;
  }

  public void SelectSkin(string name)
  {
    if (name == "advanced")
      name = "default";
    if (!ShipSkin.ShowSkin(this.SpaceObject))
      name = "default";
    if (this.appliedSkin == name)
      return;
    this.Loaded = false;
    if (!this.renderTableInit)
      this.InitRendererTable();
    if (name == "default")
    {
      this.SwitchToDefaultSkin();
    }
    else
    {
      ShipSkinMaterials skin = this.GetSkin(name);
      if (skin == null)
      {
        UnityEngine.Debug.LogWarning((object) ("ShipSkin - couldn't find skin: " + name + " , use default skin instead"));
        this.SwitchToDefaultSkin();
      }
      else
        Game.Instance.StartCoroutine(this.LoadSkin(skin));
    }
  }

  public void SwitchToDefaultSkin()
  {
    foreach (ShipSkinDefault shipSkinDefault in this.defaultSkinSetup)
    {
      for (int index1 = 0; index1 < shipSkinDefault.renderers.Count; ++index1)
      {
        if (!((Object) shipSkinDefault.renderers[index1] == (Object) null))
        {
          Material[] materials = shipSkinDefault.renderers[index1].materials;
          for (int index2 = 0; index2 < shipSkinDefault.materials.Length; ++index2)
            materials[index2] = shipSkinDefault.materials[index2];
          shipSkinDefault.renderers[index1].materials = materials;
        }
      }
    }
    this.OnSkinLoaded("default");
  }

  [DebuggerHidden]
  private IEnumerator LoadSkin(ShipSkinMaterials selectedSkin)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ShipSkin.\u003CLoadSkin\u003Ec__Iterator18() { selectedSkin = selectedSkin, \u003C\u0024\u003EselectedSkin = selectedSkin, \u003C\u003Ef__this = this };
  }

  private void OnSkinLoaded(string skinName)
  {
    this.appliedSkin = skinName;
    this.Loaded = true;
  }

  private void OnRendererChanged(Renderer[] newRenderers)
  {
    Ship ship = this.SpaceObject as Ship;
    DamagedHull component = this.GetComponent<DamagedHull>();
    if (ship != null && ship.Bindings != null && (ship.Bindings.paintSystem != null && ship.Bindings.paintSystem.paintTexture.Length > 0))
    {
      if ((Object) component != (Object) null)
        component.Rusty = false;
      this.SelectSkin(ship.Bindings.paintSystem.paintTexture);
    }
    else
    {
      this.OnSkinLoaded("default");
      if (!((Object) component != (Object) null))
        return;
      component.Rusty = true;
    }
  }

  private ShipSkinMaterials GetSkin(string name)
  {
    for (int index = 0; index < this.skins.Count; ++index)
    {
      if (this.skins[index].key == name)
        return this.skins[index];
    }
    return (ShipSkinMaterials) null;
  }
}
