﻿// Decompiled with JetBrains decompiler
// Type: CameraSwitcher
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class CameraSwitcher
{
  private Camera activeCamera;
  private readonly Camera mainCamera;
  private readonly GameObject audioListenerGo;
  private readonly int cullingMaskMainCam;

  public Camera GetMainCamera
  {
    get
    {
      return this.mainCamera;
    }
  }

  public CameraSwitcher(Camera mainCam)
  {
    this.activeCamera = mainCam;
    this.mainCamera = mainCam;
    this.audioListenerGo = mainCam.GetComponentInChildren<AudioListener>().gameObject;
    this.cullingMaskMainCam = mainCam.cullingMask;
  }

  public void SetActiveCamera(Camera camera, bool applyMainCamFov = true)
  {
    try
    {
      this.activeCamera.enabled = (UnityEngine.Object) this.activeCamera == (UnityEngine.Object) Camera.main;
      this.activeCamera.cullingMask = 0;
      this.AttachAudioListenerTo(camera);
      this.activeCamera = camera;
      this.activeCamera.cullingMask = this.cullingMaskMainCam;
      this.activeCamera.enabled = true;
      if (applyMainCamFov)
        this.activeCamera.fieldOfView = Camera.main.fieldOfView;
      LODScript.SetActiveCamera(this.activeCamera);
      if (!((UnityEngine.Object) BackgroundCameraManager.Instance() != (UnityEngine.Object) null))
        return;
      BackgroundCameraManager.Instance().SetActiveCam(this.activeCamera);
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ("Set active cam exception: " + (object) ex));
    }
  }

  public Camera GetActiveCamera()
  {
    return this.activeCamera;
  }

  private void AttachAudioListenerTo(Camera camera)
  {
    this.audioListenerGo.transform.parent = camera.gameObject.transform;
    this.audioListenerGo.transform.localPosition = Vector3.zero;
    this.audioListenerGo.transform.localRotation = Quaternion.identity;
  }
}
