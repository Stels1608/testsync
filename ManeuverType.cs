﻿// Decompiled with JetBrains decompiler
// Type: ManeuverType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum ManeuverType : byte
{
  Pulse,
  Teleport,
  Rest,
  Warp,
  Directional,
  Launch,
  Rotation,
  Flip,
  Turn,
  Follow,
  DirectionalWithoutRoll,
  TurnQweasd,
  TurnToDirectionStrikes,
  TurnByPitchYawStrikes,
  TargetLaunch,
}
