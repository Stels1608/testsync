﻿// Decompiled with JetBrains decompiler
// Type: MapStarDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MapStarDesc : IProtocolRead
{
  public uint Id;
  public Vector2 Position;
  public byte GUIIndex;
  public Faction StarFaction;
  public int ColonialThreatLevel;
  public int CylonThreatLevel;
  public uint SectorGUID;
  public bool CanColonialOutpost;
  public bool CanCylonOutpost;
  public bool CanColonialJumpBeacon;
  public bool CanCylonJumpBeacon;

  public SectorCard SectorCard
  {
    get
    {
      return (SectorCard) Game.Catalogue.FetchCard(this.SectorGUID, CardView.Sector);
    }
  }

  public bool CanOutpost
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return this.CanColonialOutpost;
      return this.CanCylonOutpost;
    }
  }

  public void Read(BgoProtocolReader r)
  {
    this.Id = r.ReadUInt32();
    this.Position = r.ReadVector2();
    this.GUIIndex = r.ReadByte();
    this.StarFaction = (Faction) r.ReadByte();
    this.ColonialThreatLevel = (int) r.ReadInt16();
    this.CylonThreatLevel = (int) r.ReadInt16();
    this.SectorGUID = r.ReadGUID();
    this.CanColonialOutpost = r.ReadBoolean();
    this.CanCylonOutpost = r.ReadBoolean();
    this.CanColonialJumpBeacon = r.ReadBoolean();
    this.CanCylonJumpBeacon = r.ReadBoolean();
  }
}
