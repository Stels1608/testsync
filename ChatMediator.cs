﻿// Decompiled with JetBrains decompiler
// Type: ChatMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;

public class ChatMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "ChatMediator";

  public ChatMediator()
    : base("ChatMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.SettingChangedChatViewGlobal);
    this.AddMessageInterest(Message.SettingChangedChatViewLocal);
    this.AddMessageInterest(Message.SettingChangedChatShowPrefix);
    this.AddMessageInterest(Message.CombatLogOk);
    this.AddMessageInterest(Message.CombatLogOuch);
    this.AddMessageInterest(Message.CombatLogNice);
    this.AddMessageInterest(Message.ChatSystemNegative);
    this.AddMessageInterest(Message.ChatSystemPositive);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.ChatSystemNegative:
        Game.ChatStorage.Danger((string) message.Data);
        break;
      case Message.ChatSystemPositive:
        Game.ChatStorage.Help((string) message.Data);
        break;
      case Message.CombatLogOuch:
        Game.ChatStorage.OuchCombatLog((string) message.Data);
        break;
      case Message.CombatLogOk:
        Game.ChatStorage.OkCombatLog((string) message.Data);
        break;
      case Message.CombatLogNice:
        Game.ChatStorage.NiceCombatLog((string) message.Data);
        break;
      case Message.SettingChangedChatViewGlobal:
        Game.GUIManager.Find<GUIChatNew>().ViewGlobal = (bool) message.Data;
        break;
      case Message.SettingChangedChatViewLocal:
        Game.GUIManager.Find<GUIChatNew>().ViewLocal = (bool) message.Data;
        break;
      case Message.SettingChangedChatShowPrefix:
        Game.GUIManager.Find<GUIChatNew>().ShowPrefix = (bool) message.Data;
        break;
    }
  }
}
