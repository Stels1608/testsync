﻿// Decompiled with JetBrains decompiler
// Type: CameraDetector
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CameraDetector : MonoBehaviour, InputListener
{
  private bool isActive = true;
  private const float DISTANCE_TO_RAYCAST = 150f;
  private RoomObject targetObject;

  public bool IsActive
  {
    get
    {
      return this.isActive;
    }
    set
    {
      this.isActive = value;
      if (value || !((Object) this.targetObject != (Object) null))
        return;
      this.targetObject.EndCaptureObject();
      this.targetObject = (RoomObject) null;
    }
  }

  public bool HandleMouseInput
  {
    get
    {
      return this.isActive;
    }
  }

  public bool HandleKeyboardInput
  {
    get
    {
      return false;
    }
  }

  public bool HandleJoystickAxesInput
  {
    get
    {
      return this.isActive;
    }
  }

  public KeyCode[] HandledKeys
  {
    get
    {
      return new KeyCode[1]{ KeyCode.Mouse0 };
    }
  }

  public void Update()
  {
    if (!this.IsActive)
      return;
    if ((Object) this.targetObject != (Object) null && Game.GUIManager.IsBigWindowShown())
    {
      this.targetObject.EndCaptureObject();
    }
    else
    {
      RoomObject roomObject = (RoomObject) null;
      if (!GUIManager.MouseOverAnyGuiElement && !GUIManager.ModalNguiWindowOpened)
        roomObject = this.RaycastRoomObject(Input.mousePosition);
      if ((Object) roomObject != (Object) null && (Object) this.targetObject == (Object) null)
      {
        roomObject.CaptureObject();
        this.targetObject = roomObject;
      }
      else if ((Object) roomObject != (Object) null && (Object) this.targetObject != (Object) null && (Object) roomObject != (Object) this.targetObject)
      {
        this.targetObject.EndCaptureObject();
        roomObject.CaptureObject();
        this.targetObject = roomObject;
      }
      else
      {
        if (!((Object) roomObject == (Object) null) || !((Object) this.targetObject != (Object) null))
          return;
        this.targetObject.EndCaptureObject();
        this.targetObject = (RoomObject) null;
      }
    }
  }

  private RoomObject RaycastRoomObject(Vector3 mousePos)
  {
    RaycastHit[] rhs = Physics.RaycastAll(Camera.main.ScreenPointToRay(mousePos));
    if (rhs != null)
    {
      foreach (RaycastHit sortRh in this.SortRhs(rhs, this.transform.position))
      {
        RoomObject component = sortRh.transform.GetComponent<RoomObject>();
        if ((Object) component != (Object) null && (double) Vector3.Distance(component.transform.position, this.transform.position) <= 150.0)
          return component;
      }
    }
    return (RoomObject) null;
  }

  private RaycastHit[] SortRhs(RaycastHit[] rhs, Vector3 basePoint)
  {
    for (int index1 = 0; index1 < rhs.Length; ++index1)
    {
      for (int index2 = rhs.Length - 1; index2 > index1; --index2)
      {
        if ((double) Vector3.Distance(rhs[index2 - 1].point, basePoint) > (double) Vector3.Distance(rhs[index2].point, basePoint))
        {
          RaycastHit raycastHit = rhs[index2 - 1];
          rhs[index2 - 1] = rhs[index2];
          rhs[index2] = raycastHit;
        }
      }
    }
    return rhs;
  }

  public bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey != KeyCode.Mouse0 || !((Object) this.targetObject != (Object) null) || Game.GUIManager.IsBigWindowShown())
      return false;
    this.targetObject.RunBehaviour();
    return true;
  }

  public bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public void OnMouseMove(float2 mousePosition)
  {
  }

  public bool OnMouseScrollDown()
  {
    return false;
  }

  public bool OnMouseScrollUp()
  {
    return false;
  }

  public bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    return false;
  }

  public bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    return false;
  }

  public bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    return false;
  }
}
