﻿// Decompiled with JetBrains decompiler
// Type: FeedbackProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class FeedbackProtocol : BgoProtocol
{
  public FeedbackProtocol()
    : base(BgoProtocol.ProtocolID.Feedback)
  {
  }

  public static FeedbackProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Feedback) as FeedbackProtocol;
  }

  public void ReportUiElementShown(FeedbackProtocol.UiElementId uiElement)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 0);
    bw.Write((ushort) uiElement);
    this.SendMessage(bw);
  }

  public void ReportUiElementHidden(FeedbackProtocol.UiElementId uiElement)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    bw.Write((ushort) uiElement);
    this.SendMessage(bw);
  }

  public enum MessageId : ushort
  {
    UiElementShown,
    UiElementHidden,
  }

  public enum UiElementId : ushort
  {
    ShopWindow,
    RepairWindow,
    ShipShop,
    ShipCustomizationWindow,
    HangarWindow,
    ChangeAmmoMenu,
    InflightShop,
  }
}
