﻿// Decompiled with JetBrains decompiler
// Type: ItemShopOffer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

internal class ItemShopOffer : AbstractShopOffer
{
  private readonly HashSet<uint> guids;

  private ItemShopOffer(int percentage, int expiry, HashSet<uint> guids)
    : base(percentage, expiry)
  {
    if (guids.Count == 0)
      throw new ArgumentException("You must specify at least one item to be discounted.", "guids");
    this.guids = guids;
  }

  public static ItemShopOffer Decode(BgoProtocolReader input)
  {
    HashSet<uint> guids = new HashSet<uint>();
    int percentage = (int) input.ReadByte();
    int expiry = input.ReadInt32();
    for (int index = (int) input.ReadUInt16(); index > 0; --index)
    {
      uint num = input.ReadGUID();
      guids.Add(num);
    }
    return new ItemShopOffer(percentage, expiry, guids);
  }

  public ShopDiscount FindDiscount(uint guid)
  {
    if (this.IsDiscounted(guid))
      return this.Discount;
    return (ShopDiscount) null;
  }

  public bool IsDiscounted(uint guid)
  {
    if (!this.Discount.HasExpired())
      return this.guids.Contains(guid);
    return false;
  }
}
