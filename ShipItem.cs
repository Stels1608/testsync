﻿// Decompiled with JetBrains decompiler
// Type: ShipItem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public abstract class ShipItem : GameItemCard, IServerItem, IComparable<ShipItem>
{
  public IContainer Container;
  public ushort serverID;

  public ushort ServerID
  {
    get
    {
      return this.serverID;
    }
    set
    {
      this.serverID = value;
    }
  }

  public ShipItem()
    : base(0U)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.CardGUID = r.ReadGUID();
    base.Read(r);
  }

  public void MoveTo(IContainer target)
  {
    this.MoveTo(target, 0U);
  }

  public void MoveTo(IContainer target, uint count)
  {
    this.MoveTo(target, count, false);
  }

  public void MoveTo(IContainer target, uint count, bool equip)
  {
    PlayerProtocol.GetProtocol().MoveItem(this.Container.ContainerID, target != null ? target.ContainerID : (IContainerID) new BlackHoleContainerID(), this.ServerID, count, equip);
  }

  public int CompareTo(ShipItem other)
  {
    return this.ShopItemCard.CompareTo(other.ShopItemCard);
  }

  public override bool SetItemIcon(ref GuiImage image, Vector2 elementSize)
  {
    if ((UnityEngine.Object) this.ItemGUICard.GUIIconTexture != (UnityEngine.Object) null)
    {
      image.Texture = this.ItemGUICard.GUIIconTexture;
      image.SourceRect = new Rect(0.0f, 0.0f, 1f, 1f);
    }
    else if ((UnityEngine.Object) this.ItemGUICard.GUIAvatarSlotTexture != (UnityEngine.Object) null)
    {
      image.Texture = this.ItemGUICard.GUIAvatarSlotTexture;
      image.SourceRect = new Rect(0.0f, 0.0f, 1f, 1f);
    }
    else
      base.SetItemIcon(ref image, elementSize);
    return (UnityEngine.Object) image.Texture != (UnityEngine.Object) null;
  }
}
