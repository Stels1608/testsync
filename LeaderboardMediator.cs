﻿// Decompiled with JetBrains decompiler
// Type: LeaderboardMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using UnityEngine;

public class LeaderboardMediator : Bigpoint.Core.Mvc.View.View<Message>, InputListener
{
  public new const string NAME = "LeaderboardMediator";
  private LeaderboardView view;

  public bool HandleMouseInput
  {
    get
    {
      return false;
    }
  }

  public bool HandleKeyboardInput
  {
    get
    {
      return true;
    }
  }

  public bool HandleJoystickAxesInput
  {
    get
    {
      return false;
    }
  }

  public LeaderboardMediator()
    : base("LeaderboardMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.ShowLeaderboards);
    this.AddMessageInterest(Message.RankingCounterUpdate);
    this.AddMessageInterest(Message.RankingCounterPlayerUpdate);
    this.AddMessageInterest(Message.RankingTournamentUpdate);
    this.AddMessageInterest(Message.RankingTournamentPlayerUpdate);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.LevelLoaded);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.RankingCounterUpdate:
        this.view.RankingCounterUpdate((RankingCounterData) message.Data);
        break;
      case Message.RankingCounterPlayerUpdate:
        this.view.RankingCounterPlayerUpdate((RankDescription) message.Data);
        break;
      case Message.RankingTournamentUpdate:
        this.view.RankingTournamentUpdate((RankingTournamentData) message.Data);
        break;
      case Message.LoadNewLevel:
        if (!this.view.gameObject.activeSelf)
          break;
        this.ToggleView();
        break;
      case Message.LevelLoaded:
        if (!((GameLevel) message.Data).IsIngameLevel)
          break;
        Game.InputDispatcher.AddListener((InputListener) this);
        break;
      case Message.UiCreated:
        this.CreateView();
        break;
      case Message.ShowLeaderboards:
        this.ToggleView();
        break;
    }
  }

  private void ToggleView()
  {
    bool flag = !this.view.gameObject.activeSelf;
    this.view.gameObject.SetActive(flag);
    if (flag)
      this.view.OnShow();
    this.OwnerFacade.SendMessage(Message.ToggleOldUi, (object) !flag);
    this.OwnerFacade.SendMessage(Message.ToggleUGuiHudCanvas, (object) !flag);
  }

  private void CreateView()
  {
    GameObject child = UguiTools.CreateChild("Leaderboard/LeaderboardUgui", UguiTools.GetCanvas(BsgoCanvas.Windows).transform);
    this.view = child.GetComponent<LeaderboardView>();
    child.SetActive(false);
  }

  public bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    return false;
  }

  public void OnMouseMove(float2 mousePosition)
  {
  }

  public bool OnMouseScrollDown()
  {
    return false;
  }

  public bool OnMouseScrollUp()
  {
    return false;
  }

  public bool OnKeyDown(KeyCode keyboardKey, Action action)
  {
    if (action != Action.ToggleWindowLeaderboard)
      return false;
    this.ToggleView();
    return true;
  }

  public bool OnKeyUp(KeyCode keyboardKey, Action action)
  {
    return false;
  }

  public bool OnJoystickAxesInput(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode, Action action, float magnitude, float delta, bool isAxisInverted)
  {
    return false;
  }
}
