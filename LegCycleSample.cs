﻿// Decompiled with JetBrains decompiler
// Type: LegCycleSample
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class LegCycleSample
{
  public Matrix4x4 ankleMatrix;
  public Vector3 heel;
  public Matrix4x4 toeMatrix;
  public Vector3 toetip;
  public Vector3 middle;
  public float balance;
  public Vector3 footBase;
  public Vector3 footBaseNormalized;
}
