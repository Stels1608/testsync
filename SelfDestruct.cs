﻿// Decompiled with JetBrains decompiler
// Type: SelfDestruct
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
  [SerializeField]
  private float m_lifetime = 1f;
  [SerializeField]
  private float m_fadeStart = 1f;
  [SerializeField]
  private float m_LightFadeOut = 1f;
  private ParticleEmitter[] m_emitters = new ParticleEmitter[0];
  private ParticleSystem[] m_particleSystems = new ParticleSystem[0];
  [SerializeField]
  public bool m_fade;
  [SerializeField]
  public Light[] m_lightsToFade;

  public float Lifetime
  {
    get
    {
      return this.m_lifetime;
    }
    set
    {
      this.m_lifetime = value;
    }
  }

  public void Awake()
  {
    this.m_emitters = this.gameObject.GetComponentsInChildren<ParticleEmitter>();
    this.m_particleSystems = this.gameObject.GetComponentsInChildren<ParticleSystem>();
  }

  public void Update()
  {
    if ((double) this.m_lifetime > 0.0)
    {
      this.m_lifetime -= Time.deltaTime;
      if ((double) this.m_lifetime <= 0.0)
      {
        for (int index = 0; index < this.m_emitters.Length; ++index)
        {
          ParticleEmitter particleEmitter = this.m_emitters[index];
          if ((Object) null != (Object) particleEmitter)
            particleEmitter.emit = false;
        }
        for (int index = 0; index < this.m_particleSystems.Length; ++index)
        {
          ParticleSystem particleSystem = this.m_particleSystems[index];
          if ((Object) null != (Object) particleSystem)
            particleSystem.enableEmission = false;
        }
      }
    }
    if ((double) this.m_lifetime <= 0.0)
    {
      bool flag = true;
      for (int index = 0; index < this.m_emitters.Length; ++index)
      {
        ParticleEmitter particleEmitter = this.m_emitters[index];
        if ((Object) null != (Object) particleEmitter && particleEmitter.particleCount > 0)
        {
          flag = false;
          break;
        }
      }
      if (flag)
      {
        for (int index = 0; index < this.m_particleSystems.Length; ++index)
        {
          ParticleSystem particleSystem = this.m_particleSystems[index];
          if ((Object) null != (Object) particleSystem && particleSystem.particleCount > 0)
          {
            flag = false;
            break;
          }
        }
      }
      if (flag)
        Object.Destroy((Object) this.gameObject);
    }
    if (this.m_fade && (double) this.m_lifetime <= (double) this.m_fadeStart)
    {
      Renderer[] componentsInChildren = this.gameObject.GetComponentsInChildren<Renderer>();
      for (int index1 = 0; index1 < componentsInChildren.Length; ++index1)
      {
        for (int index2 = 0; index2 < componentsInChildren[index1].materials.Length; ++index2)
        {
          if (componentsInChildren[index1].materials[index2].HasProperty("_Cut"))
            componentsInChildren[index1].materials[index2].SetFloat("_Cut", (float) (1.0 - (double) this.m_lifetime / (double) this.m_fadeStart));
        }
      }
    }
    if (this.m_lightsToFade.Length <= 0 || (double) this.m_lifetime > (double) this.m_LightFadeOut)
      return;
    for (int index = 0; index < this.m_lightsToFade.Length; ++index)
    {
      if (!((Object) this.m_lightsToFade[index] == (Object) null))
      {
        float num = this.m_lightsToFade[index].intensity - Time.deltaTime / this.m_lifetime;
        if ((double) this.m_lightsToFade[index].intensity > 0.0)
          this.m_lightsToFade[index].intensity = num;
      }
    }
  }
}
