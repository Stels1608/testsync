﻿// Decompiled with JetBrains decompiler
// Type: SpaceObject
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[JsonClassDesc("Position", JsonName = "position")]
[JsonClassInstantiation(ConstructorArgs = new string[] {"objectID"}, mode = JsonClassInstantiationAttribute.Modes.Manual)]
[JsonClassDesc("Rotation", JsonName = "rotation")]
[Serializable]
public class SpaceObject : ISpaceEntity, IProtocolRead
{
  private float distance = float.MaxValue;
  private float sqrDistance = float.MaxValue;
  private Vector3 position = Vector3.zero;
  private Quaternion rotation = Quaternion.identity;
  private float scale = 1f;
  private SpaceObjectState currentState = new SpaceObjectState();
  public System.Action<GameObject> Constructed;
  private Transform root;
  private RootScript rootScript;
  protected GameObject model;
  private uint objectID;
  private uint ownerGUID;
  protected uint objectGUID;
  private Texture2D avatar;
  protected IMovementController movementController;
  public WorldCard WorldCard;
  public GUICard GUICard;
  public OwnerCard OwnerCard;
  private readonly RegulationCard regulationCard;
  public SpaceSubscribeInfo Props;
  private Faction faction;
  private FactionGroup factionGroup;
  public CreatingCause CreatingCause;
  private readonly Flag isLoaded;
  public readonly Flag AreCardsLoaded;
  protected readonly Flag isLocated;
  public readonly Flag IsHighResLoaded;
  public readonly Flag IsLowResLoaded;
  public readonly Flag IsAnyResLoaded;
  protected bool dead;
  private Dictionary<ushort, Spot> spots;
  private float creatingTime;
  private float locatingTime;
  private float cardsLoadingTime;
  private float constructingTime;
  private float loadingTime;
  private uint stateRevision;
  private SpaceObject.MarkObjectType markerType;
  private bool modelIsHidden;
  public bool HasJumpInFxTriggered;
  public bool IsSubscribed;
  private Thrusters thrusters;
  private bool isPaintedTarget;
  private Bounds meshBoundsRaw;
  private bool debugWasLoaded;
  private bool isInPlayersMapRange;
  private bool isInSharedMapRange;
  private bool isInDradisRange;

  public Flag IsConstructed { get; private set; }

  public bool AutoSubscribe { get; set; }

  public TargetType TargetType
  {
    get
    {
      return TargetType.SpaceObject;
    }
  }

  public bool IsPaintedTarget
  {
    get
    {
      return this.isPaintedTarget;
    }
    set
    {
      this.isPaintedTarget = value;
    }
  }

  public bool IsCloaked { get; private set; }

  public virtual bool HideStats { get; set; }

  public Bounds MeshBoundsRaw
  {
    get
    {
      return this.meshBoundsRaw;
    }
    set
    {
      this.meshBoundsRaw = value;
    }
  }

  public virtual bool ShowIndicatorWhenInRange
  {
    get
    {
      return this.WorldCard.ShowBracketWhenInRange;
    }
  }

  public bool ForceShowOnMap
  {
    get
    {
      return this.WorldCard.ForceShowOnMap;
    }
  }

  public Faction Faction
  {
    get
    {
      return this.faction;
    }
  }

  public FactionGroup FactionGroup
  {
    get
    {
      return this.factionGroup;
    }
    set
    {
      this.factionGroup = value;
    }
  }

  public virtual bool IsHostileCongener
  {
    get
    {
      if (this.Faction != Game.Me.Faction)
        return false;
      if (this.regulationCard.TargetBracketMode != TargetBracketMode.AllEnemy)
        return this.FactionGroup != Game.Me.FactionGroup;
      return true;
    }
  }

  public virtual bool IsPlayer
  {
    get
    {
      return false;
    }
  }

  public virtual bool IsMe
  {
    get
    {
      return false;
    }
  }

  public uint ObjectID
  {
    get
    {
      return this.objectID;
    }
  }

  public SpaceEntityType SpaceEntityType
  {
    get
    {
      return SpaceObject.GetObjectType(this.objectID);
    }
  }

  public virtual string ObjectName
  {
    get
    {
      return this.GUICard.Name;
    }
  }

  public virtual string Name
  {
    get
    {
      return this.GUICard.Name;
    }
  }

  public virtual byte Level
  {
    get
    {
      return 0;
    }
  }

  public IMovementController MovementController
  {
    get
    {
      return this.movementController;
    }
  }

  public Relation PlayerRelation
  {
    get
    {
      return RelationHelper.GetRelation((ISpaceEntity) this, Game.Me.Faction, Game.Me.FactionGroup, this.regulationCard.TargetBracketMode);
    }
  }

  public virtual Texture2D Avatar
  {
    get
    {
      return this.avatar;
    }
  }

  public bool HasLevel
  {
    get
    {
      return (int) this.Level != 0;
    }
  }

  public bool Dead
  {
    get
    {
      return this.dead;
    }
  }

  public virtual UnityEngine.Sprite Icon3dMap
  {
    get
    {
      if ((int) this.IconIndex < 0)
        return (UnityEngine.Sprite) null;
      return SystemMap3DMapIconLinker.Instance.MapObjectsNPCs[(int) this.IconIndex];
    }
  }

  public virtual UnityEngine.Sprite IconBrackets
  {
    get
    {
      if ((int) this.IconIndex < 0)
        return (UnityEngine.Sprite) null;
      return SystemMap3DMapIconLinker.Instance.MapObjectsNPCsBrackets[(int) this.IconIndex];
    }
  }

  protected virtual sbyte IconIndex
  {
    get
    {
      return this.WorldCard.FrameIndex;
    }
  }

  public bool IsFriendlyHub
  {
    get
    {
      if (this.CanDock(false) && !(this is PlayerShip))
        return this.PlayerRelation == Relation.Friend;
      return false;
    }
  }

  public virtual bool IsMinion
  {
    get
    {
      return false;
    }
  }

  public Flag IsLoaded
  {
    get
    {
      return this.isLoaded;
    }
  }

  public Vector3 Position
  {
    get
    {
      return this.position;
    }
    set
    {
      this.position = value;
      if ((UnityEngine.Object) this.root != (UnityEngine.Object) null)
        this.root.position = this.position;
      this.OnLocate();
    }
  }

  public Quaternion Rotation
  {
    get
    {
      return this.rotation;
    }
    set
    {
      this.rotation = value;
      if (!((UnityEngine.Object) this.root != (UnityEngine.Object) null))
        return;
      this.root.rotation = this.rotation;
    }
  }

  public float Scale
  {
    get
    {
      return this.scale;
    }
    set
    {
      this.scale = value;
      if (!((UnityEngine.Object) this.root != (UnityEngine.Object) null))
        return;
      this.root.localScale = Vector3.one * this.scale;
    }
  }

  public Vector3 Forward
  {
    get
    {
      return this.rotation * Vector3.forward;
    }
  }

  public Vector3 Up
  {
    get
    {
      return this.rotation * Vector3.up;
    }
  }

  public virtual bool SpawnedInSector
  {
    get
    {
      return true;
    }
  }

  public virtual string PrefabName
  {
    get
    {
      return this.WorldCard.PrefabName;
    }
  }

  public Transform Root
  {
    get
    {
      return this.root;
    }
  }

  public RootScript RootScript
  {
    get
    {
      return this.rootScript;
    }
  }

  public float LocatingTime
  {
    get
    {
      return this.locatingTime - this.creatingTime;
    }
  }

  public float CardsLoadingTime
  {
    get
    {
      return this.cardsLoadingTime - this.creatingTime;
    }
  }

  public float ConstructingTime
  {
    get
    {
      return this.constructingTime - Mathf.Max(this.locatingTime, this.cardsLoadingTime);
    }
  }

  public float LoadingTime
  {
    get
    {
      return this.loadingTime - this.creatingTime;
    }
  }

  public virtual bool IsInMapRange
  {
    get
    {
      if (!this.IsInPlayersMapRange && !this.IsInSharedMapRange)
        return DradisHelper.IsAlwaysInMapRange((ISpaceEntity) this);
      return true;
    }
  }

  public bool IsInPlayersMapRange
  {
    get
    {
      return this.isInPlayersMapRange;
    }
    set
    {
      bool isInMapRange = this.IsInMapRange;
      this.isInPlayersMapRange = value;
      if (this.IsInMapRange == isInMapRange)
        return;
      FacadeFactory.GetInstance().SendMessage(Message.TargetPassedMapRange, (object) this);
    }
  }

  public bool IsInSharedMapRange
  {
    get
    {
      return this.isInSharedMapRange;
    }
    set
    {
      bool isInMapRange = this.IsInMapRange;
      this.isInSharedMapRange = value;
      if (this.IsInMapRange == isInMapRange)
        return;
      FacadeFactory.GetInstance().SendMessage(Message.TargetPassedMapRange, (object) this);
    }
  }

  public bool IsInDradisRange
  {
    get
    {
      return this.isInDradisRange;
    }
    set
    {
      this.isInDradisRange = value;
    }
  }

  public SpaceObject.MarkObjectType MarkerType
  {
    get
    {
      return this.markerType;
    }
  }

  public GameObject Model
  {
    get
    {
      return this.model;
    }
  }

  public virtual bool IsTargetable
  {
    get
    {
      if (this.WorldCard != null)
        return this.WorldCard.Targetable;
      return false;
    }
  }

  public bool ShowSkin
  {
    get
    {
      if (!this.IsMe)
        return ShipSkin.ShowShipSkins;
      return true;
    }
  }

  public SpaceObject(uint objectID)
  {
    this.objectID = objectID;
    GameObject gameObject = new GameObject("NewUnassignedRootObject");
    this.rootScript = gameObject.AddComponent<RootScript>();
    this.rootScript.SpaceObject = this;
    this.rootScript.Destroyed = new RootScript.DestroyedCallback(this.RootDestroyed);
    this.root = gameObject.transform;
    this.faction = this.ExtractFaction(objectID);
    this.factionGroup = this.ExtractFactionGroup(objectID);
    this.creatingTime = Time.realtimeSinceStartup;
    this.isLoaded = new Flag();
    this.AreCardsLoaded = new Flag();
    this.IsConstructed = new Flag();
    this.isLocated = new Flag();
    this.IsHighResLoaded = new Flag();
    this.IsLowResLoaded = new Flag();
    this.IsAnyResLoaded = new Flag();
    this.AreCardsLoaded.AddHandler(new SignalHandler(this.OnCardsLoaded));
    this.IsConstructed.AddHandler(new SignalHandler(this.OnConstructed));
    this.isLoaded.Depend((ILoadable) this.isLocated, (ILoadable) this.AreCardsLoaded, (ILoadable) this.IsConstructed);
    this.isLoaded.AddHandler(new SignalHandler(this.OnLoaded));
    this.isLoaded.Set();
    Flag.Schedule(new SignalHandler(this.Construct), (ILoadable) this.isLocated, (ILoadable) this.AreCardsLoaded);
    this.Props = new SpaceSubscribeInfo(objectID);
    if (SectorEditorHelper.IsEditorLevel())
      return;
    this.regulationCard = SpaceLevel.GetLevel().Card.regulationCard;
  }

  public static SpaceEntityType GetObjectType(uint objectID)
  {
    return (SpaceEntityType) ((int) objectID & 520093696);
  }

  public float GetDistance()
  {
    return this.distance;
  }

  public float GetSqrDistance()
  {
    return this.sqrDistance;
  }

  public bool CanDock(bool considerDockingRange)
  {
    PlayerShip playerShip1 = SpaceLevel.GetLevel().GetPlayerShip();
    if ((playerShip1 == null || !(bool) playerShip1.IsLoaded) && (this.OwnerCard.IsDockable && this.SpaceEntityType != SpaceEntityType.Player) && this.PlayerRelation == Relation.Friend)
      return true;
    if (playerShip1 == null || !(bool) playerShip1.IsLoaded || (!this.OwnerCard.IsDockable || this.PlayerRelation != Relation.Friend) || Game.Me.Anchored)
      return false;
    PlayerShip playerShip2 = this as PlayerShip;
    if (playerShip2 != null && ((int) playerShip1.ShipCardLight.Tier > 1 || !Game.Me.Party.Members.Contains(playerShip2.Player)) || playerShip1.ShipCardLight.ShipRoleDeprecated == ShipRoleDeprecated.Carrier && this is OutpostShip)
      return false;
    if (considerDockingRange)
      return (double) (this.Position - playerShip1.Position).magnitude <= (double) this.OwnerCard.DockRange;
    return true;
  }

  public float GetAccurateDistance(SpaceObject target)
  {
    Vector3 origin = this.Position;
    Vector3 vector3 = target.Position;
    foreach (RaycastHit raycastHit in Physics.RaycastAll(new Ray(origin, vector3 - origin), (vector3 - origin).magnitude))
    {
      RootScript component = raycastHit.transform.root.gameObject.GetComponent<RootScript>();
      if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      {
        if (component.SpaceObject == this)
          origin = raycastHit.point;
        else if (component.SpaceObject == target)
          vector3 = raycastHit.point;
      }
    }
    return (vector3 - origin).magnitude;
  }

  public T GetModelScript<T>() where T : ModelScript
  {
    if ((UnityEngine.Object) this.Model == (UnityEngine.Object) null)
      return (T) null;
    return this.Model.GetComponentInChildren<T>();
  }

  public T AddModelScript<T>() where T : ModelScript
  {
    if ((UnityEngine.Object) this.Model == (UnityEngine.Object) null)
      return (T) null;
    T obj = this.Model.AddComponent<T>();
    obj.SpaceObject = this;
    return obj;
  }

  public Spot GetObjectPoint(ushort objectPointHash)
  {
    if (this.spots == null)
      return (Spot) null;
    Spot spot = (Spot) null;
    this.spots.TryGetValue(objectPointHash, out spot);
    return spot;
  }

  public Dictionary<ushort, Spot> GetSpots()
  {
    return this.spots;
  }

  public Spot[] GetSpotsOfType(SpotType spotType)
  {
    if (this.spots == null)
    {
      Log.Warning((object) "Spots are requested before model initialization.");
      return (Spot[]) null;
    }
    List<Spot> spotList = new List<Spot>();
    using (Dictionary<ushort, Spot>.ValueCollection.Enumerator enumerator = this.spots.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Spot current = enumerator.Current;
        if (current.Type == SpotType.Sticker)
          spotList.Add(current);
      }
    }
    return spotList.ToArray();
  }

  private void OnLoaded()
  {
    this.loadingTime = Time.realtimeSinceStartup;
    SpaceLevel.GetLevel().GetObjectRegistry().ObjectLoaded(this);
    if (this.debugWasLoaded)
      Debug.LogError((object) ("SpaceObject " + this.Name + " was loaded twice. Report this."));
    this.debugWasLoaded = true;
  }

  private void OnConstructed()
  {
    if (this.Constructed != null)
      this.Constructed(this.root.gameObject);
    this.Constructed = (System.Action<GameObject>) null;
  }

  public virtual void OnRead()
  {
    this.AreCardsLoaded.Set();
    this.movementController = this.CreateMovementController();
  }

  protected virtual void OnLocate()
  {
    if ((bool) this.isLocated)
      return;
    this.locatingTime = Time.realtimeSinceStartup;
    this.isLocated.Set();
  }

  private void OnCardsLoaded()
  {
    this.cardsLoadingTime = Time.realtimeSinceStartup;
    this.SetGuiAvatar();
  }

  protected virtual IMovementController CreateMovementController()
  {
    return (IMovementController) new StaticMovementController(this);
  }

  public virtual void Remove(RemovingCause removingCause)
  {
    this.UnSubscribe();
    if ((UnityEngine.Object) this.root != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.root.gameObject);
    if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null)
      SpaceLevel.GetLevel().PaintedTargets.Remove(this);
    this.dead = true;
  }

  private void RootDestroyed()
  {
    this.root = (Transform) null;
    this.DestroyModel();
  }

  public void Construct()
  {
    try
    {
      if (!((UnityEngine.Object) this.rootScript != (UnityEngine.Object) null))
        return;
      this.rootScript.Construct();
    }
    catch (Exception ex)
    {
      string str = "[GuiCard Null]";
      if (this.GUICard != null)
        str = this.GUICard.Name;
      Debug.LogError((object) ("Error Constructing " + str + " \n" + (object) ex));
    }
  }

  public void SetDistance(float distance)
  {
    this.distance = distance;
    this.sqrDistance = distance * distance;
  }

  public void SetMarkObject(SpaceObject.MarkObjectType newMarkingType)
  {
    if (this.MarkerType == newMarkingType)
      return;
    this.markerType = newMarkingType;
    switch (this.markerType)
    {
      case SpaceObject.MarkObjectType.None:
        FacadeFactory.GetInstance().SendMessage(Message.TargetWaypointStatusChangeRequest, (object) new KeyValuePair<ISpaceEntity, bool>((ISpaceEntity) this, false));
        break;
      case SpaceObject.MarkObjectType.Waypoint:
        FacadeFactory.GetInstance().SendMessage(Message.TargetWaypointStatusChangeRequest, (object) new KeyValuePair<ISpaceEntity, bool>((ISpaceEntity) this, true));
        break;
    }
  }

  public override string ToString()
  {
    return string.Format("[Type: {0}, Id: {1}, Name: {2}]", (object) this.SpaceEntityType, (object) this.objectID, (object) this.Name);
  }

  public void BaseRead(BgoProtocolReader r)
  {
    this.CreatingCause = (CreatingCause) r.ReadByte();
    this.ownerGUID = r.ReadUInt32();
    this.objectGUID = r.ReadUInt32();
    this.WorldCard = (WorldCard) Game.Catalogue.FetchCard(this.objectGUID, CardView.World);
    this.GUICard = (GUICard) Game.Catalogue.FetchCard(this.ownerGUID, CardView.GUI);
    this.OwnerCard = (OwnerCard) Game.Catalogue.FetchCard(this.ownerGUID, CardView.Owner);
    this.AreCardsLoaded.Depend((ILoadable) this.WorldCard, (ILoadable) this.GUICard, (ILoadable) this.OwnerCard);
    this.SetRootName();
  }

  public virtual void Read(BgoProtocolReader r)
  {
    this.BaseRead(r);
  }

  public void Move(double time)
  {
    this.MovementController.Move(time);
  }

  public virtual void OnClicked()
  {
  }

  public virtual void OnDestroyed()
  {
  }

  private void DestroyModel()
  {
    if ((UnityEngine.Object) this.Model != (UnityEngine.Object) null)
    {
      GameObject objectInChildren = HelperUtil.FindGameObjectInChildren(this.Model.transform.parent.gameObject, "JumpEffect");
      if ((UnityEngine.Object) null != (UnityEngine.Object) objectInChildren)
        UnityEngine.Object.Destroy((UnityEngine.Object) objectInChildren);
      UnityEngine.Object.Destroy((UnityEngine.Object) this.Model);
    }
    this.model = (GameObject) null;
    this.spots = (Dictionary<ushort, Spot>) null;
  }

  public virtual void SetGuiAvatar()
  {
    if (this.GUICard == null)
      return;
    string path = this.GUICard.GUIAvatarSlotTexturePath.Length == 0 ? this.GUICard.GUIAtlasTexturePath : this.GUICard.GUIAvatarSlotTexturePath;
    if (path != string.Empty)
      this.avatar = (Texture2D) Resources.Load(path);
    else
      this.avatar = (Texture2D) Resources.Load("GUI/Slots/avatar_debris");
  }

  private void UpdateBounds(GameObject objectModel)
  {
    this.meshBoundsRaw = BgoUtils.MeasureRenderingBounds(objectModel);
  }

  public void SetHighResModel(GameObject prototype)
  {
    this.SetModel(prototype);
    this.IsHighResLoaded.Set();
  }

  public void SetLowResModel(GameObject prototype)
  {
    this.SetModel(prototype);
    this.IsLowResLoaded.Set();
  }

  protected virtual void SetModel(GameObject prototype)
  {
    this.DestroyModel();
    if ((UnityEngine.Object) prototype != (UnityEngine.Object) null)
    {
      this.model = (GameObject) UnityEngine.Object.Instantiate((UnityEngine.Object) prototype, Vector3.zero, Quaternion.identity);
      this.UpdateBounds(this.model);
      this.Model.name = prototype.name;
      this.Model.transform.parent = this.root;
      this.Model.transform.localPosition = Vector3.zero;
      this.Model.transform.localRotation = Quaternion.identity;
      this.Model.transform.localScale = Vector3.one;
      if (this.modelIsHidden && (UnityEngine.Object) this.model.GetComponent<ObjectHider>() == (UnityEngine.Object) null)
        this.model.AddComponent<ObjectHider>();
      this.InitModelScripts();
      this.spots = new Dictionary<ushort, Spot>();
      Spot[] spots = Spot.FindSpots(this.WorldCard.Spots, this.Model);
      for (int index = 0; index < spots.Length; ++index)
        this.spots.Add(spots[index].ObjectPointServerHash, spots[index]);
      this.thrusters = this.Model.GetComponent<Thrusters>();
      SectorEditorHelper.EditorModelMode(this);
    }
    else
      Debug.LogError((object) ("Setting Model to NULL for " + this.Name));
    this.IsConstructed.Set();
    if (!SectorEditorHelper.IsEditorLevel())
      this.SetRootName();
    this.constructingTime = Time.realtimeSinceStartup;
    this.Model.SendMessage("ModelSetOrSwitched", SendMessageOptions.DontRequireReceiver);
    this.IsAnyResLoaded.Set();
  }

  public void HideModel()
  {
    if (this.modelIsHidden)
      return;
    this.modelIsHidden = true;
    if (!(bool) this.IsConstructed)
      return;
    this.model.AddComponent<ObjectHider>();
  }

  public void UnhideModel()
  {
    if (!this.modelIsHidden)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.model.GetComponent<ObjectHider>());
    this.modelIsHidden = false;
  }

  public virtual void InitModelScripts()
  {
    ModelScript[] componentsInChildren = this.Model.GetComponentsInChildren<ModelScript>();
    foreach (ModelScript modelScript in componentsInChildren)
      modelScript.SpaceObject = this;
    foreach (ModelScript modelScript in componentsInChildren)
    {
      if (modelScript is Gearbox && !SectorEditorHelper.IsEditorLevel())
        SpaceLevel.GetLevel().ShipEngineTuner.Tune(modelScript as Gearbox);
      else if (modelScript is LODScript)
        (modelScript as LODScript).UpdateDistances();
    }
  }

  public void SetModelParams(float scale, Color color)
  {
    this.Model.transform.localScale = Vector3.one * scale;
    foreach (Renderer componentsInChild in this.Model.GetComponentsInChildren<Renderer>())
      componentsInChild.material.color = color;
  }

  private void SetRootName()
  {
    if ((UnityEngine.Object) null != (UnityEngine.Object) this.Model && (UnityEngine.Object) null != (UnityEngine.Object) this.root)
    {
      string str = this.GetType().Name + " - " + this.Model.name;
      if ((int) this.Level > 0)
        str = str + " - Lvl " + (object) this.Level + " - (" + this.Name + ")";
      this.root.name = str;
    }
    else
    {
      if (this.WorldCard == null || !((UnityEngine.Object) null != (UnityEngine.Object) this.root))
        return;
      this.root.name = "** " + this.GetType().Name + " - " + this.WorldCard.PrefabName;
    }
  }

  public virtual void UnSubscribe()
  {
    GameProtocol.GetProtocol().UnSubscribeInfo(this);
  }

  public virtual void Subscribe()
  {
    GameProtocol.GetProtocol().SubscribeInfo(this);
  }

  public Faction ExtractFaction(uint objectID)
  {
    switch (objectID & 3221225472U)
    {
      case 3221225472:
        return Faction.Ancient;
      case 0:
        return Faction.Neutral;
      case 1073741824:
        return Faction.Colonial;
      case 2147483648:
        return Faction.Cylon;
      default:
        throw new Exception("Unknown faction mask " + (object) objectID);
    }
  }

  public FactionGroup ExtractFactionGroup(uint objectID)
  {
    switch (objectID & 536870912U)
    {
      case 0:
        return FactionGroup.Group0;
      case 536870912:
        return FactionGroup.Group1;
      default:
        throw new Exception("Unknown faction group mask " + (object) objectID);
    }
  }

  public virtual void Update()
  {
  }

  public void UpdateThrusters(List<ThrusterEffect> thrusterEffects)
  {
    if ((UnityEngine.Object) this.Model == (UnityEngine.Object) null || thrusterEffects == null || (UnityEngine.Object) this.thrusters == (UnityEngine.Object) null)
      return;
    if ((UnityEngine.Object) this.thrusters == (UnityEngine.Object) null)
      this.thrusters = this.Model.GetComponent<Thrusters>();
    if (!((UnityEngine.Object) this.thrusters != (UnityEngine.Object) null) || this.IsCloaked)
      return;
    this.thrusters.UpdateThrusters(thrusterEffects);
  }

  public void UpdateState(SpaceObjectState state)
  {
    if (state.Revision < this.stateRevision)
      return;
    this.stateRevision = state.Revision;
    if (this.currentState.Marked != state.Marked)
      this.SetMarkedState(state.Marked);
    if (this.currentState.Cloaked != state.Cloaked)
      this.UpdateCloakedState(state.Cloaked);
    if (this.currentState.Fortified != state.Fortified)
      this.UpdateFortifiedState(state.Fortified);
    if (this.currentState.ShortCircuited != state.ShortCircuited)
      this.UpdateShortCircuitedState(state.ShortCircuited);
    this.currentState = state;
  }

  private void UpdateShortCircuitedState(bool shortCircuited)
  {
    ShortCircuitComplicated modelScript = this.GetModelScript<ShortCircuitComplicated>();
    if (shortCircuited)
    {
      if (!((UnityEngine.Object) modelScript == (UnityEngine.Object) null))
        return;
      this.AddModelScript<ShortCircuitComplicated>();
    }
    else
    {
      if (!((UnityEngine.Object) modelScript != (UnityEngine.Object) null))
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) modelScript);
    }
  }

  private void UpdateFortifiedState(bool fortified)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.IsHighResLoaded.AddHandler(new SignalHandler(new SpaceObject.\u003CUpdateFortifiedState\u003Ec__AnonStorey10E()
    {
      fortified = fortified,
      \u003C\u003Ef__this = this
    }.\u003C\u003Em__27F));
  }

  private void SetMarkedState(bool marked)
  {
    if (marked)
    {
      if (this.IsMe)
      {
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.carrier.painted_target%", NotificationCategory.Negative));
        return;
      }
      this.IsPaintedTarget = true;
      SpaceLevel.GetLevel().PaintedTargets.Add(this);
    }
    else
    {
      this.IsPaintedTarget = false;
      SpaceLevel.GetLevel().PaintedTargets.Remove(this);
    }
    FacadeFactory.GetInstance().SendMessage(Message.TargetDesignationChangeRequest, (object) new KeyValuePair<ISpaceEntity, bool>((ISpaceEntity) this, marked));
  }

  private void UpdateCloakedState(bool isCloaked)
  {
    this.IsCloaked = isCloaked;
    if (Game.Me.Ship != null)
      DradisHelper.UpdateObjectFlags(this);
    if (SpaceLevel.GetLevel().GetPlayerTarget() == this)
      FacadeFactory.GetInstance().SendMessage(Message.TargetPassedMapRange, (object) this);
    FacadeFactory.GetInstance().SendMessage(Message.PlayerChangedCloakStatus, (object) new KeyValuePair<SpaceObject, bool>(this, isCloaked));
    if (!this.IsMe)
      return;
    FacadeFactory.GetInstance().SendMessage(!isCloaked ? Message.StopAmbientSound : Message.PlayAmbientSound, (object) SpaceExposer.GetInstance().StealthAmbient);
  }

  public void ToggleEngines(bool enginesEnabled)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.IsConstructed.AddHandler(new SignalHandler(new SpaceObject.\u003CToggleEngines\u003Ec__AnonStorey10F()
    {
      enginesEnabled = enginesEnabled,
      \u003C\u003Ef__this = this
    }.\u003C\u003Em__280));
  }

  public enum MarkObjectType
  {
    None,
    Waypoint,
  }
}
