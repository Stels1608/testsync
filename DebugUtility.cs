﻿// Decompiled with JetBrains decompiler
// Type: DebugUtility
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class DebugUtility : MonoBehaviour
{
  private static DebugUtility instance = (DebugUtility) null;
  private static List<string> log = new List<string>();
  private static uint roles = 0;

  public static void Create(uint roles)
  {
    DebugUtility.roles = roles;
    if (!((Object) DebugUtility.instance == (Object) null))
      return;
    GameObject gameObject = new GameObject();
    Object.DontDestroyOnLoad((Object) gameObject);
    DebugUtility.instance = gameObject.AddComponent<DebugUtility>();
    DebugUtility.LogInfo("Welcome to BGO Console. Your roles: #" + roles.ToString("X"));
  }

  public static bool HasAccess(BgoAdminRoles rule)
  {
    return ((BgoAdminRoles) DebugUtility.roles & rule) == rule;
  }

  public static void LogInfo(string message)
  {
    DebugUtility.log.Add(message);
    Log.Info((object) message);
    Game.Console.Add(message);
  }

  public static void LogError(string message)
  {
    DebugUtility.log.Add(message);
    Debug.LogError((object) ("ERROR: " + message));
    Game.Console.Add(message);
  }

  public static List<string> Logs()
  {
    return DebugUtility.log;
  }

  private void OnApplicationQuit()
  {
    if (!(bool) ((Object) DebugUtility.instance))
      return;
    Object.DestroyImmediate((Object) DebugUtility.instance.gameObject);
  }

  private void Awake()
  {
    this.name = this.GetType().ToString();
  }

  private void Update()
  {
    if (!DebugUtility.HasAccess(BgoAdminRoles.Console) || !Input.GetKeyDown(KeyCode.F7) && !Input.GetKeyDown(KeyCode.Home))
      return;
    DebugBehaviour<DebugButtons>.Toggle();
  }

  public static bool Command(string[] commands)
  {
    if (commands[0] == "restart")
    {
      DebugProtocol.GetProtocol().Command("restart");
      return true;
    }
    if (commands[0] == "reset_mobs")
      DebugProtocol.GetProtocol().Command("reset_mobs");
    return false;
  }

  public static bool GiveSystem(string[] commands)
  {
    return DebugUtility.GiveItem("system", commands);
  }

  public static bool GiveConsumable(string[] commands)
  {
    return DebugUtility.GiveItem("consumable", commands);
  }

  public static bool GiveResource(string[] commands)
  {
    return DebugUtility.GiveItem("resource", commands);
  }

  public static bool GiveItem(string itemType, string[] commands)
  {
    List<string> stringList = new List<string>((IEnumerable<string>) commands);
    stringList.Insert(0, itemType);
    if (stringList.Count < 2 || stringList.Count > 4)
      return false;
    DebugProtocol.GetProtocol().CommandList(stringList.ToArray());
    return true;
  }

  public static bool GiveExperience(string[] commands)
  {
    return DebugUtility.GiveItem("experience", commands);
  }
}
