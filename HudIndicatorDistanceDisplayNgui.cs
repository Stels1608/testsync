﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorDistanceDisplayNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HudIndicatorDistanceDisplayNgui : HudIndicatorBaseComponentNgui, IReceivesTarget
{
  private readonly HudIndicatorDistanceStringCache distanceStringCache = HudIndicatorDistanceStringCache.Instance;
  [SerializeField]
  protected UISprite distanceLine;
  [SerializeField]
  protected UILabel distanceLabel;
  private int distanceCurrent;
  private ISpaceEntity target;

  protected UIAtlas Atlas
  {
    get
    {
      return HudIndicatorsAtlasLinker.Instance.Atlas;
    }
  }

  public override bool RequiresAnimationNgui
  {
    get
    {
      return false;
    }
  }

  protected override void ResetComponentStates()
  {
  }

  protected override void Awake()
  {
    base.Awake();
    this.distanceLine = NGUIToolsExtension.AddSprite(this.gameObject, this.Atlas, this.GetDistanceUiLineAtlasSpriteName(), true, string.Empty);
    this.distanceLine.transform.localPosition = new Vector3(50f, -6f);
    this.distanceLabel = NGUIToolsExtension.AddLabel(this.gameObject, Gui.Options.FontDS_EF_M, 9, UIWidget.Pivot.Left, string.Empty);
    this.distanceLabel.transform.localPosition = new Vector3(69f, -6f);
    this.UpdateView();
  }

  private void Update()
  {
    if (this.target == null || Game.Me.Ship == null)
      return;
    this.distanceLabel.text = this.distanceStringCache.GetDistanceString((int) (this.target.Position - Game.Me.Ship.Position).magnitude);
  }

  protected string GetDistanceUiLineAtlasSpriteName()
  {
    return "HudIndicator_Distance";
  }

  public void OnTargetInjection(ISpaceEntity target)
  {
    this.target = target;
    this.UpdateColorAndAlpha();
  }

  protected override void SetColoring()
  {
    UILabel uiLabel = this.distanceLabel;
    Color color4LegacyBracket = HudIndicatorColorInfo.GetTargetColor4LegacyBracket(this.target);
    this.distanceLine.color = color4LegacyBracket;
    Color color = color4LegacyBracket;
    uiLabel.color = color;
  }

  protected override void UpdateView()
  {
    this.gameObject.SetActive((this.IsSelected || this.IsMultiselected || (this.InScannerRange || this.IsActiveWaypoint)) && this.InScreen);
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }
}
