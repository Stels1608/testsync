﻿// Decompiled with JetBrains decompiler
// Type: EditorLevel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor;
using BsgoEditor.GUI;
using Json;
using Loaders;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class EditorLevel : MonoBehaviour
{
  public static string cylonProgressionPointName = "_CylonProgressionPoints";
  public static string colonialProgressionPointName = "_ColonialProgressionPoints";
  public static int CurrentSectorLoaded = 0;
  public static Dictionary<string, JWorldCard> asteroidTypes = (Dictionary<string, JWorldCard>) null;
  public static Dictionary<string, JWorldCard> planetoidTypes = (Dictionary<string, JWorldCard>) null;
  public static string[] asteroidFamilies = new string[3]{ "asteroid_1", "asteroid_2", "asteroid_3" };
  public static string[] asteroidVariants = new string[5]{ "1", "2", "3", "4", "5" };
  public static Dictionary<string, JWorldCard> decorationsTypes = (Dictionary<string, JWorldCard>) null;
  public static Dictionary<string, JWorldCard> eventObjectTypes = (Dictionary<string, JWorldCard>) null;
  public static string[] allEnemyStrings = (string[]) null;
  public static string[] allMobsStrings = (string[]) null;
  public static string[] allBonusStrings = (string[]) null;
  public static Dictionary<string, JWorldCard> planetCards = (Dictionary<string, JWorldCard>) null;
  public static string[] planetTypes = (string[]) null;
  public static string[] allDotAreas = (string[]) null;
  public static string[] lootRespawnPoints = (string[]) null;
  private bool showTriggerAreas = true;
  private bool showDotAreas = true;
  private bool enableBloom = true;
  protected EditorGUIManager guiManager;
  public EditorGalaxyMap galaxyMap;
  public static JSectorDescription desc;
  public GameObject boundBox;
  public GameObject testShip;
  public GameObject decorationCollector;
  public GameObject respawnPoint;
  public GameObject triggerPoint;
  public GameObject staticMobRespawnPoint;
  public GameObject randomMobRespawnPoint;
  public GameObject startingPointCylons;
  public GameObject startingPointCylonOutpost;
  public GameObject startingPointColonials;
  public GameObject startingPointColonialOutpost;
  public GameObject cruisersSP;
  public GameObject convoyPointsParent;
  public GameObject wayPointsParent;
  public GameObject dotPoint;
  public GameObject asteroidsParent;
  public GameObject planetoidsParent;
  public GameObject eventObjectsParent;
  public GameObject cylonProgressionRootPoint;
  public GameObject colonialProgressionRootPoint;
  public GameObject sectorEvents;
  private Camera mainCamera;
  private bool hideUi;
  private static EditorLevel instance;
  private SectorIO sectorIO;
  public static CoolCamera activeCoolCam;

  public static Camera GetActiveCamera
  {
    get
    {
      foreach (Camera camera in UnityEngine.Object.FindObjectsOfType<Camera>())
      {
        CoolCamera coolCamera = camera.GetComponent<CoolCamera>() ?? (!((UnityEngine.Object) camera.transform.parent == (UnityEngine.Object) null) ? camera.transform.parent.GetComponent<CoolCamera>() : (CoolCamera) null);
        if ((UnityEngine.Object) coolCamera != (UnityEngine.Object) null && coolCamera.enabled)
          return camera;
      }
      UnityEngine.Debug.LogError((object) "GetActiveCamera(): Couldn't find active cam");
      return (Camera) null;
    }
  }

  public static EditorLevel GetInstance()
  {
    return EditorLevel.instance;
  }

  public SectorIO GetSectorIO()
  {
    if (this.sectorIO == null)
      this.sectorIO = new SectorIO();
    return this.sectorIO;
  }

  public void Start()
  {
    LODScript.LodSystemEnabled = false;
    EditorLevel.CurrentSectorLoaded = 0;
    Game.Create();
    EditorLevel.instance = this;
    this.mainCamera = Camera.main ?? GameObject.FindGameObjectWithTag("MainCamera").GetComponentsInChildren<Camera>(true)[0];
    this.StartCoroutine(this.Preload());
  }

  [DebuggerHidden]
  private IEnumerator Preload()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new EditorLevel.\u003CPreload\u003Ec__Iterator29() { \u003C\u003Ef__this = this };
  }

  public void Update()
  {
    if (this.guiManager == null)
      return;
    this.guiManager.Update();
  }

  public void OnGUI()
  {
    if (this.hideUi)
      return;
    UnityEngine.GUI.BeginGroup(new Rect(5f, 5f, 250f, 150f));
    if (EditorLevel.desc != null)
      GUILayout.Label("Sector Loaded = " + EditorLevel.CurrentSectorLoaded.ToString() + "\nSector Name = " + EditorLevel.desc.Name);
    string str = "http://" + SectorEditorHelper.HelperDefaultIP + ":8000/editor/";
    if (GUILayout.Button("Open Sector ContentDB File"))
      Application.OpenURL(str + "sector" + (object) EditorLevel.CurrentSectorLoaded);
    EditorLevel.activeCoolCam = ((IEnumerable<CoolCamera>) UnityEngine.Object.FindObjectsOfType<CoolCamera>()).FirstOrDefault<CoolCamera>((Func<CoolCamera, bool>) (c => c.enabled));
    if ((UnityEngine.Object) EditorLevel.activeCoolCam == (UnityEngine.Object) null)
    {
      EditorLevel.activeCoolCam = ((IEnumerable<CoolCamera>) UnityEngine.Object.FindObjectsOfType<CoolCamera>()).FirstOrDefault<CoolCamera>();
      EditorLevel.SwitchCamera();
    }
    if ((UnityEngine.Object) EditorLevel.activeCoolCam != (UnityEngine.Object) null && GUILayout.Button("Switch Camera (Now: " + EditorLevel.activeCoolCam.gameObject.name + ")"))
      EditorLevel.SwitchCamera();
    UnityEngine.GUI.EndGroup();
  }

  public static void SwitchCameraTo(Camera camera)
  {
    Camera[] objectsOfType = UnityEngine.Object.FindObjectsOfType<Camera>();
    for (int index = 0; index < objectsOfType.Length; ++index)
    {
      if ((UnityEngine.Object) EditorLevel.SwitchCamera() == (UnityEngine.Object) camera)
        return;
    }
    UnityEngine.Debug.Log((object) ("Couldn't switch to / find given camera: " + (object) camera));
  }

  public static Camera SwitchCamera()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    EditorLevel.\u003CSwitchCamera\u003Ec__AnonStoreyD9 cameraCAnonStoreyD9 = new EditorLevel.\u003CSwitchCamera\u003Ec__AnonStoreyD9();
    Camera[] objectsOfType = UnityEngine.Object.FindObjectsOfType<Camera>();
    List<Camera> cameraList = new List<Camera>();
    // ISSUE: reference to a compiler-generated field
    cameraCAnonStoreyD9.activeCam = (Camera) null;
    foreach (Camera camera in objectsOfType)
    {
      string[] strArray = new string[3]{ "OverlayCamera", "BgCamera", "Camera" };
      CoolCamera coolCamera1 = camera.GetComponent<CoolCamera>() ?? (!((UnityEngine.Object) camera.transform.parent == (UnityEngine.Object) null) ? camera.transform.parent.GetComponent<CoolCamera>() : (CoolCamera) null);
      if ((UnityEngine.Object) coolCamera1 != (UnityEngine.Object) null && coolCamera1.enabled)
      {
        // ISSUE: reference to a compiler-generated field
        cameraCAnonStoreyD9.activeCam = camera;
        CoolCamera coolCamera2 = coolCamera1;
        bool flag = false;
        camera.enabled = flag;
        int num = flag ? 1 : 0;
        coolCamera2.enabled = num != 0;
      }
      if (!((IEnumerable<string>) strArray).Contains<string>(camera.gameObject.name))
        cameraList.Add(camera);
    }
    // ISSUE: reference to a compiler-generated method
    int index = (cameraList.FindIndex(new Predicate<Camera>(cameraCAnonStoreyD9.\u003C\u003Em__21A)) + 1) % cameraList.Count;
    Camera camera1 = cameraList[index];
    CoolCamera coolCamera3 = camera1.GetComponent<CoolCamera>() ?? (!((UnityEngine.Object) camera1.transform.parent == (UnityEngine.Object) null) ? camera1.transform.parent.GetComponent<CoolCamera>() : (CoolCamera) null);
    if ((UnityEngine.Object) coolCamera3 == (UnityEngine.Object) null)
      coolCamera3 = camera1.gameObject.AddComponent<CoolCamera>();
    coolCamera3.SetMainCamera(camera1);
    CoolCamera coolCamera4 = coolCamera3;
    bool flag1 = true;
    camera1.enabled = flag1;
    int num1 = flag1 ? 1 : 0;
    coolCamera4.enabled = num1 != 0;
    LODScript.SetActiveCamera(camera1);
    return camera1;
  }

  private void CreateEnvironment()
  {
    if (EditorLevel.asteroidTypes == null)
      EditorLevel.asteroidTypes = ((IEnumerable<string>) ContentDB.GetFiles("asteroid_*")).Select<string, JWorldCard>((Func<string, JWorldCard>) (i => ContentDB.GetDocument<JWorldCard>(i))).ToDictionary<JWorldCard, string, JWorldCard>((Func<JWorldCard, string>) (j => j.Id), (Func<JWorldCard, JWorldCard>) (j => j));
    if (EditorLevel.planetoidTypes == null)
      EditorLevel.planetoidTypes = ((IEnumerable<string>) ContentDB.GetFiles("planetoid_*")).Select<string, JWorldCard>((Func<string, JWorldCard>) (i => ContentDB.GetDocument<JWorldCard>(i))).ToDictionary<JWorldCard, string, JWorldCard>((Func<JWorldCard, string>) (j => j.Id), (Func<JWorldCard, JWorldCard>) (j => j));
    if (EditorLevel.allEnemyStrings == null)
      EditorLevel.allEnemyStrings = ContentDB.GetFiles("enemy_*");
    if (EditorLevel.allDotAreas == null)
      EditorLevel.allDotAreas = ContentDB.GetFiles("dot_area*");
    if (EditorLevel.allMobsStrings == null)
    {
      List<string> stringList = new List<string>();
      foreach (string file in ContentDB.GetFiles("mob_*"))
        stringList.Add(file);
      foreach (string file in ContentDB.GetFiles("convoy_*"))
        stringList.Add(file);
      foreach (string file in ContentDB.GetFiles("patrol_*"))
        stringList.Add(file);
      EditorLevel.allMobsStrings = stringList.ToArray();
    }
    if (EditorLevel.allBonusStrings == null)
      EditorLevel.allBonusStrings = ContentDB.GetFiles("*bonus_*");
    if (EditorLevel.lootRespawnPoints == null)
      EditorLevel.lootRespawnPoints = ContentDB.GetFiles("loot_*");
    if (EditorLevel.planetCards == null)
    {
      EditorLevel.planetCards = new Dictionary<string, JWorldCard>();
      string[] files = ContentDB.GetFiles("planet_*");
      List<string> stringList = new List<string>();
      for (int index = 0; index < files.Length; ++index)
      {
        JWorldCard document = ContentDB.GetDocument<JWorldCard>(files[index]);
        EditorLevel.planetCards.Add(document.Id, document);
        stringList.Add(document.Id);
      }
      EditorLevel.planetTypes = stringList.ToArray();
    }
    if (EditorLevel.decorationsTypes == null)
    {
      EditorLevel.decorationsTypes = new Dictionary<string, JWorldCard>();
      foreach (string file in ContentDB.GetFiles("decoration_*"))
      {
        JWorldCard document = ContentDB.GetDocument<JWorldCard>(file);
        string key = document.Id;
        if (key == string.Empty)
          key = document.prefabName;
        try
        {
          EditorLevel.decorationsTypes.Add(key, document);
        }
        catch (Exception ex)
        {
          UnityEngine.Debug.LogWarning((object) ("Duplicate decoration: " + key));
        }
      }
    }
    if (EditorLevel.eventObjectTypes == null)
    {
      EditorLevel.eventObjectTypes = new Dictionary<string, JWorldCard>();
      foreach (string file in ContentDB.GetFiles("global_bonus*"))
      {
        JWorldCard document1 = ContentDB.GetDocument<JWorldCard>(file);
        if (!(document1.category != "asteroid"))
        {
          string id = document1.Id;
          if (string.IsNullOrEmpty(document1.prefabName) && !string.IsNullOrEmpty(document1.ship))
          {
            JWorldCard document2 = ContentDB.GetDocument<JWorldCard>(document1.ship);
            document1.prefabName = document2.prefabName;
          }
          EditorLevel.eventObjectTypes.Add(id, document1);
        }
      }
    }
    this.testShip = GameObject.Find("TestShip");
    if ((UnityEngine.Object) this.testShip == (UnityEngine.Object) null)
    {
      UnityEngine.Debug.LogError((object) "TestShip not found");
    }
    else
    {
      this.testShip.SetActive(false);
      EditorLevel.desc = SectorIO.Load(EditorLevel.CurrentSectorLoaded);
      this.asteroidsParent = new GameObject("_Asteroids");
      using (List<Asteroid>.Enumerator enumerator = EditorLevel.desc.Asteroids.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Asteroid current = enumerator.Current;
          current.SetDistance(0.0f);
          current.Constructed = (System.Action<GameObject>) (go => go.transform.parent = this.asteroidsParent.transform);
          if (!EditorLevel.asteroidTypes.ContainsKey(current.type))
          {
            UnityEngine.Debug.LogError((object) ("Asteroid Type " + current.type + " is unknown."));
          }
          else
          {
            current.WorldCard = EditorLevel.asteroidTypes[current.type].FormWorldCard();
            current.Construct();
            current.Scale = current.Radius * 0.05f;
            current.Root.name = "AsteriodSpaceObject";
          }
        }
      }
      this.CreatePlanetoids(EditorLevel.desc.Planetoids);
      using (List<AsteroidBelt>.Enumerator enumerator = EditorLevel.desc.Belts.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.InitAfterAutoConstruct();
      }
      using (List<PlanetReal>.Enumerator enumerator = EditorLevel.desc.PlanetsReal.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.PostInit();
      }
      RenderSettings.ambientLight = EditorLevel.desc.AmbientColor;
      this.AlterLights(EditorLevel.desc.Lights);
      this.AlterStarFog(EditorLevel.desc.Starfog);
      this.AlterStarDust(EditorLevel.desc.Stardust);
      this.AlterGlobalFog(EditorLevel.desc.GlobalFog);
      new BackgroundConstructor(EditorLevel.desc.Nebula, SectorIO.NebulaXPath).Construct();
      new BackgroundConstructor(EditorLevel.desc.Stars, SectorIO.StarsXPath).Construct();
      new BackgroundConstructor(EditorLevel.desc.StarsMultiply, SectorIO.Starsmultiply).Construct();
      new BackgroundConstructor(EditorLevel.desc.StarsVariance, SectorIO.StarsvariancesXPath).Construct();
      using (List<SunDesc>.Enumerator enumerator = EditorLevel.desc.Suns.GetEnumerator())
      {
        while (enumerator.MoveNext())
          new SunConstructor(enumerator.Current).Construct();
      }
      using (List<MovingNebulaDesc>.Enumerator enumerator = EditorLevel.desc.MovingNebulas.GetEnumerator())
      {
        while (enumerator.MoveNext())
          new MovingNebulaConstructor(enumerator.Current).Construct();
      }
      this.CreateBoundaryBox(EditorLevel.desc.Size);
      this.CreateRespawning(EditorLevel.desc.Respawn);
      if (EditorLevel.desc.TriggerPoints == null)
        EditorLevel.desc.TriggerPoints = new List<TriggerPoint>();
      this.CreatePositionTrigger(EditorLevel.desc.TriggerPoints);
      if (EditorLevel.desc.DotAreas == null)
        EditorLevel.desc.DotAreas = new List<DotArea>();
      this.CreateDotPoint(EditorLevel.desc.DotAreas);
      if (EditorLevel.desc.StaticMobRespawn == null)
        EditorLevel.desc.StaticMobRespawn = new List<MobRespawnPoint>();
      this.CreateStaticBotRespawning(EditorLevel.desc.StaticMobRespawn);
      if (EditorLevel.desc.RandomMobRespawn == null)
        EditorLevel.desc.RandomMobRespawn = new List<MobRespawnPoint>();
      this.CreateRandomBotRespawning(EditorLevel.desc.RandomMobRespawn);
      this.CreateDecorations(EditorLevel.desc.Decorations);
      this.CreateEventObjects(EditorLevel.desc.EventObjects);
      this.CreateStartingPoint(EditorLevel.desc.StartingPoints);
      this.CreateCruisersSPs(EditorLevel.desc.CruiserSPs);
      this.CreateConvoys(EditorLevel.desc.ConvoyPoints);
      this.CreateWayPoints(EditorLevel.desc.WayPoints);
      this.CreateSectorEvents(EditorLevel.desc.sectorEvents);
      this.CreateCylonProgressionPoints(EditorLevel.desc.cylonProgressionPoints);
      this.CreateColonialProgressionPoints(EditorLevel.desc.colonialProgressionPoints);
    }
  }

  private void CreateColonialProgressionPoints(ProgressionPoint colonialProgressionPoints)
  {
    this.colonialProgressionRootPoint = new GameObject(EditorLevel.colonialProgressionPointName);
    this.CreateProgressionPoints(colonialProgressionPoints, this.colonialProgressionRootPoint);
  }

  private void CreateCylonProgressionPoints(ProgressionPoint cylonProgressionPoints)
  {
    this.cylonProgressionRootPoint = new GameObject(EditorLevel.cylonProgressionPointName);
    this.CreateProgressionPoints(cylonProgressionPoints, this.cylonProgressionRootPoint);
  }

  private void CreateProgressionPoints(ProgressionPoint point, GameObject root)
  {
    using (List<ProgressionPointEntity>.Enumerator enumerator = new List<ProgressionPointEntity>() { point.outpost, point.beacon, point.platformOne, point.platformTwo, point.platformThree, point.platformFour }.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ProgressionPointEntity current = enumerator.Current;
        if (current != null && (UnityEngine.Object) root != (UnityEngine.Object) null)
          current.gameObject.transform.parent = root.transform;
      }
    }
  }

  private void CreatePlanetoids(List<JPlanetoid> list)
  {
    this.planetoidsParent = new GameObject("_Planetoids");
    using (List<JPlanetoid>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JPlanetoid current = enumerator.Current;
        try
        {
          Tools.ConstructPlanetoid(current);
        }
        catch (Exception ex)
        {
          UnityEngine.Debug.LogError((object) ex.Message);
        }
      }
    }
    list.Clear();
  }

  private void CreateRespawning(RespawnProperty desc)
  {
    this.respawnPoint = new GameObject();
    this.respawnPoint.name = "_LootRespawnPoints(white spheres)";
    using (List<RespawnPoint>.Enumerator enumerator = desc.points.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        RespawnPoint current = enumerator.Current;
        current.gameObject.transform.parent = this.respawnPoint.transform;
        current.gameObject.name = current.Types[0].type;
      }
    }
    desc.points.Clear();
  }

  private void CreatePositionTrigger(List<TriggerPoint> desc)
  {
    this.triggerPoint = new GameObject();
    this.triggerPoint.name = "_TriggerPoints (yellow sphere)";
    using (List<TriggerPoint>.Enumerator enumerator = desc.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        TriggerPoint current = enumerator.Current;
        current.gameObject.transform.parent = this.triggerPoint.transform;
        float num = current.radius * 2f;
        current.gameObject.transform.localScale = new Vector3(num, num, num);
      }
    }
    desc.Clear();
  }

  private void CreateDotPoint(List<DotArea> desc)
  {
    this.dotPoint = new GameObject();
    this.dotPoint.name = "_DotPoint (red Spheres)";
    using (List<DotArea>.Enumerator enumerator = desc.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DotArea current = enumerator.Current;
        current.gameObject.transform.parent = this.dotPoint.transform;
        JsonData jsonData = ContentDB.GetDocumentRaw(current.key).Object["Collider"].Object["size"];
        current.gameObject.transform.localScale = new Vector3(jsonData.Object["x"].Float, jsonData.Object["y"].Float, jsonData.Object["z"].Float);
        current.gameObject.name = current.key;
      }
    }
    desc.Clear();
  }

  private void CreateStaticBotRespawning(List<MobRespawnPoint> desc)
  {
    this.staticMobRespawnPoint = new GameObject();
    this.staticMobRespawnPoint.name = "_StaticMobRespawners";
    using (List<MobRespawnPoint>.Enumerator enumerator = desc.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MobRespawnPoint current = enumerator.Current;
        current.gameObject.transform.parent = this.staticMobRespawnPoint.transform;
        current.AfterInit();
      }
    }
    desc.Clear();
  }

  private void CreateEventObjects(List<JEventObject> desc)
  {
    this.eventObjectsParent = new GameObject("_EventObjects");
    using (List<JEventObject>.Enumerator enumerator = desc.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JEventObject current = enumerator.Current;
        JWorldCard jworldCard = EditorLevel.eventObjectTypes[current.key];
        EventObject eventObject = new EventObject(0U);
        eventObject.EventActivation = current.eventActivation;
        eventObject.SpawnBox = current.spawnBox;
        eventObject.WorldCard = jworldCard.FormWorldCard();
        eventObject.Construct();
        GameObject gameObject = eventObject.Root.gameObject;
        gameObject.name = jworldCard.Id;
        gameObject.transform.parent = this.eventObjectsParent.transform;
        gameObject.transform.position = current.position;
        gameObject.transform.rotation = current.rotation;
      }
    }
    desc.Clear();
  }

  private void CreateRandomBotRespawning(List<MobRespawnPoint> desc)
  {
    this.randomMobRespawnPoint = new GameObject();
    this.randomMobRespawnPoint.name = "_RandomMobRespawners";
    using (List<MobRespawnPoint>.Enumerator enumerator = desc.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MobRespawnPoint current = enumerator.Current;
        current.gameObject.transform.parent = this.randomMobRespawnPoint.transform;
        current.m_random = true;
        current.AfterInit();
      }
    }
    desc.Clear();
  }

  private void CreateDecorations(List<JDecorationDescription> list)
  {
    this.decorationCollector = new GameObject();
    this.decorationCollector.name = "_Decorations";
    using (List<JDecorationDescription>.Enumerator enumerator1 = list.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        JDecorationDescription current1 = enumerator1.Current;
        if (!EditorLevel.decorationsTypes.ContainsKey(current1.key))
        {
          string message = "Decoration " + current1.key + " is invalid. We have only ";
          using (Dictionary<string, JWorldCard>.KeyCollection.Enumerator enumerator2 = EditorLevel.decorationsTypes.Keys.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              string current2 = enumerator2.Current;
              message = message + "'" + current2 + "' ";
            }
          }
          throw new Exception(message);
        }
        JWorldCard jworldCard = EditorLevel.decorationsTypes[current1.key];
        DebrisPile debrisPile = new DebrisPile(0U);
        debrisPile.RotationSpeed = current1.rotationSpeed;
        debrisPile.WorldCard = jworldCard.FormWorldCard();
        debrisPile.Construct();
        GameObject gameObject = debrisPile.Root.gameObject;
        gameObject.name = jworldCard.Id;
        gameObject.transform.parent = this.decorationCollector.transform;
        gameObject.transform.position = current1.position;
        gameObject.transform.rotation = current1.rotation;
        gameObject.transform.localScale = current1.scale;
      }
    }
  }

  private void CreateStartingPoint(JStartingPoints desc)
  {
    this.startingPointCylons = new GameObject();
    this.startingPointCylons.name = "_StartingPointCylons (red parallgrm)";
    this.startingPointColonials = new GameObject();
    this.startingPointColonials.name = "_StartingPointColonials (blue parallgrm)";
    this.startingPointCylonOutpost = new GameObject();
    this.startingPointCylonOutpost.name = "_StartingPointCylonOutpost (red parallgrm)";
    this.startingPointColonialOutpost = new GameObject();
    this.startingPointColonialOutpost.name = "_StartingPointColonialOutpost (blue parallgrm)";
    if (desc.Cylon == null)
      desc.Cylon = new List<PlayerRespawnPoint>();
    if (desc.Colonial == null)
      desc.Colonial = new List<PlayerRespawnPoint>();
    using (List<PlayerRespawnPoint>.Enumerator enumerator = desc.Cylon.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerRespawnPoint current = enumerator.Current;
        current.gameObject.transform.parent = this.startingPointCylons.transform;
        current.Color = current.CylonColor;
      }
    }
    using (List<PlayerRespawnPoint>.Enumerator enumerator = desc.Colonial.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerRespawnPoint current = enumerator.Current;
        current.gameObject.transform.parent = this.startingPointColonials.transform;
        current.Color = current.ColonialColor;
      }
    }
    desc.Colonial.Clear();
    desc.Cylon.Clear();
    if (desc.CylonOutpost == null)
      desc.CylonOutpost = new List<PlayerRespawnPoint>();
    if (desc.ColonialOutpost == null)
      desc.ColonialOutpost = new List<PlayerRespawnPoint>();
    using (List<PlayerRespawnPoint>.Enumerator enumerator = desc.CylonOutpost.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerRespawnPoint current = enumerator.Current;
        current.gameObject.transform.parent = this.startingPointCylonOutpost.transform;
        current.Color = current.CylonColor;
      }
    }
    using (List<PlayerRespawnPoint>.Enumerator enumerator = desc.ColonialOutpost.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        PlayerRespawnPoint current = enumerator.Current;
        current.gameObject.transform.parent = this.startingPointColonialOutpost.transform;
        current.Color = current.ColonialColor;
      }
    }
    desc.ColonialOutpost.Clear();
    desc.CylonOutpost.Clear();
  }

  private void CreateCruisersSPs(List<CruiserSP> list)
  {
    this.cruisersSP = new GameObject();
    this.cruisersSP.name = "_CruiserSPs (white parallgrm)";
    using (List<CruiserSP>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        CruiserSP current = enumerator.Current;
        current.gameObject.transform.parent = this.cruisersSP.transform;
        current.LateInit();
      }
    }
    list.Clear();
  }

  private void CreateWayPoints(List<WayPointPath> list)
  {
    this.wayPointsParent = new GameObject();
    this.wayPointsParent.name = "_Waypoints";
    using (List<WayPointPath>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        WayPointPath current = enumerator.Current;
        current.gameObject.transform.parent = this.wayPointsParent.transform;
        current.InitAfterAutoConstruct();
      }
    }
    list.Clear();
  }

  private void CreateConvoys(List<ConvoyGo> list)
  {
    this.convoyPointsParent = new GameObject();
    this.convoyPointsParent.name = "_Convoy";
    using (List<ConvoyGo>.Enumerator enumerator = list.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ConvoyGo current = enumerator.Current;
        current.gameObject.transform.parent = this.convoyPointsParent.transform;
        current.InitAfterAutoConstruct();
      }
    }
    list.Clear();
  }

  private void AlterLights(List<JLightDescription> lights)
  {
    GameObject gameObject1 = ((Component) UnityEngine.Object.FindObjectOfType(typeof (Light))).gameObject;
    using (List<JLightDescription>.Enumerator enumerator = lights.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JLightDescription current = enumerator.Current;
        GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(gameObject1);
        Light componentInChildren = gameObject2.GetComponentInChildren<Light>();
        gameObject2.name = current.name;
        gameObject2.transform.position = current.position;
        gameObject2.transform.rotation = current.rotation;
        componentInChildren.color = current.color;
        componentInChildren.intensity = current.intensity * 2f;
      }
    }
    if (lights.Count <= 0)
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) gameObject1);
  }

  private void AlterStarFog(JStarfog desc)
  {
    StarFog2 component = this.mainCamera.GetComponent<StarFog2>();
    component.Color = desc.Color;
    component.CloudCount = desc.MaxPoints;
    component.Recreate();
  }

  private void AlterStarDust(JStardust desc)
  {
    this.mainCamera.GetComponent<Stardust>().Initialize(desc.Color);
  }

  private void AlterGlobalFog(JGlobalFog desc)
  {
    desc.Apply();
  }

  private void CreateBoundaryBox(Vector3 size)
  {
    this.boundBox = new GameObject("_Boundaries");
    this.boundBox.AddComponent<CubeGizmos>().Size = size;
  }

  private void CreateSectorEvents(List<JSectorEvents> jSectorEvents)
  {
    this.sectorEvents = new GameObject("_SectorEvents");
    if (jSectorEvents == null)
      return;
    using (List<JSectorEvents>.Enumerator enumerator = jSectorEvents.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JSectorEvents current = enumerator.Current;
        GameObject primitive = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        primitive.GetComponent<Renderer>().material.color = new Color(0.5f, 0.5f, 0.1f, 0.6f);
        primitive.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
        primitive.AddComponent<SectorEventObjects>().Init(current);
        primitive.transform.localScale = new Vector3(2000f, 2000f, 2000f);
        primitive.transform.parent = this.sectorEvents.transform;
      }
    }
  }
}
