﻿// Decompiled with JetBrains decompiler
// Type: CacheAuthorizationData
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public struct CacheAuthorizationData
{
  public string ProductName;
  public string Domain;
  public long CacheSize;
  public string Signature;

  public static CacheAuthorizationData DevRidingClub
  {
    get
    {
      return new CacheAuthorizationData() { ProductName = "BGO", Domain = "http://dev.ridingclub.com/", CacheSize = 524288000, Signature = "92d847b6e10a3719237317f6df30ad34191fa68369ee087520fb2a3618f7b1c1e4c845e1646aa41e25a4eafcad98062f5382df80a798640e8335d35471ac2a64fec2b21c02865f2949b54c7ff5f80c3966b27f461e74840c86da48f12f36b9db7fdc22c00c524497d38fcdd3fed2a00eb50a1fcee7a7c93f51925f2dc5cf932a" };
    }
  }

  public static CacheAuthorizationData Bigpoint
  {
    get
    {
      return new CacheAuthorizationData() { ProductName = "BGO", Domain = "http://gs.bgo.bigpoint.com/", CacheSize = 524288000, Signature = "6098e9c3bf8298b86c526e5600e66daebc366efc87872d6abcc5e6bbb37506fed4ad3f1f59c4378ef7780a17fc7355b814604981fe5f1d348d119b02e53ed6b0279d902fb5c09914889ee50e1346f292cf51917483b1fbab7335591efad78170816798d5eda52b4e3e3cc33039de52c5f70c26f989aebccdcd5af1d4c6776366" };
    }
  }

  public static CacheAuthorizationData CDNBigpoint
  {
    get
    {
      return new CacheAuthorizationData() { ProductName = "BGO", Domain = "http://cdn.battlestar-galactica.bigpoint.com/", CacheSize = (long) int.MaxValue, Signature = "8c0c23a6a2063aa2843a5bb8ed0bc6ad0156d142560546eef5f7cea26e16109a0a7f3a4f1448eb200f128677073487e8f03144d970001aa5b0e7b990c0ab1910b9bddddd5d2e8ec741d22fc6fb42cbb56af6b7b01cc86f4d7a341667afdff0564c80d06b2a83fb1d9c7be87dad556420714f01925378a9c6324ebe43d71d077e" };
    }
  }

  public static CacheAuthorizationData BPCDN
  {
    get
    {
      return new CacheAuthorizationData() { ProductName = "BGO", Domain = "http://bpcdn.net/", CacheSize = (long) int.MaxValue, Signature = "1da2b4bd08eec112d7cfbf1090b79dea31d0bb9cb4137f2e364749664affc1ac4e1fba26df709cac101453f2bca1fab77644a0f0d8a6e3db8ed1f8fe011f7916a81dd163df63f38a26990914e30e2ad1522cdbf048dd7b3aa660a8d351e2773d9105f272cfb7be92e2e4e148dd364e07ce872dc788febf8582dab8cc5293b35c" };
    }
  }

  public static string BaseURLHack_BPCDN
  {
    get
    {
      return "bpcdn.net";
    }
  }
}
