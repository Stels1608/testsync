﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DMapIcon
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Paths;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class SystemMap3DMapIcon : MonoBehaviour
{
  private Transform[] invariantSizeChildren = new Transform[0];
  private Transform[] billboardChildren = new Transform[0];
  private string fadeClipName = "UiMapIconFadeIn";
  public float ManualColliderScaling = 1f;
  public AnonymousDelegate OnMouseEntered = (AnonymousDelegate) (() => {});
  public AnonymousDelegate OnMouseLeft = (AnonymousDelegate) (() => {});
  public AnonymousDelegate OnMouseClicked = (AnonymousDelegate) (() => {});
  public AnonymousDelegate OnMouseDoubleClicked = (AnonymousDelegate) (() => {});
  public float Scale = 0.1f;
  private float cachedScale = 0.1f;
  private float cachedFov = 1f;
  private SphereCollider clickCollider;
  private Transform myTransform;
  private SystemMap3DMapIconLinker iconLinker;
  private SystemMap3DMapIcon.BracketState _bracketStates;
  private SpriteRenderer _bracketSpriteRenderer;
  private SpriteRenderer _partyMemberSpriteRenderer;
  private SpriteRenderer _waypointSpriteRenderer;
  private IEnumerator deactivationCoroutine;
  private string _debugName;
  private float lastClickTime;
  private bool _isSelected;
  private bool _isPartyMember;
  private bool _isWaypoint;
  private bool _gameObjectDestroyedErrorDetected;
  private bool? _isShown;
  private float cachedSizeFactor;
  private float cachedParentScale;
  private float cachedParentScaleInv;
  private Camera cachedCamera;
  private Transform cachedCameraTransform;

  public bool IsSelected
  {
    get
    {
      return this._isSelected;
    }
    set
    {
      this._isSelected = value;
      if (this._isSelected)
      {
        this.Show(true);
        this.SetBracketState(SystemMap3DMapIcon.BracketState.Selected);
      }
      else
        this.UnsetBracketState(SystemMap3DMapIcon.BracketState.Selected);
    }
  }

  public bool IsPartyMember
  {
    get
    {
      return this._isPartyMember;
    }
    set
    {
      bool flag = this._isPartyMember != value;
      this._isPartyMember = value;
      if (flag && this._isPartyMember)
        this.AddPartyMemberIcon();
      if (!flag || this._isPartyMember)
        return;
      this.RemovePartyMemberIcon();
    }
  }

  public bool IsWaypoint
  {
    get
    {
      return this._isWaypoint;
    }
    set
    {
      bool flag = this._isWaypoint != value;
      this._isWaypoint = value;
      if (flag && this._isWaypoint)
        this.AddWaypointIcon();
      if (!flag || this._isWaypoint)
        return;
      this.RemoveWaypointIcon();
    }
  }

  private void Awake()
  {
    this.iconLinker = Resources.Load<GameObject>(Ui.Prefabs + "/SystemMap3D/SystemMap3DMapIconLinker").GetComponent<SystemMap3DMapIconLinker>();
    this.UpdateBracket();
    this.myTransform = this.transform;
  }

  private void Start()
  {
    this._debugName = this.name;
  }

  public void UpdateOrientationAndSize(Quaternion cameraRotation)
  {
    if ((UnityEngine.Object) this.gameObject == (UnityEngine.Object) null)
    {
      if (!this._gameObjectDestroyedErrorDetected)
        UnityEngine.Debug.LogError((object) ("SystemMap3DMapIcon: GameObject is null. Name:  " + this._debugName));
      this._gameObjectDestroyedErrorDetected = true;
    }
    else
    {
      if (!this.gameObject.activeInHierarchy)
        return;
      this.UpdateBillboards(cameraRotation);
      this.UpdateInvariantSizes();
    }
  }

  private SpriteRenderer AddSpriteBase(UnityEngine.Sprite sprite)
  {
    GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(Resources.Load<GameObject>(Ui.Prefabs + "/SystemMap3D/SystemMap3DMapIconSprite"));
    gameObject.name = "MapIcon - " + sprite.name;
    gameObject.transform.parent = this.gameObject.transform;
    gameObject.transform.localPosition = Vector3.zero;
    gameObject.transform.localRotation = Quaternion.identity;
    gameObject.transform.localScale = Vector3.one;
    gameObject.layer = this.gameObject.layer;
    SpriteRenderer component = gameObject.GetComponent<SpriteRenderer>();
    component.sprite = sprite;
    return component;
  }

  public SpriteRenderer AddSizeInvariantSprite(UnityEngine.Sprite sprite, bool isBillboard)
  {
    SpriteRenderer spriteRenderer = this.AddSpriteBase(sprite);
    this.RegisterInvariantSizeSprite(spriteRenderer.transform);
    if (isBillboard)
      this.RegisterBillboard(spriteRenderer.transform);
    return spriteRenderer;
  }

  public SpriteRenderer AddSprite(UnityEngine.Sprite sprite, float width, bool isBillboard)
  {
    SpriteRenderer spriteRenderer = this.AddSpriteBase(sprite);
    spriteRenderer.transform.localScale = width * Vector3.one * (sprite.pixelsPerUnit / (float) sprite.texture.width);
    if (isBillboard)
      this.RegisterBillboard(spriteRenderer.transform);
    return spriteRenderer;
  }

  private void RemoveSpriteChild(Transform child)
  {
    this.UnregisterBillboard(child);
    this.UnregisterInvariantSizeSprite(child);
    UnityEngine.Object.Destroy((UnityEngine.Object) child.gameObject);
  }

  private void RegisterInvariantSizeSprite(Transform billboard)
  {
    this.invariantSizeChildren = new List<Transform>((IEnumerable<Transform>) this.invariantSizeChildren)
    {
      billboard.transform
    }.ToArray();
  }

  private void UnregisterInvariantSizeSprite(Transform invariantSizeSprite)
  {
    List<Transform> transformList = new List<Transform>((IEnumerable<Transform>) this.invariantSizeChildren);
    transformList.Remove(invariantSizeSprite);
    this.invariantSizeChildren = transformList.ToArray();
  }

  private void RegisterBillboard(Transform billboard)
  {
    this.billboardChildren = new List<Transform>((IEnumerable<Transform>) this.billboardChildren)
    {
      billboard.transform
    }.ToArray();
  }

  private void UnregisterBillboard(Transform billboard)
  {
    List<Transform> transformList = new List<Transform>((IEnumerable<Transform>) this.billboardChildren);
    transformList.Remove(billboard);
    this.billboardChildren = transformList.ToArray();
  }

  private void StartDelayedDeactivation(float delay)
  {
    this.deactivationCoroutine = this.DelayedDeactivationRoutine(delay);
    this.StartCoroutine(this.deactivationCoroutine);
  }

  private void StopDelayedDeactivation()
  {
    if (!this.gameObject.activeInHierarchy || this.deactivationCoroutine == null)
      return;
    this.StopCoroutine(this.deactivationCoroutine);
  }

  [DebuggerHidden]
  private IEnumerator DelayedDeactivationRoutine(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SystemMap3DMapIcon.\u003CDelayedDeactivationRoutine\u003Ec__Iterator46() { delay = delay, \u003C\u0024\u003Edelay = delay, \u003C\u003Ef__this = this };
  }

  public void Show(bool show)
  {
    int num1 = show ? 1 : 0;
    bool? nullable = this._isShown;
    int num2 = nullable.GetValueOrDefault() ? 1 : 0;
    if ((num1 != num2 ? 0 : (nullable.HasValue ? 1 : 0)) != 0)
      return;
    this.StopDelayedDeactivation();
    float num3 = 1f;
    if (show)
    {
      this._isShown = new bool?(true);
      this.gameObject.SetActive(true);
      num3 = 1f;
    }
    else if (this.gameObject.activeInHierarchy)
    {
      this._isShown = new bool?(false);
      this.StartDelayedDeactivation(1f);
      num3 = -4f;
    }
    foreach (Animation componentsInChild in this.GetComponentsInChildren<Animation>())
    {
      componentsInChild.enabled = true;
      AnimationState animationState = componentsInChild.GetComponent<Animation>()[this.fadeClipName];
      if ((double) num3 < 1.0)
        animationState.normalizedTime = 1f;
      animationState.speed = num3;
      componentsInChild.Play(this.fadeClipName);
    }
  }

  private void AddFadeAnimComponents()
  {
    foreach (Component componentsInChild in this.GetComponentsInChildren<SpriteRenderer>())
      componentsInChild.gameObject.AddComponent<Animation>().AddClip((AnimationClip) Resources.Load(Ui.Prefabs + "/SystemMap3D/Anims/UiMapIconFadeIn"), this.fadeClipName);
  }

  private void RemoveFadeAnimComponents()
  {
    foreach (Component componentsInChild in this.GetComponentsInChildren<SpriteRenderer>(true))
      UnityEngine.Object.Destroy((UnityEngine.Object) componentsInChild.gameObject.GetComponent<Animation>());
  }

  private void AddWaypointIcon()
  {
    this._waypointSpriteRenderer = this.AddSizeInvariantSprite(this.iconLinker.WaypointIcon, true);
  }

  private void RemoveWaypointIcon()
  {
    if ((UnityEngine.Object) this._waypointSpriteRenderer == (UnityEngine.Object) null)
      return;
    this.RemoveSpriteChild(this._waypointSpriteRenderer.transform);
  }

  private void AddPartyMemberIcon()
  {
    this._partyMemberSpriteRenderer = this.AddSizeInvariantSprite(this.iconLinker.PartyMarker, true);
    this._partyMemberSpriteRenderer.color = Game.Me.Faction != Faction.Colonial ? TargetColorsLegacyBrackets.cylonPartyColor : TargetColorsLegacyBrackets.colonialPartyColor;
  }

  private void RemovePartyMemberIcon()
  {
    if ((UnityEngine.Object) this._partyMemberSpriteRenderer == (UnityEngine.Object) null)
      return;
    this.RemoveSpriteChild(this._partyMemberSpriteRenderer.transform);
  }

  private void SetBracketState(SystemMap3DMapIcon.BracketState bracketState)
  {
    this._bracketStates |= bracketState;
    this.UpdateBracket();
  }

  private void UnsetBracketState(SystemMap3DMapIcon.BracketState bracketState)
  {
    this._bracketStates &= ~bracketState;
    this.UpdateBracket();
  }

  private void UnsetAllBracketStates()
  {
    this._bracketStates = (SystemMap3DMapIcon.BracketState) 0;
    this.UpdateBracket();
  }

  private void UpdateBracket()
  {
    if (this._bracketStates == (SystemMap3DMapIcon.BracketState) 0)
    {
      if (!((UnityEngine.Object) this._bracketSpriteRenderer != (UnityEngine.Object) null))
        return;
      this.RemoveSpriteChild(this._bracketSpriteRenderer.transform);
    }
    else
    {
      if (!this.gameObject.activeInHierarchy)
        return;
      UnityEngine.Sprite sprite = this.iconLinker.MapObjectsNPCs[9];
      if (this.HasBracketState(SystemMap3DMapIcon.BracketState.Selected))
        sprite = this.iconLinker.BracketSpriteSelected;
      else if (this.HasBracketState(SystemMap3DMapIcon.BracketState.Hovered))
        sprite = this.iconLinker.BracketSpriteHover;
      if ((UnityEngine.Object) this._bracketSpriteRenderer == (UnityEngine.Object) null)
      {
        this._bracketSpriteRenderer = this.AddSizeInvariantSprite(sprite, true);
        Animation component = this._bracketSpriteRenderer.GetComponent<Animation>();
        if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
          return;
        component.enabled = false;
      }
      else
        this._bracketSpriteRenderer.sprite = sprite;
    }
  }

  private bool HasBracketState(SystemMap3DMapIcon.BracketState bracketState)
  {
    return (this._bracketStates & bracketState) == bracketState;
  }

  private void CreateCollider()
  {
    this.clickCollider = this.gameObject.AddComponent<SphereCollider>();
    this.clickCollider.radius = 0.13f;
  }

  private void DestroyCollider()
  {
    if (!((UnityEngine.Object) this.clickCollider != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.clickCollider);
  }

  private void OnEnable()
  {
    this.AddFadeAnimComponents();
    this.CreateCollider();
    this.UpdateBracket();
  }

  private void OnDisable()
  {
    this.RemoveFadeAnimComponents();
    this.DestroyCollider();
    this.UnsetBracketState(SystemMap3DMapIcon.BracketState.Hovered);
  }

  private void OnMouseEnter()
  {
    this.SetBracketState(SystemMap3DMapIcon.BracketState.Hovered);
    this.OnMouseEntered();
  }

  private void OnMouseExit()
  {
    this.UnsetBracketState(SystemMap3DMapIcon.BracketState.Hovered);
    this.OnMouseLeft();
  }

  private void OnMouseDown()
  {
    this.OnMouseClicked();
    if ((double) Time.time - (double) this.lastClickTime < 0.5)
      this.OnMouseDoubleClicked();
    this.lastClickTime = Time.time;
  }

  private void OnMouseOver()
  {
    if (!Input.GetMouseButtonDown(1))
      return;
    SpaceLevel.GetLevel().ShipControls.Move(this.gameObject.transform.position - Game.Me.Ship.Position);
  }

  private void UpdateBillboards(Quaternion rotation)
  {
    for (int index = 0; index < this.billboardChildren.Length; ++index)
      this.billboardChildren[index].rotation = rotation;
  }

  private void UpdateInvariantSizes()
  {
    float invariantSizeScale = this.GetInvariantSizeScale();
    for (int index = 0; index < this.invariantSizeChildren.Length; ++index)
    {
      Transform transform = this.invariantSizeChildren[index];
      if ((double) Math.Abs(transform.localScale.x - invariantSizeScale) > 9.99999974737875E-05)
        transform.localScale = invariantSizeScale * Vector3.one;
    }
    if (!((UnityEngine.Object) this.clickCollider != (UnityEngine.Object) null))
      return;
    float num = invariantSizeScale * 0.05f * this.ManualColliderScaling;
    if ((double) Mathf.Abs((float) (1.0 - (double) this.clickCollider.radius / (double) num)) <= 0.0500000007450581)
      return;
    this.clickCollider.radius = num;
  }

  private float GetInvariantSizeScale()
  {
    if ((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null || SpaceLevel.GetLevel().cameraSwitcher == null)
      return 0.0f;
    Camera activeCamera = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera();
    if ((double) Math.Abs(this.Scale - this.cachedScale) > 1.0000000116861E-07)
      this.UpdateVariables();
    if ((UnityEngine.Object) this.cachedCamera != (UnityEngine.Object) activeCamera)
      this.UpdateVariables();
    return this.cachedSizeFactor * Vector3.Distance(this.cachedCameraTransform.position, this.myTransform.position) * this.cachedParentScaleInv;
  }

  private void UpdateVariables()
  {
    this.cachedScale = this.Scale;
    this.cachedCamera = SpaceLevel.GetLevel().cameraSwitcher.GetActiveCamera();
    this.cachedCameraTransform = this.cachedCamera.transform;
    this.cachedFov = (float) (3.14159274101257 / (180.0 / (double) this.cachedCamera.fieldOfView));
    this.cachedSizeFactor = (float) (2.0 * (double) Mathf.Tan(this.cachedFov / 2f) * (768.0 / (double) Screen.height)) * this.Scale;
    this.cachedParentScale = this.transform.lossyScale.x;
    this.cachedParentScaleInv = 1f / this.cachedParentScale;
  }

  [Flags]
  private enum BracketState
  {
    Selected = 1,
    Hovered = 2,
  }
}
