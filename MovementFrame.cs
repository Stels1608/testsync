﻿// Decompiled with JetBrains decompiler
// Type: MovementFrame
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public struct MovementFrame : IProtocolRead
{
  private static MovementFrame invalid = MovementFrame.InvalidFrame();
  public Vector3 position;
  public Euler3 euler3;
  public Vector3 linearSpeed;
  public Vector3 strafeSpeed;
  public Euler3 Euler3Speed;
  public List<ThrusterEffect> ActiveThrusterEffects;
  public int mode;
  public bool valid;

  public Quaternion rotation
  {
    get
    {
      return this.euler3.rotation;
    }
    set
    {
      this.euler3 = Euler3.Rotation(value);
    }
  }

  public Vector3 lookDirection
  {
    get
    {
      return this.euler3.direction;
    }
  }

  public Vector3 nextPosition
  {
    get
    {
      return this.position + (this.linearSpeed + this.strafeSpeed) * 0.1f;
    }
  }

  public Euler3 NextEuler3
  {
    get
    {
      Euler3 euler3;
      if (this.mode == 0)
        euler3 = (this.euler3 + this.Euler3Speed * 0.1f).Normalized(true);
      else if (this.mode == 1)
        euler3 = (this.euler3 + this.Euler3Speed * 0.1f).Normalized(false);
      else if (this.mode == 2)
        euler3 = Euler3.RotateOverTime(this.euler3, this.Euler3Speed, 0.1f);
      else if (this.mode == 3)
      {
        euler3 = Euler3.RotateOverTimeLocal(this.euler3, this.Euler3Speed, 0.1f);
      }
      else
      {
        Debug.LogError((object) ("MovementFrame.nextEuler: unknown mode " + (object) this.mode));
        euler3 = Euler3.zero;
      }
      return euler3;
    }
  }

  public static MovementFrame Invalid
  {
    get
    {
      return MovementFrame.invalid;
    }
  }

  public MovementFrame(Vector3 position, Euler3 euler3, Vector3 linearSpeed, Vector3 strafeSpeed, Euler3 euler3Speed)
  {
    this.position = position;
    this.euler3 = euler3;
    this.linearSpeed = linearSpeed;
    this.strafeSpeed = strafeSpeed;
    this.Euler3Speed = euler3Speed;
    this.mode = 0;
    this.valid = true;
    this.ActiveThrusterEffects = new List<ThrusterEffect>();
  }

  public Vector3 GetFuturePosition(float t)
  {
    return this.position + (this.linearSpeed + this.strafeSpeed) * t;
  }

  public Quaternion GetFutureRotation(float t)
  {
    if (this.mode == 2)
      return Euler3.RotateOverTime(this.euler3, this.Euler3Speed, t).rotation;
    if (this.mode == 3)
      return Euler3.RotateOverTimeLocal(this.euler3, this.Euler3Speed, t).rotation;
    return (this.euler3 + this.Euler3Speed * t).rotation;
  }

  public override string ToString()
  {
    if (!this.valid)
      return "MovemantFrame.Invalid";
    return string.Format("MovementFrame: position = {0}, euler = {1}, linearSpeed = {2}, eulerSpeed = {3}", (object) this.position, (object) this.euler3, (object) this.linearSpeed, (object) this.Euler3Speed);
  }

  private static MovementFrame InvalidFrame()
  {
    return new MovementFrame() { position = Vector3.zero, linearSpeed = Vector3.zero, Euler3Speed = Euler3.zero, euler3 = Euler3.identity, mode = 0, valid = false };
  }

  public void Read(BgoProtocolReader pr)
  {
    this.position = pr.ReadVector3();
    this.euler3 = pr.ReadEuler();
    this.linearSpeed = pr.ReadVector3();
    this.strafeSpeed = pr.ReadVector3();
    this.Euler3Speed = pr.ReadEuler();
    this.mode = (int) pr.ReadByte();
    this.valid = true;
  }
}
