﻿// Decompiled with JetBrains decompiler
// Type: UIPopupList
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("NGUI/Interaction/Popup List")]
public class UIPopupList : UIWidgetContainer
{
  public int fontSize = 16;
  public NGUIText.Alignment alignment = NGUIText.Alignment.Left;
  public List<string> items = new List<string>();
  public List<object> itemData = new List<object>();
  public Vector2 padding = (Vector2) new Vector3(4f, 4f);
  public Color textColor = Color.white;
  public Color backgroundColor = Color.white;
  public Color highlightColor = new Color(0.8823529f, 0.7843137f, 0.5882353f, 1f);
  public bool isAnimated = true;
  public List<EventDelegate> onChange = new List<EventDelegate>();
  private List<UILabel> mLabelList = new List<UILabel>();
  [SerializeField]
  [HideInInspector]
  private string functionName = "OnSelectionChange";
  private const float animSpeed = 0.15f;
  public static UIPopupList current;
  public UIAtlas atlas;
  public UIFont bitmapFont;
  public Font trueTypeFont;
  public FontStyle fontStyle;
  public string backgroundSprite;
  public string highlightSprite;
  public UIPopupList.Position position;
  public bool isLocalized;
  public UIPopupList.OpenOn openOn;
  [SerializeField]
  [HideInInspector]
  private string mSelectedItem;
  private UIPanel mPanel;
  private GameObject mChild;
  private UISprite mBackground;
  private UISprite mHighlight;
  private UILabel mHighlightedLabel;
  private float mBgBorder;
  [SerializeField]
  [HideInInspector]
  private GameObject eventReceiver;
  [HideInInspector]
  [SerializeField]
  private float textScale;
  [HideInInspector]
  [SerializeField]
  private UIFont font;
  [SerializeField]
  [HideInInspector]
  public UILabel textLabel;
  private UIPopupList.LegacyEvent mLegacyEvent;
  private bool mUseDynamicFont;
  private bool mTweening;

  public UnityEngine.Object ambigiousFont
  {
    get
    {
      if ((UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null)
        return (UnityEngine.Object) this.trueTypeFont;
      if ((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null)
        return (UnityEngine.Object) this.bitmapFont;
      return (UnityEngine.Object) this.font;
    }
    set
    {
      if (value is Font)
      {
        this.trueTypeFont = value as Font;
        this.bitmapFont = (UIFont) null;
        this.font = (UIFont) null;
      }
      else
      {
        if (!(value is UIFont))
          return;
        this.bitmapFont = value as UIFont;
        this.trueTypeFont = (Font) null;
        this.font = (UIFont) null;
      }
    }
  }

  [Obsolete("Use EventDelegate.Add(popup.onChange, YourCallback) instead, and UIPopupList.current.value to determine the state")]
  public UIPopupList.LegacyEvent onSelectionChange
  {
    get
    {
      return this.mLegacyEvent;
    }
    set
    {
      this.mLegacyEvent = value;
    }
  }

  public bool isOpen
  {
    get
    {
      return (UnityEngine.Object) this.mChild != (UnityEngine.Object) null;
    }
  }

  public string value
  {
    get
    {
      return this.mSelectedItem;
    }
    set
    {
      this.mSelectedItem = value;
      if (this.mSelectedItem == null || this.mSelectedItem == null)
        return;
      this.TriggerCallbacks();
    }
  }

  public object data
  {
    get
    {
      int index = this.items.IndexOf(this.mSelectedItem);
      if (index < this.itemData.Count)
        return this.itemData[index];
      return (object) null;
    }
  }

  [Obsolete("Use 'value' instead")]
  public string selection
  {
    get
    {
      return this.value;
    }
    set
    {
      this.value = value;
    }
  }

  private bool handleEvents
  {
    get
    {
      UIKeyNavigation component = this.GetComponent<UIKeyNavigation>();
      if (!((UnityEngine.Object) component == (UnityEngine.Object) null))
        return !component.enabled;
      return true;
    }
    set
    {
      UIKeyNavigation component = this.GetComponent<UIKeyNavigation>();
      if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
        return;
      component.enabled = !value;
    }
  }

  private bool isValid
  {
    get
    {
      if (!((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null))
        return (UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null;
      return true;
    }
  }

  private int activeFontSize
  {
    get
    {
      if ((UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null || (UnityEngine.Object) this.bitmapFont == (UnityEngine.Object) null)
        return this.fontSize;
      return this.bitmapFont.defaultSize;
    }
  }

  private float activeFontScale
  {
    get
    {
      if ((UnityEngine.Object) this.trueTypeFont != (UnityEngine.Object) null || (UnityEngine.Object) this.bitmapFont == (UnityEngine.Object) null)
        return 1f;
      return (float) this.fontSize / (float) this.bitmapFont.defaultSize;
    }
  }

  public void Clear()
  {
    this.items.Clear();
    this.itemData.Clear();
  }

  public void AddItem(string text)
  {
    this.items.Add(text);
    this.itemData.Add((object) null);
  }

  public void AddItem(string text, object data)
  {
    this.items.Add(text);
    this.itemData.Add(data);
  }

  protected void TriggerCallbacks()
  {
    if (!((UnityEngine.Object) UIPopupList.current != (UnityEngine.Object) this))
      return;
    UIPopupList uiPopupList = UIPopupList.current;
    UIPopupList.current = this;
    if (this.mLegacyEvent != null)
      this.mLegacyEvent(this.mSelectedItem);
    if (EventDelegate.IsValid(this.onChange))
      EventDelegate.Execute(this.onChange);
    else if ((UnityEngine.Object) this.eventReceiver != (UnityEngine.Object) null && !string.IsNullOrEmpty(this.functionName))
      this.eventReceiver.SendMessage(this.functionName, (object) this.mSelectedItem, SendMessageOptions.DontRequireReceiver);
    UIPopupList.current = uiPopupList;
  }

  private void OnEnable()
  {
    if (EventDelegate.IsValid(this.onChange))
    {
      this.eventReceiver = (GameObject) null;
      this.functionName = (string) null;
    }
    if ((UnityEngine.Object) this.font != (UnityEngine.Object) null)
    {
      if (this.font.isDynamic)
      {
        this.trueTypeFont = this.font.dynamicFont;
        this.fontStyle = this.font.dynamicFontStyle;
        this.mUseDynamicFont = true;
      }
      else if ((UnityEngine.Object) this.bitmapFont == (UnityEngine.Object) null)
      {
        this.bitmapFont = this.font;
        this.mUseDynamicFont = false;
      }
      this.font = (UIFont) null;
    }
    if ((double) this.textScale != 0.0)
    {
      this.fontSize = !((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null) ? 16 : Mathf.RoundToInt((float) this.bitmapFont.defaultSize * this.textScale);
      this.textScale = 0.0f;
    }
    if (!((UnityEngine.Object) this.trueTypeFont == (UnityEngine.Object) null) || !((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null) || !this.bitmapFont.isDynamic)
      return;
    this.trueTypeFont = this.bitmapFont.dynamicFont;
    this.bitmapFont = (UIFont) null;
  }

  private void OnValidate()
  {
    Font font = this.trueTypeFont;
    UIFont uiFont = this.bitmapFont;
    this.bitmapFont = (UIFont) null;
    this.trueTypeFont = (Font) null;
    if ((UnityEngine.Object) font != (UnityEngine.Object) null && ((UnityEngine.Object) uiFont == (UnityEngine.Object) null || !this.mUseDynamicFont))
    {
      this.bitmapFont = (UIFont) null;
      this.trueTypeFont = font;
      this.mUseDynamicFont = true;
    }
    else if ((UnityEngine.Object) uiFont != (UnityEngine.Object) null)
    {
      if (uiFont.isDynamic)
      {
        this.trueTypeFont = uiFont.dynamicFont;
        this.fontStyle = uiFont.dynamicFontStyle;
        this.fontSize = uiFont.defaultSize;
        this.mUseDynamicFont = true;
      }
      else
      {
        this.bitmapFont = uiFont;
        this.mUseDynamicFont = false;
      }
    }
    else
    {
      this.trueTypeFont = font;
      this.mUseDynamicFont = true;
    }
  }

  private void Start()
  {
    if ((UnityEngine.Object) this.textLabel != (UnityEngine.Object) null)
    {
      EventDelegate.Add(this.onChange, new EventDelegate.Callback(this.textLabel.SetCurrentSelection));
      this.textLabel = (UILabel) null;
    }
    if (!Application.isPlaying)
      return;
    if (string.IsNullOrEmpty(this.mSelectedItem))
    {
      if (this.items.Count <= 0)
        return;
      this.value = this.items[0];
    }
    else
    {
      string str = this.mSelectedItem;
      this.mSelectedItem = (string) null;
      this.value = str;
    }
  }

  private void OnLocalize()
  {
    if (!this.isLocalized)
      return;
    this.TriggerCallbacks();
  }

  private void Highlight(UILabel lbl, bool instant)
  {
    if (!((UnityEngine.Object) this.mHighlight != (UnityEngine.Object) null))
      return;
    this.mHighlightedLabel = lbl;
    if (this.mHighlight.GetAtlasSprite() == null)
      return;
    Vector3 highlightPosition = this.GetHighlightPosition();
    if (instant || !this.isAnimated)
    {
      this.mHighlight.cachedTransform.localPosition = highlightPosition;
    }
    else
    {
      TweenPosition.Begin(this.mHighlight.gameObject, 0.1f, highlightPosition).method = UITweener.Method.EaseOut;
      if (this.mTweening)
        return;
      this.mTweening = true;
      this.StartCoroutine(this.UpdateTweenPosition());
    }
  }

  private Vector3 GetHighlightPosition()
  {
    if ((UnityEngine.Object) this.mHighlightedLabel == (UnityEngine.Object) null || (UnityEngine.Object) this.mHighlight == (UnityEngine.Object) null)
      return Vector3.zero;
    UISpriteData atlasSprite = this.mHighlight.GetAtlasSprite();
    if (atlasSprite == null)
      return Vector3.zero;
    float pixelSize = this.atlas.pixelSize;
    return this.mHighlightedLabel.cachedTransform.localPosition + new Vector3(-((float) atlasSprite.borderLeft * pixelSize), (float) atlasSprite.borderTop * pixelSize, 1f);
  }

  [DebuggerHidden]
  private IEnumerator UpdateTweenPosition()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new UIPopupList.\u003CUpdateTweenPosition\u003Ec__Iterator1() { \u003C\u003Ef__this = this };
  }

  private void OnItemHover(GameObject go, bool isOver)
  {
    if (!isOver)
      return;
    this.Highlight(go.GetComponent<UILabel>(), false);
  }

  private void Select(UILabel lbl, bool instant)
  {
    this.Highlight(lbl, instant);
    this.value = lbl.gameObject.GetComponent<UIEventListener>().parameter as string;
    UIPlaySound[] components = this.GetComponents<UIPlaySound>();
    int index = 0;
    for (int length = components.Length; index < length; ++index)
    {
      UIPlaySound uiPlaySound = components[index];
      if (uiPlaySound.trigger == UIPlaySound.Trigger.OnClick)
        NGUITools.PlaySound(uiPlaySound.audioClip, uiPlaySound.volume, 1f);
    }
  }

  private void OnItemPress(GameObject go, bool isPressed)
  {
    if (!isPressed)
      return;
    this.Select(go.GetComponent<UILabel>(), true);
  }

  private void OnItemClick(GameObject go)
  {
    this.Close();
  }

  private void OnKey(KeyCode key)
  {
    if (!this.enabled || !NGUITools.GetActive(this.gameObject) || !this.handleEvents)
      return;
    int num1 = this.mLabelList.IndexOf(this.mHighlightedLabel);
    if (num1 == -1)
      num1 = 0;
    int num2;
    if (key == KeyCode.UpArrow)
    {
      if (num1 <= 0)
        return;
      this.Select(this.mLabelList[num2 = num1 - 1], false);
    }
    else if (key == KeyCode.DownArrow)
    {
      if (num1 + 1 >= this.mLabelList.Count)
        return;
      this.Select(this.mLabelList[num2 = num1 + 1], false);
    }
    else
    {
      if (key != KeyCode.Escape)
        return;
      this.OnSelect(false);
    }
  }

  private void OnDisable()
  {
    this.Close();
  }

  private void OnSelect(bool isSelected)
  {
    if (isSelected)
      return;
    this.Close();
  }

  public void Close()
  {
    if (!((UnityEngine.Object) this.mChild != (UnityEngine.Object) null))
      return;
    this.mLabelList.Clear();
    this.handleEvents = false;
    if (this.isAnimated)
    {
      UIWidget[] componentsInChildren1 = this.mChild.GetComponentsInChildren<UIWidget>();
      int index1 = 0;
      for (int length = componentsInChildren1.Length; index1 < length; ++index1)
      {
        UIWidget uiWidget = componentsInChildren1[index1];
        Color color = uiWidget.color;
        color.a = 0.0f;
        TweenColor.Begin(uiWidget.gameObject, 0.15f, color).method = UITweener.Method.EaseOut;
      }
      Collider[] componentsInChildren2 = this.mChild.GetComponentsInChildren<Collider>();
      int index2 = 0;
      for (int length = componentsInChildren2.Length; index2 < length; ++index2)
        componentsInChildren2[index2].enabled = false;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.mChild, 0.15f);
    }
    else
      UnityEngine.Object.Destroy((UnityEngine.Object) this.mChild);
    this.mBackground = (UISprite) null;
    this.mHighlight = (UISprite) null;
    this.mChild = (GameObject) null;
  }

  private void AnimateColor(UIWidget widget)
  {
    Color color = widget.color;
    widget.color = new Color(color.r, color.g, color.b, 0.0f);
    TweenColor.Begin(widget.gameObject, 0.15f, color).method = UITweener.Method.EaseOut;
  }

  private void AnimatePosition(UIWidget widget, bool placeAbove, float bottom)
  {
    Vector3 localPosition = widget.cachedTransform.localPosition;
    Vector3 vector3 = !placeAbove ? new Vector3(localPosition.x, 0.0f, localPosition.z) : new Vector3(localPosition.x, bottom, localPosition.z);
    widget.cachedTransform.localPosition = vector3;
    TweenPosition.Begin(widget.gameObject, 0.15f, localPosition).method = UITweener.Method.EaseOut;
  }

  private void AnimateScale(UIWidget widget, bool placeAbove, float bottom)
  {
    GameObject gameObject = widget.gameObject;
    Transform cachedTransform = widget.cachedTransform;
    float num = (float) ((double) this.activeFontSize * (double) this.activeFontScale + (double) this.mBgBorder * 2.0);
    cachedTransform.localScale = new Vector3(1f, num / (float) widget.height, 1f);
    TweenScale.Begin(gameObject, 0.15f, Vector3.one).method = UITweener.Method.EaseOut;
    if (!placeAbove)
      return;
    Vector3 localPosition = cachedTransform.localPosition;
    cachedTransform.localPosition = new Vector3(localPosition.x, localPosition.y - (float) widget.height + num, localPosition.z);
    TweenPosition.Begin(gameObject, 0.15f, localPosition).method = UITweener.Method.EaseOut;
  }

  private void Animate(UIWidget widget, bool placeAbove, float bottom)
  {
    this.AnimateColor(widget);
    this.AnimatePosition(widget, placeAbove, bottom);
  }

  private void OnClick()
  {
    if (this.openOn == UIPopupList.OpenOn.DoubleClick || this.openOn == UIPopupList.OpenOn.Manual || this.openOn == UIPopupList.OpenOn.RightClick && UICamera.currentTouchID != -2)
      return;
    this.Show();
  }

  private void OnDoubleClick()
  {
    if (this.openOn != UIPopupList.OpenOn.DoubleClick)
      return;
    this.Show();
  }

  public void Show()
  {
    if (this.enabled && NGUITools.GetActive(this.gameObject) && ((UnityEngine.Object) this.mChild == (UnityEngine.Object) null && (UnityEngine.Object) this.atlas != (UnityEngine.Object) null) && (this.isValid && this.items.Count > 0))
    {
      this.mLabelList.Clear();
      if ((UnityEngine.Object) this.mPanel == (UnityEngine.Object) null)
      {
        this.mPanel = UIPanel.Find(this.transform);
        if ((UnityEngine.Object) this.mPanel == (UnityEngine.Object) null)
          return;
      }
      this.handleEvents = true;
      Transform transform1 = this.transform;
      Bounds relativeWidgetBounds = NGUIMath.CalculateRelativeWidgetBounds(transform1.parent, transform1);
      this.mChild = new GameObject("Drop-down List");
      this.mChild.layer = this.gameObject.layer;
      Transform transform2 = this.mChild.transform;
      transform2.parent = transform1.parent;
      transform2.localPosition = relativeWidgetBounds.min;
      transform2.localRotation = Quaternion.identity;
      transform2.localScale = Vector3.one;
      this.mBackground = NGUITools.AddSprite(this.mChild, this.atlas, this.backgroundSprite);
      this.mBackground.pivot = UIWidget.Pivot.TopLeft;
      this.mBackground.depth = NGUITools.CalculateNextDepth(this.mPanel.gameObject);
      this.mBackground.color = this.backgroundColor;
      Vector4 border = this.mBackground.border;
      this.mBgBorder = border.y;
      this.mBackground.cachedTransform.localPosition = new Vector3(0.0f, border.y, 0.0f);
      this.mHighlight = NGUITools.AddSprite(this.mChild, this.atlas, this.highlightSprite);
      this.mHighlight.pivot = UIWidget.Pivot.TopLeft;
      this.mHighlight.color = this.highlightColor;
      UISpriteData atlasSprite = this.mHighlight.GetAtlasSprite();
      if (atlasSprite == null)
        return;
      float num1 = (float) atlasSprite.borderTop;
      float num2 = (float) this.activeFontSize;
      float activeFontScale = this.activeFontScale;
      float num3 = num2 * activeFontScale;
      float a = 0.0f;
      float y = -this.padding.y;
      int num4 = !((UnityEngine.Object) this.bitmapFont != (UnityEngine.Object) null) ? this.fontSize : this.bitmapFont.defaultSize;
      List<UILabel> uiLabelList = new List<UILabel>();
      if (!this.items.Contains(this.mSelectedItem))
        this.mSelectedItem = (string) null;
      int index1 = 0;
      for (int count = this.items.Count; index1 < count; ++index1)
      {
        string key = this.items[index1];
        UILabel lbl = NGUITools.AddWidget<UILabel>(this.mChild);
        lbl.name = index1.ToString();
        lbl.pivot = UIWidget.Pivot.TopLeft;
        lbl.bitmapFont = this.bitmapFont;
        lbl.trueTypeFont = this.trueTypeFont;
        lbl.fontSize = num4;
        lbl.fontStyle = this.fontStyle;
        lbl.text = !this.isLocalized ? key : Localization.Get(key);
        lbl.color = this.textColor;
        lbl.cachedTransform.localPosition = new Vector3(border.x + this.padding.x, y, -1f);
        lbl.overflowMethod = UILabel.Overflow.ResizeFreely;
        lbl.alignment = this.alignment;
        lbl.MakePixelPerfect();
        if ((double) activeFontScale != 1.0)
          lbl.cachedTransform.localScale = Vector3.one * activeFontScale;
        uiLabelList.Add(lbl);
        y = y - num3 - this.padding.y;
        a = Mathf.Max(a, lbl.printedSize.x);
        UIEventListener uiEventListener = UIEventListener.Get(lbl.gameObject);
        uiEventListener.onHover = new UIEventListener.BoolDelegate(this.OnItemHover);
        uiEventListener.onPress = new UIEventListener.BoolDelegate(this.OnItemPress);
        uiEventListener.onClick = new UIEventListener.VoidDelegate(this.OnItemClick);
        uiEventListener.parameter = (object) key;
        if (this.mSelectedItem == key || index1 == 0 && string.IsNullOrEmpty(this.mSelectedItem))
          this.Highlight(lbl, true);
        this.mLabelList.Add(lbl);
      }
      float f1 = Mathf.Max(a, (float) ((double) relativeWidgetBounds.size.x * (double) activeFontScale - ((double) border.x + (double) this.padding.x) * 2.0));
      float x = f1 / activeFontScale;
      Vector3 vector3_1 = new Vector3(x * 0.5f, (float) (-(double) num2 * 0.5), 0.0f);
      Vector3 vector3_2 = new Vector3(x, (num3 + this.padding.y) / activeFontScale, 1f);
      int index2 = 0;
      for (int count = uiLabelList.Count; index2 < count; ++index2)
      {
        UILabel uiLabel = uiLabelList[index2];
        NGUITools.AddWidgetCollider(uiLabel.gameObject);
        uiLabel.autoResizeBoxCollider = false;
        BoxCollider component1 = uiLabel.GetComponent<BoxCollider>();
        if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
        {
          vector3_1.z = component1.center.z;
          component1.center = vector3_1;
          component1.size = vector3_2;
        }
        else
        {
          BoxCollider2D component2 = uiLabel.GetComponent<BoxCollider2D>();
          component2.offset = (Vector2) vector3_1;
          component2.size = (Vector2) vector3_2;
        }
      }
      int @int = Mathf.RoundToInt(f1);
      float f2 = f1 + (float) (((double) border.x + (double) this.padding.x) * 2.0);
      float num5 = y - border.y;
      this.mBackground.width = Mathf.RoundToInt(f2);
      this.mBackground.height = Mathf.RoundToInt(-num5 + border.y);
      int index3 = 0;
      for (int count = uiLabelList.Count; index3 < count; ++index3)
      {
        UILabel uiLabel = uiLabelList[index3];
        uiLabel.overflowMethod = UILabel.Overflow.ShrinkContent;
        uiLabel.width = @int;
      }
      float num6 = 2f * this.atlas.pixelSize;
      float f3 = (float) ((double) f2 - ((double) border.x + (double) this.padding.x) * 2.0 + (double) atlasSprite.borderLeft * (double) num6);
      float f4 = num3 + num1 * num6;
      this.mHighlight.width = Mathf.RoundToInt(f3);
      this.mHighlight.height = Mathf.RoundToInt(f4);
      bool placeAbove = this.position == UIPopupList.Position.Above;
      if (this.position == UIPopupList.Position.Auto)
      {
        UICamera cameraForLayer = UICamera.FindCameraForLayer(this.gameObject.layer);
        if ((UnityEngine.Object) cameraForLayer != (UnityEngine.Object) null)
          placeAbove = (double) cameraForLayer.cachedCamera.WorldToViewportPoint(transform1.position).y < 0.5;
      }
      if (this.isAnimated)
      {
        float bottom = num5 + num3;
        this.Animate((UIWidget) this.mHighlight, placeAbove, bottom);
        int index4 = 0;
        for (int count = uiLabelList.Count; index4 < count; ++index4)
          this.Animate((UIWidget) uiLabelList[index4], placeAbove, bottom);
        this.AnimateColor((UIWidget) this.mBackground);
        this.AnimateScale((UIWidget) this.mBackground, placeAbove, bottom);
      }
      if (!placeAbove)
        return;
      transform2.localPosition = new Vector3(relativeWidgetBounds.min.x, relativeWidgetBounds.max.y - num5 - border.y, relativeWidgetBounds.min.z);
    }
    else
      this.OnSelect(false);
  }

  public enum Position
  {
    Auto,
    Above,
    Below,
  }

  public enum OpenOn
  {
    ClickOrTap,
    RightClick,
    DoubleClick,
    Manual,
  }

  public delegate void LegacyEvent(string val);
}
