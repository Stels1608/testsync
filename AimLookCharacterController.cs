﻿// Decompiled with JetBrains decompiler
// Type: AimLookCharacterController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AimLookCharacterController : MonoBehaviour
{
  private CharacterMotor motor;

  private void Start()
  {
    this.motor = this.GetComponent(typeof (CharacterMotor)) as CharacterMotor;
    if (!((Object) this.motor == (Object) null))
      return;
    Debug.Log((object) "Motor is null!!");
  }

  private void Update()
  {
    Vector3 vector3 = new Vector3(Input.GetAxis("Horizontal2"), Input.GetAxis("Vertical2"), 0.0f);
    if ((double) vector3.magnitude > 1.0)
      vector3 = vector3.normalized;
    vector3 = Camera.main.transform.rotation * vector3;
    vector3 = Quaternion.FromToRotation(Camera.main.transform.forward * -1f, this.transform.up) * vector3;
    this.motor.desiredFacingDirection = vector3;
  }
}
