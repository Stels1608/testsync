﻿// Decompiled with JetBrains decompiler
// Type: InputBindingDialogBoxContentJoystick
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class InputBindingDialogBoxContentJoystick : InputBindingDialogBoxContentBase
{
  private readonly List<JoystickButtonCode> xbox360AxisModifierWhitelist = new List<JoystickButtonCode>() { JoystickButtonCode.Axis9_Positive, JoystickButtonCode.Axis10_Positive };
  [SerializeField]
  private UILabel normalAxisLabel;
  [SerializeField]
  private UILabel invertedAxisLabel;
  [SerializeField]
  private RadioButtonGroupWidget radioButtonGroup;
  [SerializeField]
  private GameObject bindingLabelArea;
  [SerializeField]
  private InputBindingXboxIconArea bindingXboxIconArea;
  private List<JoystickButtonCode> pressedButtons;
  private JoystickButtonCode lastPressedButtons;

  protected override void Start()
  {
    base.Start();
    this.pressedButtons = new List<JoystickButtonCode>();
    this.radioButtonGroup.gameObject.SetActive(JoystickSetup.IsDualAxisAction(this.actionToBind));
    this.radioButtonGroup.OnValueChanged = new AnonymousDelegate(this.HandleRadioButtons);
  }

  public override void Update()
  {
    this.HandleInput();
  }

  private bool IsButtonPressed(JoystickButtonCode buttonCode)
  {
    float axisRaw = Input.GetAxisRaw(Game.InputManager.JoystickSetup.InputStringMapping[buttonCode]);
    if (JoystickSetup.GetJoystickInputType(buttonCode) == JoystickInputType.Axis && JoystickSetup.IsAxisNegative(buttonCode))
    {
      if ((double) axisRaw < -0.5)
        return true;
    }
    else if ((double) axisRaw > 0.5)
      return true;
    return false;
  }

  protected override void HandleInput()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    InputBindingDialogBoxContentJoystick.\u003CHandleInput\u003Ec__AnonStoreyCA inputCAnonStoreyCa = new InputBindingDialogBoxContentJoystick.\u003CHandleInput\u003Ec__AnonStoreyCA();
    List<JoystickButtonCode> joystickButtonCodeList = new List<JoystickButtonCode>();
    // ISSUE: reference to a compiler-generated field
    inputCAnonStoreyCa.buttonsReleasedNew = new List<JoystickButtonCode>();
    using (Dictionary<JoystickButtonCode, string>.Enumerator enumerator = Game.InputManager.JoystickSetup.InputStringMapping.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JoystickButtonCode key = enumerator.Current.Key;
        if (!JoystickSetup.ShowXbox360Buttons || key != JoystickButtonCode.Axis3_Negative && key != JoystickButtonCode.Axis3_Positive)
        {
          if (this.IsButtonPressed(key))
          {
            if (!this.pressedButtons.Contains(key))
              joystickButtonCodeList.Add(key);
          }
          else if (this.pressedButtons.Contains(key))
          {
            // ISSUE: reference to a compiler-generated field
            inputCAnonStoreyCa.buttonsReleasedNew.Add(key);
          }
        }
      }
    }
    // ISSUE: reference to a compiler-generated field
    inputCAnonStoreyCa.buttonsPressedNewCopy = new List<JoystickButtonCode>((IEnumerable<JoystickButtonCode>) joystickButtonCodeList);
    // ISSUE: reference to a compiler-generated method
    joystickButtonCodeList.RemoveAll(new Predicate<JoystickButtonCode>(inputCAnonStoreyCa.\u003C\u003Em__1D5));
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    inputCAnonStoreyCa.buttonsReleasedNew.RemoveAll(new Predicate<JoystickButtonCode>(inputCAnonStoreyCa.\u003C\u003Em__1D6));
    this.pressedButtons.AddRange((IEnumerable<JoystickButtonCode>) joystickButtonCodeList);
    // ISSUE: reference to a compiler-generated method
    this.pressedButtons.RemoveAll(new Predicate<JoystickButtonCode>(inputCAnonStoreyCa.\u003C\u003Em__1D7));
    // ISSUE: reference to a compiler-generated field
    if (inputCAnonStoreyCa.buttonsReleasedNew.Contains(this.lastPressedButtons))
      this.lastPressedButtons = JoystickButtonCode.None;
    if (joystickButtonCodeList.Contains(JoystickButtonCode.Axis3_Positive) && joystickButtonCodeList.Contains(JoystickButtonCode.Axis9_Positive))
      joystickButtonCodeList.Remove(JoystickButtonCode.Axis3_Positive);
    if (joystickButtonCodeList.Contains(JoystickButtonCode.Axis3_Negative) && joystickButtonCodeList.Contains(JoystickButtonCode.Axis10_Positive))
      joystickButtonCodeList.Remove(JoystickButtonCode.Axis3_Negative);
    using (List<JoystickButtonCode>.Enumerator enumerator = joystickButtonCodeList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JoystickButtonCode current = enumerator.Current;
        this.ApplyAxisWhitelist();
        ((JoystickBinding) this.pressedBinding).SetModifierCode(this.lastPressedButtons);
        ((JoystickBinding) this.pressedBinding).SetTriggerCode(current);
        this.lastPressedButtons = current;
      }
    }
    this.UpdateUi();
  }

  private void ApplyAxisWhitelist()
  {
    if (this.lastPressedButtons == JoystickButtonCode.None || JoystickSetup.GetJoystickInputType(this.lastPressedButtons) == JoystickInputType.Button || JoystickSetup.IsXbox360Detected && this.xbox360AxisModifierWhitelist.Contains(this.lastPressedButtons))
      return;
    this.lastPressedButtons = JoystickButtonCode.None;
  }

  private void HandleRadioButtons()
  {
    ((JoystickBinding) this.pressedBinding).IsAxisInverted = this.radioButtonGroup.GetIndexOfActiveButton() == 1;
    this.UpdateUi();
  }

  protected override InputBindingState DetermineInputBindingState()
  {
    return base.DetermineInputBindingState();
  }

  public override void Setup(IInputBinding currentBinding)
  {
    base.Setup(currentBinding);
    if (JoystickSetup.IsDualAxisAction(this.actionToBind))
    {
      ((JoystickBinding) currentBinding).IsDualAxis = true;
    }
    else
    {
      ((JoystickBinding) currentBinding).IsDualAxis = false;
      ((JoystickBinding) currentBinding).IsAxisInverted = false;
    }
    this.radioButtonGroup.SetActiveButton(!((JoystickBinding) currentBinding).IsAxisInverted ? 0 : 1);
  }

  protected override void UnmapAction()
  {
    base.UnmapAction();
  }

  protected override void UpdateUi()
  {
    base.UpdateUi();
    bool flag = JoystickSetup.ShowXbox360Buttons && ((JoystickBinding) this.pressedBinding).TriggerCode != JoystickButtonCode.None;
    this.bindingLabelArea.SetActive(!flag);
    this.bindingXboxIconArea.gameObject.SetActive(flag);
    this.bindingXboxIconArea.SetInputBinding(this.pressedBinding);
    InputBindingState inputBindingState = this.DetermineInputBindingState();
    WidgetColorScheme widgetColorScheme = ColorManager.currentColorScheme;
    this.keyCombination.text = this.pressedBinding.BindingAlias;
    switch (inputBindingState)
    {
      case InputBindingState.TRIGGER_ALREADY_MAPPED:
        this.informationLabel.text = Tools.ParseMessage(this.GetTextForTriggerAlreadyMappedState(), (object) this.pressedBinding.BindingAlias, (object) Tools.GetLocalized(this.settingsData.InputBinderCache.TryGetAction(this.pressedBinding.Device, this.pressedBinding.DeviceTriggerCode, this.pressedBinding.DeviceModifierCode)));
        this.informationLabel.color = widgetColorScheme.Get(WidgetColorType.TEXT_RED);
        break;
      case InputBindingState.MODIFIER_ALREADY_MAPPED_AS_SINGLE_TRIGGER:
        this.informationLabel.text = Tools.ParseMessage(this.GetTextForModifierAlreadyMappedAsTriggerState(), (object) this.pressedBinding.ModifierAlias, (object) Tools.GetLocalized(this.settingsData.InputBinderCache.TryGetAction(this.pressedBinding.Device, this.pressedBinding.DeviceModifierCode, (ushort) 0)));
        this.informationLabel.color = widgetColorScheme.Get(WidgetColorType.TEXT_RED);
        break;
      case InputBindingState.TRIGGER_ALREADY_MAPPED_AS_MODIFIER:
        this.informationLabel.text = Tools.ParseMessage(this.GetTextForTriggerAlreadyMappedAsModifierState(), (object) this.pressedBinding.TriggerAlias, (object) Tools.GetLocalized(this.settingsData.InputBinderCache.TryGetActionUsingModifier(this.pressedBinding.Device, this.pressedBinding.DeviceTriggerCode)));
        this.informationLabel.color = widgetColorScheme.Get(WidgetColorType.TEXT_RED);
        break;
    }
  }

  public override bool IsValidInput(IInputBinding inputBinding)
  {
    return true;
  }

  protected override string GetTextForValidInputState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.valid_joystick_input%";
  }

  protected override string GetTextForInitState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.init_joystick_input%";
  }

  protected override string GetTextForInvalidInputState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.invalid_joystick_input%";
  }

  private string GetTextForModifierAlreadyMappedAsTriggerState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.already_mapped_joystick_modifier_as_trigger%";
  }

  private string GetTextForTriggerAlreadyMappedAsModifierState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.already_mapped_joystick_trigger_as_modifier%";
  }

  protected override string GetTextForTriggerAlreadyMappedState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.already_mapped_joystick_trigger%";
  }

  protected override string GetTextForNotMappedState()
  {
    return "%$bgo.ui.options.popup.keybinding.state.unmapped_joystick_input%";
  }
}
