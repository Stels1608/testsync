﻿// Decompiled with JetBrains decompiler
// Type: RcsManeuverRecorder
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class RcsManeuverRecorder : ManeuverRecorder
{
  public int knotsCount = 6;
  public string maneuverName = string.Empty;

  protected override void OnFinished()
  {
    Log.DebugInfo((object) PrettyPrinter.Format(Quaternion.LookRotation(-Vector3.forward)));
    List<RcsManeuverRecorder.RotationFrame> knots = this.frames.ConvertAll<RcsManeuverRecorder.RotationFrame>((Converter<ManeuverRecorder.Frame, RcsManeuverRecorder.RotationFrame>) (frame => new RcsManeuverRecorder.RotationFrame(frame.index, frame.rotation, frame.time)));
    for (int index = 0; index < this.frameCount - 1; ++index)
      knots[index].CalcSpeed(knots[index + 1]);
    for (int index = 0; index < this.frameCount - 1; ++index)
      knots[index].CalcAcceleration(knots[index + 1]);
    for (int index = 1; index < this.frameCount; ++index)
      knots[index].Normalize(knots[index - 1]);
    float epsilon = 1f / 1000f;
    Curves.DistanceDelegate<RcsManeuverRecorder.RotationFrame> distance = (Curves.DistanceDelegate<RcsManeuverRecorder.RotationFrame>) ((knot, from, to) => Algorithm3D.DistanceToLineSection(knot.angle, from.angle, to.angle).magnitude);
    List<RcsManeuverRecorder.RotationFrame> rotationFrameList;
    for (rotationFrameList = Curves.DouglasPeucker<RcsManeuverRecorder.RotationFrame>(knots, distance, epsilon); rotationFrameList.Count > this.knotsCount; rotationFrameList = Curves.DouglasPeucker<RcsManeuverRecorder.RotationFrame>(knots, distance, epsilon))
      epsilon += 1f / 1000f;
    using (StreamWriter streamWriter = new StreamWriter(this.maneuverName + ".txt"))
    {
      using (List<RcsManeuverRecorder.RotationFrame>.Enumerator enumerator = rotationFrameList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          RcsManeuverRecorder.RotationFrame current = enumerator.Current;
          Vector3 vector3 = current.angle;
          streamWriter.WriteLine(PrettyPrinter.Format(current.time) + " " + PrettyPrinter.Format(vector3.x) + " " + PrettyPrinter.Format(vector3.y) + " " + PrettyPrinter.Format(vector3.z));
        }
      }
    }
    using (StreamWriter streamWriter = new StreamWriter("euler.txt"))
    {
      Quaternion q1 = Quaternion.Euler(180f, 0.0f, 0.0f);
      streamWriter.WriteLine(PrettyPrinter.Format(q1) + " " + PrettyPrinter.Format(q1.eulerAngles));
      Quaternion q2 = Quaternion.Euler(0.0f, 180f, 0.0f);
      streamWriter.WriteLine(PrettyPrinter.Format(q2) + " " + PrettyPrinter.Format(q2.eulerAngles));
      Quaternion quaternion = Quaternion.Euler(0.0f, 0.0f, 180f);
      streamWriter.WriteLine(PrettyPrinter.Format(quaternion) + " " + PrettyPrinter.Format(quaternion.eulerAngles));
      quaternion = Quaternion.Euler(90f, 0.0f, 0.0f);
      streamWriter.WriteLine(PrettyPrinter.Format(quaternion) + " " + PrettyPrinter.Format(quaternion.eulerAngles));
      quaternion = Quaternion.Euler(0.0f, 90f, 0.0f);
      streamWriter.WriteLine(PrettyPrinter.Format(quaternion) + " " + PrettyPrinter.Format(quaternion.eulerAngles));
      quaternion = Quaternion.Euler(0.0f, 0.0f, 90f);
      streamWriter.WriteLine(PrettyPrinter.Format(quaternion) + " " + PrettyPrinter.Format(quaternion.eulerAngles));
      for (int index = 0; index < 100; ++index)
      {
        quaternion = UnityEngine.Random.rotation;
        streamWriter.WriteLine(PrettyPrinter.Format(quaternion) + " " + PrettyPrinter.Format(Algorithm3D.NormalizeEuler(quaternion.eulerAngles)) + PrettyPrinter.Format(Quaternion.Inverse(quaternion)));
      }
    }
  }

  public static bool AreClose(float x, float y, float delta)
  {
    return (double) Mathf.Abs(x - y) / (double) Mathf.Max(Mathf.Abs(x + y) * 0.5f, Mathf.Epsilon) < (double) delta;
  }

  public static float FindMaxSpeed(List<RcsManeuverRecorder.RotationFrame> rotationFrames, Vector3 size)
  {
    float num1 = 0.0f;
    Vector3 vector3 = new Vector3((float) ((double) size.y * (double) size.y + (double) size.z * (double) size.z), (float) ((double) size.x * (double) size.x + (double) size.z * (double) size.z), (float) ((double) size.x * (double) size.x + (double) size.y * (double) size.y));
    vector3.Normalize();
    using (List<RcsManeuverRecorder.RotationFrame>.Enumerator enumerator = rotationFrames.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        float angle;
        Vector3 axis;
        Quaternion.Euler(enumerator.Current.speed).ToAngleAxis(out angle, out axis);
        axis.Normalize();
        float num2 = Mathf.Abs(angle) * (float) rotationFrames.Count;
        float num3 = (float) (((double) axis.x * (double) axis.x * (double) vector3.x + (double) axis.y * (double) axis.y * (double) vector3.y + (double) axis.z * (double) axis.z * (double) vector3.z) * (double) num2 * (double) num2 * 0.5);
        if ((double) num3 > (double) num1)
          num1 = num3;
      }
    }
    return Mathf.Sqrt(2f * num1 / Mathf.Max(Mathf.Max(vector3.x, vector3.y), vector3.z));
  }

  public class RotationFrame
  {
    public float time;
    public int index;
    public Quaternion rotation;
    public Vector3 angle;
    public Vector3 speed;
    public Vector3 acceleration;

    public RotationFrame(int index, Quaternion rotation, float time)
    {
      this.time = time;
      this.index = index;
      this.rotation = rotation;
      this.angle = Algorithm3D.NormalizeEuler(rotation.eulerAngles);
      this.speed = Vector3.zero;
      this.acceleration = Vector3.zero;
    }

    public void Normalize(RcsManeuverRecorder.RotationFrame previousFrame)
    {
      this.angle = Algorithm3D.NormalizeEuler(this.angle, previousFrame.angle);
      this.speed = Algorithm3D.NormalizeEuler(this.speed, previousFrame.speed);
      this.acceleration = Algorithm3D.NormalizeEuler(this.acceleration, previousFrame.acceleration);
    }

    public void CalcSpeed(RcsManeuverRecorder.RotationFrame nextFrame)
    {
      this.speed = Algorithm3D.NormalizeEuler((Quaternion.Inverse(this.rotation) * nextFrame.rotation).eulerAngles);
    }

    public void CalcAcceleration(RcsManeuverRecorder.RotationFrame nextFrame)
    {
      this.acceleration = Algorithm3D.NormalizeEuler(nextFrame.speed - this.speed);
    }

    public override string ToString()
    {
      return this.index.ToString() + " " + PrettyPrinter.Format(this.angle.x) + " " + PrettyPrinter.Format(this.angle.y) + " " + PrettyPrinter.Format(this.angle.z) + " " + PrettyPrinter.Format(this.speed.x) + " " + PrettyPrinter.Format(this.speed.y) + " " + PrettyPrinter.Format(this.speed.z) + " " + PrettyPrinter.Format(this.acceleration.x) + " " + PrettyPrinter.Format(this.acceleration.y) + " " + PrettyPrinter.Format(this.acceleration.z);
    }
  }
}
