﻿// Decompiled with JetBrains decompiler
// Type: ItemFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class ItemFactory
{
  public static ShipItem ReadItem(BgoProtocolReader r)
  {
    ItemFactory.ItemType itemType = (ItemFactory.ItemType) r.ReadByte();
    switch (itemType)
    {
      case ItemFactory.ItemType.None:
        return (ShipItem) null;
      case ItemFactory.ItemType.System:
        return (ShipItem) r.ReadDesc<ShipSystem>();
      case ItemFactory.ItemType.Countable:
        return (ShipItem) r.ReadDesc<ItemCountable>();
      case ItemFactory.ItemType.Starter:
        return (ShipItem) r.ReadDesc<StarterKit>();
      case ItemFactory.ItemType.Ship:
        return (ShipItem) r.ReadDesc<StoreShip>();
      default:
        throw new Exception("Unknown Item Type: " + (object) itemType);
    }
  }

  public static ShipItem ReadItemWithID(BgoProtocolReader r)
  {
    ushort num = r.ReadUInt16();
    ShipItem shipItem = ItemFactory.ReadItem(r);
    shipItem.ServerID = num;
    return shipItem;
  }

  public static ItemList ReadItemList(BgoProtocolReader r)
  {
    List<ShipItem> items = new List<ShipItem>();
    int num = (int) r.ReadUInt16();
    for (int index = 0; index < num; ++index)
    {
      ShipItem shipItem = ItemFactory.ReadItemWithID(r);
      items.Add(shipItem);
    }
    return new ItemList((IContainerID) null, items);
  }

  public static List<T> ReadList<T>(BgoProtocolReader r) where T : ShipItem
  {
    List<T> objList = new List<T>();
    int num = (int) r.ReadUInt16();
    for (int index = 0; index < num; ++index)
    {
      T obj = (T) ItemFactory.ReadItem(r);
      objList.Add(obj);
    }
    return objList;
  }

  public enum ItemType : byte
  {
    None,
    System,
    Countable,
    Starter,
    Ship,
  }
}
