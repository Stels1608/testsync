﻿// Decompiled with JetBrains decompiler
// Type: CheckboxWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CheckboxWidget : ButtonWidget, ILocalizeable
{
  [SerializeField]
  private UIToggle checkbox;
  [HideInInspector]
  public OnSettingChanged OnSettingChanged;
  protected UserSetting setting;

  public bool IsChecked
  {
    get
    {
      return this.checkbox.value;
    }
    set
    {
      this.checkbox.value = value;
    }
  }

  public void Init(UserSetting sett, bool value)
  {
    this.setting = sett;
    this.IsChecked = value;
  }

  public override void OnClick()
  {
    base.OnClick();
    if (this.OnSettingChanged == null)
      return;
    this.OnSettingChanged(this.setting, (object) this.IsChecked);
  }

  public void ReloadLanguageData()
  {
  }
}
