﻿// Decompiled with JetBrains decompiler
// Type: LeaderboardDataProvider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Model;

public class LeaderboardDataProvider : DataProvider<Message>
{
  public LeaderboardDataProvider()
    : base("TournamentDataProvider")
  {
  }

  public void ReceiveRankingCounter(RankingCounterData data)
  {
    this.OwnerFacade.SendMessage(Message.RankingCounterUpdate, (object) data);
  }

  public void ReceiveRankingTournament(RankingTournamentData data)
  {
    this.OwnerFacade.SendMessage(Message.RankingTournamentUpdate, (object) data);
  }

  public void ReceiveRankingCounterPlayer(RankDescription data)
  {
    this.OwnerFacade.SendMessage(Message.RankingCounterPlayerUpdate, (object) data);
  }
}
