﻿// Decompiled with JetBrains decompiler
// Type: Thrusters
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Thrusters : MonoBehaviour
{
  public float maxAngle = 35f;
  public float RandomFactor = 0.1f;
  private const int MAX_VISIBILITY_DISTANCE = 400;
  public Transform ThrusterParent;
  public GameObject ThrusterEffect;
  private Dictionary<Transform, ParticleSystem> thrusters;
  private Tick lastUpdateTick;

  public void Awake()
  {
    this.thrusters = new Dictionary<Transform, ParticleSystem>();
    this.lastUpdateTick = new Tick(0);
    if ((Object) this.ThrusterParent == (Object) null)
      UnityEngine.Debug.LogError((object) ("Thruster Parent not set up for: " + this.name));
    foreach (Transform key in this.ThrusterParent)
    {
      GameObject gameObject = Object.Instantiate<GameObject>(this.ThrusterEffect);
      gameObject.transform.position = key.position;
      gameObject.transform.rotation = key.rotation;
      gameObject.transform.parent = key;
      ParticleSystem component = gameObject.GetComponent<ParticleSystem>();
      this.thrusters.Add(key, component);
    }
  }

  public void UpdateThrusters(List<global::ThrusterEffect> thrusterEffects)
  {
    Tick tick = Tick.Previous((double) Time.time);
    if (tick <= this.lastUpdateTick)
      return;
    this.lastUpdateTick = tick;
    PlayerShip playerShip = SpaceLevel.GetLevel().GetPlayerShip();
    if (playerShip == null || (double) (this.transform.position - playerShip.Position).sqrMagnitude > 160000.0)
      return;
    using (Dictionary<Transform, ParticleSystem>.Enumerator enumerator1 = this.thrusters.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        KeyValuePair<Transform, ParticleSystem> current1 = enumerator1.Current;
        Transform key = current1.Key;
        ParticleSystem particleSystem = current1.Value;
        if (thrusterEffects.Count == 0)
        {
          particleSystem.Stop();
          particleSystem.loop = false;
        }
        using (List<global::ThrusterEffect>.Enumerator enumerator2 = thrusterEffects.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            global::ThrusterEffect current2 = enumerator2.Current;
            if (current2 is ThrusterEffectStrafing)
            {
              Vector3 to = this.transform.rotation * ((ThrusterEffectStrafing) current2).ThrusterDirectionLocal;
              if ((double) Vector3.Angle(key.rotation * Vector3.forward, to) < (double) this.maxAngle && (double) Random.value < (double) this.RandomFactor)
                particleSystem.Play();
            }
            if (current2 is ThrusterEffectTurning)
            {
              ThrusterEffectTurning thrusterEffectTurning = (ThrusterEffectTurning) current2;
              float angle;
              Vector3 axis;
              thrusterEffectTurning.ShipRotationDelta.ToAngleAxis(out angle, out axis);
              if ((double) Mathf.Abs(angle) >= 1.0)
              {
                Vector3 vector3 = key.position - this.transform.position;
                Vector3 to = Quaternion.Inverse(thrusterEffectTurning.ShipRotationDelta) * vector3 - vector3;
                if ((double) Vector3.Angle(key.rotation * Vector3.forward, to) < (double) this.maxAngle && (double) Random.value < (double) this.RandomFactor / 7.0)
                  particleSystem.Play();
              }
            }
          }
        }
      }
    }
  }

  public void FireThrustersManually(Vector3 localDirection, float duration)
  {
    using (Dictionary<Transform, ParticleSystem>.Enumerator enumerator = this.thrusters.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<Transform, ParticleSystem> current = enumerator.Current;
        Transform key = current.Key;
        ParticleSystem particleSystem = current.Value;
        Vector3 to = this.transform.rotation * localDirection;
        if ((double) Vector3.Angle(key.rotation * Vector3.forward, to) < (double) this.maxAngle)
        {
          particleSystem.Play();
          particleSystem.loop = true;
        }
      }
    }
    this.StartCoroutine(this.StopAllThrusters(duration));
  }

  [DebuggerHidden]
  public IEnumerator StopAllThrusters(float delay)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Thrusters.\u003CStopAllThrusters\u003Ec__Iterator1B() { delay = delay, \u003C\u0024\u003Edelay = delay, \u003C\u003Ef__this = this };
  }
}
