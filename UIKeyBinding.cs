﻿// Decompiled with JetBrains decompiler
// Type: UIKeyBinding
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Key Binding")]
public class UIKeyBinding : MonoBehaviour
{
  public KeyCode keyCode;
  public UIKeyBinding.Modifier modifier;
  public UIKeyBinding.Action action;
  private bool mIgnoreUp;
  private bool mIsInput;
  private bool mPress;

  private void Start()
  {
    UIInput component = this.GetComponent<UIInput>();
    this.mIsInput = (Object) component != (Object) null;
    if (!((Object) component != (Object) null))
      return;
    EventDelegate.Add(component.onSubmit, new EventDelegate.Callback(this.OnSubmit));
  }

  private void OnSubmit()
  {
    if (UICamera.currentKey != this.keyCode || !this.IsModifierActive())
      return;
    this.mIgnoreUp = true;
  }

  private bool IsModifierActive()
  {
    if (this.modifier == UIKeyBinding.Modifier.None)
      return true;
    if (this.modifier == UIKeyBinding.Modifier.Alt)
    {
      if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
        return true;
    }
    else if (this.modifier == UIKeyBinding.Modifier.Control)
    {
      if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        return true;
    }
    else if (this.modifier == UIKeyBinding.Modifier.Shift && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
      return true;
    return false;
  }

  private void Update()
  {
    if (this.keyCode == KeyCode.None || !this.IsModifierActive())
      return;
    if (this.action == UIKeyBinding.Action.PressAndClick || this.action == UIKeyBinding.Action.All)
    {
      if (UICamera.inputHasFocus)
        return;
      UICamera.currentTouch = UICamera.controller;
      UICamera.currentScheme = UICamera.ControlScheme.Mouse;
      UICamera.currentTouch.current = this.gameObject;
      if (Input.GetKeyDown(this.keyCode))
      {
        this.mPress = true;
        UICamera.Notify(this.gameObject, "OnPress", (object) true);
      }
      if (Input.GetKeyUp(this.keyCode))
      {
        UICamera.Notify(this.gameObject, "OnPress", (object) false);
        if (this.mPress)
        {
          UICamera.Notify(this.gameObject, "OnClick", (object) null);
          this.mPress = false;
        }
      }
      UICamera.currentTouch.current = (GameObject) null;
    }
    if (this.action != UIKeyBinding.Action.Select && this.action != UIKeyBinding.Action.All || !Input.GetKeyUp(this.keyCode))
      return;
    if (this.mIsInput)
    {
      if (!this.mIgnoreUp && !UICamera.inputHasFocus)
        UICamera.selectedObject = this.gameObject;
      this.mIgnoreUp = false;
    }
    else
      UICamera.selectedObject = this.gameObject;
  }

  public enum Action
  {
    PressAndClick,
    Select,
    All,
  }

  public enum Modifier
  {
    None,
    Shift,
    Control,
    Alt,
  }
}
