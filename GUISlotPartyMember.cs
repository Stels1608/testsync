﻿// Decompiled with JetBrains decompiler
// Type: GUISlotPartyMember
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using System.Collections.Generic;
using UnityEngine;

public class GUISlotPartyMember : GUISlotWithBuffs
{
  private GUIButtonNew target;
  private GUIImageNew bgOffline;
  private GUILabelNew offline;
  private Player player;

  public Player Player
  {
    get
    {
      return this.player;
    }
    set
    {
      this.player = value;
      if (this.player == null)
        return;
      this.NameString = this.player.GetTaggedName(this.player.Name);
    }
  }

  private float Health
  {
    get
    {
      if (this.player.Stats != null)
        return this.player.Stats.HullPoints / this.player.Stats.MaxHullPoints;
      return 0.0f;
    }
  }

  private float Shields
  {
    get
    {
      if (this.player.Stats != null)
        return this.player.Stats.PowerPoints / this.player.Stats.MaxPowerPoints;
      return 0.0f;
    }
  }

  public GUISlotPartyMember(SmartRect parent)
    : base(parent)
  {
    Texture2D normal = (Texture2D) ResourceLoader.Load("GUI/Slots/playertarget");
    Texture2D over = (Texture2D) ResourceLoader.Load("GUI/Slots/playertarget_over");
    Texture2D pressed = (Texture2D) ResourceLoader.Load("GUI/Slots/playertarget_click");
    this.target = new GUIButtonNew(normal, over, pressed, this.root);
    this.target.Position = new float2((float) ((double) this.background.SmartRect.Width / 2.0 + 5.0 + (double) normal.width / 2.0), 0.0f);
    this.target.TextLabel.IsRendered = false;
    JWindowDescription jwindowDescription = Loaders.LoadResource("GUI/Slots/offline_layout");
    this.bgOffline = (jwindowDescription["background"] as JImage).CreateGUIImageNew(this.root);
    this.offline = (jwindowDescription["offline"] as JLabel).CreateGUILabelNew(this.root);
  }

  public override void Draw()
  {
    base.Draw();
    this.DrawTarget();
    this.DrawOffline();
  }

  private void DrawTarget()
  {
    if (this.Player == null || this.Player.PlayerShip == null || (!this.target.IsRendered || this.Player.PlayerShip.Dead))
      return;
    this.target.Draw();
  }

  private void DrawOffline()
  {
    if (!this.offline.IsRendered)
      return;
    this.bgOffline.Draw();
    this.offline.Draw();
  }

  public override void Update()
  {
    base.Update();
    this.RedLevel = this.Health;
    this.BlueLevel = this.Shields;
    if (!this.player.Online)
    {
      this.background.IsRendered = false;
      this.redStrip.IsRendered = false;
      this.blueStrip.IsRendered = false;
      this.healthLevel.IsRendered = false;
      this.energyLevel.IsRendered = false;
      this.bgOffline.IsRendered = true;
      this.offline.IsRendered = true;
    }
    else
    {
      this.background.IsRendered = true;
      this.redStrip.IsRendered = true;
      this.blueStrip.IsRendered = true;
      this.healthLevel.IsRendered = GUISlot.ShowStats;
      this.energyLevel.IsRendered = GUISlot.ShowStats;
      this.bgOffline.IsRendered = false;
      this.offline.IsRendered = false;
    }
    SpaceSubscribeInfo spaceSubscribeInfo = this.player.Stats;
    if (spaceSubscribeInfo == null)
      return;
    this.target.IsRendered = false;
    if (spaceSubscribeInfo.Target != null && spaceSubscribeInfo.Target != Game.Me.Ship && (spaceSubscribeInfo.Target != Game.Me.ActualShip && this.player.IsSameSector))
      this.target.IsRendered = true;
    this.timeToUpdateBuffs -= (float) (double) Time.deltaTime;
    if ((double) this.timeToUpdateBuffs <= 0.0)
    {
      this.buffs.Clear();
      int num = 0;
      using (List<ShipBuff>.Enumerator enumerator = spaceSubscribeInfo.DisplayedShipBuffs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ShipBuff current = enumerator.Current;
          if ((bool) current.IsLoaded && (double) current.TimeLeft > 0.0)
          {
            this.buffs.Add(current);
            ++num;
          }
          if (num >= 10)
            break;
        }
      }
      if (this.healthLevel.IsRendered)
        this.SetHealthString((int) spaceSubscribeInfo.HullPoints, (int) spaceSubscribeInfo.MaxHullPoints);
      if (this.energyLevel.IsRendered)
        this.SetEnergyString((int) spaceSubscribeInfo.PowerPoints, (int) spaceSubscribeInfo.MaxPowerPoints);
      this.timeToUpdateBuffs = 0.1f;
    }
    this.drawBuffs = this.buffs.Count > 0;
    this.timeToUpdateLevel -= (float) (double) Time.deltaTime;
    if ((double) this.timeToUpdateLevel > 0.0)
      return;
    this.leaderMark.IsRendered = Game.Me.Party.Leader == this.player;
    this.Name = this.player.Name;
    this.LevelString = this.player.Level.ToString();
    this.timeToUpdateLevel = 0.1f;
  }

  protected override void UpdateGUISlotAvatar(GUISlotAvatar avatar)
  {
    if (avatar == null || this.player == null || this.player.ShipCards == null)
      return;
    if (this.player.ShipCards.Length > 0)
    {
      avatar.Texture = this.player.ShipCards[0].ItemGUICard.GUIAvatarSlotTexture;
    }
    else
    {
      avatar.Texture = this.humanAvatar;
      if (this.player.Faction == Faction.Cylon)
        avatar.Texture = this.cylonAvatar;
    }
    avatar.IsRendered = true;
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.target.RecalculateAbsCoords();
    this.bgOffline.RecalculateAbsCoords();
    this.offline.RecalculateAbsCoords();
  }

  public override bool Contains(float2 point)
  {
    if (!base.Contains(point))
      return this.target.Contains(point);
    return true;
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse0 && this.target.IsRendered && this.target.OnMouseDown(mousePosition, mouseKey))
    {
      this.target.IsPressed = true;
      if (this.player.Stats != null && !this.player.IsMe)
        TargetSelector.SelectTargetRequest(this.player.Stats.Target, false);
      return true;
    }
    if (mouseKey == KeyCode.Mouse0 && this.background.Contains(mousePosition))
      return true;
    if (mouseKey != KeyCode.Mouse1 || !this.avatar.Contains(mousePosition))
      return false;
    this.avatarClicked = true;
    return true;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse0 && this.target.IsRendered && this.target.OnMouseUp(mousePosition, mouseKey))
      return true;
    if (mouseKey == KeyCode.Mouse0 && this.background.Contains(mousePosition))
    {
      if (Game.Me.Arena.ArenaType == ArenaType.Duel1vs1 && this.player.PlayerShip.IsCloaked)
        return true;
      this.player.SetMyTarget();
      return true;
    }
    if (!this.avatarClicked || !this.avatar.Contains(mousePosition) || mouseKey != KeyCode.Mouse1)
      return false;
    GUISlotPopup guiSlotPopup = Game.GUIManager.Find<GUISlotManager>().Popup;
    guiSlotPopup.Clear();
    guiSlotPopup.Player = this.player;
    if (!Game.Me.ActiveShip.IsCapitalShip)
      guiSlotPopup.AddDuel();
    guiSlotPopup.AppendPartyMenu();
    guiSlotPopup.Parent = this.avatar.SmartRect;
    guiSlotPopup.PlaceRightDownCornerOf((GUIPanel) this.avatar, -20f);
    guiSlotPopup.IsRendered = true;
    this.avatarClicked = false;
    return true;
  }

  public override bool OnShowTooltip(float2 mousePosition)
  {
    if (base.OnShowTooltip(mousePosition))
      return true;
    if (!this.name.Contains(mousePosition) || !this.nameWasCut)
      return false;
    this.name.SetTooltip(this.player.Name);
    Game.TooltipManager.ShowTooltip(this.name.AdvancedTooltip);
    return true;
  }
}
