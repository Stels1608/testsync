﻿// Decompiled with JetBrains decompiler
// Type: Activity
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum Activity
{
  GameclientStarted = 1,
  GameclientConnectsToServer = 2,
  GameclientConnectedToServer = 3,
  GameclientConnectionFailed = 4,
  GameclientRequestsLocalization = 5,
  GameclientDownloadedLocalization = 6,
  GameclientRequestsLogin = 7,
  LoginSuccessful = 8,
  LoginFailed = 9,
  LoginQueue = 10,
  StartScene = 11,
  FactionSelected = 12,
  Logout = 13,
  CharacterCreationStarted = 14,
  CharacterCreationCompleted = 15,
}
