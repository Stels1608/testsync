﻿// Decompiled with JetBrains decompiler
// Type: AntennaBlinkingLight
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class AntennaBlinkingLight : MonoBehaviour
{
  public float FadingTime = 1f;
  public float OnMinTime = 1f;
  public float OnMaxTime = 2f;
  public float OffMinTime = 1f;
  public float OffMaxTime = 2f;
  private Material material;
  private Color color;

  private void OnDestroy()
  {
    if (!(bool) ((Object) this.material))
      return;
    Object.Destroy((Object) this.material);
  }

  [DebuggerHidden]
  private IEnumerator Start()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AntennaBlinkingLight.\u003CStart\u003Ec__Iterator32() { \u003C\u003Ef__this = this };
  }

  private void SetAlpha(float alpha)
  {
    this.material.SetColor("_TintColor", new Color(this.color.r, this.color.g, this.color.b, this.color.a * alpha));
  }
}
