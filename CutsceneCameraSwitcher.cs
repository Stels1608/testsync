﻿// Decompiled with JetBrains decompiler
// Type: CutsceneCameraSwitcher
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CutsceneCameraSwitcher : MonoBehaviour
{
  public void SwitchToCamera(string cameraId)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    foreach (CutsceneCamera cutsceneCamera in ((IEnumerable<CutsceneCamera>) this.GetComponentsInChildren<CutsceneCamera>()).Where<CutsceneCamera>(new Func<CutsceneCamera, bool>(new CutsceneCameraSwitcher.\u003CSwitchToCamera\u003Ec__AnonStorey60() { cameraId = cameraId }.\u003C\u003Em__32)))
    {
      if (SectorEditorHelper.IsEditorLevel())
        EditorLevel.SwitchCameraTo(cutsceneCamera.Camera);
      if ((UnityEngine.Object) SpaceLevel.GetLevel() != (UnityEngine.Object) null)
        SpaceLevel.GetLevel().cameraSwitcher.SetActiveCamera(cutsceneCamera.Camera, true);
    }
  }
}
