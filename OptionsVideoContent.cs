﻿// Decompiled with JetBrains decompiler
// Type: OptionsVideoContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class OptionsVideoContent : OptionsContent
{
  private bool previousIsFullscreen = Screen.fullScreen;
  private SettingsVideoQuality settingsVideoQuality;
  private SettingsVideoAdvanced settingsVideoAdvanced;
  private InputBindingFooterButtonBar buttonBar;
  private SettingsVideoLayout layout;
  private InputBindingFooterButtonBar footerButtonBar;
  private UserSettings currentSettings;

  public void Awake()
  {
    GameObject child = new GameObject("layout");
    PositionUtils.CorrectTransform(this.gameObject, child);
    this.layout = child.AddComponent<SettingsVideoLayout>();
    child.layer = this.gameObject.layer;
    this.InitContent();
    this.currentSettings = (FacadeFactory.GetInstance().FetchDataProvider("SettingsDataProvider") as SettingsDataProvider).CurrentSettings;
  }

  public void Update()
  {
    if (this.previousIsFullscreen == Screen.fullScreen)
      return;
    this.previousIsFullscreen = Screen.fullScreen;
    this.CheckFullscreen();
  }

  private void CheckFullscreen()
  {
    if (this.currentSettings.Fullscreen == Screen.fullScreen)
      return;
    ((OptionButtonElement) this.settingsVideoAdvanced.controls[UserSetting.Fullscreen]).toggleButton.IsChecked = Screen.fullScreen;
  }

  public override void UpdateSettings(UserSettings settings)
  {
    base.UpdateSettings(settings);
    this.settingsVideoQuality.Init(settings);
    this.settingsVideoAdvanced.Init(settings);
  }

  public override void ChangeSetting(UserSetting setting, object value)
  {
    base.ChangeSetting(setting, value);
    if (setting == UserSetting.AntiAliasing || setting == UserSetting.ShowStarFog || (setting == UserSetting.ShowStarDust || setting == UserSetting.ShowGlowEffect) || (setting == UserSetting.ViewDistance || setting == UserSetting.HighResModels || (setting == UserSetting.HighResTextures || setting == UserSetting.UseProceduralTextures)) || (setting == UserSetting.ShowBulletImpactFx || setting == UserSetting.FlakFieldDensity || (setting == UserSetting.HighQualityParticles || setting == UserSetting.ShowShipSkins) || setting == UserSetting.ShowWeaponModules))
    {
      this.settings.Set(UserSetting.GraphicsQuality, (object) GraphicsQuality.Custom);
      this.UpdateSettings(this.settings);
    }
    if (setting == UserSetting.GraphicsQuality)
    {
      this.settings.ApplyGraphicPreset((GraphicsQuality) value);
      this.UpdateSettings(this.settings);
    }
    if (setting != UserSetting.Fullscreen)
      return;
    FacadeFactory.GetInstance().SendMessage(Message.SetFullscreen, value);
  }

  private void InitContent()
  {
    GameObject gameObject1 = NGUITools.AddChild(this.layout.ContentArea[0], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/video/SettingsVideoQuality"));
    if ((UnityEngine.Object) gameObject1 == (UnityEngine.Object) null)
      throw new ArgumentNullException("Settings general object failed to load");
    this.settingsVideoQuality = gameObject1.GetComponent<SettingsVideoQuality>();
    this.settingsVideoQuality.OnSettingChanged = new OnSettingChanged(this.ChangeSetting);
    GameObject gameObject2 = NGUITools.AddChild(this.layout.ContentArea[1], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/video/SettingsVideoAdvanced"));
    if ((UnityEngine.Object) gameObject2 == (UnityEngine.Object) null)
      throw new ArgumentNullException("Settings video advanced object failed to load");
    this.settingsVideoAdvanced = gameObject2.GetComponent<SettingsVideoAdvanced>();
    this.settingsVideoAdvanced.OnSettingChanged = new OnSettingChanged(this.ChangeSetting);
    GameObject gameObject3 = NGUITools.AddChild(this.layout.ContentArea[2], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingFooterButtonBar"));
    if ((UnityEngine.Object) gameObject3 == (UnityEngine.Object) null)
      throw new ArgumentNullException("initInputBindingOptionsContent");
    this.footerButtonBar = gameObject3.GetComponent<InputBindingFooterButtonBar>();
    this.footerButtonBar.SetButtonDelegates(new AnonymousDelegate(((OptionsContent) this).Undo), new AnonymousDelegate(((OptionsContent) this).Apply), new AnonymousDelegate(this.ResetDialog));
  }

  private void ResetDialog()
  {
    DialogBoxFactoryNgui.CreateResetSettingsDialogBox(new AnonymousDelegate(((OptionsContent) this).RestoreDefaultSettings));
  }

  public override void RestoreDefaultSettings()
  {
    this.settingsVideoAdvanced.RestoreDefaultSettings(this.settings);
    this.settingsVideoQuality.RestoreDefaultSettings(this.settings);
    this.UpdateSettings(this.settings);
    this.Apply();
  }
}
