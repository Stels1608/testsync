﻿// Decompiled with JetBrains decompiler
// Type: ResourceLoader
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ResourceLoader
{
  private readonly Dictionary<string, Object> cache = new Dictionary<string, Object>();

  public static Object Load(string file)
  {
    Object @object = (Object) null;
    if ((Object) GameLevel.Instance != (Object) null)
    {
      ResourceLoader resourceLoader = GameLevel.Instance.GUIManager.ResourceLoader;
      if (!resourceLoader.cache.TryGetValue(file, out @object) || @object == (Object) null)
      {
        @object = resourceLoader.LoadResource(file);
        resourceLoader.cache[file] = @object;
      }
    }
    else
      @object = new ResourceLoader().LoadResource(file);
    if (@object == (Object) null)
      Log.DebugInfo((object) ("The file, " + file + ", was not loaded. Make sure the asset exists."));
    else
      @object.name = file;
    return @object;
  }

  public static T Load<T>(string file) where T : Object
  {
    return ResourceLoader.Load(file) as T;
  }

  private Object LoadResource(string file)
  {
    if (string.IsNullOrEmpty(file))
    {
      Log.DebugInfo((object) ("Invalid filename specified " + file));
      return (Object) null;
    }
    if (Game.Me == null)
      return Resources.Load(file);
    if (Game.Language == "el" && (file.StartsWith("GUI/Fonts/") || file.StartsWith("/GUI/Fonts/")))
      file = this.GreekFontHack(file);
    string directoryName = Path.GetDirectoryName(file);
    string fileName = Path.GetFileName(file);
    Object @object = Game.Me.Faction != Faction.Cylon ? Resources.Load(file) ?? Resources.Load(directoryName + "/_human/" + fileName) : Resources.Load(directoryName + "/_cylon/" + fileName) ?? Resources.Load(file);
    if (@object == (Object) null)
      Debug.LogWarning((object) ("Resourse doesn't exist: " + file));
    return @object;
  }

  private string GreekFontHack(string path)
  {
    if (path.EndsWith("BGM BT") || path.EndsWith("DS-DIGIB") || (path.EndsWith("DS EF M") || path.EndsWith("DS EF B")))
      return "GUI/Fonts/VoxPro-Bold";
    return path;
  }
}
