﻿// Decompiled with JetBrains decompiler
// Type: LODScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class LODScript : ModelScript
{
  private static bool _lodSystemEnabled = true;
  private float lastScale = 1f;
  private const float UPDATE_INTERVAL = 2f;
  public LODListener[] Listeners;
  private Transform _transform;
  private static Transform activeCameraTransform;
  private float lastSqrDistance;

  public static bool LodSystemEnabled
  {
    get
    {
      return LODScript._lodSystemEnabled;
    }
    set
    {
      LODScript._lodSystemEnabled = value;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    if ((UnityEngine.Object) LODScript.activeCameraTransform == (UnityEngine.Object) null)
      LODScript.activeCameraTransform = Camera.main.transform;
    this._transform = this.transform;
  }

  private void Start()
  {
    this.InvokeRepeating("UpdateDistances", 2f + UnityEngine.Random.Range(0.0f, 2f), 2f);
    this.Invoke("UpdateDistances", 0.0f);
    this.enabled = false;
  }

  public void UpdateDistances()
  {
    if (this.SpaceObject != null && this.SpaceObject.IsMe || !LODScript.LodSystemEnabled)
    {
      if ((double) Math.Abs(this.lastSqrDistance - 1f) <= 1.0 / 1000.0)
        return;
      this.SetSqrDistance(1f);
    }
    else
    {
      if (this.SpaceObject != null && (double) Mathf.Abs(this.SpaceObject.Scale - this.lastScale) > (double) Mathf.Epsilon)
      {
        foreach (LODListener listener in this.Listeners)
        {
          if ((UnityEngine.Object) listener != (UnityEngine.Object) null)
            listener.Scale = this.SpaceObject.Scale;
        }
        this.lastScale = this.SpaceObject.Scale;
      }
      float sqrMagnitude = (LODScript.activeCameraTransform.position - this._transform.position).sqrMagnitude;
      float num = sqrMagnitude / this.lastSqrDistance;
      if ((double) num <= 1.21000003814697 && (double) num >= 0.810000002384186)
        return;
      this.SetSqrDistance(sqrMagnitude);
    }
  }

  private void SetSqrDistance(float sqrDistance)
  {
    foreach (LODListener listener in this.Listeners)
    {
      if ((UnityEngine.Object) listener != (UnityEngine.Object) null)
        listener.SqrDistance = sqrDistance;
    }
    this.lastSqrDistance = sqrDistance;
  }

  private void Reset()
  {
    this.Listeners = this.gameObject.GetComponentsInChildren<LODListener>();
  }

  public static void SetActiveCamera(Camera camera)
  {
    LODScript.activeCameraTransform = camera.transform;
  }
}
