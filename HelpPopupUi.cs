﻿// Decompiled with JetBrains decompiler
// Type: HelpPopupUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public abstract class HelpPopupUi : MessageBoxBase
{
  protected override void Awake()
  {
    base.Awake();
    this.ShowTimer(false);
  }

  public void SetContent(OnMessageBoxClose onClose, string titleText, string mainText, uint timeToLive)
  {
    this.SetContent(onClose, titleText, mainText, "%$bgo.common.ok%", "%$bgo.common.cancel%", timeToLive);
  }

  public void SetContent(OnMessageBoxClose onClose, string titleText, string mainText, string okButtonText, string cancelButtonText, uint timeToLive)
  {
    this.AddOnClosedAction(onClose);
    this.SetTitleText(Tools.ParseMessage(titleText).ToUpper());
    this.SetOkButtonText(Tools.ParseMessage(okButtonText));
    this.SetCancelButtonText(Tools.ParseMessage(cancelButtonText));
    this.SetMainText(Tools.ParseMessage(mainText));
    this.SetTimeToLive(timeToLive);
  }

  protected abstract void SetCancelButtonText(string text);

  protected abstract void SetTitleText(string text);

  protected abstract void SetTimerText(string text);

  public abstract void ShowTimer(bool show);

  public void OnCancelPressed()
  {
    this.CloseWindow(MessageBoxActionType.Cancel);
  }

  protected override void OnTimeLeftUpdated(string mainText)
  {
    this.SetTimerText(mainText);
  }
}
