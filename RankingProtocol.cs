﻿// Decompiled with JetBrains decompiler
// Type: RankingProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class RankingProtocol : BgoProtocol
{
  public RankingProtocol()
    : base(BgoProtocol.ProtocolID.Ranking)
  {
  }

  public static RankingProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Ranking) as RankingProtocol;
  }

  public void RequestRankingCounter(RankingGroup group, RankingType rankingType, uint page, byte sortBy)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 7);
    bw.Write((ushort) group);
    bw.Write((ushort) rankingType);
    bw.Write(page);
    bw.Write(sortBy);
    this.SendMessage(bw);
  }

  public void RequestRankingCounterPlayer(RankingGroup group, RankingType rankingType, byte sortBy)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 9);
    bw.Write((ushort) group);
    bw.Write((ushort) rankingType);
    bw.Write(sortBy);
    this.SendMessage(bw);
  }

  public void RequestRankingTournament(TournamentRankingGroup group, TournamentRankingType tournamentIdx, ushort bracketId, uint page, byte sortBy)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 11);
    bw.Write((short) group);
    bw.Write((short) tournamentIdx);
    bw.Write(bracketId);
    bw.Write(page);
    bw.Write(sortBy);
    this.SendMessage(bw);
  }

  public void RequestRankingTournamentPlayer(TournamentRankingGroup group, TournamentRankingType tournamentIdx, ushort bracketId, byte sortBy)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 13);
    bw.Write((short) group);
    bw.Write((short) tournamentIdx);
    bw.Write(bracketId);
    bw.Write(sortBy);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    RankingProtocol.Reply reply = (RankingProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case RankingProtocol.Reply.ReplyRankingCounter:
        ((LeaderboardDataProvider) FacadeFactory.GetInstance().FetchDataProvider("TournamentDataProvider")).ReceiveRankingCounter(new RankingCounterData((RankingGroup) br.ReadUInt16(), (RankingType) br.ReadUInt16(), br.ReadDescList<RankDescription>(), br.ReadUInt32(), br.ReadDateTime()));
        break;
      case RankingProtocol.Reply.ReplyRankingCounterPlayer:
      case RankingProtocol.Reply.ReplyRankingTournamentPlayer:
        uint num1 = br.ReadUInt32();
        uint num2 = br.ReadUInt32();
        double num3 = br.ReadDouble();
        double num4 = br.ReadDouble();
        double num5 = br.ReadDouble();
        ((LeaderboardDataProvider) FacadeFactory.GetInstance().FetchDataProvider("TournamentDataProvider")).ReceiveRankingCounterPlayer(new RankDescription()
        {
          name = Game.Me.Name,
          playerId = Game.Me.ServerID,
          rank = num1,
          position = num2,
          score1 = num3,
          score2 = num4,
          score3 = num5,
          faction = Game.Me.Faction
        });
        break;
      case RankingProtocol.Reply.ReplyRankingTournament:
        ((LeaderboardDataProvider) FacadeFactory.GetInstance().FetchDataProvider("TournamentDataProvider")).ReceiveRankingTournament(new RankingTournamentData((TournamentRankingGroup) br.ReadUInt16(), (TournamentRankingType) br.ReadUInt16(), br.ReadUInt16(), br.ReadDescList<RankDescription>(), br.ReadUInt32(), br.ReadDateTime()));
        break;
    }
  }

  public enum Request : ushort
  {
    [Obsolete("Old highscore system")] RequestRankingTab = 3,
    [Obsolete("Old highscore system")] RequestPlayerRank = 5,
    RequestRankingCounter = 7,
    RequestRankingCounterPlayer = 9,
    RequestRankingTournament = 11,
    RequestRankingTournamentPlayer = 13,
  }

  public enum Reply : ushort
  {
    [Obsolete("Old highscore system")] ReplyRankingTab = 4,
    [Obsolete("Old highscore system")] ReplyPlayerRank = 6,
    ReplyRankingCounter = 8,
    ReplyRankingCounterPlayer = 10,
    ReplyRankingTournament = 12,
    ReplyRankingTournamentPlayer = 14,
  }
}
