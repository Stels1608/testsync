﻿// Decompiled with JetBrains decompiler
// Type: FactionSelectionUi
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public abstract class FactionSelectionUi : Window
{
  [SerializeField]
  protected GameObject windowParent;

  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  public abstract void SetWindowHeaderText(string text);

  public abstract void SetFactionTextColonial(string text);

  public abstract void SetBonusTextColonial(string text);

  public abstract void SetFactionTextCylon(string text);

  public abstract void SetBonusTextCylon(string text);

  public abstract void SetFactionBonusHeaders(string text);

  public abstract void SetPleaseWaitText(string text);

  public abstract void ShowPleaseWaitLabel();

  protected override void Start()
  {
    this.SetLocalizedStaticTexts();
  }

  public void SetBonuses(StarterLevelProfile levelProfile)
  {
    string text1 = string.Empty;
    string text2 = string.Empty;
    if (levelProfile.ColonialBonus != null)
      text1 = levelProfile.ColonialBonus.GUICard.Description.Replace("%br%", "\n");
    this.SetBonusTextColonial(text1);
    if (levelProfile.CylonBonus != null)
      text2 = levelProfile.CylonBonus.GUICard.Description.Replace("%br%", "\n");
    this.SetBonusTextCylon(text2);
  }

  private void SetLocalizedStaticTexts()
  {
    this.SetWindowHeaderText(Tools.ParseMessage("%$bgo.FactionSelection.layout.label%"));
    this.SetFactionTextColonial(Tools.ParseMessage("%$bgo.FactionSelection.layout.label_1%"));
    this.SetFactionTextCylon(Tools.ParseMessage("%$bgo.FactionSelection.layout.label_0%"));
    this.SetFactionBonusHeaders(Tools.ParseMessage("%$bgo.FactionSelection.layout.faction_bonus%"));
    this.SetPleaseWaitText(Tools.ParseMessage("%$bgo.FactionSelection.layout.please_wait%"));
  }

  public void OnCylonsSelected()
  {
    this.ChoseFaction(Faction.Cylon);
  }

  public void OnColonialsSelected()
  {
    this.ChoseFaction(Faction.Colonial);
  }

  private void ChoseFaction(Faction faction)
  {
    this.SwitchToTransitionMode();
    Game.Funnel.ReportActivity(Activity.FactionSelected);
    PlayerProtocol.GetProtocol().SelectFaction(faction);
  }

  private void SwitchToTransitionMode()
  {
    this.windowParent.gameObject.SetActive(false);
    this.ShowPleaseWaitLabel();
  }
}
