﻿// Decompiled with JetBrains decompiler
// Type: RewardCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RewardCard : Card
{
  public List<ShipItem> Items = new List<ShipItem>();
  public string Package = string.Empty;
  public uint ItemGroup = 101;
  public int Experience;
  public AugmentActionType Action;
  public uint PackagedCubits;
  public GUICard GUICard;

  public RewardCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.Experience = r.ReadInt32();
    List<ShipItem> shipItemList = ItemFactory.ReadList<ShipItem>(r);
    this.Action = (AugmentActionType) r.ReadByte();
    this.PackagedCubits = r.ReadUInt32();
    this.Package = r.ReadString();
    this.ItemGroup = r.ReadUInt32();
    this.GUICard = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
    if (Game.Me.Faction == Faction.Cylon)
    {
      ItemFactory.ReadList<ShipItem>(r);
      this.Items = ItemFactory.ReadList<ShipItem>(r);
    }
    else
    {
      this.Items = ItemFactory.ReadList<ShipItem>(r);
      ItemFactory.ReadList<ShipItem>(r);
    }
    this.Items.AddRange((IEnumerable<ShipItem>) shipItemList);
    this.IsLoaded.Depend<ShipItem>((IEnumerable<ShipItem>) this.Items);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.GUICard
    });
  }

  public override string ToString()
  {
    return "\n[RewardCard]: " + (object) this.GUICard + "\nPackage: " + this.Package + "\nItemGroup: " + (object) this.ItemGroup + "\nPackagedCubits: " + (object) this.PackagedCubits + "\nAction: " + (object) this.Action + "\nItems size: " + (object) this.Items.Count;
  }
}
