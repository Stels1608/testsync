﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorMedalsNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class HudIndicatorMedalsNgui : HudIndicatorBaseComponentNgui, IHudIndicatorMedal, IReceivesSpaceObject
{
  private UITableWidget tableWidget;
  private UIWidget anchorWidget;
  private PlayerAwardsAtlasLinker atlasLinker;
  private SpaceObject spaceObject;

  public override bool RequiresAnimationNgui
  {
    get
    {
      return false;
    }
  }

  protected override void ResetComponentStates()
  {
    this.spaceObject = (SpaceObject) null;
  }

  protected override void Awake()
  {
    base.Awake();
    this.atlasLinker = PlayerAwardsAtlasLinker.Instance;
  }

  public void AnchorToTop(UIWidget widget)
  {
    this.anchorWidget = widget;
  }

  protected override void SetColoring()
  {
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    this.gameObject.SetActive((this.InScannerRange || this.IsSelected) && this.InScreen);
  }

  public void OnSpaceObjectInjection(SpaceObject spaceObject)
  {
    this.spaceObject = spaceObject;
    this.UpdateMedals();
  }

  public void UpdateMedals()
  {
    PlayerShip playerShip = this.spaceObject as PlayerShip;
    if (playerShip == null)
      return;
    Player player = playerShip.Player;
    if ((Object) this.tableWidget != (Object) null)
      Object.Destroy((Object) this.tableWidget.gameObject);
    this.tableWidget = NGUITools.AddChild<UITableWidget>(this.gameObject);
    this.tableWidget.SetLayout(UITableWidgetLayout.Horizontal, new Vector2(32f, 28f), UIOrigin.Centered);
    int num = 0;
    if (player.PvpMedal != PvpMedal.None && !Game.Me.TournamentParticipant)
      num += this.AddAward(this.atlasLinker.GetPvpMedal(player.PvpMedal, player.Faction));
    if (player.TournamentMedal != TournamentMedal.None)
      num += this.AddAward(this.atlasLinker.GetTournamentMedal(player.TournamentMedal));
    if (player.TournamentIndicator != TournamentIndicator.None)
      num += this.AddAward(this.atlasLinker.GetTournamentIndicator(player.TournamentIndicator));
    if (player.Nemesis)
      num += this.AddAward(this.atlasLinker.GetTournamentNemesis);
    this.tableWidget.Reposition();
    if (num == 0)
    {
      Object.Destroy((Object) this.tableWidget);
      this.tableWidget = (UITableWidget) null;
    }
    else
      NGUIToolsExtension.AnchorTo((UIRect) this.tableWidget, (UIRect) this.anchorWidget, UIAnchor.Side.Top, 0, 0);
  }

  private int AddAward(string atlasString)
  {
    if (string.IsNullOrEmpty(atlasString))
      return 0;
    return NGUIToolsExtension.AddSprite(this.tableWidget.gameObject, this.atlasLinker.Atlas, atlasString, true, string.Empty).width;
  }
}
