﻿// Decompiled with JetBrains decompiler
// Type: DistanceQualityEvaluator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class DistanceQualityEvaluator : IQualityEvaluator
{
  public DistanceQualityEvaluator.DistanceQualityEvaluatorRule Rule;
  public int MaxQuality;
  public int MinQuality;
  public float MaxQualityDist;
  public float MinQualityDist;
  public Transform Target;
  public Transform Source;
  public bool FixedSource;
  private Vector3 sourcePosition;

  public float QualityDist
  {
    set
    {
      this.MaxQualityDist = value;
      this.MinQualityDist = value;
    }
  }

  public Vector3 SourcePosition
  {
    get
    {
      return this.sourcePosition;
    }
    set
    {
      this.FixedSource = true;
      this.sourcePosition = value;
    }
  }

  int IQualityEvaluator.Evaluate()
  {
    if ((UnityEngine.Object) this.Target == (UnityEngine.Object) null || (UnityEngine.Object) this.Source == (UnityEngine.Object) null && !this.FixedSource)
      return this.MinQuality;
    if (this.MaxQuality == this.MinQuality)
      throw new Exception("MaxQaulity == MinQuality");
    if (this.MinQuality - this.MaxQuality == 1 && (double) this.MinQualityDist != (double) this.MaxQualityDist)
      throw new Exception("MinQuality - MaxQuality == 1, but MinQualityDist != MaxQualityDist");
    float num1 = !this.FixedSource ? (this.Target.position - this.Source.position).sqrMagnitude : (this.Target.position - this.sourcePosition).sqrMagnitude;
    if ((double) num1 >= (double) this.MinQualityDist * (double) this.MinQualityDist)
      return this.MinQuality;
    if ((double) num1 <= (double) this.MaxQualityDist * (double) this.MaxQualityDist)
      return this.MaxQuality;
    if (this.Rule != DistanceQualityEvaluator.DistanceQualityEvaluatorRule.Linear)
      throw new Exception("You've forgotten to add rule " + (object) this.Rule + " in switch statement");
    float num2 = this.MinQualityDist - this.MaxQualityDist;
    int num3 = this.MinQuality - this.MaxQuality - 1;
    float num4 = num2 / (float) num3;
    for (int index = 0; index < num3 - 1; ++index)
    {
      float num5 = this.MaxQualityDist + (float) index * num4;
      float num6 = this.MaxQualityDist + (float) (index + 1) * num4;
      if ((double) num1 > (double) num5 * (double) num5 && (double) num1 <= (double) num6 * (double) num6)
        return this.MaxQuality + index + 1;
    }
    return this.MinQuality - 1;
  }

  public enum DistanceQualityEvaluatorRule
  {
    Linear,
  }
}
