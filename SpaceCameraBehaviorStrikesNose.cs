﻿// Decompiled with JetBrains decompiler
// Type: SpaceCameraBehaviorStrikesNose
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SpaceCameraBehaviorStrikesNose : SpaceCameraBehaviorBase
{
  private float zoomDistance;

  public override bool BlocksControls
  {
    get
    {
      return false;
    }
  }

  public override bool ShowStrikesHud
  {
    get
    {
      return true;
    }
  }

  public SpaceCameraBehaviorStrikesNose()
  {
    this.zoomDistance = SpaceCameraBase.WantedZoomDistance;
  }

  public override SpaceCameraParameters CalculateCurrentCameraParameters()
  {
    this.zoomDistance = Mathf.Lerp(this.zoomDistance, SpaceCameraBase.WantedZoomDistance, Time.deltaTime * 5f);
    Quaternion rotation = this.PlayerShip.Rotation;
    Vector3 vector3 = this.PlayerShip.Position + this.PlayerShip.Rotation * new Vector3(0.0f, 0.0f, this.PlayerShip.ShipBounds.z / 2f);
    float fieldOfView = Camera.main.fieldOfView;
    return new SpaceCameraParameters() { Fov = fieldOfView, CamPosition = vector3, CamRotation = rotation };
  }
}
