﻿// Decompiled with JetBrains decompiler
// Type: CharacterSceneRoot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Tooltips;
using System.Collections;
using System.Diagnostics;
using Unity5AssetHandling;
using UnityEngine;

public class CharacterSceneRoot : MonoBehaviour
{
  public GameObject cameraCylon;
  public GameObject cameraHuman;
  public GameObject lightCylon;
  public GameObject lightHuman;
  public Canvas canvas;
  public CharacterModification characterModification;
  public GameObject loadingScreen;
  private CameraSwitcherCharacterScene cameraSwitcher;
  private bool serverReplyReceived;
  private bool sceneLoaded;

  public CharacterMenu Menu { get; private set; }

  private void Awake()
  {
    this.Menu = Object.FindObjectOfType<CharacterMenu>();
    this.Menu.gameObject.SetActive(false);
    Game.TooltipManager.HideTooltip((GuiAdvancedTooltipBase) null);
  }

  private void Start()
  {
    AssetRequest request = (AssetRequest) null;
    if (Game.Me.Faction == Faction.Colonial)
      request = AssetCatalogue.Instance.Request("character_creation_human.prefab", false);
    else if (Game.Me.Faction == Faction.Cylon)
      request = AssetCatalogue.Instance.Request("character_creation_cylon.prefab", false);
    else
      UnityEngine.Debug.LogError((object) ("Unknown faction: " + (object) Game.Me.Faction));
    this.StartCoroutine(this.WaitForPrefab(request));
  }

  [DebuggerHidden]
  private IEnumerator WaitForPrefab(AssetRequest request)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CharacterSceneRoot.\u003CWaitForPrefab\u003Ec__Iterator20() { request = request, \u003C\u0024\u003Erequest = request, \u003C\u003Ef__this = this };
  }

  public void OnCharacterServiceInfoReceived()
  {
    if (this.sceneLoaded)
      this.loadingScreen.SetActive(false);
    this.serverReplyReceived = true;
  }

  private void OnGUI()
  {
    Game.TooltipManager.Draw();
  }

  public void Destroy()
  {
    this.cameraSwitcher.SetActiveCamera(Camera.main);
    Object.Destroy((Object) this.canvas.gameObject);
    Object.DestroyImmediate((Object) this.gameObject);
  }
}
