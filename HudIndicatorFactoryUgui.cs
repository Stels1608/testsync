﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorFactoryUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Paths;
using UnityEngine;

public static class HudIndicatorFactoryUgui
{
  private static GameObject BracketDynamicProtoype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorBracketDynamic");
    }
  }

  private static GameObject HealthbarPrototype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorHealthBar");
    }
  }

  private static GameObject TopTextPrototype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorTopText");
    }
  }

  private static GameObject DistanceTextPrototype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorDistanceText");
    }
  }

  private static GameObject ArrowPrototype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorOutsideScreenArrow");
    }
  }

  private static GameObject WaypointPrototype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorWaypoint");
    }
  }

  private static GameObject MedalsPrototype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorMedals");
    }
  }

  private static GameObject MultiTargetPrototype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorMultiTarget");
    }
  }

  private static GameObject DesignatorPrototype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorDesignator");
    }
  }

  private static GameObject AsteroidResourcePrototype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorAsteroidResource");
    }
  }

  private static GameObject CombatInfoPrototype
  {
    get
    {
      return Resources.Load<GameObject>(Ui.Prefabs + "/HudIndicators/HudIndicatorCombatInfo");
    }
  }

  public static HudIndicatorBase CreateIndicator(ISpaceEntity target, GameObject parentCanvas)
  {
    if (target is SpacePosition)
      return HudIndicatorFactoryUgui.CreateSpacePositionIndicator(target as SpacePosition, parentCanvas);
    if (target is SpaceObject)
      return HudIndicatorFactoryUgui.CreateSpaceObjectIndicator(target as SpaceObject, parentCanvas);
    Debug.LogError((object) "HudIndicatorFactory.CreateIndicator(): Invalid target type");
    return (HudIndicatorBase) null;
  }

  private static HudIndicatorBase CreateSpacePositionIndicator(SpacePosition target, GameObject parent)
  {
    HudIndicatorSpacePositionUgui child = UguiTools.CreateChild<HudIndicatorSpacePositionUgui>(parent.transform);
    child.name = "HudIndicator SP (" + target.Name + ")";
    child.CreateMasterComponentFromObject<HudIndicatorBracketBigUgui>(HudIndicatorFactoryUgui.BracketDynamicProtoype);
    child.AddHudIndicatorComponentFromObject<HudIndicatorWaypointUgui>(HudIndicatorFactoryUgui.WaypointPrototype, (Transform) null);
    child.AddHudIndicatorComponentFromObject<HudIndicatorOutsideScreenArrowUgui>(HudIndicatorFactoryUgui.ArrowPrototype, (Transform) null);
    return (HudIndicatorBase) child;
  }

  private static HudIndicatorBase CreateSpaceObjectIndicator(SpaceObject target, GameObject parent)
  {
    HudIndicatorBase cachedIndicator;
    if (HudIndicatorCache.Instance.TryPopCachedIndicator(target.SpaceEntityType, out cachedIndicator))
    {
      HudIndicatorBaseUgui indicatorBaseUgui = (HudIndicatorBaseUgui) cachedIndicator;
      indicatorBaseUgui.Target = (ISpaceEntity) target;
      indicatorBaseUgui.gameObject.SetActive(true);
      indicatorBaseUgui.name = "HudIndicator SO (" + (!(target is PlayerShip) ? target.Name : "Player") + ")";
      return (HudIndicatorBase) indicatorBaseUgui;
    }
    HudIndicatorBaseUgui indicatorBaseUgui1 = (HudIndicatorBaseUgui) UguiTools.CreateChild<HudIndicatorSpaceObjectUgui>(parent.transform);
    indicatorBaseUgui1.name = "HudIndicator SO (" + (!(target is PlayerShip) ? target.Name : "Player") + ")";
    if (target.SpaceEntityType == SpaceEntityType.Trigger)
    {
      indicatorBaseUgui1.CreateMasterComponentFromObject<HudIndicatorWaypointUgui>(HudIndicatorFactoryUgui.WaypointPrototype);
      indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorDistanceTextUgui>(HudIndicatorFactoryUgui.DistanceTextPrototype, (Transform) null);
      indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorOutsideScreenArrowUgui>(HudIndicatorFactoryUgui.ArrowPrototype, (Transform) null);
      return (HudIndicatorBase) indicatorBaseUgui1;
    }
    indicatorBaseUgui1.CreateMasterComponentFromObject<HudIndicatorBracketBigUgui>(HudIndicatorFactoryUgui.BracketDynamicProtoype);
    indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorCombatInfoUgui>(HudIndicatorFactoryUgui.CombatInfoPrototype, (Transform) null);
    indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorMultiTargetUgui>(HudIndicatorFactoryUgui.MultiTargetPrototype, (Transform) null);
    indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorDesignatorUgui>(HudIndicatorFactoryUgui.DesignatorPrototype, (Transform) null);
    indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorWaypointUgui>(HudIndicatorFactoryUgui.WaypointPrototype, (Transform) null);
    indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorDistanceTextUgui>(HudIndicatorFactoryUgui.DistanceTextPrototype, (Transform) null);
    indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorOutsideScreenArrowUgui>(HudIndicatorFactoryUgui.ArrowPrototype, (Transform) null);
    HudIndicatorTopTextUgui indicatorTopTextUgui = indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorTopTextUgui>(HudIndicatorFactoryUgui.TopTextPrototype, (Transform) null);
    switch (target.SpaceEntityType)
    {
      case SpaceEntityType.Missile:
      case SpaceEntityType.Mine:
      case SpaceEntityType.JumpTargetTransponder:
        indicatorBaseUgui1.SetMinimumUnselectedBracketWidth(19f);
        break;
    }
    if (HudIndicatorInfo.CanBeDamaged((ISpaceEntity) target))
      indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorHealthUgui>(HudIndicatorFactoryUgui.HealthbarPrototype, (Transform) null);
    if (target.SpaceEntityType == SpaceEntityType.Player)
      indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorMedalsUgui>(HudIndicatorFactoryUgui.MedalsPrototype, indicatorTopTextUgui.transform);
    switch (target.SpaceEntityType)
    {
      case SpaceEntityType.Asteroid:
      case SpaceEntityType.Planetoid:
        indicatorBaseUgui1.AddHudIndicatorComponentFromObject<HudIndicatorAsteroidResourceUgui>(HudIndicatorFactoryUgui.AsteroidResourcePrototype, (Transform) null);
        break;
    }
    indicatorBaseUgui1.NeverBlocksRaycast = !target.IsTargetable;
    return (HudIndicatorBase) indicatorBaseUgui1;
  }
}
