﻿// Decompiled with JetBrains decompiler
// Type: StatView
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum StatView : byte
{
  MinRange,
  MaxRange,
  OptimalRange,
  Duration,
  Cooldown,
  BuffCost,
  Durability,
  Target,
  DMGLow,
  DMGHigh,
  Accuracy,
  CriticalOffense,
  Angle,
  Mining,
  FlareRange,
  RestoreBuff,
  RemoteBuffMultiply,
  StaticBuff,
  RestorePowerBuff,
  DrainLow,
  DrainHigh,
  ArmorPiercing,
  ArmorValue,
  HPRecovery,
  PPRecovery,
  Speed,
  TurnSpeed,
  TurnAcceleration,
  InertiaCompensation,
  MultiplyBuff,
  RemoteBuffAdd,
  AoERadius,
  BuffCostPerSecond,
  ToggleSystemAdd,
  ToggleSystemMultiply,
  HP,
  LifeTime,
}
