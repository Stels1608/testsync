﻿// Decompiled with JetBrains decompiler
// Type: AsteroidGroupsManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AsteroidGroupsManager
{
  private static readonly AsteroidGroupsManager instance = new AsteroidGroupsManager();
  private Dictionary<Asteroid, AsteroidGroup> asteroidGroups;
  private GameObject asteroidGroupsRoot;

  public static AsteroidGroupsManager Instance
  {
    get
    {
      return AsteroidGroupsManager.instance;
    }
  }

  public GameObject AsteroidGroupsRoot
  {
    get
    {
      if ((Object) this.asteroidGroupsRoot == (Object) null)
        this.asteroidGroupsRoot = new GameObject("SectorMap - AsteroidGroupsRoot");
      return this.asteroidGroupsRoot;
    }
  }

  private AsteroidGroupsManager()
  {
    this.asteroidGroups = new Dictionary<Asteroid, AsteroidGroup>();
  }

  public void AddToAnAsteroidGroup(Asteroid asteroid)
  {
    if ((Object) SpaceLevel.GetLevel() == (Object) null)
      return;
    AsteroidGroup asteroidGroup = (AsteroidGroup) null;
    using (Dictionary<Asteroid, AsteroidGroup>.ValueCollection.Enumerator enumerator = this.asteroidGroups.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AsteroidGroup current = enumerator.Current;
        if ((double) (current.Position - asteroid.Position).sqrMagnitude < 90000.0)
          asteroidGroup = current;
      }
    }
    if (asteroidGroup == null)
      asteroidGroup = SpaceLevel.GetLevel().GetSpacePositionRegistry().CreateSpacePosition<AsteroidGroup>();
    asteroidGroup.AddAsteroid(asteroid);
    this.asteroidGroups.Add(asteroid, asteroidGroup);
  }

  public void RemoveFromAnAsteroidGroup(Asteroid asteroid)
  {
    if (this.asteroidGroups.ContainsKey(asteroid))
      this.asteroidGroups[asteroid].RemoveAsteroid(asteroid);
    else
      Debug.LogWarning((object) ("RemoveFromAnAsteroidGroup() failed: " + asteroid.Name + " was in no asteroid group"));
    this.asteroidGroups.Remove(asteroid);
  }

  public AsteroidGroup FindAsteroidGroup(Asteroid asteroid)
  {
    return this.asteroidGroups[asteroid];
  }

  public void UpdateAsteroidGroup(Asteroid asteroid)
  {
    this.RemoveFromAnAsteroidGroup(asteroid);
    this.AddToAnAsteroidGroup(asteroid);
  }
}
