﻿// Decompiled with JetBrains decompiler
// Type: Stickers
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class Stickers : SingleLODListener
{
  private DecalSpot[] stickerSpots;
  private Ship ship;
  private bool isLodActive;

  protected override void ModelSetOrSwitched()
  {
    base.ModelSetOrSwitched();
    this.SetupStickers();
  }

  private void SetupStickers()
  {
    this.ship = this.SpaceObject as Ship;
    if (this.ship == null)
      Debug.LogError((object) "Stickers have to be attached to Ship");
    if (this.SpaceObject.GetSpotsOfType(SpotType.Sticker) == null)
    {
      Debug.LogError((object) "StickerSpots are null");
    }
    else
    {
      this.stickerSpots = Array.ConvertAll<Spot, DecalSpot>(this.SpaceObject.GetSpotsOfType(SpotType.Sticker), (Converter<Spot, DecalSpot>) (input => (DecalSpot) input));
      if (this.ship.Bindings.StickerBindings.Count > 0)
        new StickerManager((IEnumerable<DecalSpot>) this.stickerSpots, this.SpaceObject.Faction).ApplyBindingAll(this.ship.Bindings.StickerBindings[0]);
      this.OnSwitched(this.isLodActive);
    }
  }

  protected override void OnSwitched(bool value)
  {
    this.isLodActive = value;
    if (this.stickerSpots == null)
      return;
    foreach (DecalSpot stickerSpot in this.stickerSpots)
    {
      if (value)
        stickerSpot.Show();
      else
        stickerSpot.Hide();
    }
  }
}
