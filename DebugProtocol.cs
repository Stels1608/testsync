﻿// Decompiled with JetBrains decompiler
// Type: DebugProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class DebugProtocol : BgoProtocol
{
  public DebugProcessStateView ProcessStateWindow;

  public DebugProtocol()
    : base(BgoProtocol.ProtocolID.Debug)
  {
  }

  public static DebugProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Debug) as DebugProtocol;
  }

  public void CommandList(string[] paramList)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    foreach (string str in paramList)
      bw.Write(str);
    this.SendMessage(bw);
  }

  public void Command(params string[] paramList)
  {
    this.CommandList(paramList);
  }

  public void RequestProcessState()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 14);
    this.SendMessage(bw);
  }

  public void ReportActivity(string activity)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 12);
    bw.Write(activity);
    this.SendMessage(bw);
  }

  public void UpgradeSystem(ShipSystem system, byte newLevel)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 17);
    bw.Write((IProtocolWrite) system.Container.ContainerID);
    bw.Write(system.ServerID);
    bw.Write(newLevel);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    DebugProtocol.Reply reply = (DebugProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case DebugProtocol.Reply.Command:
        DebugUtility.LogInfo("Server notify: " + br.ReadString());
        break;
      case DebugProtocol.Reply.Message:
        DebugMessage.Add(br.ReadString());
        break;
      case DebugProtocol.Reply.ProcessState:
        string str = br.ReadString();
        if (!((UnityEngine.Object) this.ProcessStateWindow != (UnityEngine.Object) null))
          break;
        this.ProcessStateWindow.SetStateString(str);
        break;
      case DebugProtocol.Reply.UpdateRoles:
        DebugUtility.Create(br.ReadUInt32());
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Debug protocol: " + (object) reply);
        break;
    }
  }

  public enum Request : ushort
  {
    Command = 1,
    Activity = 12,
    ProcessState = 14,
    UpgradeSystem = 17,
  }

  public enum Reply : ushort
  {
    Command = 2,
    Message = 3,
    Counters = 9,
    ProcessState = 15,
    UpdateRoles = 16,
  }
}
