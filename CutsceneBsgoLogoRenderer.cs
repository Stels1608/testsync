﻿// Decompiled with JetBrains decompiler
// Type: CutsceneBsgoLogoRenderer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CutsceneBsgoLogoRenderer : MonoBehaviour
{
  private float duration = 8f;
  private Texture bsgoLogo;
  private float progress;

  public void LoadLogo()
  {
    BgoUtils.LoadAssetFromBundle<Texture>("bsgoLogoHiRes.png", new BgoUtils.AssetLoadedCallback<Texture>(this.OnLogoLoaded));
  }

  private void OnLogoLoaded(Texture logo)
  {
    this.bsgoLogo = logo;
  }

  public void ShowLogo()
  {
    this.StartCoroutine(this.ShowLogoCoroutine());
  }

  [DebuggerHidden]
  private IEnumerator ShowLogoCoroutine()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CutsceneBsgoLogoRenderer.\u003CShowLogoCoroutine\u003Ec__IteratorF() { \u003C\u003Ef__this = this };
  }

  private void OnGUI()
  {
    this.DrawLogo();
  }

  private void DrawLogo()
  {
    if ((double) this.progress < 1.0 / 1000.0 || (double) this.progress > 1.0 || (Object) this.bsgoLogo == (Object) null)
      return;
    GUI.depth = 0;
    Matrix4x4 matrix = GUI.matrix;
    float num = (float) (1.0 + (double) this.progress * 0.200000002980232);
    GUI.matrix = Matrix4x4.TRS(new Vector3((float) Screen.width / 2f, (float) Screen.height / 2f, 0.0f), Quaternion.identity, new Vector3(num, num, 1f));
    Color color1 = GUI.color;
    Color color2 = GUI.color;
    color2.a = Mathf.Sin(3.141593f * this.progress) * 2f;
    GUI.color = color2;
    GUI.DrawTexture(new Rect((float) ((double) -Screen.width / 2.0 * 0.699999988079071), (float) ((double) -Screen.height / 2.0 * 0.699999988079071), (float) Screen.width * 0.7f, (float) Screen.height * 0.7f), this.bsgoLogo, ScaleMode.ScaleToFit);
    GUI.color = color1;
    GUI.matrix = matrix;
  }
}
