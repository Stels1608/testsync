﻿// Decompiled with JetBrains decompiler
// Type: RemoteCollider.RemoteCapsuleCollider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace RemoteCollider
{
  public class RemoteCapsuleCollider : RemoteCollider.RemoteCollider
  {
    public string axis;
    public float radius;
    public float height;

    public RemoteCapsuleCollider()
    {
      this.axis = "x";
      this.radius = 1f;
      this.height = 1f;
    }

    public RemoteCapsuleCollider(int axis, float radius, float height)
    {
      switch (axis)
      {
        case 0:
          this.axis = "x";
          break;
        case 1:
          this.axis = "y";
          break;
        case 2:
          this.axis = "z";
          break;
      }
      this.radius = radius;
      this.height = height;
      this.type = "capsule";
    }
  }
}
