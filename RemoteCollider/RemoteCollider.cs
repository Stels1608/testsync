﻿// Decompiled with JetBrains decompiler
// Type: RemoteCollider.RemoteCollider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace RemoteCollider
{
  public class RemoteCollider
  {
    public string type = string.Empty;

    public void SaveToContentdb(string docName)
    {
      ContentDB.UpdateDocument(docName, "/Collider/", (object) this);
      Debug.Log((object) ("Collider Type " + this.type.ToString() + " being saved"));
      Debug.Log((object) ("Collider in json doc " + docName + " saved"));
    }

    public static RemoteCollider.RemoteCollider GetObjectCollider(Collider collider)
    {
      if (collider is BoxCollider)
      {
        BoxCollider boxCollider = collider as BoxCollider;
        RemoteCollider.RemoteCollider collider1 = (RemoteCollider.RemoteCollider) new RemoteBoxCollider(boxCollider.size);
        if ((double) boxCollider.center.magnitude < 1.0)
          return collider1;
        return (RemoteCollider.RemoteCollider) new RemoteCompoundCollider(collider1, boxCollider.center);
      }
      if (collider is SphereCollider)
      {
        SphereCollider sphereCollider = collider as SphereCollider;
        RemoteCollider.RemoteCollider collider1 = (RemoteCollider.RemoteCollider) new RemoteSphereCollider(sphereCollider.radius);
        if ((double) sphereCollider.center.magnitude < 1.0)
          return collider1;
        return (RemoteCollider.RemoteCollider) new RemoteCompoundCollider(collider1, sphereCollider.center);
      }
      if (!(collider is CapsuleCollider))
        return (RemoteCollider.RemoteCollider) null;
      CapsuleCollider capsuleCollider = collider as CapsuleCollider;
      RemoteCollider.RemoteCollider collider2 = (RemoteCollider.RemoteCollider) new RemoteCapsuleCollider(capsuleCollider.direction, capsuleCollider.radius, capsuleCollider.height);
      if ((double) capsuleCollider.center.magnitude < 1.0)
        return collider2;
      return (RemoteCollider.RemoteCollider) new RemoteCompoundCollider(collider2, capsuleCollider.center);
    }
  }
}
