﻿// Decompiled with JetBrains decompiler
// Type: RemoteCollider.RemoteCompoundColliderChild
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using CouchDB;
using Json;
using System.Collections.Generic;
using UnityEngine;

namespace RemoteCollider
{
  [JsonClassInstantiation(ConstructorJsonDataArgs = new string[] {"collider"})]
  public class RemoteCompoundColliderChild
  {
    public Vector3 position;
    public Quaternion rotation;
    [JsonField(deserialize = false, serialize = true)]
    public RemoteCollider.RemoteCollider collider;

    public RemoteCompoundColliderChild(JsonData colliderData)
    {
      string key = JsonSerializator.Deserialize<RemoteCollider.RemoteCollider>(colliderData).type;
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (RemoteCompoundColliderChild.\u003C\u003Ef__switch\u0024map10 == null)
        {
          // ISSUE: reference to a compiler-generated field
          RemoteCompoundColliderChild.\u003C\u003Ef__switch\u0024map10 = new Dictionary<string, int>(3)
          {
            {
              "box",
              0
            },
            {
              "capsule",
              1
            },
            {
              "sphere",
              2
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (RemoteCompoundColliderChild.\u003C\u003Ef__switch\u0024map10.TryGetValue(key, out num))
        {
          switch (num)
          {
            case 0:
              this.collider = (RemoteCollider.RemoteCollider) JsonSerializator.Deserialize<RemoteBoxCollider>(colliderData);
              return;
            case 1:
              this.collider = (RemoteCollider.RemoteCollider) JsonSerializator.Deserialize<RemoteCapsuleCollider>(colliderData);
              return;
            case 2:
              this.collider = (RemoteCollider.RemoteCollider) JsonSerializator.Deserialize<RemoteSphereCollider>(colliderData);
              return;
          }
        }
      }
      Debug.Log((object) "Collider type not known");
    }

    public RemoteCompoundColliderChild(RemoteCollider.RemoteCollider collider, Vector3 position, Quaternion rotation)
    {
      this.position = position;
      this.rotation = rotation;
      this.collider = collider;
    }
  }
}
