﻿// Decompiled with JetBrains decompiler
// Type: RemoteCollider.RemoteCompoundCollider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace RemoteCollider
{
  public class RemoteCompoundCollider : RemoteCollider.RemoteCollider
  {
    public List<RemoteCompoundColliderChild> children = new List<RemoteCompoundColliderChild>();

    public RemoteCompoundCollider()
    {
      this.type = "compound";
    }

    public RemoteCompoundCollider(IEnumerable<RemoteCompoundColliderChild> children)
      : this()
    {
      this.children.AddRange(children);
    }

    public RemoteCompoundCollider(RemoteCollider.RemoteCollider collider, Vector3 position)
      : this()
    {
      this.Add(collider, position, Quaternion.identity);
    }

    public void Add(RemoteCollider.RemoteCollider collider, Vector3 position, Quaternion rotation)
    {
      this.children.Add(new RemoteCompoundColliderChild(collider, position, rotation));
    }
  }
}
