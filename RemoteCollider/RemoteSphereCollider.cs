﻿// Decompiled with JetBrains decompiler
// Type: RemoteCollider.RemoteSphereCollider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace RemoteCollider
{
  public class RemoteSphereCollider : RemoteCollider.RemoteCollider
  {
    public float radius;

    public RemoteSphereCollider()
    {
      this.radius = 1f;
    }

    public RemoteSphereCollider(float radius)
    {
      this.radius = radius;
      this.type = "sphere";
    }
  }
}
