﻿// Decompiled with JetBrains decompiler
// Type: RemoteCollider.RemoteBoxCollider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace RemoteCollider
{
  public class RemoteBoxCollider : RemoteCollider.RemoteCollider
  {
    public Vector3 size;

    public RemoteBoxCollider()
    {
      this.size = Vector3.zero;
    }

    public RemoteBoxCollider(Vector3 size)
    {
      this.size = size;
      this.type = "box";
    }
  }
}
