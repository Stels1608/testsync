﻿// Decompiled with JetBrains decompiler
// Type: DebrisPile
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class DebrisPile : SpaceObject
{
  public float RotationSpeed;

  public override string ObjectName
  {
    get
    {
      return this.Name;
    }
  }

  public override string Name
  {
    get
    {
      if (this.GUICard == null)
      {
        Debug.Log((object) ("Can't access debris pile name: " + this.PrefabName));
        return "(#" + this.PrefabName + "#)";
      }
      if (this.GUICard.Name.StartsWith("%$bgo."))
        return BsgoLocalization.Get("%$bgo.common.debris_field%");
      return BsgoLocalization.Get(this.GUICard.Name);
    }
  }

  public DebrisPile(uint objectID)
    : base(objectID)
  {
    this.HideStats = true;
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Position = r.ReadVector3();
    this.Rotation = r.ReadQuaternion();
    this.Scale = r.ReadVector3().x;
    this.RotationSpeed = r.ReadSingle();
  }

  public override void InitModelScripts()
  {
    Revolving modelScript = this.GetModelScript<Revolving>();
    if ((Object) modelScript != (Object) null)
      modelScript.Velocity = this.RotationSpeed;
    else if ((double) this.RotationSpeed > 0.0)
      Debug.LogWarning((object) ("RotationSpeed defined for " + this.Name + " but it has no RevolvingScript on its prefab " + this.PrefabName));
    base.InitModelScripts();
  }
}
