﻿// Decompiled with JetBrains decompiler
// Type: InputBindingFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class InputBindingFactory
{
  public static IInputBinding Unmapped(Action action, InputDevice device, byte profileNo)
  {
    IInputBinding newBinding = InputBindingFactory.CreateNewBinding(action, device, profileNo, (ushort) 0, (byte) 0, (byte) 0);
    newBinding.Unmap();
    return newBinding;
  }

  public static IInputBinding CreateNewBinding(Action action, InputDevice device, byte profile, ushort deviceTriggerCode, byte modifierCode, byte flags)
  {
    switch (device)
    {
      case InputDevice.KeyboardMouse:
        return (IInputBinding) new KeyBinding(action, profile, (KeyCode) deviceTriggerCode, (KeyModifier) modifierCode, (KeyFlags) flags);
      case InputDevice.Joystick:
        return (IInputBinding) new JoystickBinding(action, profile, (JoystickButtonCode) modifierCode, (JoystickButtonCode) deviceTriggerCode, (JoystickFlags) flags);
      default:
        Debug.LogError((object) ("Undefined device: " + (object) device));
        return (IInputBinding) null;
    }
  }
}
