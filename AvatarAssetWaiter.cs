﻿// Decompiled with JetBrains decompiler
// Type: AvatarAssetWaiter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Unity5AssetHandling;
using UnityEngine;

public class AvatarAssetWaiter : MonoBehaviour
{
  public AssetRequest AssetRequest;
  public System.Action<UnityEngine.Object> Callback;

  public static void Create(AssetRequest assetRequest, System.Action<UnityEngine.Object> callback)
  {
    AvatarAssetWaiter avatarAssetWaiter = new GameObject("waiter").AddComponent<AvatarAssetWaiter>();
    avatarAssetWaiter.AssetRequest = assetRequest;
    avatarAssetWaiter.Callback = callback;
  }

  private void Update()
  {
    if (!this.AssetRequest.IsDone)
      return;
    if (this.Callback != null)
      this.Callback(this.AssetRequest.Asset);
    UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
  }
}
