﻿// Decompiled with JetBrains decompiler
// Type: ItemAmountTupel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class ItemAmountTupel
{
  private readonly ShipItem item;
  private readonly uint amount;

  public uint Amount
  {
    get
    {
      return this.amount;
    }
  }

  public ShipItem Item
  {
    get
    {
      return this.item;
    }
  }

  public ItemAmountTupel(ShipItem item, uint amount = 1)
  {
    this.item = item;
    this.amount = amount;
  }
}
