﻿// Decompiled with JetBrains decompiler
// Type: AbstractCombatInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public abstract class AbstractCombatInfo
{
  private readonly SpaceObject target;

  public SpaceObject Target
  {
    get
    {
      return this.target;
    }
  }

  public abstract string DisplayString { get; }

  public AbstractCombatInfo(SpaceObject target)
  {
    this.target = target;
  }
}
