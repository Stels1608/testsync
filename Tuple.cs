﻿// Decompiled with JetBrains decompiler
// Type: Tuple
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public static class Tuple
{
  public static Tuple<T1, T2> New<T1, T2>(T1 first, T2 second)
  {
    return new Tuple<T1, T2>(first, second);
  }
}
