﻿// Decompiled with JetBrains decompiler
// Type: MissileLauncher
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class MissileLauncher : Weapon
{
  public float Volume = 1f;
  public float MinDistance = 1f;
  public AudioClip LaunchClip;
  private AudioSource launchAudio;

  public override void Fire(SpaceObject target)
  {
    if (!this.HighQuality)
      return;
    if ((Object) this.launchAudio == (Object) null)
    {
      this.launchAudio = this.gameObject.AddComponent<AudioSource>();
      this.launchAudio.spatialBlend = 1f;
    }
    this.launchAudio.volume = this.Volume;
    this.launchAudio.minDistance = this.MinDistance;
    this.launchAudio.maxDistance = this.launchAudio.minDistance * 256f;
    this.launchAudio.clip = this.LaunchClip;
    this.launchAudio.loop = false;
    this.launchAudio.Play();
  }
}
