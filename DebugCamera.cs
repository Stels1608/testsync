﻿// Decompiled with JetBrains decompiler
// Type: DebugCamera
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugCamera : MonoBehaviour
{
  public float Delta = 1f;

  private void OnGUI()
  {
    if (Event.current.type != UnityEngine.EventType.ScrollWheel)
      return;
    this.transform.position += new Vector3(0.0f, 0.0f, this.Delta * Event.current.delta.y);
  }
}
