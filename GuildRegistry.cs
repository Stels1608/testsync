﻿// Decompiled with JetBrains decompiler
// Type: GuildRegistry
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class GuildRegistry
{
  private InviteTimeoutManager inviteTimeout = new InviteTimeoutManager();

  public void Update()
  {
    this.inviteTimeout.Update();
  }

  public void StartGuild(string name)
  {
    name = Tools.MakeFirstLetterCapital(name);
    CommunityProtocol.GetProtocol().RequestGuildStart(name);
  }

  public void PromoteDemote(Player player, GuildRole role)
  {
    if (Game.Me.Guild.GuildRole <= player.GuildRole)
      return;
    CommunityProtocol.GetProtocol().RequestGuildPromote(player.ServerID, role);
  }

  public void KickPlayer(Player player)
  {
    if (Game.Me.Guild.GuildRole <= player.GuildRole)
      return;
    CommunityProtocol.GetProtocol().RequestGuildKick(player.ServerID);
  }

  public void LeaveGuild()
  {
    CommunityProtocol.GetProtocol().RequestGuildLeave();
  }

  public void AddPlayer(Player player)
  {
    if (this.inviteTimeout.IsTimeout(player))
    {
      FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.guild.cant_invite_often%", NotificationCategory.Neutral));
    }
    else
    {
      this.inviteTimeout.DeleteTimeout(player);
      this.inviteTimeout.AddTimeout(player);
      CommunityProtocol.GetProtocol().RequestGuildInvite(player.ServerID);
    }
  }

  public void ChangeRankName(GuildRole role, string name)
  {
    CommunityProtocol.GetProtocol().RequestGuildChangeRankName(role, name);
  }

  public void ChangeRankPermissions(GuildRole role, ulong permissions)
  {
    CommunityProtocol.GetProtocol().RequestGuildChangeRankPermissions(role, permissions);
  }
}
