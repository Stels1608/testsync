﻿// Decompiled with JetBrains decompiler
// Type: AsteroidShip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Unity5AssetHandling;
using UnityEngine;

public class AsteroidShip : Ship
{
  private float rotationSpeed;
  private GameObject destroyedModel;

  public AsteroidShip(uint objectID)
    : base(objectID)
  {
    this.rotationSpeed = Random.Range(3f, 10f);
    this.Rotation = Quaternion.Euler((float) Random.Range(0, 360), (float) Random.Range(0, 360), (float) Random.Range(0, 360));
  }

  public void SetDeathExplosionLOD()
  {
    int level = this.Root.GetComponentInChildren<Body>().Level;
    if (level >= 0)
    {
      this.destroyedModel.GetComponent<ParticleLODShuriken>().InheritedLODLevel = level;
    }
    else
    {
      int num = 1000;
      Debug.LogError((object) ("LOD level returned as null, setting to " + (object) num));
      this.destroyedModel.GetComponent<ParticleLODShuriken>().InheritedLODLevel = num;
    }
  }

  private AssetRequest GetEffect()
  {
    AsteroidExplosion componentInChildren = this.Root.GetComponentInChildren<AsteroidExplosion>();
    if (!((Object) null == (Object) componentInChildren) && componentInChildren.PrefabName != null)
      return AssetCatalogue.Instance.Request(componentInChildren.PrefabName, false);
    Debug.LogError((object) "Asteroid explosion effect not found");
    return (AssetRequest) null;
  }

  public override void OnDestroyed()
  {
  }

  public override void Remove(RemovingCause removingCause)
  {
    if (removingCause == RemovingCause.Death)
    {
      AssetRequest effect = this.GetEffect();
      if (effect.IsDone && effect.Asset != (Object) null)
      {
        this.destroyedModel = (GameObject) Object.Instantiate(effect.Asset);
        if ((Object) this.destroyedModel == (Object) null)
          return;
        this.destroyedModel.transform.position = this.Position;
        this.destroyedModel.transform.rotation = this.Rotation;
        this.SetDeathExplosionLOD();
        this.destroyedModel.transform.GetComponent<ParticleSystem>().Play();
        for (int index = 0; index <= this.destroyedModel.transform.childCount - 1; ++index)
        {
          Transform child = this.destroyedModel.transform.GetChild(index);
          if (child.name == "asteroidExplosion_rockChunksA" || child.name == "asteroidExplosion_rockChunksB")
          {
            float num1 = this.Root.transform.localScale.x * Mathf.Sin(child.GetComponent<ParticleSystem>().startSize);
            if ((double) num1 < 0.75)
              num1 = 0.75f;
            else if ((double) num1 > 1.0)
              num1 = 1f;
            int count = (int) ((double) this.Root.transform.localScale.x * (double) num1);
            float num2 = this.Root.transform.localScale.x / (float) (count + 2);
            child.GetComponent<ParticleSystem>().startSize = num2;
            child.GetComponent<ParticleSystem>().Emit(count);
          }
        }
      }
    }
    base.Remove(removingCause);
  }

  public override void InitModelScripts()
  {
    Revolving modelScript = this.GetModelScript<Revolving>();
    if ((Object) modelScript != (Object) null)
      modelScript.Velocity = this.rotationSpeed;
    this.GetEffect();
    base.InitModelScripts();
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Position = r.ReadVector3();
  }
}
