﻿// Decompiled with JetBrains decompiler
// Type: RespawnPointRenderer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RespawnPointRenderer : MonoBehaviour
{
  public Color color;

  private void OnDrawGizmos()
  {
    this.Draw();
  }

  private void OnDrawGizmosSelected()
  {
    this.Draw();
    this.DrawWireframe();
    Gizmos.color = Color.cyan;
    for (int index = 0; index < 5; ++index)
      Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * 3000f + this.transform.right * ((float) index - 2.5f) * 100f);
  }

  private void Draw()
  {
    Gizmos.color = this.color;
    Gizmos.DrawCube(this.transform.position, this.transform.localScale);
  }

  private void DrawWireframe()
  {
    Gizmos.color = Color.green;
    Gizmos.DrawWireCube(this.transform.position, this.transform.localScale);
  }
}
