﻿// Decompiled with JetBrains decompiler
// Type: PrettyPrinter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class PrettyPrinter
{
  public static string Format(float f)
  {
    return string.Format("{0:0.0000000}", (object) f);
  }

  public static string Format(Quaternion q)
  {
    return string.Format("({0:0.0000000};{1:0.0000000};{2:0.0000000};{3:0.0000000})", (object) q.x, (object) q.y, (object) q.z, (object) q.w);
  }

  public static string Format(Vector3 v)
  {
    return string.Format("({0:0.0000000};{1:0.0000000};{2:0.0000000})", (object) v.x, (object) v.y, (object) v.z);
  }
}
