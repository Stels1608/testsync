﻿// Decompiled with JetBrains decompiler
// Type: Size
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public struct Size
{
  public int width;
  public int height;

  public Size(int width, int height)
  {
    this.width = width;
    this.height = height;
  }

  public static Size operator +(Size lhs, Size rhs)
  {
    return new Size(lhs.width + rhs.width, lhs.height + rhs.height);
  }

  public static Size operator -(Size lhs, Size rhs)
  {
    return new Size(lhs.width - rhs.width, lhs.height - rhs.height);
  }

  public static Size operator -(Size lhs)
  {
    return new Size(-lhs.width, -lhs.height);
  }

  public Vector2 ToV2()
  {
    return new Vector2((float) this.width, (float) this.height);
  }

  public float2 ToF2()
  {
    return new float2((float) this.width, (float) this.height);
  }

  public void FromV2(Vector2 v)
  {
    this.width = (int) v.x;
    this.height = (int) v.y;
  }
}
