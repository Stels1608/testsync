﻿// Decompiled with JetBrains decompiler
// Type: PlayerInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Common;
using UnityEngine;

public class PlayerInfo : GuiDialog
{
  private Player player;
  private bool avatarCreated;
  private bool shipListCreated;
  private GuiLabel tournamentPointsField;

  public PlayerInfo(Player player)
  {
    this.player = player;
    this.Align = Align.MiddleCenter;
    this.OnClose = (AnonymousDelegate) (() =>
    {
      this.IsRendered = false;
      AvatarView avatarView = this.Find<AvatarView>();
      if (avatarView != null)
        avatarView.DeleteModel();
      Game.UnregisterDialog((IGUIPanel) this);
    });
    Loaders.Load((GuiPanel) this, "GUI/gui_playerInfo");
    this.Find<GuiImage>("background").IsRendered = false;
    this.Find<GuiImage>("ShipOwnedRect").IsRendered = false;
    this.CloseButton.Position = new Vector2(0.0f, -1f);
    this.BringToFront((GuiElementBase) this.CloseButton);
    this.AddChild((GuiElementBase) new Gui.Timer(0.3f));
    this.player.Request(Player.InfoType.ProfileRequest);
    if (Game.Me.TournamentParticipant)
    {
      this.tournamentPointsField = new GuiLabel(BsgoLocalization.Get("%$bgo.InfoJournal.infoJournalPanel.tournamentPoints%", (object) Game.Tournament.TournamentScore), Gui.Options.FontBGM_BT, 16);
      this.AddChild((GuiElementBase) this.tournamentPointsField, this.Find<GuiLabel>("levelLabel").Position + new Vector2(0.0f, 20f));
      this.Find<GuiLabel>("wingLabel").PositionY += 15f;
      this.Find<GuiLabel>("wingPositionLabel").PositionY += 15f;
      this.Find<GuiLabel>("shipTypeLabel").PositionY += 15f;
      this.Find<GuiLabel>("shipNameLabel").PositionY += 15f;
    }
    TournamentProtocol.GetProtocol().OnTournamentParticipant += new TournamentProtocol.TournamentDelegate(this.OnTournamentParticipant);
    this.PeriodicUpdate();
  }

  private void OnTournamentParticipant(bool participating)
  {
    if (participating)
    {
      this.tournamentPointsField = new GuiLabel(BsgoLocalization.Get("%$bgo.InfoJournal.infoJournalPanel.tournamentPoints%", (object) Game.Tournament.TournamentScore), Gui.Options.FontBGM_BT, 16);
      this.AddChild((GuiElementBase) this.tournamentPointsField, this.Find<GuiLabel>("levelLabel").Position + new Vector2(0.0f, 20f));
      this.Find<GuiLabel>("wingLabel").PositionY += 15f;
      this.Find<GuiLabel>("wingPositionLabel").PositionY += 15f;
      this.Find<GuiLabel>("shipTypeLabel").PositionY += 15f;
      this.Find<GuiLabel>("shipNameLabel").PositionY += 15f;
    }
    else
    {
      if (participating || !Game.Me.TournamentParticipant)
        return;
      this.RemoveChild((GuiElementBase) this.tournamentPointsField);
      this.Find<GuiLabel>("wingLabel").PositionY -= 15f;
      this.Find<GuiLabel>("wingPositionLabel").PositionY -= 15f;
      this.Find<GuiLabel>("shipTypeLabel").PositionY -= 15f;
      this.Find<GuiLabel>("shipNameLabel").PositionY -= 15f;
    }
  }

  public override void PeriodicUpdate()
  {
    this.Find<GuiLabel>("nameLabel").Text = this.player.Name;
    this.Find<GuiLabel>("titleLabel").Text = this.player.Title;
    this.Find<GuiLabel>("rankLabel").Text = this.player.Rank;
    this.Find<GuiLabel>("levelLabel").Text = "%$bgo.pilot_log.level%%t%" + (object) this.player.Level;
    bool flag = this.player.HasGuild && this.player.GuildRole != GuildRole.Recruit;
    string str1 = !flag ? "%$bgo.pilot_log.none%" : this.player.guildName;
    string str2 = !flag ? "%$bgo.pilot_log.none%" : Tools.FormatWingRole(this.player.GuildRole);
    this.Find<GuiLabel>("wingLabel").Text = "%$bgo.gui_playerInfo.wingLabel% %t%" + Tools.SubStr(str1, 20);
    this.Find<GuiLabel>("wingPositionLabel").Text = "%$bgo.gui_playerInfo.wingPositionLabel% %t%" + str2;
    this.Find<GuiLabel>("shipTypeLabel").Text = "%$bgo.gui_playerInfo.shipLabel% " + this.player.PlayerShip.GUICard.Name;
    this.Find<GuiLabel>("shipNameLabel").Text = "%$bgo.InfoJournal.infoJournalPanel.shipNameLabel% %t%" + this.player.ShipName;
    if (Game.Me.TournamentParticipant)
      this.tournamentPointsField.Text = BsgoLocalization.Get("%$bgo.InfoJournal.infoJournalPanel.tournamentPoints%", (object) Game.Tournament.TournamentScore);
    this.CheckAvatar();
    this.CheckShipList();
    base.PeriodicUpdate();
  }

  public void CheckAvatar()
  {
    if (this.avatarCreated || this.player.AvatarDesc == null)
      return;
    GuiImage guiImage = this.Find<GuiImage>("avatar");
    AvatarView avatarView = new AvatarView((AvatarItems) this.player.AvatarDesc);
    guiImage.IsRendered = true;
    avatarView.Size = guiImage.Size;
    avatarView.Position = guiImage.Position;
    this.AddChild((GuiElementBase) avatarView);
    this.avatarCreated = true;
  }

  public void CheckShipList()
  {
    if (this.shipListCreated || this.player.ShipCards == null)
      return;
    GuiImage guiImage = this.Find<GuiImage>("ShipOwnedRect");
    guiImage.IsRendered = true;
    guiImage.PositionY -= 40f;
    guiImage.SizeY *= 1.85f;
    Log.Add(this.player.ShipCards != null ? this.player.ShipCards.Length.ToString() : "Null");
    PlayerInfo.ShipsAreaPlayer shipsAreaPlayer = new PlayerInfo.ShipsAreaPlayer(this.player.Faction, this.player.ShipCards, this.player.ShipCards[0]);
    shipsAreaPlayer.isMe = false;
    shipsAreaPlayer.Position = guiImage.Position;
    shipsAreaPlayer.Size = guiImage.Size;
    this.RemoveChild((GuiElementBase) guiImage);
    this.AddChild((GuiElementBase) shipsAreaPlayer);
    this.shipListCreated = true;
  }

  public class ShipsAreaPlayer : ShipsArea
  {
    private readonly Faction faction;
    private readonly ShipCard[] ownedCards;
    private readonly ShipCard activeShip;

    public ShipsAreaPlayer(Faction faction, ShipCard[] ownedShips, ShipCard activeShip)
    {
      this.faction = faction;
      this.ownedCards = ownedShips;
      this.activeShip = activeShip;
    }

    protected override Faction GetFaction()
    {
      return this.faction;
    }

    protected override ShipCard[] GetOwnedCards()
    {
      return this.ownedCards;
    }

    protected override ShipCard GetActiveShip()
    {
      return this.activeShip;
    }
  }
}
