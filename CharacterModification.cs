﻿// Decompiled with JetBrains decompiler
// Type: CharacterModification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CharacterModification : AvatarWindow
{
  public Text headerLabel;
  public Text customizeLabel;
  public Text genderLabel;
  public RectTransform mainPanel;
  public GameObject view;
  public Button acceptButton;
  public Button cancelButton;
  private AvatarItems avatarDesc;

  protected override void Start()
  {
    base.Start();
    this.acceptButton.onClick.AddListener(new UnityAction(this.AcceptCharacterModification));
    this.cancelButton.onClick.AddListener((UnityAction) (() => FacadeFactory.GetInstance().SendMessage(Message.CancelAvatarChange)));
  }

  protected override void Localize()
  {
    if (!this.view.activeInHierarchy)
      return;
    this.headerLabel.text = BsgoLocalization.Get("bgo.character_menu.modification.title").ToUpperInvariant();
    this.genderLabel.text = BsgoLocalization.Get("bgo.gui_avatar_human.choose_gender").ToUpperInvariant();
    this.customizeLabel.text = BsgoLocalization.Get(this.faction != Faction.Colonial ? "bgo.gui_avatar_cylon.customize" : "bgo.gui_avatar_human.customize").ToUpperInvariant();
    this.acceptButton.GetComponentInChildren<Text>().text = BsgoLocalization.Get("bgo.ui.options.key.accept");
    this.cancelButton.GetComponentInChildren<Text>().text = BsgoLocalization.Get("bgo.ui.options.key.cancel");
  }

  [DebuggerHidden]
  protected override IEnumerator Resize()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CharacterModification.\u003CResize\u003Ec__Iterator1F() { \u003C\u003Ef__this = this };
  }

  protected override void OnAvatarLoaded()
  {
    base.OnAvatarLoaded();
    AvatarSelector.ClearAvatar(this.avatar);
    this.avatar.obj.SetLayerRecursively(LayerMask.NameToLayer("Avatar"));
    FacadeFactory.GetInstance().SendMessage(Message.AvatarLoaded);
  }

  public void SetAvatarDesc(AvatarItems avatarDesc)
  {
    this.avatarDesc = avatarDesc;
  }

  protected override void Init()
  {
    this.avatar = AvatarSelector.Create(this.avatarDesc);
    this.avatar.loadedCallback = new System.Action(this.OnAvatarLoaded);
  }

  public void Show()
  {
    this.view.SetActive(true);
    this.faction = Game.Me.Faction;
    this.avatar.Destroy();
    base.Init();
  }

  public void Hide()
  {
    this.view.SetActive(false);
    this.avatar.Destroy();
    this.Init();
  }

  private void OnDestroy()
  {
    this.avatar.Destroy();
  }

  public void AcceptCharacterModification()
  {
    AvatarDescription avatarDescription = this.avatar.GetAvatarDescription(this.faction);
    this.SetAvatarDesc((AvatarItems) avatarDescription);
    FacadeFactory.GetInstance().SendMessage(Message.ConfirmAvatarChange, (object) avatarDescription);
    PlayerProtocol.GetProtocol().ChangeAvatar(avatarDescription);
  }
}
