﻿// Decompiled with JetBrains decompiler
// Type: MiningShipUnderAttackNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class MiningShipUnderAttackNotification : OnScreenNotification
{
  private readonly string message;

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.MiningShipUnderAttack;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Negative;
    }
  }

  public override string TextMessage
  {
    get
    {
      return this.message;
    }
  }

  public override bool Show
  {
    get
    {
      if (OnScreenNotification.ShowMiningShipMessages)
        return !string.IsNullOrEmpty(this.message);
      return false;
    }
  }

  public MiningShipUnderAttackNotification(GUICard sectorCard, byte damageType, bool isYours)
  {
    this.message = string.Empty;
    if (isYours)
    {
      switch (damageType)
      {
        case 2:
          this.message = BsgoLocalization.Get("%$bgo.notif.mining_attack%", (object) sectorCard.Name);
          break;
        case 3:
          this.message = BsgoLocalization.Get("%$bgo.notif.mining_damage%", (object) sectorCard.Name);
          break;
        case 4:
          this.message = BsgoLocalization.Get("%$bgo.notif.mining_driven_off%", (object) sectorCard.Name);
          break;
      }
      FacadeFactory.GetInstance().SendMessage(Message.CombatLogOuch, (object) BsgoLocalization.Get("%$bgo.notif.mining_attack_simple%"));
    }
    else if ((int) damageType == 4)
      this.message = BsgoLocalization.Get("%$bgo.notif.mining_destroyed%", (object) sectorCard.Name);
    else
      this.message = BsgoLocalization.Get("%$bgo.notif.mining_attack_1%", (object) sectorCard.Name);
  }
}
