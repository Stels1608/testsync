﻿// Decompiled with JetBrains decompiler
// Type: GUIManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GUIManager
{
  public TextureCache TextureCache = new TextureCache();
  public ResourceLoader ResourceLoader = new ResourceLoader();
  private bool isRendered = true;
  private static GuiMediator guiMediator;
  private static int _mouseOverAnyUiElementInTick;
  private static bool _mouseOverAnyUiElement;
  public static bool DebugProfile;
  public List<IGUIPanel> panels;
  public List<IGUIRenderable> DialogBlockers;
  private GUIScreenResolution resolution;
  private float timeResolutionChanged;

  public static bool ModalNguiWindowOpened
  {
    get
    {
      if (GUIManager.guiMediator == null)
        GUIManager.guiMediator = FacadeFactory.GetInstance().FetchView("GuiMediator") as GuiMediator;
      if (GUIManager.guiMediator != null)
        return GUIManager.guiMediator.Manager.IsDrawn();
      return false;
    }
  }

  public static bool MouseOverAnyOldGuiElement
  {
    get
    {
      return GUIManager._mouseOverAnyUiElement;
    }
    set
    {
      if (value)
        GUIManager._mouseOverAnyUiElementInTick = Time.frameCount;
      GUIManager._mouseOverAnyUiElement = value;
    }
  }

  public static bool MouseOverAnyOldGuiElementThisFrame
  {
    get
    {
      if (GUIManager.guiMediator == null)
        GUIManager.guiMediator = FacadeFactory.GetInstance().FetchView("GuiMediator") as GuiMediator;
      if (GUIManager._mouseOverAnyUiElementInTick == Time.frameCount)
        return true;
      if (GUIManager.guiMediator != null)
        return GUIManager.guiMediator.Manager.IsDrawn();
      return false;
    }
  }

  public static bool MouseOverNguiElement
  {
    get
    {
      return (UnityEngine.Object) UICamera.hoveredObject != (UnityEngine.Object) null;
    }
  }

  public static bool MouseOverUguiElement
  {
    get
    {
      return EventSystem.current.IsPointerOverGameObject();
    }
  }

  public static bool MouseOverOldGuiElement
  {
    get
    {
      if (!GUIManager.MouseOverAnyOldGuiElementThisFrame)
        return GUIManager.MouseOverNguiElement;
      return true;
    }
  }

  public static bool MouseOverAnyGuiElement
  {
    get
    {
      if (!GUIManager.MouseOverAnyOldGuiElementThisFrame && !GUIManager.MouseOverNguiElement)
        return GUIManager.MouseOverUguiElement;
      return true;
    }
  }

  public bool IsRendered
  {
    get
    {
      return this.isRendered;
    }
    set
    {
      this.isRendered = value;
    }
  }

  public SystemButtonWindow SystemButtons
  {
    get
    {
      return ((SystemButtonMediator) FacadeFactory.GetInstance().FetchView("SystemButtonMediator")).SystemButtons;
    }
  }

  public CameraModeWindow CameraModeWindow
  {
    get
    {
      return ((SpaceCameraMediator) FacadeFactory.GetInstance().FetchView("SpaceCameraMediator")).CameraModeWindow;
    }
  }

  public GUIScreenResolution Resolution
  {
    get
    {
      return this.resolution;
    }
  }

  public GUIManager()
  {
    this.panels = new List<IGUIPanel>();
    this.DialogBlockers = new List<IGUIRenderable>();
    this.resolution = new GUIScreenResolution(Screen.width, Screen.height);
  }

  private void HandleResolutionChanged()
  {
    if ((double) Time.time - (double) this.timeResolutionChanged <= 0.5)
      return;
    GUIScreenResolution screenResolution = new GUIScreenResolution(Screen.width, Screen.height);
    if (this.resolution.width == screenResolution.width && this.resolution.height == screenResolution.height)
      return;
    this.resolution = screenResolution;
    this.NotifyAboutResolutionChanged();
    FacadeFactory.GetInstance().SendMessage(Message.ScreenResolutionChanged, (object) screenResolution);
    this.timeResolutionChanged = Time.time;
  }

  public void NotifyAboutResolutionChanged()
  {
    using (List<IGUIPanel>.Enumerator enumerator = this.panels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.RecalculateAbsCoords();
    }
    this.ToggleDefaultResolution();
  }

  public void ToggleDefaultResolution()
  {
    GUIScaleState newScaleState = !GUIScreenResolution.IsDefault() ? GUIScaleState.Original : GUIScaleState.Downsized;
    GUIChatNew guiChatNew = this.Find<GUIChatNew>();
    if (guiChatNew == null)
      return;
    guiChatNew.Resize(newScaleState);
  }

  public void Update()
  {
    this.HandleResolutionChanged();
    for (int index = 0; index < this.panels.Count; ++index)
    {
      IGUIPanel guiPanel = this.panels[index];
      if (!GUIManager.DebugProfile)
        ;
      if (guiPanel.IsUpdated)
        guiPanel.Update();
      if (!GUIManager.DebugProfile)
        ;
    }
  }

  public void Draw()
  {
    GuiElementBase.DrawElement.StartFrame();
    using (List<IGUIPanel>.Enumerator enumerator = this.panels.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IGUIPanel current = enumerator.Current;
        if (!GUIManager.DebugProfile)
          ;
        if (current.IsRendered)
          current.Draw();
        if (!GUIManager.DebugProfile)
          ;
      }
    }
    Game.TooltipManager.Draw();
    GUIManager.MouseOverAnyOldGuiElement |= GUIUtility.hotControl != 0;
  }

  public bool IsDialogBlocked()
  {
    for (int index = 0; index < this.DialogBlockers.Count; ++index)
    {
      if (this.DialogBlockers[index].IsRendered)
        return true;
    }
    return false;
  }

  public T Find<T>() where T : IGUIPanel
  {
    using (List<IGUIPanel>.Enumerator enumerator = this.panels.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IGUIPanel current = enumerator.Current;
        if (current is T)
          return (T) current;
      }
    }
    return default (T);
  }

  public IList<T> FindAll<T>() where T : IGUIPanel
  {
    IList<T> objList = (IList<T>) new List<T>();
    using (List<IGUIPanel>.Enumerator enumerator = this.panels.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IGUIPanel current = enumerator.Current;
        if (current is T)
          objList.Add((T) current);
      }
    }
    return objList;
  }

  public T FindStraight<T>() where T : IGUIPanel
  {
    using (List<IGUIPanel>.Enumerator enumerator = this.panels.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IGUIPanel current = enumerator.Current;
        if (current.GetType() == typeof (T))
          return (T) current;
      }
    }
    return default (T);
  }

  public int FindIndex<T>() where T : IGUIPanel
  {
    return this.panels.FindIndex((Predicate<IGUIPanel>) (panel => panel is T));
  }

  public void AddPanel(IGUIPanel panel)
  {
    if (panel == null)
      throw new ArgumentException("Panel added to GUIManager is NULL");
    this.panels.Add(panel);
  }

  public void AddPanel(IGUIPanel panel, int index)
  {
    if (index < 0)
      index = 0;
    if (index > this.panels.Count)
      index = this.panels.Count;
    this.panels.Insert(index, panel);
  }

  public void RemovePanel(IGUIPanel panel)
  {
    this.panels.Remove(panel);
  }

  public void LateUpdate()
  {
    GUIManager.MouseOverAnyOldGuiElement = false;
  }

  public bool IsBigWindowShown()
  {
    using (List<IGUIPanel>.Enumerator enumerator = this.panels.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IGUIPanel current = enumerator.Current;
        if (current.IsRendered && current.IsBigWindow)
          return true;
      }
    }
    if (GUIManager.guiMediator == null)
      GUIManager.guiMediator = FacadeFactory.GetInstance().FetchView("GuiMediator") as GuiMediator;
    return GUIManager.guiMediator != null && GUIManager.guiMediator.Manager.IsDrawn();
  }
}
