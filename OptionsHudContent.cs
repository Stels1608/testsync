﻿// Decompiled with JetBrains decompiler
// Type: OptionsHudContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

public class OptionsHudContent : OptionsContent
{
  private SettingsHudIndicators hudIndicators;
  private SettingsHudMisc hudMisc;
  private SettingsHudLayout layout;
  private InputBindingFooterButtonBar footerButtonBar;

  public void Awake()
  {
    this.layout = new GameObject("layout")
    {
      transform = {
        parent = this.transform,
        localPosition = Vector3.zero,
        localScale = Vector3.one
      },
      layer = this.gameObject.layer
    }.AddComponent<SettingsHudLayout>();
    this.InitContent();
  }

  public override void UpdateSettings(UserSettings settings)
  {
    base.UpdateSettings(settings);
    this.hudIndicators.Init(settings);
    this.hudMisc.Init(settings);
  }

  private void InitContent()
  {
    GameObject gameObject1 = NGUITools.AddChild(this.layout.ContentArea[0], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/hud/SettingsHudIndicators"));
    if ((UnityEngine.Object) gameObject1 == (UnityEngine.Object) null)
      throw new ArgumentNullException("Settings HudIndicators object failed to load");
    this.hudIndicators = gameObject1.GetComponent<SettingsHudIndicators>();
    if ((UnityEngine.Object) this.hudIndicators == (UnityEngine.Object) null)
      Debug.LogError((object) "General Hud was not found");
    this.hudIndicators.OnSettingChanged = new OnSettingChanged(((OptionsContent) this).ChangeSetting);
    GameObject gameObject2 = NGUITools.AddChild(this.layout.ContentArea[1], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/options/hud/SettingsHudMisc"));
    if ((UnityEngine.Object) gameObject2 == (UnityEngine.Object) null)
      throw new ArgumentNullException("Settings HudMisc object failed to load");
    this.hudMisc = gameObject2.GetComponent<SettingsHudMisc>();
    if ((UnityEngine.Object) this.hudMisc == (UnityEngine.Object) null)
      Debug.LogError((object) "General Hud was not found");
    this.hudMisc.OnSettingChanged = new OnSettingChanged(((OptionsContent) this).ChangeSetting);
    GameObject gameObject3 = NGUITools.AddChild(this.layout.ContentArea[2], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/keybinding/InputBindingFooterButtonBar"));
    if ((UnityEngine.Object) gameObject3 == (UnityEngine.Object) null)
      throw new ArgumentNullException("InputBindingFooterButtonBar");
    this.footerButtonBar = gameObject3.GetComponent<InputBindingFooterButtonBar>();
    this.footerButtonBar.SetButtonDelegates(new AnonymousDelegate(((OptionsContent) this).Undo), new AnonymousDelegate(((OptionsContent) this).Apply), new AnonymousDelegate(this.ResetDialog));
  }

  private void ResetDialog()
  {
    DialogBoxFactoryNgui.CreateResetSettingsDialogBox(new AnonymousDelegate(((OptionsContent) this).RestoreDefaultSettings));
  }

  public override void RestoreDefaultSettings()
  {
    Debug.Log((object) "Restoring default settings");
    this.hudIndicators.RestoreDefaultSettings(this.settings);
    this.hudMisc.RestoreDefaultSettings(this.settings);
    this.UpdateSettings(this.settings);
    this.Apply();
  }
}
