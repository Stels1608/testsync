﻿// Decompiled with JetBrains decompiler
// Type: GUIShopPaperDollSlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using Gui.CharacterStatus.InflightShop;
using Gui.Tooltips;
using Paperdolls.PaperdollLayoutBig;
using System;
using UnityEngine;

public class GUIShopPaperDollSlot : GUIPanel
{
  private readonly GUIImageNew backgroundOpenImage;
  private readonly GUIImageNew backgroundClosedImage;
  private readonly GUIImageNew itemImage;
  private readonly GUIImageNew discountHalo;
  private readonly GUILabelNew slottypeLabel;
  private readonly GUIButtonNew upgradeButton;
  private readonly AtlasCache atlasCache;
  private ShipSlot cashedSlot;
  private readonly Texture2D UpgradeBtnNormalTex;
  private readonly Texture2D UpgradeBtnOverTex;
  private readonly Texture2D UpgradeBtnPressedTex;
  private readonly Texture2D UpgradeBtnOnSaleNormalTex;
  private readonly Texture2D UpgradeBtnOnSaleOverTex;
  private readonly Texture2D UpgradeBtnOnSalePressedTex;
  private bool textResized;
  private bool isClosed;
  private GUIShopPaperDollSlot.dHandler handlerUpgradeButton;

  public bool IsClosed
  {
    get
    {
      return this.isClosed;
    }
    set
    {
      if (value)
      {
        this.backgroundClosedImage.IsRendered = true;
        this.backgroundOpenImage.IsRendered = false;
      }
      else
      {
        this.backgroundClosedImage.IsRendered = false;
        this.backgroundOpenImage.IsRendered = true;
      }
    }
  }

  public GUIShopPaperDollSlot.dHandler HandlerUpgradeButton
  {
    set
    {
      this.handlerUpgradeButton = value;
    }
  }

  public GUIShopPaperDollSlot(SmartRect parent)
    : base(parent)
  {
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/EquipBuyPanel/gui_pdslot_layout");
    this.backgroundOpenImage = (jwindowDescription["background_image"] as JImage).CreateGUIImageNew(this.root);
    this.backgroundOpenImage.Position = new float2(0.0f, 0.0f);
    this.root.Width = this.backgroundOpenImage.Rect.width;
    this.root.Height = this.backgroundOpenImage.Rect.height;
    this.AddPanel((GUIPanel) this.backgroundOpenImage);
    this.backgroundClosedImage = (jwindowDescription["background_closed_image"] as JImage).CreateGUIImageNew(this.root);
    this.backgroundClosedImage.IsRendered = false;
    this.backgroundClosedImage.Position = new float2(0.0f, 0.0f);
    this.AddPanel((GUIPanel) this.backgroundClosedImage);
    this.itemImage = (jwindowDescription["item_image"] as JImage).CreateGUIImageNew(this.root);
    this.itemImage.IsRendered = false;
    this.AddPanel((GUIPanel) this.itemImage);
    this.discountHalo = (jwindowDescription["discountHalo"] as JImage).CreateGUIImageNew(this.root);
    this.discountHalo.IsRendered = false;
    this.AddPanel((GUIPanel) this.discountHalo);
    this.slottypeLabel = (jwindowDescription["slottype_label"] as JLabel).CreateGUILabelNew(this.root);
    this.slottypeLabel.IsRendered = false;
    this.AddPanel((GUIPanel) this.slottypeLabel);
    this.upgradeButton = (jwindowDescription["upgrade_button"] as JButton).CreateGUIButtonNew(this.root);
    this.upgradeButton.Handler = new AnonymousDelegate(this.OnUpgardeButton);
    this.upgradeButton.IsRendered = false;
    this.AddPanel((GUIPanel) this.upgradeButton);
    this.atlasCache = new AtlasCache(new float2(40f, 35f));
    this.UpgradeBtnNormalTex = this.upgradeButton.NormalTexture;
    this.UpgradeBtnOverTex = this.upgradeButton.OverTexture;
    this.UpgradeBtnPressedTex = this.upgradeButton.PressedTexture;
    this.UpgradeBtnOnSaleNormalTex = (Texture2D) ResourceLoader.Load("GUI/GalaxyMap/smallbutton_onsale");
    this.UpgradeBtnOnSaleOverTex = (Texture2D) ResourceLoader.Load("GUI/GalaxyMap/smallbutton_onsale_over");
    this.UpgradeBtnOnSalePressedTex = (Texture2D) ResourceLoader.Load("GUI/GalaxyMap/smallbutton_onsale_click");
  }

  public override bool OnShowTooltip(float2 position)
  {
    if (Game.GUIManager.Find<GUIShopConfirmWindow>() != null)
      return false;
    if (this.cashedSlot != null && this.cashedSlot.System == null)
      this.SetTooltip(GUIShopPaperDollSlot.GetTooltipText(this.cashedSlot));
    else
      this.advancedTooltip = (GuiAdvancedTooltipBase) null;
    return base.OnShowTooltip(position);
  }

  public void UpdateData(ShipSlot slot)
  {
    this.cashedSlot = slot;
    if (slot.System != null)
      this.discountHalo.IsRendered = slot.System.IsAnyUpgradeLevelOnSale();
    if (slot.System != null && (bool) slot.System.IsLoaded)
    {
      AtlasEntry cachedEntryBy = this.atlasCache.GetCachedEntryBy(slot.System.ItemGUICard.GUIAtlasTexturePath, (int) slot.System.ItemGUICard.FrameIndex);
      this.itemImage.IsRendered = true;
      this.itemImage.Texture = cachedEntryBy.Texture;
      this.itemImage.InnerRect = cachedEntryBy.FrameRect;
      this.upgradeButton.IsRendered = true;
      this.upgradeButton.TextLabel.Text = (int) slot.System.Card.Level == (int) slot.System.Card.MaxLevel || !slot.System.Card.UserUpgradeable ? "%$bgo.etc.slot_info%" : "%$bgo.shop.inv_upgradebutton%";
      this.UpgradeBtnIsSaleBtn(slot.System.IsAnyUpgradeLevelOnSale());
    }
    else
    {
      this.itemImage.IsRendered = false;
      this.upgradeButton.IsRendered = false;
    }
    if ((bool) slot.IsLoaded && slot.System == null)
    {
      this.slottypeLabel.Text = PaperdollSlot.GetEmptySlotFirstLetter(slot.Card.SystemType);
      if (!this.textResized && TextUtility.CalcTextSize(Tools.ParseMessage(this.slottypeLabel.Text), this.slottypeLabel.Font, this.slottypeLabel.FontSize).width > 30)
      {
        this.slottypeLabel.FontSize = 16;
        if (TextUtility.CalcTextSize(Tools.ParseMessage(this.slottypeLabel.Text), this.slottypeLabel.Font, 16).width > 30)
          this.slottypeLabel.FontSize = 12;
        this.textResized = true;
      }
      this.slottypeLabel.IsRendered = true;
    }
    else
      this.slottypeLabel.IsRendered = false;
    if (!(bool) slot.IsLoaded)
      return;
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.Position = Game.Me.ActiveShip.Card.PaperdollLayoutBig.GetSlotLayouts((int) Game.Me.ActiveShip.Card.Level).Find(new Predicate<SlotLayout>(new GUIShopPaperDollSlot.\u003CUpdateData\u003Ec__AnonStoreyBA()
    {
      slotId = slot.Card.SlotId
    }.\u003C\u003Em__199)).Position;
    this.RecalculateAbsCoords();
  }

  private void UpgradeBtnIsSaleBtn(bool paintBtnOrange)
  {
    if (paintBtnOrange)
    {
      this.upgradeButton.NormalTexture = this.UpgradeBtnOnSaleNormalTex;
      this.upgradeButton.OverTexture = this.UpgradeBtnOnSaleOverTex;
      this.upgradeButton.PressedTexture = this.UpgradeBtnOnSalePressedTex;
    }
    else
    {
      this.upgradeButton.NormalTexture = this.UpgradeBtnNormalTex;
      this.upgradeButton.OverTexture = this.UpgradeBtnOverTex;
      this.upgradeButton.PressedTexture = this.UpgradeBtnPressedTex;
    }
  }

  public static string GetTooltipText(ShipSlot slot)
  {
    if (slot != null && slot.Card != null)
      return "%$bgo.shop." + (object) slot.Card.SystemType + "%";
    return "%$bgo.shop.slot%";
  }

  protected void OnUpgardeButton()
  {
    if (this.handlerUpgradeButton == null || this.cashedSlot == null || this.cashedSlot.System == null)
      return;
    this.handlerUpgradeButton(this.cashedSlot);
  }

  public delegate void dHandler(ShipSlot slot);
}
