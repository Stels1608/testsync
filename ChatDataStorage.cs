﻿// Decompiled with JetBrains decompiler
// Type: ChatDataStorage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Chat;
using Gui;
using System.Collections.Generic;
using UnityEngine;

public class ChatDataStorage
{
  public readonly ChatDataStorage.Log Global = new ChatDataStorage.Log();
  public readonly ChatDataStorage.Log Local = new ChatDataStorage.Log();
  public readonly ChatDataStorage.Log GlobalAndLocal = new ChatDataStorage.Log();
  public readonly ChatDataStorage.Log Squadron = new ChatDataStorage.Log();
  public readonly ChatDataStorage.Log Wing = new ChatDataStorage.Log();
  public readonly ChatDataStorage.Log CombatLog = new ChatDataStorage.Log();
  private readonly List<ChatDataStorage.Log> allChannels = new List<ChatDataStorage.Log>();
  private readonly List<ChatDataStorage.Log> mainChannels = new List<ChatDataStorage.Log>();

  public ChatDataStorage()
  {
    this.allChannels.Add(this.Global);
    this.allChannels.Add(this.GlobalAndLocal);
    this.allChannels.Add(this.Local);
    this.allChannels.Add(this.Squadron);
    this.allChannels.Add(this.Wing);
    this.allChannels.Add(this.CombatLog);
    this.mainChannels.Add(this.Global);
    this.mainChannels.Add(this.Local);
    this.mainChannels.Add(this.GlobalAndLocal);
    this.mainChannels.Add(this.Squadron);
    this.mainChannels.Add(this.Wing);
  }

  public void Greetings(string text)
  {
    ChatDataStorage.Message message = new ChatDataStorage.Message(text);
    using (List<ChatDataStorage.Log>.Enumerator enumerator = this.mainChannels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Add(message, false);
    }
  }

  public void Help(string text)
  {
    using (List<ChatDataStorage.Log>.Enumerator enumerator = this.mainChannels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Add(new ChatDataStorage.Message(text, ColorScheme.Nice), false);
    }
  }

  public void Danger(string text)
  {
    ChatDataStorage.SystemMessage systemMessage = new ChatDataStorage.SystemMessage(text, ColorScheme.Danger);
    using (List<ChatDataStorage.Log>.Enumerator enumerator = this.allChannels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Add((ChatDataStorage.Message) systemMessage, false);
    }
  }

  public void DangerCombatLog(string text)
  {
    this.CombatLog.Add(new ChatDataStorage.Message(text, ColorScheme.Danger), true);
  }

  public void Ouch(string text)
  {
    ChatDataStorage.TextMessage textMessage = (ChatDataStorage.TextMessage) new ChatDataStorage.SystemMessage(text, ColorScheme.Ouch);
    using (List<ChatDataStorage.Log>.Enumerator enumerator = this.allChannels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Add((ChatDataStorage.Message) textMessage, false);
    }
  }

  public void OuchCombatLog(string text)
  {
    this.CombatLog.Add(new ChatDataStorage.Message(text, ColorScheme.Ouch), true);
  }

  public void NiceCombatLog(string text)
  {
    this.CombatLog.Add(new ChatDataStorage.Message(text, ColorScheme.Nice), true);
  }

  public void OkCombatLog(string text)
  {
    this.CombatLog.Add(new ChatDataStorage.Message(text, ColorScheme.Ok), true);
  }

  public void BeWareCombatLog(string text)
  {
    this.CombatLog.Add(new ChatDataStorage.Message(text, ColorScheme.BeWare), true);
  }

  public void SystemSays(string text)
  {
    ChatDataStorage.SystemMessage systemMessage = new ChatDataStorage.SystemMessage(text);
    using (List<ChatDataStorage.Log>.Enumerator enumerator = this.allChannels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Add((ChatDataStorage.Message) systemMessage, false);
    }
  }

  public void SystemSaysSquadron(string text)
  {
    this.Squadron.Add((ChatDataStorage.Message) new ChatDataStorage.SystemMessage(text), false);
  }

  public void SystemSaysWing(string text)
  {
    this.Wing.Add((ChatDataStorage.Message) new ChatDataStorage.SystemMessage(text), false);
  }

  public void WhisperedToYou(string who, string what)
  {
    ChatDataStorage.ExtendedTextMessage extendedTextMessage = new ChatDataStorage.ExtendedTextMessage(who, what, Commands.Whisper, BsgoLocalization.Get("%$bgo.chat.whispers_suffix%"), ColorScheme.WhisperNameColor, ColorScheme.WhisperTextColor);
    using (List<ChatDataStorage.Log>.Enumerator enumerator = this.mainChannels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Add((ChatDataStorage.Message) extendedTextMessage, false);
    }
  }

  public void Whisper(string addressee, string what, string receiver)
  {
    ChatDataStorage.ExtendedTextMessage extendedTextMessage = new ChatDataStorage.ExtendedTextMessage(addressee, what, Commands.Whisper, BsgoLocalization.Get("%$bgo.chat.whispers_suffix%") + "  |" + receiver + "|", ColorScheme.WhisperNameColor, ColorScheme.WhisperTextColor);
    using (List<ChatDataStorage.Log>.Enumerator enumerator = this.mainChannels.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Add((ChatDataStorage.Message) extendedTextMessage, false);
    }
  }

  public void Proto(ChannelName channel, string who, string what)
  {
    if (channel.IsGlobal)
    {
      Color nameColor = ColorScheme.DefaultNameColor;
      Color textColor = ColorScheme.DefaultTextColor;
      if (channel == ChannelName.FleetGlobal)
      {
        nameColor = ColorScheme.FleetName(true);
        textColor = ColorScheme.FleetText(true);
        if (Game.Me.Name == who)
          CommunityProtocol.GetProtocol().RequestCanSendFleetMessage();
      }
      else if (channel == ChannelName.SystemGlobal)
      {
        nameColor = ColorScheme.SystemName(true);
        textColor = ColorScheme.SystemText(true);
      }
      else if (channel == ChannelName.OpenGlobal)
      {
        if (string.IsNullOrEmpty(what) || what.Length < 3)
          return;
        string str = what.Substring(0, 3);
        Faction faction;
        if (str.StartsWith("(") && str.EndsWith(")"))
        {
          faction = (Faction) int.Parse(str.Substring(1, 1));
          what = what.Remove(0, 3);
        }
        else
          faction = Faction.Neutral;
        nameColor = ColorScheme.OpenName(faction);
      }
      ChatDataStorage.ExtendedTextMessage extendedTextMessage = new ChatDataStorage.ExtendedTextMessage(who, what, Commands.Get(channel), channel.Name, nameColor, textColor);
      this.Global.Add((ChatDataStorage.Message) extendedTextMessage, true);
      this.GlobalAndLocal.Add((ChatDataStorage.Message) extendedTextMessage, true);
    }
    else if (channel.IsLocal)
    {
      Color nameColor = ColorScheme.DefaultNameColor;
      Color textColor = ColorScheme.DefaultTextColor;
      if (channel == ChannelName.FleetLocal)
      {
        nameColor = ColorScheme.FleetName(false);
        textColor = ColorScheme.FleetText(false);
        if (Game.Me.Name == who)
          CommunityProtocol.GetProtocol().RequestCanSendFleetMessage();
      }
      else if (channel == ChannelName.SystemLocal)
      {
        nameColor = ColorScheme.SystemName(false);
        textColor = ColorScheme.SystemText(false);
      }
      else if (channel == ChannelName.OpenLocal)
      {
        Faction faction1 = Game.Me.Faction;
        string str = what.Substring(0, 3);
        Faction faction2;
        if (str.StartsWith("(") && str.EndsWith(")"))
        {
          faction2 = (Faction) int.Parse(str.Substring(1, 1));
          what = what.Remove(0, 3);
        }
        else
          faction2 = Faction.Neutral;
        nameColor = ColorScheme.OpenName(faction2);
      }
      ChatDataStorage.ExtendedTextMessage extendedTextMessage = new ChatDataStorage.ExtendedTextMessage(who, what, Commands.Get(channel), channel.Name, nameColor, textColor);
      this.Local.Add((ChatDataStorage.Message) extendedTextMessage, true);
      this.GlobalAndLocal.Add((ChatDataStorage.Message) extendedTextMessage, true);
    }
    else if (channel == ChannelName.Squadron)
      this.Squadron.Add((ChatDataStorage.Message) new ChatDataStorage.TextMessage(who, what), true);
    else if (channel == ChannelName.Wing)
    {
      this.Wing.Add((ChatDataStorage.Message) new ChatDataStorage.TextMessage(who, what), true);
    }
    else
    {
      if (channel != ChannelName.Officer)
        return;
      this.Wing.Add((ChatDataStorage.Message) new ChatDataStorage.ExtendedTextMessage(who, what, Commands.Get(channel), channel.Name, ColorScheme.WingNameColor, ColorScheme.WingTextColor), true);
    }
  }

  public void Mod(ChannelName channel, string who, string what)
  {
    ChatDataStorage.ModeratorMessage moderatorMessage = new ChatDataStorage.ModeratorMessage(who, what);
    if (channel.IsGlobal)
    {
      this.Global.Add((ChatDataStorage.Message) moderatorMessage, true);
      this.GlobalAndLocal.Add((ChatDataStorage.Message) moderatorMessage, true);
    }
    else if (channel.IsLocal)
    {
      this.Local.Add((ChatDataStorage.Message) moderatorMessage, true);
      this.GlobalAndLocal.Add((ChatDataStorage.Message) moderatorMessage, true);
    }
    else if (channel == ChannelName.Squadron)
      this.Squadron.Add((ChatDataStorage.Message) moderatorMessage, true);
    else if (channel == ChannelName.Wing)
    {
      this.Wing.Add((ChatDataStorage.Message) moderatorMessage, true);
    }
    else
    {
      if (channel != ChannelName.Officer)
        return;
      this.Wing.Add((ChatDataStorage.Message) new ChatDataStorage.ExtendedTextMessage(who, what, Commands.Get(channel), channel.Name, ColorScheme.WingNameColor, ColorScheme.WingTextColor), true);
    }
  }

  public class Message
  {
    public string text;
    public Color textColor;

    public Message(string text)
    {
      this.text = text;
      this.textColor = ColorScheme.Ok;
    }

    public Message(string text, Color color)
      : this(text)
    {
      this.textColor = color;
    }
  }

  public class TextMessage : ChatDataStorage.Message
  {
    public string name;
    public Color nameColor;

    public TextMessage(string userName, string text)
      : this(userName, text, ColorScheme.DefaultNameColor, ColorScheme.DefaultTextColor)
    {
    }

    public TextMessage(string userName, string text, Color nameColor, Color textColor)
      : base(text, textColor)
    {
      this.name = userName;
      this.nameColor = nameColor;
    }
  }

  public class SystemMessage : ChatDataStorage.TextMessage
  {
    public SystemMessage(string text)
      : base("system", text)
    {
      this.nameColor = this.textColor;
    }

    public SystemMessage(string text, Color textColor)
      : this(text)
    {
      this.textColor = textColor;
    }
  }

  public class ModeratorMessage : ChatDataStorage.TextMessage
  {
    public ModeratorMessage(string userName, string text)
      : base(userName, text)
    {
      this.textColor = this.nameColor;
    }
  }

  public class ExtendedTextMessage : ChatDataStorage.TextMessage
  {
    public string prefix;
    public string suffix;

    public ExtendedTextMessage(string userName, string text, string prefix, string suffix, Color nameColor, Color textColor)
      : base(userName, text, nameColor, textColor)
    {
      this.prefix = prefix.ToUpper();
      this.suffix = suffix;
    }
  }

  public class Log
  {
    public const int DEFAULT_SIZE = 60;
    private uint logSize;
    public uint elementOffset;
    private List<ChatDataStorage.Message> messages;
    private bool unread;

    public ChatDataStorage.Message this[int index]
    {
      get
      {
        return this.messages[index];
      }
    }

    public bool Unread
    {
      get
      {
        return this.unread;
      }
      set
      {
        this.unread = value;
      }
    }

    public uint MaxCount
    {
      get
      {
        return this.logSize;
      }
    }

    public int Count
    {
      get
      {
        return this.messages.Count;
      }
    }

    public int Last
    {
      get
      {
        if (this.Count > 0)
          return this.Count - 1;
        return 0;
      }
    }

    public int Offset
    {
      get
      {
        return (int) this.elementOffset;
      }
      set
      {
        this.elementOffset = (uint) Mathf.Clamp(value, 0, this.Last);
        if (this.Offset != this.Last)
          return;
        this.Unread = false;
      }
    }

    public bool ViewingLast
    {
      get
      {
        return this.Offset == this.Last;
      }
    }

    public Log()
      : this(60U)
    {
    }

    public Log(uint logSize)
    {
      this.elementOffset = 0U;
      this.logSize = logSize;
      this.messages = new List<ChatDataStorage.Message>((int) logSize);
    }

    public void PushFront(ChatDataStorage.Message message)
    {
      bool flag = this.Offset == this.Last;
      this.messages.Insert(0, message);
      if (!flag)
        return;
      this.Offset = this.Last;
    }

    public void Add(ChatDataStorage.Message message, bool setUnread = true)
    {
      bool flag = this.Offset == this.Last;
      this.messages.Add(message);
      if ((long) this.messages.Count >= (long) this.logSize)
      {
        this.messages.RemoveAt(0);
        --this.Offset;
      }
      if (flag)
        this.Offset = this.Last;
      this.Unread = setUnread;
    }

    public void ViewNewestMessage()
    {
      this.Offset = this.Last;
    }
  }
}
