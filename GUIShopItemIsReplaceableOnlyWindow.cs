﻿// Decompiled with JetBrains decompiler
// Type: GUIShopItemIsReplaceableOnlyWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using UnityEngine;

public class GUIShopItemIsReplaceableOnlyWindow : GUIPanel
{
  public GUIShopItemIsReplaceableOnlyWindow(SmartRect parent)
    : base(parent)
  {
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/EquipBuyPanel/gui_item_is_replaceable_only_layout");
    GUIImageNew guiImageNew = (jwindowDescription["background_image"] as JImage).CreateGUIImageNew(this.root);
    this.root.Width = guiImageNew.Rect.width;
    this.root.Height = guiImageNew.Rect.height;
    this.AddPanel((GUIPanel) guiImageNew);
    this.AddPanel((GUIPanel) (jwindowDescription["text_label"] as JLabel).CreateGUILabelNew(this.root));
    GUIButtonNew guiButtonNew = (jwindowDescription["ok_button"] as JButton).CreateGUIButtonNew(this.root);
    guiButtonNew.Handler = new AnonymousDelegate(this.OnOkButton);
    this.AddPanel((GUIPanel) guiButtonNew);
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    base.OnMouseDown(mousePosition, mouseKey);
    return this.IsRendered;
  }

  public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
  {
    base.OnMouseUp(mousePosition, mouseKey);
    return this.IsRendered;
  }

  public override void Show()
  {
    this.IsRendered = true;
    Game.InputDispatcher.Focused = (InputListener) this;
  }

  public override void Hide()
  {
    this.IsRendered = false;
    Game.InputDispatcher.Focused = (InputListener) null;
  }

  protected void OnOkButton()
  {
    this.Hide();
  }
}
