﻿// Decompiled with JetBrains decompiler
// Type: Battery
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour
{
  public float DelayOnReady = 5f;
  private BattleValidator validator = new BattleValidator();
  public SpaceObject Target;
  public DebugMissileLauncher[] MissileLaunchers;
  public string TargetName;
  public float MinFireInterval;
  public float MaxFireInterval;
  public float OffsetRadius;
  private bool firing;
  private bool ready;
  private float readyTime;

  public bool Ready
  {
    get
    {
      if (this.ready)
        return (double) Time.time > (double) this.readyTime + (double) this.DelayOnReady;
      return false;
    }
    set
    {
      if (!this.ready && value)
        this.readyTime = Time.time;
      if (this.ready && !value)
        this.readyTime = float.PositiveInfinity;
      this.ready = value;
    }
  }

  private void Update()
  {
    this.Ready = this.validator.IsReady();
    if (this.Ready && !this.firing)
    {
      if ((Object) SpaceLevel.GetLevel() != (Object) null)
      {
        List<SpaceObject> spaceObjectList = new List<SpaceObject>();
        foreach (SpaceObject spaceObject in SpaceLevel.GetLevel().GetObjectRegistry().GetAll())
        {
          if (this.TargetName.Equals(spaceObject.WorldCard.PrefabName))
            spaceObjectList.Add(spaceObject);
        }
        float num = float.MaxValue;
        using (List<SpaceObject>.Enumerator enumerator = spaceObjectList.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            SpaceObject current = enumerator.Current;
            float sqrMagnitude = (this.transform.position - current.Position).sqrMagnitude;
            if ((double) sqrMagnitude < (double) num)
            {
              num = sqrMagnitude;
              this.Target = current;
            }
          }
        }
      }
      this.BeginFire();
    }
    if (this.Ready || !this.firing)
      return;
    this.StopFire();
  }

  public void BeginFire()
  {
    this.firing = true;
    foreach (DebugMissileLauncher missileLauncher in this.MissileLaunchers)
    {
      missileLauncher.DebugTarget = this.Target;
      missileLauncher.DebugFireInterval = Random.Range(this.MinFireInterval, this.MaxFireInterval);
      missileLauncher.DebugOffsetRadius = this.OffsetRadius;
      missileLauncher.DebugFire = true;
    }
  }

  public void StopFire()
  {
    this.firing = false;
    foreach (DebugMissileLauncher missileLauncher in this.MissileLaunchers)
      missileLauncher.DebugFire = false;
  }

  private void Reset()
  {
    this.MissileLaunchers = this.gameObject.GetComponentsInChildren<DebugMissileLauncher>();
  }
}
