﻿// Decompiled with JetBrains decompiler
// Type: SystemMap3DHoverInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using TMPro;
using UnityEngine;

public class SystemMap3DHoverInfo : MonoBehaviour
{
  [SerializeField]
  private TextMeshProUGUI upperLabel;
  [SerializeField]
  private TextMeshProUGUI distanceLabel;
  [SerializeField]
  private SystemMap3DHoverInfoStatusBar hpBar;
  [SerializeField]
  private SystemMap3DHoverInfoStatusBar energyBar;
  private ISpaceEntity entity;
  private ISpaceEntity selectedTarget;

  public void Show(bool show)
  {
    if (!show && this.gameObject.activeSelf)
      this.ResetBars();
    this.gameObject.SetActive(show);
  }

  public void SetEntity(ISpaceEntity newEntity)
  {
    if (this.entity == newEntity)
      return;
    this.UnsubscribeFrom(this.entity);
    this.SubscribeTo(newEntity);
    this.entity = newEntity;
    this.ResetBars();
    this.Update();
  }

  public void SetSelectedTarget(ISpaceEntity selectedTarget)
  {
    this.selectedTarget = selectedTarget;
  }

  public void HandleTransitionToGame()
  {
    this.UnsubscribeFrom(this.entity);
    this.entity = (ISpaceEntity) null;
  }

  private void SubscribeTo(ISpaceEntity spaceEntity)
  {
    if (spaceEntity.TargetType != TargetType.SpaceObject || spaceEntity == this.selectedTarget)
      return;
    SpaceObject spaceObject = (SpaceObject) spaceEntity;
    if (spaceObject.IsMe || spaceObject.AutoSubscribe)
      return;
    ((SpaceObject) spaceEntity).Subscribe();
  }

  private void UnsubscribeFrom(ISpaceEntity spaceEntity)
  {
    if (spaceEntity == null || spaceEntity == this.selectedTarget || spaceEntity.TargetType != TargetType.SpaceObject)
      return;
    SpaceObject spaceObject = (SpaceObject) spaceEntity;
    if (spaceObject.IsMe || spaceObject.AutoSubscribe)
      return;
    ((SpaceObject) spaceEntity).UnSubscribe();
  }

  private void Update()
  {
    if (this.entity == null)
      return;
    this.UpdateTopLabel();
    this.UpdateDistanceLabel();
    this.UpdateBars();
  }

  private void UpdateTopLabel()
  {
    string str = "<size=14>" + Tools.ParseMessage(this.entity.ObjectName) + "</size>";
    switch (this.entity.SpaceEntityType)
    {
      case SpaceEntityType.Player:
        if (!((Object) SpaceLevel.GetLevel() != (Object) null) || !SpaceLevel.GetLevel().IsTournament)
        {
          PlayerShip playerShip = (PlayerShip) this.entity;
          str = str + "\n<size=12>" + playerShip.Player.Name + ", Level " + (object) playerShip.Player.Level + "</size>";
          break;
        }
        break;
    }
    this.upperLabel.gameObject.SetActive(this.entity.SpaceEntityType != SpaceEntityType.Trigger);
    this.upperLabel.text = str;
  }

  private void UpdateDistanceLabel()
  {
    if (Game.Me.Ship == null)
      return;
    this.distanceLabel.text = ((int) (Game.Me.Ship.Position - this.entity.Position).magnitude).ToString() + "m";
  }

  private void UpdateBars()
  {
    bool flag = this.entity.TargetType == TargetType.SpaceObject;
    this.hpBar.gameObject.SetActive(flag && this.IsAttackable(this.entity));
    this.energyBar.gameObject.SetActive(flag && this.HasEnergySystems(this.entity));
    if (!flag)
      return;
    SpaceObject spaceObject = (SpaceObject) this.entity;
    if (!spaceObject.Props.FirstStatUpdateReceived && !spaceObject.IsMe)
      return;
    this.SetHpBar(spaceObject.Props.HullPoints, spaceObject.Props.MaxHullPoints);
    this.SetEnergyBar(spaceObject.Props.PowerPoints, spaceObject.Props.MaxPowerPoints);
  }

  private bool HasEnergySystems(ISpaceEntity spaceEntity)
  {
    switch (spaceEntity.SpaceEntityType)
    {
      case SpaceEntityType.SpaceLocationMarker:
      case SpaceEntityType.AsteroidGroup:
      case SpaceEntityType.SectorMap3DFocusPoint:
      case SpaceEntityType.Missile:
      case SpaceEntityType.Debris:
      case SpaceEntityType.Asteroid:
      case SpaceEntityType.Container:
      case SpaceEntityType.MiningShip:
      case SpaceEntityType.Trigger:
      case SpaceEntityType.Planet:
      case SpaceEntityType.Planetoid:
      case SpaceEntityType.Mine:
      case SpaceEntityType.DotArea:
      case SpaceEntityType.JumpBeacon:
      case SpaceEntityType.SectorEvent:
      case SpaceEntityType.MineField:
      case SpaceEntityType.JumpTargetTransponder:
      case SpaceEntityType.Comet:
        return false;
      default:
        return true;
    }
  }

  private bool IsAttackable(ISpaceEntity spaceEntity)
  {
    switch (spaceEntity.SpaceEntityType)
    {
      case SpaceEntityType.SpaceLocationMarker:
      case SpaceEntityType.SectorMap3DFocusPoint:
      case SpaceEntityType.Debris:
      case SpaceEntityType.Trigger:
      case SpaceEntityType.Planet:
      case SpaceEntityType.Planetoid:
      case SpaceEntityType.DotArea:
      case SpaceEntityType.SectorEvent:
        return false;
      default:
        return true;
    }
  }

  private void SetHpBar(float currentHp, float maxHp)
  {
    this.hpBar.SetValue(currentHp, maxHp);
  }

  private void SetEnergyBar(float currentEnergy, float maxEnergy)
  {
    this.energyBar.SetValue(currentEnergy, maxEnergy);
  }

  private void ResetBars()
  {
    this.hpBar.Reset();
    this.energyBar.Reset();
  }
}
