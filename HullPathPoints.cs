﻿// Decompiled with JetBrains decompiler
// Type: HullPathPoints
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class HullPathPoints : MonoBehaviour
{
  [SerializeField]
  public HullPathPoints.HullPath[] paths;

  private void OnDrawGizmosSelected()
  {
    if (this.paths == null)
      return;
    Color[] colorArray = new Color[6]{ Color.blue, Color.yellow, Color.cyan, Color.red, Color.white, Color.magenta };
    Gizmos.color = Color.blue;
    int num = 0;
    for (int index1 = 0; index1 < this.paths.Length; ++index1)
    {
      Vector3[] vector3Array = this.paths[index1].points;
      for (int index2 = 1; index2 < vector3Array.Length; ++index2)
        Gizmos.DrawLine(vector3Array[index2 - 1], vector3Array[index2]);
      Gizmos.color = colorArray[++num % colorArray.Length];
    }
  }

  [Serializable]
  public struct HullPath
  {
    public Vector3[] points;
  }
}
