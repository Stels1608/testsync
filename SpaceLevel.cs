﻿// Decompiled with JetBrains decompiler
// Type: SpaceLevel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class SpaceLevel : GameLevel
{
  private uint serverId = uint.MaxValue;
  private readonly ObjectRegistry objectRegistry = new ObjectRegistry();
  private readonly SpacePositionRegistry positionRegistry = new SpacePositionRegistry();
  private readonly BurstPool burstPool = new BurstPool();
  private readonly HashSet<SpaceObject> paintedTargets = new HashSet<SpaceObject>();
  private SpaceLevelPreloaderScript spaceLevelPreloaderScript;
  protected uint cardGuid;
  private bool isTimeInit;
  private long timeOrigin;
  private bool isEmptySpaceMessageShown;
  public SectorSFX sectorSfx;
  private GUISpeedControl guiSpeedControl;
  public ShipControlsBase ShipControls;
  private PlayerShip playerShip;
  private PlayerShip actualPlayerShip;
  private IncomingMissiles incomingMissiles;
  private ShipEngineTuner shipEngineTuner;
  private GameLocation location;
  private SpaceCameraBase spaceCamera;
  private float createGuiTime;
  private float createEnviromentTime;
  private ShipRoleSpecificsFactory shipRoleSpecificsFactory;
  private GUISlotManager guiSlotManager;
  private bool ownPlayerShipSpawned;

  public GUISpeedControl GUISpeedControl
  {
    get
    {
      return this.guiSpeedControl;
    }
  }

  public override GameLevelType LevelType
  {
    get
    {
      return GameLevelType.SpaceLevel;
    }
  }

  public override bool IsIngameLevel
  {
    get
    {
      return true;
    }
  }

  public override float NearClipPlane
  {
    get
    {
      return 3f;
    }
  }

  public override float FarClipPlane
  {
    get
    {
      return 50000f;
    }
  }

  public uint ServerID
  {
    get
    {
      return this.serverId;
    }
  }

  public long TimeOrigin
  {
    get
    {
      return this.timeOrigin;
    }
  }

  public SectorCard Card
  {
    get
    {
      return (SectorCard) Game.Catalogue.FetchCard(this.cardGuid, CardView.Sector);
    }
  }

  public bool IsGlobal
  {
    get
    {
      return this.Location == GameLocation.Space;
    }
  }

  public bool IsTeaser
  {
    get
    {
      return this.Location == GameLocation.Teaser;
    }
  }

  public bool IsTutorialMission
  {
    get
    {
      if (!this.IsTeaser)
        return this.Location == GameLocation.Tutorial;
      return true;
    }
  }

  public bool IsStory
  {
    get
    {
      if (!this.IsTutorialMission)
        return this.Location == GameLocation.Story;
      return true;
    }
  }

  public bool IsBattlespace
  {
    get
    {
      return this.Location == GameLocation.BattleSpace;
    }
  }

  public bool IsTournament
  {
    get
    {
      return this.Location == GameLocation.Tournament;
    }
  }

  public bool IsArena
  {
    get
    {
      return this.Location == GameLocation.Arena;
    }
  }

  public bool Docking { get; set; }

  public SpaceObject DockTarget { get; set; }

  public PlayerShip PlayerShip
  {
    get
    {
      return this.playerShip;
    }
  }

  public PlayerShip PlayerActualShip
  {
    get
    {
      return this.actualPlayerShip;
    }
  }

  public bool HavePlayerShip
  {
    get
    {
      return this.playerShip != null;
    }
  }

  public GameLocation Location
  {
    get
    {
      return this.location;
    }
  }

  public SpaceCameraBase SpaceCamera
  {
    get
    {
      return this.spaceCamera;
    }
  }

  public HashSet<SpaceObject> PaintedTargets
  {
    get
    {
      return this.paintedTargets;
    }
  }

  public IncomingMissiles IncomingMissiles
  {
    get
    {
      return this.incomingMissiles;
    }
  }

  public BurstPool BurstPool
  {
    get
    {
      return this.burstPool;
    }
  }

  public ShipEngineTuner ShipEngineTuner
  {
    get
    {
      return this.shipEngineTuner;
    }
  }

  public ShipRoleSpecificsFactory ShipRoleSpecificsFactory
  {
    get
    {
      return this.shipRoleSpecificsFactory;
    }
  }

  public GUISlotManager GUISlotManager
  {
    get
    {
      return this.guiSlotManager;
    }
  }

  public Vector3 GetSectorSizeV3
  {
    get
    {
      return new Vector3(this.Card.Width, this.Card.Length, this.Card.Height);
    }
  }

  public bool PlayerIsInEmptySpace
  {
    get
    {
      if ((double) Mathf.Abs(this.GetPlayerPosition().x) <= (double) this.GetSectorSize().x / 2.0)
        return (double) Mathf.Abs(this.GetPlayerPosition().z) > (double) this.GetSectorSize().y / 2.0;
      return true;
    }
  }

  public static SpaceLevel GetLevel()
  {
    return GameLevel.Instance as SpaceLevel;
  }

  public Vector3 GetPlayerPosition()
  {
    if (this.playerShip != null)
      return this.playerShip.Position;
    return Vector3.zero;
  }

  public ObjectRegistry GetObjectRegistry()
  {
    return this.objectRegistry;
  }

  public SpacePositionRegistry GetSpacePositionRegistry()
  {
    return this.positionRegistry;
  }

  public PlayerShip GetPlayerShip()
  {
    return this.playerShip;
  }

  public void SetPlayerShip(PlayerShip playerShip)
  {
    this.playerShip = playerShip;
    this.SetupShipSpecificControls();
  }

  public void InjectCamera(SpaceCameraBase spaceCamera)
  {
    this.spaceCamera = spaceCamera;
    Game.InputDispatcher.RemoveListener((InputListener) spaceCamera);
    Game.InputDispatcher.AddListener((InputListener) spaceCamera);
  }

  public PlayerShip GetActualPlayerShip()
  {
    return this.actualPlayerShip;
  }

  public void SetActualPlayerShip(PlayerShip actualPlayerShip)
  {
    this.actualPlayerShip = actualPlayerShip;
  }

  public void SetTimeOrigin(long timeOrigin)
  {
    this.timeOrigin = timeOrigin;
    this.isTimeInit = true;
    Tick.Reset(Game.TimeSync.SectorTime);
    Game.TimeSync.StartSync();
  }

  public SpaceObject GetPlayerTarget()
  {
    return Game.Me.CurrentTarget;
  }

  public void GetSectorSize(out float width, out float length)
  {
    width = this.Card.Width;
    length = this.Card.Length;
  }

  public float2 GetSectorSize()
  {
    return new float2(this.Card.Width, this.Card.Length);
  }

  protected override void AddLoadingScreenDependencies()
  {
    this.IsStarted.Depend(new ILoadable[1]
    {
      (ILoadable) this.spaceLevelPreloaderScript
    });
  }

  public override void Initialize(LevelProfile levelProfile)
  {
    base.Initialize(levelProfile);
    this.cameraSwitcher = new CameraSwitcher(Camera.main);
    Game.Me.TournamentParticipant = false;
    TournamentProtocol.GetProtocol().OnTournamentParticipant = (TournamentProtocol.TournamentDelegate) null;
    SpaceLevelProfile spaceLevelProfile = levelProfile as SpaceLevelProfile;
    this.serverId = spaceLevelProfile.ServerId;
    this.cardGuid = spaceLevelProfile.CardGuid;
    this.location = spaceLevelProfile.Location;
    this.spaceLevelPreloaderScript = SpaceLevelPreloaderScript.Run(new PreloadCoroutine(this.Preload));
    this.spaceLevelPreloaderScript.TransSceneType = spaceLevelProfile.TransSceneType;
    Game.InstantiateScript<FXManager>();
    GameProtocol.GetProtocol().SetEnabled(true);
    this.incomingMissiles = new IncomingMissiles();
    this.shop = ((ShopDataProvider) FacadeFactory.GetInstance().FetchDataProvider("ShopDataProvider")).Shop;
    this.shop.RequestItems();
    this.CreateEnvironment();
    this.CreateGUI();
    Game.GUIManager.IsRendered = false;
    this.shipEngineTuner = (Resources.Load("Tuners/ShipEngineTuner") as GameObject).GetComponent<ShipEngineTuner>();
    SceneProtocol.GetProtocol().NotifySceneLoaded();
    FacadeFactory.GetInstance().SendMessage(Message.LevelLoaded, (object) this);
    DradisUpdater dradisUpdater = new DradisUpdater(this);
    this.Invoke("AutoAllocateConsumables", 4f);
  }

  private void AddToCombatGui(IGUIRenderable renderable)
  {
    FacadeFactory.GetInstance().SendMessage(Message.AddToCombatGui, (object) renderable);
  }

  private void AutoAllocateConsumables()
  {
    Game.Me.AutoAllocateConsumables();
  }

  [DebuggerHidden]
  private IEnumerator Preload(PreloaderState state)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SpaceLevel.\u003CPreload\u003Ec__Iterator2D() { state = state, \u003C\u0024\u003Estate = state, \u003C\u003Ef__this = this };
  }

  private void SetupShipSpecificControls()
  {
    if (this.ownPlayerShipSpawned)
      return;
    this.ownPlayerShipSpawned = true;
    GameObject gameObject = GameObject.Find("Camerabox");
    this.shipRoleSpecificsFactory = new ShipRoleSpecificsFactory(this.playerShip);
    System.Type specificCameraBehaviour = this.ShipRoleSpecificsFactory.GetSpecificCameraBehaviour();
    this.spaceCamera = (SpaceCameraBase) gameObject.AddComponent(specificCameraBehaviour);
    this.ShipControls = this.ShipRoleSpecificsFactory.CreateShipSpecificControls();
    Game.InputDispatcher.AddListener((InputListener) this.ShipControls);
    Game.InputDispatcher.AddListener((InputListener) this.spaceCamera);
    List<IGUIPanel> specialHudElements = this.ShipRoleSpecificsFactory.CreateSpecialHudElements();
    using (List<IGUIPanel>.Enumerator enumerator = specialHudElements.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.AddToCombatGui((IGUIRenderable) enumerator.Current);
    }
    using (List<IGUIPanel>.Enumerator enumerator = specialHudElements.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Game.InputDispatcher.AddListener((InputListener) enumerator.Current);
    }
  }

  private void CreateEnvironment()
  {
    Log.Add("Creating Environment : " + this.Card.GUICard.Name);
    this.createEnviromentTime = Time.realtimeSinceStartup;
    new BackgroundConstructor(this.Card.NebulaDesc, "Nebula").Construct();
    new BackgroundConstructor(this.Card.StarsDesc, "Stars").Construct();
    new BackgroundConstructor(this.Card.StarsMultDesc, "StarsMult").Construct();
    new BackgroundConstructor(this.Card.StarsVarianceDesc, "StarsVariance").Construct();
    RenderSettings.ambientLight = this.Card.AmbientColor;
    foreach (SunDesc sunDesc in this.Card.SunDescs)
    {
      Log.Add("Sun exists and is contructing");
      new SunConstructor(sunDesc).Construct();
    }
    foreach (MovingNebulaDesc movingNebulaDesc in this.Card.MovingNebulaDescs)
      new MovingNebulaConstructor(movingNebulaDesc).Construct();
    foreach (LightDesc lightDesc in this.Card.LightDescs)
      new LightConstructor(lightDesc).Construct();
    Camera.main.GetComponent<StarFog2>().Color = this.Card.FogColor;
    Camera.main.GetComponent<StarFog2>().CloudCount = this.Card.FogDensity;
    Camera.main.GetComponent<Stardust>().Initialize(this.Card.DustColor);
    Log.Add("Creating of " + this.Card.GUICard.Name + " finished. Time to create: " + (object) (float) ((double) Time.realtimeSinceStartup - (double) this.createEnviromentTime));
  }

  private void CreateGUI()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SpaceLevel.\u003CCreateGUI\u003Ec__AnonStoreyDB guICAnonStoreyDb = new SpaceLevel.\u003CCreateGUI\u003Ec__AnonStoreyDB();
    Log.Add("Creating GUI");
    FacadeFactory.GetInstance().SendMessage(Message.CreateLevelUi, (object) this);
    this.createGuiTime = Time.realtimeSinceStartup;
    JumpActionsHandler jumpActionsHandler = new JumpActionsHandler();
    ExperienceBar experienceBar = new ExperienceBar();
    AtlasCache atlasCache1 = new AtlasCache(new float2(23f, 23f));
    DradisManager dradisManager = new DradisManager(atlasCache1);
    GUITyliumFillbar guiTyliumFillbar = new GUITyliumFillbar();
    guiTyliumFillbar.Parent = dradisManager.SmartRect;
    guiTyliumFillbar.Position = new float2((float) -((double) dradisManager.SmartRect.Width / 2.0 + 5.0), 0.0f);
    this.guiSpeedControl = new GUISpeedControl();
    this.guiSpeedControl.Parent = dradisManager.SmartRect;
    this.guiSpeedControl.Position = new float2((float) ((double) dradisManager.SmartRect.Width / 2.0 + 10.0), 0.0f);
    GUIOptionsButtons optionsButtons = new GUIOptionsButtons();
    optionsButtons.Parent = guiTyliumFillbar.SmartRect;
    optionsButtons.Position = new float2((float) -((double) guiTyliumFillbar.SmartRect.Width / 2.0 + 20.0), 0.0f);
    AtlasCache atlasCache2 = new AtlasCache(new float2(40f, 35f));
    GUIAbilityToolbar guiAbilityToolbar = new GUIAbilityToolbar(atlasCache2);
    Game.GUIManager.AddPanel((IGUIPanel) guiAbilityToolbar);
    GUISmallPaperdoll guiSmallPaperdoll = new GUISmallPaperdoll(atlasCache2);
    guiSmallPaperdoll.Parent = guiAbilityToolbar.SmartRect;
    GUISmallPaperdollCarrierRepairPanel carrierRepairPanel = new GUISmallPaperdollCarrierRepairPanel();
    Timer.CreateTimer("carrierRepairUpdateTimer", 1f, 1f, new Timer.TickHandler(((GuiPanel) carrierRepairPanel).PeriodicUpdate));
    GUICharacterStatusWindow characterStatusWindow = new GUICharacterStatusWindow(true);
    this.guiSlotManager = new GUISlotManager();
    GUISystemMap guiSystemMap = new GUISystemMap(atlasCache1);
    GuiConsumablePanel guiConsumablePanel = new GuiConsumablePanel();
    JumpCountdown jumpCountdown = new JumpCountdown();
    GuiThreat guiThreat = new GuiThreat();
    MissileWarningBar missileWarningBar = new MissileWarningBar(this.IncomingMissiles);
    NotificationManager notificationManager = new NotificationManager();
    MessageBoxManager messageBoxManager = new MessageBoxManager();
    GUIShopCantbuyWindow shopCantbuyWindow = new GUIShopCantbuyWindow((SmartRect) null);
    shopCantbuyWindow.Hide();
    GuiDotAreaNotificationPanel notificationPanel = new GuiDotAreaNotificationPanel();
    HudMediator hudMediator = (HudMediator) FacadeFactory.GetInstance().FetchView("HudMediator");
    GuiMediator guiMediator = (GuiMediator) FacadeFactory.GetInstance().FetchView("GuiMediator");
    SystemMap3DMediator systemMap3Dmediator = (SystemMap3DMediator) FacadeFactory.GetInstance().FetchView("SectorMap3DMediator");
    GuiDockUndock.Reinitialize();
    if (this.IsGlobal)
    {
      GuiOutpostProgress guiOutpostProgress = new GuiOutpostProgress();
      Game.GUIManager.AddPanel((IGUIPanel) guiOutpostProgress);
      this.AddToCombatGui((IGUIRenderable) guiOutpostProgress);
    }
    Game.Ticker.IsRendered = !this.IsTutorialMission;
    Game.GUIManager.AddPanel((IGUIPanel) jumpActionsHandler);
    Game.GUIManager.AddPanel((IGUIPanel) experienceBar);
    Game.GUIManager.AddPanel((IGUIPanel) dradisManager);
    Game.GUIManager.AddPanel((IGUIPanel) guiTyliumFillbar);
    Game.GUIManager.AddPanel((IGUIPanel) this.guiSpeedControl);
    Game.GUIManager.AddPanel((IGUIPanel) optionsButtons);
    Game.GUIManager.AddPanel((IGUIPanel) guiSmallPaperdoll);
    Game.GUIManager.AddPanel((IGUIPanel) carrierRepairPanel);
    Game.GUIManager.AddPanel((IGUIPanel) this.guiSlotManager);
    if (this.IsGlobal || this.IsBattlespace || this.IsTournament)
    {
      GuiWingsMainPanel guiWingsMainPanel = new GuiWingsMainPanel();
      Game.GUIManager.AddPanel((IGUIPanel) guiWingsMainPanel);
      Game.InputDispatcher.AddListener((InputListener) guiWingsMainPanel);
    }
    Game.GUIManager.AddPanel((IGUIPanel) guiSystemMap);
    Game.GUIManager.AddPanel((IGUIPanel) guiConsumablePanel);
    Game.GUIManager.AddPanel((IGUIPanel) jumpCountdown);
    Game.GUIManager.AddPanel((IGUIPanel) guiThreat);
    Game.GUIManager.AddPanel((IGUIPanel) missileWarningBar);
    if (!this.IsTutorialMission)
      Game.GUIManager.AddPanel((IGUIPanel) characterStatusWindow);
    if (this.IsTournament)
    {
      GuiTournamentEventWindow tournamentEventWindow = new GuiTournamentEventWindow(Game.Tournament.EndTime);
      Game.GUIManager.AddPanel((IGUIPanel) tournamentEventWindow);
      this.AddToCombatGui((IGUIRenderable) tournamentEventWindow);
    }
    Game.GUIManager.AddPanel((IGUIPanel) messageBoxManager);
    Game.GUIManager.AddPanel((IGUIPanel) notificationPanel);
    Console console = Game.Console;
    Game.GUIManager.RemovePanel((IGUIPanel) console);
    Game.GUIManager.AddPanel((IGUIPanel) console);
    Game.GUIManager.AddPanel((IGUIPanel) notificationManager);
    Game.GUIManager.AddPanel((IGUIPanel) shopCantbuyWindow);
    Game.InputDispatcher.RemoveListener((InputListener) console);
    Game.InputDispatcher.RemoveListener((InputListener) Game.GUIManager.Find<GUIChatNew>());
    Game.InputDispatcher.AddListener((InputListener) GuiDockUndock.Instance);
    Game.InputDispatcher.AddListener((InputListener) systemMap3Dmediator);
    Game.InputDispatcher.AddListener((InputListener) hudMediator);
    Game.InputDispatcher.AddListener((InputListener) guiMediator);
    Game.InputDispatcher.AddListener((InputListener) shopCantbuyWindow);
    Game.InputDispatcher.AddListener((InputListener) console);
    Game.InputDispatcher.AddListener((InputListener) guiConsumablePanel);
    Game.InputDispatcher.AddListener((InputListener) messageBoxManager);
    Game.InputDispatcher.AddListener((InputListener) guiSystemMap);
    if (!this.IsTutorialMission)
      Game.InputDispatcher.AddListener((InputListener) characterStatusWindow);
    Game.InputDispatcher.AddListener((InputListener) this.guiSlotManager);
    Game.InputDispatcher.AddListener((InputListener) guiSmallPaperdoll);
    Game.InputDispatcher.AddListener((InputListener) carrierRepairPanel);
    Game.InputDispatcher.AddListener((InputListener) guiAbilityToolbar);
    Game.InputDispatcher.AddListener((InputListener) optionsButtons);
    Game.InputDispatcher.AddListener((InputListener) this.guiSpeedControl);
    Game.InputDispatcher.AddListener((InputListener) guiTyliumFillbar);
    Game.InputDispatcher.AddListener((InputListener) dradisManager);
    Game.InputDispatcher.AddListener((InputListener) experienceBar);
    Game.InputDispatcher.AddListener((InputListener) Game.GUIManager.Find<GUIChatNew>());
    Game.InputDispatcher.AddListener((InputListener) jumpActionsHandler);
    Game.InputDispatcher.AddListener((InputListener) notificationPanel);
    Game.InputDispatcher.AddListener((InputListener) missileWarningBar);
    Game.InputDispatcher.AddListener((InputListener) new TargetSelector());
    Game.InputDispatcher.AddListener((InputListener) notificationManager);
    this.AddToCombatGui((IGUIRenderable) this.guiSpeedControl);
    this.AddToCombatGui((IGUIRenderable) guiTyliumFillbar);
    this.AddToCombatGui((IGUIRenderable) guiSmallPaperdoll);
    this.AddToCombatGui((IGUIRenderable) guiAbilityToolbar);
    this.AddToCombatGui((IGUIRenderable) experienceBar);
    this.AddToCombatGui((IGUIRenderable) dradisManager);
    this.AddToCombatGui((IGUIRenderable) optionsButtons);
    this.AddToCombatGui((IGUIRenderable) this.guiSlotManager);
    this.AddToCombatGui((IGUIRenderable) jumpActionsHandler);
    this.AddToCombatGui((IGUIRenderable) missileWarningBar);
    this.AddToCombatGui((IGUIRenderable) Game.Ticker);
    this.AddToCombatGui((IGUIRenderable) Game.GUIManager.Find<JumpCountdown>());
    this.AddToCombatGui((IGUIRenderable) guiThreat);
    this.AddToCombatGui((IGUIRenderable) notificationManager);
    this.AddToCombatGui((IGUIRenderable) Game.Ticker);
    this.objectRegistry.OnChanged += new SpaceObjectEventHandler(this.OnRegistryChanged);
    if (!this.IsTutorialMission)
      Game.DelayedActions.Add(new DelayedActions.Predicate(Game.SpecialOfferManager.Check));
    // ISSUE: reference to a compiler-generated field
    guICAnonStoreyDb.waiter = new GuiPanel();
    // ISSUE: reference to a compiler-generated method
    TimerSimple timerSimple = new TimerSimple(3f, new AnonymousDelegate(guICAnonStoreyDb.\u003C\u003Em__223));
    // ISSUE: reference to a compiler-generated field
    guICAnonStoreyDb.waiter.AddChild((GuiElementBase) timerSimple);
    // ISSUE: reference to a compiler-generated field
    Game.RegisterDialog((IGUIPanel) guICAnonStoreyDb.waiter, true);
    if (!this.IsTutorialMission)
      LevelGuiInitializer.Init();
    GuiMailUnread guiMailUnread = Game.GUIManager.Find<GuiMailUnread>();
    if (guiMailUnread != null)
    {
      guiMailUnread.Align = Align.UpCenter;
      guiMailUnread.Position = new Vector2(0.0f, 0.0f);
      this.AddToCombatGui((IGUIRenderable) guiMailUnread);
    }
    FacadeFactory.GetInstance().SendMessage(Message.RecheckSystemButtons);
    this.CheckOptionsButtons(optionsButtons);
    Log.Add("Creating of GUI is finished taking: " + (object) (float) ((double) Time.realtimeSinceStartup - (double) this.createGuiTime));
  }

  protected override void Start()
  {
    this.sectorSfx = UnityEngine.Object.Instantiate<SectorSFX>(SpaceExposer.GetInstance().SectorSFXPrefab);
    this.sectorSfx.transform.parent = UnityEngine.Object.FindObjectOfType<AudioListener>().transform;
    this.sectorSfx.transform.localPosition = Vector3.zero;
    this.sectorSfx.transform.localRotation = Quaternion.identity;
    this.sectorSfx.IncomingMissiles = this.IncomingMissiles;
    base.Start();
  }

  protected override void Update()
  {
    base.Update();
    if (!this.isTimeInit)
      return;
    Tick.Update(Game.TimeSync.SectorTime);
    if (Tick.IsNewTick())
      this.objectRegistry.Advance();
    if (this.ShipControls != null)
    {
      this.ShipControls.Update();
      this.ShipControls.RequestFlush();
    }
    foreach (SpaceObject spaceObject in this.objectRegistry.GetAll())
    {
      spaceObject.Move(Game.TimeSync.SectorTime);
      spaceObject.Update();
    }
    if (this.GetPlayerTarget() != null)
      this.GetPlayerTarget().SetDistance((this.GetPlayerTarget().Position - this.GetPlayerPosition()).magnitude);
    bool playerIsInEmptySpace = this.PlayerIsInEmptySpace;
    if (playerIsInEmptySpace && !this.isEmptySpaceMessageShown)
    {
      FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("%$bgo.etc.fly_too_far%", NotificationCategory.Neutral));
      this.isEmptySpaceMessageShown = true;
    }
    if (playerIsInEmptySpace)
      return;
    this.isEmptySpaceMessageShown = false;
  }

  protected override void LateUpdate()
  {
    base.LateUpdate();
    if (HudIndicatorManagerUgui.CurrentManager == null)
      return;
    HudIndicatorManagerUgui.CurrentManager.RemoteLateUpdate();
  }

  public override void OnQuit()
  {
    base.OnQuit();
    if (Game.IsQuitting)
      return;
    this.objectRegistry.Clear();
  }

  private Camera[] FindCameras()
  {
    return UnityEngine.Object.FindObjectsOfType(typeof (Camera)) as Camera[];
  }

  private int[] SwitchOffCameras(Camera[] cameras)
  {
    int[] numArray = new int[cameras.Length];
    for (int index = 0; index < cameras.Length; ++index)
      numArray[index] = cameras[index].cullingMask;
    foreach (Camera camera in cameras)
      camera.cullingMask = 0;
    return numArray;
  }

  private void SwitchOnCameras(Camera[] cameras, int[] cullingMasks)
  {
    for (int index = 0; index < cameras.Length; ++index)
      cameras[index].cullingMask = cullingMasks[index];
  }

  public void Dock()
  {
    SpaceObject playerTarget = this.GetPlayerTarget();
    if (playerTarget == null || !playerTarget.CanDock(true))
      return;
    this.DockTarget = playerTarget;
    JumpCountdown jumpCountdown = Game.GUIManager.Find<JumpCountdown>();
    if (jumpCountdown != null && jumpCountdown.IsRendered)
      JumpActionsHandler.CancelJumpSequence();
    if (playerTarget is PlayerShip)
    {
      FacadeFactory.GetInstance().SendMessage(Message.DockAtPlayerShipRequest, (object) playerTarget);
      GameProtocol.GetProtocol().RequestAnchor(playerTarget.ObjectID);
      TargetSelector.SelectTargetRequest((SpaceObject) null, false);
    }
    else
      GameProtocol.GetProtocol().RequestDock(playerTarget.ObjectID, 0.0f);
  }

  public void StopSector()
  {
    GameProtocol.GetProtocol().Quit();
    if (!Game.Me.Party.inGroupJump)
      return;
    JumpActionsHandler.CancelJumpSequence();
  }

  public void PlayerDied(object parameter)
  {
    Game.Me.ActiveShip.ResetSlots();
    if (this.IsArena)
      return;
    if (parameter is DeathDesc)
    {
      DeathDesc deathDesc = parameter as DeathDesc;
      DeathNotificator.Show(deathDesc);
      Timer.CreateTimer("ShowRespawnDialogue", 2f, 2f, new Timer.TickParametrizedHandler(this.ShowRespawnDialogue), (object) deathDesc, true);
    }
    else
    {
      if (!(parameter is Price))
        return;
      using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = (parameter as Price).items.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
          if ((int) current.Key.CardGUID == 264733124)
            Game.RegisterDialog((IGUIPanel) new BuyContinueDialog((uint) current.Value), true);
        }
      }
    }
  }

  public void ShowRespawnDialogue(object parameter)
  {
    DeathDesc deathDesc = parameter as DeathDesc;
    if (this.IsStory)
    {
      this.StopSector();
    }
    else
    {
      GuiDeathPopup guiDeathPopup = Game.GUIManager.Find<GuiDeathPopup>();
      if (guiDeathPopup == null)
        PlayerRespawnDialog.Show(deathDesc);
      else
        guiDeathPopup.m_deathDesc = deathDesc;
    }
  }

  private void OnRegistryChanged(SpaceObject obj, SpaceObjectEvent notification)
  {
    if (notification != SpaceObjectEvent.Removed || Game.Me == null || Game.Me.ActiveShip == null)
      return;
    Game.Me.ActiveShip.OnTargetLost(obj);
  }

  public override void CheckSystemButtons(SystemButtonWindow systemButtons)
  {
    base.CheckSystemButtons(systemButtons);
    systemButtons.ShowArena(!this.IsArena && !this.IsBattlespace && (!this.IsStory && !this.IsTournament) && !this.IsTutorialMission);
    systemButtons.ShowTournamentLeaderboards(this.IsTournament);
    systemButtons.ShowGuild(!this.IsTutorialMission && !this.IsStory);
    if (this.IsArena || this.IsTournament || (this.IsStory || this.IsTutorialMission))
      systemButtons.ShowBattlespace(false);
    if (this.IsArena || this.IsBattlespace || this.IsStory)
      systemButtons.ShowTournament(false);
    if (this.IsBattlespace)
      systemButtons.SetBattlespaceTooltip("%$bgo.option_buttons.quit_battlespace%");
    if (this.IsTournament)
      systemButtons.SetTournamentTooltip("%$bgo.option_buttons.quit_tournament%");
    else
      systemButtons.SetTournamentTooltip("%$bgo.option_buttons.tournament%");
    if (!this.IsTutorialMission)
      return;
    systemButtons.ShowJournal(false);
    systemButtons.ShowHold(false);
  }

  protected void CheckOptionsButtons(GUIOptionsButtons optionsButtons)
  {
    if (this.IsStory)
    {
      GUIButtonNew guiButtonNew = optionsButtons.Find<GUIButtonNew>("galaxyMap");
      if (guiButtonNew != null)
        guiButtonNew.IsRendered = false;
    }
    if (!this.IsTutorialMission)
      return;
    GUIButtonNew guiButtonNew1 = optionsButtons.Find<GUIButtonNew>("restock");
    if (guiButtonNew1 == null)
      return;
    guiButtonNew1.IsRendered = false;
  }
}
