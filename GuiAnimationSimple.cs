﻿// Decompiled with JetBrains decompiler
// Type: GuiAnimationSimple
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GuiAnimationSimple : GuiPanel
{
  private AtlasAnimation animation;
  private float progress;
  private bool isAnimationPlaying;
  private float prevTime;
  private Color color;
  private bool colorOverride;

  public float Progress
  {
    set
    {
      this.progress = Mathf.Clamp01(value);
      this.animation.PlaybackPosition = this.progress;
    }
  }

  public bool isPlaying
  {
    get
    {
      return this.isAnimationPlaying;
    }
    set
    {
      if (value == this.isAnimationPlaying)
        return;
      this.isAnimationPlaying = value;
      if (value)
        this.Start();
      else
        this.Stop();
    }
  }

  public GuiAnimationSimple(Texture2D texture, uint width, uint height)
    : this(texture, width, height, Color.white)
  {
    this.colorOverride = false;
  }

  public GuiAnimationSimple(Texture2D texture, uint width, uint height, Color color)
  {
    this.animation = new AtlasAnimation(texture, width, height);
    this.SizeX = (float) texture.width / (float) width;
    this.SizeY = (float) texture.height / (float) height;
    this.color = color;
    this.Progress = 0.0f;
    this.IsRendered = true;
    this.colorOverride = true;
  }

  public override void Draw()
  {
    base.Draw();
    if (Event.current.type != UnityEngine.EventType.Repaint)
      return;
    if (this.colorOverride)
      Graphics.DrawTexture(this.Rect, (Texture) this.animation.Texture, this.animation.FrameRect, 0, 0, 0, 0, this.color);
    else
      Graphics.DrawTexture(this.Rect, (Texture) this.animation.Texture, this.animation.FrameRect, 0, 0, 0, 0);
  }

  public void Start()
  {
    this.isAnimationPlaying = true;
  }

  public void Stop()
  {
    this.isAnimationPlaying = false;
    this.progress = 0.0f;
  }

  public void Reset()
  {
    this.progress = 0.0f;
  }

  public override void Update()
  {
    base.Update();
    if (!this.isPlaying)
      return;
    float time = Time.time;
    if ((double) time - (double) this.prevTime >= 1.0)
      this.prevTime = time;
    this.Progress = time - this.prevTime;
  }
}
