﻿// Decompiled with JetBrains decompiler
// Type: ChatTextMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

internal class ChatTextMessage : ChatProtocolParser
{
  public string text;
  public string userName;
  public int roomId;

  public ChatTextMessage()
  {
    this.type = ChatProtocolParserType.Text;
  }

  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split(new char[3]{ '%', '@', '#' });
    if (strArray.Length != 5 || strArray[0] != "a" || !int.TryParse(strArray[1], out this.roomId))
      return false;
    this.userName = strArray[2];
    this.text = strArray[3];
    return true;
  }
}
