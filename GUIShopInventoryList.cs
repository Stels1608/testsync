﻿// Decompiled with JetBrains decompiler
// Type: GUIShopInventoryList
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GUIShopInventoryList : GUIPanel
{
  private readonly List<GUIShopInventorySlot> slots = new List<GUIShopInventorySlot>();
  private int selectedId = -1;
  private int tryToSelectSlotId = -1;
  public const int SLOTS_COUNT = 5;
  private const float SLOTS_SPACE = 1f;
  private int viewFirst;
  private GUIShopInventoryList.Handlr handler;

  public int ViewFirst
  {
    get
    {
      return this.viewFirst;
    }
    set
    {
      this.viewFirst = value;
    }
  }

  public int SelectedID
  {
    get
    {
      return this.selectedId;
    }
    set
    {
      this.selectedId = value;
    }
  }

  public GUIShopInventoryList.Handlr Handler
  {
    set
    {
      this.handler = value;
    }
  }

  public AnonymousDelegate HandlerUpperButton
  {
    set
    {
      using (List<GUIShopInventorySlot>.Enumerator enumerator = this.slots.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.UpperButton.Handler = value;
      }
    }
  }

  public AnonymousDelegate HandlerButton
  {
    set
    {
      using (List<GUIShopInventorySlot>.Enumerator enumerator = this.slots.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.HandlerButton = value;
      }
    }
  }

  public AnonymousDelegate HandlerDetailsButton
  {
    set
    {
      using (List<GUIShopInventorySlot>.Enumerator enumerator = this.slots.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.HandlerDetailsButton = value;
      }
    }
  }

  public GUIShopInventoryList(SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
    GUIShopInventorySlot shopInventorySlot1 = new GUIShopInventorySlot(GUIShopInventorySlot.InventoryPosition.First, this.root);
    shopInventorySlot1.IsRendered = false;
    this.slots.Add(shopInventorySlot1);
    this.AddPanel((GUIPanel) shopInventorySlot1);
    float2 float2 = new float2(shopInventorySlot1.Rect.width, shopInventorySlot1.Rect.height);
    this.root.Width = float2.x;
    this.root.Height = (float) (((double) float2.y + 1.0) * 5.0);
    float _y = (float) (-(double) this.root.Height / 2.0 + (double) float2.y / 2.0);
    shopInventorySlot1.Position = new float2(0.0f, _y);
    for (int index = 1; index < 5; ++index)
    {
      GUIShopInventorySlot shopInventorySlot2 = index != 4 ? new GUIShopInventorySlot(GUIShopInventorySlot.InventoryPosition.Middle, this.root) : new GUIShopInventorySlot(GUIShopInventorySlot.InventoryPosition.Last, this.root);
      shopInventorySlot2.IsRendered = false;
      this.slots.Add(shopInventorySlot2);
      this.AddPanel((GUIPanel) shopInventorySlot2);
      shopInventorySlot2.Position = new float2(0.0f, _y + (float2.y + 1f) * (float) index);
    }
    this.RecalculateAbsCoords();
  }

  public int GetSlotId(float2 fromPos)
  {
    for (int index = 0; index < this.slots.Count; ++index)
    {
      if (this.slots[index].Contains(fromPos))
        return index;
    }
    return -1;
  }

  public Rect? GetSlotRect(float2 fromPos)
  {
    for (int index = 0; index < this.slots.Count; ++index)
    {
      if (this.slots[index].Contains(fromPos))
        return new Rect?(this.slots[index].SmartRect.AbsRect);
    }
    return new Rect?();
  }

  public void UpdateData(ItemList data, ShopInventoryContainer container)
  {
    for (int index1 = 0; index1 < 5; ++index1)
    {
      int index2 = index1 + this.viewFirst;
      if (index2 < data.Count)
      {
        if (index1 == this.tryToSelectSlotId)
        {
          this.selectedId = index2;
          if (this.handler != null)
            this.handler();
        }
        this.slots[index1].UpdateData(data[index2], container);
        this.slots[index1].IsRendered = true;
        this.slots[index1].IsSelected = index2 == this.selectedId;
        this.slots[index1].SlotNumber = index2 + 1;
      }
      else
      {
        this.slots[index1].UpdateData((ShipItem) null, container);
        this.slots[index1].IsSelected = false;
        this.slots[index1].SlotNumber = index2 + 1;
        this.slots[index1].IsRendered = false;
      }
    }
    this.tryToSelectSlotId = -1;
  }

  public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
  {
    if (mouseKey == KeyCode.Mouse0)
    {
      for (int index = 0; index < 5; ++index)
      {
        if (this.slots[index].Contains(mousePosition))
        {
          this.tryToSelectSlotId = index;
          break;
        }
      }
    }
    return base.OnMouseDown(mousePosition, mouseKey);
  }

  public StarterKit StarterKitSelected(ItemList data)
  {
    for (int index1 = 0; index1 < 5; ++index1)
    {
      int index2 = index1 + this.viewFirst;
      if (index2 < data.Count && this.slots[index1].IsSelected && data[index2] is StarterKit)
        return (StarterKit) data[index2];
    }
    return (StarterKit) null;
  }

  public delegate void Handlr();
}
