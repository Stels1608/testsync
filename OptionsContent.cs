﻿// Decompiled with JetBrains decompiler
// Type: OptionsContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class OptionsContent : MonoBehaviour
{
  protected OnSettingsApply ApplySettings;
  protected OnSettingsUndo RevertSettings;
  protected UserSettings settings;
  public bool changed;

  public virtual void UpdateSettings(UserSettings settings)
  {
    this.settings = settings;
  }

  public abstract void RestoreDefaultSettings();

  public void InjectDelegates(OnSettingsApply ApplySettings, OnSettingsUndo RevertSettings)
  {
    this.ApplySettings = ApplySettings;
    this.RevertSettings = RevertSettings;
  }

  public virtual void Apply()
  {
    this.ApplySettings(this.settings);
    this.changed = false;
  }

  public virtual void ChangeSetting(UserSetting setting, object value)
  {
    this.settings.Set(setting, value);
    this.changed = true;
  }

  public virtual void Undo()
  {
    this.RevertSettings();
    this.changed = false;
  }
}
