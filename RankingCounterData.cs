﻿// Decompiled with JetBrains decompiler
// Type: RankingCounterData
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class RankingCounterData : RankingData<RankingGroup, RankingType>
{
  public RankingCounterData(RankingGroup group, RankingType rankingType, List<RankDescription> ranks, uint totalEntries, DateTime lastUpdate)
    : base(group, rankingType, ranks, totalEntries, lastUpdate)
  {
  }
}
