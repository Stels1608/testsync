﻿// Decompiled with JetBrains decompiler
// Type: WoFGameTargetButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class WoFGameTargetButton : ToggleButtonWidget
{
  public string iconName = "dradis_cylon_icon";
  public string iconActivName = "dradis_cylon_icon_activ";
  public WoFGamePanel gamePanel;
  [SerializeField]
  public UISprite targetIcon;
  [SerializeField]
  public ItemIcon item;
  [SerializeField]
  public UISprite itemBg;
  public TweenAlpha alpha;
  public TweenAlpha alpha2;
  public TweenAlpha alpha3;

  public override void Start()
  {
    base.Start();
    this.targetIcon.spriteName = this.iconName;
    this.ActiveIsOverlay = false;
  }

  public override void OnClick()
  {
    if (!this.IsEnabled)
      return;
    base.OnClick();
    if (this.IsActive)
    {
      this.gamePanel.AddActiveTarget(this);
      this.targetIcon.spriteName = this.iconActivName;
    }
    else
    {
      this.gamePanel.RemoveActiveTarget(this);
      this.targetIcon.spriteName = this.iconName;
    }
  }

  public override void OnStatusChanged(bool status)
  {
    base.OnStatusChanged(status);
    if (status)
    {
      this.defaultState.SetActive(true);
      this.targetIcon.alpha = 1f;
    }
    else
    {
      this.defaultState.SetActive(false);
      this.targetIcon.alpha = 0.3f;
    }
  }

  public void ResetToDefault()
  {
    if ((Object) this.alpha != (Object) null)
    {
      this.alpha.StopAllCoroutines();
      Object.Destroy((Object) this.alpha);
    }
    if ((Object) this.alpha2 != (Object) null)
    {
      this.alpha2.StopAllCoroutines();
      Object.Destroy((Object) this.alpha2);
    }
    if ((Object) this.alpha3 != (Object) null)
    {
      this.alpha3.StopAllCoroutines();
      Object.Destroy((Object) this.alpha3);
    }
    if ((Object) this.item != (Object) null)
      this.item.iconImage.alpha = 0.0f;
    if ((Object) this.itemBg != (Object) null)
      this.itemBg.alpha = 0.0f;
    if ((Object) this.targetIcon != (Object) null)
    {
      this.targetIcon.alpha = 1f;
      this.targetIcon.spriteName = this.iconName;
    }
    this.defaultState.SetActive(true);
    this.IsEnabled = true;
    this.isActive = false;
    this.Set();
  }

  protected override void HoverSubSprites(bool isOver)
  {
  }

  public void Vanish()
  {
    this.alpha = TweenAlpha.Begin(this.targetIcon.gameObject, 0.1f, 0.25f);
    this.alpha.style = UITweener.Style.PingPong;
  }

  public void StartShowItem()
  {
    this.alpha.ResetToBeginning();
    this.alpha = TweenAlpha.Begin(this.targetIcon.gameObject, 0.75f, 0.0f);
    this.alpha.style = UITweener.Style.Once;
    this.alpha.eventReceiver = this.gameObject;
    this.alpha.callWhenFinished = "ShowWonItem";
  }

  public void ShowWonItem()
  {
    this.activeState.SetActive(false);
    this.defaultState.SetActive(false);
    this.hoverState.SetActive(false);
    this.alpha2 = TweenAlpha.Begin(this.itemBg.gameObject, 1f, 1f);
    this.alpha3 = TweenAlpha.Begin(this.item.iconImage.gameObject, 1f, 1f);
  }
}
