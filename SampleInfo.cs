﻿// Decompiled with JetBrains decompiler
// Type: SampleInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SampleInfo : MonoBehaviour
{
  private void OnGUI()
  {
    GUILayout.Label("iTween can spin, shake, punch, move, handle audio, fade color and transparency \nand much more with each task needing only one line of code.");
    GUILayout.BeginHorizontal();
    GUILayout.Label("iTween works with C#, JavaScript and Boo. For full documentation and examples visit:");
    if (GUILayout.Button("http://itween.pixelplacement.com"))
      Application.OpenURL("http://itween.pixelplacement.com");
    GUILayout.EndHorizontal();
  }
}
