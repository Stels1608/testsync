﻿// Decompiled with JetBrains decompiler
// Type: StoryProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;

public class StoryProtocol : BgoProtocol
{
  public StoryProtocol()
    : base(BgoProtocol.ProtocolID.Story)
  {
  }

  public static StoryProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Story) as StoryProtocol;
  }

  public void TriggerControl(StoryProtocol.ControlType control)
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 1);
    bw.Write((byte) control);
    this.SendMessage(bw);
  }

  public void MessageBoxOk()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 2);
    this.SendMessage(bw);
  }

  public void CutsceneFinished()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 7);
    this.SendMessage(bw);
  }

  public void PlayerIsLookingAtTrigger()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 8);
    this.SendMessage(bw);
  }

  public void Skip()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 3);
    this.SendMessage(bw);
  }

  public void Abandon()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 4);
    this.SendMessage(bw);
  }

  public void Continue()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 5);
    this.SendMessage(bw);
  }

  public void Decline()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((ushort) 6);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    if ((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null || !SpaceLevel.GetLevel().IsStory)
      return;
    StoryProtocol.Reply reply = (StoryProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case StoryProtocol.Reply.BannerBox:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        StoryProtocol.\u003CParseMessage\u003Ec__AnonStoreyFF messageCAnonStoreyFf = new StoryProtocol.\u003CParseMessage\u003Ec__AnonStoreyFF();
        uint cardGUID1 = br.ReadGUID();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyFf.card = Game.Catalogue.FetchCard(cardGUID1, CardView.Banner) as BannerCard;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyFf.card.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyFf.\u003C\u003Em__251));
        break;
      case StoryProtocol.Reply.MessageBox:
        bool showOk = br.ReadBoolean();
        uint cardGUID2 = br.ReadGUID();
        string str1 = br.ReadString();
        string advice = br.ReadString();
        string imagePath = br.ReadString();
        Game.GUIManager.Find<MessageBoxManager>().DeleteAllBoxes();
        GUICard card = Game.Catalogue.FetchCard(cardGUID2, CardView.GUI) as GUICard;
        if (card != null)
        {
          if (imagePath.Length > 0)
          {
            Game.GUIManager.Find<MessageBoxManager>().ShowPictureNotify(showOk, str1, advice, card, imagePath);
            break;
          }
          Game.GUIManager.Find<MessageBoxManager>().ShowPictureNotify(showOk, str1, advice, card);
          break;
        }
        new InfoBox(str1, new AnonymousDelegate(this.MessageBoxOk)).Show();
        break;
      case StoryProtocol.Reply.HelpBox:
        Game.GUIManager.Find<MessageBoxManager>().ShowHelpScreen((HelpScreenType) br.ReadUInt32());
        break;
      case StoryProtocol.Reply.Mark:
      case StoryProtocol.Reply.HighlightObject:
        uint objectID = br.ReadUInt32();
        bool flag1 = br.ReadBoolean();
        SpaceObject spaceObject = SpaceLevel.GetLevel().GetObjectRegistry().Get(objectID);
        if (spaceObject != null)
        {
          if (flag1)
          {
            spaceObject.SetMarkObject(SpaceObject.MarkObjectType.Waypoint);
            break;
          }
          spaceObject.SetMarkObject(SpaceObject.MarkObjectType.None);
          break;
        }
        if (!flag1)
          break;
        PostCreateAction postCreateAction = new PostCreateAction(PostCreateActionType.MarkAsWaypoint);
        SpaceLevel.GetLevel().GetObjectRegistry().SetPostCreateAction(objectID, postCreateAction);
        break;
      case StoryProtocol.Reply.MissionLog:
        string str2 = br.ReadString();
        br.ReadBoolean();
        FacadeFactory.GetInstance().SendMessage(Message.StoryMissionLogAddObjective, (object) str2);
        break;
      case StoryProtocol.Reply.SelectTarget:
        TargetSelector.SelectTargetRequest(SpaceLevel.GetLevel().GetObjectRegistry().Get(br.ReadUInt32()), false);
        break;
      case StoryProtocol.Reply.HighlightControl:
        StoryProtocol.ControlType controlType1 = (StoryProtocol.ControlType) br.ReadByte();
        bool highlighted = br.ReadBoolean();
        StoryProtocol.ControlType controlType2 = controlType1;
        switch (controlType2)
        {
          case StoryProtocol.ControlType.Weapon1:
          case StoryProtocol.ControlType.Weapon2:
          case StoryProtocol.ControlType.Weapon3:
          case StoryProtocol.ControlType.Weapon4:
          case StoryProtocol.ControlType.Weapon5:
          case StoryProtocol.ControlType.Weapon6:
          case StoryProtocol.ControlType.Weapon7:
          case StoryProtocol.ControlType.Weapon8:
          case StoryProtocol.ControlType.Weapon9:
            Game.GUIManager.Find<GUISmallPaperdoll>().SetBlink(controlType1, highlighted);
            return;
          case StoryProtocol.ControlType.Ability1:
          case StoryProtocol.ControlType.Ability2:
          case StoryProtocol.ControlType.Ability3:
          case StoryProtocol.ControlType.Ability4:
          case StoryProtocol.ControlType.Ability5:
          case StoryProtocol.ControlType.Ability6:
          case StoryProtocol.ControlType.Ability7:
          case StoryProtocol.ControlType.Ability8:
          case StoryProtocol.ControlType.Ability9:
            Game.GUIManager.Find<GUIAbilityToolbar>().SetBlink(controlType1, highlighted);
            return;
          case StoryProtocol.ControlType.HpGuiSlot:
            SpaceLevel.GetLevel().GUISlotManager.player.IsBlinking = highlighted;
            return;
          default:
            switch (controlType2)
            {
              case StoryProtocol.ControlType.TargetEnemy:
              case StoryProtocol.ControlType.TargetAlly:
                Game.GUIManager.Find<DradisManager>().SetBlink(controlType1, highlighted);
                return;
              case StoryProtocol.ControlType.ThrottleBar:
              case StoryProtocol.ControlType.Boosters:
              case StoryProtocol.ControlType.MatchSpeed:
              case StoryProtocol.ControlType.Follow:
                Game.GUIManager.Find<GUISpeedControl>().SetBlink(controlType1, highlighted);
                return;
              case StoryProtocol.ControlType.Camera:
                Game.GUIManager.CameraModeWindow.IsBlinking = highlighted;
                return;
              case StoryProtocol.ControlType.SystemMap:
                Game.GUIManager.Find<GUIOptionsButtons>().IsBlinking = highlighted;
                return;
              case StoryProtocol.ControlType.Turn:
              case StoryProtocol.ControlType.Strafe:
              case StoryProtocol.ControlType.Roll:
                SpaceLevel.GetLevel().ShipControls.HighlightControl = highlighted;
                return;
              case StoryProtocol.ControlType.Battlespace:
                SystemButtonWindow systemButtons = Game.GUIManager.SystemButtons;
                if (!((UnityEngine.Object) systemButtons != (UnityEngine.Object) null))
                  return;
                systemButtons.SetBlink(controlType1, highlighted);
                return;
              default:
                return;
            }
        }
      case StoryProtocol.Reply.Progress:
        switch ((MissionActivity) br.ReadByte())
        {
          case MissionActivity.MissionCompleted:
          case MissionActivity.MissionFailed:
          case MissionActivity.MissionSkiped:
          case MissionActivity.MissionStarted:
            FacadeFactory.GetInstance().SendMessage(Message.ClearTicker);
            return;
          default:
            return;
        }
      case StoryProtocol.Reply.CloseMessageBoxes:
        Game.GUIManager.Find<MessageBoxManager>().DeleteAllBoxes();
        break;
      case StoryProtocol.Reply.CloseMissionLog:
        FacadeFactory.GetInstance().SendMessage(Message.StoryMissionLogDestroyWindow);
        break;
      case StoryProtocol.Reply.PlayCutscene:
        string str3 = br.ReadString().Replace("%br%", " ");
        char[] chArray = new char[1]{ ',' };
        foreach (string str4 in str3.Split(chArray))
        {
          CutsceneId cutsceneId = (CutsceneId) Enum.Parse(typeof (CutsceneId), str4.TrimEnd().TrimStart());
          if (Enum.IsDefined(typeof (CutsceneId), (object) cutsceneId))
            FacadeFactory.GetInstance().SendMessage(Message.CutscenePlayRequest, (object) new CutscenePlayRequestData(cutsceneId, (CutsceneManager.OnFinishCutsceneDelegate) null, Game.Me.CurrentTarget));
        }
        break;
      case StoryProtocol.Reply.SimplifyTutorialUi:
        FacadeFactory.GetInstance().SendMessage(Message.SimplifyTutorialUi);
        break;
      case StoryProtocol.Reply.AddSkipButton:
        FacadeFactory.GetInstance().SendMessage(Message.ShowAbandonMissionButton, (object) br.ReadBoolean());
        break;
      case StoryProtocol.Reply.EnableTargetting:
        FacadeFactory.GetInstance().SendMessage(Message.EnableTargetting, (object) br.ReadBoolean());
        break;
      case StoryProtocol.Reply.StartLookAtTriggerClientCheck:
        string str5 = br.ReadString();
        using (IEnumerator<SpaceObject> enumerator = ((IEnumerable<SpaceObject>) SpaceLevel.GetLevel().GetObjectRegistry().GetLoaded(typeof (BsgoTrigger))).GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            BsgoTrigger bsgoTrigger = (BsgoTrigger) enumerator.Current;
            if (bsgoTrigger.Name == str5)
            {
              StoryLookAtChecker storyLookAtChecker = new StoryLookAtChecker((SpaceObject) bsgoTrigger);
            }
          }
          break;
        }
      case StoryProtocol.Reply.EnableMissileTutorial:
        bool flag2 = br.ReadBoolean();
        uint slotId = br.ReadUInt32();
        if (flag2)
        {
          StoryMissileTutorial.EnableTutorialForSlot(slotId);
          break;
        }
        StoryMissileTutorial.DisableTutorialForSlot(slotId);
        break;
      case StoryProtocol.Reply.ShowOnScreenNotification:
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification(br.ReadString(), NotificationCategory.Neutral));
        break;
      case StoryProtocol.Reply.EnableGear:
        FacadeFactory.GetInstance().SendMessage(Message.EnableGear, (object) br.ReadBoolean());
        break;
      case StoryProtocol.Reply.AskContinue:
        Timer.CreateTimer("DisconnectTimer", 2f, 2f, new Timer.TickParametrizedHandler(SpaceLevel.GetLevel().PlayerDied), (object) br.ReadDesc<Price>(), true);
        FacadeFactory.GetInstance().SendMessage(Message.Death);
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Story protocol: " + (object) reply);
        break;
    }
  }

  public enum Reply : ushort
  {
    BannerBox = 1,
    MessageBox = 2,
    HelpBox = 3,
    Mark = 4,
    MissionLog = 5,
    SelectTarget = 6,
    HighlightControl = 7,
    Progress = 8,
    HighlightObject = 9,
    CloseMessageBoxes = 10,
    CloseMissionLog = 11,
    PlayCutscene = 12,
    SimplifyTutorialUi = 13,
    AddSkipButton = 14,
    EnableTargetting = 15,
    StartLookAtTriggerClientCheck = 16,
    EnableMissileTutorial = 17,
    ShowOnScreenNotification = 18,
    EnableGear = 19,
    AskContinue = 20,
  }

  public enum Request : ushort
  {
    TriggerControl = 1,
    MessageBoxOk = 2,
    Skip = 3,
    Abandon = 4,
    Continue = 5,
    Decline = 6,
    CutsceneFinished = 7,
    LookingAtTrigger = 8,
  }

  public enum ControlType : byte
  {
    TargetEnemy = 19,
    TargetAlly = 20,
    ThrottleBar = 21,
    Boosters = 22,
    Camera = 23,
    SystemMap = 24,
    MatchSpeed = 25,
    Follow = 26,
    Turn = 27,
    Strafe = 28,
    Roll = 29,
    Battlespace = 50,
    Tournament = 51,
    Weapon1 = 101,
    Weapon2 = 102,
    Weapon3 = 103,
    Weapon4 = 104,
    Weapon5 = 105,
    Weapon6 = 106,
    Weapon7 = 107,
    Weapon8 = 108,
    Weapon9 = 109,
    Weapon10 = 110,
    Weapon11 = 111,
    Weapon12 = 112,
    WEAPON_MAX = 113,
    Ability1 = 114,
    Ability2 = 115,
    Ability3 = 116,
    Ability4 = 117,
    Ability5 = 118,
    Ability6 = 119,
    Ability7 = 120,
    Ability8 = 121,
    Ability9 = 122,
    Ability10 = 123,
    ABILITY_MAX = 126,
    HpGuiSlot = 130,
  }
}
