﻿// Decompiled with JetBrains decompiler
// Type: CutsceneSetup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CutsceneSetup : MonoBehaviour
{
  public Camera CutsceneCamera;
  public GameObject PlayerShipLocator;
  public GameObject[] OtherShipLocators;
  public Animation AnimationChild;
  public bool PlayOnEnable;
  private CutsceneEvents cutsceneEvents;

  public CutsceneEvents CutsceneEvents
  {
    get
    {
      return this.cutsceneEvents;
    }
  }

  private void Awake()
  {
    this.cutsceneEvents = this.GetComponentInChildren<CutsceneEvents>();
    foreach (Object componentsInChild in this.GetComponentsInChildren<CoolCamera>())
      Object.Destroy(componentsInChild);
  }

  private void OnEnable()
  {
    if (!this.PlayOnEnable)
      return;
    this.AnimationChild.Play();
  }
}
