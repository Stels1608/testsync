﻿// Decompiled with JetBrains decompiler
// Type: float2
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Globalization;
using UnityEngine;

public struct float2
{
  public float x;
  public float y;

  public float magnitude
  {
    get
    {
      return Mathf.Sqrt((float) ((double) this.x * (double) this.x + (double) this.y * (double) this.y));
    }
  }

  public float sqrMagnitude
  {
    get
    {
      return (float) ((double) this.x * (double) this.x + (double) this.y * (double) this.y);
    }
  }

  public float2 normalized
  {
    get
    {
      float2 float2 = this;
      float2.Normalize();
      return float2;
    }
  }

  public static float2 up
  {
    get
    {
      return new float2(0.0f, -1f);
    }
  }

  public static float2 zero
  {
    get
    {
      return new float2(0.0f, 0.0f);
    }
  }

  public float2(float _x, float _y)
  {
    this.x = _x;
    this.y = _y;
  }

  public static float2 operator *(float2 lhs, float rhs)
  {
    return new float2(lhs.x * rhs, lhs.y * rhs);
  }

  public static float2 operator *(float lhs, float2 rhs)
  {
    return new float2(lhs * rhs.x, lhs * rhs.y);
  }

  public static float2 operator /(float2 lhs, float rhs)
  {
    return new float2(lhs.x / rhs, lhs.y / rhs);
  }

  public static float2 operator +(float2 lhs, float2 rhs)
  {
    return new float2(lhs.x + rhs.x, lhs.y + rhs.y);
  }

  public static float2 operator -(float2 lhs, float2 rhs)
  {
    return new float2(lhs.x - rhs.x, lhs.y - rhs.y);
  }

  public static float2 operator -(float2 lhs)
  {
    return new float2(-lhs.x, -lhs.y);
  }

  public void Normalize()
  {
    float magnitude = this.magnitude;
    if ((double) magnitude == 0.0)
      return;
    this.x /= magnitude;
    this.y /= magnitude;
  }

  public override string ToString()
  {
    return string.Format((IFormatProvider) new NumberFormatInfo() { NumberDecimalSeparator = "." }, "x:{0};y:{1}", new object[2]{ (object) this.x, (object) this.y });
  }

  public float Angle(float2 rhs)
  {
    return float2.Angle(this, rhs);
  }

  public float Cross(float2 rhs)
  {
    return float2.Cross(this, rhs);
  }

  public static float Angle(float2 lhs, float2 rhs)
  {
    return Vector3.Angle(lhs.ToV3XZ(), rhs.ToV3XZ());
  }

  public static float Dot(float2 lhs, float2 rhs)
  {
    return (float) ((double) lhs.x * (double) rhs.x + (double) lhs.y * (double) rhs.y);
  }

  public static float Cross(float2 lhs, float2 rhs)
  {
    return (float) ((double) lhs.x * (double) rhs.y - (double) lhs.y * (double) rhs.x);
  }

  public Vector3 ToV3XZ()
  {
    return new Vector3(this.x, 0.0f, this.y);
  }

  public Vector2 ToV2()
  {
    return float2.ToV2(this);
  }

  public static Vector3 ToV3XZ(float2 vec)
  {
    return new Vector3(vec.x, 0.0f, vec.y);
  }

  public static float2 FromV3XZ(Vector3 vec)
  {
    return new float2(vec.x, vec.z);
  }

  public static Vector2 ToV2(float2 vec)
  {
    return new Vector2(vec.x, vec.y);
  }

  public static float2 FromV2(Vector2 vec)
  {
    return new float2(vec.x, vec.y);
  }
}
