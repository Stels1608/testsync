﻿// Decompiled with JetBrains decompiler
// Type: SectorSFX
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class SectorSFX : MonoBehaviour
{
  public float MissileAlarmVolume = 1f;
  public float ColonialDockSoundVolume = 1f;
  public float CylonDockSoundVolume = 1f;
  public float TargetLockSoundVolume = 1f;
  public float FTLChargingSoundVolume = 1f;
  public float NotEnoughPowerSoundVolume = 1f;
  public AudioClip MissileAlarmLoop;
  public AudioClip ColonialDockSound;
  public AudioClip CylonDockSound;
  public AudioClip TargetLockSound;
  public AudioClip FTLChargingSound;
  public AudioClip NotEnoughPowerSound;
  private AudioSource targetLockAudio;
  private AudioSource missileAlarmAudio;
  private AudioSource ftlChargingAudio;
  private float ftlCharingSoundStopTime;
  private IncomingMissiles incomingMissiles;

  public IncomingMissiles IncomingMissiles
  {
    set
    {
      if (this.incomingMissiles != null)
        this.incomingMissiles.MissileDetected -= new MissileDetectedEventHandler(this.MissileDetected);
      this.incomingMissiles = value;
      if (this.incomingMissiles == null)
        return;
      this.incomingMissiles.MissileDetected += new MissileDetectedEventHandler(this.MissileDetected);
    }
  }

  private void Awake()
  {
    this.targetLockAudio = this.CreateAudioSource();
    this.targetLockAudio.clip = this.TargetLockSound;
    this.targetLockAudio.volume = this.TargetLockSoundVolume;
    this.missileAlarmAudio = this.CreateAudioSource();
    this.missileAlarmAudio.clip = this.MissileAlarmLoop;
    this.missileAlarmAudio.volume = this.MissileAlarmVolume;
    this.ftlChargingAudio = this.CreateAudioSource();
    this.ftlChargingAudio.clip = this.FTLChargingSound;
    this.ftlChargingAudio.volume = this.FTLChargingSoundVolume;
  }

  public void MissileDetected(Missile missile)
  {
    if (this.missileAlarmAudio.isPlaying)
      return;
    this.missileAlarmAudio.Play();
  }

  [DebuggerHidden]
  private IEnumerator CallbackAfterDelay(float delay, SectorSFX.SoundTerminateCallback callback)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SectorSFX.\u003CCallbackAfterDelay\u003Ec__Iterator3A()
    {
      delay = delay,
      callback = callback,
      \u003C\u0024\u003Edelay = delay,
      \u003C\u0024\u003Ecallback = callback
    };
  }

  public void PlayTargetLockSound()
  {
    this.targetLockAudio.Play();
  }

  public void PlayFTLChargingSound(float duration)
  {
    this.StartCoroutine(this.FTLChargingLoop(duration));
  }

  public void StopFTLChargingSound()
  {
    this.ftlCharingSoundStopTime = 0.0f;
  }

  [DebuggerHidden]
  private IEnumerator FTLChargingLoop(float duration)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new SectorSFX.\u003CFTLChargingLoop\u003Ec__Iterator3B()
    {
      duration = duration,
      \u003C\u0024\u003Eduration = duration,
      \u003C\u003Ef__this = this
    };
  }

  private AudioSource CreateAudioSource()
  {
    AudioSource audioSource = this.gameObject.AddComponent<AudioSource>();
    audioSource.playOnAwake = false;
    audioSource.ignoreListenerVolume = false;
    audioSource.loop = false;
    return audioSource;
  }

  public void PlayNotEnoughPowerSound()
  {
    AudioSource audioSource = this.CreateAudioSource();
    audioSource.clip = this.NotEnoughPowerSound;
    audioSource.volume = this.NotEnoughPowerSoundVolume;
    audioSource.Play();
    Object.Destroy((Object) audioSource, this.NotEnoughPowerSound.length);
  }

  public delegate void SoundTerminateCallback();
}
