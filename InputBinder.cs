﻿// Decompiled with JetBrains decompiler
// Type: InputBinder
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class InputBinder
{
  private readonly List<IInputBinding> inputBindings;

  public List<IInputBinding> InputBindings
  {
    get
    {
      return this.inputBindings;
    }
  }

  public InputBinder(List<IInputBinding> bindingList)
  {
    this.inputBindings = bindingList;
  }

  public InputBinder(InputBinder copy)
  {
    this.inputBindings = new List<IInputBinding>((IEnumerable<IInputBinding>) copy.InputBindings);
  }

  public InputBinder()
  {
    this.inputBindings = new List<IInputBinding>();
    this.ResetToDefaults((byte) 0);
  }

  public void MergeWith(List<IInputBinding> newBindingList)
  {
    using (List<IInputBinding>.Enumerator enumerator = newBindingList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.UpdateInputBinding(enumerator.Current);
    }
  }

  public void UnbindAction(Action action, InputDevice device)
  {
    for (int index = 0; index < this.inputBindings.Count; ++index)
    {
      if (this.inputBindings[index].Action == action && this.inputBindings[index].Device == device)
        this.inputBindings[index].Unmap();
    }
  }

  public void UnbindCollidingBindings(IInputBinding newInputBinding)
  {
    for (int index = 0; index < this.inputBindings.Count; ++index)
    {
      if (this.inputBindings[index].CombinationCollidesWith(newInputBinding))
        this.inputBindings[index].Unmap();
    }
  }

  public void UpdateInputBinding(IInputBinding newInputBinding)
  {
    int index1 = -1;
    for (int index2 = 0; index2 < this.inputBindings.Count; ++index2)
    {
      if (this.SameActionOnDevice(this.inputBindings[index2], newInputBinding))
      {
        index1 = index2;
        break;
      }
    }
    if (index1 != -1)
      this.inputBindings[index1] = newInputBinding;
    else
      this.inputBindings.Add(newInputBinding);
  }

  private bool SameActionOnDevice(IInputBinding inputBinding1, IInputBinding inputBinding2)
  {
    if (inputBinding1.Action == inputBinding2.Action)
      return inputBinding1.Device == inputBinding2.Device;
    return false;
  }

  public Action TryGetKeyboardAction(KeyCode keyCode, KeyModifier modifierCode)
  {
    return this.TryGetAction(InputDevice.KeyboardMouse, (ushort) keyCode, (ushort) modifierCode);
  }

  public Action TryGetJoystickAction(JoystickButtonCode triggerCode, JoystickButtonCode modifierCode)
  {
    return this.TryGetAction(InputDevice.Joystick, (ushort) triggerCode, (ushort) modifierCode);
  }

  public Action TryGetAction(InputDevice device, ushort deviceTriggerCode, ushort deviceModifierCode)
  {
    IInputBinding binding = this.TryGetBinding(device, deviceTriggerCode, deviceModifierCode);
    if (binding == null)
      return Action.None;
    return binding.Action;
  }

  public Action TryGetActionUsingModifier(InputDevice device, ushort deviceModifierCode)
  {
    if ((int) deviceModifierCode == 0)
      return Action.None;
    using (List<IInputBinding>.Enumerator enumerator = this.InputBindings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IInputBinding current = enumerator.Current;
        if (current.IsBoundByModifier(device, deviceModifierCode))
          return current.Action;
      }
    }
    return Action.None;
  }

  public List<IInputBinding> TryGetTriggerBindingList(InputDevice device, ushort deviceTriggerCode)
  {
    List<IInputBinding> inputBindingList = new List<IInputBinding>();
    using (List<IInputBinding>.Enumerator enumerator = this.InputBindings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IInputBinding current = enumerator.Current;
        if (current.IsBoundByTrigger(device, deviceTriggerCode))
          inputBindingList.Add(current);
      }
    }
    return inputBindingList;
  }

  public List<IInputBinding> TryGetModifierBindingList(InputDevice device, ushort deviceModifierCode)
  {
    List<IInputBinding> inputBindingList = new List<IInputBinding>();
    using (List<IInputBinding>.Enumerator enumerator = this.InputBindings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IInputBinding current = enumerator.Current;
        if (current.IsBoundByModifier(device, deviceModifierCode))
          inputBindingList.Add(current);
      }
    }
    return inputBindingList;
  }

  public IInputBinding TryGetBinding(InputDevice device, ushort deviceTriggerCode, ushort deviceModifierCode)
  {
    using (List<IInputBinding>.Enumerator enumerator = this.InputBindings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IInputBinding current = enumerator.Current;
        if (current.IsBoundByCombination(device, deviceTriggerCode, deviceModifierCode))
          return current;
      }
    }
    return (IInputBinding) null;
  }

  public IInputBinding TryGetBinding(InputDevice device, Action action)
  {
    using (List<IInputBinding>.Enumerator enumerator = this.inputBindings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IInputBinding current = enumerator.Current;
        if (current.Action == action && current.Device == device)
          return current;
      }
    }
    return (IInputBinding) null;
  }

  public void ResetToDefaults(byte profile)
  {
    this.inputBindings.Clear();
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowInventory, profile, KeyCode.H));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowPilotLog, profile, KeyCode.I));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowOptions, profile, KeyCode.O));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowLeaderboard, profile, KeyCode.L));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowWingRoster, profile, KeyCode.B));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowGalaxyMap, profile, KeyCode.N));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleSystemMap3D, profile, KeyCode.M));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleSystemMap2D, profile, KeyCode.M, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.Map3DFocusYourShip, profile, KeyCode.KeypadDivide));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.Map3DBackToOverview, profile, KeyCode.KeypadMultiply));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowStatusAssignments, profile, KeyCode.A, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowDuties, profile, KeyCode.D, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowSkills, profile, KeyCode.Z, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowShipStatus, profile, KeyCode.S, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowInFlightSupply, profile, KeyCode.R, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleTournamentRanking, profile, KeyCode.Tab));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleWindowTutorial, profile, KeyCode.H, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.NearestEnemy, profile, KeyCode.X));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.NearestFriendly, profile, KeyCode.None));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.CancelTarget, profile, KeyCode.C));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.SelectNearestMissile, profile, KeyCode.Z));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.SelectNearestMine, profile, KeyCode.None));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.Jump, profile, KeyCode.J));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.CancelJump, profile, KeyCode.K));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.TargetCamera, profile, KeyCode.T, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ChaseCamera, profile, KeyCode.C, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.FreeCamera, profile, KeyCode.F, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.NoseCamera, profile, KeyCode.N, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleCamera, profile, KeyCode.V));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleShipName, profile, KeyCode.P));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleCombatGUI, profile, KeyCode.G, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleGuns, profile, KeyCode.G));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.FireMissiles, profile, KeyCode.F));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.TurnOrSlideLeft, profile, KeyCode.A));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.TurnOrSlideRight, profile, KeyCode.D));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.SlopeForwardOrSlideUp, profile, KeyCode.W));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.SlopeBackwardOrSlideDown, profile, KeyCode.S));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.RollLeft, profile, KeyCode.Q));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.RollRight, profile, KeyCode.E));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AlignToHorizon, profile, KeyCode.Period));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.SpeedUp, profile, KeyCode.Equals));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.SlowDown, profile, KeyCode.Minus));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ZoomIn, profile, KeyCode.Minus, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ZoomOut, profile, KeyCode.Equals, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.Boost, profile, KeyCode.Space));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.FullSpeed, profile, KeyCode.PageUp));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.Stop, profile, KeyCode.PageDown));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.Follow, profile, KeyCode.Y));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.MatchSpeed, profile, KeyCode.T));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleMovementMode, profile, KeyCode.Mouse2));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.FocusChat, profile, KeyCode.Return));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.UnfocusChat, profile, KeyCode.Return, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.Reply, profile, KeyCode.R));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot1, profile, KeyCode.Alpha1));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot2, profile, KeyCode.Alpha2));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot3, profile, KeyCode.Alpha3));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot4, profile, KeyCode.Alpha4));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot5, profile, KeyCode.Alpha5));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot6, profile, KeyCode.Alpha6));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot7, profile, KeyCode.Alpha7));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot8, profile, KeyCode.Alpha8));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot9, profile, KeyCode.Alpha9));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot10, profile, KeyCode.Alpha0));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot11, profile, KeyCode.LeftBracket));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.WeaponSlot12, profile, KeyCode.RightBracket));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AbilitySlot1, profile, KeyCode.Alpha1, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AbilitySlot2, profile, KeyCode.Alpha2, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AbilitySlot3, profile, KeyCode.Alpha3, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AbilitySlot4, profile, KeyCode.Alpha4, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AbilitySlot5, profile, KeyCode.Alpha5, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AbilitySlot6, profile, KeyCode.Alpha6, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AbilitySlot7, profile, KeyCode.Alpha7, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AbilitySlot8, profile, KeyCode.Alpha8, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AbilitySlot9, profile, KeyCode.Alpha9, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.AbilitySlot10, profile, KeyCode.Alpha0, KeyModifier.Shift));
    this.inputBindings.Add((IInputBinding) new KeyBinding(Action.ToggleFps, profile, KeyCode.P, KeyModifier.Shift));
  }

  public void LoadPreset(BindingPreset preset)
  {
    switch (preset)
    {
      case BindingPreset.Xbox360Default:
        this.ApplyJoystickBindings(this.CreateXbox360BindingsDefault((byte) 0), (byte) 0);
        break;
      case BindingPreset.Xbox360Echelon:
        this.ApplyJoystickBindings(this.CreateXbox360BindingsEchelon((byte) 0), (byte) 0);
        break;
      case BindingPreset.Xbox360Striker:
        this.ApplyJoystickBindings(this.CreateXbox360BindingsStriker((byte) 0), (byte) 0);
        break;
      case BindingPreset.Xbox360Vanguard:
        this.ApplyJoystickBindings(this.CreateXbox360BindingsVanguard((byte) 0), (byte) 0);
        break;
      default:
        Debug.LogError((object) "Not a valid profile");
        break;
    }
  }

  private void ApplyJoystickBindings(List<JoystickBinding> joystickBindingList, byte profileNo)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    InputBinder.\u003CApplyJoystickBindings\u003Ec__AnonStorey9F bindingsCAnonStorey9F = new InputBinder.\u003CApplyJoystickBindings\u003Ec__AnonStorey9F();
    // ISSUE: reference to a compiler-generated field
    bindingsCAnonStorey9F.profileNo = profileNo;
    // ISSUE: reference to a compiler-generated method
    this.inputBindings.RemoveAll(new Predicate<IInputBinding>(bindingsCAnonStorey9F.\u003C\u003Em__148));
    using (List<JoystickBinding>.Enumerator enumerator = joystickBindingList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.inputBindings.Add((IInputBinding) enumerator.Current);
    }
    List<IInputBinding> inputBindingList = new List<IInputBinding>((IEnumerable<IInputBinding>) this.inputBindings);
    inputBindingList.RemoveAll((Predicate<IInputBinding>) (binding => !(binding is JoystickBinding)));
    using (List<IInputBinding>.Enumerator enumerator1 = inputBindingList.GetEnumerator())
    {
      while (enumerator1.MoveNext())
      {
        JoystickBinding joystickBinding1 = (JoystickBinding) enumerator1.Current;
        using (List<IInputBinding>.Enumerator enumerator2 = inputBindingList.GetEnumerator())
        {
          while (enumerator2.MoveNext())
          {
            JoystickBinding joystickBinding2 = (JoystickBinding) enumerator2.Current;
            if (joystickBinding1 != joystickBinding2 && joystickBinding1.CombinationCollidesWith((IInputBinding) joystickBinding2))
              Debug.LogError((object) ("Binding " + (object) joystickBinding1 + " collides with " + (object) joystickBinding2));
          }
        }
      }
    }
    foreach (int num in Enum.GetValues(typeof (Action)))
    {
      Action action = (Action) num;
      bool flag = false;
      using (List<IInputBinding>.Enumerator enumerator = this.InputBindings.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          IInputBinding current = enumerator.Current;
          // ISSUE: reference to a compiler-generated field
          if (current.Device == InputDevice.Joystick && (int) current.ProfileNo == (int) bindingsCAnonStorey9F.profileNo && current.Action == action)
            flag = true;
        }
      }
      if (!flag)
      {
        // ISSUE: reference to a compiler-generated field
        this.inputBindings.Add((IInputBinding) new JoystickBinding(action, bindingsCAnonStorey9F.profileNo, JoystickButtonCode.None, JoystickButtonCode.None));
      }
    }
  }

  private List<JoystickBinding> CreateXbox360BindingsDefault(byte profileNo)
  {
    return new List<JoystickBinding>() { new JoystickBinding(Action.NearestEnemy, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button1), new JoystickBinding(Action.SelectScreenCenterObject, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button0), new JoystickBinding(Action.CancelTarget, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button2), new JoystickBinding(Action.SelectNearestMissile, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button3), new JoystickBinding(Action.ToggleGunsOnHold, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis10_Positive), new JoystickBinding(Action.FireMissiles, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis9_Positive), new JoystickBinding(Action.AbilitySlot1, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis7_Positive), new JoystickBinding(Action.AbilitySlot2, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis6_Positive), new JoystickBinding(Action.AbilitySlot3, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis7_Negative), new JoystickBinding(Action.AbilitySlot4, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis6_Negative), new JoystickBinding(Action.AbilitySlot5, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis7_Positive), new JoystickBinding(Action.AbilitySlot6, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis6_Positive), new JoystickBinding(Action.AbilitySlot7, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis7_Negative), new JoystickBinding(Action.AbilitySlot8, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis6_Negative), new JoystickBinding(Action.JoystickTurnLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisX_Negative), new JoystickBinding(Action.JoystickTurnRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisX_Positive), new JoystickBinding(Action.JoystickTurnUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisY_Negative), new JoystickBinding(Action.JoystickTurnDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisY_Positive), new JoystickBinding(Action.JoystickStrafeLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis4_Negative), new JoystickBinding(Action.JoystickStrafeRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis4_Positive), new JoystickBinding(Action.JoystickStrafeUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis5_Positive), new JoystickBinding(Action.JoystickStrafeDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis5_Negative), new JoystickBinding(Action.JoystickLookLeft, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis4_Negative), new JoystickBinding(Action.JoystickLookRight, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis4_Positive), new JoystickBinding(Action.JoystickLookUp, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis5_Negative), new JoystickBinding(Action.JoystickLookDown, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis5_Positive), new JoystickBinding(Action.ToggleBoost, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button9), new JoystickBinding(Action.SpeedUp, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Button1), new JoystickBinding(Action.SlowDown, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Button0), new JoystickBinding(Action.AlignToHorizon, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button8), new JoystickBinding(Action.FullSpeed, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Button3), new JoystickBinding(Action.MatchSpeed, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Button2) };
  }

  private List<JoystickBinding> CreateXbox360BindingsEchelon(byte profileNo)
  {
    return new List<JoystickBinding>() { new JoystickBinding(Action.NearestEnemy, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button2), new JoystickBinding(Action.SelectScreenCenterObject, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.CancelTarget, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Button2), new JoystickBinding(Action.SelectNearestMissile, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.ToggleGunsOnHold, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis10_Positive), new JoystickBinding(Action.FireMissiles, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis9_Positive), new JoystickBinding(Action.AbilitySlot1, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis6_Negative), new JoystickBinding(Action.AbilitySlot2, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis7_Positive), new JoystickBinding(Action.AbilitySlot3, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis6_Positive), new JoystickBinding(Action.AbilitySlot4, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis7_Negative), new JoystickBinding(Action.AbilitySlot5, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis6_Negative), new JoystickBinding(Action.AbilitySlot6, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis7_Positive), new JoystickBinding(Action.AbilitySlot7, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis6_Positive), new JoystickBinding(Action.AbilitySlot8, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis7_Negative), new JoystickBinding(Action.JoystickTurnLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis4_Negative), new JoystickBinding(Action.JoystickTurnRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis4_Positive), new JoystickBinding(Action.JoystickTurnUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisY_Positive), new JoystickBinding(Action.JoystickTurnDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisY_Negative), new JoystickBinding(Action.JoystickRollLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisX_Negative), new JoystickBinding(Action.JoystickRollRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisX_Positive), new JoystickBinding(Action.JoystickStrafeLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.JoystickStrafeRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.JoystickStrafeUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis5_Negative), new JoystickBinding(Action.JoystickStrafeDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis5_Positive), new JoystickBinding(Action.JoystickLookLeft, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis4_Negative), new JoystickBinding(Action.JoystickLookRight, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis4_Positive), new JoystickBinding(Action.JoystickLookUp, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis5_Negative), new JoystickBinding(Action.JoystickLookDown, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis5_Positive), new JoystickBinding(Action.ToggleBoost, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button9), new JoystickBinding(Action.SpeedUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button0), new JoystickBinding(Action.SlowDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button1), new JoystickBinding(Action.AlignToHorizon, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button8), new JoystickBinding(Action.FullSpeed, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Button0), new JoystickBinding(Action.MatchSpeed, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Button1) };
  }

  private List<JoystickBinding> CreateXbox360BindingsStriker(byte profileNo)
  {
    return new List<JoystickBinding>() { new JoystickBinding(Action.NearestEnemy, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis6_Negative), new JoystickBinding(Action.SelectScreenCenterObject, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis7_Positive), new JoystickBinding(Action.CancelTarget, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis6_Positive), new JoystickBinding(Action.SelectNearestMissile, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis6_Negative), new JoystickBinding(Action.SelectNearestMine, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Axis6_Positive), new JoystickBinding(Action.ToggleGuns, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button7), new JoystickBinding(Action.ToggleGunsOnHold, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.FireMissiles, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button8), new JoystickBinding(Action.AbilitySlot1, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button0), new JoystickBinding(Action.AbilitySlot2, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button2), new JoystickBinding(Action.AbilitySlot3, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button3), new JoystickBinding(Action.AbilitySlot4, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button1), new JoystickBinding(Action.AbilitySlot5, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Button0), new JoystickBinding(Action.AbilitySlot6, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Button2), new JoystickBinding(Action.AbilitySlot7, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Button3), new JoystickBinding(Action.AbilitySlot8, profileNo, JoystickButtonCode.Button5, JoystickButtonCode.Button1), new JoystickBinding(Action.JoystickTurnLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis4_Negative), new JoystickBinding(Action.JoystickTurnRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis4_Positive), new JoystickBinding(Action.JoystickTurnUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis5_Positive), new JoystickBinding(Action.JoystickTurnDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis5_Negative), new JoystickBinding(Action.JoystickRollLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis9_Positive), new JoystickBinding(Action.JoystickRollRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis10_Positive), new JoystickBinding(Action.JoystickStrafeLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisX_Negative), new JoystickBinding(Action.JoystickStrafeRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisX_Positive), new JoystickBinding(Action.JoystickStrafeUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisY_Negative), new JoystickBinding(Action.JoystickStrafeDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisY_Positive), new JoystickBinding(Action.JoystickLookLeft, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis4_Negative), new JoystickBinding(Action.JoystickLookRight, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis4_Positive), new JoystickBinding(Action.JoystickLookUp, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis5_Negative), new JoystickBinding(Action.JoystickLookDown, profileNo, JoystickButtonCode.Button4, JoystickButtonCode.Axis5_Positive), new JoystickBinding(Action.ToggleBoost, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button9), new JoystickBinding(Action.SpeedUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.SlowDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.AlignToHorizon, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.FullSpeed, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis7_Positive), new JoystickBinding(Action.Stop, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis7_Negative), new JoystickBinding(Action.MatchSpeed, profileNo, JoystickButtonCode.None, JoystickButtonCode.None) };
  }

  private List<JoystickBinding> CreateXbox360BindingsVanguard(byte profileNo)
  {
    return new List<JoystickBinding>() { new JoystickBinding(Action.NearestEnemy, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button1), new JoystickBinding(Action.NearestFriendly, profileNo, JoystickButtonCode.Button3, JoystickButtonCode.Axis7_Positive), new JoystickBinding(Action.SelectScreenCenterObject, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.CancelTarget, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button6), new JoystickBinding(Action.SelectNearestMissile, profileNo, JoystickButtonCode.Button3, JoystickButtonCode.Axis6_Negative), new JoystickBinding(Action.SelectNearestMine, profileNo, JoystickButtonCode.Button3, JoystickButtonCode.Axis6_Positive), new JoystickBinding(Action.ToggleGunsOnHold, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis10_Positive), new JoystickBinding(Action.FireMissiles, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis9_Positive), new JoystickBinding(Action.AbilitySlot1, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis7_Positive), new JoystickBinding(Action.AbilitySlot2, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis6_Positive), new JoystickBinding(Action.AbilitySlot3, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis7_Negative), new JoystickBinding(Action.AbilitySlot4, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis6_Negative), new JoystickBinding(Action.AbilitySlot5, profileNo, JoystickButtonCode.Button2, JoystickButtonCode.Axis7_Positive), new JoystickBinding(Action.AbilitySlot6, profileNo, JoystickButtonCode.Button2, JoystickButtonCode.Axis6_Positive), new JoystickBinding(Action.AbilitySlot7, profileNo, JoystickButtonCode.Button2, JoystickButtonCode.Axis7_Negative), new JoystickBinding(Action.AbilitySlot8, profileNo, JoystickButtonCode.Button2, JoystickButtonCode.Axis6_Negative), new JoystickBinding(Action.JoystickTurnLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis4_Negative), new JoystickBinding(Action.JoystickTurnRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis4_Positive), new JoystickBinding(Action.JoystickTurnUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisY_Positive), new JoystickBinding(Action.JoystickTurnDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisY_Negative), new JoystickBinding(Action.JoystickRollLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisX_Negative), new JoystickBinding(Action.JoystickRollRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.AxisX_Positive), new JoystickBinding(Action.JoystickStrafeUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis5_Negative), new JoystickBinding(Action.JoystickStrafeDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.Axis5_Positive), new JoystickBinding(Action.JoystickLookLeft, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.JoystickLookRight, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.JoystickLookUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.JoystickLookDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.Boost, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button9), new JoystickBinding(Action.ToggleBoost, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button0), new JoystickBinding(Action.SpeedUp, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button5), new JoystickBinding(Action.SlowDown, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button4), new JoystickBinding(Action.Follow, profileNo, JoystickButtonCode.Button3, JoystickButtonCode.Axis7_Negative), new JoystickBinding(Action.AlignToHorizon, profileNo, JoystickButtonCode.None, JoystickButtonCode.Button8), new JoystickBinding(Action.FullSpeed, profileNo, JoystickButtonCode.None, JoystickButtonCode.None), new JoystickBinding(Action.MatchSpeed, profileNo, JoystickButtonCode.None, JoystickButtonCode.None) };
  }

  public void Print()
  {
    using (List<IInputBinding>.Enumerator enumerator = this.InputBindings.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        IInputBinding current = enumerator.Current;
        Debug.Log((object) string.Format("{0}->{1}", (object) current.Action.ToString(), (object) current.BindingAlias));
      }
    }
  }
}
