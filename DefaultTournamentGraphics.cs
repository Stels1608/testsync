﻿// Decompiled with JetBrains decompiler
// Type: DefaultTournamentGraphics
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DefaultTournamentGraphics : ITournamentGraphics
{
  public UnityEngine.Sprite EnterIconNormal
  {
    get
    {
      return Resources.Load<UnityEngine.Sprite>(Game.Me.Faction != Faction.Cylon ? "GUI/hub_menu/system_buttons/tournament_colonial" : "GUI/hub_menu/system_buttons/tournament_cylon");
    }
  }

  public UnityEngine.Sprite EnterIconOver
  {
    get
    {
      return this.EnterIconNormal;
    }
  }

  public string LoadingScreen
  {
    get
    {
      return "GUI/LoadingScene/tournament_strike";
    }
  }
}
