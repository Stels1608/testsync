﻿// Decompiled with JetBrains decompiler
// Type: SettingTextCheckboxUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SettingTextCheckboxUgui : MonoBehaviour, ISettingsReceiver
{
  private bool notifyListeners = true;
  [SerializeField]
  private Toggle toggle;
  [SerializeField]
  private Text settingName;
  private UserSetting setting;
  private Array options;

  public UserSetting Setting
  {
    get
    {
      return this.setting;
    }
  }

  public void Initialize(UserSetting setting)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SettingTextCheckboxUgui.\u003CInitialize\u003Ec__AnonStorey12A initializeCAnonStorey12A = new SettingTextCheckboxUgui.\u003CInitialize\u003Ec__AnonStorey12A();
    // ISSUE: reference to a compiler-generated field
    initializeCAnonStorey12A.setting = setting;
    // ISSUE: reference to a compiler-generated field
    initializeCAnonStorey12A.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    this.setting = initializeCAnonStorey12A.setting;
    // ISSUE: reference to a compiler-generated method
    this.toggle.onValueChanged.AddListener(new UnityAction<bool>(initializeCAnonStorey12A.\u003C\u003Em__2C6));
    this.LocalizeSettingName();
    FacadeFactory.GetInstance().SendMessage(Message.RegisterSettingsReceiver, (object) this);
  }

  private void OnDestroy()
  {
    FacadeFactory.GetInstance().SendMessage(Message.UnregisterSettingsReceiver, (object) this);
  }

  private void LocalizeSettingName()
  {
    this.settingName.text = Tools.GetLocalized(this.setting);
  }

  private void ApplyOption(object option)
  {
    bool flag = (bool) option;
    this.notifyListeners = false;
    this.toggle.isOn = flag;
    this.notifyListeners = true;
  }

  public void ReceiveSettings(UserSettings userSettings)
  {
    this.ApplyOption(userSettings.Get(this.setting));
  }

  public void ReceiveSetting(UserSetting userSetting, object data)
  {
    if (userSetting != this.setting)
      return;
    this.ApplyOption(data);
  }
}
