﻿// Decompiled with JetBrains decompiler
// Type: ShipBuilder
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ShipBuilder : MonoBehaviour
{
  public int bundleDBSelectionIndex = -1;
  public bool isPlayableShip = true;
  public string soundSetResource = string.Empty;
  public int audioPoolCapacity = 1;
  public string createAssetbundle = string.Empty;
  public int selectedCouchShip = -1;
  public int lastSelectedCouchShip = -1;
  public List<ShipBuilderSpot> shipBuilderSpotList = new List<ShipBuilderSpot>();
  public List<ShipBuilderPaperdollSmallSpot> shipBuilderPaperdollSmallSpot = new List<ShipBuilderPaperdollSmallSpot>();
  public bool hasBuiltShip;
  public bool generateShopPrefab;
  public bool generateWrekPrefab;
  public string[] couchDBShips;
  public string[] dummyList;

  private void Start()
  {
    Object.Destroy((Object) this);
  }

  public void Renumerate()
  {
    for (int index = 0; index < this.shipBuilderSpotList.Count; ++index)
      this.shipBuilderSpotList[index].place_id = index;
  }
}
