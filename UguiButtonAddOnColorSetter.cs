﻿// Decompiled with JetBrains decompiler
// Type: UguiButtonAddOnColorSetter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UguiButtonAddOnColorSetter : MonoBehaviour
{
  [SerializeField]
  private Color defaultColor = new Color((float) byte.MaxValue, (float) byte.MaxValue, (float) byte.MaxValue);
  [SerializeField]
  private bool defaultColorOnDisable = true;
  [SerializeField]
  private Text textComponent;
  [SerializeField]
  private TextMeshProUGUI textComponentTMP;
  [SerializeField]
  private Image imageComponent;
  [SerializeField]
  private Button button;
  private string colorCached;

  public void SetTextColor(string hexColor)
  {
    if (!this.button.interactable)
    {
      this.colorCached = hexColor;
    }
    else
    {
      if ((Object) this.textComponent != (Object) null)
        this.textComponent.color = ImageUtils.HexStringToColor(hexColor);
      if ((Object) this.textComponentTMP != (Object) null)
        this.textComponentTMP.color = ImageUtils.HexStringToColor(hexColor);
      if (!((Object) this.imageComponent != (Object) null))
        return;
      this.imageComponent.color = ImageUtils.HexStringToColor(hexColor);
    }
  }

  private void OnDisable()
  {
    if (!this.defaultColorOnDisable)
      return;
    this.SetTextColor(Tools.ColorToHexRGB((Color32) this.defaultColor));
  }

  private void Update()
  {
    if (this.colorCached == null || !this.button.interactable)
      return;
    this.SetTextColor(this.colorCached);
    this.colorCached = (string) null;
  }
}
