﻿// Decompiled with JetBrains decompiler
// Type: DebugManeuver
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.IO;
using UnityEngine;

public class DebugManeuver : MonoBehaviour
{
  private Quaternion[] step = new Quaternion[7]{ Quaternion.LookRotation(Vector3.forward, Vector3.up), Quaternion.LookRotation(Vector3.forward, Vector3.up + new Vector3(0.5f, 0.0f, 0.0f)), Quaternion.LookRotation(Vector3.forward, Vector3.up + new Vector3(-0.5f, 0.0f, 0.0f)), Quaternion.LookRotation(Vector3.forward, Vector3.up), Quaternion.LookRotation(Vector3.down, Vector3.forward), Quaternion.LookRotation(Vector3.back, Vector3.up), Quaternion.LookRotation(Vector3.back, Vector3.up) };
  private Quaternion begin;
  private Quaternion end;
  private int cur;
  private float timeStart;
  private float timeEnd;

  private void Awake()
  {
    this.Test();
    this.Init();
  }

  private void Init()
  {
    this.timeStart = Time.time;
    this.timeEnd = this.timeStart + 1f;
    this.cur = this.cur < this.step.Length - 1 ? this.cur : 0;
    this.begin = this.step[this.cur];
    this.end = this.step[this.cur + 1];
    ++this.cur;
  }

  private void Update()
  {
    if ((double) this.timeEnd < (double) Time.time)
      this.Init();
    this.transform.rotation = Quaternion.Lerp(this.begin, this.end, Mathf.Clamp01((float) (((double) Time.time - (double) this.timeStart) / ((double) this.timeEnd - (double) this.timeStart))));
  }

  private void Test()
  {
    StreamWriter sw = new StreamWriter("123.txt");
    Quaternion quaternion1 = Quaternion.LookRotation(Vector3.forward);
    Quaternion quaternion2 = Quaternion.LookRotation(Vector3.right);
    this.Write(sw, quaternion1);
    this.Write(sw, quaternion2);
    sw.WriteLine(string.Format("{0:0.000000}", (object) Quaternion.Angle(quaternion1, quaternion2)));
    sw.Close();
  }

  private void Write(StreamWriter sw, Quaternion q)
  {
    sw.WriteLine(string.Format("{0:0.000000}, {1:0.000000}, {2:0.000000}, {3:0.000000}", (object) q.x, (object) q.y, (object) q.z, (object) q.w));
  }
}
