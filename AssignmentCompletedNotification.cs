﻿// Decompiled with JetBrains decompiler
// Type: AssignmentCompletedNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class AssignmentCompletedNotification : OnScreenNotification
{
  private readonly Mission mission;

  public Mission Mission
  {
    get
    {
      return this.mission;
    }
  }

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.AssigmentCompleted;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Positive;
    }
  }

  public override string TextMessage
  {
    get
    {
      return BsgoLocalization.Get("%$bgo.etc.assignment_completed%", (object) this.Mission.guiCard.Name);
    }
  }

  public override bool Show
  {
    get
    {
      return OnScreenNotification.ShowAssignmentMessages;
    }
  }

  public AssignmentCompletedNotification(Mission mission)
  {
    this.mission = mission;
  }
}
