﻿// Decompiled with JetBrains decompiler
// Type: CounterDesc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class CounterDesc : IProtocolRead
{
  public uint GUID;
  public int Value;

  public void Read(BgoProtocolReader r)
  {
    this.GUID = r.ReadGUID();
    this.Value = r.ReadInt32();
  }
}
