﻿// Decompiled with JetBrains decompiler
// Type: GuiItem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GuiItem : GuiPanel
{
  private readonly GuiAtlasImage image = new GuiAtlasImage();
  private readonly GuiLabel label = new GuiLabel(Gui.Options.fontEurostileTRegCon);

  public GuiItem(GUICard guiCard, string text)
  {
    this.image.GuiCard = guiCard;
    this.label.Text = text;
    this.label.FontSize = 14;
    this.label.NormalColor = Color.white;
    this.AddChild((GuiElementBase) this.image, Align.MiddleLeft);
    this.AddChild((GuiElementBase) this.label, Align.MiddleLeft, new Vector2(50f, 0.0f));
    if (guiCard != null)
      this.SetTooltip(guiCard.Description);
    this.SizeX = (float) ((double) this.label.PositionX + (double) this.label.SizeX + 10.0);
    this.SizeY = Mathf.Max(this.image.SizeY, this.label.SizeY);
  }

  public GuiItem(ShipItem shipItem)
    : this(shipItem.ItemGUICard, (!(shipItem is ItemCountable) ? string.Empty : ((ItemCountable) shipItem).Count.ToString() + " ") + shipItem.ItemGUICard.Name)
  {
  }
}
