﻿// Decompiled with JetBrains decompiler
// Type: CountableDisplay
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CountableDisplay : MonoBehaviour
{
  [SerializeField]
  private ItemIcon icon;
  [SerializeField]
  private UILabel amountLabel;
  [SerializeField]
  private UILabel nameLabel;

  public UILabel AmountLabel
  {
    get
    {
      return this.amountLabel;
    }
  }

  public void DisplayAmount(GUICard guiCard, uint amount)
  {
    this.icon.SetGUICard(guiCard);
    this.AmountLabel.text = amount.ToString();
    if (!((Object) this.nameLabel != (Object) null))
      return;
    this.nameLabel.text = guiCard.Name;
  }

  public void DisplayAmount(ItemCountable countable)
  {
    this.DisplayAmount(countable.ItemGUICard, countable.count);
  }
}
