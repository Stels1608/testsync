﻿// Decompiled with JetBrains decompiler
// Type: CometExplosion
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CometExplosion : MonoBehaviour
{
  public GameObject ExplosionPrefab;

  public void Explode()
  {
    Object.Instantiate((Object) this.ExplosionPrefab, this.transform.position, this.transform.rotation);
  }
}
