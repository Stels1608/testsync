﻿// Decompiled with JetBrains decompiler
// Type: KeyBinding
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class KeyBinding : IInputBinding
{
  private readonly Action action;
  private KeyCode keyCode;
  private KeyModifier keyModifier;
  private readonly InputDevice device;
  private KeyFlags flags;
  private byte profileNo;

  public byte ProfileNo
  {
    get
    {
      return this.profileNo;
    }
  }

  public Action Action
  {
    get
    {
      return this.action;
    }
  }

  public KeyCode KeyCode
  {
    get
    {
      return this.keyCode;
    }
  }

  public KeyModifier KeyModifier
  {
    get
    {
      return this.keyModifier;
    }
  }

  public InputDevice Device
  {
    get
    {
      return this.device;
    }
  }

  public byte Flags
  {
    get
    {
      return (byte) this.flags;
    }
  }

  public ushort DeviceTriggerCode
  {
    get
    {
      return (ushort) this.keyCode;
    }
  }

  public ushort DeviceModifierCode
  {
    get
    {
      return (ushort) this.keyModifier;
    }
  }

  public bool IsUnbound
  {
    get
    {
      return this.keyCode == KeyCode.None;
    }
  }

  public string TriggerAlias
  {
    get
    {
      if (this.KeyCode >= KeyCode.Alpha0 && this.KeyCode <= KeyCode.Alpha9)
        return ((int) (this.KeyCode - 48)).ToString();
      if (this.KeyCode >= KeyCode.Keypad0 && this.KeyCode <= KeyCode.Keypad9)
        return "Num" + (object) (this.KeyCode - 256);
      KeyCode keyCode = this.KeyCode;
      switch (keyCode)
      {
        case KeyCode.Quote:
          return "'";
        case KeyCode.Comma:
          return ",";
        case KeyCode.Minus:
          return "-";
        case KeyCode.Period:
          return ".";
        case KeyCode.Slash:
          return "/";
        default:
          switch (keyCode - 303)
          {
            case KeyCode.None:
              return "Shift";
            case (KeyCode) 1:
              return "Shift";
            case (KeyCode) 2:
              return "Ctrl";
            case (KeyCode) 3:
              return "Ctrl";
            case (KeyCode) 6:
              return "Windows";
            case (KeyCode) 7:
              return "Windows";
            default:
              switch (keyCode - 91)
              {
                case KeyCode.None:
                  return "{";
                case (KeyCode) 1:
                  return "\\";
                case (KeyCode) 2:
                  return "}";
                case (KeyCode) 5:
                  return "~";
                default:
                  switch (keyCode - 58)
                  {
                    case KeyCode.None:
                      return ":";
                    case (KeyCode) 1:
                      return ";";
                    case (KeyCode) 2:
                      return "<";
                    case (KeyCode) 3:
                      return "=";
                    case (KeyCode) 4:
                      return ">";
                    default:
                      if (keyCode == KeyCode.KeypadDivide)
                        return "Numpad /";
                      if (keyCode == KeyCode.KeypadMultiply)
                        return "Numpad *";
                      if (keyCode == KeyCode.None)
                        return BsgoLocalization.Get("%$bgo.ui.options.popup.keybinding.key.unmapped%");
                      if (keyCode == KeyCode.Return)
                        return "Enter";
                      return this.KeyCode.ToString();
                  }
              }
          }
      }
    }
  }

  public string ModifierAlias
  {
    get
    {
      switch (this.keyModifier)
      {
        case KeyModifier.Shift:
          return "SHIFT";
        case KeyModifier.Control:
          return "CTRL";
        default:
          return string.Empty;
      }
    }
  }

  public string BindingAlias
  {
    get
    {
      return (!string.IsNullOrEmpty(this.ModifierAlias) ? this.ModifierAlias + " + " : string.Empty) + this.TriggerAlias;
    }
  }

  public KeyBinding(Action action, byte profileNo)
    : this(action, profileNo, KeyCode.None, KeyModifier.None, KeyFlags.None)
  {
  }

  public KeyBinding(Action action, byte profileNo, KeyCode keyCode)
    : this(action, profileNo, keyCode, KeyModifier.None, KeyFlags.None)
  {
  }

  public KeyBinding(Action action, byte profileNo, KeyCode keyCode, KeyModifier keyModifier)
    : this(action, profileNo, keyCode, keyModifier, KeyFlags.None)
  {
  }

  public KeyBinding(KeyBinding source)
    : this(source.action, source.profileNo, source.keyCode, source.keyModifier, source.flags)
  {
  }

  public KeyBinding(Action action, byte profileNo, KeyCode keyCode, KeyModifier keyModifier, KeyFlags flags)
  {
    this.action = action;
    this.keyCode = keyCode;
    this.keyModifier = keyModifier;
    this.device = InputDevice.KeyboardMouse;
    this.flags = flags;
    this.profileNo = profileNo;
  }

  public IInputBinding Copy()
  {
    return (IInputBinding) new KeyBinding(this);
  }

  public void Unmap()
  {
    this.keyCode = KeyCode.None;
    this.keyModifier = KeyModifier.None;
  }

  public void SetKeyCode(KeyCode newKeyCode)
  {
    this.keyCode = newKeyCode;
  }

  public void SetModifier(KeyModifier newKeyModifier)
  {
    this.keyModifier = newKeyModifier;
  }

  public static bool KeyCodeIsJoystickButton(KeyCode keyCode)
  {
    if (keyCode >= KeyCode.JoystickButton0)
      return keyCode <= KeyCode.Joystick4Button19;
    return false;
  }

  public bool IsBoundByTrigger(InputDevice device, ushort deviceTriggerCode)
  {
    if ((int) this.DeviceTriggerCode == (int) deviceTriggerCode)
      return this.device == device;
    return false;
  }

  public bool IsBoundByModifier(InputDevice device, ushort deviceModifierCode)
  {
    if ((int) this.DeviceModifierCode == (int) deviceModifierCode)
      return this.device == device;
    return false;
  }

  public bool IsBoundByCombination(InputDevice device, ushort deviceTriggerCode, ushort deviceModifierCode)
  {
    if ((int) this.DeviceTriggerCode == (int) deviceTriggerCode && (int) this.DeviceModifierCode == (int) deviceModifierCode)
      return this.device == device;
    return false;
  }

  public bool IsBoundByCombinationOf(IInputBinding otherInputBinding)
  {
    return this.IsBoundByCombination(otherInputBinding.Device, otherInputBinding.DeviceTriggerCode, otherInputBinding.DeviceModifierCode);
  }

  public bool CombinationCollidesWith(IInputBinding otherInputBinding)
  {
    return this.IsBoundByCombinationOf(otherInputBinding);
  }

  public override int GetHashCode()
  {
    return this.KeyCode.GetHashCode() * 397 ^ this.keyModifier.GetHashCode();
  }

  public override string ToString()
  {
    return "HotKey - Action: " + (object) this.action + " Modifier: " + (object) this.keyModifier + " Key: " + (object) this.KeyCode + " Device: " + (object) this.device;
  }
}
