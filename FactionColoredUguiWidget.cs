﻿// Decompiled with JetBrains decompiler
// Type: FactionColoredUguiWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

public class FactionColoredUguiWidget : FactionColoredWidget
{
  public bool useAlternateHoverColor;

  private void Start()
  {
    this.Colorize();
  }

  public override void Colorize()
  {
    Component component1;
    if ((Object) (component1 = (Component) this.GetComponent<Button>()) != (Object) null)
    {
      this.ColorizeButton(component1 as Button);
    }
    else
    {
      Component component2;
      if (!((Object) (component2 = (Component) this.GetComponent<Image>()) != (Object) null))
        return;
      this.ColorizeImage(component2 as Image);
    }
  }

  private void ColorizeButton(Button button)
  {
    Color color1 = ColorManager.currentColorScheme.Get(WidgetColorType.UGUI_FACTION_COLOR_NORMAL);
    Color color2 = !this.useAlternateHoverColor ? ColorManager.currentColorScheme.Get(WidgetColorType.UGUI_FACTION_COLOR_CLICK) : ColorManager.currentColorScheme.Get(WidgetColorType.UGUI_FACTION_COLOR_NORMAL) * 1.5f;
    Color color3 = !this.useAlternateHoverColor ? ColorManager.currentColorScheme.Get(WidgetColorType.UGUI_FACTION_COLOR_CLICK) : ColorManager.currentColorScheme.Get(WidgetColorType.UGUI_FACTION_COLOR_NORMAL) * 2f;
    Color color4 = ColorManager.currentColorScheme.Get(WidgetColorType.UGUI_FACTION_COLOR_DISABLED);
    button.colors = new ColorBlock()
    {
      colorMultiplier = 2f,
      fadeDuration = 0.05f,
      disabledColor = color4,
      highlightedColor = color2,
      normalColor = color1,
      pressedColor = color3
    };
  }

  private void ColorizeImage(Image sprite)
  {
    sprite.color = ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
  }
}
