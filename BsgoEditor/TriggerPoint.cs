﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.TriggerPoint
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor
{
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  [JsonClassDesc("gameObject/name", JsonName = "name")]
  [JsonClassDesc("radius", JsonName = "radius")]
  [JsonClassDesc("gameObject/transform/position", JsonName = "position")]
  public class TriggerPoint
  {
    public GameObject gameObject;
    public float radius;

    public TriggerPoint()
    {
      this.gameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
      this.gameObject.name = "TriggeringPoint";
      this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
      this.radius = 500f;
      this.gameObject.GetComponent<Renderer>().material.color = new Color(1f, 1f, 0.0f, 0.5f);
      this.gameObject.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
      this.gameObject.transform.localScale = new Vector3(this.radius * 2f, this.radius * 2f, this.radius * 2f);
      Object.Destroy((Object) this.gameObject.GetComponent<Collider>());
    }

    public TriggerPoint Clone()
    {
      TriggerPoint triggerPoint = new TriggerPoint();
      triggerPoint.gameObject.transform.parent = this.gameObject.transform.parent;
      triggerPoint.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x + 500f, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
      triggerPoint.radius = this.radius;
      triggerPoint.gameObject.transform.localScale = new Vector3(this.radius * 2f, this.radius * 2f, this.radius * 2f);
      return triggerPoint;
    }
  }
}
