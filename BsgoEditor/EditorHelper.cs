﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.EditorHelper
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor
{
  public class EditorHelper
  {
    public static T GetObject<T>(GameObject gameObject) where T : class
    {
      if ((Object) gameObject == (Object) null)
        return (T) null;
      EditorRootScript component = gameObject.GetComponent<EditorRootScript>();
      if ((Object) component == (Object) null)
        return (T) null;
      return component.EditorObject as T;
    }
  }
}
