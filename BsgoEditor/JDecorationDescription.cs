﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.JDecorationDescription
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Json;
using UnityEngine;

namespace BsgoEditor
{
  public class JDecorationDescription : IJsonSerializationListener
  {
    [JsonField(JsonName = "scale")]
    public Vector3 scale = new Vector3(1f, 1f, 1f);
    [JsonField(JsonName = "key")]
    public string key = string.Empty;
    public Vector3 position;
    public Quaternion rotation;
    [JsonField(JsonName = "rotationSpeed")]
    public float rotationSpeed;

    public void JsonSerialization(JsonData json)
    {
    }

    public void JsonDeserialization(JsonData json)
    {
      if (!json.Object.ContainsKey("PrefabName"))
        return;
      this.key = "decoration_" + json.Object["PrefabName"].String;
    }

    public void JsonBeforeSerialization()
    {
    }
  }
}
