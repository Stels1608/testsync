﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.Tools
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Loaders;
using UnityEngine;

namespace BsgoEditor
{
  public class Tools
  {
    public static Asteroid ConstructPlanetoid(JPlanetoid desc)
    {
      Asteroid asteroid = new Asteroid(desc.key);
      asteroid.Radius = desc.radius;
      asteroid.SetDistance(0.0f);
      asteroid.SetRotationSpeed(0.0f);
      asteroid.Constructed = (System.Action<GameObject>) (go => go.transform.parent = EditorLevel.GetInstance().planetoidsParent.transform);
      asteroid.WorldCard = EditorLevel.planetoidTypes[asteroid.type].FormWorldCard();
      asteroid.Scale = asteroid.Radius;
      asteroid.Position = desc.position;
      asteroid.Rotation = desc.rotation;
      asteroid.Construct();
      asteroid.Root.name = "PlanetoidsSpaceObject";
      return asteroid;
    }

    public static JPlanetoid GetPlanetoidDescription(Asteroid obj)
    {
      return new JPlanetoid() { key = obj.type, position = obj.Position, rotation = obj.Rotation, radius = obj.Scale };
    }
  }
}
