﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.AsteroidBelt
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace BsgoEditor
{
  [JsonClassDesc("gameObject/name", JsonName = "name")]
  [JsonClassDesc("gameObject/transform/rotation", JsonName = "rotation")]
  [JsonClassDesc("gameObject/transform/position", JsonName = "position")]
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  public class AsteroidBelt
  {
    [JsonField(JsonName = "point2")]
    public Vector3 Point2 = new Vector3(100f, 0.0f, 0.0f);
    [JsonField(JsonName = "twistYRadius")]
    public float TwistYRadius = 1500f;
    [JsonField(JsonName = "twistZRadius")]
    public float TwistZRadius = 1500f;
    [JsonField(JsonName = "twistPeriods")]
    public float TwistPeriods = 1f;
    [JsonField(JsonName = "awistAngle")]
    public float TwistAngle = 1f;
    [JsonField(JsonName = "asteroidFamily")]
    public string AsteroidFamily = "asteroid1";
    [JsonField(JsonName = "asteroidRotationSpeed")]
    public float RotationSpeed = 3f;
    [JsonField(JsonName = "radiusRandom")]
    public bool RadiusRandom = true;
    [JsonField(JsonName = "radius")]
    public float Radius = 10f;
    [JsonField(JsonName = "radiusMin")]
    public float RadiusMin = 3f;
    [JsonField(JsonName = "radiusMax")]
    public float RadiusMax = 10f;
    [JsonField(JsonName = "asteroidDistance")]
    public float AsteroidDistance = 0.1f;
    [JsonField(JsonName = "asteroids")]
    public List<Asteroid> Asteroids = new List<Asteroid>();
    public readonly int MaxAsteriodCount = 250;
    public GameObject gameObject;
    [JsonField(JsonName = "asteroidRandomPosition")]
    public float AsteroidRandomPosition;

    public AsteroidBelt()
    {
      this.gameObject = new GameObject("Belt");
      this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
    }

    public AsteroidBelt(AsteroidBelt belt)
      : this()
    {
      this.gameObject.name = belt.gameObject.name;
      this.Point2 = belt.Point2;
      this.TwistYRadius = belt.TwistYRadius;
      this.TwistZRadius = belt.TwistZRadius;
      this.TwistPeriods = belt.TwistPeriods;
      this.TwistAngle = belt.TwistAngle;
      this.AsteroidRandomPosition = belt.AsteroidRandomPosition;
      this.AsteroidDistance = belt.AsteroidDistance;
      this.AsteroidFamily = belt.AsteroidFamily;
      using (List<Asteroid>.Enumerator enumerator = belt.Asteroids.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Asteroid asteroid = enumerator.Current.Clone();
          asteroid.Constructed = (System.Action<GameObject>) (go => go.transform.parent = this.gameObject.transform);
          asteroid.WorldCard = EditorLevel.asteroidTypes[asteroid.type].FormWorldCard();
          asteroid.Construct();
          this.Asteroids.Add(asteroid);
        }
      }
    }

    public void InitAfterAutoConstruct()
    {
      using (List<Asteroid>.Enumerator enumerator = this.Asteroids.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Asteroid current = enumerator.Current;
          current.SetDistance(0.0f);
          current.Constructed = (System.Action<GameObject>) (go => go.transform.parent = this.gameObject.transform);
          current.WorldCard = EditorLevel.asteroidTypes[current.type].FormWorldCard();
          this.SetAsteroidTransform(current, current.Position);
          current.Construct();
        }
      }
    }

    public void ApplyChanges()
    {
      List<Vector3> asteroidsLocalPositions = this.GetAsteroidsLocalPositions();
      while (this.Asteroids.Count > 0)
      {
        Asteroid asteroid = this.Asteroids[0];
        if (asteroid != null)
        {
          this.Asteroids.RemoveAt(0);
          asteroid.Remove(RemovingCause.Disconnection);
        }
      }
      if (this.MaxAsteriodCount < asteroidsLocalPositions.Count)
        return;
      for (int index = 0; index < asteroidsLocalPositions.Count; ++index)
      {
        Asteroid ast1 = new Asteroid(this.AsteroidFamily + "_" + ((int) UnityEngine.Random.Range(1f, (float) EditorLevel.asteroidVariants.Length - 0.01f)).ToString());
        this.SetAsteroidTransform(ast1, asteroidsLocalPositions[index]);
        ast1.SetDistance(0.0f);
        ast1.Constructed = (System.Action<GameObject>) (go =>
        {
          go.transform.parent = this.gameObject.transform;
          Asteroid ast = go.GetComponent<RootScript>().SpaceObject as Asteroid;
          Vector3 position = ast.Position;
          go.transform.localPosition = position;
          this.UpdateAsteroidCache(ast);
        });
        ast1.WorldCard = EditorLevel.asteroidTypes[ast1.type].FormWorldCard();
        ast1.Construct();
        this.Asteroids.Add(ast1);
      }
    }

    private void SetAsteroidTransform(Asteroid ast, Vector3 pos)
    {
      ast.Radius = !this.RadiusRandom ? this.Radius : UnityEngine.Random.Range(this.RadiusMin, this.RadiusMax);
      ast.SetRotationSpeed(this.RotationSpeed);
      ast.Position = pos;
      ast.Rotation = new Quaternion((float) UnityEngine.Random.Range(0, 359), (float) UnityEngine.Random.Range(0, 359), (float) UnityEngine.Random.Range(0, 359), 1f);
      ast.Scale = ast.Radius * 0.05f;
    }

    public List<Vector3> GetAsteroidsLocalPositions()
    {
      List<Vector3> vector3List = new List<Vector3>();
      float magnitude = this.Point2.magnitude;
      float num1 = magnitude / this.TwistPeriods;
      float x = 0.0f;
      while ((double) x < (double) magnitude)
      {
        float num2 = x / magnitude;
        float num3 = this.TwistAngle + (1f - this.TwistAngle) * num2;
        Vector3 vector3 = new Vector3(x, 0.0f, 0.0f);
        vector3.y += (this.TwistAngle + (1f - this.TwistAngle) * num2) * this.TwistYRadius * Mathf.Sin(6.283185f * num2 * this.TwistPeriods);
        vector3.z += (this.TwistAngle + (1f - this.TwistAngle) * num2) * this.TwistZRadius * Mathf.Cos(6.283185f * num2 * this.TwistPeriods);
        vector3 += new Vector3(UnityEngine.Random.Range(-this.AsteroidRandomPosition, this.AsteroidRandomPosition), UnityEngine.Random.Range(-this.AsteroidRandomPosition, this.AsteroidRandomPosition), UnityEngine.Random.Range(-this.AsteroidRandomPosition, this.AsteroidRandomPosition));
        vector3List.Add(vector3);
        x += num1 / (6.283185f * num3 / this.AsteroidDistance);
      }
      return vector3List;
    }

    public void UpdateAsteroidCaches()
    {
      for (int index = 0; index < this.Asteroids.Count; ++index)
        this.UpdateAsteroidCache(this.Asteroids[index]);
    }

    private void UpdateAsteroidCache(Asteroid ast)
    {
      ast.Position = ast.Root.position;
    }
  }
}
