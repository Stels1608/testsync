﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.RespawnPoint
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace BsgoEditor
{
  [JsonClassDesc("gameObject/transform/position", JsonName = "position")]
  [JsonClassDesc("gameObject/name", JsonName = "name")]
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  public class RespawnPoint
  {
    [JsonField(JsonName = "types")]
    public List<RespawnPoint.LootType> Types = new List<RespawnPoint.LootType>();
    public GameObject gameObject;

    public RespawnPoint()
    {
      this.gameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
      this.gameObject.name = "LootRespawningPoint";
      this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
      this.gameObject.transform.localScale = new Vector3(300f, 300f, 300f);
      Object.Destroy((Object) this.gameObject.GetComponent<Collider>());
    }

    public RespawnPoint Clone()
    {
      GameObject gameObject = EditorLevel.GetInstance().respawnPoint;
      RespawnPoint respawnPoint = new RespawnPoint();
      respawnPoint.gameObject.transform.parent = gameObject.transform;
      using (List<RespawnPoint.LootType>.Enumerator enumerator = this.Types.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          RespawnPoint.LootType current = enumerator.Current;
          respawnPoint.Types.Add(new RespawnPoint.LootType(current));
          respawnPoint.gameObject.name = current.type;
        }
      }
      respawnPoint.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x + 10f, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
      return respawnPoint;
    }

    public class LootType
    {
      [JsonField(JsonName = "type")]
      public string type = "loot_dev";
      [JsonField(JsonName = "prob")]
      public int prob = 1;
      [JsonField(JsonName = "respawnTime")]
      public int respawnTime = 600;

      public LootType()
      {
      }

      public LootType(RespawnPoint.LootType obj)
      {
        this.type = obj.type;
        this.prob = obj.prob;
        this.respawnTime = obj.respawnTime;
      }
    }
  }
}
