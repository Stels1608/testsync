﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.MobRespawnPoint
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace BsgoEditor
{
  [JsonClassDesc("gameObject/transform/position", JsonName = "position")]
  [JsonClassDesc("gameObject/transform/localScale", JsonName = "box")]
  [JsonClassDesc("gameObject/name", JsonName = "name")]
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  public class MobRespawnPoint
  {
    [JsonField(JsonName = "mobs")]
    public List<Mob> Mobs = new List<Mob>();
    [JsonField(JsonName = "revive_time")]
    public float RevivalTime = 30f;
    [JsonField(JsonName = "maximum")]
    public int m_maximum = 1;
    [JsonField(JsonName = "respawn_chance")]
    public float m_respawn_chance = 10f;
    [JsonField(JsonName = "activation")]
    public List<string> activation = new List<string>();
    [JsonField(JsonName = "spawner_color")]
    public Color m_spawner_color = new Color(1f, 1f, 0.0f, 0.35f);
    [JsonField(JsonName = "path_name")]
    public string path_name = string.Empty;
    public GameObject gameObject;
    [JsonField(JsonName = "use_spawner_color")]
    public bool m_useCustomColor;
    public bool m_random;
    private RespawnPointRenderer renderer;

    public Color CylonColor
    {
      get
      {
        return new Color(1f, 0.15f, 0.1f, 0.35f);
      }
    }

    public Color ColonialColor
    {
      get
      {
        return new Color(0.1f, 0.25f, 1f, 0.35f);
      }
    }

    public Color DefaultColor
    {
      get
      {
        return new Color(1f, 1f, 0.0f, 0.35f);
      }
    }

    public Color MixedColor
    {
      get
      {
        return new Color(0.7f, 0.0f, 1f, 0.75f);
      }
    }

    public Color Color
    {
      get
      {
        return this.renderer.color;
      }
      set
      {
        this.renderer.color = value;
      }
    }

    public MobRespawnPoint()
    {
      this.gameObject = new GameObject("MobRespawningPoint");
      this.renderer = this.gameObject.AddComponent<RespawnPointRenderer>();
      this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
      this.Color = this.DefaultColor;
      this.gameObject.transform.localScale = new Vector3(500f, 50f, 500f);
    }

    public MobRespawnPoint Clone()
    {
      MobRespawnPoint mobRespawnPoint = new MobRespawnPoint();
      mobRespawnPoint.gameObject.transform.parent = this.gameObject.transform.parent;
      mobRespawnPoint.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x + 100f, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
      mobRespawnPoint.gameObject.name = this.gameObject.name;
      mobRespawnPoint.m_random = this.m_random;
      mobRespawnPoint.RevivalTime = this.RevivalTime;
      mobRespawnPoint.m_maximum = this.m_maximum;
      mobRespawnPoint.m_respawn_chance = this.m_respawn_chance;
      mobRespawnPoint.gameObject.transform.localScale = this.gameObject.transform.localScale;
      mobRespawnPoint.m_useCustomColor = this.m_useCustomColor;
      mobRespawnPoint.m_spawner_color = this.m_spawner_color;
      for (int index = 0; index < this.Mobs.Count; ++index)
        mobRespawnPoint.Mobs.Add(new Mob(this.Mobs[index]));
      mobRespawnPoint.AfterInit();
      return mobRespawnPoint;
    }

    public void AddActivation()
    {
      if (EditorLevel.allBonusStrings.Length != 0)
        this.activation.Add(EditorLevel.allBonusStrings[0]);
      else
        Debug.LogWarning((object) "There are not any items in allBonusStrings list");
    }

    public void SetSpawnerColor()
    {
      if (this.m_useCustomColor)
      {
        this.Color = this.m_spawner_color;
      }
      else
      {
        bool flag1 = false;
        bool flag2 = false;
        for (int index = 0; index < this.Mobs.Count; ++index)
        {
          this.Color = this.DefaultColor;
          if (this.Mobs[index].type.ToLower().Contains("colonial"))
          {
            flag2 = true;
            this.Color = this.ColonialColor;
          }
          if (this.Mobs[index].type.ToLower().Contains("cylon"))
          {
            flag1 = true;
            this.Color = this.CylonColor;
          }
        }
        if (!flag1 || !flag2)
          return;
        this.Color = this.MixedColor;
      }
    }

    public void LoadAllMobJData()
    {
      for (int index = 0; index < this.Mobs.Count; ++index)
        this.Mobs[index].LoadMobJData();
    }

    public void GetMobShipNames()
    {
      for (int index = 0; index < this.Mobs.Count; ++index)
        this.Mobs[index].GetShipName();
    }

    public void AfterInit()
    {
      this.LoadAllMobJData();
      this.GetMobShipNames();
      this.SetSpawnerColor();
    }
  }
}
