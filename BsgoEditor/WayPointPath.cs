﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.WayPointPath
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace BsgoEditor
{
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  [JsonClassDesc("gameObject/name", JsonName = "name")]
  public class WayPointPath
  {
    [JsonField(JsonName = "editor_color")]
    public Color color = Color.green;
    [JsonField(JsonName = "points")]
    public List<WayPoint> Points = new List<WayPoint>();
    public GameObject gameObject;
    private WayPointGizmoScript wayPointGizmoScript;

    public int PointCount
    {
      get
      {
        return this.Points.Count;
      }
      set
      {
        this.SetPointCount(value);
      }
    }

    public WayPointPath()
    {
      this.gameObject = new GameObject("Empty_Waypoint");
      EditorRootScript editorRootScript = this.gameObject.AddComponent<EditorRootScript>();
      this.wayPointGizmoScript = this.gameObject.AddComponent<WayPointGizmoScript>();
      editorRootScript.EditorObject = (object) this;
    }

    public WayPointPath(string name)
    {
      this.gameObject = new GameObject(name);
      EditorRootScript editorRootScript = this.gameObject.AddComponent<EditorRootScript>();
      this.wayPointGizmoScript = this.gameObject.AddComponent<WayPointGizmoScript>();
      editorRootScript.EditorObject = (object) this;
    }

    public void InitAfterAutoConstruct()
    {
      for (int index = 0; index < this.Points.Count; ++index)
        this.Points[index].gameObject.transform.parent = this.gameObject.transform;
      this.UpdateWayPoints();
    }

    public void UpdatePointCount()
    {
      this.PointCount = this.gameObject.transform.childCount;
    }

    public void UpdateWayPoints()
    {
      for (int index = 0; index < this.PointCount; ++index)
      {
        GameObject gameObject = this.Points[index].gameObject;
        if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
        {
          if (index == 0)
          {
            gameObject.GetComponent<Renderer>().sharedMaterial.color = this.Points[index].StartColor;
            gameObject.name = "_Start_" + this.gameObject.name;
          }
          else if (index == this.PointCount - 1)
          {
            gameObject.GetComponent<Renderer>().sharedMaterial.color = this.Points[index].EndColor;
            gameObject.name = "End_" + this.gameObject.name;
          }
          else
          {
            gameObject.GetComponent<Renderer>().sharedMaterial.color = this.Points[index].DefaultColor;
            gameObject.name = this.gameObject.name + "_" + (object) index;
          }
        }
      }
      this.wayPointGizmoScript.Points = this.Points;
      this.wayPointGizmoScript.color = this.color;
    }

    public void SetPointCount(int number)
    {
      if (number < 0)
        number = 0;
      if (number == this.Points.Count)
        return;
      if (number > this.Points.Count)
      {
        int childCount = this.gameObject.transform.childCount;
        if (number == childCount)
        {
          for (int count = this.Points.Count; count < number; ++count)
            this.Points.Add(new WayPoint(this.gameObject.transform.GetChild(count).gameObject));
        }
        else
        {
          for (int count = this.Points.Count; count < number; ++count)
            this.AddPoint(new WayPoint());
        }
      }
      else
      {
        for (int count = this.Points.Count; count > number; --count)
        {
          if ((UnityEngine.Object) this.Points[count - 1].gameObject == (UnityEngine.Object) null)
          {
            this.Points.RemoveAt(count - 1);
          }
          else
          {
            UnityEngine.Object.Destroy((UnityEngine.Object) this.Points[count - 1].gameObject);
            this.Points.RemoveAt(count - 1);
          }
        }
      }
      this.UpdateWayPoints();
    }

    private void AddPoint(WayPoint point)
    {
      this.Points.Add(point);
      point.gameObject.transform.parent = this.gameObject.transform;
    }

    public void Merge(WayPointPath obj)
    {
      if (this == obj)
        throw new Exception("Waypoint: can't merge with myself...");
      this.Points.AddRange((IEnumerable<WayPoint>) obj.Points);
      this.InitAfterAutoConstruct();
      UnityEngine.Object.Destroy((UnityEngine.Object) obj.gameObject);
    }

    public WayPointPath Clone()
    {
      WayPointPath wayPointPath = new WayPointPath(this.gameObject.name + "_Copy");
      wayPointPath.gameObject.transform.parent = this.gameObject.transform.parent;
      wayPointPath.gameObject.transform.position = this.gameObject.transform.position;
      this.UpdatePointCount();
      for (int index = 0; index < this.Points.Count; ++index)
        wayPointPath.AddPoint(this.Points[index].Clone());
      wayPointPath.gameObject.transform.Translate(150f, 0.0f, 150f);
      wayPointPath.UpdateWayPoints();
      return wayPointPath;
    }
  }
}
