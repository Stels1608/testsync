﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.SectorIO
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Loaders;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BsgoEditor
{
  public class SectorIO
  {
    private static string galaxyMapDocName = "galaxy_map";
    public static readonly string NebulaXPath = "_Nebula";
    public static readonly string StarsvariancesXPath = "StarsVariance";
    public static readonly string StarsXPath = "_Stars";
    public static readonly string Starsmultiply = "_StarsMult";

    public static string GalaxyMapName
    {
      get
      {
        return SectorIO.galaxyMapDocName;
      }
      set
      {
        SectorIO.galaxyMapDocName = value;
      }
    }

    public static JGalaxyMapDescription LoadGalaxyMap()
    {
      if (ContentDB.DocumentExists(SectorIO.GalaxyMapName))
      {
        JGalaxyMapDescription document = ContentDB.GetDocument<JGalaxyMapDescription>(SectorIO.GalaxyMapName);
        Debug.Log((object) ("Galaxy Map " + document.Id + " loaded"));
        return document;
      }
      JGalaxyMapDescription jgalaxyMapDescription = new JGalaxyMapDescription();
      ContentDB.CreateDocument<JGalaxyMapDescription>(jgalaxyMapDescription, SectorIO.GalaxyMapName);
      Debug.Log((object) ("Sector " + (object) jgalaxyMapDescription + " not found. Default description created."));
      return jgalaxyMapDescription;
    }

    public static void SaveGalaxyMap(JGalaxyMapDescription desc)
    {
      ContentDB.ReplaceDocument<JGalaxyMapDescription>(desc, SectorIO.GalaxyMapName);
    }

    public static JSectorDescription Load(int sectorNumber)
    {
      string docid = "sector" + sectorNumber.ToString();
      if (ContentDB.DocumentExists(docid))
      {
        JSectorDescription document = ContentDB.GetDocument<JSectorDescription>(docid);
        Debug.Log((object) ("Sector " + document.Id + " loaded"));
        return document;
      }
      JSectorDescription jsectorDescription = new JSectorDescription();
      jsectorDescription.MiningProperties.asteroidResources = new JAsteroidProperties();
      jsectorDescription.MiningProperties.asteroidResources.resources = new List<JResourceEntry>();
      jsectorDescription.MiningProperties.planetoidResources = new JAsteroidProperties();
      jsectorDescription.MiningProperties.planetoidResources.resources = new List<JResourceEntry>();
      jsectorDescription.MiningProperties.miningShipConfig = new JMiningShipConfig();
      Debug.Log((object) ("Sector " + (object) sectorNumber + " not found. Default description created."));
      return jsectorDescription;
    }

    public static void Save(JSectorDescription sector, int sectorNumber)
    {
      SectorIO.GrabAsteroids(sector);
      SectorIO.GrabPlanetoids(sector.Planetoids);
      SectorIO.GrabBelts(sector);
      SectorIO.GrabSuns(sector);
      SectorIO.GrabLights(sector);
      SectorIO.GrabPlanetsReal(sector.PlanetsReal);
      SectorIO.GrabNebula(sector.Nebula);
      SectorIO.GrabStarsVariances(sector.StarsVariance);
      SectorIO.GrabStars(sector.Stars);
      SectorIO.GrabStarsMultiply(sector.StarsMultiply);
      SectorIO.GrabMovingNebulas(sector.MovingNebulas);
      SectorIO.GrabSize(sector);
      SectorIO.GrabSectorEvents(sector);
      SectorIO.GrabDecorations(sector.Decorations);
      SectorIO.GrabEventObjects(sector.EventObjects);
      SectorIO.AddSectorObjectsByType<RespawnPoint>(sector.Respawn.points, EditorLevel.GetInstance().respawnPoint);
      SectorIO.AddSectorObjectsByType<MobRespawnPoint>(sector.StaticMobRespawn, EditorLevel.GetInstance().staticMobRespawnPoint);
      SectorIO.AddSectorObjectsByType<MobRespawnPoint>(sector.RandomMobRespawn, EditorLevel.GetInstance().randomMobRespawnPoint);
      SectorIO.AddSectorObjectsByType<TriggerPoint>(sector.TriggerPoints, EditorLevel.GetInstance().triggerPoint);
      SectorIO.AddSectorObjectsByType<PlayerRespawnPoint>(sector.StartingPoints.Colonial, EditorLevel.GetInstance().startingPointColonials);
      SectorIO.AddSectorObjectsByType<PlayerRespawnPoint>(sector.StartingPoints.Cylon, EditorLevel.GetInstance().startingPointCylons);
      SectorIO.AddSectorObjectsByType<PlayerRespawnPoint>(sector.StartingPoints.ColonialOutpost, EditorLevel.GetInstance().startingPointColonialOutpost);
      SectorIO.AddSectorObjectsByType<PlayerRespawnPoint>(sector.StartingPoints.CylonOutpost, EditorLevel.GetInstance().startingPointCylonOutpost);
      SectorIO.AddSectorObjectsByType<DotArea>(sector.DotAreas, EditorLevel.GetInstance().dotPoint);
      SectorIO.AddSectorObjectsByType<CruiserSP>(sector.CruiserSPs, EditorLevel.GetInstance().cruisersSP);
      SectorIO.AddSectorObjectsByType<ConvoyGo>(sector.ConvoyPoints, EditorLevel.GetInstance().convoyPointsParent);
      SectorIO.AddSectorObjectsByType<WayPointPath>(sector.WayPoints, EditorLevel.GetInstance().wayPointsParent);
      SectorIO.GrabSectorProgression(sector);
      string name = "sector" + sectorNumber.ToString();
      ContentDB.ReplaceDocument<JSectorDescription>(sector, name);
      Log.DebugInfo((object) ("Sector " + (object) sectorNumber + " being saved"));
    }

    private static void GrabSectorProgression(JSectorDescription desc)
    {
      GameObject ga1 = GameObject.Find(EditorLevel.cylonProgressionPointName);
      GameObject ga2 = GameObject.Find(EditorLevel.colonialProgressionPointName);
      SectorIO.FillSectorProgression(desc.cylonProgressionPoints, ga1);
      SectorIO.FillSectorProgression(desc.colonialProgressionPoints, ga2);
    }

    private static void FillSectorProgression(ProgressionPoint p, GameObject ga)
    {
      for (int index = 0; index < ga.transform.childCount; ++index)
      {
        GameObject gameObject = ga.transform.GetChild(index).gameObject;
        if (!((UnityEngine.Object) gameObject.GetComponent<EditorRootScript>() != (UnityEngine.Object) null) || gameObject.GetComponent<EditorRootScript>().EditorObject is ProgressionPointEntity)
        {
          ProgressionPointEntity progressionPointEntity = gameObject.GetComponent<EditorRootScript>().EditorObject as ProgressionPointEntity;
          if (progressionPointEntity == null)
            Debug.Log((object) "ERROR!!! ch is empty");
          string name = gameObject.name;
          if (name != null)
          {
            // ISSUE: reference to a compiler-generated field
            if (SectorIO.\u003C\u003Ef__switch\u0024mapF == null)
            {
              // ISSUE: reference to a compiler-generated field
              SectorIO.\u003C\u003Ef__switch\u0024mapF = new Dictionary<string, int>(6)
              {
                {
                  "Outpost",
                  0
                },
                {
                  "Beacon",
                  1
                },
                {
                  "PlatformOne",
                  2
                },
                {
                  "PlatformTwo",
                  3
                },
                {
                  "PlatformThree",
                  4
                },
                {
                  "PlatformFour",
                  5
                }
              };
            }
            int num;
            // ISSUE: reference to a compiler-generated field
            if (SectorIO.\u003C\u003Ef__switch\u0024mapF.TryGetValue(name, out num))
            {
              switch (num)
              {
                case 0:
                  p.outpost = progressionPointEntity;
                  continue;
                case 1:
                  p.beacon = progressionPointEntity;
                  continue;
                case 2:
                  p.platformOne = progressionPointEntity;
                  continue;
                case 3:
                  p.platformTwo = progressionPointEntity;
                  continue;
                case 4:
                  p.platformThree = progressionPointEntity;
                  continue;
                case 5:
                  p.platformFour = progressionPointEntity;
                  continue;
              }
            }
          }
          Debug.Log((object) ("STRANGE NAMES CHILD WAS FOUND " + gameObject.name));
        }
      }
    }

    private static void AddSectorObjectsByType<T>(List<T> list, GameObject parent) where T : class
    {
      list.Clear();
      int childCount = parent.transform.childCount;
      for (int index = 0; index < childCount; ++index)
      {
        GameObject gameObject = parent.transform.GetChild(index).gameObject;
        T @object = EditorHelper.GetObject<T>(gameObject);
        if ((object) @object == null)
        {
          Debug.LogError((object) ("The following " + typeof (T).Name + " can not be saved " + gameObject.name + " because it wasn't created correctly. Please delete it and create a replacement using correct Editor tool."));
          gameObject.name = "**NotCreatedCorrectly_" + gameObject.name + "_DeleteMe";
        }
        else
          list.Add(@object);
      }
    }

    public static void GrabSectorEvents(JSectorDescription desc)
    {
      desc.sectorEvents = new List<JSectorEvents>();
      GameObject gameObject1 = GameObject.Find("_SectorEvents");
      for (int index = 0; index < gameObject1.transform.childCount; ++index)
      {
        GameObject gameObject2 = gameObject1.transform.GetChild(index).gameObject;
        SectorEventObjects component = gameObject2.GetComponent<SectorEventObjects>();
        if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        {
          Debug.LogError((object) "editorRootScript is null");
        }
        else
        {
          JSectorEvents jsectorEvents = new JSectorEvents();
          jsectorEvents.Init(component);
          jsectorEvents.pos.position = gameObject2.transform.position;
          desc.sectorEvents.Add(jsectorEvents);
        }
      }
    }

    private static void GrabPlanetoids(List<JPlanetoid> list)
    {
      list.Clear();
      GameObject gameObject = EditorLevel.GetInstance().planetoidsParent;
      int childCount = gameObject.transform.childCount;
      for (int index = 0; index < childCount; ++index)
      {
        Asteroid ast = gameObject.transform.GetChild(index).gameObject.GetComponent<RootScript>().SpaceObject as Asteroid;
        SectorIO.UpdateAsteroidCache(ast);
        JPlanetoid planetoidDescription = Tools.GetPlanetoidDescription(ast);
        list.Add(planetoidDescription);
      }
    }

    private static void GrabDecorations(List<JDecorationDescription> list)
    {
      list.Clear();
      GameObject gameObject1 = EditorLevel.GetInstance().decorationCollector;
      int childCount = gameObject1.transform.childCount;
      for (int index = 0; index < childCount; ++index)
      {
        GameObject gameObject2 = gameObject1.transform.GetChild(index).gameObject;
        DebrisPile debrisPile = gameObject2.GetComponent<RootScript>().SpaceObject as DebrisPile;
        JDecorationDescription jdecorationDescription = new JDecorationDescription() { position = gameObject2.transform.position, rotation = gameObject2.transform.rotation, scale = gameObject2.transform.lossyScale, key = gameObject2.name, rotationSpeed = debrisPile.RotationSpeed };
        if (EditorLevel.decorationsTypes.ContainsKey(jdecorationDescription.key))
          list.Add(jdecorationDescription);
        else
          Debug.LogError((object) ("The following Decoration" + jdecorationDescription.key + " can not be saved because it wasn't created correctly. Please delete it and create a replacement using correct Editor tool."));
      }
    }

    private static void GrabEventObjects(List<JEventObject> list)
    {
      list.Clear();
      GameObject gameObject1 = EditorLevel.GetInstance().eventObjectsParent;
      int childCount = gameObject1.transform.childCount;
      for (int index = 0; index < childCount; ++index)
      {
        GameObject gameObject2 = gameObject1.transform.GetChild(index).gameObject;
        EventObject eventObject = gameObject2.GetComponent<RootScript>().SpaceObject as EventObject;
        if (eventObject == null)
        {
          Debug.LogError((object) ("Error while saving event object " + gameObject2.name));
        }
        else
        {
          JEventObject jeventObject = new JEventObject() { position = gameObject2.transform.position, rotation = gameObject2.transform.rotation, key = gameObject2.name, eventActivation = eventObject.EventActivation, spawnBox = eventObject.SpawnBox };
          if (EditorLevel.eventObjectTypes.ContainsKey(jeventObject.key))
            list.Add(jeventObject);
          else
            Debug.LogError((object) ("The following Event Object" + jeventObject.key + " can not be saved because it wasn't created correctly. Please delete it and create a replacement using correct Editor tool."));
        }
      }
    }

    private static void GrabAsteroids(JSectorDescription sector)
    {
      sector.Asteroids.Clear();
      GameObject gameObject = EditorLevel.GetInstance().asteroidsParent;
      List<Asteroid> asteroidList = new List<Asteroid>();
      int childCount = gameObject.transform.childCount;
      for (int index = 0; index < childCount; ++index)
      {
        Asteroid ast = gameObject.transform.GetChild(index).GetComponent<RootScript>().SpaceObject as Asteroid;
        if (ast != null)
        {
          SectorIO.UpdateAsteroidCache(ast);
          ast.Radius = ast.Root.transform.localScale.x / 0.05f;
          asteroidList.Add(ast);
        }
      }
      sector.Asteroids.AddRange((IEnumerable<Asteroid>) asteroidList);
    }

    private static void UpdateAsteroidCache(Asteroid ast)
    {
      ast.Position = ast.Root.position;
      ast.Rotation = ast.Root.rotation;
      ast.Scale = ast.Root.transform.localScale.x;
    }

    private static void GrabSuns(JSectorDescription sector)
    {
      sector.Suns.Clear();
      foreach (Component component in (Sun[]) UnityEngine.Object.FindObjectsOfType(typeof (Sun)))
      {
        SunDesc sunDesc = GrabbingHelpers.GrabSun(component.gameObject);
        sector.Suns.Add(sunDesc);
      }
    }

    private static void GrabLights(JSectorDescription sector)
    {
      sector.AmbientColor = RenderSettings.ambientLight;
      sector.Lights.Clear();
      foreach (Light light in ((IEnumerable<Light>) UnityEngine.Object.FindObjectsOfType(typeof (Light))).Where<Light>((Func<Light, bool>) (l => l.type == LightType.Directional)).ToArray<Light>())
      {
        GameObject gameObject = light.gameObject;
        JLightDescription jlightDescription = new JLightDescription() { name = gameObject.name, position = gameObject.transform.position, rotation = gameObject.transform.rotation, color = light.color, intensity = light.intensity / 2f };
        sector.Lights.Add(jlightDescription);
      }
    }

    private static void GrabPlanetsReal(List<PlanetReal> list)
    {
      list.Clear();
      foreach (GameObject gameObject in (GameObject[]) UnityEngine.Object.FindObjectsOfType(typeof (GameObject)))
      {
        PlanetReal @object = EditorHelper.GetObject<PlanetReal>(gameObject);
        if (@object != null)
          list.Add(@object);
      }
    }

    private static void GrabBelts(JSectorDescription sector)
    {
      sector.Belts.Clear();
      foreach (AsteroidBelt asteroidBelt in ((IEnumerable<GameObject>) UnityEngine.Object.FindObjectsOfType(typeof (GameObject))).Select<GameObject, AsteroidBelt>((Func<GameObject, AsteroidBelt>) (b => EditorHelper.GetObject<AsteroidBelt>(b))).Where<AsteroidBelt>((Func<AsteroidBelt, bool>) (b => b != null)).ToArray<AsteroidBelt>())
      {
        asteroidBelt.UpdateAsteroidCaches();
        sector.Belts.Add(asteroidBelt);
      }
    }

    public static void GrabNebula(BackgroundDesc desc)
    {
      GameObject gameObject = GameObject.Find(SectorIO.NebulaXPath);
      desc.prefabName = gameObject.transform.GetChild(0).gameObject.name;
      desc.position = gameObject.transform.position;
      desc.rotation = gameObject.transform.rotation;
      desc.scale = gameObject.transform.lossyScale;
      Renderer componentInChildren = gameObject.GetComponentInChildren<Renderer>();
      desc.color = componentInChildren.material.color;
    }

    private static void GrabStarsVariances(BackgroundDesc desc)
    {
      GameObject gameObject = GameObject.Find(SectorIO.StarsvariancesXPath).transform.GetChild(0).gameObject;
      desc.prefabName = gameObject.name;
      desc.position = gameObject.transform.position;
      desc.rotation = gameObject.transform.rotation;
      desc.scale = gameObject.transform.lossyScale;
      Renderer componentInChildren = gameObject.GetComponentInChildren<Renderer>();
      if (!componentInChildren.material.HasProperty("_Color"))
        return;
      desc.color = componentInChildren.material.color;
    }

    private static void GrabStars(BackgroundDesc desc)
    {
      GameObject gameObject = GameObject.Find(SectorIO.StarsXPath);
      desc.prefabName = gameObject.transform.GetChild(0).gameObject.name;
      desc.position = gameObject.transform.position;
      desc.rotation = gameObject.transform.rotation;
      desc.scale = gameObject.transform.lossyScale;
      Renderer componentInChildren = gameObject.GetComponentInChildren<Renderer>();
      if (!componentInChildren.material.HasProperty("_Color"))
        return;
      desc.color = componentInChildren.material.color;
    }

    private static void GrabStarsMultiply(BackgroundDesc desc)
    {
      GameObject gameObject = GameObject.Find(SectorIO.Starsmultiply);
      if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      {
        Log.DebugInfo((object) (SectorIO.Starsmultiply + " not found"));
      }
      else
      {
        desc.prefabName = gameObject.transform.GetChild(0).gameObject.name;
        desc.position = gameObject.transform.position;
        desc.rotation = gameObject.transform.rotation;
        desc.scale = gameObject.transform.lossyScale;
        Renderer componentInChildren = gameObject.GetComponentInChildren<Renderer>();
        if (!componentInChildren.material.HasProperty("_Color"))
          return;
        desc.color = componentInChildren.material.color;
      }
    }

    private static void GrabMovingNebulas(List<MovingNebulaDesc> list)
    {
      list.Clear();
      foreach (GameObject gameObject in ((IEnumerable<GameObject>) UnityEngine.Object.FindObjectsOfType(typeof (GameObject))).Where<GameObject>((Func<GameObject, bool>) (n => n.name == MovingNebulaConstructor.goName)).Select<GameObject, GameObject>((Func<GameObject, GameObject>) (m => m.transform.GetChild(0).gameObject)).ToArray<GameObject>())
      {
        MovingNebulaDesc movingNebulaDesc = new MovingNebulaDesc();
        movingNebulaDesc.position = gameObject.transform.position;
        movingNebulaDesc.rotation = gameObject.transform.rotation;
        movingNebulaDesc.scale = gameObject.transform.lossyScale;
        if (movingNebulaDesc.modelName.Contains("_"))
          movingNebulaDesc.modelName = movingNebulaDesc.modelName.Substring(0, movingNebulaDesc.modelName.LastIndexOf("_"));
        Material material = gameObject.GetComponent<Renderer>().material;
        movingNebulaDesc.textureOffset = material.GetTextureOffset("_MainTex");
        movingNebulaDesc.textureScale = material.GetTextureScale("_MainTex");
        movingNebulaDesc.color = material.GetColor("_Color");
        string materialSuffix = SectorIO.GetMaterialSuffix(gameObject);
        movingNebulaDesc.matSuffix = materialSuffix;
        list.Add(movingNebulaDesc);
      }
    }

    private static string GetMaterialSuffix(GameObject obj)
    {
      string[] strArray = obj.GetComponent<Renderer>().material.name.Split(' ')[0].Split('_');
      return strArray.Length <= 1 ? string.Empty : strArray[strArray.Length - 1];
    }

    private static void GrabSize(JSectorDescription desc)
    {
      EditorLevel instance = EditorLevel.GetInstance();
      desc.Size = instance.boundBox.GetComponent<CubeGizmos>().Size;
    }
  }
}
