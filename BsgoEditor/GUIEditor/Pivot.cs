﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.Pivot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class Pivot : GUILabel
  {
    public string name = "Pivot";
    private Texture2D texture;

    public Pivot()
      : base(string.Empty, (SmartRect) null)
    {
      this.texture = ResourceLoader.Load<Texture2D>("GUI/Common/visualtoggle_over");
      this.rect.Width = (float) this.texture.width;
      this.rect.Height = (float) this.texture.height;
    }

    public Pivot(JPivot desc)
      : this()
    {
      this.name = desc.name;
      this.SetPosition(desc.position);
    }

    public override void Draw()
    {
      GUI.DrawTexture(this.rect.AbsRect, (Texture) this.texture);
    }
  }
}
