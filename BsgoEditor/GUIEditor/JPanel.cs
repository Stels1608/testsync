﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using CouchDB;
using Gui;
using Json;
using System.Collections.Generic;

namespace BsgoEditor.GUIEditor
{
  public class JPanel : JElementBase
  {
    public List<JElementBase> Elements = new List<JElementBase>();

    public JPanel()
      : base("panel")
    {
    }

    public JPanel(string name)
      : base(name)
    {
    }

    public JPanel(Panel gui)
      : base((CustomGUIElement) gui, "panel", gui.name)
    {
    }

    public JPanel(GuiElementBase element)
      : base(element)
    {
      GuiPanel guiPanel = element as GuiPanel;
      for (int index = 0; index < guiPanel.Children.Count; ++index)
        this.Elements.Add(guiPanel.Children[index].Convert2Json());
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiPanel(this);
    }

    public override void Deserialize(JsonData jsonData)
    {
      jsonData = jsonData.Object["Elements"];
      this.Elements.Clear();
      using (List<JsonData>.Enumerator enumerator = jsonData.Array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JsonData current = enumerator.Current;
          JElementBase jelementBase = JsonSerializator.Deserialize(System.Type.GetType(current.Object["type"].String), current) as JElementBase;
          if (jelementBase != null)
          {
            jelementBase.Deserialize(current);
            this.Elements.Add(jelementBase);
          }
        }
      }
    }

    public override CustomGUIElement GetGUIElement()
    {
      return (CustomGUIElement) new Panel(this);
    }

    public JElementBase GetElement(string name)
    {
      using (List<JElementBase>.Enumerator enumerator = this.Elements.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JElementBase current = enumerator.Current;
          if (current.name == name)
            return current;
        }
      }
      return (JElementBase) null;
    }
  }
}
