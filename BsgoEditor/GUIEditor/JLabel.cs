﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JLabel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class JLabel : JElementBase
  {
    public string text = "Label";
    public string fontPath = "GUI/Fonts/BGM BT";
    public int fontSize = 12;
    public TextAnchor textAlignment = TextAnchor.MiddleCenter;
    public Color normalColor = Color.gray;
    public Color overColor = Color.white;
    private const string FONT_PATH = "GUI/Fonts/";
    public bool useWordWrap;
    public bool useAutoSize;
    public Color inactiveColor;

    public JLabel()
      : base("label")
    {
    }

    public JLabel(Label gui)
      : base((CustomGUIElement) gui, "label", gui.name)
    {
      this.text = gui.GetText();
      this.fontPath = gui.fontPath;
      this.fontSize = gui.fontSize;
      this.textAlignment = gui.style.alignment;
      this.useWordWrap = gui.style.wordWrap;
      this.normalColor = gui.GetNormalColor();
      this.overColor = gui.GetOverColor();
    }

    public JLabel(GuiElementBase element)
      : base(element)
    {
      GuiLabel guiLabel = element as GuiLabel;
      this.text = guiLabel.Text;
      if (!guiLabel.Font.name.Contains("GUI/Fonts/"))
        guiLabel.Font.name = "GUI/Fonts/" + guiLabel.Font.name;
      this.fontPath = guiLabel.Font.name;
      this.fontSize = guiLabel.FontSize;
      this.textAlignment = guiLabel.Alignment;
      this.useWordWrap = guiLabel.WordWrap;
      this.useAutoSize = guiLabel.AutoSize;
      this.normalColor = guiLabel.NormalColor;
      this.overColor = guiLabel.OverColor;
      this.inactiveColor = guiLabel.InactiveColor;
    }

    public override GuiElementBase Convert2Gui()
    {
      GuiLabel guiLabel = new GuiLabel((JElementBase) this);
      if (this.inactiveColor == new Color())
        this.inactiveColor = new Color(this.normalColor.r, this.normalColor.g, this.normalColor.b, 0.25f);
      return (GuiElementBase) guiLabel;
    }

    public override CustomGUIElement GetGUIElement()
    {
      return (CustomGUIElement) new Label(this);
    }

    public GUILabelNew CreateGUILabelNew()
    {
      return this.CreateGUILabelNew((SmartRect) null);
    }

    public GUILabelNew CreateGUILabelNew(SmartRect parent)
    {
      Font font = !(this.fontPath == string.Empty) ? (Font) ResourceLoader.Load(this.fontPath) : Gui.Options.FontBGM_BT;
      GUILabelNew guiLabelNew = new GUILabelNew(this.text, parent, font, this.normalColor, this.overColor);
      guiLabelNew.Name = this.name;
      guiLabelNew.Alignment = this.textAlignment;
      guiLabelNew.WordWrap = this.useWordWrap;
      guiLabelNew.AutoSize = this.useAutoSize;
      guiLabelNew.SmartRect.Width = this.size.x;
      guiLabelNew.SmartRect.Height = this.size.y;
      guiLabelNew.Position = this.position;
      guiLabelNew.FontSize = this.fontSize;
      return guiLabelNew;
    }
  }
}
