﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JTabPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

namespace BsgoEditor.GUIEditor
{
  public class JTabPanel : JPanel
  {
    public float m_maxButtonWidth = 100f;
    public JButton m_defaultButton;

    public JTabPanel()
      : base("TabPanel")
    {
    }

    public JTabPanel(GuiElementBase element)
      : base(element)
    {
      GuiTabPanel guiTabPanel = element as GuiTabPanel;
      this.m_defaultButton = new JButton((GuiElementBase) guiTabPanel.DefaultButton);
      this.m_maxButtonWidth = guiTabPanel.MaxButtonWidth;
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiTabPanel(this);
    }
  }
}
