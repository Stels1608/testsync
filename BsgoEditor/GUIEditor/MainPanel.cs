﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.MainPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class MainPanel : GUIPanel
  {
    private Texture2D background = TextureCache.Get(Color.gray);
    public List<CustomGUIElement> elements = new List<CustomGUIElement>();
    private SmartRect pivotRect;
    public CustomGUIElement selectedObject;
    private float2 previousPosition;
    private MainPanel.State state;

    public MainPanel(SmartRect parent)
      : base(parent)
    {
      this.root.Width = 400f;
      this.root.Height = 250f;
      this.pivotRect = new SmartRect(this.root);
      this.pivotRect.Width = 10f;
      this.pivotRect.Height = 10f;
    }

    public override void Draw()
    {
      this.root.Position = new float2((float) Screen.width / 2f, (float) Screen.height / 2f);
      this.root.RecalculateAbsCoords();
      GUI.DrawTexture(this.root.AbsRect, (Texture) this.background);
      using (List<CustomGUIElement>.Enumerator enumerator = this.elements.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.Draw();
      }
      GUI.Button(this.pivotRect.AbsRect, string.Empty);
    }

    public void AddElement(CustomGUIElement element)
    {
      this.elements.Add(element);
      element.SetPosition(element.GetPosition());
      element.GetRect().Parent = this.root;
      element.RecalculateAbsCoords();
      this.Select(element);
    }

    public void BringToFront(CustomGUIElement element)
    {
      this.elements.Remove(element);
      this.elements.Insert(0, element);
    }

    public void SendToBack(CustomGUIElement element)
    {
      this.elements.Remove(element);
      this.elements.Add(element);
    }

    public void RemoveElement(CustomGUIElement element)
    {
      this.elements.Remove(element);
    }

    public override void RecalculateAbsCoords()
    {
      this.Position = new float2((float) Screen.width / 2f, (float) Screen.height / 2f);
      base.RecalculateAbsCoords();
      this.pivotRect.RecalculateAbsCoords();
    }

    public override bool Contains(float2 point)
    {
      using (List<CustomGUIElement>.Enumerator enumerator = this.elements.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          if (enumerator.Current.Contains(point))
            return true;
        }
      }
      return false;
    }

    public override bool OnMouseDown(float2 mousePosition, KeyCode mouseKey)
    {
      if (this.selectedObject != null)
      {
        this.selectedObject.RecalculateAbsCoords();
        if (this.selectedObject is IScalable && (this.selectedObject as IScalable).ScalingPivot.AbsRect.Contains(mousePosition.ToV2()))
        {
          this.state = MainPanel.State.Scaling;
          return true;
        }
      }
      this.Select((CustomGUIElement) null);
      if (this.elements.Count == 0)
        return false;
      for (int index = this.elements.Count - 1; index >= 0; --index)
      {
        if (this.elements[index].Contains(mousePosition))
        {
          this.Select(this.elements[index]);
          this.state = MainPanel.State.Dragging;
          break;
        }
      }
      this.previousPosition = mousePosition;
      return this.selectedObject != null;
    }

    public void Select(CustomGUIElement obj)
    {
      if (this.selectedObject != null && this.selectedObject is IScalable)
        (this.selectedObject as IScalable).DrawScalingPivot = false;
      this.selectedObject = obj;
      if (this.selectedObject == null || !(this.selectedObject is IScalable))
        return;
      (this.selectedObject as IScalable).DrawScalingPivot = true;
    }

    public override bool OnMouseUp(float2 mousePosition, KeyCode mouseKey)
    {
      this.state = MainPanel.State.None;
      return true;
    }

    public override void OnMouseMove(float2 mousePosition)
    {
      if (this.selectedObject == null)
        return;
      if (this.state == MainPanel.State.Scaling)
      {
        this.selectedObject.RecalculateAbsCoords();
        (this.selectedObject as IScalable).Scale(mousePosition - this.previousPosition);
      }
      else if (this.state == MainPanel.State.Dragging)
      {
        this.selectedObject.SetPosition(this.selectedObject.GetPosition() + mousePosition - this.previousPosition);
        this.selectedObject.RecalculateAbsCoords();
      }
      this.previousPosition = mousePosition;
    }

    public override bool OnKeyDown(KeyCode keyboardKey, Action action)
    {
      if (this.selectedObject != null)
      {
        if (keyboardKey == KeyCode.DownArrow)
          this.selectedObject.SetPosition(this.selectedObject.GetPosition() + new float2(0.0f, 1f));
        if (keyboardKey == KeyCode.UpArrow)
          this.selectedObject.SetPosition(this.selectedObject.GetPosition() + new float2(0.0f, -1f));
        if (keyboardKey == KeyCode.LeftArrow)
          this.selectedObject.SetPosition(this.selectedObject.GetPosition() + new float2(-1f, 0.0f));
        if (keyboardKey == KeyCode.RightArrow)
          this.selectedObject.SetPosition(this.selectedObject.GetPosition() + new float2(1f, 0.0f));
      }
      return base.OnKeyDown(keyboardKey, action);
    }

    public override void Update()
    {
      if (this.selectedObject != null)
      {
        float2 float2 = float2.zero;
        if (Input.GetKeyDown(KeyCode.DownArrow))
          float2 = new float2(0.0f, 1f);
        if (Input.GetKeyDown(KeyCode.UpArrow))
          float2 = new float2(0.0f, -1f);
        if (Input.GetKeyDown(KeyCode.LeftArrow))
          float2 = new float2(-1f, 0.0f);
        if (Input.GetKeyDown(KeyCode.RightArrow))
          float2 = new float2(1f, 0.0f);
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
          float2 *= 5f;
          if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            float2 *= 4f;
        }
        this.selectedObject.SetPosition(this.selectedObject.GetPosition() + float2);
        this.selectedObject.RecalculateAbsCoords();
      }
      base.Update();
    }

    private enum State
    {
      None,
      Dragging,
      Scaling,
    }
  }
}
