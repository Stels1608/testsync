﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class JButton : JElementBase
  {
    public string normalTexture = "GUI/Common/smallbutton";
    public string pressedTexture = "GUI/Common/smallbutton_click";
    public string overTexture = "GUI/Common/smallbutton_over";
    public string inactiveTexture = "GUI/Common/smallbutton_inactive";
    public JLabel label = new JLabel();
    public JImage image = new JImage();

    public JButton()
      : base("button")
    {
    }

    public JButton(Button gui)
      : base((CustomGUIElement) gui, "button", gui.name)
    {
      this.normalTexture = gui.normal;
      this.pressedTexture = gui.pressed;
      this.overTexture = gui.over;
      this.label = new JLabel(gui.textLabel as Label);
    }

    public JButton(GuiElementBase element)
      : base(element)
    {
      GuiButton guiButton = element as GuiButton;
      this.label = guiButton.Label.Convert2Json() as JLabel;
      this.image = guiButton.Image.Convert2Json() as JImage;
      this.normalTexture = guiButton.NormalTexture.name;
      this.pressedTexture = guiButton.PressedTexture.name;
      this.overTexture = guiButton.OverTexture.name;
      this.inactiveTexture = guiButton.InactiveTexture.name;
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiButton((JElementBase) this);
    }

    public override CustomGUIElement GetGUIElement()
    {
      return (CustomGUIElement) new Button(this);
    }

    public GUIButtonNew CreateGUIButtonNew()
    {
      return this.CreateGUIButtonNew((SmartRect) null);
    }

    public GUIButtonNew CreateGUIButtonNew(SmartRect parent)
    {
      GUIButtonNew guiButtonNew = new GUIButtonNew((Texture2D) ResourceLoader.Load(this.normalTexture), (Texture2D) ResourceLoader.Load(this.overTexture), (Texture2D) ResourceLoader.Load(this.pressedTexture), parent);
      guiButtonNew.Name = this.name;
      guiButtonNew.SmartRect.Width = this.size.x;
      guiButtonNew.SmartRect.Height = this.size.y;
      guiButtonNew.Position = this.position;
      guiButtonNew.TextLabel = this.label.CreateGUILabelNew(guiButtonNew.SmartRect);
      return guiButtonNew;
    }
  }
}
