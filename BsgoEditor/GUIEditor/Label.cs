﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.Label
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class Label : GUILabel, IScalable
  {
    public string name = "Label";
    public int fontSize = 11;
    private Texture2D background = TextureCache.Get(ColorUtility.MakeColorFrom0To255(200U, 200U, 200U));
    public string fontPath;
    private bool drawScalingPivot;
    private SmartRect scalingPivot;

    public bool DrawScalingPivot
    {
      get
      {
        return this.drawScalingPivot;
      }
      set
      {
        this.drawScalingPivot = value;
      }
    }

    public SmartRect ScalingPivot
    {
      get
      {
        return this.scalingPivot;
      }
    }

    public float2 Size
    {
      get
      {
        return new float2(this.rect.Rect.width, this.rect.Rect.height);
      }
      set
      {
        if ((double) value.x < 1.0 || (double) value.y < 1.0)
          return;
        this.rect.Rect = new Rect(this.GetPosition().x - value.x / 2f, this.GetPosition().y - value.y / 2f, value.x, value.y);
        this.RecalculateAbsCoords();
      }
    }

    public Label(string text, float2 position)
      : base(text, (SmartRect) null)
    {
      this.SetPosition(position);
      this.scalingPivot = new SmartRect(new Rect(0.0f, 0.0f, 10f, 10f), float2.zero, (SmartRect) null);
    }

    public Label(JLabel desc)
      : this(desc.text, desc.position)
    {
      this.name = desc.name;
      this.Size = desc.size;
      this.style.font = (Font) ResourceLoader.Load(desc.fontPath);
      this.style.fontSize = desc.fontSize;
      this.style.alignment = desc.textAlignment;
      this.fontPath = desc.fontPath;
      this.fontSize = desc.fontSize;
      this.style.wordWrap = desc.useWordWrap;
      this.SetNormalColor(desc.normalColor);
      this.SetOverColor(desc.overColor);
    }

    public override void Draw()
    {
      GUI.DrawTexture(this.rect.AbsRect, (Texture) this.background);
      base.Draw();
      if (!this.DrawScalingPivot)
        return;
      GUI.Button(this.scalingPivot.AbsRect, string.Empty);
    }

    public void Scale(float2 delta)
    {
      this.Size += delta * 2f;
    }

    public override void RecalculateAbsCoords()
    {
      base.RecalculateAbsCoords();
      this.scalingPivot.Position = this.rect.AbsPosition + new float2(this.rect.Width / 2f, this.rect.Height / 2f);
      this.scalingPivot.RecalculateAbsCoords();
    }
  }
}
