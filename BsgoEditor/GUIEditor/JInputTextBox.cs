﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JInputTextBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class JInputTextBox : JElementBase
  {
    private static readonly string FONT_PATH = "GUI/Fonts/";
    public int m_maxLength = 32;
    public string m_emptyStringMessage = "<Enter Text Here>";
    public string m_fontPath = JInputTextBox.FONT_PATH + "BGM BT";
    public TextAnchor m_textAlignment = TextAnchor.MiddleCenter;
    public Color m_textColor = Color.gray;
    public float m_textPadding = 5f;
    public uint m_textCharactersAllowed = uint.MaxValue;
    public JImage m_background = new JImage();

    public JInputTextBox()
      : base("InputTextBox")
    {
    }

    public JInputTextBox(GuiElementBase element)
      : base(element)
    {
      GuiInputTextBox guiInputTextBox = element as GuiInputTextBox;
      this.m_maxLength = guiInputTextBox.MaxLength;
      this.m_emptyStringMessage = guiInputTextBox.EmptyStringMessage;
      this.m_textPadding = guiInputTextBox.TextPadding;
      if (!guiInputTextBox.Font.name.Contains(JInputTextBox.FONT_PATH))
        guiInputTextBox.Font.name = JInputTextBox.FONT_PATH + guiInputTextBox.Font.name;
      this.m_fontPath = guiInputTextBox.Font.name;
      this.m_textAlignment = guiInputTextBox.Alignment;
      this.m_textColor = guiInputTextBox.TextColor;
      this.m_textCharactersAllowed = Convert.ToUInt32((object) guiInputTextBox.TextValuesAllowed);
      this.m_background = new JImage((GuiElementBase) guiInputTextBox.BackgroundImage);
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiInputTextBox(this);
    }
  }
}
