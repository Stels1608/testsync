﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JCostPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.GameItem;

namespace BsgoEditor.GUIEditor
{
  public class JCostPanel : JPanel
  {
    public JCostPanel()
      : base("costPanel")
    {
    }

    public JCostPanel(GuiElementBase element)
      : base(element)
    {
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiCostPanel(this);
    }
  }
}
