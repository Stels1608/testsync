﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.Loaders
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using CouchDB;
using Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class Loaders
  {
    private static readonly string BASE_DIRECTORY = "Assets/Resources/GUI/";
    private static readonly string FIELD_NAME = "Elements";

    public static JWindowDescription Load(string documentName)
    {
      documentName = Loaders.BASE_DIRECTORY + documentName;
      StreamReader streamReader = new StreamReader(documentName);
      JsonData desc = JsonReader.Parse(streamReader.ReadToEnd());
      streamReader.Close();
      return Loaders.Parse(desc);
    }

    public static JWindowDescription LoadFromCDB(string documentName, string path)
    {
      return Loaders.Parse(ContentDB.GetDocumentRaw(documentName, path));
    }

    public static JWindowDescription LoadFromString(string layout)
    {
      return Loaders.Parse(JsonReader.Parse(layout));
    }

    private static JWindowDescription Parse(JsonData desc)
    {
      JWindowDescription jwindowDescription = JsonSerializator.Deserialize<JWindowDescription>(desc);
      desc = desc.Object[Loaders.FIELD_NAME];
      jwindowDescription.Elements.Clear();
      using (List<JsonData>.Enumerator enumerator = desc.Array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JsonData current = enumerator.Current;
          string @string = current.Object["type"].String;
          if (@string == "label")
            jwindowDescription.Elements.Add((JElementBase) JsonSerializator.Deserialize<JLabel>(current));
          else if (@string == "button")
            jwindowDescription.Elements.Add((JElementBase) JsonSerializator.Deserialize<JButton>(current));
          else if (@string == "image")
            jwindowDescription.Elements.Add((JElementBase) JsonSerializator.Deserialize<JImage>(current));
          else if (@string == "pivot")
            jwindowDescription.Elements.Add((JElementBase) JsonSerializator.Deserialize<JPivot>(current));
        }
      }
      return jwindowDescription;
    }

    public static JWindowDescription LoadResource(string documentName)
    {
      if (documentName.Contains(".txt"))
      {
        int startIndex = documentName.LastIndexOf(".txt");
        documentName = documentName.Remove(startIndex, ".txt".Length);
      }
      JsonData json = JsonReader.Parse(((TextAsset) ResourceLoader.Load(documentName)).text);
      JWindowDescription jwindowDescription = JsonSerializator.Deserialize<JWindowDescription>(json);
      Loaders.ParseJsonData(json, ref jwindowDescription.Elements);
      return jwindowDescription;
    }

    private static void ParseJsonData(JsonData json, ref List<JElementBase> result)
    {
      result.Clear();
      json = json.Object[Loaders.FIELD_NAME];
      using (List<JsonData>.Enumerator enumerator = json.Array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JsonData current = enumerator.Current;
          string @string = current.Object["type"].String;
          JElementBase jelementBase;
          if (@string == "label" || @string == "Gui.GuiLabel")
            jelementBase = (JElementBase) JsonSerializator.Deserialize<JLabel>(current);
          else if (@string == "button" || @string == "Gui.GuiButton")
            jelementBase = (JElementBase) JsonSerializator.Deserialize<JButton>(current);
          else if (@string == "image" || @string == "Gui.GuiImage")
            jelementBase = (JElementBase) JsonSerializator.Deserialize<JImage>(current);
          else if (@string == "pivot" || @string == "Gui.GuiPivot")
            jelementBase = (JElementBase) JsonSerializator.Deserialize<JPivot>(current);
          else if (@string == "Gui.GuiPanel")
          {
            JPanel jpanel = JsonSerializator.Deserialize<JPanel>(current);
            List<JElementBase> result1 = new List<JElementBase>();
            Loaders.ParseJsonData(current, ref result1);
            jpanel.Elements.Clear();
            for (int index = 0; index < result1.Count; ++index)
              jpanel.Elements.Add(result1[index]);
            jelementBase = (JElementBase) jpanel;
          }
          else
          {
            jelementBase = JsonSerializator.Deserialize(System.Type.GetType(@string), current) as JElementBase;
            if (jelementBase != null)
              jelementBase.Deserialize(current);
          }
          if (jelementBase != null)
            result.Add(jelementBase);
          else
            Debug.LogError((object) ("The current json element, " + @string + ", was not Deserialized correctly"));
        }
      }
    }

    public static void Save(string documentName, MainPanel panel)
    {
      documentName = Loaders.BASE_DIRECTORY + documentName;
      StreamWriter streamWriter = new StreamWriter(documentName);
      JsonData jsonData = Loaders.SaveBase(panel);
      streamWriter.Write(jsonData.ToJsonString(string.Empty));
      streamWriter.Close();
      Debug.Log((object) (documentName + " saved."));
    }

    public static void SaveToCDB(string documentName, string path, MainPanel panel)
    {
      JsonData jsonData = Loaders.SaveBase(panel);
      ContentDB.UpdateDocumentJson(documentName, path, jsonData);
      Debug.Log((object) (documentName + " saved to CDB"));
    }

    private static JsonData SaveBase(MainPanel panel)
    {
      return JsonSerializator.Serialize((object) new JWindowDescription() { Elements = Loaders.GrabElements(panel.elements) });
    }

    private static List<JElementBase> GrabElements(List<CustomGUIElement> elements)
    {
      List<JElementBase> jelementBaseList = new List<JElementBase>();
      using (List<CustomGUIElement>.Enumerator enumerator = elements.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          CustomGUIElement current = enumerator.Current;
          Debug.Log((object) current.GetType());
          JElementBase jelementBase = Loaders.GrabElement(current);
          jelementBaseList.Add(jelementBase);
        }
      }
      return jelementBaseList;
    }

    public static JElementBase GrabElement(CustomGUIElement el)
    {
      if (el == null)
        return (JElementBase) null;
      JElementBase jelementBase;
      if (el is Label)
        jelementBase = (JElementBase) new JLabel(el as Label);
      else if (el is Button)
        jelementBase = (JElementBase) new JButton(el as Button);
      else if (el is Image)
        jelementBase = (JElementBase) new JImage(el as Image);
      else if (el is Pivot)
      {
        jelementBase = (JElementBase) new JPivot(el as Pivot);
      }
      else
      {
        if (!(el is Panel))
          throw new Exception("Type " + el.GetType().ToString() + " is not supported.");
        jelementBase = (JElementBase) new JPanel(el as Panel);
      }
      if (el is IScalable)
        jelementBase.size = (el as IScalable).Size;
      return jelementBase;
    }

    public static CustomGUIElement CreateGuiElement(JElementBase el)
    {
      string key = el.type;
      if (key != null)
      {
        // ISSUE: reference to a compiler-generated field
        if (Loaders.\u003C\u003Ef__switch\u0024map9 == null)
        {
          // ISSUE: reference to a compiler-generated field
          Loaders.\u003C\u003Ef__switch\u0024map9 = new Dictionary<string, int>(5)
          {
            {
              "image",
              0
            },
            {
              "button",
              1
            },
            {
              "label",
              2
            },
            {
              "pivot",
              3
            },
            {
              "panel",
              4
            }
          };
        }
        int num;
        // ISSUE: reference to a compiler-generated field
        if (Loaders.\u003C\u003Ef__switch\u0024map9.TryGetValue(key, out num))
        {
          switch (num)
          {
            case 0:
              return (el as JImage).GetGUIElement();
            case 1:
              return (el as JButton).GetGUIElement();
            case 2:
              return (el as JLabel).GetGUIElement();
            case 3:
              return (el as JPivot).GetGUIElement();
            case 4:
              return (el as JPanel).GetGUIElement();
          }
        }
      }
      throw new ArgumentException("This type is not supported:" + el.type);
    }
  }
}
