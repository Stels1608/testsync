﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JSalesPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.GameItem;

namespace BsgoEditor.GUIEditor
{
  public class JSalesPanel : JPanel
  {
    public JSalesPanel()
      : base("YourJElement")
    {
    }

    public JSalesPanel(GuiElementBase element)
      : base(element)
    {
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiShopSalePanel(this);
    }
  }
}
