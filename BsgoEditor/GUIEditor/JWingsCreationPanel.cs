﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JWingsCreationPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

namespace BsgoEditor.GUIEditor
{
  public class JWingsCreationPanel : JPanel
  {
    public int m_minWingNameLength = 3;

    public JWingsCreationPanel()
      : base("WingsManagementPanel")
    {
    }

    public JWingsCreationPanel(GuiElementBase element)
      : base(element)
    {
      this.m_minWingNameLength = (element as GuiWingsCreationPanel).MinWingNameLength;
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiWingsCreationPanel(this);
    }
  }
}
