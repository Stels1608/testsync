﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JWingsRosterPanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

namespace BsgoEditor.GUIEditor
{
  public class JWingsRosterPanel : JPanel
  {
    public JWingsRosterPanel()
      : base("WingsRosterPanel")
    {
    }

    public JWingsRosterPanel(GuiElementBase element)
      : base(element)
    {
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiWingsRosterPanel(this);
    }
  }
}
