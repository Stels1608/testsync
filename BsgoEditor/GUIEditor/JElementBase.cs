﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JElementBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Json;
using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class JElementBase
  {
    public string name = string.Empty;
    public float2 position = new float2();
    public float2 size = new float2();
    public Vector4 elementPadding = new Vector4(5f, 5f, 5f, 5f);
    public string type;
    public Align alignment;
    public bool fullPanel;
    public bool matchParentWidth;
    public bool matchParentHeight;
    [JsonField(JsonName = "action", SkipNull = true)]
    public string action;

    public JElementBase(string name)
    {
      this.name = name;
    }

    public JElementBase()
      : this(string.Empty)
    {
    }

    public JElementBase(CustomGUIElement gui, string type, string name)
      : this(name)
    {
      this.position = gui.GetPosition();
      this.size = new float2(gui.GetRect().Width, gui.GetRect().Height);
      this.type = type;
    }

    public JElementBase(GuiElementBase element)
    {
      this.name = element.Name;
      this.position = float2.FromV2(element.Position);
      this.size = float2.FromV2(element.Size);
      this.type = this.GetType().ToString();
      this.alignment = element.Align;
      this.elementPadding = (Vector4) element.ElementPadding;
      if (element.ActionString != null && element.ActionString != string.Empty)
        this.action = element.ActionString;
      this.fullPanel = element.IsFullPanel;
      this.matchParentWidth = element.MatchParentWidth;
      this.matchParentHeight = element.MatchParentHeight;
    }

    public override string ToString()
    {
      return string.Format("Name: {0}, Type: {1}, Action: {2}", (object) this.name, (object) this.type, (object) this.action);
    }

    public virtual GuiElementBase Convert2Gui()
    {
      return new GuiElementBase(this);
    }

    public virtual void Deserialize(JsonData jsonData)
    {
    }

    public virtual CustomGUIElement GetGUIElement()
    {
      return (CustomGUIElement) null;
    }

    public class JRect
    {
      public float m_XMin;
      public float m_YMin;
      public float m_Width;
      public float m_Height;

      public JRect()
      {
      }

      public JRect(float x, float y, float width, float height)
      {
        this.m_XMin = x;
        this.m_YMin = y;
        this.m_Width = width;
        this.m_Height = height;
      }

      public static implicit operator Rect(JElementBase.JRect rect)
      {
        return new Rect(rect.m_XMin, rect.m_YMin, rect.m_Width, rect.m_Height);
      }

      public static implicit operator JElementBase.JRect(Rect rect)
      {
        return new JElementBase.JRect(rect.x, rect.y, rect.width, rect.height);
      }
    }

    public class NullableColor
    {
      public float a = 1f;
      public float r;
      public float g;
      public float b;

      public NullableColor()
      {
      }

      public NullableColor(Color color)
      {
        this.r = color.r;
        this.g = color.g;
        this.b = color.b;
        this.a = color.a;
      }

      public static implicit operator Color(JElementBase.NullableColor color)
      {
        return new Color(color.r, color.g, color.b, color.a);
      }

      public static implicit operator JElementBase.NullableColor(Color color)
      {
        return new JElementBase.NullableColor(color);
      }
    }
  }
}
