﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JDropDownMenu
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

namespace BsgoEditor.GUIEditor
{
  public class JDropDownMenu : JPanel
  {
    public JDropDownMenu()
      : base("DropDownMenu")
    {
    }

    public JDropDownMenu(GuiElementBase element)
      : base(element)
    {
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiDropdownMenu(this);
    }
  }
}
