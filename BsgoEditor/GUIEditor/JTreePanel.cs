﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JTreePanel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

namespace BsgoEditor.GUIEditor
{
  public class JTreePanel : JPanel
  {
    public float m_childIndent = 10f;
    public bool m_showHeaderInfo = true;
    public bool m_showBackground = true;
    public bool m_isAlwayOpen;
    public float m_animationLength;

    public JTreePanel()
      : base("treePanel")
    {
    }

    public JTreePanel(GuiElementBase element)
      : base(element)
    {
      GuiTreePanel guiTreePanel = element as GuiTreePanel;
      this.m_childIndent = guiTreePanel.ChildIndent;
      this.m_isAlwayOpen = guiTreePanel.IsAlwaysOpen;
      this.m_animationLength = guiTreePanel.AnimationLength;
      this.m_showHeaderInfo = guiTreePanel.ShowHeaderInfo;
      this.m_showBackground = guiTreePanel.ShowBackground;
      this.Elements.Add((JElementBase) new JImage((GuiElementBase) guiTreePanel.BackgroundImage));
      this.Elements.Add((JElementBase) new JButton((GuiElementBase) guiTreePanel.HeaderButton));
      for (int index = 0; index < guiTreePanel.PanelStateImage.Length; ++index)
        this.Elements.Add((JElementBase) new JImage((GuiElementBase) guiTreePanel.PanelStateImage[index]));
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiTreePanel(this);
    }
  }
}
