﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JImage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class JImage : JElementBase
  {
    public int padding = -1;
    public Vector4 nineSliceEdge = new Vector4(0.0f, 0.0f, 0.0f, 0.0f);
    public JElementBase.JRect sourceRect = new JElementBase.JRect(0.0f, 0.0f, 1f, 1f);
    public string texture;
    [JsonField(JsonName = "overlayColor", SkipNull = true)]
    public JElementBase.NullableColor overlayColor;
    [JsonField(JsonName = "inactiveColor", SkipNull = true)]
    public JElementBase.NullableColor inactiveColor;

    public JImage()
      : base("image")
    {
    }

    public JImage(Image gui)
      : base((CustomGUIElement) gui, "image", gui.name)
    {
      this.texture = gui.textureName;
      this.padding = gui.Padding;
      if (string.IsNullOrEmpty(gui.action))
        return;
      this.action = gui.action;
    }

    public JImage(GuiElementBase element)
      : base(element)
    {
      GuiImage guiImage = element as GuiImage;
      this.texture = guiImage.Texture.name;
      this.nineSliceEdge = (Vector4) guiImage.NineSliceEdge;
      this.sourceRect = (JElementBase.JRect) guiImage.SourceRect;
      if (guiImage.OverlayColor.HasValue)
        this.overlayColor = (JElementBase.NullableColor) guiImage.OverlayColor.Value;
      else
        this.overlayColor = (JElementBase.NullableColor) null;
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiImage((JElementBase) this);
    }

    public override CustomGUIElement GetGUIElement()
    {
      return (CustomGUIElement) new Image(this);
    }

    public GUIImageNew CreateGUIImageNew()
    {
      return this.CreateGUIImageNew((SmartRect) null);
    }

    public GUIImageNew CreateGUIImageNew(SmartRect parent)
    {
      GUIImageNew guiImageNew = new GUIImageNew((Texture2D) ResourceLoader.Load(this.texture), parent);
      guiImageNew.Name = this.name;
      guiImageNew.SmartRect.Width = this.size.x;
      guiImageNew.SmartRect.Height = this.size.y;
      guiImageNew.Position = this.position;
      guiImageNew.Padding = this.padding;
      return guiImageNew;
    }
  }
}
