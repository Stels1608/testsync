﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.ColoredBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class ColoredBox
  {
    private Rect rect;
    private Texture2D texture;

    public Color Color
    {
      get
      {
        return this.texture.GetPixel(0, 0);
      }
      set
      {
        this.texture = TextureCache.Get(value);
      }
    }

    public float2 Size
    {
      get
      {
        return new float2(this.rect.width, this.rect.height);
      }
      set
      {
        this.rect = new Rect(this.rect.x, this.rect.y, value.x, value.y);
      }
    }

    public float2 Position
    {
      get
      {
        return new float2(this.rect.x, this.rect.y);
      }
      set
      {
        this.rect = new Rect(value.x, value.y, this.rect.width, this.rect.height);
      }
    }

    public ColoredBox(float2 size, Color color)
    {
      this.texture = TextureCache.Get(Color.black);
      this.Color = color;
      this.rect = new Rect(0.0f, 0.0f, size.x, size.y);
    }

    public void Draw()
    {
      GUI.DrawTexture(this.rect, (Texture) this.texture);
    }
  }
}
