﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.Panel
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace BsgoEditor.GUIEditor
{
  public class Panel : CustomGUIElement
  {
    public string name = "Panel";
    public List<CustomGUIElement> Elements = new List<CustomGUIElement>();

    public Panel()
    {
      this.rect = new SmartRect();
    }

    public Panel(JPanel desc)
      : this()
    {
      this.name = desc.name;
      this.SetPosition(desc.position);
      this.SetSize(new Size((int) desc.size.x, (int) desc.size.y));
      for (int index = 0; index < desc.Elements.Count; ++index)
      {
        CustomGUIElement guiElement = desc.Elements[index].GetGUIElement();
        if (guiElement != null)
          this.Elements.Add(guiElement);
      }
    }

    public override void Draw()
    {
      for (int index = 0; index < this.Elements.Count; ++index)
        this.Elements[index].Draw();
    }
  }
}
