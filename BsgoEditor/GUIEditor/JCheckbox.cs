﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.JCheckbox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

namespace BsgoEditor.GUIEditor
{
  public class JCheckbox : JElementBase
  {
    public string normalTexture = "GUI/_NewGui/Buttons/btn_checkbox_idle";
    public string pressedTexture = "GUI/_NewGui/Buttons/btn_checkbox_click";
    public string overTexture = "GUI/_NewGui/Buttons/btn_checkbox_over";
    public string inactiveTexture = "GUI/_NewGui/Buttons/btn_checkbox_inactive";
    public string normalTexture_x = "GUI/_NewGui/Buttons/btn_checkedbox_idle";
    public string pressedTexture_x = "GUI/_NewGui/Buttons/btn_checkedbox_click";
    public string overTexture_x = "GUI/_NewGui/Buttons/btn_checkedbox_over";
    public string inactiveTexture_x = "GUI/_NewGui/Buttons/btn_checkedbox_inactive";
    public JImage image = new JImage();
    public JLabel label = new JLabel();
    public float gapSpace = 10f;
    public string onToolTip = "%$bgo.checkbox_tooltip.on_tool_tip%";
    public string offToolTip = "%$bgo.checkbox_tooltip.off_tool_tip%";
    public string inactiveToolTip = "%$bgo.checkbox_tooltip.inactive_tool_tip%";

    public JCheckbox()
      : base("checkbox")
    {
    }

    public JCheckbox(GuiElementBase element)
      : base(element)
    {
      GuiCheckbox guiCheckbox = element as GuiCheckbox;
      this.image = guiCheckbox.Image.Convert2Json() as JImage;
      this.label = guiCheckbox.Label.Convert2Json() as JLabel;
      this.normalTexture = guiCheckbox.NormalTexture.name;
      this.pressedTexture = guiCheckbox.PressedTexture.name;
      this.overTexture = guiCheckbox.OverTexture.name;
      this.inactiveTexture = guiCheckbox.InactiveTexture.name;
      this.normalTexture_x = guiCheckbox.NormalXTexture.name;
      this.pressedTexture_x = guiCheckbox.PressedXTexture.name;
      this.overTexture_x = guiCheckbox.OverXTexture.name;
      this.inactiveTexture_x = guiCheckbox.InactiveXTexture.name;
      this.gapSpace = guiCheckbox.GapSpace;
      this.inactiveToolTip = guiCheckbox.InactiveToolTip;
      this.onToolTip = guiCheckbox.OnToolTip;
      this.offToolTip = guiCheckbox.OffToolTip;
    }

    public override GuiElementBase Convert2Gui()
    {
      return (GuiElementBase) new GuiCheckbox(this);
    }
  }
}
