﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.Image
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class Image : GUIImage, IScalable
  {
    public string name = "Image";
    public string action = string.Empty;
    public string textureName;
    private bool drawScalingPivot;
    private SmartRect scalingPivot;

    public bool DrawScalingPivot
    {
      get
      {
        return this.drawScalingPivot;
      }
      set
      {
        this.drawScalingPivot = value;
      }
    }

    public SmartRect ScalingPivot
    {
      get
      {
        return this.scalingPivot;
      }
    }

    public float2 Size
    {
      get
      {
        return new float2(this.rect.Rect.width, this.rect.Rect.height);
      }
      set
      {
        if ((double) value.x < 1.0 || (double) value.y < 1.0)
          return;
        this.rect.Rect = new Rect(this.GetPosition().x - value.x / 2f, this.GetPosition().y - value.y / 2f, value.x, value.y);
        this.RecalculateAbsCoords();
      }
    }

    public Image(string textureName, float2 position)
      : base((Texture2D) ResourceLoader.Load(textureName), (SmartRect) null)
    {
      this.textureName = textureName;
      this.SetPosition(position);
      this.scalingPivot = new SmartRect(new Rect(0.0f, 0.0f, 10f, 10f), float2.zero, (SmartRect) null);
    }

    public Image(JImage desc)
      : this(desc.texture, desc.position)
    {
      this.name = desc.name;
      this.Size = desc.size;
      this.padding = desc.padding;
      if (string.IsNullOrEmpty(desc.action))
        return;
      this.action = desc.action;
    }

    public override void Draw()
    {
      base.Draw();
      if (!this.DrawScalingPivot)
        return;
      GUI.Button(this.scalingPivot.AbsRect, string.Empty);
    }

    public void Scale(float2 delta)
    {
      this.Size += delta * 2f;
    }

    public override void RecalculateAbsCoords()
    {
      base.RecalculateAbsCoords();
      this.scalingPivot.Position = this.rect.AbsPosition + new float2(this.rect.Width / 2f, this.rect.Height / 2f);
      this.scalingPivot.RecalculateAbsCoords();
    }
  }
}
