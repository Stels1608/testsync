﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUIEditor.Button
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor.GUIEditor
{
  public class Button : GUIButton, IScalable
  {
    public string name = "Button";
    public string normal;
    public string pressed;
    public string over;
    private bool drawScalingPivot;
    private SmartRect scalingPivot;

    public Texture2D NormalTexture
    {
      get
      {
        return this.normalTexture;
      }
      set
      {
        this.normalTexture = value;
      }
    }

    public Texture2D PressedTexture
    {
      get
      {
        return this.pressedTexture;
      }
      set
      {
        this.pressedTexture = value;
      }
    }

    public Texture2D OverTexture
    {
      get
      {
        return this.overTexture;
      }
      set
      {
        this.overTexture = value;
      }
    }

    public bool DrawScalingPivot
    {
      get
      {
        return this.drawScalingPivot;
      }
      set
      {
        this.drawScalingPivot = value;
      }
    }

    public SmartRect ScalingPivot
    {
      get
      {
        return this.scalingPivot;
      }
    }

    public float2 Size
    {
      get
      {
        return new float2(this.rect.Rect.width, this.rect.Rect.height);
      }
      set
      {
        if ((double) value.x < 1.0 || (double) value.y < 1.0)
          return;
        this.rect.Rect = new Rect(this.GetPosition().x - value.x / 2f, this.GetPosition().y - value.y / 2f, value.x, value.y);
        this.RecalculateAbsCoords();
      }
    }

    public Button(string normal, string pressed, string over, float2 position)
      : base((Texture2D) ResourceLoader.Load(normal), (Texture2D) ResourceLoader.Load(pressed), (Texture2D) ResourceLoader.Load(over), false, (SmartRect) null, string.Empty)
    {
      this.normal = normal;
      this.pressed = pressed;
      this.over = over;
      this.SetPosition(position);
      this.scalingPivot = new SmartRect(new Rect(0.0f, 0.0f, 10f, 10f), float2.zero, (SmartRect) null);
    }

    public Button(JButton desc)
      : this(desc.normalTexture, desc.pressedTexture, desc.overTexture, desc.position)
    {
      this.name = desc.name;
      this.Size = desc.size;
      this.textLabel = (GUILabel) (desc.label.GetGUIElement() as Label);
      this.textLabel.GetRect().Parent = this.GetRect();
    }

    public override void Draw()
    {
      base.Draw();
      if (!this.DrawScalingPivot)
        return;
      GUI.Button(this.scalingPivot.AbsRect, string.Empty);
    }

    public void Scale(float2 delta)
    {
      this.Size += delta * 2f;
    }

    public override void RecalculateAbsCoords()
    {
      base.RecalculateAbsCoords();
      this.scalingPivot.Position = this.rect.AbsPosition + new float2(this.rect.Width / 2f, this.rect.Height / 2f);
      this.scalingPivot.RecalculateAbsCoords();
    }
  }
}
