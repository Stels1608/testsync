﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.CruiserSP
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BsgoEditor
{
  [JsonClassDesc("gameObject/name", JsonName = "name")]
  [JsonClassDesc("gameObject/transform/rotation", JsonName = "rotation")]
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  [JsonClassDesc("gameObject/transform/position", JsonName = "position")]
  public class CruiserSP
  {
    private static readonly Vector3 defaultSize = new Vector3(90f, 90f, 600f);
    [JsonField(deserialize = false, serialize = false)]
    public JsonData m_jCruiserData = new JsonData(JsonType.Null);
    [JsonField(deserialize = false, serialize = false)]
    public JsonData m_jShipData = new JsonData(JsonType.Null);
    [JsonField(deserialize = false, serialize = false)]
    public string m_shipName = string.Empty;
    public GameObject gameObject;
    [JsonField(JsonName = "type")]
    public string Type;

    public Color Color
    {
      get
      {
        return this.gameObject.GetComponent<Renderer>().material.color;
      }
      set
      {
        this.gameObject.GetComponent<Renderer>().material.color = value;
      }
    }

    public CruiserSP()
    {
      this.gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
      this.gameObject.name = "CruiserRespawningPoint";
      this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
      this.gameObject.GetComponent<Renderer>().material.color = Color.white;
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject.GetComponent<Collider>());
      this.gameObject.transform.localScale = CruiserSP.defaultSize;
    }

    public void LateInit()
    {
      this.gameObject.name = this.Type;
      this.LoadJData();
      this.SetShipSize();
      this.GetShipName();
    }

    public void LoadJData()
    {
      this.m_jCruiserData = ContentDB.GetDocumentRaw(this.Type);
      if (this.m_jCruiserData == null)
        return;
      this.m_jShipData = ContentDB.GetDocumentRaw(this.m_jCruiserData.Object["Ship"].String);
    }

    public void SetShipSize()
    {
      if (this.m_jShipData == null)
        return;
      this.gameObject.transform.localScale = CruiserSP.defaultSize;
      JsonData jdata = JsonData.ParseJData(new List<string>() { "children", "collider", "size" }, this.m_jShipData.Object["Collider"]);
      if (jdata == null || jdata.Object == null || !jdata.Object.ContainsKey("x"))
        return;
      this.gameObject.transform.localScale = new Vector3(jdata.Object["x"].Float, jdata.Object["y"].Float, jdata.Object["z"].Float);
    }

    public void GetShipName()
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      CruiserSP.\u003CGetShipName\u003Ec__AnonStorey10B nameCAnonStorey10B = new CruiserSP.\u003CGetShipName\u003Ec__AnonStorey10B();
      if (this.m_jShipData == null || !this.m_jShipData.Object.ContainsKey("Name"))
        return;
      // ISSUE: reference to a compiler-generated field
      nameCAnonStorey10B.nameObj = this.m_jShipData.Object["Name"];
      string seed = "| ";
      // ISSUE: reference to a compiler-generated field
      // ISSUE: reference to a compiler-generated field
      if (nameCAnonStorey10B.nameObj != null && !nameCAnonStorey10B.nameObj.IsString)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        // ISSUE: reference to a compiler-generated method
        this.m_shipName = nameCAnonStorey10B.nameObj.Object.Keys.Where<string>(new Func<string, bool>(nameCAnonStorey10B.\u003C\u003Em__271)).Aggregate<string, string>(seed, new Func<string, string, string>(nameCAnonStorey10B.\u003C\u003Em__272));
      }
      else
        this.m_shipName = seed + this.m_jShipData.Object["Name"].ToJsonString(string.Empty) + " |";
    }

    public CruiserSP Clone()
    {
      return new CruiserSP() { gameObject = { transform = { parent = this.gameObject.transform.parent, position = new Vector3(this.gameObject.transform.position.x + 20f, this.gameObject.transform.position.y, this.gameObject.transform.position.z), rotation = this.gameObject.transform.rotation } }, Type = this.Type, Color = this.Color };
    }
  }
}
