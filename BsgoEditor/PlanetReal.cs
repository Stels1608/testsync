﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.PlanetReal
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Json;
using UnityEngine;

namespace BsgoEditor
{
  [JsonClassDesc("specular", JsonName = "specular")]
  [JsonClassDesc("lightDirection", JsonName = "lightDirection")]
  [JsonClassDesc("radius", JsonName = "radius")]
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  [JsonClassDesc("gameObject/transform/position", JsonName = "position")]
  [JsonClassDesc("gameObject/transform/rotation", JsonName = "rotation")]
  [JsonClassDesc("color", JsonName = "color")]
  [JsonClassDesc("shininess", JsonName = "shininess")]
  [JsonClassDesc("key", JsonName = "key")]
  public class PlanetReal : IJsonSerializationListener
  {
    public Color color = new Color(0.5921569f, 0.5921569f, 0.5921569f, 1f);
    public Color specular = new Color(0.0f, 0.6801802f, 1f, 1f);
    public string key = string.Empty;
    public float radius = 1f;
    public GameObject gameObject;
    public float shininess;
    private Vector3 lightDirection;

    public PlanetReal()
    {
      this.gameObject = new GameObject("PlanetReal");
      this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
    }

    public void PostInit()
    {
      Planet planet = new Planet(0U);
      planet.Position = this.gameObject.transform.position;
      planet.Rotation = this.gameObject.transform.rotation;
      planet.WorldCard = EditorLevel.planetCards[this.key].FormWorldCard();
      planet.Scale = this.radius;
      planet.Color = this.color;
      planet.SpecularColor = this.specular;
      planet.Shininess = this.shininess;
      planet.lightDirection = this.lightDirection;
      planet.Constructed = (System.Action<GameObject>) (go => go.transform.parent = this.gameObject.transform);
      planet.Construct();
      planet.Root.name = "PlanetRoot-" + EditorLevel.planetCards[this.key].Id;
    }

    private Vector3 GetLightDirectionPos()
    {
      Transform transformInChildren = HelperUtil.FindTransformInChildren(this.gameObject.transform, Planet.LIGHTDIRNAME);
      if ((UnityEngine.Object) null != (UnityEngine.Object) transformInChildren)
        return transformInChildren.localPosition;
      return Vector3.zero;
    }

    public void JsonBeforeSerialization()
    {
      if (this.gameObject.transform.childCount > 0)
      {
        GameObject gameObject = this.gameObject.transform.GetChild(0).gameObject;
        if ((UnityEngine.Object) null != (UnityEngine.Object) gameObject)
        {
          MeshRenderer componentInChildren = gameObject.GetComponentInChildren<MeshRenderer>();
          this.color = componentInChildren.material.GetColor("_Color");
          this.specular = componentInChildren.material.GetColor("_SpecColor");
          this.shininess = componentInChildren.material.GetFloat("_Shininess");
          this.radius = gameObject.transform.lossyScale.x;
        }
      }
      this.lightDirection = this.GetLightDirectionPos();
    }

    public void JsonSerialization(JsonData json)
    {
    }

    public void JsonDeserialization(JsonData json)
    {
    }
  }
}
