﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GrabbingHelpers
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor
{
  public class GrabbingHelpers
  {
    public static SunDesc GrabSun(GameObject go)
    {
      Sun componentInChildren = go.GetComponentInChildren<Sun>();
      SunDesc sunDesc = new SunDesc();
      sunDesc.name = go.name;
      sunDesc.position = go.transform.position;
      sunDesc.rotation = go.transform.rotation;
      sunDesc.discColor = componentInChildren.sunDiscColor;
      sunDesc.raysColor = componentInChildren.sunRaysColor;
      sunDesc.glowColor = componentInChildren.sunGlowColor;
      sunDesc.streakColor = componentInChildren.sunStreakColor;
      sunDesc.occlusionFade = componentInChildren.sunOcclusionFade;
      sunDesc.scale = go.transform.lossyScale;
      return sunDesc;
    }
  }
}
