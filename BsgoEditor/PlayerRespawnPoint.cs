﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.PlayerRespawnPoint
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor
{
  [JsonClassDesc("gameObject/transform/localScale", JsonName = "box")]
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  [JsonClassDesc("gameObject/transform/position", JsonName = "position")]
  [JsonClassDesc("gameObject/name", JsonName = "name")]
  [JsonClassDesc("gameObject/transform/rotation", JsonName = "rotation")]
  public class PlayerRespawnPoint
  {
    public GameObject gameObject;
    private RespawnPointRenderer renderer;

    public Color CylonColor
    {
      get
      {
        return new Color(1f, 0.15f, 0.1f, 0.5f);
      }
    }

    public Color ColonialColor
    {
      get
      {
        return new Color(0.1f, 0.25f, 1f, 0.5f);
      }
    }

    public Color Color
    {
      get
      {
        return this.renderer.color;
      }
      set
      {
        this.renderer.color = value;
      }
    }

    public PlayerRespawnPoint()
    {
      this.gameObject = new GameObject("PlayerRespawningPoint");
      this.renderer = this.gameObject.AddComponent<RespawnPointRenderer>();
      this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
      this.gameObject.transform.localScale = new Vector3(300f, 300f, 300f);
    }

    public PlayerRespawnPoint Clone()
    {
      return new PlayerRespawnPoint() { gameObject = { transform = { parent = this.gameObject.transform.parent, position = new Vector3(this.gameObject.transform.position.x + 10f, this.gameObject.transform.position.y, this.gameObject.transform.position.z), rotation = this.gameObject.transform.rotation, localScale = this.gameObject.transform.localScale } }, Color = this.Color };
    }
  }
}
