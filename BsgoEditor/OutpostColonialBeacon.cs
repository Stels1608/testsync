﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.OutpostColonialBeacon
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor
{
  [JsonClassDesc("gameObject/transform/position", JsonName = "position")]
  [JsonClassDesc("gameObject/transform/rotation", JsonName = "rotation")]
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  public class OutpostColonialBeacon
  {
    public static string Name = "_OutpostColonialBeacon (magenta Cube)";
    public GameObject gameObject;

    public Color Color
    {
      get
      {
        return this.gameObject.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.color;
      }
      set
      {
        this.gameObject.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.color = value;
      }
    }

    public OutpostColonialBeacon()
    {
      this.gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
      this.gameObject.name = OutpostColonialBeacon.Name;
      this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
      this.gameObject.GetComponent<Renderer>().material.color = Color.magenta;
      this.gameObject.GetComponent<Renderer>().material.shader = Shader.Find("Reflective/Diffuse");
      this.gameObject.transform.localScale = new Vector3(90f, 90f, 90f);
      this.gameObject.transform.position = new Vector3(-1000f, 0.0f, 0.0f);
      Object.Destroy((Object) this.gameObject.GetComponent<Collider>());
    }

    public OutpostColonialBeacon Clone()
    {
      return new OutpostColonialBeacon() { gameObject = { transform = { parent = this.gameObject.transform.parent, position = new Vector3(this.gameObject.transform.position.x + 10f, this.gameObject.transform.position.y, this.gameObject.transform.position.z), rotation = this.gameObject.transform.rotation } }, Color = this.Color };
    }
  }
}
