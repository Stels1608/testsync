﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.ConvoyGo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace BsgoEditor
{
  [JsonClassDesc("gameObject/name", JsonName = "name")]
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  public class ConvoyGo
  {
    [JsonField(JsonName = "points")]
    public List<ConvoyPoint> Points = new List<ConvoyPoint>();
    [JsonField(JsonName = "type")]
    public string Type = "mob_colonial";
    [JsonField(JsonName = "level")]
    public int Level = 1;
    [JsonField(JsonName = "revive_time")]
    public float RevivalTime = 600f;
    [JsonField(JsonName = "defender_revive_time")]
    public float DefenderRevivalTime = 180f;
    [JsonField(JsonName = "event_activation")]
    public List<string> EventActivation = new List<string>();
    public GameObject gameObject;

    public int PointCount
    {
      get
      {
        return this.Points.Count;
      }
      set
      {
        this.SetPointCount(value);
      }
    }

    public ConvoyGo()
    {
      this.gameObject = new GameObject("Empty_Convoy");
      this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
    }

    public void InitAfterAutoConstruct()
    {
      for (int index = 0; index < this.Points.Count; ++index)
        this.Points[index].gameObject.transform.parent = this.gameObject.transform;
      this.UpdateConvoyPoints();
    }

    public void UpdatePointCount()
    {
      int childCount = this.gameObject.transform.childCount;
      if (childCount == this.PointCount)
        return;
      this.PointCount = childCount;
    }

    public void UpdateConvoyPoints()
    {
      for (int index = 0; index < this.PointCount; ++index)
      {
        GameObject gameObject = this.Points[index].gameObject;
        if ((UnityEngine.Object) gameObject != (UnityEngine.Object) null)
        {
          if (index == 0)
          {
            gameObject.GetComponent<Renderer>().sharedMaterial.color = this.Points[index].StartColor;
            gameObject.name = "_Start_" + this.Type;
          }
          else if (index == this.PointCount - 1)
          {
            gameObject.GetComponent<Renderer>().sharedMaterial.color = this.Points[index].EndColor;
            gameObject.name = "End_" + this.Type;
          }
          else
          {
            gameObject.GetComponent<Renderer>().sharedMaterial.color = this.Points[index].DefaultColor;
            gameObject.name = this.Type + "_" + (object) index;
          }
        }
      }
    }

    public void SetPointCount(int number)
    {
      if (number < 0)
        number = 0;
      if (number == this.Points.Count)
        return;
      if (number > this.Points.Count)
      {
        int childCount = this.gameObject.transform.childCount;
        if (number == childCount)
        {
          for (int count = this.Points.Count; count < number; ++count)
            this.Points.Add(new ConvoyPoint(this.gameObject.transform.GetChild(count).gameObject));
        }
        else
        {
          for (int count = this.Points.Count; count < number; ++count)
            this.AddPoint(new ConvoyPoint());
        }
      }
      else
      {
        for (int count = this.Points.Count; count > number; --count)
        {
          if ((UnityEngine.Object) this.Points[count - 1].gameObject == (UnityEngine.Object) null)
          {
            this.Points.RemoveAt(count - 1);
          }
          else
          {
            UnityEngine.Object.Destroy((UnityEngine.Object) this.Points[count - 1].gameObject);
            this.Points.RemoveAt(count - 1);
          }
        }
      }
      this.UpdateConvoyPoints();
    }

    private void AddPoint(ConvoyPoint point)
    {
      this.Points.Add(point);
      point.gameObject.transform.parent = this.gameObject.transform;
    }

    public void AddEventActivation()
    {
      if (EditorLevel.allBonusStrings.Length != 0)
        this.EventActivation.Add(EditorLevel.allBonusStrings[0]);
      else
        Debug.LogWarning((object) "There are not any items in allBonusStrings list");
    }

    public void Merge(ConvoyGo obj)
    {
      if (this == obj)
        throw new Exception("ConvoyPoint: can't merge with myself...");
      this.Points.AddRange((IEnumerable<ConvoyPoint>) obj.Points);
      obj.Type = this.Type;
      obj.Level = this.Level;
      this.InitAfterAutoConstruct();
      UnityEngine.Object.Destroy((UnityEngine.Object) obj.gameObject);
    }

    public ConvoyGo Clone()
    {
      ConvoyGo convoyGo = new ConvoyGo();
      convoyGo.gameObject.transform.parent = this.gameObject.transform.parent;
      convoyGo.gameObject.transform.position = this.gameObject.transform.position;
      convoyGo.Type = this.Type;
      convoyGo.Level = this.Level;
      convoyGo.RevivalTime = this.RevivalTime;
      this.UpdatePointCount();
      for (int index = 0; index < this.Points.Count; ++index)
        convoyGo.AddPoint(this.Points[index].Clone());
      convoyGo.gameObject.transform.Translate(150f, 0.0f, 150f);
      convoyGo.gameObject.name = this.gameObject.name + "_Copy";
      convoyGo.UpdateConvoyPoints();
      return convoyGo;
    }
  }
}
