﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.ButtonNative
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace BsgoEditor.GUI
{
  public class ButtonNative : ElementBase
  {
    public Action<KeyCode> OnClick;
    private string text;

    public string Text
    {
      get
      {
        return this.text;
      }
      set
      {
        this.text = value;
      }
    }

    public ButtonNative(string text)
      : base(100f, 20f)
    {
      this.text = text;
      this.SizeType = SizeTypes.HJustified;
    }

    public override void Draw()
    {
      if (UnityEngine.GUI.Button(this.AbsoluteRect, this.text) && this.OnClick != null)
        this.OnClick(KeyCode.Mouse0);
      base.Draw();
    }
  }
}
