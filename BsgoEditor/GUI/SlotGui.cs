﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.SlotGui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor.GUI
{
  public class SlotGui : ElementBase
  {
    public readonly string slotTexturepath = "GUI/EquipBuyPanel/slot_open";
    private Image image;
    private Label n;
    private ShipSlotType type;

    public string N
    {
      get
      {
        return this.n.Text;
      }
      set
      {
        this.n.Text = value;
      }
    }

    public ShipSlotType Type
    {
      get
      {
        return this.type;
      }
      set
      {
        this.type = value;
      }
    }

    public SlotGui()
      : base(20f, 100f)
    {
      this.image = new Image((Texture2D) ResourceLoader.Load(this.slotTexturepath));
      this.AddChild((ElementBase) this.image);
      this.Size = this.image.Size;
      this.image.PositionFromParentCenter = new float2(0.0f, 0.0f);
      this.n = new Label(string.Empty);
      this.AddChild((ElementBase) this.n);
      this.n.PositionFromParentCenter = new float2(0.0f, 0.0f);
      Label label = new Label(this.type.ToString());
      this.AddChild((ElementBase) label);
      label.Position = new float2(this.Size.x, this.Size.y * 0.3f);
    }
  }
}
