﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.Label
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor.GUI
{
  public class Label : ElementBase
  {
    private GUIStyle style = new GUIStyle();
    private string text;

    public string Text
    {
      get
      {
        return this.GetText();
      }
      set
      {
        this.SetText(value);
      }
    }

    public Label(string text)
      : this(text, Color.white, Color.white, (Font) null)
    {
    }

    public Label(string text, Color normalColor, Color overColor, Font font)
    {
      this.style.normal.textColor = normalColor;
      this.style.font = font;
      this.SetText(text);
    }

    public void SetText(string text)
    {
      this.text = text;
      this.SetSize(TextUtility.CalcTextSize(text, this.style.font, this.style.fontSize));
    }

    public string GetText()
    {
      return this.text;
    }

    public void SetFont(Font font)
    {
      this.style.font = font;
      this.SetSize(TextUtility.CalcTextSize(this.text, this.style.font, this.style.fontSize));
    }

    public override void Draw()
    {
      UnityEngine.GUI.Label(this.AbsoluteRect, this.text, this.style);
      base.Draw();
    }
  }
}
