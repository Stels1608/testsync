﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.DropDownWithCaption
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace BsgoEditor.GUI
{
  public class DropDownWithCaption : ElementBase
  {
    private Label caption = new Label(string.Empty);
    public DropDown dropDown = new DropDown();

    public string Caption
    {
      get
      {
        return this.caption.GetText();
      }
      set
      {
        this.caption.SetText(value);
        this.dropDown.MarkCrude();
      }
    }

    public DropDownWithCaption()
      : base(20f, 100f)
    {
      this.SizeType = SizeTypes.HJustified;
      this.AddChild((ElementBase) this.caption);
      this.AddChild((ElementBase) this.dropDown);
      this.Caption = string.Empty;
    }

    protected override void RecalculateCache()
    {
      base.RecalculateCache();
      this.caption.Position = new float2(0.0f, (float) (this.dropDown.HeightPerItem / 2U) - this.caption.Size.y / 2f);
      this.dropDown.Position = new float2(this.caption.Size.x, 0.0f);
      this.dropDown.Size = new float2(this.Size.x - this.caption.Size.x, this.dropDown.Size.y);
    }
  }
}
