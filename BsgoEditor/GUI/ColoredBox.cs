﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.ColoredBox
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

namespace BsgoEditor.GUI
{
  public class ColoredBox : ElementBase
  {
    private Color color;

    public Color Color
    {
      get
      {
        return this.color;
      }
      set
      {
        this.color = value;
      }
    }

    public ColoredBox(Color color)
      : this(color, Vector2.zero, Vector2.zero)
    {
    }

    public ColoredBox(Color color, Vector2 position, Vector2 size)
    {
      this.Color = color;
    }

    public override void Draw()
    {
      base.Draw();
      if (Event.current.type != EventType.Repaint)
        return;
      Graphics.DrawTexture(this.AbsoluteRect, (Texture) TextureCache.Get(this.color));
    }
  }
}
