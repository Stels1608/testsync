﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.EditFieldWithCaption
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace BsgoEditor.GUI
{
  public class EditFieldWithCaption : ElementBase
  {
    private Label caption = new Label(string.Empty);
    private EditField editField = new EditField();
    public EditField.OnTextChanged onTextChanged;

    public string Caption
    {
      get
      {
        return this.caption.GetText();
      }
      set
      {
        this.caption.SetText(value);
        this.editField.Position = new float2(this.caption.Size.x + 5f, 0.0f);
      }
    }

    public string Text
    {
      get
      {
        return this.editField.Text;
      }
      set
      {
        this.editField.Text = value;
      }
    }

    public EditFieldWithCaption()
    {
      this.editField.onTextChanged = (EditField.OnTextChanged) (text =>
      {
        if (this.onTextChanged == null)
          return;
        this.onTextChanged(text);
      });
      this.AddChild((ElementBase) this.caption);
      this.AddChild((ElementBase) this.editField);
    }
  }
}
