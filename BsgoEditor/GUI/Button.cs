﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.Button
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor.GUI
{
  public class Button : ElementBase
  {
    public System.Action<KeyCode> OnClick;
    private Texture2D normal;
    private Texture2D pressed;
    private Texture2D over;
    private Button.State state;
    private Label label;

    public string Text
    {
      get
      {
        return this.label.GetText();
      }
      set
      {
        this.label.SetText(value);
      }
    }

    public Button(string text, Texture2D normal, Texture2D pressed, Texture2D over)
      : base((float) normal.width, (float) normal.height)
    {
      this.normal = normal;
      this.pressed = pressed;
      this.over = over;
      this.Size = new float2((float) normal.width, (float) normal.height);
      this.label = new Label(text);
      this.label.Align = Align.Center;
      this.AddChild((ElementBase) this.label);
    }

    public Label GetLabel()
    {
      return this.label;
    }

    public override void Draw()
    {
      switch (this.state)
      {
        case Button.State.Normal:
          UnityEngine.GUI.DrawTexture(this.AbsoluteRect, (Texture) this.normal);
          break;
        case Button.State.Over:
          UnityEngine.GUI.DrawTexture(this.AbsoluteRect, (Texture) this.over);
          break;
        case Button.State.Pressed:
          UnityEngine.GUI.DrawTexture(this.AbsoluteRect, (Texture) this.pressed);
          break;
      }
      base.Draw();
    }

    public override void OnMouseEnter(float2 positionAbs, float2 prevPosition)
    {
      this.state = Button.State.Over;
    }

    public override void OnMouseLeave(float2 positionAbs, float2 prevPosition)
    {
      this.state = Button.State.Normal;
    }

    public override void OnMouseDown(KeyCode key, float2 position)
    {
      base.OnMouseDown(key, position);
      this.state = Button.State.Pressed;
    }

    public override void OnMouseUp(KeyCode key, float2 position)
    {
      base.OnMouseUp(key, position);
      this.state = Button.State.Over;
      if (this.OnClick == null)
        return;
      this.OnClick(key);
    }

    public override void OnMouseMove(float2 positionAbs, float2 prevPosition)
    {
      base.OnMouseMove(positionAbs, prevPosition);
    }

    public enum State
    {
      Normal,
      Over,
      Pressed,
    }
  }
}
