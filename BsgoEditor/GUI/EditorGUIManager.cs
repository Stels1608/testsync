﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.EditorGUIManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace BsgoEditor.GUI
{
  public class EditorGUIManager
  {
    private List<EditorGUIManager.Key> keys = new List<EditorGUIManager.Key>();
    private ElementBase screen = new ElementBase((float) Screen.width, (float) Screen.height);
    private readonly float mouseDelta = 0.5f;
    private float resolutionLastCheckTime;
    private float2 prevMousePosition;

    public bool Enabled
    {
      get
      {
        return this.screen.Enabled;
      }
      set
      {
        this.screen.Enabled = value;
      }
    }

    public EditorGUIManager()
    {
      this.RegisterKeys(this.keys);
    }

    private void RegisterKeys(List<EditorGUIManager.Key> keys)
    {
      for (uint index = 97; index < 122U; ++index)
        keys.Add(new EditorGUIManager.Key((KeyCode) index));
      for (uint index = 48; index < 57U; ++index)
        keys.Add(new EditorGUIManager.Key((KeyCode) index));
      keys.Add(new EditorGUIManager.Key(KeyCode.BackQuote));
      keys.Add(new EditorGUIManager.Key(KeyCode.Return));
      keys.Add(new EditorGUIManager.Key(KeyCode.Space));
      keys.Add(new EditorGUIManager.Key(KeyCode.Mouse0));
      keys.Add(new EditorGUIManager.Key(KeyCode.Mouse1));
    }

    public void AddComponent(ElementBase component)
    {
      this.screen.AddChild(component);
    }

    public void SetFocus(ElementBase element)
    {
      this.screen.SetFocus(element);
    }

    public void ChangeResolution()
    {
      this.screen.Size = new float2((float) Screen.width, (float) Screen.height);
      this.resolutionLastCheckTime = Time.time;
    }

    public void Update()
    {
      if ((double) Time.time - (double) this.resolutionLastCheckTime > 0.300000011920929 && ((double) this.screen.Size.x != (double) Screen.width || (double) this.screen.Size.y != (double) Screen.height))
        this.ChangeResolution();
      this.CheckUserInput();
      this.screen.Update();
    }

    private void CheckUserInput()
    {
      using (List<EditorGUIManager.Key>.Enumerator enumerator = this.keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          EditorGUIManager.Key current = enumerator.Current;
          bool key = Input.GetKey(current.code);
          if (key && current.previousState == KeyState.Up)
          {
            this.KeyDown(current.code);
            current.previousState = KeyState.Down;
          }
          else if (!key && current.previousState == KeyState.Down)
          {
            this.KeyUp(current.code);
            current.previousState = KeyState.Up;
          }
        }
      }
      float2 mousePosition = this.GetMousePosition();
      if ((double) (this.prevMousePosition - mousePosition).sqrMagnitude <= (double) this.mouseDelta)
        return;
      this.screen.OnMouseMove(mousePosition, this.prevMousePosition);
      this.prevMousePosition = mousePosition;
    }

    private void KeyDown(KeyCode key)
    {
      if (key >= KeyCode.Mouse0 && key <= KeyCode.Mouse1)
        this.screen.OnMouseDown(key, this.GetMousePosition());
      else
        this.screen.KeyDown(key);
    }

    private void KeyUp(KeyCode key)
    {
      if (key >= KeyCode.Mouse0 && key <= KeyCode.Mouse1)
        this.screen.OnMouseUp(key, this.GetMousePosition());
      else
        this.screen.KeyUp(key);
    }

    private float2 GetMousePosition()
    {
      float2 float2 = float2.FromV2((Vector2) Input.mousePosition);
      float2.y = (float) Screen.height - float2.y;
      return float2;
    }

    public void Draw()
    {
      if (!this.screen.Enabled)
        return;
      this.screen.Draw();
    }

    private class Key
    {
      public KeyState previousState;
      public KeyCode code;

      public Key(KeyCode code)
      {
        this.code = code;
      }
    }
  }
}
