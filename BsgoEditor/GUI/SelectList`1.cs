﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.SelectList`1
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace BsgoEditor.GUI
{
  public class SelectList<T> : ElementBase
  {
    private List<string> Items = new List<string>();
    private List<T> Values = new List<T>();
    public int ItemsPerLine = 1;
    public SelectList<T>.OnChangeDelegate OnChange;
    private int Selected;

    public T SelectedValue
    {
      get
      {
        return this.Values[this.Selected];
      }
      set
      {
        int num = this.Values.IndexOf(value);
        if (num == -1)
          throw new Exception(value.ToString() + " is not present in the list.");
        this.Selected = num;
      }
    }

    public SelectList()
      : this(1)
    {
    }

    public SelectList(int itemsPerLine)
      : base(0.0f, 0.0f)
    {
      this.SizeType = SizeTypes.HJustified;
      this.ItemsPerLine = itemsPerLine;
    }

    public void Add(string title, T value)
    {
      this.Items.Add(title);
      this.Values.Add(value);
      this.Size = new float2(this.Size.x, (float) (this.Items.Count / this.ItemsPerLine * 20));
    }

    public override void Draw()
    {
      int num = UnityEngine.GUI.SelectionGrid(this.AbsoluteRect, this.Selected, this.Items.ToArray(), this.ItemsPerLine);
      base.Draw();
      if (this.Selected == num)
        return;
      this.Selected = num;
      if (this.OnChange == null)
        return;
      this.OnChange(this.Items[this.Selected], this.Values[this.Selected]);
    }

    public delegate void OnChangeDelegate(string title, T value);
  }
}
