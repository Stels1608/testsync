﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.Image
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor.GUI
{
  public class Image : ElementBase
  {
    private Texture2D texture;

    public Texture2D Texture
    {
      get
      {
        return this.texture;
      }
      set
      {
        this.texture = value;
        this.Size = new float2((float) this.texture.width, (float) this.texture.height);
      }
    }

    public Image(Texture2D texture)
    {
      this.Texture = texture;
    }

    public override void Draw()
    {
      UnityEngine.GUI.DrawTexture(this.AbsoluteRect, (UnityEngine.Texture) this.texture);
      base.Draw();
    }
  }
}
