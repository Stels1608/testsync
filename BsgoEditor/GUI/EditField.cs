﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.EditField
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor.GUI
{
  public class EditField : ElementBase
  {
    private GUIStyle style = new GUIStyle();
    public EditField.OnTextChanged onTextChanged;
    private string text;

    public string Text
    {
      get
      {
        return this.text;
      }
      set
      {
        this.SetText(value);
      }
    }

    public EditField()
    {
      this.Text = string.Empty;
    }

    private void SetText(string str)
    {
      if (this.style != null)
      {
        Size size = TextUtility.CalcTextSize(str, this.style.font, this.style.fontSize);
        size = new Size(size.width + 6, size.height + 6);
        this.SetSize(size);
      }
      this.text = str;
    }

    public override void Draw()
    {
      if (this.style == null)
      {
        this.style = UnityEngine.GUI.skin.textArea;
        this.SetText(this.Text);
      }
      string text = UnityEngine.GUI.TextField(this.AbsoluteRect, this.text, this.style);
      if (this.text != text)
      {
        this.Text = text;
        if (this.onTextChanged != null)
          this.onTextChanged(text);
      }
      base.Draw();
    }

    public delegate void OnTextChanged(string text);
  }
}
