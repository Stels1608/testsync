﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.DropDown
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace BsgoEditor.GUI
{
  public class DropDown : ElementBase
  {
    public uint HeightPerItem = 20;
    private GUIStyle style = new GUIStyle();
    private string[] cases = new string[1]{ string.Empty };
    public DropDown.OnChanged onChanged;
    private int selected;
    public DropDown.State state;

    public int Selected
    {
      get
      {
        return this.selected;
      }
      set
      {
        this.selected = value;
      }
    }

    public string SelectedText
    {
      get
      {
        return this.cases[this.selected];
      }
      set
      {
        this.selected = Array.IndexOf<string>(this.cases, value);
      }
    }

    public string[] Cases
    {
      get
      {
        return this.cases;
      }
      set
      {
        this.cases = value;
        this.selected = 0;
      }
    }

    public DropDown()
      : this(new string[1]{ string.Empty })
    {
    }

    public DropDown(string[] cases)
      : base(100f, 20f)
    {
      this.style.normal.textColor = Color.white;
      this.cases = cases;
    }

    public override void Draw()
    {
      if (this.state == DropDown.State.normal)
      {
        if (UnityEngine.GUI.Button(this.AbsoluteRect, this.cases[this.selected]))
        {
          this.Size = new float2(this.Size.x, (float) ((long) this.HeightPerItem * (long) this.cases.Length / 2L));
          this.state = DropDown.State.open;
        }
      }
      else
      {
        int num = UnityEngine.GUI.SelectionGrid(this.AbsoluteRect, this.selected, this.cases, 2);
        if (num != this.selected)
        {
          this.selected = num;
          this.Collapse();
          if (this.onChanged != null)
            this.onChanged(this.selected, this.cases[this.selected]);
        }
      }
      base.Draw();
    }

    private void Collapse()
    {
      this.state = DropDown.State.normal;
      this.Size = new float2(this.Size.x, (float) this.HeightPerItem);
    }

    public override void OnFocusLeave(KeyCode key, float2 position)
    {
      base.OnFocusLeave(key, position);
      if (this.AbsoluteRect.Contains(new Vector2(position.x, position.y)))
        return;
      this.Collapse();
    }

    public enum State
    {
      normal,
      open,
    }

    public delegate void OnChanged(int i, string text);
  }
}
