﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.GUI.ElementBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace BsgoEditor.GUI
{
  public class ElementBase
  {
    private bool enabled = true;
    private List<ElementBase> children = new List<ElementBase>();
    private bool needRecalculate = true;
    private ElementBase parent;
    private ElementBase focused;
    private float2 localPosition;
    private float2 localSize;
    private Rect cachedAbsoluteRect;
    private Align align;
    private SizeTypes sizeType;

    public bool Enabled
    {
      get
      {
        return this.enabled;
      }
      set
      {
        this.enabled = value;
        if (!this.enabled)
          return;
        this.MarkCrude();
      }
    }

    public ElementBase Parent
    {
      get
      {
        return this.parent;
      }
    }

    protected List<ElementBase> Children
    {
      get
      {
        return this.children;
      }
    }

    public Align Align
    {
      get
      {
        return this.align;
      }
      set
      {
        this.align = value;
        this.needRecalculate = true;
      }
    }

    public SizeTypes SizeType
    {
      get
      {
        return this.sizeType;
      }
      set
      {
        this.sizeType = value;
        this.needRecalculate = true;
      }
    }

    public float2 Position
    {
      get
      {
        return this.localPosition;
      }
      set
      {
        this.localPosition = value;
        this.needRecalculate = true;
      }
    }

    public float2 PositionFromParentCenter
    {
      get
      {
        return new float2((float) ((double) this.localPosition.x - (double) this.parent.Size.x / 2.0 + (double) this.Size.x / 2.0), (float) ((double) this.localPosition.y - (double) this.parent.Size.y / 2.0 + (double) this.Size.y / 2.0));
      }
      set
      {
        this.localPosition = new float2((float) ((double) value.x + (double) this.parent.Size.x / 2.0 - (double) this.Size.x / 2.0), (float) ((double) value.y + (double) this.parent.Size.y / 2.0 - (double) this.Size.y / 2.0));
        this.needRecalculate = true;
      }
    }

    public float2 Size
    {
      get
      {
        return this.localSize;
      }
      set
      {
        this.localSize = value;
        this.needRecalculate = true;
      }
    }

    public Rect AbsoluteRect
    {
      get
      {
        if (this.needRecalculate)
          this.RecalculateCache();
        return this.cachedAbsoluteRect;
      }
    }

    public ElementBase(float left, float top, float width, float height)
    {
      this.localPosition = new float2(left, top);
      this.localSize = new float2(width, height);
    }

    public ElementBase(float width, float height)
      : this(0.0f, 0.0f, width, height)
    {
    }

    public ElementBase()
      : this(0.0f, 0.0f)
    {
    }

    public void AddChild(ElementBase child)
    {
      if (child.parent != null)
        child.parent.RemoveChild(child);
      this.children.Add(child);
      child.parent = this;
      child.MarkCrude();
    }

    public void RemoveChild(ElementBase child)
    {
      this.children.Remove(child);
      child.parent = (ElementBase) null;
      child.MarkCrude();
    }

    public void ClearChildren()
    {
      using (List<ElementBase>.Enumerator enumerator = this.children.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ElementBase current = enumerator.Current;
          current.parent = (ElementBase) null;
          current.MarkCrude();
        }
      }
      this.children.Clear();
    }

    public void SetSize(global::Size size)
    {
      this.localSize = new float2((float) size.width, (float) size.height);
      this.needRecalculate = true;
    }

    public virtual void MarkCrude()
    {
      this.needRecalculate = true;
      using (List<ElementBase>.Enumerator enumerator = this.children.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.MarkCrude();
      }
    }

    public bool ContainsAbs(float2 point)
    {
      if ((double) this.AbsoluteRect.x <= (double) point.x && (double) point.x <= (double) this.AbsoluteRect.x + (double) this.localSize.x && (double) this.AbsoluteRect.y <= (double) point.y)
        return (double) point.y <= (double) this.AbsoluteRect.y + (double) this.localSize.y;
      return false;
    }

    protected virtual void RecalculateCache()
    {
      using (List<ElementBase>.Enumerator enumerator = this.children.GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.MarkCrude();
      }
      this.cachedAbsoluteRect = this.CalculateAbsoluteCoordinates();
      this.needRecalculate = false;
    }

    private Rect CalculateAbsoluteCoordinates()
    {
      if (this.parent == null)
        return new Rect(this.localPosition.x, this.localPosition.y, this.localSize.x, this.localSize.y);
      Rect absoluteRect = this.parent.AbsoluteRect;
      switch (this.sizeType)
      {
        case SizeTypes.HJustified:
          this.localSize.x = this.parent.Size.x;
          break;
        case SizeTypes.VJustified:
          this.localSize.y = this.parent.Size.y;
          break;
        case SizeTypes.AllJustified:
          this.localSize = new float2(this.parent.Size.x, this.parent.Size.y);
          break;
      }
      switch (this.Align)
      {
        case Align.LeftUp:
          this.PosLeft();
          this.PosUp();
          break;
        case Align.LeftCenter:
          this.PosLeft();
          this.PosVCenter();
          break;
        case Align.LeftDown:
          this.PosLeft();
          this.PosDown();
          break;
        case Align.LeftCustom:
          this.PosLeft();
          break;
        case Align.CenterUp:
          this.PosHCenter();
          this.PosUp();
          break;
        case Align.Center:
          this.PosHCenter();
          this.PosVCenter();
          break;
        case Align.CenterDown:
          this.PosHCenter();
          this.PosDown();
          break;
        case Align.CenterCustom:
          this.PosHCenter();
          break;
        case Align.RightUp:
          this.PosRight();
          this.PosUp();
          break;
        case Align.RightCenter:
          this.PosRight();
          this.PosVCenter();
          break;
        case Align.RightDown:
          this.PosRight();
          this.PosDown();
          break;
        case Align.RightCustom:
          this.PosRight();
          break;
      }
      return new Rect(absoluteRect.xMin + this.localPosition.x, absoluteRect.yMin + this.localPosition.y, this.localSize.x, this.localSize.y);
    }

    private void PosLeft()
    {
      this.localPosition.x = 0.0f;
    }

    private void PosUp()
    {
      this.localPosition.y = 0.0f;
    }

    private void PosRight()
    {
      this.localPosition.x = this.parent.Size.x - this.localSize.x;
    }

    private void PosDown()
    {
      this.localPosition.y = this.parent.Size.y - this.localSize.y;
    }

    private void PosHCenter()
    {
      this.localPosition.x = (float) (((double) this.parent.Size.x - (double) this.localSize.x) / 2.0);
    }

    private void PosVCenter()
    {
      this.localPosition.y = (float) (((double) this.parent.Size.y - (double) this.localSize.y) / 2.0);
    }

    public override string ToString()
    {
      return this.GetType().ToString() + ": " + this.localPosition.ToString() + " " + this.localSize.ToString();
    }

    public virtual void Draw()
    {
      if (this.needRecalculate)
        this.RecalculateCache();
      for (int index = 0; index < this.Children.Count; ++index)
      {
        ElementBase elementBase = this.Children[index];
        if (elementBase.Enabled)
          elementBase.Draw();
      }
    }

    public virtual void Update()
    {
      using (List<ElementBase>.Enumerator enumerator = this.Children.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ElementBase current = enumerator.Current;
          if (current.Enabled)
            current.Update();
        }
      }
    }

    public virtual void KeyDown(KeyCode key)
    {
      if (this.focused == null)
        return;
      this.focused.KeyDown(key);
    }

    public virtual void KeyUp(KeyCode key)
    {
      if (this.focused == null)
        return;
      this.focused.KeyUp(key);
    }

    public virtual bool OnMouseDownShouldFocusThere(float2 position)
    {
      return true;
    }

    public virtual void OnMouseDown(KeyCode key, float2 position)
    {
      bool flag = false;
      for (int index = this.Children.Count - 1; index >= 0; --index)
      {
        ElementBase elementBase = this.Children[index];
        if (elementBase.Enabled && elementBase.ContainsAbs(position) && elementBase.OnMouseDownShouldFocusThere(position))
        {
          if (this.focused != elementBase)
          {
            if (this.focused != null)
              this.focused.OnFocusLeave(key, position);
            this.focused = elementBase;
            this.focused.OnFocusEnter(key, position);
          }
          flag = true;
          elementBase.OnMouseDown(key, position);
          break;
        }
      }
      if (flag)
        return;
      if (this.focused != null && this.focused.Enabled)
        this.focused.OnFocusLeave(key, position);
      this.focused = (ElementBase) null;
    }

    public virtual void OnFocusEnter(KeyCode key, float2 position)
    {
      if (this.focused == null || !this.focused.Enabled)
        return;
      this.focused.OnFocusEnter(key, position);
    }

    public virtual void OnFocusLeave(KeyCode key, float2 position)
    {
      if (this.focused == null || !this.focused.Enabled)
        return;
      this.focused.OnFocusLeave(key, position);
    }

    public virtual void OnMouseUp(KeyCode key, float2 position)
    {
      if (this.focused == null || !this.focused.Enabled || !this.focused.ContainsAbs(position))
        return;
      this.focused.OnMouseUp(key, position);
    }

    public virtual void OnMouseEnter(float2 positionAbs, float2 prevPosition)
    {
      for (int index = this.Children.Count - 1; index >= 0; --index)
      {
        ElementBase elementBase = this.Children[index];
        if (elementBase.Enabled && elementBase.ContainsAbs(positionAbs))
          elementBase.OnMouseEnter(positionAbs, prevPosition);
      }
    }

    public virtual void OnMouseLeave(float2 positionAbs, float2 prevPosition)
    {
      for (int index = this.Children.Count - 1; index >= 0; --index)
      {
        ElementBase elementBase = this.Children[index];
        if (elementBase.Enabled && elementBase.ContainsAbs(prevPosition) && !elementBase.ContainsAbs(positionAbs))
          elementBase.OnMouseLeave(positionAbs, prevPosition);
      }
    }

    public virtual void OnMouseMove(float2 positionAbs, float2 prevPosition)
    {
      bool flag = false;
      for (int index = this.Children.Count - 1; index >= 0; --index)
      {
        ElementBase elementBase = this.Children[index];
        if (elementBase.Enabled)
        {
          if (!flag && elementBase.ContainsAbs(positionAbs))
          {
            flag = true;
            elementBase.OnMouseMove(positionAbs, prevPosition);
            if (!elementBase.ContainsAbs(prevPosition))
              elementBase.OnMouseEnter(positionAbs, prevPosition);
          }
          if (elementBase.ContainsAbs(prevPosition) && !elementBase.ContainsAbs(positionAbs))
            elementBase.OnMouseLeave(positionAbs, prevPosition);
        }
      }
    }

    public void SetFocus(ElementBase element)
    {
      if (element == null)
      {
        this.focused = (ElementBase) null;
      }
      else
      {
        if (element == this.focused)
          return;
        if (!this.children.Contains(element))
          throw new Exception("Element " + (object) element + " doesn't belong to this GUI manager.");
        this.children.Remove(element);
        this.children.Add(element);
        this.focused = element;
      }
    }
  }
}
