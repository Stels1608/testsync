﻿// Decompiled with JetBrains decompiler
// Type: BsgoEditor.ConvoyPoint
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BsgoEditor
{
  [JsonClassInstantiation(mode = JsonClassInstantiationAttribute.Modes.Manual)]
  [JsonClassDesc("gameObject/transform/position", JsonName = "position")]
  public class ConvoyPoint
  {
    public GameObject gameObject;

    public Color DefaultColor
    {
      get
      {
        return new Color(0.55f, 0.55f, 0.85f, 0.85f);
      }
    }

    public Color StartColor
    {
      get
      {
        return new Color(0.0f, 1f, 0.25f, 0.85f);
      }
    }

    public Color EndColor
    {
      get
      {
        return new Color(0.75f, 0.5f, 0.0f, 0.85f);
      }
    }

    public ConvoyPoint()
    {
      this.gameObject = GameObject.CreatePrimitive(PrimitiveType.Capsule);
      this.gameObject.name = "ConvoyPoint";
      this.gameObject.AddComponent<EditorRootScript>().EditorObject = (object) this;
      this.gameObject.GetComponent<Renderer>().material.color = this.DefaultColor;
      this.gameObject.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
      this.gameObject.transform.localScale = new Vector3(300f, 300f, 300f);
      Object.Destroy((Object) this.gameObject.GetComponent<Collider>());
    }

    public ConvoyPoint(GameObject obj)
    {
      Material material = new Material(Shader.Find("Transparent/Bumped Diffuse"));
      this.gameObject = obj;
      this.gameObject.GetComponent<Renderer>().material = material;
    }

    public ConvoyPoint Clone()
    {
      return new ConvoyPoint() { gameObject = { transform = { parent = this.gameObject.transform.parent, position = this.gameObject.transform.position } } };
    }
  }
}
