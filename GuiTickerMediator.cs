﻿// Decompiled with JetBrains decompiler
// Type: GuiTickerMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;

public class GuiTickerMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  private readonly List<OnScreenNotificationType> notificationsToDisplay = new List<OnScreenNotificationType>() { OnScreenNotificationType.MiningShipUnderAttack, OnScreenNotificationType.AssigmentCompleted, OnScreenNotificationType.Emergency, OnScreenNotificationType.HeavyFight, OnScreenNotificationType.OutpostAttacked, OnScreenNotificationType.BeaconAttacked, OnScreenNotificationType.SectorFortificationLevel };
  public new const string NAME = "GuiTickerMediator";
  private GuiTicker view;

  public GuiTickerMediator()
    : base("GuiTickerMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.LevelLoaded);
    this.AddMessageInterest(Message.ShowTickerMessage);
    this.AddMessageInterest(Message.ShowOnScreenNotification);
    this.AddMessageInterest(Message.ClearTicker);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    switch (message.Id)
    {
      case Message.ShowOnScreenNotification:
        if (this.view == null)
          break;
        OnScreenNotification screenNotification = (OnScreenNotification) message.Data;
        if (!screenNotification.Show || !this.notificationsToDisplay.Contains(screenNotification.NotificationType))
          break;
        this.view.AddPendingMessage(new GuiTickerMessage(screenNotification.TextMessage, 900f));
        break;
      case Message.ShowTickerMessage:
        this.view.AddPendingMessage((GuiTickerMessage) message.Data);
        break;
      case Message.ClearTicker:
        this.view.Clear();
        break;
      case Message.LevelLoaded:
        this.view = Game.Ticker;
        Game.GUIManager.AddPanel((IGUIPanel) this.view);
        break;
    }
  }
}
