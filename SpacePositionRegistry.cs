﻿// Decompiled with JetBrains decompiler
// Type: SpacePositionRegistry
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class SpacePositionRegistry
{
  private readonly List<SpacePosition> spacePositionCache = new List<SpacePosition>();

  public T CreateSpacePosition<T>() where T : SpacePosition, new()
  {
    T instance = Activator.CreateInstance<T>();
    instance.Root.name = typeof (T).ToString();
    this.spacePositionCache.Add((SpacePosition) instance);
    FacadeFactory.GetInstance().SendMessage(Message.EntityLoaded, (object) instance);
    return instance;
  }

  public void RemoveSpacePosition(SpacePosition p)
  {
    if (this.spacePositionCache.Contains(p))
      this.spacePositionCache.Remove(p);
    else
      Debug.LogError((object) ("Removing non-registered SpacePosition: " + p.Name));
    FacadeFactory.GetInstance().SendMessage(Message.EntityRemoved, (object) p);
    p.Remove();
  }
}
