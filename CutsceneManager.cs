﻿// Decompiled with JetBrains decompiler
// Type: CutsceneManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneManager : MonoBehaviour
{
  private CutsceneSequenceBase currentCutscene;
  private GameObject cutsceneManagerObject;
  private Queue<CutscenePlayRequestData> cutsceneQueue;
  private CutsceneSkipMenu cutsceneSkipMenu;
  private GameObject savedMusicPlayingObject;
  private static CutsceneManager instance;

  public static CutsceneManager Instance
  {
    get
    {
      if ((UnityEngine.Object) CutsceneManager.instance == (UnityEngine.Object) null)
      {
        GameObject gameObject = new GameObject("CutsceneManager");
        CutsceneManager.instance = gameObject.AddComponent<CutsceneManager>();
        CutsceneManager.instance.cutsceneManagerObject = gameObject;
      }
      return CutsceneManager.instance;
    }
  }

  public static bool IsCutscenePlayed
  {
    get
    {
      return (UnityEngine.Object) CutsceneManager.Instance.currentCutscene != (UnityEngine.Object) null;
    }
  }

  public void Awake()
  {
    this.cutsceneQueue = new Queue<CutscenePlayRequestData>();
  }

  public static CutsceneSequenceBase GetCurrentCutscene()
  {
    return CutsceneManager.Instance.currentCutscene;
  }

  public void CutsceneFinishProcedure()
  {
    if (this.cutsceneQueue.Count == 0)
      StoryProtocol.GetProtocol().CutsceneFinished();
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) this.currentCutscene, false);
    this.currentCutscene = (CutsceneSequenceBase) null;
    if (this.cutsceneQueue.Count > 0)
      this.PlayNext();
    else
      this.AllCutscenesFinishedProcedure();
  }

  public void AllCutscenesFinishedProcedure()
  {
    FacadeFactory.GetInstance().SendMessage(Message.CutsceneMuteGameMusic, (object) false);
    this.HideSkipMenu();
  }

  public void EnqueueCutscene(CutscenePlayRequestData cutscenePlayRequestData)
  {
    this.cutsceneQueue.Enqueue(cutscenePlayRequestData);
    this.Play();
  }

  private void Play()
  {
    if (CutsceneManager.IsCutscenePlayed)
      return;
    this.PlayNext();
  }

  private void Skip()
  {
    if (CutsceneManager.IsCutscenePlayed)
    {
      this.currentCutscene.AbortDelayed();
      this.currentCutscene = (CutsceneSequenceBase) null;
    }
    Debug.Log((object) ("Skip(). IsCutscenePlayed:  " + (object) CutsceneManager.IsCutscenePlayed));
    this.Play();
  }

  public void AbortAllCutscenes()
  {
    this.cutsceneQueue.Clear();
    if (!CutsceneManager.IsCutscenePlayed)
      return;
    this.currentCutscene.AbortImmediately();
    this.currentCutscene = (CutsceneSequenceBase) null;
  }

  private void PlayNext()
  {
    foreach (CutsceneSequenceBase component in this.cutsceneManagerObject.GetComponents<CutsceneSequenceBase>())
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) component);
      Debug.LogWarning((object) ("Dirty state - Destroyed Cutscene: " + (object) component));
    }
    if (this.cutsceneQueue.Count == 0)
      return;
    CutscenePlayRequestData playRequestData = this.cutsceneQueue.Dequeue();
    bool flag;
    try
    {
      flag = this.TryCreateCutscene(playRequestData);
    }
    catch (Exception ex)
    {
      flag = false;
      if (this.cutsceneQueue.Count == 0)
        StoryProtocol.GetProtocol().CutsceneFinished();
      Debug.LogError((object) ("Failed to create cutscene. Exception: " + (object) ex));
    }
    if (flag)
    {
      try
      {
        this.currentCutscene = this.cutsceneManagerObject.GetComponent<CutsceneSequenceBase>();
        this.currentCutscene.AddOnFinishFunction(new CutsceneManager.OnFinishCutsceneDelegate(this.CutsceneFinishProcedure));
        this.currentCutscene.Setup(playRequestData);
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("Failed to setup cutscene. Exception: " + (object) ex));
        this.currentCutscene.AbortImmediately();
      }
    }
    else
    {
      Debug.LogWarning((object) "No fitting cutscene found / Cutscene creation error");
      this.Skip();
    }
  }

  private bool TryCreateCutscene(CutscenePlayRequestData playRequestData)
  {
    switch (playRequestData.CutsceneIdToPlay)
    {
      case CutsceneId.Docking:
        byte num = Game.Me.ActualShip.ShipCardLight.Tier;
        if (playRequestData.TargetObject == null)
        {
          Debug.LogWarning((object) "No target provided for docking...");
          break;
        }
        SpaceObject targetObject = playRequestData.TargetObject;
        if (!targetObject.OwnerCard.IsDockable)
        {
          Debug.LogError((object) "Tried to dock on a non-dockable target...");
          return false;
        }
        if (targetObject.PrefabName == "galactica")
        {
          switch (num)
          {
            case 1:
              this.cutsceneManagerObject.AddComponent<CutsceneSequenceDockingGalacticaTier1>();
              return true;
            case 2:
              this.cutsceneManagerObject.AddComponent<CutsceneSequenceDockingGalacticaTier2>();
              return true;
            case 3:
              this.cutsceneManagerObject.AddComponent<CutsceneSequenceDockingGalacticaTier3>();
              return true;
            case 4:
              this.cutsceneManagerObject.AddComponent<CutsceneSequenceDockingGalacticaTier4>();
              return true;
          }
        }
        else if (targetObject.PrefabName == "basestar")
        {
          switch (num)
          {
            case 1:
              this.cutsceneManagerObject.AddComponent<CutsceneSequenceDockingBasestarTier1>();
              return true;
            case 2:
              this.cutsceneManagerObject.AddComponent<CutsceneSequenceDockingBasestarTier2>();
              return true;
            case 3:
              this.cutsceneManagerObject.AddComponent<CutsceneSequenceDockingBasestarTier3>();
              return true;
            case 4:
              this.cutsceneManagerObject.AddComponent<CutsceneSequenceDockingBasestarTier4>();
              return true;
          }
        }
        Debug.LogError((object) ("No fitting docking cutscene for docking target: " + targetObject.PrefabName + " and ship tier: " + (object) num));
        return false;
      case CutsceneId.Tut1IntroColonial:
        this.cutsceneManagerObject.AddComponent<CutsceneSequenceTut1ColonialIntro>();
        return true;
      case CutsceneId.Tut1IntroCylon:
        this.cutsceneManagerObject.AddComponent<CutsceneSequenceTut1CylonIntro>();
        return true;
      case CutsceneId.Tut1DroneActivationColonial:
        this.cutsceneManagerObject.AddComponent<CutsceneSequenceTut1ColonialDroneActivation>();
        return true;
      case CutsceneId.Tut1DroneActivationCylon:
        this.cutsceneManagerObject.AddComponent<CutsceneSequenceTut1CylonDroneActivation>();
        return true;
      case CutsceneId.Tut1ExtroPart1Colonial:
        this.cutsceneManagerObject.AddComponent<CutsceneSequenceGeneric>().SetSequenceData("cutscene_tut1_extro_part1_colonial", "Tut1_Extro_Part1_Colonial_Escape_To_Galactica", true, CutsceneSkipMode.ShowMenu);
        return true;
      case CutsceneId.Tut1ExtroPart1Cylon:
        this.cutsceneManagerObject.AddComponent<CutsceneSequenceGeneric>().SetSequenceData("cutscene_tut1_extro_part1_cylon", "Tut1_Extro_Part1_Cylon_Move_Into_Battle", true, CutsceneSkipMode.ShowMenu);
        return true;
      case CutsceneId.Tut1ExtroPart2AllFactions:
        this.cutsceneManagerObject.AddComponent<CutsceneSequenceGeneric>().SetSequenceData("cutscene_tut1_extro_part2_all_factions", "Tut1_Extro_Part2_All_Factions_Ftl_Trap_Activating", false, CutsceneSkipMode.ShowMenu);
        return true;
      case CutsceneId.Tut1CylonsFindGalactica:
        this.cutsceneManagerObject.AddComponent<CutsceneSequenceGeneric>().SetSequenceData("cutscene_tut1_cylons_find_galactica", "Tut1_Cylons_Find_Galactica", false, CutsceneSkipMode.ShowMenu);
        return true;
    }
    return false;
  }

  public void KeepMusicAlive(CutsceneKeepMusicAlive keepMusicAliveScript)
  {
    UnityEngine.Object.Destroy((UnityEngine.Object) this.savedMusicPlayingObject);
    keepMusicAliveScript.transform.parent = this.transform;
    this.savedMusicPlayingObject = keepMusicAliveScript.gameObject;
  }

  public void HideSkipMenu()
  {
    if (!((UnityEngine.Object) this.cutsceneSkipMenu != (UnityEngine.Object) null))
      return;
    this.cutsceneSkipMenu.Hide();
  }

  public void ToggleSkipMenu()
  {
    if ((UnityEngine.Object) this.cutsceneSkipMenu == (UnityEngine.Object) null)
      this.cutsceneSkipMenu = new GameObject("Cutscene Skip Menu").AddComponent<CutsceneSkipMenu>();
    this.cutsceneSkipMenu.Toggle();
  }

  public delegate void OnFinishCutsceneDelegate();
}
