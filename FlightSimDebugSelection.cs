﻿// Decompiled with JetBrains decompiler
// Type: FlightSimDebugSelection
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Tooltips;
using UnityEngine;

public class FlightSimDebugSelection : GuiPanel, InputListener
{
  private GuiInputTextBox accelerationInput;
  private GuiInputTextBox strafeAccelerationInput;
  private GuiInputTextBox strafeMaxSpeedInput;
  private GuiInputTextBox inertiaCompensationInput;
  private GuiInputTextBox pitchMaxSpeedInput;
  private GuiInputTextBox yawMaxSpeedInput;
  private GuiInputTextBox rollMaxSpeedInput;
  private GuiInputTextBox pitchAccelerationInput;
  private GuiInputTextBox yawAccelerationInput;
  private GuiInputTextBox rollAccelerationInput;
  private GuiInputTextBox speedInput;

  bool InputListener.HandleKeyboardInput
  {
    get
    {
      return false;
    }
  }

  public FlightSimDebugSelection(MovementOptions linkedOptions)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    FlightSimDebugSelection.\u003CFlightSimDebugSelection\u003Ec__AnonStorey112 selectionCAnonStorey112 = new FlightSimDebugSelection.\u003CFlightSimDebugSelection\u003Ec__AnonStorey112();
    // ISSUE: reference to a compiler-generated field
    selectionCAnonStorey112.linkedOptions = linkedOptions;
    // ISSUE: explicit constructor call
    base.\u002Ector();
    // ISSUE: reference to a compiler-generated field
    selectionCAnonStorey112.\u003C\u003Ef__this = this;
    this.Align = Align.MiddleLeft;
    this.Position = new Vector2(70f, -170f);
    this.MouseTransparent = true;
    int num1 = 0;
    // ISSUE: reference to a compiler-generated field
    selectionCAnonStorey112.showMenuCheckbox = new GuiCheckbox("ShowDebugMenu? ")
    {
      IsChecked = false
    };
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    selectionCAnonStorey112.showMenuCheckbox.OnCheckAction = new System.Action<GuiCheckbox>(selectionCAnonStorey112.\u003C\u003Em__288);
    // ISSUE: reference to a compiler-generated field
    this.AddChild((GuiElementBase) selectionCAnonStorey112.showMenuCheckbox, Align.UpCenter, new Vector2(20f, (float) num1));
    int num2 = num1 + 30;
    // ISSUE: reference to a compiler-generated field
    Debug.Log((object) ("LinkedOptions: " + (object) selectionCAnonStorey112.linkedOptions));
    GuiLabel guiLabel1 = new GuiLabel("Acceleration:");
    guiLabel1.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Forward Acceleration");
    this.AddChild((GuiElementBase) guiLabel1, Align.UpLeft, new Vector2(-60f, (float) num2));
    GuiInputTextBox guiInputTextBox1 = new GuiInputTextBox();
    guiInputTextBox1.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox1.Text = selectionCAnonStorey112.linkedOptions.acceleration.ToString();
    this.accelerationInput = guiInputTextBox1;
    this.AddChild((GuiElementBase) this.accelerationInput, Align.UpLeft, new Vector2(70f, (float) (num2 - 5)));
    int num3 = num2 + 20;
    // ISSUE: reference to a compiler-generated method
    this.accelerationInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__289);
    GuiLabel guiLabel2 = new GuiLabel("StrafeAcceleration:");
    guiLabel2.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Strafe Acceleration");
    this.AddChild((GuiElementBase) guiLabel2, Align.UpLeft, new Vector2(-60f, (float) num3));
    GuiInputTextBox guiInputTextBox2 = new GuiInputTextBox();
    guiInputTextBox2.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox2.Text = selectionCAnonStorey112.linkedOptions.strafeAcceleration.ToString();
    this.strafeAccelerationInput = guiInputTextBox2;
    this.AddChild((GuiElementBase) this.strafeAccelerationInput, Align.UpLeft, new Vector2(70f, (float) (num3 - 5)));
    int num4 = num3 + 20;
    // ISSUE: reference to a compiler-generated method
    this.strafeAccelerationInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__28A);
    GuiLabel guiLabel3 = new GuiLabel("Strafe Max Speed:");
    guiLabel3.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Strafe Max Speed");
    this.AddChild((GuiElementBase) guiLabel3, Align.UpLeft, new Vector2(-60f, (float) num4));
    GuiInputTextBox guiInputTextBox3 = new GuiInputTextBox();
    guiInputTextBox3.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox3.Text = selectionCAnonStorey112.linkedOptions.strafeMaxSpeed.ToString();
    this.strafeMaxSpeedInput = guiInputTextBox3;
    this.AddChild((GuiElementBase) this.strafeMaxSpeedInput, Align.UpLeft, new Vector2(70f, (float) (num4 - 5)));
    int num5 = num4 + 20;
    // ISSUE: reference to a compiler-generated method
    this.strafeMaxSpeedInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__28B);
    GuiLabel guiLabel4 = new GuiLabel("Inertia Compensation:");
    guiLabel4.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Inertia Compensation");
    this.AddChild((GuiElementBase) guiLabel4, Align.UpLeft, new Vector2(-60f, (float) num5));
    GuiInputTextBox guiInputTextBox4 = new GuiInputTextBox();
    guiInputTextBox4.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox4.Text = selectionCAnonStorey112.linkedOptions.inertiaCompensation.ToString();
    this.inertiaCompensationInput = guiInputTextBox4;
    this.AddChild((GuiElementBase) this.inertiaCompensationInput, Align.UpLeft, new Vector2(70f, (float) (num5 - 5)));
    int num6 = num5 + 20;
    // ISSUE: reference to a compiler-generated method
    this.inertiaCompensationInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__28C);
    GuiLabel guiLabel5 = new GuiLabel("Pitch Max Speed:");
    guiLabel5.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Inertia Compensation");
    this.AddChild((GuiElementBase) guiLabel5, Align.UpLeft, new Vector2(-60f, (float) num6));
    GuiInputTextBox guiInputTextBox5 = new GuiInputTextBox();
    guiInputTextBox5.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox5.Text = selectionCAnonStorey112.linkedOptions.pitchMaxSpeed.ToString();
    this.pitchMaxSpeedInput = guiInputTextBox5;
    this.AddChild((GuiElementBase) this.pitchMaxSpeedInput, Align.UpLeft, new Vector2(70f, (float) (num6 - 5)));
    int num7 = num6 + 20;
    // ISSUE: reference to a compiler-generated method
    this.pitchMaxSpeedInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__28D);
    GuiLabel guiLabel6 = new GuiLabel("Yaw Max Speed:");
    guiLabel6.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Yaw Max Speed");
    this.AddChild((GuiElementBase) guiLabel6, Align.UpLeft, new Vector2(-60f, (float) num7));
    GuiInputTextBox guiInputTextBox6 = new GuiInputTextBox();
    guiInputTextBox6.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox6.Text = selectionCAnonStorey112.linkedOptions.yawMaxSpeed.ToString();
    this.yawMaxSpeedInput = guiInputTextBox6;
    this.AddChild((GuiElementBase) this.yawMaxSpeedInput, Align.UpLeft, new Vector2(70f, (float) (num7 - 5)));
    int num8 = num7 + 20;
    // ISSUE: reference to a compiler-generated method
    this.yawMaxSpeedInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__28E);
    GuiLabel guiLabel7 = new GuiLabel("Roll Max Speed:");
    guiLabel7.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Roll Max Speed");
    this.AddChild((GuiElementBase) guiLabel7, Align.UpLeft, new Vector2(-60f, (float) num8));
    GuiInputTextBox guiInputTextBox7 = new GuiInputTextBox();
    guiInputTextBox7.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox7.Text = selectionCAnonStorey112.linkedOptions.rollMaxSpeed.ToString();
    this.rollMaxSpeedInput = guiInputTextBox7;
    this.AddChild((GuiElementBase) this.rollMaxSpeedInput, Align.UpLeft, new Vector2(70f, (float) (num8 - 5)));
    int num9 = num8 + 20;
    // ISSUE: reference to a compiler-generated method
    this.rollMaxSpeedInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__28F);
    GuiLabel guiLabel8 = new GuiLabel("Pitch Acceleration:");
    guiLabel8.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Pitch Acceleration");
    this.AddChild((GuiElementBase) guiLabel8, Align.UpLeft, new Vector2(-60f, (float) num9));
    GuiInputTextBox guiInputTextBox8 = new GuiInputTextBox();
    guiInputTextBox8.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox8.Text = selectionCAnonStorey112.linkedOptions.pitchAcceleration.ToString();
    this.pitchAccelerationInput = guiInputTextBox8;
    this.AddChild((GuiElementBase) this.pitchAccelerationInput, Align.UpLeft, new Vector2(70f, (float) (num9 - 5)));
    int num10 = num9 + 20;
    // ISSUE: reference to a compiler-generated method
    this.pitchAccelerationInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__290);
    GuiLabel guiLabel9 = new GuiLabel("Yaw Acceleration:");
    guiLabel9.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Yaw Acceleration");
    this.AddChild((GuiElementBase) guiLabel9, Align.UpLeft, new Vector2(-60f, (float) num10));
    GuiInputTextBox guiInputTextBox9 = new GuiInputTextBox();
    guiInputTextBox9.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox9.Text = selectionCAnonStorey112.linkedOptions.yawAcceleration.ToString();
    this.yawAccelerationInput = guiInputTextBox9;
    this.AddChild((GuiElementBase) this.yawAccelerationInput, Align.UpLeft, new Vector2(70f, (float) (num10 - 5)));
    int num11 = num10 + 20;
    // ISSUE: reference to a compiler-generated method
    this.yawAccelerationInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__291);
    GuiLabel guiLabel10 = new GuiLabel("Roll Acceleration:");
    guiLabel10.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Yaw Acceleration");
    this.AddChild((GuiElementBase) guiLabel10, Align.UpLeft, new Vector2(-60f, (float) num11));
    GuiInputTextBox guiInputTextBox10 = new GuiInputTextBox();
    guiInputTextBox10.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox10.Text = selectionCAnonStorey112.linkedOptions.rollAcceleration.ToString();
    this.rollAccelerationInput = guiInputTextBox10;
    this.AddChild((GuiElementBase) this.rollAccelerationInput, Align.UpLeft, new Vector2(70f, (float) (num11 - 5)));
    int num12 = num11 + 20;
    // ISSUE: reference to a compiler-generated method
    this.rollAccelerationInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__292);
    int num13 = num12 + 20;
    GuiLabel guiLabel11 = new GuiLabel("SPEED :");
    guiLabel11.ToolTip = (GuiAdvancedTooltipBase) new GuiAdvancedLabelTooltip("Forward Speed");
    this.AddChild((GuiElementBase) guiLabel11, Align.UpLeft, new Vector2(-60f, (float) num13));
    GuiInputTextBox guiInputTextBox11 = new GuiInputTextBox();
    guiInputTextBox11.Size = new Vector2(60f, 20f);
    // ISSUE: reference to a compiler-generated field
    guiInputTextBox11.Text = selectionCAnonStorey112.linkedOptions.speed.ToString();
    this.speedInput = guiInputTextBox11;
    this.AddChild((GuiElementBase) this.speedInput, Align.UpLeft, new Vector2(70f, (float) (num13 - 5)));
    int num14 = num13 + 20;
    // ISSUE: reference to a compiler-generated method
    this.speedInput.OnTextChanged = new System.Action<string>(selectionCAnonStorey112.\u003C\u003Em__293);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    selectionCAnonStorey112.showMenuCheckbox.OnCheckAction(selectionCAnonStorey112.showMenuCheckbox);
    // ISSUE: reference to a compiler-generated field
    selectionCAnonStorey112.showMenuCheckbox.IsRendered = true;
  }

  public override void Draw()
  {
    base.Draw();
  }
}
