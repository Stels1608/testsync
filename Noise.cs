﻿// Decompiled with JetBrains decompiler
// Type: Noise
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class Noise
{
  public static float Random(int a, int b, int c, int seed)
  {
    int num1 = seed + a * 57 + b * (int) sbyte.MaxValue + c * 307;
    int num2 = num1 << 13 ^ num1;
    return (float) (1.0 - (double) (num2 * (num2 * num2 * 15731 + 789221) + 1376312589 & int.MaxValue) / 1073741824.0);
  }

  public static float Random(int a, int b, int seed)
  {
    return Noise.Random(a, b, 0, seed);
  }

  public static float Random(int a, int seed)
  {
    return Noise.Random(a, 0, 0, seed);
  }

  public static float Random(int seed)
  {
    return Noise.Random(0, 0, 0, seed);
  }

  public static int ToRange(float randomValue, int from, int to)
  {
    float num = (float) (((double) randomValue + 1.0) / 2.0);
    return Mathf.Clamp((int) ((double) from + (double) (to + 1 - from) * (double) num), from, to);
  }
}
