﻿// Decompiled with JetBrains decompiler
// Type: HelpPopup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HelpPopup : HelpPopupUi
{
  [SerializeField]
  private TextMeshProUGUI headerText;
  [SerializeField]
  private TextMeshProUGUI okButtonText;
  [SerializeField]
  private TextMeshProUGUI messageText;
  [SerializeField]
  private Toggle doNotShowAgainToggle;
  [SerializeField]
  private TextMeshProUGUI doNotShowAgainLabel;
  private AnonymousDelegate onOkay;

  protected override void Awake()
  {
    base.Awake();
    this.doNotShowAgainToggle.onValueChanged.AddListener((UnityAction<bool>) (check => FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new ChangeSettingMessage(UserSetting.ShowPopups, (object) !check, true))));
  }

  protected override void SetTimerText(string text)
  {
  }

  public override void ShowTimer(bool show)
  {
  }

  protected override void SetCancelButtonText(string text)
  {
  }

  protected override void SetMainText(string mainText)
  {
    this.doNotShowAgainLabel.text = BsgoLocalization.Get("bgo.tip_popup.hide_popup");
    this.messageText.text = mainText;
  }

  protected override void SetTitleText(string text)
  {
    this.headerText.text = text;
  }

  protected override void SetOkButtonText(string text)
  {
    this.okButtonText.text = BsgoLocalization.Get("bgo.tip_popup.okay");
  }

  public void SetOkayDelegate(AnonymousDelegate onOkay)
  {
    this.onOkay = onOkay;
    this.AddOnClosedAction(new OnMessageBoxClose(this.OnHelpPopupClosed));
  }

  private void OnHelpPopupClosed(MessageBoxActionType result)
  {
    if (this.onOkay == null)
      return;
    this.onOkay();
  }
}
