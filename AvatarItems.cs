﻿// Decompiled with JetBrains decompiler
// Type: AvatarItems
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AvatarItems : IProtocolWrite, IProtocolRead
{
  private readonly Dictionary<AvatarItem, string> items = new Dictionary<AvatarItem, string>();

  public bool IsEmpty
  {
    get
    {
      return this.items.Count == 0;
    }
  }

  public void SetItem(AvatarItem item, string value)
  {
    this.items[item] = value;
  }

  public string GetItem(AvatarItem item)
  {
    string str;
    if (this.items.TryGetValue(item, out str))
      return str;
    Debug.Log((object) ("item " + (object) item + " not set."));
    return string.Empty;
  }

  public virtual void Read(BgoProtocolReader r)
  {
    int num = r.ReadLength();
    for (int index = 0; index < num; ++index)
      this.items[(AvatarItem) r.ReadByte()] = r.ReadString();
  }

  public virtual void Write(BgoProtocolWriter w)
  {
    w.Write((ushort) this.items.Count);
    using (Dictionary<AvatarItem, string>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<AvatarItem, string> current = enumerator.Current;
        w.Write((byte) current.Key);
        w.Write(current.Value);
      }
    }
  }

  public override string ToString()
  {
    string str = string.Empty;
    using (Dictionary<AvatarItem, string>.Enumerator enumerator = this.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<AvatarItem, string> current = enumerator.Current;
        str = str + (object) current.Key + ": " + current.Value + "\n";
      }
    }
    return str;
  }
}
