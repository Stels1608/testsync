﻿// Decompiled with JetBrains decompiler
// Type: GUIShopInventorySlot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GUIShopInventorySlot : GUIPanel
{
  private GUIImageNew backgroundImage;
  private GUILabelNew slotNumLabel;
  private GUIImageNew itemImage;
  private GUIImageNew uniqueIconImage;
  private GUIImageNew genericIcon;
  private GUILabelNew titleLabel;
  private GUILabelNew shortDescLabel;
  private GUILabelNew subtitleLabel;
  private GUIButtonNew upperButton;
  private GUIButtonNew buysellButton;
  private GUIButtonNew detailsButton;
  private GUILabelNew costLabel;
  private GUIImageNew costImage1;
  private GUIImageNew costImage2;
  private GUILabelNew costLabel1;
  private GUILabelNew costLabel2;
  private GUIImageNew saleBanner;
  private GUILabelNew percentOffLabel;
  private float2 titlePos;
  private float2 shortDescPos;
  private float titleWidth;
  private float shortDescWidth;
  private AtlasCache atlasCache;
  private Texture2D starterKitGenericIcon;
  private Texture2D BuySaleBtnNormalTex;
  private Texture2D BuySaleBtnOverTex;
  private Texture2D BuySaleBtnPressedTex;
  private Texture2D BuySaleBtnOnSaleNormalTex;
  private Texture2D BuySaleBtnOnSaleOverTex;
  private Texture2D BuySaleBtnOnSalePressedTex;
  private bool isSelected;
  private int slotNumber;

  public bool IsSelected
  {
    get
    {
      return this.isSelected;
    }
    set
    {
      if (this.isSelected == value)
        return;
      this.isSelected = value;
      this.backgroundImage.IsRendered = value;
    }
  }

  public int SlotNumber
  {
    get
    {
      return this.slotNumber;
    }
    set
    {
      this.slotNumber = value;
    }
  }

  public AnonymousDelegate HandlerButton
  {
    set
    {
      this.buysellButton.Handler = value;
    }
  }

  public AnonymousDelegate HandlerDetailsButton
  {
    set
    {
      this.detailsButton.Handler = value;
    }
  }

  public GUIButtonNew UpperButton
  {
    get
    {
      return this.upperButton;
    }
  }

  public GUIShopInventorySlot(GUIShopInventorySlot.InventoryPosition invPos, SmartRect parent)
    : base(parent)
  {
    this.IsRendered = true;
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/EquipBuyPanel/gui_slot_layout");
    this.backgroundImage = (jwindowDescription["background_image"] as JImage).CreateGUIImageNew(this.root);
    this.backgroundImage.IsRendered = false;
    this.root.Width = this.backgroundImage.Rect.width;
    this.root.Height = this.backgroundImage.Rect.height;
    this.backgroundImage.Texture = ResourceLoader.Load<Texture2D>("GUI/Common/inv_selected_middle");
    this.AddPanel((GUIPanel) this.backgroundImage);
    this.titleLabel = (jwindowDescription["title_label"] as JLabel).CreateGUILabelNew(this.root);
    this.titlePos = this.titleLabel.Position;
    this.titleWidth = this.titleLabel.Width;
    this.titleLabel.WordWrap = true;
    this.AddPanel((GUIPanel) this.titleLabel);
    this.shortDescLabel = (jwindowDescription["short_desc_label"] as JLabel).CreateGUILabelNew(this.root);
    this.shortDescPos = this.shortDescLabel.Position;
    this.shortDescWidth = this.shortDescLabel.Width;
    this.shortDescLabel.WordWrap = true;
    this.AddPanel((GUIPanel) this.shortDescLabel);
    this.subtitleLabel = (jwindowDescription["subtitle_label"] as JLabel).CreateGUILabelNew(this.root);
    this.subtitleLabel.WordWrap = true;
    this.AddPanel((GUIPanel) this.subtitleLabel);
    this.costLabel = (jwindowDescription["cost_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.costLabel);
    this.costImage1 = (jwindowDescription["cubits_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.costImage1);
    this.costImage2 = (jwindowDescription["tylium_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.costImage2);
    this.costLabel1 = (jwindowDescription["cubits_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.costLabel1);
    this.costLabel2 = (jwindowDescription["tylium_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.costLabel2);
    this.itemImage = (jwindowDescription["item_image"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.itemImage);
    this.slotNumLabel = (jwindowDescription["slot_num_label"] as JLabel).CreateGUILabelNew(this.root);
    this.AddPanel((GUIPanel) this.slotNumLabel);
    this.buysellButton = (jwindowDescription["buysell_button"] as JButton).CreateGUIButtonNew(this.root);
    this.buysellButton.IsRendered = false;
    this.AddPanel((GUIPanel) this.buysellButton);
    this.upperButton = (jwindowDescription["buysell_button"] as JButton).CreateGUIButtonNew(this.root);
    this.upperButton.IsRendered = false;
    this.upperButton.PositionY -= 36f;
    this.upperButton.TextLabel.AutoSize = true;
    this.AddPanel((GUIPanel) this.upperButton);
    this.detailsButton = (jwindowDescription["details_button"] as JButton).CreateGUIButtonNew(this.root);
    this.detailsButton.IsRendered = false;
    this.AddPanel((GUIPanel) this.detailsButton);
    this.uniqueIconImage = (jwindowDescription["ShipTexture"] as JImage).CreateGUIImageNew(this.root);
    this.AddPanel((GUIPanel) this.uniqueIconImage);
    this.genericIcon = (jwindowDescription["ShipIcon"] as JImage).CreateGUIImageNew(this.root);
    this.starterKitGenericIcon = this.genericIcon.Texture;
    this.genericIcon.TakeTextureSize();
    this.AddPanel((GUIPanel) this.genericIcon);
    this.saleBanner = (jwindowDescription["SaleBanner"] as JImage).CreateGUIImageNew(this.root);
    this.saleBanner.IsRendered = false;
    this.AddPanel((GUIPanel) this.saleBanner);
    this.percentOffLabel = (jwindowDescription["percentOff"] as JLabel).CreateGUILabelNew(this.root);
    this.percentOffLabel.Font = Gui.Options.fontEurostileTRegCon;
    this.percentOffLabel.NormalColor = Color.black;
    this.percentOffLabel.Style.fontSize = 14;
    this.percentOffLabel.IsRendered = false;
    this.percentOffLabel.AutoSize = true;
    this.AddPanel((GUIPanel) this.percentOffLabel);
    this.atlasCache = new AtlasCache(new float2(40f, 35f));
    this.BuySaleBtnNormalTex = this.buysellButton.NormalTexture;
    this.BuySaleBtnOverTex = this.buysellButton.OverTexture;
    this.BuySaleBtnPressedTex = this.buysellButton.PressedTexture;
    this.BuySaleBtnOnSaleNormalTex = (Texture2D) ResourceLoader.Load("GUI/Common/Buttons/buysell_onsale_normal");
    this.BuySaleBtnOnSaleOverTex = (Texture2D) ResourceLoader.Load("GUI/Common/Buttons/buysell_onsale_over");
    this.BuySaleBtnOnSalePressedTex = (Texture2D) ResourceLoader.Load("GUI/Common/Buttons/buysell_onsale_pressed");
  }

  public void UpdateData(ShipItem item, ShopInventoryContainer container)
  {
    this.upperButton.IsRendered = false;
    bool slotHasItem = item != null && (bool) item.IsLoaded;
    this.UpdateBuySellDetailButtons(item, container, slotHasItem);
    this.UpdateItemImageAndDesc(item, container, slotHasItem);
    this.UpdatePriceData(item, container, slotHasItem);
    this.RecalculateAbsCoords();
  }

  private void UpdateItemImageAndDesc(ShipItem item, ShopInventoryContainer container, bool slotHasItem)
  {
    if (!slotHasItem)
    {
      AtlasEntry emptyItem = this.atlasCache.EmptyItem;
      this.itemImage.Texture = emptyItem.Texture;
      this.itemImage.InnerRect = emptyItem.FrameRect;
      this.itemImage.IsRendered = true;
      this.uniqueIconImage.IsRendered = false;
      this.genericIcon.IsRendered = false;
      this.slotNumLabel.IsRendered = true;
      this.slotNumLabel.Text = this.slotNumber.ToString();
      this.titleLabel.IsRendered = false;
      this.shortDescLabel.IsRendered = false;
      this.subtitleLabel.IsRendered = false;
    }
    else
    {
      bool flag1 = item is ShipSystem && (item as ShipSystem).Card.SlotType == ShipSlotType.ship_paint;
      bool flag2 = item is StarterKit;
      AtlasEntry cachedEntryBy = this.atlasCache.GetCachedEntryBy(item.ItemGUICard.GUIAtlasTexturePath, (int) item.ItemGUICard.FrameIndex);
      if (flag2)
      {
        this.uniqueIconImage.Texture = ((StarterKit) item).Card.icon;
        this.uniqueIconImage.TakeTextureSize();
        this.uniqueIconImage.InnerRect = new Rect(0.0f, 0.0f, 1f, 1f);
        this.genericIcon.Texture = this.starterKitGenericIcon;
        this.genericIcon.InnerRect = new Rect(0.0f, 0.0f, 1f, 1f);
        this.genericIcon.TakeTextureSize();
      }
      else
      {
        this.itemImage.Texture = !flag1 ? cachedEntryBy.Texture : item.ItemGUICard.GUIIconTexture;
        this.itemImage.InnerRect = !flag1 ? cachedEntryBy.FrameRect : new Rect(0.0f, 0.0f, 1f, 1f);
        this.itemImage.Height = !flag1 ? 35f : 40f;
      }
      this.uniqueIconImage.IsRendered = flag2;
      this.genericIcon.IsRendered = flag2;
      this.itemImage.IsRendered = !flag2;
      this.slotNumLabel.IsRendered = false;
      this.titleLabel.Position = !flag2 ? this.titlePos : this.titlePos + new float2(this.uniqueIconImage.Width / 2f, 0.0f);
      this.titleLabel.Width = !flag2 ? this.titleWidth : this.titleWidth - this.uniqueIconImage.Width;
      this.titleLabel.Text = item.ItemGUICard.Name;
      this.titleLabel.IsRendered = true;
      this.shortDescLabel.Position = !flag2 ? this.shortDescPos : this.shortDescPos + new float2(this.uniqueIconImage.Width / 2f, 0.0f);
      this.shortDescLabel.Width = !flag2 ? this.shortDescWidth : this.shortDescWidth - this.uniqueIconImage.Width;
      if (flag2)
        this.shortDescLabel.Width = this.shortDescWidth - this.uniqueIconImage.Width;
      else if (this.upperButton.IsRendered)
      {
        this.shortDescLabel.Width = this.shortDescWidth - this.upperButton.Width;
        this.shortDescLabel.Position = this.shortDescLabel.Position - new float2(this.upperButton.Width / 2f, 0.0f);
      }
      else
        this.shortDescLabel.Width = this.shortDescWidth;
      this.shortDescLabel.Text = item.ItemGUICard.ShortDescription;
      if (!string.IsNullOrEmpty(this.shortDescLabel.Text) && this.shortDescLabel.Text.Length > 60)
        this.shortDescLabel.Text = this.shortDescLabel.Text.Substring(0, 60) + "...";
      this.shortDescLabel.IsRendered = true;
      if (item is ItemCountable && container != ShopInventoryContainer.Store)
        this.subtitleLabel.Text = "%$bgo.shop.count% " + (object) (item as ItemCountable).Count;
      else if (item is ShipSystem && !flag1)
        this.subtitleLabel.Text = "%$bgo.shop.level%" + (object) (item as ShipSystem).Card.Level + "   %$bgo.shop.max_level%" + (object) (item as ShipSystem).Card.MaxLevel;
      else
        this.subtitleLabel.Text = string.Empty;
      this.subtitleLabel.IsRendered = !flag2;
    }
  }

  private void UpdateBuySellDetailButtons(ShipItem item, ShopInventoryContainer container, bool slotHasItem)
  {
    if (item is StarterKit || !slotHasItem)
    {
      this.detailsButton.IsRendered = false;
      this.buysellButton.IsRendered = false;
    }
    else
    {
      if (container == ShopInventoryContainer.Hold)
      {
        bool flag = item is ItemCountable && (item as ItemCountable).Card.IsAugment;
        this.buysellButton.TextLabel.Text = "%$bgo.shop.salvage%";
        this.upperButton.IsRendered = flag;
        if (flag)
          this.upperButton.TextLabel.Text = (item as ItemCountable).Card.Action != AugmentActionType.LootItem ? "%$bgo.shop.activate%" : "%$bgo.inflight_shop.button_augment_item%";
        this.buysellButton.IsRendered = item.ShopItemCard.CanBeSold && (!(item is ItemCountable) || (item as ItemCountable).Count > 0U) && (int) item.ItemGUICard.Level < 8;
      }
      else if (container == ShopInventoryContainer.Store)
      {
        this.buysellButton.TextLabel.Text = "%$bgo.shop.requisition%";
        this.buysellButton.IsRendered = true;
      }
      else if (container == ShopInventoryContainer.Locker)
      {
        this.buysellButton.TextLabel.Text = "%$bgo.shop.to_hold%";
        this.buysellButton.IsRendered = true;
      }
      this.detailsButton.IsRendered = true;
      if (container == ShopInventoryContainer.Store)
      {
        this.detailsButton.TextLabel.Text = "%$bgo.shop.inv_detailsbutton%";
        this.DetailsBtnIsSaleBtn(false);
      }
      else
        this.SetDetailsButtonText(item);
      this.detailsButton.TextLabel.MakeCenteredRect();
      this.buysellButton.TextLabel.MakeCenteredRect();
    }
  }

  private void SetDetailsButtonText(ShipItem item)
  {
    if (item is ShipSystem)
    {
      ShipSystemCard shipSystemCard = (item as ShipSystem).Card;
      if (shipSystemCard.UserUpgradeable && (int) shipSystemCard.Level < (int) shipSystemCard.MaxLevel)
      {
        this.detailsButton.TextLabel.Text = "%$bgo.shop.inv_upgradebutton%";
        this.DetailsBtnIsSaleBtn((item as ShipSystem).IsAnyUpgradeLevelOnSale());
        return;
      }
    }
    this.detailsButton.TextLabel.Text = "%$bgo.shop.inv_detailsbutton%";
    this.DetailsBtnIsSaleBtn(false);
  }

  private void DetailsBtnIsSaleBtn(bool paintBtnOrange)
  {
    if (paintBtnOrange)
    {
      this.detailsButton.NormalTexture = this.BuySaleBtnOnSaleNormalTex;
      this.detailsButton.OverTexture = this.BuySaleBtnOnSaleOverTex;
      this.detailsButton.PressedTexture = this.BuySaleBtnOnSalePressedTex;
    }
    else
    {
      this.detailsButton.NormalTexture = this.BuySaleBtnNormalTex;
      this.detailsButton.OverTexture = this.BuySaleBtnOverTex;
      this.detailsButton.PressedTexture = this.BuySaleBtnPressedTex;
    }
  }

  private void UpdatePriceData(ShipItem item, ShopInventoryContainer container, bool slotHasItem)
  {
    this.costImage1.IsRendered = false;
    this.costLabel1.IsRendered = false;
    this.costImage2.IsRendered = false;
    this.costLabel2.IsRendered = false;
    this.saleBanner.IsRendered = false;
    this.percentOffLabel.IsRendered = false;
    if (slotHasItem)
    {
      Price price1 = container != ShopInventoryContainer.Store ? item.ShopItemCard.SellPrice : item.ShopItemCard.BuyPrice;
      int num1 = 0;
      bool flag = true;
      GUIPanel panel = (GUIPanel) this.costLabel;
      int num2 = 3;
      using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = price1.items.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
          float price2 = current.Value;
          if (container == ShopInventoryContainer.Store)
          {
            ShopDiscount itemDiscount = Shop.FindItemDiscount(item.CardGUID);
            if (itemDiscount != null)
            {
              this.saleBanner.IsRendered = true;
              this.percentOffLabel.IsRendered = true;
              price2 = itemDiscount.ApplyDiscount(price2);
              this.percentOffLabel.Text = "-" + (object) itemDiscount.Percentage + "%";
            }
          }
          if ((double) price2 > 0.0)
          {
            if (num1 == 0)
            {
              this.costImage1.Texture = ResourceLoader.Load<Texture2D>(current.Key.GUICard.GUIIcon);
              this.costImage1.Size = new float2((float) this.costImage1.Texture.width, (float) this.costImage1.Texture.height);
              this.costImage1.IsRendered = true;
              this.costLabel1.Text = price2.ToString("#0.0");
              this.costLabel1.IsRendered = true;
              this.costLabel1.StyleColor = !this.saleBanner.IsRendered ? this.costLabel1.NormalColor : this.costLabel1.OverColor;
              this.costLabel1.MakeCenteredRect();
              this.costImage1.PlaceRightOf(panel, (float) num2);
              this.costLabel1.PlaceRightOf((GUIPanel) this.costImage1, (float) num2);
              panel = (GUIPanel) this.costLabel1;
            }
            else
            {
              this.costImage2.Texture = ResourceLoader.Load<Texture2D>(current.Key.GUICard.GUIIcon);
              this.costImage2.Size = new float2((float) this.costImage2.Texture.width, (float) this.costImage2.Texture.height);
              this.costImage2.IsRendered = true;
              this.costLabel2.Text = price2.ToString("#0.0");
              this.costLabel2.IsRendered = true;
              this.costLabel2.StyleColor = !this.saleBanner.IsRendered ? this.costLabel2.NormalColor : this.costLabel2.OverColor;
              this.costLabel2.MakeCenteredRect();
              this.costImage2.PlaceRightOf(panel, (float) num2);
              this.costLabel2.PlaceRightOf((GUIPanel) this.costImage2, (float) num2);
              panel = (GUIPanel) this.costLabel2;
            }
          }
          if ((double) current.Value > 0.0)
            flag = false;
          ++num1;
        }
      }
      this.costLabel.IsRendered = true;
      if (!flag)
        return;
      this.costLabel1.Text = "%$bgo.EquipBuyPanel.gui_slot_layout.for_free%";
      this.costLabel1.IsRendered = false;
      this.costLabel.IsRendered = false;
      this.costLabel1.PlaceRightOf((GUIPanel) this.costLabel, 3f);
    }
    else
      this.costLabel.IsRendered = false;
  }

  public enum InventoryPosition
  {
    First,
    Middle,
    Last,
  }
}
