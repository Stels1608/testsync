﻿// Decompiled with JetBrains decompiler
// Type: CutsceneLetterboxRenderer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

internal class CutsceneLetterboxRenderer : MonoBehaviour
{
  private Color drawColor = new Color(0.0f, 0.0f, 0.0f, 1f);
  private const float BAR_HEIGHT = 0.1f;
  private float fadeProgress;
  private Texture2D blackTex;

  public void Awake()
  {
    this.blackTex = ImageUtils.CreateTexture(Vector2i.one, this.drawColor);
  }

  private void OnGUI()
  {
    this.drawColor.a = this.fadeProgress;
    float num = (float) Screen.height;
    float width = (float) Screen.width;
    Color color = GUI.color;
    int depth = GUI.depth;
    GUI.depth = 10;
    GUI.color = this.drawColor;
    GUI.DrawTexture(new Rect(0.0f, 0.0f, width, num * 0.1f * this.fadeProgress), (Texture) this.blackTex);
    GUI.DrawTexture(new Rect(0.0f, (float) (1.0 - 0.100000001490116 * (double) this.fadeProgress) * num, width, num * 0.1f), (Texture) this.blackTex);
    GUI.color = color;
    GUI.depth = depth;
  }

  public void FadeIn(float fadeInTime)
  {
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 0, (object) "to", (object) 1f, (object) "time", (object) fadeInTime, (object) "onupdate", (object) "UpdateFade"));
  }

  public void FadeOut()
  {
    iTween.ValueTo(this.gameObject, iTween.Hash((object) "from", (object) 1f, (object) "to", (object) 0.0f, (object) "time", (object) 1f, (object) "onupdate", (object) "UpdateFade"));
  }

  private void UpdateFade(float newAlpha)
  {
    this.fadeProgress = newAlpha;
  }
}
