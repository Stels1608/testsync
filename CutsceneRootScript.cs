﻿// Decompiled with JetBrains decompiler
// Type: CutsceneRootScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections;
using System.Diagnostics;
using Unity5AssetHandling;
using UnityEngine;

public class CutsceneRootScript : RootScriptBase
{
  private bool useHighRes = true;
  public AnonymousDelegate LowResModelLoadedCallback = (AnonymousDelegate) (() => {});
  public Flag IsAnyModelLoaded = new Flag();
  public AnonymousDelegate ModelWasSetCallback = (AnonymousDelegate) (() => {});
  private string lowResAssetName;
  private string hiResAssetName;
  private GameObject model;

  public static bool ForceLowRes { get; set; }

  public override GameObject Model
  {
    get
    {
      return this.model;
    }
  }

  public void Construct(string prefabName)
  {
    this.StartCoroutine(this.DelayableConstruct(prefabName));
  }

  [DebuggerHidden]
  private IEnumerator DelayableConstruct(string prefabName)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CutsceneRootScript.\u003CDelayableConstruct\u003Ec__Iterator13() { prefabName = prefabName, \u003C\u0024\u003EprefabName = prefabName, \u003C\u003Ef__this = this };
  }

  [DebuggerHidden]
  private IEnumerator WaitForAsset(AssetRequest highResAsset, AssetRequest lowResAsset)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new CutsceneRootScript.\u003CWaitForAsset\u003Ec__Iterator14() { highResAsset = highResAsset, lowResAsset = lowResAsset, \u003C\u0024\u003EhighResAsset = highResAsset, \u003C\u0024\u003ElowResAsset = lowResAsset, \u003C\u003Ef__this = this };
  }

  private void ShowUglyPlaceholder(GameObject lowResAsset)
  {
    if ((Object) lowResAsset != (Object) null)
      this.SetModel(lowResAsset);
    else
      this.SetModel((GameObject) Resources.Load("placeholder/placeholder_ship"));
  }

  private void DestroyModel()
  {
    if ((Object) this.model != (Object) null)
      Object.DestroyImmediate((Object) this.model);
    this.model = (GameObject) null;
  }

  public virtual void SetModel(GameObject prototype)
  {
    this.DestroyModel();
    if ((Object) prototype != (Object) null)
    {
      this.model = (GameObject) Object.Instantiate((Object) prototype, Vector3.zero, Quaternion.identity);
      this.model.name = prototype.name;
      this.model.transform.parent = this.transform;
      this.model.transform.localPosition = Vector3.zero;
      this.model.transform.localRotation = Quaternion.identity;
      this.model.transform.localScale = Vector3.one;
    }
    BgoUtils.CleanupModelForCutscene(this.model);
    BgoUtils.ApplyDefaultSkin(this.model, true);
    CutsceneShipRemoteControl componentInParent = this.transform.GetComponentInParent<CutsceneShipRemoteControl>();
    if ((Object) componentInParent != (Object) null)
      componentInParent.OnModelChange();
    this.ModelWasSetCallback();
    this.IsAnyModelLoaded.Set();
  }
}
