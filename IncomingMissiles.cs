﻿// Decompiled with JetBrains decompiler
// Type: IncomingMissiles
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class IncomingMissiles
{
  private readonly List<uint> inner;

  public bool IsEmpty
  {
    get
    {
      return this.inner.Count == 0;
    }
  }

  public int Count
  {
    get
    {
      return this.inner.Count;
    }
  }

  public event MissileDetectedEventHandler MissileDetected;

  public event MissileLostEventHandler MissileLost;

  public IncomingMissiles()
  {
    this.inner = new List<uint>();
  }

  public void Add(Missile missile)
  {
    this.inner.Add(missile.ObjectID);
    this.OnMissileDetected(missile);
  }

  public void Remove(uint missileID)
  {
    int index = this.inner.IndexOf(missileID);
    if (index <= -1)
      return;
    this.inner.Remove(missileID);
    this.OnMissileLost(index);
  }

  public bool HasNuclearMissiles()
  {
    for (int innerIndex = 0; innerIndex < this.inner.Count; ++innerIndex)
    {
      if (this.GetMissile(innerIndex) != null && this.GetMissile(innerIndex).MissileCard.IsNuke)
        return true;
    }
    return false;
  }

  public Missile GetMissile(int innerIndex)
  {
    if (innerIndex >= 0 && innerIndex < this.inner.Count)
      return (Missile) SpaceLevel.GetLevel().GetObjectRegistry().Get(this.inner[innerIndex]);
    return (Missile) null;
  }

  protected virtual void OnMissileDetected(Missile missile)
  {
    if (this.MissileDetected == null)
      return;
    this.MissileDetected(missile);
  }

  protected virtual void OnMissileLost(int index)
  {
    if (this.MissileLost == null)
      return;
    this.MissileLost(index);
  }
}
