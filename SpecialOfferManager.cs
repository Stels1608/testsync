﻿// Decompiled with JetBrains decompiler
// Type: SpecialOfferManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class SpecialOfferManager
{
  public SpecialOfferManager.HappyHourDelegate OnHappyHourChanged;
  public SpecialOfferManager.SpecialOfferDelegate OnSpecialOfferChanged;
  private RewardCard _specialOfferCardCard;
  private double specialOfferBonusEndTime;

  public RewardCard SpecialOfferCard
  {
    get
    {
      return this._specialOfferCardCard;
    }
    set
    {
      this._specialOfferCardCard = value;
    }
  }

  public bool IsSpecialOfferDialogShownThisLaunch { get; set; }

  public bool ShowSpecialOfferDialog
  {
    get
    {
      if (this._specialOfferCardCard != null)
        return !this.IsSpecialOfferDialogShownThisLaunch;
      return false;
    }
  }

  public double SpecialOfferBonusEndTime
  {
    get
    {
      return this.specialOfferBonusEndTime;
    }
    set
    {
      this.specialOfferBonusEndTime = 0.001 * value;
    }
  }

  public SpecialOfferManager()
  {
    this.IsSpecialOfferDialogShownThisLaunch = false;
  }

  public bool IsSpecialOffer()
  {
    return this._specialOfferCardCard != null;
  }

  public bool IsHappyHourActive()
  {
    return this.SpecialOfferBonusEndTime - Game.TimeSync.ServerTime > 0.0;
  }

  public void ProceedSpecialOffer()
  {
    FacadeFactory.GetInstance().SendMessage(Message.SetFullscreen, (object) false);
    ExternalApi.EvalJs("window.createSpecialPaymentSession(\"" + this.SpecialOfferCard.Package + "\");");
  }

  public bool Check()
  {
    if (!this.ShowSpecialOfferDialog)
      return false;
    FacadeFactory.GetInstance().SendMessage(Message.ShowSpecialOffer);
    this.IsSpecialOfferDialogShownThisLaunch = true;
    return true;
  }

  public void UpdateHappyHour(bool happyHourActive)
  {
    if (this.OnHappyHourChanged != null)
      this.OnHappyHourChanged(happyHourActive);
    if (happyHourActive)
      return;
    Game.Ticker.Clear();
  }

  public void UpdateSpecialOffer(bool specialOfferActive)
  {
    if (this.OnSpecialOfferChanged == null)
      return;
    this.OnSpecialOfferChanged(specialOfferActive);
  }

  public delegate void HappyHourDelegate(bool activeFlag);

  public delegate void SpecialOfferDelegate(bool activeFlag);
}
