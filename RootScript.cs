﻿// Decompiled with JetBrains decompiler
// Type: RootScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using Unity5AssetHandling;
using UnityEngine;

public class RootScript : RootScriptBase
{
  private bool useHighRes = true;
  public RootScript.DestroyedCallback Destroyed;
  public SpaceObject SpaceObject;
  private string lowResAssetName;
  private string hiResAssetName;

  public static bool ForceLowRes { get; set; }

  public override GameObject Model
  {
    get
    {
      return this.SpaceObject.Model;
    }
  }

  public void Construct()
  {
    if (string.IsNullOrEmpty(this.SpaceObject.PrefabName))
      return;
    this.lowResAssetName = this.SpaceObject.PrefabName + "_lowres.prefab";
    this.hiResAssetName = this.SpaceObject.PrefabName + ".prefab";
    AssetRequest lowResAsset = AssetCatalogue.Instance.Request(this.lowResAssetName, false);
    AssetRequest highResAsset = (AssetRequest) null;
    if (this.useHighRes = !RootScript.ForceLowRes || this.SpaceObject.IsMe)
      highResAsset = AssetCatalogue.Instance.Request(this.hiResAssetName, this.SpaceObject.IsMe);
    this.StartCoroutine(this.WaitForAsset(highResAsset, lowResAsset));
  }

  [DebuggerHidden]
  private IEnumerator WaitForAsset(AssetRequest highResAsset, AssetRequest lowResAsset)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new RootScript.\u003CWaitForAsset\u003Ec__Iterator3E()
    {
      highResAsset = highResAsset,
      lowResAsset = lowResAsset,
      \u003C\u0024\u003EhighResAsset = highResAsset,
      \u003C\u0024\u003ElowResAsset = lowResAsset,
      \u003C\u003Ef__this = this
    };
  }

  private void ShowUglyPlaceholder(GameObject lowResAsset)
  {
    if ((Object) lowResAsset != (Object) null)
    {
      this.SpaceObject.SetLowResModel(lowResAsset);
    }
    else
    {
      this.SpaceObject.SetLowResModel((GameObject) Resources.Load("placeholder/placeholder_ship"));
      float scale = 1f;
      if (this.hiResAssetName.Contains("t2"))
        scale = 1.5f;
      else if (this.hiResAssetName.Contains("t3"))
        scale = 2f;
      Color color = TargetColorsLegacyBrackets.neutralColor;
      if (this.hiResAssetName.Contains("cylon"))
        color = TargetColorsLegacyBrackets.cylonColor;
      else if (this.hiResAssetName.Contains("human"))
        color = TargetColorsLegacyBrackets.colonialColor;
      this.SpaceObject.SetModelParams(scale, color);
    }
  }

  private void OnDestroy()
  {
    if (this.Destroyed == null)
      return;
    this.Destroyed();
  }

  public delegate void DestroyedCallback();
}
