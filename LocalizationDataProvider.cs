﻿// Decompiled with JetBrains decompiler
// Type: LocalizationDataProvider
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Model;
using Gui;

public class LocalizationDataProvider : DataProvider<Message>
{
  public BsgoLocalization LocalizationVO { get; set; }

  public LocalizationDataProvider(string name)
    : base(name)
  {
    this.LocalizationVO = new BsgoLocalization();
  }
}
