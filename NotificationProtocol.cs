﻿// Decompiled with JetBrains decompiler
// Type: NotificationProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class NotificationProtocol : BgoProtocol
{
  public NotificationProtocol()
    : base(BgoProtocol.ProtocolID.Notification)
  {
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    NotificationProtocol.Reply reply = (NotificationProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    switch (reply)
    {
      case NotificationProtocol.Reply.Message:
        DebugMessage.Add(br.ReadString());
        break;
      case NotificationProtocol.Reply.MiningShipUnderAttack:
        if (!((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null) && SpaceLevel.GetLevel().IsStory)
          break;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyE9 messageCAnonStoreyE9 = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyE9();
        uint cardGUID1 = br.ReadGUID();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE9.damageType = br.ReadByte();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE9.isYour = br.ReadBoolean();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyE9.gui = (GUICard) Game.Catalogue.FetchCard(cardGUID1, CardView.GUI);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyE9.gui.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyE9.\u003C\u003Em__237));
        break;
      case NotificationProtocol.Reply.OutpostAttacked:
        if (!((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null) && SpaceLevel.GetLevel().IsStory)
          break;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyEA messageCAnonStoreyEa = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyEA();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEa.faction = (Faction) br.ReadByte();
        uint cardGUID2 = br.ReadGUID();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEa.damageType = br.ReadByte();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEa.sectorCard = (GUICard) Game.Catalogue.FetchCard(cardGUID2, CardView.GUI);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyEa.sectorCard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyEa.\u003C\u003Em__238));
        break;
      case NotificationProtocol.Reply.HeavyFight:
        if (!((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null) && SpaceLevel.GetLevel().IsStory)
          break;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyEC messageCAnonStoreyEc = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyEC();
        uint cardGUID3 = br.ReadGUID();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEc.gui = (GUICard) Game.Catalogue.FetchCard(cardGUID3, CardView.GUI);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyEc.gui.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyEc.\u003C\u003Em__23A));
        break;
      case NotificationProtocol.Reply.Experience:
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new ExperienceNotification(br.ReadByte(), br.ReadInt32()));
        break;
      case NotificationProtocol.Reply.DutyUpdated:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyED messageCAnonStoreyEd = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyED();
        ushort id1 = br.ReadUInt16();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEd.duty = Game.Me.DutyBook.GetByID(id1);
        // ISSUE: reference to a compiler-generated field
        if (messageCAnonStoreyEd.duty == null)
          break;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyEd.duty.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyEd.\u003C\u003Em__23B));
        break;
      case NotificationProtocol.Reply.OreMined:
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new OreMinedNotification(ItemFactory.ReadItem(br) as ItemCountable));
        break;
      case NotificationProtocol.Reply.Reward:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyEE messageCAnonStoreyEe = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyEE();
        uint cardGUID4 = br.ReadGUID();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEe.guiCard = (GUICard) Game.Catalogue.FetchCard(cardGUID4, CardView.GUI);
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEe.items = ItemFactory.ReadList<ShipItem>(br);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEe.guiCard.IsLoaded.Depend<ShipItem>((IEnumerable<ShipItem>) messageCAnonStoreyEe.items);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyEe.guiCard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyEe.\u003C\u003Em__23C));
        break;
      case NotificationProtocol.Reply.MissionCompleted:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyEF messageCAnonStoreyEf = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyEF();
        ushort id2 = br.ReadUInt16();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEf.mission = Game.Me.MissionBook.GetByID(id2);
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEf.mission.Status = Mission.State.Completed;
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyEf.mission.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyEf.\u003C\u003Em__23D));
        break;
      case NotificationProtocol.Reply.DailyLoginBonus:
        NotificationManager.ShowDailyLoginBonus(br.ReadUInt32(), br.ReadUInt32List());
        break;
      case NotificationProtocol.Reply.SystemUpgradeResult:
        NotificationManager.ShowSystemUpgradeResult(br.ReadBoolean());
        break;
      case NotificationProtocol.Reply.EmergencyMessage:
        string message1 = "%$bgo." + br.ReadString() + ".description%";
        int num1 = (int) br.ReadUInt16();
        float time = br.ReadSingle();
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new EmergencyNotification(message1, time));
        break;
      case NotificationProtocol.Reply.AugmentItem:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF0 messageCAnonStoreyF0 = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF0();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF0.items = ItemFactory.ReadList<ShipItem>(br);
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF0.loadedItemCount = new int[1];
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF0.itemCount = messageCAnonStoreyF0.items.Count;
        // ISSUE: reference to a compiler-generated field
        using (List<ShipItem>.Enumerator enumerator = messageCAnonStoreyF0.items.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            // ISSUE: reference to a compiler-generated method
            enumerator.Current.ItemGUICard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyF0.\u003C\u003Em__23E));
          }
          break;
        }
      case NotificationProtocol.Reply.DeathPaymentBonus:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF1 messageCAnonStoreyF1 = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF1();
        uint cardGUID5 = br.ReadUInt32();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF1.discountAmount = br.ReadUInt32();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF1.guiCard = (GUICard) Game.Catalogue.FetchCard(cardGUID5, CardView.GUI);
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF1.bonusCard = (RewardCard) Game.Catalogue.FetchCard(cardGUID5, CardView.Reward);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyF1.bonusCard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyF1.\u003C\u003Em__23F));
        break;
      case NotificationProtocol.Reply.JumpBeaconAttacked:
        if (!((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null) && SpaceLevel.GetLevel().IsStory)
          break;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF2 messageCAnonStoreyF2 = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF2();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF2.faction = (Faction) br.ReadByte();
        uint cardGUID6 = br.ReadGUID();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF2.damageType = br.ReadByte();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF2.sectorCard = (GUICard) Game.Catalogue.FetchCard(cardGUID6, CardView.GUI);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyF2.sectorCard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyF2.\u003C\u003Em__240));
        break;
      case NotificationProtocol.Reply.MonthlyShipSale:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF3 messageCAnonStoreyF3 = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF3();
        uint cardGUID7 = br.ReadGUID();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF3.shipSaleCard = (ShipSaleCard) Game.Catalogue.FetchCard(cardGUID7, CardView.ShipSale);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyF3.shipSaleCard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyF3.\u003C\u003Em__241));
        break;
      case NotificationProtocol.Reply.SectorEventReward:
        FacadeFactory.GetInstance().SendMessage(Message.SectorEventReward, (object) new SectorEventReward(br.ReadUInt32(), br.ReadGUID(), (SectorEventRanking) br.ReadByte(), ItemFactory.ReadList<ShipItem>(br)));
        break;
      case NotificationProtocol.Reply.SectorEventState:
        FacadeFactory.GetInstance().SendMessage(Message.SectorEventStateChanged, (object) new SectorEventStateUpdate(br.ReadUInt32(), (Faction) br.ReadByte(), (SectorEventState) br.ReadByte(), br.ReadVector3(), br.ReadSingle()));
        break;
      case NotificationProtocol.Reply.SectorEventTasks:
        FacadeFactory.GetInstance().SendMessage((IMessage<Message>) new SectorEventTaskMessage(br.ReadUInt32(), br.ReadEventTaskList()));
        break;
      case NotificationProtocol.Reply.FtlMissionsOff:
        FacadeFactory.GetInstance().SendMessage(Message.FtlMissionsOff);
        break;
      case NotificationProtocol.Reply.ErrorMessage:
        FacadeFactory.GetInstance().SendMessage(Message.NotificationServerError, (object) (NotificationErrorMessageType) br.ReadUInt32());
        break;
      case NotificationProtocol.Reply.SectorFortificationLevel:
        if (!((UnityEngine.Object) SpaceLevel.GetLevel() == (UnityEngine.Object) null) && SpaceLevel.GetLevel().IsStory)
          break;
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyEB messageCAnonStoreyEb = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyEB();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEb.faction = (Faction) br.ReadByte();
        uint cardGUID8 = br.ReadGUID();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEb.level = br.ReadByte();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEb.changeType = (SectorFortificationChangeType) br.ReadByte();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyEb.sectorCard = (GUICard) Game.Catalogue.FetchCard(cardGUID8, CardView.GUI);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyEb.sectorCard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyEb.\u003C\u003Em__239));
        break;
      case NotificationProtocol.Reply.LootMessage:
        int num2 = (int) br.ReadUInt16();
        for (int index1 = 0; index1 < num2; ++index1)
        {
          // ISSUE: object of a compiler-generated type is created
          // ISSUE: variable of a compiler-generated type
          NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF4 messageCAnonStoreyF4 = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF4();
          // ISSUE: reference to a compiler-generated field
          messageCAnonStoreyF4.item = ItemFactory.ReadItem(br);
          int num3 = (int) br.ReadUInt16();
          // ISSUE: reference to a compiler-generated field
          messageCAnonStoreyF4.bonusAmounts = (Dictionary<LootBonusType, float>) null;
          if (num3 > 0)
          {
            // ISSUE: reference to a compiler-generated field
            messageCAnonStoreyF4.bonusAmounts = new Dictionary<LootBonusType, float>();
          }
          for (int index2 = 0; index2 < num3; ++index2)
          {
            LootBonusType key = (LootBonusType) br.ReadUInt16();
            // ISSUE: reference to a compiler-generated field
            messageCAnonStoreyF4.bonusAmounts.Add(key, (float) br.ReadUInt32());
          }
          // ISSUE: reference to a compiler-generated field
          // ISSUE: reference to a compiler-generated method
          messageCAnonStoreyF4.item.ItemGUICard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyF4.\u003C\u003Em__242));
        }
        int num4 = (int) br.ReadUInt16();
        for (int index = 0; index < num4; ++index)
          FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new SpecialActionNotification((SpecialAction) br.ReadUInt16()));
        if (!br.ReadBoolean())
          break;
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification("bgo.loot_message.full_hold", NotificationCategory.Negative));
        break;
      case NotificationProtocol.Reply.InboxMailLimit:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF5 messageCAnonStoreyF5 = new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF5();
        uint cardGUID9 = br.ReadGUID();
        // ISSUE: reference to a compiler-generated field
        messageCAnonStoreyF5.mailTemplateCard = (MailTemplateCard) Game.Catalogue.FetchCard(cardGUID9, CardView.MailTemplate);
        // ISSUE: reference to a compiler-generated field
        // ISSUE: reference to a compiler-generated method
        messageCAnonStoreyF5.mailTemplateCard.IsLoaded.AddHandler(new SignalHandler(messageCAnonStoreyF5.\u003C\u003Em__243));
        break;
      case NotificationProtocol.Reply.JumpNotification:
        JumpErrorSeverity jumpErrorSeverity = (JumpErrorSeverity) br.ReadByte();
        JumpErrorReason jumpErrorReason = (JumpErrorReason) br.ReadByte();
        string message2 = BsgoLocalization.Get("bgo.jump_notification." + jumpErrorReason.ToString().ToLowerInvariant());
        NotificationCategory category = jumpErrorSeverity != JumpErrorSeverity.Error ? NotificationCategory.Neutral : NotificationCategory.Negative;
        FacadeFactory.GetInstance().SendMessage(Message.ShowOnScreenNotification, (object) new PlainNotification(message2, category));
        if (jumpErrorSeverity != JumpErrorSeverity.Info)
        {
          JumpActionsHandler.JumpFailed();
          FacadeFactory.GetInstance().SendMessage(Message.FtlJumpCanceled);
        }
        if (jumpErrorReason == JumpErrorReason.Substitute)
          FacadeFactory.GetInstance().SendMessage(Message.ChatSystemNegative, (object) message2);
        Debug.Log((object) ("[JumpNotification] Severity: " + (object) jumpErrorSeverity + " Reason: " + (object) jumpErrorReason));
        if (jumpErrorReason != JumpErrorReason.Queued)
          break;
        FacadeFactory.GetInstance().SendMessage(Message.FtlJumpQueued);
        break;
      case NotificationProtocol.Reply.ConversionCampaignOffer:
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: reference to a compiler-generated method
        FacadeFactory.GetInstance().SendMessage(Message.ExecuteWhenIngame, (object) new IngameLevelStartedCommand(new System.Action(new NotificationProtocol.\u003CParseMessage\u003Ec__AnonStoreyF6()
        {
          campaignGuid = br.ReadGUID(),
          endTime = br.ReadDateTime()
        }.\u003C\u003Em__244)));
        break;
      default:
        DebugUtility.LogError("Unknown MessageType in Notification protocol: " + (object) reply);
        break;
    }
  }

  public enum Reply : ushort
  {
    Message = 1,
    MiningShipUnderAttack = 2,
    OutpostAttacked = 5,
    HeavyFight = 6,
    Experience = 7,
    DutyUpdated = 8,
    OreMined = 9,
    Reward = 10,
    MissionCompleted = 11,
    DailyLoginBonus = 12,
    SystemUpgradeResult = 13,
    EmergencyMessage = 14,
    AugmentItem = 15,
    DeathPaymentBonus = 16,
    JumpBeaconAttacked = 19,
    MonthlyShipSale = 20,
    SectorEventReward = 21,
    SectorEventState = 22,
    SectorEventTasks = 23,
    FtlMissionsOff = 24,
    ErrorMessage = 25,
    SectorFortificationLevel = 26,
    LootMessage = 27,
    InboxMailLimit = 28,
    JumpNotification = 29,
    ConversionCampaignOffer = 30,
  }
}
