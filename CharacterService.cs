﻿// Decompiled with JetBrains decompiler
// Type: CharacterService
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class CharacterService
{
  public CharacterServiceId Id { get; private set; }

  public long Cooldown { get; private set; }

  public long LastUse { get; private set; }

  public uint RemainingTimeInSeconds
  {
    get
    {
      return (uint) ((double) (this.LastUse + this.Cooldown) - Game.TimeSync.ServerTime);
    }
  }

  public int CubitsPrice { get; private set; }

  public uint CooldownInDays
  {
    get
    {
      return CharacterService.ConvertSecondsToDays((double) this.Cooldown);
    }
  }

  public bool Eligible { get; private set; }

  public bool CooldownExpired
  {
    get
    {
      return (double) (this.LastUse + this.Cooldown) < Game.TimeSync.ServerTime;
    }
  }

  public bool CanAfford
  {
    get
    {
      return (long) Game.Me.Hold.Cubits >= (long) this.CubitsPrice;
    }
  }

  public CharacterService(CharacterServiceId id, long cooldown, long lastUse, int cubitsPrice, bool eligible)
  {
    this.Id = id;
    this.Cooldown = cooldown;
    this.LastUse = lastUse;
    this.CubitsPrice = cubitsPrice;
    this.Eligible = eligible;
  }

  private static uint ConvertSecondsToDays(double seconds)
  {
    return (uint) TimeSpan.FromSeconds(seconds).TotalDays;
  }
}
