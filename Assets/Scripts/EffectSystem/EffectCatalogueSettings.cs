﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.EffectCatalogueSettings
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Assets.Scripts.EffectSystem
{
  [XmlRoot("settings")]
  [Serializable]
  public class EffectCatalogueSettings
  {
    private Dictionary<string, EffectGroupConfig> groups;

    [XmlArray("groups")]
    [XmlArrayItem("group")]
    public List<EffectGroupConfig> GroupList { get; set; }

    private Dictionary<string, EffectGroupConfig> Groups
    {
      get
      {
        if (this.groups == null)
        {
          this.groups = new Dictionary<string, EffectGroupConfig>();
          using (List<EffectGroupConfig>.Enumerator enumerator = this.GroupList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              EffectGroupConfig current = enumerator.Current;
              this.groups[current.Name] = current;
            }
          }
        }
        return this.groups;
      }
    }

    public EffectCatalogueSettings()
    {
      this.GroupList = new List<EffectGroupConfig>();
    }

    public EffectGroupConfig AddGroup(string name)
    {
      this.RemoveGroup(name);
      EffectGroupConfig effectGroupConfig = new EffectGroupConfig();
      effectGroupConfig.Name = name;
      this.GroupList.Add(effectGroupConfig);
      this.Groups[name] = effectGroupConfig;
      return effectGroupConfig;
    }

    public EffectGroupConfig GetGroup(string name)
    {
      EffectGroupConfig effectGroupConfig = (EffectGroupConfig) null;
      if (this.Groups.ContainsKey(name))
        effectGroupConfig = this.Groups[name];
      return effectGroupConfig;
    }

    public List<EffectGroupConfig> GetAllGroups()
    {
      return new List<EffectGroupConfig>((IEnumerable<EffectGroupConfig>) this.GroupList);
    }

    public void RemoveGroup(string name)
    {
      if (!this.Groups.ContainsKey(name))
        return;
      this.GroupList.Remove(this.Groups[name]);
      this.Groups.Remove(name);
    }

    public bool HasConfigForGroup(string name)
    {
      return this.Groups.ContainsKey(name);
    }
  }
}
