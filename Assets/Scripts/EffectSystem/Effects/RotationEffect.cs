﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.Effects.RotationEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.EffectSystem.Effects
{
  public class RotationEffect : IEffect
  {
    public Quaternion rotation = new Quaternion(1.1f, 1.1f, 1.1f, 1.1f);
    public float duration = 0.2f;
    public UITweener.Method method = UITweener.Method.EaseInOut;
    private EffectConfig config;

    public EffectConfig Config
    {
      get
      {
        return this.config;
      }
      set
      {
        this.config = value;
        if (this.Config.HasParameter("rotation"))
          this.rotation = Quaternion.Euler(this.config.GetParameterValue<float>("rotation_x"), this.config.GetParameterValue<float>("rotation_y"), this.config.GetParameterValue<float>("rotation_z"));
        if (this.Config.HasParameter("duration"))
          this.duration = this.Config.GetParameterValue<float>("duration");
        if (!this.Config.HasParameter("tweenMethod"))
          return;
        this.method = (UITweener.Method) Enum.Parse(typeof (UITweener.Method), this.Config.GetParameterValue<string>("tweenMethod"));
      }
    }

    public void DoEffect(List<GameObject> targetObjects)
    {
      using (List<GameObject>.Enumerator enumerator = targetObjects.GetEnumerator())
      {
        while (enumerator.MoveNext())
          TweenRotation.Begin(enumerator.Current, this.duration, this.rotation).method = this.method;
      }
    }
  }
}
