﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.Effects.SwapSpriteEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.EffectSystem.Effects
{
  public class SwapSpriteEffect : IEffect
  {
    private EffectConfig config;
    public string swapSprite;

    public EffectConfig Config
    {
      get
      {
        return this.config;
      }
      set
      {
        this.config = value;
        if (!this.Config.HasParameter("spriteName"))
          return;
        this.swapSprite = this.Config.GetParameterValue<string>("spriteName");
      }
    }

    public void DoEffect(List<GameObject> targetObjects)
    {
      using (List<GameObject>.Enumerator enumerator = targetObjects.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          UISprite component = enumerator.Current.GetComponent<UISprite>();
          if ((Object) null != (Object) component)
          {
            component.spriteName = this.swapSprite;
            component.MakePixelPerfect();
          }
        }
      }
    }
  }
}
