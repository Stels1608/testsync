﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.Effects.PlayAnimationEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using AnimationOrTween;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.EffectSystem.Effects
{
  public class PlayAnimationEffect : IEffect
  {
    public AnimationOrTween.Direction playDirection = AnimationOrTween.Direction.Forward;
    private EffectConfig config;
    public Animation animation;
    public string clipName;
    public AnimationClip animationClip;
    public EnableCondition ifDisabledOnPlay;
    public DisableCondition disableWhenFinished;

    public EffectConfig Config
    {
      get
      {
        return this.config;
      }
      set
      {
        this.config = value;
        if (this.Config.HasParameter("clipName"))
          this.clipName = this.Config.GetParameterValue<string>("clipName");
        if (this.Config.HasParameter("playDirection"))
          this.playDirection = (AnimationOrTween.Direction) Enum.Parse(typeof (AnimationOrTween.Direction), this.Config.GetParameterValue<string>("playDirection"));
        if (this.Config.HasParameter("ifDisabledOnPlay"))
          this.ifDisabledOnPlay = (EnableCondition) Enum.Parse(typeof (EnableCondition), this.Config.GetParameterValue<string>("ifDisabledOnPlay"));
        if (!this.Config.HasParameter("disableWhenFinished"))
          return;
        this.disableWhenFinished = (DisableCondition) Enum.Parse(typeof (DisableCondition), this.Config.GetParameterValue<string>("disableWhenFinished"));
      }
    }

    public void DoEffect(List<GameObject> targetObjects)
    {
      using (List<GameObject>.Enumerator enumerator = targetObjects.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          GameObject current = enumerator.Current;
          this.animation = current.GetComponent<Animation>() ?? current.AddComponent<Animation>();
          this.animation.AddClip(this.animationClip, this.clipName);
          ActiveAnimation.Play(this.animation, this.clipName, this.playDirection, this.ifDisabledOnPlay, this.disableWhenFinished);
        }
      }
    }
  }
}
