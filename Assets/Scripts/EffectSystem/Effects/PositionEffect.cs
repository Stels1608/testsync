﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.Effects.PositionEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.EffectSystem.Effects
{
  public class PositionEffect : IEffect
  {
    public Vector3 position = new Vector3(1.1f, 1.1f, 1.1f);
    public float duration = 0.2f;
    public UITweener.Method method = UITweener.Method.EaseInOut;
    private EffectConfig config;

    public EffectConfig Config
    {
      get
      {
        return this.config;
      }
      set
      {
        this.config = value;
        if (this.Config.HasParameter("position_x") && this.Config.HasParameter("position_y") && this.Config.HasParameter("position_z"))
          this.position = new Vector3(this.config.GetParameterValue<float>("position_x"), this.config.GetParameterValue<float>("position_y"), this.config.GetParameterValue<float>("position_z"));
        if (this.Config.HasParameter("duration"))
          this.duration = this.Config.GetParameterValue<float>("duration");
        if (!this.Config.HasParameter("tweenMethod"))
          return;
        this.method = (UITweener.Method) Enum.Parse(typeof (UITweener.Method), this.Config.GetParameterValue<string>("tweenMethod"));
      }
    }

    public void DoEffect(List<GameObject> targetObjects)
    {
      using (List<GameObject>.Enumerator enumerator = targetObjects.GetEnumerator())
      {
        while (enumerator.MoveNext())
          TweenPosition.Begin(enumerator.Current, this.duration, this.position).method = this.method;
      }
    }
  }
}
