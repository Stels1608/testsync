﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.Effects.ChangeColorEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.EffectSystem.Effects
{
  public class ChangeColorEffect : IEffect
  {
    public float duration = 0.2f;
    public Color applyColor = new Color(0.6f, 1f, 0.2f, 1f);
    public UITweener.Method method = UITweener.Method.EaseInOut;
    private EffectConfig config;

    public EffectConfig Config
    {
      get
      {
        return this.config;
      }
      set
      {
        this.config = value;
        if (this.Config.HasParameter("duration"))
          this.duration = this.Config.GetParameterValue<float>("duration");
        if (this.Config.HasParameter("color_a") && this.Config.HasParameter("color_r") && (this.Config.HasParameter("color_g") && this.Config.HasParameter("color_b")))
          this.applyColor = new Color(this.Config.GetParameterValue<float>("color_r"), this.Config.GetParameterValue<float>("color_g"), this.Config.GetParameterValue<float>("color_b"), this.Config.GetParameterValue<float>("color_a"));
        if (!this.Config.HasParameter("tweenMethod"))
          return;
        this.method = (UITweener.Method) Enum.Parse(typeof (UITweener.Method), this.Config.GetParameterValue<string>("tweenMethod"));
      }
    }

    public void DoEffect(List<GameObject> targetObjects)
    {
      using (List<GameObject>.Enumerator enumerator = targetObjects.GetEnumerator())
      {
        while (enumerator.MoveNext())
          TweenColor.Begin(enumerator.Current, this.duration, this.applyColor).method = this.method;
      }
    }
  }
}
