﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.Effects.AlphaEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.EffectSystem.Effects
{
  public class AlphaEffect : IEffect
  {
    public float alpha = 1f;
    public float duration = 0.2f;
    public UITweener.Method method = UITweener.Method.EaseInOut;
    private EffectConfig config;

    public EffectConfig Config
    {
      get
      {
        return this.config;
      }
      set
      {
        this.config = value;
        if (this.Config.HasParameter("duration"))
          this.duration = this.Config.GetParameterValue<float>("duration");
        if (this.Config.HasParameter("alpha"))
          this.alpha = this.Config.GetParameterValue<float>("alpha");
        if (!this.Config.HasParameter("tweenMethod"))
          return;
        this.method = (UITweener.Method) Enum.Parse(typeof (UITweener.Method), this.Config.GetParameterValue<string>("tweenMethod"));
      }
    }

    public void DoEffect(List<GameObject> targetObjects)
    {
      using (List<GameObject>.Enumerator enumerator = targetObjects.GetEnumerator())
      {
        while (enumerator.MoveNext())
          TweenAlpha.Begin(enumerator.Current, this.duration, this.alpha).method = this.method;
      }
    }
  }
}
