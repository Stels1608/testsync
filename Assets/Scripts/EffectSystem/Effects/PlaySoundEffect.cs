﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.Effects.PlaySoundEffect
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.EffectSystem.Effects
{
  public class PlaySoundEffect : IEffect
  {
    public float volume = 1f;
    public float pitch = 1f;
    private EffectConfig config;
    public AudioClip audioClip;
    public string audioClipName;

    public EffectConfig Config
    {
      get
      {
        return this.config;
      }
      set
      {
        this.config = value;
        if (this.Config.HasParameter("audioClip"))
          this.audioClipName = this.config.GetParameterValue<string>("audioClip");
        if (this.Config.HasParameter("volume"))
          this.volume = this.Config.GetParameterValue<float>("volume");
        if (!this.Config.HasParameter("pitch"))
          return;
        this.pitch = this.Config.GetParameterValue<float>("pitch");
      }
    }

    public void DoEffect(List<GameObject> targetObjects)
    {
      if (!((Object) null == (Object) this.audioClip))
        ;
      if (!((Object) null != (Object) this.audioClip))
        return;
      NGUITools.PlaySound(this.audioClip, this.volume, this.pitch);
    }
  }
}
