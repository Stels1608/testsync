﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.EffectConfig
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Assets.Scripts.EffectSystem.Effects;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Assets.Scripts.EffectSystem
{
  [XmlRoot("effect")]
  [Serializable]
  public class EffectConfig
  {
    protected Dictionary<string, EffectConfigParameter> parameters;

    [XmlAttribute("type")]
    public string EffectTypeName
    {
      get
      {
        return this.EffectType.ToString();
      }
      set
      {
        this.EffectType = Type.GetType(value);
      }
    }

    [XmlIgnore]
    public Type EffectType { get; set; }

    [XmlAttribute("eventId")]
    public string EventId { get; set; }

    [XmlAttribute("name")]
    public string Name { get; set; }

    [XmlArrayItem("param")]
    [XmlArray("params")]
    public List<EffectConfigParameter> ParameterList { get; set; }

    protected Dictionary<string, EffectConfigParameter> Parameters
    {
      get
      {
        if (this.parameters == null)
        {
          this.parameters = new Dictionary<string, EffectConfigParameter>();
          using (List<EffectConfigParameter>.Enumerator enumerator = this.ParameterList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              EffectConfigParameter current = enumerator.Current;
              this.parameters[current.Key] = current;
            }
          }
        }
        return this.parameters;
      }
    }

    public EffectConfig()
    {
      this.ParameterList = new List<EffectConfigParameter>();
    }

    public EffectConfigParameter AddParameter(string key, object value)
    {
      this.RemoveParameter(key);
      EffectConfigParameter effectConfigParameter = new EffectConfigParameter();
      effectConfigParameter.Key = key;
      effectConfigParameter.Value = value;
      effectConfigParameter.ParamType = value.GetType();
      this.Parameters[key] = effectConfigParameter;
      this.ParameterList.Add(effectConfigParameter);
      return effectConfigParameter;
    }

    public EffectConfigParameter GetParameter(string key)
    {
      EffectConfigParameter effectConfigParameter = (EffectConfigParameter) null;
      if (this.Parameters.ContainsKey(key))
        effectConfigParameter = this.Parameters[key];
      return effectConfigParameter;
    }

    public void SetParameterValue<TValueType>(string key, TValueType value)
    {
      if (!this.HasParameter(key))
        return;
      EffectConfigParameter effectConfigParameter = this.Parameters[key];
      effectConfigParameter.Value = (object) value;
      effectConfigParameter.ParamType = value.GetType();
    }

    public TValueType GetParameterValue<TValueType>(string key)
    {
      TValueType valueType = default (TValueType);
      if (this.HasParameter(key))
        valueType = this.Parameters[key].GetValue<TValueType>();
      return valueType;
    }

    public EffectConfigParameter RemoveParameter(string key)
    {
      EffectConfigParameter effectConfigParameter = (EffectConfigParameter) null;
      if (this.HasParameter(key))
      {
        effectConfigParameter = this.Parameters[key];
        this.Parameters.Remove(key);
        this.ParameterList.Remove(effectConfigParameter);
      }
      return effectConfigParameter;
    }

    public bool HasParameter(string key)
    {
      return this.Parameters.ContainsKey(key);
    }

    public IEffect CreateEffectInstance()
    {
      object instance = Activator.CreateInstance(this.EffectType);
      if (!(instance is IEffect))
        throw new Exception("Effect-object does not implement IEffect!");
      IEffect effect = instance as IEffect;
      effect.Config = this;
      return effect;
    }
  }
}
