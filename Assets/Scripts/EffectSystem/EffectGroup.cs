﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.EffectGroup
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Assets.Scripts.EffectSystem.Effects;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.EffectSystem
{
  public class EffectGroup
  {
    public Dictionary<string, List<IEffect>> effects;

    public string Name { get; set; }

    public EffectGroup()
    {
      this.effects = new Dictionary<string, List<IEffect>>();
    }

    public bool CheckValidEffectGroupName(List<string> allNames, string groupName)
    {
      bool flag = true;
      using (List<string>.Enumerator enumerator = allNames.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          if (string.Empty == groupName || string.CompareOrdinal(current, groupName) == 0)
          {
            flag = false;
            break;
          }
        }
      }
      return flag;
    }

    public bool EventTypeExists(string eventType)
    {
      return this.effects.ContainsKey(eventType);
    }

    public void RegisterEventType(string eventType)
    {
      if (this.EventTypeExists(eventType))
        return;
      this.effects.Add(eventType, new List<IEffect>());
    }

    public void AddEffect(string eventType, IEffect effect)
    {
      if (!this.EventTypeExists(eventType) || effect == null)
        return;
      this.effects[eventType].Add(effect);
    }

    public void RemoveEffect(string eventType, IEffect effect)
    {
      if (!this.EventTypeExists(eventType) || effect == null)
        return;
      this.effects[eventType].Remove(effect);
    }

    public void ExecuteEffectsByEventType(List<GameObject> targetObjects, string eventType)
    {
      using (List<IEffect>.Enumerator enumerator = this.effects[eventType].GetEnumerator())
      {
        while (enumerator.MoveNext())
          enumerator.Current.DoEffect(targetObjects);
      }
    }

    public void ExecuteEffectsByEventTypeAndName(List<GameObject> targetObjects, string eventType, string eventName)
    {
      using (List<IEffect>.Enumerator enumerator = this.effects[eventType].GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          IEffect current = enumerator.Current;
          if (current.Config.Name == eventName)
            current.DoEffect(targetObjects);
        }
      }
    }
  }
}
