﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.EventTypes
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace Assets.Scripts.EffectSystem
{
  public class EventTypes
  {
    public const string CLICK = "Click";
    public const string DOUBLE_CLICK = "DoubleClick";
    public const string DRAG = "Drag";
    public const string DROP = "Drop";
    public const string HOVER_ROLLOVER = "Hover_Rollover";
    public const string HOVER_ROLLOUT = "Hover_Rollout";
    public const string INPUT = "Input";
    public const string KEY = "Key";
    public const string PRESS_DOWN = "Press_Down";
    public const string PRESS_UP = "Press_Up";
    public const string SCROLL = "Scroll";
    public const string SELECT_IS = "Select_Is";
    public const string SELECT_NOT = "Select_Not";
    public const string TOOLTIP_VISIBLE = "Tooltip_Visible";
    public const string TOOLTIP_HIDDEN = "Tooltip_Hidden";
    public const string STATUS_CHANGED_ACTIVE = "Status_Canged_activ";
    public const string STATUS_CHANGED_INACTIVE = "Status_Canged_inactiv";
  }
}
