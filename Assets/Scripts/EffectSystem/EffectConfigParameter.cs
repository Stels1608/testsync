﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.EffectConfigParameter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Assets.Scripts.EffectSystem
{
  public class EffectConfigParameter : IXmlSerializable
  {
    public string Key { get; set; }

    [XmlAttribute("type")]
    public string ParamTypeName
    {
      get
      {
        return this.ParamType.ToString();
      }
      set
      {
        this.ParamType = Type.GetType(value);
      }
    }

    [XmlIgnore]
    public Type ParamType { get; set; }

    public object Value { get; set; }

    public XmlSchema GetSchema()
    {
      return (XmlSchema) null;
    }

    public void ReadXml(XmlReader reader)
    {
      int num = (int) reader.MoveToContent();
      this.Key = reader.GetAttribute("key");
      string attribute = reader.GetAttribute("type");
      if (attribute == null || string.Empty == attribute)
        throw new Exception("No valid type-attribute given!");
      this.ParamType = Type.GetType(attribute, true);
      if (this.ParamType == null)
        throw new Exception("Parameter-type " + attribute + " could not be resolved!");
      reader.ReadStartElement("param");
      if (this.ParamType.IsPrimitive)
        this.Value = reader.ReadContentAs(this.ParamType, (IXmlNamespaceResolver) null);
      else if (this.ParamType == typeof (string))
      {
        this.Value = (object) reader.ReadContentAsString();
      }
      else
      {
        object instance = Activator.CreateInstance(this.ParamType);
        if (!(instance is IXmlSerializable))
          throw new Exception("Parameter " + this.Key + " is not of type IXmlSerializable!");
        IXmlSerializable xmlSerializable = instance as IXmlSerializable;
        xmlSerializable.ReadXml(reader);
        this.Value = (object) xmlSerializable;
      }
      reader.ReadEndElement();
    }

    public void WriteXml(XmlWriter writer)
    {
      writer.WriteAttributeString("type", this.Value.GetType().ToString());
      writer.WriteAttributeString("key", this.Key);
      if (this.Value.GetType().IsPrimitive)
        writer.WriteString(this.Value.ToString());
      else if (this.Value is string)
      {
        writer.WriteString((string) this.Value);
      }
      else
      {
        if (!(this.Value is IXmlSerializable))
          throw new Exception("Parameter " + this.Key + " is not of type IXmlSerializable!");
        (this.Value as IXmlSerializable).WriteXml(writer);
      }
    }

    public TValueType GetValue<TValueType>()
    {
      return (TValueType) Convert.ChangeType(this.Value, this.ParamType);
    }
  }
}
