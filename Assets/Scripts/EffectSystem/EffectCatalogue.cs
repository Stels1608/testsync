﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.EffectCatalogue
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Assets.Scripts.EffectSystem.Effects;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace Assets.Scripts.EffectSystem
{
  public class EffectCatalogue
  {
    public EffectCatalogueSettings catalogueSettings;

    private Dictionary<string, EffectGroup> EffectGroups { get; set; }

    public EffectCatalogue()
    {
      this.EffectGroups = new Dictionary<string, EffectGroup>();
    }

    public EffectGroup GetEffectGroup(string name)
    {
      EffectGroup effectGroup = (EffectGroup) null;
      if (!this.EffectGroups.ContainsKey(name))
      {
        if (this.catalogueSettings.HasConfigForGroup(name))
        {
          this.EffectGroups[name] = this.AssembleGroup(name);
          effectGroup = this.EffectGroups[name];
        }
      }
      else
        effectGroup = this.EffectGroups[name];
      return effectGroup;
    }

    protected EffectGroup AssembleGroup(string name)
    {
      EffectGroupConfig group = this.catalogueSettings.GetGroup(name);
      EffectGroup effectGroup = new EffectGroup();
      effectGroup.Name = name;
      using (List<string>.Enumerator enumerator1 = group.GetEventIds().GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          string current = enumerator1.Current;
          effectGroup.RegisterEventType(current);
          using (List<EffectConfig>.Enumerator enumerator2 = group.GetEffectsForEventId(current).GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              IEffect effectInstance = enumerator2.Current.CreateEffectInstance();
              effectGroup.AddEffect(current, effectInstance);
            }
          }
        }
      }
      return effectGroup;
    }

    public bool HasGroupConfig(string name)
    {
      return this.catalogueSettings.HasConfigForGroup(name);
    }

    public EffectGroupConfig AddGroupConfig(string name)
    {
      return this.catalogueSettings.AddGroup(name);
    }

    public EffectGroupConfig GetGroupConfig(string name)
    {
      return this.catalogueSettings.GetGroup(name);
    }

    public List<EffectGroupConfig> GetAllGroupConfigs()
    {
      return this.catalogueSettings.GetAllGroups();
    }

    public void RemoveGroupConfig(string name)
    {
      this.catalogueSettings.RemoveGroup(name);
      if (!this.EffectGroups.ContainsKey(name))
        return;
      this.EffectGroups.Remove(name);
    }

    public bool LoadSettings()
    {
      bool flag = false;
      TextAsset textAsset = (TextAsset) Resources.Load("Settings/EffectCatalogue", typeof (TextAsset));
      if ((Object) null != (Object) textAsset && textAsset.text != null)
      {
        this.catalogueSettings = (EffectCatalogueSettings) new XmlSerializer(typeof (EffectCatalogueSettings)).Deserialize((XmlReader) new XmlTextReader((Stream) new MemoryStream(Encoding.UTF8.GetBytes(textAsset.text))));
        flag = true;
      }
      else
        this.catalogueSettings = new EffectCatalogueSettings();
      return flag;
    }
  }
}
