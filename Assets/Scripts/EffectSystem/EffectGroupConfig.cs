﻿// Decompiled with JetBrains decompiler
// Type: Assets.Scripts.EffectSystem.EffectGroupConfig
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Assets.Scripts.EffectSystem
{
  [XmlRoot("group")]
  [Serializable]
  public class EffectGroupConfig
  {
    private Dictionary<string, List<EffectConfig>> effectMap;

    [XmlAttribute("name")]
    public string Name { get; set; }

    private Dictionary<string, List<EffectConfig>> Effects
    {
      get
      {
        if (this.effectMap == null)
        {
          this.effectMap = new Dictionary<string, List<EffectConfig>>();
          using (List<EffectConfig>.Enumerator enumerator = this.EffectList.GetEnumerator())
          {
            while (enumerator.MoveNext())
            {
              EffectConfig current = enumerator.Current;
              if (!this.effectMap.ContainsKey(current.EventId))
                this.effectMap[current.EventId] = new List<EffectConfig>();
              this.effectMap[current.EventId].Add(current);
            }
          }
        }
        return this.effectMap;
      }
    }

    [XmlArray("effects")]
    [XmlArrayItem("effect")]
    public List<EffectConfig> EffectList { get; set; }

    public EffectGroupConfig()
    {
      this.EffectList = new List<EffectConfig>();
    }

    public EffectConfig AddEffect(Type tEffectClass, string eventId, string name)
    {
      EffectConfig effectConfig = new EffectConfig();
      effectConfig.EffectType = tEffectClass;
      effectConfig.EventId = eventId;
      effectConfig.Name = name;
      if (!this.Effects.ContainsKey(eventId))
        this.Effects[eventId] = new List<EffectConfig>();
      this.Effects[eventId].Add(effectConfig);
      this.EffectList.Add(effectConfig);
      return effectConfig;
    }

    public bool HasEffectForEventId(Type tEffectClass, string eventId)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: reference to a compiler-generated method
      return this.Effects.ContainsKey(eventId) && this.Effects[eventId].Any<EffectConfig>(new Func<EffectConfig, bool>(new EffectGroupConfig.\u003CHasEffectForEventId\u003Ec__AnonStorey4C()
      {
        tEffectClass = tEffectClass
      }.\u003C\u003Em__3));
    }

    public bool HasEffectsForEventId(string eventId)
    {
      return this.Effects.ContainsKey(eventId);
    }

    public EffectConfig RemoveEffectForEventId(Type tEffectClass, string eventId, int index)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      EffectGroupConfig.\u003CRemoveEffectForEventId\u003Ec__AnonStorey4D eventIdCAnonStorey4D = new EffectGroupConfig.\u003CRemoveEffectForEventId\u003Ec__AnonStorey4D();
      // ISSUE: reference to a compiler-generated field
      eventIdCAnonStorey4D.tEffectClass = tEffectClass;
      EffectConfig effectConfig = (EffectConfig) null;
      // ISSUE: reference to a compiler-generated field
      if (this.HasEffectForEventId(eventIdCAnonStorey4D.tEffectClass, eventId))
      {
        // ISSUE: reference to a compiler-generated method
        effectConfig = this.Effects[eventId].Single<EffectConfig>(new Func<EffectConfig, bool>(eventIdCAnonStorey4D.\u003C\u003Em__4));
        this.Effects[eventId].Remove(effectConfig);
        this.EffectList.Remove(effectConfig);
        if (this.Effects[eventId].Count < 1)
          this.Effects.Remove(eventId);
      }
      return effectConfig;
    }

    public EffectConfig RemoveEffectAtIndex(int index)
    {
      EffectConfig effectConfig = (EffectConfig) null;
      if (index <= this.EffectList.Count - 1 && index >= 0)
      {
        effectConfig = this.EffectList[index];
        this.EffectList.RemoveAt(index);
      }
      return effectConfig;
    }

    public List<EffectConfig> GetEffectsForEventId(string eventId)
    {
      List<EffectConfig> effectConfigList = (List<EffectConfig>) null;
      if (this.Effects.ContainsKey(eventId))
        effectConfigList = new List<EffectConfig>((IEnumerable<EffectConfig>) this.Effects[eventId]);
      return effectConfigList;
    }

    public List<string> GetEventIds()
    {
      return this.Effects.Keys.ToList<string>();
    }
  }
}
