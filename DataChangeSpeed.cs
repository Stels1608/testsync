﻿// Decompiled with JetBrains decompiler
// Type: DataChangeSpeed
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class DataChangeSpeed
{
  private readonly ShipControlsBase.SpeedMode speedMode;
  private readonly float speedChange;

  public ShipControlsBase.SpeedMode SpeedMode
  {
    get
    {
      return this.speedMode;
    }
  }

  public float SpeedChange
  {
    get
    {
      return this.speedChange;
    }
  }

  public DataChangeSpeed(ShipControlsBase.SpeedMode speedMode, float speedChange)
  {
    this.speedMode = speedMode;
    this.speedChange = speedChange;
  }
}
