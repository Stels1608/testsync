﻿// Decompiled with JetBrains decompiler
// Type: ChatDynamicRoomClosed
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

internal class ChatDynamicRoomClosed : ChatProtocolParser
{
  public int roomID;

  public ChatDynamicRoomClosed()
  {
    this.type = ChatProtocolParserType.DynamicRoomClosed;
  }

  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split(new char[2]{ '%', '#' });
    return strArray.Length == 3 && !(strArray[0] != "fr") && int.TryParse(strArray[1], out this.roomID);
  }
}
