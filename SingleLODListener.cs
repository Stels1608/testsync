﻿// Decompiled with JetBrains decompiler
// Type: SingleLODListener
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class SingleLODListener : LODListener
{
  public float Limit;

  protected override void Awake()
  {
    this.levelLimits = new float[1]{ this.Limit };
    base.Awake();
  }

  protected override void OnLevelChanged()
  {
    this.OnSwitched(this.Level == 0);
  }

  protected virtual void OnSwitched(bool value)
  {
  }
}
