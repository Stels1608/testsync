﻿// Decompiled with JetBrains decompiler
// Type: iTweenPathEnhanced
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("iTween/iTweenPathEnhanced")]
public class iTweenPathEnhanced : MonoBehaviour
{
  public static Dictionary<string, iTweenPathEnhanced> paths = new Dictionary<string, iTweenPathEnhanced>();
  public string pathName = string.Empty;
  public Color pathColor = Color.cyan;
  public List<GameObject> nodes = new List<GameObject>();
  public string initialName = string.Empty;
  public bool pathVisible = true;
  public AnimationCurve animCurve = new AnimationCurve();
  public const float MAX_SPEED = 120f;
  public int nodeCount;
  public bool initialized;

  public void AddNode()
  {
    GameObject gameObject = new GameObject("Node " + (object) (this.nodes.Count + 1));
    gameObject.transform.parent = this.transform;
    gameObject.transform.position = this.nodes.Count != 0 ? this.nodes.FindLast((Predicate<GameObject>) (t => true)).transform.position : Vector3.zero;
    gameObject.transform.position += Vector3.right * 5f;
    this.nodes.Add(gameObject);
    this.AddNodeToAnimCurve();
  }

  public void RemoveNode(int index)
  {
    GameObject gameObject = this.nodes[index];
    this.nodes.Remove(gameObject);
    UnityEngine.Object.DestroyImmediate((UnityEngine.Object) gameObject, true);
    this.animCurve.RemoveKey(index);
  }

  private void AddNodeToAnimCurve()
  {
    if (this.nodes.Count == 0)
      Debug.LogError((object) "Errr0000r");
    float time = this.nodes.Count != 1 ? iTween.PathLength(this.GetNodePositions().ToArray()) : 0.0f;
    float num = 0.0f;
    if (this.animCurve.length == 1)
      num = 120f;
    if (this.animCurve.length > 1)
      num = this.animCurve[this.animCurve.length - 1].value;
    this.animCurve.AddKey(new Keyframe(time, num));
  }

  private void OnEnable()
  {
    if (iTweenPathEnhanced.paths.ContainsKey(this.pathName))
      return;
    iTweenPathEnhanced.paths.Add(this.pathName.ToLower(), this);
  }

  private void OnDisable()
  {
    iTweenPathEnhanced.paths.Remove(this.pathName.ToLower());
  }

  private void OnDrawGizmosSelected()
  {
    if (!this.pathVisible || this.nodes.Count <= 0)
      return;
    iTween.DrawPath(this.GetNodePositions().ToArray(), this.pathColor);
    this.DrawOrientations();
  }

  private void DrawOrientations()
  {
    using (List<GameObject>.Enumerator enumerator = this.nodes.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        Vector3 position = current.transform.position;
        Quaternion rotation = current.transform.rotation;
        Debug.DrawLine(position, position + rotation * Vector3.forward * 1f, Color.blue);
        Debug.DrawLine(position, position + rotation * Vector3.right * 1f, Color.red);
      }
    }
  }

  public List<Vector3> GetNodePositions()
  {
    List<Vector3> vector3List = new List<Vector3>();
    using (List<GameObject>.Enumerator enumerator = this.nodes.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        vector3List.Add(current.transform.position);
      }
    }
    return vector3List;
  }

  public static Vector3[] GetPath(string requestedName)
  {
    requestedName = requestedName.ToLower();
    if (iTweenPathEnhanced.paths.ContainsKey(requestedName))
      return iTweenPathEnhanced.paths[requestedName].GetNodePositions().ToArray();
    Debug.Log((object) ("No path with that name (" + requestedName + ") exists! Are you sure you wrote it correctly?"));
    return (Vector3[]) null;
  }

  public static Vector3[] GetPathReversed(string requestedName)
  {
    requestedName = requestedName.ToLower();
    if (iTweenPathEnhanced.paths.ContainsKey(requestedName))
    {
      List<Vector3> range = iTweenPathEnhanced.paths[requestedName].GetNodePositions().GetRange(0, iTweenPathEnhanced.paths[requestedName].nodes.Count);
      range.Reverse();
      return range.ToArray();
    }
    Debug.Log((object) ("No path with that name (" + requestedName + ") exists! Are you sure you wrote it correctly?"));
    return (Vector3[]) null;
  }
}
