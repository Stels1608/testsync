﻿// Decompiled with JetBrains decompiler
// Type: CouchDB.SerProperty
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Reflection;

namespace CouchDB
{
  internal class SerProperty
  {
    public bool Read = true;
    public bool Write = true;
    public bool SkipNull = true;
    public string JsonName;
    public string MemberXPath;
    private Type ObjType;

    public SerProperty(Type type, string memberXPath, string JsonName, bool skipNull)
    {
      this.ObjType = type;
      this.JsonName = JsonName;
      this.MemberXPath = memberXPath;
      this.SkipNull = skipNull;
    }

    public void SetValue(object obj, object value)
    {
      if (obj.GetType() != this.ObjType)
        throw new Exception("SetValue error: " + this.ObjType.ToString() + " expected. Got " + obj.GetType().ToString());
      MemberInfo memberInfo;
      object objValue;
      object parentValue;
      this.TraverseXPath(this.ObjType, this.MemberXPath, obj, out memberInfo, out objValue, out parentValue);
      this.MSetValue(parentValue, memberInfo, value);
    }

    public object GetValue(object obj)
    {
      if (obj.GetType() != this.ObjType)
        throw new Exception("GetValue error: " + this.ObjType.ToString() + " expected. Got " + obj.GetType().ToString());
      MemberInfo memberInfo;
      object objValue;
      object parentValue;
      this.TraverseXPath(this.ObjType, this.MemberXPath, obj, out memberInfo, out objValue, out parentValue);
      return objValue;
    }

    public Type GetPropertyType()
    {
      MemberInfo memberInfo;
      object objValue;
      object parentValue;
      this.TraverseXPath(this.ObjType, this.MemberXPath, (object) null, out memberInfo, out objValue, out parentValue);
      return this.MGetType(memberInfo);
    }

    private void TraverseXPath(Type type, string propertyName, object obj, out MemberInfo memberInfo, out object objValue, out object parentValue)
    {
      string[] strArray = propertyName.Split(new char[1]{ '/' }, 2);
      MemberInfo[] members = type.FindMembers(MemberTypes.Field | MemberTypes.Property, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, (MemberFilter) ((candidate, part) => candidate.Name == part.ToString()), (object) strArray[0]);
      if (members.Length == 0)
        throw new Exception(this.ObjType.ToString() + ": Invalid XPath: " + propertyName + ". Type = " + type.ToString());
      MemberInfo member = members[0];
      if (strArray.Length > 1)
      {
        object obj1 = obj != null ? this.MGetValue(obj, member) : (object) null;
        this.TraverseXPath(this.MGetType(member), strArray[1], obj1, out memberInfo, out objValue, out parentValue);
      }
      else
      {
        if (!(member.Name == propertyName))
          throw new Exception();
        memberInfo = member;
        objValue = obj == null ? (object) null : this.MGetValue(obj, member);
        parentValue = obj;
      }
    }

    private Type MGetType(MemberInfo member)
    {
      if (member is FieldInfo)
        return (member as FieldInfo).FieldType;
      if (member is PropertyInfo)
        return (member as PropertyInfo).PropertyType;
      throw new Exception("MGetType: wrong MemberInfo: " + member.Name);
    }

    private object MGetValue(object obj, MemberInfo member)
    {
      if (member is FieldInfo)
        return (member as FieldInfo).GetValue(obj);
      if (member is PropertyInfo)
        return (member as PropertyInfo).GetValue(obj, (object[]) null);
      throw new Exception("MGetValue: wrong MemberInfo: " + member.Name);
    }

    private void MSetValue(object obj, MemberInfo member, object value)
    {
      if (member is FieldInfo)
      {
        (member as FieldInfo).SetValue(obj, value);
      }
      else
      {
        if (!(member is PropertyInfo))
          throw new Exception("MSetValue: wrong MemberInfo: " + member.Name + " " + member.GetType().ToString());
        (member as PropertyInfo).SetValue(obj, value, (object[]) null);
      }
    }
  }
}
