﻿// Decompiled with JetBrains decompiler
// Type: CouchDB.JsonSerializator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace CouchDB
{
  public class JsonSerializator
  {
    public static JsonData Serialize(object obj)
    {
      if (obj == null)
        return new JsonData(JsonType.Null);
      if (obj is IJsonSerializationListener)
        (obj as IJsonSerializationListener).JsonBeforeSerialization();
      System.Type type = obj.GetType();
      if (type == typeof (string))
        return new JsonData((string) obj);
      if (type == typeof (int))
        return new JsonData((int) obj);
      if (type == typeof (uint))
        return new JsonData((int) (uint) obj);
      if (type == typeof (byte))
        return new JsonData((int) (byte) obj);
      if (type == typeof (short))
        return new JsonData((int) (short) obj);
      if (type == typeof (ushort))
        return new JsonData((int) (ushort) obj);
      if (type == typeof (float))
        return new JsonData((float) obj);
      if (type == typeof (double))
        return new JsonData((double) obj);
      if (type == typeof (bool))
        return new JsonData((bool) obj);
      if (type.GetInterface("System.Collections.IDictionary", true) != null)
      {
        if (type.GetGenericArguments()[0] != typeof (string))
          throw new Exception("The type " + type.FullName + " is a strange Dictionary.");
        JsonData createObject = JsonData.CreateObject;
        IDictionary dictionary = (IDictionary) obj;
        foreach (string key in (IEnumerable) dictionary.Keys)
        {
          JsonData jsonData = JsonSerializator.Serialize(dictionary[(object) key]);
          createObject.Object.Add(key, jsonData);
        }
        if (obj is IJsonSerializationListener)
          (obj as IJsonSerializationListener).JsonSerialization(createObject);
        return createObject;
      }
      if (type.IsArray || type.GetInterface("System.Collections.IList", true) != null)
      {
        JsonData createArray = JsonData.CreateArray;
        foreach (object obj1 in (IEnumerable) obj)
          createArray.Array.Add(JsonSerializator.Serialize(obj1));
        if (obj is IJsonSerializationListener)
          (obj as IJsonSerializationListener).JsonSerialization(createArray);
        return createArray;
      }
      if (type.IsEnum)
        return new JsonData(Enum.GetName(obj.GetType(), obj));
      if (!type.IsClass && !type.IsValueType)
        throw new Exception("The type " + type.FullName + " is not supported");
      JsonData json = JsonSerializator.SerializeObject(obj);
      if (obj is IJsonSerializationListener)
        (obj as IJsonSerializationListener).JsonSerialization(json);
      json.SortKeys();
      return json;
    }

    private static JsonData SerializeObject(object obj)
    {
      List<SerProperty> serializableMembers = JsonSerializator.GetSerializableMembers(obj.GetType(), true);
      JsonData createObject = JsonData.CreateObject;
      using (List<SerProperty>.Enumerator enumerator = serializableMembers.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          SerProperty current = enumerator.Current;
          object obj1 = current.GetValue(obj);
          if (obj1 != null || !current.SkipNull)
          {
            JsonData jsonData = JsonSerializator.Serialize(obj1);
            if (createObject.Object.ContainsKey(current.JsonName))
              throw new Exception("Property " + current.JsonName + " already exists in " + obj.GetType().ToString() + ". " + createObject.ToJsonString(string.Empty));
            createObject.Object.Add(current.JsonName, jsonData);
          }
        }
      }
      return createObject;
    }

    public static T Deserialize<T>(JsonData json)
    {
      return (T) JsonSerializator.Deserialize(typeof (T), json);
    }

    public static object Deserialize(System.Type type, JsonData json)
    {
      if (json.IsNull)
        return (object) null;
      if (type == typeof (string))
      {
        string str = json.String;
        if (json.String.StartsWith("bgo."))
          str = "%$" + json.String + "%";
        return (object) str;
      }
      if (type == typeof (int))
        return (object) json.Int32;
      if (type == typeof (uint))
        return (object) (uint) json.Int32;
      if (type == typeof (short))
        return (object) (short) json.Int32;
      if (type == typeof (ushort))
        return (object) (ushort) json.Int32;
      if (type == typeof (byte))
        return (object) (byte) json.Int32;
      if (type == typeof (byte))
        return (object) (byte) json.Int32;
      if (type == typeof (float))
        return (object) json.Float;
      if (type == typeof (double))
        return (object) json.Double;
      if (type == typeof (bool))
        return (object) json.Boolean;
      object obj;
      if (type.GetInterface("System.Collections.IDictionary", true) != null)
      {
        System.Type[] genericArguments = type.GetGenericArguments();
        if (genericArguments[0] != typeof (string))
          throw new Exception("The type " + type.FullName + " is a strange Dictionary.");
        IDictionary dictionary = (IDictionary) Activator.CreateInstance(type);
        using (Dictionary<string, JsonData>.KeyCollection.Enumerator enumerator = json.Object.Keys.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            string current = enumerator.Current;
            dictionary.Add((object) current, JsonSerializator.Deserialize(genericArguments[1], json.Object[current]));
          }
        }
        obj = (object) dictionary;
      }
      else if (type.IsArray)
      {
        Array instance = Array.CreateInstance(type.GetElementType(), json.Array.Count);
        for (int index = 0; index < instance.Length; ++index)
          instance.SetValue(JsonSerializator.Deserialize(type.GetElementType(), json.Array[index]), index);
        obj = (object) instance;
      }
      else if (type.GetInterface("System.Collections.IList", true) != null)
      {
        IList list = (IList) Activator.CreateInstance(type);
        System.Type type1 = type.GetGenericArguments()[0];
        using (List<JsonData>.Enumerator enumerator = json.Array.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            JsonData current = enumerator.Current;
            list.Add(JsonSerializator.Deserialize(type1, current));
          }
        }
        obj = (object) list;
      }
      else if (type.IsEnum && json.JsonType != JsonType.Object)
      {
        obj = JsonSerializator.DeserializeEnum(type, json);
      }
      else
      {
        if (!type.IsClass && !type.IsValueType)
          throw new Exception("The type is not supported");
        obj = JsonSerializator.DeserializeObject(type, json, (object) null);
      }
      if (obj is IJsonSerializationListener)
        (obj as IJsonSerializationListener).JsonDeserialization(json);
      if (obj is IJsonEvent)
        ((IJsonEvent) obj).AfterSerialize();
      return obj;
    }

    public static object DeserializeEnum(System.Type type, JsonData json)
    {
      if (!json.IsString)
      {
        Debug.Log((object) ("Error deserializing Enum... jsondata is no string. Type: " + (object) type + " --- has JsonType: " + (object) json.JsonType));
        return (object) null;
      }
      try
      {
        return Enum.Parse(type, json.String, true);
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("DeserializeEnum had an exception\n The type " + type.ToString() + " had an error\n" + ex.Message + "\n" + ex.StackTrace));
        throw ex;
      }
    }

    public static object DeserializeObject(System.Type type, JsonData json, object objectToFill)
    {
      try
      {
        List<SerProperty> serializableMembers = JsonSerializator.GetSerializableMembers(type, false);
        object obj1 = objectToFill ?? JsonSerializator.InstantiateObject(type, serializableMembers, json);
        using (List<SerProperty>.Enumerator enumerator = serializableMembers.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            SerProperty current = enumerator.Current;
            if (json.Object.ContainsKey(current.JsonName))
            {
              try
              {
                object obj2 = JsonSerializator.Deserialize(current.GetPropertyType(), json.Object[current.JsonName]);
                current.SetValue(obj1, obj2);
              }
              catch
              {
              }
            }
          }
        }
        return obj1;
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ("DeserializeObject had an exception\n The type " + type.ToString() + " had an error\n" + ex.Message + "\n" + ex.StackTrace));
        throw;
      }
    }

    private static object InstantiateObject(System.Type type, List<SerProperty> members, JsonData json)
    {
      JsonClassInstantiationAttribute attribute = JsonSerializator.GetAttribute<JsonClassInstantiationAttribute>((ICustomAttributeProvider) type, false);
      if ((attribute == null || attribute.ConstructorArgs == null || attribute.ConstructorArgs.Length == 0) && (attribute == null || attribute.ConstructorJsonDataArgs == null))
        return Activator.CreateInstance(type);
      List<object> objectList = new List<object>();
      if (attribute.ConstructorArgs != null)
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        JsonSerializator.\u003CInstantiateObject\u003Ec__AnonStorey114 objectCAnonStorey114 = new JsonSerializator.\u003CInstantiateObject\u003Ec__AnonStorey114();
        foreach (string constructorArg in attribute.ConstructorArgs)
        {
          // ISSUE: reference to a compiler-generated field
          objectCAnonStorey114.param = constructorArg;
          // ISSUE: reference to a compiler-generated field
          if (!json.Object.ContainsKey(objectCAnonStorey114.param))
          {
            // ISSUE: reference to a compiler-generated field
            throw new Exception("Can't construct type " + type.ToString() + ": can't find parameters required: " + objectCAnonStorey114.param);
          }
          // ISSUE: reference to a compiler-generated method
          // ISSUE: reference to a compiler-generated field
          object obj = JsonSerializator.Deserialize(members.Find(new Predicate<SerProperty>(objectCAnonStorey114.\u003C\u003Em__29B)).GetPropertyType(), json.Object[objectCAnonStorey114.param]);
          objectList.Add(obj);
        }
      }
      if (attribute.ConstructorJsonDataArgs != null)
      {
        foreach (string constructorJsonDataArg in attribute.ConstructorJsonDataArgs)
        {
          if (!json.Object.ContainsKey(constructorJsonDataArg))
            throw new Exception("Can't construct type " + type.ToString() + ": can't find parameters required: " + constructorJsonDataArg);
          objectList.Add((object) json.Object[constructorJsonDataArg]);
        }
      }
      return Activator.CreateInstance(type, objectList.ToArray());
    }

    private static List<SerProperty> GetSerializableMembers(System.Type type, bool serialize)
    {
      JsonClassInstantiationAttribute attribute = JsonSerializator.GetAttribute<JsonClassInstantiationAttribute>((ICustomAttributeProvider) type, true);
      if (attribute != null && attribute.mode == JsonClassInstantiationAttribute.Modes.Manual)
        return JsonSerializator.GetSerializableMembersManual(type, serialize);
      return JsonSerializator.GetSerializableMembersAuto(type, serialize, false);
    }

    private static List<SerProperty> GetSerializableMembersManual(System.Type type, bool serialize)
    {
      List<SerProperty> serPropertyList = new List<SerProperty>(type.FindMembers(MemberTypes.Field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, (MemberFilter) null, (object) null).Length);
      foreach (JsonClassDescAttribute customAttribute in (JsonClassDescAttribute[]) type.GetCustomAttributes(typeof (JsonClassDescAttribute), true))
      {
        if (customAttribute != null && (serialize && customAttribute.serialize || !serialize && customAttribute.deserialize))
        {
          string JsonName = customAttribute == null || string.IsNullOrEmpty(customAttribute.JsonName) ? customAttribute.FieldName : customAttribute.JsonName;
          SerProperty serProperty = new SerProperty(type, customAttribute.FieldName, JsonName, customAttribute == null || customAttribute.SkipNull);
          serPropertyList.Add(serProperty);
        }
      }
      List<SerProperty> serializableMembersAuto = JsonSerializator.GetSerializableMembersAuto(type, serialize, true);
      serPropertyList.AddRange((IEnumerable<SerProperty>) serializableMembersAuto);
      return serPropertyList;
    }

    private static List<SerProperty> GetSerializableMembersAuto(System.Type type, bool serialize, bool withAttrsOnly)
    {
      MemberInfo[] members = type.FindMembers(MemberTypes.Field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, (MemberFilter) null, (object) null);
      List<SerProperty> serPropertyList = new List<SerProperty>(members.Length);
      foreach (MemberInfo memberInfo in members)
      {
        JsonFieldAttribute attribute = JsonSerializator.GetAttribute<JsonFieldAttribute>((ICustomAttributeProvider) memberInfo, true);
        if ((!withAttrsOnly || attribute != null) && (attribute == null || (serialize && attribute.serialize || !serialize && attribute.deserialize)))
        {
          string JsonName = attribute == null || string.IsNullOrEmpty(attribute.JsonName) ? memberInfo.Name : attribute.JsonName;
          SerProperty serProperty = new SerProperty(type, memberInfo.Name, JsonName, attribute == null || attribute.SkipNull);
          serPropertyList.Add(serProperty);
        }
      }
      return serPropertyList;
    }

    private static TT GetAttribute<TT>(ICustomAttributeProvider member, bool inherit) where TT : Attribute
    {
      TT[] tArray = (TT[]) member.GetCustomAttributes(typeof (TT), inherit);
      if (tArray.Length > 1)
        throw new Exception("2 or more attributes.");
      if (tArray.Length == 0)
        return (TT) null;
      return tArray[0];
    }
  }
}
