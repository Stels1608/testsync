﻿// Decompiled with JetBrains decompiler
// Type: CouchDB.CouchServer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace CouchDB
{
  public class CouchServer
  {
    public static readonly ServerInfo[] couchServers = new ServerInfo[11]{ new ServerInfo("http://62.146.190.39:5984", "BigPoint", "bgo2"), new ServerInfo("http://62.146.190.39:5984", "SFO-Dev", "bgo704"), new ServerInfo("http://62.146.190.39:5984", "SFO-Dev-george", "bgo704-george"), new ServerInfo("http://62.146.190.39:5984", "SFO-Dev-reggie", "bgo704-reggie"), new ServerInfo("http://62.146.190.39:5984", "SFO-Dev-bill", "bgo704-bill"), new ServerInfo("http://62.146.190.39:5984", "SFO-Dev-brian", "bgo2-brian"), new ServerInfo("http://62.146.190.39:5984", "SFO-Dev-paul", "bgo2-paul"), new ServerInfo("http://62.146.190.39:5984", "SFO-Dev-ron", "bgo704-ron"), new ServerInfo("http://62.146.190.39:5984", "SFO-Dev-ron", "bgo-cottle-1-2"), new ServerInfo("http://62.146.190.39:5984", "Stemp", "bgo_temp"), new ServerInfo("http://127.0.0.1:5984", "127.0.0.1", "bgo2") };
    private const string COUCH_SETTINGS_FILE = "couch.couchsettings";
    private static ServerInfo serverInfo;

    public static ServerInfo ServerInfo
    {
      get
      {
        if (CouchServer.serverInfo == null)
        {
          try
          {
            Stream stream = (Stream) System.IO.File.Open("couch.couchsettings", FileMode.Open);
            CouchServer.serverInfo = (ServerInfo) new XmlSerializer(typeof (ServerInfo)).Deserialize(stream);
            stream.Close();
          }
          catch (IOException ex)
          {
            Debug.Log((object) ex);
            CouchServer.serverInfo = CouchServer.couchServers[0];
          }
        }
        return CouchServer.serverInfo;
      }
      set
      {
        CouchServer.serverInfo = value;
        Stream stream = (Stream) System.IO.File.Open("couch.couchsettings", FileMode.Create);
        new XmlSerializer(typeof (ServerInfo)).Serialize(stream, (object) CouchServer.serverInfo);
        stream.Close();
      }
    }

    public static List<string> ListDatabases()
    {
      JsonData jsonData = CouchServer.DoRequest(CouchServer.ServerInfo.server + "/_all_dbs", "GET");
      List<string> stringList = new List<string>();
      using (List<JsonData>.Enumerator enumerator = jsonData.Array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          JsonData current = enumerator.Current;
          stringList.Add(current.String);
        }
      }
      return stringList;
    }

    public static void CreateDatabase(string dbName)
    {
      JsonData jsonData = CouchServer.DoRequest(CouchServer.ServerInfo.server + "/" + dbName, "PUT");
      if (!jsonData.Object.ContainsKey("ok") || !jsonData.Object["ok"].Boolean)
        throw new NotFoundException("Failed to create database: " + (object) jsonData);
    }

    public static void DeleteDatabase()
    {
      JsonData jsonData = CouchServer.DoRequest(CouchServer.ServerInfo.FullPath, "DELETE");
      if (!jsonData.Object.ContainsKey("ok") || !jsonData.Object["ok"].Boolean)
        throw new NotFoundException("Failed to delete database: " + (object) jsonData);
    }

    public static int CountDocuments()
    {
      return CouchServer.DoRequest(CouchServer.ServerInfo.FullPath, "GET").Object["doc_count"].Int32;
    }

    public static List<DocumentInfo> GetAllDocumentsInfo()
    {
      JsonData jsonData = CouchServer.DoRequestServer("_all_docs", "GET");
      List<DocumentInfo> documentInfoList = new List<DocumentInfo>();
      using (List<JsonData>.Enumerator enumerator = jsonData.Object["rows"].Array.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          DocumentInfo documentInfo = CouchServer.ParseDocumentInfo(enumerator.Current);
          documentInfoList.Add(documentInfo);
        }
      }
      return documentInfoList;
    }

    private static DocumentInfo ParseDocumentInfo(JsonData json)
    {
      return new DocumentInfo() { id = json.Object["id"].String, key = json.Object["key"].String, revision = json.Object["value"].Object["rev"].String };
    }

    public static DocumentInfo GetDocumentInfo(string name)
    {
      JsonData jsonData = CouchServer.DoRequestServer("_all_docs?startkey=\"" + name + "\"&limit=1", "GET").Object["rows"];
      if (jsonData.Array.Count == 0)
        return (DocumentInfo) null;
      DocumentInfo documentInfo = CouchServer.ParseDocumentInfo(jsonData.Array[0]);
      if (documentInfo.id != name)
        return (DocumentInfo) null;
      return documentInfo;
    }

    public static bool DocumentExists(string id)
    {
      return CouchServer.GetDocumentInfo(id) != null;
    }

    public static void DeleteDocument(string name)
    {
      DocumentInfo documentInfo = CouchServer.GetDocumentInfo(name);
      if (documentInfo == null)
        return;
      CouchServer.DoRequestServer(name + "?rev=" + documentInfo.revision, "DELETE");
    }

    public static JsonData CreateDocument(JsonData json, string name)
    {
      if (json.Object.ContainsKey("_rev"))
        json.Object.Remove("_rev");
      json.Object["_id"] = new JsonData(name);
      JsonData jsonData = CouchServer.DoRequestServer(name, "PUT", json.ToJsonString(string.Empty));
      if (!jsonData.Object.ContainsKey("ok") || !jsonData.Object["ok"].Boolean)
        throw new NotFoundException("Creation document failed: " + jsonData.ToJsonString(string.Empty));
      return jsonData;
    }

    public static void CreateDocument<T>(T obj, string name)
    {
      JsonData document = CouchServer.CreateDocument(JsonSerializator.Serialize((object) obj), name);
      if (!((object) obj is CouchDocumentBase))
        return;
      ((object) obj as CouchDocumentBase).SetId(document.Object["id"].String);
    }

    public static T GetDocument<T>(string docid)
    {
      return CouchServer.GetDocument<T>(docid, "/");
    }

    public static T GetDocument<T>(string docid, string xpath)
    {
      return JsonSerializator.Deserialize<T>(CouchServer.GetDocumentRaw(docid, xpath));
    }

    public static T GetDocument<T>(string docid, string xpath, T objectToFill)
    {
      JsonData documentRaw = CouchServer.GetDocumentRaw(docid, xpath);
      return (T) JsonSerializator.DeserializeObject(objectToFill.GetType(), documentRaw, (object) objectToFill);
    }

    public static JsonData GetDocumentRaw(string docid, string xpath)
    {
      return XPathTraverser.Traverse(CouchServer.GetDocumentRaw(docid), xpath);
    }

    public static JsonData GetDocumentRaw(string docid)
    {
      return CouchServer.DoRequestServer(docid, "GET");
    }

    public static void ReplaceDocument<T>(T obj, string name)
    {
      JsonData json = JsonSerializator.Serialize((object) obj);
      if (json.Object.ContainsKey("_id"))
        json.Object.Remove("_id");
      if (json.Object.ContainsKey("_rev"))
        json.Object.Remove("_rev");
      JsonData jsonData = !CouchServer.DocumentExists(name) ? CouchServer.CreateDocument(json, name) : CouchServer.UpdateDocumentJson(name, "/", json);
      if (!((object) obj is CouchDocumentBase))
        return;
      ((object) obj as CouchDocumentBase).SetId(jsonData.Object["id"].String);
    }

    public static JsonData UpdateDocumentRaw(JsonData json)
    {
      JsonData jsonData = CouchServer.DoRequestServer(json.Object["_id"].String, "PUT", json.ToJsonString(string.Empty));
      if (!jsonData.Object.ContainsKey("ok") || !jsonData.Object["ok"].Boolean)
        throw new NotFoundException("Creation document failed: " + jsonData.ToJsonString(string.Empty));
      return jsonData;
    }

    public static JsonData UpdateDocument(string docid, string xpath, object obj)
    {
      JsonData jsonData = JsonSerializator.Serialize(obj);
      return CouchServer.UpdateDocumentJson(docid, xpath, jsonData);
    }

    public static JsonData UpdateDocumentJson(string docid, string xpath, JsonData obj)
    {
      JsonData documentRaw = CouchServer.GetDocumentRaw(docid);
      CouchServer.MergeJson(documentRaw, xpath, obj);
      return CouchServer.UpdateDocumentRaw(documentRaw);
    }

    public static void MergeJson(JsonData obj, string xpath, JsonData data)
    {
      JsonData jsonData = XPathTraverser.Traverse(obj, xpath);
      if (jsonData.IsArray && data.IsArray)
      {
        jsonData.Array.Clear();
        using (List<JsonData>.Enumerator enumerator = data.Array.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            JsonData current = enumerator.Current;
            jsonData.Array.Add(current);
          }
        }
      }
      else
      {
        if (!jsonData.IsObject || !data.IsObject)
          throw new Exception("Can't merge json array and json object.");
        using (Dictionary<string, JsonData>.Enumerator enumerator = data.Object.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            KeyValuePair<string, JsonData> current = enumerator.Current;
            jsonData.Object[current.Key] = current.Value;
          }
        }
      }
    }

    public static JsonData DoRequestServer(string url, string method)
    {
      return CouchServer.DoRequestServer(url, method, (string) null);
    }

    public static JsonData DoRequestServer(string url, string method, string postdata)
    {
      return CouchServer.DoRequest(CouchServer.ServerInfo.FullPath + "/" + url, method, postdata);
    }

    private static JsonData DoRequest(string url, string method)
    {
      return CouchServer.DoRequest(url, method, (string) null);
    }

    private static JsonData DoRequest(string url, string method, string postdata)
    {
      HttpWebRequest httpWebRequest = WebRequest.Create(url) as HttpWebRequest;
      httpWebRequest.Method = method;
      httpWebRequest.Timeout = -1;
      if (postdata != null)
      {
        httpWebRequest.ContentType = "application/json";
        byte[] bytes = Encoding.UTF8.GetBytes(postdata.ToString());
        httpWebRequest.ContentLength = (long) bytes.Length;
        httpWebRequest.GetRequestStream().Write(bytes, 0, bytes.Length);
      }
      try
      {
        return JsonReader.Parse(new StreamReader((httpWebRequest.GetResponse() as HttpWebResponse).GetResponseStream()).ReadToEnd());
      }
      catch (WebException ex)
      {
        if (ex is WebException && ex.Status == WebExceptionStatus.ConnectFailure)
          throw new Exception(url + " connection failed.", (Exception) ex);
        if ((ex.Response as HttpWebResponse).StatusCode == HttpStatusCode.NotFound)
          throw new NotFoundException(url + " not found.", (Exception) ex);
        if ((ex.Response as HttpWebResponse).StatusCode == HttpStatusCode.Conflict)
          throw new ConflictException(url + "conflict.", (Exception) ex);
        throw new NotFoundException("o_O\n" + ex.ToString() + "\nurl: " + url + "\nmethod: " + method + "\ndata: " + postdata);
      }
    }
  }
}
