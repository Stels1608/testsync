﻿// Decompiled with JetBrains decompiler
// Type: CouchDB.ServerInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.Serialization;

namespace CouchDB
{
  [Serializable]
  public class ServerInfo : ISerializable
  {
    public string server;
    public string name;
    public string db;

    public string FullPath
    {
      get
      {
        return this.server + "/" + this.db;
      }
    }

    public ServerInfo()
    {
    }

    public ServerInfo(string server, string name, string db)
    {
      this.server = server;
      this.name = name;
      this.db = db;
    }

    public ServerInfo(SerializationInfo info, StreamingContext ctxt)
    {
      this.name = (string) info.GetValue("Name", typeof (string));
      this.server = (string) info.GetValue("Server", typeof (string));
      this.db = (string) info.GetValue("Db", typeof (string));
    }

    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
      info.AddValue("Name", (object) this.name);
      info.AddValue("Server", (object) this.server);
      info.AddValue("Db", (object) this.db);
    }
  }
}
