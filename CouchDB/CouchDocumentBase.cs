﻿// Decompiled with JetBrains decompiler
// Type: CouchDB.CouchDocumentBase
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

namespace CouchDB
{
  [JsonClassInstantiation]
  public class CouchDocumentBase : IJsonEvent
  {
    [JsonField(JsonName = "_id")]
    public string _id = string.Empty;
    [JsonField(JsonName = "Category")]
    public string _category;

    public string Id
    {
      get
      {
        return this._id;
      }
    }

    public string Category
    {
      get
      {
        return this._category;
      }
    }

    public void SetId(string Id)
    {
      this._id = Id;
    }

    public virtual void AfterSerialize()
    {
    }
  }
}
