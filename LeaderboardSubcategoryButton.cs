﻿// Decompiled with JetBrains decompiler
// Type: LeaderboardSubcategoryButton
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LeaderboardSubcategoryButton : LeaderboardToggle
{
  private static UnityEngine.Sprite selectedGfx;

  public static void LoadIcons()
  {
    LeaderboardSubcategoryButton.selectedGfx = Resources.Load<UnityEngine.Sprite>("GUI/Leaderboard/side_menu_sub_selected");
  }

  protected override void OnAwake()
  {
    this.NotSelectedGfx = this.image.sprite;
    this.SelectedGfx = LeaderboardSubcategoryButton.selectedGfx;
  }

  protected override void OnSelectedChanged(bool newState)
  {
    base.OnSelectedChanged(newState);
    this.image.color = !newState ? Color.white : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
    this.label.color = !newState ? Color.white : ColorManager.currentColorScheme.Get(WidgetColorType.FACTION_COLOR);
  }

  public void SetText(string text)
  {
    this.label.text = text.ToUpperInvariant();
  }
}
