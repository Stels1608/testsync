﻿// Decompiled with JetBrains decompiler
// Type: DebugTracer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DebugTracer
{
  private Color[] colors = new Color[7]{ Color.white, Color.red, Color.green, Color.blue, Color.yellow, Color.magenta, Color.cyan };
  private List<DebugTracer.PointTracer> points;
  private int count;

  public DebugTracer()
  {
    this.points = new List<DebugTracer.PointTracer>();
  }

  private void UptateCount()
  {
    this.count = 0;
    using (List<DebugTracer.PointTracer>.Enumerator enumerator = this.points.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DebugTracer.PointTracer current = enumerator.Current;
        if ((int) current.num + 1 > this.count)
          this.count = (int) current.num + 1;
      }
    }
  }

  public int GetCount()
  {
    return this.count;
  }

  public void AddPoint(Vector3 point, byte num, float time)
  {
    this.points.Add(new DebugTracer.PointTracer()
    {
      num = num,
      point = point,
      time = time
    });
    this.count = Math.Max(this.count, (int) num + 1);
  }

  public void AddPoint(Vector3 point, byte num)
  {
    this.AddPoint(point, num, Time.time);
  }

  public Vector3[] GetPath(int num, float timeStart, float timeEnd)
  {
    List<Vector3> vector3List = new List<Vector3>();
    using (List<DebugTracer.PointTracer>.Enumerator enumerator = this.points.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        DebugTracer.PointTracer current = enumerator.Current;
        if ((int) current.num == num)
          vector3List.Add(current.point);
      }
    }
    return vector3List.ToArray();
  }

  public Vector3 GetStartPoint()
  {
    if (this.points.Count > 0)
      return this.points[0].point;
    return Vector3.zero;
  }

  public float GetCreateTime()
  {
    float num = 0.0f;
    if (this.points.Count > 0)
      num = this.points[0].time;
    return num;
  }

  public float GetMaxTime()
  {
    float num = 0.0f;
    if (this.points.Count > 0)
      num = this.points[this.points.Count - 1].time;
    return num;
  }

  public void Draw()
  {
    Debug.Log((object) ("draw " + (object) this.points.Count));
    this.Draw(0.0f, Time.time + 1f);
  }

  public void Draw(float timeStart, float timeEnd)
  {
    for (int num = 0; num < this.GetCount() && num < this.colors.Length; ++num)
    {
      if (num == 0)
        BgoDebug.DrawPathWithNormal(this.GetPath(num, timeStart, timeEnd), Vector3.up, 1f, this.colors[num]);
      else
        BgoDebug.DrawPath(this.GetPath(num, timeStart, timeEnd), this.colors[num]);
    }
  }

  public void Save(string path)
  {
    try
    {
      StreamWriter streamWriter = new StreamWriter(path, false);
      using (List<DebugTracer.PointTracer>.Enumerator enumerator = this.points.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          DebugTracer.PointTracer current = enumerator.Current;
          streamWriter.WriteLine(((int) current.num).ToString() + " " + (object) current.point.x + " " + (object) current.point.y + " " + (object) current.point.z + " " + (object) current.time);
        }
      }
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ex.Message);
    }
  }

  public void Load(string path)
  {
    try
    {
      this.points.Clear();
      StreamReader streamReader = new StreamReader(path);
      while (!streamReader.EndOfStream)
      {
        string[] strArray = streamReader.ReadLine().Split(' ');
        DebugTracer.PointTracer pointTracer;
        pointTracer.num = byte.Parse(strArray[0]);
        pointTracer.point.x = float.Parse(strArray[1]);
        pointTracer.point.y = float.Parse(strArray[2]);
        pointTracer.point.z = float.Parse(strArray[3]);
        pointTracer.time = float.Parse(strArray[4]);
        this.points.Add(pointTracer);
      }
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ex.Message);
    }
    this.UptateCount();
  }

  public void Clear()
  {
    this.points.Clear();
    this.count = 0;
  }

  public void Clear(int num)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.points.RemoveAll(new Predicate<DebugTracer.PointTracer>(new DebugTracer.\u003CClear\u003Ec__AnonStorey62()
    {
      num = num
    }.\u003C\u003Em__34));
    this.UptateCount();
  }

  private struct PointTracer
  {
    public byte num;
    public Vector3 point;
    public float time;
  }
}
