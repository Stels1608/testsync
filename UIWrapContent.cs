﻿// Decompiled with JetBrains decompiler
// Type: UIWrapContent
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Wrap Content")]
public class UIWrapContent : MonoBehaviour
{
  public int itemSize = 100;
  public bool cullContent = true;
  private bool mFirstTime = true;
  private List<Transform> mChildren = new List<Transform>();
  public int minIndex;
  public int maxIndex;
  public UIWrapContent.OnInitializeItem onInitializeItem;
  private Transform mTrans;
  private UIPanel mPanel;
  private UIScrollView mScroll;
  private bool mHorizontal;

  protected virtual void Start()
  {
    this.SortBasedOnScrollMovement();
    this.WrapContent();
    if ((UnityEngine.Object) this.mScroll != (UnityEngine.Object) null)
      this.mScroll.GetComponent<UIPanel>().onClipMove = new UIPanel.OnClippingMoved(this.OnMove);
    this.mFirstTime = false;
  }

  protected virtual void OnMove(UIPanel panel)
  {
    this.WrapContent();
  }

  [ContextMenu("Sort Based on Scroll Movement")]
  public void SortBasedOnScrollMovement()
  {
    if (!this.CacheScrollView())
      return;
    this.mChildren.Clear();
    for (int index = 0; index < this.mTrans.childCount; ++index)
      this.mChildren.Add(this.mTrans.GetChild(index));
    if (this.mHorizontal)
      this.mChildren.Sort(new Comparison<Transform>(UIGrid.SortHorizontal));
    else
      this.mChildren.Sort(new Comparison<Transform>(UIGrid.SortVertical));
    this.ResetChildPositions();
  }

  [ContextMenu("Sort Alphabetically")]
  public void SortAlphabetically()
  {
    if (!this.CacheScrollView())
      return;
    this.mChildren.Clear();
    for (int index = 0; index < this.mTrans.childCount; ++index)
      this.mChildren.Add(this.mTrans.GetChild(index));
    this.mChildren.Sort(new Comparison<Transform>(UIGrid.SortByName));
    this.ResetChildPositions();
  }

  protected bool CacheScrollView()
  {
    this.mTrans = this.transform;
    this.mPanel = NGUITools.FindInParents<UIPanel>(this.gameObject);
    this.mScroll = this.mPanel.GetComponent<UIScrollView>();
    if ((UnityEngine.Object) this.mScroll == (UnityEngine.Object) null)
      return false;
    if (this.mScroll.movement == UIScrollView.Movement.Horizontal)
    {
      this.mHorizontal = true;
    }
    else
    {
      if (this.mScroll.movement != UIScrollView.Movement.Vertical)
        return false;
      this.mHorizontal = false;
    }
    return true;
  }

  private void ResetChildPositions()
  {
    int index = 0;
    for (int count = this.mChildren.Count; index < count; ++index)
      this.mChildren[index].localPosition = !this.mHorizontal ? new Vector3(0.0f, (float) (-index * this.itemSize), 0.0f) : new Vector3((float) (index * this.itemSize), 0.0f, 0.0f);
  }

  public void WrapContent()
  {
    float num1 = (float) (this.itemSize * this.mChildren.Count) * 0.5f;
    Vector3[] worldCorners = this.mPanel.worldCorners;
    for (int index = 0; index < 4; ++index)
    {
      Vector3 vector3 = this.mTrans.InverseTransformPoint(worldCorners[index]);
      worldCorners[index] = vector3;
    }
    Vector3 vector3_1 = Vector3.Lerp(worldCorners[0], worldCorners[2], 0.5f);
    bool flag = true;
    float num2 = num1 * 2f;
    if (this.mHorizontal)
    {
      float num3 = worldCorners[0].x - (float) this.itemSize;
      float num4 = worldCorners[2].x + (float) this.itemSize;
      int index = 0;
      for (int count = this.mChildren.Count; index < count; ++index)
      {
        Transform transform = this.mChildren[index];
        float num5 = transform.localPosition.x - vector3_1.x;
        if ((double) num5 < -(double) num1)
        {
          Vector3 localPosition = transform.localPosition;
          localPosition.x += num2;
          num5 = localPosition.x - vector3_1.x;
          int @int = Mathf.RoundToInt(localPosition.x / (float) this.itemSize);
          if (this.minIndex == this.maxIndex || this.minIndex <= @int && @int <= this.maxIndex)
          {
            transform.localPosition = localPosition;
            this.UpdateItem(transform, index);
            transform.name = @int.ToString();
          }
          else
            flag = false;
        }
        else if ((double) num5 > (double) num1)
        {
          Vector3 localPosition = transform.localPosition;
          localPosition.x -= num2;
          num5 = localPosition.x - vector3_1.x;
          int @int = Mathf.RoundToInt(localPosition.x / (float) this.itemSize);
          if (this.minIndex == this.maxIndex || this.minIndex <= @int && @int <= this.maxIndex)
          {
            transform.localPosition = localPosition;
            this.UpdateItem(transform, index);
            transform.name = @int.ToString();
          }
          else
            flag = false;
        }
        else if (this.mFirstTime)
          this.UpdateItem(transform, index);
        if (this.cullContent)
        {
          float num6 = num5 + (this.mPanel.clipOffset.x - this.mTrans.localPosition.x);
          if (!UICamera.IsPressed(transform.gameObject))
            NGUITools.SetActive(transform.gameObject, (double) num6 > (double) num3 && (double) num6 < (double) num4, false);
        }
      }
    }
    else
    {
      float num3 = worldCorners[0].y - (float) this.itemSize;
      float num4 = worldCorners[2].y + (float) this.itemSize;
      int index = 0;
      for (int count = this.mChildren.Count; index < count; ++index)
      {
        Transform transform = this.mChildren[index];
        float num5 = transform.localPosition.y - vector3_1.y;
        if ((double) num5 < -(double) num1)
        {
          Vector3 localPosition = transform.localPosition;
          localPosition.y += num2;
          num5 = localPosition.y - vector3_1.y;
          int @int = Mathf.RoundToInt(localPosition.y / (float) this.itemSize);
          if (this.minIndex == this.maxIndex || this.minIndex <= @int && @int <= this.maxIndex)
          {
            transform.localPosition = localPosition;
            this.UpdateItem(transform, index);
            transform.name = @int.ToString();
          }
          else
            flag = false;
        }
        else if ((double) num5 > (double) num1)
        {
          Vector3 localPosition = transform.localPosition;
          localPosition.y -= num2;
          num5 = localPosition.y - vector3_1.y;
          int @int = Mathf.RoundToInt(localPosition.y / (float) this.itemSize);
          if (this.minIndex == this.maxIndex || this.minIndex <= @int && @int <= this.maxIndex)
          {
            transform.localPosition = localPosition;
            this.UpdateItem(transform, index);
            transform.name = @int.ToString();
          }
          else
            flag = false;
        }
        else if (this.mFirstTime)
          this.UpdateItem(transform, index);
        if (this.cullContent)
        {
          float num6 = num5 + (this.mPanel.clipOffset.y - this.mTrans.localPosition.y);
          if (!UICamera.IsPressed(transform.gameObject))
            NGUITools.SetActive(transform.gameObject, (double) num6 > (double) num3 && (double) num6 < (double) num4, false);
        }
      }
    }
    this.mScroll.restrictWithinPanel = !flag;
  }

  private void OnValidate()
  {
    if (this.maxIndex < this.minIndex)
      this.maxIndex = this.minIndex;
    if (this.minIndex <= this.maxIndex)
      return;
    this.maxIndex = this.minIndex;
  }

  protected virtual void UpdateItem(Transform item, int index)
  {
    if (this.onInitializeItem == null)
      return;
    int realIndex = this.mScroll.movement != UIScrollView.Movement.Vertical ? Mathf.RoundToInt(item.localPosition.x / (float) this.itemSize) : Mathf.RoundToInt(item.localPosition.y / (float) this.itemSize);
    this.onInitializeItem(item.gameObject, index, realIndex);
  }

  public delegate void OnInitializeItem(GameObject go, int wrapIndex, int realIndex);
}
