﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class HudIndicatorInfo
{
  public static float MiniModeRangeOption = 0.5f;
  public static float TopTextRenderRangeOption = 0.5f;

  public static float MiniModeRangeThreshold
  {
    get
    {
      return Game.Me.Stats.DetectionOuterRadius * HudIndicatorInfo.MiniModeRangeOption;
    }
  }

  public static float TopTextRenderRangeThreshold
  {
    get
    {
      return Game.Me.Stats.DetectionOuterRadius * HudIndicatorInfo.TopTextRenderRangeOption;
    }
  }

  public static bool IsAlwaysInMiniMode(ISpaceEntity target)
  {
    return target.SpaceEntityType == SpaceEntityType.SectorEvent;
  }

  public static bool IsNeverInMiniMode(ISpaceEntity entity)
  {
    return entity.IsFriendlyHub && entity.SpaceEntityType != SpaceEntityType.Outpost || HudIndicatorInfo.IsSmallProjectile(entity);
  }

  public static bool IsSmallProjectile(ISpaceEntity entity)
  {
    switch (entity.SpaceEntityType)
    {
      case SpaceEntityType.Missile:
      case SpaceEntityType.Mine:
      case SpaceEntityType.JumpTargetTransponder:
        return true;
      default:
        return false;
    }
  }

  public static bool CanBeDamaged(ISpaceEntity entity)
  {
    switch (entity.SpaceEntityType)
    {
      case SpaceEntityType.SpaceLocationMarker:
      case SpaceEntityType.AsteroidGroup:
      case SpaceEntityType.SectorMap3DFocusPoint:
      case SpaceEntityType.Debris:
      case SpaceEntityType.Container:
      case SpaceEntityType.Trigger:
      case SpaceEntityType.Planet:
      case SpaceEntityType.Planetoid:
      case SpaceEntityType.DotArea:
      case SpaceEntityType.SectorEvent:
      case SpaceEntityType.Comet:
        return false;
      default:
        return true;
    }
  }

  public static bool HasStaticIndicator(ISpaceEntity target)
  {
    if (!target.ShowIndicatorWhenInRange || target.IsMe)
      return false;
    switch (target.SpaceEntityType)
    {
      case SpaceEntityType.AsteroidGroup:
      case SpaceEntityType.Asteroid:
      case SpaceEntityType.Container:
      case SpaceEntityType.AsteroidBot:
      case SpaceEntityType.Planetoid:
      case SpaceEntityType.DotArea:
      case SpaceEntityType.Comet:
        return false;
      case SpaceEntityType.Player:
      case SpaceEntityType.Cruiser:
      case SpaceEntityType.BotFighter:
      case SpaceEntityType.JumpBeacon:
        if (!target.IsMe)
          return (target as SpaceObject).WorldCard.Targetable;
        return false;
      case SpaceEntityType.Missile:
        return target.PlayerRelation != Relation.Friend;
      case SpaceEntityType.WeaponPlatform:
      case SpaceEntityType.Debris:
      case SpaceEntityType.MiningShip:
      case SpaceEntityType.Outpost:
      case SpaceEntityType.Trigger:
      case SpaceEntityType.Mine:
      case SpaceEntityType.SectorEvent:
      case SpaceEntityType.MineField:
      case SpaceEntityType.JumpTargetTransponder:
        return true;
      case SpaceEntityType.Planet:
        return target.IsFriendlyHub;
      default:
        Debug.LogError((object) ("Please define a bracket behavior for the SpaceEntityType: " + (object) target.SpaceEntityType));
        return true;
    }
  }
}
