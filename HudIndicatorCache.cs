﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorCache
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class HudIndicatorCache
{
  private static readonly HudIndicatorCache instance = new HudIndicatorCache();
  private const int CACHE_SIZE = 200;
  private readonly Stack<HudIndicatorBase> missileIndicatorCache;
  private readonly Stack<HudIndicatorBase> mineIndicatorCache;
  private readonly List<Stack<HudIndicatorBase>> cacheList;

  public static HudIndicatorCache Instance
  {
    get
    {
      return HudIndicatorCache.instance;
    }
  }

  private HudIndicatorCache()
  {
    this.cacheList = new List<Stack<HudIndicatorBase>>();
    this.cacheList.Add(this.missileIndicatorCache = new Stack<HudIndicatorBase>());
    this.cacheList.Add(this.mineIndicatorCache = new Stack<HudIndicatorBase>());
  }

  public void ClearAllIndicatorCaches()
  {
    using (List<Stack<HudIndicatorBase>>.Enumerator enumerator = this.cacheList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Stack<HudIndicatorBase> current = enumerator.Current;
        for (int index = current.Count - 1; index >= 0; --index)
          Object.Destroy((Object) current.Pop().gameObject);
        current.Clear();
      }
    }
  }

  public bool TryCacheIndicator(HudIndicatorBase hudIndicator, ISpaceEntity target)
  {
    Stack<HudIndicatorBase> indicatorCache;
    if (target.TargetType != TargetType.SpaceObject || !this.TryGetIndicatorCache((target as SpaceObject).SpaceEntityType, out indicatorCache))
      return false;
    if (indicatorCache.Count < 200)
    {
      hudIndicator.gameObject.SetActive(false);
      indicatorCache.Push(hudIndicator);
    }
    return true;
  }

  public bool TryPopCachedIndicator(SpaceEntityType entityType, out HudIndicatorBase cachedIndicator)
  {
    Stack<HudIndicatorBase> indicatorCache;
    if (this.TryGetIndicatorCache(entityType, out indicatorCache) && indicatorCache.Count > 0)
    {
      cachedIndicator = indicatorCache.Pop();
      cachedIndicator.ResetStates();
      return true;
    }
    cachedIndicator = (HudIndicatorBase) null;
    return false;
  }

  private bool TryGetIndicatorCache(SpaceEntityType entityType, out Stack<HudIndicatorBase> indicatorCache)
  {
    switch (entityType)
    {
      case SpaceEntityType.Missile:
        indicatorCache = this.missileIndicatorCache;
        return true;
      case SpaceEntityType.Mine:
        indicatorCache = this.mineIndicatorCache;
        return true;
      default:
        indicatorCache = (Stack<HudIndicatorBase>) null;
        return false;
    }
  }
}
