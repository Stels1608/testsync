﻿// Decompiled with JetBrains decompiler
// Type: SpaceLevelProfile
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class SpaceLevelProfile : LevelProfile
{
  private static readonly List<string> requiredSpaceAssets = new List<string>() { "asteroid1_1_lowres.prefab", "HumanT1Fighter_lowres.prefab", "item_container1.prefab", "light_normal_mine.prefab", "AncientLargeMissile.prefab", "sun_1.prefab", "stars.prefab" };
  private TransSceneType transSceneType;
  private readonly uint serverId;
  private readonly uint cardGuid;
  private readonly GameLocation location;

  public uint ServerId
  {
    get
    {
      return this.serverId;
    }
  }

  public string SceneName
  {
    get
    {
      return "SpaceLevel";
    }
  }

  public uint CardGuid
  {
    get
    {
      return this.cardGuid;
    }
  }

  public GameLocation Location
  {
    get
    {
      return this.location;
    }
  }

  public SectorCard Card
  {
    get
    {
      return (SectorCard) Game.Catalogue.FetchCard(this.cardGuid, CardView.Sector);
    }
  }

  public bool Ready
  {
    get
    {
      return (bool) this.Card.IsLoaded;
    }
  }

  public IEnumerable<string> RequiredAssets
  {
    get
    {
      List<string> stringList = new List<string>((IEnumerable<string>) SpaceLevelProfile.requiredSpaceAssets);
      stringList.Add(this.Card.NebulaDesc.prefabName + ".prefab");
      foreach (MovingNebulaDesc movingNebulaDesc in this.Card.MovingNebulaDescs)
        stringList.Add(movingNebulaDesc.modelName + "_" + movingNebulaDesc.matSuffix + ".prefab");
      stringList.Add(this.Card.StarsDesc.prefabName + ".prefab");
      stringList.Add(this.Card.StarsMultDesc.prefabName + ".prefab");
      stringList.Add(this.Card.StarsVarianceDesc.prefabName + ".prefab");
      foreach (string requiredAsset in this.Card.RequiredAssets)
        stringList.Add(requiredAsset.ToLower());
      return (IEnumerable<string>) stringList;
    }
  }

  public TransSceneType TransSceneType
  {
    get
    {
      return this.transSceneType;
    }
    set
    {
      this.transSceneType = value;
    }
  }

  public SpaceLevelProfile(uint serverId, uint cardGuid, GameLocation location)
  {
    this.serverId = serverId;
    this.cardGuid = cardGuid;
    this.location = location;
  }
}
