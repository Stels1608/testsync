﻿// Decompiled with JetBrains decompiler
// Type: ConquestPriceUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class ConquestPriceUpdate : GalaxyMapUpdate
{
  public uint Price { get; private set; }

  public ConquestPriceUpdate(Faction faction, uint price)
    : base(GalaxyUpdateType.ConquestPrice, faction, -1)
  {
    this.Price = price;
  }

  public override string ToString()
  {
    return "[ConquestPriceUpdate] (Faction: " + (object) this.Faction + ", SectorId: " + (object) this.SectorId + ", Price:" + (object) this.Price + ")";
  }
}
