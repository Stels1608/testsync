﻿// Decompiled with JetBrains decompiler
// Type: DelayedActions
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;

public class DelayedActions
{
  private bool isSleeping1 = true;
  private List<DelayedActions.Predicate> actions = new List<DelayedActions.Predicate>();

  public bool isSleeping
  {
    get
    {
      return this.isSleeping1;
    }
    set
    {
      this.isSleeping1 = value;
    }
  }

  [Gui2Editor]
  public static bool Sleeping
  {
    get
    {
      return Game.DelayedActions.isSleeping;
    }
    set
    {
      Game.DelayedActions.isSleeping = value;
    }
  }

  [Gui2Editor]
  public static int Queue
  {
    get
    {
      return Game.DelayedActions.actions.Count;
    }
  }

  public void Add(DelayedActions.Predicate action)
  {
    this.actions.Add(action);
    if (this.isSleeping)
      return;
    this.Execute();
  }

  public void Execute()
  {
    if (this.isSleeping || this.actions.Count == 0)
      return;
    DelayedActions.Predicate predicate = this.actions[0];
    this.actions.Remove(predicate);
    this.isSleeping = true;
    if (predicate())
      return;
    this.isSleeping = false;
    this.Execute();
  }

  public void Awake()
  {
    this.isSleeping = false;
    this.Execute();
  }

  public delegate bool Predicate();
}
