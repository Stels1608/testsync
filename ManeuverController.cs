﻿// Decompiled with JetBrains decompiler
// Type: ManeuverController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class ManeuverController : IMovementController
{
  private Dictionary<Tick, MovementFrame> syncFrames = new Dictionary<Tick, MovementFrame>();
  private readonly List<Maneuver> maneuvers = new List<Maneuver>();
  private readonly Stack<Maneuver> maneuverStack = new Stack<Maneuver>();
  private MovementFrame[] frames;
  private Tick oldTick;
  protected float currentSpeed;
  private Vector3 currentStrafingSpeed;
  private SpaceObject spaceObject;
  private MovementCard card;

  public Gear Gear
  {
    get
    {
      Maneuver lastManeuverAtTick = this.GetLastManeuverAtTick(Tick.Current);
      if (lastManeuverAtTick == null)
        return Gear.Regular;
      return lastManeuverAtTick.GetGear();
    }
  }

  public float MarchSpeed
  {
    get
    {
      Maneuver lastManeuverAtTick = this.GetLastManeuverAtTick(Tick.Current);
      if (lastManeuverAtTick == null)
        return 0.0f;
      return lastManeuverAtTick.GetMarchSpeed();
    }
  }

  public float CurrentSpeed
  {
    get
    {
      return this.currentSpeed;
    }
  }

  public Vector3 CurrentStrafingSpeed
  {
    get
    {
      return this.currentStrafingSpeed;
    }
  }

  public ManeuverController(SpaceObject spaceObject, MovementCard card)
  {
    this.frames = new MovementFrame[5];
    this.oldTick = Tick.Current - 3;
    this.spaceObject = spaceObject;
    this.card = card;
  }

  public void Advance(Tick tick)
  {
    this.Build(tick);
  }

  protected void Build(Tick tick)
  {
    int index = this.TickIndex(tick);
    if (index < 0 || index >= 5)
      return;
    this.BuildFrame(tick, out this.frames[index]);
  }

  private void BuildFrame(Tick tick, out MovementFrame frame)
  {
    frame = MovementFrame.Invalid;
    Stack<Maneuver> maneuvers = this.GetManeuvers(tick);
    if (maneuvers == null)
    {
      frame.valid = false;
    }
    else
    {
      Maneuver maneuver = maneuvers.Pop();
      MovementFrame frame1 = this.GetTickFrame(tick - 1);
      if (!frame1.valid && tick > this.oldTick)
        this.BuildFrame(tick - 1, out frame1);
      frame = maneuver.NextFrame(tick, frame1);
      Vector3 vector3 = frame.position;
      Euler3 euler3 = frame.euler3;
      using (Stack<Maneuver>.Enumerator enumerator = maneuvers.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Maneuver current = enumerator.Current;
          frame = current.NextFrame(tick, frame);
        }
      }
      frame.position = vector3;
      frame.euler3 = euler3;
      MovementFrame movementFrame;
      if (!this.syncFrames.TryGetValue(tick, out movementFrame) || (double) (movementFrame.position - frame.position).sqrMagnitude <= 0.0199999995529652 && (double) Quaternion.Angle(movementFrame.rotation, frame.rotation) <= 0.200000002980232)
        return;
      movementFrame.ActiveThrusterEffects = frame.ActiveThrusterEffects;
      frame = movementFrame;
    }
  }

  private int TickIndex(Tick tick)
  {
    return tick - this.oldTick;
  }

  public void PostAdvance()
  {
    ++this.oldTick;
    for (int index = 0; index < 4; ++index)
      this.frames[index] = this.frames[index + 1];
    this.frames[4].valid = false;
    this.DropOldSyncFrames();
    this.DropOldManeuvers();
  }

  private void DropOldSyncFrames()
  {
    List<Tick> tickList = new List<Tick>();
    using (Dictionary<Tick, MovementFrame>.KeyCollection.Enumerator enumerator = this.syncFrames.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Tick current = enumerator.Current;
        if (current < this.oldTick)
          tickList.Add(current);
      }
    }
    using (List<Tick>.Enumerator enumerator = tickList.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.syncFrames.Remove(enumerator.Current);
    }
  }

  private void DropOldManeuvers()
  {
    int lastIndex = this.maneuvers.FindLastIndex((Predicate<Maneuver>) (maneuver => maneuver.GetStartTick() < this.oldTick));
    if (lastIndex <= 0)
      return;
    this.maneuvers.RemoveRange(0, lastIndex);
  }

  public Maneuver GetLastManeuverAtTick(Tick tick)
  {
    Maneuver maneuver = (Maneuver) null;
    if (this.maneuvers.Count == 0)
      return (Maneuver) null;
    for (int index = 0; index < this.maneuvers.Count; ++index)
    {
      if (this.maneuvers[index].GetStartTick() <= tick)
        maneuver = this.maneuvers[index];
    }
    return maneuver;
  }

  public Stack<Maneuver> GetManeuvers(Tick tick)
  {
    Maneuver lastManeuverAtTick = this.GetLastManeuverAtTick(tick);
    if (lastManeuverAtTick == null)
      return (Stack<Maneuver>) null;
    Tick startTick = lastManeuverAtTick.GetStartTick();
    this.maneuverStack.Clear();
    for (int index = 0; index < this.maneuvers.Count; ++index)
    {
      Maneuver maneuver = this.maneuvers[index];
      if (maneuver.GetStartTick() == startTick)
        this.maneuverStack.Push(maneuver);
    }
    return this.maneuverStack;
  }

  public bool GetFrame(double time, out Vector3 position, out Quaternion rotation, out float speed, out Vector3 strafingSpeed, out MovementFrame frame)
  {
    Tick prevTick = Tick.Previous(time);
    float dt = (float) (time - prevTick.Time);
    return this.GetFrame(prevTick, dt, out position, out rotation, out speed, out strafingSpeed, out frame);
  }

  public bool GetFrameFromOldFrame(double time, out Vector3 position, out Quaternion rotation, out float speed, out Vector3 strafingSpeed, out MovementFrame frame)
  {
    Tick prevTick = Tick.Previous(time - 0.100000001490116);
    float dt = (float) (time - prevTick.Time);
    return this.GetFrame(prevTick, dt, out position, out rotation, out speed, out strafingSpeed, out frame);
  }

  private bool GetFrame(Tick prevTick, float dt, out Vector3 position, out Quaternion rotation, out float speed, out Vector3 strafingSpeed, out MovementFrame frame)
  {
    frame = this.GetTickFrame(prevTick);
    position = frame.GetFuturePosition(dt);
    rotation = frame.GetFutureRotation(dt);
    speed = frame.linearSpeed.magnitude;
    strafingSpeed = frame.strafeSpeed;
    return frame.valid;
  }

  public MovementFrame GetTickFrame(Tick tick)
  {
    int index = this.TickIndex(tick);
    if (index >= 0 && index < 5)
      return this.frames[index];
    return MovementFrame.Invalid;
  }

  public MovementFrame GetNextTickFrame()
  {
    return this.GetTickFrame(Tick.Next);
  }

  private bool NewManeuverEliminatesExistingManeuver(Maneuver existingManeuver, Maneuver newManeuver)
  {
    bool flag = existingManeuver.GetStartTick() >= newManeuver.GetStartTick();
    if (!flag)
      return false;
    if (flag)
    {
      switch ((int) existingManeuver.IsExclusive + (int) newManeuver.IsExclusive)
      {
        case 0:
          return existingManeuver.GetType() == newManeuver.GetType();
        case 1:
          return false;
        case 2:
          return true;
      }
    }
    Debug.LogError((object) "We should never get here.");
    return true;
  }

  public void AddManeuver(Maneuver newManeuver)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    ManeuverController.\u003CAddManeuver\u003Ec__AnonStoreyDD maneuverCAnonStoreyDd = new ManeuverController.\u003CAddManeuver\u003Ec__AnonStoreyDD();
    // ISSUE: reference to a compiler-generated field
    maneuverCAnonStoreyDd.newManeuver = newManeuver;
    // ISSUE: reference to a compiler-generated field
    maneuverCAnonStoreyDd.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    maneuverCAnonStoreyDd.newManeuver.Card = this.card;
    // ISSUE: reference to a compiler-generated method
    this.maneuvers.RemoveAll(new Predicate<Maneuver>(maneuverCAnonStoreyDd.\u003C\u003Em__227));
    // ISSUE: reference to a compiler-generated field
    this.maneuvers.Add(maneuverCAnonStoreyDd.newManeuver);
    this.maneuvers.Sort();
  }

  public void AddSyncFrame(Tick tick, MovementFrame frame)
  {
    int index = this.TickIndex(tick);
    if (index >= 0 && index < 5)
      this.frames[index] = frame;
    if (index < 4)
      return;
    this.syncFrames[tick] = frame;
  }

  public override string ToString()
  {
    string str = string.Empty;
    using (List<Maneuver>.Enumerator enumerator = this.maneuvers.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Maneuver current = enumerator.Current;
        str += current.ToString();
      }
    }
    return string.Format("MovementController {0}", (object) str);
  }

  public virtual bool Move(double time)
  {
    Vector3 position;
    Quaternion rotation;
    MovementFrame frame;
    if (!this.GetFrame(time, out position, out rotation, out this.currentSpeed, out this.currentStrafingSpeed, out frame))
    {
      if (!this.GetFrameFromOldFrame(time, out position, out rotation, out this.currentSpeed, out this.currentStrafingSpeed, out frame))
        return false;
      this.spaceObject.Position = position;
      this.spaceObject.Rotation = rotation;
      return false;
    }
    if (frame.valid)
      this.spaceObject.UpdateThrusters(frame.ActiveThrusterEffects);
    this.spaceObject.Position = position;
    this.spaceObject.Rotation = rotation;
    return true;
  }

  private enum FrameIndex
  {
    Old3,
    Old2,
    Old1,
    Current,
    Next,
    Count,
  }
}
