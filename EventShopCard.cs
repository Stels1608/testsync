﻿// Decompiled with JetBrains decompiler
// Type: EventShopCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class EventShopCard : Card
{
  public string ShopNameCylon { get; private set; }

  public string ShopNameColonial { get; private set; }

  public string ShopDescriptionCylon { get; private set; }

  public string ShopDescriptionColonial { get; private set; }

  public string ShopErrorMissingResources { get; private set; }

  public string ShopErrorCannotBuy { get; private set; }

  public string ShopName
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return this.ShopNameColonial;
      return this.ShopNameCylon;
    }
  }

  public string ShopDescription
  {
    get
    {
      if (Game.Me.Faction == Faction.Colonial)
        return this.ShopDescriptionColonial;
      return this.ShopDescriptionCylon;
    }
  }

  public List<uint> EventResources { get; private set; }

  public EventShopCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.ShopNameCylon = r.ReadString();
    this.ShopNameColonial = r.ReadString();
    this.ShopDescriptionCylon = r.ReadString();
    this.ShopDescriptionColonial = r.ReadString();
    this.ShopErrorMissingResources = r.ReadString();
    this.ShopErrorCannotBuy = r.ReadString();
    this.EventResources = r.ReadUInt32List();
    using (List<uint>.Enumerator enumerator = this.EventResources.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.IsLoaded.Depend(new ILoadable[1]
        {
          (ILoadable) Game.Catalogue.FetchCard(enumerator.Current, CardView.GUI)
        });
    }
    this.IsLoaded.Set();
  }
}
