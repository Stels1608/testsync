﻿// Decompiled with JetBrains decompiler
// Type: SpaceEntityType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum SpaceEntityType : uint
{
  Player = 16777216,
  Missile = 33554432,
  WeaponPlatform = 50331648,
  Cruiser = 67108864,
  BotFighter = 83886080,
  Debris = 100663296,
  Asteroid = 117440512,
  Container = 134217728,
  MiningShip = 150994944,
  Outpost = 167772160,
  AsteroidBot = 184549376,
  Trigger = 201326592,
  Planet = 218103808,
  Planetoid = 234881024,
  Mine = 251658240,
  DotArea = 268435456,
  JumpBeacon = 285212672,
  SectorEvent = 301989888,
  MineField = 318767104,
  JumpTargetTransponder = 335544320,
  Comet = 352321536,
  SmartMine = 369098752,
  TypeMask = 520093696,
  SpaceLocationMarker = 4026531840,
  AsteroidGroup = 4043309056,
  SectorMap3DFocusPoint = 4060086272,
}
