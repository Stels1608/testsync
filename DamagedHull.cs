﻿// Decompiled with JetBrains decompiler
// Type: DamagedHull
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DamagedHull : SingleLODListener
{
  public bool Rusty;
  public float HullPoints;
  public float MaxHullPoints;
  private List<Material> _rustMaterials;
  private List<Color> _rustColors;
  private bool _appliedRusty;
  private float _appliedHullPoints;
  private float _appliedMaxHullPoints;

  public List<Material> DamageMaterials { get; set; }

  protected override void Awake()
  {
    base.Awake();
    this.DamageMaterials = new List<Material>();
    this._rustMaterials = new List<Material>();
    this._rustColors = new List<Color>();
    Material[] array = ((IEnumerable<Renderer>) this.gameObject.GetComponentsInChildren<Renderer>()).SelectMany<Renderer, Material>((Func<Renderer, IEnumerable<Material>>) (r => (IEnumerable<Material>) r.materials)).ToArray<Material>();
    for (int index = 0; index < array.Length; ++index)
    {
      if (array[index].HasProperty("_Damage"))
        this.DamageMaterials.Add(array[index]);
      if (array[index].HasProperty("_RustColor"))
      {
        this._rustMaterials.Add(array[index]);
        this._rustColors.Add(array[index].GetColor("_RustColor"));
      }
    }
  }

  private void Start()
  {
    this.ApplyRust();
    this.ApplyDamage();
  }

  private void ApplyRust()
  {
    Color color = Color.clear;
    for (int index = 0; index < this._rustMaterials.Count; ++index)
    {
      if (this.Rusty)
        color = this._rustColors[index];
      this._rustMaterials[index].SetColor("_RustColor", color);
    }
    this._appliedRusty = this.Rusty;
  }

  private void ApplyDamage()
  {
    if ((double) Math.Abs(this.MaxHullPoints) > 0.00999999977648258)
    {
      float num1 = (float) (1.0 - (double) this.HullPoints / (double) this.MaxHullPoints);
      float num2 = num1 * num1;
      for (int index = 0; index != this.DamageMaterials.Count; ++index)
        this.DamageMaterials[index].SetFloat("_Damage", num2);
    }
    this._appliedHullPoints = this.HullPoints;
    this._appliedMaxHullPoints = this.MaxHullPoints;
  }

  protected override void OnSwitched(bool value)
  {
    this.enabled = value;
  }

  private void Update()
  {
    Ship ship = this.SpaceObject as Ship;
    if (ship != null)
    {
      this.HullPoints = ship.Props.HullPoints;
      this.MaxHullPoints = ship.Props.MaxHullPoints;
    }
    if (this.Rusty != this._appliedRusty)
      this.ApplyRust();
    float num = this.MaxHullPoints / 10f;
    if ((double) Mathf.Abs(this.HullPoints - this._appliedHullPoints) <= (double) num && (double) Math.Abs(this.MaxHullPoints - this._appliedMaxHullPoints) <= (double) num)
      return;
    this.ApplyDamage();
  }
}
