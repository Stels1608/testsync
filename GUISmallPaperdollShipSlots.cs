﻿// Decompiled with JetBrains decompiler
// Type: GUISmallPaperdollShipSlots
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui.Tooltips;

public class GUISmallPaperdollShipSlots
{
  public bool IsValid = true;
  public string countString = string.Empty;
  public GUIButtonNew button;
  public GUIImageNew image;
  public GUILabelNew label;
  public GUIBlinker blinker;
  public uint count;
  public ushort slotID;
  public Action action;

  public ShipAbility Ability
  {
    get
    {
      ShipSlot slot = Game.Me.ActiveShip.GetSlot(this.slotID);
      if (slot == null)
        return (ShipAbility) null;
      return slot.Ability;
    }
  }

  public bool IsBlinking
  {
    get
    {
      return this.blinker.IsRendered;
    }
    set
    {
      if (value)
      {
        if (this.Ability.card.Launch == ShipAbilityLaunch.Manual)
          this.blinker.SetInfoBox((GuiAdvancedTooltipBase) new GuiAdvancedHotkeyTooltip("%$bgo.custom_tooltip.fire_missile%", this.action));
        else
          this.blinker.SetInfoBox((GuiAdvancedTooltipBase) new GuiAdvancedHotkeyTooltip("%$bgo.custom_tooltip.toggle_gun%", this.action));
      }
      this.blinker.IsRendered = value;
    }
  }
}
