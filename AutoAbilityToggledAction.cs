﻿// Decompiled with JetBrains decompiler
// Type: AutoAbilityToggledAction
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Controller;
using Bigpoint.Core.Mvc.Messaging;

public class AutoAbilityToggledAction : Action<Message>
{
  private TargetSelectionDataProvider selectionDataProvider;

  public override void Execute(IMessage<Message> message)
  {
    this.selectionDataProvider = (TargetSelectionDataProvider) this.OwnerFacade.FetchDataProvider("TargetSelectionProvider");
    this.HandleTargetChange((ShipAbility) message.Data);
  }

  public void HandleTargetChange(ShipAbility touchedAbility)
  {
    ISpaceEntity abilityTarget = this.selectionDataProvider.GetAbilityTarget(touchedAbility);
    ISpaceEntity newTarget = (ISpaceEntity) touchedAbility.Target;
    if (abilityTarget == newTarget)
      return;
    this.selectionDataProvider.SetAbilityTarget(touchedAbility, (ISpaceEntity) touchedAbility.Target);
    FacadeFactory.GetInstance().SendMessage(Message.AutoAbilityTargetChanged, (object) new AbilityTargetChangedData(touchedAbility, abilityTarget, newTarget));
  }
}
