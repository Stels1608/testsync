﻿// Decompiled with JetBrains decompiler
// Type: StaticCards
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class StaticCards
{
  private static StaticCards instance = new StaticCards();

  public static StaticCards Instance
  {
    get
    {
      return StaticCards.instance;
    }
  }

  public ShipListCard ShipListCard
  {
    get
    {
      return this.GetShipList(Game.Me.Faction);
    }
  }

  public ShipListCard Colonial
  {
    get
    {
      return (ShipListCard) Game.Catalogue.FetchCard(73551268U, CardView.ShipList);
    }
  }

  public ShipListCard Cylon
  {
    get
    {
      return (ShipListCard) Game.Catalogue.FetchCard(188756164U, CardView.ShipList);
    }
  }

  public StickerListCard Stickers
  {
    get
    {
      return (StickerListCard) Game.Catalogue.FetchCard(166885587U, CardView.StickerList);
    }
  }

  public static AvatarCatalogueCard Avatar
  {
    get
    {
      return (AvatarCatalogueCard) Game.Catalogue.FetchCard(109873795U, CardView.AvatarCatalogue);
    }
  }

  public static GalaxyMapCard GalaxyMap
  {
    get
    {
      return (GalaxyMapCard) Game.Catalogue.FetchCard(150576033U, CardView.GalaxyMap);
    }
  }

  public static GlobalCard GlobalCard
  {
    get
    {
      return (GlobalCard) Game.Catalogue.FetchCard(49842157U, CardView.Global);
    }
  }

  public ShipListCard GetShipList(Faction faction)
  {
    if (faction == Faction.Colonial)
      return this.Colonial;
    return this.Cylon;
  }
}
