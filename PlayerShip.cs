﻿// Decompiled with JetBrains decompiler
// Type: PlayerShip
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShip : Ship
{
  private readonly Queue<JumpEffectData> jumpEffectQueue = new Queue<JumpEffectData>();
  private readonly Dictionary<Material, Shader> savedShaders = new Dictionary<Material, Shader>();
  private const string IMPACT_SOUND_RESOURCE = "Sound/ImpactSoundSet";
  private const string INVISIBLE_SHADER = "Artplant/Smoke";
  private Player player;
  private CameraCard cameraCard;
  public bool IsVisible;
  public Flag HasSpawnedInSector;
  private Vector3 shipBounds;
  private Transform thrustersRoot;
  private SingleLODListener shipSkinScript;
  private bool usingGhostShader;
  private bool jumpEffectInProgress;

  public bool SkinLoaded
  {
    get
    {
      ShipSkinSubstance shipSkinSubstance = this.shipSkinScript as ShipSkinSubstance;
      if ((Object) shipSkinSubstance != (Object) null)
        return shipSkinSubstance.Loaded;
      ShipSkin shipSkin = this.shipSkinScript as ShipSkin;
      if ((Object) shipSkin != (Object) null)
        return shipSkin.Loaded;
      return true;
    }
  }

  public bool IsStrafing
  {
    get
    {
      return (double) this.MovementController.CurrentStrafingSpeed.sqrMagnitude > 0.00999999977648258;
    }
  }

  public Player Player
  {
    get
    {
      return this.player;
    }
  }

  public CameraCard CameraCard
  {
    get
    {
      return this.cameraCard;
    }
  }

  public override bool IsPlayer
  {
    get
    {
      return true;
    }
  }

  public override bool IsMe
  {
    get
    {
      return this.Player.IsMe;
    }
  }

  public override bool IsInMapRange
  {
    get
    {
      if (base.IsInMapRange)
        return !this.Player.IsAnchored;
      return false;
    }
  }

  public override string Name
  {
    get
    {
      if (Game.Me.TournamentParticipant)
        return BsgoLocalization.Get("bgo.common.enemy");
      return this.player.GetTaggedName(this.player.Name);
    }
  }

  public override byte Level
  {
    get
    {
      return this.player.Level;
    }
  }

  public override bool SpawnedInSector
  {
    get
    {
      return (bool) this.HasSpawnedInSector;
    }
  }

  public Vector3 ShipBounds
  {
    get
    {
      return this.shipBounds;
    }
  }

  protected override sbyte IconIndex
  {
    get
    {
      if (this.IsHostileCongener)
        return (sbyte) ((int) base.IconIndex + (this.Faction != Faction.Colonial ? -1 : 1));
      return base.IconIndex;
    }
  }

  public override UnityEngine.Sprite Icon3dMap
  {
    get
    {
      if ((int) this.IconIndex < 0)
        return (UnityEngine.Sprite) null;
      return SystemMap3DMapIconLinker.Instance.MapObjectsPlayers[(int) this.IconIndex];
    }
  }

  public override UnityEngine.Sprite IconBrackets
  {
    get
    {
      if ((int) this.IconIndex < 0)
        return (UnityEngine.Sprite) null;
      return SystemMap3DMapIconLinker.Instance.MapObjectsPlayersBrackets[(int) this.IconIndex];
    }
  }

  public PlayerShip(uint objectID)
    : base(objectID)
  {
    this.HasSpawnedInSector = new Flag();
    this.IsReadyForJumpIn.Depend(new ILoadable[1]
    {
      (ILoadable) this.HasSpawnedInSector
    });
    this.AutoSubscribe = true;
  }

  private void SetPlayer(uint playerID)
  {
    this.player = Game.Players.GetPlayer(playerID);
    this.player.SetPlayerShip(this);
    if (this.player.IsMe)
      this.Props = (SpaceSubscribeInfo) Game.Me.Stats;
    else
      this.Props = this.player.Stats;
  }

  public override void Subscribe()
  {
    this.IsSubscribed = true;
    this.Player.Request(Player.InfoType.SpaceRequest);
    if (Game.Me.Party.IsMember(this.player))
    {
      this.Player.Subscribe(Player.InfoType.Wing | Player.InfoType.Title | Player.InfoType.Medal | Player.InfoType.TournamentIndicator);
    }
    else
    {
      this.Player.Subscribe(Player.InfoType.SpaceSubscription);
      SubscribeProtocol.GetProtocol().SubscribeStats(this.Player.ServerID);
    }
  }

  public override void UnSubscribe()
  {
    this.IsSubscribed = false;
    if (Game.Me.Party.IsMember(this.player))
    {
      this.Player.Unsubscribe(Player.InfoType.Wing | Player.InfoType.Title | Player.InfoType.Medal | Player.InfoType.TournamentIndicator);
    }
    else
    {
      SubscribeProtocol.GetProtocol().UnsubscribeStats(this.Player.ServerID);
      this.Player.Unsubscribe(Player.InfoType.SpaceSubscription);
    }
  }

  public void ChangeVisibility(bool isVisible, ChangeVisibilityReason reason)
  {
    this.IsVisible = isVisible;
    bool flag1 = reason == ChangeVisibilityReason.Default;
    this.SetModelVisibility(isVisible);
    if (flag1 && !isVisible)
      this.ApplyGhostShader();
    else
      this.RemoveGhostShader();
    if (reason == ChangeVisibilityReason.Jump)
    {
      if (isVisible)
        this.QueueJump(new JumpEffectData(JumpType.JumpIn, (AnonymousDelegate) (() => this.SetModelVisibility(true))));
      else
        this.QueueJump(new JumpEffectData(JumpType.JumpOut, (AnonymousDelegate) (() => this.SetModelVisibility(false))));
    }
    else if (reason == ChangeVisibilityReason.Death && !isVisible)
      Ship.ShipExplosion((Ship) this);
    bool flag2 = this.IsMe && flag1 && !Game.Me.Anchored && !SpaceLevel.GetLevel().IsStory;
    if (!isVisible && flag2)
      GuiInvisibilityCountdown.Show();
    else if (isVisible)
    {
      GuiInvisibilityCountdown invisibilityCountdown = Game.GUIManager.Find<GuiInvisibilityCountdown>();
      if (invisibilityCountdown != null)
        invisibilityCountdown.Stop();
    }
    if (isVisible)
      this.HasSpawnedInSector.Set();
    FacadeFactory.GetInstance().SendMessage(Message.PlayerShipSpawnStateChanged, (object) new KeyValuePair<PlayerShip, bool>(this, isVisible));
  }

  private void QueueJump(JumpEffectData jumpEffect)
  {
    this.jumpEffectQueue.Enqueue(jumpEffect);
    this.ProcessJumpEffects();
  }

  private void ProcessJumpEffects()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    PlayerShip.\u003CProcessJumpEffects\u003Ec__AnonStorey111 effectsCAnonStorey111 = new PlayerShip.\u003CProcessJumpEffects\u003Ec__AnonStorey111();
    // ISSUE: reference to a compiler-generated field
    effectsCAnonStorey111.\u003C\u003Ef__this = this;
    if (this.jumpEffectInProgress || this.jumpEffectQueue.Count == 0)
      return;
    // ISSUE: reference to a compiler-generated field
    effectsCAnonStorey111.jumpEffectData = this.jumpEffectQueue.Dequeue();
    this.jumpEffectInProgress = true;
    // ISSUE: reference to a compiler-generated field
    switch (effectsCAnonStorey111.jumpEffectData.JumpType)
    {
      case JumpType.JumpIn:
        // ISSUE: reference to a compiler-generated method
        this.PlayJumpInEffect(new AnonymousDelegate(effectsCAnonStorey111.\u003C\u003Em__284));
        break;
      case JumpType.JumpOut:
        // ISSUE: reference to a compiler-generated method
        this.PlayJumpOutEffect(new AnonymousDelegate(effectsCAnonStorey111.\u003C\u003Em__285));
        break;
    }
  }

  private void OnJumpEffectFinished(JumpEffectData jumpEffectData)
  {
    if (jumpEffectData.OnJumpCompleted != null)
      jumpEffectData.OnJumpCompleted();
    this.jumpEffectInProgress = false;
    this.ProcessJumpEffects();
  }

  private void ApplyGhostShader()
  {
    if (this.CreatingCause != CreatingCause.AlreadyExists || Game.GraphicsQuality < GraphicsQuality.Medium || this.Player.IsAnchored)
      return;
    this.usingGhostShader = true;
    this.Model.SetActive(true);
    this.savedShaders.Clear();
    foreach (Renderer componentsInChild in this.Model.GetComponentsInChildren<MeshRenderer>())
    {
      foreach (Material material in componentsInChild.materials)
      {
        this.savedShaders.Add(material, material.shader);
        material.shader = Shader.Find("Artplant/Smoke");
      }
    }
  }

  private void RemoveGhostShader()
  {
    if (!this.usingGhostShader || this.savedShaders.Count == 0)
      return;
    foreach (Renderer componentsInChild in this.Model.GetComponentsInChildren<MeshRenderer>())
    {
      foreach (Material material in componentsInChild.materials)
      {
        Shader shader;
        this.savedShaders.TryGetValue(material, out shader);
        if ((Object) shader != (Object) null)
          material.shader = shader;
      }
    }
    this.savedShaders.Clear();
  }

  protected override void SetModel(GameObject prototype)
  {
    base.SetModel(prototype);
    if (this.IsMe)
      this.shipSkinScript = (SingleLODListener) this.Model.GetComponentInChildren<ShipSkinSubstance>() ?? (SingleLODListener) this.Model.GetComponentInChildren<ShipSkin>();
    if (this.player.IsAnchored || !(bool) this.HasSpawnedInSector && (this.CreatingCause == CreatingCause.JumpIn || Game.GraphicsQuality < GraphicsQuality.Medium))
      this.Model.SetActive(false);
    else
      this.Model.SetActive(true);
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.SetPlayer(r.ReadUInt32());
    this.player.Roles |= (BgoAdminRoles) r.ReadUInt32();
    if (r.ReadBoolean())
      this.ChangeVisibility(true, ChangeVisibilityReason.Default);
    if (!this.IsMe)
      this.Subscribe();
    this.shipBounds = new Vector3(this.WorldCard.Radius * 2f, this.WorldCard.Radius * 2f, this.WorldCard.Radius * 2f);
  }

  public override void OnRead()
  {
    base.OnRead();
    if (this.IsMe)
    {
      SpaceLevel.GetLevel().SetPlayerShip(this);
      SpaceLevel.GetLevel().SetActualPlayerShip(this);
      SpaceLevel.GetLevel().ShipControls.Enabled = true;
      GUISlotManager guiSlotManager = Game.GUIManager.Find<GUISlotManager>();
      if (guiSlotManager != null)
        guiSlotManager.player.SetName(this.player.GetTaggedName(Game.Me.Name));
      this.player.Name = Game.Me.Name;
      FacadeFactory.GetInstance().SendMessage(Message.MyShipCreated);
    }
    this.ShipCardLight.IsLoaded.AddHandler((SignalHandler) (() =>
    {
      if (!this.IsMe && this.ShipCardLight.ShipRoleDeprecated != ShipRoleDeprecated.Carrier)
        return;
      this.cameraCard = (CameraCard) Game.Catalogue.FetchCard(this.objectGUID, CardView.Camera);
      this.cameraCard.IsLoaded.AddHandler((SignalHandler) (() =>
      {
        if (!this.IsMe)
          return;
        SpaceLevel.GetLevel().SpaceCamera.SetCameraCard(this.cameraCard);
      }));
    }));
  }

  protected override IMovementController CreateMovementController()
  {
    return (IMovementController) new PlayerManeuverController((SpaceObject) this, (MovementCard) Game.Catalogue.FetchCard(this.WorldCard.CardGUID, CardView.Movement));
  }

  public override void InitModelScripts()
  {
    if (this.IsMe)
    {
      this.AddModelScript<FiringArc>();
      ImpactSoundPlayer impactSoundPlayer = this.AddModelScript<ImpactSoundPlayer>();
      impactSoundPlayer.Limit = 1000f;
      impactSoundPlayer.SoundSetResource = "Sound/ImpactSoundSet";
      impactSoundPlayer.AudioPoolCapacity = 3;
    }
    base.InitModelScripts();
  }

  public override string ToString()
  {
    return "[PlayerShip] PlayerName: " + (this.player == null ? "<null>" : this.player.Name) + " Tier: " + (object) this.ShipCardLight.Tier + " Role: " + (object) this.ShipCardLight.ShipRoleDeprecated;
  }
}
