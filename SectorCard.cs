﻿// Decompiled with JetBrains decompiler
// Type: SectorCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Loaders;
using UnityEngine;

public class SectorCard : Card
{
  public JGlobalFog GlobalFogDesc = new JGlobalFog();
  public GUICard GUICard;
  public float Width;
  public float Height;
  public float Length;
  public RegulationCard regulationCard;
  public Color AmbientColor;
  public Color FogColor;
  public int FogDensity;
  public Color DustColor;
  public int DustDensity;
  public BackgroundDesc NebulaDesc;
  public BackgroundDesc StarsDesc;
  public BackgroundDesc StarsMultDesc;
  public BackgroundDesc StarsVarianceDesc;
  public MovingNebulaDesc[] MovingNebulaDescs;
  public LightDesc[] LightDescs;
  public SunDesc[] SunDescs;
  public string[] RequiredAssets;

  public SectorCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.Width = r.ReadSingle();
    this.Height = r.ReadSingle();
    this.Length = r.ReadSingle();
    uint cardGUID = r.ReadGUID();
    this.AmbientColor = r.ReadColor();
    this.FogColor = r.ReadColor();
    this.FogDensity = r.ReadInt32();
    this.DustColor = r.ReadColor();
    this.DustDensity = r.ReadInt32();
    this.NebulaDesc = r.ReadDesc<BackgroundDesc>();
    this.StarsDesc = r.ReadDesc<BackgroundDesc>();
    this.StarsMultDesc = r.ReadDesc<BackgroundDesc>();
    this.StarsVarianceDesc = r.ReadDesc<BackgroundDesc>();
    int length1 = (int) r.ReadUInt16();
    this.MovingNebulaDescs = new MovingNebulaDesc[length1];
    for (int index = 0; index < length1; ++index)
      this.MovingNebulaDescs[index] = r.ReadDesc<MovingNebulaDesc>();
    int length2 = (int) r.ReadUInt16();
    this.LightDescs = new LightDesc[length2];
    for (int index = 0; index < length2; ++index)
      this.LightDescs[index] = r.ReadDesc<LightDesc>();
    int length3 = (int) r.ReadUInt16();
    this.SunDescs = new SunDesc[length3];
    for (int index = 0; index < length3; ++index)
      this.SunDescs[index] = r.ReadDesc<SunDesc>();
    this.GlobalFogDesc.Read(r);
    int length4 = (int) r.ReadUInt16();
    this.RequiredAssets = new string[length4];
    for (int index = 0; index < length4; ++index)
      this.RequiredAssets[index] = r.ReadString();
    this.GUICard = (GUICard) Game.Catalogue.FetchCard(this.CardGUID, CardView.GUI);
    this.regulationCard = (RegulationCard) Game.Catalogue.FetchCard(cardGUID, CardView.Regulation);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.GUICard
    });
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.regulationCard
    });
    this.regulationCard.IsLoaded.AddHandler((SignalHandler) (() => FacadeFactory.GetInstance().SendMessage(Message.UpdateSectorRegulation)));
  }
}
