﻿// Decompiled with JetBrains decompiler
// Type: GuiRewardToast
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GuiRewardToast : GuiPanel
{
  private static readonly Texture2D top = ResourceLoader.Load<Texture2D>("GUI/_NewGui/rewards/reward_toast_top");
  private static readonly Texture2D center = ResourceLoader.Load<Texture2D>("GUI/_NewGui/rewards/reward_toast_center");
  private static readonly Texture2D bottom = ResourceLoader.Load<Texture2D>("GUI/_NewGui/rewards/reward_toast_bottom");
  private const int FADEOUT_TIME = 5;
  private static float lastRewardTime;
  private static float nextPositionY;
  private static GuiRewardToast.XAnchor xAnchor;
  private readonly GuiLabel titleLabel;
  private readonly GuiImage topImage;
  private readonly GuiImage centerImage;
  private readonly GuiImage bottomImage;
  private readonly GuiButton closeButton;
  private float time;

  public GuiRewardToast(GUICard guiCard, List<ShipItem> reward)
  {
    this.Size = new Vector2((float) GuiRewardToast.top.width, 100f);
    this.Align = Align.DownCenter;
    this.topImage = new GuiImage(GuiRewardToast.top);
    this.AddChild((GuiElementBase) this.topImage);
    this.centerImage = new GuiImage(GuiRewardToast.center);
    this.AddChild((GuiElementBase) this.centerImage);
    this.bottomImage = new GuiImage(GuiRewardToast.bottom);
    this.AddChild((GuiElementBase) this.bottomImage);
    this.titleLabel = new GuiLabel(Gui.Options.fontEurostileTBlaExt);
    this.titleLabel.NormalColor = ColorUtility.MakeColorFrom0To255((uint) sbyte.MaxValue, 179U, 67U);
    this.titleLabel.FontSize = 12;
    GuiLabel guiLabel = this.titleLabel;
    string str;
    if (guiCard != null)
      str = BsgoLocalization.Get("%$bgo.etc.assignment_completed_title%", (object) guiCard.Name);
    else
      str = string.Empty;
    guiLabel.Text = str;
    this.titleLabel.WordWrap = true;
    this.titleLabel.Position = new Vector2(24f, 14f);
    this.titleLabel.Size = new Vector2(this.SizeX - 48f, 24f);
    this.AddChild((GuiElementBase) this.titleLabel);
    for (int index = 0; index < reward.Count; ++index)
    {
      ShipItem shipItem = reward[index];
      GuiRewardToast.Item obj = new GuiRewardToast.Item(shipItem.ItemGUICard, (!(shipItem is ItemCountable) ? string.Empty : ((ItemCountable) shipItem).Count.ToString() + " ") + shipItem.ItemGUICard.Name);
      float x = 16f;
      float y = (float) ((double) this.titleLabel.PositionY + (double) this.titleLabel.SizeY + 8.0) + (float) (index * 40);
      obj.Position = new Vector2(x, y);
      this.AddChild((GuiElementBase) obj);
    }
    this.closeButton = new GuiButton(string.Empty, Gui.Options.closeButtonNormal, Gui.Options.closeButtonOver, Gui.Options.closeButtonPressed);
    this.closeButton.Align = Align.UpRight;
    this.closeButton.PressedSound = new GUISoundHandler(GUISound.Instance.OnWindowClose);
    this.closeButton.Position = new Vector2(3f, -3f);
    this.closeButton.Pressed = new AnonymousDelegate(this.Close);
    this.AddChild((GuiElementBase) this.closeButton);
    this.Resize(64 + reward.Count * 40);
    this.Position = this.GetFreePosition();
    this.IsMouseInputGrabber = true;
    GuiRewardToast.lastRewardTime = Time.realtimeSinceStartup;
  }

  private Vector2 GetFreePosition()
  {
    Vector2 vector2 = new Vector2(this.PositionX, -100f);
    if ((double) (Time.realtimeSinceStartup - GuiRewardToast.lastRewardTime) >= 5.0)
    {
      GuiRewardToast.nextPositionY = -this.SizeY;
      GuiRewardToast.xAnchor = GuiRewardToast.XAnchor.Default;
      return vector2;
    }
    vector2.y = GuiRewardToast.nextPositionY - 100f;
    if ((double) vector2.y < (double) -Screen.height * 0.699999988079071)
    {
      GuiRewardToast.nextPositionY = 0.0f;
      vector2.y = -100f;
      if (++GuiRewardToast.xAnchor > GuiRewardToast.XAnchor.TwoRight)
        GuiRewardToast.xAnchor = GuiRewardToast.XAnchor.Default;
    }
    vector2.x = this.GetXPosition(GuiRewardToast.xAnchor);
    GuiRewardToast.nextPositionY -= this.SizeY;
    return vector2;
  }

  private float GetXPosition(GuiRewardToast.XAnchor anchor)
  {
    switch (anchor)
    {
      case GuiRewardToast.XAnchor.OneLeft:
        return -this.SizeX;
      case GuiRewardToast.XAnchor.OneRight:
        return this.SizeX;
      case GuiRewardToast.XAnchor.TwoLeft:
        return -2f * this.SizeX;
      case GuiRewardToast.XAnchor.TwoRight:
        return -2f * this.SizeX;
      default:
        return 0.0f;
    }
  }

  private void Close()
  {
    this.IsRendered = false;
    this.IsMouseInputGrabber = false;
    this.RemoveThisFromParentLater();
  }

  private void Resize(int height)
  {
    this.SizeY = (float) height;
    this.centerImage.SizeY = this.Size.y - (float) GuiRewardToast.top.height - (float) GuiRewardToast.bottom.height;
    this.centerImage.PositionY = this.topImage.SizeY;
    this.bottomImage.PositionY = this.centerImage.PositionY + this.centerImage.SizeY;
  }

  public override void Draw()
  {
    if (Event.current.type == UnityEngine.EventType.Repaint)
    {
      this.time += Time.deltaTime;
      if ((double) this.time >= 5.0)
      {
        this.Close();
        return;
      }
    }
    Color color1 = GUI.color;
    Color color2 = color1;
    color2.a = (float) (1.5 - (double) this.time / 5.0);
    GUI.color = color2;
    this.topImage.OverlayColor = this.centerImage.OverlayColor = this.bottomImage.OverlayColor = new Color?(color2);
    base.Draw();
    GUI.color = color1;
  }

  private enum XAnchor
  {
    Default,
    OneLeft,
    OneRight,
    TwoLeft,
    TwoRight,
  }

  private class Item : GuiPanel
  {
    private readonly GuiAtlasImage image = new GuiAtlasImage();
    private readonly GuiLabel label = new GuiLabel(Gui.Options.fontEurostileTRegCon);

    public Item(GUICard guiCard, string text)
    {
      this.image.GuiCard = guiCard;
      this.label.Text = text;
      this.label.NormalColor = Color.white;
      this.label.FontSize = 14;
      this.AddChild((GuiElementBase) this.image, Align.MiddleLeft);
      this.AddChild((GuiElementBase) this.label, Align.MiddleLeft, new Vector2(50f, 0.0f));
      if (guiCard != null)
        this.SetTooltip(guiCard.Description);
      this.SizeX = (float) ((double) this.label.PositionX + (double) this.label.SizeX + 10.0);
      this.SizeY = Mathf.Max(this.image.SizeY, this.label.SizeY);
    }
  }
}
