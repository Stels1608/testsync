﻿// Decompiled with JetBrains decompiler
// Type: GUICharacterStatusWindow
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUICharacterStatusWindow : GuiTabs
{
  private readonly string info = "%$bgo.pilot_log.info%";
  private readonly string assignments = "%$bgo.pilot_log.assignments%";
  private readonly string duties = "%$bgo.pilot_log.duties%";
  private readonly string skills = "%$bgo.pilot_log.skills%";
  private readonly string ship = "%$bgo.pilot_log.ship%";
  private readonly string bonuses = "%$bgo.pilot_log.bonuses%";
  private readonly string mail = "%$bgo.pilot_log.mail%";
  private readonly string friendInvites = "%$bgo.pilot_log.friend_invites%";
  private InfoJournal infoJounalPanel = new InfoJournal();
  private GuiMissions missionsPanel = new GuiMissions();
  private DutyPanel dutyPanel = new DutyPanel();
  private SkillBuyPanel skillPanel = new SkillBuyPanel();
  private GuiBonuses bonusesPanel = new GuiBonuses();
  private GuiMail mailPanel = new GuiMail();
  private GuiFriendInvites friendInvitesPanel = new GuiFriendInvites();
  private GuiButton closeButton = new GuiButton(string.Empty, Gui.Options.closeButtonNormal, Gui.Options.closeButtonOver, Gui.Options.closeButtonPressed);
  private Gui.CharacterStatus.InflightShop.InflightShop inflightShop;
  public System.Action HandlerClose;

  public override bool IsBigWindow
  {
    get
    {
      return true;
    }
  }

  public GUICharacterStatusWindow(bool inSpace)
  {
    this.IsRendered = false;
    this.Align = Align.MiddleCenter;
    this.PositionY = -250f;
    this.infoJounalPanel.OnClose = new AnonymousDelegate(this.Close);
    this.missionsPanel.OnClose = new AnonymousDelegate(this.Close);
    this.inflightShop = new Gui.CharacterStatus.InflightShop.InflightShop(inSpace);
    this.inflightShop.OnClose = new AnonymousDelegate(this.Close);
    this.friendInvitesPanel.OnClose = new AnonymousDelegate(this.Close);
    this.mailPanel.OnClose = new AnonymousDelegate(this.Close);
    this.bonusesPanel.OnClose = new AnonymousDelegate(this.Close);
    this.closeButton.IsRendered = false;
    this.closeButton.Pressed = new AnonymousDelegate(this.Close);
    this.AddChild((GuiElementBase) this.closeButton);
    this.AddTab(this.info, (GuiElementBase) this.infoJounalPanel);
    this.AddTab(this.assignments, (GuiElementBase) this.missionsPanel);
    this.AddTab(this.duties, (GuiElementBase) this.dutyPanel);
    this.AddTab(this.bonuses, (GuiElementBase) this.bonusesPanel);
    this.AddTab(this.skills, (GuiElementBase) this.skillPanel);
    this.AddTab(this.mail, (GuiElementBase) this.mailPanel);
    this.AddTab(this.ship, (GuiElementBase) this.inflightShop);
    this.GetButton(this.info).SetTooltip("%$bgo.pilot_log.info_tooltip%");
    this.GetButton(this.assignments).SetTooltip("%$bgo.pilot_log.assignments_tooltip%");
    this.GetButton(this.duties).SetTooltip("%$bgo.pilot_log.duties_tooltip%");
    this.GetButton(this.skills).SetTooltip("%$bgo.pilot_log.skills_tooltip%");
    this.GetButton(this.ship).SetTooltip("%$bgo.pilot_log.ship_tooltip%");
    this.GetButton(this.bonuses).SetTooltip("%$bgo.pilot_log.bonuses_tooltip%");
    this.GetButton(this.mail).SetTooltip("%$bgo.pilot_log.mail_tooltip%");
    this.RegisterHotKey(Action.ToggleWindowPilotLog, new AnonymousDelegate(this.OpenPilotLog));
    this.RegisterHotKey(Action.ToggleWindowStatusInfo, new AnonymousDelegate(this.OpenPilotLog));
    this.RegisterHotKey(Action.ToggleWindowInventory, new AnonymousDelegate(this.OpenInventory));
    this.RegisterHotKey(Action.ToggleWindowDuties, new AnonymousDelegate(this.OpenDuties));
    this.RegisterHotKey(Action.ToggleWindowStatusAssignments, new AnonymousDelegate(this.OpenMissions));
    this.RegisterHotKey(Action.ToggleWindowSkills, new AnonymousDelegate(this.OpenSkills));
    this.RegisterHotKey(Action.ToggleWindowShipStatus, new AnonymousDelegate(this.OpenShipStatus));
    this.RegisterHotKey(Action.ToggleWindowInFlightSupply, new AnonymousDelegate(this.OpenRestock));
    this.AddChild((GuiElementBase) new Gui.Timer(1f));
  }

  public override bool KeyDown(KeyCode key, Action action)
  {
    if (action == Action.NearestEnemy)
      return true;
    return base.KeyDown(key, action);
  }

  public override bool KeyUp(KeyCode key, Action action)
  {
    if (action != Action.NearestEnemy)
      return base.KeyUp(key, action);
    this.SelectNext();
    return true;
  }

  public GuiMissions GetMissionsPanel()
  {
    return this.missionsPanel;
  }

  public override bool Contains(float2 point)
  {
    if (!base.Contains(point))
      return this.closeButton.Contains(point);
    return true;
  }

  protected override void OnSelect(GuiButton button, GuiElementBase content)
  {
    base.OnSelect(button, content);
    if (content == null)
      return;
    content.Align = Align.UpCenter;
    content.PositionY = this.SizeY + 10f;
    if (content == this.dutyPanel || content == this.skillPanel)
    {
      this.closeButton.IsRendered = true;
      this.closeButton.Position = content.PositionNormal + new Vector2(content.SizeX + 3f - this.closeButton.SizeX, -3f);
      this.BringToFront((GuiElementBase) this.closeButton);
    }
    else if (content == this.friendInvitesPanel)
      CommunityProtocol.GetProtocol().RequestRecruitBonusLevel();
    else
      this.closeButton.IsRendered = false;
    content.PeriodicUpdate();
  }

  private void RequestPanel(GuiElementBase panel)
  {
    if (!Game.GUIManager.IsRendered)
      return;
    if (this.GetSelectedContent() == panel)
      this.IsRendered = !this.IsRendered;
    else
      this.IsRendered = true;
    this.SelectByContent(panel);
  }

  [Gui2Editor]
  public void OpenPilotLog()
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.infoJounalPanel.UpdateDisplay();
    this.RequestPanel((GuiElementBase) this.infoJounalPanel);
  }

  [Gui2Editor]
  public void OpenInventory()
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.RequestPanel((GuiElementBase) this.inflightShop);
    this.inflightShop.SelectHold();
  }

  [Gui2Editor]
  public void OpenDuties()
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.RequestPanel((GuiElementBase) this.dutyPanel);
  }

  [Gui2Editor]
  public void OpenMissions()
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.RequestPanel((GuiElementBase) this.missionsPanel);
  }

  [Gui2Editor]
  public void OpenMail()
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.RequestPanel((GuiElementBase) this.mailPanel);
  }

  public void OpenSkills()
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.RequestPanel((GuiElementBase) this.skillPanel);
  }

  [Gui2Editor]
  public void OpenShipStatus()
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.RequestPanel((GuiElementBase) this.inflightShop);
    this.inflightShop.SelectStatus();
  }

  [Gui2Editor]
  public void OpenRestock()
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.RequestPanel((GuiElementBase) this.inflightShop);
    this.inflightShop.SelectResupply();
  }

  [Gui2Editor]
  public void OpenBonuses()
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.RequestPanel((GuiElementBase) this.bonusesPanel);
  }

  [Gui2Editor]
  public void OpenFriendInvite()
  {
    if (!GameLevel.Instance.IsIngameLevel)
      return;
    this.RequestPanel((GuiElementBase) this.friendInvitesPanel);
    CommunityProtocol.GetProtocol().RequestRecruitBonusLevel();
  }

  [Gui2Editor]
  public void Close()
  {
    this.IsRendered = false;
    if (this.HandlerClose == null)
      return;
    this.HandlerClose();
  }

  public void UpdateAvatar(AvatarDescription desc)
  {
    this.infoJounalPanel.UpdateAvatar(desc);
  }
}
