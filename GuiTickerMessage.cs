﻿// Decompiled with JetBrains decompiler
// Type: GuiTickerMessage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class GuiTickerMessage
{
  public const int STD_MSG_TIME_TO_LIVE = 900;

  public string Text { get; private set; }

  public float TimeToLive { get; private set; }

  public GuiTickerMessage(string text, float timeToLive = 900)
  {
    this.Text = text;
    this.TimeToLive = timeToLive;
  }
}
