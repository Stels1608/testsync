﻿// Decompiled with JetBrains decompiler
// Type: SectorMiningShipUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class SectorMiningShipUpdate : GalaxyMapUpdate
{
  public int MiningShipCount { get; private set; }

  public SectorMiningShipUpdate(Faction faction, uint sectorId, int miningShipCount)
    : base(GalaxyUpdateType.SectorMiningShips, faction, (int) sectorId)
  {
    this.MiningShipCount = miningShipCount;
  }

  public override string ToString()
  {
    return "[SectorMiningShipUpdate] (Faction: " + (object) this.Faction + ", SectorId: " + (object) this.SectorId + ", MiningShipCount:" + (object) this.MiningShipCount + ")";
  }
}
