﻿// Decompiled with JetBrains decompiler
// Type: GalaxyUpdateType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public enum GalaxyUpdateType : byte
{
  Rcp = 1,
  ConquestLocation = 2,
  ConquestPrice = 3,
  SectorOutpostPoints = 4,
  SectorMiningShips = 5,
  SectorPvPKills = 6,
  SectorDynamicMissions = 7,
  SectorOutpostState = 8,
  SectorBeaconState = 9,
  SectorJumpTargetTransponders = 10,
  SectorPlayerSlots = 11,
}
