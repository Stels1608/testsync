﻿// Decompiled with JetBrains decompiler
// Type: Mob
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Json;
using System.Collections.Generic;

public class Mob
{
  public string type = "mob_colonial";
  public int level = 1;
  public int count = 1;
  public int chance = 1;
  [JsonField(deserialize = false, serialize = false)]
  public JsonData m_jMobData = new JsonData(JsonType.Null);
  [JsonField(deserialize = false, serialize = false)]
  public JsonData m_jShipData = new JsonData(JsonType.Null);
  [JsonField(deserialize = false, serialize = false)]
  public string m_shipName = string.Empty;

  public Mob()
  {
  }

  public Mob(Mob bot)
  {
    this.type = bot.type;
    this.level = bot.level;
    this.count = bot.count;
    this.chance = bot.chance;
  }

  public void LoadMobJData()
  {
    this.m_jMobData = ContentDB.GetDocumentRaw(this.type);
    if (this.m_jMobData == null)
      return;
    if (this.m_jMobData.Object.ContainsKey("Ship"))
      this.m_jShipData = ContentDB.GetDocumentRaw(this.m_jMobData.Object["Ship"].String);
    else
      this.m_jShipData = (JsonData) null;
  }

  public void GetShipName()
  {
    if (this.m_jShipData == null || !this.m_jShipData.Object.ContainsKey("Name"))
      return;
    JsonData jsonData = this.m_jShipData.Object["Name"];
    string str = "| ";
    if (!jsonData.IsString)
    {
      using (Dictionary<string, JsonData>.KeyCollection.Enumerator enumerator = jsonData.Object.Keys.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          string current = enumerator.Current;
          if (jsonData.Object[current].IsString)
            str = str + jsonData.Object[current].ToJsonString(string.Empty) + " | ";
        }
      }
      this.m_shipName = str;
    }
    else
      str = str + this.m_jShipData.Object["Name"].ToJsonString(string.Empty) + " |";
    this.m_shipName = str;
  }
}
