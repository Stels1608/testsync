﻿// Decompiled with JetBrains decompiler
// Type: GuiMailUnread
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class GuiMailUnread : GuiPanel
{
  private GuiButton button = new GuiButton(string.Empty, "GUI/Mail/envelope", "GUI/Mail/envelope_click", "GUI/Mail/envelope_over");
  private bool mouseEntered;

  public GuiMailUnread()
  {
    this.Align = Align.UpLeft;
    this.Position = new Vector2(70f, 5f);
    this.Size = this.button.Size;
    this.AddChild((GuiElementBase) this.button);
    this.AddChild((GuiElementBase) new Gui.Timer(0.5f, (AnonymousDelegate) (() =>
    {
      if ((Object) this.button.NormalTexture == (Object) ResourceLoader.Load<Texture2D>("GUI/Mail/envelope"))
        this.button.NormalTexture = ResourceLoader.Load<Texture2D>("GUI/Mail/envelope_blink");
      else
        this.button.NormalTexture = ResourceLoader.Load<Texture2D>("GUI/Mail/envelope");
    })));
    this.AddButtonBehaviour();
    Game.InputDispatcher.AddListener((InputListener) this);
  }

  public static void CheckMail()
  {
    GuiMailUnread guiMailUnread = Game.GUIManager.Find<GuiMailUnread>();
    foreach (Mail mail in (IEnumerable<Mail>) Game.Me.MailBox)
    {
      if (mail.Status == Mail.MailStatus.Unread)
      {
        if (guiMailUnread == null)
        {
          guiMailUnread = new GuiMailUnread();
          Game.GUIManager.AddPanel((IGUIPanel) guiMailUnread, 0);
          Game.InputDispatcher.AddListener((InputListener) guiMailUnread);
          return;
        }
        guiMailUnread.IsRendered = true;
        return;
      }
    }
    if (guiMailUnread == null)
      return;
    guiMailUnread.IsRendered = false;
  }

  public override void Update()
  {
    base.Update();
    if (this.mouseEntered && Input.GetMouseButtonDown(0))
      this.button.Pressed();
    if (Game.GUIManager.Find<GuiOutpostProgress>() == null)
      return;
    this.Position = new Vector2((float) ((double) -Screen.width / 2.0 + (double) Game.GUIManager.Find<GuiOutpostProgress>().SizeX + 30.0), 10f);
  }

  private void AddButtonBehaviour()
  {
    this.button.Pressed = new AnonymousDelegate(this.PressThisButton);
  }

  private void PressThisButton()
  {
    GUICharacterStatusWindow characterStatusWindow = Game.GUIManager.Find<GUICharacterStatusWindow>();
    if (characterStatusWindow == null || characterStatusWindow.IsRendered)
      return;
    characterStatusWindow.OpenMail();
  }

  public override void MouseEnter(float2 position, float2 previousPosition)
  {
    base.MouseEnter(position, previousPosition);
    this.mouseEntered = true;
  }

  public override void MouseLeave(float2 position, float2 previousPosition)
  {
    base.MouseLeave(position, previousPosition);
    this.mouseEntered = false;
  }
}
