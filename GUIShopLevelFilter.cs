﻿// Decompiled with JetBrains decompiler
// Type: GUIShopLevelFilter
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using System.Collections.Generic;

public class GUIShopLevelFilter : GUIPanel
{
  private Dictionary<int, GUIButtonNew> buttons = new Dictionary<int, GUIButtonNew>();
  private const int COUNT_BUTTONS = 10;
  private const float SPACE_BUTTONS = 20f;
  private int selectedFilter;

  public int SelectedFilter
  {
    get
    {
      return this.selectedFilter;
    }
  }

  public GUIShopLevelFilter(SmartRect parent)
  {
    this.IsRendered = true;
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/EquipBuyPanel/gui_levelfilter_layout");
    this.buttons[0] = (jwindowDescription["button"] as JButton).CreateGUIButtonNew(this.root);
    this.buttons[0].Handler = new AnonymousDelegate(this.OnFilterLevel);
    this.buttons[0].TextLabel.Text = "%$bgo.shop.all%";
    this.buttons[0].TextLabel.MakeCenteredRect();
    this.root.Width = this.buttons[0].SmartRect.Width;
    this.root.Height = (float) ((20.0 + (double) this.buttons[0].SmartRect.Height) * 10.0 - 20.0);
    this.buttons[0].Position = new float2(0.0f, (float) (-(double) this.root.Height / 2.0 + (double) this.buttons[0].SmartRect.Height / 2.0));
    this.AddPanel((GUIPanel) this.buttons[0]);
    for (int index = 1; index <= 10; ++index)
    {
      this.buttons[index] = (jwindowDescription["button"] as JButton).CreateGUIButtonNew(this.root);
      this.buttons[index].PlaceBelowOf((GUIPanel) this.buttons[index - 1], 20f);
      this.buttons[index].Handler = new AnonymousDelegate(this.OnFilterLevel);
      this.buttons[index].TextLabel.Text = index.ToString();
      this.buttons[index].TextLabel.MakeCenteredRect();
      this.AddPanel((GUIPanel) this.buttons[index]);
    }
    this.RecalculateAbsCoords();
    this.buttons[0].IsPressed = true;
  }

  protected void OnFilterLevel()
  {
    using (Dictionary<int, GUIButtonNew>.Enumerator enumerator = this.buttons.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<int, GUIButtonNew> current = enumerator.Current;
        if (current.Value.Contains(MouseSetup.MousePositionGui))
        {
          this.selectedFilter = current.Key;
          current.Value.IsPressed = true;
        }
        else
          current.Value.IsPressed = false;
      }
    }
  }
}
