﻿// Decompiled with JetBrains decompiler
// Type: RegulationCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class RegulationCard : Card
{
  public ConsumableEffectType[] effectTypeBlacklist;

  public Dictionary<uint, HashSet<ShipAbilitySide>> AbilityTargetRelations { get; private set; }

  public Dictionary<uint, HashSet<ShipAbilityTarget>> AbilityTargetTypes { get; private set; }

  public TargetBracketMode TargetBracketMode { get; private set; }

  public RegulationCard(uint cardGuid)
    : base(cardGuid)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.TargetBracketMode = (TargetBracketMode) r.ReadByte();
    this.AbilityTargetRelations = new Dictionary<uint, HashSet<ShipAbilitySide>>();
    this.AbilityTargetTypes = new Dictionary<uint, HashSet<ShipAbilityTarget>>();
    ushort num1 = r.ReadUInt16();
    for (int index = 0; index < (int) num1; ++index)
    {
      uint key = r.ReadUInt32();
      HashSet<ShipAbilitySide> shipAbilitySideSet = r.ReadSet<ShipAbilitySide>();
      HashSet<ShipAbilityTarget> shipAbilityTargetSet = r.ReadSet<ShipAbilityTarget>();
      this.AbilityTargetRelations.Add(key, shipAbilitySideSet);
      this.AbilityTargetTypes.Add(key, shipAbilityTargetSet);
    }
    int num2 = r.ReadLength();
    this.effectTypeBlacklist = new ConsumableEffectType[(int) num1];
    for (int index = 0; index < num2; ++index)
      this.effectTypeBlacklist[index] = (ConsumableEffectType) r.ReadByte();
  }
}
