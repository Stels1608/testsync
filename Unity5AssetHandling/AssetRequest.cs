﻿// Decompiled with JetBrains decompiler
// Type: Unity5AssetHandling.AssetRequest
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

namespace Unity5AssetHandling
{
  public class AssetRequest
  {
    public string AssetName { get; private set; }

    public AssetBundleCard BundleCard { get; private set; }

    public int Priority { get; private set; }

    public bool Persistent { get; private set; }

    public bool ASync { get; private set; }

    public bool IsDone { get; private set; }

    public bool InProgress { get; set; }

    public UnityEngine.Object Asset { get; private set; }

    public int Tries { get; set; }

    public AssetRequest(string assetName, AssetBundleCard bundleCard, int priority, bool async, bool persistent)
    {
      this.AssetName = assetName;
      this.BundleCard = bundleCard;
      this.Priority = priority;
      this.ASync = async;
      this.Persistent = persistent;
    }

    public void Fulfil(UnityEngine.Object asset)
    {
      if (this.IsDone)
        throw new Exception("Cannot fulfil an AssetRequest more than once!");
      this.Asset = asset;
      this.IsDone = true;
    }

    public override string ToString()
    {
      return string.Format("[AssetRequest] AssetName: {0}, Priority: {1}, Persistent: {2}, ASync: {3}, IsDone: {4}, InProgress: {5}", (object) this.AssetName, (object) this.Priority, (object) this.Persistent, (object) this.ASync, (object) this.IsDone, (object) this.InProgress);
    }
  }
}
