﻿// Decompiled with JetBrains decompiler
// Type: Unity5AssetHandling.AssetCatalogue
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace Unity5AssetHandling
{
  public class AssetCatalogue : MonoBehaviour
  {
    private readonly Dictionary<string, AssetBundleCard> assetMap = new Dictionary<string, AssetBundleCard>();
    private readonly Dictionary<AssetBundleCard, AssetBundle> cache = new Dictionary<AssetBundleCard, AssetBundle>();
    private readonly Dictionary<AssetBundleCard, AssetBundleCreateRequest> runningDownloads = new Dictionary<AssetBundleCard, AssetBundleCreateRequest>();
    private readonly Dictionary<string, AssetRequest> pendingRequests = new Dictionary<string, AssetRequest>();
    private const string LOCAL_BUNDLE_FOLDER = "/../assetbundles/";
    private const string REMOTE_BUNDLES_FOLDER = "/assetbundles/";
    private const string LOW_RES_STRING = "lowres";
    private const int MAX_CACHED_BUNDLES_LOW = 24;
    private const int MAX_CACHED_BUNDLES_MEDIUM = 48;
    private const int MAX_CACHED_BUNDLES_HIGH = 96;
    private const int MAX_SIMULTANEOUS_DOWNLOADS = 2;
    private const int MAX_DOWNLOAD_RETRIES = 5;
    private static AssetCatalogue instance;
    private int assetsRequested;
    private int requestsCreated;
    private string assetMapVersion;

    public bool UseLocalFiles { get; set; }

    public bool IsReady { get; private set; }

    public string BaseUrl { get; set; }

    private int MaxBundles { get; set; }

    private int Downloading { get; set; }

    public static AssetCatalogue Instance
    {
      get
      {
        return AssetCatalogue.instance ?? (AssetCatalogue.instance = new GameObject("AssetDatabase").AddComponent<AssetCatalogue>());
      }
    }

    public bool AuthorizeCache(CacheAuthorizationData data)
    {
      return Caching.Authorize(data.ProductName, data.Domain, data.CacheSize, data.Signature);
    }

    private string GetUrl(AssetBundleCard bundle)
    {
      if (this.UseLocalFiles)
        return this.GetLocalUrl(bundle);
      return this.GetRemoteUrl(bundle);
    }

    private string GetLocalUrl(AssetBundleCard bundle)
    {
      return "file://" + Application.dataPath + "/../assetbundles/" + bundle.Name;
    }

    private string GetRemoteUrl(AssetBundleCard bundle)
    {
      return (!string.IsNullOrEmpty(this.BaseUrl) ? this.BaseUrl : Application.dataPath) + "/assetbundles/" + bundle.Name + (!string.IsNullOrEmpty(bundle.Hash) ? "?__cv=" + bundle.Hash : string.Empty);
    }

    private string GetAssetMapUrl(string version)
    {
      this.assetMapVersion = version;
      return this.GetUrl(new AssetBundleCard("assetmap.json", version, string.Empty, (List<string>) null, (List<string>) null, 100));
    }

    private void Awake()
    {
      this.SetMaxBundles();
      UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
      this.InvokeRepeating("ProcessQueue", 0.25f, 0.25f);
    }

    private void SetMaxBundles()
    {
      this.MaxBundles = SystemInfo.systemMemorySize <= 4000 ? (SystemInfo.systemMemorySize <= 2000 ? 24 : 48) : 96;
      UnityEngine.Debug.Log((object) ("System memory of " + (object) SystemInfo.systemMemorySize + "MB detected; setting maxAssetBundles to " + (object) this.MaxBundles));
    }

    public void LoadAssetMap(string version)
    {
      this.IsReady = false;
      this.assetMap.Clear();
      this.StartCoroutine(this.WaitForAssetMap(version));
    }

    [DebuggerHidden]
    private IEnumerator WaitForAssetMap(string version)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new AssetCatalogue.\u003CWaitForAssetMap\u003Ec__Iterator8() { version = version, \u003C\u0024\u003Eversion = version, \u003C\u003Ef__this = this };
    }

    public AssetRequest Request(string assetName, bool persistent = false)
    {
      return this.Request(assetName, string.Empty, persistent);
    }

    public AssetRequest Request(string assetName, string variant, bool persistent = false)
    {
      ++this.assetsRequested;
      string lowerInvariant = assetName.ToLowerInvariant();
      AssetBundleCard assetBundle = this.FindAssetBundle(lowerInvariant, variant);
      int priority = Mathf.Clamp(assetBundle == null ? 0 : assetBundle.Priority, 0, 100);
      bool async = true;
      if (assetBundle == null)
      {
        if (!lowerInvariant.Contains("lowres"))
          UnityEngine.Debug.LogWarning((object) ("Requested unknown asset '" + assetName + "'"));
        AssetRequest assetRequest = new AssetRequest(lowerInvariant, assetBundle, priority, async, persistent);
        assetRequest.Fulfil((UnityEngine.Object) null);
        return assetRequest;
      }
      assetBundle.Persistent |= persistent;
      AssetRequest assetRequest1;
      if (this.pendingRequests.ContainsKey(lowerInvariant))
      {
        assetRequest1 = this.pendingRequests[lowerInvariant];
      }
      else
      {
        ++this.requestsCreated;
        assetRequest1 = new AssetRequest(lowerInvariant, assetBundle, priority, async, persistent);
        this.pendingRequests.Add(lowerInvariant, assetRequest1);
      }
      return assetRequest1;
    }

    private void ProcessQueue()
    {
      if (!this.IsReady || this.Downloading >= 2 || this.pendingRequests.Count == 0)
        return;
      List<AssetRequest> assetRequestList = new List<AssetRequest>((IEnumerable<AssetRequest>) this.pendingRequests.Values);
      assetRequestList.RemoveAll((Predicate<AssetRequest>) (request => request.InProgress));
      assetRequestList.Sort((Comparison<AssetRequest>) ((x, y) => x.Priority.CompareTo(y.Priority)));
      while (this.Downloading < 2 && assetRequestList.Count > 0)
      {
        int index = assetRequestList.Count - 1;
        AssetRequest request = assetRequestList[index];
        assetRequestList.RemoveAt(index);
        this.LoadAsset(request);
      }
    }

    private void LoadAsset(AssetRequest request)
    {
      request.InProgress = true;
      if (this.cache.ContainsKey(request.BundleCard))
      {
        this.StartCoroutine(this.Fulfil(request, this.cache[request.BundleCard]));
      }
      else
      {
        Stack<AssetBundleCard> dependencies = this.GetDependencies(request.BundleCard);
        ++this.Downloading;
        this.StartCoroutine(this.DownloadWorker(request, dependencies));
      }
    }

    [DebuggerHidden]
    private IEnumerator DownloadWorker(AssetRequest request, Stack<AssetBundleCard> dependencyChain)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new AssetCatalogue.\u003CDownloadWorker\u003Ec__Iterator9() { dependencyChain = dependencyChain, request = request, \u003C\u0024\u003EdependencyChain = dependencyChain, \u003C\u0024\u003Erequest = request, \u003C\u003Ef__this = this };
    }

    private void DownloadFailed(AssetRequest request)
    {
      UnityEngine.Debug.LogWarning((object) ("Failed downloading '" + request.AssetName + "', trying again.."));
      request.InProgress = false;
      if (request.Tries > 5)
        throw new Exception("Download for '" + request.AssetName + "' failed " + (object) request.Tries + " times, AssetMapVersion " + this.assetMapVersion + " - aborting...");
    }

    private void LockBundle(AssetBundleCard bundle)
    {
      ++bundle.Locks;
    }

    private void UnlockBundle(AssetBundleCard bundle)
    {
      --bundle.Locks;
    }

    private bool IsBundleLocked(AssetBundleCard bundle)
    {
      return bundle.Locks > 0;
    }

    private Stack<AssetBundleCard> GetDependencies(AssetBundleCard bundle)
    {
      Stack<AssetBundleCard> dependencyChain = new Stack<AssetBundleCard>();
      this.AddDependencies(bundle, dependencyChain);
      return dependencyChain;
    }

    private void AddDependencies(AssetBundleCard bundle, Stack<AssetBundleCard> dependencyChain)
    {
      if (!dependencyChain.Contains(bundle))
        dependencyChain.Push(bundle);
      if (bundle.Dependencies == null || bundle.Dependencies.Count == 0)
        return;
      using (List<string>.Enumerator enumerator1 = bundle.Dependencies.GetEnumerator())
      {
        while (enumerator1.MoveNext())
        {
          string current1 = enumerator1.Current;
          using (Dictionary<string, AssetBundleCard>.ValueCollection.Enumerator enumerator2 = this.assetMap.Values.GetEnumerator())
          {
            while (enumerator2.MoveNext())
            {
              AssetBundleCard current2 = enumerator2.Current;
              if (current2.Name == current1)
              {
                current2.Persistent = true;
                this.AddDependencies(current2, dependencyChain);
              }
            }
          }
        }
      }
    }

    [DebuggerHidden]
    private IEnumerator Fulfil(AssetRequest request, AssetBundle bundle)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new AssetCatalogue.\u003CFulfil\u003Ec__IteratorA() { bundle = bundle, request = request, \u003C\u0024\u003Ebundle = bundle, \u003C\u0024\u003Erequest = request, \u003C\u003Ef__this = this };
    }

    private AssetBundleCard FindAssetBundle(string assetName, string variant)
    {
      string key = assetName + variant;
      if (!this.assetMap.ContainsKey(key))
        return (AssetBundleCard) null;
      return this.assetMap[key];
    }

    public void Unload()
    {
      this.StopAllCoroutines();
      this.pendingRequests.Clear();
      List<AssetBundleCard> assetBundleCardList1 = new List<AssetBundleCard>((IEnumerable<AssetBundleCard>) this.cache.Keys);
      assetBundleCardList1.Sort((Comparison<AssetBundleCard>) ((b1, b2) => b1.Hits.CompareTo(b2.Hits)));
      List<AssetBundleCard> assetBundleCardList2 = new List<AssetBundleCard>();
      using (List<AssetBundleCard>.Enumerator enumerator = assetBundleCardList1.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AssetBundleCard current = enumerator.Current;
          if (assetBundleCardList1.Count - assetBundleCardList2.Count > this.MaxBundles)
          {
            if (!current.Persistent && !this.runningDownloads.ContainsKey(current) && !this.IsBundleLocked(current))
              assetBundleCardList2.Add(current);
          }
          else
            break;
        }
      }
      UnityEngine.Debug.Log((object) (this.assetsRequested.ToString() + " assets requested, " + (object) this.requestsCreated + " requests created"));
      UnityEngine.Debug.Log((object) ("Unloading " + (object) assetBundleCardList2.Count + " asset bundles."));
      using (List<AssetBundleCard>.Enumerator enumerator = assetBundleCardList2.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          AssetBundleCard current = enumerator.Current;
          AssetBundle assetBundle = this.cache[current];
          this.cache.Remove(current);
          if (!((UnityEngine.Object) assetBundle == (UnityEngine.Object) null))
            assetBundle.Unload(true);
        }
      }
    }
  }
}
