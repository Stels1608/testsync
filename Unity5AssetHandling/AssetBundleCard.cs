﻿// Decompiled with JetBrains decompiler
// Type: Unity5AssetHandling.AssetBundleCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace Unity5AssetHandling
{
  public class AssetBundleCard
  {
    private readonly string name;
    private readonly string hash;
    private readonly string variant;
    private readonly List<string> assets;
    private readonly List<string> dependencies;
    private readonly int priority;
    [JsonField(deserialize = false, serialize = false)]
    private int hits;
    [JsonField(deserialize = false, serialize = false)]
    private int locks;
    [JsonField(deserialize = false, serialize = false)]
    private bool persistent;

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public string Hash
    {
      get
      {
        return this.hash;
      }
    }

    public string Variant
    {
      get
      {
        return this.variant;
      }
    }

    public List<string> Assets
    {
      get
      {
        return this.assets;
      }
    }

    public List<string> Dependencies
    {
      get
      {
        return this.dependencies;
      }
    }

    public int Priority
    {
      get
      {
        return this.priority;
      }
    }

    public int Hits
    {
      get
      {
        return this.hits;
      }
      set
      {
        this.hits = value;
      }
    }

    public int Locks
    {
      get
      {
        return this.locks;
      }
      set
      {
        this.locks = value;
      }
    }

    public bool Persistent
    {
      get
      {
        return this.persistent;
      }
      set
      {
        this.persistent = value;
      }
    }

    public AssetBundleCard()
    {
    }

    public AssetBundleCard(string name, string hash, string variant, List<string> assets, List<string> dependencies, int priority)
    {
      this.name = name;
      this.hash = hash;
      this.variant = variant;
      this.assets = assets;
      this.dependencies = dependencies;
      this.priority = priority;
    }
  }
}
