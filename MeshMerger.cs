﻿// Decompiled with JetBrains decompiler
// Type: MeshMerger
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class MeshMerger : Object
{
  private GameObject go = new GameObject();
  private bool UpdateOptimizationMeshOnListChange = true;
  private List<Component> Components = new List<Component>();
  private List<bool> ComponentVisibleFlags = new List<bool>();
  public string tInfo = "meshes were not optimized yet";
  public Material material;
  private MeshFilter[] meshFilters;
  private SkinnedMeshRenderer smr;
  private int numNullRefs;
  private int numAdded;
  private int numRemoved;
  private bool needToOptimize;

  public void SetAutoUpdate(bool flag)
  {
    this.UpdateOptimizationMeshOnListChange = flag;
    if (!flag || !this.needToOptimize)
      return;
    this.OptimizeComponents();
  }

  public void SetComponentVisible(Component c, bool flag)
  {
    int index = this.Components.IndexOf(c);
    if (index == -1 || this.ComponentVisibleFlags[index] == flag)
      return;
    this.ComponentVisibleFlags[index] = flag;
    this.needToOptimize = true;
    if (!this.UpdateOptimizationMeshOnListChange)
      return;
    this.OptimizeComponents();
  }

  public void RegisterObjectToOptimize(Component c)
  {
    this.ComponentVisibleFlags.Add(true);
    this.Components.Add(c);
    this.needToOptimize = true;
    ++this.numAdded;
    if (!this.UpdateOptimizationMeshOnListChange)
      return;
    this.OptimizeComponents();
  }

  public void RemoveComponentToOptimize(Component c)
  {
    if ((Object) c == (Object) null)
      Debug.Log((object) "!! MeshMerger: RemoveComponentToOptimize() - component is NULL!");
    int index = this.Components.IndexOf(c);
    if (index == -1)
      return;
    ++this.numRemoved;
    this.Components.Remove(c);
    this.needToOptimize = true;
    this.ComponentVisibleFlags.RemoveAt(index);
    if (!this.UpdateOptimizationMeshOnListChange)
      return;
    this.OptimizeComponents();
  }

  public void RemoveAllComponentsToOptimize(bool RestoreOriginalMeshes)
  {
    this.tInfo = "meshes were not optimized yet";
    this.numAdded = 0;
    this.numRemoved = 0;
    this.numNullRefs = 0;
    if (RestoreOriginalMeshes)
      this.UnOptimize();
    else if ((bool) ((Object) this.smr))
    {
      Object.DestroyImmediate((Object) this.smr);
      this.smr = (SkinnedMeshRenderer) null;
    }
    this.Components.Clear();
    Object.DestroyImmediate((Object) this.go);
    this.go = new GameObject();
  }

  public void UnOptimize()
  {
    this.tInfo = "meshes were not optimized yet";
    this.numAdded = 0;
    this.numRemoved = 0;
    this.numNullRefs = 0;
    using (List<Component>.Enumerator enumerator = this.Components.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Component current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          foreach (Component componentsInChild in current.GetComponentsInChildren<MeshFilter>())
            componentsInChild.GetComponent<Renderer>().enabled = true;
        }
      }
    }
    if (!(bool) ((Object) this.smr))
      return;
    Object.DestroyImmediate((Object) this.smr);
    this.smr = (SkinnedMeshRenderer) null;
  }

  public void OptimizeComponents()
  {
    if (!this.needToOptimize)
      return;
    this.numNullRefs = 0;
    this.tInfo = "meshes optimized; ";
    int length = 0;
    using (List<Component>.Enumerator enumerator = this.Components.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Component current = enumerator.Current;
        if ((Object) current == (Object) null)
        {
          ++this.numNullRefs;
        }
        else
        {
          foreach (Component componentsInChild in current.GetComponentsInChildren<MeshFilter>())
          {
            componentsInChild.GetComponent<Renderer>().enabled = true;
            ++length;
          }
        }
      }
    }
    if (this.numNullRefs != 0)
    {
      List<bool> boolList = new List<bool>();
      List<Component> componentList = new List<Component>();
      int index = 0;
      using (List<Component>.Enumerator enumerator = this.Components.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          Component current = enumerator.Current;
          if ((Object) current != (Object) null)
          {
            componentList.Add(current);
            boolList.Add(this.ComponentVisibleFlags[index]);
          }
          ++index;
        }
      }
      this.Components = componentList;
      this.ComponentVisibleFlags = boolList;
    }
    MeshFilter[] meshFilterArray = new MeshFilter[length];
    this.tInfo = this.tInfo + " #components: " + length.ToString() + "; # null refs: " + this.numNullRefs.ToString();
    if (this.numAdded != this.numRemoved)
      this.tInfo = this.tInfo + " #[+]: " + this.numAdded.ToString() + ", #[-]: " + this.numRemoved.ToString();
    int index1 = 0;
    using (List<Component>.Enumerator enumerator = this.Components.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Component current = enumerator.Current;
        if (!((Object) current == (Object) null))
        {
          foreach (MeshFilter componentsInChild in current.GetComponentsInChildren<MeshFilter>())
          {
            meshFilterArray[index1] = componentsInChild;
            this.material = componentsInChild.GetComponent<Renderer>().material;
            ++index1;
          }
        }
      }
    }
    this.meshFilters = meshFilterArray;
    this.DoIt();
  }

  private void DoIt()
  {
    if (this.meshFilters.Length == 0)
    {
      if (!((Object) this.smr != (Object) null))
        return;
      this.UnOptimize();
    }
    else
    {
      int length1 = 0;
      int length2 = 0;
      int length3 = 0;
      int length4 = 0;
      foreach (MeshFilter meshFilter in this.meshFilters)
      {
        length1 += meshFilter.mesh.vertices.Length;
        length2 += meshFilter.mesh.normals.Length;
        length3 += meshFilter.mesh.triangles.Length;
        length4 += meshFilter.mesh.uv.Length;
        if ((Object) this.material == (Object) null)
          this.material = meshFilter.gameObject.GetComponent<Renderer>().material;
      }
      Vector3[] vector3Array1 = new Vector3[length1];
      Vector3[] vector3Array2 = new Vector3[length2];
      Transform[] transformArray = new Transform[this.meshFilters.Length];
      Matrix4x4[] matrix4x4Array = new Matrix4x4[this.meshFilters.Length];
      BoneWeight[] boneWeightArray = new BoneWeight[length1];
      int[] numArray = new int[length3];
      Vector2[] vector2Array = new Vector2[length4];
      int index1 = 0;
      int num1 = 0;
      int num2 = 0;
      int num3 = 0;
      int index2 = 0;
      foreach (MeshFilter meshFilter in this.meshFilters)
      {
        foreach (int triangle in meshFilter.mesh.triangles)
          numArray[num2++] = triangle + index1;
        transformArray[index2] = meshFilter.transform;
        matrix4x4Array[index2] = Matrix4x4.identity;
        foreach (Vector3 vertex in meshFilter.mesh.vertices)
        {
          boneWeightArray[index1].weight0 = 1f;
          boneWeightArray[index1].boneIndex0 = index2;
          vector3Array1[index1++] = vertex;
        }
        foreach (Vector3 normal in meshFilter.mesh.normals)
          vector3Array2[num1++] = normal;
        foreach (Vector2 vector2 in meshFilter.mesh.uv)
          vector2Array[num3++] = vector2;
        ++index2;
        MeshRenderer meshRenderer = meshFilter.gameObject.GetComponent(typeof (MeshRenderer)) as MeshRenderer;
        if ((bool) ((Object) meshRenderer))
          meshRenderer.enabled = false;
      }
      Mesh mesh = new Mesh();
      mesh.vertices = vector3Array1;
      mesh.normals = vector3Array2;
      mesh.boneWeights = boneWeightArray;
      mesh.uv = vector2Array;
      mesh.triangles = numArray;
      mesh.bindposes = matrix4x4Array;
      if ((bool) ((Object) this.smr))
      {
        Object.DestroyImmediate((Object) this.smr.sharedMesh);
        Object.DestroyImmediate((Object) this.smr);
      }
      if ((Object) this.go == (Object) null)
        this.go = new GameObject();
      if ((Object) this.go != (Object) null)
        this.smr = this.go.AddComponent(typeof (SkinnedMeshRenderer)) as SkinnedMeshRenderer;
      if (!(bool) ((Object) this.smr))
      {
        Debug.Log((object) "CANNOT ADD SkinnedMesh!");
      }
      else
      {
        this.smr.sharedMesh = mesh;
        this.smr.bones = transformArray;
        this.smr.GetComponent<Renderer>().material = this.material;
        this.needToOptimize = false;
      }
    }
  }
}
