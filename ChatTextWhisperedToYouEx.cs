﻿// Decompiled with JetBrains decompiler
// Type: ChatTextWhisperedToYouEx
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

internal class ChatTextWhisperedToYouEx : ChatTextWhisperedToYou
{
  public uint roomId;

  public override bool TryParse(string textMessage)
  {
    string[] strArray = textMessage.Split(new char[3]{ '%', '@', '#' });
    if (strArray.Length != 5 || strArray[0] != "cv")
      return false;
    this.userName = strArray[1];
    this.text = strArray[2];
    if (!uint.TryParse(strArray[3], out this.roomId))
      Log.Add("Invalid roomId received:", new object[1]
      {
        (object) strArray[3]
      });
    return true;
  }
}
