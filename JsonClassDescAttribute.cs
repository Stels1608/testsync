﻿// Decompiled with JetBrains decompiler
// Type: JsonClassDescAttribute
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class JsonClassDescAttribute : JsonFieldAttribute
{
  public string FieldName;

  public JsonClassDescAttribute(string fieldName)
  {
    this.FieldName = fieldName;
  }
}
