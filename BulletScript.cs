﻿// Decompiled with JetBrains decompiler
// Type: BulletScript
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BulletScript : MonoBehaviour
{
  private BulletScript.State state;
  public Transform Target;
  public float Speed;
  public float PursuitTime;
  public bool Miss;
  public Vector3 Offset;
  public float FreeFlightInterval;
  private Vector3 initPosition;
  private Vector3 lastPosition;
  private float rateCoef;
  private float delta;
  private float removalTime;
  public BulletScript.DestroyedEventHandler Destroyed;
  public BulletScript.HitEventHandler Hit;
  private Transform _transform;

  private void Awake()
  {
    this.enabled = false;
    this._transform = this.transform;
  }

  public void Go()
  {
    this.initPosition = this._transform.position;
    this.lastPosition = this.initPosition;
    this.rateCoef = 1f / this.PursuitTime;
    this.delta = 0.0f;
    this.state = BulletScript.State.Pursuit;
    this.enabled = true;
  }

  public void UpdateBulletPosition()
  {
    if (this.state == BulletScript.State.Pursuit)
    {
      if ((Object) this.Target != (Object) null)
      {
        if ((double) this.delta < 1.0)
        {
          this.lastPosition = this._transform.position;
          this._transform.position = Vector3.Lerp(this.initPosition, this.Target.position + this.Offset, this.delta);
          this.delta += Time.deltaTime * this.rateCoef;
        }
        else if (this.Miss)
        {
          Vector3 forward = this._transform.position - this.lastPosition;
          if ((double) forward.sqrMagnitude > 1.0 / 1000.0)
            this._transform.rotation = Quaternion.LookRotation(forward);
          this.state = BulletScript.State.FreeFlight;
          this.removalTime = Time.time + this.FreeFlightInterval;
        }
        else
          this.state = BulletScript.State.Hit;
      }
      else
      {
        this.state = BulletScript.State.FreeFlight;
        this.removalTime = Time.time + this.FreeFlightInterval;
      }
    }
    if (this.state == BulletScript.State.FreeFlight)
    {
      this._transform.position += this._transform.forward * this.Speed * Time.deltaTime;
      if ((double) Time.time >= (double) this.removalTime)
        this.state = BulletScript.State.Removal;
    }
    if (this.state == BulletScript.State.Hit)
    {
      this.OnHit();
      this.state = BulletScript.State.Removal;
    }
    if (this.state != BulletScript.State.Removal)
      return;
    this.OnDestroyed();
    this.enabled = false;
  }

  private void LateUpdate()
  {
    this.UpdateBulletPosition();
  }

  protected virtual void OnDestroyed()
  {
    if (this.Destroyed == null)
      return;
    this.Destroyed(this);
  }

  protected virtual void OnHit()
  {
    if (this.Hit == null)
      return;
    this.Hit(this);
  }

  public Vector3 GetDirection()
  {
    return (this._transform.position - this.lastPosition).normalized;
  }

  private enum State
  {
    Pursuit,
    FreeFlight,
    Hit,
    Removal,
  }

  public delegate void DestroyedEventHandler(BulletScript bullet);

  public delegate void HitEventHandler(BulletScript bullet);
}
