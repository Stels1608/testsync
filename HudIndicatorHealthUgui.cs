﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorHealthUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;
using UnityEngine.UI;

public class HudIndicatorHealthUgui : HudIndicatorBaseComponentUgui, IReceivesSetting, IReceivesSpaceObject, IRequiresLateUpdate
{
  private bool enabledInSettings = true;
  [SerializeField]
  private Image healthbar_fg;
  [SerializeField]
  private Image healthbar_bg;
  [SerializeField]
  private Image filler_bar;
  private SpaceObject spaceObject;

  protected override void Awake()
  {
    base.Awake();
  }

  protected override void ResetComponentStates()
  {
  }

  protected override void SetColoring()
  {
    this.healthbar_fg.color = HudIndicatorColorScheme.Instance.HealthBarOutlineColor((ISpaceEntity) this.spaceObject);
    this.filler_bar.color = HudIndicatorColorScheme.Instance.HealthBarColor((ISpaceEntity) this.spaceObject);
  }

  public override bool RequiresScreenBorderClamping()
  {
    return false;
  }

  protected override void UpdateView()
  {
    this.gameObject.SetActive(this.IsSelected && this.InScreen && this.enabledInSettings);
  }

  public void OnSpaceObjectInjection(SpaceObject target)
  {
    this.spaceObject = target;
    this.UpdateColorAndAlpha();
  }

  public void RemoteLateUpdate()
  {
    this.UpdateHealthbar();
  }

  private void UpdateHealthbar()
  {
    float num = !this.spaceObject.IsSubscribed ? 1f : this.spaceObject.Props.HullPoints / this.spaceObject.Props.MaxHullPoints;
    if ((double) Math.Abs(this.filler_bar.fillAmount - num) <= (double) Mathf.Epsilon)
      return;
    this.filler_bar.fillAmount = num;
  }

  public void ApplyOptionsSetting(UserSetting userSetting, object data)
  {
    switch (userSetting)
    {
      case UserSetting.HudIndicatorColorScheme:
        this.SetColoring();
        break;
      case UserSetting.HudIndicatorHealthBar:
        this.enabledInSettings = (bool) data;
        this.UpdateView();
        break;
    }
  }
}
