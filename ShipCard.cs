﻿// Decompiled with JetBrains decompiler
// Type: ShipCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using CouchDB;
using Paperdolls;
using Paperdolls.PaperdollServerData;
using System.Collections.Generic;
using UnityEngine;

public class ShipCard : GameItemCard
{
  public int ParentHangarID = -1;
  private const int INVALID = -1;
  public WorldCard WorldCard;
  public uint ShipObjectKey;
  public ShipRole[] ShipRoles;
  public byte HangarID;
  public byte Level;
  public byte MaxLevel;
  public byte LevelRequirement;
  public ShipCard NextCard;
  public float Durability;
  public string PaperdollUiLayoutfile;
  public List<ShipSlotCard> Slots;
  public List<ShipImmutableSlot> ImmutableSlots;
  public Paperdolls.PaperdollLayoutSmall.PaperdollLayoutSmall PaperdollLayoutSmall;
  public Paperdolls.PaperdollLayoutBig.PaperdollLayoutBig PaperdollLayoutBig;
  public byte Tier;
  public ShipRoleDeprecated ShipRoleDeprecated;
  public bool CubitOnlyRepair;
  public List<uint> VariantHangarIDs;
  public ObjectStats Stats;
  public Faction Faction;
  public RewardCard UpgradeRewardCard;

  public HangarShip GetHangarShip
  {
    get
    {
      HangarShip hangarShip = (HangarShip) null;
      foreach (HangarShip ship in Game.Me.Hangar.Ships)
      {
        if ((int) this.HangarID == (int) ship.ServerID)
          hangarShip = ship;
      }
      return hangarShip;
    }
  }

  public bool HaveShip
  {
    get
    {
      return this.GetHangarShip != null;
    }
  }

  public bool IsUpgraded
  {
    get
    {
      return (int) this.Level == (int) this.MaxLevel;
    }
  }

  public bool HasParent
  {
    get
    {
      return this.ParentHangarID != -1;
    }
  }

  public ShipCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public bool Equals(ShipCard obj)
  {
    if (this == null || obj == null || ((int) this.Tier != (int) obj.Tier || this.ShipRoleDeprecated != obj.ShipRoleDeprecated))
      return false;
    return (int) this.HangarID == (int) obj.HangarID;
  }

  public bool HasVariant(uint index)
  {
    return this.VariantHangarIDs != null && (long) this.VariantHangarIDs.Count > (long) index;
  }

  public int RequiredPlayerLevel()
  {
    return (int) this.LevelRequirement;
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.ShipObjectKey = r.ReadGUID();
    this.Level = r.ReadByte();
    this.MaxLevel = r.ReadByte();
    this.LevelRequirement = r.ReadByte();
    this.HangarID = r.ReadByte();
    uint cardGUID = r.ReadGUID();
    this.Durability = r.ReadSingle();
    this.Tier = r.ReadByte();
    int length = r.ReadLength();
    this.ShipRoles = new ShipRole[length];
    for (int index = 0; index < length; ++index)
      this.ShipRoles[index] = (ShipRole) r.ReadByte();
    this.ShipRoleDeprecated = (ShipRoleDeprecated) r.ReadByte();
    this.PaperdollUiLayoutfile = r.ReadString();
    this.Slots = r.ReadDescList<ShipSlotCard>();
    this.CubitOnlyRepair = r.ReadBoolean();
    this.VariantHangarIDs = r.ReadUInt32List();
    this.ParentHangarID = r.ReadInt32();
    this.Stats = r.ReadDesc<ObjectStats>();
    this.Faction = (Faction) r.ReadByte();
    this.ImmutableSlots = r.ReadDescList<ShipImmutableSlot>();
    int num = (int) r.ReadGUID();
    this.WorldCard = (WorldCard) Game.Catalogue.FetchCard(this.CardGUID, CardView.World);
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.WorldCard
    });
    if ((int) cardGUID != 0)
    {
      this.NextCard = (ShipCard) Game.Catalogue.FetchCard(cardGUID, CardView.Ship);
      this.IsLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) this.NextCard
      });
    }
    if (!string.IsNullOrEmpty(this.PaperdollUiLayoutfile))
    {
      TextAsset textAsset = Resources.Load(PaperdollLayouts.PaperdollLayoutPath + this.PaperdollUiLayoutfile, typeof (TextAsset)) as TextAsset;
      if ((Object) textAsset == (Object) null)
        Debug.LogError((object) ("Couldn't load json paperdoll layout file Layout: " + PaperdollLayouts.PaperdollLayoutPath + this.PaperdollUiLayoutfile + " ||| " + this.ToString()));
      PaperdollLayouts paperdollLayouts = JsonSerializator.Deserialize<PaperdollLayouts>(ContentDB.GetDocumentRawFromTextAsset(textAsset, this.PaperdollUiLayoutfile));
      this.PaperdollLayoutBig = paperdollLayouts.PaperdollLayoutBig;
      this.PaperdollLayoutSmall = paperdollLayouts.PaperdollLayoutSmall;
    }
    this.ItemGUICard.IsLoaded.AddHandler((SignalHandler) (() => this.ItemGUICard.Level = this.Level));
    this.IsLoaded.Set();
  }

  public override string ToString()
  {
    return "ShipCard: Level = " + (object) this.Level + ", MaxLevel = " + (object) this.MaxLevel + ", HangarID = " + (object) this.HangarID + ", Durability = " + (object) this.Durability + ", Tier = " + (object) this.Tier + ", Role = " + (object) this.ShipRoleDeprecated + ", PaperdollUiLayoutFile = " + this.PaperdollUiLayoutfile + ", Slots(Count) = " + (object) this.Slots.Count + ", CubitOnlyRepair = " + (object) this.CubitOnlyRepair + ", VariantHangarId = " + (object) this.VariantHangarIDs + ", ParentHangarId = " + (object) this.ParentHangarID + ", Stats = " + this.Stats.ToString() + ", Faction = " + (object) this.Faction + ", WorldCard = " + (object) this.WorldCard;
  }
}
