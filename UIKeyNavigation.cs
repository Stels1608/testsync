﻿// Decompiled with JetBrains decompiler
// Type: UIKeyNavigation
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[AddComponentMenu("NGUI/Interaction/Key Navigation")]
public class UIKeyNavigation : MonoBehaviour
{
  public static BetterList<UIKeyNavigation> list = new BetterList<UIKeyNavigation>();
  public UIKeyNavigation.Constraint constraint;
  public GameObject onUp;
  public GameObject onDown;
  public GameObject onLeft;
  public GameObject onRight;
  public GameObject onClick;
  public bool startsSelected;

  protected virtual void OnEnable()
  {
    UIKeyNavigation.list.Add(this);
    if (!this.startsSelected || !((Object) UICamera.selectedObject == (Object) null) && NGUITools.GetActive(UICamera.selectedObject))
      return;
    UICamera.currentScheme = UICamera.ControlScheme.Controller;
    UICamera.selectedObject = this.gameObject;
  }

  protected virtual void OnDisable()
  {
    UIKeyNavigation.list.Remove(this);
  }

  protected GameObject GetLeft()
  {
    if (NGUITools.GetActive(this.onLeft))
      return this.onLeft;
    if (this.constraint == UIKeyNavigation.Constraint.Vertical || this.constraint == UIKeyNavigation.Constraint.Explicit)
      return (GameObject) null;
    return this.Get(Vector3.left, true);
  }

  private GameObject GetRight()
  {
    if (NGUITools.GetActive(this.onRight))
      return this.onRight;
    if (this.constraint == UIKeyNavigation.Constraint.Vertical || this.constraint == UIKeyNavigation.Constraint.Explicit)
      return (GameObject) null;
    return this.Get(Vector3.right, true);
  }

  protected GameObject GetUp()
  {
    if (NGUITools.GetActive(this.onUp))
      return this.onUp;
    if (this.constraint == UIKeyNavigation.Constraint.Horizontal || this.constraint == UIKeyNavigation.Constraint.Explicit)
      return (GameObject) null;
    return this.Get(Vector3.up, false);
  }

  protected GameObject GetDown()
  {
    if (NGUITools.GetActive(this.onDown))
      return this.onDown;
    if (this.constraint == UIKeyNavigation.Constraint.Horizontal || this.constraint == UIKeyNavigation.Constraint.Explicit)
      return (GameObject) null;
    return this.Get(Vector3.down, false);
  }

  protected GameObject Get(Vector3 myDir, bool horizontal)
  {
    Transform transform = this.transform;
    myDir = transform.TransformDirection(myDir);
    Vector3 center = UIKeyNavigation.GetCenter(this.gameObject);
    float num = float.MaxValue;
    GameObject gameObject = (GameObject) null;
    for (int index = 0; index < UIKeyNavigation.list.size; ++index)
    {
      UIKeyNavigation uiKeyNavigation = UIKeyNavigation.list[index];
      if (!((Object) uiKeyNavigation == (Object) this))
      {
        UIButton component = uiKeyNavigation.GetComponent<UIButton>();
        if (!((Object) component != (Object) null) || component.isEnabled)
        {
          Vector3 direction = UIKeyNavigation.GetCenter(uiKeyNavigation.gameObject) - center;
          if ((double) Vector3.Dot(myDir, direction.normalized) >= 0.707000017166138)
          {
            Vector3 vector3 = transform.InverseTransformDirection(direction);
            if (horizontal)
              vector3.y *= 2f;
            else
              vector3.x *= 2f;
            float sqrMagnitude = vector3.sqrMagnitude;
            if ((double) sqrMagnitude <= (double) num)
            {
              gameObject = uiKeyNavigation.gameObject;
              num = sqrMagnitude;
            }
          }
        }
      }
    }
    return gameObject;
  }

  protected static Vector3 GetCenter(GameObject go)
  {
    UIWidget component = go.GetComponent<UIWidget>();
    if (!((Object) component != (Object) null))
      return go.transform.position;
    Vector3[] worldCorners = component.worldCorners;
    return (worldCorners[0] + worldCorners[2]) * 0.5f;
  }

  protected virtual void OnKey(KeyCode key)
  {
    if (!NGUITools.GetActive((Behaviour) this))
      return;
    GameObject gameObject = (GameObject) null;
    switch (key)
    {
      case KeyCode.UpArrow:
        gameObject = this.GetUp();
        break;
      case KeyCode.DownArrow:
        gameObject = this.GetDown();
        break;
      case KeyCode.RightArrow:
        gameObject = this.GetRight();
        break;
      case KeyCode.LeftArrow:
        gameObject = this.GetLeft();
        break;
      case KeyCode.Tab:
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
          gameObject = this.GetLeft();
          if ((Object) gameObject == (Object) null)
            gameObject = this.GetUp();
          if ((Object) gameObject == (Object) null)
            gameObject = this.GetDown();
          if ((Object) gameObject == (Object) null)
          {
            gameObject = this.GetRight();
            break;
          }
          break;
        }
        gameObject = this.GetRight();
        if ((Object) gameObject == (Object) null)
          gameObject = this.GetDown();
        if ((Object) gameObject == (Object) null)
          gameObject = this.GetUp();
        if ((Object) gameObject == (Object) null)
        {
          gameObject = this.GetLeft();
          break;
        }
        break;
    }
    if (!((Object) gameObject != (Object) null))
      return;
    UICamera.selectedObject = gameObject;
  }

  protected virtual void OnClick()
  {
    if (!NGUITools.GetActive((Behaviour) this) || !NGUITools.GetActive(this.onClick))
      return;
    UICamera.selectedObject = this.onClick;
  }

  public enum Constraint
  {
    None,
    Vertical,
    Horizontal,
    Explicit,
  }
}
