﻿// Decompiled with JetBrains decompiler
// Type: HudIndicatorManagerUgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HudIndicatorManagerUgui : HudIndicatorManagerBase
{
  public readonly RectTransform hudIndicatorRoot;
  private readonly CanvasScaler canvasScaler;
  public readonly Canvas canvas;
  public GraphicRaycaster raycaster;
  public static HudIndicatorManagerUgui CurrentManager;

  public override bool IsRendered
  {
    get
    {
      return this.canvas.enabled;
    }
    set
    {
      if (this.canvas.enabled == value)
        return;
      this.canvas.enabled = value;
    }
  }

  public HudIndicatorManagerUgui(Canvas canvas)
  {
    this.canvas = canvas;
    this.hudIndicatorRoot = canvas.gameObject.transform as RectTransform;
    this.raycaster = canvas.gameObject.GetComponent<GraphicRaycaster>();
    this.raycaster.enabled = false;
  }

  public static HudIndicatorManagerUgui CreateInstance()
  {
    HudIndicatorManagerUgui indicatorManagerUgui = new HudIndicatorManagerUgui(UguiTools.GetCanvas(BsgoCanvas.HudIndicators));
    HudIndicatorManagerUgui.CurrentManager = indicatorManagerUgui;
    return indicatorManagerUgui;
  }

  protected override HudIndicatorBase CreateIndicatorForMyUiSystem(ISpaceEntity target)
  {
    return HudIndicatorFactoryUgui.CreateIndicator(target, this.hudIndicatorRoot.gameObject);
  }

  public GameObject GetUiRaycastTarget()
  {
    PointerEventData eventData = new PointerEventData(EventSystem.current);
    eventData.position = (Vector2) Input.mousePosition;
    List<RaycastResult> resultAppendList = new List<RaycastResult>();
    this.raycaster.Raycast(eventData, resultAppendList);
    if (resultAppendList.Count > 0)
      return resultAppendList[0].gameObject;
    return (GameObject) null;
  }

  public void RemoteLateUpdate()
  {
    using (Dictionary<ISpaceEntity, HudIndicatorBase>.Enumerator enumerator = this.IndicatorMapping.GetEnumerator())
    {
      while (enumerator.MoveNext())
        enumerator.Current.Value.RemoteLateUpdate();
    }
  }
}
