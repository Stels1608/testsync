﻿// Decompiled with JetBrains decompiler
// Type: AudioPool
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class AudioPool
{
  private List<AudioSource> audios = new List<AudioSource>();
  private GameObject gameObject;
  private int capacity;

  public AudioPool(GameObject gameObject, int capacity)
  {
    this.gameObject = gameObject;
    this.capacity = capacity;
  }

  public AudioSource GetAudio()
  {
    using (List<AudioSource>.Enumerator enumerator = this.audios.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        AudioSource current = enumerator.Current;
        if (!current.isPlaying)
          return current;
      }
    }
    if (this.audios.Count >= this.capacity)
      return (AudioSource) null;
    AudioSource audioSource = this.gameObject.AddComponent<AudioSource>();
    audioSource.spatialBlend = 1f;
    this.audios.Add(audioSource);
    return audioSource;
  }
}
