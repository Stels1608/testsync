﻿// Decompiled with JetBrains decompiler
// Type: CameraSwitcherCharacterScene
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class CameraSwitcherCharacterScene
{
  private Camera activeCamera;
  private readonly int cullingMaskMainCam;

  public CameraSwitcherCharacterScene(Camera mainCam)
  {
    this.activeCamera = mainCam;
    this.cullingMaskMainCam = mainCam.cullingMask;
  }

  public void SetActiveCamera(Camera camera)
  {
    try
    {
      if ((UnityEngine.Object) this.activeCamera == (UnityEngine.Object) Camera.main)
      {
        this.activeCamera.cullingMask = 0;
        this.activeCamera.GetComponent<AudioListener>().enabled = false;
      }
      else
        this.activeCamera.enabled = false;
      this.activeCamera = camera;
      if ((UnityEngine.Object) this.activeCamera == (UnityEngine.Object) Camera.main)
      {
        this.activeCamera.cullingMask = this.cullingMaskMainCam;
        this.activeCamera.GetComponent<AudioListener>().enabled = true;
      }
      else
        this.activeCamera.enabled = true;
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ("Set active cam exception: " + (object) ex));
    }
  }

  public Camera GetActiveCamera()
  {
    return this.activeCamera;
  }
}
