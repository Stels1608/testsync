﻿// Decompiled with JetBrains decompiler
// Type: CameraCard
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public class CameraCard : Card
{
  public float DefaultZoom = 1f;
  public float MinZoom = 10f;
  public float MaxZoom = 20f;
  public float SoftTrembleSpeed = 1f;
  public float HardTrembleSpeed = 1f;

  public CameraCard(uint cardGUID)
    : base(cardGUID)
  {
  }

  public override void Read(BgoProtocolReader r)
  {
    this.DefaultZoom = r.ReadSingle();
    this.MaxZoom = r.ReadSingle();
    this.MinZoom = r.ReadSingle();
    this.SoftTrembleSpeed = r.ReadSingle();
    this.HardTrembleSpeed = r.ReadSingle();
  }

  public override string ToString()
  {
    return string.Format("DefaultZoom: {0}, MinZoom: {1}, MaxZoom: {2}, SoftTrembleSpeed: {3}, HardTrembleSpeed: {4}", (object) this.DefaultZoom, (object) this.MinZoom, (object) this.MaxZoom, (object) this.SoftTrembleSpeed, (object) this.HardTrembleSpeed);
  }
}
