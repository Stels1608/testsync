﻿// Decompiled with JetBrains decompiler
// Type: Avatar
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity5AssetHandling;
using UnityEngine;

public class Avatar
{
  private readonly List<AvatarProperty> waitProperty = new List<AvatarProperty>();
  public GameObject obj;
  public System.Action loadedCallback;
  protected string race;
  protected string sex;
  private Vector3 initPos;
  private Quaternion initRot;
  protected AvatarPropertyManager propManager;
  private AssetRequest objRequest;
  private bool isLock;
  private bool isLoaded;
  private bool destroyed;

  public string Race
  {
    get
    {
      return this.race;
    }
  }

  public string Sex
  {
    get
    {
      return this.sex;
    }
  }

  public bool IsReady
  {
    get
    {
      if (this.isLoaded)
        return this.propManager.IsReady;
      return false;
    }
  }

  public void Destroy()
  {
    this.destroyed = true;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.obj);
  }

  public bool Init(string race, string sex, Vector3 position, Quaternion rotation)
  {
    this.race = race;
    this.sex = sex;
    this.initPos = position;
    this.initRot = rotation;
    this.propManager = new AvatarPropertyManager(this);
    this.objRequest = Avatar.LoadObj(race, sex);
    return true;
  }

  public static AssetRequest LoadObj(string race, string sex)
  {
    return AssetCatalogue.Instance.Request("avatar_" + race + "_" + sex + ".prefab", false);
  }

  protected virtual void OnAvatarLoaded()
  {
    if ((UnityEngine.Object) this.obj.GetComponent<Turntable>() == (UnityEngine.Object) null)
      this.obj.AddComponent<Turntable>();
    Game.Instance.StartCoroutine(this.WaitForReady());
  }

  [DebuggerHidden]
  private IEnumerator WaitForReady()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new Avatar.\u003CWaitForReady\u003Ec__IteratorB() { \u003C\u003Ef__this = this };
  }

  public void Update()
  {
    if (this.objRequest != null && this.objRequest.IsDone)
    {
      if (this.objRequest.Asset == (UnityEngine.Object) null)
        Log.Add(string.Format("ERROR: Avatar is null. race {0}, sex {1}", (object) this.race.ToString().ToLower(), (object) this.sex));
      this.obj = (GameObject) UnityEngine.Object.Instantiate(this.objRequest.Asset, new Vector3(999f, 999f, 999f), this.initRot);
      this.OnAvatarLoaded();
      this.UseWaitProperties();
      this.objRequest = (AssetRequest) null;
    }
    if ((UnityEngine.Object) this.obj == (UnityEngine.Object) null || this.isLock)
      return;
    if (this.isLoaded && this.waitProperty.Count > 0)
      this.UseWaitProperties();
    if (!this.propManager.IsReady)
      return;
    this.propManager.Use();
    if (this.isLoaded)
      return;
    this.isLoaded = true;
  }

  private void UseWaitProperties()
  {
    this.SetProperties(this.waitProperty);
    this.waitProperty.Clear();
  }

  public void SetProperties(List<AvatarProperty> properties)
  {
    if ((UnityEngine.Object) this.obj == (UnityEngine.Object) null)
      this.waitProperty.AddRange((IEnumerable<AvatarProperty>) properties);
    else
      this.propManager.AddProperties(properties);
  }

  public virtual void ParseProperty(AvatarProperty prop)
  {
  }

  public void SetProperty(string prop, string value)
  {
    AvatarProperty avatarProperty = new AvatarProperty(prop, value);
    if (this.propManager.GetType(prop) == AvatarPropertyType.Object)
      this.SetProperties(new List<AvatarProperty>()
      {
        avatarProperty
      });
    else
      this.waitProperty.Add(avatarProperty);
  }

  public string GetProperty(string prop)
  {
    AvatarProperty property = this.propManager.GetProperty(prop);
    if (property != null)
      return property.value;
    return string.Empty;
  }

  public virtual string GetItemNameByTexProp(string texProp)
  {
    return AvatarInfo.GetItemFromTexture(texProp);
  }

  public virtual string GetItemNameByMatProp(string matProp)
  {
    return matProp.Remove(matProp.Length - 1, 1);
  }

  public void Lock()
  {
    this.isLock = true;
  }

  public void Unlock()
  {
    this.isLock = false;
  }

  public virtual bool CheckBadObjectName(string name)
  {
    return false;
  }

  public virtual bool AdditionalObject(string name, AvatarProperty property)
  {
    return false;
  }

  public AvatarDescription GetAvatarDescription(Faction faction)
  {
    AvatarDescription avatarDescription = new AvatarDescription();
    avatarDescription.SetItem(AvatarItem.Race, this.Race);
    avatarDescription.SetItem(AvatarItem.Sex, this.Sex);
    if (faction == Faction.Colonial)
    {
      avatarDescription.SetItem(AvatarItem.HumanHead, this.GetProperty("head"));
      avatarDescription.SetItem(AvatarItem.HumanFace, this.GetProperty(AvatarInfo.GetTextureFromItem("faces")));
      avatarDescription.SetItem(AvatarItem.HumanHands, this.GetProperty(AvatarInfo.GetTextureFromItem("hands")));
      avatarDescription.SetItem(AvatarItem.HumanGlasses, this.GetProperty("glasses"));
      avatarDescription.SetItem(AvatarItem.HumanHelmet, this.GetProperty("helmet"));
      avatarDescription.SetItem(AvatarItem.HumanHair, this.GetProperty("hair"));
      avatarDescription.SetItem(AvatarItem.HumanHairColor, this.GetProperty(AvatarInfo.GetMaterialFromItem("hair")));
      avatarDescription.SetItem(AvatarItem.HumanSuit, this.GetProperty("suit"));
      avatarDescription.SetItem(AvatarItem.HumanBeard, this.GetProperty("beard"));
      avatarDescription.SetItem(AvatarItem.HumanBeardColor, this.GetProperty(AvatarInfo.GetMaterialFromItem("beard")));
    }
    else
    {
      avatarDescription.SetItem(AvatarItem.CylonHead, this.GetProperty("head"));
      avatarDescription.SetItem(AvatarItem.CylonHeadSkin, this.GetProperty(AvatarInfo.GetMaterialFromItem("head")));
      avatarDescription.SetItem(AvatarItem.CylonArms, this.GetProperty("arms"));
      avatarDescription.SetItem(AvatarItem.CylonArmsSkin, this.GetProperty(AvatarInfo.GetMaterialFromItem("arms")));
      avatarDescription.SetItem(AvatarItem.CylonBody, this.GetProperty("body"));
      avatarDescription.SetItem(AvatarItem.CylonBodySkin, this.GetProperty(AvatarInfo.GetMaterialFromItem("body")));
      avatarDescription.SetItem(AvatarItem.CylonLegs, this.GetProperty("legs"));
      avatarDescription.SetItem(AvatarItem.CylonLegsSkin, this.GetProperty(AvatarInfo.GetMaterialFromItem("legs")));
    }
    return avatarDescription;
  }
}
