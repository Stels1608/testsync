﻿// Decompiled with JetBrains decompiler
// Type: ShipSystem
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class ShipSystem : ShipItem
{
  private double timeOfLastUse;
  private float durability;
  public ShipSystemCard Card;
  public ShipSystemPaintCard PaintCard;

  public float Durability
  {
    get
    {
      if ((double) this.durability < 0.0)
        return this.Card.Durability;
      return this.durability;
    }
  }

  public double TimeOfLastUse
  {
    get
    {
      return this.timeOfLastUse;
    }
  }

  public bool IsBroken
  {
    get
    {
      return (double) this.Durability == 0.0;
    }
  }

  public float Quality
  {
    get
    {
      return this.Durability / ((double) this.Card.Durability <= 0.0 ? 1f : this.Card.Durability);
    }
  }

  public CurrencyAmount GetPriceToUpgrade(int targetSystemLevel, bool applyDiscounts)
  {
    CurrencyAmount currencyAmount = new CurrencyAmount();
    if ((int) this.Card.Level == targetSystemLevel)
      return currencyAmount;
    for (int level = (int) this.Card.Level; level < targetSystemLevel; ++level)
    {
      ShipSystemCard nextCardByLevel = this.Card.GetNextCardByLevel(level);
      using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = nextCardByLevel.shopItemCard.UpgradePrice.items.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          KeyValuePair<ShipConsumableCard, float> current = enumerator.Current;
          float levelUpgradePrice = this.getLevelUpgradePrice(nextCardByLevel, level, current.Value, applyDiscounts);
          switch ((ResourceType) current.Key.CardGUID)
          {
            case ResourceType.Token:
              currencyAmount.Token += levelUpgradePrice;
              continue;
            case ResourceType.Titanium:
              currencyAmount.Titanium += levelUpgradePrice;
              continue;
            case ResourceType.Tylium:
              currencyAmount.Tylium += levelUpgradePrice;
              continue;
            case ResourceType.TuningKit:
              currencyAmount.TuningKits += levelUpgradePrice;
              continue;
            case ResourceType.Cubits:
              currencyAmount.Cubits += levelUpgradePrice;
              continue;
            default:
              continue;
          }
        }
      }
    }
    return currencyAmount;
  }

  private float getLevelUpgradePrice(ShipSystemCard card, int level, float price, bool applyDiscounts)
  {
    ShopDiscount upgradeDiscount = Shop.FindUpgradeDiscount(Shop.FindShopCategoryForSlotType(card.SlotType), level);
    if (upgradeDiscount != null && applyDiscounts)
      price = upgradeDiscount.ApplyDiscount(price);
    return price;
  }

  public ShopDiscount FindDiscountInUpgradeLevels()
  {
    for (int level = 0; level < (int) this.Card.MaxLevel; ++level)
    {
      ShopDiscount upgradeDiscount = Shop.FindUpgradeDiscount(Shop.FindShopCategoryForSlotType(this.Card.SlotType), level);
      if (upgradeDiscount != null)
        return upgradeDiscount;
    }
    return (ShopDiscount) null;
  }

  public ShipSystemCard GetMaxLevelCard()
  {
    ShipSystemCard shipSystemCard = this.Card;
    while (shipSystemCard.NextCard != null)
      shipSystemCard = shipSystemCard.NextCard;
    return shipSystemCard;
  }

  public bool IsAnyUpgradeLevelOnSale()
  {
    return null != this.FindDiscountInUpgradeLevels();
  }

  public override void Read(BgoProtocolReader r)
  {
    base.Read(r);
    this.Card = (ShipSystemCard) Game.Catalogue.FetchCard(this.CardGUID, CardView.ShipSystem);
    this.durability = r.ReadSingle();
    this.timeOfLastUse = r.ReadDouble();
    this.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) this.Card
    });
    this.IsLoaded.Set();
    this.Card.IsLoaded.AddHandler((SignalHandler) (() =>
    {
      if (this.Card.SlotType != ShipSlotType.ship_paint)
        return;
      this.PaintCard = (ShipSystemPaintCard) Game.Catalogue.FetchCard(this.CardGUID, CardView.ShipPaint);
      this.IsLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) this.PaintCard
      });
      this.IsLoaded.Set();
    }));
  }

  public void Upgrade(int level)
  {
    PlayerProtocol.GetProtocol().UpgradeSystem(this, (byte) level);
  }

  public void UpgradeSystemByPack(uint packCount)
  {
    PlayerProtocol.GetProtocol().UpgradeSystemByPack(this, packCount);
  }

  public override bool SetItemIcon(ref GuiImage image, Vector2 elementSize)
  {
    if (this.Card.SlotType == ShipSlotType.ship_paint)
    {
      image.Texture = this.ItemGUICard.GUIIconTexture;
      image.SourceRect = new Rect(0.0f, 0.0f, 1f, 1f);
    }
    else
      base.SetItemIcon(ref image, elementSize);
    return (Object) image.Texture != (Object) null;
  }

  public bool MeetsShipRestrictions(uint shipObjectKey, ShipRole[] shipRoles)
  {
    ShipSystem shipSystem = this;
    if (shipSystem.Card.ShipObjectKeyRestrictions.Count > 0 && !shipSystem.Card.ShipObjectKeyRestrictions.Contains(shipObjectKey))
      return false;
    ShipRole[] shipRoleArray = shipSystem.Card.ShipRoleRestrictions;
    if (shipRoleArray.Length == 0)
      return true;
    for (int index1 = 0; index1 < shipRoleArray.Length; ++index1)
    {
      for (int index2 = 0; index2 < shipRoles.Length; ++index2)
      {
        if (shipRoleArray[index1] == shipRoles[index2])
          return true;
      }
    }
    return false;
  }
}
