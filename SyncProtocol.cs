﻿// Decompiled with JetBrains decompiler
// Type: SyncProtocol
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;

public class SyncProtocol : BgoProtocol
{
  private TimeSync sync;

  public SyncProtocol(TimeSync sync)
    : base(BgoProtocol.ProtocolID.Sync)
  {
    this.sync = sync;
  }

  public void SendSyncRequest()
  {
    BgoProtocolWriter bw = this.NewMessage();
    bw.Write((short) 0);
    this.SendMessage(bw);
  }

  public override void ParseMessage(ushort msgType, BgoProtocolReader br)
  {
    SyncProtocol.Reply reply = (SyncProtocol.Reply) msgType;
    this.DebugAddMsg((Enum) reply);
    if (reply == SyncProtocol.Reply.SyncReply)
      this.sync.ServerReply(br.ReadInt64());
    else
      DebugUtility.LogError("Unknown MessageType in Sync protocol: " + (object) reply);
  }

  public static SyncProtocol GetProtocol()
  {
    return Game.GetProtocolManager().GetProtocol(BgoProtocol.ProtocolID.Sync) as SyncProtocol;
  }

  public enum Request : ushort
  {
    SyncRequest,
  }

  public enum Reply : ushort
  {
    SyncReply = 1,
  }
}
