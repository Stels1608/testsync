﻿// Decompiled with JetBrains decompiler
// Type: QueueHorizontalWithRows
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;

public class QueueHorizontalWithRows : QueueHorizontal
{
  private int rowMargin = 55;
  private int startingHeight = -32;

  [Gui2Editor]
  public new float Space
  {
    get
    {
      return this.space;
    }
    set
    {
      this.space = value;
      this.ForceReposition();
    }
  }

  public QueueHorizontalWithRows()
  {
    this.space = 3f;
    this.RequireBackRepositioning = true;
  }

  public override void Reposition()
  {
    this.RepositionElements();
  }

  protected new virtual void RepositionElements()
  {
    float num1 = 0.0f;
    int num2 = 0;
    using (List<GuiElementBase>.Enumerator enumerator = this.ChildrenVisible.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GuiElementBase current = enumerator.Current;
        if ((double) num1 + (double) current.Size.x + (double) this.space >= (double) this.Size.x)
        {
          num1 = 0.0f;
          ++num2;
        }
        current.PositionX = num1;
        float num3 = num1 + current.Size.x;
        current.PositionY = (float) (num2 * this.rowMargin + this.startingHeight);
        num1 = num3 + this.space;
      }
    }
  }

  public override void AddChild(GuiElementBase ch)
  {
    base.AddChild(ch);
    this.RepositionElements();
  }

  public override void RemoveChild(GuiElementBase ch)
  {
    base.RemoveChild(ch);
    this.RepositionElements();
  }
}
