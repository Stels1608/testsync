﻿// Decompiled with JetBrains decompiler
// Type: JsonVector3
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class JsonVector3
{
  public float x;
  public float y;
  public float z;

  public JsonVector3()
  {
    this.x = 0.0f;
    this.y = 0.0f;
    this.z = 0.0f;
  }

  public Vector3 ToV3()
  {
    return new Vector3(this.x, this.y, this.z);
  }

  public override string ToString()
  {
    return string.Format("X: {0}, Y: {1}, Z: {2}", (object) this.x, (object) this.y, (object) this.z);
  }
}
