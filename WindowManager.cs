﻿// Decompiled with JetBrains decompiler
// Type: WindowManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class WindowManager
{
  private static WindowManager instance;
  private readonly List<Window> registeredWindows;

  public static WindowManager Instance
  {
    get
    {
      return WindowManager.instance ?? (WindowManager.instance = new WindowManager());
    }
  }

  public WindowManager()
  {
    this.registeredWindows = new List<Window>();
  }

  public void RegisterWindow(Window window)
  {
    this.registeredWindows.Add(window);
  }

  public void UnregisterWindow(Window window)
  {
    this.registeredWindows.Remove(window);
  }

  public T Find<T>() where T : Window
  {
    using (List<Window>.Enumerator enumerator = this.registeredWindows.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Window current = enumerator.Current;
        if (current is T)
          return (T) current;
      }
    }
    return (T) null;
  }

  public IList<T> FindAll<T>() where T : Window
  {
    IList<T> objList = (IList<T>) new List<T>();
    using (List<Window>.Enumerator enumerator = this.registeredWindows.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Window current = enumerator.Current;
        if (current is T)
          objList.Add((T) current);
      }
    }
    return objList;
  }
}
