﻿// Decompiled with JetBrains decompiler
// Type: BallisticProjectile
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BallisticProjectile : AbstractProjectile
{
  private float autoTargetTimePreSpeed = 100f;
  private float _oldDistance = float.MaxValue;
  private float totalTime;
  private float time;
  private Vector3 startDirection;
  private float maxRandomFirecone;
  private float autoTargetTime;
  private bool IsAutoTargetting;

  public float MaxRandomFirecone
  {
    get
    {
      return this.maxRandomFirecone;
    }
    set
    {
      this.maxRandomFirecone = value;
    }
  }

  public float AutoTargetTime
  {
    get
    {
      return this.autoTargetTime;
    }
    set
    {
      this.autoTargetTime = value;
    }
  }

  public float AutoTargetTimePreSpeed
  {
    get
    {
      return this.autoTargetTimePreSpeed;
    }
    set
    {
      this.autoTargetTimePreSpeed = value;
    }
  }

  protected override void OnUpdate()
  {
    if ((Object) this.target == (Object) null)
    {
      Object.Destroy((Object) this.gameObject);
    }
    else
    {
      this.time += Time.deltaTime;
      Vector3 position = this.transform.position;
      if ((double) this.time > (double) this.autoTargetTime || this.IsAutoTargetting)
      {
        if (!this.IsAutoTargetting)
        {
          this.IsAutoTargetting = true;
          this.time = 0.0f;
          this.start = this.transform.position;
          this.totalTime = Vector3.Distance(this.start, this.target.position) / this.speed;
        }
        float num = Vector3.Distance(this.transform.position, this.target.position);
        if ((double) num < 8.0 || (double) num > (double) this._oldDistance)
        {
          this.Impact();
          return;
        }
        this._oldDistance = num;
        this.transform.position += Vector3.Lerp(this.startDirection, (this.target.position - this.transform.position).normalized, this.time / 1f) * this.speed * Time.deltaTime;
      }
      else
        this.transform.position = this.start + this.startDirection * (this.time * this.autoTargetTimePreSpeed);
      if ((double) (this.transform.position - position).sqrMagnitude <= 9.99999974737875E-05)
        return;
      this.transform.forward = this.transform.position - position;
    }
  }

  protected override void OnSetTarget(Transform target)
  {
    this.startDirection = (target.position - this.transform.position).normalized;
    this.startDirection = Quaternion.Euler(new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f) * this.maxRandomFirecone) * this.startDirection;
  }

  private void Impact()
  {
    Object.Destroy((Object) this.gameObject);
    this.enabled = false;
    if (!((Object) this.impact != (Object) null))
      return;
    (Object.Instantiate((Object) this.impact, !((Object) this.target != (Object) null) ? this.transform.position : this.target.position, Quaternion.identity) as GameObject).transform.parent = ProjectileForge.ProjectileForgeRoot();
  }
}
