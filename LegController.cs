﻿// Decompiled with JetBrains decompiler
// Type: LegController
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LegController : MonoBehaviour
{
  public float groundPlaneHeight;
  public AnimationClip groundedPose;
  public Transform rootBone;
  public LegInfo[] legs;
  public MotionAnalyzer[] sourceAnimations;
  [HideInInspector]
  public bool initialized;
  [HideInInspector]
  public MotionAnalyzerBackwards[] sourceAnimationsBackwards;
  [HideInInspector]
  public Vector3 m_HipAverage;
  [HideInInspector]
  public Vector3 m_HipAverageGround;
  [HideInInspector]
  public IMotionAnalyzer[] m_Motions;
  [HideInInspector]
  public IMotionAnalyzer[] m_CycleMotions;
  [HideInInspector]
  public MotionGroupInfo[] m_MotionGroups;
  [HideInInspector]
  public IMotionAnalyzer[] m_NonGroupMotions;

  public Transform root
  {
    get
    {
      return this.rootBone;
    }
  }

  public Vector3 hipAverage
  {
    get
    {
      return this.m_HipAverage;
    }
  }

  public Vector3 hipAverageGround
  {
    get
    {
      return this.m_HipAverageGround;
    }
  }

  public IMotionAnalyzer[] motions
  {
    get
    {
      return this.m_Motions;
    }
  }

  public IMotionAnalyzer[] cycleMotions
  {
    get
    {
      return this.m_CycleMotions;
    }
  }

  public MotionGroupInfo[] motionGroups
  {
    get
    {
      return this.m_MotionGroups;
    }
  }

  public IMotionAnalyzer[] nonGroupMotions
  {
    get
    {
      return this.m_NonGroupMotions;
    }
  }

  public void InitFootData(int leg)
  {
    this.groundedPose.SampleAnimation(this.gameObject, 0.0f);
    Vector3 vector3_1 = Quaternion.AngleAxis((float) leg * 360f / (float) this.legs.Length, Vector3.one) * Vector3.right;
    this.legs[leg].debugColor = new Color(vector3_1.x, vector3_1.y, vector3_1.z);
    Matrix4x4 matrix4x4_1 = Util.RelativeMatrix(this.legs[leg].ankle, this.gameObject.transform);
    Vector3 vector3_2 = matrix4x4_1.MultiplyPoint(Vector3.zero);
    Vector3 vector3_3 = vector3_2;
    vector3_3.y = this.groundPlaneHeight;
    Matrix4x4 matrix4x4_2 = Util.RelativeMatrix(this.legs[leg].toe, this.gameObject.transform);
    Vector3 vector3_4 = matrix4x4_2.MultiplyPoint(Vector3.zero);
    Vector3 vector3_5 = vector3_4;
    vector3_5.y = this.groundPlaneHeight;
    Vector3 vector3_6 = (vector3_3 + vector3_5) / 2f;
    Vector3 rhs;
    if (vector3_4 == vector3_2)
    {
      rhs = matrix4x4_1.MultiplyVector(this.legs[leg].ankle.localPosition);
      rhs.y = 0.0f;
      rhs = rhs.normalized;
    }
    else
      rhs = (vector3_5 - vector3_3).normalized;
    Vector3 vector3_7 = Vector3.Cross(Vector3.up, rhs);
    this.legs[leg].ankleHeelVector = vector3_6 + ((float) (-(double) this.legs[leg].footLength / 2.0) + this.legs[leg].footOffset.y) * rhs + this.legs[leg].footOffset.x * vector3_7;
    this.legs[leg].ankleHeelVector = matrix4x4_1.inverse.MultiplyVector(this.legs[leg].ankleHeelVector - vector3_2);
    this.legs[leg].toeToetipVector = vector3_6 + (this.legs[leg].footLength / 2f + this.legs[leg].footOffset.y) * rhs + this.legs[leg].footOffset.x * vector3_7;
    this.legs[leg].toeToetipVector = matrix4x4_2.inverse.MultiplyVector(this.legs[leg].toeToetipVector - vector3_4);
  }

  public void Init()
  {
    this.initialized = false;
    Debug.Log((object) ("Initializing " + this.name + " Locomotion System..."));
    if ((UnityEngine.Object) this.rootBone == (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) this.legs[0].hip == (UnityEngine.Object) null)
      {
        Debug.LogError((object) (this.name + ": Leg Transforms are null."), (UnityEngine.Object) this);
        return;
      }
      this.rootBone = this.legs[0].hip;
      while ((UnityEngine.Object) this.root.parent != (UnityEngine.Object) this.transform)
        this.rootBone = this.root.parent;
    }
    this.m_HipAverage = Vector3.zero;
    for (int leg = 0; leg < this.legs.Length; ++leg)
    {
      if ((UnityEngine.Object) this.legs[leg].toe == (UnityEngine.Object) null)
        this.legs[leg].toe = this.legs[leg].ankle;
      this.legs[leg].legChain = this.GetTransformChain(this.legs[leg].hip, this.legs[leg].ankle);
      this.legs[leg].footChain = this.GetTransformChain(this.legs[leg].ankle, this.legs[leg].toe);
      this.legs[leg].legLength = 0.0f;
      for (int index = 0; index < this.legs[leg].legChain.Length - 1; ++index)
        this.legs[leg].legLength += (this.transform.InverseTransformPoint(this.legs[leg].legChain[index + 1].position) - this.transform.InverseTransformPoint(this.legs[leg].legChain[index].position)).magnitude;
      this.m_HipAverage += this.transform.InverseTransformPoint(this.legs[leg].legChain[0].position);
      this.InitFootData(leg);
    }
    this.m_HipAverage /= (float) this.legs.Length;
    this.m_HipAverageGround = this.m_HipAverage;
    this.m_HipAverageGround.y = this.groundPlaneHeight;
  }

  public void Init2()
  {
    List<MotionAnalyzerBackwards> analyzerBackwardsList = new List<MotionAnalyzerBackwards>();
    for (int index = 0; index < this.sourceAnimations.Length; ++index)
    {
      Debug.Log((object) ("Analysing sourceAnimations[" + (object) index + "]: " + this.sourceAnimations[index].name));
      this.sourceAnimations[index].Analyze(this.gameObject);
      if (this.sourceAnimations[index].alsoUseBackwards)
      {
        MotionAnalyzerBackwards analyzerBackwards = new MotionAnalyzerBackwards();
        analyzerBackwards.orig = this.sourceAnimations[index];
        analyzerBackwards.Analyze(this.gameObject);
        analyzerBackwardsList.Add(analyzerBackwards);
      }
    }
    this.sourceAnimationsBackwards = analyzerBackwardsList.ToArray();
    this.groundedPose.SampleAnimation(this.gameObject, 0.0f);
    this.initialized = true;
    Debug.Log((object) ("Initializing " + this.name + " Locomotion System... Done!"));
  }

  private void Awake()
  {
    if (!this.initialized)
    {
      Debug.LogError((object) (this.name + ": Locomotion System has not been initialized."), (UnityEngine.Object) this);
    }
    else
    {
      this.m_Motions = new IMotionAnalyzer[this.sourceAnimations.Length + this.sourceAnimationsBackwards.Length];
      for (int index = 0; index < this.sourceAnimations.Length; ++index)
        this.motions[index] = (IMotionAnalyzer) this.sourceAnimations[index];
      for (int index = 0; index < this.sourceAnimationsBackwards.Length; ++index)
        this.motions[this.sourceAnimations.Length + index] = (IMotionAnalyzer) this.sourceAnimationsBackwards[index];
      int length = 0;
      for (int index = 0; index < this.motions.Length; ++index)
      {
        if (this.motions[index].motionType == MotionType.WalkCycle)
          ++length;
      }
      this.m_CycleMotions = new IMotionAnalyzer[length];
      int index1 = 0;
      for (int index2 = 0; index2 < this.motions.Length; ++index2)
      {
        if (this.motions[index2].motionType == MotionType.WalkCycle)
        {
          this.cycleMotions[index1] = this.motions[index2];
          ++index1;
        }
      }
      List<string> stringList = new List<string>();
      List<MotionGroupInfo> motionGroupInfoList = new List<MotionGroupInfo>();
      List<List<IMotionAnalyzer>> imotionAnalyzerListList = new List<List<IMotionAnalyzer>>();
      List<IMotionAnalyzer> imotionAnalyzerList = new List<IMotionAnalyzer>();
      for (int index2 = 0; index2 < this.motions.Length; ++index2)
      {
        if (this.motions[index2].motionGroup == string.Empty)
        {
          imotionAnalyzerList.Add(this.motions[index2]);
        }
        else
        {
          string str = this.motions[index2].motionGroup;
          if (!stringList.Contains(str))
          {
            motionGroupInfoList.Add(new MotionGroupInfo()
            {
              name = str
            });
            stringList.Add(str);
            imotionAnalyzerListList.Add(new List<IMotionAnalyzer>());
          }
          imotionAnalyzerListList[stringList.IndexOf(str)].Add(this.motions[index2]);
        }
      }
      this.m_NonGroupMotions = imotionAnalyzerList.ToArray();
      this.m_MotionGroups = motionGroupInfoList.ToArray();
      for (int index2 = 0; index2 < this.motionGroups.Length; ++index2)
        this.motionGroups[index2].motions = imotionAnalyzerListList[index2].ToArray();
      for (int index2 = 0; index2 < this.motionGroups.Length; ++index2)
      {
        MotionGroupInfo motionGroupInfo = this.motionGroups[index2];
        Vector3[] vector3Array = new Vector3[motionGroupInfo.motions.Length];
        float[][] samplePoints = new float[motionGroupInfo.motions.Length][];
        for (int index3 = 0; index3 < motionGroupInfo.motions.Length; ++index3)
        {
          vector3Array[index3] = motionGroupInfo.motions[index3].cycleVelocity;
          samplePoints[index3] = new float[3]
          {
            vector3Array[index3].x,
            vector3Array[index3].y,
            vector3Array[index3].z
          };
        }
        motionGroupInfo.interpolator = (Interpolator) new PolarGradientBandInterpolator(samplePoints);
      }
      this.CalculateTimeOffsets();
    }
  }

  public Transform[] GetTransformChain(Transform upper, Transform lower)
  {
    Transform transform1 = lower;
    int length = 1;
    while ((UnityEngine.Object) transform1 != (UnityEngine.Object) upper)
    {
      transform1 = transform1.parent;
      ++length;
    }
    Transform[] transformArray = new Transform[length];
    Transform transform2 = lower;
    for (int index = 0; index < length; ++index)
    {
      transformArray[length - 1 - index] = transform2;
      transform2 = transform2.parent;
    }
    return transformArray;
  }

  public void CalculateTimeOffsets()
  {
    float[] numArray1 = new float[this.cycleMotions.Length];
    float[] numArray2 = new float[this.cycleMotions.Length];
    for (int index = 0; index < this.cycleMotions.Length; ++index)
      numArray1[index] = 0.0f;
    int num1 = (this.cycleMotions.Length * this.cycleMotions.Length - this.cycleMotions.Length) / 2;
    int num2 = 0;
    bool flag = false;
    while (num2 < 100 && !flag)
    {
      for (int index = 0; index < this.cycleMotions.Length; ++index)
        numArray2[index] = 0.0f;
      for (int index1 = 1; index1 < this.cycleMotions.Length; ++index1)
      {
        for (int index2 = 0; index2 < index1; ++index2)
        {
          for (int index3 = 0; index3 < this.legs.Length; ++index3)
          {
            float num3 = this.cycleMotions[index1].cycles[index3].stanceTime + numArray1[index1];
            float num4 = this.cycleMotions[index2].cycles[index3].stanceTime + numArray1[index2];
            Vector2 vector2_1 = new Vector2(Mathf.Cos((float) ((double) num3 * 2.0 * 3.14159274101257)), Mathf.Sin((float) ((double) num3 * 2.0 * 3.14159274101257)));
            Vector2 vector2_2 = new Vector2(Mathf.Cos((float) ((double) num4 * 2.0 * 3.14159274101257)), Mathf.Sin((float) ((double) num4 * 2.0 * 3.14159274101257)));
            Vector2 vector2_3 = vector2_2 - vector2_1;
            Vector2 vector2_4 = vector2_1 + vector2_3 * 0.1f;
            Vector2 vector2_5 = vector2_2 - vector2_3 * 0.1f;
            float num5 = Util.Mod((float) ((double) Mathf.Atan2(vector2_4.y, vector2_4.x) / 2.0 / 3.14159274101257));
            float num6 = Util.Mod((float) ((double) Mathf.Atan2(vector2_5.y, vector2_5.x) / 2.0 / 3.14159274101257));
            float num7 = Util.Mod(num5 - num3);
            float num8 = Util.Mod(num6 - num4);
            if ((double) num7 > 0.5)
              --num7;
            if ((double) num8 > 0.5)
              --num8;
            numArray2[index1] += num7 * 5f / (float) num1;
            numArray2[index2] += num8 * 5f / (float) num1;
          }
        }
      }
      float a = 0.0f;
      for (int index = 0; index < this.cycleMotions.Length; ++index)
      {
        numArray1[index] += numArray2[index];
        a = Mathf.Max(a, Mathf.Abs(numArray2[index]));
      }
      ++num2;
      if ((double) a < 0.0001)
        flag = true;
    }
    for (int index1 = 0; index1 < this.cycleMotions.Length; ++index1)
    {
      this.cycleMotions[index1].cycleOffset = numArray1[index1];
      for (int index2 = 0; index2 < this.legs.Length; ++index2)
        this.cycleMotions[index1].cycles[index2].stanceTime = Util.Mod(this.cycleMotions[index1].cycles[index2].stanceTime + numArray1[index1]);
    }
  }
}
