﻿// Decompiled with JetBrains decompiler
// Type: PositionUtils
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public static class PositionUtils
{
  private const float marginTop = 20f;
  private const float marginLeft = 30f;
  private const float marginRight = 30f;
  private const float marginButtom = 20f;
  private static Camera nguiCam;
  private static UIRoot uiRoot;

  private static UIRoot UIRoot
  {
    get
    {
      return PositionUtils.uiRoot ?? (PositionUtils.uiRoot = Object.FindObjectOfType<UIRoot>());
    }
  }

  public static void SetUIPosition(GameObject widget, bool checkSize = true, UIWidget.Pivot pivot = UIWidget.Pivot.Center)
  {
    PositionUtils.nguiCam = PositionUtils.GetNGUICamera();
    if ((Object) PositionUtils.nguiCam == (Object) null)
    {
      Debug.LogError((object) "ngui camera was not found, widget cant be positioned");
    }
    else
    {
      Vector2 screenPixelSize = PositionUtils.GetScreenPixelSize(widget, checkSize);
      Vector3 position = new Vector3((float) Screen.width / 2f - screenPixelSize.x / 2f, (float) Screen.height / 2f + screenPixelSize.y / 2f, widget.transform.position.z);
      Vector3 worldPoint = PositionUtils.nguiCam.ScreenToWorldPoint(position);
      widget.transform.position = new Vector3(worldPoint.x, worldPoint.y, widget.transform.position.z);
    }
  }

  public static void CenterWindowHorizontally(WindowWidget widget)
  {
    PositionUtils.nguiCam = PositionUtils.GetNGUICamera();
    if ((Object) PositionUtils.nguiCam == (Object) null)
    {
      Debug.LogError((object) "ngui camera was not found, widget cant be positioned");
    }
    else
    {
      float x = (float) ((-(double) widget.WindowSize.x + (double) widget.PivotOffset.x) * 0.5);
      float y = (float) (((double) widget.WindowSize.y + (double) widget.PivotOffset.y) * 0.5);
      widget.transform.localPosition = new Vector3(x, y, widget.transform.position.z);
    }
  }

  public static Vector2 GetScreenPixelSize(GameObject widget, bool checkSize = false)
  {
    Bounds absoluteWidgetBounds = NGUIMath.CalculateAbsoluteWidgetBounds(widget.transform);
    Vector3 screenPoint1 = PositionUtils.nguiCam.WorldToScreenPoint(absoluteWidgetBounds.min);
    Vector3 screenPoint2 = PositionUtils.nguiCam.WorldToScreenPoint(absoluteWidgetBounds.max);
    float num1 = screenPoint2.x - screenPoint1.x;
    float num2 = screenPoint2.y - screenPoint1.y;
    float x = num1;
    float y = num2;
    if (checkSize)
    {
      if ((double) x > (double) Screen.width)
        x = (float) (Screen.width - Screen.width / 4);
      if ((double) y > (double) Screen.height)
        y = (float) (Screen.height - Screen.height / 4);
    }
    return new Vector2(x, y);
  }

  public static Vector2 GetScreenPixelSizeUnscaled(GameObject widget)
  {
    return PositionUtils.GetScreenPixelSize(widget, false) * PositionUtils.UIRoot.pixelSizeAdjustment;
  }

  public static void HoldWithinScreen(GameObject widget, float _marginTop = 0.0f, float _marginButtom = 0.0f, float _marginLeft = 0.0f, float _marginRight = 0.0f, bool onlyOuterBounds = false)
  {
    PositionUtils.nguiCam = PositionUtils.GetNGUICamera();
    if ((Object) PositionUtils.nguiCam == (Object) null)
    {
      Debug.LogError((object) "NGUI Camera was not found");
    }
    else
    {
      Vector3 screenPoint = PositionUtils.nguiCam.WorldToScreenPoint(widget.transform.position);
      Vector2 vector2 = new Vector2(screenPoint.x, (float) Screen.height - screenPoint.y);
      float x = vector2.x;
      float num = vector2.y;
      Vector2 screenPixelSize = PositionUtils.GetScreenPixelSize(widget, true);
      if ((double) vector2.x < (!onlyOuterBounds ? -(double) screenPixelSize.x : 0.0) + (double) _marginLeft)
        x = (!onlyOuterBounds ? -screenPixelSize.x : 0.0f) + _marginLeft;
      else if ((double) vector2.x + (!onlyOuterBounds ? 0.0 : (double) screenPixelSize.x) > (double) Screen.width - (double) _marginRight)
        x = (float) ((double) Screen.width - (double) _marginRight - (!onlyOuterBounds ? 0.0 : (double) screenPixelSize.x));
      if ((double) vector2.y < (double) _marginTop)
        num = _marginTop;
      else if ((!onlyOuterBounds ? (double) _marginButtom : (double) screenPixelSize.y) > (double) Screen.height - (double) vector2.y)
        num = (float) (Screen.height - Screen.height / 3);
      Vector3 worldPoint = PositionUtils.nguiCam.ScreenToWorldPoint(new Vector3(x, (float) Screen.height - num, screenPoint.z));
      widget.transform.position = worldPoint;
    }
  }

  private static Camera GetNGUICamera()
  {
    if ((Object) PositionUtils.nguiCam != (Object) null)
      return PositionUtils.nguiCam;
    GameObject gameObject = GameObject.Find("UIRoot(Clone)");
    if (!((Object) gameObject != (Object) null))
      return (Camera) null;
    PositionUtils.nguiCam = gameObject.GetComponentInChildren<Camera>();
    return PositionUtils.nguiCam;
  }

  public static void HoldWithinScreen(GameObject widget, bool holdWithingWidgetBounds)
  {
    PositionUtils.HoldWithinScreen(widget, 20f, 20f, 30f, 30f, holdWithingWidgetBounds);
  }

  public static void CorrectTransform(GameObject parent, GameObject child)
  {
    child.transform.parent = parent.transform;
    child.transform.localPosition = Vector3.zero;
    child.transform.localScale = Vector3.one;
    child.layer = parent.layer;
  }

  public static GameObject CreateCorrectedGameObject(GameObject parent, string name)
  {
    GameObject child = new GameObject(name);
    PositionUtils.CorrectTransform(parent, child);
    return child;
  }
}
