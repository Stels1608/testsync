﻿// Decompiled with JetBrains decompiler
// Type: AvatarDescriptionInDB
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AvatarDescriptionInDB
{
  [JsonField(JsonName = "Category")]
  public readonly string _category = "logic";
  public List<AvatarIndex> Avatars = new List<AvatarIndex>();
}
