﻿// Decompiled with JetBrains decompiler
// Type: GuiFadingImage
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using Gui.Tooltips;
using UnityEngine;

public class GuiFadingImage : GuiImage
{
  private float speed = 5f;
  private const float mc_maxFrame = 20f;
  private float curFrame;
  private float curAlpha;
  private IDrawable overlapDrawable;

  public GuiFadingImage(Texture2D texture, GuiAdvancedTooltipBase tooltip, AnonymousDelegate handler, float speed = 5f, IDrawable overlapDrawable = null)
    : base(texture)
  {
    this.speed = speed;
    this.ToolTip = tooltip;
    this.OnClick = handler;
    this.overlapDrawable = overlapDrawable;
  }

  private float EaseFunction(float x)
  {
    return Mathf.Sin((float) ((double) x * 2.0 * 3.14159274101257));
  }

  private void EaseInOut()
  {
    this.curFrame = (float) (((double) this.curFrame + (double) this.speed * (double) Time.deltaTime) % 20.0);
    this.curAlpha = this.EaseFunction(this.curFrame / 20f);
    Color? nullable = this.OverlayColor;
    if (!nullable.HasValue)
      nullable = new Color?(new Color(1f, 1f, 1f, 1f));
    this.OverlayColor = new Color?(new Color(nullable.Value.r, nullable.Value.g, nullable.Value.b, this.curAlpha));
  }

  public override void Draw()
  {
    this.EaseInOut();
    base.Draw();
    if (this.overlapDrawable == null)
      return;
    this.overlapDrawable.Draw();
  }
}
