﻿// Decompiled with JetBrains decompiler
// Type: MessageBoxFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Paths;
using UnityEngine;

public class MessageBoxFactory
{
  public static SpecialOfferWindowUi CreateConversionCampaignOfferWindow()
  {
    return MessageBoxFactory.CreateMessageBoxWidget<SpecialOfferWindowUi>("SpecialOfferWindowUgui", BsgoCanvas.Banner);
  }

  public static BannerWindowUi CreateBannerWindow()
  {
    return MessageBoxFactory.CreateMessageBoxWidget<BannerWindowUi>("BannerWindowUgui", BsgoCanvas.Windows);
  }

  public static HelpPopup CreateHelpPopup()
  {
    return MessageBoxFactory.CreateMessageBoxWidget<HelpPopup>("HelpPopup", BsgoCanvas.Windows);
  }

  public static DialogBoxUi CreateDialogBox(BsgoCanvas canvas = BsgoCanvas.Windows)
  {
    return MessageBoxFactory.CreateMessageBoxWidget<DialogBoxUi>("DialogBoxUgui", canvas);
  }

  public static NotifyBoxUi CreateNotifyBox(BsgoCanvas canvas = BsgoCanvas.Windows)
  {
    return MessageBoxFactory.CreateMessageBoxWidget<NotifyBoxUi>("NotifyBoxUgui", canvas);
  }

  public static RadioMessageBoxUi CreateRadioMessageBox()
  {
    return MessageBoxFactory.CreateMessageBoxWidget<RadioMessageBoxUi>("RadioMessageBoxUgui", BsgoCanvas.Windows);
  }

  public static BuyBoxUi CreateBuyBox()
  {
    return MessageBoxFactory.CreateMessageBoxWidget<BuyBoxUi>("BuyBoxUgui", BsgoCanvas.Windows);
  }

  private static T CreateMessageBoxWidget<T>(string prefabFilename, BsgoCanvas canvas = BsgoCanvas.Windows) where T : MessageBoxBase
  {
    return UguiTools.CreateChild(Resources.Load<GameObject>(Ui.Prefabs + "/MessageBoxes/" + prefabFilename), UguiTools.GetCanvas(canvas).transform).GetComponent<T>();
  }
}
