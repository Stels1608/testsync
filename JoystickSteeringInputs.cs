﻿// Decompiled with JetBrains decompiler
// Type: JoystickSteeringInputs
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class JoystickSteeringInputs
{
  private readonly Dictionary<Action, float> inputValues;

  public Vector3 JoystickXyzInputsConditioned
  {
    get
    {
      Vector2 turnInputs = this.GetTurnInputs();
      Vector2 vector2 = (double) turnInputs.magnitude <= 1.0 ? turnInputs : turnInputs.normalized;
      float magnitude = vector2.magnitude;
      Vector3 vector3 = Vector3.zero;
      if ((double) Math.Abs(magnitude) > 1.0 / 1000.0)
      {
        float num = Mathf.Sign(magnitude) * Mathf.Pow(Mathf.Abs(magnitude), JoystickSetup.SensitivityExponent);
        vector3 = (Vector3) (vector2 * num / magnitude);
      }
      vector3.z = this.GetRollInput();
      return vector3;
    }
  }

  public JoystickSteeringInputs()
  {
    this.inputValues = new Dictionary<Action, float>();
    foreach (int num in Enum.GetValues(typeof (Action)))
    {
      Action action = (Action) num;
      if (JoystickSteeringInputs.ActionIsJoystickInput(action))
        this.inputValues.Add(action, 0.0f);
    }
  }

  public static bool ActionIsJoystickInput(Action action)
  {
    if (action >= Action.JoystickTurnLeft)
      return action <= Action.JoystickStrafeDown;
    return false;
  }

  public void UpdateInputValue(Action action, float magnitude)
  {
    this.inputValues[action] = magnitude;
  }

  public float GetActionMagnitude(Action action)
  {
    if (this.inputValues.ContainsKey(action))
      return this.inputValues[action];
    return 0.0f;
  }

  public Vector2 GetStrafeInputs()
  {
    return new Vector2(this.inputValues[Action.JoystickStrafeRight] - this.inputValues[Action.JoystickStrafeLeft], this.inputValues[Action.JoystickStrafeUp] - this.inputValues[Action.JoystickStrafeDown]);
  }

  public Vector2 GetTurnInputs()
  {
    return new Vector2(this.inputValues[Action.JoystickTurnDown] - this.inputValues[Action.JoystickTurnUp], this.inputValues[Action.JoystickTurnRight] - this.inputValues[Action.JoystickTurnLeft]);
  }

  public float GetRollInput()
  {
    return (float) -((double) this.inputValues[Action.JoystickRollRight] - (double) this.inputValues[Action.JoystickRollLeft]);
  }
}
