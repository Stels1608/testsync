﻿// Decompiled with JetBrains decompiler
// Type: GuiElementPadding
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GuiElementPadding
{
  private Rect padding;

  public float Left
  {
    get
    {
      return this.padding.x;
    }
    set
    {
      this.padding.x = value;
    }
  }

  public float Top
  {
    get
    {
      return this.padding.y;
    }
    set
    {
      this.padding.y = value;
    }
  }

  public float Right
  {
    get
    {
      return this.padding.width;
    }
    set
    {
      this.padding.width = value;
    }
  }

  public float Bottom
  {
    get
    {
      return this.padding.height;
    }
    set
    {
      this.padding.height = value;
    }
  }

  public Vector2 BottomLeft
  {
    get
    {
      return new Vector2(this.Left, this.Bottom);
    }
  }

  public Vector2 BottomRight
  {
    get
    {
      return new Vector2(this.Right, this.Bottom);
    }
  }

  public Vector2 TopLeft
  {
    get
    {
      return new Vector2(this.Left, this.Top);
    }
  }

  public Vector2 TopRight
  {
    get
    {
      return new Vector2(this.Right, this.Top);
    }
  }

  public GuiElementPadding()
  {
    this.padding = new Rect(5f, 5f, 5f, 5f);
  }

  public GuiElementPadding(Rect rect)
  {
    this.padding = new Rect(rect.x, rect.y, rect.width, rect.height);
  }

  public GuiElementPadding(float left, float top, float right, float bottom)
  {
    this.padding = new Rect(left, top, right, bottom);
  }

  public GuiElementPadding(Vector4 vec)
  {
    this.padding = new Rect(vec.x, vec.y, vec.z, vec.w);
  }

  public GuiElementPadding(GuiElementPadding pad)
  {
    this.padding = new Rect(pad.Left, pad.Top, pad.Right, pad.Bottom);
  }

  public static implicit operator GuiElementPadding(Rect rect)
  {
    return new GuiElementPadding(rect);
  }

  public static implicit operator GuiElementPadding(Vector4 vec)
  {
    return new GuiElementPadding(vec);
  }

  public static implicit operator Rect(GuiElementPadding padding)
  {
    return new Rect(padding.Left, padding.Top, padding.Right, padding.Bottom);
  }

  public static implicit operator Vector4(GuiElementPadding padding)
  {
    return new Vector4(padding.Left, padding.Top, padding.Right, padding.Bottom);
  }
}
