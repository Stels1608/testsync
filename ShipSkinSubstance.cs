﻿// Decompiled with JetBrains decompiler
// Type: ShipSkinSubstance
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class ShipSkinSubstance : SingleLODListener
{
  public List<ProceduralMaterial> requiredMaterials = new List<ProceduralMaterial>();
  public List<ShipSkinMaterial> skins = new List<ShipSkinMaterial>();
  public List<ShipSkinRenderer> skinRenderers = new List<ShipSkinRenderer>();
  public const string DEFAULT_SKIN_KEY = "advanced";
  private string appliedSkin;
  private Coroutine loadingRoutine;

  public static bool ShowShipSkins { get; set; }

  public static bool UseProceduralTextures { get; set; }

  public bool Loaded { get; set; }

  public void SelectSkin(string name, Renderer[] activeRenderers, bool isAdvanced, bool isShipInShopView)
  {
    if (name == string.Empty || name == "default")
      name = "advanced";
    if (!ShipSkin.ShowSkin(this.SpaceObject))
      name = "advanced";
    if (this.appliedSkin == name)
      return;
    this.Loaded = false;
    ShipSkinMaterial skin = this.GetSkin(name);
    if (skin == null)
    {
      UnityEngine.Debug.LogWarning((object) ("ShipSkin - couldn't find substance skin '" + name + "' for object: " + this.transform.root.name));
      this.OnSkinLoaded((string) null);
    }
    else
    {
      this.BuildRequiredMats(isShipInShopView);
      this.loadingRoutine = Game.Instance.StartCoroutine(this.LoadSubstance(skin, isShipInShopView, isAdvanced));
    }
  }

  [DebuggerHidden]
  private IEnumerator LoadSubstance(ShipSkinMaterial selectedSkin, bool isShipInShopView, bool isAdvanced)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ShipSkinSubstance.\u003CLoadSubstance\u003Ec__Iterator19() { selectedSkin = selectedSkin, isShipInShopView = isShipInShopView, isAdvanced = isAdvanced, \u003C\u0024\u003EselectedSkin = selectedSkin, \u003C\u0024\u003EisShipInShopView = isShipInShopView, \u003C\u0024\u003EisAdvanced = isAdvanced, \u003C\u003Ef__this = this };
  }

  protected override void OnDestroy()
  {
    if (this.loadingRoutine == null || !((Object) Game.Instance != (Object) null))
      return;
    Game.Instance.StopCoroutine(this.loadingRoutine);
  }

  private void OnSkinLoaded(string name)
  {
    this.Loaded = true;
    this.appliedSkin = name;
  }

  private void BuildRequiredMats(bool isShipInShopView)
  {
    using (List<ProceduralMaterial>.Enumerator enumerator = this.requiredMaterials.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ProceduralMaterial current = enumerator.Current;
        if ((Object) current == (Object) null)
          UnityEngine.Debug.LogError((object) ("Required material is missing for ship skin: " + this.name));
        else
          current.RebuildTexturesImmediately();
      }
    }
  }

  private ShipSkinMaterial GetSkin(string name)
  {
    for (int index = 0; index < this.skins.Count; ++index)
    {
      if (this.skins[index].key == name)
        return this.skins[index];
    }
    return (ShipSkinMaterial) null;
  }

  public void SwitchToDefaultSkin()
  {
    this.SelectSkin("advanced", new Renderer[0], false, true);
  }

  private bool CheckUsingSubstanceMaterial(bool isShipInShopView)
  {
    if (isShipInShopView || ShipSkinSubstance.UseProceduralTextures)
      return true;
    if (this.SpaceObject != null)
      return this.SpaceObject.IsMe;
    return false;
  }

  protected virtual void OnRendererChanged(Renderer[] newRenderers)
  {
    Ship ship = this.SpaceObject as Ship;
    if (ship != null && ship.Bindings != null && (ship.Bindings.paintSystem != null && ship.Bindings.paintSystem.paintTexture.Length > 0))
    {
      this.SelectSkin(ship.Bindings.paintSystem.paintTexture, newRenderers, true, false);
    }
    else
    {
      if (ship == null)
        return;
      this.SelectSkin("advanced", newRenderers, false, false);
    }
  }
}
