﻿// Decompiled with JetBrains decompiler
// Type: AnchorToUiCameraOnAwake
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnchorToUiCameraOnAwake : MonoBehaviour
{
  private void Awake()
  {
    UIWidget component = this.GetComponent<UIWidget>();
    if ((Object) component == (Object) null)
    {
      Debug.LogError((object) ("AnchorToUiRootOnAwake(): " + (object) this.gameObject + " has no UIWidget component"));
    }
    else
    {
      UICamera getUiCamera = NGUIToolsExtension.GetUiCamera;
      if ((Object) getUiCamera == (Object) null)
        Debug.LogError((object) "AnchorToUiRootOnAwake():  Didn't find a UICamera.");
      else
        component.leftAnchor.target = component.rightAnchor.target = component.topAnchor.target = component.bottomAnchor.target = getUiCamera.transform;
    }
  }
}
