﻿// Decompiled with JetBrains decompiler
// Type: LoopSoundPlayer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.IO;
using Unity5AssetHandling;
using UnityEngine;

public class LoopSoundPlayer : SingleLODListener
{
  public LoopSoundSet LoopSoundSet;
  public string LoopSoundSetResource;
  private AudioSource audioSource;
  private AudioSource _audioSourceStart;
  private float stopTime;
  private bool playing;
  private bool combineStartAndLoop;
  private AssetRequest assetRequest;

  private AudioSource AudioSourceStart
  {
    get
    {
      if ((Object) this._audioSourceStart == (Object) null)
      {
        this._audioSourceStart = this.gameObject.AddComponent<AudioSource>();
        this._audioSourceStart.spatialBlend = 1f;
      }
      return this._audioSourceStart;
    }
  }

  protected override void Awake()
  {
    base.Awake();
    this.audioSource = this.gameObject.AddComponent<AudioSource>();
    this.audioSource.spatialBlend = 1f;
  }

  protected void Start()
  {
    if (!((Object) this.LoopSoundSet == (Object) null) || string.IsNullOrEmpty(this.LoopSoundSetResource))
      return;
    GameObject gameObject = Resources.Load(this.LoopSoundSetResource) as GameObject;
    if ((Object) gameObject != (Object) null)
    {
      this.LoopSoundSet = gameObject.GetComponent<LoopSoundSet>();
      this.combineStartAndLoop = this.LoopSoundSet.CombineStartAndLoop;
    }
    else
      this.assetRequest = AssetCatalogue.Instance.Request(Path.GetFileNameWithoutExtension(this.LoopSoundSetResource).ToLower() + ".prefab", false);
  }

  protected override void OnSwitched(bool value)
  {
    this.enabled = value;
  }

  public void Play(float duration)
  {
    if (!this.enabled)
      return;
    this.stopTime = Time.time + duration;
    if (this.playing || !((Object) this.LoopSoundSet != (Object) null))
      return;
    this.StartPlay();
  }

  private void StartPlay()
  {
    if ((Object) this.LoopSoundSet.Start != (Object) null && !this.audioSource.isPlaying)
    {
      AudioSource audioSource = this.audioSource;
      if (this.combineStartAndLoop)
        audioSource = this.AudioSourceStart;
      audioSource.clip = this.LoopSoundSet.Start;
      audioSource.loop = false;
      audioSource.Play();
    }
    this.audioSource.clip = this.LoopSoundSet.Loop;
    this.audioSource.loop = true;
    this.audioSource.Play();
    this.playing = true;
  }

  private void StopPlay()
  {
    this.playing = false;
    this.audioSource.clip = this.LoopSoundSet.End;
    this.audioSource.loop = false;
    if (!this.audioSource.isActiveAndEnabled)
      Debug.LogWarning((object) ("AudioSource with clip " + (object) this.audioSource.clip + " on " + (object) this.audioSource.gameObject + " is disabled."));
    this.audioSource.Play();
  }

  private void Update()
  {
    this.WaitForAsset();
    if (!this.playing)
      return;
    if ((double) this.stopTime < (double) Time.time)
    {
      this.StopPlay();
    }
    else
    {
      if (this.audioSource.isPlaying && !((Object) this.audioSource.clip == (Object) this.LoopSoundSet.End))
        return;
      this.audioSource.clip = this.LoopSoundSet.Loop;
      this.audioSource.loop = true;
      this.audioSource.Play();
    }
  }

  private void HardStop()
  {
    if (!this.playing)
      return;
    this.playing = false;
    if (this.combineStartAndLoop && (Object) this.AudioSourceStart != (Object) null)
      this.AudioSourceStart.Stop();
    if ((Object) this.audioSource == (Object) null)
      return;
    this.audioSource.loop = false;
    this.audioSource.Stop();
  }

  private void WaitForAsset()
  {
    if (this.assetRequest == null || !this.assetRequest.IsDone)
      return;
    if (this.assetRequest.Asset != (Object) null)
    {
      this.LoopSoundSet = (this.assetRequest.Asset as GameObject).GetComponent<LoopSoundSet>();
      this.ApplySoundSetValues(this.audioSource, this.LoopSoundSet);
      this.combineStartAndLoop = this.LoopSoundSet.CombineStartAndLoop;
      if (this.combineStartAndLoop)
        this.ApplySoundSetValues(this.AudioSourceStart, this.LoopSoundSet);
    }
    else
      Debug.LogError((object) ("Requested sound asset is NULL: " + this.assetRequest.AssetName));
    this.assetRequest = (AssetRequest) null;
  }

  private void OnDisable()
  {
    this.HardStop();
  }

  private void ApplySoundSetValues(AudioSource src, LoopSoundSet soundSet)
  {
    src.volume = soundSet.Volume;
    src.minDistance = soundSet.MinDistance;
    src.maxDistance = this.LoopSoundSet.MinDistance * 256f;
  }
}
