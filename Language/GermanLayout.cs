﻿// Decompiled with JetBrains decompiler
// Type: Language.GermanLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Language
{
  public class GermanLayout : LanguageLayout
  {
    public GermanLayout()
    {
      this.keyCodesToVirtualKeys.Add(KeyCode.Backslash, KeyCode.BackQuote);
      this.keyCodesToVirtualKeys.Add(KeyCode.LeftBracket, KeyCode.Minus);
      this.keyCodesToVirtualKeys.Add(KeyCode.RightBracket, KeyCode.Equals);
      this.keyCodesToVirtualKeys.Add(KeyCode.Z, KeyCode.Y);
      this.keyCodesToVirtualKeys.Add(KeyCode.Semicolon, KeyCode.LeftBracket);
      this.keyCodesToVirtualKeys.Add(KeyCode.Equals, KeyCode.RightBracket);
      this.keyCodesToVirtualKeys.Add(KeyCode.BackQuote, KeyCode.Colon);
      this.keyCodesToVirtualKeys.Add(KeyCode.Slash, KeyCode.Backslash);
      this.keyCodesToVirtualKeys.Add(KeyCode.Y, KeyCode.Z);
      this.keyCodesToVirtualKeys.Add(KeyCode.Minus, KeyCode.Slash);
      this.keyCodesToVirtualKeys.Add(KeyCode.Comma, KeyCode.Less);
      this.keyCodesToVirtualKeys.Add(KeyCode.Period, KeyCode.Greater);
    }
  }
}
