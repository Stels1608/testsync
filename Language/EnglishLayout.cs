﻿// Decompiled with JetBrains decompiler
// Type: Language.EnglishLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Language
{
  public class EnglishLayout : LanguageLayout
  {
    public EnglishLayout()
    {
      this.keyCodesToVirtualKeys.Add(KeyCode.Semicolon, KeyCode.Colon);
      this.keyCodesToVirtualKeys.Add(KeyCode.Comma, KeyCode.Less);
      this.keyCodesToVirtualKeys.Add(KeyCode.Period, KeyCode.Greater);
    }
  }
}
