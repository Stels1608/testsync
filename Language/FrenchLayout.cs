﻿// Decompiled with JetBrains decompiler
// Type: Language.FrenchLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Language
{
  public class FrenchLayout : LanguageLayout
  {
    public FrenchLayout()
    {
      this.keyCodesToVirtualKeys.Add(KeyCode.Quote, KeyCode.BackQuote);
      this.keyCodesToVirtualKeys.Add(KeyCode.A, KeyCode.Q);
      this.keyCodesToVirtualKeys.Add(KeyCode.Z, KeyCode.W);
      this.keyCodesToVirtualKeys.Add(KeyCode.LeftBracket, KeyCode.Minus);
      this.keyCodesToVirtualKeys.Add(KeyCode.RightBracket, KeyCode.LeftBracket);
      this.keyCodesToVirtualKeys.Add(KeyCode.Semicolon, KeyCode.RightBracket);
      this.keyCodesToVirtualKeys.Add(KeyCode.Q, KeyCode.A);
      this.keyCodesToVirtualKeys.Add(KeyCode.M, KeyCode.Colon);
      this.keyCodesToVirtualKeys.Add(KeyCode.BackQuote, KeyCode.Quote);
      this.keyCodesToVirtualKeys.Add(KeyCode.W, KeyCode.Z);
      this.keyCodesToVirtualKeys.Add(KeyCode.Comma, KeyCode.M);
      this.keyCodesToVirtualKeys.Add(KeyCode.Period, KeyCode.Less);
      this.keyCodesToVirtualKeys.Add(KeyCode.Slash, KeyCode.Greater);
    }
  }
}
