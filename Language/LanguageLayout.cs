﻿// Decompiled with JetBrains decompiler
// Type: Language.LanguageLayout
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

namespace Language
{
  public class LanguageLayout
  {
    protected readonly Dictionary<KeyCode, KeyCode> keyCodesToVirtualKeys = new Dictionary<KeyCode, KeyCode>();

    public KeyCode this[KeyCode val]
    {
      get
      {
        if (this.keyCodesToVirtualKeys.ContainsKey(val))
          return this.keyCodesToVirtualKeys[val];
        return val;
      }
    }
  }
}
