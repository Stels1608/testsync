﻿// Decompiled with JetBrains decompiler
// Type: WireframeRenderer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class WireframeRenderer : MonoBehaviour
{
  public Material mat;

  private void Start()
  {
    if (!((Object) this.mat == (Object) null))
      return;
    this.GetComponent<Renderer>().material = this.GetDefaultMaterial();
  }

  private Material GetDefaultMaterial()
  {
    Material material = new Material("Shader \"Solid Color\" {Properties { _Color (\"Color\", Color) = (1,1,1)} SubShader {Color [_Color]Pass {Blend SrcAlpha OneMinusSrcAlpha Fog { Mode Off }}     }          }");
    material.hideFlags = HideFlags.HideAndDontSave;
    material.shader.hideFlags = HideFlags.HideAndDontSave;
    material.color = new Color(0.9f, 0.9f, 0.1f, 0.1f);
    return material;
  }

  private void OnWillRenderObject()
  {
    GL.wireframe = true;
  }

  private void OnRenderObject()
  {
    GL.wireframe = false;
  }
}
