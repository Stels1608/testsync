﻿// Decompiled with JetBrains decompiler
// Type: LightConstructor
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class LightConstructor : Constructor
{
  private LightDesc lightDesc;

  public LightConstructor(LightDesc lightDesc)
    : base(new GameObject("Light"))
  {
    this.lightDesc = lightDesc;
  }

  public override void Construct()
  {
    this.root.transform.rotation = this.lightDesc.rotation;
    Light light = this.root.AddComponent<Light>();
    light.type = LightType.Directional;
    light.color = this.lightDesc.color;
    light.intensity = this.lightDesc.intensity * 2f;
  }
}
