﻿// Decompiled with JetBrains decompiler
// Type: OutpostCamera
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class OutpostCamera : MonoBehaviour
{
  public Transform Target;
  public float KeptDistance;
  public float HeightAboveHead;

  private void LateUpdate()
  {
    Vector3 center = this.Target.GetComponent<Collider>().bounds.center;
    Vector3 worldPosition = center;
    worldPosition.y = this.Target.GetComponent<Collider>().bounds.max.y;
    Quaternion quaternion = Quaternion.LookRotation(this.Target.forward);
    Vector3 vector3 = center + quaternion * new Vector3(0.0f, 0.0f, -this.KeptDistance);
    vector3.y = worldPosition.y + this.HeightAboveHead;
    this.transform.position = vector3;
    this.transform.LookAt(worldPosition);
  }
}
