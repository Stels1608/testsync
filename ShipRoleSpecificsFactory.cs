﻿// Decompiled with JetBrains decompiler
// Type: ShipRoleSpecificsFactory
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

public class ShipRoleSpecificsFactory
{
  private readonly FlightMode flightMode;
  private readonly PlayerShip playerShip;

  public ShipRoleSpecificsFactory(PlayerShip playerShip)
  {
    this.playerShip = playerShip;
    this.flightMode = this.DetermineFlightModel();
  }

  public FlightMode DetermineFlightModel()
  {
    FlightMode flightMode = FlightMode.Bams;
    if (this.playerShip == null)
      return flightMode;
    return !this.playerShip.HasAspect(ShipAspect.Dogfight) ? FlightMode.Bams : FlightMode.Cams;
  }

  public System.Type GetSpecificCameraBehaviour()
  {
    if (this.flightMode == FlightMode.Cams)
      return typeof (SpaceCameraStrikes);
    return typeof (SpaceCameraTacticals);
  }

  public ShipControlsBase CreateShipSpecificControls()
  {
    if (this.flightMode == FlightMode.Cams)
      return (ShipControlsBase) new ShipControlsForStrikes();
    return (ShipControlsBase) new ShipControlsForTacticals();
  }

  public List<IGUIPanel> CreateSpecialHudElements()
  {
    List<IGUIPanel> guiPanelList = new List<IGUIPanel>();
    if (this.flightMode == FlightMode.Cams)
    {
      IGUIPanel panel = (IGUIPanel) GuiHudStrikesMouseMovementIndicators.Instance;
      guiPanelList.Add(panel);
      Game.GUIManager.AddPanel(panel);
    }
    return guiPanelList;
  }

  public List<SpaceCameraBase.CameraMode> GetSupportedCameraModes()
  {
    switch (this.flightMode)
    {
      case FlightMode.Bams:
        return SpaceCameraTacticals.SupportedCameraModes;
      case FlightMode.Cams:
        return SpaceCameraStrikes.SupportedCameraModes;
      default:
        throw new ArgumentOutOfRangeException();
    }
  }
}
