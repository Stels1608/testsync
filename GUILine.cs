﻿// Decompiled with JetBrains decompiler
// Type: GUILine
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using UnityEngine;

public class GUILine
{
  private SmartRect rect;
  private float angle;
  private Texture2D texture;

  public GUILine()
  {
    this.texture = TextureCache.Get(Color.red);
    this.rect = new SmartRect(new Rect(0.0f, 0.0f, 1f, 1f), float2.zero, (SmartRect) null);
  }

  public void SetColor(Color newValue)
  {
    this.texture = TextureCache.Get(newValue);
  }

  public Color GetColor()
  {
    return this.texture.GetPixel(0, 0);
  }

  public void SetPoints(float2 begin, float2 end)
  {
    float2 float2 = end - begin;
    this.angle = 360f - this.CalcAngle0to360CW(new float2(1f, 0.0f).ToV3XZ(), float2.ToV3XZ());
    this.rect.Position = begin + float2 / 2f;
    this.rect.Width = float2.magnitude;
    this.rect.RecalculateAbsCoords();
  }

  public void Draw()
  {
    Matrix4x4 matrix = GUI.matrix;
    GUIUtility.RotateAroundPivot(this.angle, this.rect.AbsPosition.ToV2());
    GUI.DrawTexture(this.rect.AbsRect, (Texture) this.texture);
    GUI.matrix = matrix;
  }

  private float CalcAngle0to360CW(Vector3 vector1, Vector3 vector2)
  {
    float num = Vector3.Angle(vector1, vector2);
    if ((double) Vector3.Cross(vector1, vector2).y > 0.0)
      return num;
    return 360f - num;
  }
}
