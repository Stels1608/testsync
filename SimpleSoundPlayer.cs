﻿// Decompiled with JetBrains decompiler
// Type: SimpleSoundPlayer
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[RequireComponent(typeof (AudioSource))]
public class SimpleSoundPlayer : MonoBehaviour
{
  [SerializeField]
  private AudioClip[] clips;
  private AudioSource audioSource;
  private static AudioListener audioListener;

  private void Start()
  {
    this.audioSource = this.GetComponent<AudioSource>();
    this.Play();
  }

  private void Play()
  {
    if ((Object) SimpleSoundPlayer.audioListener == (Object) null)
    {
      SimpleSoundPlayer.audioListener = Object.FindObjectOfType<AudioListener>();
      if ((Object) SimpleSoundPlayer.audioListener == (Object) null)
      {
        Debug.LogError((object) "No AudioListener in Scene");
        return;
      }
    }
    float maxDistance = this.audioSource.maxDistance;
    float num1 = Mathf.Max(1f, maxDistance * maxDistance);
    float sqrMagnitude = (SimpleSoundPlayer.audioListener.transform.position - this.transform.position).sqrMagnitude;
    if (this.clips.Length < 1 || (double) sqrMagnitude > (double) maxDistance * (double) maxDistance)
      return;
    this.audioSource.clip = this.clips[Random.Range(0, this.clips.Length)];
    float num2 = Mathf.Clamp01(sqrMagnitude / num1);
    this.audioSource.priority = (int) ((double) num2 * (double) num2 * (double) byte.MaxValue);
    this.audioSource.Play();
  }
}
