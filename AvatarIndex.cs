﻿// Decompiled with JetBrains decompiler
// Type: AvatarIndex
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AvatarIndex : IProtocolRead
{
  public Dictionary<string, List<string>> items = new Dictionary<string, List<string>>();
  public Dictionary<string, List<string>> textures = new Dictionary<string, List<string>>();
  public Dictionary<string, Dictionary<string, List<string>>> materials = new Dictionary<string, Dictionary<string, List<string>>>();
  public string race;
  public string sex;

  public void Read(BgoProtocolReader r)
  {
    this.sex = r.ReadString();
    this.race = r.ReadString();
    this.items = new Dictionary<string, List<string>>();
    ushort num1 = r.ReadUInt16();
    for (int index1 = 0; index1 < (int) num1; ++index1)
    {
      string key = r.ReadString();
      ushort num2 = r.ReadUInt16();
      List<string> stringList = new List<string>((int) num2);
      for (int index2 = 0; index2 < (int) num2; ++index2)
      {
        string str = r.ReadString();
        stringList.Add(str);
      }
      this.items.Add(key, stringList);
    }
    this.materials = new Dictionary<string, Dictionary<string, List<string>>>();
    ushort num3 = r.ReadUInt16();
    for (int index1 = 0; index1 < (int) num3; ++index1)
    {
      string key1 = r.ReadString();
      ushort num2 = r.ReadUInt16();
      Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();
      for (int index2 = 0; index2 < (int) num2; ++index2)
      {
        string key2 = r.ReadString();
        ushort num4 = r.ReadUInt16();
        List<string> stringList = new List<string>();
        for (int index3 = 0; index3 < (int) num4; ++index3)
        {
          string str = r.ReadString();
          stringList.Add(str);
        }
        dictionary.Add(key2, stringList);
      }
      this.materials.Add(key1, dictionary);
    }
    this.textures = new Dictionary<string, List<string>>();
    ushort num5 = r.ReadUInt16();
    for (int index1 = 0; index1 < (int) num5; ++index1)
    {
      string key = r.ReadString();
      ushort num2 = r.ReadUInt16();
      List<string> stringList = new List<string>((int) num2);
      for (int index2 = 0; index2 < (int) num2; ++index2)
      {
        string str = r.ReadString();
        stringList.Add(str);
      }
      this.textures.Add(key, stringList);
    }
  }
}
