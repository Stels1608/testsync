﻿// Decompiled with JetBrains decompiler
// Type: SectorEventManager
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SectorEventManager
{
  private readonly Dictionary<uint, SectorEventTrackerUgui> activeTrackers = new Dictionary<uint, SectorEventTrackerUgui>();

  public void RemoveAllTrackers()
  {
    foreach (uint eventObjectId in this.activeTrackers.Keys.ToArray<uint>())
      this.RemoveEvent(eventObjectId);
  }

  public void UpdateEventState(SectorEventStateUpdate eventStateUpdate)
  {
    SectorEventTrackerUgui tracker = this.GetTracker(eventStateUpdate.EventObjectId);
    tracker.SectorEvent.Update(eventStateUpdate);
    tracker.Refresh();
  }

  public void UpdateEventTasks(uint eventObjectId, List<SectorEventTask> tasks)
  {
    this.GetTracker(eventObjectId).UpdateTasks(tasks);
  }

  public void RemoveEvent(uint eventObjectId)
  {
    Object.Destroy((Object) this.GetTracker(eventObjectId).gameObject);
    this.activeTrackers.Remove(eventObjectId);
  }

  public void ShowReward(SectorEventReward reward)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SectorEventManager.\u003CShowReward\u003Ec__AnonStorey88 rewardCAnonStorey88 = new SectorEventManager.\u003CShowReward\u003Ec__AnonStorey88();
    // ISSUE: reference to a compiler-generated field
    rewardCAnonStorey88.reward = reward;
    // ISSUE: reference to a compiler-generated field
    rewardCAnonStorey88.\u003C\u003Ef__this = this;
    // ISSUE: reference to a compiler-generated field
    rewardCAnonStorey88.tracker = (SectorEventTrackerUgui) null;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated field
    rewardCAnonStorey88.sectorEventCard = (SectorEventCard) Game.Catalogue.FetchCard(rewardCAnonStorey88.reward.SectorEventGuid, CardView.SectorEvent);
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    rewardCAnonStorey88.sectorEventCard.IsLoaded.AddHandler(new SignalHandler(rewardCAnonStorey88.\u003C\u003Em__D2));
  }

  private SectorEventTrackerUgui GetTracker(uint eventObjectId)
  {
    if (this.activeTrackers.ContainsKey(eventObjectId))
      return this.activeTrackers[eventObjectId];
    SectorEventTrackerUgui newTracker = this.CreateNewTracker((SectorEvent) SpaceLevel.GetLevel().GetObjectRegistry().Get(eventObjectId));
    this.activeTrackers.Add(eventObjectId, newTracker);
    return newTracker;
  }

  private SectorEventTrackerUgui CreateNewTracker(SectorEventCard eventCard)
  {
    SectorEventTrackerUgui newTracker = this.CreateNewTracker();
    newTracker.ApplyEventCard(eventCard);
    return newTracker;
  }

  private SectorEventTrackerUgui CreateNewTracker(SectorEvent sectorEvent)
  {
    SectorEventTrackerUgui newTracker = this.CreateNewTracker();
    newTracker.ApplySectorEvent(sectorEvent);
    return newTracker;
  }

  private SectorEventTrackerUgui CreateNewTracker()
  {
    return UguiTools.CreateChild<SectorEventTrackerUgui>("AccordeonSidebarRight/SectorEventTrackerUgui", AccordeonSidebarRightUgui.Instance.SectorEventsRoot.transform);
  }
}
