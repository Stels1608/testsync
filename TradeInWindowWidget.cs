﻿// Decompiled with JetBrains decompiler
// Type: TradeInWindowWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class TradeInWindowWidget : ContentWindowWidget
{
  private readonly List<uint> eventResources = new List<uint>();
  private readonly Dictionary<string, FoldOutWidget> categoryFoldouts = new Dictionary<string, FoldOutWidget>();
  private readonly Dictionary<NguiWidget, ShipItem> buttonToItemMap = new Dictionary<NguiWidget, ShipItem>();
  private uint currentEventResourcesHash = uint.MaxValue;
  private const uint PAYMENT_ITEMGROUP = 17;
  [SerializeField]
  private GameObject navListContent;
  [SerializeField]
  private UITexture itemImage;
  [SerializeField]
  private UITexture itemBackgroundImage;
  [SerializeField]
  private CountableDisplay[] resourceAmountLabel;
  [SerializeField]
  private CountableDisplay[] priceLabel;
  [SerializeField]
  private UILabel tradeInLabel;
  [SerializeField]
  private GameObject errorLayer;
  [SerializeField]
  private UILabel errorMessageLabel;
  [SerializeField]
  private GameObject descriptionLayer;
  [SerializeField]
  private UILabel itemNameLabel;
  [SerializeField]
  private UILabel itemDescriptionLabel;
  [SerializeField]
  private GameObject tutorialLayer;
  [SerializeField]
  private GameObject defaultLayer;
  [SerializeField]
  private UILabel tutorialLabel;
  [SerializeField]
  private UILabel tutorialDescriptionLabel;
  [SerializeField]
  private ButtonWidget buyMoreButton;
  [SerializeField]
  private ButtonWidget constructButton;
  [SerializeField]
  private ButtonWidget closeDescriptionButton;
  [SerializeField]
  private ToggleButtonWidget infoButton;
  private RadioButtonGroupWidget radioGroup;
  private EventShopCard eventShopCard;
  private ShipItem selectedItem;
  private bool missingResources;
  private int currentContentHash;
  private int currentAvailableItems;

  public void UpdateItems(ItemList items)
  {
    if (!this.UpdateList(items))
      return;
    this.ClearCurrentContent();
    for (int index = 0; index < items.Count; ++index)
    {
      ShipItem shipItem = items[index];
      if (shipItem.ShopItemCard.SortingNames != null && shipItem.ShopItemCard.SortingNames.Length != 0 && (!this.eventResources.Contains(shipItem.CardGUID) && this.IsAvailableForPlayer(shipItem)))
      {
        string key = shipItem.ShopItemCard.SortingNames[0];
        if (!this.categoryFoldouts.ContainsKey(key))
          this.categoryFoldouts.Add(key, this.CreateFoldOutWidget(TradeInWindowWidget.GetCategoryLocaKey(shipItem.ShopItemCard)));
        FoldOutWidget foldOutWidget = this.categoryFoldouts[key];
        foldOutWidget.parentTable = this.navListContent.GetComponent<UITable>();
        RadioButtonWidget itemListEntry = this.CreateItemListEntry(foldOutWidget.foldOutContent);
        itemListEntry.GetComponent<UILabel>().text = shipItem.ItemGUICard.Name;
        this.buttonToItemMap.Add((NguiWidget) itemListEntry, shipItem);
      }
    }
    this.navListContent.GetComponent<UITable>().Reposition();
    this.SetSelectedItem((ShipItem) null);
  }

  private bool UpdateList(ItemList items)
  {
    int contentHash = items.ContentHash;
    int num = 0;
    foreach (ShipItem shipItem in (IEnumerable<ShipItem>) items)
    {
      if (this.IsAvailableForPlayer(shipItem))
        ++num;
    }
    if (contentHash == this.currentContentHash && num == this.currentAvailableItems)
      return false;
    this.currentContentHash = contentHash;
    this.currentAvailableItems = num;
    return true;
  }

  private bool IsAvailableForPlayer(ShipItem shipItem)
  {
    if (!(shipItem is ShipSystem))
      return true;
    ShipSystem shipSystem = (ShipSystem) shipItem;
    if (shipSystem.Card.Unique && Game.Me.AlreadyHaveThisItem(shipItem))
      return false;
    if (shipSystem.Card.ShipObjectKeyRestrictions == null || shipSystem.Card.ShipObjectKeyRestrictions.Count == 0)
      return true;
    using (List<uint>.Enumerator enumerator = shipSystem.Card.ShipObjectKeyRestrictions.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (((ShipCard) Game.Catalogue.FetchCard(enumerator.Current, CardView.Ship)).Faction != Game.Me.Faction)
          return false;
      }
    }
    return true;
  }

  private void ClearCurrentContent()
  {
    using (Dictionary<string, FoldOutWidget>.ValueCollection.Enumerator enumerator = this.categoryFoldouts.Values.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.DestroyImmediate((Object) enumerator.Current.gameObject);
    }
    this.categoryFoldouts.Clear();
    this.buttonToItemMap.Clear();
    if ((Object) this.radioGroup != (Object) null)
      Object.DestroyImmediate((Object) this.radioGroup);
    this.radioGroup = this.navListContent.AddComponent<RadioButtonGroupWidget>();
    this.radioGroup.SelectNoneAtStart = true;
    this.radioGroup.OnValueChanged += new AnonymousDelegate(this.OnSelectionChanged);
    foreach (Component component in this.priceLabel)
      component.gameObject.SetActive(false);
  }

  private FoldOutWidget CreateFoldOutWidget(string categoryName)
  {
    FoldOutWidget component = NGUITools.AddChild(this.navListContent, (GameObject) Resources.Load("GUI/gui_2013/ui_elements/event_shop/CategoryFoldOut")).GetComponent<FoldOutWidget>();
    component.groupname.text = categoryName;
    return component;
  }

  private RadioButtonWidget CreateItemListEntry(GameObject parent)
  {
    GameObject prefab = (GameObject) Resources.Load("GUI/gui_2013/ui_elements/event_shop/ItemEntry");
    GameObject gameObject = NGUITools.AddChild(parent, prefab);
    gameObject.transform.localPosition += Vector3.right * 20f;
    RadioButtonWidget component = gameObject.GetComponent<RadioButtonWidget>();
    component.inactiveColor = ColorManager.currentColorScheme.Get(WidgetColorType.TEXT_EVENT);
    this.radioGroup.AddRadioButton(component);
    gameObject.AddComponent<UIDragScrollView>().scrollView = this.GetComponentInChildren<UIScrollView>();
    return component;
  }

  private static string GetCategoryLocaKey(ShopItemCard shopItemCard)
  {
    return "%$bgo.event_shop.sorting_name." + shopItemCard.SortingNames[0] + "%";
  }

  private void OnSelectionChanged()
  {
    this.SetSelectedItem(this.buttonToItemMap[(NguiWidget) this.radioGroup.activeRadioButton]);
  }

  private void SetSelectedItem(ShipItem shipItem)
  {
    this.selectedItem = shipItem;
    this.itemBackgroundImage.enabled = true;
    if (shipItem == null)
    {
      this.TitleBar.TitleLabel.text = BsgoLocalization.Get(this.eventShopCard.ShopName).ToUpperInvariant();
      this.tutorialLabel.text = BsgoLocalization.Get("bgo.event_shop.tutorial");
      this.tutorialDescriptionLabel.text = BsgoLocalization.Get(this.eventShopCard.ShopDescription).Replace("\\n", "\n");
      this.constructButton.TextLabel.text = BsgoLocalization.Get("bgo.event_shop.construct");
      this.itemImage.mainTexture = (Texture) null;
      this.ShowLayer(TradeInWindowWidget.LayerMode.Tutorial);
      this.infoButton.IsEnabled = false;
      this.constructButton.IsEnabled = false;
    }
    else
    {
      this.infoButton.IsEnabled = true;
      this.itemNameLabel.text = shipItem.ItemGUICard.Name;
      this.itemDescriptionLabel.text = shipItem.ItemGUICard.Description.Replace("%br%", "\n");
      this.itemImage.mainTexture = (Texture) (shipItem.ItemGUICard.GUITexture ?? shipItem.ItemGUICard.GUIIconTexture);
      if ((bool) ((Object) this.itemImage.mainTexture))
      {
        this.itemImage.width = this.itemImage.mainTexture.width;
        this.itemImage.height = this.itemImage.mainTexture.height;
      }
      this.ShowLayer(TradeInWindowWidget.LayerMode.Description);
      this.UpdatePrice(shipItem);
    }
  }

  private void Construct()
  {
    DialogBoxFactoryNgui.CreateConstructDialog("%$bgo.event_shop.dialog.constructing%", 2.5f).onComplete = (AnonymousDelegate) (() => FacadeFactory.GetInstance().SendMessage(Message.EventShopBuy, (object) new ItemAmountTupel(this.selectedItem, 1U)));
  }

  private void UpdatePrice(ShipItem shipItem)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    TradeInWindowWidget.\u003CUpdatePrice\u003Ec__AnonStoreyD6 priceCAnonStoreyD6 = new TradeInWindowWidget.\u003CUpdatePrice\u003Ec__AnonStoreyD6();
    // ISSUE: reference to a compiler-generated field
    priceCAnonStoreyD6.\u003C\u003Ef__this = this;
    ShopItemCard shopItemCard = shipItem.ShopItemCard;
    foreach (Component component in this.priceLabel)
      component.gameObject.SetActive(false);
    int index = 0;
    this.missingResources = false;
    using (Dictionary<ShipConsumableCard, float>.KeyCollection.Enumerator enumerator = shopItemCard.BuyPrice.items.Keys.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipConsumableCard current = enumerator.Current;
        uint amount = (uint) shopItemCard.BuyPrice.items[current];
        this.priceLabel[index].gameObject.SetActive(true);
        this.priceLabel[index].DisplayAmount(current.GUICard, amount);
        if (Game.Me.Hold.GetCountByGUID(current.CardGUID) < amount)
        {
          this.missingResources = true;
          this.priceLabel[index].AmountLabel.color = new Color(0.9803922f, 0.2078431f, 0.08235294f);
        }
        else
          this.priceLabel[index].AmountLabel.color = Color.white;
        if (++index >= this.priceLabel.Length)
          break;
      }
    }
    // ISSUE: reference to a compiler-generated field
    priceCAnonStoreyD6.missingCubits = 0U;
    if (Game.Me.Hold.IsFull)
    {
      this.constructButton.IsEnabled = false;
      this.errorMessageLabel.text = BsgoLocalization.Get("bgo.etc.hold_full");
    }
    else if (this.missingResources && !this.PurchasableByEventCurrency(shopItemCard))
    {
      this.constructButton.IsEnabled = false;
      this.constructButton.TextLabel.text = BsgoLocalization.Get("bgo.event_shop.construct");
    }
    else if (this.missingResources)
    {
      // ISSUE: reference to a compiler-generated field
      priceCAnonStoreyD6.missingCubits = shopItemCard.BuyPrice.GetBuyForCubitsPrice();
      uint cubits = Game.Me.Hold.Cubits;
      // ISSUE: reference to a compiler-generated field
      bool flag = priceCAnonStoreyD6.missingCubits > cubits;
      this.errorMessageLabel.text = BsgoLocalization.Get(!flag ? this.eventShopCard.ShopErrorMissingResources : this.eventShopCard.ShopErrorCannotBuy);
      this.constructButton.IsEnabled = !flag;
      // ISSUE: reference to a compiler-generated field
      this.constructButton.TextLabel.text = BsgoLocalization.Get("bgo.event_shop.construct") + (" (" + (object) priceCAnonStoreyD6.missingCubits + " " + BsgoLocalization.Get("bgo.common.cubits_c") + ")");
    }
    else
    {
      this.constructButton.IsEnabled = true;
      this.constructButton.TextLabel.text = BsgoLocalization.Get("bgo.event_shop.construct");
    }
    this.ShowDescription(!this.missingResources);
    // ISSUE: reference to a compiler-generated method
    this.constructButton.handleClick = new AnonymousDelegate(priceCAnonStoreyD6.\u003C\u003Em__211);
  }

  private bool PurchasableByEventCurrency(ShopItemCard item)
  {
    using (Dictionary<ShipConsumableCard, float>.Enumerator enumerator = item.BuyPrice.items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        if (this.eventResources.Contains(enumerator.Current.Key.CardGUID))
          return true;
      }
    }
    return false;
  }

  private void ShowDescription(bool show)
  {
    this.infoButton.IsActive = show;
    if (show)
      this.ShowLayer(TradeInWindowWidget.LayerMode.Description);
    else if (this.missingResources)
      this.ShowLayer(TradeInWindowWidget.LayerMode.Error);
    else
      this.ShowLayer(TradeInWindowWidget.LayerMode.None);
  }

  private void ShowLayer(TradeInWindowWidget.LayerMode layerMode)
  {
    this.descriptionLayer.SetActive(layerMode == TradeInWindowWidget.LayerMode.Description);
    this.errorLayer.SetActive(layerMode == TradeInWindowWidget.LayerMode.Error);
    this.tutorialLayer.SetActive(layerMode == TradeInWindowWidget.LayerMode.Tutorial);
    this.defaultLayer.SetActive(layerMode == TradeInWindowWidget.LayerMode.None);
  }

  public void SetEventShopCard(EventShopCard eventShopCard)
  {
    this.eventShopCard = eventShopCard;
    this.infoButton.handleClick = (AnonymousDelegate) (() => this.ShowDescription(this.infoButton.IsActive));
    this.buyMoreButton.handleClick = (AnonymousDelegate) (() => Game.Payment.OpenPaymentWindow((string) null, 17U));
    this.closeDescriptionButton.handleClick = (AnonymousDelegate) (() => this.ShowDescription(false));
    this.eventResources.Clear();
    this.eventResources.AddRange((IEnumerable<uint>) eventShopCard.EventResources);
    ShopProtocol.GetProtocol().RequestEventShopItems();
    this.SetSelectedItem((ShipItem) null);
    this.ReloadLanguageData();
    this.SetAvailableResources((ItemList) Game.Me.Hold);
  }

  public override void ReloadLanguageData()
  {
    base.ReloadLanguageData();
    this.tradeInLabel.text = BsgoLocalization.Get("bgo.event_shop.trade_in");
    this.buyMoreButton.TextLabel.text = BsgoLocalization.Get("bgo.event_shop.buy_more");
    this.constructButton.TextLabel.text = BsgoLocalization.Get("bgo.event_shop.construct");
  }

  public void SetAvailableResources(ItemList hold)
  {
    uint eventResourcesHash = this.CalculateEventResourcesHash(hold);
    if ((int) eventResourcesHash == (int) this.currentEventResourcesHash)
      return;
    this.currentEventResourcesHash = eventResourcesHash;
    for (int index = 0; index < this.resourceAmountLabel.Length; ++index)
    {
      CountableDisplay countableDisplay = this.resourceAmountLabel[index];
      if (index >= this.eventResources.Count)
      {
        countableDisplay.gameObject.SetActive(false);
      }
      else
      {
        uint num = this.eventResources[index];
        countableDisplay.gameObject.SetActive(true);
        GUICard guiCard = (GUICard) Game.Catalogue.FetchCard(num, CardView.GUI);
        uint countByGuid = hold.GetCountByGUID(num);
        countableDisplay.DisplayAmount(guiCard, countByGuid);
      }
    }
    if (this.eventShopCard == null)
      return;
    this.SetSelectedItem(this.selectedItem);
  }

  private uint CalculateEventResourcesHash(ItemList hold)
  {
    uint num = (uint) this.eventResources.Count;
    for (int index = 0; index < this.eventResources.Count; ++index)
      num = num * 17U + hold.GetCountByGUID(this.eventResources[index]);
    return num * (!Game.Me.Hold.IsFull ? 1U : 2U);
  }

  private enum LayerMode
  {
    None,
    Tutorial,
    Description,
    Error,
  }
}
