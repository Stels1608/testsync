﻿// Decompiled with JetBrains decompiler
// Type: Mission
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;

public class Mission : IServerItem, ILoadable, IProtocolRead
{
  public List<MissionCountable> countables = new List<MissionCountable>();
  private readonly List<string> objectives = new List<string>();
  private readonly List<string> rewards = new List<string>();
  private Mission.State state;
  private ushort serverID;
  public MissionCard missionCard;
  public GUICard guiCard;
  public GUICard sectorCard;
  private readonly Flag isLoaded;

  public string Name
  {
    get
    {
      return this.guiCard.Name;
    }
  }

  public string SectorName
  {
    get
    {
      if (this.sectorCard == null)
        return "%$bgo.missions.all%";
      return this.sectorCard.Name;
    }
  }

  public Flag IsLoaded
  {
    get
    {
      return this.isLoaded;
    }
  }

  public ushort ServerID
  {
    get
    {
      return this.serverID;
    }
  }

  public Mission.State Status
  {
    get
    {
      return this.state;
    }
    set
    {
      this.state = value;
    }
  }

  public int Level
  {
    get
    {
      return this.missionCard.Level;
    }
  }

  public string Description
  {
    get
    {
      return this.guiCard.Description;
    }
  }

  public List<string> Objectives
  {
    get
    {
      return this.objectives;
    }
  }

  public List<string> Rewards
  {
    get
    {
      return this.rewards;
    }
  }

  public string FirstObjectiveCounter
  {
    get
    {
      MissionCountable missionCountable = this.countables[0];
      int num1 = missionCountable.Count;
      int num2 = missionCountable.NeedCount;
      if (this.Status == Mission.State.Completed || num1 > num2)
        num1 = num2;
      return num1.ToString() + " / " + (object) num2;
    }
  }

  public Mission()
  {
    this.isLoaded = new Flag();
  }

  public void Read(BgoProtocolReader r)
  {
    this.serverID = r.ReadUInt16();
    uint cardGUID1 = r.ReadGUID();
    uint cardGUID2 = r.ReadUInt32();
    this.countables = r.ReadDescList<MissionCountable>();
    using (List<MissionCountable>.Enumerator enumerator = this.countables.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.isLoaded.Depend(new ILoadable[1]
        {
          (ILoadable) enumerator.Current
        });
    }
    this.state = (Mission.State) r.ReadInt32();
    if (this.missionCard == null)
    {
      this.missionCard = (MissionCard) Game.Catalogue.FetchCard(cardGUID1, CardView.Mission);
      this.isLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) this.missionCard
      });
    }
    if (this.guiCard == null)
    {
      this.guiCard = (GUICard) Game.Catalogue.FetchCard(cardGUID1, CardView.GUI);
      this.isLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) this.guiCard
      });
    }
    if ((int) cardGUID2 != 0 && this.sectorCard == null)
    {
      this.sectorCard = (GUICard) Game.Catalogue.FetchCard(cardGUID2, CardView.GUI);
      this.isLoaded.Depend(new ILoadable[1]
      {
        (ILoadable) this.sectorCard
      });
    }
    this.isLoaded.AddHandler(new SignalHandler(this.OnLoad));
    this.isLoaded.Set();
  }

  private void OnLoad()
  {
    this.UpdateObjectives();
    this.UpdateRewards();
  }

  private void UpdateObjectives()
  {
    this.objectives.Clear();
    if (!string.IsNullOrEmpty(this.missionCard.Action) && this.missionCard.Action != "undefined")
      this.objectives.Add(BsgoLocalization.Get("bgo.text_actions." + this.missionCard.Action));
    using (List<MissionCountable>.Enumerator enumerator = this.countables.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        MissionCountable current = enumerator.Current;
        int num1 = current.Count;
        int num2 = current.NeedCount;
        if (this.Status == Mission.State.Completed || num1 > num2)
          num1 = num2;
        this.objectives.Add(string.Format("{0} {1}/{2}", (object) BsgoLocalization.Get("bgo.text_counters." + current.Counter.Name), (object) num1, (object) num2));
      }
    }
  }

  private void UpdateRewards()
  {
    this.rewards.Clear();
    using (List<ShipItem>.Enumerator enumerator = this.missionCard.RewardCard.Items.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipItem current = enumerator.Current;
        string name = current.ItemGUICard.Name;
        if (current is ItemCountable)
        {
          ItemCountable itemCountable = current as ItemCountable;
          this.rewards.Add(string.Format(current.ItemGUICard.Name + " {0}", (object) itemCountable.Count));
        }
        else
          this.rewards.Add(name);
      }
    }
    if (this.missionCard.RewardCard.Experience <= 0)
      return;
    this.rewards.Add(string.Format(BsgoLocalization.Get("%$bgo.etc.rewards_experience%") + " {0}", (object) this.missionCard.RewardCard.Experience));
  }

  public enum State
  {
    InProgress,
    Completed,
    Submitting,
  }
}
