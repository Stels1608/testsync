﻿// Decompiled with JetBrains decompiler
// Type: SpecialOfferMediator
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.Messaging;
using Paths;
using UnityEngine;

public class SpecialOfferMediator : Bigpoint.Core.Mvc.View.View<Message>
{
  public new const string NAME = "SpecialOfferMediator";
  private SpecialOfferButton specialOfferButton;
  private HappyHourLayer specialOfferView;

  public SpecialOfferMediator()
    : base("SpecialOfferMediator")
  {
  }

  public override void OnAfterAttach()
  {
    this.AddMessageInterest(Message.UiCreated);
    this.AddMessageInterest(Message.LevelStarted);
    this.AddMessageInterest(Message.LoadNewLevel);
    this.AddMessageInterest(Message.GuiToggleHubMenuVisibility);
    this.AddMessageInterest(Message.ShowSpecialOffer);
  }

  public override void ReceiveMessage(IMessage<Message> message)
  {
    Message id = message.Id;
    switch (id)
    {
      case Message.LoadNewLevel:
        SpecialOfferButton specialOfferButton = this.specialOfferButton;
        bool flag1 = false;
        this.specialOfferButton.IsRendered = flag1;
        int num = flag1 ? 1 : 0;
        specialOfferButton.IsEnabledInLevel = num != 0;
        break;
      case Message.LevelStarted:
        this.ToggleVisibility((GameLevel) message.Data);
        this.specialOfferButton.RegisterCallbacks();
        break;
      default:
        if (id != Message.UiCreated)
        {
          if (id != Message.GuiToggleHubMenuVisibility)
          {
            if (id != Message.ShowSpecialOffer)
              break;
            if (Game.SpecialOfferManager.IsHappyHourActive())
            {
              this.specialOfferView.Show(Game.SpecialOfferManager.SpecialOfferCard);
              break;
            }
            GuiSpecialOfferDialog.Show();
            break;
          }
          bool flag2 = (bool) message.Data;
          if (!((Object) this.specialOfferButton != (Object) null))
            break;
          this.specialOfferButton.IsRendered = flag2;
          break;
        }
        this.CreateSpecialOfferView();
        this.CreateSpecialOfferButton();
        break;
    }
  }

  private void ToggleVisibility(GameLevel level)
  {
    if ((Object) this.specialOfferButton == (Object) null)
      return;
    if (level is RoomLevel)
    {
      SpecialOfferButton specialOfferButton = this.specialOfferButton;
      bool flag = true;
      this.specialOfferButton.IsRendered = flag;
      int num = flag ? 1 : 0;
      specialOfferButton.IsEnabledInLevel = num != 0;
    }
    else
    {
      SpaceLevel spaceLevel = level as SpaceLevel;
      if ((Object) spaceLevel == (Object) null)
        return;
      SpecialOfferButton specialOfferButton = this.specialOfferButton;
      bool flag = !spaceLevel.IsStory && !spaceLevel.IsTournament;
      this.specialOfferButton.IsRendered = flag;
      int num = flag ? 1 : 0;
      specialOfferButton.IsEnabledInLevel = num != 0;
      if (!this.specialOfferButton.IsEnabledInLevel)
        return;
      this.SendMessage(Message.AddToCombatGui, (object) this.specialOfferButton);
    }
  }

  private void CreateSpecialOfferButton()
  {
    this.specialOfferButton = UguiTools.CreateChild<SpecialOfferButton>("SpecialOfferButton", AccordeonSidebarRightUgui.Instance.SpecialOfferButton.transform);
    this.specialOfferButton.IsRendered = false;
  }

  private void CreateSpecialOfferView()
  {
    GameObject child = UguiTools.CreateChild(Resources.Load<GameObject>(Ui.Prefabs + "/HappyHourLayer"), UguiTools.GetCanvas(BsgoCanvas.Windows).transform);
    this.specialOfferView = child.GetComponent<HappyHourLayer>();
    child.SetActive(false);
  }
}
