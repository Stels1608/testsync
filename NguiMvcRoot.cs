﻿// Decompiled with JetBrains decompiler
// Type: NguiMvcRoot
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Bigpoint.Core.Mvc.View;
using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (UIRoot))]
public class NguiMvcRoot : MonoBehaviour
{
  [SerializeField]
  protected string mvcFacadeClass = "NguiFacade";

  public NguiFacade Facade { get; protected set; }

  public NguiWindowManager WindowManager { get; protected set; }

  public void Awake()
  {
    try
    {
      UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) this.gameObject);
      this.InitializeFacade();
      this.InitializeWindowManager();
      this.InitializeWidgets();
    }
    catch (Exception ex)
    {
      Debug.LogError((object) ("Error while initializing UI: " + ex.Message));
      throw;
    }
  }

  protected void InitializeFacade()
  {
    this.Facade = FacadeFactory.GetInstance();
    if (this.Facade == null)
      throw new Exception("Facade class could not be initialised!");
  }

  protected void InitializeWindowManager()
  {
    this.WindowManager = new NguiWindowManager();
    this.WindowManager.BindMvcRoot(this);
    this.Facade.SendMessage(Message.RegisterWindowManager, (object) this.WindowManager);
  }

  protected void InitializeViews(List<Bigpoint.Core.Mvc.View.View<Message>> views)
  {
    using (List<Bigpoint.Core.Mvc.View.View<Message>>.Enumerator enumerator = views.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Bigpoint.Core.Mvc.View.View<Message> current = enumerator.Current;
        if (current != null)
          this.InitializeView(current);
      }
    }
  }

  protected void InitializeView(Bigpoint.Core.Mvc.View.View<Message> view)
  {
    this.Facade.AttachView((IView<Message>) view);
  }

  protected void InitializeWidgets()
  {
    this.BindRootToWidgets(this.gameObject, (NguiWidget) null);
  }

  protected void BindRootToWidgets(GameObject parentGameObject, NguiWidget parentView)
  {
    NguiWidget component = parentGameObject.GetComponent<NguiWidget>();
    NguiWidget parentView1 = parentView;
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
    {
      component.BindMvcRoot(this);
      parentView1 = component;
    }
    for (int index = 0; index < parentGameObject.transform.childCount; ++index)
      this.BindRootToWidgets(parentGameObject.transform.GetChild(index).gameObject, parentView1);
  }
}
