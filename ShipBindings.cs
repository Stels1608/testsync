﻿// Decompiled with JetBrains decompiler
// Type: ShipBindings
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class ShipBindings : IProtocolRead
{
  public List<ShipModuleBinding> ModuleBindings = new List<ShipModuleBinding>();
  public List<StickerBinding> StickerBindings = new List<StickerBinding>();
  public bool Syfy;
  public ShipSystemPaintCard paintSystem;
  public readonly Flag AreLoaded;
  private bool isRead;

  public ShipBindings()
  {
    this.AreLoaded = new Flag();
    this.AreLoaded.Depend<ShipModuleBinding>((IEnumerable<ShipModuleBinding>) this.ModuleBindings);
  }

  public void Read(BgoProtocolReader r)
  {
    int num = r.ReadLength();
    for (int index = 0; index < num; ++index)
    {
      switch (r.ReadByte())
      {
        case 1:
          this.StickerBindings.Add(r.ReadDesc<StickerBinding>());
          break;
        case 2:
          this.ModuleBindings.Add(r.ReadDesc<ShipModuleBinding>());
          break;
        case 3:
          this.Syfy = true;
          break;
        case 4:
          this.paintSystem = (ShipSystemPaintCard) Game.Catalogue.FetchCard(r.ReadGUID(), CardView.ShipPaint);
          this.AreLoaded.Depend(new ILoadable[1]
          {
            (ILoadable) this.paintSystem
          });
          break;
      }
    }
    this.AreLoaded.Set();
    if (this.isRead)
      Debug.LogError((object) "ShipBindings.Read() was invoked twice. Report to [mw]");
    this.isRead = true;
  }
}
