﻿// Decompiled with JetBrains decompiler
// Type: AvatarCylonMaterials
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

public class AvatarCylonMaterials
{
  private static readonly Dictionary<string, ACMObject> objects = new Dictionary<string, ACMObject>();

  public static void Parse(List<string> materials)
  {
    AvatarCylonMaterials.objects.Clear();
    using (List<string>.Enumerator enumerator = AvatarInfo.GetItemsOfRace("cylon").GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        ACMObject acmObject = new ACMObject();
        acmObject.Parse(AvatarCylonMaterials.FindMaterialsFromObj(current, materials));
        AvatarCylonMaterials.objects[current] = acmObject;
      }
    }
  }

  private static List<string> FindMaterialsFromObj(string obj, List<string> materials)
  {
    List<string> stringList = new List<string>();
    using (List<string>.Enumerator enumerator = materials.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        if (!current.Contains("norm") && current.Contains(obj))
          stringList.Add(current);
      }
    }
    return stringList;
  }

  public static string GetMaterial(string obj, string version, string color, int number)
  {
    if (!AvatarCylonMaterials.objects.ContainsKey(obj))
      return string.Empty;
    return AvatarCylonMaterials.objects[obj].GetMaterial(version, color, number);
  }

  public int GetMaterialNumber(string material)
  {
    using (Dictionary<string, ACMObject>.Enumerator enumerator = AvatarCylonMaterials.objects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        int materialNumber = enumerator.Current.Value.GetMaterialNumber(material);
        if (materialNumber != -1)
          return materialNumber;
      }
    }
    return -1;
  }

  public static int GetMaterialsCount(string obj, string version, string color)
  {
    ACMObject acmObject;
    if (AvatarCylonMaterials.objects.TryGetValue(obj, out acmObject))
      return acmObject.GetMaterialsCount(version, color);
    return 0;
  }

  public static List<string> GetVersions()
  {
    return ACMObject.GetVersions();
  }

  public static List<string> GetColors()
  {
    return ACMVersion.GetColors();
  }
}
