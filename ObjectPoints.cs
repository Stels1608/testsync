﻿// Decompiled with JetBrains decompiler
// Type: ObjectPoints
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Loaders;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoints
{
  private static ObjectPoints.Pair[] filters = new ObjectPoints.Pair[5]{ new ObjectPoints.Pair("bullet", SpotType.Weapon.ToString().ToLower(), ObjectPoints.NamePositon.Prefix), new ObjectPoints.Pair("elitebullet", SpotType.Weapon.ToString().ToLower(), ObjectPoints.NamePositon.Prefix), new ObjectPoints.Pair("sticker", SpotType.Sticker.ToString().ToLower(), ObjectPoints.NamePositon.Prefix), new ObjectPoints.Pair("locator", SpotType.Mining.ToString().ToLower(), ObjectPoints.NamePositon.Postfix), new ObjectPoints.Pair("door", SpotType.Door.ToString().ToLower(), ObjectPoints.NamePositon.Prefix) };
  public List<JObjectPoint> objectPoints = new List<JObjectPoint>();
  public string docName;

  public ObjectPoints(string docName)
  {
    this.docName = docName;
  }

  public void ApplyObject(GameObject gameObject)
  {
    this.objectPoints.Clear();
    foreach (Transform componentsInChild in gameObject.GetComponentsInChildren<Transform>(true))
    {
      string type;
      if (this.CheckName(componentsInChild.name, out type))
        this.objectPoints.Add(new JObjectPoint()
        {
          position = componentsInChild.position,
          rotation = componentsInChild.rotation,
          name = componentsInChild.name,
          type = type
        });
    }
  }

  private bool CheckName(string name, out string type)
  {
    type = string.Empty;
    string lower = name.ToLower();
    foreach (ObjectPoints.Pair filter in ObjectPoints.filters)
    {
      switch (filter.namePositon)
      {
        case ObjectPoints.NamePositon.Prefix:
          if (lower.StartsWith(filter.filter))
          {
            type = filter.type;
            return true;
          }
          break;
        case ObjectPoints.NamePositon.Postfix:
          if (lower.EndsWith(filter.filter))
          {
            type = filter.type;
            return true;
          }
          break;
        case ObjectPoints.NamePositon.Any:
          if (lower.Contains(filter.filter))
          {
            type = filter.type;
            return true;
          }
          break;
      }
    }
    return false;
  }

  public void Log()
  {
    Debug.Log((object) ("Name: " + this.docName));
    using (List<JObjectPoint>.Enumerator enumerator = this.objectPoints.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        JObjectPoint current = enumerator.Current;
        Debug.Log((object) (current.name + " " + current.type + " " + (object) current.position + " " + (object) current.rotation));
      }
    }
  }

  public void SaveToContentDB()
  {
    ContentDB.UpdateDocument(this.docName, "/ObjectPoints/", (object) this.objectPoints);
    this.Log();
    Debug.Log((object) ("ObjectPoints.cs: " + this.docName + ".json saved"));
  }

  private enum NamePositon
  {
    Prefix,
    Postfix,
    Any,
  }

  private struct Pair
  {
    public ObjectPoints.NamePositon namePositon;
    public string filter;
    public string type;

    public Pair(string filter, string type, ObjectPoints.NamePositon namePositon)
    {
      this.filter = filter;
      this.type = type;
      this.namePositon = namePositon;
    }
  }
}
