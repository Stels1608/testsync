﻿// Decompiled with JetBrains decompiler
// Type: ChatSystemOpenChannelsUpdate
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

internal class ChatSystemOpenChannelsUpdate : ChatProtocolParser
{
  private static readonly string impliedFormat = "fz%-1|2359|System-Colonial|0}0|1}-1|2599|Open Channel|0}0}1}#";
  public ChatSystemOpenChannelsUpdate.Room SystemLocal;
  public ChatSystemOpenChannelsUpdate.Room SystemGlobal;
  public ChatSystemOpenChannelsUpdate.Room OpenLocal;
  public ChatSystemOpenChannelsUpdate.Room OpenGlobal;

  public ChatSystemOpenChannelsUpdate()
  {
    this.type = ChatProtocolParserType.SystemOpenChannelsUpdate;
  }

  public override bool TryParse(string textMessage)
  {
    textMessage = textMessage.ToLower();
    List<string> input = new List<string>((IEnumerable<string>) textMessage.Split(new char[3]{ '%', '|', '}' }));
    if (input[0] != "fz")
      return false;
    input.RemoveAt(0);
    input.RemoveAt(input.Count - 1);
    List<ChatSystemOpenChannelsUpdate.Room> roomList = new List<ChatSystemOpenChannelsUpdate.Room>();
    int numChunks = 0;
    ChatSystemOpenChannelsUpdate.Room room1;
    while (this.ReadRoom(input, out room1, out numChunks))
    {
      roomList.Add(room1);
      input.RemoveRange(0, numChunks);
    }
    roomList.ForEach((System.Action<ChatSystemOpenChannelsUpdate.Room>) (room =>
    {
      if (room.name.Contains("system"))
      {
        room.name = "system" + (!room.global ? "Local" : "Global");
      }
      else
      {
        if (!room.name.Contains("open"))
          return;
        room.name = "open" + (!room.global ? "Local" : "Global");
      }
    }));
    this.SystemLocal = roomList.Find((Predicate<ChatSystemOpenChannelsUpdate.Room>) (match => match.name == "systemLocal"));
    this.SystemGlobal = roomList.Find((Predicate<ChatSystemOpenChannelsUpdate.Room>) (match => match.name == "systemGlobal"));
    this.OpenLocal = roomList.Find((Predicate<ChatSystemOpenChannelsUpdate.Room>) (match => match.name == "openLocal"));
    this.OpenGlobal = roomList.Find((Predicate<ChatSystemOpenChannelsUpdate.Room>) (match => match.name == "openGlobal"));
    return input.Count == 0;
  }

  private bool ReadRoom(List<string> input, out ChatSystemOpenChannelsUpdate.Room room, out int numChunks)
  {
    room = new ChatSystemOpenChannelsUpdate.Room();
    numChunks = 0;
    if (input.Count < 6 || !int.TryParse(input[1], out room.id))
      return false;
    room.name = input[2];
    if (!Console.ParseBool(input[5], out room.global))
      return false;
    numChunks = 6;
    return true;
  }

  public class Room
  {
    public string name = string.Empty;
    public int id;
    public bool global;

    public override string ToString()
    {
      return string.Format("{0}, id:{1}, global:{2}", (object) this.name, (object) this.id, (object) this.global);
    }
  }
}
