﻿// Decompiled with JetBrains decompiler
// Type: EffectState
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

public abstract class EffectState
{
  public float qualityEvaluationInterval = 0.1f;
  public const int TheBestQuality = 0;
  private EffectState.IState state;
  private EffectState.IState newState;
  private EffectState.IState activeState;
  private EffectState.IState idleState;
  private EffectState.IState terminatedState;
  private int quality;
  public float lastQualityEvaluationTime;
  private int maxQuality;
  protected IQualityEvaluator qualityEvaluator;

  public bool New
  {
    get
    {
      return this.state == this.newState;
    }
  }

  public bool Idle
  {
    get
    {
      return this.state == this.idleState;
    }
  }

  public bool Active
  {
    get
    {
      return this.state == this.activeState;
    }
  }

  public bool Terminated
  {
    get
    {
      return this.state == this.terminatedState;
    }
  }

  public int Quality
  {
    get
    {
      return this.quality;
    }
    set
    {
      if (value < this.maxQuality)
        value = this.maxQuality;
      if (value == this.quality)
        return;
      this.quality = value;
      this.state.ChangeQuality(this.quality);
    }
  }

  public int MaxQuality
  {
    get
    {
      return this.maxQuality;
    }
    set
    {
      if (value < 0)
        value = 0;
      this.maxQuality = value;
      this.Quality = this.maxQuality;
    }
  }

  public abstract EffectCategory Category { get; }

  public EffectState()
  {
    this.newState = (EffectState.IState) new EffectState.NewState(this);
    this.activeState = (EffectState.IState) new EffectState.ActiveState(this);
    this.idleState = (EffectState.IState) new EffectState.IdleState(this);
    this.terminatedState = (EffectState.IState) new EffectState.TerminatedState(this);
    this.state = this.newState;
  }

  private void SetState(EffectState.IState state)
  {
    this.state = state;
  }

  public void Launch()
  {
    this.state.Launch();
  }

  protected virtual bool OnLaunching()
  {
    return true;
  }

  protected virtual void OnLaunched()
  {
  }

  public void Terminate()
  {
    this.state.Terminate();
  }

  protected virtual void OnTerminate()
  {
  }

  public void Suspend()
  {
    this.state.Suspend();
  }

  protected virtual void OnSuspend()
  {
  }

  public void Resume()
  {
    this.state.Resume();
  }

  protected virtual void OnResume()
  {
  }

  public virtual int EvaluateQuality()
  {
    if (this.qualityEvaluator == null)
      return this.maxQuality;
    return this.qualityEvaluator.Evaluate();
  }

  protected virtual void OnQualityChange(int quality)
  {
  }

  public void Update()
  {
    this.state.Update();
  }

  protected virtual void OnUpdate()
  {
  }

  private interface IState
  {
    void Launch();

    void Terminate();

    void Suspend();

    void Resume();

    void Update();

    void ChangeQuality(int quality);
  }

  private abstract class State : EffectState.IState
  {
    protected EffectState effect;

    public State(EffectState effect)
    {
      this.effect = effect;
    }

    protected virtual void OnStateChange(EffectState.IState state)
    {
      this.effect.SetState(state);
    }

    public virtual void Launch()
    {
    }

    public virtual void Terminate()
    {
    }

    public virtual void Suspend()
    {
    }

    public virtual void Resume()
    {
    }

    public virtual void Update()
    {
    }

    public virtual void ChangeQuality(int quality)
    {
    }
  }

  private class NewState : EffectState.State
  {
    public NewState(EffectState effect)
      : base(effect)
    {
    }

    public override void Launch()
    {
      if (this.effect.OnLaunching())
      {
        this.OnStateChange(this.effect.activeState);
        this.effect.OnLaunched();
      }
      else
        this.OnStateChange(this.effect.terminatedState);
    }

    public override void Terminate()
    {
      this.OnStateChange(this.effect.terminatedState);
    }
  }

  private class ActiveState : EffectState.State
  {
    public ActiveState(EffectState effect)
      : base(effect)
    {
    }

    public override void Terminate()
    {
      this.OnStateChange(this.effect.terminatedState);
      this.effect.OnTerminate();
    }

    public override void Suspend()
    {
      this.OnStateChange(this.effect.idleState);
      this.effect.OnSuspend();
    }

    public override void Update()
    {
      this.effect.OnUpdate();
    }

    public override void ChangeQuality(int quality)
    {
      this.effect.OnQualityChange(quality);
    }
  }

  private class IdleState : EffectState.State
  {
    public IdleState(EffectState effect)
      : base(effect)
    {
    }

    public override void Terminate()
    {
      this.OnStateChange(this.effect.terminatedState);
      this.effect.OnTerminate();
    }

    public override void Resume()
    {
      this.OnStateChange(this.effect.activeState);
      this.effect.OnResume();
    }

    public override void ChangeQuality(int quality)
    {
      this.effect.OnQualityChange(quality);
    }
  }

  private class TerminatedState : EffectState.State
  {
    public TerminatedState(EffectState effect)
      : base(effect)
    {
    }

    public override void Launch()
    {
      if (!this.effect.OnLaunching())
        return;
      this.OnStateChange(this.effect.activeState);
      this.effect.OnLaunched();
    }
  }
}
