﻿// Decompiled with JetBrains decompiler
// Type: DialogBoxNgui
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DialogBoxNgui : DialogBoxUi
{
  [SerializeField]
  private UILabel headerLabel;
  [SerializeField]
  private UILabel okButtonLabel;
  [SerializeField]
  private UILabel cancelButtonLabel;
  [SerializeField]
  private UILabel messageLabel;
  [SerializeField]
  private UILabel timerLabel;

  protected override void SetOkButtonText(string text)
  {
    this.okButtonLabel.text = text;
  }

  protected override void SetCancelButtonText(string text)
  {
    this.cancelButtonLabel.text = text;
  }

  protected override void SetTitleText(string text)
  {
    this.headerLabel.text = text;
  }

  protected override void SetMainText(string mainText)
  {
    this.messageLabel.text = mainText;
  }

  protected override void SetTimerText(string timeLeftText)
  {
    this.timerLabel.text = timeLeftText;
  }

  public override void ShowTimer(bool show)
  {
    this.timerLabel.enabled = show;
  }
}
