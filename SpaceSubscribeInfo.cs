﻿// Decompiled with JetBrains decompiler
// Type: SpaceSubscribeInfo
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class SpaceSubscribeInfo : ObjectStats
{
  private static readonly List<ClientBuffIcon> clientBuffs = new List<ClientBuffIcon>();
  private readonly Dictionary<uint, ShipBuff> buffDisplayHelper = new Dictionary<uint, ShipBuff>();
  private readonly uint objectId;
  protected List<ShipBuff> ShipBuffs;
  public bool IsSubscribe;
  public float HullPoints;
  public float PowerPoints;
  public uint TargetID;

  public bool FirstStatUpdateReceived { get; set; }

  public SpaceObject Target
  {
    get
    {
      return SpaceLevel.GetLevel().GetObjectRegistry().Get(this.TargetID);
    }
  }

  public virtual bool InCombat { get; set; }

  public List<ShipBuff> DisplayedShipBuffs
  {
    get
    {
      this.buffDisplayHelper.Clear();
      SpaceObject spaceObject = !(this is MyShipStats) ? SpaceLevel.GetLevel().GetObjectRegistry().Get(this.objectId) : (SpaceObject) Game.Me.Ship;
      if (spaceObject != null)
      {
        using (List<ClientBuffIcon>.Enumerator enumerator = SpaceSubscribeInfo.clientBuffs.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            ClientBuffIcon current = enumerator.Current;
            if (current.Applies(spaceObject))
            {
              ShipBuff clientBuff = current.ClientBuff;
              this.buffDisplayHelper.Add(clientBuff.AbilityGuid, clientBuff);
            }
          }
        }
      }
      if (this.buffDisplayHelper.Count == 0)
        return this.ShipBuffs;
      using (List<ShipBuff>.Enumerator enumerator = this.ShipBuffs.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          ShipBuff current = enumerator.Current;
          uint key = current.AbilityGuid;
          if (this.buffDisplayHelper.ContainsKey(key))
          {
            ShipBuff shipBuff = this.buffDisplayHelper[key];
            if ((double) current.EndTime > (double) shipBuff.EndTime)
              this.buffDisplayHelper.Remove(key);
            else
              continue;
          }
          this.buffDisplayHelper.Add(key, current);
        }
      }
      return new List<ShipBuff>((IEnumerable<ShipBuff>) this.buffDisplayHelper.Values);
    }
  }

  public uint ObjectId
  {
    get
    {
      return this.objectId;
    }
  }

  static SpaceSubscribeInfo()
  {
    SpaceSubscribeInfo.clientBuffs.Add((ClientBuffIcon) new GroupBuff());
    SpaceSubscribeInfo.clientBuffs.Add((ClientBuffIcon) new MagneticStorm());
  }

  public SpaceSubscribeInfo(uint objectId)
  {
    this.objectId = objectId;
    this.Reset();
  }

  private void Reset()
  {
    this.TargetID = 0U;
    this.ShipBuffs = new List<ShipBuff>();
    this.HullPoints = 1f;
    this.PowerPoints = 1f;
    this.SetObfuscatedFloat(ObjectStat.MaxHullPoints, 1f);
    this.SetObfuscatedFloat(ObjectStat.MaxPowerPoints, 1f);
    this.InCombat = false;
    this.FirstStatUpdateReceived = false;
  }

  protected virtual void AddBuff(ShipBuff buff)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    SpaceSubscribeInfo.\u003CAddBuff\u003Ec__AnonStorey64 buffCAnonStorey64 = new SpaceSubscribeInfo.\u003CAddBuff\u003Ec__AnonStorey64();
    // ISSUE: reference to a compiler-generated field
    buffCAnonStorey64.buff = buff;
    // ISSUE: reference to a compiler-generated method
    this.ShipBuffs.RemoveAll(new Predicate<ShipBuff>(buffCAnonStorey64.\u003C\u003Em__37));
    // ISSUE: reference to a compiler-generated field
    this.ShipBuffs.Add(buffCAnonStorey64.buff);
  }

  protected void RemoveBuff(uint buffID)
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: reference to a compiler-generated method
    this.ShipBuffs.RemoveAll(new Predicate<ShipBuff>(new SpaceSubscribeInfo.\u003CRemoveBuff\u003Ec__AnonStorey65()
    {
      buffID = buffID
    }.\u003C\u003Em__38));
  }

  public void Read(BgoProtocolReader r)
  {
    this.FirstStatUpdateReceived = true;
    int num1 = (int) r.ReadInt16();
    for (int index = 0; index < num1; ++index)
    {
      switch (r.ReadByte())
      {
        case 1:
          ObjectStat objectStat = (ObjectStat) r.ReadUInt16();
          float num2 = r.ReadSingle();
          this.SetObfuscatedFloat(objectStat, num2);
          this.OnStatChanged(objectStat, num2);
          break;
        case 2:
          this.AddBuff(r.ReadDesc<ShipBuff>());
          break;
        case 3:
          this.InCombat = r.ReadBoolean();
          break;
        case 4:
          this.TargetID = r.ReadUInt32();
          break;
        case 5:
          this.RemoveBuff(r.ReadUInt32());
          break;
        case 6:
          this.PowerPoints = r.ReadSingle();
          break;
        case 7:
          this.HullPoints = r.ReadSingle();
          break;
        case 9:
          this.Reset();
          break;
        case 12:
          this.OnSlotStat((ushort) r.ReadByte(), (ObjectStat) r.ReadUInt16(), r.ReadSingle());
          break;
        case 13:
          ShipAspects shipAspects = new ShipAspects();
          shipAspects.Read(r);
          this.OnShipAspectsUpdate(shipAspects);
          break;
        case 14:
          this.OnAddToggleBuff((ushort) r.ReadByte(), r.ReadGUID());
          break;
        case 15:
          this.OnRemoveToggleBuff((ushort) r.ReadByte(), r.ReadGUID());
          break;
        case 16:
          this.OnAddStatsModifier(r.ReadGUID(), r.ReadSingle());
          break;
        case 17:
          this.RemoveBuff(r.ReadGUID());
          break;
        case 18:
          ShipBuff buff = r.ReadDesc<ShipBuff>();
          this.OnShortCircuited(buff);
          this.AddBuff(buff);
          break;
        case 19:
          uint num3 = r.ReadGUID();
          this.OnShortRemoved(num3);
          this.RemoveBuff(num3);
          break;
      }
    }
  }

  protected virtual void OnStatChanged(ObjectStat stat, float value)
  {
  }

  protected virtual void OnShortCircuited(ShipBuff buff)
  {
  }

  protected virtual void OnShortRemoved(uint guid)
  {
  }

  protected virtual void OnAddStatsModifier(uint guid, float duration)
  {
    ShipBuff buff = new ShipBuff();
    buff.ServerID = buff.AbilityGuid = guid;
    buff.GuiCard = (GUICard) Game.Catalogue.FetchCard(guid, CardView.GUI);
    buff.MaxTime = duration;
    buff.EndTime = Time.time + duration;
    buff.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) buff.GuiCard
    });
    buff.IsLoaded.Set();
    this.AddBuff(buff);
  }

  protected virtual void OnShipAspectsUpdate(ShipAspects shipAspects)
  {
  }

  protected virtual void OnSlotStat(ushort slotId, ObjectStat stat, float value)
  {
  }

  protected virtual void OnAddToggleBuff(ushort slotId, uint abilityGuid)
  {
    ShipBuff buff = new ShipBuff();
    buff.ServerID = buff.AbilityGuid = abilityGuid;
    buff.Ability = (ShipAbilityCard) Game.Catalogue.FetchCard(abilityGuid, CardView.ShipAbility);
    buff.GuiCard = (GUICard) Game.Catalogue.FetchCard(abilityGuid, CardView.GUI);
    buff.MaxTime = float.MaxValue;
    buff.EndTime = float.MaxValue;
    buff.IsLoaded.Depend(new ILoadable[1]
    {
      (ILoadable) buff.Ability
    });
    buff.IsLoaded.Set();
    this.AddBuff(buff);
  }

  protected virtual void OnRemoveToggleBuff(ushort slotId, uint abilityGuid)
  {
    this.RemoveBuff(abilityGuid);
  }

  public override string ToString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.Append("IsSubscribe = " + (object) this.IsSubscribe + "\n");
    stringBuilder.Append("Combat = " + (object) this.InCombat + "\n");
    stringBuilder.Append("TargetID = " + (object) this.TargetID + "\n");
    stringBuilder.Append("---------------------------\n");
    stringBuilder.Append("Buffs:\n");
    using (List<ShipBuff>.Enumerator enumerator = this.ShipBuffs.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        ShipBuff current = enumerator.Current;
        if ((bool) current.IsLoaded && (double) current.TimeLeft > 0.0)
          stringBuilder.AppendFormat("Buff: {0} {1:0.0}\n", (object) current.Ability.GUICard.Name, (object) current.TimeLeft);
      }
    }
    stringBuilder.Append("---------------------------\n");
    stringBuilder.Append("Stats:\n");
    List<string> stringList = new List<string>();
    stringList.Add("HullPoints = " + (object) this.HullPoints);
    stringList.Add("PowerPoints = " + (object) this.PowerPoints);
    using (List<Tuple<string, float>>.Enumerator enumerator = this.ToPrintableList().GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Tuple<string, float> current = enumerator.Current;
        stringList.Add(current.First + " = " + (object) current.Second);
      }
    }
    stringList.Sort();
    using (List<string>.Enumerator enumerator = stringList.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        string current = enumerator.Current;
        stringBuilder.Append(current);
        stringBuilder.Append("\n");
      }
    }
    return stringBuilder.ToString();
  }
}
