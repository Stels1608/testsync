﻿// Decompiled with JetBrains decompiler
// Type: DradisHud
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using BsgoEditor.GUIEditor;
using Gui;
using System.Collections.Generic;
using UnityEngine;

public abstract class DradisHud : Map
{
  protected float radius = 66f;
  private const float SWIPE_INTERVAL = 2f;
  private float currentSwipeTime;
  private Texture2D swipeTexture;
  private GUICountdownNew swipeAnimation;
  protected GUIBlinker friendlyBlinker;
  protected GUIBlinker hostileBlinker;
  protected GUIButtonNew cycleFriendly;
  protected GUIButtonNew cycleHostile;
  private GUIButtonNew cancelTarget;
  private Transform mainCamTransform;
  private static HudIndicatorColorSchemeMode _indicatorColorMode;

  protected float DradisRange
  {
    get
    {
      return Game.Me.Stats.DetectionInnerRadius;
    }
  }

  public DradisHud(AtlasCache atlasCache)
    : base(atlasCache)
  {
    this.mainCamTransform = GameLevel.Instance.cameraSwitcher.GetMainCamera.transform;
    this.swipeAnimation = new GUICountdownNew(this.swipeTexture, 5U, 5U);
    this.swipeAnimation.Parent = this.root;
    JWindowDescription jwindowDescription = BsgoEditor.GUIEditor.Loaders.LoadResource("GUI/Map/Dradis/buttons_layout");
    this.cycleFriendly = (GUIButtonNew) new GUIRoundButton(jwindowDescription["cycleFriendly"] as JButton);
    this.cycleHostile = (GUIButtonNew) new GUIRoundButton(jwindowDescription["cycleEnemy"] as JButton);
    this.cancelTarget = (GUIButtonNew) new GUIRoundButton(jwindowDescription["cancelTarget"] as JButton);
    this.cancelTarget.Handler = (AnonymousDelegate) (() => TargetSelector.SelectTargetRequest((SpaceObject) null, false));
    this.cycleFriendly.SetTooltip("%$bgo.option_buttons.target_friendly%", Action.NearestFriendly);
    this.cycleHostile.SetTooltip("%$bgo.option_buttons.target_enemy%", Action.NearestEnemy);
    this.cancelTarget.SetTooltip("%$bgo.option_buttons.cancel_target%", Action.CancelTarget);
    this.friendlyBlinker = new GUIBlinker("%$bgo.option_buttons.target_friendly%", Action.NearestFriendly);
    this.friendlyBlinker.Width *= 1.6f;
    this.friendlyBlinker.Height *= 1.6f;
    this.hostileBlinker = new GUIBlinker("%$bgo.option_buttons.target_enemy%", Action.NearestEnemy);
    this.hostileBlinker.Width *= 1.6f;
    this.hostileBlinker.Height *= 1.6f;
    this.AddPanel((GUIPanel) this.cycleFriendly);
    this.AddPanel((GUIPanel) this.cycleHostile);
    this.AddPanel((GUIPanel) this.cancelTarget);
    this.cycleFriendly.AddPanel((GUIPanel) this.friendlyBlinker);
    this.cycleHostile.AddPanel((GUIPanel) this.hostileBlinker);
    this.friendlyBlinker.IsRendered = false;
    this.hostileBlinker.IsRendered = false;
    this.handlers.Add(Action.CancelTarget, this.cancelTarget.Handler);
    this.UpdateTargetButtonColors();
  }

  public override void Update()
  {
    base.Update();
    this.currentSwipeTime += Time.deltaTime;
    if ((double) this.currentSwipeTime >= 2.0)
      this.currentSwipeTime = 0.0f;
    this.swipeAnimation.Progress = this.currentSwipeTime / 2f;
  }

  public override void Draw()
  {
    base.Draw();
    this.swipeAnimation.Draw();
  }

  protected override void LoadTextures()
  {
    this.backgroundTexture = (Texture2D) ResourceLoader.Load("GUI/Map/Dradis/background");
    this.swipeTexture = (Texture2D) ResourceLoader.Load("GUI/Map/Dradis/dradis_sphere");
    this.markerTexture = (Texture2D) ResourceLoader.Load("GUI/Map/Dradis/direction_marker");
  }

  public static void SetIndicatorColorMode(HudIndicatorColorSchemeMode scheme)
  {
    DradisHud._indicatorColorMode = scheme;
    DradisManager dradisManager = Game.GUIManager.Find<DradisManager>();
    if (dradisManager == null)
      return;
    dradisManager.UpdateTargetButtonColors();
  }

  private void UpdateTargetButtonColors()
  {
    Texture2D texture2D1 = Resources.Load<Texture2D>("GUI/Map/Dradis/selectfriendly");
    Texture2D texture2D2 = Resources.Load<Texture2D>("GUI/Map/Dradis/selectfriendly_over");
    Texture2D texture2D3 = Resources.Load<Texture2D>("GUI/Map/Dradis/selectfriendly_click");
    Texture2D texture2D4 = Resources.Load<Texture2D>("GUI/Map/Dradis/selectenemy");
    Texture2D texture2D5 = Resources.Load<Texture2D>("GUI/Map/Dradis/selectenemy_over");
    Texture2D texture2D6 = Resources.Load<Texture2D>("GUI/Map/Dradis/selectenemy_click");
    if (DradisHud._indicatorColorMode == HudIndicatorColorSchemeMode.FriendOrFoeColors || DradisHud._indicatorColorMode == HudIndicatorColorSchemeMode.FactionColors && Game.Me.Faction == Faction.Colonial)
    {
      this.cycleFriendly.NormalTexture = texture2D1;
      this.cycleFriendly.OverTexture = texture2D2;
      this.cycleFriendly.PressedTexture = texture2D3;
      this.cycleHostile.NormalTexture = texture2D4;
      this.cycleHostile.OverTexture = texture2D5;
      this.cycleHostile.PressedTexture = texture2D6;
    }
    else
    {
      this.cycleFriendly.NormalTexture = texture2D4;
      this.cycleFriendly.OverTexture = texture2D5;
      this.cycleFriendly.PressedTexture = texture2D6;
      this.cycleHostile.NormalTexture = texture2D1;
      this.cycleHostile.OverTexture = texture2D2;
      this.cycleHostile.PressedTexture = texture2D3;
    }
  }

  protected override float2 CalcScreenPosition(float screenWidth, float screenHeight)
  {
    float num = 45f;
    GuiTicker ticker = Game.Ticker;
    ExperienceBar experienceBar = Game.GUIManager.Find<ExperienceBar>();
    if (ticker != null && experienceBar != null)
      num = (float) ((double) ticker.SizeY + (double) experienceBar.Height + 25.0);
    return new float2(screenWidth / 2f, screenHeight - (float) this.backgroundTexture.height / 2f - num);
  }

  protected void ProcessObjects(List<Map.Object3D> objAroundPlayer, Vector3 playerPosition)
  {
    this.RemoveOldObjects();
    this.GenerateMapObjects(objAroundPlayer, playerPosition);
    this.RecalculateAbsCoords();
  }

  protected void GenerateMapObjects(List<Map.Object3D> objects, Vector3 playerPosition)
  {
    using (List<Map.Object3D>.Enumerator enumerator = objects.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        Map.Object3D current = enumerator.Current;
        float2 float2 = float2.FromV3XZ(this.AccountForAlignment(current.position - playerPosition));
        float2.y = -float2.y;
        float2 centerPosition = float2.normalized * float2.magnitude / this.DradisRange * this.radius;
        if ((int) current.obj.WorldCard.FrameIndex >= 0)
        {
          int frameIndex = (int) current.obj.WorldCard.FrameIndex;
          if (current.obj.IsHostileCongener)
            frameIndex += current.obj.Faction != Faction.Colonial ? -1 : 1;
          AtlasEntry cachedEntryBy = this.atlasCache.GetCachedEntryBy(current.obj.WorldCard.SystemMapTexture, frameIndex);
          bool isTarget = current.obj == SpaceLevel.GetLevel().GetPlayerTarget();
          if (current.type >= Map.ObjectType.FighterFriendly && current.type <= Map.ObjectType.CruiserNeutral)
          {
            bool isPartyMember = false;
            if (current.obj.PlayerRelation == Relation.Friend && current.obj.IsPlayer)
              isPartyMember = Game.Me.Party.IsMember((current.obj as PlayerShip).Player);
            this.ships.Add(new Map.MapObject(this.mapObjectSizeRect, centerPosition, this.root, cachedEntryBy, current.obj, isTarget, isPartyMember));
          }
          else if (current.type == Map.ObjectType.Asteroid || current.type == Map.ObjectType.Debris || current.type == Map.ObjectType.Planetoid)
            this.asteroids.Add(new Map.MapObject(this.mapObjectSizeRect, centerPosition, this.root, cachedEntryBy, current.obj, isTarget, false));
          else if (current.type == Map.ObjectType.MiningShip)
            this.ships.Add(new Map.MapObject(this.mapObjectSizeRect, centerPosition, this.root, cachedEntryBy, current.obj, isTarget, false));
          else if (current.type == Map.ObjectType.JumpBeacon)
            this.ships.Add(new Map.MapObject(this.mapObjectSizeRect, centerPosition, this.root, cachedEntryBy, current.obj, isTarget, false));
        }
      }
    }
  }

  public override void RecalculateAbsCoords()
  {
    base.RecalculateAbsCoords();
    this.swipeAnimation.RecalculateAbsCoords();
  }

  public override bool Contains(float2 point)
  {
    return (double) (this.root.AbsPosition - point).magnitude < (double) this.radius || this.cycleFriendly.Contains(point) || (this.cycleHostile.Contains(point) || this.cancelTarget.Contains(point));
  }

  public void SetBlink(StoryProtocol.ControlType toggler, bool value)
  {
    switch (toggler)
    {
      case StoryProtocol.ControlType.TargetEnemy:
        this.hostileBlinker.IsRendered = value;
        break;
      case StoryProtocol.ControlType.TargetAlly:
        this.friendlyBlinker.IsRendered = value;
        break;
    }
  }

  public Vector3 AccountForAlignment(Vector3 point)
  {
    return (Vector3) (this.mainCamTransform.worldToLocalMatrix * (Vector4) point);
  }
}
