﻿// Decompiled with JetBrains decompiler
// Type: AtlasEntry
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AtlasEntry
{
  private Texture2D texture;
  private Rect frameRect;

  public Texture2D Texture
  {
    get
    {
      return this.texture;
    }
  }

  public Rect FrameRect
  {
    get
    {
      return this.frameRect;
    }
  }

  public AtlasEntry(Texture2D texture, Rect frameRect)
  {
    this.texture = texture;
    this.frameRect = frameRect;
  }
}
