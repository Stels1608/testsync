﻿// Decompiled with JetBrains decompiler
// Type: SettingsGeneralMisc
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System.Collections.Generic;
using UnityEngine;

public class SettingsGeneralMisc : SettingsGeneralGroup, ILocalizeable
{
  [SerializeField]
  private UILabel optionsHeadlineLabel;
  [SerializeField]
  private GameObject content;

  private void Awake()
  {
    this.CoveredSettings.Add(UserSetting.AutomaticAmmoReload);
    this.CoveredSettings.Add(UserSetting.ShowCutscenes);
    using (List<UserSetting>.Enumerator enumerator = this.CoveredSettings.GetEnumerator())
    {
      while (enumerator.MoveNext())
        this.controls.Add(enumerator.Current, (OptionsElement) NguiWidgetFactory.Options.CreateButtonElement(this.content));
    }
    this.content.GetComponent<UITable>().Reposition();
  }

  private void Start()
  {
    this.ReloadLanguageData();
  }

  public void ReloadLanguageData()
  {
    this.optionsHeadlineLabel.text = Tools.ParseMessage("%$bgo.ui.options.general.misc%");
  }
}
