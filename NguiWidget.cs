﻿// Decompiled with JetBrains decompiler
// Type: NguiWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Assets.Scripts.EffectSystem;
using Bigpoint.Core.Mvc.Messaging;
using System.Collections.Generic;
using UnityEngine;

public abstract class NguiWidget : MonoBehaviour
{
  [SerializeField]
  private bool isEnabled = true;
  [SerializeField]
  public EffectCatalogue catalogue = EffectCatalogueFactory.GetInstance();
  public string effectGroupName = string.Empty;
  private NguiMvcRoot mvcRoot;
  [SerializeField]
  private EffectGroup effectGroup;
  [SerializeField]
  public List<EffectTarget> effectTargetList;
  public Dictionary<string, Dictionary<string, List<GameObject>>> effectTargets;
  private TooltipContent tooltip;

  public NguiMvcRoot MvcRoot
  {
    get
    {
      return this.mvcRoot;
    }
    set
    {
      this.mvcRoot = value;
    }
  }

  public virtual bool IsEnabled
  {
    get
    {
      return this.isEnabled;
    }
    set
    {
      this.isEnabled = value;
      this.OnStatusChanged(value);
    }
  }

  public EffectGroup EffectGroup
  {
    get
    {
      return this.effectGroup;
    }
    set
    {
      this.effectGroup = value;
      this.effectGroupName = this.effectGroup.Name;
    }
  }

  public Dictionary<string, Dictionary<string, List<GameObject>>> EffectTargets
  {
    get
    {
      this.effectTargets = new Dictionary<string, Dictionary<string, List<GameObject>>>();
      using (List<EffectTarget>.Enumerator enumerator = this.effectTargetList.GetEnumerator())
      {
        while (enumerator.MoveNext())
        {
          EffectTarget current = enumerator.Current;
          if (!this.effectTargets.ContainsKey(current.eventType))
          {
            this.effectTargets.Add(current.eventType, new Dictionary<string, List<GameObject>>());
            this.effectTargets[current.eventType].Add(current.effectName, new List<GameObject>());
            this.effectTargets[current.eventType][current.effectName].Add(current.target);
          }
          else if (!this.effectTargets[current.eventType].ContainsKey(current.effectName))
          {
            this.effectTargets[current.eventType].Add(current.effectName, new List<GameObject>());
            this.effectTargets[current.eventType][current.effectName].Add(current.target);
          }
          else
            this.effectTargets[current.eventType][current.effectName].Add(current.target);
        }
      }
      return this.effectTargets;
    }
  }

  public TooltipContent Tooltip
  {
    get
    {
      return this.tooltip;
    }
    set
    {
      this.tooltip = value;
    }
  }

  public NguiWidget()
  {
    this.effectTargetList = new List<EffectTarget>();
  }

  public virtual void OnAfterBindingRoot()
  {
  }

  public void BindMvcRoot(NguiMvcRoot root)
  {
    this.MvcRoot = root;
    this.OnAfterBindingRoot();
  }

  public void SendMvcMessage(Message messageId, object data)
  {
    if (messageId == Message.None)
      return;
    FacadeFactory.GetInstance().SendMessage(messageId, data);
  }

  public void SendMvcMessage(Message messageId)
  {
    FacadeFactory.GetInstance().SendMessage(messageId, (object) null);
  }

  public void SendMvcMessage(IMessage<Message> message)
  {
    FacadeFactory.GetInstance().SendMessage(message);
  }

  public virtual void Start()
  {
    if (this.catalogue.catalogueSettings == null)
      this.catalogue.LoadSettings();
    if (!string.IsNullOrEmpty(this.effectGroupName))
      this.effectGroup = this.catalogue.GetEffectGroup(this.effectGroupName);
    this.IsEnabled = this.isEnabled;
  }

  public virtual void OnStatusChanged(bool status)
  {
    this.ExecuteEffect(!status ? "Status_Canged_inactiv" : "Status_Canged_activ");
  }

  public virtual void OnClick()
  {
    if (!this.IsEnabled)
      return;
    this.ExecuteEffect("Click");
  }

  public virtual void OnDoubleClick()
  {
    if (!this.IsEnabled)
      return;
    this.ExecuteEffect("DoubleClick");
  }

  public virtual void OnDrag(Vector2 delta)
  {
    if (!this.IsEnabled)
      return;
    this.ExecuteEffect("Drag");
  }

  public virtual void OnDrop(GameObject drag)
  {
    if (!this.IsEnabled)
      return;
    this.ExecuteEffect("Drop");
  }

  public virtual void OnHover(bool isOver)
  {
    if (!this.IsEnabled)
      return;
    this.ExecuteEffect(!isOver ? "Hover_Rollout" : "Hover_Rollover");
  }

  public virtual void OnInput(string text)
  {
    if (!this.IsEnabled)
      return;
    this.ExecuteEffect("Input");
  }

  public virtual void OnKey(KeyCode key)
  {
    if (!this.IsEnabled)
      return;
    this.ExecuteEffect("Key");
  }

  public virtual void OnPress(bool isDown)
  {
    if (!this.IsEnabled)
      return;
    this.ExecuteEffect(!isDown ? "Press_Up" : "Press_Down");
  }

  public virtual void OnScroll(float delta)
  {
    if (!this.IsEnabled)
      return;
    this.ExecuteEffect("Scroll");
  }

  public virtual void OnSelect(bool selected)
  {
    if (!this.IsEnabled)
      return;
    this.ExecuteEffect(!selected ? "Select_Not" : "Select_Is");
  }

  public virtual void OnTooltip(bool show)
  {
    if (this.tooltip == null)
      return;
    FacadeFactory.GetInstance().SendMessage(!show ? Message.HideTooltip : Message.ShowTooltip, (object) this.tooltip);
  }

  public void ExecuteEffect(string eventType)
  {
    if (this.EffectGroup == null || !this.EffectTargets.ContainsKey(eventType))
      return;
    using (Dictionary<string, List<GameObject>>.Enumerator enumerator = this.EffectTargets[eventType].GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        KeyValuePair<string, List<GameObject>> current = enumerator.Current;
        this.EffectGroup.ExecuteEffectsByEventTypeAndName(current.Value, eventType, current.Key);
      }
    }
  }

  public void RemoveTargetsForEffect(EffectConfig effectConfig)
  {
    string name = effectConfig.Name;
    using (List<EffectTarget>.Enumerator enumerator = new List<EffectTarget>((IEnumerable<EffectTarget>) this.effectTargetList).GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        EffectTarget current = enumerator.Current;
        if (string.CompareOrdinal(current.effectName, name) == 0)
          this.effectTargetList.Remove(current);
      }
    }
  }
}
