﻿// Decompiled with JetBrains decompiler
// Type: LegState
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class LegState
{
  public float cycleTime = 1f;
  public float designatedCycleTimePrev = 0.9f;
  public float liftTime = 0.1f;
  public float liftoffTime = 0.2f;
  public float postliftTime = 0.3f;
  public float prelandTime = 0.7f;
  public float strikeTime = 0.8f;
  public float landTime = 0.9f;
  public List<string> debugHistory = new List<string>();
  public Vector3 stepFromPosition;
  public Vector3 stepToPosition;
  public Vector3 stepToPositionGoal;
  public Matrix4x4 stepFromMatrix;
  public Matrix4x4 stepToMatrix;
  public float stepFromTime;
  public float stepToTime;
  public int stepNr;
  public Vector3 hipReference;
  public Vector3 ankleReference;
  public Vector3 footBase;
  public Quaternion footBaseRotation;
  public Vector3 ankle;
  public float stanceTime;
  public bool parked;
  public Vector3 stancePosition;
  public Vector3 heelToetipVector;

  public float GetFootGrounding(float time)
  {
    if ((double) time <= (double) this.liftTime || (double) time >= (double) this.landTime)
      return 0.0f;
    if ((double) time >= (double) this.postliftTime && (double) time <= (double) this.prelandTime)
      return 1f;
    if ((double) time < (double) this.postliftTime)
      return (float) (((double) time - (double) this.liftTime) / ((double) this.postliftTime - (double) this.liftTime));
    return (float) (1.0 - ((double) time - (double) this.prelandTime) / ((double) this.landTime - (double) this.prelandTime));
  }
}
