﻿// Decompiled with JetBrains decompiler
// Type: ShipType
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using System.ComponentModel;

public enum ShipType
{
  Undefined = 0,
  [Description("humant1fighter")] ViperMkII = 10,
  [Description("humant1command")] Raptor = 11,
  [Description("humant1scout")] RaptorFR = 12,
  [Description("galactica")] Galactica = 50,
  [Description("cylont1fighter")] Raider = 110,
  [Description("cylont1command")] HeavyRaider = 111,
  [Description("basestar")] Basestar = 151,
  [Description("cylon_drone_small")] DroneSmallDps = 210,
}
