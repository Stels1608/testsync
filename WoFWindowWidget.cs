﻿// Decompiled with JetBrains decompiler
// Type: WoFWindowWidget
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;
using System;
using System.Collections.Generic;
using UnityEngine;

public class WoFWindowWidget : ContentWindowWidget
{
  public UILabel windowTitle;
  private OneTwoButtonLayout layout;
  private WoFGamePanel dradisGame;
  private WofBonusMapPanel mapPanel;
  private WofRewardLog rewardLog;
  private OneButtonBar oneButtonBar;
  private WofDrawReply drawReply;
  public AnonymousDelegate closeDelegate;

  public void Awake()
  {
    this.type = WindowTypes.WoFWindow;
    this.InitContent();
  }

  public override void Start()
  {
    base.Start();
    PositionUtils.CenterWindowHorizontally((WindowWidget) this);
    this.ReloadLanguageData();
  }

  private void InitContent()
  {
    GameObject child = new GameObject("layout") { layer = this.gameObject.layer };
    this.layout = child.AddComponent<OneTwoButtonLayout>();
    PositionUtils.CorrectTransform(this.gameObject, child);
    child.transform.localPosition = new Vector3(0.0f, -36f, 0.0f);
    GameObject gameObject1 = NGUITools.AddChild(this.layout.ContentArea[0], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/WoF/WoFGame"));
    this.dradisGame = gameObject1.GetComponent<WoFGamePanel>();
    gameObject1.layer = this.layout.gameObject.layer;
    if ((UnityEngine.Object) gameObject1 == (UnityEngine.Object) null)
      throw new ArgumentNullException("woFContent");
    GameObject gameObject2 = NGUITools.AddChild(this.layout.ContentArea[1], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/WoF/WofBonusMap"));
    this.mapPanel = gameObject2.GetComponent<WofBonusMapPanel>();
    if ((UnityEngine.Object) gameObject2 == (UnityEngine.Object) null)
      throw new ArgumentNullException("bonusMapContent");
    GameObject gameObject3 = NGUITools.AddChild(this.layout.ContentArea[2], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/WoF/WofRewardLog"));
    this.rewardLog = gameObject3.GetComponent<WofRewardLog>();
    if ((UnityEngine.Object) gameObject3 == (UnityEngine.Object) null)
      throw new ArgumentNullException("rewardLogContent");
    GameObject gameObject4 = NGUITools.AddChild(this.layout.ContentArea[3], (GameObject) Resources.Load("GUI/gui_2013/ui_elements/buttonbar/OneButtonBar"));
    gameObject4.layer = this.layout.gameObject.layer;
    if ((UnityEngine.Object) gameObject4 == (UnityEngine.Object) null)
      throw new ArgumentNullException("buttonBar");
    this.oneButtonBar = gameObject4.GetComponent<OneButtonBar>();
    this.oneButtonBar.Init((int) this.WindowSize.x - 40);
    this.oneButtonBar.SetButtonPositionX(this.WindowSize.x - 92f);
    this.oneButtonBar.SetButtonDelegate(new AnonymousDelegate(this.CloseOnButtonClick), Message.None);
    this.CloseButton.handleClick = new AnonymousDelegate(this.CloseOnButtonClick);
    this.CloseButton.IsEnabled = true;
  }

  public override void Close()
  {
    this.dradisGame.forceStop = true;
    this.dradisGame.ResetGame();
    base.Close();
  }

  private void CloseOnButtonClick()
  {
    if (this.closeDelegate != null)
      this.closeDelegate();
    this.Close();
  }

  public void ShowRewad(WofDrawReply reply)
  {
    this.drawReply = reply;
    this.Invoke("DelayShowReward", 1f);
  }

  public void DelayShowReward()
  {
    if (!this.drawReply.bonusMapCards.TrueForAll((Predicate<WofBonusMapPart>) (part => (bool) part.Guicard.IsLoaded)) && !this.drawReply.rewardItems.TrueForAll((Predicate<WofRewardItemContainer>) (part => (bool) part.itemCard.IsLoaded)))
    {
      this.Invoke("DelayShowReward", 0.1f);
    }
    else
    {
      if (this.dradisGame.gameObject.activeSelf)
        this.dradisGame.ShowReward(this.drawReply);
      this.rewardLog.AddRewardMessage(this.drawReply);
    }
  }

  public void SetDradisGameDetails(WofJackpotContainer jackpotItem, bool free, List<int> costs)
  {
    this.dradisGame.SetGameDetails(jackpotItem, free, costs);
  }

  public void UpdateBonusMapParts(Dictionary<int, List<int>> dict)
  {
    this.mapPanel.UpdateBonusMapParts(dict);
  }

  public void UpdateLevel(byte level)
  {
    this.mapPanel.UpdateLevel(level);
  }

  public void UpdateCubits(uint cubits)
  {
    this.dradisGame.UpdateCubits(cubits);
  }

  public override void ReloadLanguageData()
  {
    base.ReloadLanguageData();
    this.windowTitle.text = Tools.ParseMessage("%$bgo.wof.headline%");
  }

  public void SetVisibleBonusLevel(List<int> visibleLevel)
  {
    this.mapPanel.SetVisibleBonusLevel(visibleLevel);
  }

  public override void OnWindowOpen()
  {
    base.OnWindowOpen();
    this.UpdateCubits(Game.Me.Hold.Cubits);
  }

  public void UpdateMapInformation(Dictionary<int, GUICard> dictionary)
  {
    this.dradisGame.UpdateMapInformation(dictionary);
  }

  public void DisableFtlMissions()
  {
    this.mapPanel.DisableFtlMissions();
  }

  public void SetWofConfirmation(bool p)
  {
    this.dradisGame.SetWofConfirmation(p);
  }

  private void Update()
  {
    this.mapPanel.BlockedByActiveGame(this.dradisGame.ActiveGame());
  }
}
