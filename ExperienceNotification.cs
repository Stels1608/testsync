﻿// Decompiled with JetBrains decompiler
// Type: ExperienceNotification
// Assembly: Assembly-CSharp, Version=1.0.3.0, Culture=neutral, PublicKeyToken=null
// MVID: A4016130-F2DB-4F13-9FAB-0C36E930620D
// Assembly location: E:\Games\BSGO\client\live\bsgo_Data\Managed\Assembly-CSharp.dll

using Gui;

public class ExperienceNotification : OnScreenNotification
{
  private readonly ExperienceNotification.ExperienceType experienceType;
  private readonly int amount;
  private readonly NotificationCategory category;

  public int Amount
  {
    get
    {
      return this.amount;
    }
  }

  public ExperienceNotification.ExperienceType ExperienceMessageType
  {
    get
    {
      return this.experienceType;
    }
  }

  public override OnScreenNotificationType NotificationType
  {
    get
    {
      return OnScreenNotificationType.Experience;
    }
  }

  public override NotificationCategory Category
  {
    get
    {
      return NotificationCategory.Neutral;
    }
  }

  public override string TextMessage
  {
    get
    {
      return BsgoLocalization.Get("%$bgo.notif.xp_gained%", (object) this.Amount);
    }
  }

  public override bool Show
  {
    get
    {
      return OnScreenNotification.ShowExperienceMessages;
    }
  }

  public ExperienceNotification(byte experienceType, int amount)
  {
    this.experienceType = (ExperienceNotification.ExperienceType) experienceType;
    this.amount = amount;
  }

  public enum ExperienceType : byte
  {
    None,
    LootFull,
    LootReduced,
    LootMin,
  }
}
